package org.openintents.openpgp.util;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.atomic.AtomicInteger;
import org.openintents.openpgp.IOpenPgpService2;
import org.openintents.openpgp.OpenPgpError;
import org.openintents.openpgp.util.ParcelFileDescriptorUtil;

public class OpenPgpApi {
    public static final String ACTION_CHECK_PERMISSION = "org.openintents.openpgp.action.CHECK_PERMISSION";
    public static final String ACTION_CLEARTEXT_SIGN = "org.openintents.openpgp.action.CLEARTEXT_SIGN";
    public static final String ACTION_DECRYPT_METADATA = "org.openintents.openpgp.action.DECRYPT_METADATA";
    public static final String ACTION_DECRYPT_VERIFY = "org.openintents.openpgp.action.DECRYPT_VERIFY";
    public static final String ACTION_DETACHED_SIGN = "org.openintents.openpgp.action.DETACHED_SIGN";
    public static final String ACTION_ENCRYPT = "org.openintents.openpgp.action.ENCRYPT";
    public static final String ACTION_GET_KEY = "org.openintents.openpgp.action.GET_KEY";
    public static final String ACTION_GET_KEY_IDS = "org.openintents.openpgp.action.GET_KEY_IDS";
    public static final String ACTION_GET_SIGN_KEY_ID = "org.openintents.openpgp.action.GET_SIGN_KEY_ID";
    public static final String ACTION_SIGN = "org.openintents.openpgp.action.SIGN";
    public static final String ACTION_SIGN_AND_ENCRYPT = "org.openintents.openpgp.action.SIGN_AND_ENCRYPT";
    public static final int API_VERSION = 10;
    public static final String EXTRA_ACCOUNT_NAME = "account_name";
    public static final String EXTRA_API_VERSION = "api_version";
    public static final String EXTRA_CALL_UUID1 = "call_uuid1";
    public static final String EXTRA_CALL_UUID2 = "call_uuid2";
    public static final String EXTRA_DATA_LENGTH = "data_length";
    public static final String EXTRA_DECRYPTION_RESULT = "decryption_result";
    public static final String EXTRA_DETACHED_SIGNATURE = "detached_signature";
    public static final String EXTRA_ENABLE_COMPRESSION = "enable_compression";
    public static final String EXTRA_ENCRYPT_OPPORTUNISTIC = "opportunistic";
    public static final String EXTRA_KEY_ID = "key_id";
    public static final String EXTRA_KEY_IDS = "key_ids";
    public static final String EXTRA_ORIGINAL_FILENAME = "original_filename";
    public static final String EXTRA_PASSPHRASE = "passphrase";
    public static final String EXTRA_PROGRESS_MESSENGER = "progress_messenger";
    public static final String EXTRA_REQUEST_ASCII_ARMOR = "ascii_armor";
    public static final String EXTRA_SENDER_ADDRESS = "sender_address";
    public static final String EXTRA_SIGN_KEY_ID = "sign_key_id";
    public static final String EXTRA_USER_ID = "user_id";
    public static final String EXTRA_USER_IDS = "user_ids";
    public static final String RESULT_CHARSET = "charset";
    public static final String RESULT_CODE = "result_code";
    public static final int RESULT_CODE_ERROR = 0;
    public static final int RESULT_CODE_SUCCESS = 1;
    public static final int RESULT_CODE_USER_INTERACTION_REQUIRED = 2;
    public static final String RESULT_DECRYPTION = "decryption";
    public static final String RESULT_DETACHED_SIGNATURE = "detached_signature";
    public static final String RESULT_ERROR = "error";
    public static final String RESULT_INTENT = "intent";
    public static final String RESULT_KEY_IDS = "key_ids";
    public static final String RESULT_METADATA = "metadata";
    public static final String RESULT_SIGNATURE = "signature";
    public static final String RESULT_SIGNATURE_MICALG = "signature_micalg";
    public static final String SERVICE_INTENT_2 = "org.openintents.openpgp.IOpenPgpService2";
    public static final String TAG = "OpenPgp API";
    Context mContext;
    final AtomicInteger mPipeIdGen = new AtomicInteger();
    IOpenPgpService2 mService;

    public interface CancelableBackgroundOperation {
        void cancelOperation();
    }

    public interface IOpenPgpCallback {
        void onReturn(Intent intent);
    }

    public interface IOpenPgpSinkResultCallback<T> {
        void onProgress(int i, int i2);

        void onReturn(Intent intent, T t);
    }

    public interface OpenPgpDataSink<T> {
        T processData(InputStream inputStream) throws IOException;
    }

    public interface PermissionPingCallback {
        void onPgpPermissionCheckResult(Intent intent);
    }

    public OpenPgpApi(Context context, IOpenPgpService2 service) {
        this.mContext = context;
        this.mService = service;
    }

    private class OpenPgpSourceSinkAsyncTask<T> extends AsyncTask<Void, Integer, OpenPgpDataResult<T>> implements CancelableBackgroundOperation {
        IOpenPgpSinkResultCallback<T> callback;
        Intent data;
        OpenPgpDataSink<T> dataSink;
        OpenPgpDataSource dataSource;

        private OpenPgpSourceSinkAsyncTask(Intent data2, OpenPgpDataSource dataSource2, OpenPgpDataSink<T> dataSink2, IOpenPgpSinkResultCallback<T> callback2) {
            this.data = data2;
            this.dataSource = dataSource2;
            this.dataSink = dataSink2;
            this.callback = callback2;
        }

        /* access modifiers changed from: protected */
        public OpenPgpDataResult<T> doInBackground(Void... unused) {
            return OpenPgpApi.this.executeApi(this.data, this.dataSource, this.dataSink);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(OpenPgpDataResult<T> result) {
            this.callback.onReturn(result.apiResult, result.sinkResult);
        }

        public void cancelOperation() {
            cancel(true);
            if (this.dataSource != null) {
                this.dataSource.cancel();
            }
        }
    }

    private class OpenPgpAsyncTask extends AsyncTask<Void, Integer, Intent> {
        IOpenPgpCallback callback;
        Intent data;
        InputStream is;
        OutputStream os;

        private OpenPgpAsyncTask(Intent data2, InputStream is2, OutputStream os2, IOpenPgpCallback callback2) {
            this.data = data2;
            this.is = is2;
            this.os = os2;
            this.callback = callback2;
        }

        /* access modifiers changed from: protected */
        public Intent doInBackground(Void... unused) {
            return OpenPgpApi.this.executeApi(this.data, this.is, this.os);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Intent result) {
            this.callback.onReturn(result);
        }
    }

    public <T> CancelableBackgroundOperation executeApiAsync(Intent data, OpenPgpDataSource dataSource, OpenPgpDataSink<T> dataSink, final IOpenPgpSinkResultCallback<T> callback) {
        data.putExtra(EXTRA_PROGRESS_MESSENGER, new Messenger(new Handler(new Handler.Callback() {
            public boolean handleMessage(Message message) {
                callback.onProgress(message.arg1, message.arg2);
                return true;
            }
        })));
        OpenPgpSourceSinkAsyncTask<T> task = new OpenPgpSourceSinkAsyncTask<>(data, dataSource, dataSink, callback);
        if (Build.VERSION.SDK_INT >= 11) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
        } else {
            task.execute((Object[]) null);
        }
        return task;
    }

    public AsyncTask executeApiAsync(Intent data, OpenPgpDataSource dataSource, IOpenPgpSinkResultCallback<Void> callback) {
        OpenPgpSourceSinkAsyncTask<Void> task = new OpenPgpSourceSinkAsyncTask<>(data, dataSource, null, callback);
        if (Build.VERSION.SDK_INT >= 11) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
        } else {
            task.execute((Object[]) null);
        }
        return task;
    }

    public void executeApiAsync(Intent data, InputStream is, OutputStream os, IOpenPgpCallback callback) {
        OpenPgpAsyncTask task = new OpenPgpAsyncTask(data, is, os, callback);
        if (Build.VERSION.SDK_INT >= 11) {
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
        } else {
            task.execute((Object[]) null);
        }
    }

    public static class OpenPgpDataResult<T> {
        Intent apiResult;
        T sinkResult;

        public OpenPgpDataResult(Intent apiResult2, T sinkResult2) {
            this.apiResult = apiResult2;
            this.sinkResult = sinkResult2;
        }
    }

    public <T> OpenPgpDataResult<T> executeApi(Intent data, OpenPgpDataSource dataSource, OpenPgpDataSink<T> dataSink) {
        OpenPgpDataResult<T> openPgpDataResult;
        ParcelFileDescriptor input = null;
        ParcelFileDescriptor output = null;
        if (dataSource != null) {
            try {
                Long expectedSize = dataSource.getSizeForProgress();
                if (expectedSize != null) {
                    data.putExtra(EXTRA_DATA_LENGTH, expectedSize.longValue());
                } else {
                    data.removeExtra(EXTRA_PROGRESS_MESSENGER);
                }
                input = dataSource.startPumpThread();
            } catch (Exception e) {
                Log.e(TAG, "Exception in executeApi call", e);
                Intent result = new Intent();
                result.putExtra(RESULT_CODE, 0);
                result.putExtra(RESULT_ERROR, new OpenPgpError(-1, e.getMessage()));
                openPgpDataResult = new OpenPgpDataResult<>(result, null);
                closeLoudly(output);
            } catch (Throwable th) {
                closeLoudly(output);
                throw th;
            }
        }
        ParcelFileDescriptorUtil.DataSinkTransferThread<T> pumpThread = null;
        int outputPipeId = 0;
        if (dataSink != null) {
            outputPipeId = this.mPipeIdGen.incrementAndGet();
            output = this.mService.createOutputPipe(outputPipeId);
            pumpThread = ParcelFileDescriptorUtil.asyncPipeToDataSink(dataSink, output);
        }
        Intent result2 = executeApi(data, input, outputPipeId);
        if (pumpThread == null) {
            openPgpDataResult = new OpenPgpDataResult<>(result2, null);
            closeLoudly(output);
        } else {
            pumpThread.join();
            openPgpDataResult = new OpenPgpDataResult<>(result2, pumpThread.getResult());
            closeLoudly(output);
        }
        return openPgpDataResult;
    }

    public Intent executeApi(Intent data, InputStream is, OutputStream os) {
        Intent result;
        ParcelFileDescriptor input = null;
        ParcelFileDescriptor output = null;
        if (is != null) {
            try {
                input = ParcelFileDescriptorUtil.pipeFrom(is);
            } catch (Exception e) {
                Log.e(TAG, "Exception in executeApi call", e);
                result = new Intent();
                result.putExtra(RESULT_CODE, 0);
                result.putExtra(RESULT_ERROR, new OpenPgpError(-1, e.getMessage()));
                closeLoudly(null);
            } catch (Throwable th) {
                closeLoudly(null);
                throw th;
            }
        }
        Thread pumpThread = null;
        int outputPipeId = 0;
        if (os != null) {
            outputPipeId = this.mPipeIdGen.incrementAndGet();
            output = this.mService.createOutputPipe(outputPipeId);
            pumpThread = ParcelFileDescriptorUtil.pipeTo(os, output);
        }
        result = executeApi(data, input, outputPipeId);
        if (pumpThread != null) {
            pumpThread.join();
        }
        closeLoudly(output);
        return result;
    }

    public static abstract class OpenPgpDataSource {
        private boolean isCancelled;
        private ParcelFileDescriptor writeSidePfd;

        public abstract void writeTo(OutputStream outputStream) throws IOException;

        public Long getSizeForProgress() {
            return null;
        }

        public boolean isCancelled() {
            return this.isCancelled;
        }

        /* access modifiers changed from: private */
        public ParcelFileDescriptor startPumpThread() throws IOException {
            if (this.writeSidePfd != null) {
                throw new IllegalStateException("startPumpThread() must only be called once!");
            }
            ParcelFileDescriptor[] pipe = ParcelFileDescriptor.createPipe();
            ParcelFileDescriptor readSidePfd = pipe[0];
            this.writeSidePfd = pipe[1];
            new ParcelFileDescriptorUtil.DataSourceTransferThread(this, new ParcelFileDescriptor.AutoCloseOutputStream(this.writeSidePfd)).start();
            return readSidePfd;
        }

        /* access modifiers changed from: private */
        public void cancel() {
            this.isCancelled = true;
            try {
                this.writeSidePfd.close();
            } catch (IOException e) {
            }
        }
    }

    public Intent executeApi(Intent data, OpenPgpDataSource dataSource, OutputStream os) {
        ParcelFileDescriptor input = null;
        if (dataSource != null) {
            try {
                Long expectedSize = dataSource.getSizeForProgress();
                if (expectedSize != null) {
                    data.putExtra(EXTRA_DATA_LENGTH, expectedSize.longValue());
                } else {
                    data.removeExtra(EXTRA_PROGRESS_MESSENGER);
                }
                input = dataSource.startPumpThread();
            } catch (Exception e) {
                Log.e(TAG, "Exception in executeApi call", e);
                Intent result = new Intent();
                result.putExtra(RESULT_CODE, 0);
                result.putExtra(RESULT_ERROR, new OpenPgpError(-1, e.getMessage()));
                return result;
            }
        }
        Thread pumpThread = null;
        int outputPipeId = 0;
        if (os != null) {
            outputPipeId = this.mPipeIdGen.incrementAndGet();
            pumpThread = ParcelFileDescriptorUtil.pipeTo(os, this.mService.createOutputPipe(outputPipeId));
        }
        Intent result2 = executeApi(data, input, outputPipeId);
        if (pumpThread == null) {
            return result2;
        }
        pumpThread.join();
        return result2;
    }

    private Intent executeApi(Intent data, ParcelFileDescriptor input, int outputPipeId) {
        Intent result;
        try {
            data.putExtra(EXTRA_API_VERSION, 10);
            result = this.mService.execute(data, input, outputPipeId);
            result.setExtrasClassLoader(this.mContext.getClassLoader());
        } catch (Exception e) {
            Log.e(TAG, "Exception in executeApi call", e);
            result = new Intent();
            result.putExtra(RESULT_CODE, 0);
            result.putExtra(RESULT_ERROR, new OpenPgpError(-1, e.getMessage()));
        } finally {
            closeLoudly(input);
        }
        return result;
    }

    private static void closeLoudly(ParcelFileDescriptor input) {
        if (input != null) {
            try {
                input.close();
            } catch (IOException e) {
                Log.e(TAG, "IOException when closing ParcelFileDescriptor!", e);
            }
        }
    }

    public void checkPermissionPing(final PermissionPingCallback permissionPingCallback) {
        executeApiAsync(new Intent(ACTION_CHECK_PERMISSION), (InputStream) null, (OutputStream) null, new IOpenPgpCallback() {
            public void onReturn(Intent result) {
                permissionPingCallback.onPgpPermissionCheckResult(result);
            }
        });
    }
}
