package org.openintents.openpgp.util;

import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.system.ErrnoException;
import android.system.OsConstants;
import android.util.Log;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.openintents.openpgp.util.OpenPgpApi;

public class ParcelFileDescriptorUtil {
    public static ParcelFileDescriptor pipeFrom(InputStream inputStream) throws IOException {
        ParcelFileDescriptor[] pipe = ParcelFileDescriptor.createPipe();
        ParcelFileDescriptor readSide = pipe[0];
        new TransferThread(inputStream, new ParcelFileDescriptor.AutoCloseOutputStream(pipe[1])).start();
        return readSide;
    }

    public static TransferThread pipeTo(OutputStream outputStream, ParcelFileDescriptor output) throws IOException {
        TransferThread t = new TransferThread(new ParcelFileDescriptor.AutoCloseInputStream(output), outputStream);
        t.start();
        return t;
    }

    static class TransferThread extends Thread {
        final InputStream mIn;
        final OutputStream mOut;

        TransferThread(InputStream in, OutputStream out) {
            super("IPC Transfer Thread");
            this.mIn = in;
            this.mOut = out;
            setDaemon(true);
        }

        public void run() {
            byte[] buf = new byte[4096];
            while (true) {
                try {
                    int len = this.mIn.read(buf);
                    if (len > 0) {
                        this.mOut.write(buf, 0, len);
                    } else {
                        try {
                            break;
                        } catch (IOException e) {
                        }
                    }
                } catch (IOException e2) {
                    Log.e(OpenPgpApi.TAG, "IOException when writing to out", e2);
                    try {
                        this.mIn.close();
                    } catch (IOException e3) {
                    }
                    try {
                        this.mOut.close();
                        return;
                    } catch (IOException e4) {
                        return;
                    }
                } catch (Throwable th) {
                    try {
                        this.mIn.close();
                    } catch (IOException e5) {
                    }
                    try {
                        this.mOut.close();
                    } catch (IOException e6) {
                    }
                    throw th;
                }
            }
            this.mIn.close();
            try {
                this.mOut.close();
            } catch (IOException e7) {
            }
        }
    }

    public static <T> DataSinkTransferThread<T> asyncPipeToDataSink(OpenPgpApi.OpenPgpDataSink<T> dataSink, ParcelFileDescriptor output) throws IOException {
        DataSinkTransferThread<T> dataSinkTransferThread = new DataSinkTransferThread<>(dataSink, new BufferedInputStream(new ParcelFileDescriptor.AutoCloseInputStream(output)));
        dataSinkTransferThread.start();
        return dataSinkTransferThread;
    }

    static class DataSourceTransferThread extends Thread {
        final OpenPgpApi.OpenPgpDataSource dataSource;
        final OutputStream outputStream;

        DataSourceTransferThread(OpenPgpApi.OpenPgpDataSource dataSource2, OutputStream outputStream2) {
            super("IPC Transfer Thread (TO service)");
            this.dataSource = dataSource2;
            this.outputStream = outputStream2;
            setDaemon(true);
        }

        public void run() {
            try {
                this.dataSource.writeTo(this.outputStream);
                try {
                    this.outputStream.close();
                } catch (IOException e) {
                }
            } catch (IOException e2) {
                if (this.dataSource.isCancelled()) {
                    Log.d(OpenPgpApi.TAG, "Stopped writing because operation was cancelled.");
                } else if (ParcelFileDescriptorUtil.isIOExceptionCausedByEPIPE(e2)) {
                    Log.d(OpenPgpApi.TAG, "Stopped writing due to broken pipe (other end closed pipe?)");
                } else {
                    Log.e(OpenPgpApi.TAG, "IOException when writing to out", e2);
                }
                try {
                    this.outputStream.close();
                } catch (IOException e3) {
                }
            } catch (Throwable th) {
                try {
                    this.outputStream.close();
                } catch (IOException e4) {
                }
                throw th;
            }
        }
    }

    /* access modifiers changed from: private */
    public static boolean isIOExceptionCausedByEPIPE(IOException e) {
        if (Build.VERSION.SDK_INT < 21) {
            return e.getMessage().contains("EPIPE");
        }
        Throwable cause = e.getCause();
        return (cause instanceof ErrnoException) && ((ErrnoException) cause).errno == OsConstants.EPIPE;
    }

    static class DataSinkTransferThread<T> extends Thread {
        final OpenPgpApi.OpenPgpDataSink<T> dataSink;
        final InputStream inputStream;
        T sinkResult;

        DataSinkTransferThread(OpenPgpApi.OpenPgpDataSink<T> dataSink2, InputStream inputStream2) {
            super("IPC Transfer Thread (FROM service)");
            this.dataSink = dataSink2;
            this.inputStream = inputStream2;
            setDaemon(true);
        }

        public void run() {
            try {
                this.sinkResult = this.dataSink.processData(this.inputStream);
                try {
                    this.inputStream.close();
                } catch (IOException e) {
                }
            } catch (IOException e2) {
                if (ParcelFileDescriptorUtil.isIOExceptionCausedByEPIPE(e2)) {
                    Log.e(OpenPgpApi.TAG, "Stopped read due to broken pipe (other end closed pipe?)");
                } else {
                    Log.e(OpenPgpApi.TAG, "IOException while reading from in", e2);
                }
                this.sinkResult = null;
                try {
                    this.inputStream.close();
                } catch (IOException e3) {
                }
            } catch (Throwable th) {
                try {
                    this.inputStream.close();
                } catch (IOException e4) {
                }
                throw th;
            }
        }

        /* access modifiers changed from: package-private */
        public T getResult() {
            if (!isAlive()) {
                return this.sinkResult;
            }
            throw new IllegalStateException("result must be accessed only *after* the thread finished execution!");
        }
    }
}
