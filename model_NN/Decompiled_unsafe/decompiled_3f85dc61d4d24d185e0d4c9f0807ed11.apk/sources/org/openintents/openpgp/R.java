package org.openintents.openpgp;

public final class R {

    public static final class drawable {
        public static final int ic_action_cancel_launchersize = 2130837672;
        public static final int ic_action_cancel_launchersize_light = 2130837673;
    }

    public static final class string {
        public static final int openpgp_install_openkeychain_via = 2131231584;
        public static final int openpgp_key_selected = 2131231612;
        public static final int openpgp_list_preference_none = 2131231585;
        public static final int openpgp_no_key_selected = 2131231613;
    }
}
