package org.openintents.openpgp;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.openintents.openpgp.util.OpenPgpUtils;

public class OpenPgpSignatureResult implements Parcelable {
    public static final Parcelable.Creator<OpenPgpSignatureResult> CREATOR = new Parcelable.Creator<OpenPgpSignatureResult>() {
        public OpenPgpSignatureResult createFromParcel(Parcel source) {
            int version = source.readInt();
            int parcelableSize = source.readInt();
            int startPosition = source.dataPosition();
            OpenPgpSignatureResult vr = new OpenPgpSignatureResult(source, version);
            source.setDataPosition(startPosition + parcelableSize);
            return vr;
        }

        public OpenPgpSignatureResult[] newArray(int size) {
            return new OpenPgpSignatureResult[size];
        }
    };
    public static final int PARCELABLE_VERSION = 3;
    @Deprecated
    public static final int RESULT_INVALID_INSECURE = 6;
    public static final int RESULT_INVALID_KEY_EXPIRED = 5;
    public static final int RESULT_INVALID_KEY_INSECURE = 6;
    public static final int RESULT_INVALID_KEY_REVOKED = 4;
    public static final int RESULT_INVALID_SIGNATURE = 0;
    public static final int RESULT_KEY_MISSING = 2;
    public static final int RESULT_NO_SIGNATURE = -1;
    @Deprecated
    public static final int RESULT_VALID_CONFIRMED = 1;
    public static final int RESULT_VALID_KEY_CONFIRMED = 1;
    public static final int RESULT_VALID_KEY_UNCONFIRMED = 3;
    @Deprecated
    public static final int RESULT_VALID_UNCONFIRMED = 3;
    public static final int SENDER_RESULT_NO_SENDER = 0;
    public static final int SENDER_RESULT_UID_CONFIRMED = 1;
    public static final int SENDER_RESULT_UID_MISSING = 3;
    public static final int SENDER_RESULT_UID_UNCONFIRMED = 2;
    private final ArrayList<String> confirmedUserIds;
    private final long keyId;
    private final String primaryUserId;
    private final int result;
    private final int senderResult;
    private final ArrayList<String> userIds;

    private OpenPgpSignatureResult(int signatureStatus, String signatureUserId, long keyId2, ArrayList<String> userIds2, ArrayList<String> confirmedUserIds2, int senderResult2, Boolean signatureOnly) {
        this.result = signatureStatus;
        this.primaryUserId = signatureUserId;
        this.keyId = keyId2;
        this.userIds = userIds2;
        this.confirmedUserIds = confirmedUserIds2;
        this.senderResult = senderResult2;
    }

    private OpenPgpSignatureResult(Parcel source, int version) {
        this.result = source.readInt();
        source.readByte();
        this.primaryUserId = source.readString();
        this.keyId = source.readLong();
        if (version > 1) {
            this.userIds = source.createStringArrayList();
        } else {
            this.userIds = null;
        }
        if (version > 2) {
            this.senderResult = source.readInt();
            this.confirmedUserIds = source.createStringArrayList();
            return;
        }
        this.senderResult = 0;
        this.confirmedUserIds = null;
    }

    public int getResult() {
        return this.result;
    }

    public int getSenderResult() {
        return this.senderResult;
    }

    public String getPrimaryUserId() {
        return this.primaryUserId;
    }

    public List<String> getUserIds() {
        return Collections.unmodifiableList(this.userIds);
    }

    public List<String> getConfirmedUserIds() {
        return Collections.unmodifiableList(this.confirmedUserIds);
    }

    public long getKeyId() {
        return this.keyId;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(3);
        int sizePosition = dest.dataPosition();
        dest.writeInt(0);
        int startPosition = dest.dataPosition();
        dest.writeInt(this.result);
        dest.writeByte((byte) 0);
        dest.writeString(this.primaryUserId);
        dest.writeLong(this.keyId);
        dest.writeStringList(this.userIds);
        dest.writeInt(this.senderResult);
        dest.writeStringList(this.confirmedUserIds);
        int parcelableSize = dest.dataPosition() - startPosition;
        dest.setDataPosition(sizePosition);
        dest.writeInt(parcelableSize);
        dest.setDataPosition(startPosition + parcelableSize);
    }

    public String toString() {
        return ((("\nresult: " + this.result) + "\nprimaryUserId: " + this.primaryUserId) + "\nuserIds: " + this.userIds) + "\nkeyId: " + OpenPgpUtils.convertKeyIdToHex(this.keyId);
    }

    public static OpenPgpSignatureResult createWithValidSignature(int signatureStatus, String primaryUserId2, long keyId2, ArrayList<String> userIds2, ArrayList<String> confirmedUserIds2, int senderStatus) {
        if (signatureStatus != -1 && signatureStatus != 2 && signatureStatus != 0) {
            return new OpenPgpSignatureResult(signatureStatus, primaryUserId2, keyId2, userIds2, confirmedUserIds2, senderStatus, null);
        }
        throw new IllegalArgumentException("can only use this method for valid types of signatures");
    }

    public static OpenPgpSignatureResult createWithNoSignature() {
        return new OpenPgpSignatureResult(-1, null, 0, null, null, 0, null);
    }

    public static OpenPgpSignatureResult createWithKeyMissing(long keyId2) {
        return new OpenPgpSignatureResult(2, null, keyId2, null, null, 0, null);
    }

    public static OpenPgpSignatureResult createWithInvalidSignature() {
        return new OpenPgpSignatureResult(0, null, 0, null, null, 0, null);
    }

    @Deprecated
    public OpenPgpSignatureResult withSignatureOnlyFlag(boolean signatureOnly) {
        return new OpenPgpSignatureResult(this.result, this.primaryUserId, this.keyId, this.userIds, this.confirmedUserIds, this.senderResult, Boolean.valueOf(signatureOnly));
    }
}
