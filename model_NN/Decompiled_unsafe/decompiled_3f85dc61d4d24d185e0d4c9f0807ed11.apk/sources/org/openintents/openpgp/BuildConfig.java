package org.openintents.openpgp;

public final class BuildConfig {
    public static final String APPLICATION_ID = "org.openintents.openpgp";
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String FLAVOR = "";
    public static final int VERSION_CODE = 8;
    public static final String VERSION_NAME = "9.0";
}
