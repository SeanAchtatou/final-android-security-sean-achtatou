package org.openintents.openpgp;

import android.os.Parcel;
import android.os.Parcelable;

public class OpenPgpError implements Parcelable {
    public static final int CLIENT_SIDE_ERROR = -1;
    public static final Parcelable.Creator<OpenPgpError> CREATOR = new Parcelable.Creator<OpenPgpError>() {
        public OpenPgpError createFromParcel(Parcel source) {
            source.readInt();
            int parcelableSize = source.readInt();
            int startPosition = source.dataPosition();
            OpenPgpError error = new OpenPgpError();
            error.errorId = source.readInt();
            error.message = source.readString();
            source.setDataPosition(startPosition + parcelableSize);
            return error;
        }

        public OpenPgpError[] newArray(int size) {
            return new OpenPgpError[size];
        }
    };
    public static final int GENERIC_ERROR = 0;
    public static final int INCOMPATIBLE_API_VERSIONS = 1;
    public static final int NO_OR_WRONG_PASSPHRASE = 2;
    public static final int NO_USER_IDS = 3;
    public static final int OPPORTUNISTIC_MISSING_KEYS = 4;
    public static final int PARCELABLE_VERSION = 1;
    int errorId;
    String message;

    public OpenPgpError() {
    }

    public OpenPgpError(int errorId2, String message2) {
        this.errorId = errorId2;
        this.message = message2;
    }

    public OpenPgpError(OpenPgpError b) {
        this.errorId = b.errorId;
        this.message = b.message;
    }

    public int getErrorId() {
        return this.errorId;
    }

    public void setErrorId(int errorId2) {
        this.errorId = errorId2;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message2) {
        this.message = message2;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(1);
        int sizePosition = dest.dataPosition();
        dest.writeInt(0);
        int startPosition = dest.dataPosition();
        dest.writeInt(this.errorId);
        dest.writeString(this.message);
        int parcelableSize = dest.dataPosition() - startPosition;
        dest.setDataPosition(sizePosition);
        dest.writeInt(parcelableSize);
        dest.setDataPosition(startPosition + parcelableSize);
    }
}
