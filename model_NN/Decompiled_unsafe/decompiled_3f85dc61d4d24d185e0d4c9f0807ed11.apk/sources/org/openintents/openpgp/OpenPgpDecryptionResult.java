package org.openintents.openpgp;

import android.os.Parcel;
import android.os.Parcelable;

public class OpenPgpDecryptionResult implements Parcelable {
    public static final Parcelable.Creator<OpenPgpDecryptionResult> CREATOR = new Parcelable.Creator<OpenPgpDecryptionResult>() {
        public OpenPgpDecryptionResult createFromParcel(Parcel source) {
            byte[] sessionKey;
            byte[] decryptedSessionKey = null;
            int version = source.readInt();
            int parcelableSize = source.readInt();
            int startPosition = source.dataPosition();
            int result = source.readInt();
            if (version > 1) {
                sessionKey = source.createByteArray();
            } else {
                sessionKey = null;
            }
            if (version > 1) {
                decryptedSessionKey = source.createByteArray();
            }
            OpenPgpDecryptionResult vr = new OpenPgpDecryptionResult(result, sessionKey, decryptedSessionKey);
            source.setDataPosition(startPosition + parcelableSize);
            return vr;
        }

        public OpenPgpDecryptionResult[] newArray(int size) {
            return new OpenPgpDecryptionResult[size];
        }
    };
    public static final int PARCELABLE_VERSION = 2;
    public static final int RESULT_ENCRYPTED = 1;
    public static final int RESULT_INSECURE = 0;
    public static final int RESULT_NOT_ENCRYPTED = -1;
    public final byte[] decryptedSessionKey;
    public final int result;
    public final byte[] sessionKey;

    public int getResult() {
        return this.result;
    }

    public OpenPgpDecryptionResult(int result2) {
        this.result = result2;
        this.sessionKey = null;
        this.decryptedSessionKey = null;
    }

    public OpenPgpDecryptionResult(int result2, byte[] sessionKey2, byte[] decryptedSessionKey2) {
        boolean z;
        boolean z2 = true;
        this.result = result2;
        if (sessionKey2 == null) {
            z = true;
        } else {
            z = false;
        }
        if (z != (decryptedSessionKey2 != null ? false : z2)) {
            throw new AssertionError("sessionkey must be null iff decryptedSessionKey is null");
        }
        this.sessionKey = sessionKey2;
        this.decryptedSessionKey = decryptedSessionKey2;
    }

    public OpenPgpDecryptionResult(OpenPgpDecryptionResult b) {
        this.result = b.result;
        this.sessionKey = b.sessionKey;
        this.decryptedSessionKey = b.decryptedSessionKey;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(2);
        int sizePosition = dest.dataPosition();
        dest.writeInt(0);
        int startPosition = dest.dataPosition();
        dest.writeInt(this.result);
        dest.writeByteArray(this.sessionKey);
        dest.writeByteArray(this.decryptedSessionKey);
        int parcelableSize = dest.dataPosition() - startPosition;
        dest.setDataPosition(sizePosition);
        dest.writeInt(parcelableSize);
        dest.setDataPosition(startPosition + parcelableSize);
    }

    public String toString() {
        return "\nresult: " + this.result;
    }
}
