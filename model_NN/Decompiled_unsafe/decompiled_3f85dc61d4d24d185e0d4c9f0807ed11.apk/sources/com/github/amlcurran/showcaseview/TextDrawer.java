package com.github.amlcurran.showcaseview;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.text.DynamicLayout;
import android.text.Layout;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.TextAppearanceSpan;

class TextDrawer {
    private static final int INDEX_TEXT_START_X = 0;
    private static final int INDEX_TEXT_START_Y = 1;
    private static final int INDEX_TEXT_WIDTH = 2;
    private final float actionBarOffset;
    private final Context context;
    private Layout.Alignment detailTextAlignment = Layout.Alignment.ALIGN_NORMAL;
    private int forcedTextPosition = -1;
    private boolean hasRecalculated;
    private float[] mBestTextPosition = new float[3];
    private TextAppearanceSpan mDetailSpan;
    private CharSequence mDetails;
    private DynamicLayout mDynamicDetailLayout;
    private DynamicLayout mDynamicTitleLayout;
    private CharSequence mTitle;
    private TextAppearanceSpan mTitleSpan;
    private final float padding;
    private final TextPaint textPaint;
    private final TextPaint titlePaint;
    private Layout.Alignment titleTextAlignment = Layout.Alignment.ALIGN_NORMAL;

    public TextDrawer(Resources resources, Context context2) {
        this.padding = resources.getDimension(R.dimen.text_padding);
        this.actionBarOffset = resources.getDimension(R.dimen.action_bar_offset);
        this.context = context2;
        this.titlePaint = new TextPaint();
        this.titlePaint.setAntiAlias(true);
        this.textPaint = new TextPaint();
        this.textPaint.setAntiAlias(true);
    }

    public void draw(Canvas canvas) {
        if (shouldDrawText()) {
            float[] textPosition = getBestTextPosition();
            int width = Math.max(0, (int) this.mBestTextPosition[2]);
            if (!TextUtils.isEmpty(this.mTitle)) {
                canvas.save();
                if (this.hasRecalculated) {
                    this.mDynamicTitleLayout = new DynamicLayout(this.mTitle, this.titlePaint, width, this.titleTextAlignment, 1.0f, 1.0f, true);
                }
                if (this.mDynamicTitleLayout != null) {
                    canvas.translate(textPosition[0], textPosition[1]);
                    this.mDynamicTitleLayout.draw(canvas);
                    canvas.restore();
                }
            }
            if (!TextUtils.isEmpty(this.mDetails)) {
                canvas.save();
                if (this.hasRecalculated) {
                    this.mDynamicDetailLayout = new DynamicLayout(this.mDetails, this.textPaint, width, this.detailTextAlignment, 1.2f, 1.0f, true);
                }
                float offsetForTitle = this.mDynamicTitleLayout != null ? (float) this.mDynamicTitleLayout.getHeight() : 0.0f;
                if (this.mDynamicDetailLayout != null) {
                    canvas.translate(textPosition[0], textPosition[1] + offsetForTitle);
                    this.mDynamicDetailLayout.draw(canvas);
                    canvas.restore();
                }
            }
        }
        this.hasRecalculated = false;
    }

    public void setContentText(CharSequence details) {
        if (details != null) {
            SpannableString ssbDetail = new SpannableString(details);
            ssbDetail.setSpan(this.mDetailSpan, 0, ssbDetail.length(), 0);
            this.mDetails = ssbDetail;
        }
    }

    public void setContentTitle(CharSequence title) {
        if (title != null) {
            SpannableString ssbTitle = new SpannableString(title);
            ssbTitle.setSpan(this.mTitleSpan, 0, ssbTitle.length(), 0);
            this.mTitle = ssbTitle;
        }
    }

    public void calculateTextPosition(int canvasW, int canvasH, boolean shouldCentreText, Rect showcase) {
        int[] areas = {showcase.left * canvasH, showcase.top * canvasW, (canvasW - showcase.right) * canvasH, (canvasH - showcase.bottom) * canvasW};
        int largest = 0;
        for (int i = 1; i < areas.length; i++) {
            if (areas[i] > areas[largest]) {
                largest = i;
            }
        }
        if (this.forcedTextPosition != -1) {
            largest = this.forcedTextPosition;
        }
        switch (largest) {
            case 0:
                this.mBestTextPosition[0] = this.padding;
                this.mBestTextPosition[1] = this.padding;
                this.mBestTextPosition[2] = ((float) showcase.left) - (this.padding * 2.0f);
                break;
            case 1:
                this.mBestTextPosition[0] = this.padding;
                this.mBestTextPosition[1] = this.padding + this.actionBarOffset;
                this.mBestTextPosition[2] = ((float) canvasW) - (this.padding * 2.0f);
                break;
            case 2:
                this.mBestTextPosition[0] = ((float) showcase.right) + this.padding;
                this.mBestTextPosition[1] = this.padding;
                this.mBestTextPosition[2] = ((float) (canvasW - showcase.right)) - (this.padding * 2.0f);
                break;
            case 3:
                this.mBestTextPosition[0] = this.padding;
                this.mBestTextPosition[1] = ((float) showcase.bottom) + this.padding;
                this.mBestTextPosition[2] = ((float) canvasW) - (this.padding * 2.0f);
                break;
        }
        if (!shouldCentreText) {
            switch (largest) {
                case 0:
                case 2:
                    float[] fArr = this.mBestTextPosition;
                    fArr[1] = fArr[1] + this.actionBarOffset;
                    break;
            }
        } else {
            switch (largest) {
                case 0:
                case 2:
                    float[] fArr2 = this.mBestTextPosition;
                    fArr2[1] = fArr2[1] + ((float) (canvasH / 4));
                    break;
                case 1:
                case 3:
                    float[] fArr3 = this.mBestTextPosition;
                    fArr3[2] = fArr3[2] / 2.0f;
                    float[] fArr4 = this.mBestTextPosition;
                    fArr4[0] = fArr4[0] + ((float) (canvasW / 4));
                    break;
            }
        }
        this.hasRecalculated = true;
    }

    public void setTitleStyling(int styleId) {
        this.mTitleSpan = new TextAppearanceSpan(this.context, styleId);
        setContentTitle(this.mTitle);
    }

    public void setDetailStyling(int styleId) {
        this.mDetailSpan = new TextAppearanceSpan(this.context, styleId);
        setContentText(this.mDetails);
    }

    public float[] getBestTextPosition() {
        return this.mBestTextPosition;
    }

    public boolean shouldDrawText() {
        return !TextUtils.isEmpty(this.mTitle) || !TextUtils.isEmpty(this.mDetails);
    }

    public void setContentPaint(TextPaint contentPaint) {
        this.textPaint.set(contentPaint);
    }

    public void setTitlePaint(TextPaint textPaint2) {
        this.titlePaint.set(textPaint2);
    }

    public void setDetailTextAlignment(Layout.Alignment textAlignment) {
        this.detailTextAlignment = textAlignment;
    }

    public void setTitleTextAlignment(Layout.Alignment titleTextAlignment2) {
        this.titleTextAlignment = titleTextAlignment2;
    }

    public void forceTextPosition(int textPosition) {
        if (textPosition > 3 || textPosition < -1) {
            throw new IllegalArgumentException("ShowcaseView text was forced with an invalid position");
        }
        this.forcedTextPosition = textPosition;
    }
}
