package com.github.amlcurran.showcaseview;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.support.v4.view.ViewCompat;

public class MaterialShowcaseDrawer implements ShowcaseDrawer {
    private int backgroundColor;
    private final Paint basicPaint;
    private final Paint eraserPaint = new Paint();
    private final float radius;

    public MaterialShowcaseDrawer(Resources resources) {
        this.radius = resources.getDimension(R.dimen.showcase_radius_material);
        this.eraserPaint.setColor((int) ViewCompat.MEASURED_SIZE_MASK);
        this.eraserPaint.setAlpha(0);
        this.eraserPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.MULTIPLY));
        this.eraserPaint.setAntiAlias(true);
        this.basicPaint = new Paint();
    }

    public void setShowcaseColour(int color) {
    }

    public void drawShowcase(Bitmap buffer, float x, float y, float scaleMultiplier) {
        new Canvas(buffer).drawCircle(x, y, this.radius, this.eraserPaint);
    }

    public int getShowcaseWidth() {
        return (int) (this.radius * 2.0f);
    }

    public int getShowcaseHeight() {
        return (int) (this.radius * 2.0f);
    }

    public float getBlockedRadius() {
        return this.radius;
    }

    public void setBackgroundColour(int backgroundColor2) {
        this.backgroundColor = backgroundColor2;
    }

    public void erase(Bitmap bitmapBuffer) {
        bitmapBuffer.eraseColor(this.backgroundColor);
    }

    public void drawToCanvas(Canvas canvas, Bitmap bitmapBuffer) {
        canvas.drawBitmap(bitmapBuffer, 0.0f, 0.0f, this.basicPaint);
    }
}
