package com.github.amlcurran.showcaseview;

import android.os.Build;

class ApiUtils {
    ApiUtils() {
    }

    public boolean isCompatWith(int versionCode) {
        return Build.VERSION.SDK_INT >= versionCode;
    }

    public boolean isCompatWithHoneycomb() {
        return isCompatWith(11);
    }
}
