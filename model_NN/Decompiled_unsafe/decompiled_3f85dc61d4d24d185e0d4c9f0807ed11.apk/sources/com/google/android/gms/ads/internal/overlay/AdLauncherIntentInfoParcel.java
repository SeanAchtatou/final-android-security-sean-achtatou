package com.google.android.gms.ads.internal.overlay;

import android.content.Intent;
import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.zzhb;

@zzhb
public final class AdLauncherIntentInfoParcel implements SafeParcelable {
    public static final zzb CREATOR = new zzb();
    public final Intent intent;
    public final String intentAction;
    public final String mimeType;
    public final String packageName;
    public final String url;
    public final int versionCode;
    public final String zzDK;
    public final String zzDL;
    public final String zzDM;

    public AdLauncherIntentInfoParcel(int versionCode2, String intentAction2, String url2, String mimeType2, String packageName2, String componentName, String intentFlagsString, String intentExtrasString, Intent intent2) {
        this.versionCode = versionCode2;
        this.intentAction = intentAction2;
        this.url = url2;
        this.mimeType = mimeType2;
        this.packageName = packageName2;
        this.zzDK = componentName;
        this.zzDL = intentFlagsString;
        this.zzDM = intentExtrasString;
        this.intent = intent2;
    }

    public AdLauncherIntentInfoParcel(Intent intent2) {
        this(2, null, null, null, null, null, null, null, intent2);
    }

    public AdLauncherIntentInfoParcel(String intentAction2, String url2, String mimeType2, String packageName2, String componentName, String intentFlagsString, String intentExtrasString) {
        this(2, intentAction2, url2, mimeType2, packageName2, componentName, intentFlagsString, intentExtrasString, null);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        zzb.zza(this, out, flags);
    }
}
