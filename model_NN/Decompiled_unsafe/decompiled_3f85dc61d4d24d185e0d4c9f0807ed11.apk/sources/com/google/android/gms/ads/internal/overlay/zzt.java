package com.google.android.gms.ads.internal.overlay;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.TextureView;
import com.fsck.k9.preferences.SettingsExporter;
import com.google.android.gms.ads.internal.util.client.VersionInfoParcel;
import com.google.android.gms.ads.internal.zzr;
import com.google.android.gms.internal.zzbt;
import com.google.android.gms.internal.zzbx;
import com.google.android.gms.internal.zzbz;
import com.google.android.gms.internal.zzcb;
import com.google.android.gms.internal.zzhb;
import com.google.android.gms.internal.zzin;
import com.google.android.gms.internal.zziv;
import java.util.concurrent.TimeUnit;

@zzhb
public class zzt {
    private final Context mContext;
    private final String zzEY;
    private final VersionInfoParcel zzEZ;
    @Nullable
    private final zzbz zzFa;
    @Nullable
    private final zzcb zzFb;
    private final zziv zzFc = new zziv.zzb().zza("min_1", Double.MIN_VALUE, 1.0d).zza("1_5", 1.0d, 5.0d).zza("5_10", 5.0d, 10.0d).zza("10_20", 10.0d, 20.0d).zza("20_30", 20.0d, 30.0d).zza("30_max", 30.0d, Double.MAX_VALUE).zzhA();
    private final long[] zzFd;
    private final String[] zzFe;
    @Nullable
    private zzbz zzFf;
    @Nullable
    private zzbz zzFg;
    @Nullable
    private zzbz zzFh;
    @Nullable
    private zzbz zzFi;
    private boolean zzFj;
    private zzi zzFk;
    private boolean zzFl;
    private boolean zzFm;
    private long zzFn = -1;

    public zzt(Context context, VersionInfoParcel versionInfoParcel, String str, @Nullable zzcb zzcb, @Nullable zzbz zzbz) {
        this.mContext = context;
        this.zzEZ = versionInfoParcel;
        this.zzEY = str;
        this.zzFb = zzcb;
        this.zzFa = zzbz;
        String str2 = zzbt.zzvV.get();
        if (str2 == null) {
            this.zzFe = new String[0];
            this.zzFd = new long[0];
            return;
        }
        String[] split = TextUtils.split(str2, ",");
        this.zzFe = new String[split.length];
        this.zzFd = new long[split.length];
        for (int i = 0; i < split.length; i++) {
            try {
                this.zzFd[i] = Long.parseLong(split[i]);
            } catch (NumberFormatException e) {
                zzin.zzd("Unable to parse frame hash target time number.", e);
                this.zzFd[i] = -1;
            }
        }
    }

    private void zzc(zzi zzi) {
        long longValue = zzbt.zzvW.get().longValue();
        long currentPosition = (long) zzi.getCurrentPosition();
        for (int i = 0; i < this.zzFe.length; i++) {
            if (this.zzFe[i] == null && longValue > Math.abs(currentPosition - this.zzFd[i])) {
                this.zzFe[i] = zza((TextureView) zzi);
                return;
            }
        }
    }

    private void zzfN() {
        if (this.zzFh != null && this.zzFi == null) {
            zzbx.zza(this.zzFb, this.zzFh, "vff");
            zzbx.zza(this.zzFb, this.zzFa, "vtt");
            this.zzFi = zzbx.zzb(this.zzFb);
        }
        long nanoTime = zzr.zzbG().nanoTime();
        if (this.zzFj && this.zzFm && this.zzFn != -1) {
            this.zzFc.zza(((double) TimeUnit.SECONDS.toNanos(1)) / ((double) (nanoTime - this.zzFn)));
        }
        this.zzFm = this.zzFj;
        this.zzFn = nanoTime;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzir.zza(android.content.Context, java.lang.String, java.lang.String, android.os.Bundle, boolean):void
     arg types: [android.content.Context, java.lang.String, java.lang.String, android.os.Bundle, int]
     candidates:
      com.google.android.gms.internal.zzir.zza(android.content.Context, java.lang.String, boolean, java.net.HttpURLConnection, java.lang.String):void
      com.google.android.gms.internal.zzir.zza(android.content.Context, java.lang.String, boolean, java.net.HttpURLConnection, boolean):void
      com.google.android.gms.internal.zzir.zza(android.content.Context, java.lang.String, java.lang.String, android.os.Bundle, boolean):void */
    public void onStop() {
        if (zzbt.zzvU.get().booleanValue() && !this.zzFl) {
            Bundle bundle = new Bundle();
            bundle.putString(SettingsExporter.TYPE_ATTRIBUTE, "native-player-metrics");
            bundle.putString("request", this.zzEY);
            bundle.putString("player", this.zzFk.zzeZ());
            for (zziv.zza next : this.zzFc.getBuckets()) {
                bundle.putString("fps_c_" + next.name, Integer.toString(next.count));
                bundle.putString("fps_p_" + next.name, Double.toString(next.zzMu));
            }
            for (int i = 0; i < this.zzFd.length; i++) {
                String str = this.zzFe[i];
                if (str != null) {
                    bundle.putString("fh_" + Long.valueOf(this.zzFd[i]), str);
                }
            }
            zzr.zzbC().zza(this.mContext, this.zzEZ.afmaVersion, "gmob-apps", bundle, true);
            this.zzFl = true;
        }
    }

    /* access modifiers changed from: package-private */
    public String zza(TextureView textureView) {
        long j;
        Bitmap bitmap = textureView.getBitmap(8, 8);
        long j2 = 0;
        long j3 = 63;
        int i = 0;
        while (i < 8) {
            int i2 = 0;
            long j4 = j2;
            while (true) {
                j = j3;
                int i3 = i2;
                if (i3 >= 8) {
                    break;
                }
                int pixel = bitmap.getPixel(i3, i);
                j4 |= (Color.green(pixel) + (Color.blue(pixel) + Color.red(pixel)) > 128 ? 1 : 0) << ((int) j);
                i2 = i3 + 1;
                j3 = j - 1;
            }
            i++;
            j3 = j;
            j2 = j4;
        }
        return String.format("%016X", Long.valueOf(j2));
    }

    public void zza(zzi zzi) {
        zzbx.zza(this.zzFb, this.zzFa, "vpc");
        this.zzFf = zzbx.zzb(this.zzFb);
        this.zzFk = zzi;
    }

    public void zzb(zzi zzi) {
        zzfN();
        zzc(zzi);
    }

    public void zzfO() {
        this.zzFj = true;
        if (this.zzFg != null && this.zzFh == null) {
            zzbx.zza(this.zzFb, this.zzFg, "vfp");
            this.zzFh = zzbx.zzb(this.zzFb);
        }
    }

    public void zzfP() {
        this.zzFj = false;
    }

    public void zzfz() {
        if (this.zzFf != null && this.zzFg == null) {
            zzbx.zza(this.zzFb, this.zzFf, "vfr");
            this.zzFg = zzbx.zzb(this.zzFb);
        }
    }
}
