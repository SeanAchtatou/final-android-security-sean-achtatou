package com.google.android.gms.ads.internal.client;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.google.android.gms.internal.zzhb;

@zzhb
public class ThinAdSizeParcel extends AdSizeParcel {
    public ThinAdSizeParcel(AdSizeParcel originalAdSize) {
        super(originalAdSize.versionCode, originalAdSize.zzuh, originalAdSize.height, originalAdSize.heightPixels, originalAdSize.zzui, originalAdSize.width, originalAdSize.widthPixels, originalAdSize.zzuj, originalAdSize.zzuk, originalAdSize.zzul, originalAdSize.zzum);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.util.List<java.lang.Integer>, boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, float[], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, int[], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, boolean[], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, byte[][], boolean):void
      com.google.android.gms.common.internal.safeparcel.zzb.zza(android.os.Parcel, int, java.lang.String, boolean):void */
    public void writeToParcel(Parcel parcel, int flags) {
        int zzav = zzb.zzav(parcel);
        zzb.zzc(parcel, 1, this.versionCode);
        zzb.zza(parcel, 2, this.zzuh, false);
        zzb.zzc(parcel, 3, this.height);
        zzb.zzc(parcel, 6, this.width);
        zzb.zzI(parcel, zzav);
    }
}
