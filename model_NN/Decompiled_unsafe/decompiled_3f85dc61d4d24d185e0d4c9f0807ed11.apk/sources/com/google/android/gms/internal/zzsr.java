package com.google.android.gms.internal;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

class zzsr implements Cloneable {
    private zzsp<?, ?> zzbuq;
    private Object zzbur;
    private List<zzsw> zzbus = new ArrayList();

    zzsr() {
    }

    private byte[] toByteArray() throws IOException {
        byte[] bArr = new byte[zzz()];
        writeTo(zzsn.zzE(bArr));
        return bArr;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof zzsr)) {
            return false;
        }
        zzsr zzsr = (zzsr) o;
        if (this.zzbur == null || zzsr.zzbur == null) {
            if (this.zzbus != null && zzsr.zzbus != null) {
                return this.zzbus.equals(zzsr.zzbus);
            }
            try {
                return Arrays.equals(toByteArray(), zzsr.toByteArray());
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        } else if (this.zzbuq == zzsr.zzbuq) {
            return !this.zzbuq.zzbuk.isArray() ? this.zzbur.equals(zzsr.zzbur) : this.zzbur instanceof byte[] ? Arrays.equals((byte[]) this.zzbur, (byte[]) zzsr.zzbur) : this.zzbur instanceof int[] ? Arrays.equals((int[]) this.zzbur, (int[]) zzsr.zzbur) : this.zzbur instanceof long[] ? Arrays.equals((long[]) this.zzbur, (long[]) zzsr.zzbur) : this.zzbur instanceof float[] ? Arrays.equals((float[]) this.zzbur, (float[]) zzsr.zzbur) : this.zzbur instanceof double[] ? Arrays.equals((double[]) this.zzbur, (double[]) zzsr.zzbur) : this.zzbur instanceof boolean[] ? Arrays.equals((boolean[]) this.zzbur, (boolean[]) zzsr.zzbur) : Arrays.deepEquals((Object[]) this.zzbur, (Object[]) zzsr.zzbur);
        } else {
            return false;
        }
    }

    public int hashCode() {
        try {
            return Arrays.hashCode(toByteArray()) + 527;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    /* access modifiers changed from: package-private */
    public void writeTo(zzsn output) throws IOException {
        if (this.zzbur != null) {
            this.zzbuq.zza(this.zzbur, output);
            return;
        }
        for (zzsw writeTo : this.zzbus) {
            writeTo.writeTo(output);
        }
    }

    /* renamed from: zzJr */
    public final zzsr clone() {
        zzsr zzsr = new zzsr();
        try {
            zzsr.zzbuq = this.zzbuq;
            if (this.zzbus == null) {
                zzsr.zzbus = null;
            } else {
                zzsr.zzbus.addAll(this.zzbus);
            }
            if (this.zzbur != null) {
                if (this.zzbur instanceof zzsu) {
                    zzsr.zzbur = ((zzsu) this.zzbur).clone();
                } else if (this.zzbur instanceof byte[]) {
                    zzsr.zzbur = ((byte[]) this.zzbur).clone();
                } else if (this.zzbur instanceof byte[][]) {
                    byte[][] bArr = (byte[][]) this.zzbur;
                    byte[][] bArr2 = new byte[bArr.length][];
                    zzsr.zzbur = bArr2;
                    for (int i = 0; i < bArr.length; i++) {
                        bArr2[i] = (byte[]) bArr[i].clone();
                    }
                } else if (this.zzbur instanceof boolean[]) {
                    zzsr.zzbur = ((boolean[]) this.zzbur).clone();
                } else if (this.zzbur instanceof int[]) {
                    zzsr.zzbur = ((int[]) this.zzbur).clone();
                } else if (this.zzbur instanceof long[]) {
                    zzsr.zzbur = ((long[]) this.zzbur).clone();
                } else if (this.zzbur instanceof float[]) {
                    zzsr.zzbur = ((float[]) this.zzbur).clone();
                } else if (this.zzbur instanceof double[]) {
                    zzsr.zzbur = ((double[]) this.zzbur).clone();
                } else if (this.zzbur instanceof zzsu[]) {
                    zzsu[] zzsuArr = (zzsu[]) this.zzbur;
                    zzsu[] zzsuArr2 = new zzsu[zzsuArr.length];
                    zzsr.zzbur = zzsuArr2;
                    for (int i2 = 0; i2 < zzsuArr.length; i2++) {
                        zzsuArr2[i2] = zzsuArr[i2].clone();
                    }
                }
            }
            return zzsr;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    /* access modifiers changed from: package-private */
    public void zza(zzsw zzsw) {
        this.zzbus.add(zzsw);
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [com.google.android.gms.internal.zzsp<?, ?>, com.google.android.gms.internal.zzsp<?, T>, com.google.android.gms.internal.zzsp] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    <T> T zzb(com.google.android.gms.internal.zzsp<?, T> r3) {
        /*
            r2 = this;
            java.lang.Object r0 = r2.zzbur
            if (r0 == 0) goto L_0x0010
            com.google.android.gms.internal.zzsp<?, ?> r0 = r2.zzbuq
            if (r0 == r3) goto L_0x001d
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "Tried to getExtension with a differernt Extension."
            r0.<init>(r1)
            throw r0
        L_0x0010:
            r2.zzbuq = r3
            java.util.List<com.google.android.gms.internal.zzsw> r0 = r2.zzbus
            java.lang.Object r0 = r3.zzJ(r0)
            r2.zzbur = r0
            r0 = 0
            r2.zzbus = r0
        L_0x001d:
            java.lang.Object r0 = r2.zzbur
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzsr.zzb(com.google.android.gms.internal.zzsp):java.lang.Object");
    }

    /* access modifiers changed from: package-private */
    public int zzz() {
        int i = 0;
        if (this.zzbur != null) {
            return this.zzbuq.zzY(this.zzbur);
        }
        Iterator<zzsw> it = this.zzbus.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            i = it.next().zzz() + i2;
        }
    }
}
