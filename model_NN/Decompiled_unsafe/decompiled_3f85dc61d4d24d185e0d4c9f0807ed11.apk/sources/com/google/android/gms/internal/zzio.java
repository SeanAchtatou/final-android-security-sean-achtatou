package com.google.android.gms.internal;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.google.android.gms.ads.internal.zzr;

@zzhb
public class zzio extends Handler {
    public zzio(Looper looper) {
        super(looper);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.zzih.zzb(java.lang.Throwable, boolean):void
     arg types: [java.lang.Exception, int]
     candidates:
      com.google.android.gms.internal.zzih.zzb(android.content.Context, com.google.android.gms.ads.internal.util.client.VersionInfoParcel):void
      com.google.android.gms.internal.zzih.zzb(java.lang.Throwable, boolean):void */
    public void handleMessage(Message msg) {
        try {
            super.handleMessage(msg);
        } catch (Exception e) {
            zzr.zzbF().zzb((Throwable) e, false);
            throw e;
        }
    }
}
