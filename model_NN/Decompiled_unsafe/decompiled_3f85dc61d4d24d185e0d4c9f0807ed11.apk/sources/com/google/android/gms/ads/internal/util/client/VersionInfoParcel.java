package com.google.android.gms.ads.internal.util.client;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.internal.zzhb;
import exts.whats.Constants;

@zzhb
public final class VersionInfoParcel implements SafeParcelable {
    public static final zzc CREATOR = new zzc();
    public String afmaVersion;
    public final int versionCode;
    public int zzMZ;
    public int zzNa;
    public boolean zzNb;

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public VersionInfoParcel(int buddyApkVersion, int clientJarVersion, boolean isClientJar) {
        this(1, "afma-sdk-a-v" + buddyApkVersion + "." + clientJarVersion + "." + (isClientJar ? "0" : Constants.INSTALL_ID), buddyApkVersion, clientJarVersion, isClientJar);
    }

    VersionInfoParcel(int versionCode2, String afmaVersion2, int buddyApkVersion, int clientJarVersion, boolean isClientJar) {
        this.versionCode = versionCode2;
        this.afmaVersion = afmaVersion2;
        this.zzMZ = buddyApkVersion;
        this.zzNa = clientJarVersion;
        this.zzNb = isClientJar;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        zzc.zza(this, out, flags);
    }
}
