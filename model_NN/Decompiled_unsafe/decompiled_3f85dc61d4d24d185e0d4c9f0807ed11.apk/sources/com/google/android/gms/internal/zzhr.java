package com.google.android.gms.internal;

import com.google.android.gms.ads.internal.reward.client.zza;

@zzhb
public class zzhr extends zza.C0015zza {
    private final String zzJN;
    private final int zzKt;

    public zzhr(String str, int i) {
        this.zzJN = str;
        this.zzKt = i;
    }

    public int getAmount() {
        return this.zzKt;
    }

    public String getType() {
        return this.zzJN;
    }
}
