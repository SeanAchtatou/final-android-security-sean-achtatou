package com.google.android.gms.ads.internal.client;

import com.google.android.gms.ads.internal.client.zzp;
import com.google.android.gms.internal.zzhb;

@zzhb
public final class zzb extends zzp.zza {
    private final zza zztz;

    public zzb(zza zza) {
        this.zztz = zza;
    }

    public void onAdClicked() {
        this.zztz.onAdClicked();
    }
}
