package com.google.android.gms.internal;

import java.io.IOException;
import java.util.Arrays;

public abstract class zzsu {
    protected volatile int zzbuu = -1;

    public static final <T extends zzsu> T mergeFrom(T msg, byte[] data) throws zzst {
        return mergeFrom(msg, data, 0, data.length);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public static final <T extends com.google.android.gms.internal.zzsu> T mergeFrom(T r2, byte[] r3, int r4, int r5) throws com.google.android.gms.internal.zzst {
        /*
            com.google.android.gms.internal.zzsm r0 = com.google.android.gms.internal.zzsm.zza(r3, r4, r5)     // Catch:{ zzst -> 0x000c, IOException -> 0x000e }
            r2.mergeFrom(r0)     // Catch:{ zzst -> 0x000c, IOException -> 0x000e }
            r1 = 0
            r0.zzmn(r1)     // Catch:{ zzst -> 0x000c, IOException -> 0x000e }
            return r2
        L_0x000c:
            r0 = move-exception
            throw r0
        L_0x000e:
            r0 = move-exception
            java.lang.RuntimeException r0 = new java.lang.RuntimeException
            java.lang.String r1 = "Reading from a byte array threw an IOException (should never happen)."
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.zzsu.mergeFrom(com.google.android.gms.internal.zzsu, byte[], int, int):com.google.android.gms.internal.zzsu");
    }

    public static final boolean messageNanoEquals(zzsu a, zzsu b) {
        int serializedSize;
        if (a == b) {
            return true;
        }
        if (a == null || b == null || a.getClass() != b.getClass() || b.getSerializedSize() != (serializedSize = a.getSerializedSize())) {
            return false;
        }
        byte[] bArr = new byte[serializedSize];
        byte[] bArr2 = new byte[serializedSize];
        toByteArray(a, bArr, 0, serializedSize);
        toByteArray(b, bArr2, 0, serializedSize);
        return Arrays.equals(bArr, bArr2);
    }

    public static final void toByteArray(zzsu msg, byte[] data, int offset, int length) {
        try {
            zzsn zzb = zzsn.zzb(data, offset, length);
            msg.writeTo(zzb);
            zzb.zzJo();
        } catch (IOException e) {
            throw new RuntimeException("Serializing to a byte array threw an IOException (should never happen).", e);
        }
    }

    public static final byte[] toByteArray(zzsu msg) {
        byte[] bArr = new byte[msg.getSerializedSize()];
        toByteArray(msg, bArr, 0, bArr.length);
        return bArr;
    }

    public zzsu clone() throws CloneNotSupportedException {
        return (zzsu) super.clone();
    }

    public int getCachedSize() {
        if (this.zzbuu < 0) {
            getSerializedSize();
        }
        return this.zzbuu;
    }

    public int getSerializedSize() {
        int zzz = zzz();
        this.zzbuu = zzz;
        return zzz;
    }

    public abstract zzsu mergeFrom(zzsm zzsm) throws IOException;

    public String toString() {
        return zzsv.zzf(this);
    }

    public void writeTo(zzsn output) throws IOException {
    }

    /* access modifiers changed from: protected */
    public int zzz() {
        return 0;
    }
}
