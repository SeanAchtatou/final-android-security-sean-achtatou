package com.getkeepsafe.relinker;

import com.getkeepsafe.relinker.ReLinker;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ApkLibraryInstaller implements ReLinker.LibraryInstaller {
    private static final int COPY_BUFFER_SIZE = 4096;
    private static final int MAX_TRIES = 5;

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x009f, code lost:
        if (r13 == null) goto L_0x00b3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00a8, code lost:
        throw new com.getkeepsafe.relinker.MissingLibraryException(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00bc, code lost:
        throw new com.getkeepsafe.relinker.MissingLibraryException(r28);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void installLibrary(android.content.Context r26, java.lang.String[] r27, java.lang.String r28, java.io.File r29, com.getkeepsafe.relinker.ReLinkerInstance r30) {
        /*
            r25 = this;
            r20 = 0
            android.content.pm.ApplicationInfo r5 = r26.getApplicationInfo()     // Catch:{ all -> 0x00a9 }
            r16 = 0
            r17 = r16
        L_0x000a:
            int r16 = r17 + 1
            r22 = 5
            r0 = r17
            r1 = r22
            if (r0 >= r1) goto L_0x0026
            java.util.zip.ZipFile r21 = new java.util.zip.ZipFile     // Catch:{ IOException -> 0x0037 }
            java.io.File r22 = new java.io.File     // Catch:{ IOException -> 0x0037 }
            java.lang.String r0 = r5.sourceDir     // Catch:{ IOException -> 0x0037 }
            r23 = r0
            r22.<init>(r23)     // Catch:{ IOException -> 0x0037 }
            r23 = 1
            r21.<init>(r22, r23)     // Catch:{ IOException -> 0x0037 }
            r20 = r21
        L_0x0026:
            if (r20 != 0) goto L_0x003b
            java.lang.String r22 = "FATAL! Couldn't find application APK!"
            r0 = r30
            r1 = r22
            r0.log(r1)     // Catch:{ all -> 0x00a9 }
            if (r20 == 0) goto L_0x0036
            r20.close()     // Catch:{ IOException -> 0x018f }
        L_0x0036:
            return
        L_0x0037:
            r22 = move-exception
            r17 = r16
            goto L_0x000a
        L_0x003b:
            r16 = 0
            r17 = r16
        L_0x003f:
            int r16 = r17 + 1
            r22 = 5
            r0 = r17
            r1 = r22
            if (r0 >= r1) goto L_0x017c
            r13 = 0
            r15 = 0
            r6 = r27
            int r14 = r6.length     // Catch:{ all -> 0x00a9 }
            r10 = 0
        L_0x004f:
            if (r10 >= r14) goto L_0x0084
            r4 = r6[r10]     // Catch:{ all -> 0x00a9 }
            java.lang.StringBuilder r22 = new java.lang.StringBuilder     // Catch:{ all -> 0x00a9 }
            r22.<init>()     // Catch:{ all -> 0x00a9 }
            java.lang.String r23 = "lib"
            java.lang.StringBuilder r22 = r22.append(r23)     // Catch:{ all -> 0x00a9 }
            char r23 = java.io.File.separatorChar     // Catch:{ all -> 0x00a9 }
            java.lang.StringBuilder r22 = r22.append(r23)     // Catch:{ all -> 0x00a9 }
            r0 = r22
            java.lang.StringBuilder r22 = r0.append(r4)     // Catch:{ all -> 0x00a9 }
            char r23 = java.io.File.separatorChar     // Catch:{ all -> 0x00a9 }
            java.lang.StringBuilder r22 = r22.append(r23)     // Catch:{ all -> 0x00a9 }
            r0 = r22
            r1 = r28
            java.lang.StringBuilder r22 = r0.append(r1)     // Catch:{ all -> 0x00a9 }
            java.lang.String r13 = r22.toString()     // Catch:{ all -> 0x00a9 }
            r0 = r20
            java.util.zip.ZipEntry r15 = r0.getEntry(r13)     // Catch:{ all -> 0x00a9 }
            if (r15 == 0) goto L_0x00b0
        L_0x0084:
            if (r13 == 0) goto L_0x009d
            java.lang.String r22 = "Looking for %s in APK..."
            r23 = 1
            r0 = r23
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ all -> 0x00a9 }
            r23 = r0
            r24 = 0
            r23[r24] = r13     // Catch:{ all -> 0x00a9 }
            r0 = r30
            r1 = r22
            r2 = r23
            r0.log(r1, r2)     // Catch:{ all -> 0x00a9 }
        L_0x009d:
            if (r15 != 0) goto L_0x00bd
            if (r13 == 0) goto L_0x00b3
            com.getkeepsafe.relinker.MissingLibraryException r22 = new com.getkeepsafe.relinker.MissingLibraryException     // Catch:{ all -> 0x00a9 }
            r0 = r22
            r0.<init>(r13)     // Catch:{ all -> 0x00a9 }
            throw r22     // Catch:{ all -> 0x00a9 }
        L_0x00a9:
            r22 = move-exception
            if (r20 == 0) goto L_0x00af
            r20.close()     // Catch:{ IOException -> 0x0192 }
        L_0x00af:
            throw r22
        L_0x00b0:
            int r10 = r10 + 1
            goto L_0x004f
        L_0x00b3:
            com.getkeepsafe.relinker.MissingLibraryException r22 = new com.getkeepsafe.relinker.MissingLibraryException     // Catch:{ all -> 0x00a9 }
            r0 = r22
            r1 = r28
            r0.<init>(r1)     // Catch:{ all -> 0x00a9 }
            throw r22     // Catch:{ all -> 0x00a9 }
        L_0x00bd:
            java.lang.String r22 = "Found %s! Extracting..."
            r23 = 1
            r0 = r23
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ all -> 0x00a9 }
            r23 = r0
            r24 = 0
            r23[r24] = r13     // Catch:{ all -> 0x00a9 }
            r0 = r30
            r1 = r22
            r2 = r23
            r0.log(r1, r2)     // Catch:{ all -> 0x00a9 }
            boolean r22 = r29.exists()     // Catch:{ IOException -> 0x00e4 }
            if (r22 != 0) goto L_0x00e9
            boolean r22 = r29.createNewFile()     // Catch:{ IOException -> 0x00e4 }
            if (r22 != 0) goto L_0x00e9
            r17 = r16
            goto L_0x003f
        L_0x00e4:
            r11 = move-exception
            r17 = r16
            goto L_0x003f
        L_0x00e9:
            r12 = 0
            r8 = 0
            r0 = r20
            java.io.InputStream r12 = r0.getInputStream(r15)     // Catch:{ FileNotFoundException -> 0x0152, IOException -> 0x0161, all -> 0x0170 }
            java.io.FileOutputStream r9 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x0152, IOException -> 0x0161, all -> 0x0170 }
            r0 = r29
            r9.<init>(r0)     // Catch:{ FileNotFoundException -> 0x0152, IOException -> 0x0161, all -> 0x0170 }
            r0 = r25
            long r18 = r0.copy(r12, r9)     // Catch:{ FileNotFoundException -> 0x019b, IOException -> 0x0198, all -> 0x0195 }
            java.io.FileDescriptor r22 = r9.getFD()     // Catch:{ FileNotFoundException -> 0x019b, IOException -> 0x0198, all -> 0x0195 }
            r22.sync()     // Catch:{ FileNotFoundException -> 0x019b, IOException -> 0x0198, all -> 0x0195 }
            long r22 = r29.length()     // Catch:{ FileNotFoundException -> 0x019b, IOException -> 0x0198, all -> 0x0195 }
            int r22 = (r18 > r22 ? 1 : (r18 == r22 ? 0 : -1))
            if (r22 == 0) goto L_0x011b
            r0 = r25
            r0.closeSilently(r12)     // Catch:{ all -> 0x00a9 }
            r0 = r25
            r0.closeSilently(r9)     // Catch:{ all -> 0x00a9 }
            r17 = r16
            goto L_0x003f
        L_0x011b:
            r0 = r25
            r0.closeSilently(r12)     // Catch:{ all -> 0x00a9 }
            r0 = r25
            r0.closeSilently(r9)     // Catch:{ all -> 0x00a9 }
            r22 = 1
            r23 = 0
            r0 = r29
            r1 = r22
            r2 = r23
            r0.setReadable(r1, r2)     // Catch:{ all -> 0x00a9 }
            r22 = 1
            r23 = 0
            r0 = r29
            r1 = r22
            r2 = r23
            r0.setExecutable(r1, r2)     // Catch:{ all -> 0x00a9 }
            r22 = 1
            r0 = r29
            r1 = r22
            r0.setWritable(r1)     // Catch:{ all -> 0x00a9 }
            if (r20 == 0) goto L_0x0036
            r20.close()     // Catch:{ IOException -> 0x014f }
            goto L_0x0036
        L_0x014f:
            r22 = move-exception
            goto L_0x0036
        L_0x0152:
            r7 = move-exception
        L_0x0153:
            r0 = r25
            r0.closeSilently(r12)     // Catch:{ all -> 0x00a9 }
            r0 = r25
            r0.closeSilently(r8)     // Catch:{ all -> 0x00a9 }
            r17 = r16
            goto L_0x003f
        L_0x0161:
            r7 = move-exception
        L_0x0162:
            r0 = r25
            r0.closeSilently(r12)     // Catch:{ all -> 0x00a9 }
            r0 = r25
            r0.closeSilently(r8)     // Catch:{ all -> 0x00a9 }
            r17 = r16
            goto L_0x003f
        L_0x0170:
            r22 = move-exception
        L_0x0171:
            r0 = r25
            r0.closeSilently(r12)     // Catch:{ all -> 0x00a9 }
            r0 = r25
            r0.closeSilently(r8)     // Catch:{ all -> 0x00a9 }
            throw r22     // Catch:{ all -> 0x00a9 }
        L_0x017c:
            java.lang.String r22 = "FATAL! Couldn't extract the library from the APK!"
            r0 = r30
            r1 = r22
            r0.log(r1)     // Catch:{ all -> 0x00a9 }
            if (r20 == 0) goto L_0x0036
            r20.close()     // Catch:{ IOException -> 0x018c }
            goto L_0x0036
        L_0x018c:
            r22 = move-exception
            goto L_0x0036
        L_0x018f:
            r22 = move-exception
            goto L_0x0036
        L_0x0192:
            r23 = move-exception
            goto L_0x00af
        L_0x0195:
            r22 = move-exception
            r8 = r9
            goto L_0x0171
        L_0x0198:
            r7 = move-exception
            r8 = r9
            goto L_0x0162
        L_0x019b:
            r7 = move-exception
            r8 = r9
            goto L_0x0153
        */
        throw new UnsupportedOperationException("Method not decompiled: com.getkeepsafe.relinker.ApkLibraryInstaller.installLibrary(android.content.Context, java.lang.String[], java.lang.String, java.io.File, com.getkeepsafe.relinker.ReLinkerInstance):void");
    }

    private long copy(InputStream in, OutputStream out) throws IOException {
        long copied = 0;
        byte[] buf = new byte[4096];
        while (true) {
            int read = in.read(buf);
            if (read == -1) {
                out.flush();
                return copied;
            }
            out.write(buf, 0, read);
            copied += (long) read;
        }
    }

    private void closeSilently(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
            }
        }
    }
}
