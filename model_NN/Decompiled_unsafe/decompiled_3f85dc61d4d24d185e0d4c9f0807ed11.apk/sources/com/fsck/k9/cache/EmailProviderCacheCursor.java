package com.fsck.k9.cache;

import android.content.Context;
import android.database.Cursor;
import android.database.CursorWrapper;
import com.fsck.k9.provider.EmailProvider;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class EmailProviderCacheCursor extends CursorWrapper {
    private EmailProviderCache mCache;
    private int mFolderIdColumn;
    private List<Integer> mHiddenRows = new ArrayList();
    private int mMessageIdColumn;
    private int mPosition;
    private int mThreadRootColumn;

    public EmailProviderCacheCursor(String accountUuid, Cursor cursor, Context context) {
        super(cursor);
        this.mCache = EmailProviderCache.getCache(accountUuid, context);
        this.mMessageIdColumn = cursor.getColumnIndex("id");
        this.mFolderIdColumn = cursor.getColumnIndex(EmailProvider.MessageColumns.FOLDER_ID);
        this.mThreadRootColumn = cursor.getColumnIndex(EmailProvider.ThreadColumns.ROOT);
        if (this.mMessageIdColumn == -1 || this.mFolderIdColumn == -1 || this.mThreadRootColumn == -1) {
            throw new IllegalArgumentException("The supplied cursor needs to contain the following columns: id, folder_id, root");
        }
        while (cursor.moveToNext()) {
            long messageId = cursor.getLong(this.mMessageIdColumn);
            if (this.mCache.isMessageHidden(Long.valueOf(messageId), cursor.getLong(this.mFolderIdColumn))) {
                this.mHiddenRows.add(Integer.valueOf(cursor.getPosition()));
            }
        }
        cursor.moveToFirst();
        cursor.moveToPrevious();
    }

    public int getInt(int columnIndex) {
        long messageId = getLong(this.mMessageIdColumn);
        long threadRootId = getLong(this.mThreadRootColumn);
        String columnName = getColumnName(columnIndex);
        String value = this.mCache.getValueForMessage(Long.valueOf(messageId), columnName);
        if (value != null) {
            return Integer.parseInt(value);
        }
        String value2 = this.mCache.getValueForThread(Long.valueOf(threadRootId), columnName);
        if (value2 != null) {
            return Integer.parseInt(value2);
        }
        return super.getInt(columnIndex);
    }

    public int getCount() {
        return super.getCount() - this.mHiddenRows.size();
    }

    public boolean moveToFirst() {
        return moveToPosition(0);
    }

    public boolean moveToLast() {
        return moveToPosition(getCount());
    }

    public boolean moveToNext() {
        return moveToPosition(getPosition() + 1);
    }

    public boolean moveToPrevious() {
        return moveToPosition(getPosition() - 1);
    }

    public boolean move(int offset) {
        return moveToPosition(getPosition() + offset);
    }

    public boolean moveToPosition(int position) {
        if (this.mHiddenRows.isEmpty()) {
            return super.moveToPosition(position);
        }
        this.mPosition = position;
        int newPosition = position;
        Iterator<Integer> it = this.mHiddenRows.iterator();
        while (it.hasNext() && it.next().intValue() <= newPosition) {
            newPosition++;
        }
        return super.moveToPosition(newPosition);
    }

    public int getPosition() {
        if (this.mHiddenRows.isEmpty()) {
            return super.getPosition();
        }
        return this.mPosition;
    }

    public boolean isLast() {
        if (this.mHiddenRows.isEmpty()) {
            return super.isLast();
        }
        return this.mPosition == getCount() + -1;
    }
}
