package com.fsck.k9.helper;

import android.content.res.Resources;
import android.util.Log;
import com.fsck.k9.Account;
import com.fsck.k9.K9;
import com.fsck.k9.R;
import com.fsck.k9.mail.Address;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.Part;
import com.fsck.k9.mail.internet.MessageExtractor;
import com.fsck.k9.mail.internet.MimeUtility;
import com.fsck.k9.message.InsertableHtmlContent;
import com.fsck.k9.message.SimpleMessageFormat;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.james.mime4j.dom.field.ContentTypeField;
import org.htmlcleaner.CleanerProperties;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.SimpleHtmlSerializer;

public class QuotedMessageHelper {
    private static final Pattern BLOCKQUOTE_END = Pattern.compile("</blockquote>", 2);
    private static final Pattern BLOCKQUOTE_START = Pattern.compile("<blockquote", 2);
    private static final Pattern DASH_SIGNATURE_HTML = Pattern.compile("(<br( /)?>|\r?\n)-- <br( /)?>", 2);
    private static final Pattern DASH_SIGNATURE_PLAIN = Pattern.compile("\r\n-- \r\n.*", 32);
    private static final Pattern FIND_INSERTION_POINT_BODY = Pattern.compile("(?si:.*?(<body(?:>|\\s+[^>]*>)).*)");
    private static final Pattern FIND_INSERTION_POINT_BODY_END = Pattern.compile("(?si:.*(</body>).*?)");
    private static final int FIND_INSERTION_POINT_FIRST_GROUP = 1;
    private static final Pattern FIND_INSERTION_POINT_HEAD = Pattern.compile("(?si:.*?(<head(?:>|\\s+[^>]*>)).*)");
    private static final String FIND_INSERTION_POINT_HEAD_CONTENT = "<head><meta content=\"text/html; charset=utf-8\" http-equiv=\"Content-Type\"></head>";
    private static final Pattern FIND_INSERTION_POINT_HTML = Pattern.compile("(?si:.*?(<html(?:>|\\s+[^>]*>)).*)");
    private static final String FIND_INSERTION_POINT_HTML_CONTENT = "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\r\n<html>";
    private static final Pattern FIND_INSERTION_POINT_HTML_END = Pattern.compile("(?si:.*(</html>).*?)");
    private static final String FIND_INSERTION_POINT_HTML_END_CONTENT = "</html>";
    private static final int FIND_INSERTION_POINT_START_OF_STRING = 0;
    private static final int QUOTE_BUFFER_LENGTH = 512;
    private static final int REPLY_WRAP_LINE_WIDTH = 72;

    public static InsertableHtmlContent quoteOriginalHtmlMessage(Resources resources, Message originalMessage, String messageBody, Account.QuoteStyle quoteStyle) throws MessagingException {
        InsertableHtmlContent insertable = findInsertionPoints(messageBody);
        String sentDate = getSentDateText(resources, originalMessage);
        String fromAddress = Address.toString(originalMessage.getFrom());
        if (quoteStyle == Account.QuoteStyle.PREFIX) {
            StringBuilder header = new StringBuilder(512);
            header.append("<div class=\"gmail_quote\">");
            if (sentDate.length() != 0) {
                header.append(HtmlConverter.textToHtmlFragment(String.format(resources.getString(R.string.message_compose_reply_header_fmt_with_date), sentDate, fromAddress)));
            } else {
                header.append(HtmlConverter.textToHtmlFragment(String.format(resources.getString(R.string.message_compose_reply_header_fmt), fromAddress)));
            }
            header.append("<blockquote class=\"gmail_quote\" style=\"margin: 0pt 0pt 0pt 0.8ex; border-left: 1px solid rgb(204, 204, 204); padding-left: 1ex;\">\r\n");
            insertable.insertIntoQuotedHeader(header.toString());
            insertable.insertIntoQuotedFooter("</blockquote></div>");
        } else if (quoteStyle == Account.QuoteStyle.HEADER) {
            StringBuilder header2 = new StringBuilder();
            header2.append("<div style='font-size:10.0pt;font-family:\"Tahoma\",\"sans-serif\";padding:3.0pt 0in 0in 0in'>\r\n");
            header2.append("<hr style='border:none;border-top:solid #E1E1E1 1.0pt'>\r\n");
            if (!(originalMessage.getFrom() == null || fromAddress.length() == 0)) {
                header2.append("<b>").append(resources.getString(R.string.message_compose_quote_header_from)).append("</b> ").append(HtmlConverter.textToHtmlFragment(fromAddress)).append("<br>\r\n");
            }
            if (sentDate.length() != 0) {
                header2.append("<b>").append(resources.getString(R.string.message_compose_quote_header_send_date)).append("</b> ").append(sentDate).append("<br>\r\n");
            }
            if (!(originalMessage.getRecipients(Message.RecipientType.TO) == null || originalMessage.getRecipients(Message.RecipientType.TO).length == 0)) {
                header2.append("<b>").append(resources.getString(R.string.message_compose_quote_header_to)).append("</b> ").append(HtmlConverter.textToHtmlFragment(Address.toString(originalMessage.getRecipients(Message.RecipientType.TO)))).append("<br>\r\n");
            }
            if (!(originalMessage.getRecipients(Message.RecipientType.CC) == null || originalMessage.getRecipients(Message.RecipientType.CC).length == 0)) {
                header2.append("<b>").append(resources.getString(R.string.message_compose_quote_header_cc)).append("</b> ").append(HtmlConverter.textToHtmlFragment(Address.toString(originalMessage.getRecipients(Message.RecipientType.CC)))).append("<br>\r\n");
            }
            if (originalMessage.getSubject() != null) {
                header2.append("<b>").append(resources.getString(R.string.message_compose_quote_header_subject)).append("</b> ").append(HtmlConverter.textToHtmlFragment(originalMessage.getSubject())).append("<br>\r\n");
            }
            header2.append("</div>\r\n");
            header2.append("<br>\r\n");
            insertable.insertIntoQuotedHeader(header2.toString());
        }
        return insertable;
    }

    private static String getSentDateText(Resources resources, Message message) {
        try {
            return DateFormat.getDateTimeInstance(1, 1, resources.getConfiguration().locale).format(message.getSentDate());
        } catch (Exception e) {
            return "";
        }
    }

    private static InsertableHtmlContent findInsertionPoints(String content) {
        InsertableHtmlContent insertable = new InsertableHtmlContent();
        if (content != null && !content.equals("")) {
            boolean hasHtmlTag = false;
            boolean hasHeadTag = false;
            boolean hasBodyTag = false;
            Matcher htmlMatcher = FIND_INSERTION_POINT_HTML.matcher(content);
            if (htmlMatcher.matches()) {
                hasHtmlTag = true;
            }
            Matcher headMatcher = FIND_INSERTION_POINT_HEAD.matcher(content);
            if (headMatcher.matches()) {
                hasHeadTag = true;
            }
            Matcher bodyMatcher = FIND_INSERTION_POINT_BODY.matcher(content);
            if (bodyMatcher.matches()) {
                hasBodyTag = true;
            }
            if (K9.DEBUG) {
                Log.d("k9", "Open: hasHtmlTag:" + hasHtmlTag + " hasHeadTag:" + hasHeadTag + " hasBodyTag:" + hasBodyTag);
            }
            if (hasBodyTag) {
                insertable.setQuotedContent(new StringBuilder(content));
                insertable.setHeaderInsertionPoint(bodyMatcher.end(1));
            } else if (hasHeadTag) {
                insertable.setQuotedContent(new StringBuilder(content));
                insertable.setHeaderInsertionPoint(headMatcher.end(1));
            } else if (hasHtmlTag) {
                StringBuilder newContent = new StringBuilder(content);
                newContent.insert(htmlMatcher.end(1), FIND_INSERTION_POINT_HEAD_CONTENT);
                insertable.setQuotedContent(newContent);
                insertable.setHeaderInsertionPoint(htmlMatcher.end(1) + FIND_INSERTION_POINT_HEAD_CONTENT.length());
            } else {
                StringBuilder newContent2 = new StringBuilder(content);
                newContent2.insert(0, FIND_INSERTION_POINT_HEAD_CONTENT);
                newContent2.insert(0, FIND_INSERTION_POINT_HTML_CONTENT);
                newContent2.append(FIND_INSERTION_POINT_HTML_END_CONTENT);
                insertable.setQuotedContent(newContent2);
                insertable.setHeaderInsertionPoint(FIND_INSERTION_POINT_HTML_CONTENT.length() + FIND_INSERTION_POINT_HEAD_CONTENT.length());
            }
            boolean hasHtmlEndTag = false;
            boolean hasBodyEndTag = false;
            Matcher htmlEndMatcher = FIND_INSERTION_POINT_HTML_END.matcher(insertable.getQuotedContent());
            if (htmlEndMatcher.matches()) {
                hasHtmlEndTag = true;
            }
            Matcher bodyEndMatcher = FIND_INSERTION_POINT_BODY_END.matcher(insertable.getQuotedContent());
            if (bodyEndMatcher.matches()) {
                hasBodyEndTag = true;
            }
            if (K9.DEBUG) {
                Log.d("k9", "Close: hasHtmlEndTag:" + hasHtmlEndTag + " hasBodyEndTag:" + hasBodyEndTag);
            }
            if (hasBodyEndTag) {
                insertable.setFooterInsertionPoint(bodyEndMatcher.start(1));
            } else if (hasHtmlEndTag) {
                insertable.setFooterInsertionPoint(htmlEndMatcher.start(1));
            } else {
                insertable.setFooterInsertionPoint(insertable.getQuotedContent().length());
            }
        }
        return insertable;
    }

    public static String quoteOriginalTextMessage(Resources resources, Message originalMessage, String messageBody, Account.QuoteStyle quoteStyle, String prefix) throws MessagingException {
        String body;
        if (messageBody == null) {
            body = "";
        } else {
            body = messageBody;
        }
        String sentDate = getSentDateText(resources, originalMessage);
        if (quoteStyle == Account.QuoteStyle.PREFIX) {
            StringBuilder quotedText = new StringBuilder(body.length() + 512);
            if (sentDate.length() != 0) {
                quotedText.append(String.format(resources.getString(R.string.message_compose_reply_header_fmt_with_date) + "\r\n", sentDate, Address.toString(originalMessage.getFrom())));
            } else {
                quotedText.append(String.format(resources.getString(R.string.message_compose_reply_header_fmt) + "\r\n", Address.toString(originalMessage.getFrom())));
            }
            quotedText.append(Utility.wrap(body, 72 - prefix.length()).replaceAll("(?m)^", prefix.replaceAll("(\\\\|\\$)", "\\\\$1")));
            return quotedText.toString().replaceAll("\\\r", "");
        } else if (quoteStyle != Account.QuoteStyle.HEADER) {
            return body;
        } else {
            StringBuilder quotedText2 = new StringBuilder(body.length() + 512);
            quotedText2.append("\r\n");
            quotedText2.append(resources.getString(R.string.message_compose_quote_header_separator)).append("\r\n");
            if (!(originalMessage.getFrom() == null || Address.toString(originalMessage.getFrom()).length() == 0)) {
                quotedText2.append(resources.getString(R.string.message_compose_quote_header_from)).append(" ").append(Address.toString(originalMessage.getFrom())).append("\r\n");
            }
            if (sentDate.length() != 0) {
                quotedText2.append(resources.getString(R.string.message_compose_quote_header_send_date)).append(" ").append(sentDate).append("\r\n");
            }
            if (!(originalMessage.getRecipients(Message.RecipientType.TO) == null || originalMessage.getRecipients(Message.RecipientType.TO).length == 0)) {
                quotedText2.append(resources.getString(R.string.message_compose_quote_header_to)).append(" ").append(Address.toString(originalMessage.getRecipients(Message.RecipientType.TO))).append("\r\n");
            }
            if (!(originalMessage.getRecipients(Message.RecipientType.CC) == null || originalMessage.getRecipients(Message.RecipientType.CC).length == 0)) {
                quotedText2.append(resources.getString(R.string.message_compose_quote_header_cc)).append(" ").append(Address.toString(originalMessage.getRecipients(Message.RecipientType.CC))).append("\r\n");
            }
            if (originalMessage.getSubject() != null) {
                quotedText2.append(resources.getString(R.string.message_compose_quote_header_subject)).append(" ").append(originalMessage.getSubject()).append("\r\n");
            }
            quotedText2.append("\r\n");
            quotedText2.append(body);
            return quotedText2.toString();
        }
    }

    public static String getBodyTextFromMessage(Part messagePart, SimpleMessageFormat format) {
        if (format == SimpleMessageFormat.HTML) {
            Part part = MimeUtility.findFirstPartByMimeType(messagePart, "text/html");
            if (part != null) {
                if (K9.DEBUG) {
                    Log.d("k9", "getBodyTextFromMessage: HTML requested, HTML found.");
                }
                return MessageExtractor.getTextFromPart(part);
            }
            Part part2 = MimeUtility.findFirstPartByMimeType(messagePart, ContentTypeField.TYPE_TEXT_PLAIN);
            if (part2 != null) {
                if (K9.DEBUG) {
                    Log.d("k9", "getBodyTextFromMessage: HTML requested, text found.");
                }
                return HtmlConverter.textToHtml(MessageExtractor.getTextFromPart(part2));
            }
        } else if (format == SimpleMessageFormat.TEXT) {
            Part part3 = MimeUtility.findFirstPartByMimeType(messagePart, ContentTypeField.TYPE_TEXT_PLAIN);
            if (part3 != null) {
                if (K9.DEBUG) {
                    Log.d("k9", "getBodyTextFromMessage: Text requested, text found.");
                }
                return MessageExtractor.getTextFromPart(part3);
            }
            Part part4 = MimeUtility.findFirstPartByMimeType(messagePart, "text/html");
            if (part4 != null) {
                if (K9.DEBUG) {
                    Log.d("k9", "getBodyTextFromMessage: Text requested, HTML found.");
                }
                return HtmlConverter.htmlToText(MessageExtractor.getTextFromPart(part4));
            }
        }
        return "";
    }

    public static String stripSignatureForHtmlMessage(String content) {
        Matcher dashSignatureHtml = DASH_SIGNATURE_HTML.matcher(content);
        if (dashSignatureHtml.find()) {
            Matcher blockquoteStart = BLOCKQUOTE_START.matcher(content);
            Matcher blockquoteEnd = BLOCKQUOTE_END.matcher(content);
            List<Integer> start = new ArrayList<>();
            List<Integer> end = new ArrayList<>();
            while (blockquoteStart.find()) {
                start.add(Integer.valueOf(blockquoteStart.start()));
            }
            while (blockquoteEnd.find()) {
                end.add(Integer.valueOf(blockquoteEnd.start()));
            }
            if (start.size() != end.size()) {
                Log.d("k9", "There are " + start.size() + " <blockquote> tags, but " + end.size() + " </blockquote> tags. Refusing to strip.");
            } else if (start.size() > 0) {
                dashSignatureHtml.region(0, ((Integer) start.get(0)).intValue());
                if (dashSignatureHtml.find()) {
                    content = content.substring(0, dashSignatureHtml.start());
                } else {
                    int i = 0;
                    while (true) {
                        if (i >= start.size() - 1) {
                            break;
                        }
                        if (((Integer) end.get(i)).intValue() < ((Integer) start.get(i + 1)).intValue()) {
                            dashSignatureHtml.region(((Integer) end.get(i)).intValue(), ((Integer) start.get(i + 1)).intValue());
                            if (dashSignatureHtml.find()) {
                                content = content.substring(0, dashSignatureHtml.start());
                                break;
                            }
                        }
                        i++;
                    }
                    if (((Integer) end.get(end.size() - 1)).intValue() < content.length()) {
                        dashSignatureHtml.region(((Integer) end.get(end.size() - 1)).intValue(), content.length());
                        if (dashSignatureHtml.find()) {
                            content = content.substring(0, dashSignatureHtml.start());
                        }
                    }
                }
            } else {
                content = content.substring(0, dashSignatureHtml.start());
            }
        }
        HtmlCleaner cleaner = new HtmlCleaner();
        CleanerProperties properties = cleaner.getProperties();
        properties.setNamespacesAware(false);
        properties.setAdvancedXmlEscape(false);
        properties.setOmitXmlDeclaration(true);
        properties.setOmitDoctypeDeclaration(false);
        properties.setTranslateSpecialEntities(false);
        properties.setRecognizeUnicodeChars(false);
        return new SimpleHtmlSerializer(properties).getAsString(cleaner.clean(content), "UTF8");
    }

    public static String stripSignatureForTextMessage(String content) {
        if (DASH_SIGNATURE_PLAIN.matcher(content).find()) {
            return DASH_SIGNATURE_PLAIN.matcher(content).replaceFirst("\r\n");
        }
        return content;
    }
}
