package com.fsck.k9.helper;

import android.content.ClipData;
import android.content.Context;

public class ClipboardManager {
    private static ClipboardManager sInstance = null;
    protected Context mContext;

    public static ClipboardManager getInstance(Context context) {
        Context appContext = context.getApplicationContext();
        if (sInstance == null) {
            sInstance = new ClipboardManager(appContext);
        }
        return sInstance;
    }

    protected ClipboardManager(Context context) {
        this.mContext = context;
    }

    public void setText(String label, String text) {
        ((android.content.ClipboardManager) this.mContext.getSystemService("clipboard")).setPrimaryClip(ClipData.newPlainText(label, text));
    }
}
