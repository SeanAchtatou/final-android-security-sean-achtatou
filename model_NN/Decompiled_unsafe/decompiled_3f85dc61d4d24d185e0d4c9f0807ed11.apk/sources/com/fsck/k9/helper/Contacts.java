package com.fsck.k9.helper;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;
import com.fsck.k9.mail.Address;
import com.fsck.k9.provider.AttachmentProvider;

public class Contacts {
    protected static final int CONTACT_ID_INDEX = 2;
    protected static final int NAME_INDEX = 1;
    protected static final String[] PROJECTION = {AttachmentProvider.AttachmentProviderColumns._ID, "display_name", "contact_id"};
    protected static final String SORT_ORDER = "times_contacted DESC, display_name, _id";
    protected ContentResolver mContentResolver;
    protected Context mContext;

    public static Contacts getInstance(Context context) {
        return new Contacts(context);
    }

    protected Contacts(Context context) {
        this.mContext = context;
        this.mContentResolver = context.getContentResolver();
    }

    public void createContact(Address email) {
        Uri contactUri = Uri.fromParts("mailto", email.getAddress(), null);
        Intent contactIntent = new Intent("com.android.contacts.action.SHOW_OR_CREATE_CONTACT");
        contactIntent.setFlags(268435456);
        contactIntent.setData(contactUri);
        contactIntent.putExtra("com.android.contacts.action.CREATE_DESCRIPTION", email.toString());
        String senderPersonal = email.getPersonal();
        if (senderPersonal != null) {
            contactIntent.putExtra("name", senderPersonal);
        }
        this.mContext.startActivity(contactIntent);
    }

    public void addPhoneContact(String phoneNumber) {
        Intent addIntent = new Intent("android.intent.action.INSERT_OR_EDIT");
        addIntent.setType("vnd.android.cursor.item/contact");
        addIntent.putExtra("phone", Uri.decode(phoneNumber));
        addIntent.setFlags(268435456);
        this.mContext.startActivity(addIntent);
    }

    public boolean isInContacts(String emailAddress) {
        boolean result = false;
        Cursor c = getContactByAddress(emailAddress);
        if (c != null) {
            if (c.getCount() > 0) {
                result = true;
            }
            c.close();
        }
        return result;
    }

    public boolean isAnyInContacts(Address[] addresses) {
        if (addresses == null) {
            return false;
        }
        for (Address addr : addresses) {
            if (isInContacts(addr.getAddress())) {
                return true;
            }
        }
        return false;
    }

    public String getNameForAddress(String address) {
        if (address == null) {
            return null;
        }
        Cursor c = getContactByAddress(address);
        String name = null;
        if (c == null) {
            return null;
        }
        if (c.getCount() > 0) {
            c.moveToFirst();
            name = c.getString(1);
        }
        c.close();
        return name;
    }

    public void markAsContacted(Address[] addresses) {
        for (Address address : addresses) {
            Cursor c = getContactByAddress(address.getAddress());
            if (c != null) {
                if (c.getCount() > 0) {
                    c.moveToFirst();
                    ContactsContract.Contacts.markAsContacted(this.mContentResolver, c.getLong(2));
                }
                c.close();
            }
        }
    }

    public Intent contactPickerIntent() {
        return new Intent("android.intent.action.PICK", ContactsContract.CommonDataKinds.Email.CONTENT_URI);
    }

    public Uri getPhotoUri(String address) {
        Cursor c;
        try {
            c = getContactByAddress(address);
            if (c == null) {
                return null;
            }
            if (!c.moveToFirst()) {
                c.close();
                return null;
            }
            Long contactId = Long.valueOf(c.getLong(2));
            c.close();
            Cursor cur = this.mContentResolver.query(ContactsContract.Data.CONTENT_URI, null, "contact_id=" + contactId + " AND " + "mimetype" + "='" + "vnd.android.cursor.item/photo" + "'", null, null);
            if (cur == null) {
                return null;
            }
            if (!cur.moveToFirst()) {
                cur.close();
                return null;
            }
            cur.close();
            return Uri.withAppendedPath(ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, contactId.longValue()), "photo");
        } catch (Exception e) {
            Log.e("k9", "Couldn't fetch photo for contact with email " + address, e);
            return null;
        } catch (Throwable th) {
            c.close();
            throw th;
        }
    }

    private Cursor getContactByAddress(String address) {
        return this.mContentResolver.query(Uri.withAppendedPath(ContactsContract.CommonDataKinds.Email.CONTENT_LOOKUP_URI, Uri.encode(address)), PROJECTION, null, null, SORT_ORDER);
    }
}
