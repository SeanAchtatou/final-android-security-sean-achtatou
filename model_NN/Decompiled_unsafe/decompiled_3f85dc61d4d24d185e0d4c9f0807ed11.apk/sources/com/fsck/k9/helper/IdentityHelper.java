package com.fsck.k9.helper;

import com.fsck.k9.Account;
import com.fsck.k9.Identity;
import com.fsck.k9.mail.Address;
import com.fsck.k9.mail.Message;

public class IdentityHelper {
    public static Identity getRecipientIdentityFromMessage(Account account, Message message) {
        Identity recipient = null;
        Address[] recipients = message.getRecipients(Message.RecipientType.TO);
        int length = recipients.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                break;
            }
            Identity identity = account.findIdentity(recipients[i]);
            if (identity != null) {
                recipient = identity;
                break;
            }
            i++;
        }
        if (recipient == null) {
            Address[] ccAddresses = message.getRecipients(Message.RecipientType.CC);
            if (ccAddresses.length > 0) {
                int length2 = ccAddresses.length;
                int i2 = 0;
                while (true) {
                    if (i2 >= length2) {
                        break;
                    }
                    Identity identity2 = account.findIdentity(ccAddresses[i2]);
                    if (identity2 != null) {
                        recipient = identity2;
                        break;
                    }
                    i2++;
                }
            }
        }
        if (recipient == null) {
            return account.getIdentity(0);
        }
        return recipient;
    }
}
