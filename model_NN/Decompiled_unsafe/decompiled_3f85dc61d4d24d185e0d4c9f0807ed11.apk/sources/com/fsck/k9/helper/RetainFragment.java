package com.fsck.k9.helper;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Build;
import android.os.Bundle;

public class RetainFragment<T> extends Fragment {
    private T data;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public T getData() {
        return this.data;
    }

    public boolean hasData() {
        return this.data != null;
    }

    public void setData(T data2) {
        this.data = data2;
    }

    public static <T> RetainFragment<T> findOrCreate(FragmentManager fm, String tag) {
        RetainFragment<T> retainFragment = (RetainFragment) fm.findFragmentByTag(tag);
        if (retainFragment != null) {
            return retainFragment;
        }
        RetainFragment<T> retainFragment2 = new RetainFragment<>();
        fm.beginTransaction().add(retainFragment2, tag).commitAllowingStateLoss();
        return retainFragment2;
    }

    public void clearAndRemove(FragmentManager fm) {
        this.data = null;
        if (Build.VERSION.SDK_INT < 17 || !fm.isDestroyed()) {
            fm.beginTransaction().remove(this).commitAllowingStateLoss();
        }
    }
}
