package com.fsck.k9.helper;

import com.fsck.k9.Account;
import com.fsck.k9.mail.Address;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.internet.ListHeaders;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class ReplyToParser {
    public ReplyToAddresses getRecipientsToReplyTo(Message message, Account account) {
        Address[] candidateAddress;
        Address[] replyToAddresses = message.getReplyTo();
        Address[] listPostAddresses = ListHeaders.getListPostAddresses(message);
        Address[] fromAddresses = message.getFrom();
        if (replyToAddresses.length > 0) {
            candidateAddress = replyToAddresses;
        } else if (listPostAddresses.length > 0) {
            candidateAddress = listPostAddresses;
        } else {
            candidateAddress = fromAddresses;
        }
        if (account.isAnIdentity(candidateAddress)) {
            candidateAddress = message.getRecipients(Message.RecipientType.TO);
        }
        return new ReplyToAddresses(candidateAddress);
    }

    public ReplyToAddresses getRecipientsToReplyAllTo(Message message, Account account) {
        List<Address> replyToAddresses = Arrays.asList(getRecipientsToReplyTo(message, account).to);
        HashSet<Address> alreadyAddedAddresses = new HashSet<>(replyToAddresses);
        ArrayList<Address> toAddresses = new ArrayList<>(replyToAddresses);
        ArrayList<Address> ccAddresses = new ArrayList<>();
        for (Address address : message.getFrom()) {
            if (!alreadyAddedAddresses.contains(address) && !account.isAnIdentity(address)) {
                toAddresses.add(address);
                alreadyAddedAddresses.add(address);
            }
        }
        for (Address address2 : message.getRecipients(Message.RecipientType.TO)) {
            if (!alreadyAddedAddresses.contains(address2) && !account.isAnIdentity(address2)) {
                toAddresses.add(address2);
                alreadyAddedAddresses.add(address2);
            }
        }
        for (Address address3 : message.getRecipients(Message.RecipientType.CC)) {
            if (!alreadyAddedAddresses.contains(address3) && !account.isAnIdentity(address3)) {
                ccAddresses.add(address3);
                alreadyAddedAddresses.add(address3);
            }
        }
        return new ReplyToAddresses(toAddresses, ccAddresses);
    }

    public static class ReplyToAddresses {
        public final Address[] cc;
        public final Address[] to;

        public ReplyToAddresses(List<Address> toAddresses, List<Address> ccAddresses) {
            this.to = (Address[]) toAddresses.toArray(new Address[toAddresses.size()]);
            this.cc = (Address[]) ccAddresses.toArray(new Address[ccAddresses.size()]);
        }

        public ReplyToAddresses(Address[] toAddresses) {
            this.to = toAddresses;
            this.cc = new Address[0];
        }
    }
}
