package com.fsck.k9.search;

import android.os.Parcel;
import android.os.Parcelable;
import com.fsck.k9.search.SearchSpecification;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class LocalSearch implements SearchSpecification {
    public static final Parcelable.Creator<LocalSearch> CREATOR = new Parcelable.Creator<LocalSearch>() {
        public LocalSearch createFromParcel(Parcel in) {
            return new LocalSearch(in);
        }

        public LocalSearch[] newArray(int size) {
            return new LocalSearch[size];
        }
    };
    private Set<String> mAccountUuids;
    private ConditionsTreeNode mConditions;
    private Set<ConditionsTreeNode> mLeafSet;
    private boolean mManualSearch;
    private String mName;
    private boolean mPredefined;

    public LocalSearch() {
        this.mManualSearch = false;
        this.mAccountUuids = new HashSet();
        this.mConditions = null;
        this.mLeafSet = new HashSet();
    }

    public LocalSearch(String name) {
        this.mManualSearch = false;
        this.mAccountUuids = new HashSet();
        this.mConditions = null;
        this.mLeafSet = new HashSet();
        this.mName = name;
    }

    protected LocalSearch(String name, ConditionsTreeNode searchConditions, String accounts, boolean predefined) {
        this(name);
        this.mConditions = searchConditions;
        this.mPredefined = predefined;
        this.mLeafSet = new HashSet();
        if (this.mConditions != null) {
            this.mLeafSet.addAll(this.mConditions.getLeafSet());
        }
        if (accounts != null) {
            for (String account : accounts.split(",")) {
                this.mAccountUuids.add(account);
            }
        }
    }

    public LocalSearch clone() {
        LocalSearch copy = new LocalSearch(this.mName, this.mConditions == null ? null : this.mConditions.cloneTree(), null, this.mPredefined);
        copy.mManualSearch = this.mManualSearch;
        copy.mAccountUuids = new HashSet(this.mAccountUuids);
        return copy;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public void addAccountUuid(String uuid) {
        if (uuid.equals(SearchSpecification.ALL_ACCOUNTS)) {
            this.mAccountUuids.clear();
        } else {
            this.mAccountUuids.add(uuid);
        }
    }

    public void addAccountUuids(String[] accountUuids) {
        for (String acc : accountUuids) {
            addAccountUuid(acc);
        }
    }

    public boolean removeAccountUuid(String uuid) {
        return this.mAccountUuids.remove(uuid);
    }

    public void and(SearchSpecification.SearchField field, String value, SearchSpecification.Attribute attribute) {
        and(new SearchSpecification.SearchCondition(field, attribute, value));
    }

    public ConditionsTreeNode and(SearchSpecification.SearchCondition condition) {
        try {
            return and(new ConditionsTreeNode(condition));
        } catch (Exception e) {
            return null;
        }
    }

    public ConditionsTreeNode and(ConditionsTreeNode node) throws Exception {
        this.mLeafSet.addAll(node.getLeafSet());
        if (this.mConditions == null) {
            this.mConditions = node;
            return node;
        }
        this.mConditions = this.mConditions.and(node);
        return this.mConditions;
    }

    public ConditionsTreeNode or(SearchSpecification.SearchCondition condition) {
        try {
            return or(new ConditionsTreeNode(condition));
        } catch (Exception e) {
            return null;
        }
    }

    public ConditionsTreeNode or(ConditionsTreeNode node) throws Exception {
        this.mLeafSet.addAll(node.getLeafSet());
        if (this.mConditions == null) {
            this.mConditions = node;
            return node;
        }
        this.mConditions = this.mConditions.or(node);
        return this.mConditions;
    }

    public void addAllowedFolder(String name) {
        this.mConditions = and(new SearchSpecification.SearchCondition(SearchSpecification.SearchField.FOLDER, SearchSpecification.Attribute.EQUALS, name));
    }

    public List<String> getFolderNames() {
        List<String> results = new ArrayList<>();
        for (ConditionsTreeNode node : this.mLeafSet) {
            if (node.mCondition.field == SearchSpecification.SearchField.FOLDER && node.mCondition.attribute == SearchSpecification.Attribute.EQUALS) {
                results.add(node.mCondition.value);
            }
        }
        return results;
    }

    public Set<ConditionsTreeNode> getLeafSet() {
        return this.mLeafSet;
    }

    /* JADX WARNING: Removed duplicated region for block: B:5:0x0012  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String getRemoteSearchArguments() {
        /*
            r6 = this;
            r2 = 0
            java.util.Set r0 = r6.getLeafSet()
            if (r0 != 0) goto L_0x0008
        L_0x0007:
            return r2
        L_0x0008:
            java.util.Iterator r3 = r0.iterator()
        L_0x000c:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x0007
            java.lang.Object r1 = r3.next()
            com.fsck.k9.search.ConditionsTreeNode r1 = (com.fsck.k9.search.ConditionsTreeNode) r1
            com.fsck.k9.search.SearchSpecification$SearchCondition r4 = r1.getCondition()
            com.fsck.k9.search.SearchSpecification$SearchField r4 = r4.field
            com.fsck.k9.search.SearchSpecification$SearchField r5 = com.fsck.k9.search.SearchSpecification.SearchField.SUBJECT
            if (r4 == r5) goto L_0x002c
            com.fsck.k9.search.SearchSpecification$SearchCondition r4 = r1.getCondition()
            com.fsck.k9.search.SearchSpecification$SearchField r4 = r4.field
            com.fsck.k9.search.SearchSpecification$SearchField r5 = com.fsck.k9.search.SearchSpecification.SearchField.SENDER
            if (r4 != r5) goto L_0x000c
        L_0x002c:
            com.fsck.k9.search.SearchSpecification$SearchCondition r2 = r1.getCondition()
            java.lang.String r2 = r2.value
            goto L_0x0007
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fsck.k9.search.LocalSearch.getRemoteSearchArguments():java.lang.String");
    }

    public String getName() {
        return this.mName == null ? "" : this.mName;
    }

    public boolean isPredefined() {
        return this.mPredefined;
    }

    public boolean isManualSearch() {
        return this.mManualSearch;
    }

    public void setManualSearch(boolean manualSearch) {
        this.mManualSearch = manualSearch;
    }

    public String[] getAccountUuids() {
        if (this.mAccountUuids.isEmpty()) {
            return new String[]{SearchSpecification.ALL_ACCOUNTS};
        }
        String[] tmp = new String[this.mAccountUuids.size()];
        this.mAccountUuids.toArray(tmp);
        return tmp;
    }

    public boolean searchAllAccounts() {
        return this.mAccountUuids.isEmpty();
    }

    public ConditionsTreeNode getConditions() {
        return this.mConditions;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        int i;
        int i2 = 1;
        dest.writeString(this.mName);
        if (this.mPredefined) {
            i = 1;
        } else {
            i = 0;
        }
        dest.writeByte((byte) i);
        if (!this.mManualSearch) {
            i2 = 0;
        }
        dest.writeByte((byte) i2);
        dest.writeStringList(new ArrayList(this.mAccountUuids));
        dest.writeParcelable(this.mConditions, flags);
    }

    public LocalSearch(Parcel in) {
        boolean z = true;
        this.mManualSearch = false;
        this.mAccountUuids = new HashSet();
        this.mConditions = null;
        this.mLeafSet = new HashSet();
        this.mName = in.readString();
        this.mPredefined = in.readByte() == 1;
        this.mManualSearch = in.readByte() != 1 ? false : z;
        this.mAccountUuids.addAll(in.createStringArrayList());
        this.mConditions = (ConditionsTreeNode) in.readParcelable(LocalSearch.class.getClassLoader());
        this.mLeafSet = this.mConditions == null ? null : this.mConditions.getLeafSet();
    }
}
