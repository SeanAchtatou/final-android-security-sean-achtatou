package com.fsck.k9.search;

import android.util.Log;
import com.fsck.k9.Account;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mailstore.LocalFolder;
import com.fsck.k9.provider.EmailProvider;
import com.fsck.k9.search.SearchSpecification;
import exts.whats.Constants;
import java.util.List;

public class SqlQueryBuilder {
    public static void buildWhereClause(Account account, ConditionsTreeNode node, StringBuilder query, List<String> selectionArgs) {
        buildWhereClauseInternal(account, node, query, selectionArgs);
    }

    private static void buildWhereClauseInternal(Account account, ConditionsTreeNode node, StringBuilder query, List<String> selectionArgs) {
        if (node == null) {
            query.append(Constants.INSTALL_ID);
        } else if (node.mLeft == null && node.mRight == null) {
            SearchSpecification.SearchCondition condition = node.mCondition;
            switch (condition.field) {
                case FOLDER:
                    long folderId = getFolderId(account, condition.value);
                    if (condition.attribute == SearchSpecification.Attribute.EQUALS) {
                        query.append("folder_id = ?");
                    } else {
                        query.append("folder_id != ?");
                    }
                    selectionArgs.add(Long.toString(folderId));
                    return;
                case SEARCHABLE:
                    switch (account.getSearchableFolders()) {
                        case ALL:
                            LocalSearch tempSearch = new LocalSearch();
                            account.excludeUnwantedFolders(tempSearch);
                            buildWhereClauseInternal(account, tempSearch.getConditions(), query, selectionArgs);
                            return;
                        case DISPLAYABLE:
                            LocalSearch tempSearch2 = new LocalSearch();
                            account.excludeSpecialFolders(tempSearch2);
                            account.limitToDisplayableFolders(tempSearch2);
                            buildWhereClauseInternal(account, tempSearch2.getConditions(), query, selectionArgs);
                            return;
                        case NONE:
                            query.append("0");
                            return;
                        default:
                            return;
                    }
                case MESSAGE_CONTENTS:
                    String fulltextQueryString = condition.value;
                    if (condition.attribute != SearchSpecification.Attribute.CONTAINS) {
                        Log.e("k9", "message contents can only be matched!");
                    }
                    query.append("(EXISTS (SELECT docid FROM messages_fulltext WHERE docid = id AND fulltext MATCH ?))");
                    selectionArgs.add(fulltextQueryString);
                    return;
                default:
                    appendCondition(condition, query, selectionArgs);
                    return;
            }
        } else {
            query.append("(");
            buildWhereClauseInternal(account, node.mLeft, query, selectionArgs);
            query.append(") ");
            query.append(node.mValue.name());
            query.append(" (");
            buildWhereClauseInternal(account, node.mRight, query, selectionArgs);
            query.append(")");
        }
    }

    private static void appendCondition(SearchSpecification.SearchCondition condition, StringBuilder query, List<String> selectionArgs) {
        query.append(getColumnName(condition));
        appendExprRight(condition, query, selectionArgs);
    }

    private static long getFolderId(Account account, String folderName) {
        try {
            LocalFolder folder = account.getLocalStore().getFolder(folderName);
            folder.open(1);
            return folder.getId();
        } catch (MessagingException e) {
            e.printStackTrace();
            return 0;
        }
    }

    private static String getColumnName(SearchSpecification.SearchCondition condition) {
        String columnName = null;
        switch (condition.field) {
            case ATTACHMENT_COUNT:
                columnName = EmailProvider.MessageColumns.ATTACHMENT_COUNT;
                break;
            case BCC:
                columnName = EmailProvider.MessageColumns.BCC_LIST;
                break;
            case CC:
                columnName = EmailProvider.MessageColumns.CC_LIST;
                break;
            case DATE:
                columnName = "date";
                break;
            case DELETED:
                columnName = EmailProvider.InternalMessageColumns.DELETED;
                break;
            case FLAG:
                columnName = EmailProvider.MessageColumns.FLAGS;
                break;
            case ID:
                columnName = "id";
                break;
            case REPLY_TO:
                columnName = EmailProvider.MessageColumns.REPLY_TO_LIST;
                break;
            case SENDER:
                columnName = EmailProvider.MessageColumns.SENDER_LIST;
                break;
            case SUBJECT:
                columnName = "subject";
                break;
            case TO:
                columnName = EmailProvider.MessageColumns.TO_LIST;
                break;
            case UID:
                columnName = EmailProvider.MessageColumns.UID;
                break;
            case INTEGRATE:
                columnName = "integrate";
                break;
            case READ:
                columnName = EmailProvider.MessageColumns.READ;
                break;
            case FLAGGED:
                columnName = EmailProvider.MessageColumns.FLAGGED;
                break;
            case DISPLAY_CLASS:
                columnName = EmailProvider.FolderColumns.DISPLAY_CLASS;
                break;
            case THREAD_ID:
                columnName = "threads.root";
                break;
        }
        if (columnName != null) {
            return columnName;
        }
        throw new RuntimeException("Unhandled case");
    }

    private static void appendExprRight(SearchSpecification.SearchCondition condition, StringBuilder query, List<String> selectionArgs) {
        String value = condition.value;
        SearchSpecification.SearchField field = condition.field;
        query.append(" ");
        String selectionArg = null;
        switch (condition.attribute) {
            case NOT_CONTAINS:
                query.append("NOT ");
            case CONTAINS:
                query.append("LIKE ?");
                selectionArg = "%" + value + "%";
                break;
            case NOT_STARTSWITH:
                query.append("NOT ");
            case STARTSWITH:
                query.append("LIKE ?");
                selectionArg = "%" + value;
                break;
            case NOT_ENDSWITH:
                query.append("NOT ");
            case ENDSWITH:
                query.append("LIKE ?");
                selectionArg = value + "%";
                break;
            case NOT_EQUALS:
                if (isNumberColumn(field)) {
                    query.append("!= ?");
                } else {
                    query.append("NOT LIKE ?");
                }
                selectionArg = value;
                break;
            case EQUALS:
                if (isNumberColumn(field)) {
                    query.append("= ?");
                } else {
                    query.append("LIKE ?");
                }
                selectionArg = value;
                break;
        }
        if (selectionArg == null) {
            throw new RuntimeException("Unhandled case");
        }
        selectionArgs.add(selectionArg);
    }

    private static boolean isNumberColumn(SearchSpecification.SearchField field) {
        switch (field) {
            case FOLDER:
            case ATTACHMENT_COUNT:
            case DATE:
            case DELETED:
            case ID:
            case INTEGRATE:
            case READ:
            case FLAGGED:
            case THREAD_ID:
                return true;
            case SEARCHABLE:
            case MESSAGE_CONTENTS:
            case BCC:
            case CC:
            case FLAG:
            case REPLY_TO:
            case SENDER:
            case SUBJECT:
            case TO:
            case UID:
            case DISPLAY_CLASS:
            default:
                return false;
        }
    }

    public static String addPrefixToSelection(String[] columnNames, String prefix, String selection) {
        String result = selection;
        for (String columnName : columnNames) {
            result = result.replaceAll("(?<=^|[^\\.])\\b" + columnName + "\\b", prefix + columnName);
        }
        return result;
    }
}
