package com.fsck.k9.service;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import com.fsck.k9.Account;
import com.fsck.k9.K9;
import com.fsck.k9.Preferences;
import com.fsck.k9.controller.MessagingController;
import com.fsck.k9.helper.Utility;
import com.fsck.k9.mail.Pusher;
import com.fsck.k9.preferences.Storage;
import com.fsck.k9.preferences.StorageEditor;
import java.util.Date;

public class MailService extends CoreService {
    private static final String ACTION_CANCEL = "com.fsck.k9.intent.action.MAIL_SERVICE_CANCEL";
    private static final String ACTION_CHECK_MAIL = "com.fsck.k9.intent.action.MAIL_SERVICE_WAKEUP";
    private static final String ACTION_REFRESH_PUSHERS = "com.fsck.k9.intent.action.MAIL_SERVICE_REFRESH_PUSHERS";
    private static final String ACTION_RESCHEDULE_POLL = "com.fsck.k9.intent.action.MAIL_SERVICE_RESCHEDULE_POLL";
    private static final String ACTION_RESET = "com.fsck.k9.intent.action.MAIL_SERVICE_RESET";
    private static final String ACTION_RESTART_PUSHERS = "com.fsck.k9.intent.action.MAIL_SERVICE_RESTART_PUSHERS";
    private static final String CANCEL_CONNECTIVITY_NOTICE = "com.fsck.k9.intent.action.MAIL_SERVICE_CANCEL_CONNECTIVITY_NOTICE";
    private static final String CONNECTIVITY_CHANGE = "com.fsck.k9.intent.action.MAIL_SERVICE_CONNECTIVITY_CHANGE";
    private static final String LAST_CHECK_END = "MailService.lastCheckEnd";
    private static final String PREVIOUS_INTERVAL = "MailService.previousInterval";
    private static long nextCheck = -1;
    private static boolean pollingRequested = false;
    private static boolean pushingRequested = false;
    private static boolean syncBlocked = false;

    public static void actionReset(Context context, Integer wakeLockId) {
        Intent i = new Intent();
        i.setClass(context, MailService.class);
        i.setAction(ACTION_RESET);
        addWakeLockId(context, i, wakeLockId, true);
        context.startService(i);
    }

    public static void actionRestartPushers(Context context, Integer wakeLockId) {
        Intent i = new Intent();
        i.setClass(context, MailService.class);
        i.setAction(ACTION_RESTART_PUSHERS);
        addWakeLockId(context, i, wakeLockId, true);
        context.startService(i);
    }

    public static void actionReschedulePoll(Context context, Integer wakeLockId) {
        Intent i = new Intent();
        i.setClass(context, MailService.class);
        i.setAction(ACTION_RESCHEDULE_POLL);
        addWakeLockId(context, i, wakeLockId, true);
        context.startService(i);
    }

    public static void actionCancel(Context context, Integer wakeLockId) {
        Intent i = new Intent();
        i.setClass(context, MailService.class);
        i.setAction(ACTION_CANCEL);
        addWakeLockId(context, i, wakeLockId, false);
        context.startService(i);
    }

    public static void connectivityChange(Context context, Integer wakeLockId) {
        Intent i = new Intent();
        i.setClass(context, MailService.class);
        i.setAction(CONNECTIVITY_CHANGE);
        addWakeLockId(context, i, wakeLockId, false);
        context.startService(i);
    }

    public void onCreate() {
        super.onCreate();
        if (K9.DEBUG) {
            Log.v("k9", "***** MailService *****: onCreate");
        }
    }

    public int startService(Intent intent, int startId) {
        boolean z;
        long startTime = System.currentTimeMillis();
        boolean oldIsSyncDisabled = isSyncDisabled();
        boolean doBackground = true;
        boolean hasConnectivity = Utility.hasConnectivity(getApplication());
        boolean autoSync = ContentResolver.getMasterSyncAutomatically();
        switch (K9.getBackgroundOps()) {
            case NEVER:
                doBackground = false;
                break;
            case ALWAYS:
                doBackground = true;
                break;
            case WHEN_CHECKED_AUTO_SYNC:
                doBackground = autoSync;
                break;
        }
        if (!doBackground || !hasConnectivity) {
            z = true;
        } else {
            z = false;
        }
        syncBlocked = z;
        if (K9.DEBUG) {
            Log.i("k9", "MailService.onStart(" + intent + ", " + startId + "), hasConnectivity = " + hasConnectivity + ", doBackground = " + doBackground);
        }
        if (ACTION_CHECK_MAIL.equals(intent.getAction())) {
            if (K9.DEBUG) {
                Log.i("k9", "***** MailService *****: checking mail");
            }
            if (hasConnectivity && doBackground) {
                PollService.startService(this);
            }
            reschedulePollInBackground(hasConnectivity, doBackground, Integer.valueOf(startId), false);
        } else if (ACTION_CANCEL.equals(intent.getAction())) {
            if (K9.DEBUG) {
                Log.v("k9", "***** MailService *****: cancel");
            }
            cancel();
        } else if (ACTION_RESET.equals(intent.getAction())) {
            if (K9.DEBUG) {
                Log.v("k9", "***** MailService *****: reschedule");
            }
            rescheduleAllInBackground(hasConnectivity, doBackground, Integer.valueOf(startId));
        } else if (ACTION_RESTART_PUSHERS.equals(intent.getAction())) {
            if (K9.DEBUG) {
                Log.v("k9", "***** MailService *****: restarting pushers");
            }
            reschedulePushersInBackground(hasConnectivity, doBackground, Integer.valueOf(startId));
        } else if (ACTION_RESCHEDULE_POLL.equals(intent.getAction())) {
            if (K9.DEBUG) {
                Log.v("k9", "***** MailService *****: rescheduling poll");
            }
            reschedulePollInBackground(hasConnectivity, doBackground, Integer.valueOf(startId), true);
        } else if (ACTION_REFRESH_PUSHERS.equals(intent.getAction())) {
            refreshPushersInBackground(hasConnectivity, doBackground, Integer.valueOf(startId));
        } else if (CONNECTIVITY_CHANGE.equals(intent.getAction())) {
            rescheduleAllInBackground(hasConnectivity, doBackground, Integer.valueOf(startId));
            if (K9.DEBUG) {
                Log.i("k9", "Got connectivity action with hasConnectivity = " + hasConnectivity + ", doBackground = " + doBackground);
            }
        } else if (CANCEL_CONNECTIVITY_NOTICE.equals(intent.getAction())) {
        }
        if (isSyncDisabled() != oldIsSyncDisabled) {
            MessagingController.getInstance(getApplication()).systemStatusChanged();
        }
        if (!K9.DEBUG) {
            return 2;
        }
        Log.i("k9", "MailService.onStart took " + (System.currentTimeMillis() - startTime) + "ms");
        return 2;
    }

    public void onDestroy() {
        if (K9.DEBUG) {
            Log.v("k9", "***** MailService *****: onDestroy()");
        }
        super.onDestroy();
    }

    private void cancel() {
        Intent i = new Intent(this, MailService.class);
        i.setAction(ACTION_CHECK_MAIL);
        BootReceiver.cancelIntent(this, i);
    }

    public static void saveLastCheckEnd(Context context) {
        long lastCheckEnd = System.currentTimeMillis();
        if (K9.DEBUG) {
            Log.i("k9", "Saving lastCheckEnd = " + new Date(lastCheckEnd));
        }
        StorageEditor editor = Preferences.getPreferences(context).getStorage().edit();
        editor.putLong(LAST_CHECK_END, lastCheckEnd);
        editor.commit();
    }

    private void rescheduleAllInBackground(final boolean hasConnectivity, final boolean doBackground, Integer startId) {
        execute(getApplication(), new Runnable() {
            public void run() {
                MailService.this.reschedulePoll(hasConnectivity, doBackground, true);
                MailService.this.reschedulePushers(hasConnectivity, doBackground);
            }
        }, 60000, startId);
    }

    private void reschedulePollInBackground(final boolean hasConnectivity, final boolean doBackground, Integer startId, final boolean considerLastCheckEnd) {
        execute(getApplication(), new Runnable() {
            public void run() {
                MailService.this.reschedulePoll(hasConnectivity, doBackground, considerLastCheckEnd);
            }
        }, 60000, startId);
    }

    private void reschedulePushersInBackground(final boolean hasConnectivity, final boolean doBackground, Integer startId) {
        execute(getApplication(), new Runnable() {
            public void run() {
                MailService.this.reschedulePushers(hasConnectivity, doBackground);
            }
        }, 60000, startId);
    }

    private void refreshPushersInBackground(boolean hasConnectivity, boolean doBackground, Integer startId) {
        if (hasConnectivity && doBackground) {
            execute(getApplication(), new Runnable() {
                public void run() {
                    MailService.this.refreshPushers();
                    MailService.this.schedulePushers();
                }
            }, 60000, startId);
        }
    }

    /* access modifiers changed from: private */
    public void reschedulePoll(boolean hasConnectivity, boolean doBackground, boolean considerLastCheckEnd) {
        long base;
        if (!hasConnectivity || !doBackground) {
            if (K9.DEBUG) {
                Log.i("k9", "No connectivity, canceling check for " + getApplication().getPackageName());
            }
            nextCheck = -1;
            cancel();
            return;
        }
        Preferences prefs = Preferences.getPreferences(this);
        Storage storage = prefs.getStorage();
        int previousInterval = storage.getInt(PREVIOUS_INTERVAL, -1);
        long lastCheckEnd = storage.getLong(LAST_CHECK_END, -1);
        if (lastCheckEnd > System.currentTimeMillis()) {
            Log.i("k9", "The database claims that the last time mail was checked was in the future (" + lastCheckEnd + "). To try to get things back to normal, " + "the last check time has been reset to: " + System.currentTimeMillis());
            lastCheckEnd = System.currentTimeMillis();
        }
        int shortestInterval = -1;
        for (Account account : prefs.getAvailableAccounts()) {
            if (!(account.getAutomaticCheckIntervalMinutes() == -1 || account.getFolderSyncMode() == Account.FolderMode.NONE)) {
                if (account.getAutomaticCheckIntervalMinutes() < shortestInterval || shortestInterval == -1) {
                    shortestInterval = account.getAutomaticCheckIntervalMinutes();
                }
            }
        }
        StorageEditor editor = storage.edit();
        editor.putInt(PREVIOUS_INTERVAL, shortestInterval);
        editor.commit();
        if (shortestInterval == -1) {
            if (K9.DEBUG) {
                Log.i("k9", "No next check scheduled for package " + getApplication().getPackageName());
            }
            nextCheck = -1;
            pollingRequested = false;
            cancel();
            return;
        }
        long delay = (long) (60000 * shortestInterval);
        if (previousInterval == -1 || lastCheckEnd == -1 || !considerLastCheckEnd) {
            base = System.currentTimeMillis();
        } else {
            base = lastCheckEnd;
        }
        long nextTime = base + delay;
        if (K9.DEBUG) {
            Log.i("k9", "previousInterval = " + previousInterval + ", shortestInterval = " + shortestInterval + ", lastCheckEnd = " + new Date(lastCheckEnd) + ", considerLastCheckEnd = " + considerLastCheckEnd);
        }
        nextCheck = nextTime;
        pollingRequested = true;
        try {
            if (K9.DEBUG) {
                Log.i("k9", "Next check for package " + getApplication().getPackageName() + " scheduled for " + new Date(nextTime));
            }
        } catch (Exception e) {
            Log.e("k9", "Exception while logging", e);
        }
        Intent i = new Intent(this, MailService.class);
        i.setAction(ACTION_CHECK_MAIL);
        BootReceiver.scheduleIntent(this, nextTime, i);
    }

    public static boolean isSyncDisabled() {
        return syncBlocked || (!pollingRequested && !pushingRequested);
    }

    private void stopPushers() {
        MessagingController.getInstance(getApplication()).stopAllPushing();
        PushService.stopService(this);
    }

    /* access modifiers changed from: private */
    public void reschedulePushers(boolean hasConnectivity, boolean doBackground) {
        if (K9.DEBUG) {
            Log.i("k9", "Rescheduling pushers");
        }
        stopPushers();
        if (hasConnectivity && doBackground) {
            setupPushers();
            schedulePushers();
        } else if (K9.DEBUG) {
            Log.i("k9", "Not scheduling pushers:  connectivity? " + hasConnectivity + " -- doBackground? " + doBackground);
        }
    }

    private void setupPushers() {
        boolean pushing = false;
        for (Account account : Preferences.getPreferences(this).getAccounts()) {
            if (K9.DEBUG) {
                Log.i("k9", "Setting up pushers for account " + account.getDescription());
            }
            if (account.isEnabled() && account.isAvailable(getApplicationContext())) {
                pushing |= MessagingController.getInstance(getApplication()).setupPushing(account);
            }
        }
        if (pushing) {
            PushService.startService(this);
        }
        pushingRequested = pushing;
    }

    /* access modifiers changed from: private */
    public void refreshPushers() {
        try {
            long nowTime = System.currentTimeMillis();
            if (K9.DEBUG) {
                Log.i("k9", "Refreshing pushers");
            }
            for (Pusher pusher : MessagingController.getInstance(getApplication()).getPushers()) {
                long lastRefresh = pusher.getLastRefresh();
                int refreshInterval = pusher.getRefreshInterval();
                long sinceLast = nowTime - lastRefresh;
                if (10000 + sinceLast > ((long) refreshInterval)) {
                    if (K9.DEBUG) {
                        Log.d("k9", "PUSHREFRESH: refreshing lastRefresh = " + lastRefresh + ", interval = " + refreshInterval + ", nowTime = " + nowTime + ", sinceLast = " + sinceLast);
                    }
                    pusher.refresh();
                    pusher.setLastRefresh(nowTime);
                } else if (K9.DEBUG) {
                    Log.d("k9", "PUSHREFRESH: NOT refreshing lastRefresh = " + lastRefresh + ", interval = " + refreshInterval + ", nowTime = " + nowTime + ", sinceLast = " + sinceLast);
                }
            }
            if (K9.DEBUG) {
                Log.d("k9", "PUSHREFRESH:  trying to send mail in all folders!");
            }
            MessagingController.getInstance(getApplication()).sendPendingMessages(null);
        } catch (Exception e) {
            Log.e("k9", "Exception while refreshing pushers", e);
        }
    }

    /* access modifiers changed from: private */
    public void schedulePushers() {
        int minInterval = -1;
        for (Pusher pusher : MessagingController.getInstance(getApplication()).getPushers()) {
            int interval = pusher.getRefreshInterval();
            if (interval > 0 && (interval < minInterval || minInterval == -1)) {
                minInterval = interval;
            }
        }
        if (K9.DEBUG) {
            Log.v("k9", "Pusher refresh interval = " + minInterval);
        }
        if (minInterval > 0) {
            long nextTime = System.currentTimeMillis() + ((long) minInterval);
            if (K9.DEBUG) {
                Log.d("k9", "Next pusher refresh scheduled for " + new Date(nextTime));
            }
            Intent i = new Intent(this, MailService.class);
            i.setAction(ACTION_REFRESH_PUSHERS);
            BootReceiver.scheduleIntent(this, nextTime, i);
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public static long getNextPollTime() {
        return nextCheck;
    }
}
