package com.fsck.k9.service;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import com.fsck.k9.K9;
import com.fsck.k9.helper.K9AlarmManager;
import java.util.Date;

public class BootReceiver extends CoreReceiver {
    public static final String ALARMED_INTENT = "com.fsck.k9.service.BroadcastReceiver.pendingIntent";
    public static final String AT_TIME = "com.fsck.k9.service.BroadcastReceiver.atTime";
    public static final String CANCEL_INTENT = "com.fsck.k9.service.BroadcastReceiver.cancelIntent";
    public static final String FIRE_INTENT = "com.fsck.k9.service.BroadcastReceiver.fireIntent";
    public static final String SCHEDULE_INTENT = "com.fsck.k9.service.BroadcastReceiver.scheduleIntent";

    public Integer receive(Context context, Intent intent, Integer tmpWakeLockId) {
        if (K9.DEBUG) {
            Log.i("k9", "BootReceiver.onReceive" + intent);
        }
        String action = intent.getAction();
        if ("android.intent.action.BOOT_COMPLETED".equals(action)) {
            return tmpWakeLockId;
        }
        if ("android.intent.action.DEVICE_STORAGE_LOW".equals(action)) {
            MailService.actionCancel(context, tmpWakeLockId);
            return null;
        } else if ("android.intent.action.DEVICE_STORAGE_OK".equals(action)) {
            MailService.actionReset(context, tmpWakeLockId);
            return null;
        } else if ("android.net.conn.CONNECTIVITY_CHANGE".equals(action)) {
            MailService.connectivityChange(context, tmpWakeLockId);
            return null;
        } else if ("com.android.sync.SYNC_CONN_STATUS_CHANGED".equals(action)) {
            if (K9.getBackgroundOps() != K9.BACKGROUND_OPS.WHEN_CHECKED_AUTO_SYNC) {
                return tmpWakeLockId;
            }
            MailService.actionReset(context, tmpWakeLockId);
            return null;
        } else if (FIRE_INTENT.equals(action)) {
            Intent alarmedIntent = (Intent) intent.getParcelableExtra(ALARMED_INTENT);
            String alarmedAction = alarmedIntent.getAction();
            if (K9.DEBUG) {
                Log.i("k9", "BootReceiver Got alarm to fire alarmedIntent " + alarmedAction);
            }
            alarmedIntent.putExtra(CoreReceiver.WAKE_LOCK_ID, tmpWakeLockId);
            context.startService(alarmedIntent);
            return null;
        } else if (SCHEDULE_INTENT.equals(action)) {
            long atTime = intent.getLongExtra(AT_TIME, -1);
            Intent alarmedIntent2 = (Intent) intent.getParcelableExtra(ALARMED_INTENT);
            if (K9.DEBUG) {
                Log.i("k9", "BootReceiver Scheduling intent " + alarmedIntent2 + " for " + new Date(atTime));
            }
            K9AlarmManager.getAlarmManager(context).set(0, atTime, buildPendingIntent(context, intent));
            return tmpWakeLockId;
        } else if (!CANCEL_INTENT.equals(action)) {
            return tmpWakeLockId;
        } else {
            Intent alarmedIntent3 = (Intent) intent.getParcelableExtra(ALARMED_INTENT);
            if (K9.DEBUG) {
                Log.i("k9", "BootReceiver Canceling alarmedIntent " + alarmedIntent3);
            }
            K9AlarmManager.getAlarmManager(context).cancel(buildPendingIntent(context, intent));
            return tmpWakeLockId;
        }
    }

    private PendingIntent buildPendingIntent(Context context, Intent intent) {
        Intent alarmedIntent = (Intent) intent.getParcelableExtra(ALARMED_INTENT);
        String alarmedAction = alarmedIntent.getAction();
        Intent i = new Intent(context, BootReceiver.class);
        i.setAction(FIRE_INTENT);
        i.putExtra(ALARMED_INTENT, alarmedIntent);
        i.setData(Uri.parse("action://" + alarmedAction));
        return PendingIntent.getBroadcast(context, 0, i, 0);
    }

    public static void scheduleIntent(Context context, long atTime, Intent alarmedIntent) {
        if (K9.DEBUG) {
            Log.i("k9", "BootReceiver Got request to schedule alarmedIntent " + alarmedIntent.getAction());
        }
        Intent i = new Intent();
        i.setClass(context, BootReceiver.class);
        i.setAction(SCHEDULE_INTENT);
        i.putExtra(ALARMED_INTENT, alarmedIntent);
        i.putExtra(AT_TIME, atTime);
        context.sendBroadcast(i);
    }

    public static void cancelIntent(Context context, Intent alarmedIntent) {
        if (K9.DEBUG) {
            Log.i("k9", "BootReceiver Got request to cancel alarmedIntent " + alarmedIntent.getAction());
        }
        Intent i = new Intent();
        i.setClass(context, BootReceiver.class);
        i.setAction(CANCEL_INTENT);
        i.putExtra(ALARMED_INTENT, alarmedIntent);
        context.sendBroadcast(i);
    }

    public static void purgeSchedule(Context context) {
        K9AlarmManager.getAlarmManager(context).cancel(PendingIntent.getBroadcast(context, 0, new Intent() {
            public boolean filterEquals(Intent other) {
                return true;
            }
        }, 0));
    }
}
