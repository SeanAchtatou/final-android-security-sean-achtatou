package com.fsck.k9.service;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import com.fsck.k9.Account;
import com.fsck.k9.K9;
import com.fsck.k9.Preferences;
import com.fsck.k9.remotecontrol.K9RemoteControl;
import java.util.List;

public class RemoteControlReceiver extends CoreReceiver {
    public Integer receive(Context context, Intent intent, Integer tmpWakeLockId) {
        if (K9.DEBUG) {
            Log.i("k9", "RemoteControlReceiver.onReceive" + intent);
        }
        if (K9RemoteControl.K9_SET.equals(intent.getAction())) {
            RemoteControlService.set(context, intent, tmpWakeLockId);
            return null;
        } else if (!K9RemoteControl.K9_REQUEST_ACCOUNTS.equals(intent.getAction())) {
            return tmpWakeLockId;
        } else {
            try {
                List<Account> accounts = Preferences.getPreferences(context).getAccounts();
                String[] uuids = new String[accounts.size()];
                String[] descriptions = new String[accounts.size()];
                for (int i = 0; i < accounts.size(); i++) {
                    Account account = accounts.get(i);
                    uuids[i] = account.getUuid();
                    descriptions[i] = account.getDescription();
                }
                Bundle bundle = getResultExtras(true);
                bundle.putStringArray(K9RemoteControl.K9_ACCOUNT_UUIDS, uuids);
                bundle.putStringArray(K9RemoteControl.K9_ACCOUNT_DESCRIPTIONS, descriptions);
                return tmpWakeLockId;
            } catch (Exception e) {
                Log.e("k9", "Could not handle K9_RESPONSE_INTENT", e);
                return tmpWakeLockId;
            }
        }
    }
}
