package com.fsck.k9.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import com.fsck.k9.K9;
import com.fsck.k9.mailstore.StorageManager;

public class StorageReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Uri uri = intent.getData();
        if (uri != null && uri.getPath() != null) {
            if (K9.DEBUG) {
                Log.v("k9", "StorageReceiver: " + intent.toString());
            }
            String path = uri.getPath();
            if ("android.intent.action.MEDIA_MOUNTED".equals(action)) {
                StorageManager.getInstance(context).onMount(path, intent.getBooleanExtra("read-only", true));
            }
        }
    }
}
