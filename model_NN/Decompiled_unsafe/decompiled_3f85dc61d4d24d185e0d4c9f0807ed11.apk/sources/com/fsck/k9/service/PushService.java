package com.fsck.k9.service;

import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import com.fsck.k9.K9;

public class PushService extends CoreService {
    private static String START_SERVICE = "com.fsck.k9.service.PushService.startService";
    private static String STOP_SERVICE = "com.fsck.k9.service.PushService.stopService";

    public static void startService(Context context) {
        Intent i = new Intent();
        i.setClass(context, PushService.class);
        i.setAction(START_SERVICE);
        addWakeLock(context, i);
        context.startService(i);
    }

    public static void stopService(Context context) {
        Intent i = new Intent();
        i.setClass(context, PushService.class);
        i.setAction(STOP_SERVICE);
        addWakeLock(context, i);
        context.startService(i);
    }

    public int startService(Intent intent, int startId) {
        if (START_SERVICE.equals(intent.getAction())) {
            if (!K9.DEBUG) {
                return 1;
            }
            Log.i("k9", "PushService started with startId = " + startId);
            return 1;
        } else if (!STOP_SERVICE.equals(intent.getAction())) {
            return 1;
        } else {
            if (K9.DEBUG) {
                Log.i("k9", "PushService stopping with startId = " + startId);
            }
            stopSelf(startId);
            return 2;
        }
    }

    public void onCreate() {
        super.onCreate();
        setAutoShutdown(false);
    }

    public IBinder onBind(Intent arg0) {
        return null;
    }
}
