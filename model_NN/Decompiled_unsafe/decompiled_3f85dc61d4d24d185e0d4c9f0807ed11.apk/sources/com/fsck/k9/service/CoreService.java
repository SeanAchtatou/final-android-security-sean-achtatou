package com.fsck.k9.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import com.fsck.k9.K9;
import com.fsck.k9.controller.MessagingController;
import com.fsck.k9.mail.power.TracingPowerManager;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class CoreService extends Service {
    public static final String WAKE_LOCK_ID = "com.fsck.k9.service.CoreService.wakeLockId";
    private static AtomicInteger sWakeLockSeq = new AtomicInteger(0);
    private static ConcurrentHashMap<Integer, TracingPowerManager.TracingWakeLock> sWakeLocks = new ConcurrentHashMap<>();
    /* access modifiers changed from: private */
    public final String className = getClass().getName();
    private boolean mAutoShutdown = true;
    protected boolean mImmediateShutdown = true;
    private volatile boolean mShutdown = false;
    private ExecutorService mThreadPool = null;

    public abstract int startService(Intent intent, int i);

    protected static void addWakeLockId(Context context, Intent intent, Integer wakeLockId, boolean createIfNotExists) {
        if (wakeLockId != null) {
            intent.putExtra(CoreReceiver.WAKE_LOCK_ID, wakeLockId);
        } else if (createIfNotExists) {
            addWakeLock(context, intent);
        }
    }

    protected static void addWakeLock(Context context, Intent intent) {
        intent.putExtra(WAKE_LOCK_ID, registerWakeLock(acquireWakeLock(context, "CoreService addWakeLock", 60000)));
    }

    protected static Integer registerWakeLock(TracingPowerManager.TracingWakeLock wakeLock) {
        Integer tmpWakeLockId = Integer.valueOf(sWakeLockSeq.getAndIncrement());
        sWakeLocks.put(tmpWakeLockId, wakeLock);
        return tmpWakeLockId;
    }

    protected static TracingPowerManager.TracingWakeLock acquireWakeLock(Context context, String tag, long timeout) {
        TracingPowerManager.TracingWakeLock wakeLock = TracingPowerManager.getPowerManager(context).newWakeLock(1, tag);
        wakeLock.setReferenceCounted(false);
        wakeLock.acquire(timeout);
        return wakeLock;
    }

    public void onCreate() {
        if (K9.DEBUG) {
            Log.i("k9", "CoreService: " + this.className + ".onCreate()");
        }
        this.mThreadPool = Executors.newFixedThreadPool(1);
    }

    public final int onStartCommand(Intent intent, int flags, int startId) {
        boolean z;
        boolean z2;
        if (intent == null) {
            stopSelf(startId);
            return 2;
        }
        TracingPowerManager.TracingWakeLock wakeLock = acquireWakeLock(this, "CoreService onStart", 60000);
        if (K9.DEBUG) {
            Log.i("k9", "CoreService: " + this.className + ".onStart(" + intent + ", " + startId + ")");
        }
        int wakeLockId = intent.getIntExtra(CoreReceiver.WAKE_LOCK_ID, -1);
        if (wakeLockId != -1) {
            BootReceiver.releaseWakeLock(this, wakeLockId);
        }
        int coreWakeLockId = intent.getIntExtra(WAKE_LOCK_ID, -1);
        if (coreWakeLockId != -1) {
            if (K9.DEBUG) {
                Log.d("k9", "Got core wake lock id " + coreWakeLockId);
            }
            TracingPowerManager.TracingWakeLock coreWakeLock = sWakeLocks.remove(Integer.valueOf(coreWakeLockId));
            if (coreWakeLock != null) {
                if (K9.DEBUG) {
                    Log.d("k9", "Found core wake lock with id " + coreWakeLockId + ", releasing");
                }
                coreWakeLock.release();
            }
        }
        this.mImmediateShutdown = true;
        try {
            int startService = startService(intent, startId);
            try {
                if (!z || !z2 || startId == -1) {
                    return startService;
                }
                return 2;
            } catch (Exception e) {
                return startService;
            }
        } finally {
            try {
                wakeLock.release();
            } catch (Exception e2) {
            }
            try {
                if (this.mAutoShutdown && this.mImmediateShutdown && startId != -1) {
                    stopSelf(startId);
                }
            } catch (Exception e3) {
            }
        }
    }

    public void execute(Context context, Runnable runner, int wakeLockTime, Integer startId) {
        boolean z = true;
        boolean serviceShutdownScheduled = false;
        final boolean autoShutdown = this.mAutoShutdown;
        final TracingPowerManager.TracingWakeLock wakeLock = acquireWakeLock(context, "CoreService execute", (long) wakeLockTime);
        final Runnable runnable = runner;
        final Integer num = startId;
        Runnable myRunner = new Runnable() {
            public void run() {
                try {
                    boolean oldIsSyncDisabled = MailService.isSyncDisabled();
                    if (K9.DEBUG) {
                        Log.d("k9", "CoreService (" + CoreService.this.className + ") running Runnable " + runnable.hashCode() + " with startId " + num);
                    }
                    runnable.run();
                    if (MailService.isSyncDisabled() != oldIsSyncDisabled) {
                        MessagingController.getInstance(CoreService.this.getApplication()).systemStatusChanged();
                    }
                    try {
                        if (K9.DEBUG) {
                            Log.d("k9", "CoreService (" + CoreService.this.className + ") completed " + "Runnable " + runnable.hashCode() + " with startId " + num);
                        }
                        wakeLock.release();
                    } finally {
                        if (autoShutdown && num != null) {
                            CoreService.this.stopSelf(num.intValue());
                        }
                    }
                } catch (Throwable th) {
                    if (autoShutdown && num != null) {
                        CoreService.this.stopSelf(num.intValue());
                    }
                    throw th;
                }
            }
        };
        if (this.mThreadPool == null) {
            Log.e("k9", "CoreService.execute (" + this.className + ") called with no thread " + "pool available; running Runnable " + runner.hashCode() + " in calling thread");
            synchronized (this) {
                myRunner.run();
                serviceShutdownScheduled = startId != null;
            }
        } else {
            if (K9.DEBUG) {
                Log.d("k9", "CoreService (" + this.className + ") queueing Runnable " + runner.hashCode() + " with startId " + startId);
            }
            try {
                this.mThreadPool.execute(myRunner);
                serviceShutdownScheduled = startId != null;
            } catch (RejectedExecutionException e) {
                if (!this.mShutdown) {
                    throw e;
                }
                Log.i("k9", "CoreService: " + this.className + " is shutting down, ignoring " + "rejected execution exception: " + e.getMessage());
            }
        }
        if (serviceShutdownScheduled) {
            z = false;
        }
        this.mImmediateShutdown = z;
    }

    public void onLowMemory() {
        Log.w("k9", "CoreService: " + this.className + ".onLowMemory() - Running low on memory");
    }

    public void onDestroy() {
        if (K9.DEBUG) {
            Log.i("k9", "CoreService: " + this.className + ".onDestroy()");
        }
        this.mShutdown = true;
        this.mThreadPool.shutdown();
    }

    /* access modifiers changed from: protected */
    public boolean isAutoShutdown() {
        return this.mAutoShutdown;
    }

    /* access modifiers changed from: protected */
    public void setAutoShutdown(boolean autoShutdown) {
        this.mAutoShutdown = autoShutdown;
    }

    public IBinder onBind(Intent intent) {
        return null;
    }
}
