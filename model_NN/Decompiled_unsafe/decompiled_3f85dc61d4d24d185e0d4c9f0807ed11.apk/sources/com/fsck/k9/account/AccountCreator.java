package com.fsck.k9.account;

import com.fsck.k9.Account;
import com.fsck.k9.mail.ConnectionSecurity;
import com.fsck.k9.mail.ServerSettings;

public class AccountCreator {
    public static Account.DeletePolicy getDefaultDeletePolicy(ServerSettings.Type type) {
        switch (type) {
            case IMAP:
                return Account.DeletePolicy.ON_DELETE;
            case POP3:
                return Account.DeletePolicy.NEVER;
            case WebDAV:
                return Account.DeletePolicy.ON_DELETE;
            case SMTP:
                throw new IllegalStateException("Delete policy doesn't apply to SMTP");
            default:
                throw new AssertionError("Unhandled case: " + type);
        }
    }

    public static int getDefaultPort(ConnectionSecurity securityType, ServerSettings.Type storeType) {
        switch (securityType) {
            case NONE:
            case STARTTLS_REQUIRED:
                return storeType.defaultPort;
            case SSL_TLS_REQUIRED:
                return storeType.defaultTlsPort;
            default:
                throw new AssertionError("Unhandled ConnectionSecurity type encountered: " + securityType);
        }
    }
}
