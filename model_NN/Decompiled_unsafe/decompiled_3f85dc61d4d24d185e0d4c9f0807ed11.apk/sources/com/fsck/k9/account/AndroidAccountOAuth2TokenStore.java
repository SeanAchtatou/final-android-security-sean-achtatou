package com.fsck.k9.account;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import atomicgonza.App;
import com.fsck.k9.K9;
import com.fsck.k9.R;
import com.fsck.k9.mail.AuthenticationFailedException;
import com.fsck.k9.mail.oauth.AuthorizationException;
import com.fsck.k9.mail.oauth.OAuth2TokenProvider;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class AndroidAccountOAuth2TokenStore implements OAuth2TokenProvider {
    private static final String GMAIL_AUTH_TOKEN_TYPE = "oauth2:https://mail.google.com/";
    private static final String GOOGLE_ACCOUNT_TYPE = "com.google";
    private AccountManager accountManager;
    private Context applicationContext;
    private Map<String, String> authTokens = new HashMap();

    public AndroidAccountOAuth2TokenStore(Context applicationContext2) {
        this.accountManager = AccountManager.get(applicationContext2);
        this.applicationContext = applicationContext2;
    }

    public void authorizeAPI(final String emailAddress, final Activity activity, final OAuth2TokenProvider.OAuth2TokenProviderAuthCallback callback) {
        Account account = getAccountFromManager(emailAddress);
        if (account == null) {
            callback.failure(new AuthorizationException(activity.getString(R.string.xoauth2_account_doesnt_exist)));
        } else if (account.name.equals(emailAddress)) {
            this.accountManager.getAuthToken(account, GMAIL_AUTH_TOKEN_TYPE, (Bundle) null, activity, new AccountManagerCallback<Bundle>() {
                public void run(AccountManagerFuture<Bundle> future) {
                    try {
                        Object keyAccountName = future.getResult().get("authAccount");
                        if (keyAccountName == null) {
                            callback.failure(new AuthorizationException(activity.getString(R.string.xoauth2_no_account)));
                        } else if (keyAccountName.equals(emailAddress)) {
                            callback.success();
                        } else {
                            callback.failure(new AuthorizationException(activity.getString(R.string.xoauth2_incorrect_auth_info_provided)));
                        }
                    } catch (OperationCanceledException e) {
                        callback.failure(new AuthorizationException(activity.getString(R.string.xoauth2_auth_cancelled_by_user), e));
                    } catch (IOException e2) {
                        callback.failure(new AuthorizationException(activity.getString(R.string.xoauth2_unable_to_contact_auth_server), e2));
                    } catch (AuthenticatorException e3) {
                        callback.failure(new AuthorizationException(activity.getString(R.string.xoauth2_error_contacting_auth_server), e3));
                    }
                }
            }, (Handler) null);
        }
    }

    public String getToken(String username, long timeoutMillis) throws AuthenticationFailedException {
        if (this.authTokens.get(username) == null) {
            Account account = getAccountFromManager(username);
            if (account == null) {
                String token = App.getTokenGmailAccount(this.applicationContext, username);
                if (TextUtils.isEmpty(token)) {
                    if (K9.DEBUG) {
                        Log.e("AtomicGonza", "okAG token not token for account " + username + new Exception().getStackTrace()[0].toString());
                    }
                    throw new AuthenticationFailedException("Account not available");
                } else if (!K9.DEBUG) {
                    return token;
                } else {
                    Log.e("AtomicGonza", "okAG token not null " + token + new Exception().getStackTrace()[0].toString());
                    return token;
                }
            } else {
                fetchNewAuthToken(username, account, timeoutMillis);
            }
        }
        return this.authTokens.get(username);
    }

    private Account getAccountFromManager(String username) {
        for (Account account : this.accountManager.getAccountsByType("com.google")) {
            if (account.name.equals(username)) {
                return account;
            }
        }
        return null;
    }

    private void fetchNewAuthToken(String username, Account account, long timeoutMillis) throws AuthenticationFailedException {
        try {
            Bundle bundle = this.accountManager.getAuthToken(account, GMAIL_AUTH_TOKEN_TYPE, false, null, null).getResult(timeoutMillis, TimeUnit.MILLISECONDS);
            if (bundle == null) {
                throw new AuthenticationFailedException("No token provided");
            } else if (bundle.get("authAccount") == null) {
                throw new AuthenticationFailedException("No account information provided");
            } else if (bundle.get("authAccount").equals(username)) {
                this.authTokens.put(username, bundle.get("authtoken").toString());
            } else {
                throw new AuthenticationFailedException("Unexpected account information provided");
            }
        } catch (Exception e) {
            throw new AuthenticationFailedException(e.getMessage());
        }
    }

    public void invalidateToken(String username) {
        this.accountManager.invalidateAuthToken("com.google", this.authTokens.get(username));
        this.authTokens.remove(username);
    }

    public List<String> getAccounts() {
        Account[] accounts = this.accountManager.getAccountsByType("com.google");
        ArrayList<String> accountNames = new ArrayList<>();
        for (Account account : accounts) {
            accountNames.add(account.name);
        }
        return accountNames;
    }
}
