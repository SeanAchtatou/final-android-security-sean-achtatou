package com.fsck.k9.preferences;

import com.fsck.k9.Account;
import com.fsck.k9.K9;
import com.fsck.k9.R;
import com.fsck.k9.mailstore.StorageManager;
import com.fsck.k9.preferences.Settings;
import com.fsck.k9.remotecontrol.K9RemoteControl;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class AccountSettings {
    public static final Map<String, TreeMap<Integer, Settings.SettingsDescription>> SETTINGS;
    public static final Map<Integer, Settings.SettingsUpgrader> UPGRADERS = Collections.unmodifiableMap(new HashMap<>());

    static {
        Map<String, TreeMap<Integer, Settings.SettingsDescription>> s = new LinkedHashMap<>();
        s.put("alwaysBcc", Settings.versions(new Settings.V(11, new Settings.StringSetting(""))));
        s.put("alwaysShowCcBcc", Settings.versions(new Settings.V(13, new Settings.BooleanSetting(false))));
        s.put("archiveFolderName", Settings.versions(new Settings.V(1, new Settings.StringSetting("Archive"))));
        s.put("autoExpandFolderName", Settings.versions(new Settings.V(1, new Settings.StringSetting(Account.INBOX))));
        s.put("automaticCheckIntervalMinutes", Settings.versions(new Settings.V(1, new IntegerResourceSetting(120, R.array.account_settings_check_frequency_values))));
        s.put("chipColor", Settings.versions(new Settings.V(1, new Settings.ColorSetting(-16776961))));
        s.put("cryptoApp", Settings.versions(new Settings.V(1, new Settings.StringSetting("apg")), new Settings.V(36, new Settings.StringSetting(""))));
        s.put("defaultQuotedTextShown", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(true))));
        s.put("deletePolicy", Settings.versions(new Settings.V(1, new DeletePolicySetting(Account.DeletePolicy.NEVER))));
        s.put("displayCount", Settings.versions(new Settings.V(1, new IntegerResourceSetting(25, R.array.account_settings_display_count_values))));
        s.put("draftsFolderName", Settings.versions(new Settings.V(1, new Settings.StringSetting("Drafts"))));
        s.put("expungePolicy", Settings.versions(new Settings.V(1, new StringResourceSetting(Account.Expunge.EXPUNGE_IMMEDIATELY.name(), R.array.account_setup_expunge_policy_values))));
        s.put("folderDisplayMode", Settings.versions(new Settings.V(1, new Settings.EnumSetting(Account.FolderMode.class, Account.FolderMode.NOT_SECOND_CLASS))));
        s.put("folderPushMode", Settings.versions(new Settings.V(1, new Settings.EnumSetting(Account.FolderMode.class, Account.FolderMode.FIRST_CLASS))));
        s.put("folderSyncMode", Settings.versions(new Settings.V(1, new Settings.EnumSetting(Account.FolderMode.class, Account.FolderMode.FIRST_CLASS))));
        s.put("folderTargetMode", Settings.versions(new Settings.V(1, new Settings.EnumSetting(Account.FolderMode.class, Account.FolderMode.NOT_SECOND_CLASS))));
        s.put("goToUnreadMessageSearch", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(false))));
        s.put("idleRefreshMinutes", Settings.versions(new Settings.V(1, new IntegerResourceSetting(24, R.array.idle_refresh_period_values))));
        s.put("inboxFolderName", Settings.versions(new Settings.V(1, new Settings.StringSetting(Account.INBOX))));
        s.put("led", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(true))));
        s.put("ledColor", Settings.versions(new Settings.V(1, new Settings.ColorSetting(-16776961))));
        s.put("localStorageProvider", Settings.versions(new Settings.V(1, new StorageProviderSetting())));
        s.put("markMessageAsReadOnView", Settings.versions(new Settings.V(7, new Settings.BooleanSetting(true))));
        s.put("maxPushFolders", Settings.versions(new Settings.V(1, new Settings.IntegerRangeSetting(0, 100, 10))));
        s.put("maximumAutoDownloadMessageSize", Settings.versions(new Settings.V(1, new IntegerResourceSetting(32768, R.array.account_settings_autodownload_message_size_values))));
        s.put("maximumPolledMessageAge", Settings.versions(new Settings.V(1, new IntegerResourceSetting(-1, R.array.account_settings_message_age_values))));
        s.put("messageFormat", Settings.versions(new Settings.V(1, new Settings.EnumSetting(Account.MessageFormat.class, Account.DEFAULT_MESSAGE_FORMAT))));
        s.put("messageFormatAuto", Settings.versions(new Settings.V(2, new Settings.BooleanSetting(false))));
        s.put("messageReadReceipt", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(false))));
        s.put("notifyMailCheck", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(false))));
        s.put("notifyNewMail", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(false))));
        s.put("folderNotifyNewMailMode", Settings.versions(new Settings.V(34, new Settings.EnumSetting(Account.FolderMode.class, Account.FolderMode.ALL))));
        s.put("notifySelfNewMail", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(true))));
        s.put("pushPollOnConnect", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(true))));
        s.put("quotePrefix", Settings.versions(new Settings.V(1, new Settings.StringSetting(Account.DEFAULT_QUOTE_PREFIX))));
        s.put("quoteStyle", Settings.versions(new Settings.V(1, new Settings.EnumSetting(Account.QuoteStyle.class, Account.DEFAULT_QUOTE_STYLE))));
        s.put("replyAfterQuote", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(false))));
        s.put("ring", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(true))));
        s.put("ringtone", Settings.versions(new Settings.V(1, new RingtoneSetting("content://settings/system/notification_sound"))));
        s.put("searchableFolders", Settings.versions(new Settings.V(1, new Settings.EnumSetting(Account.Searchable.class, Account.Searchable.ALL))));
        s.put("sentFolderName", Settings.versions(new Settings.V(1, new Settings.StringSetting("Sent"))));
        s.put("sortTypeEnum", Settings.versions(new Settings.V(9, new Settings.EnumSetting(Account.SortType.class, Account.DEFAULT_SORT_TYPE))));
        s.put("sortAscending", Settings.versions(new Settings.V(9, new Settings.BooleanSetting(false))));
        s.put("showPicturesEnum", Settings.versions(new Settings.V(1, new Settings.EnumSetting(Account.ShowPictures.class, Account.ShowPictures.ALWAYS))));
        s.put("signatureBeforeQuotedText", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(false))));
        s.put("spamFolderName", Settings.versions(new Settings.V(1, new Settings.StringSetting("Spam"))));
        s.put("stripSignature", Settings.versions(new Settings.V(2, new Settings.BooleanSetting(true))));
        s.put("subscribedFoldersOnly", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(false))));
        s.put("syncRemoteDeletions", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(true))));
        s.put("trashFolderName", Settings.versions(new Settings.V(1, new Settings.StringSetting("Trash"))));
        s.put("useCompression.MOBILE", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(true))));
        s.put("useCompression.OTHER", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(true))));
        s.put("useCompression.WIFI", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(true))));
        s.put("vibrate", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(true))));
        s.put("vibratePattern", Settings.versions(new Settings.V(1, new IntegerResourceSetting(5, R.array.account_settings_vibrate_pattern_values))));
        s.put("vibrateTimes", Settings.versions(new Settings.V(1, new IntegerResourceSetting(1, R.array.account_settings_vibrate_times_label))));
        s.put("allowRemoteSearch", Settings.versions(new Settings.V(18, new Settings.BooleanSetting(true))));
        s.put("remoteSearchNumResults", Settings.versions(new Settings.V(18, new IntegerResourceSetting(25, R.array.account_settings_remote_search_num_results_values))));
        s.put("remoteSearchFullText", Settings.versions(new Settings.V(18, new Settings.BooleanSetting(true))));
        s.put("notifyContactsMailOnly", Settings.versions(new Settings.V(42, new Settings.BooleanSetting(false))));
        SETTINGS = Collections.unmodifiableMap(s);
    }

    public static Map<String, Object> validate(int version, Map<String, String> importedSettings, boolean useDefaultValues) {
        return Settings.validate(version, SETTINGS, importedSettings, useDefaultValues);
    }

    public static Set<String> upgrade(int version, Map<String, Object> validatedSettings) {
        return Settings.upgrade(version, UPGRADERS, SETTINGS, validatedSettings);
    }

    public static Map<String, String> convert(Map<String, Object> settings) {
        return Settings.convert(settings, SETTINGS);
    }

    public static Map<String, String> getAccountSettings(Storage storage, String uuid) {
        Map<String, String> result = new HashMap<>();
        String prefix = uuid + ".";
        for (String key : SETTINGS.keySet()) {
            String value = storage.getString(prefix + key, null);
            if (value != null) {
                result.put(key, value);
            }
        }
        return result;
    }

    public static class IntegerResourceSetting extends Settings.PseudoEnumSetting<Integer> {
        private final Map<Integer, String> mMapping;

        public IntegerResourceSetting(int defaultValue, int resId) {
            super(Integer.valueOf(defaultValue));
            Map<Integer, String> mapping = new HashMap<>();
            for (String value : K9.app.getResources().getStringArray(resId)) {
                mapping.put(Integer.valueOf(Integer.parseInt(value)), value);
            }
            this.mMapping = Collections.unmodifiableMap(mapping);
        }

        /* access modifiers changed from: protected */
        public Map<Integer, String> getMapping() {
            return this.mMapping;
        }

        public Object fromString(String value) throws Settings.InvalidSettingValueException {
            try {
                return Integer.valueOf(Integer.parseInt(value));
            } catch (NumberFormatException e) {
                throw new Settings.InvalidSettingValueException();
            }
        }
    }

    public static class StringResourceSetting extends Settings.PseudoEnumSetting<String> {
        private final Map<String, String> mMapping;

        public StringResourceSetting(String defaultValue, int resId) {
            super(defaultValue);
            Map<String, String> mapping = new HashMap<>();
            for (String value : K9.app.getResources().getStringArray(resId)) {
                mapping.put(value, value);
            }
            this.mMapping = Collections.unmodifiableMap(mapping);
        }

        /* access modifiers changed from: protected */
        public Map<String, String> getMapping() {
            return this.mMapping;
        }

        public Object fromString(String value) throws Settings.InvalidSettingValueException {
            if (this.mMapping.containsKey(value)) {
                return value;
            }
            throw new Settings.InvalidSettingValueException();
        }
    }

    public static class RingtoneSetting extends Settings.SettingsDescription {
        public RingtoneSetting(String defaultValue) {
            super(defaultValue);
        }

        public Object fromString(String value) {
            return value;
        }
    }

    public static class StorageProviderSetting extends Settings.SettingsDescription {
        public StorageProviderSetting() {
            super(null);
        }

        public Object getDefaultValue() {
            return StorageManager.getInstance(K9.app).getDefaultProviderId();
        }

        public Object fromString(String value) {
            if (StorageManager.getInstance(K9.app).getAvailableProviders().containsKey(value)) {
                return value;
            }
            throw new RuntimeException("Validation failed");
        }
    }

    public static class DeletePolicySetting extends Settings.PseudoEnumSetting<Integer> {
        private Map<Integer, String> mMapping;

        public DeletePolicySetting(Account.DeletePolicy defaultValue) {
            super(defaultValue);
            Map<Integer, String> mapping = new HashMap<>();
            mapping.put(Integer.valueOf(Account.DeletePolicy.NEVER.setting), K9RemoteControl.K9_BACKGROUND_OPERATIONS_NEVER);
            mapping.put(Integer.valueOf(Account.DeletePolicy.ON_DELETE.setting), "DELETE");
            mapping.put(Integer.valueOf(Account.DeletePolicy.MARK_AS_READ.setting), "MARK_AS_READ");
            this.mMapping = Collections.unmodifiableMap(mapping);
        }

        /* access modifiers changed from: protected */
        public Map<Integer, String> getMapping() {
            return this.mMapping;
        }

        public Object fromString(String value) throws Settings.InvalidSettingValueException {
            try {
                Integer deletePolicy = Integer.valueOf(Integer.parseInt(value));
                if (this.mMapping.containsKey(deletePolicy)) {
                    return deletePolicy;
                }
            } catch (NumberFormatException e) {
            }
            throw new Settings.InvalidSettingValueException();
        }
    }
}
