package com.fsck.k9.preferences;

import com.fsck.k9.mail.Folder;
import com.fsck.k9.preferences.Settings;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class FolderSettings {
    public static final Map<String, TreeMap<Integer, Settings.SettingsDescription>> SETTINGS;
    public static final Map<Integer, Settings.SettingsUpgrader> UPGRADERS = Collections.unmodifiableMap(new HashMap<>());

    static {
        Map<String, TreeMap<Integer, Settings.SettingsDescription>> s = new LinkedHashMap<>();
        s.put("displayMode", Settings.versions(new Settings.V(1, new Settings.EnumSetting(Folder.FolderClass.class, Folder.FolderClass.NO_CLASS))));
        s.put("notifyMode", Settings.versions(new Settings.V(34, new Settings.EnumSetting(Folder.FolderClass.class, Folder.FolderClass.INHERITED))));
        s.put("syncMode", Settings.versions(new Settings.V(1, new Settings.EnumSetting(Folder.FolderClass.class, Folder.FolderClass.INHERITED))));
        s.put("pushMode", Settings.versions(new Settings.V(1, new Settings.EnumSetting(Folder.FolderClass.class, Folder.FolderClass.INHERITED))));
        s.put("inTopGroup", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(false))));
        s.put("integrate", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(false))));
        SETTINGS = Collections.unmodifiableMap(s);
    }

    public static Map<String, Object> validate(int version, Map<String, String> importedSettings, boolean useDefaultValues) {
        return Settings.validate(version, SETTINGS, importedSettings, useDefaultValues);
    }

    public static Set<String> upgrade(int version, Map<String, Object> validatedSettings) {
        return Settings.upgrade(version, UPGRADERS, SETTINGS, validatedSettings);
    }

    public static Map<String, String> convert(Map<String, Object> settings) {
        return Settings.convert(settings, SETTINGS);
    }

    public static Map<String, String> getFolderSettings(Storage storage, String uuid, String folderName) {
        Map<String, String> result = new HashMap<>();
        String prefix = uuid + "." + folderName + ".";
        for (String key : SETTINGS.keySet()) {
            String value = storage.getString(prefix + key, null);
            if (value != null) {
                result.put(key, value);
            }
        }
        return result;
    }
}
