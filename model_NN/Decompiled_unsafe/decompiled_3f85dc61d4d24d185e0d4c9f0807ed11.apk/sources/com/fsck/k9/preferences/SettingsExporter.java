package com.fsck.k9.preferences;

import android.content.Context;
import android.util.Log;
import android.util.Xml;
import com.fsck.k9.Account;
import com.fsck.k9.K9;
import com.fsck.k9.Preferences;
import com.fsck.k9.mail.ServerSettings;
import com.fsck.k9.mail.Transport;
import com.fsck.k9.mail.store.RemoteStore;
import com.fsck.k9.preferences.Settings;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import org.xmlpull.v1.XmlSerializer;

public class SettingsExporter {
    public static final String ACCOUNTS_ELEMENT = "accounts";
    public static final String ACCOUNT_ELEMENT = "account";
    public static final String AUTHENTICATION_TYPE_ELEMENT = "authentication-type";
    public static final String CLIENT_CERTIFICATE_ALIAS_ELEMENT = "client-cert-alias";
    public static final String CONNECTION_SECURITY_ELEMENT = "connection-security";
    public static final String DESCRIPTION_ELEMENT = "description";
    public static final String EMAIL_ELEMENT = "email";
    private static final String EXPORT_FILENAME = "settings.k9s";
    public static final String EXTRA_ELEMENT = "extra";
    public static final String FILE_FORMAT_ATTRIBUTE = "format";
    public static final int FILE_FORMAT_VERSION = 1;
    public static final String FOLDERS_ELEMENT = "folders";
    public static final String FOLDER_ELEMENT = "folder";
    public static final String GLOBAL_ELEMENT = "global";
    public static final String HOST_ELEMENT = "host";
    public static final String IDENTITIES_ELEMENT = "identities";
    public static final String IDENTITY_ELEMENT = "identity";
    public static final String INCOMING_SERVER_ELEMENT = "incoming-server";
    public static final String KEY_ATTRIBUTE = "key";
    public static final String NAME_ATTRIBUTE = "name";
    public static final String NAME_ELEMENT = "name";
    public static final String OUTGOING_SERVER_ELEMENT = "outgoing-server";
    public static final String PASSWORD_ELEMENT = "password";
    public static final String PORT_ELEMENT = "port";
    public static final String ROOT_ELEMENT = "k9settings";
    public static final String SETTINGS_ELEMENT = "settings";
    public static final String TYPE_ATTRIBUTE = "type";
    public static final String USERNAME_ELEMENT = "username";
    public static final String UUID_ATTRIBUTE = "uuid";
    public static final String VALUE_ELEMENT = "value";
    public static final String VERSION_ATTRIBUTE = "version";

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0084 A[SYNTHETIC, Splitter:B:20:0x0084] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String exportToFile(android.content.Context r11, boolean r12, java.util.Set<java.lang.String> r13) throws com.fsck.k9.preferences.SettingsImportExportException {
        /*
            r5 = 0
            r3 = 0
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x007a }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x007a }
            r7.<init>()     // Catch:{ Exception -> 0x007a }
            java.io.File r8 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ Exception -> 0x007a }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x007a }
            java.lang.String r8 = java.io.File.separator     // Catch:{ Exception -> 0x007a }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x007a }
            java.lang.String r8 = r11.getPackageName()     // Catch:{ Exception -> 0x007a }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x007a }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x007a }
            r0.<init>(r7)     // Catch:{ Exception -> 0x007a }
            boolean r7 = r0.mkdirs()     // Catch:{ Exception -> 0x007a }
            if (r7 != 0) goto L_0x0048
            java.lang.String r7 = "k9"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x007a }
            r8.<init>()     // Catch:{ Exception -> 0x007a }
            java.lang.String r9 = "Unable to create directory: "
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Exception -> 0x007a }
            java.lang.String r9 = r0.getAbsolutePath()     // Catch:{ Exception -> 0x007a }
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Exception -> 0x007a }
            java.lang.String r8 = r8.toString()     // Catch:{ Exception -> 0x007a }
            android.util.Log.d(r7, r8)     // Catch:{ Exception -> 0x007a }
        L_0x0048:
            java.lang.String r7 = "settings.k9s"
            java.io.File r2 = com.fsck.k9.helper.FileHelper.createUniqueFile(r0, r7)     // Catch:{ Exception -> 0x007a }
            java.lang.String r3 = r2.getAbsolutePath()     // Catch:{ Exception -> 0x007a }
            java.io.FileOutputStream r6 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x007a }
            r6.<init>(r3)     // Catch:{ Exception -> 0x007a }
            exportPreferences(r11, r6, r12, r13)     // Catch:{ Exception -> 0x00a5, all -> 0x00a2 }
            if (r6 == 0) goto L_0x005f
            r6.close()     // Catch:{ IOException -> 0x0060 }
        L_0x005f:
            return r3
        L_0x0060:
            r4 = move-exception
            java.lang.String r7 = "k9"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "Couldn't close exported settings file: "
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.StringBuilder r8 = r8.append(r3)
            java.lang.String r8 = r8.toString()
            android.util.Log.w(r7, r8)
            goto L_0x005f
        L_0x007a:
            r1 = move-exception
        L_0x007b:
            com.fsck.k9.preferences.SettingsImportExportException r7 = new com.fsck.k9.preferences.SettingsImportExportException     // Catch:{ all -> 0x0081 }
            r7.<init>(r1)     // Catch:{ all -> 0x0081 }
            throw r7     // Catch:{ all -> 0x0081 }
        L_0x0081:
            r7 = move-exception
        L_0x0082:
            if (r5 == 0) goto L_0x0087
            r5.close()     // Catch:{ IOException -> 0x0088 }
        L_0x0087:
            throw r7
        L_0x0088:
            r4 = move-exception
            java.lang.String r8 = "k9"
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r10 = "Couldn't close exported settings file: "
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.StringBuilder r9 = r9.append(r3)
            java.lang.String r9 = r9.toString()
            android.util.Log.w(r8, r9)
            goto L_0x0087
        L_0x00a2:
            r7 = move-exception
            r5 = r6
            goto L_0x0082
        L_0x00a5:
            r1 = move-exception
            r5 = r6
            goto L_0x007b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fsck.k9.preferences.SettingsExporter.exportToFile(android.content.Context, boolean, java.util.Set):java.lang.String");
    }

    public static void exportPreferences(Context context, OutputStream os, boolean includeGlobals, Set<String> accountUuids) throws SettingsImportExportException {
        Set<String> exportAccounts;
        try {
            XmlSerializer serializer = Xml.newSerializer();
            serializer.setOutput(os, "UTF-8");
            serializer.startDocument(null, Boolean.TRUE);
            serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
            serializer.startTag(null, ROOT_ELEMENT);
            serializer.attribute(null, VERSION_ATTRIBUTE, Integer.toString(42));
            serializer.attribute(null, FILE_FORMAT_ATTRIBUTE, Integer.toString(1));
            Log.i("k9", "Exporting preferences");
            Preferences preferences = Preferences.getPreferences(context);
            Storage storage = preferences.getStorage();
            if (accountUuids == null) {
                List<Account> accounts = preferences.getAccounts();
                exportAccounts = new HashSet<>();
                for (Account account : accounts) {
                    exportAccounts.add(account.getUuid());
                }
            } else {
                exportAccounts = accountUuids;
            }
            Map<String, Object> prefs = new TreeMap<>(storage.getAll());
            if (includeGlobals) {
                serializer.startTag(null, GLOBAL_ELEMENT);
                writeSettings(serializer, prefs);
                serializer.endTag(null, GLOBAL_ELEMENT);
            }
            serializer.startTag(null, ACCOUNTS_ELEMENT);
            for (String accountUuid : exportAccounts) {
                writeAccount(serializer, preferences.getAccount(accountUuid), prefs);
            }
            serializer.endTag(null, ACCOUNTS_ELEMENT);
            serializer.endTag(null, ROOT_ELEMENT);
            serializer.endDocument();
            serializer.flush();
        } catch (Exception e) {
            throw new SettingsImportExportException(e.getLocalizedMessage(), e);
        }
    }

    private static void writeSettings(XmlSerializer serializer, Map<String, Object> prefs) throws IOException {
        for (Map.Entry<String, TreeMap<Integer, Settings.SettingsDescription>> versionedSetting : GlobalSettings.SETTINGS.entrySet()) {
            String key = (String) versionedSetting.getKey();
            String valueString = (String) prefs.get(key);
            TreeMap<Integer, Settings.SettingsDescription> versions = versionedSetting.getValue();
            Settings.SettingsDescription setting = (Settings.SettingsDescription) versions.get((Integer) versions.lastKey());
            if (setting != null) {
                if (valueString != null) {
                    try {
                        writeKeyValue(serializer, key, setting.toPrettyString(setting.fromString(valueString)));
                    } catch (Settings.InvalidSettingValueException e) {
                        Log.w("k9", "Global setting \"" + key + "\" has invalid value \"" + valueString + "\" in preference storage. This shouldn't happen!");
                    }
                } else {
                    if (K9.DEBUG) {
                        Log.d("k9", "Couldn't find key \"" + key + "\" in preference storage." + "Using default value.");
                    }
                    writeKeyValue(serializer, key, setting.toPrettyString(setting.getDefaultValue()));
                }
            }
        }
    }

    private static void writeAccount(XmlSerializer serializer, Account account, Map<String, Object> prefs) throws IOException {
        Set<Integer> identities = new HashSet<>();
        Set<String> folders = new HashSet<>();
        String accountUuid = account.getUuid();
        serializer.startTag(null, "account");
        serializer.attribute(null, UUID_ATTRIBUTE, accountUuid);
        String name = (String) prefs.get(accountUuid + "." + "description");
        if (name != null) {
            serializer.startTag(null, "name");
            serializer.text(name);
            serializer.endTag(null, "name");
        }
        ServerSettings incoming = RemoteStore.decodeStoreUri(account.getStoreUri());
        serializer.startTag(null, INCOMING_SERVER_ELEMENT);
        serializer.attribute(null, TYPE_ATTRIBUTE, incoming.type.name());
        writeElement(serializer, HOST_ELEMENT, incoming.host);
        if (incoming.port != -1) {
            writeElement(serializer, PORT_ELEMENT, Integer.toString(incoming.port));
        }
        if (incoming.connectionSecurity != null) {
            writeElement(serializer, CONNECTION_SECURITY_ELEMENT, incoming.connectionSecurity.name());
        }
        if (incoming.authenticationType != null) {
            writeElement(serializer, AUTHENTICATION_TYPE_ELEMENT, incoming.authenticationType.name());
        }
        writeElement(serializer, USERNAME_ELEMENT, incoming.username);
        writeElement(serializer, CLIENT_CERTIFICATE_ALIAS_ELEMENT, incoming.clientCertificateAlias);
        Map<String, String> extras = incoming.getExtra();
        if (extras != null && extras.size() > 0) {
            serializer.startTag(null, EXTRA_ELEMENT);
            for (Map.Entry<String, String> extra : extras.entrySet()) {
                writeKeyValue(serializer, (String) extra.getKey(), (String) extra.getValue());
            }
            serializer.endTag(null, EXTRA_ELEMENT);
        }
        serializer.endTag(null, INCOMING_SERVER_ELEMENT);
        ServerSettings outgoing = Transport.decodeTransportUri(account.getTransportUri());
        serializer.startTag(null, OUTGOING_SERVER_ELEMENT);
        serializer.attribute(null, TYPE_ATTRIBUTE, outgoing.type.name());
        writeElement(serializer, HOST_ELEMENT, outgoing.host);
        if (outgoing.port != -1) {
            writeElement(serializer, PORT_ELEMENT, Integer.toString(outgoing.port));
        }
        if (outgoing.connectionSecurity != null) {
            writeElement(serializer, CONNECTION_SECURITY_ELEMENT, outgoing.connectionSecurity.name());
        }
        if (outgoing.authenticationType != null) {
            writeElement(serializer, AUTHENTICATION_TYPE_ELEMENT, outgoing.authenticationType.name());
        }
        writeElement(serializer, USERNAME_ELEMENT, outgoing.username);
        writeElement(serializer, CLIENT_CERTIFICATE_ALIAS_ELEMENT, outgoing.clientCertificateAlias);
        Map<String, String> extras2 = outgoing.getExtra();
        if (extras2 != null && extras2.size() > 0) {
            serializer.startTag(null, EXTRA_ELEMENT);
            for (Map.Entry<String, String> extra2 : extras2.entrySet()) {
                writeKeyValue(serializer, (String) extra2.getKey(), (String) extra2.getValue());
            }
            serializer.endTag(null, EXTRA_ELEMENT);
        }
        serializer.endTag(null, OUTGOING_SERVER_ELEMENT);
        serializer.startTag(null, SETTINGS_ELEMENT);
        for (Map.Entry<String, Object> entry : prefs.entrySet()) {
            String valueString = entry.getValue().toString();
            String[] comps = ((String) entry.getKey()).split("\\.", 2);
            if (comps.length >= 2) {
                String keyUuid = comps[0];
                String keyPart = comps[1];
                if (keyUuid.equals(accountUuid)) {
                    int indexOfLastDot = keyPart.lastIndexOf(".");
                    if (indexOfLastDot != -1 && indexOfLastDot < keyPart.length() + -1) {
                        String secondPart = keyPart.substring(0, indexOfLastDot);
                        String thirdPart = keyPart.substring(indexOfLastDot + 1);
                        if ("description".equals(secondPart)) {
                            try {
                                identities.add(Integer.valueOf(Integer.parseInt(thirdPart)));
                            } catch (NumberFormatException e) {
                            }
                        } else if (FolderSettings.SETTINGS.containsKey(thirdPart)) {
                            folders.add(secondPart);
                        }
                    }
                    TreeMap<Integer, Settings.SettingsDescription> versionedSetting = AccountSettings.SETTINGS.get(keyPart);
                    if (versionedSetting != null) {
                        Settings.SettingsDescription setting = (Settings.SettingsDescription) versionedSetting.get((Integer) versionedSetting.lastKey());
                        if (setting != null) {
                            try {
                                writeKeyValue(serializer, keyPart, setting.toPrettyString(setting.fromString(valueString)));
                            } catch (Settings.InvalidSettingValueException e2) {
                                Log.w("k9", "Account setting \"" + keyPart + "\" (" + account.getDescription() + ") has invalid value \"" + valueString + "\" in preference storage. This shouldn't happen!");
                            }
                        }
                    }
                }
            }
        }
        serializer.endTag(null, SETTINGS_ELEMENT);
        if (identities.size() > 0) {
            serializer.startTag(null, IDENTITIES_ELEMENT);
            ArrayList<Integer> arrayList = new ArrayList<>(identities);
            Collections.sort(arrayList);
            for (Integer identityIndex : arrayList) {
                writeIdentity(serializer, accountUuid, identityIndex.toString(), prefs);
            }
            serializer.endTag(null, IDENTITIES_ELEMENT);
        }
        if (folders.size() > 0) {
            serializer.startTag(null, FOLDERS_ELEMENT);
            for (String folder : folders) {
                writeFolder(serializer, accountUuid, folder, prefs);
            }
            serializer.endTag(null, FOLDERS_ELEMENT);
        }
        serializer.endTag(null, "account");
    }

    private static void writeIdentity(XmlSerializer serializer, String accountUuid, String identity, Map<String, Object> prefs) throws IOException {
        TreeMap<Integer, Settings.SettingsDescription> versionedSetting;
        serializer.startTag(null, IDENTITY_ELEMENT);
        String prefix = accountUuid + ".";
        String suffix = "." + identity;
        serializer.startTag(null, "name");
        serializer.text((String) prefs.get(prefix + "name" + suffix));
        serializer.endTag(null, "name");
        serializer.startTag(null, "email");
        serializer.text((String) prefs.get(prefix + "email" + suffix));
        serializer.endTag(null, "email");
        String description = (String) prefs.get(prefix + "description" + suffix);
        if (description != null) {
            serializer.startTag(null, "description");
            serializer.text(description);
            serializer.endTag(null, "description");
        }
        serializer.startTag(null, SETTINGS_ELEMENT);
        for (Map.Entry<String, Object> entry : prefs.entrySet()) {
            String valueString = entry.getValue().toString();
            String[] comps = ((String) entry.getKey()).split("\\.");
            if (comps.length >= 3) {
                String keyUuid = comps[0];
                String identityKey = comps[1];
                String identityIndex = comps[2];
                if (keyUuid.equals(accountUuid) && identityIndex.equals(identity) && (versionedSetting = IdentitySettings.SETTINGS.get(identityKey)) != null) {
                    Settings.SettingsDescription setting = (Settings.SettingsDescription) versionedSetting.get((Integer) versionedSetting.lastKey());
                    if (setting != null) {
                        try {
                            writeKeyValue(serializer, identityKey, setting.toPrettyString(setting.fromString(valueString)));
                        } catch (Settings.InvalidSettingValueException e) {
                            Log.w("k9", "Identity setting \"" + identityKey + "\" has invalid value \"" + valueString + "\" in preference storage. This shouldn't happen!");
                        }
                    }
                }
            }
        }
        serializer.endTag(null, SETTINGS_ELEMENT);
        serializer.endTag(null, IDENTITY_ELEMENT);
    }

    private static void writeFolder(XmlSerializer serializer, String accountUuid, String folder, Map<String, Object> prefs) throws IOException {
        TreeMap<Integer, Settings.SettingsDescription> versionedSetting;
        serializer.startTag(null, FOLDER_ELEMENT);
        serializer.attribute(null, "name", folder);
        for (Map.Entry<String, Object> entry : prefs.entrySet()) {
            String key = (String) entry.getKey();
            String valueString = entry.getValue().toString();
            int indexOfFirstDot = key.indexOf(46);
            int indexOfLastDot = key.lastIndexOf(46);
            if (!(indexOfFirstDot == -1 || indexOfLastDot == -1 || indexOfFirstDot == indexOfLastDot)) {
                String keyUuid = key.substring(0, indexOfFirstDot);
                String folderName = key.substring(indexOfFirstDot + 1, indexOfLastDot);
                String folderKey = key.substring(indexOfLastDot + 1);
                if (keyUuid.equals(accountUuid) && folderName.equals(folder) && (versionedSetting = FolderSettings.SETTINGS.get(folderKey)) != null) {
                    Settings.SettingsDescription setting = (Settings.SettingsDescription) versionedSetting.get((Integer) versionedSetting.lastKey());
                    if (setting != null) {
                        try {
                            writeKeyValue(serializer, folderKey, setting.toPrettyString(setting.fromString(valueString)));
                        } catch (Settings.InvalidSettingValueException e) {
                            Log.w("k9", "Folder setting \"" + folderKey + "\" has invalid value \"" + valueString + "\" in preference storage. This shouldn't happen!");
                        }
                    }
                }
            }
        }
        serializer.endTag(null, FOLDER_ELEMENT);
    }

    private static void writeElement(XmlSerializer serializer, String elementName, String value) throws IllegalArgumentException, IllegalStateException, IOException {
        if (value != null) {
            serializer.startTag(null, elementName);
            serializer.text(value);
            serializer.endTag(null, elementName);
        }
    }

    private static void writeKeyValue(XmlSerializer serializer, String key, String value) throws IllegalArgumentException, IllegalStateException, IOException {
        serializer.startTag(null, VALUE_ELEMENT);
        serializer.attribute(null, KEY_ATTRIBUTE, key);
        if (value != null) {
            serializer.text(value);
        }
        serializer.endTag(null, VALUE_ELEMENT);
    }
}
