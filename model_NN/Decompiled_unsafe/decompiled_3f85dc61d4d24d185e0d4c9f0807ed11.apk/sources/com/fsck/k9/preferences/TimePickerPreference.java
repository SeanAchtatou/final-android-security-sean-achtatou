package com.fsck.k9.preferences;

import android.content.Context;
import android.preference.DialogPreference;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TimePicker;
import java.util.Locale;

public class TimePickerPreference extends DialogPreference implements TimePicker.OnTimeChangedListener {
    public static final String VALIDATION_EXPRESSION = "[0-2]*[0-9]:[0-5]*[0-9]";
    private String defaultValue;
    private int originalHour = 0;
    private int originalMinute = 0;

    public TimePickerPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    public TimePickerPreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initialize();
    }

    private void initialize() {
        setPersistent(true);
    }

    /* access modifiers changed from: protected */
    public View onCreateDialogView() {
        TimePicker tp = new TimePicker(getContext());
        tp.setIs24HourView(Boolean.valueOf(DateFormat.is24HourFormat(getContext())));
        tp.setOnTimeChangedListener(this);
        this.originalHour = getHour();
        this.originalMinute = getMinute();
        if (this.originalHour >= 0 && this.originalMinute >= 0) {
            tp.setCurrentHour(Integer.valueOf(this.originalHour));
            tp.setCurrentMinute(Integer.valueOf(this.originalMinute));
        }
        return tp;
    }

    public void onTimeChanged(TimePicker view, int hour, int minute) {
        persistString(String.format(Locale.US, "%02d:%02d", Integer.valueOf(hour), Integer.valueOf(minute)));
        callChangeListener(String.format(Locale.US, "%02d:%02d", Integer.valueOf(hour), Integer.valueOf(minute)));
    }

    /* access modifiers changed from: protected */
    public void onDialogClosed(boolean positiveResult) {
        if (!positiveResult) {
            persistString(String.format(Locale.US, "%02d:%02d", Integer.valueOf(this.originalHour), Integer.valueOf(this.originalMinute)));
            callChangeListener(String.format(Locale.US, "%02d:%02d", Integer.valueOf(this.originalHour), Integer.valueOf(this.originalMinute)));
        }
        super.onDialogClosed(positiveResult);
    }

    public void setDefaultValue(Object defaultValue2) {
        super.setDefaultValue(defaultValue2);
        if ((defaultValue2 instanceof String) && ((String) defaultValue2).matches(VALIDATION_EXPRESSION)) {
            this.defaultValue = (String) defaultValue2;
        }
    }

    private int getHour() {
        String time = getTime();
        if (time == null || !time.matches(VALIDATION_EXPRESSION)) {
            return -1;
        }
        return Integer.parseInt(time.split(":")[0]);
    }

    private int getMinute() {
        String time = getTime();
        if (time == null || !time.matches(VALIDATION_EXPRESSION)) {
            return -1;
        }
        return Integer.parseInt(time.split(":")[1]);
    }

    public String getTime() {
        return getPersistedString(this.defaultValue);
    }
}
