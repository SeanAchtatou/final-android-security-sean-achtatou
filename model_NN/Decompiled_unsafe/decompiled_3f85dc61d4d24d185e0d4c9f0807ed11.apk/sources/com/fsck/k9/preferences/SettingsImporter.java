package com.fsck.k9.preferences;

import android.util.Log;
import com.fsck.k9.Account;
import com.fsck.k9.Identity;
import com.fsck.k9.K9;
import com.fsck.k9.Preferences;
import com.fsck.k9.mail.AuthType;
import com.fsck.k9.mail.ConnectionSecurity;
import com.fsck.k9.mail.ServerSettings;
import com.fsck.k9.preferences.Settings;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class SettingsImporter {

    public static class ImportContents {
        public final List<AccountDescription> accounts;
        public final boolean globalSettings;

        private ImportContents(boolean globalSettings2, List<AccountDescription> accounts2) {
            this.globalSettings = globalSettings2;
            this.accounts = accounts2;
        }
    }

    public static class AccountDescription {
        public final String name;
        public final String uuid;

        private AccountDescription(String name2, String uuid2) {
            this.name = name2;
            this.uuid = uuid2;
        }
    }

    public static class AccountDescriptionPair {
        public final AccountDescription imported;
        public final AccountDescription original;
        public final boolean overwritten;

        private AccountDescriptionPair(AccountDescription original2, AccountDescription imported2, boolean overwritten2) {
            this.original = original2;
            this.imported = imported2;
            this.overwritten = overwritten2;
        }
    }

    public static class ImportResults {
        public final List<AccountDescription> errorneousAccounts;
        public final boolean globalSettings;
        public final List<AccountDescriptionPair> importedAccounts;

        private ImportResults(boolean globalSettings2, List<AccountDescriptionPair> importedAccounts2, List<AccountDescription> errorneousAccounts2) {
            this.globalSettings = globalSettings2;
            this.importedAccounts = importedAccounts2;
            this.errorneousAccounts = errorneousAccounts2;
        }
    }

    public static ImportContents getImportStreamContents(InputStream inputStream) throws SettingsImportExportException {
        boolean globalSettings = true;
        try {
            Imported imported = parseSettings(inputStream, false, null, true);
            if (imported.globalSettings == null) {
                globalSettings = false;
            }
            List<AccountDescription> accounts = new ArrayList<>();
            if (imported.accounts != null) {
                for (ImportedAccount account : imported.accounts.values()) {
                    accounts.add(new AccountDescription(account.name, account.uuid));
                }
            }
            return new ImportContents(globalSettings, accounts);
        } catch (SettingsImportExportException e) {
            throw e;
        } catch (Exception e2) {
            throw new SettingsImportExportException(e2);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0122, code lost:
        r7 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0125, code lost:
        if (com.fsck.k9.K9.DEBUG != false) goto L_0x0127;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0127, code lost:
        android.util.Log.e("k9", "Encountered invalid setting while importing account \"" + r4.name + "\"", r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x014d, code lost:
        r9.add(new com.fsck.k9.preferences.SettingsImporter.AccountDescription(r4.name, r4.uuid, null));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0163, code lost:
        r7 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0164, code lost:
        throw r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x016e, code lost:
        r7 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
        android.util.Log.e("k9", "Exception while importing global settings", r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x019d, code lost:
        r7 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:?, code lost:
        android.util.Log.e("k9", "Exception while importing account \"" + r4.name + "\"", r7);
        r9.add(new com.fsck.k9.preferences.SettingsImporter.AccountDescription(r4.name, r4.uuid, null));
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0163 A[ExcHandler: SettingsImportExportException (r7v1 'e' com.fsck.k9.preferences.SettingsImportExportException A[CUSTOM_DECLARE]), Splitter:B:1:0x0001] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.fsck.k9.preferences.SettingsImporter.ImportResults importSettings(android.content.Context r24, java.io.InputStream r25, boolean r26, java.util.List<java.lang.String> r27, boolean r28) throws com.fsck.k9.preferences.SettingsImportExportException {
        /*
            r10 = 0
            java.util.ArrayList r13 = new java.util.ArrayList     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            r13.<init>()     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            java.util.ArrayList r9 = new java.util.ArrayList     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            r9.<init>()     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            r19 = 0
            r0 = r25
            r1 = r26
            r2 = r27
            r3 = r19
            com.fsck.k9.preferences.SettingsImporter$Imported r12 = parseSettings(r0, r1, r2, r3)     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            com.fsck.k9.Preferences r17 = com.fsck.k9.Preferences.getPreferences(r24)     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            com.fsck.k9.preferences.Storage r18 = r17.getStorage()     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            if (r26 == 0) goto L_0x0050
            com.fsck.k9.preferences.StorageEditor r8 = r18.edit()     // Catch:{ Exception -> 0x016e, SettingsImportExportException -> 0x0163 }
            com.fsck.k9.preferences.SettingsImporter$ImportedSettings r0 = r12.globalSettings     // Catch:{ Exception -> 0x016e, SettingsImportExportException -> 0x0163 }
            r19 = r0
            if (r19 == 0) goto L_0x0165
            int r0 = r12.contentVersion     // Catch:{ Exception -> 0x016e, SettingsImportExportException -> 0x0163 }
            r19 = r0
            com.fsck.k9.preferences.SettingsImporter$ImportedSettings r0 = r12.globalSettings     // Catch:{ Exception -> 0x016e, SettingsImportExportException -> 0x0163 }
            r20 = r0
            r0 = r18
            r1 = r19
            r2 = r20
            importGlobalSettings(r0, r8, r1, r2)     // Catch:{ Exception -> 0x016e, SettingsImportExportException -> 0x0163 }
        L_0x003e:
            boolean r19 = r8.commit()     // Catch:{ Exception -> 0x016e, SettingsImportExportException -> 0x0163 }
            if (r19 == 0) goto L_0x0185
            boolean r19 = com.fsck.k9.K9.DEBUG     // Catch:{ Exception -> 0x016e, SettingsImportExportException -> 0x0163 }
            if (r19 == 0) goto L_0x004f
            java.lang.String r19 = "k9"
            java.lang.String r20 = "Committed global settings to the preference storage."
            android.util.Log.v(r19, r20)     // Catch:{ Exception -> 0x016e, SettingsImportExportException -> 0x0163 }
        L_0x004f:
            r10 = 1
        L_0x0050:
            if (r27 == 0) goto L_0x026b
            int r19 = r27.size()     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            if (r19 <= 0) goto L_0x026b
            java.util.Map<java.lang.String, com.fsck.k9.preferences.SettingsImporter$ImportedAccount> r0 = r12.accounts     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            r19 = r0
            if (r19 == 0) goto L_0x0264
            java.util.Iterator r19 = r27.iterator()     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
        L_0x0062:
            boolean r20 = r19.hasNext()     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            if (r20 == 0) goto L_0x0233
            java.lang.Object r5 = r19.next()     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            java.util.Map<java.lang.String, com.fsck.k9.preferences.SettingsImporter$ImportedAccount> r0 = r12.accounts     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            r20 = r0
            r0 = r20
            boolean r20 = r0.containsKey(r5)     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            if (r20 == 0) goto L_0x0211
            java.util.Map<java.lang.String, com.fsck.k9.preferences.SettingsImporter$ImportedAccount> r0 = r12.accounts     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            r20 = r0
            r0 = r20
            java.lang.Object r4 = r0.get(r5)     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            com.fsck.k9.preferences.SettingsImporter$ImportedAccount r4 = (com.fsck.k9.preferences.SettingsImporter.ImportedAccount) r4     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            com.fsck.k9.preferences.StorageEditor r8 = r18.edit()     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
            int r0 = r12.contentVersion     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
            r20 = r0
            r0 = r24
            r1 = r20
            r2 = r28
            com.fsck.k9.preferences.SettingsImporter$AccountDescriptionPair r11 = importAccount(r0, r8, r1, r4, r2)     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
            boolean r20 = r8.commit()     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
            if (r20 == 0) goto L_0x01da
            boolean r20 = com.fsck.k9.K9.DEBUG     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
            if (r20 == 0) goto L_0x00ca
            java.lang.String r20 = "k9"
            java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
            r21.<init>()     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
            java.lang.String r22 = "Committed settings for account \""
            java.lang.StringBuilder r21 = r21.append(r22)     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
            com.fsck.k9.preferences.SettingsImporter$AccountDescription r0 = r11.imported     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
            r22 = r0
            r0 = r22
            java.lang.String r0 = r0.name     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
            r22 = r0
            java.lang.StringBuilder r21 = r21.append(r22)     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
            java.lang.String r22 = "\" to the settings database."
            java.lang.StringBuilder r21 = r21.append(r22)     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
            java.lang.String r21 = r21.toString()     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
            android.util.Log.v(r20, r21)     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
        L_0x00ca:
            boolean r0 = r11.overwritten     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
            r20 = r0
            if (r20 != 0) goto L_0x0195
            com.fsck.k9.preferences.StorageEditor r8 = r18.edit()     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
            com.fsck.k9.preferences.SettingsImporter$AccountDescription r0 = r11.imported     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
            r20 = r0
            r0 = r20
            java.lang.String r15 = r0.uuid     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
            java.lang.String r20 = "accountUuids"
            java.lang.String r21 = ""
            r0 = r18
            r1 = r20
            r2 = r21
            java.lang.String r16 = r0.getString(r1, r2)     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
            int r20 = r16.length()     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
            if (r20 <= 0) goto L_0x0192
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
            r20.<init>()     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
            r0 = r20
            r1 = r16
            java.lang.StringBuilder r20 = r0.append(r1)     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
            java.lang.String r21 = ","
            java.lang.StringBuilder r20 = r20.append(r21)     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
            r0 = r20
            java.lang.StringBuilder r20 = r0.append(r15)     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
            java.lang.String r14 = r20.toString()     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
        L_0x010d:
            java.lang.String r20 = "accountUuids"
            r0 = r20
            putString(r8, r0, r14)     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
            boolean r20 = r8.commit()     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
            if (r20 != 0) goto L_0x0195
            com.fsck.k9.preferences.SettingsImportExportException r20 = new com.fsck.k9.preferences.SettingsImportExportException     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
            java.lang.String r21 = "Failed to set account UUID list"
            r20.<init>(r21)     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
            throw r20     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
        L_0x0122:
            r7 = move-exception
            boolean r20 = com.fsck.k9.K9.DEBUG     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            if (r20 == 0) goto L_0x014d
            java.lang.String r20 = "k9"
            java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            r21.<init>()     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            java.lang.String r22 = "Encountered invalid setting while importing account \""
            java.lang.StringBuilder r21 = r21.append(r22)     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            java.lang.String r0 = r4.name     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            r22 = r0
            java.lang.StringBuilder r21 = r21.append(r22)     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            java.lang.String r22 = "\""
            java.lang.StringBuilder r21 = r21.append(r22)     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            java.lang.String r21 = r21.toString()     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            r0 = r20
            r1 = r21
            android.util.Log.e(r0, r1, r7)     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
        L_0x014d:
            com.fsck.k9.preferences.SettingsImporter$AccountDescription r20 = new com.fsck.k9.preferences.SettingsImporter$AccountDescription     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            java.lang.String r0 = r4.name     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            r21 = r0
            java.lang.String r0 = r4.uuid     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            r22 = r0
            r23 = 0
            r20.<init>(r21, r22)     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            r0 = r20
            r9.add(r0)     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            goto L_0x0062
        L_0x0163:
            r7 = move-exception
            throw r7
        L_0x0165:
            java.lang.String r19 = "k9"
            java.lang.String r20 = "Was asked to import global settings but none found."
            android.util.Log.w(r19, r20)     // Catch:{ Exception -> 0x016e, SettingsImportExportException -> 0x0163 }
            goto L_0x003e
        L_0x016e:
            r7 = move-exception
            java.lang.String r19 = "k9"
            java.lang.String r20 = "Exception while importing global settings"
            r0 = r19
            r1 = r20
            android.util.Log.e(r0, r1, r7)     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            goto L_0x0050
        L_0x017c:
            r7 = move-exception
            com.fsck.k9.preferences.SettingsImportExportException r19 = new com.fsck.k9.preferences.SettingsImportExportException
            r0 = r19
            r0.<init>(r7)
            throw r19
        L_0x0185:
            boolean r19 = com.fsck.k9.K9.DEBUG     // Catch:{ Exception -> 0x016e, SettingsImportExportException -> 0x0163 }
            if (r19 == 0) goto L_0x0050
            java.lang.String r19 = "k9"
            java.lang.String r20 = "Failed to commit global settings to the preference storage"
            android.util.Log.v(r19, r20)     // Catch:{ Exception -> 0x016e, SettingsImportExportException -> 0x0163 }
            goto L_0x0050
        L_0x0192:
            r14 = r15
            goto L_0x010d
        L_0x0195:
            r17.loadAccounts()     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
            r13.add(r11)     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
            goto L_0x0062
        L_0x019d:
            r7 = move-exception
            java.lang.String r20 = "k9"
            java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            r21.<init>()     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            java.lang.String r22 = "Exception while importing account \""
            java.lang.StringBuilder r21 = r21.append(r22)     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            java.lang.String r0 = r4.name     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            r22 = r0
            java.lang.StringBuilder r21 = r21.append(r22)     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            java.lang.String r22 = "\""
            java.lang.StringBuilder r21 = r21.append(r22)     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            java.lang.String r21 = r21.toString()     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            r0 = r20
            r1 = r21
            android.util.Log.e(r0, r1, r7)     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            com.fsck.k9.preferences.SettingsImporter$AccountDescription r20 = new com.fsck.k9.preferences.SettingsImporter$AccountDescription     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            java.lang.String r0 = r4.name     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            r21 = r0
            java.lang.String r0 = r4.uuid     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            r22 = r0
            r23 = 0
            r20.<init>(r21, r22)     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            r0 = r20
            r9.add(r0)     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            goto L_0x0062
        L_0x01da:
            boolean r20 = com.fsck.k9.K9.DEBUG     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
            if (r20 == 0) goto L_0x0206
            java.lang.String r20 = "k9"
            java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
            r21.<init>()     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
            java.lang.String r22 = "Error while committing settings for account \""
            java.lang.StringBuilder r21 = r21.append(r22)     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
            com.fsck.k9.preferences.SettingsImporter$AccountDescription r0 = r11.original     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
            r22 = r0
            r0 = r22
            java.lang.String r0 = r0.name     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
            r22 = r0
            java.lang.StringBuilder r21 = r21.append(r22)     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
            java.lang.String r22 = "\" to the settings database."
            java.lang.StringBuilder r21 = r21.append(r22)     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
            java.lang.String r21 = r21.toString()     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
            android.util.Log.w(r20, r21)     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
        L_0x0206:
            com.fsck.k9.preferences.SettingsImporter$AccountDescription r0 = r11.original     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
            r20 = r0
            r0 = r20
            r9.add(r0)     // Catch:{ InvalidSettingValueException -> 0x0122, Exception -> 0x019d, SettingsImportExportException -> 0x0163 }
            goto L_0x0062
        L_0x0211:
            java.lang.String r20 = "k9"
            java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            r21.<init>()     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            java.lang.String r22 = "Was asked to import account with UUID "
            java.lang.StringBuilder r21 = r21.append(r22)     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            r0 = r21
            java.lang.StringBuilder r21 = r0.append(r5)     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            java.lang.String r22 = ". But this account wasn't found."
            java.lang.StringBuilder r21 = r21.append(r22)     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            java.lang.String r21 = r21.toString()     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            android.util.Log.w(r20, r21)     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            goto L_0x0062
        L_0x0233:
            com.fsck.k9.preferences.StorageEditor r8 = r18.edit()     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            java.lang.String r19 = "defaultAccountUuid"
            r20 = 0
            java.lang.String r6 = r18.getString(r19, r20)     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            if (r6 != 0) goto L_0x0256
            java.lang.String r20 = "defaultAccountUuid"
            r19 = 0
            r0 = r27
            r1 = r19
            java.lang.Object r19 = r0.get(r1)     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            java.lang.String r19 = (java.lang.String) r19     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            r0 = r20
            r1 = r19
            putString(r8, r0, r1)     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
        L_0x0256:
            boolean r19 = r8.commit()     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            if (r19 != 0) goto L_0x026b
            com.fsck.k9.preferences.SettingsImportExportException r19 = new com.fsck.k9.preferences.SettingsImportExportException     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            java.lang.String r20 = "Failed to set default account"
            r19.<init>(r20)     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            throw r19     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
        L_0x0264:
            java.lang.String r19 = "k9"
            java.lang.String r20 = "Was asked to import at least one account but none found."
            android.util.Log.w(r19, r20)     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
        L_0x026b:
            r17.loadAccounts()     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            com.fsck.k9.K9.loadPrefs(r17)     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            com.fsck.k9.K9.setServicesEnabled(r24)     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            com.fsck.k9.preferences.SettingsImporter$ImportResults r19 = new com.fsck.k9.preferences.SettingsImporter$ImportResults     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            r20 = 0
            r0 = r19
            r1 = r20
            r0.<init>(r10, r13, r9)     // Catch:{ SettingsImportExportException -> 0x0163, Exception -> 0x017c }
            return r19
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fsck.k9.preferences.SettingsImporter.importSettings(android.content.Context, java.io.InputStream, boolean, java.util.List, boolean):com.fsck.k9.preferences.SettingsImporter$ImportResults");
    }

    private static void importGlobalSettings(Storage storage, StorageEditor editor, int contentVersion, ImportedSettings settings) {
        Map<String, Object> validatedSettings = GlobalSettings.validate(contentVersion, settings.settings);
        if (contentVersion != 42) {
            GlobalSettings.upgrade(contentVersion, validatedSettings);
        }
        Map<String, String> stringSettings = GlobalSettings.convert(validatedSettings);
        Map<String, String> mergedSettings = new HashMap<>(GlobalSettings.getGlobalSettings(storage));
        mergedSettings.putAll(stringSettings);
        for (Map.Entry<String, String> setting : mergedSettings.entrySet()) {
            putString(editor, (String) setting.getKey(), (String) setting.getValue());
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r30v0, resolved type: java.util.Map<java.lang.String, java.lang.String>} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r34v0, resolved type: java.util.HashMap} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r34v1, resolved type: java.util.HashMap} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v51, resolved type: java.util.HashMap} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.fsck.k9.preferences.SettingsImporter.AccountDescriptionPair importAccount(android.content.Context r35, com.fsck.k9.preferences.StorageEditor r36, int r37, com.fsck.k9.preferences.SettingsImporter.ImportedAccount r38, boolean r39) throws com.fsck.k9.preferences.Settings.InvalidSettingValueException {
        /*
            com.fsck.k9.preferences.SettingsImporter$AccountDescription r25 = new com.fsck.k9.preferences.SettingsImporter$AccountDescription
            r0 = r38
            java.lang.String r3 = r0.name
            r0 = r38
            java.lang.String r4 = r0.uuid
            r6 = 0
            r0 = r25
            r0.<init>(r3, r4)
            com.fsck.k9.Preferences r9 = com.fsck.k9.Preferences.getPreferences(r35)
            java.util.List r18 = r9.getAccounts()
            r0 = r38
            java.lang.String r5 = r0.uuid
            com.fsck.k9.Account r8 = r9.getAccount(r5)
            if (r39 == 0) goto L_0x00ab
            if (r8 == 0) goto L_0x00ab
            r14 = 1
        L_0x0025:
            if (r39 != 0) goto L_0x0031
            if (r8 == 0) goto L_0x0031
            java.util.UUID r3 = java.util.UUID.randomUUID()
            java.lang.String r5 = r3.toString()
        L_0x0031:
            r0 = r38
            java.lang.String r0 = r0.name
            r17 = r0
            boolean r3 = isAccountNameUsed(r17, r18)
            if (r3 == 0) goto L_0x0070
            r20 = 1
        L_0x003f:
            int r3 = r18.size()
            r0 = r20
            if (r0 > r3) goto L_0x0070
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r0 = r38
            java.lang.String r4 = r0.name
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = " ("
            java.lang.StringBuilder r3 = r3.append(r4)
            r0 = r20
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r4 = ")"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r17 = r3.toString()
            boolean r3 = isAccountNameUsed(r17, r18)
            if (r3 != 0) goto L_0x00ae
        L_0x0070:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r4 = "."
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r16 = r3.toString()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r0 = r16
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r4 = "description"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r0 = r36
            r1 = r17
            putString(r0, r3, r1)
            r0 = r38
            com.fsck.k9.preferences.SettingsImporter$ImportedServer r3 = r0.incoming
            if (r3 != 0) goto L_0x00b1
            com.fsck.k9.preferences.Settings$InvalidSettingValueException r3 = new com.fsck.k9.preferences.Settings$InvalidSettingValueException
            r3.<init>()
            throw r3
        L_0x00ab:
            r14 = 0
            goto L_0x0025
        L_0x00ae:
            int r20 = r20 + 1
            goto L_0x003f
        L_0x00b1:
            com.fsck.k9.preferences.SettingsImporter$ImportedServerSettings r22 = new com.fsck.k9.preferences.SettingsImporter$ImportedServerSettings
            r0 = r38
            com.fsck.k9.preferences.SettingsImporter$ImportedServer r3 = r0.incoming
            r0 = r22
            r0.<init>(r3)
            java.lang.String r29 = com.fsck.k9.mail.store.RemoteStore.createStoreUri(r22)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r0 = r16
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r4 = "storeUri"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            java.lang.String r4 = com.fsck.k9.mail.filter.Base64.encode(r29)
            r0 = r36
            putString(r0, r3, r4)
            com.fsck.k9.mail.AuthType r3 = com.fsck.k9.mail.AuthType.EXTERNAL
            r0 = r22
            com.fsck.k9.mail.AuthType r4 = r0.authenticationType
            if (r3 == r4) goto L_0x011e
            com.fsck.k9.mail.AuthType r3 = com.fsck.k9.mail.AuthType.XOAUTH2
            r0 = r22
            com.fsck.k9.mail.AuthType r4 = r0.authenticationType
            if (r3 == r4) goto L_0x011e
            r0 = r22
            java.lang.String r3 = r0.password
            if (r3 == 0) goto L_0x00fe
            r0 = r22
            java.lang.String r3 = r0.password
            boolean r3 = r3.isEmpty()
            if (r3 == 0) goto L_0x011e
        L_0x00fe:
            r19 = 1
        L_0x0100:
            r0 = r38
            com.fsck.k9.preferences.SettingsImporter$ImportedServer r3 = r0.outgoing
            if (r3 != 0) goto L_0x0121
            com.fsck.k9.mail.ServerSettings$Type r3 = com.fsck.k9.mail.ServerSettings.Type.WebDAV
            java.lang.String r3 = r3.name()
            r0 = r38
            com.fsck.k9.preferences.SettingsImporter$ImportedServer r4 = r0.incoming
            java.lang.String r4 = r4.type
            boolean r3 = r3.equals(r4)
            if (r3 != 0) goto L_0x0121
            com.fsck.k9.preferences.Settings$InvalidSettingValueException r3 = new com.fsck.k9.preferences.Settings$InvalidSettingValueException
            r3.<init>()
            throw r3
        L_0x011e:
            r19 = 0
            goto L_0x0100
        L_0x0121:
            r0 = r38
            com.fsck.k9.preferences.SettingsImporter$ImportedServer r3 = r0.outgoing
            if (r3 == 0) goto L_0x0194
            com.fsck.k9.preferences.SettingsImporter$ImportedServerSettings r26 = new com.fsck.k9.preferences.SettingsImporter$ImportedServerSettings
            r0 = r38
            com.fsck.k9.preferences.SettingsImporter$ImportedServer r3 = r0.outgoing
            r0 = r26
            r0.<init>(r3)
            java.lang.String r31 = com.fsck.k9.mail.Transport.createTransportUri(r26)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r0 = r16
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r4 = "transportUri"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            java.lang.String r4 = com.fsck.k9.mail.filter.Base64.encode(r31)
            r0 = r36
            putString(r0, r3, r4)
            com.fsck.k9.mail.AuthType r3 = com.fsck.k9.mail.AuthType.EXTERNAL
            r0 = r26
            com.fsck.k9.mail.AuthType r4 = r0.authenticationType
            if (r3 == r4) goto L_0x0226
            com.fsck.k9.mail.AuthType r3 = com.fsck.k9.mail.AuthType.XOAUTH2
            r0 = r26
            com.fsck.k9.mail.AuthType r4 = r0.authenticationType
            if (r3 == r4) goto L_0x0226
            com.fsck.k9.mail.ServerSettings$Type r3 = com.fsck.k9.mail.ServerSettings.Type.WebDAV
            r0 = r26
            com.fsck.k9.mail.ServerSettings$Type r4 = r0.type
            if (r3 == r4) goto L_0x0226
            r0 = r26
            java.lang.String r3 = r0.username
            if (r3 == 0) goto L_0x0226
            r0 = r26
            java.lang.String r3 = r0.username
            boolean r3 = r3.isEmpty()
            if (r3 != 0) goto L_0x0226
            r0 = r26
            java.lang.String r3 = r0.password
            if (r3 == 0) goto L_0x018c
            r0 = r26
            java.lang.String r3 = r0.password
            boolean r3 = r3.isEmpty()
            if (r3 == 0) goto L_0x0226
        L_0x018c:
            r27 = 1
        L_0x018e:
            if (r27 != 0) goto L_0x0192
            if (r19 == 0) goto L_0x022a
        L_0x0192:
            r19 = 1
        L_0x0194:
            if (r19 == 0) goto L_0x01b1
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r0 = r16
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r4 = "enabled"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r4 = 0
            r0 = r36
            r0.putBoolean(r3, r4)
        L_0x01b1:
            r0 = r38
            com.fsck.k9.preferences.SettingsImporter$ImportedSettings r3 = r0.settings
            java.util.Map<java.lang.String, java.lang.String> r4 = r3.settings
            if (r14 != 0) goto L_0x022e
            r3 = 1
        L_0x01ba:
            r0 = r37
            java.util.Map r32 = com.fsck.k9.preferences.AccountSettings.validate(r0, r4, r3)
            r3 = 42
            r0 = r37
            if (r0 == r3) goto L_0x01cd
            r0 = r37
            r1 = r32
            com.fsck.k9.preferences.AccountSettings.upgrade(r0, r1)
        L_0x01cd:
            java.util.Map r30 = com.fsck.k9.preferences.AccountSettings.convert(r32)
            if (r14 == 0) goto L_0x0230
            java.util.HashMap r34 = new java.util.HashMap
            com.fsck.k9.preferences.Storage r3 = r9.getStorage()
            java.util.Map r3 = com.fsck.k9.preferences.AccountSettings.getAccountSettings(r3, r5)
            r0 = r34
            r0.<init>(r3)
            r0 = r34
            r1 = r30
            r0.putAll(r1)
        L_0x01e9:
            java.util.Set r3 = r34.entrySet()
            java.util.Iterator r4 = r3.iterator()
        L_0x01f1:
            boolean r3 = r4.hasNext()
            if (r3 == 0) goto L_0x0233
            java.lang.Object r28 = r4.next()
            java.util.Map$Entry r28 = (java.util.Map.Entry) r28
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r0 = r16
            java.lang.StringBuilder r6 = r3.append(r0)
            java.lang.Object r3 = r28.getKey()
            java.lang.String r3 = (java.lang.String) r3
            java.lang.StringBuilder r3 = r6.append(r3)
            java.lang.String r23 = r3.toString()
            java.lang.Object r33 = r28.getValue()
            java.lang.String r33 = (java.lang.String) r33
            r0 = r36
            r1 = r23
            r2 = r33
            putString(r0, r1, r2)
            goto L_0x01f1
        L_0x0226:
            r27 = 0
            goto L_0x018e
        L_0x022a:
            r19 = 0
            goto L_0x0194
        L_0x022e:
            r3 = 0
            goto L_0x01ba
        L_0x0230:
            r34 = r30
            goto L_0x01e9
        L_0x0233:
            if (r14 != 0) goto L_0x0257
            int r24 = com.fsck.k9.Account.generateAccountNumber(r9)
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r0 = r16
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r4 = "accountNumber"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            java.lang.String r4 = java.lang.Integer.toString(r24)
            r0 = r36
            putString(r0, r3, r4)
        L_0x0257:
            r0 = r38
            java.util.List<com.fsck.k9.preferences.SettingsImporter$ImportedIdentity> r3 = r0.identities
            if (r3 == 0) goto L_0x028c
            r3 = r36
            r4 = r37
            r6 = r38
            r7 = r39
            importIdentities(r3, r4, r5, r6, r7, r8, r9)
        L_0x0268:
            r0 = r38
            java.util.List<com.fsck.k9.preferences.SettingsImporter$ImportedFolder> r3 = r0.folders
            if (r3 == 0) goto L_0x0294
            r0 = r38
            java.util.List<com.fsck.k9.preferences.SettingsImporter$ImportedFolder> r3 = r0.folders
            java.util.Iterator r3 = r3.iterator()
        L_0x0276:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x0294
            java.lang.Object r13 = r3.next()
            com.fsck.k9.preferences.SettingsImporter$ImportedFolder r13 = (com.fsck.k9.preferences.SettingsImporter.ImportedFolder) r13
            r10 = r36
            r11 = r37
            r12 = r5
            r15 = r9
            importFolder(r10, r11, r12, r13, r14, r15)
            goto L_0x0276
        L_0x028c:
            if (r14 != 0) goto L_0x0268
            com.fsck.k9.preferences.Settings$InvalidSettingValueException r3 = new com.fsck.k9.preferences.Settings$InvalidSettingValueException
            r3.<init>()
            throw r3
        L_0x0294:
            com.fsck.k9.preferences.SettingsImporter$AccountDescription r21 = new com.fsck.k9.preferences.SettingsImporter$AccountDescription
            r3 = 0
            r0 = r21
            r1 = r17
            r0.<init>(r1, r5)
            com.fsck.k9.preferences.SettingsImporter$AccountDescriptionPair r3 = new com.fsck.k9.preferences.SettingsImporter$AccountDescriptionPair
            r4 = 0
            r0 = r25
            r1 = r21
            r3.<init>(r0, r1, r14)
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fsck.k9.preferences.SettingsImporter.importAccount(android.content.Context, com.fsck.k9.preferences.StorageEditor, int, com.fsck.k9.preferences.SettingsImporter$ImportedAccount, boolean):com.fsck.k9.preferences.SettingsImporter$AccountDescriptionPair");
    }

    private static void importFolder(StorageEditor editor, int contentVersion, String uuid, ImportedFolder folder, boolean overwrite, Preferences prefs) {
        Map<String, String> writeSettings;
        Map<String, Object> validatedSettings = FolderSettings.validate(contentVersion, folder.settings.settings, !overwrite);
        if (contentVersion != 42) {
            FolderSettings.upgrade(contentVersion, validatedSettings);
        }
        Map<String, String> stringSettings = FolderSettings.convert(validatedSettings);
        if (overwrite) {
            writeSettings = FolderSettings.getFolderSettings(prefs.getStorage(), uuid, folder.name);
            writeSettings.putAll(stringSettings);
        } else {
            writeSettings = stringSettings;
        }
        String prefix = uuid + "." + folder.name + ".";
        for (Map.Entry<String, String> setting : writeSettings.entrySet()) {
            putString(editor, prefix + ((String) setting.getKey()), (String) setting.getValue());
        }
    }

    private static void importIdentities(StorageEditor editor, int contentVersion, String uuid, ImportedAccount account, boolean overwrite, Account existingAccount, Preferences prefs) throws Settings.InvalidSettingValueException {
        List<Identity> existingIdentities;
        Map<String, String> writeSettings;
        int identityIndex;
        String accountKeyPrefix = uuid + ".";
        int nextIdentityIndex = 0;
        if (!overwrite || existingAccount == null) {
            existingIdentities = new ArrayList<>();
        } else {
            existingIdentities = existingAccount.getIdentities();
            nextIdentityIndex = existingIdentities.size();
        }
        for (ImportedIdentity identity : account.identities) {
            int writeIdentityIndex = nextIdentityIndex;
            boolean mergeSettings = false;
            if (overwrite && existingIdentities.size() > 0 && (identityIndex = findIdentity(identity, existingIdentities)) != -1) {
                writeIdentityIndex = identityIndex;
                mergeSettings = true;
            }
            if (!mergeSettings) {
                nextIdentityIndex++;
            }
            String identityDescription = identity.description == null ? "Imported" : identity.description;
            if (isIdentityDescriptionUsed(identityDescription, existingIdentities)) {
                for (int i = 1; i <= existingIdentities.size(); i++) {
                    identityDescription = identity.description + " (" + i + ")";
                    if (!isIdentityDescriptionUsed(identityDescription, existingIdentities)) {
                        break;
                    }
                }
            }
            String identitySuffix = "." + writeIdentityIndex;
            putString(editor, accountKeyPrefix + "name" + identitySuffix, identity.name == null ? "" : identity.name);
            if (!IdentitySettings.isEmailAddressValid(identity.email)) {
                throw new Settings.InvalidSettingValueException();
            }
            putString(editor, accountKeyPrefix + "email" + identitySuffix, identity.email);
            putString(editor, accountKeyPrefix + "description" + identitySuffix, identityDescription);
            if (identity.settings != null) {
                Map<String, Object> validatedSettings = IdentitySettings.validate(contentVersion, identity.settings.settings, !mergeSettings);
                if (contentVersion != 42) {
                    IdentitySettings.upgrade(contentVersion, validatedSettings);
                }
                Map<String, String> stringSettings = IdentitySettings.convert(validatedSettings);
                if (mergeSettings) {
                    writeSettings = new HashMap<>(IdentitySettings.getIdentitySettings(prefs.getStorage(), uuid, writeIdentityIndex));
                    writeSettings.putAll(stringSettings);
                } else {
                    writeSettings = stringSettings;
                }
                for (Map.Entry<String, String> setting : writeSettings.entrySet()) {
                    putString(editor, accountKeyPrefix + ((String) setting.getKey()) + identitySuffix, (String) setting.getValue());
                }
            }
        }
    }

    private static boolean isAccountNameUsed(String name, List<Account> accounts) {
        for (Account account : accounts) {
            if (account != null && account.getDescription().equals(name)) {
                return true;
            }
        }
        return false;
    }

    private static boolean isIdentityDescriptionUsed(String description, List<Identity> identities) {
        for (Identity identity : identities) {
            if (identity.getDescription().equals(description)) {
                return true;
            }
        }
        return false;
    }

    private static int findIdentity(ImportedIdentity identity, List<Identity> identities) {
        for (int i = 0; i < identities.size(); i++) {
            Identity existingIdentity = identities.get(i);
            if (existingIdentity.getName().equals(identity.name) && existingIdentity.getEmail().equals(identity.email)) {
                return i;
            }
        }
        return -1;
    }

    private static void putString(StorageEditor editor, String key, String value) {
        if (K9.DEBUG) {
            String outputValue = value;
            if (!K9.DEBUG_SENSITIVE && (key.endsWith(".transportUri") || key.endsWith(".storeUri"))) {
                outputValue = "*sensitive*";
            }
            Log.v("k9", "Setting " + key + "=" + outputValue);
        }
        editor.putString(key, value);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x006a, code lost:
        if (r3.accounts == null) goto L_0x006c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static com.fsck.k9.preferences.SettingsImporter.Imported parseSettings(java.io.InputStream r9, boolean r10, java.util.List<java.lang.String> r11, boolean r12) throws com.fsck.k9.preferences.SettingsImportExportException {
        /*
            if (r12 != 0) goto L_0x000c
            if (r11 != 0) goto L_0x000c
            java.lang.IllegalArgumentException r6 = new java.lang.IllegalArgumentException
            java.lang.String r7 = "Argument 'accountUuids' must not be null."
            r6.<init>(r7)
            throw r6
        L_0x000c:
            org.xmlpull.v1.XmlPullParserFactory r2 = org.xmlpull.v1.XmlPullParserFactory.newInstance()     // Catch:{ Exception -> 0x0059 }
            org.xmlpull.v1.XmlPullParser r5 = r2.newPullParser()     // Catch:{ Exception -> 0x0059 }
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0059 }
            r4.<init>(r9)     // Catch:{ Exception -> 0x0059 }
            r5.setInput(r4)     // Catch:{ Exception -> 0x0059 }
            r3 = 0
            int r1 = r5.getEventType()     // Catch:{ Exception -> 0x0059 }
        L_0x0021:
            r6 = 1
            if (r1 == r6) goto L_0x0060
            r6 = 2
            if (r1 != r6) goto L_0x0037
            java.lang.String r6 = "k9settings"
            java.lang.String r7 = r5.getName()     // Catch:{ Exception -> 0x0059 }
            boolean r6 = r6.equals(r7)     // Catch:{ Exception -> 0x0059 }
            if (r6 == 0) goto L_0x003c
            com.fsck.k9.preferences.SettingsImporter$Imported r3 = parseRoot(r5, r10, r11, r12)     // Catch:{ Exception -> 0x0059 }
        L_0x0037:
            int r1 = r5.next()     // Catch:{ Exception -> 0x0059 }
            goto L_0x0021
        L_0x003c:
            java.lang.String r6 = "k9"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0059 }
            r7.<init>()     // Catch:{ Exception -> 0x0059 }
            java.lang.String r8 = "Unexpected start tag: "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x0059 }
            java.lang.String r8 = r5.getName()     // Catch:{ Exception -> 0x0059 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x0059 }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x0059 }
            android.util.Log.w(r6, r7)     // Catch:{ Exception -> 0x0059 }
            goto L_0x0037
        L_0x0059:
            r0 = move-exception
            com.fsck.k9.preferences.SettingsImportExportException r6 = new com.fsck.k9.preferences.SettingsImportExportException
            r6.<init>(r0)
            throw r6
        L_0x0060:
            if (r3 == 0) goto L_0x006c
            if (r12 == 0) goto L_0x0074
            com.fsck.k9.preferences.SettingsImporter$ImportedSettings r6 = r3.globalSettings     // Catch:{ Exception -> 0x0059 }
            if (r6 != 0) goto L_0x0074
            java.util.Map<java.lang.String, com.fsck.k9.preferences.SettingsImporter$ImportedAccount> r6 = r3.accounts     // Catch:{ Exception -> 0x0059 }
            if (r6 != 0) goto L_0x0074
        L_0x006c:
            com.fsck.k9.preferences.SettingsImportExportException r6 = new com.fsck.k9.preferences.SettingsImportExportException     // Catch:{ Exception -> 0x0059 }
            java.lang.String r7 = "Invalid import data"
            r6.<init>(r7)     // Catch:{ Exception -> 0x0059 }
            throw r6     // Catch:{ Exception -> 0x0059 }
        L_0x0074:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fsck.k9.preferences.SettingsImporter.parseSettings(java.io.InputStream, boolean, java.util.List, boolean):com.fsck.k9.preferences.SettingsImporter$Imported");
    }

    private static void skipToEndTag(XmlPullParser xpp, String endTag) throws XmlPullParserException, IOException {
        int eventType = xpp.next();
        while (true) {
            if (eventType != 3 || !endTag.equals(xpp.getName())) {
                eventType = xpp.next();
            } else {
                return;
            }
        }
    }

    private static String getText(XmlPullParser xpp) throws XmlPullParserException, IOException {
        if (xpp.next() != 4) {
            return "";
        }
        return xpp.getText();
    }

    private static Imported parseRoot(XmlPullParser xpp, boolean globalSettings, List<String> accountUuids, boolean overview) throws XmlPullParserException, IOException, SettingsImportExportException {
        Imported result = new Imported();
        validateFileFormatVersion(xpp.getAttributeValue(null, SettingsExporter.FILE_FORMAT_ATTRIBUTE));
        result.contentVersion = validateContentVersion(xpp.getAttributeValue(null, SettingsExporter.VERSION_ATTRIBUTE));
        int eventType = xpp.next();
        while (true) {
            if (eventType == 3 && SettingsExporter.ROOT_ELEMENT.equals(xpp.getName())) {
                return result;
            }
            if (eventType == 2) {
                String element = xpp.getName();
                if (SettingsExporter.GLOBAL_ELEMENT.equals(element)) {
                    if (!overview && !globalSettings) {
                        skipToEndTag(xpp, SettingsExporter.GLOBAL_ELEMENT);
                        Log.i("k9", "Skipping global settings");
                    } else if (result.globalSettings != null) {
                        skipToEndTag(xpp, SettingsExporter.GLOBAL_ELEMENT);
                        Log.w("k9", "More than one global settings element. Only using the first one!");
                    } else if (overview) {
                        result.globalSettings = new ImportedSettings();
                        skipToEndTag(xpp, SettingsExporter.GLOBAL_ELEMENT);
                    } else {
                        result.globalSettings = parseSettings(xpp, SettingsExporter.GLOBAL_ELEMENT);
                    }
                } else if (!SettingsExporter.ACCOUNTS_ELEMENT.equals(element)) {
                    Log.w("k9", "Unexpected start tag: " + xpp.getName());
                } else if (result.accounts == null) {
                    result.accounts = parseAccounts(xpp, accountUuids, overview);
                } else {
                    Log.w("k9", "More than one accounts element. Only using the first one!");
                }
            }
            eventType = xpp.next();
        }
    }

    private static int validateFileFormatVersion(String versionString) throws SettingsImportExportException {
        if (versionString == null) {
            throw new SettingsImportExportException("Missing file format version");
        }
        try {
            int version = Integer.parseInt(versionString);
            if (version == 1) {
                return version;
            }
            throw new SettingsImportExportException("Unsupported file format version: " + versionString);
        } catch (NumberFormatException e) {
            throw new SettingsImportExportException("Invalid file format version: " + versionString);
        }
    }

    private static int validateContentVersion(String versionString) throws SettingsImportExportException {
        if (versionString == null) {
            throw new SettingsImportExportException("Missing content version");
        }
        try {
            int version = Integer.parseInt(versionString);
            if (version >= 1) {
                return version;
            }
            throw new SettingsImportExportException("Unsupported content version: " + versionString);
        } catch (NumberFormatException e) {
            throw new SettingsImportExportException("Invalid content version: " + versionString);
        }
    }

    private static ImportedSettings parseSettings(XmlPullParser xpp, String endTag) throws XmlPullParserException, IOException {
        ImportedSettings result = null;
        int eventType = xpp.next();
        while (true) {
            if (eventType == 3 && endTag.equals(xpp.getName())) {
                return result;
            }
            if (eventType == 2) {
                if (SettingsExporter.VALUE_ELEMENT.equals(xpp.getName())) {
                    String key = xpp.getAttributeValue(null, SettingsExporter.KEY_ATTRIBUTE);
                    String value = getText(xpp);
                    if (result == null) {
                        result = new ImportedSettings();
                    }
                    if (result.settings.containsKey(key)) {
                        Log.w("k9", "Already read key \"" + key + "\". Ignoring value \"" + value + "\"");
                    } else {
                        result.settings.put(key, value);
                    }
                } else {
                    Log.w("k9", "Unexpected start tag: " + xpp.getName());
                }
            }
            eventType = xpp.next();
        }
    }

    private static Map<String, ImportedAccount> parseAccounts(XmlPullParser xpp, List<String> accountUuids, boolean overview) throws XmlPullParserException, IOException {
        Map<String, ImportedAccount> accounts = null;
        int eventType = xpp.next();
        while (true) {
            if (eventType == 3 && SettingsExporter.ACCOUNTS_ELEMENT.equals(xpp.getName())) {
                return accounts;
            }
            if (eventType == 2) {
                if ("account".equals(xpp.getName())) {
                    if (accounts == null) {
                        accounts = new HashMap<>();
                    }
                    ImportedAccount account = parseAccount(xpp, accountUuids, overview);
                    if (account != null) {
                        if (!accounts.containsKey(account.uuid)) {
                            accounts.put(account.uuid, account);
                        } else {
                            Log.w("k9", "Duplicate account entries with UUID " + account.uuid + ". Ignoring!");
                        }
                    }
                } else {
                    Log.w("k9", "Unexpected start tag: " + xpp.getName());
                }
            }
            eventType = xpp.next();
        }
    }

    private static ImportedAccount parseAccount(XmlPullParser xpp, List<String> accountUuids, boolean overview) throws XmlPullParserException, IOException {
        ImportedAccount account = null;
        String uuid = xpp.getAttributeValue(null, SettingsExporter.UUID_ATTRIBUTE);
        try {
            UUID.fromString(uuid);
            account = new ImportedAccount();
            account.uuid = uuid;
            if (overview || accountUuids.contains(uuid)) {
                int eventType = xpp.next();
                while (true) {
                    if (eventType == 3 && "account".equals(xpp.getName())) {
                        break;
                    }
                    if (eventType == 2) {
                        String element = xpp.getName();
                        if ("name".equals(element)) {
                            account.name = getText(xpp);
                        } else if (SettingsExporter.INCOMING_SERVER_ELEMENT.equals(element)) {
                            if (overview) {
                                skipToEndTag(xpp, SettingsExporter.INCOMING_SERVER_ELEMENT);
                            } else {
                                account.incoming = parseServerSettings(xpp, SettingsExporter.INCOMING_SERVER_ELEMENT);
                            }
                        } else if (SettingsExporter.OUTGOING_SERVER_ELEMENT.equals(element)) {
                            if (overview) {
                                skipToEndTag(xpp, SettingsExporter.OUTGOING_SERVER_ELEMENT);
                            } else {
                                account.outgoing = parseServerSettings(xpp, SettingsExporter.OUTGOING_SERVER_ELEMENT);
                            }
                        } else if (SettingsExporter.SETTINGS_ELEMENT.equals(element)) {
                            if (overview) {
                                skipToEndTag(xpp, SettingsExporter.SETTINGS_ELEMENT);
                            } else {
                                account.settings = parseSettings(xpp, SettingsExporter.SETTINGS_ELEMENT);
                            }
                        } else if (SettingsExporter.IDENTITIES_ELEMENT.equals(element)) {
                            if (overview) {
                                skipToEndTag(xpp, SettingsExporter.IDENTITIES_ELEMENT);
                            } else {
                                account.identities = parseIdentities(xpp);
                            }
                        } else if (!SettingsExporter.FOLDERS_ELEMENT.equals(element)) {
                            Log.w("k9", "Unexpected start tag: " + xpp.getName());
                        } else if (overview) {
                            skipToEndTag(xpp, SettingsExporter.FOLDERS_ELEMENT);
                        } else {
                            account.folders = parseFolders(xpp);
                        }
                    }
                    eventType = xpp.next();
                }
            } else {
                skipToEndTag(xpp, "account");
                Log.i("k9", "Skipping account with UUID " + uuid);
            }
            if (account.name == null) {
                account.name = uuid;
            }
        } catch (Exception e) {
            skipToEndTag(xpp, "account");
            Log.w("k9", "Skipping account with invalid UUID " + uuid);
        }
        return account;
    }

    private static ImportedServer parseServerSettings(XmlPullParser xpp, String endTag) throws XmlPullParserException, IOException {
        ImportedServer server = new ImportedServer();
        server.type = xpp.getAttributeValue(null, SettingsExporter.TYPE_ATTRIBUTE);
        int eventType = xpp.next();
        while (true) {
            if (eventType == 3 && endTag.equals(xpp.getName())) {
                return server;
            }
            if (eventType == 2) {
                String element = xpp.getName();
                if (SettingsExporter.HOST_ELEMENT.equals(element)) {
                    server.host = getText(xpp);
                } else if (SettingsExporter.PORT_ELEMENT.equals(element)) {
                    server.port = getText(xpp);
                } else if (SettingsExporter.CONNECTION_SECURITY_ELEMENT.equals(element)) {
                    server.connectionSecurity = getText(xpp);
                } else if (SettingsExporter.AUTHENTICATION_TYPE_ELEMENT.equals(element)) {
                    server.authenticationType = AuthType.valueOf(getText(xpp));
                } else if (SettingsExporter.USERNAME_ELEMENT.equals(element)) {
                    server.username = getText(xpp);
                } else if (SettingsExporter.CLIENT_CERTIFICATE_ALIAS_ELEMENT.equals(element)) {
                    server.clientCertificateAlias = getText(xpp);
                } else if (SettingsExporter.PASSWORD_ELEMENT.equals(element)) {
                    server.password = getText(xpp);
                } else if (SettingsExporter.EXTRA_ELEMENT.equals(element)) {
                    server.extras = parseSettings(xpp, SettingsExporter.EXTRA_ELEMENT);
                } else {
                    Log.w("k9", "Unexpected start tag: " + xpp.getName());
                }
            }
            eventType = xpp.next();
        }
    }

    private static List<ImportedIdentity> parseIdentities(XmlPullParser xpp) throws XmlPullParserException, IOException {
        List<ImportedIdentity> identities = null;
        int eventType = xpp.next();
        while (true) {
            if (eventType == 3 && SettingsExporter.IDENTITIES_ELEMENT.equals(xpp.getName())) {
                return identities;
            }
            if (eventType == 2) {
                if (SettingsExporter.IDENTITY_ELEMENT.equals(xpp.getName())) {
                    if (identities == null) {
                        identities = new ArrayList<>();
                    }
                    identities.add(parseIdentity(xpp));
                } else {
                    Log.w("k9", "Unexpected start tag: " + xpp.getName());
                }
            }
            eventType = xpp.next();
        }
    }

    private static ImportedIdentity parseIdentity(XmlPullParser xpp) throws XmlPullParserException, IOException {
        ImportedIdentity identity = new ImportedIdentity();
        int eventType = xpp.next();
        while (true) {
            if (eventType == 3 && SettingsExporter.IDENTITY_ELEMENT.equals(xpp.getName())) {
                return identity;
            }
            if (eventType == 2) {
                String element = xpp.getName();
                if ("name".equals(element)) {
                    identity.name = getText(xpp);
                } else if ("email".equals(element)) {
                    identity.email = getText(xpp);
                } else if ("description".equals(element)) {
                    identity.description = getText(xpp);
                } else if (SettingsExporter.SETTINGS_ELEMENT.equals(element)) {
                    identity.settings = parseSettings(xpp, SettingsExporter.SETTINGS_ELEMENT);
                } else {
                    Log.w("k9", "Unexpected start tag: " + xpp.getName());
                }
            }
            eventType = xpp.next();
        }
    }

    private static List<ImportedFolder> parseFolders(XmlPullParser xpp) throws XmlPullParserException, IOException {
        List<ImportedFolder> folders = null;
        int eventType = xpp.next();
        while (true) {
            if (eventType == 3 && SettingsExporter.FOLDERS_ELEMENT.equals(xpp.getName())) {
                return folders;
            }
            if (eventType == 2) {
                if (SettingsExporter.FOLDER_ELEMENT.equals(xpp.getName())) {
                    if (folders == null) {
                        folders = new ArrayList<>();
                    }
                    folders.add(parseFolder(xpp));
                } else {
                    Log.w("k9", "Unexpected start tag: " + xpp.getName());
                }
            }
            eventType = xpp.next();
        }
    }

    private static ImportedFolder parseFolder(XmlPullParser xpp) throws XmlPullParserException, IOException {
        ImportedFolder folder = new ImportedFolder();
        folder.name = xpp.getAttributeValue(null, "name");
        folder.settings = parseSettings(xpp, SettingsExporter.FOLDER_ELEMENT);
        return folder;
    }

    private static class ImportedServerSettings extends ServerSettings {
        private final ImportedServer mImportedServer;

        public ImportedServerSettings(ImportedServer server) {
            super(ServerSettings.Type.valueOf(server.type), server.host, convertPort(server.port), convertConnectionSecurity(server.connectionSecurity), server.authenticationType, server.username, server.password, server.clientCertificateAlias);
            this.mImportedServer = server;
        }

        public Map<String, String> getExtra() {
            if (this.mImportedServer.extras != null) {
                return Collections.unmodifiableMap(this.mImportedServer.extras.settings);
            }
            return null;
        }

        private static int convertPort(String port) {
            try {
                return Integer.parseInt(port);
            } catch (NumberFormatException e) {
                return -1;
            }
        }

        private static ConnectionSecurity convertConnectionSecurity(String connectionSecurity) {
            try {
                if ("SSL_TLS_OPTIONAL".equals(connectionSecurity)) {
                    return ConnectionSecurity.SSL_TLS_REQUIRED;
                }
                if ("STARTTLS_OPTIONAL".equals(connectionSecurity)) {
                    return ConnectionSecurity.STARTTLS_REQUIRED;
                }
                return ConnectionSecurity.valueOf(connectionSecurity);
            } catch (Exception e) {
                return ConnectionSecurity.SSL_TLS_REQUIRED;
            }
        }
    }

    protected static class Imported {
        public Map<String, ImportedAccount> accounts;
        public int contentVersion;
        public ImportedSettings globalSettings;

        protected Imported() {
        }
    }

    private static class ImportedSettings {
        public Map<String, String> settings;

        private ImportedSettings() {
            this.settings = new HashMap();
        }
    }

    protected static class ImportedAccount {
        public List<ImportedFolder> folders;
        public List<ImportedIdentity> identities;
        public ImportedServer incoming;
        public String name;
        public ImportedServer outgoing;
        public ImportedSettings settings;
        public String uuid;

        protected ImportedAccount() {
        }
    }

    protected static class ImportedServer {
        public AuthType authenticationType;
        public String clientCertificateAlias;
        public String connectionSecurity;
        public ImportedSettings extras;
        public String host;
        public String password;
        public String port;
        public String type;
        public String username;

        protected ImportedServer() {
        }
    }

    private static class ImportedIdentity {
        public String description;
        public String email;
        public String name;
        public ImportedSettings settings;

        private ImportedIdentity() {
        }
    }

    private static class ImportedFolder {
        public String name;
        public ImportedSettings settings;

        private ImportedFolder() {
        }
    }
}
