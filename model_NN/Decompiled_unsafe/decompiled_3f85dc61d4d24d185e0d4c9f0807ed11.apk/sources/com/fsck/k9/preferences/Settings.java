package com.fsck.k9.preferences;

import android.support.v4.view.ViewCompat;
import android.util.Log;
import com.fsck.k9.K9;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

public class Settings {
    public static final int VERSION = 42;

    public static class InvalidSettingValueException extends Exception {
        private static final long serialVersionUID = 1;
    }

    public interface SettingsUpgrader {
        Set<String> upgrade(Map<String, Object> map);
    }

    public static Map<String, Object> validate(int version, Map<String, TreeMap<Integer, SettingsDescription>> settings, Map<String, String> importedSettings, boolean useDefaultValues) {
        SettingsDescription desc;
        boolean useDefaultValue;
        Map<String, Object> validatedSettings = new HashMap<>();
        for (Map.Entry<String, TreeMap<Integer, SettingsDescription>> versionedSetting : settings.entrySet()) {
            TreeMap<Integer, SettingsDescription> versions = versionedSetting.getValue();
            SortedMap<Integer, SettingsDescription> headMap = versions.headMap(Integer.valueOf(version + 1));
            if (!headMap.isEmpty() && (desc = (SettingsDescription) versions.get((Integer) headMap.lastKey())) != null) {
                String key = (String) versionedSetting.getKey();
                if (!importedSettings.containsKey(key)) {
                    Log.v("k9", "Key \"" + key + "\" wasn't found in the imported file." + (useDefaultValues ? " Using default value." : ""));
                    useDefaultValue = useDefaultValues;
                } else {
                    String prettyValue = importedSettings.get(key);
                    try {
                        validatedSettings.put(key, desc.fromPrettyString(prettyValue));
                        useDefaultValue = false;
                    } catch (InvalidSettingValueException e) {
                        Log.v("k9", "Key \"" + key + "\" has invalid value \"" + prettyValue + "\" in imported file. " + (useDefaultValues ? "Using default value." : "Skipping."));
                        useDefaultValue = useDefaultValues;
                    }
                }
                if (useDefaultValue) {
                    validatedSettings.put(key, desc.getDefaultValue());
                }
            }
        }
        return validatedSettings;
    }

    public static Set<String> upgrade(int version, Map<Integer, SettingsUpgrader> upgraders, Map<String, TreeMap<Integer, SettingsDescription>> settings, Map<String, Object> validatedSettings) {
        Map<String, Object> upgradedSettings = validatedSettings;
        Set<String> deletedSettings = null;
        for (int toVersion = version + 1; toVersion <= 42; toVersion++) {
            SettingsUpgrader upgrader = upgraders.get(Integer.valueOf(toVersion));
            if (upgrader != null) {
                deletedSettings = upgrader.upgrade(upgradedSettings);
            }
            for (Map.Entry<String, TreeMap<Integer, SettingsDescription>> versions : settings.entrySet()) {
                String settingName = (String) versions.getKey();
                TreeMap<Integer, SettingsDescription> versionedSettings = versions.getValue();
                if (((Integer) versionedSettings.firstKey()).intValue() == toVersion && !upgradedSettings.containsKey(settingName)) {
                    SettingsDescription setting = (SettingsDescription) versionedSettings.get(Integer.valueOf(toVersion));
                    Object defaultValue = setting.getDefaultValue();
                    upgradedSettings.put(settingName, defaultValue);
                    if (K9.DEBUG) {
                        Log.v("k9", "Added new setting \"" + settingName + "\" with default value \"" + setting.toPrettyString(defaultValue) + "\"");
                    }
                }
                Integer highestVersion = (Integer) versionedSettings.lastKey();
                if (highestVersion.intValue() == toVersion && versionedSettings.get(highestVersion) == null) {
                    upgradedSettings.remove(settingName);
                    if (deletedSettings == null) {
                        deletedSettings = new HashSet<>();
                    }
                    deletedSettings.add(settingName);
                    if (K9.DEBUG) {
                        Log.v("k9", "Removed setting \"" + settingName + "\"");
                    }
                }
            }
        }
        return deletedSettings;
    }

    public static Map<String, String> convert(Map<String, Object> settings, Map<String, TreeMap<Integer, SettingsDescription>> settingDescriptions) {
        Map<String, String> serializedSettings = new HashMap<>();
        for (Map.Entry<String, Object> setting : settings.entrySet()) {
            String settingName = (String) setting.getKey();
            Object internalValue = setting.getValue();
            TreeMap<Integer, SettingsDescription> versionedSetting = settingDescriptions.get(settingName);
            SettingsDescription settingDesc = (SettingsDescription) versionedSetting.get((Integer) versionedSetting.lastKey());
            if (settingDesc != null) {
                serializedSettings.put(settingName, settingDesc.toString(internalValue));
            } else if (K9.DEBUG) {
                Log.w("k9", "Settings.serialize() called with a setting that should have been removed: " + settingName);
            }
        }
        return serializedSettings;
    }

    public static TreeMap<Integer, SettingsDescription> versions(V... versionDescriptions) {
        TreeMap<Integer, SettingsDescription> map = new TreeMap<>();
        for (V v : versionDescriptions) {
            map.put(v.version, v.description);
        }
        return map;
    }

    public static abstract class SettingsDescription {
        protected Object mDefaultValue;

        public abstract Object fromString(String str) throws InvalidSettingValueException;

        public SettingsDescription(Object defaultValue) {
            this.mDefaultValue = defaultValue;
        }

        public Object getDefaultValue() {
            return this.mDefaultValue;
        }

        public String toString(Object value) {
            return value.toString();
        }

        public String toPrettyString(Object value) {
            return toString(value);
        }

        public Object fromPrettyString(String value) throws InvalidSettingValueException {
            return fromString(value);
        }
    }

    public static class V {
        public final SettingsDescription description;
        public final Integer version;

        public V(Integer version2, SettingsDescription description2) {
            this.version = version2;
            this.description = description2;
        }
    }

    public static class StringSetting extends SettingsDescription {
        public StringSetting(String defaultValue) {
            super(defaultValue);
        }

        public Object fromString(String value) {
            return value;
        }
    }

    public static class BooleanSetting extends SettingsDescription {
        public BooleanSetting(boolean defaultValue) {
            super(Boolean.valueOf(defaultValue));
        }

        public Object fromString(String value) throws InvalidSettingValueException {
            if (Boolean.TRUE.toString().equals(value)) {
                return true;
            }
            if (Boolean.FALSE.toString().equals(value)) {
                return false;
            }
            throw new InvalidSettingValueException();
        }
    }

    public static class ColorSetting extends SettingsDescription {
        public ColorSetting(int defaultValue) {
            super(Integer.valueOf(defaultValue));
        }

        public Object fromString(String value) throws InvalidSettingValueException {
            try {
                return Integer.valueOf(Integer.parseInt(value));
            } catch (NumberFormatException e) {
                throw new InvalidSettingValueException();
            }
        }

        public String toPrettyString(Object value) {
            return String.format("#%06x", Integer.valueOf(((Integer) value).intValue() & ViewCompat.MEASURED_SIZE_MASK));
        }

        public Object fromPrettyString(String value) throws InvalidSettingValueException {
            try {
                if (value.length() == 7) {
                    return Integer.valueOf(Integer.parseInt(value.substring(1), 16) | ViewCompat.MEASURED_STATE_MASK);
                }
            } catch (NumberFormatException e) {
            }
            throw new InvalidSettingValueException();
        }
    }

    public static class EnumSetting<T extends Enum<T>> extends SettingsDescription {
        private Class<T> mEnumClass;

        public EnumSetting(Class<T> enumClass, Object defaultValue) {
            super(defaultValue);
            this.mEnumClass = enumClass;
        }

        public Object fromString(String value) throws InvalidSettingValueException {
            try {
                return Enum.valueOf(this.mEnumClass, value);
            } catch (Exception e) {
                throw new InvalidSettingValueException();
            }
        }
    }

    public static abstract class PseudoEnumSetting<A> extends SettingsDescription {
        /* access modifiers changed from: protected */
        public abstract Map<A, String> getMapping();

        public PseudoEnumSetting(Object defaultValue) {
            super(defaultValue);
        }

        public String toPrettyString(Object value) {
            return (String) getMapping().get(value);
        }

        public Object fromPrettyString(String value) throws InvalidSettingValueException {
            for (Map.Entry<A, String> entry : getMapping().entrySet()) {
                if (entry.getValue().equals(value)) {
                    return entry.getKey();
                }
            }
            throw new InvalidSettingValueException();
        }
    }

    public static class FontSizeSetting extends PseudoEnumSetting<Integer> {
        private final Map<Integer, String> mMapping;

        public FontSizeSetting(int defaultValue) {
            super(Integer.valueOf(defaultValue));
            Map<Integer, String> mapping = new HashMap<>();
            mapping.put(10, "tiniest");
            mapping.put(12, "tiny");
            mapping.put(14, "smaller");
            mapping.put(16, "small");
            mapping.put(18, "medium");
            mapping.put(20, "large");
            mapping.put(22, "larger");
            this.mMapping = Collections.unmodifiableMap(mapping);
        }

        /* access modifiers changed from: protected */
        public Map<Integer, String> getMapping() {
            return this.mMapping;
        }

        public Object fromString(String value) throws InvalidSettingValueException {
            try {
                Integer fontSize = Integer.valueOf(Integer.parseInt(value));
                if (this.mMapping.containsKey(fontSize)) {
                    return fontSize;
                }
            } catch (NumberFormatException e) {
            }
            throw new InvalidSettingValueException();
        }
    }

    public static class WebFontSizeSetting extends PseudoEnumSetting<Integer> {
        private final Map<Integer, String> mMapping;

        public WebFontSizeSetting(int defaultValue) {
            super(Integer.valueOf(defaultValue));
            Map<Integer, String> mapping = new HashMap<>();
            mapping.put(1, "smallest");
            mapping.put(2, "smaller");
            mapping.put(3, "normal");
            mapping.put(4, "larger");
            mapping.put(5, "largest");
            this.mMapping = Collections.unmodifiableMap(mapping);
        }

        /* access modifiers changed from: protected */
        public Map<Integer, String> getMapping() {
            return this.mMapping;
        }

        public Object fromString(String value) throws InvalidSettingValueException {
            try {
                Integer fontSize = Integer.valueOf(Integer.parseInt(value));
                if (this.mMapping.containsKey(fontSize)) {
                    return fontSize;
                }
            } catch (NumberFormatException e) {
            }
            throw new InvalidSettingValueException();
        }
    }

    public static class IntegerRangeSetting extends SettingsDescription {
        private int mEnd;
        private int mStart;

        public IntegerRangeSetting(int start, int end, int defaultValue) {
            super(Integer.valueOf(defaultValue));
            this.mStart = start;
            this.mEnd = end;
        }

        public Object fromString(String value) throws InvalidSettingValueException {
            try {
                int intValue = Integer.parseInt(value);
                if (this.mStart <= intValue && intValue <= this.mEnd) {
                    return Integer.valueOf(intValue);
                }
            } catch (NumberFormatException e) {
            }
            throw new InvalidSettingValueException();
        }
    }
}
