package com.fsck.k9.mail.internet;

import android.util.Log;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.Part;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.util.Locale;
import org.apache.commons.io.IOUtils;

public class CharsetSupport {
    private static final String[][] CHARSET_FALLBACK_MAP = {new String[]{"koi8-u", "koi8-r"}, new String[]{"iso-2022-jp-[\\d]+", "iso-2022-jp"}, new String[]{".*", "US-ASCII"}};

    public static void setCharset(String charset, Part part) throws MessagingException {
        part.setHeader("Content-Type", part.getMimeType() + ";\r\n charset=" + getExternalCharset(charset));
    }

    public static String getCharsetFromAddress(String address) {
        String variant = JisSupport.getJisVariantFromAddress(address);
        if (variant != null) {
            String charset = "x-" + variant + "-shift_jis-2007";
            if (Charset.isSupported(charset)) {
                return charset;
            }
        }
        return "UTF-8";
    }

    static String getExternalCharset(String charset) {
        if (JisSupport.isShiftJis(charset)) {
            return JisSupport.SHIFT_JIS;
        }
        return charset;
    }

    static String fixupCharset(String charset, Message message) throws MessagingException {
        String variant;
        if (charset == null || "0".equals(charset)) {
            charset = "US-ASCII";
        }
        String charset2 = charset.toLowerCase(Locale.US);
        if (charset2.equals("cp932")) {
            charset2 = JisSupport.SHIFT_JIS;
        }
        if ((charset2.equals(JisSupport.SHIFT_JIS) || charset2.equals("iso-2022-jp")) && (variant = JisSupport.getJisVariantFromMessage(message)) != null) {
            return "x-" + variant + "-" + charset2 + "-2007";
        }
        return charset2;
    }

    static String readToString(InputStream in, String charset) throws IOException {
        boolean supported;
        boolean isIphoneString = false;
        if (charset.length() > 19 && charset.startsWith("x-") && charset.endsWith("-iso-2022-jp-2007") && !Charset.isSupported(charset)) {
            charset = "x-" + charset.substring(2, charset.length() - 17) + "-shift_jis-2007";
            in = new Iso2022JpToShiftJisInputStream(in);
        }
        if (JisSupport.isShiftJis(charset) && !Charset.isSupported(charset)) {
            if (charset.substring(2, charset.length() - 15).equals("iphone")) {
                isIphoneString = true;
            }
            charset = JisSupport.SHIFT_JIS;
        }
        try {
            supported = Charset.isSupported(charset);
        } catch (IllegalCharsetNameException e) {
            supported = false;
        }
        for (String[] rule : CHARSET_FALLBACK_MAP) {
            if (supported) {
                break;
            }
            if (charset.matches(rule[0])) {
                Log.e("k9", "I don't know how to deal with the charset " + charset + ". Falling back to " + rule[1]);
                charset = rule[1];
                try {
                    supported = Charset.isSupported(charset);
                } catch (IllegalCharsetNameException e2) {
                    supported = false;
                }
            }
        }
        String str = IOUtils.toString(in, charset);
        if (isIphoneString) {
            return importStringFromIphone(str);
        }
        return str;
    }

    private static String importStringFromIphone(String str) {
        StringBuilder buff = new StringBuilder(str.length());
        int i = 0;
        while (i < str.length()) {
            buff.appendCodePoint(importCodePointFromIphone(str.codePointAt(i)));
            i = str.offsetByCodePoints(i, 1);
        }
        return buff.toString();
    }

    private static int importCodePointFromIphone(int codePoint) {
        switch (codePoint) {
            case 57345:
                return 1040795;
            case 57346:
                return 1040796;
            case 57347:
                return 1042467;
            case 57348:
                return 1040797;
            case 57349:
                return 1040798;
            case 57350:
                return 1041615;
            case 57351:
                return 1041613;
            case 57352:
                return 1041647;
            case 57353:
                return 1041699;
            case 57354:
                return 1041701;
            case 57355:
                return 1041704;
            case 57356:
                return 1041720;
            case 57357:
                return 1043350;
            case 57358:
                return 1043351;
            case 57359:
                return 1043352;
            case 57360:
                return 1043347;
            case 57361:
                return 1043348;
            case 57362:
                return 1043349;
            case 57363:
                return 1042389;
            case 57364:
                return 1042386;
            case 57365:
                return 1042387;
            case 57366:
                return 1042385;
            case 57367:
                return 1042394;
            case 57368:
                return 1042388;
            case 57369:
                return 1040829;
            case 57370:
                return 1040830;
            case 57371:
                return 1042404;
            case 57372:
                return 1042410;
            case 57373:
                return 1042409;
            case 57374:
                return 1042399;
            case 57375:
                return 1042403;
            case 57376:
                return 1043209;
            case 57377:
                return 1043204;
            case 57378:
                return 1043212;
            case 57379:
                return 1043214;
            case 57380:
                return 1040414;
            case 57381:
                return 1040415;
            case 57382:
                return 1040416;
            case 57383:
                return 1040417;
            case 57384:
                return 1040418;
            case 57385:
                return 1040419;
            case 57386:
                return 1040420;
            case 57387:
                return 1040421;
            case 57388:
                return 1040422;
            case 57389:
                return 1040423;
            case 57390:
                return 1040424;
            case 57391:
                return 1040425;
            case 57392:
                return 1040448;
            case 57393:
                return 1041618;
            case 57394:
                return 1040449;
            case 57395:
                return 1041682;
            case 57396:
                return 1042469;
            case 57397:
                return 1042470;
            case 57398:
                return 1041584;
            case 57399:
                return 1041595;
            case 57400:
                return 1041586;
            case 57401:
                return 1042412;
            case 57402:
                return 1042421;
            case 57403:
                return 1041603;
            case 57404:
                return 1042432;
            case 57405:
                return 1042433;
            case 57406:
                return 1042451;
            case 57407:
                return 1043330;
            case 57408:
                return 1042453;
            case 57409:
                return 1042454;
            case 57410:
                return 1042456;
            case 57411:
                return 1042816;
            case 57412:
                return 1042818;
            case 57413:
                return 1042817;
            case 57414:
                return 1042786;
            case 57415:
                return 1042819;
            case 57416:
                return 1040387;
            case 57417:
                return 1040385;
            case 57418:
                return 1040384;
            case 57419:
                return 1040386;
            case 57420:
                return 1040404;
            case 57421:
                return 1040393;
            case 57422:
                return 1040815;
            case 57423:
                return 1040824;
            case 57424:
                return 1040832;
            case 57425:
                return 1040833;
            case 57426:
                return 1040823;
            case 57427:
                return 1040834;
            case 57428:
                return 1040835;
            case 57429:
                return 1040828;
            case 57430:
                return 1041205;
            case 57431:
                return 1041200;
            case 57432:
                return 1041187;
            case 57433:
                return 1041184;
            case 57434:
                return 1041652;
            case 57601:
                return 1041709;
            case 57602:
                return 1041710;
            case 57603:
                return 1041707;
            case 57604:
                return 1041702;
            case 57605:
                return 1041193;
            case 57606:
                return 1041191;
            case 57607:
                return 1041217;
            case 57608:
                return 1041220;
            case 57609:
                return 1040836;
            case 57610:
                return 1040837;
            case 57611:
                return 1040831;
            case 57612:
                return 1040816;
            case 57613:
                return 1042413;
            case 57614:
                return 1041617;
            case 57615:
                return 1043286;
            case 57616:
                return 1040444;
            case 57617:
                return 1042471;
            case 57618:
                return 1041680;
            case 57619:
                return 1041653;
            case 57620:
                return 1043333;
            case 57621:
                return 1042393;
            case 57622:
                return 1041610;
            case 57623:
                return 1041685;
            case 57624:
                return 1040447;
            case 57625:
                return 1040450;
            case 57626:
                return 1040818;
            case 57627:
                return 1040814;
            case 57628:
                return 1040819;
            case 57629:
                return 1041654;
            case 57630:
                return 1041723;
            case 57631:
                return 1041719;
            case 57632:
                return 1042784;
            case 57633:
                return 1041596;
            case 57634:
                return 1042427;
            case 57635:
                return 1042426;
            case 57636:
                return 1042429;
            case 57637:
                return 1042439;
            case 57638:
                return 1042461;
            case 57639:
                return 1042462;
            case 57640:
                return 1042463;
            case 57641:
                return 1042464;
            case 57642:
                return 1042460;
            case 57643:
                return 1040817;
            case 57644:
                return 1042459;
            case 57645:
                return 1042443;
            case 57646:
                return 1043250;
            case 57647:
                return 1041629;
            case 57648:
                return 1042444;
            case 57649:
                return 1042395;
            case 57650:
                return 1042391;
            case 57651:
                return 1042445;
            case 57652:
                return 1042396;
            case 57653:
                return 1042414;
            case 57654:
                return 1042411;
            case 57655:
                return 1042424;
            case 57656:
                return 1043251;
            case 57657:
                return 1043252;
            case 57658:
                return 1043253;
            case 57659:
                return 1041673;
            case 57660:
                return 1043289;
            case 57661:
                return 1040388;
            case 57662:
                return 1041622;
            case 57663:
                return 1041669;
            case 57664:
                return 1041671;
            case 57665:
                return 1042465;
            case 57666:
                return 1041711;
            case 57667:
                return 1041684;
            case 57668:
                return 1043334;
            case 57669:
                return 1043335;
            case 57670:
                return 1040395;
            case 57671:
                return 1042789;
            case 57672:
                return 1041734;
            case 57673:
                return 1041630;
            case 57674:
                return 1041631;
            case 57675:
                return 1041713;
            case 57676:
                return 1043294;
            case 57677:
                return 1041589;
            case 57678:
                return 1042423;
            case 57679:
                return 1042422;
            case 57680:
                return 1042407;
            case 57681:
                return 1041670;
            case 57682:
                return 1040801;
            case 57683:
                return 1041587;
            case 57684:
                return 1041590;
            case 57685:
                return 1041588;
            case 57686:
                return 1041593;
            case 57687:
                return 1041594;
            case 57688:
                return 1041591;
            case 57689:
                return 1042406;
            case 57690:
                return 1042415;
            case 57857:
                return 1042416;
            case 57858:
                return 1042408;
            case 57859:
                return 1043236;
            case 57860:
                return 1043225;
            case 57861:
                return 1043297;
            case 57862:
                return 1043298;
            case 57863:
                return 1043237;
            case 57864:
                return 1043231;
            case 57865:
                return 1040452;
            case 57866:
                return 1043232;
            case 57867:
                return 1042488;
            case 57868:
                return 1043226;
            case 57869:
                return 1043228;
            case 57870:
                return 1043227;
            case 57871:
                return 1043229;
            case 57872:
                return 1042476;
            case 57873:
                return 1042475;
            case 57874:
                return 1043254;
            case 57875:
                return 1043255;
            case 57876:
                return 1043256;
            case 57877:
                return 1043257;
            case 57878:
                return 1043258;
            case 57879:
                return 1043259;
            case 57880:
                return 1043260;
            case 57881:
                return 1043299;
            case 57882:
                return 1043300;
            case 57883:
                return 1043303;
            case 57884:
                return 1042478;
            case 57885:
                return 1042479;
            case 57886:
                return 1042480;
            case 57887:
                return 1042481;
            case 57888:
                return 1042482;
            case 57889:
                return 1042483;
            case 57890:
                return 1042484;
            case 57891:
                return 1042485;
            case 57892:
                return 1042486;
            case 57893:
                return 1042487;
            case 57894:
                return 1043261;
            case 57895:
                return 1043262;
            case 57896:
                return 1043263;
            case 57897:
                return 1043329;
            case 57898:
                return 1043249;
            case 57899:
                return 1043247;
            case 57900:
                return 1043264;
            case 57901:
                return 1043265;
            case 57902:
                return 1043353;
            case 57903:
                return 1043354;
            case 57904:
                return 1043355;
            case 57905:
                return 1043356;
            case 57906:
                return 1043192;
            case 57907:
                return 1043193;
            case 57908:
                return 1043194;
            case 57909:
                return 1043195;
            case 57910:
                return 1043184;
            case 57911:
                return 1043186;
            case 57912:
                return 1043185;
            case 57913:
                return 1043187;
            case 57914:
                return 1043196;
            case 57915:
                return 1043197;
            case 57916:
                return 1043198;
            case 57917:
                return 1043199;
            case 57918:
                return 1041656;
            case 57919:
                return 1040427;
            case 57920:
                return 1040428;
            case 57921:
                return 1040429;
            case 57922:
                return 1040430;
            case 57923:
                return 1040431;
            case 57924:
                return 1040432;
            case 57925:
                return 1040433;
            case 57926:
                return 1040434;
            case 57927:
                return 1040435;
            case 57928:
                return 1040436;
            case 57929:
                return 1040437;
            case 57930:
                return 1040438;
            case 57931:
                return 1040439;
            case 57932:
                return 1043266;
            case 57933:
                return 1043239;
            case 57934:
                return 1043241;
            case 57935:
                return 1043245;
            case 57936:
                return 1042489;
            case 57937:
                return 1042490;
            case 57938:
                return 1043235;
            case 57939:
                return 1040820;
            case 57940:
                return 1044087;
            case 57941:
                return 1044088;
            case 57942:
                return 1044089;
            case 57943:
                return 1044090;
            case 57944:
                return 1044091;
            case 57945:
                return 1044092;
            case 57946:
                return 1044093;
            case 58113:
                return 1041703;
            case 58114:
                return 1041619;
            case 58115:
                return 1040453;
            case 58116:
                return 1040445;
            case 58117:
                return 1040454;
            case 58118:
                return 1042472;
            case 58119:
                return 1040455;
            case 58120:
                return 1040456;
            case 58121:
                return 1041672;
            case 58122:
                return 1042435;
            case 58123:
                return 1042821;
            case 58124:
                return 1042823;
            case 58125:
                return 1043267;
            case 58126:
                return 1043230;
            case 58127:
                return 1041674;
            case 58128:
                return 1041686;
            case 58129:
                return 1043288;
            case 58130:
                return 1041687;
            case 58131:
                return 1041726;
            case 58132:
                return 1041679;
            case 58133:
                return 1043243;
            case 58134:
                return 1041724;
            case 58135:
                return 1041712;
            case 58136:
                return 1041620;
            case 58137:
                return 1041621;
            case 58138:
                return 1041623;
            case 58139:
                return 1041624;
            case 58140:
                return 1040789;
            case 58141:
                return 1040790;
            case 58142:
                return 1040791;
            case 58143:
                return 1040792;
            case 58144:
                return 1040793;
            case 58145:
                return 1041625;
            case 58146:
                return 1041626;
            case 58147:
                return 1041648;
            case 58148:
                return 1042440;
            case 58149:
                return 1041650;
            case 58150:
                return 1042452;
            case 58151:
                return 1043213;
            case 58152:
                return 1043217;
            case 58153:
                return 1043218;
            case 58154:
                return 1043219;
            case 58155:
                return 1043220;
            case 58156:
                return 1043221;
            case 58157:
                return 1043222;
            case 58158:
                return 1043296;
            case 58159:
                return 1043304;
            case 58160:
                return 1043293;
            case 58161:
                return 1043291;
            case 58162:
                return 1043268;
            case 58163:
                return 1043269;
            case 58164:
                return 1043287;
            case 58165:
                return 1043305;
            case 58166:
                return 1043210;
            case 58167:
                return 1043211;
            case 58168:
                return 1042820;
            case 58169:
                return 1042788;
            case 58170:
                return 1042790;
            case 58171:
                return 1042791;
            case 58172:
                return 1042792;
            case 58173:
                return 1042793;
            case 58174:
                return 1042794;
            case 58175:
                return 1042795;
            case 58176:
                return 1042787;
            case 58177:
                return 1042796;
            case 58178:
                return 1042785;
            case 58179:
                return 1042797;
            case 58180:
                return 1042798;
            case 58181:
                return 1040465;
            case 58182:
                return 1040466;
            case 58183:
                return 1040467;
            case 58184:
                return 1040468;
            case 58185:
                return 1040469;
            case 58186:
                return 1040470;
            case 58187:
                return 1041681;
            case 58188:
                return 1042799;
            case 58189:
                return 1042800;
            case 58369:
                return 1041221;
            case 58370:
                return 1041219;
            case 58371:
                return 1041216;
            case 58372:
                return 1041203;
            case 58373:
                return 1041223;
            case 58374:
                return 1041212;
            case 58375:
                return 1041215;
            case 58376:
                return 1041218;
            case 58377:
                return 1041194;
            case 58378:
                return 1041214;
            case 58379:
                return 1041211;
            case 58380:
                return 1041198;
            case 58381:
                return 1041199;
            case 58382:
                return 1041190;
            case 58383:
                return 1041189;
            case 58384:
                return 1041186;
            case 58385:
                return 1041210;
            case 58386:
                return 1041204;
            case 58387:
                return 1041209;
            case 58388:
                return 1041206;
            case 58389:
                return 1041208;
            case 58390:
                return 1041213;
            case 58391:
                return 1041197;
            case 58392:
                return 1041196;
            case 58393:
                return 1040784;
            case 58394:
                return 1040786;
            case 58395:
                return 1040785;
            case 58396:
                return 1040787;
            case 58397:
                return 1041243;
            case 58398:
                return 1043357;
            case 58399:
                return 1043358;
            case 58400:
                return 1043359;
            case 58401:
                return 1043360;
            case 58402:
                return 1043361;
            case 58403:
                return 1041233;
            case 58404:
                return 1041234;
            case 58405:
                return 1042473;
            case 58406:
                return 1041235;
            case 58407:
                return 1041240;
            case 58408:
                return 1040800;
            case 58409:
                return 1040802;
            case 58410:
                return 1042390;
            case 58411:
                return 1042397;
            case 58412:
                return 1042446;
            case 58413:
                return 1042398;
            case 58414:
                return 1042405;
            case 58415:
                return 1042417;
            case 58416:
                return 1042418;
            case 58417:
                return 1042419;
            case 58418:
                return 1042420;
            case 58419:
                return 1042430;
            case 58420:
                return 1042400;
            case 58421:
                return 1042402;
            case 58422:
                return 1041688;
            case 58423:
                return 1043223;
            case 58424:
                return 1041689;
            case 58425:
                return 1041690;
            case 58426:
                return 1041691;
            case 58427:
                return 1041692;
            case 58428:
                return 1040391;
            case 58429:
                return 1042474;
            case 58430:
                return 1040440;
            case 58431:
                return 1042801;
            case 58432:
                return 1041693;
            case 58433:
                return 1040838;
            case 58434:
                return 1041694;
            case 58435:
                return 1040389;
            case 58436:
                return 1040457;
            case 58437:
                return 1041695;
            case 58438:
                return 1040407;
            case 58439:
                return 1040451;
            case 58440:
                return 1041683;
            case 58441:
                return 1040394;
            case 58442:
                return 1040396;
            case 58443:
                return 1040392;
            case 58444:
                return 1040397;
            case 58625:
                return 1041592;
            case 58626:
                return 1042436;
            case 58627:
                return 1042437;
            case 58628:
                return 1041597;
            case 58629:
                return 1041598;
            case 58630:
                return 1041599;
            case 58631:
                return 1042434;
            case 58632:
                return 1041600;
            case 58633:
                return 1041604;
            case 58634:
                return 1041605;
            case 58635:
                return 1041637;
            case 58636:
                return 1041638;
            case 58637:
                return 1041639;
            case 58638:
                return 1041640;
            case 58639:
                return 1041641;
            case 58640:
                return 1041642;
            case 58641:
                return 1041643;
            case 58642:
                return 1041644;
            case 58643:
                return 1041645;
            case 58644:
                return 1041646;
            case 58645:
                return 1040804;
            case 58646:
                return 1040805;
            case 58647:
                return 1040806;
            case 58648:
                return 1040807;
            case 58649:
                return 1040808;
            case 58650:
                return 1040809;
            case 58651:
                return 1040810;
            case 58652:
                return 1040811;
            case 58653:
                return 1041606;
            case 58654:
                return 1040821;
            case 58655:
                return 1040822;
            case 58656:
                return 1040839;
            case 58657:
                return 1040840;
            case 58658:
                return 1040841;
            case 58659:
                return 1040826;
            case 58660:
                return 1040842;
            case 58661:
                return 1040843;
            case 58662:
                return 1040844;
            case 58663:
                return 1040845;
            case 58664:
                return 1040846;
            case 58665:
                return 1040847;
            case 58666:
                return 1040848;
            case 58667:
                return 1040849;
            case 58668:
                return 1040850;
            case 58669:
                return 1040851;
            case 58670:
                return 1040852;
            case 58671:
                return 1040853;
            case 58672:
                return 1040854;
            case 58673:
                return 1040855;
            case 58674:
                return 1041675;
            case 58675:
                return 1041676;
            case 58676:
                return 1041677;
            case 58677:
                return 1041678;
            case 58678:
                return 1041747;
            case 58679:
                return 1043242;
            case 58680:
                return 1044080;
            case 58681:
                return 1044081;
            case 58682:
                return 1044082;
            case 58683:
                return 1044083;
            case 58684:
                return 1044084;
            case 58685:
                return 1044085;
            case 58686:
                return 1044086;
            default:
                return codePoint;
        }
    }
}
