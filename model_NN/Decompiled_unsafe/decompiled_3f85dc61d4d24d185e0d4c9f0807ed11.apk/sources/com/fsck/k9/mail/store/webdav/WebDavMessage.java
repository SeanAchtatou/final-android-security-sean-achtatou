package com.fsck.k9.mail.store.webdav;

import android.util.Log;
import com.fsck.k9.mail.Flag;
import com.fsck.k9.mail.Folder;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.helper.UrlEncodingHelper;
import com.fsck.k9.mail.internet.MimeMessage;
import java.util.Collections;
import java.util.Locale;
import java.util.Map;
import org.apache.james.mime4j.dom.field.FieldName;

class WebDavMessage extends MimeMessage {
    private String mUrl = "";

    WebDavMessage(String uid, Folder folder) {
        this.mUid = uid;
        this.mFolder = folder;
    }

    public void setUrl(String url) {
        if (!url.toLowerCase(Locale.US).contains("http")) {
            if (!url.startsWith("/")) {
                url = "/" + url;
            }
            url = ((WebDavFolder) this.mFolder).getUrl() + url;
        }
        String[] urlParts = url.split("/");
        int length = urlParts.length;
        String end = urlParts[length - 1];
        this.mUrl = "";
        String url2 = "";
        try {
            end = UrlEncodingHelper.encodeUtf8(UrlEncodingHelper.decodeUtf8(end)).replaceAll("\\+", "%20");
        } catch (IllegalArgumentException iae) {
            Log.e("k9", "IllegalArgumentException caught in setUrl: " + iae + "\nTrace: " + WebDavUtils.processException(iae));
        }
        for (int i = 0; i < length - 1; i++) {
            if (i != 0) {
                url2 = url2 + "/" + urlParts[i];
            } else {
                url2 = urlParts[i];
            }
        }
        this.mUrl = url2 + "/" + end;
    }

    public String getUrl() {
        return this.mUrl;
    }

    public void setSize(int size) {
        this.mSize = size;
    }

    public void setFlagInternal(Flag flag, boolean set) throws MessagingException {
        super.setFlag(flag, set);
    }

    public void setNewHeaders(ParsedMessageEnvelope envelope) throws MessagingException {
        String[] headers = envelope.getHeaderList();
        Map<String, String> messageHeaders = envelope.getMessageHeaders();
        for (String header : headers) {
            String headerValue = messageHeaders.get(header);
            if (header.equals(FieldName.CONTENT_LENGTH)) {
                setSize(Integer.parseInt(headerValue));
            }
            if (headerValue != null && !headerValue.equals("")) {
                addHeader(header, headerValue);
            }
        }
    }

    public void delete(String trashFolderName) throws MessagingException {
        WebDavFolder wdFolder = (WebDavFolder) getFolder();
        Log.i("k9", "Deleting message by moving to " + trashFolderName);
        wdFolder.moveMessages(Collections.singletonList(this), wdFolder.getStore().getFolder(trashFolderName));
    }

    public void setFlag(Flag flag, boolean set) throws MessagingException {
        super.setFlag(flag, set);
        this.mFolder.setFlags(Collections.singletonList(this), Collections.singleton(flag), set);
    }
}
