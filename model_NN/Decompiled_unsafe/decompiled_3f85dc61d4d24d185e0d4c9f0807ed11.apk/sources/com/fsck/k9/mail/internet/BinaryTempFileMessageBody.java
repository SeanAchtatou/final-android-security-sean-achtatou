package com.fsck.k9.mail.internet;

import com.fsck.k9.mail.Body;
import com.fsck.k9.mail.MessagingException;
import org.apache.james.mime4j.util.MimeUtil;

public class BinaryTempFileMessageBody extends BinaryTempFileBody implements Body {
    public BinaryTempFileMessageBody(String encoding) {
        super(encoding);
    }

    public void setEncoding(String encoding) throws MessagingException {
        if (MimeUtil.ENC_7BIT.equalsIgnoreCase(encoding) || MimeUtil.ENC_8BIT.equalsIgnoreCase(encoding)) {
            this.mEncoding = encoding;
            return;
        }
        throw new MessagingException("Incompatible content-transfer-encoding for a message/rfc822 body");
    }
}
