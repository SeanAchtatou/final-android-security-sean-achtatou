package com.fsck.k9.mail.transport;

import android.util.Log;
import com.fsck.k9.Account;
import com.fsck.k9.mail.Address;
import com.fsck.k9.mail.AuthType;
import com.fsck.k9.mail.Authentication;
import com.fsck.k9.mail.AuthenticationFailedException;
import com.fsck.k9.mail.ConnectionSecurity;
import com.fsck.k9.mail.K9MailLib;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.ServerSettings;
import com.fsck.k9.mail.Transport;
import com.fsck.k9.mail.filter.Base64;
import com.fsck.k9.mail.filter.EOLConvertingOutputStream;
import com.fsck.k9.mail.filter.LineWrapOutputStream;
import com.fsck.k9.mail.filter.PeekableInputStream;
import com.fsck.k9.mail.filter.SmtpDataStuffing;
import com.fsck.k9.mail.internet.CharsetSupport;
import com.fsck.k9.mail.oauth.OAuth2TokenProvider;
import com.fsck.k9.mail.ssl.TrustedSocketFactory;
import com.fsck.k9.mail.store.StoreConfig;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class SmtpTransport extends Transport {
    public static final int SMTP_AUTHENTICATION_FAILURE_ERROR_CODE = 535;
    private boolean m8bitEncodingAllowed;
    private AuthType mAuthType;
    private String mClientCertificateAlias;
    private ConnectionSecurity mConnectionSecurity;
    private String mHost;
    private PeekableInputStream mIn;
    private int mLargestAcceptableMessage;
    private OutputStream mOut;
    private String mPassword;
    private int mPort;
    private Socket mSocket;
    private TrustedSocketFactory mTrustedSocketFactory;
    private String mUsername;
    private OAuth2TokenProvider oauthTokenProvider;

    public static ServerSettings decodeUri(String uri) {
        ConnectionSecurity connectionSecurity;
        int port;
        AuthType authType = null;
        String username = null;
        String password = null;
        String clientCertificateAlias = null;
        try {
            URI smtpUri = new URI(uri);
            String scheme = smtpUri.getScheme();
            if (scheme.equals("smtp")) {
                connectionSecurity = ConnectionSecurity.NONE;
                port = ServerSettings.Type.SMTP.defaultPort;
            } else if (scheme.startsWith("smtp+tls")) {
                connectionSecurity = ConnectionSecurity.STARTTLS_REQUIRED;
                port = ServerSettings.Type.SMTP.defaultPort;
            } else if (scheme.startsWith("smtp+ssl")) {
                connectionSecurity = ConnectionSecurity.SSL_TLS_REQUIRED;
                port = ServerSettings.Type.SMTP.defaultTlsPort;
            } else {
                throw new IllegalArgumentException("Unsupported protocol (" + scheme + ")");
            }
            String host = smtpUri.getHost();
            if (smtpUri.getPort() != -1) {
                port = smtpUri.getPort();
            }
            if (smtpUri.getUserInfo() != null) {
                String[] userInfoParts = smtpUri.getUserInfo().split(":");
                if (userInfoParts.length == 1) {
                    authType = AuthType.PLAIN;
                    username = decodeUtf8(userInfoParts[0]);
                } else if (userInfoParts.length == 2) {
                    authType = AuthType.PLAIN;
                    username = decodeUtf8(userInfoParts[0]);
                    password = decodeUtf8(userInfoParts[1]);
                } else if (userInfoParts.length == 3) {
                    authType = AuthType.valueOf(userInfoParts[2]);
                    username = decodeUtf8(userInfoParts[0]);
                    if (authType == AuthType.EXTERNAL) {
                        clientCertificateAlias = decodeUtf8(userInfoParts[1]);
                    } else {
                        password = decodeUtf8(userInfoParts[1]);
                    }
                }
            }
            return new ServerSettings(ServerSettings.Type.SMTP, host, port, connectionSecurity, authType, username, password, clientCertificateAlias);
        } catch (URISyntaxException use) {
            throw new IllegalArgumentException("Invalid SmtpTransport URI", use);
        }
    }

    public static String createUri(ServerSettings server) {
        String scheme;
        String userInfo;
        String userEnc = server.username != null ? encodeUtf8(server.username) : "";
        String passwordEnc = server.password != null ? encodeUtf8(server.password) : "";
        String clientCertificateAliasEnc = server.clientCertificateAlias != null ? encodeUtf8(server.clientCertificateAlias) : "";
        switch (server.connectionSecurity) {
            case SSL_TLS_REQUIRED:
                scheme = "smtp+ssl+";
                break;
            case STARTTLS_REQUIRED:
                scheme = "smtp+tls+";
                break;
            default:
                scheme = "smtp";
                break;
        }
        AuthType authType = server.authenticationType;
        if (authType == null) {
            userInfo = userEnc + ":" + passwordEnc;
        } else if (AuthType.EXTERNAL == authType) {
            userInfo = userEnc + ":" + clientCertificateAliasEnc + ":" + authType.name();
        } else {
            userInfo = userEnc + ":" + passwordEnc + ":" + authType.name();
        }
        try {
            return new URI(scheme, userInfo, server.host, server.port, null, null, null).toString();
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException("Can't create SmtpTransport URI", e);
        }
    }

    public SmtpTransport(StoreConfig storeConfig, TrustedSocketFactory trustedSocketFactory, OAuth2TokenProvider oauth2TokenProvider) throws MessagingException {
        try {
            ServerSettings settings = decodeUri(storeConfig.getTransportUri());
            this.mHost = settings.host;
            this.mPort = settings.port;
            this.mConnectionSecurity = settings.connectionSecurity;
            this.mAuthType = settings.authenticationType;
            this.mUsername = settings.username;
            this.mPassword = settings.password;
            this.mClientCertificateAlias = settings.clientCertificateAlias;
            this.mTrustedSocketFactory = trustedSocketFactory;
            this.oauthTokenProvider = oauth2TokenProvider;
        } catch (IllegalArgumentException e) {
            throw new MessagingException("Error while decoding transport URI", e);
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void open() throws com.fsck.k9.mail.MessagingException {
        /*
            r25 = this;
            r18 = 0
            r0 = r25
            java.lang.String r0 = r0.mHost     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r20 = r0
            java.net.InetAddress[] r3 = java.net.InetAddress.getAllByName(r20)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r12 = 0
        L_0x000d:
            int r0 = r3.length     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r20 = r0
            r0 = r20
            if (r12 >= r0) goto L_0x0066
            java.net.InetSocketAddress r19 = new java.net.InetSocketAddress     // Catch:{ SocketException -> 0x027b }
            r20 = r3[r12]     // Catch:{ SocketException -> 0x027b }
            r0 = r25
            int r0 = r0.mPort     // Catch:{ SocketException -> 0x027b }
            r21 = r0
            r19.<init>(r20, r21)     // Catch:{ SocketException -> 0x027b }
            r0 = r25
            com.fsck.k9.mail.ConnectionSecurity r0 = r0.mConnectionSecurity     // Catch:{ SocketException -> 0x027b }
            r20 = r0
            com.fsck.k9.mail.ConnectionSecurity r21 = com.fsck.k9.mail.ConnectionSecurity.SSL_TLS_REQUIRED     // Catch:{ SocketException -> 0x027b }
            r0 = r20
            r1 = r21
            if (r0 != r1) goto L_0x025d
            r0 = r25
            com.fsck.k9.mail.ssl.TrustedSocketFactory r0 = r0.mTrustedSocketFactory     // Catch:{ SocketException -> 0x027b }
            r20 = r0
            r21 = 0
            r0 = r25
            java.lang.String r0 = r0.mHost     // Catch:{ SocketException -> 0x027b }
            r22 = r0
            r0 = r25
            int r0 = r0.mPort     // Catch:{ SocketException -> 0x027b }
            r23 = r0
            r0 = r25
            java.lang.String r0 = r0.mClientCertificateAlias     // Catch:{ SocketException -> 0x027b }
            r24 = r0
            java.net.Socket r20 = r20.createSocket(r21, r22, r23, r24)     // Catch:{ SocketException -> 0x027b }
            r0 = r20
            r1 = r25
            r1.mSocket = r0     // Catch:{ SocketException -> 0x027b }
            r0 = r25
            java.net.Socket r0 = r0.mSocket     // Catch:{ SocketException -> 0x027b }
            r20 = r0
            r21 = 10000(0x2710, float:1.4013E-41)
            r0 = r20
            r1 = r19
            r2 = r21
            r0.connect(r1, r2)     // Catch:{ SocketException -> 0x027b }
            r18 = 1
        L_0x0066:
            r0 = r25
            java.net.Socket r0 = r0.mSocket     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r20 = r0
            r21 = 300000(0x493e0, float:4.2039E-40)
            r20.setSoTimeout(r21)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            com.fsck.k9.mail.filter.PeekableInputStream r20 = new com.fsck.k9.mail.filter.PeekableInputStream     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            java.io.BufferedInputStream r21 = new java.io.BufferedInputStream     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r0 = r25
            java.net.Socket r0 = r0.mSocket     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r22 = r0
            java.io.InputStream r22 = r22.getInputStream()     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r23 = 1024(0x400, float:1.435E-42)
            r21.<init>(r22, r23)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r20.<init>(r21)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r0 = r20
            r1 = r25
            r1.mIn = r0     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            java.io.BufferedOutputStream r20 = new java.io.BufferedOutputStream     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r0 = r25
            java.net.Socket r0 = r0.mSocket     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r21 = r0
            java.io.OutputStream r21 = r21.getOutputStream()     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r22 = 1024(0x400, float:1.435E-42)
            r20.<init>(r21, r22)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r0 = r20
            r1 = r25
            r1.mOut = r0     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r20 = 0
            r0 = r25
            r1 = r20
            r0.executeSimpleCommand(r1)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r0 = r25
            java.net.Socket r0 = r0.mSocket     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r20 = r0
            java.net.InetAddress r15 = r20.getLocalAddress()     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            java.lang.String r16 = r15.getCanonicalHostName()     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            java.lang.String r14 = r15.getHostAddress()     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            java.lang.String r20 = ""
            r0 = r16
            r1 = r20
            boolean r20 = r0.equals(r1)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            if (r20 != 0) goto L_0x00e0
            r0 = r16
            boolean r20 = r0.equals(r14)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            if (r20 != 0) goto L_0x00e0
            java.lang.String r20 = "_"
            r0 = r16
            r1 = r20
            boolean r20 = r0.contains(r1)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            if (r20 == 0) goto L_0x010b
        L_0x00e0:
            java.lang.String r20 = ""
            r0 = r20
            boolean r20 = r14.equals(r0)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            if (r20 != 0) goto L_0x02bf
            boolean r0 = r15 instanceof java.net.Inet6Address     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r20 = r0
            if (r20 == 0) goto L_0x02a2
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r20.<init>()     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            java.lang.String r21 = "[IPv6:"
            java.lang.StringBuilder r20 = r20.append(r21)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r0 = r20
            java.lang.StringBuilder r20 = r0.append(r14)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            java.lang.String r21 = "]"
            java.lang.StringBuilder r20 = r20.append(r21)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            java.lang.String r16 = r20.toString()     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
        L_0x010b:
            r0 = r25
            r1 = r16
            java.util.Map r10 = r0.sendHello(r1)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            java.lang.String r20 = "8BITMIME"
            r0 = r20
            boolean r20 = r10.containsKey(r0)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r0 = r20
            r1 = r25
            r1.m8bitEncodingAllowed = r0     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r0 = r25
            com.fsck.k9.mail.ConnectionSecurity r0 = r0.mConnectionSecurity     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r20 = r0
            com.fsck.k9.mail.ConnectionSecurity r21 = com.fsck.k9.mail.ConnectionSecurity.STARTTLS_REQUIRED     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r0 = r20
            r1 = r21
            if (r0 != r1) goto L_0x01a7
            java.lang.String r20 = "STARTTLS"
            r0 = r20
            boolean r20 = r10.containsKey(r0)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            if (r20 == 0) goto L_0x02c3
            java.lang.String r20 = "STARTTLS"
            r0 = r25
            r1 = r20
            r0.executeSimpleCommand(r1)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r0 = r25
            com.fsck.k9.mail.ssl.TrustedSocketFactory r0 = r0.mTrustedSocketFactory     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r20 = r0
            r0 = r25
            java.net.Socket r0 = r0.mSocket     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r21 = r0
            r0 = r25
            java.lang.String r0 = r0.mHost     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r22 = r0
            r0 = r25
            int r0 = r0.mPort     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r23 = r0
            r0 = r25
            java.lang.String r0 = r0.mClientCertificateAlias     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r24 = r0
            java.net.Socket r20 = r20.createSocket(r21, r22, r23, r24)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r0 = r20
            r1 = r25
            r1.mSocket = r0     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            com.fsck.k9.mail.filter.PeekableInputStream r20 = new com.fsck.k9.mail.filter.PeekableInputStream     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            java.io.BufferedInputStream r21 = new java.io.BufferedInputStream     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r0 = r25
            java.net.Socket r0 = r0.mSocket     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r22 = r0
            java.io.InputStream r22 = r22.getInputStream()     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r23 = 1024(0x400, float:1.435E-42)
            r21.<init>(r22, r23)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r20.<init>(r21)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r0 = r20
            r1 = r25
            r1.mIn = r0     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            java.io.BufferedOutputStream r20 = new java.io.BufferedOutputStream     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r0 = r25
            java.net.Socket r0 = r0.mSocket     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r21 = r0
            java.io.OutputStream r21 = r21.getOutputStream()     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r22 = 1024(0x400, float:1.435E-42)
            r20.<init>(r21, r22)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r0 = r20
            r1 = r25
            r1.mOut = r0     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r0 = r25
            r1 = r16
            java.util.Map r10 = r0.sendHello(r1)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r18 = 1
        L_0x01a7:
            r6 = 0
            r7 = 0
            r4 = 0
            r5 = 0
            r8 = 0
            java.lang.String r20 = "AUTH"
            r0 = r20
            boolean r20 = r10.containsKey(r0)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            if (r20 == 0) goto L_0x01fc
            java.lang.String r20 = "AUTH"
            r0 = r20
            java.lang.Object r20 = r10.get(r0)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            java.lang.String r20 = (java.lang.String) r20     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            java.lang.String r21 = " "
            java.lang.String[] r20 = r20.split(r21)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            java.util.List r17 = java.util.Arrays.asList(r20)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            java.lang.String r20 = "LOGIN"
            r0 = r17
            r1 = r20
            boolean r6 = r0.contains(r1)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            java.lang.String r20 = "PLAIN"
            r0 = r17
            r1 = r20
            boolean r7 = r0.contains(r1)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            java.lang.String r20 = "CRAM-MD5"
            r0 = r17
            r1 = r20
            boolean r4 = r0.contains(r1)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            java.lang.String r20 = "EXTERNAL"
            r0 = r17
            r1 = r20
            boolean r5 = r0.contains(r1)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            java.lang.String r20 = "XOAUTH2"
            r0 = r17
            r1 = r20
            boolean r8 = r0.contains(r1)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
        L_0x01fc:
            r0 = r25
            r0.parseOptionalSizeValue(r10)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r0 = r25
            java.lang.String r0 = r0.mUsername     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r20 = r0
            boolean r20 = android.text.TextUtils.isEmpty(r20)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            if (r20 != 0) goto L_0x0219
            r0 = r25
            java.lang.String r0 = r0.mPassword     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r20 = r0
            boolean r20 = android.text.TextUtils.isEmpty(r20)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            if (r20 == 0) goto L_0x0235
        L_0x0219:
            com.fsck.k9.mail.AuthType r20 = com.fsck.k9.mail.AuthType.EXTERNAL     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r0 = r25
            com.fsck.k9.mail.AuthType r0 = r0.mAuthType     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r21 = r0
            r0 = r20
            r1 = r21
            if (r0 == r1) goto L_0x0235
            com.fsck.k9.mail.AuthType r20 = com.fsck.k9.mail.AuthType.XOAUTH2     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r0 = r25
            com.fsck.k9.mail.AuthType r0 = r0.mAuthType     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r21 = r0
            r0 = r20
            r1 = r21
            if (r0 != r1) goto L_0x02ef
        L_0x0235:
            int[] r20 = com.fsck.k9.mail.transport.SmtpTransport.AnonymousClass1.$SwitchMap$com$fsck$k9$mail$AuthType     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r0 = r25
            com.fsck.k9.mail.AuthType r0 = r0.mAuthType     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r21 = r0
            int r21 = r21.ordinal()     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r20 = r20[r21]     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            switch(r20) {
                case 1: goto L_0x02d8;
                case 2: goto L_0x02d8;
                case 3: goto L_0x0310;
                case 4: goto L_0x0330;
                case 5: goto L_0x0348;
                case 6: goto L_0x0360;
                default: goto L_0x0246;
            }     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
        L_0x0246:
            com.fsck.k9.mail.MessagingException r20 = new com.fsck.k9.mail.MessagingException     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            java.lang.String r21 = "Unhandled authentication method found in the server settings (bug)."
            r20.<init>(r21)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            throw r20     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
        L_0x024e:
            r9 = move-exception
            com.fsck.k9.mail.CertificateValidationException r20 = new com.fsck.k9.mail.CertificateValidationException
            java.lang.String r21 = r9.getMessage()
            r0 = r20
            r1 = r21
            r0.<init>(r1, r9)
            throw r20
        L_0x025d:
            java.net.Socket r20 = new java.net.Socket     // Catch:{ SocketException -> 0x027b }
            r20.<init>()     // Catch:{ SocketException -> 0x027b }
            r0 = r20
            r1 = r25
            r1.mSocket = r0     // Catch:{ SocketException -> 0x027b }
            r0 = r25
            java.net.Socket r0 = r0.mSocket     // Catch:{ SocketException -> 0x027b }
            r20 = r0
            r21 = 10000(0x2710, float:1.4013E-41)
            r0 = r20
            r1 = r19
            r2 = r21
            r0.connect(r1, r2)     // Catch:{ SocketException -> 0x027b }
            goto L_0x0066
        L_0x027b:
            r9 = move-exception
            int r0 = r3.length     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r20 = r0
            int r20 = r20 + -1
            r0 = r20
            if (r12 >= r0) goto L_0x0289
            int r12 = r12 + 1
            goto L_0x000d
        L_0x0289:
            com.fsck.k9.mail.MessagingException r20 = new com.fsck.k9.mail.MessagingException     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            java.lang.String r21 = "Cannot connect to host"
            r0 = r20
            r1 = r21
            r0.<init>(r1, r9)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            throw r20     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
        L_0x0295:
            r11 = move-exception
            com.fsck.k9.mail.MessagingException r20 = new com.fsck.k9.mail.MessagingException
            java.lang.String r21 = "Unable to open connection to SMTP server due to security error."
            r0 = r20
            r1 = r21
            r0.<init>(r1, r11)
            throw r20
        L_0x02a2:
            java.lang.StringBuilder r20 = new java.lang.StringBuilder     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r20.<init>()     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            java.lang.String r21 = "["
            java.lang.StringBuilder r20 = r20.append(r21)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r0 = r20
            java.lang.StringBuilder r20 = r0.append(r14)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            java.lang.String r21 = "]"
            java.lang.StringBuilder r20 = r20.append(r21)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            java.lang.String r16 = r20.toString()     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            goto L_0x010b
        L_0x02bf:
            java.lang.String r16 = "android"
            goto L_0x010b
        L_0x02c3:
            com.fsck.k9.mail.CertificateValidationException r20 = new com.fsck.k9.mail.CertificateValidationException     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            java.lang.String r21 = "STARTTLS connection security not available"
            r20.<init>(r21)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            throw r20     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
        L_0x02cb:
            r13 = move-exception
            com.fsck.k9.mail.MessagingException r20 = new com.fsck.k9.mail.MessagingException
            java.lang.String r21 = "Unable to open connection to SMTP server."
            r0 = r20
            r1 = r21
            r0.<init>(r1, r13)
            throw r20
        L_0x02d8:
            if (r7 == 0) goto L_0x02f0
            r0 = r25
            java.lang.String r0 = r0.mUsername     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r20 = r0
            r0 = r25
            java.lang.String r0 = r0.mPassword     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r21 = r0
            r0 = r25
            r1 = r20
            r2 = r21
            r0.saslAuthPlain(r1, r2)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
        L_0x02ef:
            return
        L_0x02f0:
            if (r6 == 0) goto L_0x0308
            r0 = r25
            java.lang.String r0 = r0.mUsername     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r20 = r0
            r0 = r25
            java.lang.String r0 = r0.mPassword     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r21 = r0
            r0 = r25
            r1 = r20
            r2 = r21
            r0.saslAuthLogin(r1, r2)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            goto L_0x02ef
        L_0x0308:
            com.fsck.k9.mail.MessagingException r20 = new com.fsck.k9.mail.MessagingException     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            java.lang.String r21 = "Authentication methods SASL PLAIN and LOGIN are unavailable."
            r20.<init>(r21)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            throw r20     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
        L_0x0310:
            if (r4 == 0) goto L_0x0328
            r0 = r25
            java.lang.String r0 = r0.mUsername     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r20 = r0
            r0 = r25
            java.lang.String r0 = r0.mPassword     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r21 = r0
            r0 = r25
            r1 = r20
            r2 = r21
            r0.saslAuthCramMD5(r1, r2)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            goto L_0x02ef
        L_0x0328:
            com.fsck.k9.mail.MessagingException r20 = new com.fsck.k9.mail.MessagingException     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            java.lang.String r21 = "Authentication method CRAM-MD5 is unavailable."
            r20.<init>(r21)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            throw r20     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
        L_0x0330:
            if (r8 == 0) goto L_0x0340
            r0 = r25
            java.lang.String r0 = r0.mUsername     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r20 = r0
            r0 = r25
            r1 = r20
            r0.saslXoauth2(r1)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            goto L_0x02ef
        L_0x0340:
            com.fsck.k9.mail.MessagingException r20 = new com.fsck.k9.mail.MessagingException     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            java.lang.String r21 = "Authentication method XOAUTH2 is unavailable."
            r20.<init>(r21)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            throw r20     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
        L_0x0348:
            if (r5 == 0) goto L_0x0358
            r0 = r25
            java.lang.String r0 = r0.mUsername     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r20 = r0
            r0 = r25
            r1 = r20
            r0.saslAuthExternal(r1)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            goto L_0x02ef
        L_0x0358:
            com.fsck.k9.mail.CertificateValidationException r20 = new com.fsck.k9.mail.CertificateValidationException     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            com.fsck.k9.mail.CertificateValidationException$Reason r21 = com.fsck.k9.mail.CertificateValidationException.Reason.MissingCapability     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r20.<init>(r21)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            throw r20     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
        L_0x0360:
            if (r18 == 0) goto L_0x03b5
            if (r7 == 0) goto L_0x037b
            r0 = r25
            java.lang.String r0 = r0.mUsername     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r20 = r0
            r0 = r25
            java.lang.String r0 = r0.mPassword     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r21 = r0
            r0 = r25
            r1 = r20
            r2 = r21
            r0.saslAuthPlain(r1, r2)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            goto L_0x02ef
        L_0x037b:
            if (r6 == 0) goto L_0x0394
            r0 = r25
            java.lang.String r0 = r0.mUsername     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r20 = r0
            r0 = r25
            java.lang.String r0 = r0.mPassword     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r21 = r0
            r0 = r25
            r1 = r20
            r2 = r21
            r0.saslAuthLogin(r1, r2)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            goto L_0x02ef
        L_0x0394:
            if (r4 == 0) goto L_0x03ad
            r0 = r25
            java.lang.String r0 = r0.mUsername     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r20 = r0
            r0 = r25
            java.lang.String r0 = r0.mPassword     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r21 = r0
            r0 = r25
            r1 = r20
            r2 = r21
            r0.saslAuthCramMD5(r1, r2)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            goto L_0x02ef
        L_0x03ad:
            com.fsck.k9.mail.MessagingException r20 = new com.fsck.k9.mail.MessagingException     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            java.lang.String r21 = "No supported authentication methods available."
            r20.<init>(r21)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            throw r20     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
        L_0x03b5:
            if (r4 == 0) goto L_0x03ce
            r0 = r25
            java.lang.String r0 = r0.mUsername     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r20 = r0
            r0 = r25
            java.lang.String r0 = r0.mPassword     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            r21 = r0
            r0 = r25
            r1 = r20
            r2 = r21
            r0.saslAuthCramMD5(r1, r2)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            goto L_0x02ef
        L_0x03ce:
            com.fsck.k9.mail.MessagingException r20 = new com.fsck.k9.mail.MessagingException     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            java.lang.String r21 = "Update your outgoing server authentication setting. AUTOMATIC auth. is unavailable."
            r20.<init>(r21)     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
            throw r20     // Catch:{ SSLException -> 0x024e, GeneralSecurityException -> 0x0295, IOException -> 0x02cb }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fsck.k9.mail.transport.SmtpTransport.open():void");
    }

    private void parseOptionalSizeValue(Map<String, String> extensions) {
        String optionalsizeValue;
        if (extensions.containsKey("SIZE") && (optionalsizeValue = extensions.get("SIZE")) != null && optionalsizeValue != "") {
            try {
                this.mLargestAcceptableMessage = Integer.parseInt(optionalsizeValue);
            } catch (NumberFormatException e) {
                if (K9MailLib.isDebug() && K9MailLib.DEBUG_PROTOCOL_SMTP) {
                    Log.d("k9", "Tried to parse " + optionalsizeValue + " and get an int", e);
                }
            }
        }
    }

    private Map<String, String> sendHello(String host) throws IOException, MessagingException {
        Map<String, String> extensions = new HashMap<>();
        try {
            List<String> results = executeSimpleCommand("EHLO " + host);
            results.remove(0);
            for (String result : results) {
                String[] pair = result.split(" ", 2);
                extensions.put(pair[0].toUpperCase(Locale.US), pair.length == 1 ? "" : pair[1]);
            }
        } catch (NegativeSmtpReplyException e) {
            if (K9MailLib.isDebug()) {
                Log.v("k9", "Server doesn't support the EHLO command. Trying HELO...");
            }
            try {
                executeSimpleCommand("HELO " + host);
            } catch (NegativeSmtpReplyException e2) {
                Log.w("k9", "Server doesn't support the HELO command. Continuing anyway.");
            }
        }
        return extensions;
    }

    public void sendMessage(Message message) throws MessagingException {
        List<Address> addresses = new ArrayList<>();
        addresses.addAll(Arrays.asList(message.getRecipients(Message.RecipientType.TO)));
        addresses.addAll(Arrays.asList(message.getRecipients(Message.RecipientType.CC)));
        addresses.addAll(Arrays.asList(message.getRecipients(Message.RecipientType.BCC)));
        message.setRecipients(Message.RecipientType.BCC, null);
        Map<String, List<String>> charsetAddressesMap = new HashMap<>();
        for (Address address : addresses) {
            String addressString = address.getAddress();
            String charset = CharsetSupport.getCharsetFromAddress(addressString);
            List<String> addressesOfCharset = charsetAddressesMap.get(charset);
            if (addressesOfCharset == null) {
                addressesOfCharset = new ArrayList<>();
                charsetAddressesMap.put(charset, addressesOfCharset);
            }
            addressesOfCharset.add(addressString);
        }
        for (Map.Entry<String, List<String>> charsetAddressesMapEntry : charsetAddressesMap.entrySet()) {
            message.setCharset((String) charsetAddressesMapEntry.getKey());
            sendMessageTo(charsetAddressesMapEntry.getValue(), message);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fsck.k9.mail.MessagingException.<init>(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.fsck.k9.mail.MessagingException.<init>(java.lang.String, java.lang.Throwable):void
      com.fsck.k9.mail.MessagingException.<init>(java.lang.String, boolean):void */
    private void sendMessageTo(List<String> addresses, Message message) throws MessagingException {
        close();
        open();
        if (!this.m8bitEncodingAllowed) {
            Log.d("k9", "Server does not support 8bit transfer encoding");
        }
        if (this.mLargestAcceptableMessage <= 0 || !message.hasAttachments() || message.calculateSize() <= ((long) this.mLargestAcceptableMessage)) {
            boolean entireMessageSent = false;
            try {
                executeSimpleCommand("MAIL FROM:<" + message.getFrom()[0].getAddress() + Account.DEFAULT_QUOTE_PREFIX + (this.m8bitEncodingAllowed ? " BODY=8BITMIME" : ""));
                for (String address : addresses) {
                    executeSimpleCommand("RCPT TO:<" + address + Account.DEFAULT_QUOTE_PREFIX);
                }
                executeSimpleCommand("DATA");
                EOLConvertingOutputStream msgOut = new EOLConvertingOutputStream(new LineWrapOutputStream(new SmtpDataStuffing(this.mOut), 1000));
                message.writeTo(msgOut);
                msgOut.endWithCrLfAndFlush();
                entireMessageSent = true;
                executeSimpleCommand(".");
                close();
            } catch (NegativeSmtpReplyException e) {
                throw e;
            } catch (Exception e2) {
                MessagingException me2 = new MessagingException("Unable to send message", e2);
                me2.setPermanentFailure(entireMessageSent);
                throw me2;
            } catch (Throwable th) {
                close();
                throw th;
            }
        } else {
            throw new MessagingException("Message too large for server", true);
        }
    }

    public void close() {
        try {
            executeSimpleCommand("QUIT");
        } catch (Exception e) {
        }
        try {
            this.mIn.close();
        } catch (Exception e2) {
        }
        try {
            this.mOut.close();
        } catch (Exception e3) {
        }
        try {
            this.mSocket.close();
        } catch (Exception e4) {
        }
        this.mIn = null;
        this.mOut = null;
        this.mSocket = null;
    }

    private String readLine() throws IOException {
        StringBuilder sb = new StringBuilder();
        while (true) {
            int d = this.mIn.read();
            if (d == -1) {
                break;
            } else if (((char) d) != 13) {
                if (((char) d) == 10) {
                    break;
                }
                sb.append((char) d);
            }
        }
        String ret = sb.toString();
        if (K9MailLib.isDebug() && K9MailLib.DEBUG_PROTOCOL_SMTP) {
            Log.d("k9", "SMTP <<< " + ret);
        }
        return ret;
    }

    private void writeLine(String s, boolean sensitive) throws IOException {
        String commandToLog;
        if (K9MailLib.isDebug() && K9MailLib.DEBUG_PROTOCOL_SMTP) {
            if (!sensitive || K9MailLib.isDebugSensitive()) {
                commandToLog = "SMTP >>> " + s;
            } else {
                commandToLog = "SMTP >>> *sensitive*";
            }
            Log.d("k9", commandToLog);
        }
        this.mOut.write(s.concat("\r\n").getBytes());
        this.mOut.flush();
    }

    private void checkLine(String line) throws MessagingException {
        int length = line.length();
        if (length < 1) {
            throw new MessagingException("SMTP response is 0 length");
        }
        char c = line.charAt(0);
        if (c == '4' || c == '5') {
            int replyCode = -1;
            String message = line;
            if (length >= 3) {
                try {
                    replyCode = Integer.parseInt(line.substring(0, 3));
                } catch (NumberFormatException e) {
                }
                if (length > 4) {
                    message = line.substring(4);
                } else {
                    message = "";
                }
            }
            throw new NegativeSmtpReplyException(replyCode, message);
        }
    }

    private List<String> executeSimpleCommand(String command) throws IOException, MessagingException {
        return executeSimpleCommand(command, false);
    }

    private List<String> executeSimpleCommand(String command, boolean sensitive) throws IOException, MessagingException {
        List<String> results = new ArrayList<>();
        if (command != null) {
            writeLine(command, sensitive);
        }
        String line = readLine();
        while (line.length() >= 4) {
            if (line.length() > 4) {
                results.add(line.substring(4));
            }
            if (line.charAt(3) != '-') {
                break;
            }
            line = readLine();
        }
        checkLine(line);
        return results;
    }

    private void saslAuthLogin(String username, String password) throws MessagingException, AuthenticationFailedException, IOException {
        try {
            executeSimpleCommand("AUTH LOGIN");
            executeSimpleCommand(Base64.encode(username), true);
            executeSimpleCommand(Base64.encode(password), true);
        } catch (NegativeSmtpReplyException exception) {
            if (exception.getReplyCode() == 535) {
                throw new AuthenticationFailedException("AUTH LOGIN failed (" + exception.getMessage() + ")");
            }
            throw exception;
        }
    }

    private void saslAuthPlain(String username, String password) throws MessagingException, AuthenticationFailedException, IOException {
        try {
            executeSimpleCommand("AUTH PLAIN " + Base64.encode("\u0000" + username + "\u0000" + password), true);
        } catch (NegativeSmtpReplyException exception) {
            if (exception.getReplyCode() == 535) {
                throw new AuthenticationFailedException("AUTH PLAIN failed (" + exception.getMessage() + ")");
            }
            throw exception;
        }
    }

    private void saslAuthCramMD5(String username, String password) throws MessagingException, AuthenticationFailedException, IOException {
        List<String> respList = executeSimpleCommand("AUTH CRAM-MD5");
        if (respList.size() != 1) {
            throw new MessagingException("Unable to negotiate CRAM-MD5");
        }
        try {
            executeSimpleCommand(Authentication.computeCramMd5(this.mUsername, this.mPassword, respList.get(0)), true);
        } catch (NegativeSmtpReplyException exception) {
            if (exception.getReplyCode() == 535) {
                throw new AuthenticationFailedException(exception.getMessage(), exception);
            }
            throw exception;
        }
    }

    private void saslXoauth2(String username) throws MessagingException, IOException {
        try {
            attemptXoauth2(username);
        } catch (NegativeSmtpReplyException negativeResponseFromOldToken) {
            if (negativeResponseFromOldToken.getReplyCode() != 535) {
                throw negativeResponseFromOldToken;
            }
            Log.v("k9", "Authentication exception, invalidating token and re-trying", negativeResponseFromOldToken);
            this.oauthTokenProvider.invalidateToken(username);
            try {
                attemptXoauth2(username);
            } catch (NegativeSmtpReplyException negativeResponseFromNewToken) {
                if (negativeResponseFromNewToken.getReplyCode() != 535) {
                    throw negativeResponseFromNewToken;
                }
                Log.v("k9", "Authentication exception for new token, permanent error assumed", negativeResponseFromNewToken);
                this.oauthTokenProvider.invalidateToken(username);
                throw new AuthenticationFailedException(negativeResponseFromNewToken.getMessage(), negativeResponseFromNewToken);
            }
        }
    }

    private void attemptXoauth2(String username) throws MessagingException, IOException {
        executeSimpleCommand("AUTH XOAUTH2 " + Authentication.computeXoauth(username, this.oauthTokenProvider.getToken(username, 30000)), true);
    }

    private void saslAuthExternal(String username) throws MessagingException, IOException {
        executeSimpleCommand(String.format("AUTH EXTERNAL %s", Base64.encode(username)), false);
    }

    static class NegativeSmtpReplyException extends MessagingException {
        private static final long serialVersionUID = 8696043577357897135L;
        private final int mReplyCode;
        private final String mReplyText;

        public NegativeSmtpReplyException(int replyCode, String replyText) {
            super("Negative SMTP reply: " + replyCode + " " + replyText, isPermanentSmtpError(replyCode));
            this.mReplyCode = replyCode;
            this.mReplyText = replyText;
        }

        private static boolean isPermanentSmtpError(int replyCode) {
            return replyCode >= 500 && replyCode <= 599;
        }

        public int getReplyCode() {
            return this.mReplyCode;
        }

        public String getReplyText() {
            return this.mReplyText;
        }
    }
}
