package com.fsck.k9.mail.store.imap;

import android.util.Log;

class ImapPushState {
    private static final long DEFAULT_UID_NEXT = -1;
    private static final String PUSH_STATE_PREFIX = "uidNext=";
    private static final int PUSH_STATE_PREFIX_LENGTH = 8;
    public final long uidNext;

    public static ImapPushState parse(String pushState) {
        if (pushState == null || !pushState.startsWith(PUSH_STATE_PREFIX)) {
            return createDefaultImapPushState();
        }
        String value = pushState.substring(8);
        try {
            return new ImapPushState(Long.parseLong(value));
        } catch (NumberFormatException e) {
            Log.e("k9", "Unable to part uidNext value " + value, e);
            return createDefaultImapPushState();
        }
    }

    static ImapPushState createDefaultImapPushState() {
        return new ImapPushState(-1);
    }

    public ImapPushState(long uidNext2) {
        this.uidNext = uidNext2;
    }

    public String toString() {
        return PUSH_STATE_PREFIX + this.uidNext;
    }
}
