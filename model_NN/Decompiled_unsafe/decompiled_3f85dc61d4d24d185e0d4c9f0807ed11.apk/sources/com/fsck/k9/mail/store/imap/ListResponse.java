package com.fsck.k9.mail.store.imap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

class ListResponse {
    private final List<String> attributes;
    private final String hierarchyDelimiter;
    private final String name;

    private ListResponse(List<String> attributes2, String hierarchyDelimiter2, String name2) {
        this.attributes = Collections.unmodifiableList(attributes2);
        this.hierarchyDelimiter = hierarchyDelimiter2;
        this.name = name2;
    }

    public static List<ListResponse> parseList(List<ImapResponse> responses) {
        return parse(responses, "LIST");
    }

    public static List<ListResponse> parseLsub(List<ImapResponse> responses) {
        return parse(responses, Responses.LSUB);
    }

    private static List<ListResponse> parse(List<ImapResponse> responses, String commandResponse) {
        List<ListResponse> listResponses = new ArrayList<>();
        for (ImapResponse response : responses) {
            ListResponse listResponse = parseSingleLine(response, commandResponse);
            if (listResponse != null) {
                listResponses.add(listResponse);
            }
        }
        return Collections.unmodifiableList(listResponses);
    }

    private static ListResponse parseSingleLine(ImapResponse response, String commandResponse) {
        List<String> attributes2;
        if (response.size() < 4 || !ImapResponseParser.equalsIgnoreCase(response.get(0), commandResponse) || (attributes2 = extractAttributes(response)) == null) {
            return null;
        }
        String hierarchyDelimiter2 = response.getString(2);
        if (hierarchyDelimiter2.length() == 1) {
            return new ListResponse(attributes2, hierarchyDelimiter2, response.getString(3));
        }
        return null;
    }

    private static List<String> extractAttributes(ImapResponse response) {
        ImapList nameAttributes = response.getList(1);
        List<String> attributes2 = new ArrayList<>(nameAttributes.size());
        Iterator it = nameAttributes.iterator();
        while (it.hasNext()) {
            Object nameAttribute = it.next();
            if (!(nameAttribute instanceof String)) {
                return null;
            }
            attributes2.add((String) nameAttribute);
        }
        return attributes2;
    }

    public List<String> getAttributes() {
        return this.attributes;
    }

    public boolean hasAttribute(String attribute) {
        for (String attributeInResponse : this.attributes) {
            if (attributeInResponse.equalsIgnoreCase(attribute)) {
                return true;
            }
        }
        return false;
    }

    public String getHierarchyDelimiter() {
        return this.hierarchyDelimiter;
    }

    public String getName() {
        return this.name;
    }
}
