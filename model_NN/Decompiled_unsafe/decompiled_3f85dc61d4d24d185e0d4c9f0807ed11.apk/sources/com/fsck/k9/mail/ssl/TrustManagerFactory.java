package com.fsck.k9.mail.ssl;

import android.util.Log;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public final class TrustManagerFactory {
    private static final String LOG_TAG = "TrustManagerFactory";
    /* access modifiers changed from: private */
    public static X509TrustManager defaultTrustManager;
    /* access modifiers changed from: private */
    public static LocalKeyStore keyStore;

    private static class SecureX509TrustManager implements X509TrustManager {
        private static final Map<String, SecureX509TrustManager> mTrustManager = new HashMap();
        private final String mHost;
        private final int mPort;

        private SecureX509TrustManager(String host, int port) {
            this.mHost = host;
            this.mPort = port;
        }

        public static synchronized X509TrustManager getInstance(String host, int port) {
            SecureX509TrustManager trustManager;
            synchronized (SecureX509TrustManager.class) {
                String key = host + ":" + port;
                if (mTrustManager.containsKey(key)) {
                    trustManager = mTrustManager.get(key);
                } else {
                    trustManager = new SecureX509TrustManager(host, port);
                    mTrustManager.put(key, trustManager);
                }
            }
            return trustManager;
        }

        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            TrustManagerFactory.defaultTrustManager.checkClientTrusted(chain, authType);
        }

        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: javax.net.ssl.SSLException} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: javax.net.ssl.SSLException} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v1, resolved type: java.security.cert.CertificateException} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v3, resolved type: javax.net.ssl.SSLException} */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void checkServerTrusted(java.security.cert.X509Certificate[] r8, java.lang.String r9) throws java.security.cert.CertificateException {
            /*
                r7 = this;
                r3 = 0
                r4 = 0
                r1 = r8[r4]
                r0 = 0
                javax.net.ssl.X509TrustManager r4 = com.fsck.k9.mail.ssl.TrustManagerFactory.defaultTrustManager     // Catch:{ CertificateException -> 0x0017, SSLException -> 0x0031 }
                r4.checkServerTrusted(r8, r9)     // Catch:{ CertificateException -> 0x0017, SSLException -> 0x0031 }
                org.apache.http.conn.ssl.StrictHostnameVerifier r4 = new org.apache.http.conn.ssl.StrictHostnameVerifier     // Catch:{ CertificateException -> 0x0017, SSLException -> 0x0031 }
                r4.<init>()     // Catch:{ CertificateException -> 0x0017, SSLException -> 0x0031 }
                java.lang.String r5 = r7.mHost     // Catch:{ CertificateException -> 0x0017, SSLException -> 0x0031 }
                r4.verify(r5, r1)     // Catch:{ CertificateException -> 0x0017, SSLException -> 0x0031 }
            L_0x0016:
                return
            L_0x0017:
                r2 = move-exception
                java.lang.String r3 = r2.getMessage()
                r0 = r2
            L_0x001d:
                com.fsck.k9.mail.ssl.LocalKeyStore r4 = com.fsck.k9.mail.ssl.TrustManagerFactory.keyStore
                java.lang.String r5 = r7.mHost
                int r6 = r7.mPort
                boolean r4 = r4.isValidCertificate(r1, r5, r6)
                if (r4 != 0) goto L_0x0016
                com.fsck.k9.mail.CertificateChainException r4 = new com.fsck.k9.mail.CertificateChainException
                r4.<init>(r3, r8, r0)
                throw r4
            L_0x0031:
                r2 = move-exception
                java.lang.String r3 = r2.getMessage()
                r0 = r2
                goto L_0x001d
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fsck.k9.mail.ssl.TrustManagerFactory.SecureX509TrustManager.checkServerTrusted(java.security.cert.X509Certificate[], java.lang.String):void");
        }

        public X509Certificate[] getAcceptedIssuers() {
            return TrustManagerFactory.defaultTrustManager.getAcceptedIssuers();
        }
    }

    static {
        try {
            keyStore = LocalKeyStore.getInstance();
            javax.net.ssl.TrustManagerFactory tmf = javax.net.ssl.TrustManagerFactory.getInstance("X509");
            tmf.init((KeyStore) null);
            TrustManager[] tms = tmf.getTrustManagers();
            if (tms != null) {
                for (TrustManager tm : tms) {
                    if (tm instanceof X509TrustManager) {
                        defaultTrustManager = (X509TrustManager) tm;
                        return;
                    }
                }
            }
        } catch (NoSuchAlgorithmException e) {
            Log.e(LOG_TAG, "Unable to get X509 Trust Manager ", e);
        } catch (KeyStoreException e2) {
            Log.e(LOG_TAG, "Key Store exception while initializing TrustManagerFactory ", e2);
        }
    }

    private TrustManagerFactory() {
    }

    public static X509TrustManager get(String host, int port) {
        return SecureX509TrustManager.getInstance(host, port);
    }
}
