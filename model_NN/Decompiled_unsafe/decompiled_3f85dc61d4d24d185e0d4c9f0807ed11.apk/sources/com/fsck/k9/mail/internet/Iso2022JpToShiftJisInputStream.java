package com.fsck.k9.mail.internet;

import android.support.v4.media.TransportMediator;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.MalformedInputException;

class Iso2022JpToShiftJisInputStream extends InputStream {
    private Charset charset = Charset.ASCII;
    private boolean hasOut = false;
    private InputStream mIn;
    private int out;

    private enum Charset {
        ASCII,
        JISX0201,
        JISX0208
    }

    public Iso2022JpToShiftJisInputStream(InputStream in) {
        this.mIn = in;
    }

    public int read() throws IOException {
        int i;
        if (this.hasOut) {
            this.hasOut = false;
            return this.out;
        }
        int in1 = this.mIn.read();
        while (in1 == 27) {
            int in12 = this.mIn.read();
            if (in12 == 40) {
                int in13 = this.mIn.read();
                if (in13 == 66 || in13 == 74) {
                    this.charset = Charset.ASCII;
                } else if (in13 == 73) {
                    this.charset = Charset.JISX0201;
                } else {
                    throw new MalformedInputException(0);
                }
            } else if (in12 == 36) {
                int in14 = this.mIn.read();
                if (in14 == 64 || in14 == 66) {
                    this.charset = Charset.JISX0208;
                } else {
                    throw new MalformedInputException(0);
                }
            } else {
                throw new MalformedInputException(0);
            }
            in1 = this.mIn.read();
        }
        if (in1 == 10 || in1 == 13) {
            this.charset = Charset.ASCII;
        }
        if (in1 < 33 || in1 >= 127) {
            return in1;
        }
        switch (this.charset) {
            case ASCII:
                return in1;
            case JISX0201:
                return in1 + 128;
            case JISX0208:
                int in2 = this.mIn.read();
                if (in2 < 33 || in2 >= 127) {
                    throw new MalformedInputException(0);
                }
                int out1 = ((in1 + 1) / 2) + (in1 < 95 ? 112 : 176);
                if (in1 % 2 == 0) {
                    i = TransportMediator.KEYCODE_MEDIA_PLAY;
                } else {
                    i = in2 < 96 ? 31 : 32;
                }
                this.out = in2 + i;
                this.hasOut = true;
                return out1;
            default:
                throw new RuntimeException();
        }
    }
}
