package com.fsck.k9.mail.transport;

import android.util.Log;
import com.fsck.k9.mail.K9MailLib;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.ServerSettings;
import com.fsck.k9.mail.Transport;
import com.fsck.k9.mail.store.StoreConfig;
import com.fsck.k9.mail.store.webdav.WebDavHttpClient;
import com.fsck.k9.mail.store.webdav.WebDavStore;
import java.util.Collections;

public class WebDavTransport extends Transport {
    private WebDavStore store;

    public static ServerSettings decodeUri(String uri) {
        return WebDavStore.decodeUri(uri);
    }

    public static String createUri(ServerSettings server) {
        return WebDavStore.createUri(server);
    }

    public WebDavTransport(StoreConfig storeConfig) throws MessagingException {
        this.store = new WebDavStore(storeConfig, new WebDavHttpClient.WebDavHttpClientFactory());
        if (K9MailLib.isDebug()) {
            Log.d("k9", ">>> New WebDavTransport creation complete");
        }
    }

    public void open() throws MessagingException {
        if (K9MailLib.isDebug()) {
            Log.d("k9", ">>> open called on WebDavTransport ");
        }
        this.store.getHttpClient();
    }

    public void close() {
    }

    public void sendMessage(Message message) throws MessagingException {
        this.store.sendMessages(Collections.singletonList(message));
    }
}
