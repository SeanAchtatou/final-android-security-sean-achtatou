package com.fsck.k9.mail.store.imap;

import com.fsck.k9.mail.AuthType;
import com.fsck.k9.mail.ConnectionSecurity;
import com.fsck.k9.mail.ServerSettings;
import java.util.HashMap;
import java.util.Map;

public class ImapStoreSettings extends ServerSettings {
    public static final String AUTODETECT_NAMESPACE_KEY = "autoDetectNamespace";
    public static final String PATH_PREFIX_KEY = "pathPrefix";
    public final boolean autoDetectNamespace;
    public final String pathPrefix;

    protected ImapStoreSettings(String host, int port, ConnectionSecurity connectionSecurity, AuthType authenticationType, String username, String password, String clientCertificateAlias, boolean autodetectNamespace, String pathPrefix2) {
        super(ServerSettings.Type.IMAP, host, port, connectionSecurity, authenticationType, username, password, clientCertificateAlias);
        this.autoDetectNamespace = autodetectNamespace;
        this.pathPrefix = pathPrefix2;
    }

    public Map<String, String> getExtra() {
        Map<String, String> extra = new HashMap<>();
        extra.put(AUTODETECT_NAMESPACE_KEY, Boolean.valueOf(this.autoDetectNamespace).toString());
        putIfNotNull(extra, PATH_PREFIX_KEY, this.pathPrefix);
        return extra;
    }

    public ServerSettings newPassword(String newPassword) {
        return new ImapStoreSettings(this.host, this.port, this.connectionSecurity, this.authenticationType, this.username, newPassword, this.clientCertificateAlias, this.autoDetectNamespace, this.pathPrefix);
    }
}
