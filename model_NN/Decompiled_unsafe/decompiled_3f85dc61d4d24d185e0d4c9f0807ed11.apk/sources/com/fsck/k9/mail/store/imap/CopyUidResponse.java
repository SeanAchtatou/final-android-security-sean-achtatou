package com.fsck.k9.mail.store.imap;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class CopyUidResponse {
    private final Map<String, String> uidMapping;

    private CopyUidResponse(Map<String, String> uidMapping2) {
        this.uidMapping = Collections.unmodifiableMap(uidMapping2);
    }

    public static CopyUidResponse parse(ImapResponse response) {
        if (!response.isTagged() || response.size() < 2 || !ImapResponseParser.equalsIgnoreCase(response.get(0), Responses.OK) || !response.isList(1)) {
            return null;
        }
        ImapList responseTextList = response.getList(1);
        if (responseTextList.size() < 4 || !ImapResponseParser.equalsIgnoreCase(responseTextList.get(0), Responses.COPYUID) || !responseTextList.isString(1) || !responseTextList.isString(2) || !responseTextList.isString(3)) {
            return null;
        }
        List<String> sourceUids = ImapUtility.getImapSequenceValues(responseTextList.getString(2));
        List<String> destinationUids = ImapUtility.getImapSequenceValues(responseTextList.getString(3));
        int size = sourceUids.size();
        if (size == 0 || size != destinationUids.size()) {
            return null;
        }
        Map<String, String> uidMapping2 = new HashMap<>(size);
        for (int i = 0; i < size; i++) {
            uidMapping2.put(sourceUids.get(i), destinationUids.get(i));
        }
        return new CopyUidResponse(uidMapping2);
    }

    public Map<String, String> getUidMapping() {
        return this.uidMapping;
    }
}
