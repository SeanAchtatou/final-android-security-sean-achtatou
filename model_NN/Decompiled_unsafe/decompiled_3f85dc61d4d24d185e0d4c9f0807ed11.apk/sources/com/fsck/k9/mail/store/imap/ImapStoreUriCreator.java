package com.fsck.k9.mail.store.imap;

import com.fsck.k9.mail.AuthType;
import com.fsck.k9.mail.ServerSettings;
import com.fsck.k9.mail.helper.UrlEncodingHelper;
import exts.whats.Constants;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

class ImapStoreUriCreator {
    ImapStoreUriCreator() {
    }

    public static String create(ServerSettings server) {
        String scheme;
        String userInfo;
        String path;
        String pathPrefix;
        String userEnc = UrlEncodingHelper.encodeUtf8(server.username);
        String passwordEnc = server.password != null ? UrlEncodingHelper.encodeUtf8(server.password) : "";
        String clientCertificateAliasEnc = server.clientCertificateAlias != null ? UrlEncodingHelper.encodeUtf8(server.clientCertificateAlias) : "";
        switch (server.connectionSecurity) {
            case SSL_TLS_REQUIRED:
                scheme = "imap+ssl+";
                break;
            case STARTTLS_REQUIRED:
                scheme = "imap+tls+";
                break;
            default:
                scheme = "imap";
                break;
        }
        AuthType authType = server.authenticationType;
        if (authType == AuthType.EXTERNAL) {
            userInfo = authType.name() + ":" + userEnc + ":" + clientCertificateAliasEnc;
        } else {
            userInfo = authType.name() + ":" + userEnc + ":" + passwordEnc;
        }
        try {
            Map<String, String> extra = server.getExtra();
            if (extra != null) {
                boolean autoDetectNamespace = Boolean.TRUE.toString().equals(extra.get(ImapStoreSettings.AUTODETECT_NAMESPACE_KEY));
                if (autoDetectNamespace) {
                    pathPrefix = null;
                } else {
                    pathPrefix = extra.get(ImapStoreSettings.PATH_PREFIX_KEY);
                }
                StringBuilder append = new StringBuilder().append("/").append(autoDetectNamespace ? Constants.INSTALL_ID : "0").append("|");
                if (pathPrefix == null) {
                    pathPrefix = "";
                }
                path = append.append(pathPrefix).toString();
            } else {
                path = "/1|";
            }
            return new URI(scheme, userInfo, server.host, server.port, path, null, null).toString();
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException("Can't create ImapStore URI", e);
        }
    }
}
