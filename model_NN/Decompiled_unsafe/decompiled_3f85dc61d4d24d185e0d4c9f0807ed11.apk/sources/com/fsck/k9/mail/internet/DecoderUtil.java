package com.fsck.k9.mail.internet;

import android.util.Log;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.MessagingException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import org.apache.james.mime4j.codec.Base64InputStream;
import org.apache.james.mime4j.codec.QuotedPrintableInputStream;
import org.apache.james.mime4j.util.CharsetUtil;

class DecoderUtil {
    DecoderUtil() {
    }

    private static String decodeB(String encodedWord, String charset) {
        try {
            return CharsetSupport.readToString(new Base64InputStream(new ByteArrayInputStream(encodedWord.getBytes(Charset.forName("US-ASCII")))), charset);
        } catch (IOException e) {
            return null;
        }
    }

    private static String decodeQ(String encodedWord, String charset) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < encodedWord.length(); i++) {
            char c = encodedWord.charAt(i);
            if (c == '_') {
                sb.append("=20");
            } else {
                sb.append(c);
            }
        }
        try {
            return CharsetSupport.readToString(new QuotedPrintableInputStream(new ByteArrayInputStream(sb.toString().getBytes(Charset.forName("US-ASCII")))), charset);
        } catch (IOException e) {
            return null;
        }
    }

    public static String decodeEncodedWords(String body, Message message) {
        if (!body.contains("=?")) {
            return body;
        }
        int previousEnd = 0;
        boolean previousWasEncoded = false;
        StringBuilder sb = new StringBuilder();
        while (true) {
            int begin = body.indexOf("=?", previousEnd);
            if (begin == -1) {
                sb.append(body.substring(previousEnd));
                return sb.toString();
            }
            int qm1 = body.indexOf(63, begin + 2);
            if (qm1 == -1) {
                sb.append(body.substring(previousEnd));
                return sb.toString();
            }
            int qm2 = body.indexOf(63, qm1 + 1);
            if (qm2 == -1) {
                sb.append(body.substring(previousEnd));
                return sb.toString();
            }
            int end = body.indexOf("?=", qm2 + 1);
            if (end == -1) {
                sb.append(body.substring(previousEnd));
                return sb.toString();
            }
            int end2 = end + 2;
            String sep = body.substring(previousEnd, begin);
            String decoded = decodeEncodedWord(body, begin, end2, message);
            if (decoded == null) {
                sb.append(sep);
                sb.append(body.substring(begin, end2));
            } else {
                if (!previousWasEncoded || !CharsetUtil.isWhitespace(sep)) {
                    sb.append(sep);
                }
                sb.append(decoded);
            }
            previousEnd = end2;
            previousWasEncoded = decoded != null;
        }
    }

    private static String decodeEncodedWord(String body, int begin, int end, Message message) {
        int qm2;
        int qm1 = body.indexOf(63, begin + 2);
        if (qm1 == end - 2 || (qm2 = body.indexOf(63, qm1 + 1)) == end - 2) {
            return null;
        }
        String mimeCharset = body.substring(begin + 2, qm1);
        String encoding = body.substring(qm1 + 1, qm2);
        String encodedText = body.substring(qm2 + 1, end - 2);
        try {
            String charset = CharsetSupport.fixupCharset(mimeCharset, message);
            if (encodedText.isEmpty()) {
                Log.w("k9", "Missing encoded text in encoded word: '" + body.substring(begin, end) + "'");
                return null;
            } else if (encoding.equalsIgnoreCase("Q")) {
                return decodeQ(encodedText, charset);
            } else {
                if (encoding.equalsIgnoreCase("B")) {
                    return decodeB(encodedText, charset);
                }
                Log.w("k9", "Warning: Unknown encoding in encoded word '" + body.substring(begin, end) + "'");
                return null;
            }
        } catch (MessagingException e) {
            return null;
        }
    }
}
