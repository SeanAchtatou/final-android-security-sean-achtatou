package com.fsck.k9.mail.store.imap;

class SelectOrExamineResponse {
    private final Boolean readWriteMode;

    private SelectOrExamineResponse(Boolean readWriteMode2) {
        this.readWriteMode = readWriteMode2;
    }

    public static SelectOrExamineResponse parse(ImapResponse response) {
        if (!response.isTagged() || !ImapResponseParser.equalsIgnoreCase(response.get(0), Responses.OK)) {
            return null;
        }
        if (!response.isList(1)) {
            return noOpenModeInResponse();
        }
        ImapList responseTextList = response.getList(1);
        if (!responseTextList.isString(0)) {
            return noOpenModeInResponse();
        }
        String responseCode = responseTextList.getString(0);
        if ("READ-ONLY".equalsIgnoreCase(responseCode)) {
            return new SelectOrExamineResponse(false);
        }
        if ("READ-WRITE".equalsIgnoreCase(responseCode)) {
            return new SelectOrExamineResponse(true);
        }
        return noOpenModeInResponse();
    }

    private static SelectOrExamineResponse noOpenModeInResponse() {
        return new SelectOrExamineResponse(null);
    }

    public boolean hasOpenMode() {
        return this.readWriteMode != null;
    }

    public int getOpenMode() {
        if (hasOpenMode()) {
            return this.readWriteMode.booleanValue() ? 0 : 1;
        }
        throw new IllegalStateException("Called getOpenMode() despite hasOpenMode() returning false");
    }
}
