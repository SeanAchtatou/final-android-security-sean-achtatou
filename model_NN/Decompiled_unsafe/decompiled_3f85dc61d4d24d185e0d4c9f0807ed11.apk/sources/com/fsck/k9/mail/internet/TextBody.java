package com.fsck.k9.mail.internet;

import android.support.annotation.Nullable;
import android.util.Log;
import com.fsck.k9.mail.Body;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.filter.CountingOutputStream;
import com.fsck.k9.mail.filter.SignSafeOutputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import org.apache.james.mime4j.codec.QuotedPrintableOutputStream;
import org.apache.james.mime4j.util.MimeUtil;

public class TextBody implements Body, SizeAware {
    private static final byte[] EMPTY_BYTE_ARRAY = new byte[0];
    private String charset = "UTF-8";
    @Nullable
    private Integer composedMessageLength;
    @Nullable
    private Integer composedMessageOffset;
    private String encoding;
    private final String text;

    public TextBody(String body) {
        this.text = body;
    }

    public void writeTo(OutputStream out) throws IOException, MessagingException {
        if (this.text != null) {
            byte[] bytes = this.text.getBytes(this.charset);
            if (MimeUtil.ENC_QUOTED_PRINTABLE.equalsIgnoreCase(this.encoding)) {
                writeSignSafeQuotedPrintable(out, bytes);
            } else if (MimeUtil.ENC_8BIT.equalsIgnoreCase(this.encoding)) {
                out.write(bytes);
            } else {
                throw new IllegalStateException("Cannot get size for encoding!");
            }
        }
    }

    public String getRawText() {
        return this.text;
    }

    public InputStream getInputStream() throws MessagingException {
        byte[] b;
        try {
            if (this.text != null) {
                b = this.text.getBytes(this.charset);
            } else {
                b = EMPTY_BYTE_ARRAY;
            }
            return new ByteArrayInputStream(b);
        } catch (UnsupportedEncodingException uee) {
            Log.e("k9", "Unsupported charset: " + this.charset, uee);
            return null;
        }
    }

    public void setEncoding(String encoding2) {
        if (!(MimeUtil.ENC_QUOTED_PRINTABLE.equalsIgnoreCase(encoding2) || MimeUtil.ENC_8BIT.equalsIgnoreCase(encoding2))) {
            throw new IllegalArgumentException("Cannot encode to " + encoding2);
        }
        this.encoding = encoding2;
    }

    public void setCharset(String charset2) {
        this.charset = charset2;
    }

    @Nullable
    public Integer getComposedMessageLength() {
        return this.composedMessageLength;
    }

    public void setComposedMessageLength(@Nullable Integer composedMessageLength2) {
        this.composedMessageLength = composedMessageLength2;
    }

    @Nullable
    public Integer getComposedMessageOffset() {
        return this.composedMessageOffset;
    }

    public void setComposedMessageOffset(@Nullable Integer composedMessageOffset2) {
        this.composedMessageOffset = composedMessageOffset2;
    }

    public long getSize() {
        try {
            byte[] bytes = this.text.getBytes(this.charset);
            if (MimeUtil.ENC_QUOTED_PRINTABLE.equalsIgnoreCase(this.encoding)) {
                return getLengthWhenQuotedPrintableEncoded(bytes);
            }
            if (MimeUtil.ENC_8BIT.equalsIgnoreCase(this.encoding)) {
                return (long) bytes.length;
            }
            throw new IllegalStateException("Cannot get size for encoding!");
        } catch (IOException e) {
            throw new RuntimeException("Couldn't get body size", e);
        }
    }

    private long getLengthWhenQuotedPrintableEncoded(byte[] bytes) throws IOException {
        CountingOutputStream countingOutputStream = new CountingOutputStream();
        writeSignSafeQuotedPrintable(countingOutputStream, bytes);
        return countingOutputStream.getCount();
    }

    private void writeSignSafeQuotedPrintable(OutputStream out, byte[] bytes) throws IOException {
        QuotedPrintableOutputStream signSafeQuotedPrintableOutputStream;
        SignSafeOutputStream signSafeOutputStream = new SignSafeOutputStream(out);
        try {
            signSafeQuotedPrintableOutputStream = new QuotedPrintableOutputStream(signSafeOutputStream, false);
            signSafeQuotedPrintableOutputStream.write(bytes);
            signSafeQuotedPrintableOutputStream.close();
            signSafeOutputStream.close();
        } catch (Throwable th) {
            signSafeOutputStream.close();
            throw th;
        }
    }

    public String getEncoding() {
        return this.encoding;
    }
}
