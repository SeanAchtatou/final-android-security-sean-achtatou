package com.fsck.k9.mail;

public abstract class BodyPart implements Part {
    private Multipart parent;
    private String serverExtra;

    public abstract void setEncoding(String str) throws MessagingException;

    public String getServerExtra() {
        return this.serverExtra;
    }

    public void setServerExtra(String serverExtra2) {
        this.serverExtra = serverExtra2;
    }

    public Multipart getParent() {
        return this.parent;
    }

    public void setParent(Multipart parent2) {
        this.parent = parent2;
    }
}
