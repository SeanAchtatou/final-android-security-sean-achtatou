package com.fsck.k9.mail.store.imap;

import com.fsck.k9.mail.filter.FixedLengthInputStream;

interface ImapResponseCallback {
    Object foundLiteral(ImapResponse imapResponse, FixedLengthInputStream fixedLengthInputStream) throws Exception;
}
