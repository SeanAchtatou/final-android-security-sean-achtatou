package com.fsck.k9.mail.message;

import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.Part;
import java.io.IOException;
import java.io.InputStream;
import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.parser.ContentHandler;
import org.apache.james.mime4j.parser.MimeStreamParser;
import org.apache.james.mime4j.stream.BodyDescriptor;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.stream.MimeConfig;

public class MessageHeaderParser {
    public static void parse(Part part, InputStream headerInputStream) throws MessagingException {
        MimeStreamParser parser = getMimeStreamParser();
        parser.setContentHandler(new MessageHeaderParserContentHandler(part));
        try {
            parser.parse(headerInputStream);
        } catch (MimeException me2) {
            throw new MessagingException("Error parsing headers", me2);
        } catch (IOException e) {
            throw new MessagingException("I/O error parsing headers", e);
        }
    }

    private static MimeStreamParser getMimeStreamParser() {
        MimeConfig parserConfig = new MimeConfig();
        parserConfig.setMaxHeaderLen(-1);
        parserConfig.setMaxLineLen(-1);
        parserConfig.setMaxHeaderCount(-1);
        return new MimeStreamParser(parserConfig);
    }

    private static class MessageHeaderParserContentHandler implements ContentHandler {
        private final Part part;

        public MessageHeaderParserContentHandler(Part part2) {
            this.part = part2;
        }

        public void field(Field rawField) throws MimeException {
            this.part.addRawHeader(rawField.getName(), rawField.getRaw().toString());
        }

        public void startMessage() throws MimeException {
        }

        public void endMessage() throws MimeException {
        }

        public void startBodyPart() throws MimeException {
        }

        public void endBodyPart() throws MimeException {
        }

        public void startHeader() throws MimeException {
        }

        public void endHeader() throws MimeException {
        }

        public void preamble(InputStream is) throws MimeException, IOException {
        }

        public void epilogue(InputStream is) throws MimeException, IOException {
        }

        public void startMultipart(BodyDescriptor bd) throws MimeException {
        }

        public void endMultipart() throws MimeException {
        }

        public void body(BodyDescriptor bd, InputStream is) throws MimeException, IOException {
        }

        public void raw(InputStream is) throws MimeException, IOException {
        }
    }
}
