package com.fsck.k9.mail.internet;

import android.support.annotation.NonNull;
import com.fsck.k9.mail.Address;
import com.fsck.k9.mail.Body;
import com.fsck.k9.mail.BodyPart;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.Multipart;
import com.fsck.k9.mail.Part;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;
import org.apache.commons.io.IOUtils;
import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.dom.field.ContentTypeField;
import org.apache.james.mime4j.dom.field.DateTimeField;
import org.apache.james.mime4j.dom.field.FieldName;
import org.apache.james.mime4j.field.DefaultFieldParser;
import org.apache.james.mime4j.io.EOLConvertingInputStream;
import org.apache.james.mime4j.parser.ContentHandler;
import org.apache.james.mime4j.parser.MimeStreamParser;
import org.apache.james.mime4j.stream.BodyDescriptor;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.stream.MimeConfig;

public class MimeMessage extends Message {
    protected Address[] mBcc;
    private Body mBody;
    protected Address[] mCc;
    private SimpleDateFormat mDateFormat;
    protected Address[] mFrom;
    private MimeHeader mHeader = new MimeHeader();
    private String[] mInReplyTo;
    protected String mMessageId;
    private String[] mReferences;
    protected Address[] mReplyTo;
    private Date mSentDate;
    protected int mSize;
    protected Address[] mTo;
    private String serverExtra;

    public static MimeMessage parseMimeMessage(InputStream in, boolean recurse) throws IOException, MessagingException {
        MimeMessage mimeMessage = new MimeMessage();
        mimeMessage.parse(in, recurse);
        return mimeMessage;
    }

    public final void parse(InputStream in) throws IOException, MessagingException {
        parse(in, false);
    }

    private void parse(InputStream in, boolean recurse) throws IOException, MessagingException {
        this.mHeader.clear();
        this.mFrom = null;
        this.mTo = null;
        this.mCc = null;
        this.mBcc = null;
        this.mReplyTo = null;
        this.mMessageId = null;
        this.mReferences = null;
        this.mInReplyTo = null;
        this.mSentDate = null;
        this.mBody = null;
        MimeConfig parserConfig = new MimeConfig();
        parserConfig.setMaxHeaderLen(-1);
        parserConfig.setMaxLineLen(-1);
        parserConfig.setMaxHeaderCount(-1);
        MimeStreamParser parser = new MimeStreamParser(parserConfig);
        parser.setContentHandler(new MimeMessageBuilder());
        if (recurse) {
            parser.setRecurse();
        }
        try {
            parser.parse(new EOLConvertingInputStream(in));
        } catch (MimeException me2) {
            throw new MessagingException(me2.getMessage(), me2);
        }
    }

    public Date getSentDate() {
        if (this.mSentDate == null) {
            try {
                this.mSentDate = ((DateTimeField) DefaultFieldParser.parse("Date: " + MimeUtility.unfoldAndDecode(getFirstHeader(FieldName.DATE)))).getDate();
            } catch (Exception e) {
            }
        }
        return this.mSentDate;
    }

    public void addSentDate(Date sentDate, boolean hideTimeZone) {
        if (this.mDateFormat == null) {
            this.mDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z", Locale.US);
        }
        if (hideTimeZone) {
            this.mDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        }
        addHeader(FieldName.DATE, this.mDateFormat.format(sentDate));
        setInternalSentDate(sentDate);
    }

    public void setSentDate(Date sentDate, boolean hideTimeZone) {
        removeHeader(FieldName.DATE);
        addSentDate(sentDate, hideTimeZone);
    }

    public void setInternalSentDate(Date sentDate) {
        this.mSentDate = sentDate;
    }

    public String getContentType() {
        String contentType = getFirstHeader("Content-Type");
        return contentType == null ? ContentTypeField.TYPE_TEXT_PLAIN : MimeUtility.unfoldAndDecode(contentType);
    }

    public String getDisposition() {
        return MimeUtility.unfoldAndDecode(getFirstHeader("Content-Disposition"));
    }

    public String getContentId() {
        return null;
    }

    public String getMimeType() {
        return MimeUtility.getHeaderParameter(getContentType(), null);
    }

    public boolean isMimeType(String mimeType) {
        return getMimeType().equalsIgnoreCase(mimeType);
    }

    public int getSize() {
        return this.mSize;
    }

    public Address[] getRecipients(Message.RecipientType type) {
        switch (type) {
            case TO:
                if (this.mTo == null) {
                    this.mTo = Address.parse(MimeUtility.unfold(getFirstHeader(FieldName.TO)));
                }
                return this.mTo;
            case CC:
                if (this.mCc == null) {
                    this.mCc = Address.parse(MimeUtility.unfold(getFirstHeader("CC")));
                }
                return this.mCc;
            case BCC:
                if (this.mBcc == null) {
                    this.mBcc = Address.parse(MimeUtility.unfold(getFirstHeader("BCC")));
                }
                return this.mBcc;
            default:
                throw new IllegalArgumentException("Unrecognized recipient type.");
        }
    }

    public void setRecipients(Message.RecipientType type, Address[] addresses) {
        if (type == Message.RecipientType.TO) {
            if (addresses == null || addresses.length == 0) {
                removeHeader(FieldName.TO);
                this.mTo = null;
                return;
            }
            setHeader(FieldName.TO, Address.toEncodedString(addresses));
            this.mTo = addresses;
        } else if (type == Message.RecipientType.CC) {
            if (addresses == null || addresses.length == 0) {
                removeHeader("CC");
                this.mCc = null;
                return;
            }
            setHeader("CC", Address.toEncodedString(addresses));
            this.mCc = addresses;
        } else if (type != Message.RecipientType.BCC) {
            throw new IllegalStateException("Unrecognized recipient type.");
        } else if (addresses == null || addresses.length == 0) {
            removeHeader("BCC");
            this.mBcc = null;
        } else {
            setHeader("BCC", Address.toEncodedString(addresses));
            this.mBcc = addresses;
        }
    }

    public String getSubject() {
        return MimeUtility.unfoldAndDecode(getFirstHeader(FieldName.SUBJECT), this);
    }

    public void setSubject(String subject) {
        setHeader(FieldName.SUBJECT, subject);
    }

    public Address[] getFrom() {
        if (this.mFrom == null) {
            String list = MimeUtility.unfold(getFirstHeader(FieldName.FROM));
            if (list == null || list.length() == 0) {
                list = MimeUtility.unfold(getFirstHeader(FieldName.SENDER));
            }
            this.mFrom = Address.parse(list);
        }
        return this.mFrom;
    }

    public void setFrom(Address from) {
        if (from != null) {
            setHeader(FieldName.FROM, from.toEncodedString());
            this.mFrom = new Address[]{from};
            return;
        }
        this.mFrom = null;
    }

    public Address[] getReplyTo() {
        if (this.mReplyTo == null) {
            this.mReplyTo = Address.parse(MimeUtility.unfold(getFirstHeader("Reply-to")));
        }
        return this.mReplyTo;
    }

    public void setReplyTo(Address[] replyTo) {
        if (replyTo == null || replyTo.length == 0) {
            removeHeader("Reply-to");
            this.mReplyTo = null;
            return;
        }
        setHeader("Reply-to", Address.toEncodedString(replyTo));
        this.mReplyTo = replyTo;
    }

    public String getMessageId() {
        if (this.mMessageId == null) {
            this.mMessageId = getFirstHeader(FieldName.MESSAGE_ID);
        }
        return this.mMessageId;
    }

    public void setMessageId(String messageId) {
        setHeader(FieldName.MESSAGE_ID, messageId);
        this.mMessageId = messageId;
    }

    public void setInReplyTo(String inReplyTo) {
        setHeader("In-Reply-To", inReplyTo);
    }

    public String[] getReferences() {
        if (this.mReferences == null) {
            this.mReferences = getHeader("References");
        }
        return this.mReferences;
    }

    public void setReferences(String references) {
        String references2 = references.replaceAll("\\s+", " ");
        int originalLength = references2.length();
        if (originalLength >= 985) {
            int start = references2.indexOf(60);
            String firstReference = references2.substring(start, references2.indexOf(60, start + 1));
            references2 = firstReference + references2.substring(references2.indexOf(60, (firstReference.length() + originalLength) - 985));
        }
        setHeader("References", references2);
    }

    public Body getBody() {
        return this.mBody;
    }

    public void setBody(Body body) {
        this.mBody = body;
    }

    private String getFirstHeader(String name) {
        return this.mHeader.getFirstHeader(name);
    }

    public void addHeader(String name, String value) {
        this.mHeader.addHeader(name, value);
    }

    public void addRawHeader(String name, String raw) {
        this.mHeader.addRawHeader(name, raw);
    }

    public void setHeader(String name, String value) {
        this.mHeader.setHeader(name, value);
    }

    @NonNull
    public String[] getHeader(String name) {
        return this.mHeader.getHeader(name);
    }

    public void removeHeader(String name) {
        this.mHeader.removeHeader(name);
    }

    public Set<String> getHeaderNames() {
        return this.mHeader.getHeaderNames();
    }

    public void writeTo(OutputStream out) throws IOException, MessagingException {
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out), 1024);
        this.mHeader.writeTo(out);
        writer.write("\r\n");
        writer.flush();
        if (this.mBody != null) {
            this.mBody.writeTo(out);
        }
    }

    public void writeHeaderTo(OutputStream out) throws IOException, MessagingException {
        this.mHeader.writeTo(out);
    }

    public InputStream getInputStream() throws MessagingException {
        return null;
    }

    public void setEncoding(String encoding) throws MessagingException {
        if (this.mBody != null) {
            this.mBody.setEncoding(encoding);
        }
        setHeader("Content-Transfer-Encoding", encoding);
    }

    public void setCharset(String charset) throws MessagingException {
        this.mHeader.setCharset(charset);
        if (this.mBody instanceof Multipart) {
            ((Multipart) this.mBody).setCharset(charset);
        } else if (this.mBody instanceof TextBody) {
            CharsetSupport.setCharset(charset, this);
            ((TextBody) this.mBody).setCharset(charset);
        }
    }

    private class MimeMessageBuilder implements ContentHandler {
        private final LinkedList<Object> stack = new LinkedList<>();

        public MimeMessageBuilder() {
        }

        private void expect(Class<?> c) {
            if (!c.isInstance(this.stack.peek())) {
                throw new IllegalStateException("Internal stack error: Expected '" + c.getName() + "' found '" + this.stack.peek().getClass().getName() + "'");
            }
        }

        public void startMessage() {
            if (this.stack.isEmpty()) {
                this.stack.addFirst(MimeMessage.this);
                return;
            }
            expect(Part.class);
            MimeMessage m = new MimeMessage();
            ((Part) this.stack.peek()).setBody(m);
            this.stack.addFirst(m);
        }

        public void endMessage() {
            expect(MimeMessage.class);
            this.stack.removeFirst();
        }

        public void startHeader() {
            expect(Part.class);
        }

        public void endHeader() {
            expect(Part.class);
        }

        public void startMultipart(BodyDescriptor bd) throws MimeException {
            expect(Part.class);
            MimeMultipart multiPart = new MimeMultipart(bd.getMimeType(), bd.getBoundary());
            ((Part) this.stack.peek()).setBody(multiPart);
            this.stack.addFirst(multiPart);
        }

        public void body(BodyDescriptor bd, InputStream in) throws IOException, MimeException {
            expect(Part.class);
            ((Part) this.stack.peek()).setBody(MimeUtility.createBody(in, bd.getTransferEncoding(), bd.getMimeType()));
        }

        public void endMultipart() {
            boolean hasNoBodyParts;
            boolean hasNoEpilogue;
            expect(Multipart.class);
            Multipart multipart = (Multipart) this.stack.removeFirst();
            if (multipart.getCount() == 0) {
                hasNoBodyParts = true;
            } else {
                hasNoBodyParts = false;
            }
            if (multipart.getEpilogue() == null) {
                hasNoEpilogue = true;
            } else {
                hasNoEpilogue = false;
            }
            if (hasNoBodyParts && hasNoEpilogue) {
                expect(Part.class);
                ((Part) this.stack.peek()).setBody(null);
            }
        }

        public void startBodyPart() throws MimeException {
            expect(MimeMultipart.class);
            try {
                MimeBodyPart bodyPart = new MimeBodyPart();
                ((MimeMultipart) this.stack.peek()).addBodyPart(bodyPart);
                this.stack.addFirst(bodyPart);
            } catch (MessagingException me2) {
                throw new MimeException(me2);
            }
        }

        public void endBodyPart() {
            expect(BodyPart.class);
            this.stack.removeFirst();
        }

        public void preamble(InputStream is) throws IOException {
            expect(MimeMultipart.class);
            ByteArrayOutputStream preamble = new ByteArrayOutputStream();
            IOUtils.copy(is, preamble);
            ((MimeMultipart) this.stack.peek()).setPreamble(preamble.toByteArray());
        }

        public void epilogue(InputStream is) throws IOException {
            expect(MimeMultipart.class);
            ByteArrayOutputStream epilogue = new ByteArrayOutputStream();
            IOUtils.copy(is, epilogue);
            ((MimeMultipart) this.stack.peek()).setEpilogue(epilogue.toByteArray());
        }

        public void raw(InputStream is) throws IOException {
            throw new UnsupportedOperationException("Not supported");
        }

        public void field(Field parsedField) throws MimeException {
            expect(Part.class);
            ((Part) this.stack.peek()).addRawHeader(parsedField.getName(), parsedField.getRaw().toString());
        }
    }

    /* access modifiers changed from: protected */
    public void copy(MimeMessage destination) {
        super.copy((Message) destination);
        destination.mHeader = this.mHeader.clone();
        destination.mBody = this.mBody;
        destination.mMessageId = this.mMessageId;
        destination.mSentDate = this.mSentDate;
        destination.mDateFormat = this.mDateFormat;
        destination.mSize = this.mSize;
        destination.mFrom = this.mFrom;
        destination.mTo = this.mTo;
        destination.mCc = this.mCc;
        destination.mBcc = this.mBcc;
        destination.mReplyTo = this.mReplyTo;
        destination.mReferences = this.mReferences;
        destination.mInReplyTo = this.mInReplyTo;
    }

    public MimeMessage clone() {
        MimeMessage message = new MimeMessage();
        copy(message);
        return message;
    }

    public long getId() {
        return Long.parseLong(this.mUid);
    }

    public boolean hasAttachments() {
        return false;
    }

    public String getServerExtra() {
        return this.serverExtra;
    }

    public void setServerExtra(String serverExtra2) {
        this.serverExtra = serverExtra2;
    }

    public MimeBodyPart toBodyPart() throws MessagingException {
        MimeHeader contentHeaders = new MimeHeader();
        for (String header : this.mHeader.getHeaderNames()) {
            if (header.toLowerCase().startsWith("content-")) {
                for (String value : this.mHeader.getHeader(header)) {
                    contentHeaders.addHeader(header, value);
                }
            }
        }
        return new MimeBodyPart(contentHeaders, getBody());
    }
}
