package com.fsck.k9.mail.filter;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class SmtpDataStuffing extends FilterOutputStream {
    private static final int STATE_CR = 1;
    private static final int STATE_CRLF = 2;
    private static final int STATE_NORMAL = 0;
    private int state = 0;

    public SmtpDataStuffing(OutputStream out) {
        super(out);
    }

    public void write(int oneByte) throws IOException {
        if (oneByte == 13) {
            this.state = 1;
        } else if (this.state == 1 && oneByte == 10) {
            this.state = 2;
        } else if (this.state == 2 && oneByte == 46) {
            super.write(46);
            this.state = 0;
        } else {
            this.state = 0;
        }
        super.write(oneByte);
    }
}
