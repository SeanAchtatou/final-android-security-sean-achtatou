package com.fsck.k9.mail.filter;

import com.fsck.k9.ui.messageview.MessageCryptoPresenter;
import java.math.BigInteger;
import java.nio.charset.Charset;

public class Base64 {
    static final byte[] CHUNK_SEPARATOR = {13, 10};
    static final int CHUNK_SIZE = 76;
    private static final int MASK_6BITS = 63;
    private static final int MASK_8BITS = 255;
    private static final byte PAD = 61;
    private static final byte[] base64ToInt;
    private static final byte[] intToBase64 = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
    private byte[] buf;
    private int currentLinePos;
    private final int decodeSize;
    private final int encodeSize;
    private boolean eof;
    private final int lineLength;
    private final byte[] lineSeparator;
    private int modulus;
    private int pos;
    private int readPos;
    private int x;

    public static String decode(String encoded) {
        if (encoded == null) {
            return null;
        }
        return new String(new Base64().decode(encoded.getBytes()));
    }

    public static String encode(String s) {
        if (s == null) {
            return null;
        }
        return new String(new Base64().encode(s.getBytes()));
    }

    static {
        byte[] bArr = new byte[MessageCryptoPresenter.REQUEST_CODE_UNKNOWN_KEY];
        // fill-array-data instruction
        bArr[0] = -1;
        bArr[1] = -1;
        bArr[2] = -1;
        bArr[3] = -1;
        bArr[4] = -1;
        bArr[5] = -1;
        bArr[6] = -1;
        bArr[7] = -1;
        bArr[8] = -1;
        bArr[9] = -1;
        bArr[10] = -1;
        bArr[11] = -1;
        bArr[12] = -1;
        bArr[13] = -1;
        bArr[14] = -1;
        bArr[15] = -1;
        bArr[16] = -1;
        bArr[17] = -1;
        bArr[18] = -1;
        bArr[19] = -1;
        bArr[20] = -1;
        bArr[21] = -1;
        bArr[22] = -1;
        bArr[23] = -1;
        bArr[24] = -1;
        bArr[25] = -1;
        bArr[26] = -1;
        bArr[27] = -1;
        bArr[28] = -1;
        bArr[29] = -1;
        bArr[30] = -1;
        bArr[31] = -1;
        bArr[32] = -1;
        bArr[33] = -1;
        bArr[34] = -1;
        bArr[35] = -1;
        bArr[36] = -1;
        bArr[37] = -1;
        bArr[38] = -1;
        bArr[39] = -1;
        bArr[40] = -1;
        bArr[41] = -1;
        bArr[42] = -1;
        bArr[43] = 62;
        bArr[44] = -1;
        bArr[45] = -1;
        bArr[46] = -1;
        bArr[47] = 63;
        bArr[48] = 52;
        bArr[49] = 53;
        bArr[50] = 54;
        bArr[51] = 55;
        bArr[52] = 56;
        bArr[53] = 57;
        bArr[54] = 58;
        bArr[55] = 59;
        bArr[56] = 60;
        bArr[57] = 61;
        bArr[58] = -1;
        bArr[59] = -1;
        bArr[60] = -1;
        bArr[61] = -1;
        bArr[62] = -1;
        bArr[63] = -1;
        bArr[64] = -1;
        bArr[65] = 0;
        bArr[66] = 1;
        bArr[67] = 2;
        bArr[68] = 3;
        bArr[69] = 4;
        bArr[70] = 5;
        bArr[71] = 6;
        bArr[72] = 7;
        bArr[73] = 8;
        bArr[74] = 9;
        bArr[75] = 10;
        bArr[76] = 11;
        bArr[77] = 12;
        bArr[78] = 13;
        bArr[79] = 14;
        bArr[80] = 15;
        bArr[81] = 16;
        bArr[82] = 17;
        bArr[83] = 18;
        bArr[84] = 19;
        bArr[85] = 20;
        bArr[86] = 21;
        bArr[87] = 22;
        bArr[88] = 23;
        bArr[89] = 24;
        bArr[90] = 25;
        bArr[91] = -1;
        bArr[92] = -1;
        bArr[93] = -1;
        bArr[94] = -1;
        bArr[95] = -1;
        bArr[96] = -1;
        bArr[97] = 26;
        bArr[98] = 27;
        bArr[99] = 28;
        bArr[100] = 29;
        bArr[101] = 30;
        bArr[102] = 31;
        bArr[103] = 32;
        bArr[104] = 33;
        bArr[105] = 34;
        bArr[106] = 35;
        bArr[107] = 36;
        bArr[108] = 37;
        bArr[109] = 38;
        bArr[110] = 39;
        bArr[111] = 40;
        bArr[112] = 41;
        bArr[113] = 42;
        bArr[114] = 43;
        bArr[115] = 44;
        bArr[116] = 45;
        bArr[117] = 46;
        bArr[118] = 47;
        bArr[119] = 48;
        bArr[120] = 49;
        bArr[121] = 50;
        bArr[122] = 51;
        base64ToInt = bArr;
    }

    public Base64() {
        this(76, CHUNK_SEPARATOR);
    }

    public Base64(int lineLength2) {
        this(lineLength2, CHUNK_SEPARATOR);
    }

    public Base64(int lineLength2, byte[] lineSeparator2) {
        this.lineLength = lineLength2;
        this.lineSeparator = new byte[lineSeparator2.length];
        System.arraycopy(lineSeparator2, 0, this.lineSeparator, 0, lineSeparator2.length);
        if (lineLength2 > 0) {
            this.encodeSize = lineSeparator2.length + 4;
        } else {
            this.encodeSize = 4;
        }
        this.decodeSize = this.encodeSize - 1;
        if (containsBase64Byte(lineSeparator2)) {
            throw new IllegalArgumentException("lineSeperator must not contain base64 characters: [" + new String(lineSeparator2, Charset.forName("UTF-8")) + "]");
        }
    }

    /* access modifiers changed from: package-private */
    public boolean hasData() {
        return this.buf != null;
    }

    /* access modifiers changed from: package-private */
    public int avail() {
        if (this.buf != null) {
            return this.pos - this.readPos;
        }
        return 0;
    }

    private void resizeBuf() {
        if (this.buf == null) {
            this.buf = new byte[8192];
            this.pos = 0;
            this.readPos = 0;
            return;
        }
        byte[] b = new byte[(this.buf.length * 2)];
        System.arraycopy(this.buf, 0, b, 0, this.buf.length);
        this.buf = b;
    }

    /* access modifiers changed from: package-private */
    public int readResults(byte[] b, int bPos, int bAvail) {
        if (this.buf != null) {
            int len = Math.min(avail(), bAvail);
            if (this.buf != b) {
                System.arraycopy(this.buf, this.readPos, b, bPos, len);
                this.readPos += len;
                if (this.readPos < this.pos) {
                    return len;
                }
                this.buf = null;
                return len;
            }
            this.buf = null;
            return len;
        }
        return this.eof ? -1 : 0;
    }

    /* access modifiers changed from: package-private */
    public void setInitialBuffer(byte[] out, int outPos, int outAvail) {
        if (out != null && out.length == outAvail) {
            this.buf = out;
            this.pos = outPos;
            this.readPos = outPos;
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v3, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v4, resolved type: byte} */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void encode(byte[] r10, int r11, int r12) {
        /*
            r9 = this;
            r8 = 61
            r7 = 0
            boolean r3 = r9.eof
            if (r3 == 0) goto L_0x0008
        L_0x0007:
            return
        L_0x0008:
            if (r12 >= 0) goto L_0x00c1
            r3 = 1
            r9.eof = r3
            byte[] r3 = r9.buf
            if (r3 == 0) goto L_0x001b
            byte[] r3 = r9.buf
            int r3 = r3.length
            int r4 = r9.pos
            int r3 = r3 - r4
            int r4 = r9.encodeSize
            if (r3 >= r4) goto L_0x001e
        L_0x001b:
            r9.resizeBuf()
        L_0x001e:
            int r3 = r9.modulus
            switch(r3) {
                case 1: goto L_0x003c;
                case 2: goto L_0x0079;
                default: goto L_0x0023;
            }
        L_0x0023:
            int r3 = r9.lineLength
            if (r3 <= 0) goto L_0x0007
            byte[] r3 = r9.lineSeparator
            byte[] r4 = r9.buf
            int r5 = r9.pos
            byte[] r6 = r9.lineSeparator
            int r6 = r6.length
            java.lang.System.arraycopy(r3, r7, r4, r5, r6)
            int r3 = r9.pos
            byte[] r4 = r9.lineSeparator
            int r4 = r4.length
            int r3 = r3 + r4
            r9.pos = r3
            goto L_0x0007
        L_0x003c:
            byte[] r3 = r9.buf
            int r4 = r9.pos
            int r5 = r4 + 1
            r9.pos = r5
            byte[] r5 = com.fsck.k9.mail.filter.Base64.intToBase64
            int r6 = r9.x
            int r6 = r6 >> 2
            r6 = r6 & 63
            byte r5 = r5[r6]
            r3[r4] = r5
            byte[] r3 = r9.buf
            int r4 = r9.pos
            int r5 = r4 + 1
            r9.pos = r5
            byte[] r5 = com.fsck.k9.mail.filter.Base64.intToBase64
            int r6 = r9.x
            int r6 = r6 << 4
            r6 = r6 & 63
            byte r5 = r5[r6]
            r3[r4] = r5
            byte[] r3 = r9.buf
            int r4 = r9.pos
            int r5 = r4 + 1
            r9.pos = r5
            r3[r4] = r8
            byte[] r3 = r9.buf
            int r4 = r9.pos
            int r5 = r4 + 1
            r9.pos = r5
            r3[r4] = r8
            goto L_0x0023
        L_0x0079:
            byte[] r3 = r9.buf
            int r4 = r9.pos
            int r5 = r4 + 1
            r9.pos = r5
            byte[] r5 = com.fsck.k9.mail.filter.Base64.intToBase64
            int r6 = r9.x
            int r6 = r6 >> 10
            r6 = r6 & 63
            byte r5 = r5[r6]
            r3[r4] = r5
            byte[] r3 = r9.buf
            int r4 = r9.pos
            int r5 = r4 + 1
            r9.pos = r5
            byte[] r5 = com.fsck.k9.mail.filter.Base64.intToBase64
            int r6 = r9.x
            int r6 = r6 >> 4
            r6 = r6 & 63
            byte r5 = r5[r6]
            r3[r4] = r5
            byte[] r3 = r9.buf
            int r4 = r9.pos
            int r5 = r4 + 1
            r9.pos = r5
            byte[] r5 = com.fsck.k9.mail.filter.Base64.intToBase64
            int r6 = r9.x
            int r6 = r6 << 2
            r6 = r6 & 63
            byte r5 = r5[r6]
            r3[r4] = r5
            byte[] r3 = r9.buf
            int r4 = r9.pos
            int r5 = r4 + 1
            r9.pos = r5
            r3[r4] = r8
            goto L_0x0023
        L_0x00c1:
            r1 = 0
            r2 = r11
        L_0x00c3:
            if (r1 >= r12) goto L_0x016c
            byte[] r3 = r9.buf
            if (r3 == 0) goto L_0x00d3
            byte[] r3 = r9.buf
            int r3 = r3.length
            int r4 = r9.pos
            int r3 = r3 - r4
            int r4 = r9.encodeSize
            if (r3 >= r4) goto L_0x00d6
        L_0x00d3:
            r9.resizeBuf()
        L_0x00d6:
            int r3 = r9.modulus
            int r3 = r3 + 1
            r9.modulus = r3
            int r3 = r3 % 3
            r9.modulus = r3
            int r11 = r2 + 1
            byte r0 = r10[r2]
            if (r0 >= 0) goto L_0x00e8
            int r0 = r0 + 256
        L_0x00e8:
            int r3 = r9.x
            int r3 = r3 << 8
            int r3 = r3 + r0
            r9.x = r3
            int r3 = r9.modulus
            if (r3 != 0) goto L_0x0167
            byte[] r3 = r9.buf
            int r4 = r9.pos
            int r5 = r4 + 1
            r9.pos = r5
            byte[] r5 = com.fsck.k9.mail.filter.Base64.intToBase64
            int r6 = r9.x
            int r6 = r6 >> 18
            r6 = r6 & 63
            byte r5 = r5[r6]
            r3[r4] = r5
            byte[] r3 = r9.buf
            int r4 = r9.pos
            int r5 = r4 + 1
            r9.pos = r5
            byte[] r5 = com.fsck.k9.mail.filter.Base64.intToBase64
            int r6 = r9.x
            int r6 = r6 >> 12
            r6 = r6 & 63
            byte r5 = r5[r6]
            r3[r4] = r5
            byte[] r3 = r9.buf
            int r4 = r9.pos
            int r5 = r4 + 1
            r9.pos = r5
            byte[] r5 = com.fsck.k9.mail.filter.Base64.intToBase64
            int r6 = r9.x
            int r6 = r6 >> 6
            r6 = r6 & 63
            byte r5 = r5[r6]
            r3[r4] = r5
            byte[] r3 = r9.buf
            int r4 = r9.pos
            int r5 = r4 + 1
            r9.pos = r5
            byte[] r5 = com.fsck.k9.mail.filter.Base64.intToBase64
            int r6 = r9.x
            r6 = r6 & 63
            byte r5 = r5[r6]
            r3[r4] = r5
            int r3 = r9.currentLinePos
            int r3 = r3 + 4
            r9.currentLinePos = r3
            int r3 = r9.lineLength
            if (r3 <= 0) goto L_0x0167
            int r3 = r9.lineLength
            int r4 = r9.currentLinePos
            if (r3 > r4) goto L_0x0167
            byte[] r3 = r9.lineSeparator
            byte[] r4 = r9.buf
            int r5 = r9.pos
            byte[] r6 = r9.lineSeparator
            int r6 = r6.length
            java.lang.System.arraycopy(r3, r7, r4, r5, r6)
            int r3 = r9.pos
            byte[] r4 = r9.lineSeparator
            int r4 = r4.length
            int r3 = r3 + r4
            r9.pos = r3
            r9.currentLinePos = r7
        L_0x0167:
            int r1 = r1 + 1
            r2 = r11
            goto L_0x00c3
        L_0x016c:
            r11 = r2
            goto L_0x0007
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fsck.k9.mail.filter.Base64.encode(byte[], int, int):void");
    }

    /* access modifiers changed from: package-private */
    public void decode(byte[] in, int inPos, int inAvail) {
        byte b;
        if (!this.eof) {
            if (inAvail < 0) {
                this.eof = true;
            }
            int i = 0;
            int inPos2 = inPos;
            while (i < inAvail) {
                if (this.buf == null || this.buf.length - this.pos < this.decodeSize) {
                    resizeBuf();
                }
                int inPos3 = inPos2 + 1;
                byte b2 = in[inPos2];
                if (b2 == 61) {
                    this.x <<= 6;
                    switch (this.modulus) {
                        case 2:
                            this.x <<= 6;
                            byte[] bArr = this.buf;
                            int i2 = this.pos;
                            this.pos = i2 + 1;
                            bArr[i2] = (byte) ((this.x >> 16) & 255);
                            break;
                        case 3:
                            byte[] bArr2 = this.buf;
                            int i3 = this.pos;
                            this.pos = i3 + 1;
                            bArr2[i3] = (byte) ((this.x >> 16) & 255);
                            byte[] bArr3 = this.buf;
                            int i4 = this.pos;
                            this.pos = i4 + 1;
                            bArr3[i4] = (byte) ((this.x >> 8) & 255);
                            break;
                    }
                    this.eof = true;
                    return;
                }
                if (b2 >= 0 && b2 < base64ToInt.length && (b = base64ToInt[b2]) >= 0) {
                    int i5 = this.modulus + 1;
                    this.modulus = i5;
                    this.modulus = i5 % 4;
                    this.x = (this.x << 6) + b;
                    if (this.modulus == 0) {
                        byte[] bArr4 = this.buf;
                        int i6 = this.pos;
                        this.pos = i6 + 1;
                        bArr4[i6] = (byte) ((this.x >> 16) & 255);
                        byte[] bArr5 = this.buf;
                        int i7 = this.pos;
                        this.pos = i7 + 1;
                        bArr5[i7] = (byte) ((this.x >> 8) & 255);
                        byte[] bArr6 = this.buf;
                        int i8 = this.pos;
                        this.pos = i8 + 1;
                        bArr6[i8] = (byte) (this.x & 255);
                    }
                }
                i++;
                inPos2 = inPos3;
            }
        }
    }

    public static boolean isBase64(byte octet) {
        return octet == 61 || (octet >= 0 && octet < base64ToInt.length && base64ToInt[octet] != -1);
    }

    public static boolean isArrayByteBase64(byte[] arrayOctet) {
        for (byte anArrayOctet : arrayOctet) {
            if (!isBase64(anArrayOctet) && !isWhiteSpace(anArrayOctet)) {
                return false;
            }
        }
        return true;
    }

    private static boolean containsBase64Byte(byte[] arrayOctet) {
        for (byte element : arrayOctet) {
            if (isBase64(element)) {
                return true;
            }
        }
        return false;
    }

    public static byte[] encodeBase64(byte[] binaryData) {
        return encodeBase64(binaryData, false);
    }

    public static byte[] encodeBase64Chunked(byte[] binaryData) {
        return encodeBase64(binaryData, true);
    }

    public Object decode(Object pObject) throws DecoderException {
        if (pObject instanceof byte[]) {
            return decode((byte[]) ((byte[]) pObject));
        }
        throw new DecoderException("Parameter supplied to Base64 decode is not a byte[]");
    }

    public byte[] decode(byte[] pArray) {
        return decodeBase64(pArray);
    }

    public static byte[] encodeBase64(byte[] binaryData, boolean isChunked) {
        Base64 b64;
        if (binaryData == null || binaryData.length == 0) {
            return binaryData;
        }
        if (isChunked) {
            b64 = new Base64();
        } else {
            b64 = new Base64(0);
        }
        long len = (long) ((binaryData.length * 4) / 3);
        long mod = len % 4;
        if (mod != 0) {
            len += 4 - mod;
        }
        if (isChunked) {
            len += (1 + (len / 76)) * ((long) CHUNK_SEPARATOR.length);
        }
        if (len > 2147483647L) {
            throw new IllegalArgumentException("Input array too big, output array would be bigger than Integer.MAX_VALUE=2147483647");
        }
        byte[] buf2 = new byte[((int) len)];
        b64.setInitialBuffer(buf2, 0, buf2.length);
        b64.encode(binaryData, 0, binaryData.length);
        b64.encode(binaryData, 0, -1);
        if (b64.buf == buf2) {
            return buf2;
        }
        b64.readResults(buf2, 0, buf2.length);
        return buf2;
    }

    public static byte[] decodeBase64(byte[] base64Data) {
        if (base64Data == null || base64Data.length == 0) {
            return base64Data;
        }
        Base64 b64 = new Base64();
        byte[] buf2 = new byte[((int) ((long) ((base64Data.length * 3) / 4)))];
        b64.setInitialBuffer(buf2, 0, buf2.length);
        b64.decode(base64Data, 0, base64Data.length);
        b64.decode(base64Data, 0, -1);
        byte[] result = new byte[b64.pos];
        b64.readResults(result, 0, result.length);
        return result;
    }

    private static boolean isWhiteSpace(byte byteToCheck) {
        switch (byteToCheck) {
            case 9:
            case 10:
            case 13:
            case 32:
                return true;
            default:
                return false;
        }
    }

    static byte[] discardNonBase64(byte[] data) {
        int bytesCopied;
        byte[] groomedData = new byte[data.length];
        int length = data.length;
        int i = 0;
        int bytesCopied2 = 0;
        while (i < length) {
            byte element = data[i];
            if (isBase64(element)) {
                bytesCopied = bytesCopied2 + 1;
                groomedData[bytesCopied2] = element;
            } else {
                bytesCopied = bytesCopied2;
            }
            i++;
            bytesCopied2 = bytesCopied;
        }
        byte[] packedData = new byte[bytesCopied2];
        System.arraycopy(groomedData, 0, packedData, 0, bytesCopied2);
        return packedData;
    }

    public Object encode(Object pObject) throws EncoderException {
        if (pObject instanceof byte[]) {
            return encode((byte[]) ((byte[]) pObject));
        }
        throw new EncoderException("Parameter supplied to Base64 encode is not a byte[]");
    }

    public byte[] encode(byte[] pArray) {
        return encodeBase64(pArray, false);
    }

    public static BigInteger decodeInteger(byte[] pArray) {
        return new BigInteger(1, decodeBase64(pArray));
    }

    public static byte[] encodeInteger(BigInteger bigInt) {
        if (bigInt != null) {
            return encodeBase64(toIntegerBytes(bigInt), false);
        }
        throw new NullPointerException("encodeInteger called with null parameter");
    }

    static byte[] toIntegerBytes(BigInteger bigInt) {
        int bitlen = ((bigInt.bitLength() + 7) >> 3) << 3;
        byte[] bigBytes = bigInt.toByteArray();
        if (bigInt.bitLength() % 8 != 0 && (bigInt.bitLength() / 8) + 1 == bitlen / 8) {
            return bigBytes;
        }
        int startSrc = 0;
        int len = bigBytes.length;
        if (bigInt.bitLength() % 8 == 0) {
            startSrc = 1;
            len--;
        }
        byte[] resizedBytes = new byte[(bitlen / 8)];
        System.arraycopy(bigBytes, startSrc, resizedBytes, (bitlen / 8) - len, len);
        return resizedBytes;
    }

    static class DecoderException extends Exception {
        private static final long serialVersionUID = -3786485780312120437L;

        DecoderException(String error) {
            super(error);
        }
    }

    static class EncoderException extends Exception {
        private static final long serialVersionUID = -5204809025392124652L;

        EncoderException(String error) {
            super(error);
        }
    }
}
