package com.fsck.k9.mail.internet;

import android.util.Log;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.filter.Base64OutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.apache.commons.io.IOUtils;
import org.apache.james.mime4j.codec.QuotedPrintableOutputStream;
import org.apache.james.mime4j.util.MimeUtil;

public class BinaryTempFileBody implements RawDataBody, SizeAware {
    private static File mTempDirectory;
    String mEncoding = null;
    /* access modifiers changed from: private */
    public File mFile;

    public static void setTempDirectory(File tempDirectory) {
        mTempDirectory = tempDirectory;
    }

    public static File getTempDirectory() {
        return mTempDirectory;
    }

    public String getEncoding() {
        return this.mEncoding;
    }

    public void setEncoding(String encoding) throws MessagingException {
        OutputStream out;
        OutputStream wrappedOut;
        InputStream in;
        if (this.mEncoding != null && this.mEncoding.equalsIgnoreCase(encoding)) {
            return;
        }
        if (!MimeUtil.ENC_8BIT.equalsIgnoreCase(this.mEncoding)) {
            throw new RuntimeException("Can't convert from encoding: " + this.mEncoding);
        }
        try {
            File newFile = File.createTempFile("body", null, mTempDirectory);
            out = new FileOutputStream(newFile);
            if (MimeUtil.ENC_QUOTED_PRINTABLE.equals(encoding)) {
                wrappedOut = new QuotedPrintableOutputStream(out, false);
            } else if (MimeUtil.ENC_BASE64.equals(encoding)) {
                wrappedOut = new Base64OutputStream(out);
            } else {
                throw new RuntimeException("Target encoding not supported: " + encoding);
            }
            in = getInputStream();
            IOUtils.copy(in, wrappedOut);
            IOUtils.closeQuietly(in);
            IOUtils.closeQuietly(wrappedOut);
            IOUtils.closeQuietly(out);
            this.mFile = newFile;
            this.mEncoding = encoding;
        } catch (IOException e) {
            throw new MessagingException("Unable to convert body", e);
        } catch (Throwable th) {
            IOUtils.closeQuietly(out);
            throw th;
        }
    }

    public BinaryTempFileBody(String encoding) {
        if (mTempDirectory == null) {
            throw new RuntimeException("setTempDirectory has not been called on BinaryTempFileBody!");
        }
        this.mEncoding = encoding;
    }

    public OutputStream getOutputStream() throws IOException {
        this.mFile = File.createTempFile("body", null, mTempDirectory);
        this.mFile.deleteOnExit();
        return new FileOutputStream(this.mFile);
    }

    public InputStream getInputStream() throws MessagingException {
        try {
            return new BinaryTempFileBodyInputStream(new FileInputStream(this.mFile));
        } catch (IOException ioe) {
            throw new MessagingException("Unable to open body", ioe);
        }
    }

    public void writeTo(OutputStream out) throws IOException, MessagingException {
        InputStream in = getInputStream();
        try {
            IOUtils.copy(in, out);
        } finally {
            IOUtils.closeQuietly(in);
        }
    }

    public long getSize() {
        return this.mFile.length();
    }

    public File getFile() {
        return this.mFile;
    }

    class BinaryTempFileBodyInputStream extends FilterInputStream {
        public BinaryTempFileBodyInputStream(InputStream in) {
            super(in);
        }

        public void close() throws IOException {
            String str;
            String str2;
            try {
                super.close();
            } finally {
                str = "k9";
                str2 = "deleting temp file";
                Log.d(str, str2);
                BinaryTempFileBody.this.mFile.delete();
            }
        }

        public void closeWithoutDeleting() throws IOException {
            super.close();
        }
    }
}
