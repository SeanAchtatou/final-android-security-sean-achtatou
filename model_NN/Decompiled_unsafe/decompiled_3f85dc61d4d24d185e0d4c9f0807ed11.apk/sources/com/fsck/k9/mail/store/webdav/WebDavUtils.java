package com.fsck.k9.mail.store.webdav;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

class WebDavUtils {
    WebDavUtils() {
    }

    static String processException(Throwable t) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        t.printStackTrace(ps);
        ps.close();
        return baos.toString();
    }
}
