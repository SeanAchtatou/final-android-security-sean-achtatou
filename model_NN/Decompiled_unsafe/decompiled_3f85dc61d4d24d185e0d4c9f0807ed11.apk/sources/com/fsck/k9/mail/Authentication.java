package com.fsck.k9.mail;

import com.fsck.k9.mail.filter.Base64;
import com.fsck.k9.mail.filter.Hex;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;

public class Authentication {
    private static final String US_ASCII = "US-ASCII";
    private static final String XOAUTH_FORMAT = "user=%1s\u0001auth=Bearer %2s\u0001\u0001";

    public static String computeCramMd5(String username, String password, String b64Nonce) throws MessagingException {
        try {
            return new String(computeCramMd5Bytes(username, password, b64Nonce.getBytes(US_ASCII)), US_ASCII);
        } catch (MessagingException e) {
            throw e;
        } catch (Exception e2) {
            throw new MessagingException("This shouldn't happen", e2);
        }
    }

    public static byte[] computeCramMd5Bytes(String username, String password, byte[] b64Nonce) throws MessagingException {
        try {
            byte[] nonce = Base64.decodeBase64(b64Nonce);
            byte[] secretBytes = password.getBytes();
            MessageDigest md = MessageDigest.getInstance("MD5");
            if (secretBytes.length > 64) {
                secretBytes = md.digest(secretBytes);
            }
            byte[] ipad = new byte[64];
            byte[] opad = new byte[64];
            System.arraycopy(secretBytes, 0, ipad, 0, secretBytes.length);
            System.arraycopy(secretBytes, 0, opad, 0, secretBytes.length);
            for (int i = 0; i < ipad.length; i++) {
                ipad[i] = (byte) (ipad[i] ^ 54);
            }
            for (int i2 = 0; i2 < opad.length; i2++) {
                opad[i2] = (byte) (opad[i2] ^ 92);
            }
            md.update(ipad);
            byte[] firstPass = md.digest(nonce);
            md.update(opad);
            return Base64.encodeBase64((username + " " + new String(Hex.encodeHex(md.digest(firstPass)))).getBytes());
        } catch (Exception e) {
            throw new MessagingException("Something went wrong during CRAM-MD5 computation", e);
        }
    }

    public static String computeXoauth(String username, String authToken) throws UnsupportedEncodingException {
        return new String(Base64.encodeBase64(String.format(XOAUTH_FORMAT, username, authToken).getBytes()), US_ASCII);
    }
}
