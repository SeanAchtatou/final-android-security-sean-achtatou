package com.fsck.k9.mail.ssl;

import com.fsck.k9.mail.MessagingException;
import java.io.IOException;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

public interface TrustedSocketFactory {
    Socket createSocket(Socket socket, String str, int i, String str2) throws NoSuchAlgorithmException, KeyManagementException, MessagingException, IOException;
}
