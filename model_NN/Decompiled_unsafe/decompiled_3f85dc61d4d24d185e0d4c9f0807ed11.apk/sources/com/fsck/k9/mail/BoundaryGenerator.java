package com.fsck.k9.mail;

import android.support.annotation.VisibleForTesting;
import java.util.Locale;
import java.util.Random;

public class BoundaryGenerator {
    private static final BoundaryGenerator INSTANCE = new BoundaryGenerator(new Random());
    private final Random random;

    public static BoundaryGenerator getInstance() {
        return INSTANCE;
    }

    @VisibleForTesting
    BoundaryGenerator(Random random2) {
        this.random = random2;
    }

    public String generateBoundary() {
        StringBuilder sb = new StringBuilder();
        sb.append("----");
        for (int i = 0; i < 30; i++) {
            sb.append(Integer.toString(this.random.nextInt(36), 36));
        }
        return sb.toString().toUpperCase(Locale.US);
    }
}
