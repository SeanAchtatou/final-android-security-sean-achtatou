package com.fsck.k9.mail.store.imap;

import com.fsck.k9.mail.Flag;
import java.util.Collections;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

class PermanentFlagsResponse {
    private final boolean canCreateKeywords;
    private final Set<Flag> flags;

    private PermanentFlagsResponse(Set<Flag> flags2, boolean canCreateKeywords2) {
        this.flags = Collections.unmodifiableSet(flags2);
        this.canCreateKeywords = canCreateKeywords2;
    }

    public static PermanentFlagsResponse parse(ImapResponse response) {
        if (response.isTagged() || !ImapResponseParser.equalsIgnoreCase(response.get(0), Responses.OK) || !response.isList(1)) {
            return null;
        }
        ImapList responseTextList = response.getList(1);
        if (responseTextList.size() < 2 || !ImapResponseParser.equalsIgnoreCase(responseTextList.get(0), Responses.PERMANENTFLAGS) || !responseTextList.isList(1)) {
            return null;
        }
        ImapList permanentFlagsList = responseTextList.getList(1);
        int size = permanentFlagsList.size();
        Set<Flag> flags2 = new HashSet<>(size);
        boolean canCreateKeywords2 = false;
        for (int i = 0; i < size; i++) {
            if (!permanentFlagsList.isString(i)) {
                return null;
            }
            String compareFlag = permanentFlagsList.getString(i).toLowerCase(Locale.US);
            char c = 65535;
            switch (compareFlag.hashCode()) {
                case -1319743427:
                    if (compareFlag.equals("\\deleted")) {
                        c = 0;
                        break;
                    }
                    break;
                case -681710976:
                    if (compareFlag.equals("$forwarded")) {
                        c = 4;
                        break;
                    }
                    break;
                case 2894:
                    if (compareFlag.equals("\\*")) {
                        c = 5;
                        break;
                    }
                    break;
                case 88490199:
                    if (compareFlag.equals("\\seen")) {
                        c = 2;
                        break;
                    }
                    break;
                case 645556350:
                    if (compareFlag.equals("\\flagged")) {
                        c = 3;
                        break;
                    }
                    break;
                case 718354745:
                    if (compareFlag.equals("\\answered")) {
                        c = 1;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    flags2.add(Flag.DELETED);
                    break;
                case 1:
                    flags2.add(Flag.ANSWERED);
                    break;
                case 2:
                    flags2.add(Flag.SEEN);
                    break;
                case 3:
                    flags2.add(Flag.FLAGGED);
                    break;
                case 4:
                    flags2.add(Flag.FORWARDED);
                    break;
                case 5:
                    canCreateKeywords2 = true;
                    break;
            }
        }
        return new PermanentFlagsResponse(flags2, canCreateKeywords2);
    }

    public Set<Flag> getFlags() {
        return this.flags;
    }

    public boolean canCreateKeywords() {
        return this.canCreateKeywords;
    }
}
