package com.fsck.k9.mail.store.imap;

import java.util.List;

class NamespaceResponse {
    private String hierarchyDelimiter;
    private String prefix;

    private NamespaceResponse(String prefix2, String hierarchyDelimiter2) {
        this.prefix = prefix2;
        this.hierarchyDelimiter = hierarchyDelimiter2;
    }

    public static NamespaceResponse parse(List<ImapResponse> responses) {
        for (ImapResponse response : responses) {
            NamespaceResponse prefix2 = parse(response);
            if (prefix2 != null) {
                return prefix2;
            }
        }
        return null;
    }

    static NamespaceResponse parse(ImapResponse response) {
        if (response.size() < 4 || !ImapResponseParser.equalsIgnoreCase(response.get(0), "NAMESPACE") || !response.isList(1)) {
            return null;
        }
        ImapList personalNamespaces = response.getList(1);
        if (!personalNamespaces.isList(0)) {
            return null;
        }
        ImapList firstPersonalNamespace = personalNamespaces.getList(0);
        if (!firstPersonalNamespace.isString(0) || !firstPersonalNamespace.isString(1)) {
            return null;
        }
        return new NamespaceResponse(firstPersonalNamespace.getString(0), firstPersonalNamespace.getString(1));
    }

    public String getPrefix() {
        return this.prefix;
    }

    public String getHierarchyDelimiter() {
        return this.hierarchyDelimiter;
    }
}
