package com.fsck.k9.mail;

import android.support.annotation.NonNull;
import java.io.IOException;
import java.io.OutputStream;

public interface Part {
    void addHeader(String str, String str2);

    void addRawHeader(String str, String str2);

    Body getBody();

    String getContentId();

    String getContentType();

    String getDisposition();

    @NonNull
    String[] getHeader(String str);

    String getMimeType();

    String getServerExtra();

    boolean isMimeType(String str);

    void removeHeader(String str);

    void setBody(Body body);

    void setHeader(String str, String str2);

    void setServerExtra(String str);

    void writeHeaderTo(OutputStream outputStream) throws IOException, MessagingException;

    void writeTo(OutputStream outputStream) throws IOException, MessagingException;
}
