package com.fsck.k9.mail.store.webdav;

import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HttpContext;

public class WebDavHttpClient extends DefaultHttpClient {

    public static class WebDavHttpClientFactory {
        public WebDavHttpClient create() {
            return new WebDavHttpClient();
        }
    }

    public static void modifyRequestToAcceptGzipResponse(HttpRequest request) {
        Log.i("k9", "Requesting gzipped data");
        request.addHeader("Accept-Encoding", "gzip");
    }

    public static InputStream getUngzippedContent(HttpEntity entity) throws IOException {
        InputStream responseStream = entity.getContent();
        if (responseStream == null) {
            return null;
        }
        Header header = entity.getContentEncoding();
        if (header == null) {
            return responseStream;
        }
        String contentEncoding = header.getValue();
        if (contentEncoding == null) {
            return responseStream;
        }
        if (contentEncoding.contains("gzip")) {
            Log.i("k9", "Response is gzipped");
            responseStream = new GZIPInputStream(responseStream);
        }
        return responseStream;
    }

    public HttpResponse executeOverride(HttpUriRequest request, HttpContext context) throws IOException {
        modifyRequestToAcceptGzipResponse(request);
        return WebDavHttpClient.super.execute(request, context);
    }
}
