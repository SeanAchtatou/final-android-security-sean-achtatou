package com.fsck.k9.mail;

import com.fsck.k9.mail.internet.CharsetSupport;
import com.fsck.k9.mail.internet.TextBody;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.james.mime4j.util.MimeUtil;

public abstract class Multipart implements Body {
    private Part mParent;
    private final List<BodyPart> mParts = new ArrayList();

    public abstract String getBoundary();

    public abstract byte[] getEpilogue();

    public abstract String getMimeType();

    public abstract byte[] getPreamble();

    public void addBodyPart(BodyPart part) {
        this.mParts.add(part);
        part.setParent(this);
    }

    public BodyPart getBodyPart(int index) {
        return this.mParts.get(index);
    }

    public List<BodyPart> getBodyParts() {
        return Collections.unmodifiableList(this.mParts);
    }

    public int getCount() {
        return this.mParts.size();
    }

    public Part getParent() {
        return this.mParent;
    }

    public void setParent(Part parent) {
        this.mParent = parent;
    }

    public void setEncoding(String encoding) throws MessagingException {
        if (!MimeUtil.ENC_7BIT.equalsIgnoreCase(encoding) && !MimeUtil.ENC_8BIT.equalsIgnoreCase(encoding)) {
            throw new MessagingException("Incompatible content-transfer-encoding for a multipart/* body");
        }
    }

    public void setCharset(String charset) throws MessagingException {
        if (!this.mParts.isEmpty()) {
            BodyPart part = this.mParts.get(0);
            Body body = part.getBody();
            if (body instanceof TextBody) {
                CharsetSupport.setCharset(charset, part);
                ((TextBody) body).setCharset(charset);
            }
        }
    }
}
