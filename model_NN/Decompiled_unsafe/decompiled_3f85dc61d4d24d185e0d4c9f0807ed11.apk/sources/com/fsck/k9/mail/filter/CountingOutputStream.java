package com.fsck.k9.mail.filter;

import java.io.IOException;
import java.io.OutputStream;

public class CountingOutputStream extends OutputStream {
    private long mCount;

    public long getCount() {
        return this.mCount;
    }

    public void write(int oneByte) throws IOException {
        this.mCount++;
    }

    public void write(byte[] b, int offset, int len) throws IOException {
        this.mCount += (long) len;
    }

    public void write(byte[] b) throws IOException {
        this.mCount += (long) b.length;
    }
}
