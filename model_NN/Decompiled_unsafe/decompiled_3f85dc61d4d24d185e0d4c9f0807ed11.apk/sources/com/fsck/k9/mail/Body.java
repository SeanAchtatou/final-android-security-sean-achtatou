package com.fsck.k9.mail;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface Body {
    InputStream getInputStream() throws MessagingException;

    void setEncoding(String str) throws MessagingException;

    void writeTo(OutputStream outputStream) throws IOException, MessagingException;
}
