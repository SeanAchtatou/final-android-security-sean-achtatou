package com.fsck.k9.mail.filter;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class EOLConvertingOutputStream extends FilterOutputStream {
    private static final int CR = 13;
    private static final int LF = 10;
    private boolean ignoreLf = false;
    private int lastByte;

    public EOLConvertingOutputStream(OutputStream out) {
        super(out);
    }

    public void write(int oneByte) throws IOException {
        if (oneByte != 10 || !this.ignoreLf) {
            if (oneByte == 10 && this.lastByte != 13) {
                writeByte(13);
            } else if (oneByte != 10 && this.lastByte == 13) {
                writeByte(10);
            }
            writeByte(oneByte);
            this.ignoreLf = false;
            return;
        }
        this.ignoreLf = false;
    }

    public void flush() throws IOException {
        completeCrLf();
        super.flush();
    }

    public void endWithCrLfAndFlush() throws IOException {
        completeCrLf();
        if (this.lastByte != 10) {
            writeByte(13);
            writeByte(10);
        }
        super.flush();
    }

    private void completeCrLf() throws IOException {
        if (this.lastByte == 13) {
            writeByte(10);
            this.ignoreLf = true;
        }
    }

    private void writeByte(int oneByte) throws IOException {
        super.write(oneByte);
        this.lastByte = oneByte;
    }
}
