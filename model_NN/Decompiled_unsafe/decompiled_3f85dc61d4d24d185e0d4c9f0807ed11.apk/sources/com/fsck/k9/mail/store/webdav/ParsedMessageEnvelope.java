package com.fsck.k9.mail.store.webdav;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.james.mime4j.dom.field.FieldName;

class ParsedMessageEnvelope {
    private static final Map<String, String> HEADER_MAPPINGS;
    private List<String> mHeaders = new ArrayList();
    private Map<String, String> mMessageHeaders = new HashMap();
    private boolean mReadStatus = false;
    private String mUid = "";

    ParsedMessageEnvelope() {
    }

    static {
        Map<String, String> map = new HashMap<>();
        map.put("mime-version", FieldName.MIME_VERSION);
        map.put("content-type", "Content-Type");
        map.put("subject", FieldName.SUBJECT);
        map.put("date", FieldName.DATE);
        map.put("thread-topic", "Thread-Topic");
        map.put("thread-index", "Thread-Index");
        map.put("from", FieldName.FROM);
        map.put("to", FieldName.TO);
        map.put("in-reply-to", "In-Reply-To");
        map.put("cc", FieldName.CC);
        map.put("getcontentlength", FieldName.CONTENT_LENGTH);
        HEADER_MAPPINGS = Collections.unmodifiableMap(map);
    }

    public void addHeader(String field, String value) {
        if (HEADER_MAPPINGS.get(field) != null) {
            this.mMessageHeaders.put(HEADER_MAPPINGS.get(field), value);
            this.mHeaders.add(HEADER_MAPPINGS.get(field));
        }
    }

    public Map<String, String> getMessageHeaders() {
        return this.mMessageHeaders;
    }

    public String[] getHeaderList() {
        return (String[]) this.mHeaders.toArray(WebDavConstants.EMPTY_STRING_ARRAY);
    }

    public void setReadStatus(boolean status) {
        this.mReadStatus = status;
    }

    public boolean getReadStatus() {
        return this.mReadStatus;
    }

    public void setUid(String uid) {
        if (uid != null) {
            this.mUid = uid;
        }
    }

    public String getUid() {
        return this.mUid;
    }
}
