package com.fsck.k9.mail;

public enum NetworkType {
    WIFI,
    MOBILE,
    OTHER;

    public static NetworkType fromConnectivityManagerType(int type) {
        switch (type) {
            case 0:
                return MOBILE;
            case 1:
                return WIFI;
            default:
                return OTHER;
        }
    }
}
