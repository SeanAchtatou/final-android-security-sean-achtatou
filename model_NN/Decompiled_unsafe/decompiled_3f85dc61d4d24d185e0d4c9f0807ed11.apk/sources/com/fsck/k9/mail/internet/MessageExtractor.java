package com.fsck.k9.mail.internet;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import com.fsck.k9.mail.Body;
import com.fsck.k9.mail.BodyPart;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.Multipart;
import com.fsck.k9.mail.Part;
import com.fsck.k9.mail.internet.Viewable;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.io.input.BoundedInputStream;
import org.apache.james.mime4j.dom.field.ContentDispositionField;
import org.apache.james.mime4j.dom.field.ContentTypeField;

public class MessageExtractor {
    public static final long NO_TEXT_SIZE_LIMIT = -1;

    private MessageExtractor() {
    }

    public static String getTextFromPart(Part part) {
        return getTextFromPart(part, -1);
    }

    public static String getTextFromPart(Part part, long textSizeLimit) {
        if (part != null) {
            try {
                if (part.getBody() != null) {
                    Body body = part.getBody();
                    if (body instanceof TextBody) {
                        return ((TextBody) body).getRawText();
                    }
                    String mimeType = part.getMimeType();
                    if ((mimeType != null && MimeUtility.mimeTypeMatches(mimeType, "text/*")) || part.isMimeType("application/pgp")) {
                        return getTextFromTextPart(part, body, mimeType, textSizeLimit);
                    }
                    throw new MessagingException("Provided non-text part: " + part);
                }
            } catch (IOException e) {
                Log.e("k9", "Unable to getTextFromPart", e);
                return null;
            } catch (MessagingException e2) {
                Log.e("k9", "Unable to getTextFromPart", e2);
                return null;
            }
        }
        if (part == null) {
            throw new MessagingException("Provided null part: " + part);
        }
        throw new MessagingException("Provided null body from part: " + part);
    }

    private static String getTextFromTextPart(Part part, Body body, String mimeType, long textSizeLimit) throws IOException, MessagingException {
        InputStream possiblyLimitedIn;
        String readToString;
        String charset = MimeUtility.getHeaderParameter(part.getContentType(), "charset");
        if (MimeUtility.isSameMimeType(mimeType, "text/html") && charset == null) {
            InputStream in = MimeUtility.decodeBody(body);
            try {
                byte[] buf = new byte[256];
                in.read(buf, 0, buf.length);
                String str = new String(buf, "US-ASCII");
                if (str.isEmpty()) {
                    readToString = "";
                    return readToString;
                }
                Matcher m = Pattern.compile("<meta http-equiv=\"?Content-Type\"? content=\"text/html; charset=(.+?)\">", 2).matcher(str);
                if (m.find()) {
                    charset = m.group(1);
                }
                try {
                    MimeUtility.closeInputStreamWithoutDeletingTemporaryFiles(in);
                } catch (IOException e) {
                }
            } finally {
                try {
                    MimeUtility.closeInputStreamWithoutDeletingTemporaryFiles(in);
                } catch (IOException e2) {
                }
            }
        }
        String charset2 = CharsetSupport.fixupCharset(charset, getMessageFromPart(part));
        InputStream in2 = MimeUtility.decodeBody(body);
        if (textSizeLimit != -1) {
            possiblyLimitedIn = new BoundedInputStream(in2, textSizeLimit);
        } else {
            possiblyLimitedIn = in2;
        }
        try {
            readToString = CharsetSupport.readToString(possiblyLimitedIn, charset2);
            return readToString;
        } finally {
            try {
                MimeUtility.closeInputStreamWithoutDeletingTemporaryFiles(in2);
            } catch (IOException e3) {
            }
        }
    }

    public static boolean hasMissingParts(Part part) {
        Body body = part.getBody();
        if (body == null) {
            return true;
        }
        if (body instanceof Multipart) {
            for (BodyPart subPart : ((Multipart) body).getBodyParts()) {
                if (hasMissingParts(subPart)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static void findViewablesAndAttachments(Part part, @Nullable List<Viewable> outputViewableParts, @Nullable List<Part> outputNonViewableParts) throws MessagingException {
        boolean skipSavingNonViewableParts = outputNonViewableParts == null;
        boolean skipSavingViewableParts = outputViewableParts == null;
        if (!skipSavingNonViewableParts || !skipSavingViewableParts) {
            Body body = part.getBody();
            if (body instanceof Multipart) {
                Multipart multipart = (Multipart) body;
                if (MimeUtility.isSameMimeType(part.getMimeType(), "multipart/alternative")) {
                    List<Viewable> text = findTextPart(multipart, true);
                    List<Viewable> html = findHtmlPart(multipart, getParts(text), outputNonViewableParts, true);
                    if (!skipSavingViewableParts) {
                        if (!text.isEmpty() || !html.isEmpty()) {
                            outputViewableParts.add(new Viewable.Alternative(text, html));
                            return;
                        }
                        return;
                    }
                    return;
                }
                for (BodyPart bodyPart : multipart.getBodyParts()) {
                    findViewablesAndAttachments(bodyPart, outputViewableParts, outputNonViewableParts);
                }
            } else if (!(body instanceof Message) || ContentDispositionField.DISPOSITION_TYPE_ATTACHMENT.equalsIgnoreCase(getContentDisposition(part))) {
                if (isPartTextualBody(part).booleanValue()) {
                    if (skipSavingViewableParts) {
                        return;
                    }
                    if (MimeUtility.isSameMimeType(part.getMimeType(), ContentTypeField.TYPE_TEXT_PLAIN)) {
                        outputViewableParts.add(new Viewable.Text(part));
                    } else {
                        outputViewableParts.add(new Viewable.Html(part));
                    }
                } else if (!MimeUtility.isSameMimeType(part.getMimeType(), "application/pgp-signature") && !skipSavingNonViewableParts) {
                    outputNonViewableParts.add(part);
                }
            } else if (!skipSavingViewableParts) {
                Message message = (Message) body;
                outputViewableParts.add(new Viewable.MessageHeader(part, message));
                findViewablesAndAttachments(message, outputViewableParts, outputNonViewableParts);
            }
        } else {
            throw new IllegalArgumentException("method was called but no output is to be collected - this a bug!");
        }
    }

    public static Set<Part> getTextParts(Part part) throws MessagingException {
        List<Viewable> viewableParts = new ArrayList<>();
        findViewablesAndAttachments(part, viewableParts, new ArrayList<>());
        return getParts(viewableParts);
    }

    public static List<Part> collectAttachments(Message message) throws MessagingException {
        try {
            List<Part> attachments = new ArrayList<>();
            findViewablesAndAttachments(message, new ArrayList(), attachments);
            return attachments;
        } catch (Exception e) {
            throw new MessagingException("Couldn't collect attachment parts", e);
        }
    }

    public static Set<Part> collectTextParts(Message message) throws MessagingException {
        try {
            return getTextParts(message);
        } catch (Exception e) {
            throw new MessagingException("Couldn't extract viewable parts", e);
        }
    }

    private static Message getMessageFromPart(Part part) {
        while (part != null) {
            if (part instanceof Message) {
                return (Message) part;
            }
            if (!(part instanceof BodyPart)) {
                return null;
            }
            Multipart multipart = ((BodyPart) part).getParent();
            if (multipart == null) {
                return null;
            }
            part = multipart.getParent();
        }
        return null;
    }

    private static List<Viewable> findTextPart(Multipart multipart, boolean directChild) throws MessagingException {
        List<Viewable> viewables = new ArrayList<>();
        for (Part part : multipart.getBodyParts()) {
            Body body = part.getBody();
            if (body instanceof Multipart) {
                List<Viewable> textViewables = findTextPart((Multipart) body, false);
                if (!textViewables.isEmpty()) {
                    viewables.addAll(textViewables);
                    if (directChild) {
                        break;
                    }
                } else {
                    continue;
                }
            } else if (isPartTextualBody(part).booleanValue() && MimeUtility.isSameMimeType(part.getMimeType(), ContentTypeField.TYPE_TEXT_PLAIN)) {
                viewables.add(new Viewable.Text(part));
                if (directChild) {
                    break;
                }
            }
        }
        return viewables;
    }

    private static List<Viewable> findHtmlPart(Multipart multipart, Set<Part> knownTextParts, @Nullable List<Part> outputNonViewableParts, boolean directChild) throws MessagingException {
        boolean saveNonViewableParts;
        if (outputNonViewableParts != null) {
            saveNonViewableParts = true;
        } else {
            saveNonViewableParts = false;
        }
        List<Viewable> viewables = new ArrayList<>();
        boolean partFound = false;
        for (Part part : multipart.getBodyParts()) {
            Body body = part.getBody();
            if (body instanceof Multipart) {
                Multipart innerMultipart = (Multipart) body;
                if (!directChild || !partFound) {
                    List<Viewable> htmlViewables = findHtmlPart(innerMultipart, knownTextParts, outputNonViewableParts, false);
                    if (!htmlViewables.isEmpty()) {
                        partFound = true;
                        viewables.addAll(htmlViewables);
                    }
                } else if (saveNonViewableParts) {
                    findAttachments(innerMultipart, knownTextParts, outputNonViewableParts);
                }
            } else if ((!directChild || !partFound) && isPartTextualBody(part).booleanValue() && MimeUtility.isSameMimeType(part.getMimeType(), "text/html")) {
                viewables.add(new Viewable.Html(part));
                partFound = true;
            } else if (!knownTextParts.contains(part) && saveNonViewableParts) {
                outputNonViewableParts.add(part);
            }
        }
        return viewables;
    }

    private static void findAttachments(Multipart multipart, Set<Part> knownTextParts, @NonNull List<Part> attachments) {
        for (Part part : multipart.getBodyParts()) {
            Body body = part.getBody();
            if (body instanceof Multipart) {
                findAttachments((Multipart) body, knownTextParts, attachments);
            } else if (!knownTextParts.contains(part)) {
                attachments.add(part);
            }
        }
    }

    private static Set<Part> getParts(List<Viewable> viewables) {
        Set<Part> parts = new HashSet<>();
        for (Viewable viewable : viewables) {
            if (viewable instanceof Viewable.Textual) {
                parts.add(((Viewable.Textual) viewable).getPart());
            } else if (viewable instanceof Viewable.Alternative) {
                Viewable.Alternative alternative = (Viewable.Alternative) viewable;
                parts.addAll(getParts(alternative.getText()));
                parts.addAll(getParts(alternative.getHtml()));
            }
        }
        return parts;
    }

    private static Boolean isPartTextualBody(Part part) throws MessagingException {
        boolean isAttachmentDisposition;
        String disposition = part.getDisposition();
        String dispositionType = null;
        String dispositionFilename = null;
        if (disposition != null) {
            dispositionType = MimeUtility.getHeaderParameter(disposition, null);
            dispositionFilename = MimeUtility.getHeaderParameter(disposition, ContentDispositionField.PARAM_FILENAME);
        }
        if (ContentDispositionField.DISPOSITION_TYPE_ATTACHMENT.equalsIgnoreCase(dispositionType) || dispositionFilename != null) {
            isAttachmentDisposition = true;
        } else {
            isAttachmentDisposition = false;
        }
        if (isAttachmentDisposition) {
            return false;
        }
        if (part.isMimeType("text/html")) {
            return true;
        }
        if (part.isMimeType(ContentTypeField.TYPE_TEXT_PLAIN)) {
            return true;
        }
        if (part.isMimeType("application/pgp")) {
            return true;
        }
        return false;
    }

    private static String getContentDisposition(Part part) {
        String disposition = part.getDisposition();
        if (disposition != null) {
            return MimeUtility.getHeaderParameter(disposition, null);
        }
        return null;
    }
}
