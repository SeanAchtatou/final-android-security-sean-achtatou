package com.fsck.k9.mail.store;

import com.fsck.k9.mail.NetworkType;

public interface StoreConfig {
    boolean allowRemoteSearch();

    int getDisplayCount();

    String getDraftsFolderName();

    int getIdleRefreshMinutes();

    String getInboxFolderName();

    int getMaximumAutoDownloadMessageSize();

    String getOutboxFolderName();

    String getStoreUri();

    String getTransportUri();

    boolean isPushPollOnConnect();

    boolean isRemoteSearchFullText();

    void setArchiveFolderName(String str);

    void setAutoExpandFolderName(String str);

    void setDraftsFolderName(String str);

    void setInboxFolderName(String str);

    void setSentFolderName(String str);

    void setSpamFolderName(String str);

    void setTrashFolderName(String str);

    boolean subscribedFoldersOnly();

    boolean useCompression(NetworkType networkType);
}
