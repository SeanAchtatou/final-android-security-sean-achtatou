package com.fsck.k9.mail.store.imap;

class ImapResponse extends ImapList {
    private static final long serialVersionUID = 6886458551615975669L;
    private ImapResponseCallback callback;
    private final boolean commandContinuationRequested;
    private final String tag;

    private ImapResponse(ImapResponseCallback callback2, boolean commandContinuationRequested2, String tag2) {
        this.callback = callback2;
        this.commandContinuationRequested = commandContinuationRequested2;
        this.tag = tag2;
    }

    public static ImapResponse newContinuationRequest(ImapResponseCallback callback2) {
        return new ImapResponse(callback2, true, null);
    }

    public static ImapResponse newUntaggedResponse(ImapResponseCallback callback2) {
        return new ImapResponse(callback2, false, null);
    }

    public static ImapResponse newTaggedResponse(ImapResponseCallback callback2, String tag2) {
        return new ImapResponse(callback2, false, tag2);
    }

    public boolean isContinuationRequested() {
        return this.commandContinuationRequested;
    }

    public String getTag() {
        return this.tag;
    }

    public boolean isTagged() {
        return this.tag != null;
    }

    public ImapResponseCallback getCallback() {
        return this.callback;
    }

    public void setCallback(ImapResponseCallback callback2) {
        this.callback = callback2;
    }

    public String toString() {
        return "#" + (this.commandContinuationRequested ? "+" : this.tag) + "# " + super.toString();
    }
}
