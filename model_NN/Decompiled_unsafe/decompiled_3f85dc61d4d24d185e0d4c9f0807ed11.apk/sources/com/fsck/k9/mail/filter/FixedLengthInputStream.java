package com.fsck.k9.mail.filter;

import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

public class FixedLengthInputStream extends InputStream {
    private int mCount = 0;
    private final InputStream mIn;
    private final int mLength;

    public FixedLengthInputStream(InputStream in, int length) {
        this.mIn = in;
        this.mLength = length;
    }

    public int available() throws IOException {
        return this.mLength - this.mCount;
    }

    public int read() throws IOException {
        if (this.mCount >= this.mLength) {
            return -1;
        }
        int d = this.mIn.read();
        if (d == -1) {
            return d;
        }
        this.mCount++;
        return d;
    }

    public int read(byte[] b, int offset, int length) throws IOException {
        if (this.mCount >= this.mLength) {
            return -1;
        }
        int d = this.mIn.read(b, offset, Math.min(this.mLength - this.mCount, length));
        if (d == -1) {
            return d;
        }
        this.mCount += d;
        return d;
    }

    public int read(byte[] b) throws IOException {
        return read(b, 0, b.length);
    }

    public long skip(long n) throws IOException {
        long d = this.mIn.skip(Math.min(n, (long) available()));
        if (d > 0) {
            this.mCount = (int) (((long) this.mCount) + d);
        }
        return d;
    }

    public String toString() {
        return String.format(Locale.US, "FixedLengthInputStream(in=%s, length=%d)", this.mIn.toString(), Integer.valueOf(this.mLength));
    }
}
