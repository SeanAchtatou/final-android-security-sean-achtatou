package com.fsck.k9.mail.store.webdav;

import java.util.LinkedList;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

class WebDavHandler extends DefaultHandler {
    private DataSet mDataSet = new DataSet();
    private final LinkedList<String> mOpenTags = new LinkedList<>();

    WebDavHandler() {
    }

    public DataSet getDataSet() {
        return this.mDataSet;
    }

    public void startDocument() throws SAXException {
        this.mDataSet = new DataSet();
    }

    public void endDocument() throws SAXException {
    }

    public void startElement(String namespaceURI, String localName, String qName, Attributes atts) throws SAXException {
        this.mOpenTags.addFirst(localName);
    }

    public void endElement(String namespaceURI, String localName, String qName) {
        this.mOpenTags.removeFirst();
        if (localName.equals("response")) {
            this.mDataSet.finish();
        }
    }

    public void characters(char[] ch, int start, int length) {
        this.mDataSet.addValue(new String(ch, start, length), this.mOpenTags.peek());
    }
}
