package com.fsck.k9.mail.internet;

import com.fsck.k9.mail.Address;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.Part;

class JisSupport {
    public static final String SHIFT_JIS = "shift_jis";

    JisSupport() {
    }

    public static String getJisVariantFromMessage(Message message) throws MessagingException {
        if (message == null) {
            return null;
        }
        String variant = getJisVariantFromReceivedHeaders(message);
        if (variant != null) {
            return variant;
        }
        String variant2 = getJisVariantFromFromHeaders(message);
        return variant2 == null ? getJisVariantFromMailerHeaders(message) : variant2;
    }

    public static boolean isShiftJis(String charset) {
        return charset.length() > 17 && charset.startsWith("x-") && charset.endsWith("-shift_jis-2007");
    }

    public static String getJisVariantFromAddress(String address) {
        if (address == null) {
            return null;
        }
        if (isInDomain(address, "docomo.ne.jp") || isInDomain(address, "dwmail.jp") || isInDomain(address, "pdx.ne.jp") || isInDomain(address, "willcom.com") || isInDomain(address, "emnet.ne.jp") || isInDomain(address, "emobile.ne.jp")) {
            return "docomo";
        }
        if (isInDomain(address, "softbank.ne.jp") || isInDomain(address, "vodafone.ne.jp") || isInDomain(address, "disney.ne.jp") || isInDomain(address, "vertuclub.ne.jp")) {
            return "softbank";
        }
        if (isInDomain(address, "ezweb.ne.jp") || isInDomain(address, "ido.ne.jp")) {
            return "kddi";
        }
        return null;
    }

    private static String getJisVariantFromMailerHeaders(Message message) {
        String[] mailerHeaders = message.getHeader("X-Mailer");
        if (mailerHeaders.length == 0) {
            return null;
        }
        if (mailerHeaders[0].startsWith("iPhone Mail ") || mailerHeaders[0].startsWith("iPad Mail ")) {
            return "iphone";
        }
        return null;
    }

    private static String getJisVariantFromReceivedHeaders(Part message) throws MessagingException {
        String variant;
        String[] receivedHeaders = message.getHeader("Received");
        if (receivedHeaders.length == 0) {
            return null;
        }
        for (String receivedHeader : receivedHeaders) {
            String address = getAddressFromReceivedHeader(receivedHeader);
            if (address != null && (variant = getJisVariantFromAddress(address)) != null) {
                return variant;
            }
        }
        return null;
    }

    private static String getAddressFromReceivedHeader(String receivedHeader) {
        return null;
    }

    private static String getJisVariantFromFromHeaders(Message message) throws MessagingException {
        Address[] addresses = message.getFrom();
        if (addresses == null || addresses.length == 0) {
            return null;
        }
        return getJisVariantFromAddress(addresses[0].getAddress());
    }

    private static boolean isInDomain(String address, String domain) {
        int index = (address.length() - domain.length()) - 1;
        if (index < 0) {
            return false;
        }
        char c = address.charAt(index);
        if (c == '@' || c == '.') {
            return address.endsWith(domain);
        }
        return false;
    }
}
