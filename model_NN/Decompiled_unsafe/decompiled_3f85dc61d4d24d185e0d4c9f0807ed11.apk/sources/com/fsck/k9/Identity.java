package com.fsck.k9;

import java.io.Serializable;

public class Identity implements Serializable {
    private static final long serialVersionUID = -1666669071480985760L;
    private String mDescription;
    private String mEmail;
    private String mName;
    private String mSignature;
    private boolean mSignatureUse;
    private String replyTo;

    public synchronized String getName() {
        return this.mName;
    }

    public synchronized void setName(String name) {
        this.mName = name;
    }

    public synchronized String getEmail() {
        return this.mEmail;
    }

    public synchronized void setEmail(String email) {
        this.mEmail = email;
    }

    public synchronized boolean getSignatureUse() {
        return this.mSignatureUse;
    }

    public synchronized void setSignatureUse(boolean signatureUse) {
        this.mSignatureUse = signatureUse;
    }

    public synchronized String getSignature() {
        return this.mSignature;
    }

    public synchronized void setSignature(String signature) {
        this.mSignature = signature;
    }

    public synchronized String getDescription() {
        return this.mDescription;
    }

    public synchronized void setDescription(String description) {
        this.mDescription = description;
    }

    public synchronized String getReplyTo() {
        return this.replyTo;
    }

    public synchronized void setReplyTo(String replyTo2) {
        this.replyTo = replyTo2;
    }

    public synchronized String toString() {
        return "Account.Identity(description=" + this.mDescription + ", name=" + this.mName + ", email=" + this.mEmail + ", replyTo=" + this.replyTo + ", signature=" + this.mSignature;
    }
}
