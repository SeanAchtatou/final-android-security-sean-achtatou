package com.fsck.k9.notification;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.fsck.k9.Account;
import com.fsck.k9.K9;
import com.fsck.k9.Preferences;
import com.fsck.k9.activity.MessageReference;
import com.fsck.k9.controller.MessagingController;
import com.fsck.k9.mail.Flag;
import com.fsck.k9.service.CoreService;
import java.util.ArrayList;

public class NotificationActionService extends CoreService {
    private static final String ACTION_ARCHIVE = "ACTION_ARCHIVE";
    private static final String ACTION_DELETE = "ACTION_DELETE";
    private static final String ACTION_DISMISS = "ACTION_DISMISS";
    private static final String ACTION_MARK_AS_READ = "ACTION_MARK_AS_READ";
    private static final String ACTION_SPAM = "ACTION_SPAM";
    private static final String EXTRA_ACCOUNT_UUID = "accountUuid";
    private static final String EXTRA_MESSAGE_REFERENCE = "messageReference";
    private static final String EXTRA_MESSAGE_REFERENCES = "messageReferences";

    static Intent createMarkMessageAsReadIntent(Context context, MessageReference messageReference) {
        return createMarkAllAsReadIntent(context, messageReference.getAccountUuid(), createSingleItemArrayList(messageReference));
    }

    static Intent createMarkAllAsReadIntent(Context context, String accountUuid, ArrayList<MessageReference> messageReferences) {
        Intent intent = new Intent(context, NotificationActionService.class);
        intent.setAction(ACTION_MARK_AS_READ);
        intent.putExtra(EXTRA_ACCOUNT_UUID, accountUuid);
        intent.putExtra(EXTRA_MESSAGE_REFERENCES, messageReferences);
        return intent;
    }

    static Intent createDismissMessageIntent(Context context, MessageReference messageReference) {
        Intent intent = new Intent(context, NotificationActionService.class);
        intent.setAction(ACTION_DISMISS);
        intent.putExtra(EXTRA_ACCOUNT_UUID, messageReference.getAccountUuid());
        intent.putExtra(EXTRA_MESSAGE_REFERENCE, messageReference);
        return intent;
    }

    static Intent createDismissAllMessagesIntent(Context context, Account account) {
        Intent intent = new Intent(context, NotificationActionService.class);
        intent.setAction(ACTION_DISMISS);
        intent.putExtra(EXTRA_ACCOUNT_UUID, account.getUuid());
        return intent;
    }

    static Intent createDeleteMessageIntent(Context context, MessageReference messageReference) {
        return createDeleteAllMessagesIntent(context, messageReference.getAccountUuid(), createSingleItemArrayList(messageReference));
    }

    public static Intent createDeleteAllMessagesIntent(Context context, String accountUuid, ArrayList<MessageReference> messageReferences) {
        Intent intent = new Intent(context, NotificationActionService.class);
        intent.setAction(ACTION_DELETE);
        intent.putExtra(EXTRA_ACCOUNT_UUID, accountUuid);
        intent.putExtra(EXTRA_MESSAGE_REFERENCES, messageReferences);
        return intent;
    }

    static Intent createArchiveMessageIntent(Context context, MessageReference messageReference) {
        ArrayList<MessageReference> messageReferences = createSingleItemArrayList(messageReference);
        Intent intent = new Intent(context, NotificationActionService.class);
        intent.setAction(ACTION_ARCHIVE);
        intent.putExtra(EXTRA_ACCOUNT_UUID, messageReference.getAccountUuid());
        intent.putExtra(EXTRA_MESSAGE_REFERENCES, messageReferences);
        return intent;
    }

    static Intent createArchiveAllIntent(Context context, Account account, ArrayList<MessageReference> messageReferences) {
        Intent intent = new Intent(context, NotificationActionService.class);
        intent.setAction(ACTION_ARCHIVE);
        intent.putExtra(EXTRA_ACCOUNT_UUID, account.getUuid());
        intent.putExtra(EXTRA_MESSAGE_REFERENCES, messageReferences);
        return intent;
    }

    static Intent createMarkMessageAsSpamIntent(Context context, MessageReference messageReference) {
        Intent intent = new Intent(context, NotificationActionService.class);
        intent.setAction(ACTION_SPAM);
        intent.putExtra(EXTRA_ACCOUNT_UUID, messageReference.getAccountUuid());
        intent.putExtra(EXTRA_MESSAGE_REFERENCE, messageReference);
        return intent;
    }

    private static ArrayList<MessageReference> createSingleItemArrayList(MessageReference messageReference) {
        ArrayList<MessageReference> messageReferences = new ArrayList<>(1);
        messageReferences.add(messageReference);
        return messageReferences;
    }

    public int startService(Intent intent, int startId) {
        if (K9.DEBUG) {
            Log.i("k9", "NotificationActionService started with startId = " + startId);
        }
        Account account = Preferences.getPreferences(this).getAccount(intent.getStringExtra(EXTRA_ACCOUNT_UUID));
        if (account == null) {
            Log.w("k9", "Could not find account for notification action.");
        } else {
            MessagingController controller = MessagingController.getInstance(getApplication());
            String action = intent.getAction();
            if (ACTION_MARK_AS_READ.equals(action)) {
                markMessagesAsRead(intent, account, controller);
            } else if (ACTION_DELETE.equals(action)) {
                deleteMessages(intent, controller);
            } else if (ACTION_ARCHIVE.equals(action)) {
                archiveMessages(intent, account, controller);
            } else if (ACTION_SPAM.equals(action)) {
                markMessageAsSpam(intent, account, controller);
            } else if (ACTION_DISMISS.equals(action) && K9.DEBUG) {
                Log.i("k9", "Notification dismissed");
            }
            cancelNotifications(intent, account, controller);
        }
        return 2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fsck.k9.controller.MessagingController.setFlag(com.fsck.k9.Account, java.lang.String, java.lang.String, com.fsck.k9.mail.Flag, boolean):void
     arg types: [com.fsck.k9.Account, java.lang.String, java.lang.String, com.fsck.k9.mail.Flag, int]
     candidates:
      com.fsck.k9.controller.MessagingController.setFlag(com.fsck.k9.Account, java.lang.String, java.util.List<? extends com.fsck.k9.mail.Message>, com.fsck.k9.mail.Flag, boolean):void
      com.fsck.k9.controller.MessagingController.setFlag(com.fsck.k9.Account, java.lang.String, java.lang.String, com.fsck.k9.mail.Flag, boolean):void */
    private void markMessagesAsRead(Intent intent, Account account, MessagingController controller) {
        if (K9.DEBUG) {
            Log.i("k9", "NotificationActionService marking messages as read");
        }
        for (MessageReference messageReference : intent.getParcelableArrayListExtra(EXTRA_MESSAGE_REFERENCES)) {
            controller.setFlag(account, messageReference.getFolderName(), messageReference.getUid(), Flag.SEEN, true);
        }
    }

    private void deleteMessages(Intent intent, MessagingController controller) {
        if (K9.DEBUG) {
            Log.i("k9", "NotificationActionService deleting messages");
        }
        controller.deleteMessages(intent.getParcelableArrayListExtra(EXTRA_MESSAGE_REFERENCES), null);
    }

    private void archiveMessages(Intent intent, Account account, MessagingController controller) {
        if (K9.DEBUG) {
            Log.i("k9", "NotificationActionService archiving messages");
        }
        String archiveFolderName = account.getArchiveFolderName();
        if (archiveFolderName == null || ((archiveFolderName.equals(account.getSpamFolderName()) && K9.confirmSpam()) || !isMovePossible(controller, account, archiveFolderName))) {
            Log.w("k9", "Can not archive messages");
            return;
        }
        for (MessageReference messageReference : intent.getParcelableArrayListExtra(EXTRA_MESSAGE_REFERENCES)) {
            if (controller.isMoveCapable(messageReference)) {
                controller.moveMessage(account, messageReference.getFolderName(), messageReference, archiveFolderName);
            }
        }
    }

    private void markMessageAsSpam(Intent intent, Account account, MessagingController controller) {
        if (K9.DEBUG) {
            Log.i("k9", "NotificationActionService moving messages to spam");
        }
        MessageReference messageReference = (MessageReference) intent.getParcelableExtra(EXTRA_MESSAGE_REFERENCE);
        String spamFolderName = account.getSpamFolderName();
        if (spamFolderName != null && !K9.confirmSpam() && isMovePossible(controller, account, spamFolderName)) {
            controller.moveMessage(account, messageReference.getFolderName(), messageReference, spamFolderName);
        }
    }

    private void cancelNotifications(Intent intent, Account account, MessagingController controller) {
        if (intent.hasExtra(EXTRA_MESSAGE_REFERENCE)) {
            controller.cancelNotificationForMessage(account, (MessageReference) intent.getParcelableExtra(EXTRA_MESSAGE_REFERENCE));
        } else if (intent.hasExtra(EXTRA_MESSAGE_REFERENCES)) {
            for (MessageReference messageReference : intent.getParcelableArrayListExtra(EXTRA_MESSAGE_REFERENCES)) {
                controller.cancelNotificationForMessage(account, messageReference);
            }
        } else {
            controller.cancelNotificationsForAccount(account);
        }
    }

    private boolean isMovePossible(MessagingController controller, Account account, String destinationFolderName) {
        boolean isSpecialFolderConfigured;
        if (!K9.FOLDER_NONE.equalsIgnoreCase(destinationFolderName)) {
            isSpecialFolderConfigured = true;
        } else {
            isSpecialFolderConfigured = false;
        }
        return isSpecialFolderConfigured && controller.isMoveCapable(account);
    }
}
