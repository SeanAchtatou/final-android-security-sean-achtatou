package com.fsck.k9.notification;

import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.internal.view.SupportMenu;
import com.fsck.k9.Account;
import com.fsck.k9.R;
import com.fsck.k9.helper.ExceptionHelper;

class SendFailedNotifications {
    private final NotificationActionCreator actionBuilder;
    private final NotificationController controller;

    public SendFailedNotifications(NotificationController controller2, NotificationActionCreator actionBuilder2) {
        this.controller = controller2;
        this.actionBuilder = actionBuilder2;
    }

    public void showSendFailedNotification(Account account, Exception exception) {
        String title = this.controller.getContext().getString(R.string.send_failure_subject);
        String text = ExceptionHelper.getRootCauseMessage(exception);
        int notificationId = NotificationIds.getSendFailedNotificationId(account);
        NotificationCompat.Builder builder = this.controller.createNotificationBuilder().setSmallIcon(getSendFailedNotificationIcon()).setWhen(System.currentTimeMillis()).setAutoCancel(true).setTicker(title).setContentTitle(title).setContentText(text).setContentIntent(this.actionBuilder.createViewFolderListPendingIntent(account, notificationId)).setVisibility(1);
        this.controller.configureNotification(builder, null, null, Integer.valueOf((int) SupportMenu.CATEGORY_MASK), 1, true);
        getNotificationManager().notify(notificationId, builder.build());
    }

    public void clearSendFailedNotification(Account account) {
        getNotificationManager().cancel(NotificationIds.getSendFailedNotificationId(account));
    }

    private int getSendFailedNotificationIcon() {
        return R.drawable.notification_icon_new_mail;
    }

    private NotificationManagerCompat getNotificationManager() {
        return this.controller.getNotificationManager();
    }
}
