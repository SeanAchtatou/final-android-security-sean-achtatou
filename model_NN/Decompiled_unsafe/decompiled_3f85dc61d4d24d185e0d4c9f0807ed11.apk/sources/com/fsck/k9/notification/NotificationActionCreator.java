package com.fsck.k9.notification;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.TaskStackBuilder;
import android.text.TextUtils;
import com.fsck.k9.Account;
import com.fsck.k9.K9;
import com.fsck.k9.Preferences;
import com.fsck.k9.activity.Accounts;
import com.fsck.k9.activity.FolderList;
import com.fsck.k9.activity.MessageList;
import com.fsck.k9.activity.MessageReference;
import com.fsck.k9.activity.NotificationDeleteConfirmation;
import com.fsck.k9.activity.compose.MessageActions;
import com.fsck.k9.search.LocalSearch;
import java.util.ArrayList;
import java.util.List;

class NotificationActionCreator {
    private final Context context;

    public NotificationActionCreator(Context context2) {
        this.context = context2;
    }

    public PendingIntent createViewMessagePendingIntent(MessageReference messageReference, int notificationId) {
        return buildMessageViewBackStack(messageReference).getPendingIntent(notificationId, 1342177280);
    }

    public PendingIntent createViewFolderPendingIntent(Account account, String folderName, int notificationId) {
        return buildMessageListBackStack(account, folderName).getPendingIntent(notificationId, 1342177280);
    }

    public PendingIntent createViewMessagesPendingIntent(Account account, List<MessageReference> messageReferences, int notificationId) {
        TaskStackBuilder stack;
        if (account.goToUnreadMessageSearch()) {
            stack = buildUnreadBackStack(account);
        } else {
            String folderName = getFolderNameOfAllMessages(messageReferences);
            if (folderName == null) {
                stack = buildFolderListBackStack(account);
            } else {
                stack = buildMessageListBackStack(account, folderName);
            }
        }
        return stack.getPendingIntent(notificationId, 1342177280);
    }

    public PendingIntent createViewFolderListPendingIntent(Account account, int notificationId) {
        return buildFolderListBackStack(account).getPendingIntent(notificationId, 1342177280);
    }

    public PendingIntent createDismissAllMessagesPendingIntent(Account account, int notificationId) {
        return PendingIntent.getService(this.context, notificationId, NotificationActionService.createDismissAllMessagesIntent(this.context, account), 1342177280);
    }

    public PendingIntent createDismissMessagePendingIntent(Context context2, MessageReference messageReference, int notificationId) {
        return PendingIntent.getService(context2, notificationId, NotificationActionService.createDismissMessageIntent(context2, messageReference), 1342177280);
    }

    public PendingIntent createReplyPendingIntent(MessageReference messageReference, int notificationId) {
        return PendingIntent.getActivity(this.context, notificationId, MessageActions.getActionReplyIntent(this.context, messageReference), 1342177280);
    }

    public PendingIntent createMarkMessageAsReadPendingIntent(MessageReference messageReference, int notificationId) {
        return PendingIntent.getService(this.context, notificationId, NotificationActionService.createMarkMessageAsReadIntent(this.context, messageReference), 1342177280);
    }

    public PendingIntent createMarkAllAsReadPendingIntent(Account account, ArrayList<MessageReference> messageReferences, int notificationId) {
        return getMarkAsReadPendingIntent(account, messageReferences, notificationId, this.context, 1342177280);
    }

    public PendingIntent getMarkAllAsReadPendingIntent(Account account, ArrayList<MessageReference> messageReferences, int notificationId) {
        return getMarkAsReadPendingIntent(account, messageReferences, notificationId, this.context, 536870912);
    }

    private PendingIntent getMarkAsReadPendingIntent(Account account, ArrayList<MessageReference> messageReferences, int notificationId, Context context2, int flags) {
        return PendingIntent.getService(context2, notificationId, NotificationActionService.createMarkAllAsReadIntent(context2, account.getUuid(), messageReferences), flags);
    }

    public PendingIntent createDeleteMessagePendingIntent(MessageReference messageReference, int notificationId) {
        if (K9.confirmDeleteFromNotification()) {
            return createDeleteConfirmationPendingIntent(messageReference, notificationId);
        }
        return createDeleteServicePendingIntent(messageReference, notificationId);
    }

    private PendingIntent createDeleteServicePendingIntent(MessageReference messageReference, int notificationId) {
        return PendingIntent.getService(this.context, notificationId, NotificationActionService.createDeleteMessageIntent(this.context, messageReference), 1342177280);
    }

    private PendingIntent createDeleteConfirmationPendingIntent(MessageReference messageReference, int notificationId) {
        return PendingIntent.getActivity(this.context, notificationId, NotificationDeleteConfirmation.getIntent(this.context, messageReference), 268435456);
    }

    public PendingIntent createDeleteAllPendingIntent(Account account, ArrayList<MessageReference> messageReferences, int notificationId) {
        if (K9.confirmDeleteFromNotification()) {
            return getDeleteAllConfirmationPendingIntent(messageReferences, notificationId, 268435456);
        }
        return getDeleteAllServicePendingIntent(account, messageReferences, notificationId, 1342177280);
    }

    public PendingIntent getDeleteAllPendingIntent(Account account, ArrayList<MessageReference> messageReferences, int notificationId) {
        if (K9.confirmDeleteFromNotification()) {
            return getDeleteAllConfirmationPendingIntent(messageReferences, notificationId, 536870912);
        }
        return getDeleteAllServicePendingIntent(account, messageReferences, notificationId, 536870912);
    }

    private PendingIntent getDeleteAllConfirmationPendingIntent(ArrayList<MessageReference> messageReferences, int notificationId, int flags) {
        return PendingIntent.getActivity(this.context, notificationId, NotificationDeleteConfirmation.getIntent(this.context, messageReferences), flags);
    }

    private PendingIntent getDeleteAllServicePendingIntent(Account account, ArrayList<MessageReference> messageReferences, int notificationId, int flags) {
        return PendingIntent.getService(this.context, notificationId, NotificationActionService.createDeleteAllMessagesIntent(this.context, account.getUuid(), messageReferences), flags);
    }

    public PendingIntent createArchiveMessagePendingIntent(MessageReference messageReference, int notificationId) {
        return PendingIntent.getService(this.context, notificationId, NotificationActionService.createArchiveMessageIntent(this.context, messageReference), 1342177280);
    }

    public PendingIntent createArchiveAllPendingIntent(Account account, ArrayList<MessageReference> messageReferences, int notificationId) {
        return PendingIntent.getService(this.context, notificationId, NotificationActionService.createArchiveAllIntent(this.context, account, messageReferences), 1342177280);
    }

    public PendingIntent createMarkMessageAsSpamPendingIntent(MessageReference messageReference, int notificationId) {
        return PendingIntent.getService(this.context, notificationId, NotificationActionService.createMarkMessageAsSpamIntent(this.context, messageReference), 1342177280);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    private TaskStackBuilder buildAccountsBackStack() {
        TaskStackBuilder stack = TaskStackBuilder.create(this.context);
        if (!skipAccountsInBackStack()) {
            Intent intent = new Intent(this.context, Accounts.class);
            intent.putExtra(Accounts.EXTRA_STARTUP, false);
            stack.addNextIntent(intent);
        }
        return stack;
    }

    private TaskStackBuilder buildFolderListBackStack(Account account) {
        TaskStackBuilder stack = buildAccountsBackStack();
        stack.addNextIntent(FolderList.actionHandleAccountIntent(this.context, account, false));
        return stack;
    }

    private TaskStackBuilder buildUnreadBackStack(Account account) {
        TaskStackBuilder stack = buildAccountsBackStack();
        stack.addNextIntent(MessageList.intentDisplaySearch(this.context, Accounts.createUnreadSearch(this.context, account), true, false, false));
        return stack;
    }

    private TaskStackBuilder buildMessageListBackStack(Account account, String folderName) {
        TaskStackBuilder stack = skipFolderListInBackStack(account, folderName) ? buildAccountsBackStack() : buildFolderListBackStack(account);
        LocalSearch search = new LocalSearch(folderName);
        search.addAllowedFolder(folderName);
        search.addAccountUuid(account.getUuid());
        stack.addNextIntent(MessageList.intentDisplaySearch(this.context, search, false, true, true));
        return stack;
    }

    private TaskStackBuilder buildMessageViewBackStack(MessageReference message) {
        TaskStackBuilder stack = buildMessageListBackStack(Preferences.getPreferences(this.context).getAccount(message.getAccountUuid()), message.getFolderName());
        stack.addNextIntent(MessageList.actionDisplayMessageIntent(this.context, message));
        return stack;
    }

    private String getFolderNameOfAllMessages(List<MessageReference> messageReferences) {
        String folderName = messageReferences.get(0).getFolderName();
        for (MessageReference messageReference : messageReferences) {
            if (!TextUtils.equals(folderName, messageReference.getFolderName())) {
                return null;
            }
        }
        return folderName;
    }

    private boolean skipFolderListInBackStack(Account account, String folderName) {
        return folderName != null && folderName.equals(account.getAutoExpandFolderName());
    }

    private boolean skipAccountsInBackStack() {
        return Preferences.getPreferences(this.context).getAccounts().size() == 1;
    }
}
