package com.fsck.k9.notification;

import android.util.SparseBooleanArray;
import com.fsck.k9.Account;
import com.fsck.k9.activity.MessageReference;
import java.util.ArrayList;
import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

class NotificationData {
    static final int MAX_NUMBER_OF_MESSAGES_FOR_SUMMARY_NOTIFICATION = 5;
    static final int MAX_NUMBER_OF_STACKED_NOTIFICATIONS = 8;
    private final Account account;
    private final LinkedList<NotificationHolder> activeNotifications = new LinkedList<>();
    private final Deque<NotificationContent> additionalNotifications = new LinkedList();
    private final SparseBooleanArray notificationIdsInUse = new SparseBooleanArray();
    private int unreadMessageCount;

    public NotificationData(Account account2) {
        this.account = account2;
    }

    public AddNotificationResult addNotificationContent(NotificationContent content) {
        int notificationId;
        boolean cancelNotificationIdBeforeReuse;
        if (isMaxNumberOfActiveNotificationsReached()) {
            NotificationHolder notificationHolder = this.activeNotifications.removeLast();
            addToAdditionalNotifications(notificationHolder);
            notificationId = notificationHolder.notificationId;
            cancelNotificationIdBeforeReuse = true;
        } else {
            notificationId = getNewNotificationId();
            cancelNotificationIdBeforeReuse = false;
        }
        NotificationHolder notificationHolder2 = createNotificationHolder(notificationId, content);
        this.activeNotifications.addFirst(notificationHolder2);
        if (cancelNotificationIdBeforeReuse) {
            return AddNotificationResult.replaceNotification(notificationHolder2);
        }
        return AddNotificationResult.newNotification(notificationHolder2);
    }

    private boolean isMaxNumberOfActiveNotificationsReached() {
        return this.activeNotifications.size() == 8;
    }

    private void addToAdditionalNotifications(NotificationHolder notificationHolder) {
        this.additionalNotifications.addFirst(notificationHolder.content);
    }

    private int getNewNotificationId() {
        for (int i = 0; i < 8; i++) {
            int notificationId = NotificationIds.getNewMailStackedNotificationId(this.account, i);
            if (!isNotificationInUse(notificationId)) {
                markNotificationIdAsInUse(notificationId);
                return notificationId;
            }
        }
        throw new AssertionError("getNewNotificationId() called with no free notification ID");
    }

    private boolean isNotificationInUse(int notificationId) {
        return this.notificationIdsInUse.get(notificationId);
    }

    private void markNotificationIdAsInUse(int notificationId) {
        this.notificationIdsInUse.put(notificationId, true);
    }

    /* access modifiers changed from: package-private */
    public NotificationHolder createNotificationHolder(int notificationId, NotificationContent content) {
        return new NotificationHolder(notificationId, content);
    }

    public boolean containsStarredMessages() {
        Iterator<NotificationHolder> it = this.activeNotifications.iterator();
        while (it.hasNext()) {
            if (it.next().content.starred) {
                return true;
            }
        }
        for (NotificationContent content : this.additionalNotifications) {
            if (content.starred) {
                return true;
            }
        }
        return false;
    }

    public boolean hasAdditionalMessages() {
        return this.activeNotifications.size() > 5;
    }

    public int getAdditionalMessagesCount() {
        return this.additionalNotifications.size();
    }

    public int getNewMessagesCount() {
        return this.activeNotifications.size() + this.additionalNotifications.size();
    }

    public boolean isSingleMessageNotification() {
        return this.activeNotifications.size() == 1;
    }

    public NotificationHolder getHolderForLatestNotification() {
        return this.activeNotifications.getFirst();
    }

    public List<NotificationContent> getContentForSummaryNotification() {
        List<NotificationContent> result = new ArrayList<>(calculateNumberOfMessagesForSummaryNotification());
        Iterator<NotificationHolder> iterator = this.activeNotifications.iterator();
        int notificationCount = 0;
        while (iterator.hasNext() && notificationCount < 5) {
            result.add(iterator.next().content);
            notificationCount++;
        }
        return result;
    }

    private int calculateNumberOfMessagesForSummaryNotification() {
        return Math.min(this.activeNotifications.size(), 5);
    }

    public int[] getActiveNotificationIds() {
        int size = this.activeNotifications.size();
        int[] notificationIds = new int[size];
        for (int i = 0; i < size; i++) {
            notificationIds[i] = this.activeNotifications.get(i).notificationId;
        }
        return notificationIds;
    }

    public RemoveNotificationResult removeNotificationForMessage(MessageReference messageReference) {
        NotificationHolder holder = getNotificationHolderForMessage(messageReference);
        if (holder == null) {
            return RemoveNotificationResult.unknownNotification();
        }
        this.activeNotifications.remove(holder);
        int notificationId = holder.notificationId;
        if (this.additionalNotifications.isEmpty()) {
            return RemoveNotificationResult.cancelNotification(notificationId);
        }
        NotificationHolder replacement = createNotificationHolder(notificationId, this.additionalNotifications.removeFirst());
        this.activeNotifications.addLast(replacement);
        return RemoveNotificationResult.createNotification(replacement);
    }

    private NotificationHolder getNotificationHolderForMessage(MessageReference messageReference) {
        Iterator<NotificationHolder> it = this.activeNotifications.iterator();
        while (it.hasNext()) {
            NotificationHolder holder = it.next();
            if (messageReference.equals(holder.content.messageReference)) {
                return holder;
            }
        }
        return null;
    }

    public Account getAccount() {
        return this.account;
    }

    public int getUnreadMessageCount() {
        return this.unreadMessageCount + getNewMessagesCount();
    }

    public void setUnreadMessageCount(int unreadMessageCount2) {
        this.unreadMessageCount = unreadMessageCount2;
    }

    public ArrayList<MessageReference> getAllMessageReferences() {
        ArrayList<MessageReference> messageReferences = new ArrayList<>(this.activeNotifications.size() + this.additionalNotifications.size());
        Iterator<NotificationHolder> it = this.activeNotifications.iterator();
        while (it.hasNext()) {
            messageReferences.add(it.next().content.messageReference);
        }
        for (NotificationContent content : this.additionalNotifications) {
            messageReferences.add(content.messageReference);
        }
        return messageReferences;
    }
}
