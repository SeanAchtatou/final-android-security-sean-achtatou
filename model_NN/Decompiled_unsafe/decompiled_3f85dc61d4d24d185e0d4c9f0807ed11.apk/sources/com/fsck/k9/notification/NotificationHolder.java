package com.fsck.k9.notification;

class NotificationHolder {
    public final NotificationContent content;
    public final int notificationId;

    public NotificationHolder(int notificationId2, NotificationContent content2) {
        this.notificationId = notificationId2;
        this.content = content2;
    }
}
