package com.fsck.k9.notification;

import com.fsck.k9.activity.MessageReference;

class NotificationContent {
    public final MessageReference messageReference;
    public final CharSequence preview;
    public final String sender;
    public final boolean starred;
    public final String subject;
    public final CharSequence summary;

    public NotificationContent(MessageReference messageReference2, String sender2, String subject2, CharSequence preview2, CharSequence summary2, boolean starred2) {
        this.messageReference = messageReference2;
        this.sender = sender2;
        this.subject = subject2;
        this.preview = preview2;
        this.summary = summary2;
        this.starred = starred2;
    }
}
