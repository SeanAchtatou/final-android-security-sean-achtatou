package com.fsck.k9.notification;

import android.app.Notification;
import android.support.v4.app.NotificationCompat;
import com.fsck.k9.Account;
import com.fsck.k9.K9;
import com.fsck.k9.R;
import com.fsck.k9.controller.MessagingController;

class WearNotifications extends BaseNotifications {
    public WearNotifications(NotificationController controller, NotificationActionCreator actionCreator) {
        super(controller, actionCreator);
    }

    public Notification buildStackedNotification(Account account, NotificationHolder holder) {
        int notificationId = holder.notificationId;
        NotificationContent content = holder.content;
        NotificationCompat.Builder builder = createBigTextStyleNotification(account, holder, notificationId);
        builder.setDeleteIntent(this.actionCreator.createDismissMessagePendingIntent(this.context, content.messageReference, holder.notificationId));
        addActions(builder, account, holder);
        return builder.build();
    }

    public void addSummaryActions(NotificationCompat.Builder builder, NotificationData notificationData) {
        NotificationCompat.WearableExtender wearableExtender = new NotificationCompat.WearableExtender();
        addMarkAllAsReadAction(wearableExtender, notificationData);
        if (isDeleteActionAvailableForWear()) {
            addDeleteAllAction(wearableExtender, notificationData);
        }
        if (isArchiveActionAvailableForWear(notificationData.getAccount())) {
            addArchiveAllAction(wearableExtender, notificationData);
        }
        builder.extend(wearableExtender);
    }

    private void addMarkAllAsReadAction(NotificationCompat.WearableExtender wearableExtender, NotificationData notificationData) {
        String title = this.context.getString(R.string.notification_action_mark_all_as_read);
        Account account = notificationData.getAccount();
        wearableExtender.addAction(new NotificationCompat.Action.Builder(R.drawable.ic_action_mark_as_read_dark, title, this.actionCreator.getMarkAllAsReadPendingIntent(account, notificationData.getAllMessageReferences(), NotificationIds.getNewMailSummaryNotificationId(account))).build());
    }

    private void addDeleteAllAction(NotificationCompat.WearableExtender wearableExtender, NotificationData notificationData) {
        String title = this.context.getString(R.string.notification_action_delete_all);
        Account account = notificationData.getAccount();
        wearableExtender.addAction(new NotificationCompat.Action.Builder(R.drawable.ic_action_delete_dark, title, this.actionCreator.getDeleteAllPendingIntent(account, notificationData.getAllMessageReferences(), NotificationIds.getNewMailSummaryNotificationId(account))).build());
    }

    private void addArchiveAllAction(NotificationCompat.WearableExtender wearableExtender, NotificationData notificationData) {
        String title = this.context.getString(R.string.notification_action_archive_all);
        Account account = notificationData.getAccount();
        wearableExtender.addAction(new NotificationCompat.Action.Builder(R.drawable.ic_action_archive_dark, title, this.actionCreator.createArchiveAllPendingIntent(account, notificationData.getAllMessageReferences(), NotificationIds.getNewMailSummaryNotificationId(account))).build());
    }

    private void addActions(NotificationCompat.Builder builder, Account account, NotificationHolder holder) {
        NotificationCompat.WearableExtender wearableExtender = new NotificationCompat.WearableExtender();
        addReplyAction(wearableExtender, holder);
        addMarkAsReadAction(wearableExtender, holder);
        if (isDeleteActionAvailableForWear()) {
            addDeleteAction(wearableExtender, holder);
        }
        if (isArchiveActionAvailableForWear(account)) {
            addArchiveAction(wearableExtender, holder);
        }
        if (isSpamActionAvailableForWear(account)) {
            addMarkAsSpamAction(wearableExtender, holder);
        }
        builder.extend(wearableExtender);
    }

    private void addReplyAction(NotificationCompat.WearableExtender wearableExtender, NotificationHolder holder) {
        wearableExtender.addAction(new NotificationCompat.Action.Builder(R.drawable.ic_action_single_message_options_dark, this.context.getString(R.string.notification_action_reply), this.actionCreator.createReplyPendingIntent(holder.content.messageReference, holder.notificationId)).build());
    }

    private void addMarkAsReadAction(NotificationCompat.WearableExtender wearableExtender, NotificationHolder holder) {
        wearableExtender.addAction(new NotificationCompat.Action.Builder(R.drawable.ic_action_mark_as_read_dark, this.context.getString(R.string.notification_action_mark_as_read), this.actionCreator.createMarkMessageAsReadPendingIntent(holder.content.messageReference, holder.notificationId)).build());
    }

    private void addDeleteAction(NotificationCompat.WearableExtender wearableExtender, NotificationHolder holder) {
        wearableExtender.addAction(new NotificationCompat.Action.Builder(R.drawable.ic_action_delete_dark, this.context.getString(R.string.notification_action_delete), this.actionCreator.createDeleteMessagePendingIntent(holder.content.messageReference, holder.notificationId)).build());
    }

    private void addArchiveAction(NotificationCompat.WearableExtender wearableExtender, NotificationHolder holder) {
        wearableExtender.addAction(new NotificationCompat.Action.Builder(R.drawable.ic_action_archive_dark, this.context.getString(R.string.notification_action_archive), this.actionCreator.createArchiveMessagePendingIntent(holder.content.messageReference, holder.notificationId)).build());
    }

    private void addMarkAsSpamAction(NotificationCompat.WearableExtender wearableExtender, NotificationHolder holder) {
        wearableExtender.addAction(new NotificationCompat.Action.Builder(R.drawable.ic_action_spam_dark, this.context.getString(R.string.notification_action_spam), this.actionCreator.createMarkMessageAsSpamPendingIntent(holder.content.messageReference, holder.notificationId)).build());
    }

    private boolean isDeleteActionAvailableForWear() {
        return isDeleteActionEnabled() && !K9.confirmDeleteFromNotification();
    }

    private boolean isArchiveActionAvailableForWear(Account account) {
        String archiveFolderName = account.getArchiveFolderName();
        return archiveFolderName != null && isMovePossible(account, archiveFolderName);
    }

    private boolean isSpamActionAvailableForWear(Account account) {
        String spamFolderName = account.getSpamFolderName();
        return spamFolderName != null && !K9.confirmSpam() && isMovePossible(account, spamFolderName);
    }

    private boolean isMovePossible(Account account, String destinationFolderName) {
        if (K9.FOLDER_NONE.equalsIgnoreCase(destinationFolderName)) {
            return false;
        }
        return createMessagingController().isMoveCapable(account);
    }

    /* access modifiers changed from: package-private */
    public MessagingController createMessagingController() {
        return MessagingController.getInstance(this.context);
    }
}
