package com.fsck.k9.notification;

import android.content.Context;
import android.support.v4.app.NotificationCompat;
import com.fsck.k9.Account;
import com.fsck.k9.K9;
import com.fsck.k9.R;

abstract class BaseNotifications {
    protected static final String NOTIFICATION_GROUP_KEY = "newMailNotifications";
    protected final NotificationActionCreator actionCreator;
    protected final Context context;
    protected final NotificationController controller;

    protected BaseNotifications(NotificationController controller2, NotificationActionCreator actionCreator2) {
        this.context = controller2.getContext();
        this.controller = controller2;
        this.actionCreator = actionCreator2;
    }

    /* access modifiers changed from: protected */
    public NotificationCompat.Builder createBigTextStyleNotification(Account account, NotificationHolder holder, int notificationId) {
        String accountName = this.controller.getAccountName(account);
        NotificationContent content = holder.content;
        NotificationCompat.Builder builder = createAndInitializeNotificationBuilder(account).setTicker(content.summary).setGroup(NOTIFICATION_GROUP_KEY).setContentTitle(content.sender).setContentText(content.subject).setSubText(accountName);
        NotificationCompat.BigTextStyle style = createBigTextStyle(builder);
        style.bigText(content.preview);
        builder.setStyle(style);
        builder.setContentIntent(this.actionCreator.createViewMessagePendingIntent(content.messageReference, notificationId));
        return builder;
    }

    /* access modifiers changed from: protected */
    public NotificationCompat.Builder createAndInitializeNotificationBuilder(Account account) {
        return this.controller.createNotificationBuilder().setSmallIcon(getNewMailNotificationIcon()).setColor(account.getChipColor()).setWhen(System.currentTimeMillis()).setAutoCancel(true);
    }

    /* access modifiers changed from: protected */
    public boolean isDeleteActionEnabled() {
        K9.NotificationQuickDelete deleteOption = K9.getNotificationQuickDeleteBehaviour();
        return deleteOption == K9.NotificationQuickDelete.ALWAYS || deleteOption == K9.NotificationQuickDelete.FOR_SINGLE_MSG;
    }

    /* access modifiers changed from: protected */
    public NotificationCompat.BigTextStyle createBigTextStyle(NotificationCompat.Builder builder) {
        return new NotificationCompat.BigTextStyle(builder);
    }

    private int getNewMailNotificationIcon() {
        return R.drawable.fedserdenhoutlook_action_notification_icon;
    }
}
