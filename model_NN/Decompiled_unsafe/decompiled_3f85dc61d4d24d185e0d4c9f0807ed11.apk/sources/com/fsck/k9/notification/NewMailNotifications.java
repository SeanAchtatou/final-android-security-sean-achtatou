package com.fsck.k9.notification;

import android.app.Notification;
import android.support.v4.app.NotificationManagerCompat;
import android.util.SparseArray;
import com.fsck.k9.Account;
import com.fsck.k9.K9;
import com.fsck.k9.activity.MessageReference;
import com.fsck.k9.mailstore.LocalMessage;

class NewMailNotifications {
    private final NotificationContentCreator contentCreator;
    private final NotificationController controller;
    private final DeviceNotifications deviceNotifications;
    private final Object lock = new Object();
    private final SparseArray<NotificationData> notifications = new SparseArray<>();
    private final WearNotifications wearNotifications;

    NewMailNotifications(NotificationController controller2, NotificationContentCreator contentCreator2, DeviceNotifications deviceNotifications2, WearNotifications wearNotifications2) {
        this.controller = controller2;
        this.deviceNotifications = deviceNotifications2;
        this.wearNotifications = wearNotifications2;
        this.contentCreator = contentCreator2;
    }

    public static NewMailNotifications newInstance(NotificationController controller2, NotificationActionCreator actionCreator) {
        NotificationContentCreator contentCreator2 = new NotificationContentCreator(controller2.getContext());
        WearNotifications wearNotifications2 = new WearNotifications(controller2, actionCreator);
        return new NewMailNotifications(controller2, contentCreator2, DeviceNotifications.newInstance(controller2, actionCreator, wearNotifications2), wearNotifications2);
    }

    public void addNewMailNotification(Account account, LocalMessage message, int unreadMessageCount) {
        NotificationContent content = this.contentCreator.createFromMessage(account, message);
        synchronized (this.lock) {
            NotificationData notificationData = getOrCreateNotificationData(account, unreadMessageCount);
            AddNotificationResult result = notificationData.addNotificationContent(content);
            if (result.shouldCancelNotification()) {
                cancelNotification(result.getNotificationId());
            }
            createStackedNotification(account, result.getNotificationHolder());
            createSummaryNotification(account, notificationData, false);
        }
    }

    public void removeNewMailNotification(Account account, MessageReference messageReference) {
        synchronized (this.lock) {
            NotificationData notificationData = getNotificationData(account);
            if (notificationData != null) {
                RemoveNotificationResult result = notificationData.removeNotificationForMessage(messageReference);
                if (!result.isUnknownNotification()) {
                    cancelNotification(result.getNotificationId());
                    if (result.shouldCreateNotification()) {
                        createStackedNotification(account, result.getNotificationHolder());
                    }
                    updateSummaryNotification(account, notificationData);
                }
            }
        }
    }

    public void clearNewMailNotifications(Account account) {
        NotificationData notificationData;
        synchronized (this.lock) {
            notificationData = removeNotificationData(account);
        }
        if (notificationData != null) {
            for (int notificationId : notificationData.getActiveNotificationIds()) {
                cancelNotification(notificationId);
            }
            cancelNotification(NotificationIds.getNewMailSummaryNotificationId(account));
        }
    }

    private NotificationData getOrCreateNotificationData(Account account, int unreadMessageCount) {
        NotificationData notificationData = getNotificationData(account);
        if (notificationData != null) {
            return notificationData;
        }
        int accountNumber = account.getAccountNumber();
        NotificationData newNotificationHolder = createNotificationData(account, unreadMessageCount);
        this.notifications.put(accountNumber, newNotificationHolder);
        return newNotificationHolder;
    }

    private NotificationData getNotificationData(Account account) {
        return this.notifications.get(account.getAccountNumber());
    }

    private NotificationData removeNotificationData(Account account) {
        int accountNumber = account.getAccountNumber();
        NotificationData notificationData = this.notifications.get(accountNumber);
        this.notifications.remove(accountNumber);
        return notificationData;
    }

    /* access modifiers changed from: package-private */
    public NotificationData createNotificationData(Account account, int unreadMessageCount) {
        NotificationData notificationData = new NotificationData(account);
        notificationData.setUnreadMessageCount(unreadMessageCount);
        return notificationData;
    }

    private void cancelNotification(int notificationId) {
        getNotificationManager().cancel(notificationId);
    }

    private void updateSummaryNotification(Account account, NotificationData notificationData) {
        if (notificationData.getNewMessagesCount() == 0) {
            clearNewMailNotifications(account);
        } else {
            createSummaryNotification(account, notificationData, true);
        }
    }

    private void createSummaryNotification(Account account, NotificationData notificationData, boolean silent) {
        Notification notification = this.deviceNotifications.buildSummaryNotification(account, notificationData, silent);
        getNotificationManager().notify(NotificationIds.getNewMailSummaryNotificationId(account), notification);
    }

    private void createStackedNotification(Account account, NotificationHolder holder) {
        if (!isPrivacyModeEnabled()) {
            Notification notification = this.wearNotifications.buildStackedNotification(account, holder);
            getNotificationManager().notify(holder.notificationId, notification);
        }
    }

    private boolean isPrivacyModeEnabled() {
        return K9.getNotificationHideSubject() != K9.NotificationHideSubject.NEVER;
    }

    private NotificationManagerCompat getNotificationManager() {
        return this.controller.getNotificationManager();
    }
}
