package com.fsck.k9;

import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.text.format.Time;
import android.util.Log;
import com.fsck.k9.Account;
import com.fsck.k9.account.AndroidAccountOAuth2TokenStore;
import com.fsck.k9.activity.MessageCompose;
import com.fsck.k9.controller.MessagingController;
import com.fsck.k9.controller.MessagingListener;
import com.fsck.k9.mail.Address;
import com.fsck.k9.mail.K9MailLib;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.internet.BinaryTempFileBody;
import com.fsck.k9.mail.ssl.LocalKeyStore;
import com.fsck.k9.preferences.Storage;
import com.fsck.k9.preferences.StorageEditor;
import com.fsck.k9.provider.UnreadWidgetProvider;
import com.fsck.k9.service.BootReceiver;
import com.fsck.k9.service.MailService;
import com.fsck.k9.service.ShutdownReceiver;
import com.fsck.k9.service.StorageGoneReceiver;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.SynchronousQueue;

public class K9 extends Application {
    public static final int BOOT_RECEIVER_WAKE_LOCK_TIMEOUT = 60000;
    private static final String DATABASE_VERSION_CACHE = "database_version_cache";
    public static boolean DEBUG = false;
    public static boolean DEBUG_SENSITIVE = false;
    public static final int DEFAULT_VISIBLE_LIMIT = 25;
    public static boolean DEVELOPER_MODE = false;
    public static final String ERROR_FOLDER_NAME = "Mail-Errors";
    public static final String FOLDER_NONE = "-NONE-";
    public static final String IDENTITY_HEADER = "X-K9mail-Identity";
    private static final String KEY_LAST_ACCOUNT_DATABASE_VERSION = "last_account_database_version";
    public static final String LOCAL_UID_PREFIX = "K9LOCAL:";
    public static final String LOG_TAG = "k9";
    public static final int MAIL_SERVICE_WAKE_LOCK_TIMEOUT = 60000;
    public static final int MANUAL_WAKE_LOCK_TIMEOUT = 120000;
    public static final int MAX_ATTACHMENT_DOWNLOAD_SIZE = 134217728;
    public static final int MAX_SEND_ATTEMPTS = 5;
    public static final int PUSH_WAKE_LOCK_TIMEOUT = 60000;
    public static final String REMOTE_UID_PREFIX = "K9REMOTE:";
    public static final int WAKE_LOCK_TIMEOUT = 600000;
    public static Application app = null;
    private static BACKGROUND_OPS backgroundOps = BACKGROUND_OPS.ALWAYS;
    private static Theme composerTheme = Theme.USE_GLOBAL;
    private static final FontSizes fontSizes = new FontSizes();
    private static String language = "";
    public static final String logFile = null;
    private static boolean mAnimations = false;
    private static String mAttachmentDefaultPath = "";
    private static boolean mAutofitWidth;
    private static boolean mChangeContactNameColor = false;
    private static boolean mConfirmDelete = false;
    private static boolean mConfirmDeleteFromNotification = true;
    private static boolean mConfirmDeleteStarred = false;
    private static boolean mConfirmDiscardMessage = true;
    private static boolean mConfirmSpam = false;
    private static int mContactNameColor = -16777073;
    private static boolean mCountSearchMessages = false;
    private static boolean mGesturesEnabled = true;
    private static boolean mHideSpecialAccounts = false;
    private static boolean mHideTimeZone = false;
    private static boolean mHideUserAgent = false;
    private static boolean mMeasureAccounts = false;
    private static boolean mMessageListCheckboxes = true;
    private static int mMessageListPreviewLines = 2;
    private static boolean mMessageListSenderAboveSubject = true;
    private static boolean mMessageListStars = true;
    private static boolean mMessageViewFixedWidthFont = true;
    private static boolean mMessageViewReturnToList = false;
    private static boolean mMessageViewShowNext = false;
    private static boolean mNotificationDuringQuietTimeEnabled = true;
    private static boolean mQuietTimeEnabled = true;
    private static String mQuietTimeEnds = null;
    private static String mQuietTimeStarts = null;
    private static boolean mShowContactName = true;
    private static boolean mShowCorrespondentNames = true;
    private static Map<Account.SortType, Boolean> mSortAscending = new HashMap();
    private static Account.SortType mSortType;
    private static boolean mStartIntegratedInbox = false;
    private static boolean mUseVolumeKeysForListNavigation = false;
    private static boolean mUseVolumeKeysForNavigation = false;
    private static boolean mWrapFolderNames = false;
    private static Theme messageViewTheme = Theme.USE_GLOBAL;
    private static final List<ApplicationAware> observers = new ArrayList();
    private static boolean sChipEnabledOnSingleAccount = false;
    private static boolean sColorizeMissingContactPictures = true;
    private static SharedPreferences sDatabaseVersionCache;
    private static boolean sDatabasesUpToDate = false;
    private static boolean sInitialized = false;
    private static LockScreenNotificationVisibility sLockScreenNotificationVisibility = LockScreenNotificationVisibility.MESSAGE_COUNT;
    private static boolean sMessageViewArchiveActionVisible = false;
    private static boolean sMessageViewCopyActionVisible = false;
    private static boolean sMessageViewDeleteActionVisible = true;
    private static boolean sMessageViewMoveActionVisible = false;
    private static boolean sMessageViewSpamActionVisible = false;
    private static NotificationHideSubject sNotificationHideSubject = NotificationHideSubject.NEVER;
    private static NotificationQuickDelete sNotificationQuickDelete = NotificationQuickDelete.ALWAYS;
    private static boolean sShowContactPicture = true;
    private static SplitViewMode sSplitViewMode = SplitViewMode.NEVER;
    private static boolean sThreadedViewEnabled = true;
    private static boolean sToolBarCollapsible = true;
    private static boolean sUseBackgroundAsUnreadIndicator = true;
    public static File tempDirectory;
    private static Theme theme = Theme.LIGHT;
    private static boolean useFixedMessageTheme = true;

    public interface ApplicationAware {
        void initializeComponent(Application application);
    }

    public enum BACKGROUND_OPS {
        ALWAYS,
        NEVER,
        WHEN_CHECKED_AUTO_SYNC
    }

    public static class Intents {

        public static class EmailReceived {
            public static final String ACTION_EMAIL_DELETED = "yahoo.mail.app.intent.action.EMAIL_DELETED";
            public static final String ACTION_EMAIL_RECEIVED = "yahoo.mail.app.intent.action.EMAIL_RECEIVED";
            public static final String ACTION_REFRESH_OBSERVER = "yahoo.mail.app.intent.action.REFRESH_OBSERVER";
            public static final String EXTRA_ACCOUNT = "yahoo.mail.app.intent.extra.ACCOUNT";
            public static final String EXTRA_BCC = "yahoo.mail.app.intent.extra.BCC";
            public static final String EXTRA_CC = "yahoo.mail.app.intent.extra.CC";
            public static final String EXTRA_FOLDER = "yahoo.mail.app.intent.extra.FOLDER";
            public static final String EXTRA_FROM = "yahoo.mail.app.intent.extra.FROM";
            public static final String EXTRA_FROM_SELF = "yahoo.mail.app.intent.extra.FROM_SELF";
            public static final String EXTRA_SENT_DATE = "yahoo.mail.app.intent.extra.SENT_DATE";
            public static final String EXTRA_SUBJECT = "yahoo.mail.app.intent.extra.SUBJECT";
            public static final String EXTRA_TO = "yahoo.mail.app.intent.extra.TO";
        }

        public static class Share {
            public static final String EXTRA_FROM = "yahoo.mail.app.intent.extra.SENDER";
        }
    }

    public enum LockScreenNotificationVisibility {
        EVERYTHING,
        SENDERS,
        MESSAGE_COUNT,
        APP_NAME,
        NOTHING
    }

    public enum NotificationHideSubject {
        ALWAYS,
        WHEN_LOCKED,
        NEVER
    }

    public enum NotificationQuickDelete {
        ALWAYS,
        FOR_SINGLE_MSG,
        NEVER
    }

    public enum SplitViewMode {
        ALWAYS,
        NEVER,
        WHEN_IN_LANDSCAPE
    }

    public enum Theme {
        LIGHT,
        DARK,
        USE_GLOBAL
    }

    public static void setServicesEnabled(Context context) {
        setServicesEnabled(context, Preferences.getPreferences(context).getAvailableAccounts().size() > 0, null);
    }

    private static void setServicesEnabled(Context context, boolean enabled, Integer wakeLockId) {
        boolean alreadyEnabled;
        int i;
        PackageManager pm = context.getPackageManager();
        if (!enabled && pm.getComponentEnabledSetting(new ComponentName(context, MailService.class)) == 1) {
            MailService.actionReset(context, wakeLockId);
        }
        for (Class<?> clazz : new Class[]{MessageCompose.class, BootReceiver.class, MailService.class}) {
            if (pm.getComponentEnabledSetting(new ComponentName(context, clazz)) == 1) {
                alreadyEnabled = true;
            } else {
                alreadyEnabled = false;
            }
            if (enabled != alreadyEnabled) {
                ComponentName componentName = new ComponentName(context, clazz);
                if (enabled) {
                    i = 1;
                } else {
                    i = 2;
                }
                pm.setComponentEnabledSetting(componentName, i, 1);
            }
        }
        if (enabled && pm.getComponentEnabledSetting(new ComponentName(context, MailService.class)) == 1) {
            MailService.actionReset(context, wakeLockId);
        }
    }

    /* access modifiers changed from: protected */
    public void registerReceivers() {
        StorageGoneReceiver receiver = new StorageGoneReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.intent.action.MEDIA_EJECT");
        filter.addAction("android.intent.action.MEDIA_UNMOUNTED");
        filter.addDataScheme("file");
        final BlockingQueue<Handler> queue = new SynchronousQueue<>();
        new Thread(new Runnable() {
            public void run() {
                Looper.prepare();
                try {
                    queue.put(new Handler());
                } catch (InterruptedException e) {
                    Log.e("k9", "", e);
                }
                Looper.loop();
            }
        }, "Unmount-thread").start();
        try {
            registerReceiver(receiver, filter, null, (Handler) queue.take());
            Log.i("k9", "Registered: unmount receiver");
        } catch (InterruptedException e) {
            Log.e("k9", "Unable to register unmount receiver", e);
        }
        registerReceiver(new ShutdownReceiver(), new IntentFilter("android.intent.action.ACTION_SHUTDOWN"));
        Log.i("k9", "Registered: shutdown receiver");
    }

    public static void save(StorageEditor editor) {
        editor.putBoolean("enableDebugLogging", DEBUG);
        editor.putBoolean("enableSensitiveLogging", DEBUG_SENSITIVE);
        editor.putString("backgroundOperations", backgroundOps.name());
        editor.putBoolean("animations", mAnimations);
        editor.putBoolean("gesturesEnabled", mGesturesEnabled);
        editor.putBoolean("useVolumeKeysForNavigation", mUseVolumeKeysForNavigation);
        editor.putBoolean("useVolumeKeysForListNavigation", mUseVolumeKeysForListNavigation);
        editor.putBoolean("autofitWidth", mAutofitWidth);
        editor.putBoolean("quietTimeEnabled", mQuietTimeEnabled);
        editor.putBoolean("notificationDuringQuietTimeEnabled", mNotificationDuringQuietTimeEnabled);
        editor.putString("quietTimeStarts", mQuietTimeStarts);
        editor.putString("quietTimeEnds", mQuietTimeEnds);
        editor.putBoolean("startIntegratedInbox", mStartIntegratedInbox);
        editor.putBoolean("measureAccounts", mMeasureAccounts);
        editor.putBoolean("countSearchMessages", mCountSearchMessages);
        editor.putBoolean("messageListSenderAboveSubject", mMessageListSenderAboveSubject);
        editor.putBoolean("hideSpecialAccounts", mHideSpecialAccounts);
        editor.putBoolean("messageListStars", mMessageListStars);
        editor.putInt("messageListPreviewLines", mMessageListPreviewLines);
        editor.putBoolean("messageListCheckboxes", mMessageListCheckboxes);
        editor.putBoolean("showCorrespondentNames", mShowCorrespondentNames);
        editor.putBoolean("showContactName", mShowContactName);
        editor.putBoolean("showContactPicture", sShowContactPicture);
        editor.putBoolean("changeRegisteredNameColor", mChangeContactNameColor);
        editor.putInt("registeredNameColor", mContactNameColor);
        editor.putBoolean("messageViewFixedWidthFont", mMessageViewFixedWidthFont);
        editor.putBoolean("messageViewReturnToList", mMessageViewReturnToList);
        editor.putBoolean("messageViewShowNext", mMessageViewShowNext);
        editor.putBoolean("wrapFolderNames", mWrapFolderNames);
        editor.putBoolean("hideUserAgent", mHideUserAgent);
        editor.putBoolean("hideTimeZone", mHideTimeZone);
        editor.putString("language", language);
        editor.putInt("theme", theme.ordinal());
        editor.putInt("messageViewTheme", messageViewTheme.ordinal());
        editor.putInt("messageComposeTheme", composerTheme.ordinal());
        editor.putBoolean("fixedMessageViewTheme", useFixedMessageTheme);
        editor.putBoolean("confirmDelete", mConfirmDelete);
        editor.putBoolean("confirmDiscardMessage", mConfirmDiscardMessage);
        editor.putBoolean("confirmDeleteStarred", mConfirmDeleteStarred);
        editor.putBoolean("confirmSpam", mConfirmSpam);
        editor.putBoolean("confirmDeleteFromNotification", mConfirmDeleteFromNotification);
        editor.putString("sortTypeEnum", mSortType.name());
        editor.putBoolean("sortAscending", mSortAscending.get(mSortType).booleanValue());
        editor.putString("notificationHideSubject", sNotificationHideSubject.toString());
        editor.putString("notificationQuickDelete", sNotificationQuickDelete.toString());
        editor.putString("lockScreenNotificationVisibility", sLockScreenNotificationVisibility.toString());
        editor.putString("attachmentdefaultpath", mAttachmentDefaultPath);
        editor.putBoolean("useBackgroundAsUnreadIndicator", sUseBackgroundAsUnreadIndicator);
        editor.putBoolean("threadedView", sThreadedViewEnabled);
        editor.putString("splitViewMode", sSplitViewMode.name());
        editor.putBoolean("colorizeMissingContactPictures", sColorizeMissingContactPictures);
        editor.putBoolean("messageViewArchiveActionVisible", sMessageViewArchiveActionVisible);
        editor.putBoolean("messageViewDeleteActionVisible", sMessageViewDeleteActionVisible);
        editor.putBoolean("messageViewMoveActionVisible", sMessageViewMoveActionVisible);
        editor.putBoolean("messageViewCopyActionVisible", sMessageViewCopyActionVisible);
        editor.putBoolean("messageViewSpamActionVisible", sMessageViewSpamActionVisible);
        editor.putBoolean("ToolBarCollapsible", sToolBarCollapsible);
        editor.putBoolean("ChipEnabledOnSingleAccount", sChipEnabledOnSingleAccount);
        fontSizes.save(editor);
    }

    public void onCreate() {
        if (DEVELOPER_MODE) {
            StrictMode.enableDefaults();
        }
        PRNGFixes.apply();
        super.onCreate();
        app = this;
        Globals.setContext(this);
        Globals.setOAuth2TokenProvider(new AndroidAccountOAuth2TokenStore(this));
        K9MailLib.setDebugStatus(new K9MailLib.DebugStatus() {
            public boolean enabled() {
                return K9.DEBUG;
            }

            public boolean debugSensitive() {
                return K9.DEBUG_SENSITIVE;
            }
        });
        checkCachedDatabaseVersion();
        loadPrefs(Preferences.getPreferences(this));
        BinaryTempFileBody.setTempDirectory(getCacheDir());
        LocalKeyStore.setKeyStoreLocation(getDir("KeyStore", 0).toString());
        setServicesEnabled(this);
        registerReceivers();
        MessagingController.getInstance(this).addListener(new MessagingListener() {
            private void broadcastIntent(String action, Account account, String folder, Message message) {
                Intent intent = new Intent(action, Uri.parse("email://messages/" + account.getAccountNumber() + "/" + Uri.encode(folder) + "/" + Uri.encode(message.getUid())));
                intent.putExtra(Intents.EmailReceived.EXTRA_ACCOUNT, account.getDescription());
                intent.putExtra(Intents.EmailReceived.EXTRA_FOLDER, folder);
                intent.putExtra(Intents.EmailReceived.EXTRA_SENT_DATE, message.getSentDate());
                intent.putExtra(Intents.EmailReceived.EXTRA_FROM, Address.toString(message.getFrom()));
                intent.putExtra(Intents.EmailReceived.EXTRA_TO, Address.toString(message.getRecipients(Message.RecipientType.TO)));
                intent.putExtra(Intents.EmailReceived.EXTRA_CC, Address.toString(message.getRecipients(Message.RecipientType.CC)));
                intent.putExtra(Intents.EmailReceived.EXTRA_BCC, Address.toString(message.getRecipients(Message.RecipientType.BCC)));
                intent.putExtra(Intents.EmailReceived.EXTRA_SUBJECT, message.getSubject());
                intent.putExtra(Intents.EmailReceived.EXTRA_FROM_SELF, account.isAnIdentity(message.getFrom()));
                K9.this.sendBroadcast(intent);
                if (K9.DEBUG) {
                    Log.d("k9", "Broadcasted: action=" + action + " account=" + account.getDescription() + " folder=" + folder + " message uid=" + message.getUid());
                }
            }

            private void updateUnreadWidget() {
                try {
                    UnreadWidgetProvider.updateUnreadCount(K9.this);
                } catch (Exception e) {
                    if (K9.DEBUG) {
                        Log.e("k9", "Error while updating unread widget(s)", e);
                    }
                }
            }

            public void synchronizeMailboxRemovedMessage(Account account, String folder, Message message) {
                broadcastIntent(Intents.EmailReceived.ACTION_EMAIL_DELETED, account, folder, message);
                updateUnreadWidget();
            }

            public void messageDeleted(Account account, String folder, Message message) {
                broadcastIntent(Intents.EmailReceived.ACTION_EMAIL_DELETED, account, folder, message);
                updateUnreadWidget();
            }

            public void synchronizeMailboxNewMessage(Account account, String folder, Message message) {
                broadcastIntent(Intents.EmailReceived.ACTION_EMAIL_RECEIVED, account, folder, message);
                updateUnreadWidget();
            }

            public void folderStatusChanged(Account account, String folderName, int unreadMessageCount) {
                updateUnreadWidget();
                Intent intent = new Intent(Intents.EmailReceived.ACTION_REFRESH_OBSERVER, (Uri) null);
                intent.putExtra(Intents.EmailReceived.EXTRA_ACCOUNT, account.getDescription());
                intent.putExtra(Intents.EmailReceived.EXTRA_FOLDER, folderName);
                K9.this.sendBroadcast(intent);
            }
        });
        notifyObservers();
    }

    public void checkCachedDatabaseVersion() {
        sDatabaseVersionCache = getSharedPreferences(DATABASE_VERSION_CACHE, 0);
        if (sDatabaseVersionCache.getInt(KEY_LAST_ACCOUNT_DATABASE_VERSION, 0) >= 55) {
            setDatabasesUpToDate(false);
        }
    }

    public static void loadPrefs(Preferences prefs) {
        Storage storage = prefs.getStorage();
        DEBUG = storage.getBoolean("enableDebugLogging", false);
        DEBUG_SENSITIVE = storage.getBoolean("enableSensitiveLogging", false);
        mAnimations = storage.getBoolean("animations", false);
        mGesturesEnabled = storage.getBoolean("gesturesEnabled", true);
        mUseVolumeKeysForNavigation = storage.getBoolean("useVolumeKeysForNavigation", false);
        mUseVolumeKeysForListNavigation = storage.getBoolean("useVolumeKeysForListNavigation", false);
        mStartIntegratedInbox = storage.getBoolean("startIntegratedInbox", false);
        mMeasureAccounts = storage.getBoolean("measureAccounts", false);
        mCountSearchMessages = storage.getBoolean("countSearchMessages", false);
        mHideSpecialAccounts = storage.getBoolean("hideSpecialAccounts", false);
        mMessageListSenderAboveSubject = storage.getBoolean("messageListSenderAboveSubject", true);
        mMessageListCheckboxes = storage.getBoolean("messageListCheckboxes", false);
        mMessageListStars = storage.getBoolean("messageListStars", true);
        mMessageListPreviewLines = storage.getInt("messageListPreviewLines", 2);
        mAutofitWidth = storage.getBoolean("autofitWidth", true);
        mQuietTimeEnabled = storage.getBoolean("quietTimeEnabled", true);
        mNotificationDuringQuietTimeEnabled = storage.getBoolean("notificationDuringQuietTimeEnabled", true);
        mQuietTimeStarts = storage.getString("quietTimeStarts", "21:00");
        mQuietTimeEnds = storage.getString("quietTimeEnds", "9:00");
        mShowCorrespondentNames = storage.getBoolean("showCorrespondentNames", true);
        mShowContactName = storage.getBoolean("showContactName", true);
        sShowContactPicture = storage.getBoolean("showContactPicture", true);
        mChangeContactNameColor = storage.getBoolean("changeRegisteredNameColor", false);
        mContactNameColor = storage.getInt("registeredNameColor", -16777073);
        mMessageViewFixedWidthFont = storage.getBoolean("messageViewFixedWidthFont", true);
        mMessageViewReturnToList = storage.getBoolean("messageViewReturnToList", false);
        mMessageViewShowNext = storage.getBoolean("messageViewShowNext", false);
        mWrapFolderNames = storage.getBoolean("wrapFolderNames", false);
        mHideUserAgent = storage.getBoolean("hideUserAgent", false);
        mHideTimeZone = storage.getBoolean("hideTimeZone", false);
        mConfirmDelete = storage.getBoolean("confirmDelete", false);
        mConfirmDiscardMessage = storage.getBoolean("confirmDiscardMessage", true);
        mConfirmDeleteStarred = storage.getBoolean("confirmDeleteStarred", false);
        mConfirmSpam = storage.getBoolean("confirmSpam", false);
        mConfirmDeleteFromNotification = storage.getBoolean("confirmDeleteFromNotification", true);
        try {
            mSortType = Account.SortType.valueOf(storage.getString("sortTypeEnum", Account.DEFAULT_SORT_TYPE.name()));
        } catch (Exception e) {
            mSortType = Account.DEFAULT_SORT_TYPE;
        }
        mSortAscending.put(mSortType, Boolean.valueOf(storage.getBoolean("sortAscending", false)));
        String notificationHideSubject = storage.getString("notificationHideSubject", null);
        if (notificationHideSubject == null) {
            sNotificationHideSubject = storage.getBoolean("keyguardPrivacy", false) ? NotificationHideSubject.WHEN_LOCKED : NotificationHideSubject.NEVER;
        } else {
            sNotificationHideSubject = NotificationHideSubject.valueOf(notificationHideSubject);
        }
        String notificationQuickDelete = storage.getString("notificationQuickDelete", null);
        if (notificationQuickDelete != null) {
            sNotificationQuickDelete = NotificationQuickDelete.valueOf(notificationQuickDelete);
        }
        String lockScreenNotificationVisibility = storage.getString("lockScreenNotificationVisibility", null);
        if (lockScreenNotificationVisibility != null) {
            sLockScreenNotificationVisibility = LockScreenNotificationVisibility.valueOf(lockScreenNotificationVisibility);
        }
        String splitViewMode = storage.getString("splitViewMode", null);
        if (splitViewMode != null) {
            sSplitViewMode = SplitViewMode.valueOf(splitViewMode);
        }
        mAttachmentDefaultPath = storage.getString("attachmentdefaultpath", Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString());
        sUseBackgroundAsUnreadIndicator = storage.getBoolean("useBackgroundAsUnreadIndicator", true);
        sThreadedViewEnabled = storage.getBoolean("threadedView", true);
        fontSizes.load(storage);
        try {
            setBackgroundOps(BACKGROUND_OPS.valueOf(storage.getString("backgroundOperations", BACKGROUND_OPS.ALWAYS.name())));
        } catch (Exception e2) {
            setBackgroundOps(BACKGROUND_OPS.ALWAYS);
        }
        sColorizeMissingContactPictures = storage.getBoolean("colorizeMissingContactPictures", true);
        sMessageViewArchiveActionVisible = storage.getBoolean("messageViewArchiveActionVisible", false);
        sMessageViewDeleteActionVisible = storage.getBoolean("messageViewDeleteActionVisible", true);
        sMessageViewMoveActionVisible = storage.getBoolean("messageViewMoveActionVisible", false);
        sMessageViewCopyActionVisible = storage.getBoolean("messageViewCopyActionVisible", false);
        sMessageViewSpamActionVisible = storage.getBoolean("messageViewSpamActionVisible", false);
        sToolBarCollapsible = storage.getBoolean("ToolBarCollapsible", true);
        sChipEnabledOnSingleAccount = storage.getBoolean("ChipEnabledOnSingleAccount", false);
        setK9Language(storage.getString("language", ""));
        int themeValue = storage.getInt("theme", Theme.LIGHT.ordinal());
        if (themeValue == Theme.DARK.ordinal() || themeValue == 16973829) {
            setK9Theme(Theme.DARK);
        } else {
            setK9Theme(Theme.LIGHT);
        }
        setK9MessageViewThemeSetting(Theme.values()[storage.getInt("messageViewTheme", Theme.USE_GLOBAL.ordinal())]);
        setK9ComposerThemeSetting(Theme.values()[storage.getInt("messageComposeTheme", Theme.USE_GLOBAL.ordinal())]);
        setUseFixedMessageViewTheme(storage.getBoolean("fixedMessageViewTheme", true));
    }

    /* access modifiers changed from: protected */
    public void notifyObservers() {
        synchronized (observers) {
            for (ApplicationAware aware : observers) {
                if (DEBUG) {
                    Log.v("k9", "Initializing observer: " + aware);
                }
                try {
                    aware.initializeComponent(this);
                } catch (Exception e) {
                    Log.w("k9", "Failure when notifying " + aware, e);
                }
            }
            sInitialized = true;
            observers.clear();
        }
    }

    public static void registerApplicationAware(ApplicationAware component) {
        synchronized (observers) {
            if (sInitialized) {
                component.initializeComponent(app);
            } else if (!observers.contains(component)) {
                observers.add(component);
            }
        }
    }

    public static String getK9Language() {
        return language;
    }

    public static void setK9Language(String nlanguage) {
        language = nlanguage;
    }

    public static int getK9ThemeResourceId(Theme themeId) {
        return themeId == Theme.LIGHT ? R.style.Theme_K9_Light : R.style.Theme_K9_Dark;
    }

    public static int getK9ThemeResourceId() {
        return getK9ThemeResourceId(theme);
    }

    public static Theme getK9MessageViewTheme() {
        return messageViewTheme == Theme.USE_GLOBAL ? theme : messageViewTheme;
    }

    public static Theme getK9MessageViewThemeSetting() {
        return messageViewTheme;
    }

    public static Theme getK9ComposerTheme() {
        return composerTheme == Theme.USE_GLOBAL ? theme : composerTheme;
    }

    public static Theme getK9ComposerThemeSetting() {
        return composerTheme;
    }

    public static Theme getK9Theme() {
        return theme;
    }

    public static void setK9Theme(Theme ntheme) {
        if (ntheme != Theme.USE_GLOBAL) {
            theme = ntheme;
        }
    }

    public static void setK9MessageViewThemeSetting(Theme nMessageViewTheme) {
        messageViewTheme = nMessageViewTheme;
    }

    public static boolean useFixedMessageViewTheme() {
        return useFixedMessageTheme;
    }

    public static void setK9ComposerThemeSetting(Theme compTheme) {
        composerTheme = compTheme;
    }

    public static void setUseFixedMessageViewTheme(boolean useFixed) {
        useFixedMessageTheme = useFixed;
        if (!useFixedMessageTheme && messageViewTheme == Theme.USE_GLOBAL) {
            messageViewTheme = theme;
        }
    }

    public static BACKGROUND_OPS getBackgroundOps() {
        return backgroundOps;
    }

    public static boolean setBackgroundOps(BACKGROUND_OPS backgroundOps2) {
        BACKGROUND_OPS oldBackgroundOps = backgroundOps;
        backgroundOps = backgroundOps2;
        return backgroundOps2 != oldBackgroundOps;
    }

    public static boolean setBackgroundOps(String nbackgroundOps) {
        return setBackgroundOps(BACKGROUND_OPS.valueOf(nbackgroundOps));
    }

    public static boolean gesturesEnabled() {
        return mGesturesEnabled;
    }

    public static void setGesturesEnabled(boolean gestures) {
        mGesturesEnabled = gestures;
    }

    public static boolean useVolumeKeysForNavigationEnabled() {
        return mUseVolumeKeysForNavigation;
    }

    public static void setUseVolumeKeysForNavigation(boolean volume) {
        mUseVolumeKeysForNavigation = volume;
    }

    public static boolean useVolumeKeysForListNavigationEnabled() {
        return mUseVolumeKeysForListNavigation;
    }

    public static void setUseVolumeKeysForListNavigation(boolean enabled) {
        mUseVolumeKeysForListNavigation = enabled;
    }

    public static boolean autofitWidth() {
        return mAutofitWidth;
    }

    public static void setAutofitWidth(boolean autofitWidth) {
        mAutofitWidth = autofitWidth;
    }

    public static boolean getQuietTimeEnabled() {
        return mQuietTimeEnabled;
    }

    public static void setQuietTimeEnabled(boolean quietTimeEnabled) {
        mQuietTimeEnabled = quietTimeEnabled;
    }

    public static boolean isNotificationDuringQuietTimeEnabled() {
        return mNotificationDuringQuietTimeEnabled;
    }

    public static void setNotificationDuringQuietTimeEnabled(boolean notificationDuringQuietTimeEnabled) {
        mNotificationDuringQuietTimeEnabled = notificationDuringQuietTimeEnabled;
    }

    public static String getQuietTimeStarts() {
        return mQuietTimeStarts;
    }

    public static void setQuietTimeStarts(String quietTimeStarts) {
        mQuietTimeStarts = quietTimeStarts;
    }

    public static String getQuietTimeEnds() {
        return mQuietTimeEnds;
    }

    public static void setQuietTimeEnds(String quietTimeEnds) {
        mQuietTimeEnds = quietTimeEnds;
    }

    public static boolean isQuietTime() {
        if (!mQuietTimeEnabled) {
            return false;
        }
        Time time = new Time();
        time.setToNow();
        Integer startHour = Integer.valueOf(Integer.parseInt(mQuietTimeStarts.split(":")[0]));
        Integer startMinute = Integer.valueOf(Integer.parseInt(mQuietTimeStarts.split(":")[1]));
        Integer endHour = Integer.valueOf(Integer.parseInt(mQuietTimeEnds.split(":")[0]));
        Integer endMinute = Integer.valueOf(Integer.parseInt(mQuietTimeEnds.split(":")[1]));
        Integer now = Integer.valueOf((time.hour * 60) + time.minute);
        Integer quietStarts = Integer.valueOf((startHour.intValue() * 60) + startMinute.intValue());
        Integer quietEnds = Integer.valueOf((endHour.intValue() * 60) + endMinute.intValue());
        if (quietStarts.equals(quietEnds)) {
            return false;
        }
        if (quietStarts.intValue() > quietEnds.intValue()) {
            if (now.intValue() >= quietStarts.intValue() || now.intValue() <= quietEnds.intValue()) {
                return true;
            }
            return false;
        } else if (now.intValue() < quietStarts.intValue() || now.intValue() > quietEnds.intValue()) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean startIntegratedInbox() {
        return mStartIntegratedInbox;
    }

    public static void setStartIntegratedInbox(boolean startIntegratedInbox) {
        mStartIntegratedInbox = startIntegratedInbox;
    }

    public static boolean showAnimations() {
        return mAnimations;
    }

    public static void setAnimations(boolean animations) {
        mAnimations = animations;
    }

    public static int messageListPreviewLines() {
        return mMessageListPreviewLines;
    }

    public static void setMessageListPreviewLines(int lines) {
        mMessageListPreviewLines = lines;
    }

    public static boolean messageListCheckboxes() {
        return mMessageListCheckboxes;
    }

    public static void setMessageListCheckboxes(boolean checkboxes) {
        mMessageListCheckboxes = checkboxes;
    }

    public static boolean messageListStars() {
        return mMessageListStars;
    }

    public static void setMessageListStars(boolean stars) {
        mMessageListStars = stars;
    }

    public static boolean showCorrespondentNames() {
        return mShowCorrespondentNames;
    }

    public static boolean messageListSenderAboveSubject() {
        return mMessageListSenderAboveSubject;
    }

    public static void setMessageListSenderAboveSubject(boolean sender) {
        mMessageListSenderAboveSubject = sender;
    }

    public static void setShowCorrespondentNames(boolean showCorrespondentNames) {
        mShowCorrespondentNames = showCorrespondentNames;
    }

    public static boolean showContactName() {
        return mShowContactName;
    }

    public static void setShowContactName(boolean showContactName) {
        mShowContactName = showContactName;
    }

    public static boolean changeContactNameColor() {
        return mChangeContactNameColor;
    }

    public static void setChangeContactNameColor(boolean changeContactNameColor) {
        mChangeContactNameColor = changeContactNameColor;
    }

    public static int getContactNameColor() {
        return mContactNameColor;
    }

    public static void setContactNameColor(int contactNameColor) {
        mContactNameColor = contactNameColor;
    }

    public static boolean messageViewFixedWidthFont() {
        return mMessageViewFixedWidthFont;
    }

    public static void setMessageViewFixedWidthFont(boolean fixed) {
        mMessageViewFixedWidthFont = fixed;
    }

    public static boolean messageViewReturnToList() {
        return mMessageViewReturnToList;
    }

    public static void setMessageViewReturnToList(boolean messageViewReturnToList) {
        mMessageViewReturnToList = messageViewReturnToList;
    }

    public static boolean messageViewShowNext() {
        return mMessageViewShowNext;
    }

    public static void setMessageViewShowNext(boolean messageViewShowNext) {
        mMessageViewShowNext = messageViewShowNext;
    }

    public static FontSizes getFontSizes() {
        return fontSizes;
    }

    public static boolean measureAccounts() {
        return mMeasureAccounts;
    }

    public static void setMeasureAccounts(boolean measureAccounts) {
        mMeasureAccounts = measureAccounts;
    }

    public static boolean countSearchMessages() {
        return mCountSearchMessages;
    }

    public static void setCountSearchMessages(boolean countSearchMessages) {
        mCountSearchMessages = countSearchMessages;
    }

    public static boolean isHideSpecialAccounts() {
        return mHideSpecialAccounts;
    }

    public static void setHideSpecialAccounts(boolean hideSpecialAccounts) {
        mHideSpecialAccounts = hideSpecialAccounts;
    }

    public static boolean confirmDelete() {
        return mConfirmDelete;
    }

    public static void setConfirmDelete(boolean confirm) {
        mConfirmDelete = confirm;
    }

    public static boolean confirmDeleteStarred() {
        return mConfirmDeleteStarred;
    }

    public static void setConfirmDeleteStarred(boolean confirm) {
        mConfirmDeleteStarred = confirm;
    }

    public static boolean confirmSpam() {
        return mConfirmSpam;
    }

    public static boolean confirmDiscardMessage() {
        return mConfirmDiscardMessage;
    }

    public static void setConfirmSpam(boolean confirm) {
        mConfirmSpam = confirm;
    }

    public static void setConfirmDiscardMessage(boolean confirm) {
        mConfirmDiscardMessage = confirm;
    }

    public static boolean confirmDeleteFromNotification() {
        return mConfirmDeleteFromNotification;
    }

    public static void setConfirmDeleteFromNotification(boolean confirm) {
        mConfirmDeleteFromNotification = confirm;
    }

    public static NotificationHideSubject getNotificationHideSubject() {
        return sNotificationHideSubject;
    }

    public static void setNotificationHideSubject(NotificationHideSubject mode) {
        sNotificationHideSubject = mode;
    }

    public static NotificationQuickDelete getNotificationQuickDeleteBehaviour() {
        return sNotificationQuickDelete;
    }

    public static void setNotificationQuickDeleteBehaviour(NotificationQuickDelete mode) {
        sNotificationQuickDelete = mode;
    }

    public static LockScreenNotificationVisibility getLockScreenNotificationVisibility() {
        return sLockScreenNotificationVisibility;
    }

    public static void setLockScreenNotificationVisibility(LockScreenNotificationVisibility visibility) {
        sLockScreenNotificationVisibility = visibility;
    }

    public static boolean wrapFolderNames() {
        return mWrapFolderNames;
    }

    public static void setWrapFolderNames(boolean state) {
        mWrapFolderNames = state;
    }

    public static boolean hideUserAgent() {
        return mHideUserAgent;
    }

    public static void setHideUserAgent(boolean state) {
        mHideUserAgent = state;
    }

    public static boolean hideTimeZone() {
        return mHideTimeZone;
    }

    public static void setHideTimeZone(boolean state) {
        mHideTimeZone = state;
    }

    public static String getAttachmentDefaultPath() {
        return mAttachmentDefaultPath;
    }

    public static void setAttachmentDefaultPath(String attachmentDefaultPath) {
        mAttachmentDefaultPath = attachmentDefaultPath;
    }

    public static synchronized Account.SortType getSortType() {
        Account.SortType sortType;
        synchronized (K9.class) {
            sortType = mSortType;
        }
        return sortType;
    }

    public static synchronized void setSortType(Account.SortType sortType) {
        synchronized (K9.class) {
            mSortType = sortType;
        }
    }

    public static synchronized boolean isSortAscending(Account.SortType sortType) {
        boolean booleanValue;
        synchronized (K9.class) {
            if (mSortAscending.get(sortType) == null) {
                mSortAscending.put(sortType, Boolean.valueOf(sortType.isDefaultAscending()));
            }
            booleanValue = mSortAscending.get(sortType).booleanValue();
        }
        return booleanValue;
    }

    public static synchronized void setSortAscending(Account.SortType sortType, boolean sortAscending) {
        synchronized (K9.class) {
            mSortAscending.put(sortType, Boolean.valueOf(sortAscending));
        }
    }

    public static synchronized boolean useBackgroundAsUnreadIndicator() {
        boolean z;
        synchronized (K9.class) {
            z = sUseBackgroundAsUnreadIndicator;
        }
        return z;
    }

    public static synchronized void setUseBackgroundAsUnreadIndicator(boolean enabled) {
        synchronized (K9.class) {
            sUseBackgroundAsUnreadIndicator = enabled;
        }
    }

    public static synchronized boolean isThreadedViewEnabled() {
        boolean z;
        synchronized (K9.class) {
            z = sThreadedViewEnabled;
        }
        return z;
    }

    public static synchronized void setThreadedViewEnabled(boolean enable) {
        synchronized (K9.class) {
            sThreadedViewEnabled = enable;
        }
    }

    public static synchronized SplitViewMode getSplitViewMode() {
        SplitViewMode splitViewMode;
        synchronized (K9.class) {
            splitViewMode = sSplitViewMode;
        }
        return splitViewMode;
    }

    public static synchronized void setSplitViewMode(SplitViewMode mode) {
        synchronized (K9.class) {
            sSplitViewMode = mode;
        }
    }

    public static boolean showContactPicture() {
        return sShowContactPicture;
    }

    public static void setShowContactPicture(boolean show) {
        sShowContactPicture = show;
    }

    public static boolean isColorizeMissingContactPictures() {
        return sColorizeMissingContactPictures;
    }

    public static void setColorizeMissingContactPictures(boolean enabled) {
        sColorizeMissingContactPictures = enabled;
    }

    public static boolean isMessageViewArchiveActionVisible() {
        return sMessageViewArchiveActionVisible;
    }

    public static void setMessageViewArchiveActionVisible(boolean visible) {
        sMessageViewArchiveActionVisible = visible;
    }

    public static boolean isMessageViewDeleteActionVisible() {
        return sMessageViewDeleteActionVisible;
    }

    public static void setMessageViewDeleteActionVisible(boolean visible) {
        sMessageViewDeleteActionVisible = visible;
    }

    public static boolean isMessageViewMoveActionVisible() {
        return sMessageViewMoveActionVisible;
    }

    public static void setMessageViewMoveActionVisible(boolean visible) {
        sMessageViewMoveActionVisible = visible;
    }

    public static boolean isMessageViewCopyActionVisible() {
        return sMessageViewCopyActionVisible;
    }

    public static void setMessageViewCopyActionVisible(boolean visible) {
        sMessageViewCopyActionVisible = visible;
    }

    public static boolean isMessageViewSpamActionVisible() {
        return sMessageViewSpamActionVisible;
    }

    public static void setMessageViewSpamActionVisible(boolean visible) {
        sMessageViewSpamActionVisible = visible;
    }

    public static boolean isToolBarCollapsible() {
        return sToolBarCollapsible;
    }

    public static void setToolBarCollapsible(boolean collapsible) {
        sToolBarCollapsible = collapsible;
    }

    public static boolean isChipEnabledOnSingleAccount() {
        return sChipEnabledOnSingleAccount;
    }

    public static void setChipEnabledOnSingleAccount(boolean collapsible) {
        sChipEnabledOnSingleAccount = collapsible;
    }

    public static synchronized boolean areDatabasesUpToDate() {
        boolean z;
        synchronized (K9.class) {
            z = sDatabasesUpToDate;
        }
        return z;
    }

    public static synchronized void setDatabasesUpToDate(boolean save) {
        synchronized (K9.class) {
            sDatabasesUpToDate = true;
            if (save) {
                SharedPreferences.Editor editor = sDatabaseVersionCache.edit();
                editor.putInt(KEY_LAST_ACCOUNT_DATABASE_VERSION, 55);
                editor.commit();
            }
        }
    }
}
