package com.fsck.k9.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;
import com.fsck.k9.Account;
import com.fsck.k9.Identity;
import com.fsck.k9.Preferences;
import com.fsck.k9.R;
import java.util.List;

public class ChooseIdentity extends K9ListActivity {
    public static final String EXTRA_ACCOUNT = "com.fsck.k9.ChooseIdentity_account";
    public static final String EXTRA_IDENTITY = "com.fsck.k9.ChooseIdentity_identity";
    ArrayAdapter<String> adapter;
    protected List<Identity> identities = null;
    Account mAccount;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(5);
        setContentView((int) R.layout.list_content_simple);
        getListView().setTextFilterEnabled(true);
        getListView().setItemsCanFocus(false);
        getListView().setChoiceMode(0);
        this.mAccount = Preferences.getPreferences(this).getAccount(getIntent().getStringExtra(EXTRA_ACCOUNT));
        this.adapter = new ArrayAdapter<>(this, 17367043);
        setListAdapter(this.adapter);
        setupClickListeners();
    }

    public void onResume() {
        super.onResume();
        refreshView();
    }

    /* access modifiers changed from: protected */
    public void refreshView() {
        this.adapter.setNotifyOnChange(false);
        this.adapter.clear();
        this.identities = this.mAccount.getIdentities();
        for (Identity identity : this.identities) {
            String description = identity.getDescription();
            if (description == null || description.trim().isEmpty()) {
                description = getString(R.string.message_view_from_format, new Object[]{identity.getName(), identity.getEmail()});
            }
            this.adapter.add(description);
        }
        this.adapter.notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public void setupClickListeners() {
        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                String email = ChooseIdentity.this.mAccount.getIdentity(position).getEmail();
                if (email == null || email.trim().equals("")) {
                    Toast.makeText(ChooseIdentity.this, ChooseIdentity.this.getString(R.string.identity_has_no_email), 1).show();
                    return;
                }
                Intent intent = new Intent();
                intent.putExtra(ChooseIdentity.EXTRA_IDENTITY, ChooseIdentity.this.mAccount.getIdentity(position));
                ChooseIdentity.this.setResult(-1, intent);
                ChooseIdentity.this.finish();
            }
        });
    }
}
