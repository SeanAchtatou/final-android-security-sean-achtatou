package com.fsck.k9.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.format.DateUtils;
import com.fsck.k9.Account;
import com.fsck.k9.AccountStats;
import com.fsck.k9.R;
import com.fsck.k9.controller.MessagingListener;
import com.fsck.k9.service.MailService;

public class ActivityListener extends MessagingListener {
    private Account mAccount = null;
    private int mFolderCompleted = 0;
    private int mFolderTotal = 0;
    private String mLoadingAccountDescription = null;
    private String mLoadingFolderName = null;
    private String mLoadingHeaderFolderName = null;
    private String mProcessingAccountDescription = null;
    private String mProcessingCommandTitle = null;
    private String mSendingAccountDescription = null;
    private BroadcastReceiver mTickReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            ActivityListener.this.informUserOfStatus();
        }
    };

    public String getOperation(Context context) {
        if (this.mLoadingAccountDescription == null && this.mSendingAccountDescription == null && this.mLoadingHeaderFolderName == null && this.mProcessingAccountDescription == null) {
            long nextPollTime = MailService.getNextPollTime();
            if (nextPollTime != -1) {
                return context.getString(R.string.status_next_poll, DateUtils.getRelativeTimeSpanString(nextPollTime, System.currentTimeMillis(), 60000, 0));
            } else if (MailService.isSyncDisabled()) {
                return context.getString(R.string.status_syncing_off);
            } else {
                return "";
            }
        } else {
            String progress = this.mFolderTotal > 0 ? context.getString(R.string.folder_progress, Integer.valueOf(this.mFolderCompleted), Integer.valueOf(this.mFolderTotal)) : "";
            if (this.mLoadingFolderName != null || this.mLoadingHeaderFolderName != null) {
                String displayName = this.mLoadingFolderName;
                if (this.mAccount != null && this.mAccount.getInboxFolderName() != null && this.mAccount.getInboxFolderName().equalsIgnoreCase(displayName)) {
                    displayName = context.getString(R.string.special_mailbox_name_inbox);
                } else if (this.mAccount != null && this.mAccount.getOutboxFolderName().equals(displayName)) {
                    displayName = context.getString(R.string.special_mailbox_name_outbox);
                }
                if (this.mLoadingHeaderFolderName != null) {
                    return context.getString(R.string.status_loading_account_folder_headers, this.mLoadingAccountDescription, displayName, progress);
                }
                return context.getString(R.string.status_loading_account_folder, this.mLoadingAccountDescription, displayName, progress);
            } else if (this.mSendingAccountDescription != null) {
                return context.getString(R.string.status_sending_account, this.mSendingAccountDescription, progress);
            } else if (this.mProcessingAccountDescription == null) {
                return "";
            } else {
                Object[] objArr = new Object[3];
                objArr[0] = this.mProcessingAccountDescription;
                objArr[1] = this.mProcessingCommandTitle != null ? this.mProcessingCommandTitle : "";
                objArr[2] = progress;
                return context.getString(R.string.status_processing_account, objArr);
            }
        }
    }

    public void onResume(Context context) {
        context.registerReceiver(this.mTickReceiver, new IntentFilter("android.intent.action.TIME_TICK"));
    }

    public void onPause(Context context) {
        context.unregisterReceiver(this.mTickReceiver);
    }

    public void informUserOfStatus() {
    }

    public void synchronizeMailboxFinished(Account account, String folder, int totalMessagesInMailbox, int numNewMessages) {
        this.mLoadingAccountDescription = null;
        this.mLoadingFolderName = null;
        this.mAccount = null;
        informUserOfStatus();
    }

    public void synchronizeMailboxStarted(Account account, String folder) {
        this.mLoadingAccountDescription = account.getDescription();
        this.mLoadingFolderName = folder;
        this.mAccount = account;
        this.mFolderCompleted = 0;
        this.mFolderTotal = 0;
        informUserOfStatus();
    }

    public void synchronizeMailboxHeadersStarted(Account account, String folder) {
        this.mLoadingHeaderFolderName = folder;
        informUserOfStatus();
    }

    public void synchronizeMailboxHeadersProgress(Account account, String folder, int completed, int total) {
        this.mFolderCompleted = completed;
        this.mFolderTotal = total;
        informUserOfStatus();
    }

    public void synchronizeMailboxHeadersFinished(Account account, String folder, int total, int completed) {
        this.mLoadingHeaderFolderName = null;
        this.mFolderCompleted = 0;
        this.mFolderTotal = 0;
        informUserOfStatus();
    }

    public void synchronizeMailboxProgress(Account account, String folder, int completed, int total) {
        this.mFolderCompleted = completed;
        this.mFolderTotal = total;
        informUserOfStatus();
    }

    public void synchronizeMailboxFailed(Account account, String folder, String message) {
        this.mLoadingAccountDescription = null;
        this.mLoadingFolderName = null;
        this.mAccount = null;
        informUserOfStatus();
    }

    public void sendPendingMessagesStarted(Account account) {
        this.mSendingAccountDescription = account.getDescription();
        informUserOfStatus();
    }

    public void sendPendingMessagesCompleted(Account account) {
        this.mSendingAccountDescription = null;
        informUserOfStatus();
    }

    public void sendPendingMessagesFailed(Account account) {
        this.mSendingAccountDescription = null;
        informUserOfStatus();
    }

    public void pendingCommandsProcessing(Account account) {
        this.mProcessingAccountDescription = account.getDescription();
        this.mFolderCompleted = 0;
        this.mFolderTotal = 0;
        informUserOfStatus();
    }

    public void pendingCommandsFinished(Account account) {
        this.mProcessingAccountDescription = null;
        informUserOfStatus();
    }

    public void pendingCommandStarted(Account account, String commandTitle) {
        this.mProcessingCommandTitle = commandTitle;
        informUserOfStatus();
    }

    public void pendingCommandCompleted(Account account, String commandTitle) {
        this.mProcessingCommandTitle = null;
        informUserOfStatus();
    }

    public void searchStats(AccountStats stats) {
        informUserOfStatus();
    }

    public void systemStatusChanged() {
        informUserOfStatus();
    }

    public void folderStatusChanged(Account account, String folder, int unreadMessageCount) {
        informUserOfStatus();
    }

    public int getFolderCompleted() {
        return this.mFolderCompleted;
    }

    public int getFolderTotal() {
        return this.mFolderTotal;
    }
}
