package com.fsck.k9.activity;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import com.fsck.k9.K9;

public class K9PreferenceActivity extends PreferenceActivity {
    public String prefKey = null;

    public void onCreate(Bundle icicle) {
        K9ActivityCommon.setLanguage(this, K9.getK9Language());
        setTheme(K9.getK9ThemeResourceId());
        super.onCreate(icicle);
        ActionBar mActionBar = getActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(true);
        mActionBar.setDisplayShowHomeEnabled(false);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        outState.putString("prefKey", this.prefKey);
        super.onSaveInstanceState(outState);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle state) {
        PreferenceScreen preferenceScreen;
        Preference preference;
        super.onRestoreInstanceState(state);
        this.prefKey = state.getString("prefKey");
        if (this.prefKey != null && (preferenceScreen = getPreferenceScreen()) != null && (preference = preferenceScreen.findPreference(this.prefKey)) != null && (preference instanceof PreferenceScreen)) {
            initializeActionBar((PreferenceScreen) preference);
        }
    }

    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        super.onPreferenceTreeClick(preferenceScreen, preference);
        if (!(preference instanceof PreferenceScreen)) {
            return false;
        }
        initializeActionBar((PreferenceScreen) preference);
        return false;
    }

    public void initializeActionBar(PreferenceScreen preferenceScreen) {
        Dialog dialog = preferenceScreen.getDialog();
        if (dialog != null) {
            this.prefKey = preferenceScreen.getKey();
            initializeActionBar(preferenceScreen.getContext(), dialog);
        }
    }

    public static void initializeActionBar(Context ctx, final Dialog dialog) {
        if (dialog != null) {
            ActionBar mActionBar = dialog.getActionBar();
            mActionBar.setDisplayHomeAsUpEnabled(true);
            mActionBar.setDisplayShowHomeEnabled(false);
            View homeBtn = dialog.findViewById(16908332);
            if (homeBtn != null) {
                View.OnClickListener dismissDialogClickListener = new View.OnClickListener() {
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                };
                ViewParent homeBtnContainer = homeBtn.getParent();
                if (homeBtnContainer instanceof FrameLayout) {
                    ViewGroup containerParent = (ViewGroup) homeBtnContainer.getParent();
                    if (containerParent instanceof LinearLayout) {
                        ((LinearLayout) containerParent).setOnClickListener(dismissDialogClickListener);
                    } else {
                        ((FrameLayout) homeBtnContainer).setOnClickListener(dismissDialogClickListener);
                    }
                } else {
                    homeBtn.setOnClickListener(dismissDialogClickListener);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public ListPreference setupListPreference(String key, String value) {
        ListPreference prefView = (ListPreference) findPreference(key);
        prefView.setValue(value);
        prefView.setSummary(prefView.getEntry());
        prefView.setOnPreferenceChangeListener(new PreferenceChangeListener(prefView));
        return prefView;
    }

    /* access modifiers changed from: protected */
    public void initListPreference(ListPreference prefView, String value, CharSequence[] entries, CharSequence[] entryValues) {
        prefView.setEntries(entries);
        prefView.setEntryValues(entryValues);
        prefView.setValue(value);
        prefView.setSummary(prefView.getEntry());
        prefView.setOnPreferenceChangeListener(new PreferenceChangeListener(prefView));
    }

    private static class PreferenceChangeListener implements Preference.OnPreferenceChangeListener {
        private ListPreference mPrefView;

        private PreferenceChangeListener(ListPreference prefView) {
            this.mPrefView = prefView;
        }

        public boolean onPreferenceChange(Preference preference, Object newValue) {
            String summary = newValue.toString();
            this.mPrefView.setSummary(this.mPrefView.getEntries()[this.mPrefView.findIndexOfValue(summary)]);
            this.mPrefView.setValue(summary);
            return false;
        }
    }
}
