package com.fsck.k9.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.fsck.k9.Account;
import com.fsck.k9.BaseAccount;
import com.fsck.k9.FontSizes;
import com.fsck.k9.K9;
import com.fsck.k9.Preferences;
import com.fsck.k9.R;
import com.fsck.k9.search.SearchAccount;
import java.util.ArrayList;
import java.util.List;

public abstract class AccountList extends K9ListActivity implements AdapterView.OnItemClickListener {
    /* access modifiers changed from: private */
    public FontSizes mFontSizes = K9.getFontSizes();

    /* access modifiers changed from: protected */
    public abstract boolean displaySpecialAccounts();

    /* access modifiers changed from: protected */
    public abstract void onAccountSelected(BaseAccount baseAccount);

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setResult(0);
        setContentView((int) R.layout.account_list);
        ListView listView = getListView();
        listView.setOnItemClickListener(this);
        listView.setItemsCanFocus(false);
    }

    public void onResume() {
        super.onResume();
        new LoadAccounts().execute(new Void[0]);
    }

    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        onAccountSelected((BaseAccount) parent.getItemAtPosition(position));
    }

    public void populateListView(List<Account> realAccounts) {
        List<BaseAccount> accounts = new ArrayList<>();
        if (displaySpecialAccounts() && !K9.isHideSpecialAccounts()) {
            BaseAccount unifiedInboxAccount = SearchAccount.createUnifiedInboxAccount(this);
            BaseAccount allMessagesAccount = SearchAccount.createAllMessagesAccount(this);
            accounts.add(unifiedInboxAccount);
            accounts.add(allMessagesAccount);
        }
        accounts.addAll(realAccounts);
        AccountsAdapter adapter = new AccountsAdapter(accounts);
        ListView listView = getListView();
        listView.setAdapter((ListAdapter) adapter);
        listView.invalidate();
    }

    class AccountsAdapter extends ArrayAdapter<BaseAccount> {
        public AccountsAdapter(List<BaseAccount> accounts) {
            super(AccountList.this, 0, accounts);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;
            BaseAccount account = (BaseAccount) getItem(position);
            if (convertView != null) {
                view = convertView;
            } else {
                view = AccountList.this.getLayoutInflater().inflate((int) R.layout.accounts_item, parent, false);
                view.findViewById(R.id.active_icons).setVisibility(8);
                view.findViewById(R.id.folders).setVisibility(8);
            }
            AccountViewHolder holder = (AccountViewHolder) view.getTag();
            if (holder == null) {
                holder = new AccountViewHolder();
                holder.description = (TextView) view.findViewById(R.id.description);
                holder.email = (TextView) view.findViewById(R.id.email);
                holder.chip = view.findViewById(R.id.chip);
                view.setTag(holder);
            }
            String description = account.getDescription();
            if (account.getEmail().equals(description)) {
                holder.email.setVisibility(8);
            } else {
                holder.email.setVisibility(0);
                holder.email.setText(account.getEmail());
            }
            if (description == null || description.isEmpty()) {
                description = account.getEmail();
            }
            holder.description.setText(description);
            if (account instanceof Account) {
                holder.chip.setBackgroundColor(((Account) account).getChipColor());
            } else {
                holder.chip.setBackgroundColor(-6710887);
            }
            holder.chip.getBackground().setAlpha(255);
            AccountList.this.mFontSizes.setViewTextSize(holder.description, AccountList.this.mFontSizes.getAccountName());
            AccountList.this.mFontSizes.setViewTextSize(holder.email, AccountList.this.mFontSizes.getAccountDescription());
            return view;
        }

        class AccountViewHolder {
            public View chip;
            public TextView description;
            public TextView email;

            AccountViewHolder() {
            }
        }
    }

    class LoadAccounts extends AsyncTask<Void, Void, List<Account>> {
        LoadAccounts() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
            onPostExecute((List<Account>) ((List) obj));
        }

        /* access modifiers changed from: protected */
        public List<Account> doInBackground(Void... params) {
            return Preferences.getPreferences(AccountList.this.getApplicationContext()).getAccounts();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<Account> accounts) {
            AccountList.this.populateListView(accounts);
        }
    }
}
