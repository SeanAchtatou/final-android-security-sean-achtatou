package com.fsck.k9.activity.setup;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Parcelable;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.SeekBar;
import com.fsck.k9.R;

public class SliderPreference extends DialogPreference {
    protected static final int SEEKBAR_RESOLUTION = 10000;
    private static final String STATE_KEY_SEEK_BAR_VALUE = "seek_bar_value";
    private static final String STATE_KEY_SUPER = "super";
    protected int mSeekBarValue;
    protected CharSequence[] mSummaries;
    protected float mValue;

    public SliderPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        setup(context, attrs);
    }

    public SliderPreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setup(context, attrs);
    }

    private void setup(Context context, AttributeSet attrs) {
        setDialogLayoutResource(R.layout.slider_preference_dialog);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.SliderPreference);
        try {
            setSummary(a.getTextArray(0));
        } catch (Exception e) {
        }
        a.recycle();
    }

    /* access modifiers changed from: protected */
    public Object onGetDefaultValue(TypedArray a, int index) {
        return Float.valueOf(a.getFloat(index, 0.0f));
    }

    /* access modifiers changed from: protected */
    public void onSetInitialValue(boolean restoreValue, Object defaultValue) {
        setValue(restoreValue ? getPersistedFloat(this.mValue) : ((Float) defaultValue).floatValue());
    }

    public CharSequence getSummary() {
        if (this.mSummaries == null || this.mSummaries.length <= 0) {
            return super.getSummary();
        }
        return this.mSummaries[Math.min((int) (this.mValue * ((float) this.mSummaries.length)), this.mSummaries.length - 1)];
    }

    public void setSummary(CharSequence[] summaries) {
        this.mSummaries = summaries;
    }

    public void setSummary(CharSequence summary) {
        super.setSummary(summary);
        this.mSummaries = null;
    }

    public void setSummary(int summaryResId) {
        try {
            setSummary(getContext().getResources().getStringArray(summaryResId));
        } catch (Exception e) {
            super.setSummary(summaryResId);
        }
    }

    public float getValue() {
        return this.mValue;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public void setValue(float value) {
        float value2 = Math.max(0.0f, Math.min(value, 1.0f));
        if (shouldPersist()) {
            persistFloat(value2);
        }
        if (value2 != this.mValue) {
            this.mValue = value2;
            notifyChanged();
        }
    }

    /* access modifiers changed from: protected */
    public View onCreateDialogView() {
        this.mSeekBarValue = (int) (this.mValue * 10000.0f);
        View view = super.onCreateDialogView();
        SeekBar seekbar = (SeekBar) view.findViewById(R.id.slider_preference_seekbar);
        seekbar.setMax(SEEKBAR_RESOLUTION);
        seekbar.setProgress(this.mSeekBarValue);
        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    SliderPreference.this.mSeekBarValue = progress;
                    boolean unused = SliderPreference.this.callChangeListener(Float.valueOf(((float) SliderPreference.this.mSeekBarValue) / 10000.0f));
                }
            }
        });
        return view;
    }

    /* access modifiers changed from: protected */
    public void onDialogClosed(boolean positiveResult) {
        float newValue = ((float) this.mSeekBarValue) / 10000.0f;
        if (!positiveResult || !callChangeListener(Float.valueOf(newValue))) {
            callChangeListener(Float.valueOf(this.mValue));
        } else {
            setValue(newValue);
        }
        super.onDialogClosed(positiveResult);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        Bundle state = new Bundle();
        state.putParcelable(STATE_KEY_SUPER, superState);
        state.putInt(STATE_KEY_SEEK_BAR_VALUE, this.mSeekBarValue);
        return state;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable state) {
        Bundle bundle = (Bundle) state;
        super.onRestoreInstanceState(bundle.getParcelable(STATE_KEY_SUPER));
        this.mSeekBarValue = bundle.getInt(STATE_KEY_SEEK_BAR_VALUE);
        callChangeListener(Float.valueOf(((float) this.mSeekBarValue) / 10000.0f));
    }
}
