package com.fsck.k9.activity;

import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class FolderListFilter<T> extends Filter {
    private ArrayAdapter<T> mFolders;
    private List<T> mOriginalValues = null;

    public FolderListFilter(ArrayAdapter<T> folderNames) {
        this.mFolders = folderNames;
    }

    /* access modifiers changed from: protected */
    public Filter.FilterResults performFiltering(CharSequence searchTerm) {
        Filter.FilterResults results = new Filter.FilterResults();
        if (this.mOriginalValues == null) {
            int count = this.mFolders.getCount();
            this.mOriginalValues = new ArrayList(count);
            for (int i = 0; i < count; i++) {
                this.mOriginalValues.add(this.mFolders.getItem(i));
            }
        }
        Locale locale = Locale.getDefault();
        if (searchTerm == null || searchTerm.length() == 0) {
            List<T> list = new ArrayList<>(this.mOriginalValues);
            results.values = list;
            results.count = list.size();
        } else {
            String[] words = searchTerm.toString().toLowerCase(locale).split(" ");
            int wordCount = words.length;
            List<T> values = this.mOriginalValues;
            List<T> newValues = new ArrayList<>();
            for (T value : values) {
                String valueText = value.toString().toLowerCase(locale);
                int k = 0;
                while (true) {
                    if (k >= wordCount) {
                        break;
                    } else if (valueText.contains(words[k])) {
                        newValues.add(value);
                        break;
                    } else {
                        k++;
                    }
                }
            }
            results.values = newValues;
            results.count = newValues.size();
        }
        return results;
    }

    /* access modifiers changed from: protected */
    public void publishResults(CharSequence constraint, Filter.FilterResults results) {
        this.mFolders.setNotifyOnChange(false);
        try {
            List<T> folders = (List) results.values;
            this.mFolders.clear();
            if (folders != null) {
                for (T folder : folders) {
                    if (folder != null) {
                        this.mFolders.add(folder);
                    }
                }
            } else {
                Log.w("k9", "FolderListFilter.publishResults - null search-result ");
            }
            this.mFolders.notifyDataSetChanged();
        } finally {
            this.mFolders.setNotifyOnChange(true);
        }
    }

    public void invalidate() {
        this.mOriginalValues = null;
    }
}
