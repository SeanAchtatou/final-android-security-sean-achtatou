package com.fsck.k9.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.fsck.k9.Account;
import com.fsck.k9.Preferences;
import com.fsck.k9.R;
import com.fsck.k9.activity.setup.AccountSetupBasics;
import java.util.List;

public class SplashActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(1);
        super.onCreate(savedInstanceState);
        List<Account> accounts = Preferences.getPreferences(this).getAccounts();
        Intent intent = getIntent();
        if (accounts.size() < 1) {
            setContentView((int) R.layout.splash);
            new Thread() {
                public void run() {
                    int waited = 0;
                    while (waited < 2000) {
                        try {
                            sleep(100);
                            waited += 100;
                        } catch (InterruptedException e) {
                            SplashActivity.this.startActivity(new Intent(SplashActivity.this.getApplicationContext(), AccountSetupBasics.class));
                            SplashActivity.this.finish();
                            return;
                        } catch (Throwable th) {
                            SplashActivity.this.startActivity(new Intent(SplashActivity.this.getApplicationContext(), AccountSetupBasics.class));
                            SplashActivity.this.finish();
                            throw th;
                        }
                    }
                    SplashActivity.this.startActivity(new Intent(SplashActivity.this.getApplicationContext(), AccountSetupBasics.class));
                    SplashActivity.this.finish();
                }
            }.start();
            return;
        }
        startActivity(new Intent(getApplicationContext(), Accounts.class));
        finish();
    }
}
