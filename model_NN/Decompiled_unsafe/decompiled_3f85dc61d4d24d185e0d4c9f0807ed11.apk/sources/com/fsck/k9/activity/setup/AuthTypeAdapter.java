package com.fsck.k9.activity.setup;

import android.content.Context;
import android.widget.ArrayAdapter;
import com.fsck.k9.mail.AuthType;

class AuthTypeAdapter extends ArrayAdapter<AuthTypeHolder> {
    public AuthTypeAdapter(Context context, int resource, AuthTypeHolder[] holders) {
        super(context, resource, holders);
    }

    public static AuthTypeAdapter get(Context context) {
        AuthType[] authTypes = {AuthType.PLAIN, AuthType.CRAM_MD5, AuthType.XOAUTH2, AuthType.EXTERNAL};
        AuthTypeHolder[] holders = new AuthTypeHolder[authTypes.length];
        for (int i = 0; i < authTypes.length; i++) {
            holders[i] = new AuthTypeHolder(authTypes[i], context.getResources());
        }
        AuthTypeAdapter authTypesAdapter = new AuthTypeAdapter(context, 17367048, holders);
        authTypesAdapter.setDropDownViewResource(17367049);
        return authTypesAdapter;
    }

    public void useInsecureText(boolean insecure) {
        for (int i = 0; i < getCount(); i++) {
            ((AuthTypeHolder) getItem(i)).setInsecure(insecure);
        }
        notifyDataSetChanged();
    }

    public int getAuthPosition(AuthType authenticationType) {
        for (int i = 0; i < getCount(); i++) {
            if (((AuthTypeHolder) getItem(i)).authType == authenticationType) {
                return i;
            }
        }
        return -1;
    }
}
