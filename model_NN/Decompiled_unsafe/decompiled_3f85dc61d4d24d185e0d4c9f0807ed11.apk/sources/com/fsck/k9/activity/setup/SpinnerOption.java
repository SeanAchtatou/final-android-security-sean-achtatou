package com.fsck.k9.activity.setup;

import android.widget.Spinner;

public class SpinnerOption {
    public String label;
    public Object value;

    public static void setSpinnerOptionValue(Spinner spinner, Object value2) {
        int count = spinner.getCount();
        for (int i = 0; i < count; i++) {
            if (((SpinnerOption) spinner.getItemAtPosition(i)).value.equals(value2)) {
                spinner.setSelection(i, true);
                return;
            }
        }
    }

    public SpinnerOption(Object value2, String label2) {
        this.value = value2;
        this.label = label2;
    }

    public String toString() {
        return this.label;
    }
}
