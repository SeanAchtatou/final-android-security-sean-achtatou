package com.fsck.k9.activity;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.annotation.AttrRes;
import android.support.annotation.DrawableRes;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.QuickContactBadge;
import android.widget.TextView;
import com.fsck.k9.R;
import com.fsck.k9.activity.compose.RecipientAdapter;
import com.fsck.k9.view.RecipientSelectView;
import com.fsck.k9.view.ThemeUtils;
import java.util.List;

public class AlternateRecipientAdapter extends BaseAdapter {
    private static final int NUMBER_OF_FIXED_LIST_ITEMS = 2;
    private static final int POSITION_CURRENT_ADDRESS = 1;
    private static final int POSITION_HEADER_VIEW = 0;
    private final Context context;
    /* access modifiers changed from: private */
    public RecipientSelectView.Recipient currentRecipient;
    /* access modifiers changed from: private */
    public final AlternateRecipientListener listener;
    private List<RecipientSelectView.Recipient> recipients;

    public interface AlternateRecipientListener {
        void onRecipientChange(RecipientSelectView.Recipient recipient, RecipientSelectView.Recipient recipient2);

        void onRecipientRemove(RecipientSelectView.Recipient recipient);
    }

    public AlternateRecipientAdapter(Context context2, AlternateRecipientListener listener2) {
        this.context = context2;
        this.listener = listener2;
    }

    public void setCurrentRecipient(RecipientSelectView.Recipient currentRecipient2) {
        this.currentRecipient = currentRecipient2;
    }

    public void setAlternateRecipientInfo(List<RecipientSelectView.Recipient> recipients2) {
        this.recipients = recipients2;
        int indexOfCurrentRecipient = recipients2.indexOf(this.currentRecipient);
        if (indexOfCurrentRecipient >= 0) {
            this.currentRecipient = recipients2.get(indexOfCurrentRecipient);
        }
        recipients2.remove(this.currentRecipient);
        notifyDataSetChanged();
    }

    public int getCount() {
        if (this.recipients == null) {
            return 2;
        }
        return this.recipients.size() + 2;
    }

    public RecipientSelectView.Recipient getItem(int position) {
        if (position == 0 || position == 1) {
            return this.currentRecipient;
        }
        if (this.recipients == null) {
            return null;
        }
        return getRecipientFromPosition(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    private RecipientSelectView.Recipient getRecipientFromPosition(int position) {
        return this.recipients.get(position - 2);
    }

    public View getView(int position, View view, ViewGroup parent) {
        if (view == null) {
            view = newView(parent);
        }
        RecipientSelectView.Recipient recipient = getItem(position);
        if (position == 0) {
            bindHeaderView(view, recipient);
        } else {
            bindItemView(view, recipient);
        }
        return view;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View newView(ViewGroup parent) {
        View view = LayoutInflater.from(this.context).inflate((int) R.layout.recipient_alternate_item, parent, false);
        view.setTag(new RecipientTokenHolder(view));
        return view;
    }

    public boolean isEnabled(int position) {
        return position != 0;
    }

    public void bindHeaderView(View view, RecipientSelectView.Recipient recipient) {
        RecipientTokenHolder holder = (RecipientTokenHolder) view.getTag();
        holder.setShowAsHeader(true);
        holder.headerName.setText(recipient.getNameOrUnknown(this.context));
        if (!TextUtils.isEmpty(recipient.addressLabel)) {
            holder.headerAddressLabel.setText(recipient.addressLabel);
            holder.headerAddressLabel.setVisibility(0);
        } else {
            holder.headerAddressLabel.setVisibility(8);
        }
        RecipientAdapter.setContactPhotoOrPlaceholder(this.context, holder.headerPhoto, recipient);
        holder.headerPhoto.assignContactUri(recipient.getContactLookupUri());
        holder.headerRemove.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AlternateRecipientAdapter.this.listener.onRecipientRemove(AlternateRecipientAdapter.this.currentRecipient);
            }
        });
    }

    public void bindItemView(View view, final RecipientSelectView.Recipient recipient) {
        boolean isCurrent;
        int i;
        int i2 = 1;
        RecipientTokenHolder holder = (RecipientTokenHolder) view.getTag();
        holder.setShowAsHeader(false);
        holder.itemAddress.setText(recipient.address.getAddress());
        if (!TextUtils.isEmpty(recipient.addressLabel)) {
            holder.itemAddressLabel.setText(recipient.addressLabel);
            holder.itemAddressLabel.setVisibility(0);
        } else {
            holder.itemAddressLabel.setVisibility(8);
        }
        if (this.currentRecipient == recipient) {
            isCurrent = true;
        } else {
            isCurrent = false;
        }
        TextView textView = holder.itemAddress;
        if (isCurrent) {
            i = 1;
        } else {
            i = 0;
        }
        textView.setTypeface(null, i);
        TextView textView2 = holder.itemAddressLabel;
        if (!isCurrent) {
            i2 = 0;
        }
        textView2.setTypeface(null, i2);
        holder.layoutItem.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AlternateRecipientAdapter.this.listener.onRecipientChange(AlternateRecipientAdapter.this.currentRecipient, recipient);
            }
        });
        configureCryptoStatusView(holder, recipient);
    }

    private void configureCryptoStatusView(RecipientTokenHolder holder, RecipientSelectView.Recipient recipient) {
        switch (recipient.getCryptoStatus()) {
            case AVAILABLE_TRUSTED:
                setCryptoStatusView(holder, R.drawable.status_lock_dots_3, R.attr.openpgp_green);
                return;
            case AVAILABLE_UNTRUSTED:
                setCryptoStatusView(holder, R.drawable.status_lock_dots_2, R.attr.openpgp_orange);
                return;
            case UNAVAILABLE:
                setCryptoStatusView(holder, R.drawable.status_lock_disabled_dots_1, R.attr.openpgp_red);
                return;
            case UNDEFINED:
                holder.itemCryptoStatus.setVisibility(8);
                return;
            default:
                return;
        }
    }

    private void setCryptoStatusView(RecipientTokenHolder holder, @DrawableRes int cryptoStatusRes, @AttrRes int cryptoStatusColorAttr) {
        Drawable drawable = this.context.getResources().getDrawable(cryptoStatusRes);
        drawable.mutate();
        drawable.setColorFilter(ThemeUtils.getStyledColor(this.context, cryptoStatusColorAttr), PorterDuff.Mode.SRC_ATOP);
        holder.itemCryptoStatus.setImageDrawable(drawable);
        holder.itemCryptoStatus.setVisibility(0);
    }

    private static class RecipientTokenHolder {
        public final TextView headerAddressLabel;
        public final TextView headerName;
        public final QuickContactBadge headerPhoto;
        public final View headerRemove;
        public final TextView itemAddress;
        public final TextView itemAddressLabel;
        public final ImageView itemCryptoStatus;
        public final View layoutHeader;
        public final View layoutItem;

        public RecipientTokenHolder(View view) {
            this.layoutHeader = view.findViewById(R.id.alternate_container_header);
            this.layoutItem = view.findViewById(R.id.alternate_container_item);
            this.headerName = (TextView) view.findViewById(R.id.alternate_header_name);
            this.headerAddressLabel = (TextView) view.findViewById(R.id.alternate_header_label);
            this.headerPhoto = (QuickContactBadge) view.findViewById(R.id.alternate_contact_photo);
            this.headerRemove = view.findViewById(R.id.alternate_remove);
            this.itemAddress = (TextView) view.findViewById(R.id.alternate_address);
            this.itemAddressLabel = (TextView) view.findViewById(R.id.alternate_address_label);
            this.itemCryptoStatus = (ImageView) view.findViewById(R.id.alternate_crypto_status);
        }

        public void setShowAsHeader(boolean isHeader) {
            int i;
            int i2 = 8;
            View view = this.layoutHeader;
            if (isHeader) {
                i = 0;
            } else {
                i = 8;
            }
            view.setVisibility(i);
            View view2 = this.layoutItem;
            if (!isHeader) {
                i2 = 0;
            }
            view2.setVisibility(i2);
        }
    }
}
