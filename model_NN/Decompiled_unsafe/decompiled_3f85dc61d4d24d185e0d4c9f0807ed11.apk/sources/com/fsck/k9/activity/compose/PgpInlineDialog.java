package com.fsck.k9.activity.compose;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fsck.k9.R;
import com.fsck.k9.view.HighlightDialogFragment;

public class PgpInlineDialog extends HighlightDialogFragment {
    public static final String ARG_FIRST_TIME = "first_time";

    public interface OnOpenPgpInlineChangeListener {
        void onOpenPgpInlineChange(boolean z);
    }

    public static PgpInlineDialog newInstance(boolean firstTime, @IdRes int showcaseView) {
        PgpInlineDialog dialog = new PgpInlineDialog();
        Bundle args = new Bundle();
        args.putInt(ARG_FIRST_TIME, firstTime ? 1 : 0);
        args.putInt(HighlightDialogFragment.ARG_HIGHLIGHT_VIEW, showcaseView);
        dialog.setArguments(args);
        return dialog;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Activity activity = getActivity();
        View view = LayoutInflater.from(activity).inflate((int) R.layout.openpgp_inline_dialog, (ViewGroup) null);
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setView(view);
        if (getArguments().getInt(ARG_FIRST_TIME) != 0) {
            builder.setPositiveButton((int) R.string.openpgp_inline_ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        } else {
            builder.setPositiveButton((int) R.string.openpgp_inline_disable, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Activity activity = PgpInlineDialog.this.getActivity();
                    if (activity != null) {
                        ((OnOpenPgpInlineChangeListener) activity).onOpenPgpInlineChange(false);
                        dialog.dismiss();
                    }
                }
            });
            builder.setNegativeButton((int) R.string.openpgp_inline_keep_enabled, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        }
        return builder.create();
    }
}
