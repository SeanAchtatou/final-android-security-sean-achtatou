package com.fsck.k9.activity;

import com.fsck.k9.mailstore.LocalMessage;
import java.util.Date;

public class MessageInfoHolder {
    public String account;
    public boolean answered;
    public Date compareArrival;
    public String compareCounterparty;
    public Date compareDate;
    public String compareSubject;
    public String date;
    public boolean dirty;
    public boolean flagged;
    public FolderInfoHolder folder;
    public boolean forwarded;
    public LocalMessage message;
    public boolean read;
    public String[] recipients;
    public boolean selected = false;
    public CharSequence sender;
    public String senderAddress;
    public String uid;
    public String uri;

    public boolean equals(Object o) {
        if (!(o instanceof MessageInfoHolder)) {
            return false;
        }
        return this.message.equals(((MessageInfoHolder) o).message);
    }

    public int hashCode() {
        return this.uid.hashCode();
    }
}
