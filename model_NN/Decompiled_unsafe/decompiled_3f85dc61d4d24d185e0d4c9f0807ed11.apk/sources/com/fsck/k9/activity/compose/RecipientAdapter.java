package com.fsck.k9.activity.compose;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.fsck.k9.R;
import com.fsck.k9.helper.ContactPicture;
import com.fsck.k9.view.RecipientSelectView;
import com.fsck.k9.view.ThemeUtils;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RecipientAdapter extends BaseAdapter implements Filterable {
    private final Context context;
    private String highlight;
    /* access modifiers changed from: private */
    public List<RecipientSelectView.Recipient> recipients;

    public RecipientAdapter(Context context2) {
        this.context = context2;
    }

    public void setRecipients(List<RecipientSelectView.Recipient> recipients2) {
        this.recipients = recipients2;
        notifyDataSetChanged();
    }

    public void setHighlight(String highlight2) {
        this.highlight = highlight2;
    }

    public int getCount() {
        if (this.recipients == null) {
            return 0;
        }
        return this.recipients.size();
    }

    public RecipientSelectView.Recipient getItem(int position) {
        if (this.recipients == null) {
            return null;
        }
        return this.recipients.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View view, ViewGroup parent) {
        if (view == null) {
            view = newView(parent);
        }
        bindView(view, getItem(position));
        return view;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View newView(ViewGroup parent) {
        View view = LayoutInflater.from(this.context).inflate((int) R.layout.recipient_dropdown_item, parent, false);
        view.setTag(new RecipientTokenHolder(view));
        return view;
    }

    public void bindView(View view, RecipientSelectView.Recipient recipient) {
        RecipientTokenHolder holder = (RecipientTokenHolder) view.getTag();
        holder.name.setText(highlightText(recipient.getDisplayNameOrUnknown(this.context)));
        holder.email.setText(highlightText(recipient.address.getAddress()));
        setContactPhotoOrPlaceholder(this.context, holder.photo, recipient);
        Integer cryptoStatusRes = null;
        Integer cryptoStatusColor = null;
        switch (recipient.getCryptoStatus()) {
            case AVAILABLE_TRUSTED:
                cryptoStatusRes = Integer.valueOf((int) R.drawable.status_lock_dots_3);
                cryptoStatusColor = Integer.valueOf(ThemeUtils.getStyledColor(this.context, (int) R.attr.openpgp_green));
                break;
            case AVAILABLE_UNTRUSTED:
                cryptoStatusRes = Integer.valueOf((int) R.drawable.status_lock_dots_2);
                cryptoStatusColor = Integer.valueOf(ThemeUtils.getStyledColor(this.context, (int) R.attr.openpgp_orange));
                break;
            case UNAVAILABLE:
                cryptoStatusRes = Integer.valueOf((int) R.drawable.status_lock_disabled_dots_1);
                cryptoStatusColor = Integer.valueOf(ThemeUtils.getStyledColor(this.context, (int) R.attr.openpgp_red));
                break;
        }
        if (cryptoStatusRes != null) {
            Drawable drawable = this.context.getResources().getDrawable(cryptoStatusRes.intValue());
            drawable.mutate();
            drawable.setColorFilter(cryptoStatusColor.intValue(), PorterDuff.Mode.SRC_ATOP);
            holder.cryptoStatus.setImageDrawable(drawable);
            holder.cryptoStatus.setVisibility(0);
            return;
        }
        holder.cryptoStatus.setVisibility(8);
    }

    public static void setContactPhotoOrPlaceholder(Context context2, ImageView imageView, RecipientSelectView.Recipient recipient) {
        if (recipient.photoThumbnailUri != null) {
            Glide.with(context2).load(recipient.photoThumbnailUri).placeholder((Drawable) null).dontAnimate().into(imageView);
        } else {
            ContactPicture.getContactPictureLoader(context2).loadContactPicture(recipient.address, imageView);
        }
    }

    public Filter getFilter() {
        return new Filter() {
            /* access modifiers changed from: protected */
            public Filter.FilterResults performFiltering(CharSequence constraint) {
                if (RecipientAdapter.this.recipients == null) {
                    return null;
                }
                Filter.FilterResults result = new Filter.FilterResults();
                result.values = RecipientAdapter.this.recipients;
                result.count = RecipientAdapter.this.recipients.size();
                return result;
            }

            /* access modifiers changed from: protected */
            public void publishResults(CharSequence constraint, Filter.FilterResults results) {
                RecipientAdapter.this.notifyDataSetChanged();
            }
        };
    }

    private static class RecipientTokenHolder {
        public final ImageView cryptoStatus;
        public final TextView email;
        public final TextView name;
        public final ImageView photo;

        public RecipientTokenHolder(View view) {
            this.name = (TextView) view.findViewById(R.id.text1);
            this.email = (TextView) view.findViewById(2131755396);
            this.photo = (ImageView) view.findViewById(R.id.contact_photo);
            this.cryptoStatus = (ImageView) view.findViewById(R.id.contact_crypto_status);
        }
    }

    public Spannable highlightText(String text) {
        Spannable highlightedSpannable = Spannable.Factory.getInstance().newSpannable(text);
        if (this.highlight != null) {
            Matcher matcher = Pattern.compile(this.highlight, 18).matcher(text);
            while (matcher.find()) {
                highlightedSpannable.setSpan(new ForegroundColorSpan(this.context.getResources().getColor(17170450)), matcher.start(), matcher.end(), 33);
            }
        }
        return highlightedSpannable;
    }
}
