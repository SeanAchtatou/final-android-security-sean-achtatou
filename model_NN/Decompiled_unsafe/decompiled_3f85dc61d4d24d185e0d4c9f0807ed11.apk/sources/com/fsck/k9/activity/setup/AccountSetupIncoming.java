package com.fsck.k9.activity.setup;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import atomicgonza.GmailUtils;
import com.fsck.k9.Account;
import com.fsck.k9.Globals;
import com.fsck.k9.Preferences;
import com.fsck.k9.R;
import com.fsck.k9.account.AccountCreator;
import com.fsck.k9.activity.K9Activity;
import com.fsck.k9.activity.setup.AccountSetupCheckSettings;
import com.fsck.k9.helper.Utility;
import com.fsck.k9.mail.AuthType;
import com.fsck.k9.mail.ConnectionSecurity;
import com.fsck.k9.mail.NetworkType;
import com.fsck.k9.mail.ServerSettings;
import com.fsck.k9.mail.Transport;
import com.fsck.k9.mail.oauth.AuthorizationException;
import com.fsck.k9.mail.oauth.OAuth2TokenProvider;
import com.fsck.k9.mail.store.RemoteStore;
import com.fsck.k9.mail.store.imap.ImapStoreSettings;
import com.fsck.k9.mail.store.webdav.WebDavStoreSettings;
import com.fsck.k9.service.MailService;
import com.fsck.k9.view.ClientCertificateSpinner;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

public class AccountSetupIncoming extends K9Activity implements View.OnClickListener {
    private static final String EXTRA_ACCOUNT = "account";
    private static final String EXTRA_MAKE_DEFAULT = "makeDefault";
    private static final String GMAIL_AUTH_TOKEN_TYPE = "oauth2:https://mail.google.com/";
    private static final String STATE_AUTH_TYPE_POSITION = "authTypePosition";
    private static final String STATE_SECURITY_TYPE_POSITION = "stateSecurityTypePosition";
    ClientCertificateSpinner.OnClientCertificateChangedListener clientCertificateChangedListener = new ClientCertificateSpinner.OnClientCertificateChangedListener() {
        public void onClientCertificateChanged(String alias) {
            AccountSetupIncoming.this.validateFields();
        }
    };
    /* access modifiers changed from: private */
    public boolean isNewAccount = false;
    /* access modifiers changed from: private */
    public Account mAccount;
    private AuthTypeAdapter mAuthTypeAdapter;
    private Spinner mAuthTypeView;
    private TextView mClientCertificateLabelView;
    /* access modifiers changed from: private */
    public ClientCertificateSpinner mClientCertificateSpinner;
    private CheckBox mCompressionMobile;
    private CheckBox mCompressionOther;
    private CheckBox mCompressionWifi;
    private ConnectionSecurity[] mConnectionSecurityChoices = ConnectionSecurity.values();
    /* access modifiers changed from: private */
    public int mCurrentAuthTypeViewPosition;
    private String mCurrentPortViewSetting;
    /* access modifiers changed from: private */
    public int mCurrentSecurityTypeViewPosition;
    private CheckBox mImapAutoDetectNamespaceView;
    /* access modifiers changed from: private */
    public EditText mImapPathPrefixView;
    private boolean mMakeDefault;
    private Button mNextButton;
    private TextView mPasswordLabelView;
    /* access modifiers changed from: private */
    public EditText mPasswordView;
    private EditText mPortView;
    private Spinner mSecurityTypeView;
    private EditText mServerView;
    private ServerSettings.Type mStoreType;
    private CheckBox mSubscribedFoldersOnly;
    private EditText mUsernameView;
    private EditText mWebdavAuthPathView;
    private EditText mWebdavMailboxPathView;
    private EditText mWebdavPathPrefixView;
    TextWatcher validationTextWatcher = new TextWatcher() {
        public void afterTextChanged(Editable s) {
            AccountSetupIncoming.this.validateFields();
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    };

    public static void actionIncomingSettings(Activity context, Account account, boolean makeDefault, boolean isNewAccount2) {
        Intent i = new Intent(context, AccountSetupIncoming.class);
        i.putExtra("account", account.getUuid());
        i.putExtra(EXTRA_MAKE_DEFAULT, makeDefault);
        i.putExtra(AccountSetupCheckSettings.EXTRA_NEW_ACCOUNT, isNewAccount2);
        context.startActivity(i);
    }

    public static void actionEditIncomingSettings(Activity context, Account account) {
        context.startActivity(intentActionEditIncomingSettings(context, account));
    }

    public static Intent intentActionEditIncomingSettings(Context context, Account account) {
        Intent i = new Intent(context, AccountSetupIncoming.class);
        i.setAction("android.intent.action.EDIT");
        i.putExtra("account", account.getUuid());
        return i;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.account_setup_incoming);
        if (savedInstanceState != null) {
            this.isNewAccount = savedInstanceState.getBoolean(AccountSetupCheckSettings.EXTRA_NEW_ACCOUNT, false);
        } else {
            Intent i = getIntent();
            if (i != null) {
                this.isNewAccount = i.getBooleanExtra(AccountSetupCheckSettings.EXTRA_NEW_ACCOUNT, false);
            }
        }
        this.mUsernameView = (EditText) findViewById(R.id.account_username);
        this.mPasswordView = (EditText) findViewById(R.id.account_password);
        this.mClientCertificateSpinner = (ClientCertificateSpinner) findViewById(R.id.account_client_certificate_spinner);
        this.mClientCertificateLabelView = (TextView) findViewById(R.id.account_client_certificate_label);
        this.mPasswordLabelView = (TextView) findViewById(R.id.account_password_label);
        TextView serverLabelView = (TextView) findViewById(R.id.account_server_label);
        this.mServerView = (EditText) findViewById(R.id.account_server);
        this.mPortView = (EditText) findViewById(R.id.account_port);
        this.mSecurityTypeView = (Spinner) findViewById(R.id.account_security_type);
        this.mAuthTypeView = (Spinner) findViewById(R.id.account_auth_type);
        this.mImapAutoDetectNamespaceView = (CheckBox) findViewById(R.id.imap_autodetect_namespace);
        this.mImapPathPrefixView = (EditText) findViewById(R.id.imap_path_prefix);
        this.mWebdavPathPrefixView = (EditText) findViewById(R.id.webdav_path_prefix);
        this.mWebdavAuthPathView = (EditText) findViewById(R.id.webdav_auth_path);
        this.mWebdavMailboxPathView = (EditText) findViewById(R.id.webdav_mailbox_path);
        this.mNextButton = (Button) findViewById(R.id.next);
        this.mCompressionMobile = (CheckBox) findViewById(R.id.compression_mobile);
        this.mCompressionWifi = (CheckBox) findViewById(R.id.compression_wifi);
        this.mCompressionOther = (CheckBox) findViewById(R.id.compression_other);
        this.mSubscribedFoldersOnly = (CheckBox) findViewById(R.id.subscribed_folders_only);
        this.mNextButton.setOnClickListener(this);
        this.mImapAutoDetectNamespaceView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                AccountSetupIncoming.this.mImapPathPrefixView.setEnabled(!isChecked);
                if (isChecked && AccountSetupIncoming.this.mImapPathPrefixView.hasFocus()) {
                    AccountSetupIncoming.this.mImapPathPrefixView.focusSearch(33).requestFocus();
                } else if (!isChecked) {
                    AccountSetupIncoming.this.mImapPathPrefixView.requestFocus();
                }
            }
        });
        this.mAuthTypeAdapter = AuthTypeAdapter.get(this);
        this.mAuthTypeView.setAdapter((SpinnerAdapter) this.mAuthTypeAdapter);
        this.mPortView.setKeyListener(DigitsKeyListener.getInstance("0123456789"));
        this.mAccount = Preferences.getPreferences(this).getAccount(getIntent().getStringExtra("account"));
        this.mMakeDefault = getIntent().getBooleanExtra(EXTRA_MAKE_DEFAULT, false);
        if (savedInstanceState != null && savedInstanceState.containsKey("account")) {
            this.mAccount = Preferences.getPreferences(this).getAccount(savedInstanceState.getString("account"));
        }
        boolean editSettings = "android.intent.action.EDIT".equals(getIntent().getAction());
        try {
            Log.i("k9", "Setting up based on settings: " + this.mAccount.getStoreUri());
            ServerSettings settings = RemoteStore.decodeStoreUri(this.mAccount.getStoreUri());
            if (savedInstanceState == null) {
                this.mCurrentAuthTypeViewPosition = this.mAuthTypeAdapter.getAuthPosition(settings.authenticationType);
            } else {
                this.mCurrentAuthTypeViewPosition = savedInstanceState.getInt(STATE_AUTH_TYPE_POSITION);
            }
            this.mAuthTypeView.setSelection(this.mCurrentAuthTypeViewPosition, false);
            updateViewFromAuthType();
            if (settings.username != null) {
                this.mUsernameView.setText(settings.username);
            }
            if (settings.password != null) {
                this.mPasswordView.setText(settings.password);
            }
            if (settings.clientCertificateAlias != null) {
                this.mClientCertificateSpinner.setAlias(settings.clientCertificateAlias);
            }
            this.mStoreType = settings.type;
            if (ServerSettings.Type.POP3 == settings.type) {
                serverLabelView.setText((int) R.string.account_setup_incoming_pop_server_label);
                findViewById(R.id.imap_path_prefix_section).setVisibility(8);
                findViewById(R.id.webdav_advanced_header).setVisibility(8);
                findViewById(R.id.webdav_mailbox_alias_section).setVisibility(8);
                findViewById(R.id.webdav_owa_path_section).setVisibility(8);
                findViewById(R.id.webdav_auth_path_section).setVisibility(8);
                findViewById(R.id.compression_section).setVisibility(8);
                findViewById(R.id.compression_label).setVisibility(8);
                this.mSubscribedFoldersOnly.setVisibility(8);
            } else if (ServerSettings.Type.IMAP == settings.type) {
                serverLabelView.setText((int) R.string.account_setup_incoming_imap_server_label);
                ImapStoreSettings imapSettings = (ImapStoreSettings) settings;
                this.mImapAutoDetectNamespaceView.setChecked(imapSettings.autoDetectNamespace);
                if (imapSettings.pathPrefix != null) {
                    this.mImapPathPrefixView.setText(imapSettings.pathPrefix);
                }
                findViewById(R.id.webdav_advanced_header).setVisibility(8);
                findViewById(R.id.webdav_mailbox_alias_section).setVisibility(8);
                findViewById(R.id.webdav_owa_path_section).setVisibility(8);
                findViewById(R.id.webdav_auth_path_section).setVisibility(8);
                if (!editSettings) {
                    findViewById(R.id.imap_folder_setup_section).setVisibility(8);
                }
            } else if (ServerSettings.Type.WebDAV == settings.type) {
                serverLabelView.setText((int) R.string.account_setup_incoming_webdav_server_label);
                this.mConnectionSecurityChoices = new ConnectionSecurity[]{ConnectionSecurity.NONE, ConnectionSecurity.SSL_TLS_REQUIRED};
                findViewById(R.id.imap_path_prefix_section).setVisibility(8);
                findViewById(R.id.account_auth_type_label).setVisibility(8);
                findViewById(R.id.account_auth_type).setVisibility(8);
                findViewById(R.id.compression_section).setVisibility(8);
                findViewById(R.id.compression_label).setVisibility(8);
                this.mSubscribedFoldersOnly.setVisibility(8);
                WebDavStoreSettings webDavSettings = (WebDavStoreSettings) settings;
                if (webDavSettings.path != null) {
                    this.mWebdavPathPrefixView.setText(webDavSettings.path);
                }
                if (webDavSettings.authPath != null) {
                    this.mWebdavAuthPathView.setText(webDavSettings.authPath);
                }
                if (webDavSettings.mailboxPath != null) {
                    this.mWebdavMailboxPathView.setText(webDavSettings.mailboxPath);
                }
            } else {
                throw new Exception("Unknown account type: " + this.mAccount.getStoreUri());
            }
            if (!editSettings) {
                this.mAccount.setDeletePolicy(AccountCreator.getDefaultDeletePolicy(settings.type));
            }
            ConnectionSecurityAdapter securityTypesAdapter = ConnectionSecurityAdapter.get(this, this.mConnectionSecurityChoices);
            this.mSecurityTypeView.setAdapter((SpinnerAdapter) securityTypesAdapter);
            if (savedInstanceState == null) {
                this.mCurrentSecurityTypeViewPosition = securityTypesAdapter.getConnectionSecurityPosition(settings.connectionSecurity);
            } else {
                this.mCurrentSecurityTypeViewPosition = savedInstanceState.getInt(STATE_SECURITY_TYPE_POSITION);
            }
            this.mSecurityTypeView.setSelection(this.mCurrentSecurityTypeViewPosition, false);
            updateAuthPlainTextFromSecurityType(settings.connectionSecurity);
            this.mCompressionMobile.setChecked(this.mAccount.useCompression(NetworkType.MOBILE));
            this.mCompressionWifi.setChecked(this.mAccount.useCompression(NetworkType.WIFI));
            this.mCompressionOther.setChecked(this.mAccount.useCompression(NetworkType.OTHER));
            if (settings.host != null) {
                this.mServerView.setText(settings.host);
            }
            if (settings.port != -1) {
                this.mPortView.setText(String.format("%d", Integer.valueOf(settings.port)));
            } else {
                updatePortFromSecurityType();
            }
            this.mCurrentPortViewSetting = this.mPortView.getText().toString();
            this.mSubscribedFoldersOnly.setChecked(this.mAccount.subscribedFoldersOnly());
        } catch (Exception e) {
            failure(e);
        }
    }

    private void initializeViewListeners() {
        this.mSecurityTypeView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (AccountSetupIncoming.this.mCurrentSecurityTypeViewPosition != position) {
                    AccountSetupIncoming.this.updatePortFromSecurityType();
                    AccountSetupIncoming.this.validateFields();
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.mAuthTypeView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (AccountSetupIncoming.this.mCurrentAuthTypeViewPosition != position) {
                    AccountSetupIncoming.this.updateViewFromAuthType();
                    AccountSetupIncoming.this.validateFields();
                    if (AuthType.EXTERNAL == AccountSetupIncoming.this.getSelectedAuthType()) {
                        AccountSetupIncoming.this.mClientCertificateSpinner.chooseCertificate();
                    } else {
                        AccountSetupIncoming.this.mPasswordView.requestFocus();
                    }
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.mClientCertificateSpinner.setOnClientCertificateChangedListener(this.clientCertificateChangedListener);
        this.mUsernameView.addTextChangedListener(this.validationTextWatcher);
        this.mPasswordView.addTextChangedListener(this.validationTextWatcher);
        this.mServerView.addTextChangedListener(this.validationTextWatcher);
        this.mPortView.addTextChangedListener(this.validationTextWatcher);
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("account", this.mAccount.getUuid());
        outState.putBoolean(AccountSetupCheckSettings.EXTRA_NEW_ACCOUNT, this.isNewAccount);
        outState.putInt(STATE_SECURITY_TYPE_POSITION, this.mCurrentSecurityTypeViewPosition);
        outState.putInt(STATE_AUTH_TYPE_POSITION, this.mCurrentAuthTypeViewPosition);
    }

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        initializeViewListeners();
        validateFields();
    }

    /* access modifiers changed from: private */
    public void updateViewFromAuthType() {
        boolean isAuthTypeExternal;
        boolean isAuthTypeXOAuth2;
        AuthType authType = getSelectedAuthType();
        if (AuthType.EXTERNAL == authType) {
            isAuthTypeExternal = true;
        } else {
            isAuthTypeExternal = false;
        }
        if (AuthType.XOAUTH2 == authType) {
            isAuthTypeXOAuth2 = true;
        } else {
            isAuthTypeXOAuth2 = false;
        }
        if (isAuthTypeExternal) {
            this.mPasswordView.setVisibility(8);
            this.mPasswordLabelView.setVisibility(8);
            this.mClientCertificateLabelView.setVisibility(0);
            this.mClientCertificateSpinner.setVisibility(0);
        } else if (isAuthTypeXOAuth2) {
            this.mPasswordView.setVisibility(8);
            this.mPasswordLabelView.setVisibility(8);
            this.mClientCertificateLabelView.setVisibility(8);
            this.mClientCertificateSpinner.setVisibility(8);
        } else {
            this.mPasswordView.setVisibility(0);
            this.mPasswordLabelView.setVisibility(0);
            this.mClientCertificateLabelView.setVisibility(8);
            this.mClientCertificateSpinner.setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public void validateFields() {
        AuthType authType = getSelectedAuthType();
        boolean isAuthTypeExternal = AuthType.EXTERNAL == authType;
        boolean isAuthTypeXOAuth2 = AuthType.XOAUTH2 == authType;
        boolean hasConnectionSecurity = getSelectedSecurity() != ConnectionSecurity.NONE;
        if (!isAuthTypeExternal || hasConnectionSecurity) {
            this.mCurrentAuthTypeViewPosition = this.mAuthTypeView.getSelectedItemPosition();
            this.mCurrentSecurityTypeViewPosition = this.mSecurityTypeView.getSelectedItemPosition();
            this.mCurrentPortViewSetting = this.mPortView.getText().toString();
        } else {
            Toast.makeText(this, getString(R.string.account_setup_incoming_invalid_setting_combo_notice, new Object[]{getString(R.string.account_setup_incoming_auth_type_label), AuthType.EXTERNAL.toString(), getString(R.string.account_setup_incoming_security_label), ConnectionSecurity.NONE.toString()}), 1).show();
            AdapterView.OnItemSelectedListener onItemSelectedListener = this.mAuthTypeView.getOnItemSelectedListener();
            this.mAuthTypeView.setOnItemSelectedListener(null);
            this.mAuthTypeView.setSelection(this.mCurrentAuthTypeViewPosition, false);
            this.mAuthTypeView.setOnItemSelectedListener(onItemSelectedListener);
            updateViewFromAuthType();
            AdapterView.OnItemSelectedListener onItemSelectedListener2 = this.mSecurityTypeView.getOnItemSelectedListener();
            this.mSecurityTypeView.setOnItemSelectedListener(null);
            this.mSecurityTypeView.setSelection(this.mCurrentSecurityTypeViewPosition, false);
            this.mSecurityTypeView.setOnItemSelectedListener(onItemSelectedListener2);
            updateAuthPlainTextFromSecurityType(getSelectedSecurity());
            this.mPortView.removeTextChangedListener(this.validationTextWatcher);
            this.mPortView.setText(this.mCurrentPortViewSetting);
            this.mPortView.addTextChangedListener(this.validationTextWatcher);
            isAuthTypeExternal = AuthType.EXTERNAL == getSelectedAuthType();
            hasConnectionSecurity = getSelectedSecurity() != ConnectionSecurity.NONE;
        }
        boolean hasValidCertificateAlias = this.mClientCertificateSpinner.getAlias() != null;
        boolean hasValidUserName = Utility.requiredFieldValid(this.mUsernameView);
        this.mNextButton.setEnabled(Utility.domainFieldValid(this.mServerView) && Utility.requiredFieldValid(this.mPortView) && ((hasValidUserName && !isAuthTypeExternal && !isAuthTypeXOAuth2 && Utility.requiredFieldValid(this.mPasswordView)) || (hasValidUserName && isAuthTypeExternal && hasConnectionSecurity && hasValidCertificateAlias) || (hasValidUserName && isAuthTypeXOAuth2)));
        Utility.setCompoundDrawablesAlpha(this.mNextButton, this.mNextButton.isEnabled() ? 255 : 128);
    }

    /* access modifiers changed from: private */
    public void updatePortFromSecurityType() {
        ConnectionSecurity securityType = getSelectedSecurity();
        updateAuthPlainTextFromSecurityType(securityType);
        this.mPortView.removeTextChangedListener(this.validationTextWatcher);
        this.mPortView.setText(String.valueOf(AccountCreator.getDefaultPort(securityType, this.mStoreType)));
        this.mPortView.addTextChangedListener(this.validationTextWatcher);
    }

    private void updateAuthPlainTextFromSecurityType(ConnectionSecurity securityType) {
        this.mAuthTypeAdapter.useInsecureText(securityType == ConnectionSecurity.NONE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == -1) {
            if ("android.intent.action.EDIT".equals(getIntent().getAction())) {
                boolean isPushCapable = false;
                try {
                    isPushCapable = this.mAccount.getRemoteStore().isPushCapable();
                } catch (Exception e) {
                    Log.e("k9", "Could not get remote store", e);
                }
                if (isPushCapable && this.mAccount.getFolderPushMode() != Account.FolderMode.NONE) {
                    MailService.actionRestartPushers(this, null);
                }
                this.mAccount.save(Preferences.getPreferences(this));
                finish();
                return;
            }
            try {
                String username = this.mUsernameView.getText().toString();
                String password = null;
                String clientCertificateAlias = null;
                AuthType authType = getSelectedAuthType();
                if (AuthType.EXTERNAL == authType) {
                    clientCertificateAlias = this.mClientCertificateSpinner.getAlias();
                } else {
                    password = this.mPasswordView.getText().toString();
                }
                URI oldUri = new URI(this.mAccount.getTransportUri());
                this.mAccount.setTransportUri(Transport.createTransportUri(new ServerSettings(ServerSettings.Type.SMTP, oldUri.getHost(), oldUri.getPort(), ConnectionSecurity.SSL_TLS_REQUIRED, authType, username, password, clientCertificateAlias)));
            } catch (URISyntaxException e2) {
            }
            AccountSetupOutgoing.actionOutgoingSettings(this, this.mAccount, this.mMakeDefault, this.isNewAccount);
            finish();
        } else if (resultCode == -11) {
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onNext() {
        if (getSelectedAuthType() == AuthType.XOAUTH2) {
            Globals.getOAuth2TokenProvider().authorizeAPI(this.mAccount.getEmail(), this, new OAuth2TokenProvider.OAuth2TokenProviderAuthCallback() {
                public void success() {
                    AccountSetupIncoming.this.updateAccountSettings("");
                    AccountSetupCheckSettings.actionCheckSettings(AccountSetupIncoming.this, AccountSetupIncoming.this.mAccount, AccountSetupCheckSettings.CheckDirection.INCOMING, AccountSetupIncoming.this.isNewAccount);
                }

                public void failure(AuthorizationException e) {
                    if (AccountSetupIncoming.this.getString(R.string.xoauth2_account_doesnt_exist).equals(e.getMessage())) {
                        GmailUtils.displayDialogOauth2(AccountSetupIncoming.this, AccountSetupIncoming.this.mAccount, AccountSetupIncoming.this.isNewAccount);
                    } else {
                        Toast.makeText(AccountSetupIncoming.this.getApplication(), AccountSetupIncoming.this.getString(R.string.account_setup_bad_uri, new Object[]{e.getMessage()}) + " " + AccountSetupIncoming.this.mAccount.getEmail(), 1).show();
                    }
                    Log.e("k9", "Failure", e);
                }
            });
            return;
        }
        updateAccountSettings(this.mPasswordView.getText().toString());
        AccountSetupCheckSettings.actionCheckSettings(this, this.mAccount, AccountSetupCheckSettings.CheckDirection.INCOMING, this.isNewAccount);
    }

    /* access modifiers changed from: private */
    public void updateAccountSettings(String password) {
        ConnectionSecurity connectionSecurity = getSelectedSecurity();
        String username = this.mUsernameView.getText().toString();
        String clientCertificateAlias = null;
        AuthType authType = getSelectedAuthType();
        if (authType == AuthType.EXTERNAL) {
            clientCertificateAlias = this.mClientCertificateSpinner.getAlias();
        }
        String host = this.mServerView.getText().toString();
        int port = Integer.parseInt(this.mPortView.getText().toString());
        Map<String, String> extra = null;
        if (ServerSettings.Type.IMAP == this.mStoreType) {
            extra = new HashMap<>();
            extra.put(ImapStoreSettings.AUTODETECT_NAMESPACE_KEY, Boolean.toString(this.mImapAutoDetectNamespaceView.isChecked()));
            extra.put(ImapStoreSettings.PATH_PREFIX_KEY, this.mImapPathPrefixView.getText().toString());
        } else if (ServerSettings.Type.WebDAV == this.mStoreType) {
            extra = new HashMap<>();
            extra.put(WebDavStoreSettings.PATH_KEY, this.mWebdavPathPrefixView.getText().toString());
            extra.put(WebDavStoreSettings.AUTH_PATH_KEY, this.mWebdavAuthPathView.getText().toString());
            extra.put(WebDavStoreSettings.MAILBOX_PATH_KEY, this.mWebdavMailboxPathView.getText().toString());
        }
        this.mAccount.deleteCertificate(host, port, AccountSetupCheckSettings.CheckDirection.INCOMING);
        this.mAccount.setStoreUri(RemoteStore.createStoreUri(new ServerSettings(this.mStoreType, host, port, connectionSecurity, authType, username, password, clientCertificateAlias, extra)));
        this.mAccount.setCompression(NetworkType.MOBILE, this.mCompressionMobile.isChecked());
        this.mAccount.setCompression(NetworkType.WIFI, this.mCompressionWifi.isChecked());
        this.mAccount.setCompression(NetworkType.OTHER, this.mCompressionOther.isChecked());
        this.mAccount.setSubscribedFoldersOnly(this.mSubscribedFoldersOnly.isChecked());
    }

    public void onClick(View v) {
        try {
            switch (v.getId()) {
                case R.id.next:
                    onNext();
                    return;
                default:
                    return;
            }
        } catch (Exception e) {
            failure(e);
        }
    }

    private void failure(Exception use) {
        Log.e("k9", "Failure", use);
        Toast.makeText(getApplication(), getString(R.string.account_setup_bad_uri, new Object[]{use.getMessage()}), 1).show();
    }

    /* access modifiers changed from: private */
    public AuthType getSelectedAuthType() {
        return ((AuthTypeHolder) this.mAuthTypeView.getSelectedItem()).authType;
    }

    private ConnectionSecurity getSelectedSecurity() {
        return ((ConnectionSecurityHolder) this.mSecurityTypeView.getSelectedItem()).connectionSecurity;
    }
}
