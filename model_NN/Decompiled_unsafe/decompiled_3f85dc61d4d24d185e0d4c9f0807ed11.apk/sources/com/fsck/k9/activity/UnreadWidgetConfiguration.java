package com.fsck.k9.activity;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import com.fsck.k9.BaseAccount;
import com.fsck.k9.provider.UnreadWidgetProvider;
import com.fsck.k9.search.SearchAccount;

public class UnreadWidgetConfiguration extends AccountList {
    private static final String PREFS_NAME = "unread_widget_configuration.xml";
    private static final String PREF_PREFIX_KEY = "unread_widget.";
    private int mAppWidgetId = 0;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.mAppWidgetId = extras.getInt("appWidgetId", 0);
        }
        if (this.mAppWidgetId == 0) {
            finish();
            return;
        }
        saveAccountUuid(this, this.mAppWidgetId, SearchAccount.UNIFIED_INBOX);
        Context context = getApplicationContext();
        UnreadWidgetProvider.updateWidget(context, AppWidgetManager.getInstance(context), this.mAppWidgetId, SearchAccount.UNIFIED_INBOX);
        Intent resultValue = new Intent();
        resultValue.putExtra("appWidgetId", this.mAppWidgetId);
        setResult(-1, resultValue);
        finish();
    }

    /* access modifiers changed from: protected */
    public boolean displaySpecialAccounts() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void onAccountSelected(BaseAccount account) {
        String accountUuid = account.getUuid();
        saveAccountUuid(this, this.mAppWidgetId, accountUuid);
        Context context = getApplicationContext();
        UnreadWidgetProvider.updateWidget(context, AppWidgetManager.getInstance(context), this.mAppWidgetId, accountUuid);
        Intent resultValue = new Intent();
        resultValue.putExtra("appWidgetId", this.mAppWidgetId);
        setResult(-1, resultValue);
        finish();
    }

    private static void saveAccountUuid(Context context, int appWidgetId, String accountUuid) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, 0).edit();
        editor.putString(PREF_PREFIX_KEY + appWidgetId, accountUuid);
        editor.commit();
    }

    public static String getAccountUuid(Context context, int appWidgetId) {
        return context.getSharedPreferences(PREFS_NAME, 0).getString(PREF_PREFIX_KEY + appWidgetId, null);
    }

    public static void deleteWidgetConfiguration(Context context, int appWidgetId) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, 0).edit();
        editor.remove(PREF_PREFIX_KEY + appWidgetId);
        editor.commit();
    }
}
