package com.fsck.k9.activity.setup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.TextKeyListener;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.fsck.k9.Account;
import com.fsck.k9.Preferences;
import com.fsck.k9.R;
import com.fsck.k9.activity.Accounts;
import com.fsck.k9.activity.K9Activity;
import com.fsck.k9.helper.Utility;

public class AccountSetupNames extends K9Activity implements View.OnClickListener {
    private static final String EXTRA_ACCOUNT = "account";
    private Account mAccount;
    private EditText mDescription;
    private Button mDoneButton;
    private EditText mName;

    public static void actionSetNames(Context context, Account account) {
        Intent i = new Intent(context, AccountSetupNames.class);
        i.putExtra("account", account.getUuid());
        context.startActivity(i);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.account_setup_names);
        this.mDescription = (EditText) findViewById(R.id.account_description);
        this.mName = (EditText) findViewById(R.id.account_name);
        this.mDoneButton = (Button) findViewById(R.id.done);
        this.mDoneButton.setOnClickListener(this);
        this.mName.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                AccountSetupNames.this.validateFields();
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
        this.mName.setKeyListener(TextKeyListener.getInstance(false, TextKeyListener.Capitalize.WORDS));
        this.mAccount = Preferences.getPreferences(this).getAccount(getIntent().getStringExtra("account"));
        if (this.mAccount.getName() != null) {
            this.mName.setText(this.mAccount.getName());
        }
        if (!Utility.requiredFieldValid(this.mName)) {
            this.mDoneButton.setEnabled(false);
        }
    }

    /* access modifiers changed from: private */
    public void validateFields() {
        this.mDoneButton.setEnabled(Utility.requiredFieldValid(this.mName));
        Utility.setCompoundDrawablesAlpha(this.mDoneButton, this.mDoneButton.isEnabled() ? 255 : 128);
    }

    /* access modifiers changed from: protected */
    public void onNext() {
        if (Utility.requiredFieldValid(this.mDescription)) {
            this.mAccount.setDescription(this.mDescription.getText().toString());
        }
        this.mAccount.setName(this.mName.getText().toString());
        this.mAccount.save(Preferences.getPreferences(this));
        Accounts.listAccounts(this);
        finish();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.done:
                onNext();
                return;
            default:
                return;
        }
    }
}
