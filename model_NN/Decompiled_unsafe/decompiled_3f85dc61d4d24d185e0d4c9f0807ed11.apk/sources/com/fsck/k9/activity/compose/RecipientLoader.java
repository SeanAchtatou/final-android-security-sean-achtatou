package com.fsck.k9.activity.compose;

import android.content.AsyncTaskLoader;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import com.fsck.k9.R;
import com.fsck.k9.mail.Address;
import com.fsck.k9.provider.AttachmentProvider;
import com.fsck.k9.view.RecipientSelectView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RecipientLoader extends AsyncTaskLoader<List<RecipientSelectView.Recipient>> {
    private static final int CRYPTO_PROVIDER_STATUS_TRUSTED = 2;
    private static final int CRYPTO_PROVIDER_STATUS_UNTRUSTED = 1;
    private static final int INDEX_CONTACT_ID = 6;
    private static final int INDEX_EMAIL = 3;
    private static final int INDEX_EMAIL_ADDRESS = 0;
    private static final int INDEX_EMAIL_CUSTOM_LABEL = 5;
    private static final int INDEX_EMAIL_STATUS = 1;
    private static final int INDEX_EMAIL_TYPE = 4;
    private static final int INDEX_LOOKUP_KEY = 2;
    private static final int INDEX_NAME = 1;
    private static final int INDEX_PHOTO_URI = 7;
    private static final String[] PROJECTION = {AttachmentProvider.AttachmentProviderColumns._ID, "display_name", "lookup", "data1", "data2", "data3", "contact_id", "photo_thumb_uri"};
    private static final String[] PROJECTION_CRYPTO_STATUS = {"email_address", "email_status"};
    private static final String SORT_ORDER = "times_contacted DESC, sort_key";
    private final Address[] addresses;
    private List<RecipientSelectView.Recipient> cachedRecipients;
    private final Uri contactUri;
    private final String cryptoProvider;
    private final Uri lookupKeyUri;
    private Loader<List<RecipientSelectView.Recipient>>.ForceLoadContentObserver observerContact;
    private Loader<List<RecipientSelectView.Recipient>>.ForceLoadContentObserver observerKey;
    private final String query;

    public /* bridge */ /* synthetic */ void deliverResult(Object obj) {
        deliverResult((List<RecipientSelectView.Recipient>) ((List) obj));
    }

    public RecipientLoader(Context context, String cryptoProvider2, String query2) {
        super(context);
        this.query = query2;
        this.lookupKeyUri = null;
        this.addresses = null;
        this.contactUri = null;
        this.cryptoProvider = cryptoProvider2;
    }

    public RecipientLoader(Context context, String cryptoProvider2, Address... addresses2) {
        super(context);
        this.query = null;
        this.addresses = addresses2;
        this.contactUri = null;
        this.cryptoProvider = cryptoProvider2;
        this.lookupKeyUri = null;
    }

    public RecipientLoader(Context context, String cryptoProvider2, Uri contactUri2, boolean isLookupKey) {
        super(context);
        Uri uri;
        this.query = null;
        this.addresses = null;
        if (isLookupKey) {
            uri = null;
        } else {
            uri = contactUri2;
        }
        this.contactUri = uri;
        this.lookupKeyUri = !isLookupKey ? null : contactUri2;
        this.cryptoProvider = cryptoProvider2;
    }

    public List<RecipientSelectView.Recipient> loadInBackground() {
        List<RecipientSelectView.Recipient> recipients = new ArrayList<>();
        Map<String, RecipientSelectView.Recipient> recipientMap = new HashMap<>();
        if (this.addresses != null) {
            fillContactDataFromAddresses(this.addresses, recipients, recipientMap);
        } else if (this.contactUri != null) {
            fillContactDataFromEmailContentUri(this.contactUri, recipients, recipientMap);
        } else if (this.query != null) {
            fillContactDataFromQuery(this.query, recipients, recipientMap);
        } else if (this.lookupKeyUri != null) {
            fillContactDataFromLookupKey(this.lookupKeyUri, recipients, recipientMap);
        } else {
            throw new IllegalStateException("loader must be initialized with query or list of addresses!");
        }
        if (!recipients.isEmpty() && this.cryptoProvider != null) {
            fillCryptoStatusData(recipientMap);
        }
        return recipients;
    }

    private void fillContactDataFromAddresses(Address[] addresses2, List<RecipientSelectView.Recipient> recipients, Map<String, RecipientSelectView.Recipient> recipientMap) {
        for (Address address : addresses2) {
            RecipientSelectView.Recipient recipient = new RecipientSelectView.Recipient(address);
            recipients.add(recipient);
            recipientMap.put(address.getAddress(), recipient);
        }
    }

    private void fillContactDataFromEmailContentUri(Uri contactUri2, List<RecipientSelectView.Recipient> recipients, Map<String, RecipientSelectView.Recipient> recipientMap) {
        Cursor cursor = getContext().getContentResolver().query(contactUri2, PROJECTION, null, null, null);
        if (cursor != null) {
            fillContactDataFromCursor(cursor, recipients, recipientMap);
        }
    }

    private void fillContactDataFromLookupKey(Uri lookupKeyUri2, List<RecipientSelectView.Recipient> recipients, Map<String, RecipientSelectView.Recipient> recipientMap) {
        Cursor cursor;
        Uri contactContentUri = ContactsContract.Contacts.lookupContact(getContext().getContentResolver(), lookupKeyUri2);
        if (contactContentUri != null && (cursor = getContext().getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, PROJECTION, "contact_id=?", new String[]{getContactIdFromContactUri(contactContentUri)}, null)) != null) {
            fillContactDataFromCursor(cursor, recipients, recipientMap);
        }
    }

    private static String getContactIdFromContactUri(Uri contactUri2) {
        return contactUri2.getLastPathSegment();
    }

    private void fillContactDataFromQuery(String query2, List<RecipientSelectView.Recipient> recipients, Map<String, RecipientSelectView.Recipient> recipientMap) {
        ContentResolver contentResolver = getContext().getContentResolver();
        String query3 = "%" + query2 + "%";
        Uri queryUri = ContactsContract.CommonDataKinds.Email.CONTENT_URI;
        Cursor cursor = contentResolver.query(queryUri, PROJECTION, "display_name LIKE ?  OR (data1 LIKE ? AND mimetype = 'vnd.android.cursor.item/email_v2')", new String[]{query3, query3}, SORT_ORDER);
        if (cursor != null) {
            fillContactDataFromCursor(cursor, recipients, recipientMap);
            if (this.observerContact != null) {
                this.observerContact = new Loader.ForceLoadContentObserver(this);
                contentResolver.registerContentObserver(queryUri, false, this.observerContact);
            }
        }
    }

    private void fillContactDataFromCursor(Cursor cursor, List<RecipientSelectView.Recipient> recipients, Map<String, RecipientSelectView.Recipient> recipientMap) {
        while (cursor.moveToNext()) {
            String name = cursor.getString(1);
            String email = cursor.getString(3);
            long contactId = cursor.getLong(6);
            String lookupKey = cursor.getString(2);
            if (!recipientMap.containsKey(email)) {
                String addressLabel = null;
                switch (cursor.getInt(4)) {
                    case 0:
                        addressLabel = cursor.getString(5);
                        break;
                    case 1:
                        addressLabel = getContext().getString(R.string.address_type_home);
                        break;
                    case 2:
                        addressLabel = getContext().getString(R.string.address_type_work);
                        break;
                    case 3:
                        addressLabel = getContext().getString(R.string.address_type_other);
                        break;
                    case 4:
                        addressLabel = getContext().getString(R.string.address_type_mobile);
                        break;
                }
                RecipientSelectView.Recipient recipient = new RecipientSelectView.Recipient(name, email, addressLabel, contactId, lookupKey);
                if (recipient.isValidEmailAddress()) {
                    recipient.photoThumbnailUri = cursor.isNull(7) ? null : Uri.parse(cursor.getString(7));
                    recipientMap.put(email, recipient);
                    recipients.add(recipient);
                }
            }
        }
        cursor.close();
    }

    private void fillCryptoStatusData(Map<String, RecipientSelectView.Recipient> recipientMap) {
        List<String> recipientList = new ArrayList<>(recipientMap.keySet());
        String[] recipientAddresses = (String[]) recipientList.toArray(new String[recipientList.size()]);
        Uri queryUri = Uri.parse("content://" + this.cryptoProvider + ".provider.exported/email_status");
        try {
            Cursor cursor = getContext().getContentResolver().query(queryUri, PROJECTION_CRYPTO_STATUS, null, recipientAddresses, null);
            initializeCryptoStatusForAllRecipients(recipientMap);
            if (cursor == null) {
                return;
            }
            while (cursor.moveToNext()) {
                String email = cursor.getString(0);
                int status = cursor.getInt(1);
                for (Address address : Address.parseUnencoded(email)) {
                    String emailAddress = address.getAddress();
                    if (recipientMap.containsKey(emailAddress)) {
                        RecipientSelectView.Recipient recipient = recipientMap.get(emailAddress);
                        switch (status) {
                            case 1:
                                if (recipient.getCryptoStatus() == RecipientSelectView.RecipientCryptoStatus.UNAVAILABLE) {
                                    recipient.setCryptoStatus(RecipientSelectView.RecipientCryptoStatus.AVAILABLE_UNTRUSTED);
                                    break;
                                }
                                break;
                            case 2:
                                if (recipient.getCryptoStatus() != RecipientSelectView.RecipientCryptoStatus.AVAILABLE_TRUSTED) {
                                    recipient.setCryptoStatus(RecipientSelectView.RecipientCryptoStatus.AVAILABLE_TRUSTED);
                                    break;
                                }
                                break;
                        }
                    }
                }
            }
            cursor.close();
            if (this.observerKey != null) {
                this.observerKey = new Loader.ForceLoadContentObserver(this);
                getContext().getContentResolver().registerContentObserver(queryUri, false, this.observerKey);
            }
        } catch (SecurityException e) {
        }
    }

    private void initializeCryptoStatusForAllRecipients(Map<String, RecipientSelectView.Recipient> recipientMap) {
        for (RecipientSelectView.Recipient recipient : recipientMap.values()) {
            recipient.setCryptoStatus(RecipientSelectView.RecipientCryptoStatus.UNAVAILABLE);
        }
    }

    public void deliverResult(List<RecipientSelectView.Recipient> data) {
        this.cachedRecipients = data;
        if (isStarted()) {
            super.deliverResult((Object) data);
        }
    }

    /* access modifiers changed from: protected */
    public void onStartLoading() {
        if (this.cachedRecipients != null) {
            super.deliverResult((Object) this.cachedRecipients);
        } else if (takeContentChanged() || this.cachedRecipients == null) {
            forceLoad();
        }
    }

    /* access modifiers changed from: protected */
    public void onAbandon() {
        super.onAbandon();
        if (this.observerKey != null) {
            getContext().getContentResolver().unregisterContentObserver(this.observerKey);
        }
        if (this.observerContact != null) {
            getContext().getContentResolver().unregisterContentObserver(this.observerContact);
        }
    }
}
