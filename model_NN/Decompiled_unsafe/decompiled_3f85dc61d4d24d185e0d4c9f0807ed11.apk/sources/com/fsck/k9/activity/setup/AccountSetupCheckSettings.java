package com.fsck.k9.activity.setup;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import atomicgonza.AccountUtils;
import com.fsck.k9.Account;
import com.fsck.k9.K9;
import com.fsck.k9.Preferences;
import com.fsck.k9.R;
import com.fsck.k9.account.AndroidAccountOAuth2TokenStore;
import com.fsck.k9.activity.K9Activity;
import com.fsck.k9.controller.MessagingController;
import com.fsck.k9.fragment.ConfirmationDialogFragment;
import com.fsck.k9.mail.AuthenticationFailedException;
import com.fsck.k9.mail.CertificateValidationException;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.Store;
import com.fsck.k9.mail.Transport;
import com.fsck.k9.mail.store.webdav.WebDavStore;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Locale;

public class AccountSetupCheckSettings extends K9Activity implements View.OnClickListener, ConfirmationDialogFragment.ConfirmationDialogFragmentListener {
    public static final int ACTIVITY_REQUEST_CODE = 1;
    private static final String EXTRA_ACCOUNT = "account";
    private static final String EXTRA_CHECK_DIRECTION = "checkDirection";
    public static final String EXTRA_NEW_ACCOUNT = "newaccount";
    /* access modifiers changed from: private */
    public Account mAccount;
    /* access modifiers changed from: private */
    public boolean mCanceled;
    /* access modifiers changed from: private */
    public boolean mDestroyed;
    private CheckDirection mDirection;
    private Handler mHandler = new Handler();
    private TextView mMessageView;
    /* access modifiers changed from: private */
    public ProgressBar mProgressBar;

    public enum CheckDirection {
        INCOMING,
        OUTGOING
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static void actionCheckSettings(Activity context, Account account, CheckDirection direction, boolean newAccount) {
        Intent i = new Intent(context, AccountSetupCheckSettings.class);
        i.putExtra("account", account.getUuid());
        i.putExtra(EXTRA_CHECK_DIRECTION, direction);
        i.putExtra(EXTRA_NEW_ACCOUNT, false);
        context.startActivityForResult(i, 1);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.account_setup_check_settings);
        this.mMessageView = (TextView) findViewById(R.id.message);
        this.mProgressBar = (ProgressBar) findViewById(R.id.progress);
        findViewById(R.id.cancel).setOnClickListener(this);
        setMessage(R.string.account_setup_check_settings_retr_info_msg);
        this.mProgressBar.setIndeterminate(true);
        Intent intent = getIntent();
        this.mAccount = Preferences.getPreferences(this).getAccount(intent.getStringExtra("account"));
        this.mDirection = (CheckDirection) getIntent().getSerializableExtra(EXTRA_CHECK_DIRECTION);
        new CheckAccountTask(this.mAccount, Boolean.valueOf(intent.getBooleanExtra(EXTRA_NEW_ACCOUNT, false))).execute(this.mDirection);
    }

    /* access modifiers changed from: private */
    public void handleCertificateValidationException(CertificateValidationException cve) {
        Log.e("k9", "Error while testing settings (cve)", cve);
        if (cve.getCertChain() != null) {
            acceptKeyDialog(R.string.account_setup_failed_dlg_certificate_message_fmt, cve);
            return;
        }
        showErrorDialog(R.string.account_setup_failed_dlg_server_message_fmt, errorMessageForCertificateException(cve));
    }

    public void onDestroy() {
        super.onDestroy();
        this.mDestroyed = true;
        this.mCanceled = true;
    }

    /* access modifiers changed from: private */
    public void setMessage(int resId) {
        this.mMessageView.setText(getString(resId));
    }

    private void acceptKeyDialog(final int msgResId, final CertificateValidationException ex) {
        this.mHandler.post(new Runnable() {
            /* JADX WARNING: Removed duplicated region for block: B:43:0x01b9 A[Catch:{ Exception -> 0x0123 }] */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r24 = this;
                    r0 = r24
                    com.fsck.k9.activity.setup.AccountSetupCheckSettings r0 = com.fsck.k9.activity.setup.AccountSetupCheckSettings.this
                    r18 = r0
                    boolean r18 = r18.mDestroyed
                    if (r18 == 0) goto L_0x000d
                L_0x000c:
                    return
                L_0x000d:
                    java.lang.String r7 = "Unknown Error"
                    r0 = r24
                    com.fsck.k9.mail.CertificateValidationException r0 = r4
                    r18 = r0
                    if (r18 == 0) goto L_0x0045
                    r0 = r24
                    com.fsck.k9.mail.CertificateValidationException r0 = r4
                    r18 = r0
                    java.lang.Throwable r18 = r18.getCause()
                    if (r18 == 0) goto L_0x018b
                    r0 = r24
                    com.fsck.k9.mail.CertificateValidationException r0 = r4
                    r18 = r0
                    java.lang.Throwable r18 = r18.getCause()
                    java.lang.Throwable r18 = r18.getCause()
                    if (r18 == 0) goto L_0x017b
                    r0 = r24
                    com.fsck.k9.mail.CertificateValidationException r0 = r4
                    r18 = r0
                    java.lang.Throwable r18 = r18.getCause()
                    java.lang.Throwable r18 = r18.getCause()
                    java.lang.String r7 = r18.getMessage()
                L_0x0045:
                    r0 = r24
                    com.fsck.k9.activity.setup.AccountSetupCheckSettings r0 = com.fsck.k9.activity.setup.AccountSetupCheckSettings.this
                    r18 = r0
                    android.widget.ProgressBar r18 = r18.mProgressBar
                    r19 = 0
                    r18.setIndeterminate(r19)
                    java.lang.StringBuilder r4 = new java.lang.StringBuilder
                    r18 = 100
                    r0 = r18
                    r4.<init>(r0)
                    r10 = 0
                    java.lang.String r18 = "SHA-1"
                    java.security.MessageDigest r10 = java.security.MessageDigest.getInstance(r18)     // Catch:{ NoSuchAlgorithmException -> 0x0197 }
                L_0x0064:
                    r0 = r24
                    com.fsck.k9.mail.CertificateValidationException r0 = r4
                    r18 = r0
                    java.security.cert.X509Certificate[] r3 = r18.getCertChain()
                    r8 = 0
                L_0x006f:
                    int r0 = r3.length
                    r18 = r0
                    r0 = r18
                    if (r8 >= r0) goto L_0x0253
                    java.lang.String r18 = "Certificate chain["
                    r0 = r18
                    java.lang.StringBuilder r18 = r4.append(r0)
                    r0 = r18
                    java.lang.StringBuilder r18 = r0.append(r8)
                    java.lang.String r19 = "]:\n"
                    r18.append(r19)
                    java.lang.String r18 = "Subject: "
                    r0 = r18
                    java.lang.StringBuilder r18 = r4.append(r0)
                    r19 = r3[r8]
                    java.security.Principal r19 = r19.getSubjectDN()
                    java.lang.String r19 = r19.toString()
                    java.lang.StringBuilder r18 = r18.append(r19)
                    java.lang.String r19 = "\n"
                    r18.append(r19)
                    r18 = r3[r8]     // Catch:{ Exception -> 0x0123 }
                    java.util.Collection r14 = r18.getSubjectAlternativeNames()     // Catch:{ Exception -> 0x0123 }
                    if (r14 == 0) goto L_0x012f
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0123 }
                    r2.<init>()     // Catch:{ Exception -> 0x0123 }
                    java.lang.String r18 = "Subject has "
                    r0 = r18
                    java.lang.StringBuilder r18 = r2.append(r0)     // Catch:{ Exception -> 0x0123 }
                    int r19 = r14.size()     // Catch:{ Exception -> 0x0123 }
                    java.lang.StringBuilder r18 = r18.append(r19)     // Catch:{ Exception -> 0x0123 }
                    java.lang.String r19 = " alternative names\n"
                    r18.append(r19)     // Catch:{ Exception -> 0x0123 }
                    r0 = r24
                    com.fsck.k9.activity.setup.AccountSetupCheckSettings r0 = com.fsck.k9.activity.setup.AccountSetupCheckSettings.this     // Catch:{ Exception -> 0x0123 }
                    r18 = r0
                    com.fsck.k9.Account r18 = r18.mAccount     // Catch:{ Exception -> 0x0123 }
                    java.lang.String r18 = r18.getStoreUri()     // Catch:{ Exception -> 0x0123 }
                    android.net.Uri r18 = android.net.Uri.parse(r18)     // Catch:{ Exception -> 0x0123 }
                    java.lang.String r12 = r18.getHost()     // Catch:{ Exception -> 0x0123 }
                    r0 = r24
                    com.fsck.k9.activity.setup.AccountSetupCheckSettings r0 = com.fsck.k9.activity.setup.AccountSetupCheckSettings.this     // Catch:{ Exception -> 0x0123 }
                    r18 = r0
                    com.fsck.k9.Account r18 = r18.mAccount     // Catch:{ Exception -> 0x0123 }
                    java.lang.String r18 = r18.getTransportUri()     // Catch:{ Exception -> 0x0123 }
                    android.net.Uri r18 = android.net.Uri.parse(r18)     // Catch:{ Exception -> 0x0123 }
                    java.lang.String r15 = r18.getHost()     // Catch:{ Exception -> 0x0123 }
                    java.util.Iterator r18 = r14.iterator()     // Catch:{ Exception -> 0x0123 }
                L_0x00f6:
                    boolean r19 = r18.hasNext()     // Catch:{ Exception -> 0x0123 }
                    if (r19 == 0) goto L_0x0240
                    java.lang.Object r13 = r18.next()     // Catch:{ Exception -> 0x0123 }
                    java.util.List r13 = (java.util.List) r13     // Catch:{ Exception -> 0x0123 }
                    r19 = 0
                    r0 = r19
                    java.lang.Object r16 = r13.get(r0)     // Catch:{ Exception -> 0x0123 }
                    java.lang.Integer r16 = (java.lang.Integer) r16     // Catch:{ Exception -> 0x0123 }
                    r19 = 1
                    r0 = r19
                    java.lang.Object r17 = r13.get(r0)     // Catch:{ Exception -> 0x0123 }
                    int r19 = r16.intValue()     // Catch:{ Exception -> 0x0123 }
                    switch(r19) {
                        case 0: goto L_0x01a5;
                        case 1: goto L_0x01ae;
                        case 2: goto L_0x01d4;
                        case 3: goto L_0x01da;
                        case 4: goto L_0x01e3;
                        case 5: goto L_0x01ec;
                        case 6: goto L_0x01f5;
                        case 7: goto L_0x01fb;
                        default: goto L_0x011b;
                    }     // Catch:{ Exception -> 0x0123 }
                L_0x011b:
                    java.lang.String r19 = "k9"
                    java.lang.String r20 = "unsupported SubjectAltName of unknown type"
                    android.util.Log.w(r19, r20)     // Catch:{ Exception -> 0x0123 }
                    goto L_0x00f6
                L_0x0123:
                    r6 = move-exception
                    java.lang.String r18 = "k9"
                    java.lang.String r19 = "cannot display SubjectAltNames in dialog"
                    r0 = r18
                    r1 = r19
                    android.util.Log.w(r0, r1, r6)
                L_0x012f:
                    java.lang.String r18 = "Issuer: "
                    r0 = r18
                    java.lang.StringBuilder r18 = r4.append(r0)
                    r19 = r3[r8]
                    java.security.Principal r19 = r19.getIssuerDN()
                    java.lang.String r19 = r19.toString()
                    java.lang.StringBuilder r18 = r18.append(r19)
                    java.lang.String r19 = "\n"
                    r18.append(r19)
                    if (r10 == 0) goto L_0x0177
                    r10.reset()
                    r18 = r3[r8]     // Catch:{ CertificateEncodingException -> 0x0245 }
                    byte[] r18 = r18.getEncoded()     // Catch:{ CertificateEncodingException -> 0x0245 }
                    r0 = r18
                    byte[] r18 = r10.digest(r0)     // Catch:{ CertificateEncodingException -> 0x0245 }
                    char[] r11 = com.fsck.k9.mail.filter.Hex.encodeHex(r18)     // Catch:{ CertificateEncodingException -> 0x0245 }
                    java.lang.String r18 = "Fingerprint (SHA-1): "
                    r0 = r18
                    java.lang.StringBuilder r18 = r4.append(r0)     // Catch:{ CertificateEncodingException -> 0x0245 }
                    java.lang.String r19 = new java.lang.String     // Catch:{ CertificateEncodingException -> 0x0245 }
                    r0 = r19
                    r0.<init>(r11)     // Catch:{ CertificateEncodingException -> 0x0245 }
                    java.lang.StringBuilder r18 = r18.append(r19)     // Catch:{ CertificateEncodingException -> 0x0245 }
                    java.lang.String r19 = "\n"
                    r18.append(r19)     // Catch:{ CertificateEncodingException -> 0x0245 }
                L_0x0177:
                    int r8 = r8 + 1
                    goto L_0x006f
                L_0x017b:
                    r0 = r24
                    com.fsck.k9.mail.CertificateValidationException r0 = r4
                    r18 = r0
                    java.lang.Throwable r18 = r18.getCause()
                    java.lang.String r7 = r18.getMessage()
                    goto L_0x0045
                L_0x018b:
                    r0 = r24
                    com.fsck.k9.mail.CertificateValidationException r0 = r4
                    r18 = r0
                    java.lang.String r7 = r18.getMessage()
                    goto L_0x0045
                L_0x0197:
                    r5 = move-exception
                    java.lang.String r18 = "k9"
                    java.lang.String r19 = "Error while initializing MessageDigest"
                    r0 = r18
                    r1 = r19
                    android.util.Log.e(r0, r1, r5)
                    goto L_0x0064
                L_0x01a5:
                    java.lang.String r19 = "k9"
                    java.lang.String r20 = "SubjectAltName of type OtherName not supported."
                    android.util.Log.w(r19, r20)     // Catch:{ Exception -> 0x0123 }
                    goto L_0x00f6
                L_0x01ae:
                    r0 = r17
                    java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0123 }
                    r9 = r0
                L_0x01b3:
                    boolean r19 = r9.equalsIgnoreCase(r12)     // Catch:{ Exception -> 0x0123 }
                    if (r19 != 0) goto L_0x01bf
                    boolean r19 = r9.equalsIgnoreCase(r15)     // Catch:{ Exception -> 0x0123 }
                    if (r19 == 0) goto L_0x0201
                L_0x01bf:
                    java.lang.String r19 = "Subject(alt): "
                    r0 = r19
                    java.lang.StringBuilder r19 = r2.append(r0)     // Catch:{ Exception -> 0x0123 }
                    r0 = r19
                    java.lang.StringBuilder r19 = r0.append(r9)     // Catch:{ Exception -> 0x0123 }
                    java.lang.String r20 = ",...\n"
                    r19.append(r20)     // Catch:{ Exception -> 0x0123 }
                    goto L_0x00f6
                L_0x01d4:
                    r0 = r17
                    java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0123 }
                    r9 = r0
                    goto L_0x01b3
                L_0x01da:
                    java.lang.String r19 = "k9"
                    java.lang.String r20 = "unsupported SubjectAltName of type x400Address"
                    android.util.Log.w(r19, r20)     // Catch:{ Exception -> 0x0123 }
                    goto L_0x00f6
                L_0x01e3:
                    java.lang.String r19 = "k9"
                    java.lang.String r20 = "unsupported SubjectAltName of type directoryName"
                    android.util.Log.w(r19, r20)     // Catch:{ Exception -> 0x0123 }
                    goto L_0x00f6
                L_0x01ec:
                    java.lang.String r19 = "k9"
                    java.lang.String r20 = "unsupported SubjectAltName of type ediPartyName"
                    android.util.Log.w(r19, r20)     // Catch:{ Exception -> 0x0123 }
                    goto L_0x00f6
                L_0x01f5:
                    r0 = r17
                    java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0123 }
                    r9 = r0
                    goto L_0x01b3
                L_0x01fb:
                    r0 = r17
                    java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0123 }
                    r9 = r0
                    goto L_0x01b3
                L_0x0201:
                    java.lang.String r19 = "*."
                    r0 = r19
                    boolean r19 = r9.startsWith(r0)     // Catch:{ Exception -> 0x0123 }
                    if (r19 == 0) goto L_0x00f6
                    r19 = 2
                    r0 = r19
                    java.lang.String r19 = r9.substring(r0)     // Catch:{ Exception -> 0x0123 }
                    r0 = r19
                    boolean r19 = r12.endsWith(r0)     // Catch:{ Exception -> 0x0123 }
                    if (r19 != 0) goto L_0x022b
                    r19 = 2
                    r0 = r19
                    java.lang.String r19 = r9.substring(r0)     // Catch:{ Exception -> 0x0123 }
                    r0 = r19
                    boolean r19 = r15.endsWith(r0)     // Catch:{ Exception -> 0x0123 }
                    if (r19 == 0) goto L_0x00f6
                L_0x022b:
                    java.lang.String r19 = "Subject(alt): "
                    r0 = r19
                    java.lang.StringBuilder r19 = r2.append(r0)     // Catch:{ Exception -> 0x0123 }
                    r0 = r19
                    java.lang.StringBuilder r19 = r0.append(r9)     // Catch:{ Exception -> 0x0123 }
                    java.lang.String r20 = ",...\n"
                    r19.append(r20)     // Catch:{ Exception -> 0x0123 }
                    goto L_0x00f6
                L_0x0240:
                    r4.append(r2)     // Catch:{ Exception -> 0x0123 }
                    goto L_0x012f
                L_0x0245:
                    r5 = move-exception
                    java.lang.String r18 = "k9"
                    java.lang.String r19 = "Error while encoding certificate"
                    r0 = r18
                    r1 = r19
                    android.util.Log.e(r0, r1, r5)
                    goto L_0x0177
                L_0x0253:
                    android.app.AlertDialog$Builder r18 = new android.app.AlertDialog$Builder
                    r0 = r24
                    com.fsck.k9.activity.setup.AccountSetupCheckSettings r0 = com.fsck.k9.activity.setup.AccountSetupCheckSettings.this
                    r19 = r0
                    r18.<init>(r19)
                    r0 = r24
                    com.fsck.k9.activity.setup.AccountSetupCheckSettings r0 = com.fsck.k9.activity.setup.AccountSetupCheckSettings.this
                    r19 = r0
                    r20 = 2131230933(0x7f0800d5, float:1.8077933E38)
                    java.lang.String r19 = r19.getString(r20)
                    android.app.AlertDialog$Builder r18 = r18.setTitle(r19)
                    java.lang.StringBuilder r19 = new java.lang.StringBuilder
                    r19.<init>()
                    r0 = r24
                    com.fsck.k9.activity.setup.AccountSetupCheckSettings r0 = com.fsck.k9.activity.setup.AccountSetupCheckSettings.this
                    r20 = r0
                    r0 = r24
                    int r0 = r3
                    r21 = r0
                    r22 = 1
                    r0 = r22
                    java.lang.Object[] r0 = new java.lang.Object[r0]
                    r22 = r0
                    r23 = 0
                    r22[r23] = r7
                    java.lang.String r20 = r20.getString(r21, r22)
                    java.lang.StringBuilder r19 = r19.append(r20)
                    java.lang.String r20 = " "
                    java.lang.StringBuilder r19 = r19.append(r20)
                    java.lang.String r20 = r4.toString()
                    java.lang.StringBuilder r19 = r19.append(r20)
                    java.lang.String r19 = r19.toString()
                    android.app.AlertDialog$Builder r18 = r18.setMessage(r19)
                    r19 = 1
                    android.app.AlertDialog$Builder r18 = r18.setCancelable(r19)
                    r0 = r24
                    com.fsck.k9.activity.setup.AccountSetupCheckSettings r0 = com.fsck.k9.activity.setup.AccountSetupCheckSettings.this
                    r19 = r0
                    r20 = 2131230931(0x7f0800d3, float:1.8077929E38)
                    java.lang.String r19 = r19.getString(r20)
                    com.fsck.k9.activity.setup.AccountSetupCheckSettings$1$2 r20 = new com.fsck.k9.activity.setup.AccountSetupCheckSettings$1$2
                    r0 = r20
                    r1 = r24
                    r0.<init>(r3)
                    android.app.AlertDialog$Builder r18 = r18.setPositiveButton(r19, r20)
                    r0 = r24
                    com.fsck.k9.activity.setup.AccountSetupCheckSettings r0 = com.fsck.k9.activity.setup.AccountSetupCheckSettings.this
                    r19 = r0
                    r20 = 2131230932(0x7f0800d4, float:1.807793E38)
                    java.lang.String r19 = r19.getString(r20)
                    com.fsck.k9.activity.setup.AccountSetupCheckSettings$1$1 r20 = new com.fsck.k9.activity.setup.AccountSetupCheckSettings$1$1
                    r0 = r20
                    r1 = r24
                    r0.<init>()
                    android.app.AlertDialog$Builder r18 = r18.setNegativeButton(r19, r20)
                    r18.show()
                    goto L_0x000c
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fsck.k9.activity.setup.AccountSetupCheckSettings.AnonymousClass1.run():void");
            }
        });
    }

    /* access modifiers changed from: private */
    public void acceptCertificate(X509Certificate certificate) {
        try {
            this.mAccount.addCertificate(this.mDirection, certificate);
        } catch (CertificateException e) {
            Object[] objArr = new Object[1];
            objArr[0] = e.getMessage() == null ? "" : e.getMessage();
            showErrorDialog(R.string.account_setup_failed_dlg_certificate_message_fmt, objArr);
        }
        actionCheckSettings(this, this.mAccount, this.mDirection, false);
    }

    public void onActivityResult(int reqCode, int resCode, Intent data) {
        setResult(resCode);
        finish();
    }

    private void onCancel() {
        this.mCanceled = true;
        setMessage(R.string.account_setup_check_settings_canceling_msg);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancel:
                onCancel();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void showErrorDialog(final int msgResId, final Object... args) {
        this.mHandler.post(new Runnable() {
            public void run() {
                AccountSetupCheckSettings.this.showDialogFragment(R.id.dialog_account_setup_error, AccountSetupCheckSettings.this.getString(msgResId, args));
            }
        });
    }

    /* access modifiers changed from: private */
    public void showDialogFragment(int dialogId, String customMessage) {
        if (!this.mDestroyed) {
            this.mProgressBar.setIndeterminate(false);
            switch (dialogId) {
                case R.id.dialog_account_setup_error:
                    DialogFragment fragment = ConfirmationDialogFragment.newInstance(dialogId, getString(R.string.account_setup_failed_dlg_title), customMessage, getString(R.string.account_setup_failed_dlg_edit_details_action), getString(R.string.account_setup_failed_dlg_continue_action));
                    FragmentTransaction ta = getFragmentManager().beginTransaction();
                    ta.add(fragment, getDialogTag(dialogId));
                    ta.commitAllowingStateLoss();
                    return;
                default:
                    throw new RuntimeException("Called showDialog(int) with unknown dialog id.");
            }
        }
    }

    private String getDialogTag(int dialogId) {
        return String.format(Locale.US, "dialog-%d", Integer.valueOf(dialogId));
    }

    public void doPositiveClick(int dialogId) {
        switch (dialogId) {
            case R.id.dialog_account_setup_error:
                finish();
                return;
            default:
                return;
        }
    }

    public void doNegativeClick(int dialogId) {
        switch (dialogId) {
            case R.id.dialog_account_setup_error:
                this.mCanceled = false;
                setResult(-1);
                finish();
                return;
            default:
                return;
        }
    }

    public void dialogCancelled(int dialogId) {
    }

    private String errorMessageForCertificateException(CertificateValidationException e) {
        switch (e.getReason()) {
            case Expired:
                return getString(R.string.client_certificate_expired, new Object[]{e.getAlias(), e.getMessage()});
            case MissingCapability:
                return getString(R.string.auth_external_error);
            case RetrievalFailure:
                return getString(R.string.client_certificate_retrieval_failure, new Object[]{e.getAlias()});
            case UseMessage:
                return e.getMessage();
            default:
                return "";
        }
    }

    private class CheckAccountTask extends AsyncTask<CheckDirection, Integer, Exception> {
        private final Account account;
        private final Boolean isNewAccount;

        private CheckAccountTask(Account account2, Boolean isNewAccount2) {
            this.account = account2;
            this.isNewAccount = isNewAccount2;
        }

        /* access modifiers changed from: protected */
        public Exception doInBackground(CheckDirection... params) {
            CheckDirection direction = params[0];
            try {
                if (cancelled()) {
                    return null;
                }
                clearCertificateErrorNotifications(direction);
                checkServerSettings(direction);
                if (cancelled()) {
                    return null;
                }
                AccountSetupCheckSettings.this.setResult(-1);
                AccountSetupCheckSettings.this.finish();
                return null;
            } catch (AuthenticationFailedException afe) {
                if (this.isNewAccount.booleanValue()) {
                    AccountUtils.removeAccountTask(AccountSetupCheckSettings.this.mAccount, AccountSetupCheckSettings.this);
                    return afe;
                }
                Log.e("k9", "Error while testing settings (auth failed)", afe);
                AccountSetupCheckSettings accountSetupCheckSettings = AccountSetupCheckSettings.this;
                Object[] objArr = new Object[1];
                objArr[0] = afe.getMessage() == null ? "" : afe.getMessage();
                accountSetupCheckSettings.showErrorDialog(R.string.account_setup_failed_dlg_auth_message_fmt, objArr);
                return afe;
            } catch (CertificateValidationException cve) {
                AccountSetupCheckSettings.this.handleCertificateValidationException(cve);
                return null;
            } catch (Exception e) {
                if (this.isNewAccount.booleanValue()) {
                    AccountUtils.removeAccountTask(AccountSetupCheckSettings.this.mAccount, AccountSetupCheckSettings.this);
                } else {
                    Log.e("k9", "Error while testing settings", e);
                    AccountSetupCheckSettings.this.showErrorDialog(R.string.account_setup_failed_dlg_server_message_fmt, e.getMessage() == null ? "" : e.getMessage());
                }
                return e;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Exception e) {
            String msg;
            if (this.isNewAccount.booleanValue() && e != null) {
                if (e instanceof AuthenticationFailedException) {
                    msg = AccountSetupCheckSettings.this.getString(R.string.account_setup_failed_dlg_auth_message_fmt);
                } else {
                    msg = AccountSetupCheckSettings.this.getString(R.string.account_setup_failed_dlg_server_message_fmt);
                }
                new AlertDialog.Builder(AccountSetupCheckSettings.this).setMessage(msg).setNegativeButton((int) R.string.okay_action, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        AccountSetupCheckSettings.this.setResult(-11);
                        AccountSetupCheckSettings.this.finish();
                    }
                }).setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        AccountSetupCheckSettings.this.setResult(-11);
                        AccountSetupCheckSettings.this.finish();
                    }
                }).show();
            }
        }

        private void clearCertificateErrorNotifications(CheckDirection direction) {
            MessagingController.getInstance(AccountSetupCheckSettings.this.getApplication()).clearCertificateErrorNotifications(this.account, direction);
        }

        private boolean cancelled() {
            if (AccountSetupCheckSettings.this.mDestroyed) {
                return true;
            }
            if (!AccountSetupCheckSettings.this.mCanceled) {
                return false;
            }
            AccountSetupCheckSettings.this.finish();
            return true;
        }

        private void checkServerSettings(CheckDirection direction) throws MessagingException {
            switch (direction) {
                case INCOMING:
                    checkIncoming();
                    return;
                case OUTGOING:
                    checkOutgoing();
                    return;
                default:
                    return;
            }
        }

        private void checkOutgoing() throws MessagingException {
            if (!(this.account.getRemoteStore() instanceof WebDavStore)) {
                publishProgress(Integer.valueOf((int) R.string.account_setup_check_settings_check_outgoing_msg));
            }
            Transport transport = Transport.getInstance(K9.app, this.account, new AndroidAccountOAuth2TokenStore(AccountSetupCheckSettings.this));
            transport.close();
            try {
                transport.open();
            } finally {
                transport.close();
            }
        }

        private void checkIncoming() throws MessagingException {
            Store store = this.account.getRemoteStore();
            if (store instanceof WebDavStore) {
                publishProgress(Integer.valueOf((int) R.string.account_setup_check_settings_authenticate));
            } else {
                publishProgress(Integer.valueOf((int) R.string.account_setup_check_settings_check_incoming_msg));
            }
            store.checkSettings();
            if (store instanceof WebDavStore) {
                publishProgress(Integer.valueOf((int) R.string.account_setup_check_settings_fetch));
            }
            MessagingController.getInstance(AccountSetupCheckSettings.this.getApplication()).listFoldersSynchronous(this.account, true, null);
            MessagingController.getInstance(AccountSetupCheckSettings.this.getApplication()).synchronizeMailbox(this.account, this.account.getInboxFolderName(), null, null);
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(Integer... values) {
            AccountSetupCheckSettings.this.setMessage(values[0].intValue());
        }
    }
}
