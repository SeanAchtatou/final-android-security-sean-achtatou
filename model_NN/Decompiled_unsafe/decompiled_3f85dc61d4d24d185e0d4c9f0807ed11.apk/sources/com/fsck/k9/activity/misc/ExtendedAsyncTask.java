package com.fsck.k9.activity.misc;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public abstract class ExtendedAsyncTask<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> implements NonConfigurationInstance {
    protected Activity mActivity;
    protected Context mContext;
    protected ProgressDialog mProgressDialog;

    /* access modifiers changed from: protected */
    public abstract void showProgressDialog();

    protected ExtendedAsyncTask(Activity activity) {
        this.mActivity = activity;
        this.mContext = activity.getApplicationContext();
    }

    public void restore(Activity activity) {
        this.mActivity = activity;
        showProgressDialog();
    }

    public boolean retain() {
        boolean retain = false;
        if (this.mProgressDialog != null) {
            removeProgressDialog();
            retain = true;
        }
        this.mActivity = null;
        return retain;
    }

    /* access modifiers changed from: protected */
    public void removeProgressDialog() {
        this.mProgressDialog.dismiss();
        this.mProgressDialog = null;
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        showProgressDialog();
    }
}
