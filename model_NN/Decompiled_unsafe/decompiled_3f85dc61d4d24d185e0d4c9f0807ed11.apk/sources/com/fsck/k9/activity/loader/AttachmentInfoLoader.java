package com.fsck.k9.activity.loader;

import android.content.AsyncTaskLoader;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;
import com.fsck.k9.activity.misc.Attachment;
import com.fsck.k9.mail.internet.MimeUtility;
import com.fsck.k9.provider.AttachmentProvider;
import java.io.File;

public class AttachmentInfoLoader extends AsyncTaskLoader<Attachment> {
    private Attachment cachedResultAttachment;
    private final Attachment sourceAttachment;

    public AttachmentInfoLoader(Context context, Attachment attachment) {
        super(context);
        if (attachment.state != Attachment.LoadingState.URI_ONLY) {
            throw new IllegalArgumentException("Attachment provided to metadata loader must be in URI_ONLY state");
        }
        this.sourceAttachment = attachment;
    }

    /* access modifiers changed from: protected */
    public void onStartLoading() {
        if (this.cachedResultAttachment != null) {
            deliverResult(this.cachedResultAttachment);
        }
        if (takeContentChanged() || this.cachedResultAttachment == null) {
            forceLoad();
        }
    }

    public Attachment loadInBackground() {
        Uri uri = this.sourceAttachment.uri;
        String contentType = this.sourceAttachment.contentType;
        long size = -1;
        String name = null;
        ContentResolver contentResolver = getContext().getContentResolver();
        Cursor metadataCursor = contentResolver.query(uri, new String[]{AttachmentProvider.AttachmentProviderColumns.DISPLAY_NAME, AttachmentProvider.AttachmentProviderColumns.SIZE}, null, null, null);
        if (metadataCursor != null) {
            try {
                if (metadataCursor.moveToFirst()) {
                    name = metadataCursor.getString(0);
                    size = (long) metadataCursor.getInt(1);
                }
            } finally {
                metadataCursor.close();
            }
        }
        if (name == null) {
            name = uri.getLastPathSegment();
        }
        String usableContentType = contentResolver.getType(uri);
        if (!(usableContentType != null || contentType == null || contentType.indexOf(42) == -1)) {
            usableContentType = contentType;
        }
        if (usableContentType == null) {
            usableContentType = MimeUtility.getMimeTypeByExtension(name);
        }
        if (size <= 0) {
            String uriString = uri.toString();
            if (uriString.startsWith("file://")) {
                Log.v("k9", uriString.substring("file://".length()));
                size = new File(uriString.substring("file://".length())).length();
            } else {
                Log.v("k9", "Not a file: " + uriString);
            }
        } else {
            Log.v("k9", "old attachment.size: " + size);
        }
        Log.v("k9", "new attachment.size: " + size);
        this.cachedResultAttachment = this.sourceAttachment.deriveWithMetadataLoaded(usableContentType, name, size);
        return this.cachedResultAttachment;
    }
}
