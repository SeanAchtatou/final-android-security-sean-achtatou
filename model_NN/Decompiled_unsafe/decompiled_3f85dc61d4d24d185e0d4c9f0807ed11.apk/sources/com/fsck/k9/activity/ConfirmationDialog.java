package com.fsck.k9.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;

public class ConfirmationDialog {
    public static Dialog create(Activity activity, int dialogId, int title, String message, int confirmButton, int cancelButton, Runnable action) {
        return create(activity, dialogId, title, message, confirmButton, cancelButton, action, null);
    }

    public static Dialog create(final Activity activity, final int dialogId, int title, String message, int confirmButton, int cancelButton, final Runnable action, final Runnable negativeAction) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(confirmButton, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                activity.dismissDialog(dialogId);
                action.run();
            }
        });
        builder.setNegativeButton(cancelButton, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                activity.dismissDialog(dialogId);
                if (negativeAction != null) {
                    negativeAction.run();
                }
            }
        });
        return builder.create();
    }

    public static Dialog create(Activity activity, int dialogId, int title, int message, int confirmButton, int cancelButton, Runnable action) {
        return create(activity, dialogId, title, activity.getString(message), confirmButton, cancelButton, action, null);
    }
}
