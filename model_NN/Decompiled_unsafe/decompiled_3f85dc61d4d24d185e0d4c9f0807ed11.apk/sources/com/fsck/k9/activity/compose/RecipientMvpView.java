package com.fsck.k9.activity.compose;

import android.app.LoaderManager;
import android.app.PendingIntent;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Toast;
import android.widget.ViewAnimator;
import com.fsck.k9.FontSizes;
import com.fsck.k9.R;
import com.fsck.k9.activity.MessageCompose;
import com.fsck.k9.activity.compose.RecipientPresenter;
import com.fsck.k9.mail.Address;
import com.fsck.k9.mail.Message;
import com.fsck.k9.view.RecipientSelectView;
import java.util.Arrays;
import java.util.List;

public class RecipientMvpView implements View.OnFocusChangeListener, View.OnClickListener {
    private static final int VIEW_INDEX_BCC_EXPANDER_HIDDEN = 1;
    private static final int VIEW_INDEX_BCC_EXPANDER_VISIBLE = 0;
    private static final int VIEW_INDEX_CRYPTO_STATUS_DISABLED = 0;
    private static final int VIEW_INDEX_CRYPTO_STATUS_DISABLED_NO_KEY = 4;
    private static final int VIEW_INDEX_CRYPTO_STATUS_ERROR = 1;
    private static final int VIEW_INDEX_CRYPTO_STATUS_ERROR_NO_KEY = 3;
    private static final int VIEW_INDEX_CRYPTO_STATUS_NO_RECIPIENTS = 2;
    private static final int VIEW_INDEX_CRYPTO_STATUS_SIGN_ONLY = 7;
    private static final int VIEW_INDEX_CRYPTO_STATUS_TRUSTED = 6;
    private static final int VIEW_INDEX_CRYPTO_STATUS_UNTRUSTED = 5;
    private static final int VIEW_INDEX_HIDDEN = -1;
    private final MessageCompose activity;
    private final View bccDivider;
    private final RecipientSelectView bccView;
    private final View bccWrapper;
    private final View ccDivider;
    private final RecipientSelectView ccView;
    private final View ccWrapper;
    private final ViewAnimator cryptoStatusView;
    private final View pgpInlineIndicator;
    private RecipientPresenter presenter;
    private final ViewAnimator recipientExpanderContainer;
    private final RecipientSelectView toView;

    public RecipientMvpView(MessageCompose activity2) {
        this.activity = activity2;
        this.toView = (RecipientSelectView) activity2.findViewById(R.id.to);
        this.ccView = (RecipientSelectView) activity2.findViewById(R.id.cc);
        this.bccView = (RecipientSelectView) activity2.findViewById(R.id.bcc);
        this.ccWrapper = activity2.findViewById(R.id.cc_wrapper);
        this.ccDivider = activity2.findViewById(R.id.cc_divider);
        this.bccWrapper = activity2.findViewById(R.id.bcc_wrapper);
        this.bccDivider = activity2.findViewById(R.id.bcc_divider);
        this.recipientExpanderContainer = (ViewAnimator) activity2.findViewById(R.id.recipient_expander_container);
        this.cryptoStatusView = (ViewAnimator) activity2.findViewById(R.id.crypto_status);
        this.cryptoStatusView.setOnClickListener(this);
        this.pgpInlineIndicator = activity2.findViewById(R.id.pgp_inline_indicator);
        this.toView.setOnFocusChangeListener(this);
        this.ccView.setOnFocusChangeListener(this);
        this.bccView.setOnFocusChangeListener(this);
        activity2.findViewById(R.id.recipient_expander).setOnClickListener(this);
        View toLabel = activity2.findViewById(R.id.to_label);
        View ccLabel = activity2.findViewById(R.id.cc_label);
        View bccLabel = activity2.findViewById(R.id.bcc_label);
        toLabel.setOnClickListener(this);
        ccLabel.setOnClickListener(this);
        bccLabel.setOnClickListener(this);
        this.pgpInlineIndicator.setOnClickListener(this);
    }

    public void setPresenter(final RecipientPresenter presenter2) {
        this.presenter = presenter2;
        if (presenter2 == null) {
            this.toView.setTokenListener((RecipientSelectView.TokenListener<RecipientSelectView.Recipient>) null);
            this.ccView.setTokenListener((RecipientSelectView.TokenListener<RecipientSelectView.Recipient>) null);
            this.bccView.setTokenListener((RecipientSelectView.TokenListener<RecipientSelectView.Recipient>) null);
            return;
        }
        this.toView.setTokenListener((RecipientSelectView.TokenListener<RecipientSelectView.Recipient>) new RecipientSelectView.TokenListener<RecipientSelectView.Recipient>() {
            public void onTokenAdded(RecipientSelectView.Recipient recipient) {
                presenter2.onToTokenAdded(recipient);
            }

            public void onTokenRemoved(RecipientSelectView.Recipient recipient) {
                presenter2.onToTokenRemoved(recipient);
            }

            public void onTokenChanged(RecipientSelectView.Recipient recipient) {
                presenter2.onToTokenChanged(recipient);
            }
        });
        this.ccView.setTokenListener((RecipientSelectView.TokenListener<RecipientSelectView.Recipient>) new RecipientSelectView.TokenListener<RecipientSelectView.Recipient>() {
            public void onTokenAdded(RecipientSelectView.Recipient recipient) {
                presenter2.onCcTokenAdded(recipient);
            }

            public void onTokenRemoved(RecipientSelectView.Recipient recipient) {
                presenter2.onCcTokenRemoved(recipient);
            }

            public void onTokenChanged(RecipientSelectView.Recipient recipient) {
                presenter2.onCcTokenChanged(recipient);
            }
        });
        this.bccView.setTokenListener((RecipientSelectView.TokenListener<RecipientSelectView.Recipient>) new RecipientSelectView.TokenListener<RecipientSelectView.Recipient>() {
            public void onTokenAdded(RecipientSelectView.Recipient recipient) {
                presenter2.onBccTokenAdded(recipient);
            }

            public void onTokenRemoved(RecipientSelectView.Recipient recipient) {
                presenter2.onBccTokenRemoved(recipient);
            }

            public void onTokenChanged(RecipientSelectView.Recipient recipient) {
                presenter2.onBccTokenChanged(recipient);
            }
        });
    }

    public void addTextChangedListener(TextWatcher textWatcher) {
        this.toView.addTextChangedListener(textWatcher);
        this.ccView.addTextChangedListener(textWatcher);
        this.bccView.addTextChangedListener(textWatcher);
    }

    public void setCryptoProvider(String openPgpProvider) {
        this.toView.setCryptoProvider(openPgpProvider);
        this.ccView.setCryptoProvider(openPgpProvider);
        this.bccView.setCryptoProvider(openPgpProvider);
    }

    public void requestFocusOnToField() {
        this.toView.requestFocus();
    }

    public void requestFocusOnCcField() {
        this.ccView.requestFocus();
    }

    public void requestFocusOnBccField() {
        this.bccView.requestFocus();
    }

    public void setFontSizes(FontSizes fontSizes, int fontSize) {
        fontSizes.setViewTextSize(this.toView, fontSize);
        fontSizes.setViewTextSize(this.ccView, fontSize);
        fontSizes.setViewTextSize(this.bccView, fontSize);
    }

    public void addRecipients(Message.RecipientType recipientType, RecipientSelectView.Recipient... recipients) {
        switch (recipientType) {
            case TO:
                this.toView.addRecipients(recipients);
                return;
            case CC:
                this.ccView.addRecipients(recipients);
                return;
            case BCC:
                this.bccView.addRecipients(recipients);
                return;
            default:
                return;
        }
    }

    public void setCcVisibility(boolean visible) {
        int i;
        int i2 = 0;
        View view = this.ccWrapper;
        if (visible) {
            i = 0;
        } else {
            i = 8;
        }
        view.setVisibility(i);
        View view2 = this.ccDivider;
        if (!visible) {
            i2 = 8;
        }
        view2.setVisibility(i2);
    }

    public void setBccVisibility(boolean visible) {
        int i;
        int i2 = 0;
        View view = this.bccWrapper;
        if (visible) {
            i = 0;
        } else {
            i = 8;
        }
        view.setVisibility(i);
        View view2 = this.bccDivider;
        if (!visible) {
            i2 = 8;
        }
        view2.setVisibility(i2);
    }

    public void setRecipientExpanderVisibility(boolean visible) {
        int childToDisplay = visible ? 0 : 1;
        if (this.recipientExpanderContainer.getDisplayedChild() != childToDisplay) {
            this.recipientExpanderContainer.setDisplayedChild(childToDisplay);
        }
    }

    public boolean isCcVisible() {
        return this.ccWrapper.getVisibility() == 0;
    }

    public boolean isBccVisible() {
        return this.bccWrapper.getVisibility() == 0;
    }

    public void showNoRecipientsError() {
        this.toView.setError(this.toView.getContext().getString(R.string.message_compose_error_no_recipients));
    }

    public List<Address> getToAddresses() {
        return Arrays.asList(this.toView.getAddresses());
    }

    public List<Address> getCcAddresses() {
        return Arrays.asList(this.ccView.getAddresses());
    }

    public List<Address> getBccAddresses() {
        return Arrays.asList(this.bccView.getAddresses());
    }

    public List<RecipientSelectView.Recipient> getToRecipients() {
        return this.toView.getObjects();
    }

    public List<RecipientSelectView.Recipient> getCcRecipients() {
        return this.ccView.getObjects();
    }

    public List<RecipientSelectView.Recipient> getBccRecipients() {
        return this.bccView.getObjects();
    }

    public boolean recipientToHasUncompletedText() {
        return this.toView.hasUncompletedText();
    }

    public boolean recipientCcHasUncompletedText() {
        return this.ccView.hasUncompletedText();
    }

    public boolean recipientBccHasUncompletedText() {
        return this.bccView.hasUncompletedText();
    }

    public boolean recipientToTryPerformCompletion() {
        return this.toView.tryPerformCompletion();
    }

    public boolean recipientCcTryPerformCompletion() {
        return this.ccView.tryPerformCompletion();
    }

    public boolean recipientBccTryPerformCompletion() {
        return this.bccView.tryPerformCompletion();
    }

    public void showToUncompletedError() {
        this.toView.setError(this.toView.getContext().getString(R.string.compose_error_incomplete_recipient));
    }

    public void showCcUncompletedError() {
        this.ccView.setError(this.ccView.getContext().getString(R.string.compose_error_incomplete_recipient));
    }

    public void showBccUncompletedError() {
        this.bccView.setError(this.bccView.getContext().getString(R.string.compose_error_incomplete_recipient));
    }

    public void showPgpInlineModeIndicator(boolean pgpInlineModeEnabled) {
        this.pgpInlineIndicator.setVisibility(pgpInlineModeEnabled ? 0 : 8);
        this.activity.invalidateOptionsMenu();
    }

    public void showCryptoStatus(CryptoStatusDisplayType cryptoStatusDisplayType) {
        boolean shouldBeHidden;
        if (cryptoStatusDisplayType.childToDisplay == -1) {
            shouldBeHidden = true;
        } else {
            shouldBeHidden = false;
        }
        if (shouldBeHidden) {
            this.cryptoStatusView.setVisibility(8);
            return;
        }
        this.cryptoStatusView.setVisibility(0);
        this.cryptoStatusView.setDisplayedChild(cryptoStatusDisplayType.childToDisplay);
    }

    public void showContactPicker(int requestCode) {
        this.activity.showContactPicker(requestCode);
    }

    public void showErrorContactNoAddress() {
        Toast.makeText(this.activity, (int) R.string.error_contact_address_not_found, 1).show();
    }

    public void showErrorOpenPgpConnection() {
        Toast.makeText(this.activity, (int) R.string.error_crypto_provider_connect, 1).show();
    }

    public void showErrorOpenPgpUserInteractionRequired() {
        Toast.makeText(this.activity, (int) R.string.error_crypto_provider_ui_required, 1).show();
    }

    public void showErrorMissingSignKey() {
        Toast.makeText(this.activity, (int) R.string.compose_error_no_signing_key, 1).show();
    }

    public void showErrorPrivateButMissingKeys() {
        Toast.makeText(this.activity, (int) R.string.compose_error_private_missing_keys, 1).show();
    }

    public void showErrorAttachInline() {
        Toast.makeText(this.activity, (int) R.string.error_crypto_inline_attach, 1).show();
    }

    public void onFocusChange(View view, boolean hasFocus) {
        if (hasFocus) {
            switch (view.getId()) {
                case R.id.to:
                    this.presenter.onToFocused();
                    return;
                case R.id.cc:
                    this.presenter.onCcFocused();
                    return;
                case R.id.bcc:
                    this.presenter.onBccFocused();
                    return;
                default:
                    return;
            }
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.pgp_inline_indicator:
                this.presenter.onClickPgpInlineIndicator();
                return;
            case R.id.crypto_status:
                this.presenter.onClickCryptoStatus();
                return;
            case R.id.to_wrapper:
            case R.id.to:
            case R.id.recipient_expander_container:
            case R.id.cc_wrapper:
            case R.id.cc:
            case R.id.cc_divider:
            case R.id.bcc_wrapper:
            default:
                return;
            case R.id.to_label:
                this.presenter.onClickToLabel();
                return;
            case R.id.recipient_expander:
                this.presenter.onClickRecipientExpander();
                return;
            case R.id.cc_label:
                this.presenter.onClickCcLabel();
                return;
            case R.id.bcc_label:
                this.presenter.onClickBccLabel();
                return;
        }
    }

    public void showCryptoDialog(RecipientPresenter.CryptoMode currentCryptoMode) {
        CryptoSettingsDialog.newInstance(currentCryptoMode).show(this.activity.getFragmentManager(), "crypto_settings");
    }

    public void showOpenPgpInlineDialog(boolean firstTime) {
        PgpInlineDialog.newInstance(firstTime, R.id.pgp_inline_indicator).show(this.activity.getFragmentManager(), "openpgp_inline");
    }

    public void launchUserInteractionPendingIntent(PendingIntent pendingIntent, int requestCode) {
        this.activity.launchUserInteractionPendingIntent(pendingIntent, requestCode);
    }

    public void setLoaderManager(LoaderManager loaderManager) {
        this.toView.setLoaderManager(loaderManager);
        this.ccView.setLoaderManager(loaderManager);
        this.bccView.setLoaderManager(loaderManager);
    }

    public enum CryptoStatusDisplayType {
        UNCONFIGURED(-1),
        UNINITIALIZED(-1),
        DISABLED(0),
        SIGN_ONLY(7),
        OPPORTUNISTIC_EMPTY(2),
        OPPORTUNISTIC_NOKEY(4),
        OPPORTUNISTIC_UNTRUSTED(5),
        OPPORTUNISTIC_TRUSTED(6),
        PRIVATE_EMPTY(2),
        PRIVATE_NOKEY(3),
        PRIVATE_UNTRUSTED(5),
        PRIVATE_TRUSTED(6),
        ERROR(1);
        
        final int childToDisplay;

        private CryptoStatusDisplayType(int childToDisplay2) {
            this.childToDisplay = childToDisplay2;
        }
    }
}
