package com.fsck.k9.activity.setup;

import android.content.res.Resources;
import com.fsck.k9.R;
import com.fsck.k9.mail.AuthType;

class AuthTypeHolder {
    final AuthType authType;
    private boolean insecure;
    private final Resources resources;

    public AuthTypeHolder(AuthType authType2, Resources resources2) {
        this.authType = authType2;
        this.resources = resources2;
    }

    public void setInsecure(boolean insecure2) {
        this.insecure = insecure2;
    }

    public String toString() {
        int resourceId = resourceId();
        if (resourceId == 0) {
            return this.authType.name();
        }
        return this.resources.getString(resourceId);
    }

    private int resourceId() {
        switch (this.authType) {
            case PLAIN:
                if (this.insecure) {
                    return R.string.account_setup_auth_type_insecure_password;
                }
                return R.string.account_setup_auth_type_normal_password;
            case CRAM_MD5:
                return R.string.account_setup_auth_type_encrypted_password;
            case XOAUTH2:
                return R.string.account_setup_auth_type_xoauth2;
            case EXTERNAL:
                return R.string.account_setup_auth_type_tls_client_certificate;
            default:
                return 0;
        }
    }
}
