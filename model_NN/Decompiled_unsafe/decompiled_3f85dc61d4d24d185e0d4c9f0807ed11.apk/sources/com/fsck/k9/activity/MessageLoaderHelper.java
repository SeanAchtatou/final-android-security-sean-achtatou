package com.fsck.k9.activity;

import android.app.FragmentManager;
import android.app.LoaderManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.Loader;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import android.util.Log;
import com.fsck.k9.Account;
import com.fsck.k9.K9;
import com.fsck.k9.Preferences;
import com.fsck.k9.controller.MessagingController;
import com.fsck.k9.controller.MessagingListener;
import com.fsck.k9.helper.RetainFragment;
import com.fsck.k9.mail.Flag;
import com.fsck.k9.mailstore.LocalMessage;
import com.fsck.k9.mailstore.MessageViewInfo;
import com.fsck.k9.ui.crypto.MessageCryptoAnnotations;
import com.fsck.k9.ui.crypto.MessageCryptoCallback;
import com.fsck.k9.ui.crypto.MessageCryptoHelper;
import com.fsck.k9.ui.message.LocalMessageExtractorLoader;
import com.fsck.k9.ui.message.LocalMessageLoader;
import org.openintents.openpgp.OpenPgpDecryptionResult;

public class MessageLoaderHelper {
    private static final int DECODE_MESSAGE_LOADER_ID = 2;
    private static final int LOCAL_MESSAGE_LOADER_ID = 1;
    /* access modifiers changed from: private */
    public Account account;
    private OpenPgpDecryptionResult cachedDecryptionResult;
    /* access modifiers changed from: private */
    @Nullable
    public MessageLoaderCallbacks callback;
    /* access modifiers changed from: private */
    public Context context;
    private LoaderManager.LoaderCallbacks<MessageViewInfo> decodeMessageLoaderCallback = new LoaderManager.LoaderCallbacks<MessageViewInfo>() {
        public /* bridge */ /* synthetic */ void onLoadFinished(Loader loader, Object obj) {
            onLoadFinished((Loader<MessageViewInfo>) loader, (MessageViewInfo) obj);
        }

        public Loader<MessageViewInfo> onCreateLoader(int id, Bundle args) {
            if (id == 2) {
                return new LocalMessageExtractorLoader(MessageLoaderHelper.this.context, MessageLoaderHelper.this.localMessage, MessageLoaderHelper.this.messageCryptoAnnotations);
            }
            throw new IllegalStateException("loader id must be message decoder id");
        }

        public void onLoadFinished(Loader<MessageViewInfo> loader, MessageViewInfo messageViewInfo) {
            if (loader.getId() != 2) {
                throw new IllegalStateException("loader id must be message decoder id");
            }
            MessageLoaderHelper.this.onDecodeMessageFinished(messageViewInfo);
        }

        public void onLoaderReset(Loader<MessageViewInfo> loader) {
            if (loader.getId() != 2) {
                throw new IllegalStateException("loader id must be message decoder id");
            }
        }
    };
    MessagingListener downloadMessageListener = new MessagingListener() {
        public void loadMessageRemoteFinished(Account account, String folder, String uid) {
            if (MessageLoaderHelper.this.messageReference.equals(account.getUuid(), folder, uid)) {
                MessageLoaderHelper.this.onMessageDownloadFinished();
                if (K9.DEBUG) {
                    Log.e("AtomicGonza", "okAG Message Downloaded " + new Exception().getStackTrace()[0].toString());
                }
            }
        }

        public void loadMessageRemoteFailed(Account account, String folder, String uid, Throwable t) {
            MessageLoaderHelper.this.onDownloadMessageFailed(t);
        }
    };
    private FragmentManager fragmentManager;
    private LoaderManager loaderManager;
    /* access modifiers changed from: private */
    public LocalMessage localMessage;
    private LoaderManager.LoaderCallbacks<LocalMessage> localMessageLoaderCallback = new LoaderManager.LoaderCallbacks<LocalMessage>() {
        public /* bridge */ /* synthetic */ void onLoadFinished(Loader loader, Object obj) {
            onLoadFinished((Loader<LocalMessage>) loader, (LocalMessage) obj);
        }

        public Loader<LocalMessage> onCreateLoader(int id, Bundle args) {
            if (id == 1) {
                return new LocalMessageLoader(MessageLoaderHelper.this.context, MessagingController.getInstance(MessageLoaderHelper.this.context), MessageLoaderHelper.this.account, MessageLoaderHelper.this.messageReference);
            }
            throw new IllegalStateException("loader id must be message loader id");
        }

        public void onLoadFinished(Loader<LocalMessage> loader, LocalMessage message) {
            if (loader.getId() != 1) {
                throw new IllegalStateException("loader id must be message loader id");
            }
            LocalMessage unused = MessageLoaderHelper.this.localMessage = message;
            if (message == null) {
                MessageLoaderHelper.this.onLoadMessageFromDatabaseFailed();
            } else {
                MessageLoaderHelper.this.onLoadMessageFromDatabaseFinished();
            }
        }

        public void onLoaderReset(Loader<LocalMessage> loader) {
            if (loader.getId() != 1) {
                throw new IllegalStateException("loader id must be message loader id");
            }
        }
    };
    /* access modifiers changed from: private */
    public MessageCryptoAnnotations messageCryptoAnnotations;
    private MessageCryptoCallback messageCryptoCallback = new MessageCryptoCallback() {
        public void onCryptoHelperProgress(int current, int max) {
            if (MessageLoaderHelper.this.callback == null) {
                throw new IllegalStateException("unexpected call when callback is already detached");
            }
            MessageLoaderHelper.this.callback.setLoadingProgress(current, max);
        }

        public void onCryptoOperationsFinished(MessageCryptoAnnotations annotations) {
            if (MessageLoaderHelper.this.callback == null) {
                throw new IllegalStateException("unexpected call when callback is already detached");
            }
            MessageCryptoAnnotations unused = MessageLoaderHelper.this.messageCryptoAnnotations = annotations;
            MessageLoaderHelper.this.startOrResumeDecodeMessage();
        }

        public void startPendingIntentForCryptoHelper(IntentSender si, int requestCode, Intent fillIntent, int flagsMask, int flagValues, int extraFlags) {
            if (MessageLoaderHelper.this.callback == null) {
                throw new IllegalStateException("unexpected call when callback is already detached");
            }
            MessageLoaderHelper.this.callback.startIntentSenderForMessageLoaderHelper(si, requestCode, fillIntent, flagsMask, flagValues, extraFlags);
        }
    };
    private MessageCryptoHelper messageCryptoHelper;
    /* access modifiers changed from: private */
    public MessageReference messageReference;

    public interface MessageLoaderCallbacks {
        void onDownloadErrorMessageNotFound();

        void onDownloadErrorNetworkError();

        void onMessageDataLoadFailed();

        void onMessageDataLoadFinished(LocalMessage localMessage);

        void onMessageViewInfoLoadFailed(MessageViewInfo messageViewInfo);

        void onMessageViewInfoLoadFinished(MessageViewInfo messageViewInfo);

        void setLoadingProgress(int i, int i2);

        void startIntentSenderForMessageLoaderHelper(IntentSender intentSender, int i, Intent intent, int i2, int i3, int i4);
    }

    public MessageLoaderHelper(Context context2, LoaderManager loaderManager2, FragmentManager fragmentManager2, @NonNull MessageLoaderCallbacks callback2) {
        this.context = context2;
        this.loaderManager = loaderManager2;
        this.fragmentManager = fragmentManager2;
        this.callback = callback2;
    }

    @UiThread
    public void asyncStartOrResumeLoadingMessage(MessageReference messageReference2, Parcelable cachedDecryptionResult2) {
        this.messageReference = messageReference2;
        this.account = Preferences.getPreferences(this.context).getAccount(messageReference2.getAccountUuid());
        if (cachedDecryptionResult2 != null) {
            if (cachedDecryptionResult2 instanceof OpenPgpDecryptionResult) {
                this.cachedDecryptionResult = (OpenPgpDecryptionResult) cachedDecryptionResult2;
            } else {
                Log.e("k9", "Got decryption result of unknown type - ignoring");
            }
        }
        startOrResumeLocalMessageLoader();
    }

    @UiThread
    public void asyncReloadMessage() {
        startOrResumeLocalMessageLoader();
    }

    @UiThread
    public void asyncRestartMessageCryptoProcessing() {
        cancelAndClearCryptoOperation();
        cancelAndClearDecodeLoader();
        startOrResumeCryptoOperation();
    }

    @UiThread
    public void onDestroy() {
        if (this.messageCryptoHelper != null) {
            this.messageCryptoHelper.cancelIfRunning();
        }
        this.callback = null;
        this.context = null;
        this.fragmentManager = null;
        this.loaderManager = null;
    }

    @UiThread
    public void onDestroyChangingConfigurations() {
        if (this.messageCryptoHelper != null) {
            this.messageCryptoHelper.detachCallback();
        }
        this.callback = null;
        this.context = null;
        this.fragmentManager = null;
        this.loaderManager = null;
    }

    @UiThread
    public void downloadCompleteMessage() {
        startDownloadingMessageBody(true);
    }

    @UiThread
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        this.messageCryptoHelper.onActivityResult(requestCode, resultCode, data);
    }

    private void startOrResumeLocalMessageLoader() {
        LocalMessageLoader loader = (LocalMessageLoader) this.loaderManager.getLoader(1);
        if (loader == null || !loader.isCreatedFor(this.messageReference)) {
            Log.d("k9", "Creating new local message loader");
            cancelAndClearCryptoOperation();
            cancelAndClearDecodeLoader();
            this.loaderManager.restartLoader(1, null, this.localMessageLoaderCallback);
            return;
        }
        Log.d("k9", "Reusing local message loader");
        this.loaderManager.initLoader(1, null, this.localMessageLoaderCallback);
    }

    /* access modifiers changed from: private */
    @UiThread
    public void onLoadMessageFromDatabaseFinished() {
        boolean messageIncomplete;
        if (this.callback == null) {
            throw new IllegalStateException("unexpected call when callback is already detached");
        }
        this.callback.onMessageDataLoadFinished(this.localMessage);
        if (this.localMessage.isSet(Flag.X_DOWNLOADED_FULL) || this.localMessage.isSet(Flag.X_DOWNLOADED_PARTIAL)) {
            messageIncomplete = false;
        } else {
            messageIncomplete = true;
        }
        if (messageIncomplete) {
            startDownloadingMessageBody(false);
        } else if (this.account.isOpenPgpProviderConfigured()) {
            startOrResumeCryptoOperation();
        } else {
            startOrResumeDecodeMessage();
        }
    }

    /* access modifiers changed from: private */
    public void onLoadMessageFromDatabaseFailed() {
        if (this.callback == null) {
            throw new IllegalStateException("unexpected call when callback is already detached");
        }
        this.callback.onMessageDataLoadFailed();
    }

    private void cancelAndClearLocalMessageLoader() {
        this.loaderManager.destroyLoader(1);
    }

    private void startOrResumeCryptoOperation() {
        RetainFragment<MessageCryptoHelper> retainCryptoHelperFragment = getMessageCryptoHelperRetainFragment();
        if (retainCryptoHelperFragment.hasData()) {
            this.messageCryptoHelper = retainCryptoHelperFragment.getData();
        } else {
            this.messageCryptoHelper = new MessageCryptoHelper(this.context, this.account.getOpenPgpProvider());
            retainCryptoHelperFragment.setData(this.messageCryptoHelper);
        }
        this.messageCryptoHelper.asyncStartOrResumeProcessingMessage(this.localMessage, this.messageCryptoCallback, this.cachedDecryptionResult);
    }

    private void cancelAndClearCryptoOperation() {
        RetainFragment<MessageCryptoHelper> retainCryptoHelperFragment = getMessageCryptoHelperRetainFragment();
        if (retainCryptoHelperFragment != null) {
            if (retainCryptoHelperFragment.hasData()) {
                this.messageCryptoHelper = retainCryptoHelperFragment.getData();
                this.messageCryptoHelper.cancelIfRunning();
                this.messageCryptoHelper = null;
            }
            retainCryptoHelperFragment.clearAndRemove(this.fragmentManager);
        }
    }

    private RetainFragment<MessageCryptoHelper> getMessageCryptoHelperRetainFragment() {
        return RetainFragment.findOrCreate(this.fragmentManager, "crypto_helper_" + this.messageReference.hashCode());
    }

    /* access modifiers changed from: private */
    public void startOrResumeDecodeMessage() {
        LocalMessageExtractorLoader loader = (LocalMessageExtractorLoader) this.loaderManager.getLoader(2);
        if (loader == null || !loader.isCreatedFor(this.localMessage, this.messageCryptoAnnotations)) {
            Log.d("k9", "Creating new decode message loader");
            this.loaderManager.restartLoader(2, null, this.decodeMessageLoaderCallback);
            return;
        }
        Log.d("k9", "Reusing decode message loader");
        this.loaderManager.initLoader(2, null, this.decodeMessageLoaderCallback);
    }

    /* access modifiers changed from: private */
    public void onDecodeMessageFinished(MessageViewInfo messageViewInfo) {
        if (this.callback == null) {
            throw new IllegalStateException("unexpected call when callback is already detached");
        } else if (messageViewInfo == null) {
            this.callback.onMessageViewInfoLoadFailed(createErrorStateMessageViewInfo());
        } else {
            this.callback.onMessageViewInfoLoadFinished(messageViewInfo);
        }
    }

    @NonNull
    private MessageViewInfo createErrorStateMessageViewInfo() {
        return MessageViewInfo.createWithErrorState(this.localMessage, !this.localMessage.isSet(Flag.X_DOWNLOADED_FULL));
    }

    private void cancelAndClearDecodeLoader() {
        this.loaderManager.destroyLoader(2);
    }

    private void startDownloadingMessageBody(boolean downloadComplete) {
        if (K9.DEBUG) {
            Log.e("AtomicGonza", "okAG START DOWNLOADING BODY  " + (downloadComplete ? "Complete " : "PArtial ") + new Exception().getStackTrace()[0].toString());
        }
        if (downloadComplete) {
            MessagingController.getInstance(this.context).loadMessageRemote(this.account, this.messageReference.getFolderName(), this.messageReference.getUid(), this.downloadMessageListener);
        } else {
            MessagingController.getInstance(this.context).loadMessageRemotePartial(this.account, this.messageReference.getFolderName(), this.messageReference.getUid(), this.downloadMessageListener);
        }
    }

    /* access modifiers changed from: private */
    public void onMessageDownloadFinished() {
        if (this.callback != null) {
            cancelAndClearLocalMessageLoader();
            cancelAndClearDecodeLoader();
            cancelAndClearCryptoOperation();
            startOrResumeLocalMessageLoader();
        }
    }

    /* access modifiers changed from: private */
    public void onDownloadMessageFailed(Throwable t) {
        if (this.callback != null) {
            if (t instanceof IllegalArgumentException) {
                this.callback.onDownloadErrorMessageNotFound();
            } else {
                this.callback.onDownloadErrorNetworkError();
            }
        }
    }
}
