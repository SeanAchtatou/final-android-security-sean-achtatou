package com.fsck.k9.activity.setup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.fsck.k9.Account;
import com.fsck.k9.Preferences;
import com.fsck.k9.R;
import com.fsck.k9.account.AccountCreator;
import com.fsck.k9.activity.K9Activity;
import com.fsck.k9.activity.setup.AccountSetupCheckSettings;
import com.fsck.k9.helper.Utility;
import com.fsck.k9.mail.AuthType;
import com.fsck.k9.mail.ConnectionSecurity;
import com.fsck.k9.mail.ServerSettings;
import com.fsck.k9.mail.Transport;
import com.fsck.k9.view.ClientCertificateSpinner;
import java.net.URI;
import java.net.URISyntaxException;

public class AccountSetupOutgoing extends K9Activity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    private static final String EXTRA_ACCOUNT = "account";
    private static final String EXTRA_MAKE_DEFAULT = "makeDefault";
    private static final String STATE_AUTH_TYPE_POSITION = "authTypePosition";
    private static final String STATE_SECURITY_TYPE_POSITION = "stateSecurityTypePosition";
    ClientCertificateSpinner.OnClientCertificateChangedListener clientCertificateChangedListener = new ClientCertificateSpinner.OnClientCertificateChangedListener() {
        public void onClientCertificateChanged(String alias) {
            AccountSetupOutgoing.this.validateFields();
        }
    };
    private boolean isNewAccount = false;
    private Account mAccount;
    /* access modifiers changed from: private */
    public AuthTypeAdapter mAuthTypeAdapter;
    /* access modifiers changed from: private */
    public Spinner mAuthTypeView;
    private TextView mClientCertificateLabelView;
    /* access modifiers changed from: private */
    public ClientCertificateSpinner mClientCertificateSpinner;
    /* access modifiers changed from: private */
    public int mCurrentAuthTypeViewPosition;
    private String mCurrentPortViewSetting;
    /* access modifiers changed from: private */
    public int mCurrentSecurityTypeViewPosition;
    private boolean mMakeDefault;
    private Button mNextButton;
    private TextView mPasswordLabelView;
    /* access modifiers changed from: private */
    public EditText mPasswordView;
    private EditText mPortView;
    private ViewGroup mRequireLoginSettingsView;
    /* access modifiers changed from: private */
    public CheckBox mRequireLoginView;
    private Spinner mSecurityTypeView;
    private EditText mServerView;
    private EditText mUsernameView;
    TextWatcher validationTextWatcher = new TextWatcher() {
        public void afterTextChanged(Editable s) {
            AccountSetupOutgoing.this.validateFields();
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    };

    public static void actionOutgoingSettings(Context context, Account account, boolean makeDefault, boolean isNewAccount2) {
        Intent i = new Intent(context, AccountSetupOutgoing.class);
        i.putExtra("account", account.getUuid());
        i.putExtra(EXTRA_MAKE_DEFAULT, makeDefault);
        i.putExtra(AccountSetupCheckSettings.EXTRA_NEW_ACCOUNT, isNewAccount2);
        context.startActivity(i);
    }

    public static void actionEditOutgoingSettings(Context context, Account account) {
        context.startActivity(intentActionEditOutgoingSettings(context, account));
    }

    public static Intent intentActionEditOutgoingSettings(Context context, Account account) {
        Intent i = new Intent(context, AccountSetupOutgoing.class);
        i.setAction("android.intent.action.EDIT");
        i.putExtra("account", account.getUuid());
        return i;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.account_setup_outgoing);
        this.mAccount = Preferences.getPreferences(this).getAccount(getIntent().getStringExtra("account"));
        if (savedInstanceState != null) {
            this.isNewAccount = savedInstanceState.getBoolean(AccountSetupCheckSettings.EXTRA_NEW_ACCOUNT, false);
        } else {
            Intent i = getIntent();
            if (i != null) {
                this.isNewAccount = i.getBooleanExtra(AccountSetupCheckSettings.EXTRA_NEW_ACCOUNT, false);
            }
        }
        try {
            if (new URI(this.mAccount.getStoreUri()).getScheme().startsWith("webdav")) {
                this.mAccount.setTransportUri(this.mAccount.getStoreUri());
                AccountSetupCheckSettings.actionCheckSettings(this, this.mAccount, AccountSetupCheckSettings.CheckDirection.OUTGOING, this.isNewAccount);
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        this.mUsernameView = (EditText) findViewById(R.id.account_username);
        this.mPasswordView = (EditText) findViewById(R.id.account_password);
        this.mClientCertificateSpinner = (ClientCertificateSpinner) findViewById(R.id.account_client_certificate_spinner);
        this.mClientCertificateLabelView = (TextView) findViewById(R.id.account_client_certificate_label);
        this.mPasswordLabelView = (TextView) findViewById(R.id.account_password_label);
        this.mServerView = (EditText) findViewById(R.id.account_server);
        this.mPortView = (EditText) findViewById(R.id.account_port);
        this.mRequireLoginView = (CheckBox) findViewById(R.id.account_require_login);
        this.mRequireLoginSettingsView = (ViewGroup) findViewById(R.id.account_require_login_settings);
        this.mSecurityTypeView = (Spinner) findViewById(R.id.account_security_type);
        this.mAuthTypeView = (Spinner) findViewById(R.id.account_auth_type);
        this.mNextButton = (Button) findViewById(R.id.next);
        this.mNextButton.setOnClickListener(this);
        this.mSecurityTypeView.setAdapter((SpinnerAdapter) ConnectionSecurityAdapter.get(this));
        this.mAuthTypeAdapter = AuthTypeAdapter.get(this);
        this.mAuthTypeView.setAdapter((SpinnerAdapter) this.mAuthTypeAdapter);
        this.mPortView.setKeyListener(DigitsKeyListener.getInstance("0123456789"));
        this.mAccount = Preferences.getPreferences(this).getAccount(getIntent().getStringExtra("account"));
        this.mMakeDefault = getIntent().getBooleanExtra(EXTRA_MAKE_DEFAULT, false);
        if (savedInstanceState != null && savedInstanceState.containsKey("account")) {
            this.mAccount = Preferences.getPreferences(this).getAccount(savedInstanceState.getString("account"));
        }
        try {
            ServerSettings settings = Transport.decodeTransportUri(this.mAccount.getTransportUri());
            updateAuthPlainTextFromSecurityType(settings.connectionSecurity);
            if (savedInstanceState == null) {
                this.mCurrentAuthTypeViewPosition = this.mAuthTypeAdapter.getAuthPosition(settings.authenticationType);
            } else {
                this.mCurrentAuthTypeViewPosition = savedInstanceState.getInt(STATE_AUTH_TYPE_POSITION);
            }
            this.mAuthTypeView.setSelection(this.mCurrentAuthTypeViewPosition, false);
            updateViewFromAuthType();
            if (savedInstanceState == null) {
                this.mCurrentSecurityTypeViewPosition = settings.connectionSecurity.ordinal();
            } else {
                this.mCurrentSecurityTypeViewPosition = savedInstanceState.getInt(STATE_SECURITY_TYPE_POSITION);
            }
            this.mSecurityTypeView.setSelection(this.mCurrentSecurityTypeViewPosition, false);
            if (settings.username != null && !settings.username.isEmpty()) {
                this.mUsernameView.setText(settings.username);
                this.mRequireLoginView.setChecked(true);
                this.mRequireLoginSettingsView.setVisibility(0);
            }
            if (settings.password != null) {
                this.mPasswordView.setText(settings.password);
            }
            if (settings.clientCertificateAlias != null) {
                this.mClientCertificateSpinner.setAlias(settings.clientCertificateAlias);
            }
            if (settings.host != null) {
                this.mServerView.setText(settings.host);
            }
            if (settings.port != -1) {
                this.mPortView.setText(String.format("%d", Integer.valueOf(settings.port)));
            } else {
                updatePortFromSecurityType();
            }
            this.mCurrentPortViewSetting = this.mPortView.getText().toString();
        } catch (Exception e2) {
            failure(e2);
        }
    }

    private void initializeViewListeners() {
        this.mSecurityTypeView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                boolean isAuthExternal;
                boolean loginNotRequired;
                if (AccountSetupOutgoing.this.mCurrentSecurityTypeViewPosition != position) {
                    AccountSetupOutgoing.this.updatePortFromSecurityType();
                    boolean isInsecure = ConnectionSecurity.NONE == AccountSetupOutgoing.this.getSelectedSecurity();
                    if (AuthType.EXTERNAL == AccountSetupOutgoing.this.getSelectedAuthType()) {
                        isAuthExternal = true;
                    } else {
                        isAuthExternal = false;
                    }
                    if (!AccountSetupOutgoing.this.mRequireLoginView.isChecked()) {
                        loginNotRequired = true;
                    } else {
                        loginNotRequired = false;
                    }
                    if (isInsecure && isAuthExternal && loginNotRequired) {
                        AdapterView.OnItemSelectedListener onItemSelectedListener = AccountSetupOutgoing.this.mAuthTypeView.getOnItemSelectedListener();
                        AccountSetupOutgoing.this.mAuthTypeView.setOnItemSelectedListener(null);
                        int unused = AccountSetupOutgoing.this.mCurrentAuthTypeViewPosition = AccountSetupOutgoing.this.mAuthTypeAdapter.getAuthPosition(AuthType.PLAIN);
                        AccountSetupOutgoing.this.mAuthTypeView.setSelection(AccountSetupOutgoing.this.mCurrentAuthTypeViewPosition, false);
                        AccountSetupOutgoing.this.mAuthTypeView.setOnItemSelectedListener(onItemSelectedListener);
                        AccountSetupOutgoing.this.updateViewFromAuthType();
                    }
                    AccountSetupOutgoing.this.validateFields();
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.mAuthTypeView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (AccountSetupOutgoing.this.mCurrentAuthTypeViewPosition != position) {
                    AccountSetupOutgoing.this.updateViewFromAuthType();
                    AccountSetupOutgoing.this.validateFields();
                    if (AuthType.EXTERNAL == AccountSetupOutgoing.this.getSelectedAuthType()) {
                        AccountSetupOutgoing.this.mClientCertificateSpinner.chooseCertificate();
                    } else {
                        AccountSetupOutgoing.this.mPasswordView.requestFocus();
                    }
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.mRequireLoginView.setOnCheckedChangeListener(this);
        this.mClientCertificateSpinner.setOnClientCertificateChangedListener(this.clientCertificateChangedListener);
        this.mUsernameView.addTextChangedListener(this.validationTextWatcher);
        this.mPasswordView.addTextChangedListener(this.validationTextWatcher);
        this.mServerView.addTextChangedListener(this.validationTextWatcher);
        this.mPortView.addTextChangedListener(this.validationTextWatcher);
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(AccountSetupCheckSettings.EXTRA_NEW_ACCOUNT, this.isNewAccount);
        outState.putString("account", this.mAccount.getUuid());
        outState.putInt(STATE_SECURITY_TYPE_POSITION, this.mCurrentSecurityTypeViewPosition);
        outState.putInt(STATE_AUTH_TYPE_POSITION, this.mCurrentAuthTypeViewPosition);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (this.mRequireLoginView.isChecked()) {
            this.mRequireLoginSettingsView.setVisibility(0);
        } else {
            this.mRequireLoginSettingsView.setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        initializeViewListeners();
        validateFields();
    }

    /* access modifiers changed from: private */
    public void updateViewFromAuthType() {
        boolean isAuthTypeExternal;
        boolean isAuthTypeXOAuth2;
        AuthType authType = getSelectedAuthType();
        if (AuthType.EXTERNAL == authType) {
            isAuthTypeExternal = true;
        } else {
            isAuthTypeExternal = false;
        }
        if (AuthType.XOAUTH2 == authType) {
            isAuthTypeXOAuth2 = true;
        } else {
            isAuthTypeXOAuth2 = false;
        }
        if (isAuthTypeExternal) {
            this.mPasswordView.setVisibility(8);
            this.mPasswordLabelView.setVisibility(8);
            this.mClientCertificateLabelView.setVisibility(0);
            this.mClientCertificateSpinner.setVisibility(0);
        } else if (isAuthTypeXOAuth2) {
            this.mPasswordView.setVisibility(8);
            this.mPasswordLabelView.setVisibility(8);
            this.mClientCertificateLabelView.setVisibility(8);
            this.mClientCertificateSpinner.setVisibility(8);
        } else {
            this.mPasswordView.setVisibility(0);
            this.mPasswordLabelView.setVisibility(0);
            this.mClientCertificateLabelView.setVisibility(8);
            this.mClientCertificateSpinner.setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public void validateFields() {
        AuthType authType = getSelectedAuthType();
        boolean isAuthTypeExternal = AuthType.EXTERNAL == authType;
        boolean isAuthTypeXOAuth2 = AuthType.XOAUTH2 == authType;
        boolean hasConnectionSecurity = getSelectedSecurity() != ConnectionSecurity.NONE;
        if (!isAuthTypeExternal || hasConnectionSecurity) {
            this.mCurrentAuthTypeViewPosition = this.mAuthTypeView.getSelectedItemPosition();
            this.mCurrentSecurityTypeViewPosition = this.mSecurityTypeView.getSelectedItemPosition();
            this.mCurrentPortViewSetting = this.mPortView.getText().toString();
        } else {
            Toast.makeText(this, getString(R.string.account_setup_outgoing_invalid_setting_combo_notice, new Object[]{getString(R.string.account_setup_incoming_auth_type_label), AuthType.EXTERNAL.toString(), getString(R.string.account_setup_incoming_security_label), ConnectionSecurity.NONE.toString()}), 1).show();
            AdapterView.OnItemSelectedListener onItemSelectedListener = this.mAuthTypeView.getOnItemSelectedListener();
            this.mAuthTypeView.setOnItemSelectedListener(null);
            this.mAuthTypeView.setSelection(this.mCurrentAuthTypeViewPosition, false);
            this.mAuthTypeView.setOnItemSelectedListener(onItemSelectedListener);
            updateViewFromAuthType();
            AdapterView.OnItemSelectedListener onItemSelectedListener2 = this.mSecurityTypeView.getOnItemSelectedListener();
            this.mSecurityTypeView.setOnItemSelectedListener(null);
            this.mSecurityTypeView.setSelection(this.mCurrentSecurityTypeViewPosition, false);
            this.mSecurityTypeView.setOnItemSelectedListener(onItemSelectedListener2);
            updateAuthPlainTextFromSecurityType(getSelectedSecurity());
            this.mPortView.removeTextChangedListener(this.validationTextWatcher);
            this.mPortView.setText(this.mCurrentPortViewSetting);
            this.mPortView.addTextChangedListener(this.validationTextWatcher);
            isAuthTypeExternal = AuthType.EXTERNAL == getSelectedAuthType();
            hasConnectionSecurity = getSelectedSecurity() != ConnectionSecurity.NONE;
        }
        boolean hasValidCertificateAlias = this.mClientCertificateSpinner.getAlias() != null;
        boolean hasValidUserName = Utility.requiredFieldValid(this.mUsernameView);
        this.mNextButton.setEnabled(Utility.domainFieldValid(this.mServerView) && Utility.requiredFieldValid(this.mPortView) && (!this.mRequireLoginView.isChecked() || (hasValidUserName && !isAuthTypeExternal && Utility.requiredFieldValid(this.mPasswordView)) || (hasValidUserName && isAuthTypeExternal && hasConnectionSecurity && hasValidCertificateAlias) || (hasValidUserName && isAuthTypeXOAuth2)));
        Utility.setCompoundDrawablesAlpha(this.mNextButton, this.mNextButton.isEnabled() ? 255 : 128);
    }

    /* access modifiers changed from: private */
    public void updatePortFromSecurityType() {
        ConnectionSecurity securityType = getSelectedSecurity();
        updateAuthPlainTextFromSecurityType(securityType);
        this.mPortView.removeTextChangedListener(this.validationTextWatcher);
        this.mPortView.setText(String.valueOf(AccountCreator.getDefaultPort(securityType, ServerSettings.Type.SMTP)));
        this.mPortView.addTextChangedListener(this.validationTextWatcher);
    }

    private void updateAuthPlainTextFromSecurityType(ConnectionSecurity securityType) {
        this.mAuthTypeAdapter.useInsecureText(securityType == ConnectionSecurity.NONE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == -1) {
            if ("android.intent.action.EDIT".equals(getIntent().getAction())) {
                this.mAccount.save(Preferences.getPreferences(this));
                finish();
                return;
            }
            AccountSetupOptions.actionOptions(this, this.mAccount, this.mMakeDefault);
            finish();
        } else if (resultCode == -11) {
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onNext() {
        ConnectionSecurity securityType = getSelectedSecurity();
        String username = null;
        String password = null;
        String clientCertificateAlias = null;
        AuthType authType = null;
        if (this.mRequireLoginView.isChecked()) {
            username = this.mUsernameView.getText().toString();
            authType = getSelectedAuthType();
            if (AuthType.EXTERNAL == authType) {
                clientCertificateAlias = this.mClientCertificateSpinner.getAlias();
            } else {
                password = this.mPasswordView.getText().toString();
            }
        }
        String newHost = this.mServerView.getText().toString();
        int newPort = Integer.parseInt(this.mPortView.getText().toString());
        String uri = Transport.createTransportUri(new ServerSettings(ServerSettings.Type.SMTP, newHost, newPort, securityType, authType, username, password, clientCertificateAlias));
        this.mAccount.deleteCertificate(newHost, newPort, AccountSetupCheckSettings.CheckDirection.OUTGOING);
        this.mAccount.setTransportUri(uri);
        AccountSetupCheckSettings.actionCheckSettings(this, this.mAccount, AccountSetupCheckSettings.CheckDirection.OUTGOING, this.isNewAccount);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.next:
                onNext();
                return;
            default:
                return;
        }
    }

    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        this.mRequireLoginSettingsView.setVisibility(isChecked ? 0 : 8);
        validateFields();
    }

    private void failure(Exception use) {
        Log.e("k9", "Failure", use);
        Toast.makeText(getApplication(), getString(R.string.account_setup_bad_uri, new Object[]{use.getMessage()}), 1).show();
    }

    /* access modifiers changed from: private */
    public AuthType getSelectedAuthType() {
        return ((AuthTypeHolder) this.mAuthTypeView.getSelectedItem()).authType;
    }

    /* access modifiers changed from: private */
    public ConnectionSecurity getSelectedSecurity() {
        return ((ConnectionSecurityHolder) this.mSecurityTypeView.getSelectedItem()).connectionSecurity;
    }
}
