package com.fsck.k9.activity;

import android.content.Intent;
import android.os.Bundle;
import com.fsck.k9.Account;
import com.fsck.k9.BaseAccount;
import com.fsck.k9.R;
import com.fsck.k9.search.SearchAccount;

public class LauncherShortcuts extends AccountList {
    public void onCreate(Bundle icicle) {
        if (!"android.intent.action.CREATE_SHORTCUT".equals(getIntent().getAction())) {
            finish();
        } else {
            super.onCreate(icicle);
        }
    }

    /* access modifiers changed from: protected */
    public boolean displaySpecialAccounts() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void onAccountSelected(BaseAccount account) {
        Intent shortcutIntent;
        if (account instanceof SearchAccount) {
            shortcutIntent = MessageList.shortcutIntent(this, ((SearchAccount) account).getId());
        } else {
            shortcutIntent = FolderList.actionHandleAccountIntent(this, (Account) account, true);
        }
        Intent intent = new Intent();
        intent.putExtra("android.intent.extra.shortcut.INTENT", shortcutIntent);
        String description = account.getDescription();
        if (description == null || description.isEmpty()) {
            description = account.getEmail();
        }
        intent.putExtra("android.intent.extra.shortcut.NAME", description);
        intent.putExtra("android.intent.extra.shortcut.ICON_RESOURCE", Intent.ShortcutIconResource.fromContext(this, R.drawable.icon));
        setResult(-1, intent);
        finish();
    }
}
