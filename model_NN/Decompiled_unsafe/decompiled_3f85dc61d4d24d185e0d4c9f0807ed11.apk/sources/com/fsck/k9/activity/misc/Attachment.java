package com.fsck.k9.activity.misc;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

public class Attachment implements Parcelable {
    public static final Parcelable.Creator<Attachment> CREATOR = new Parcelable.Creator<Attachment>() {
        public Attachment createFromParcel(Parcel in) {
            return new Attachment(in);
        }

        public Attachment[] newArray(int size) {
            return new Attachment[size];
        }
    };
    public final String contentType;
    public final String filename;
    public final int loaderId;
    public final String name;
    public final Long size;
    public final LoadingState state;
    public final Uri uri;

    public enum LoadingState {
        URI_ONLY,
        METADATA,
        COMPLETE,
        CANCELLED
    }

    private Attachment(Uri uri2, LoadingState state2, int loaderId2, String contentType2, String name2, Long size2, String filename2) {
        this.uri = uri2;
        this.state = state2;
        this.loaderId = loaderId2;
        this.contentType = contentType2;
        this.name = name2;
        this.size = size2;
        this.filename = filename2;
    }

    private Attachment(Parcel in) {
        this.uri = (Uri) in.readParcelable(Uri.class.getClassLoader());
        this.state = (LoadingState) in.readSerializable();
        this.loaderId = in.readInt();
        this.contentType = in.readString();
        this.name = in.readString();
        if (in.readInt() != 0) {
            this.size = Long.valueOf(in.readLong());
        } else {
            this.size = null;
        }
        this.filename = in.readString();
    }

    public static Attachment createAttachment(Uri uri2, int loaderId2, String contentType2) {
        return new Attachment(uri2, LoadingState.URI_ONLY, loaderId2, contentType2, null, null, null);
    }

    public Attachment deriveWithMetadataLoaded(String usableContentType, String name2, long size2) {
        if (this.state != LoadingState.URI_ONLY) {
            throw new IllegalStateException("deriveWithMetadataLoaded can only be called on a URI_ONLY attachment!");
        }
        return new Attachment(this.uri, LoadingState.METADATA, this.loaderId, usableContentType, name2, Long.valueOf(size2), null);
    }

    public Attachment deriveWithLoadCancelled() {
        if (this.state == LoadingState.METADATA) {
            return new Attachment(this.uri, LoadingState.CANCELLED, this.loaderId, this.contentType, this.name, this.size, null);
        }
        throw new IllegalStateException("deriveWitLoadCancelled can only be called on a METADATA attachment!");
    }

    public Attachment deriveWithLoadComplete(String absolutePath) {
        if (this.state == LoadingState.METADATA) {
            return new Attachment(this.uri, LoadingState.COMPLETE, this.loaderId, this.contentType, this.name, this.size, absolutePath);
        }
        throw new IllegalStateException("deriveWithLoadComplete can only be called on a METADATA attachment!");
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.uri, flags);
        dest.writeSerializable(this.state);
        dest.writeInt(this.loaderId);
        dest.writeString(this.contentType);
        dest.writeString(this.name);
        if (this.size != null) {
            dest.writeInt(1);
            dest.writeLong(this.size.longValue());
        } else {
            dest.writeInt(0);
        }
        dest.writeString(this.filename);
    }
}
