package com.fsck.k9.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fsck.k9.R;
import com.larswerkman.colorpicker.ColorPicker;

public class ColorPickerDialog extends AlertDialog {
    OnColorChangedListener mColorChangedListener;
    ColorPicker mColorPicker;

    public interface OnColorChangedListener {
        void colorChanged(int i);
    }

    public ColorPickerDialog(Context context, OnColorChangedListener listener, int color) {
        super(context);
        this.mColorChangedListener = listener;
        View view = LayoutInflater.from(context).inflate((int) R.layout.color_picker_dialog, (ViewGroup) null);
        this.mColorPicker = (ColorPicker) view.findViewById(R.id.color_picker);
        this.mColorPicker.setColor(color);
        setView(view);
        setButton(-1, context.getString(R.string.okay_action), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (ColorPickerDialog.this.mColorChangedListener != null) {
                    ColorPickerDialog.this.mColorChangedListener.colorChanged(ColorPickerDialog.this.mColorPicker.getColor());
                }
            }
        });
        setButton(-2, context.getString(R.string.cancel_action), (DialogInterface.OnClickListener) null);
    }

    public void setColor(int color) {
        this.mColorPicker.setColor(color);
    }
}
