package com.fsck.k9.activity.misc;

import android.app.ActivityManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.util.LruCache;
import android.text.TextUtils;
import android.widget.ImageView;
import atomicgonza.RoundedQuickContactBadge;
import com.fsck.k9.helper.Contacts;
import com.fsck.k9.mail.Address;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.Locale;
import java.util.concurrent.RejectedExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ContactPictureLoader {
    private static final int[] CONTACT_DUMMY_COLORS_ARGB = {-13388315, -5609780, -6697984, -17613, -48060, -16737844, -6736948, -10053376, -30720, -3407872};
    private static final Pattern EXTRACT_LETTER_PATTERN = Pattern.compile("[a-zA-Z]");
    private static final String FALLBACK_CONTACT_LETTER = "?";
    private static final int PICTURE_SIZE = 40;
    private final LruCache<Address, Bitmap> mBitmapCache;
    /* access modifiers changed from: private */
    public Contacts mContactsHelper;
    /* access modifiers changed from: private */
    public ContentResolver mContentResolver;
    private int mDefaultBackgroundColor;
    /* access modifiers changed from: private */
    public int mPictureSizeInPx = ((int) (40.0f * this.mResources.getDisplayMetrics().density));
    private Resources mResources;

    public ContactPictureLoader(Context context, int defaultBackgroundColor) {
        Context appContext = context.getApplicationContext();
        this.mContentResolver = appContext.getContentResolver();
        this.mResources = appContext.getResources();
        this.mContactsHelper = Contacts.getInstance(appContext);
        this.mDefaultBackgroundColor = defaultBackgroundColor;
        this.mBitmapCache = new LruCache<Address, Bitmap>((1048576 * ((ActivityManager) appContext.getSystemService("activity")).getMemoryClass()) / 16) {
            /* access modifiers changed from: protected */
            public int sizeOf(Address key, Bitmap bitmap) {
                return bitmap.getByteCount();
            }
        };
    }

    public void loadContactPicture(Address address, ImageView imageView) {
        Bitmap bitmap = getBitmapFromCache(address);
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
        } else if (cancelPotentialWork(address, imageView)) {
            ContactPictureRetrievalTask task = new ContactPictureRetrievalTask(imageView, address);
            imageView.setImageDrawable(new AsyncDrawable(this.mResources, calculateFallbackBitmap(address), task));
            try {
                task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
            } catch (RejectedExecutionException e) {
                imageView.setImageBitmap(calculateFallbackBitmap(address));
            }
        }
    }

    private int calcUnknownContactColor(Address address) {
        if (this.mDefaultBackgroundColor != 0) {
            return this.mDefaultBackgroundColor;
        }
        return CONTACT_DUMMY_COLORS_ARGB[(Integer.MAX_VALUE & address.hashCode()) % CONTACT_DUMMY_COLORS_ARGB.length];
    }

    private String calcUnknownContactLetter(Address address) {
        String letter = null;
        String personal = address.getPersonal();
        Matcher m = EXTRACT_LETTER_PATTERN.matcher(personal != null ? personal : address.getAddress());
        if (m.find()) {
            letter = m.group(0).toUpperCase(Locale.US);
        }
        if (TextUtils.isEmpty(letter)) {
            return FALLBACK_CONTACT_LETTER;
        }
        return letter.substring(0, 1);
    }

    /* access modifiers changed from: private */
    public Bitmap calculateFallbackBitmap(Address address) {
        Bitmap result = Bitmap.createBitmap(this.mPictureSizeInPx, this.mPictureSizeInPx, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(result);
        result.eraseColor(calcUnknownContactColor(address));
        String letter = calcUnknownContactLetter(address);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);
        paint.setARGB(255, 255, 255, 255);
        paint.setTextSize((float) ((this.mPictureSizeInPx * 3) / 4));
        Rect rect = new Rect();
        paint.getTextBounds(letter, 0, 1, rect);
        canvas.drawText(letter, (((float) this.mPictureSizeInPx) / 2.0f) - (paint.measureText(letter) / 2.0f), (((float) this.mPictureSizeInPx) / 2.0f) + (((float) rect.height()) / 2.0f), paint);
        return RoundedQuickContactBadge.getCroppedBitmap(result);
    }

    /* access modifiers changed from: private */
    public void addBitmapToCache(Address key, Bitmap bitmap) {
        if (getBitmapFromCache(key) == null) {
            this.mBitmapCache.put(key, bitmap);
        }
    }

    private Bitmap getBitmapFromCache(Address key) {
        return this.mBitmapCache.get(key);
    }

    private boolean cancelPotentialWork(Address address, ImageView imageView) {
        ContactPictureRetrievalTask task = getContactPictureRetrievalTask(imageView);
        if (task == null || address == null) {
            return true;
        }
        if (address.equals(task.getAddress())) {
            return false;
        }
        task.cancel(true);
        return true;
    }

    /* access modifiers changed from: private */
    public ContactPictureRetrievalTask getContactPictureRetrievalTask(ImageView imageView) {
        if (imageView != null) {
            Drawable drawable = imageView.getDrawable();
            if (drawable instanceof AsyncDrawable) {
                return ((AsyncDrawable) drawable).getContactPictureRetrievalTask();
            }
        }
        return null;
    }

    class ContactPictureRetrievalTask extends AsyncTask<Void, Void, Bitmap> {
        private final Address mAddress;
        private final WeakReference<ImageView> mImageViewReference;

        ContactPictureRetrievalTask(ImageView imageView, Address address) {
            this.mImageViewReference = new WeakReference<>(imageView);
            this.mAddress = new Address(address);
        }

        public Address getAddress() {
            return this.mAddress;
        }

        /* access modifiers changed from: protected */
        public Bitmap doInBackground(Void... args) {
            InputStream stream;
            Uri photoUri = ContactPictureLoader.this.mContactsHelper.getPhotoUri(this.mAddress.getAddress());
            Bitmap bitmap = null;
            if (photoUri != null) {
                try {
                    stream = ContactPictureLoader.this.mContentResolver.openInputStream(photoUri);
                    if (stream != null) {
                        Bitmap tempBitmap = RoundedQuickContactBadge.getCroppedBitmap(BitmapFactory.decodeStream(stream));
                        if (!(tempBitmap == null || tempBitmap == (bitmap = Bitmap.createScaledBitmap(tempBitmap, ContactPictureLoader.this.mPictureSizeInPx, ContactPictureLoader.this.mPictureSizeInPx, true)))) {
                            tempBitmap.recycle();
                        }
                        try {
                            stream.close();
                        } catch (IOException e) {
                        }
                    }
                } catch (FileNotFoundException e2) {
                } catch (Throwable th) {
                    try {
                        stream.close();
                    } catch (IOException e3) {
                    }
                    throw th;
                }
            }
            if (bitmap == null) {
                bitmap = ContactPictureLoader.this.calculateFallbackBitmap(this.mAddress);
            }
            ContactPictureLoader.this.addBitmapToCache(this.mAddress, bitmap);
            return bitmap;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Bitmap bitmap) {
            ImageView imageView = this.mImageViewReference.get();
            if (imageView != null && ContactPictureLoader.this.getContactPictureRetrievalTask(imageView) == this) {
                imageView.setImageBitmap(bitmap);
            }
        }
    }

    static class AsyncDrawable extends BitmapDrawable {
        private final WeakReference<ContactPictureRetrievalTask> mAsyncTaskReference;

        public AsyncDrawable(Resources res, Bitmap bitmap, ContactPictureRetrievalTask task) {
            super(res, bitmap);
            this.mAsyncTaskReference = new WeakReference<>(task);
        }

        public ContactPictureRetrievalTask getContactPictureRetrievalTask() {
            return this.mAsyncTaskReference.get();
        }
    }
}
