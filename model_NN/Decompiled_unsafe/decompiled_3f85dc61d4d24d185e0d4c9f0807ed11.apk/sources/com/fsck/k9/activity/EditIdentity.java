package com.fsck.k9.activity;

import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import com.fsck.k9.Account;
import com.fsck.k9.Identity;
import com.fsck.k9.Preferences;
import com.fsck.k9.R;
import java.util.List;

public class EditIdentity extends K9Activity {
    public static final String EXTRA_ACCOUNT = "com.fsck.k9.EditIdentity_account";
    public static final String EXTRA_IDENTITY = "com.fsck.k9.EditIdentity_identity";
    public static final String EXTRA_IDENTITY_INDEX = "com.fsck.k9.EditIdentity_identity_index";
    private Account mAccount;
    private EditText mDescriptionView;
    private EditText mEmailView;
    /* access modifiers changed from: private */
    public Identity mIdentity;
    private int mIdentityIndex;
    private EditText mNameView;
    private EditText mReplyTo;
    /* access modifiers changed from: private */
    public LinearLayout mSignatureLayout;
    private CheckBox mSignatureUse;
    /* access modifiers changed from: private */
    public EditText mSignatureView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mIdentity = (Identity) getIntent().getSerializableExtra(EXTRA_IDENTITY);
        this.mIdentityIndex = getIntent().getIntExtra(EXTRA_IDENTITY_INDEX, -1);
        this.mAccount = Preferences.getPreferences(this).getAccount(getIntent().getStringExtra(EXTRA_ACCOUNT));
        if (this.mIdentityIndex == -1) {
            this.mIdentity = new Identity();
        }
        setContentView((int) R.layout.edit_identity);
        if (savedInstanceState != null && savedInstanceState.containsKey(EXTRA_IDENTITY)) {
            this.mIdentity = (Identity) savedInstanceState.getSerializable(EXTRA_IDENTITY);
        }
        this.mDescriptionView = (EditText) findViewById(R.id.description);
        this.mDescriptionView.setText(this.mIdentity.getDescription());
        this.mNameView = (EditText) findViewById(R.id.name);
        this.mNameView.setText(this.mIdentity.getName());
        this.mEmailView = (EditText) findViewById(R.id.email);
        this.mEmailView.setText(this.mIdentity.getEmail());
        this.mReplyTo = (EditText) findViewById(R.id.reply_to);
        this.mReplyTo.setText(this.mIdentity.getReplyTo());
        this.mSignatureLayout = (LinearLayout) findViewById(R.id.signature_layout);
        this.mSignatureUse = (CheckBox) findViewById(R.id.signature_use);
        this.mSignatureView = (EditText) findViewById(R.id.signature);
        this.mSignatureUse.setChecked(this.mIdentity.getSignatureUse());
        this.mSignatureUse.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    EditIdentity.this.mSignatureLayout.setVisibility(0);
                    EditIdentity.this.mSignatureView.setText(EditIdentity.this.mIdentity.getSignature());
                    return;
                }
                EditIdentity.this.mSignatureLayout.setVisibility(8);
            }
        });
        if (this.mSignatureUse.isChecked()) {
            this.mSignatureView.setText(this.mIdentity.getSignature());
        } else {
            this.mSignatureLayout.setVisibility(8);
        }
    }

    private void saveIdentity() {
        this.mIdentity.setDescription(this.mDescriptionView.getText().toString());
        this.mIdentity.setEmail(this.mEmailView.getText().toString());
        this.mIdentity.setName(this.mNameView.getText().toString());
        this.mIdentity.setSignatureUse(this.mSignatureUse.isChecked());
        this.mIdentity.setSignature(this.mSignatureView.getText().toString());
        if (this.mReplyTo.getText().length() == 0) {
            this.mIdentity.setReplyTo(null);
        } else {
            this.mIdentity.setReplyTo(this.mReplyTo.getText().toString());
        }
        List<Identity> identities = this.mAccount.getIdentities();
        if (this.mIdentityIndex == -1) {
            identities.add(this.mIdentity);
        } else {
            identities.remove(this.mIdentityIndex);
            identities.add(this.mIdentityIndex, this.mIdentity);
        }
        this.mAccount.save(Preferences.getPreferences(getApplication().getApplicationContext()));
        finish();
    }

    public void onBackPressed() {
        saveIdentity();
        super.onBackPressed();
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(EXTRA_IDENTITY, this.mIdentity);
    }
}
