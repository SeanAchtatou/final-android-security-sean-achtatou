package com.fsck.k9;

import android.content.Context;
import android.util.Log;
import com.fsck.k9.mail.store.RemoteStore;
import com.fsck.k9.mailstore.LocalStore;
import com.fsck.k9.preferences.Storage;
import com.fsck.k9.preferences.StorageEditor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Preferences {
    private static Preferences preferences;
    private Map<String, Account> accounts = null;
    private List<Account> accountsInOrder = null;
    private Context mContext;
    private Storage mStorage;
    private Account newAccount;

    public static synchronized Preferences getPreferences(Context context) {
        Preferences preferences2;
        synchronized (Preferences.class) {
            Context appContext = context.getApplicationContext();
            if (preferences == null) {
                preferences = new Preferences(appContext);
            }
            preferences2 = preferences;
        }
        return preferences2;
    }

    private Preferences(Context context) {
        this.mStorage = Storage.getStorage(context);
        this.mContext = context;
        if (this.mStorage.isEmpty()) {
            Log.i("k9", "Preferences storage is zero-size, importing from Android-style preferences");
            StorageEditor editor = this.mStorage.edit();
            editor.copy(context.getSharedPreferences("AndroidMail.Main", 0));
            editor.commit();
        }
    }

    public synchronized void loadAccounts() {
        this.accounts = new HashMap();
        this.accountsInOrder = new LinkedList();
        String accountUuids = getStorage().getString("accountUuids", null);
        if (!(accountUuids == null || accountUuids.length() == 0)) {
            for (String uuid : accountUuids.split(",")) {
                Account newAccount2 = new Account(this, uuid);
                this.accounts.put(uuid, newAccount2);
                this.accountsInOrder.add(newAccount2);
            }
        }
        if (!(this.newAccount == null || this.newAccount.getAccountNumber() == -1)) {
            this.accounts.put(this.newAccount.getUuid(), this.newAccount);
            if (!this.accountsInOrder.contains(this.newAccount)) {
                this.accountsInOrder.add(this.newAccount);
            }
            this.newAccount = null;
        }
    }

    public synchronized List<Account> getAccounts() {
        if (this.accounts == null) {
            loadAccounts();
        }
        return Collections.unmodifiableList(new ArrayList(this.accountsInOrder));
    }

    public synchronized Collection<Account> getAvailableAccounts() {
        Collection<Account> retval;
        List<Account> allAccounts = getAccounts();
        retval = new ArrayList<>(this.accounts.size());
        for (Account account : allAccounts) {
            if (account.isEnabled() && account.isAvailable(this.mContext)) {
                retval.add(account);
            }
        }
        return retval;
    }

    public synchronized Account getAccount(String uuid) {
        if (this.accounts == null) {
            loadAccounts();
        }
        return this.accounts.get(uuid);
    }

    public synchronized Account newAccount() {
        this.newAccount = new Account(this.mContext);
        this.accounts.put(this.newAccount.getUuid(), this.newAccount);
        this.accountsInOrder.add(this.newAccount);
        return this.newAccount;
    }

    public synchronized void deleteAccount(Account account) {
        if (this.accounts != null) {
            this.accounts.remove(account.getUuid());
        }
        if (this.accountsInOrder != null) {
            this.accountsInOrder.remove(account);
        }
        try {
            RemoteStore.removeInstance(account);
        } catch (Exception e) {
            Log.e("k9", "Failed to reset remote store for account " + account.getUuid(), e);
        }
        LocalStore.removeAccount(account);
        account.deleteCertificates();
        account.delete(this);
        if (this.newAccount == account) {
            this.newAccount = null;
        }
        return;
    }

    public Account getDefaultAccount() {
        Account defaultAccount = getAccount(getStorage().getString("defaultAccountUuid", null));
        if (defaultAccount != null) {
            return defaultAccount;
        }
        Collection<Account> accounts2 = getAvailableAccounts();
        if (accounts2.isEmpty()) {
            return defaultAccount;
        }
        Account defaultAccount2 = accounts2.iterator().next();
        setDefaultAccount(defaultAccount2);
        return defaultAccount2;
    }

    public void setDefaultAccount(Account account) {
        getStorage().edit().putString("defaultAccountUuid", account.getUuid()).commit();
    }

    public Storage getStorage() {
        return this.mStorage;
    }

    public static <T extends Enum<T>> T getEnumStringPref(Storage storage, String key, T defaultEnum) {
        String stringPref = storage.getString(key, null);
        if (stringPref == null) {
            return defaultEnum;
        }
        try {
            return Enum.valueOf(defaultEnum.getDeclaringClass(), stringPref);
        } catch (IllegalArgumentException ex) {
            Log.w("k9", "Unable to convert preference key [" + key + "] value [" + stringPref + "] to enum of type " + defaultEnum.getDeclaringClass(), ex);
            return defaultEnum;
        }
    }
}
