package com.fsck.k9;

import android.os.Handler;
import android.util.Log;
import java.util.Timer;
import java.util.TimerTask;

public class Throttle {
    public static final boolean DEBUG = false;
    public static final int DEFAULT_MAX_TIMEOUT = 2500;
    public static final int DEFAULT_MIN_TIMEOUT = 150;
    static final int TIMEOUT_EXTEND_INTERVAL = 500;
    private static Timer TIMER = new Timer();
    /* access modifiers changed from: private */
    public final Runnable mCallback;
    private final Clock mClock;
    /* access modifiers changed from: private */
    public final Handler mHandler;
    private long mLastEventTime;
    private final int mMaxTimeout;
    private final int mMinTimeout;
    private final String mName;
    /* access modifiers changed from: private */
    public MyTimerTask mRunningTimerTask;
    private int mTimeout;
    private final Timer mTimer;

    public Throttle(String name, Runnable callback, Handler handler) {
        this(name, callback, handler, 150, 2500);
    }

    public Throttle(String name, Runnable callback, Handler handler, int minTimeout, int maxTimeout) {
        this(name, callback, handler, minTimeout, maxTimeout, Clock.INSTANCE, TIMER);
    }

    Throttle(String name, Runnable callback, Handler handler, int minTimeout, int maxTimeout, Clock clock, Timer timer) {
        if (maxTimeout < minTimeout) {
            throw new IllegalArgumentException();
        }
        this.mName = name;
        this.mCallback = callback;
        this.mClock = clock;
        this.mTimer = timer;
        this.mHandler = handler;
        this.mMinTimeout = minTimeout;
        this.mMaxTimeout = maxTimeout;
        this.mTimeout = this.mMinTimeout;
    }

    private void debugLog(String message) {
        Log.d("k9", "Throttle: [" + this.mName + "] " + message);
    }

    private boolean isCallbackScheduled() {
        return this.mRunningTimerTask != null;
    }

    public void cancelScheduledCallback() {
        if (this.mRunningTimerTask != null) {
            this.mRunningTimerTask.cancel();
            this.mRunningTimerTask = null;
        }
    }

    /* access modifiers changed from: package-private */
    public void updateTimeout() {
        long now = this.mClock.getTime();
        if (now - this.mLastEventTime <= 500) {
            this.mTimeout *= 2;
            if (this.mTimeout >= this.mMaxTimeout) {
                this.mTimeout = this.mMaxTimeout;
            }
        } else {
            this.mTimeout = this.mMinTimeout;
        }
        this.mLastEventTime = now;
    }

    public void onEvent() {
        updateTimeout();
        if (!isCallbackScheduled()) {
            this.mRunningTimerTask = new MyTimerTask();
            this.mTimer.schedule(this.mRunningTimerTask, (long) this.mTimeout);
        }
    }

    private class MyTimerTask extends TimerTask {
        /* access modifiers changed from: private */
        public boolean mCanceled;

        private MyTimerTask() {
        }

        public void run() {
            Throttle.this.mHandler.post(new HandlerRunnable());
        }

        public boolean cancel() {
            this.mCanceled = true;
            return super.cancel();
        }

        private class HandlerRunnable implements Runnable {
            private HandlerRunnable() {
            }

            public void run() {
                MyTimerTask unused = Throttle.this.mRunningTimerTask = null;
                if (!MyTimerTask.this.mCanceled) {
                    Throttle.this.mCallback.run();
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public int getTimeoutForTest() {
        return this.mTimeout;
    }

    /* access modifiers changed from: package-private */
    public long getLastEventTimeForTest() {
        return this.mLastEventTime;
    }
}
