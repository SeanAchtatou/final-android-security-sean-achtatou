package com.fsck.k9.message;

import android.net.Uri;
import android.util.Log;
import com.fsck.k9.K9;
import com.fsck.k9.mail.filter.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

public class IdentityHeaderParser {
    public static Map<IdentityField, String> parse(String identityString) {
        Map<IdentityField, String> identity = new HashMap<>();
        if (K9.DEBUG) {
            Log.d("k9", "Decoding identity: " + identityString);
        }
        if (identityString != null && identityString.length() >= 1) {
            if (identityString.charAt(0) != "!".charAt(0) || identityString.length() <= 2) {
                if (K9.DEBUG) {
                    Log.d("k9", "Got a saved legacy identity: " + identityString);
                }
                StringTokenizer tokenizer = new StringTokenizer(identityString, ":", false);
                if (tokenizer.hasMoreTokens()) {
                    String bodyLengthS = Base64.decode(tokenizer.nextToken());
                    try {
                        identity.put(IdentityField.LENGTH, Integer.valueOf(bodyLengthS).toString());
                    } catch (Exception e) {
                        Log.e("k9", "Unable to parse bodyLength '" + bodyLengthS + "'");
                    }
                }
                if (tokenizer.hasMoreTokens()) {
                    identity.put(IdentityField.SIGNATURE, Base64.decode(tokenizer.nextToken()));
                }
                if (tokenizer.hasMoreTokens()) {
                    identity.put(IdentityField.NAME, Base64.decode(tokenizer.nextToken()));
                }
                if (tokenizer.hasMoreTokens()) {
                    identity.put(IdentityField.EMAIL, Base64.decode(tokenizer.nextToken()));
                }
                if (tokenizer.hasMoreTokens()) {
                    identity.put(IdentityField.QUOTED_TEXT_MODE, Base64.decode(tokenizer.nextToken()));
                }
            } else {
                Uri.Builder builder = new Uri.Builder();
                builder.encodedQuery(identityString.substring(1));
                Uri uri = builder.build();
                for (IdentityField key : IdentityField.values()) {
                    String value = uri.getQueryParameter(key.value());
                    if (value != null) {
                        identity.put(key, value);
                    }
                }
                if (K9.DEBUG) {
                    Log.d("k9", "Decoded identity: " + identity.toString());
                }
                for (IdentityField key2 : IdentityField.getIntegerFields()) {
                    if (identity.get(key2) != null) {
                        try {
                            Integer.parseInt((String) identity.get(key2));
                        } catch (NumberFormatException e2) {
                            Log.e("k9", "Invalid " + key2.name() + " field in identity: " + ((String) identity.get(key2)));
                        }
                    }
                }
            }
        }
        return identity;
    }
}
