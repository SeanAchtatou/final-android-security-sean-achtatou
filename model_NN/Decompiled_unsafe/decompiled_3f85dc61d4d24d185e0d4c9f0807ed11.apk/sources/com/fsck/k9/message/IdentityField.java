package com.fsck.k9.message;

public enum IdentityField {
    LENGTH("l"),
    OFFSET("o"),
    FOOTER_OFFSET("fo"),
    PLAIN_LENGTH("pl"),
    PLAIN_OFFSET("po"),
    MESSAGE_FORMAT("f"),
    MESSAGE_READ_RECEIPT("r"),
    SIGNATURE("s"),
    NAME("n"),
    EMAIL("e"),
    ORIGINAL_MESSAGE("m"),
    CURSOR_POSITION("p"),
    QUOTED_TEXT_MODE("q"),
    QUOTE_STYLE("qs");
    
    static final String IDENTITY_VERSION_1 = "!";
    private final String value;

    private IdentityField(String value2) {
        this.value = value2;
    }

    public String value() {
        return this.value;
    }

    public static IdentityField[] getIntegerFields() {
        return new IdentityField[]{LENGTH, OFFSET, FOOTER_OFFSET, PLAIN_LENGTH, PLAIN_OFFSET};
    }
}
