package com.fsck.k9.message.extractors;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.util.Log;
import com.fsck.k9.Globals;
import com.fsck.k9.mail.Body;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.Part;
import com.fsck.k9.mail.internet.MimeUtility;
import com.fsck.k9.mailstore.AttachmentViewInfo;
import com.fsck.k9.mailstore.DeferredFileBody;
import com.fsck.k9.mailstore.LocalMessage;
import com.fsck.k9.mailstore.LocalPart;
import com.fsck.k9.provider.AttachmentProvider;
import com.fsck.k9.provider.DecryptedFileProvider;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.james.mime4j.dom.field.ContentDispositionField;

public class AttachmentInfoExtractor {
    private final Context context;

    public static AttachmentInfoExtractor getInstance() {
        return new AttachmentInfoExtractor(Globals.getContext());
    }

    AttachmentInfoExtractor(Context context2) {
        this.context = context2;
    }

    @WorkerThread
    public List<AttachmentViewInfo> extractAttachmentInfoForView(List<Part> attachmentParts) throws MessagingException {
        List<AttachmentViewInfo> attachments = new ArrayList<>();
        for (Part part : attachmentParts) {
            AttachmentViewInfo attachmentViewInfo = extractAttachmentInfo(part);
            if (!attachmentViewInfo.inlineAttachment) {
                attachments.add(attachmentViewInfo);
            }
        }
        return attachments;
    }

    @WorkerThread
    public AttachmentViewInfo extractAttachmentInfo(Part part) throws MessagingException {
        long size;
        Uri uri;
        boolean isContentAvailable = true;
        if (part instanceof LocalPart) {
            LocalPart localPart = (LocalPart) part;
            String accountUuid = localPart.getAccountUuid();
            long messagePartId = localPart.getId();
            size = localPart.getSize();
            if (part.getBody() == null) {
                isContentAvailable = false;
            }
            uri = AttachmentProvider.getAttachmentUri(accountUuid, messagePartId);
        } else if (part instanceof LocalMessage) {
            LocalMessage localMessage = (LocalMessage) part;
            String accountUuid2 = localMessage.getAccount().getUuid();
            long messagePartId2 = localMessage.getMessagePartId();
            size = (long) localMessage.getSize();
            if (part.getBody() == null) {
                isContentAvailable = false;
            }
            uri = AttachmentProvider.getAttachmentUri(accountUuid2, messagePartId2);
        } else {
            Body body = part.getBody();
            if (body instanceof DeferredFileBody) {
                DeferredFileBody decryptedTempFileBody = (DeferredFileBody) body;
                size = decryptedTempFileBody.getSize();
                uri = getDecryptedFileProviderUri(decryptedTempFileBody, part.getMimeType());
                isContentAvailable = true;
            } else {
                throw new IllegalArgumentException("Unsupported part type provided");
            }
        }
        return extractAttachmentInfo(part, uri, size, isContentAvailable);
    }

    /* access modifiers changed from: protected */
    @Nullable
    public Uri getDecryptedFileProviderUri(DeferredFileBody decryptedTempFileBody, String mimeType) {
        try {
            return DecryptedFileProvider.getUriForProvidedFile(this.context, decryptedTempFileBody.getFile(), decryptedTempFileBody.getEncoding(), mimeType);
        } catch (IOException e) {
            Log.e("k9", "Decrypted temp file (no longer?) exists!", e);
            return null;
        }
    }

    public AttachmentViewInfo extractAttachmentInfoForDatabase(Part part) throws MessagingException {
        return extractAttachmentInfo(part, Uri.EMPTY, -1, part.getBody() != null);
    }

    @WorkerThread
    private AttachmentViewInfo extractAttachmentInfo(Part part, Uri uri, long size, boolean isContentAvailable) throws MessagingException {
        boolean inlineAttachment = false;
        String mimeType = part.getMimeType();
        String contentTypeHeader = MimeUtility.unfoldAndDecode(part.getContentType());
        String contentDisposition = MimeUtility.unfoldAndDecode(part.getDisposition());
        String name = MimeUtility.getHeaderParameter(contentDisposition, ContentDispositionField.PARAM_FILENAME);
        if (name == null) {
            name = MimeUtility.getHeaderParameter(contentTypeHeader, "name");
        }
        if (name == null) {
            String extension = null;
            if (mimeType != null) {
                extension = MimeUtility.getExtensionByMimeType(mimeType);
            }
            name = "noname" + (extension != null ? "." + extension : "");
        }
        if (contentDisposition != null && MimeUtility.getHeaderParameter(contentDisposition, null).matches("^(?i:inline)") && part.getHeader("Content-ID").length > 0) {
            inlineAttachment = true;
        }
        return new AttachmentViewInfo(mimeType, name, extractAttachmentSize(contentDisposition, size), uri, inlineAttachment, part, isContentAvailable);
    }

    @WorkerThread
    private long extractAttachmentSize(String contentDisposition, long size) {
        if (size != -1) {
            return size;
        }
        long result = -1;
        String sizeParam = MimeUtility.getHeaderParameter(contentDisposition, ContentDispositionField.PARAM_SIZE);
        if (sizeParam != null) {
            try {
                result = (long) Integer.parseInt(sizeParam);
            } catch (NumberFormatException e) {
            }
        }
        return result;
    }
}
