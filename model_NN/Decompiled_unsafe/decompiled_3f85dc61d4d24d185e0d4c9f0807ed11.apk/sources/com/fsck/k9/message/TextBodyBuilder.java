package com.fsck.k9.message;

import android.text.TextUtils;
import android.util.Log;
import com.fsck.k9.K9;
import com.fsck.k9.helper.HtmlConverter;
import com.fsck.k9.mail.internet.TextBody;
import com.fsck.k9.message.InsertableHtmlContent;

class TextBodyBuilder {
    private boolean mAppendSignature = true;
    private boolean mIncludeQuotedText = true;
    private boolean mInsertSeparator = false;
    private String mMessageContent;
    private String mQuotedText;
    private InsertableHtmlContent mQuotedTextHtml;
    private boolean mReplyAfterQuote = false;
    private String mSignature;
    private boolean mSignatureBeforeQuotedText = false;

    public TextBodyBuilder(String messageContent) {
        this.mMessageContent = messageContent;
    }

    public TextBody buildTextHtml() {
        String text;
        int composedMessageLength;
        int composedMessageOffset;
        String text2 = this.mMessageContent;
        if (this.mIncludeQuotedText) {
            InsertableHtmlContent quotedHtmlContent = getQuotedTextHtml();
            if (K9.DEBUG) {
                Log.d("k9", "insertable: " + quotedHtmlContent.toDebugString());
            }
            if (this.mAppendSignature && (this.mReplyAfterQuote || this.mSignatureBeforeQuotedText)) {
                text2 = text2 + getSignature();
            }
            String text3 = textToHtmlFragment(text2);
            if (this.mReplyAfterQuote) {
                quotedHtmlContent.setInsertionLocation(InsertableHtmlContent.InsertionLocation.AFTER_QUOTE);
                if (this.mInsertSeparator) {
                    text3 = "<br clear=\"all\">" + text3;
                }
            } else {
                quotedHtmlContent.setInsertionLocation(InsertableHtmlContent.InsertionLocation.BEFORE_QUOTE);
                if (this.mInsertSeparator) {
                    text3 = text3 + "<br><br>";
                }
            }
            if (this.mAppendSignature && !this.mReplyAfterQuote && !this.mSignatureBeforeQuotedText) {
                quotedHtmlContent.insertIntoQuotedFooter(getSignatureHtml());
            }
            quotedHtmlContent.setUserContent(text3);
            composedMessageLength = text3.length();
            composedMessageOffset = quotedHtmlContent.getInsertionPoint();
            text = quotedHtmlContent.toString();
        } else {
            if (this.mAppendSignature) {
                text2 = text2 + getSignature();
            }
            text = textToHtmlFragment(text2);
            composedMessageLength = text.length();
            composedMessageOffset = 0;
        }
        TextBody body = new TextBody(text);
        body.setComposedMessageLength(Integer.valueOf(composedMessageLength));
        body.setComposedMessageOffset(Integer.valueOf(composedMessageOffset));
        return body;
    }

    public TextBody buildTextPlain() {
        String text = this.mMessageContent;
        int composedMessageLength = text.length();
        int composedMessageOffset = 0;
        if (this.mIncludeQuotedText) {
            String quotedText = getQuotedText();
            if (this.mAppendSignature && (this.mReplyAfterQuote || this.mSignatureBeforeQuotedText)) {
                text = text + getSignature();
            }
            if (this.mReplyAfterQuote) {
                composedMessageOffset = quotedText.length() + "\r\n".length();
                text = quotedText + "\r\n" + text;
            } else {
                text = text + "\r\n\r\n" + quotedText;
            }
            if (this.mAppendSignature && !this.mReplyAfterQuote && !this.mSignatureBeforeQuotedText) {
                text = text + getSignature();
            }
        } else if (this.mAppendSignature) {
            text = text + getSignature();
        }
        TextBody body = new TextBody(text);
        body.setComposedMessageLength(Integer.valueOf(composedMessageLength));
        body.setComposedMessageOffset(Integer.valueOf(composedMessageOffset));
        return body;
    }

    private String getSignature() {
        if (!TextUtils.isEmpty(this.mSignature)) {
            return "\r\n" + this.mSignature;
        }
        return "";
    }

    private String getSignatureHtml() {
        if (!TextUtils.isEmpty(this.mSignature)) {
            return textToHtmlFragment("\r\n" + this.mSignature);
        }
        return "";
    }

    private String getQuotedText() {
        if (!TextUtils.isEmpty(this.mQuotedText)) {
            return this.mQuotedText;
        }
        return "";
    }

    private InsertableHtmlContent getQuotedTextHtml() {
        return this.mQuotedTextHtml;
    }

    /* access modifiers changed from: protected */
    public String textToHtmlFragment(String text) {
        return HtmlConverter.textToHtmlFragment(text);
    }

    public void setSignature(String signature) {
        this.mSignature = signature;
    }

    public void setIncludeQuotedText(boolean includeQuotedText) {
        this.mIncludeQuotedText = includeQuotedText;
    }

    public void setQuotedText(String quotedText) {
        this.mQuotedText = quotedText;
    }

    public void setQuotedTextHtml(InsertableHtmlContent quotedTextHtml) {
        this.mQuotedTextHtml = quotedTextHtml;
    }

    public void setInsertSeparator(boolean insertSeparator) {
        this.mInsertSeparator = insertSeparator;
    }

    public void setSignatureBeforeQuotedText(boolean signatureBeforeQuotedText) {
        this.mSignatureBeforeQuotedText = signatureBeforeQuotedText;
    }

    public void setReplyAfterQuote(boolean replyAfterQuote) {
        this.mReplyAfterQuote = replyAfterQuote;
    }

    public void setAppendSignature(boolean appendSignature) {
        this.mAppendSignature = appendSignature;
    }
}
