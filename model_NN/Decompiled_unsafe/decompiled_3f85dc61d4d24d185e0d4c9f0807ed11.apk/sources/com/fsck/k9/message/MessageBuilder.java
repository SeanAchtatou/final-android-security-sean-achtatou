package com.fsck.k9.message;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import com.fsck.k9.Account;
import com.fsck.k9.Identity;
import com.fsck.k9.K9;
import com.fsck.k9.R;
import com.fsck.k9.activity.MessageReference;
import com.fsck.k9.activity.misc.Attachment;
import com.fsck.k9.mail.Address;
import com.fsck.k9.mail.BoundaryGenerator;
import com.fsck.k9.mail.Flag;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.internet.MessageIdGenerator;
import com.fsck.k9.mail.internet.MimeBodyPart;
import com.fsck.k9.mail.internet.MimeMessage;
import com.fsck.k9.mail.internet.MimeMessageHelper;
import com.fsck.k9.mail.internet.MimeMultipart;
import com.fsck.k9.mail.internet.MimeUtility;
import com.fsck.k9.mail.internet.TextBody;
import com.fsck.k9.mailstore.TempFileBody;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import org.apache.james.mime4j.codec.EncoderUtil;
import org.apache.james.mime4j.dom.field.ContentTypeField;
import org.apache.james.mime4j.util.MimeUtil;

public abstract class MessageBuilder {
    private Callback asyncCallback;
    private List<Attachment> attachments;
    private Address[] bcc;
    private final BoundaryGenerator boundaryGenerator;
    private final Object callbackLock = new Object();
    private Address[] cc;
    private final Context context;
    private int cursorPosition;
    private boolean hideTimeZone;
    private Identity identity;
    private boolean identityChanged;
    private String inReplyTo;
    private boolean isDraft;
    private boolean isPgpInlineEnabled;
    private boolean isReplyAfterQuote;
    private boolean isSignatureBeforeQuotedText;
    private SimpleMessageFormat messageFormat;
    private final MessageIdGenerator messageIdGenerator;
    private MessageReference messageReference;
    private MessagingException queuedException;
    private MimeMessage queuedMimeMessage;
    private PendingIntent queuedPendingIntent;
    private int queuedRequestCode;
    private Account.QuoteStyle quoteStyle;
    private InsertableHtmlContent quotedHtmlContent;
    private String quotedText;
    private QuotedTextMode quotedTextMode;
    private String references;
    private boolean requestReadReceipt;
    private Date sentDate;
    private String signature;
    private boolean signatureChanged;
    private String subject;
    private String text;
    private Address[] to;

    public interface Callback {
        void onMessageBuildCancel();

        void onMessageBuildException(MessagingException messagingException);

        void onMessageBuildReturnPendingIntent(PendingIntent pendingIntent, int i);

        void onMessageBuildSuccess(MimeMessage mimeMessage, boolean z);
    }

    /* access modifiers changed from: protected */
    public abstract void buildMessageInternal();

    /* access modifiers changed from: protected */
    public abstract void buildMessageOnActivityResult(int i, Intent intent);

    protected MessageBuilder(Context context2, MessageIdGenerator messageIdGenerator2, BoundaryGenerator boundaryGenerator2) {
        this.context = context2;
        this.messageIdGenerator = messageIdGenerator2;
        this.boundaryGenerator = boundaryGenerator2;
    }

    /* access modifiers changed from: protected */
    public MimeMessage build() throws MessagingException {
        MimeMessage message = new MimeMessage();
        buildHeader(message);
        buildBody(message);
        return message;
    }

    private void buildHeader(MimeMessage message) throws MessagingException {
        message.addSentDate(this.sentDate, this.hideTimeZone);
        Address from = new Address(this.identity.getEmail(), this.identity.getName());
        message.setFrom(from);
        message.setRecipients(Message.RecipientType.TO, this.to);
        message.setRecipients(Message.RecipientType.CC, this.cc);
        message.setRecipients(Message.RecipientType.BCC, this.bcc);
        message.setSubject(this.subject);
        if (this.requestReadReceipt) {
            message.setHeader("Disposition-Notification-To", from.toEncodedString());
            message.setHeader("X-Confirm-Reading-To", from.toEncodedString());
            message.setHeader("Return-Receipt-To", from.toEncodedString());
        }
        if (!K9.hideUserAgent()) {
            message.setHeader("User-Agent", this.context.getString(R.string.message_header_mua));
        }
        String replyTo = this.identity.getReplyTo();
        if (replyTo != null) {
            message.setReplyTo(new Address[]{new Address(replyTo)});
        }
        if (this.inReplyTo != null) {
            message.setInReplyTo(this.inReplyTo);
        }
        if (this.references != null) {
            message.setReferences(this.references);
        }
        message.setMessageId(this.messageIdGenerator.generateMessageId(message));
        if (this.isDraft && this.isPgpInlineEnabled) {
            message.setFlag(Flag.X_DRAFT_OPENPGP_INLINE, true);
        }
    }

    /* access modifiers changed from: protected */
    public MimeMultipart createMimeMultipart() {
        return new MimeMultipart(this.boundaryGenerator.generateBoundary());
    }

    private void buildBody(MimeMessage message) throws MessagingException {
        TextBody body = buildText(this.isDraft);
        TextBody bodyPlain = null;
        boolean hasAttachments = !this.attachments.isEmpty();
        if (this.messageFormat == SimpleMessageFormat.HTML) {
            MimeMultipart composedMimeMessage = createMimeMultipart();
            composedMimeMessage.setSubType("alternative");
            composedMimeMessage.addBodyPart(new MimeBodyPart(body, "text/html"));
            bodyPlain = buildText(this.isDraft, SimpleMessageFormat.TEXT);
            composedMimeMessage.addBodyPart(new MimeBodyPart(bodyPlain, ContentTypeField.TYPE_TEXT_PLAIN));
            if (hasAttachments) {
                MimeMultipart mp = createMimeMultipart();
                mp.addBodyPart(new MimeBodyPart(composedMimeMessage));
                addAttachmentsToMessage(mp);
                MimeMessageHelper.setBody(message, mp);
            } else {
                MimeMessageHelper.setBody(message, composedMimeMessage);
            }
        } else if (this.messageFormat == SimpleMessageFormat.TEXT) {
            if (hasAttachments) {
                MimeMultipart mp2 = createMimeMultipart();
                mp2.addBodyPart(new MimeBodyPart(body, ContentTypeField.TYPE_TEXT_PLAIN));
                addAttachmentsToMessage(mp2);
                MimeMessageHelper.setBody(message, mp2);
            } else {
                MimeMessageHelper.setBody(message, body);
            }
        }
        if (this.isDraft) {
            message.addHeader("X-K9mail-Identity", buildIdentityHeader(body, bodyPlain));
        }
    }

    private String buildIdentityHeader(TextBody body, TextBody bodyPlain) {
        return new IdentityHeaderBuilder().setCursorPosition(this.cursorPosition).setIdentity(this.identity).setIdentityChanged(this.identityChanged).setMessageFormat(this.messageFormat).setMessageReference(this.messageReference).setQuotedHtmlContent(this.quotedHtmlContent).setQuoteStyle(this.quoteStyle).setQuoteTextMode(this.quotedTextMode).setSignature(this.signature).setSignatureChanged(this.signatureChanged).setBody(body).setBodyPlain(bodyPlain).build();
    }

    private void addAttachmentsToMessage(MimeMultipart mp) throws MessagingException {
        for (Attachment attachment : this.attachments) {
            if (attachment.state == Attachment.LoadingState.COMPLETE) {
                String contentType = attachment.contentType;
                if (MimeUtil.isMessage(contentType)) {
                    contentType = MimeUtility.DEFAULT_ATTACHMENT_MIME_TYPE;
                }
                MimeBodyPart bp = new MimeBodyPart(new TempFileBody(attachment.filename));
                bp.addHeader("Content-Type", String.format("%s;\r\n name=\"%s\"", contentType, EncoderUtil.encodeIfNecessary(attachment.name, EncoderUtil.Usage.WORD_ENTITY, 7)));
                bp.setEncoding(MimeUtility.getEncodingforType(contentType));
                bp.addHeader("Content-Disposition", String.format(Locale.US, "attachment;\r\n filename=\"%s\";\r\n size=%d", attachment.name, attachment.size));
                mp.addBodyPart(bp);
            }
        }
    }

    private TextBody buildText(boolean isDraft2) {
        return buildText(isDraft2, this.messageFormat);
    }

    private TextBody buildText(boolean isDraft2, SimpleMessageFormat simpleMessageFormat) {
        boolean includeQuotedText;
        boolean isReplyAfterQuote2;
        boolean z;
        boolean useSignature;
        TextBodyBuilder textBodyBuilder = new TextBodyBuilder(this.text);
        if (isDraft2 || this.quotedTextMode == QuotedTextMode.SHOW) {
            includeQuotedText = true;
        } else {
            includeQuotedText = false;
        }
        if (this.quoteStyle != Account.QuoteStyle.PREFIX || !this.isReplyAfterQuote) {
            isReplyAfterQuote2 = false;
        } else {
            isReplyAfterQuote2 = true;
        }
        textBodyBuilder.setIncludeQuotedText(false);
        if (includeQuotedText) {
            if (simpleMessageFormat == SimpleMessageFormat.HTML && this.quotedHtmlContent != null) {
                textBodyBuilder.setIncludeQuotedText(true);
                textBodyBuilder.setQuotedTextHtml(this.quotedHtmlContent);
                textBodyBuilder.setReplyAfterQuote(isReplyAfterQuote2);
            }
            if (simpleMessageFormat == SimpleMessageFormat.TEXT && this.quotedText.length() > 0) {
                textBodyBuilder.setIncludeQuotedText(true);
                textBodyBuilder.setQuotedText(this.quotedText);
                textBodyBuilder.setReplyAfterQuote(isReplyAfterQuote2);
            }
        }
        if (!isDraft2) {
            z = true;
        } else {
            z = false;
        }
        textBodyBuilder.setInsertSeparator(z);
        if (isDraft2 || !this.identity.getSignatureUse()) {
            useSignature = false;
        } else {
            useSignature = true;
        }
        if (useSignature) {
            textBodyBuilder.setAppendSignature(true);
            textBodyBuilder.setSignature(this.signature);
            textBodyBuilder.setSignatureBeforeQuotedText(this.isSignatureBeforeQuotedText);
        } else {
            textBodyBuilder.setAppendSignature(false);
        }
        if (simpleMessageFormat == SimpleMessageFormat.HTML) {
            return textBodyBuilder.buildTextHtml();
        }
        return textBodyBuilder.buildTextPlain();
    }

    public MessageBuilder setSubject(String subject2) {
        this.subject = subject2;
        return this;
    }

    public MessageBuilder setSentDate(Date sentDate2) {
        this.sentDate = sentDate2;
        return this;
    }

    public MessageBuilder setHideTimeZone(boolean hideTimeZone2) {
        this.hideTimeZone = hideTimeZone2;
        return this;
    }

    public MessageBuilder setTo(List<Address> to2) {
        this.to = (Address[]) to2.toArray(new Address[to2.size()]);
        return this;
    }

    public MessageBuilder setCc(List<Address> cc2) {
        this.cc = (Address[]) cc2.toArray(new Address[cc2.size()]);
        return this;
    }

    public MessageBuilder setBcc(List<Address> bcc2) {
        this.bcc = (Address[]) bcc2.toArray(new Address[bcc2.size()]);
        return this;
    }

    public MessageBuilder setInReplyTo(String inReplyTo2) {
        this.inReplyTo = inReplyTo2;
        return this;
    }

    public MessageBuilder setReferences(String references2) {
        this.references = references2;
        return this;
    }

    public MessageBuilder setRequestReadReceipt(boolean requestReadReceipt2) {
        this.requestReadReceipt = requestReadReceipt2;
        return this;
    }

    public MessageBuilder setIdentity(Identity identity2) {
        this.identity = identity2;
        return this;
    }

    public MessageBuilder setMessageFormat(SimpleMessageFormat messageFormat2) {
        this.messageFormat = messageFormat2;
        return this;
    }

    public MessageBuilder setText(String text2) {
        this.text = text2;
        return this;
    }

    public MessageBuilder setAttachments(List<Attachment> attachments2) {
        this.attachments = attachments2;
        return this;
    }

    public MessageBuilder setSignature(String signature2) {
        this.signature = signature2;
        return this;
    }

    public MessageBuilder setQuoteStyle(Account.QuoteStyle quoteStyle2) {
        this.quoteStyle = quoteStyle2;
        return this;
    }

    public MessageBuilder setQuotedTextMode(QuotedTextMode quotedTextMode2) {
        this.quotedTextMode = quotedTextMode2;
        return this;
    }

    public MessageBuilder setQuotedText(String quotedText2) {
        this.quotedText = quotedText2;
        return this;
    }

    public MessageBuilder setQuotedHtmlContent(InsertableHtmlContent quotedHtmlContent2) {
        this.quotedHtmlContent = quotedHtmlContent2;
        return this;
    }

    public MessageBuilder setReplyAfterQuote(boolean isReplyAfterQuote2) {
        this.isReplyAfterQuote = isReplyAfterQuote2;
        return this;
    }

    public MessageBuilder setSignatureBeforeQuotedText(boolean isSignatureBeforeQuotedText2) {
        this.isSignatureBeforeQuotedText = isSignatureBeforeQuotedText2;
        return this;
    }

    public MessageBuilder setIdentityChanged(boolean identityChanged2) {
        this.identityChanged = identityChanged2;
        return this;
    }

    public MessageBuilder setSignatureChanged(boolean signatureChanged2) {
        this.signatureChanged = signatureChanged2;
        return this;
    }

    public MessageBuilder setCursorPosition(int cursorPosition2) {
        this.cursorPosition = cursorPosition2;
        return this;
    }

    public MessageBuilder setMessageReference(MessageReference messageReference2) {
        this.messageReference = messageReference2;
        return this;
    }

    public MessageBuilder setDraft(boolean isDraft2) {
        this.isDraft = isDraft2;
        return this;
    }

    public MessageBuilder setIsPgpInlineEnabled(boolean isPgpInlineEnabled2) {
        this.isPgpInlineEnabled = isPgpInlineEnabled2;
        return this;
    }

    public boolean isDraft() {
        return this.isDraft;
    }

    public final void buildAsync(Callback callback) {
        synchronized (this.callbackLock) {
            this.asyncCallback = callback;
            this.queuedMimeMessage = null;
            this.queuedException = null;
            this.queuedPendingIntent = null;
        }
        new AsyncTask<Void, Void, Void>() {
            /* access modifiers changed from: protected */
            public Void doInBackground(Void... params) {
                MessageBuilder.this.buildMessageInternal();
                return null;
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(Void aVoid) {
                MessageBuilder.this.deliverResult();
            }
        }.execute(new Void[0]);
    }

    public final void onActivityResult(final int requestCode, int resultCode, final Intent data, Callback callback) {
        synchronized (this.callbackLock) {
            this.asyncCallback = callback;
            this.queuedMimeMessage = null;
            this.queuedException = null;
            this.queuedPendingIntent = null;
        }
        if (resultCode != -1) {
            this.asyncCallback.onMessageBuildCancel();
        } else {
            new AsyncTask<Void, Void, Void>() {
                /* access modifiers changed from: protected */
                public Void doInBackground(Void... params) {
                    MessageBuilder.this.buildMessageOnActivityResult(requestCode, data);
                    return null;
                }

                /* access modifiers changed from: protected */
                public void onPostExecute(Void aVoid) {
                    MessageBuilder.this.deliverResult();
                }
            }.execute(new Void[0]);
        }
    }

    public final void detachCallback() {
        synchronized (this.callbackLock) {
            this.asyncCallback = null;
        }
    }

    public final void reattachCallback(Callback callback) {
        synchronized (this.callbackLock) {
            if (this.asyncCallback != null) {
                throw new IllegalStateException("need to detach callback before new one can be attached!");
            }
            this.asyncCallback = callback;
            deliverResult();
        }
    }

    /* access modifiers changed from: protected */
    public final void queueMessageBuildSuccess(MimeMessage message) {
        synchronized (this.callbackLock) {
            this.queuedMimeMessage = message;
        }
    }

    /* access modifiers changed from: protected */
    public final void queueMessageBuildException(MessagingException exception) {
        synchronized (this.callbackLock) {
            this.queuedException = exception;
        }
    }

    /* access modifiers changed from: protected */
    public final void queueMessageBuildPendingIntent(PendingIntent pendingIntent, int requestCode) {
        synchronized (this.callbackLock) {
            this.queuedPendingIntent = pendingIntent;
            this.queuedRequestCode = requestCode;
        }
    }

    /* access modifiers changed from: protected */
    public final void deliverResult() {
        synchronized (this.callbackLock) {
            if (this.asyncCallback == null) {
                Log.d("k9", "Keeping message builder result in queue for later delivery");
                return;
            }
            if (this.queuedMimeMessage != null) {
                this.asyncCallback.onMessageBuildSuccess(this.queuedMimeMessage, this.isDraft);
                this.queuedMimeMessage = null;
            } else if (this.queuedException != null) {
                this.asyncCallback.onMessageBuildException(this.queuedException);
                this.queuedException = null;
            } else if (this.queuedPendingIntent != null) {
                this.asyncCallback.onMessageBuildReturnPendingIntent(this.queuedPendingIntent, this.queuedRequestCode);
                this.queuedPendingIntent = null;
            }
            this.asyncCallback = null;
        }
    }
}
