package com.fsck.k9.message.extractors;

import android.support.annotation.NonNull;
import android.support.v4.media.session.PlaybackStateCompat;
import com.fsck.k9.helper.HtmlConverter;
import com.fsck.k9.mail.Part;
import com.fsck.k9.mail.internet.MessageExtractor;
import com.fsck.k9.mail.internet.MimeUtility;

class PreviewTextExtractor {
    private static final int MAX_CHARACTERS_CHECKED_FOR_PREVIEW = 8192;
    private static final int MAX_PREVIEW_LENGTH = 512;

    PreviewTextExtractor() {
    }

    @NonNull
    public String extractPreview(@NonNull Part textPart) throws PreviewExtractionException {
        String text = MessageExtractor.getTextFromPart(textPart, PlaybackStateCompat.ACTION_PLAY_FROM_URI);
        if (text != null) {
            return stripTextForPreview(convertFromHtmlIfNecessary(textPart, text));
        }
        throw new PreviewExtractionException("Couldn't get text from part");
    }

    private String convertFromHtmlIfNecessary(Part textPart, String text) {
        return !MimeUtility.isSameMimeType(textPart.getMimeType(), "text/html") ? text : HtmlConverter.htmlToText(text);
    }

    private String stripTextForPreview(String text) {
        if (text == null) {
            return "";
        }
        String text2 = text.replaceAll("(?ms)^-- [\\r\\n]+.*", "").replaceAll("(?m)^----.*?$", "").replaceAll("(?m)^[#>].*$", "").replaceAll("(?m)^On .*wrote.?$", "").replaceAll("(?m)^.*\\w+:$", "").replaceAll("\\s*([-=_]{30,}+)\\s*", " ").replaceAll("https?://\\S+", "...").replaceAll("(\\r|\\n)+", " ").replaceAll("\\s+", " ").trim();
        return text2.length() > 512 ? text2.substring(0, 511) + "…" : text2;
    }
}
