package com.fsck.k9.message;

import java.io.Serializable;

public class InsertableHtmlContent implements Serializable {
    private static final long serialVersionUID = 2397327034L;
    private int footerInsertionPoint = 0;
    private int headerInsertionPoint = 0;
    private InsertionLocation insertionLocation = InsertionLocation.BEFORE_QUOTE;
    private StringBuilder quotedContent = new StringBuilder();
    private StringBuilder userContent = new StringBuilder();

    public enum InsertionLocation {
        BEFORE_QUOTE,
        AFTER_QUOTE
    }

    public void setHeaderInsertionPoint(int headerInsertionPoint2) {
        if (headerInsertionPoint2 < 0 || headerInsertionPoint2 > this.quotedContent.length()) {
            this.headerInsertionPoint = 0;
        } else {
            this.headerInsertionPoint = headerInsertionPoint2;
        }
    }

    public void setFooterInsertionPoint(int footerInsertionPoint2) {
        int len = this.quotedContent.length();
        if (footerInsertionPoint2 < 0 || footerInsertionPoint2 > len) {
            this.footerInsertionPoint = len;
        } else {
            this.footerInsertionPoint = footerInsertionPoint2;
        }
    }

    public String getQuotedContent() {
        return this.quotedContent.toString();
    }

    public void setQuotedContent(StringBuilder content) {
        this.quotedContent = content;
    }

    public void insertIntoQuotedHeader(String content) {
        this.quotedContent.insert(this.headerInsertionPoint, content);
        this.footerInsertionPoint += content.length();
    }

    public void insertIntoQuotedFooter(String content) {
        this.quotedContent.insert(this.footerInsertionPoint, content);
        this.footerInsertionPoint += content.length();
    }

    public void clearQuotedContent() {
        this.quotedContent.setLength(0);
        this.footerInsertionPoint = 0;
        this.headerInsertionPoint = 0;
    }

    public void setUserContent(String content) {
        this.userContent = new StringBuilder(content);
    }

    public void setInsertionLocation(InsertionLocation insertionLocation2) {
        this.insertionLocation = insertionLocation2;
    }

    public int getInsertionPoint() {
        if (this.insertionLocation == InsertionLocation.BEFORE_QUOTE) {
            return this.headerInsertionPoint;
        }
        return this.footerInsertionPoint;
    }

    public int getFooterInsertionPoint() {
        return this.footerInsertionPoint;
    }

    public String toString() {
        int insertionPoint = getInsertionPoint();
        String result = this.quotedContent.insert(insertionPoint, this.userContent.toString()).toString();
        this.quotedContent.delete(insertionPoint, this.userContent.length() + insertionPoint);
        return result;
    }

    public String toDebugString() {
        return "InsertableHtmlContent{headerInsertionPoint=" + this.headerInsertionPoint + ", footerInsertionPoint=" + this.footerInsertionPoint + ", insertionLocation=" + this.insertionLocation + ", quotedContent=" + ((Object) this.quotedContent) + ", userContent=" + ((Object) this.userContent) + ", compiledResult=" + toString() + '}';
    }
}
