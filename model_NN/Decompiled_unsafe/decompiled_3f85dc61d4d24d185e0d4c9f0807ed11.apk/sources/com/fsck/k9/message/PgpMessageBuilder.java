package com.fsck.k9.message;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import com.fsck.k9.Globals;
import com.fsck.k9.activity.compose.ComposeCryptoStatus;
import com.fsck.k9.mail.Body;
import com.fsck.k9.mail.BodyPart;
import com.fsck.k9.mail.BoundaryGenerator;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.internet.BinaryTempFileBody;
import com.fsck.k9.mail.internet.MessageIdGenerator;
import com.fsck.k9.mail.internet.MimeBodyPart;
import com.fsck.k9.mail.internet.MimeMessage;
import com.fsck.k9.mail.internet.MimeMessageHelper;
import com.fsck.k9.mail.internet.MimeMultipart;
import com.fsck.k9.mail.internet.MimeUtility;
import com.fsck.k9.mail.internet.TextBody;
import com.fsck.k9.mailstore.BinaryMemoryBody;
import java.io.IOException;
import java.io.OutputStream;
import org.apache.commons.io.IOUtils;
import org.apache.james.mime4j.dom.field.ContentTypeField;
import org.apache.james.mime4j.util.MimeUtil;
import org.openintents.openpgp.OpenPgpError;
import org.openintents.openpgp.util.OpenPgpApi;

public class PgpMessageBuilder extends MessageBuilder {
    public static final int REQUEST_USER_INTERACTION = 1;
    private ComposeCryptoStatus cryptoStatus;
    private MimeMessage currentProcessedMimeMessage;
    private OpenPgpApi openPgpApi;
    private boolean opportunisticSecondPass;
    private boolean opportunisticSkipEncryption;

    public static PgpMessageBuilder newInstance() {
        return new PgpMessageBuilder(Globals.getContext(), MessageIdGenerator.getInstance(), BoundaryGenerator.getInstance());
    }

    PgpMessageBuilder(Context context, MessageIdGenerator messageIdGenerator, BoundaryGenerator boundaryGenerator) {
        super(context, messageIdGenerator, boundaryGenerator);
    }

    public void setOpenPgpApi(OpenPgpApi openPgpApi2) {
        this.openPgpApi = openPgpApi2;
    }

    /* access modifiers changed from: protected */
    public void buildMessageInternal() {
        if (this.currentProcessedMimeMessage != null) {
            throw new IllegalStateException("message can only be built once!");
        } else if (this.cryptoStatus == null) {
            throw new IllegalStateException("PgpMessageBuilder must have cryptoStatus set before building!");
        } else if (this.cryptoStatus.isCryptoDisabled()) {
            throw new AssertionError("PgpMessageBuilder must not be used if crypto is disabled!");
        } else {
            try {
                if (!this.cryptoStatus.isProviderStateOk()) {
                    throw new MessagingException("OpenPGP Provider is not ready!");
                }
                this.currentProcessedMimeMessage = build();
                startOrContinueBuildMessage(null);
            } catch (MessagingException me2) {
                queueMessageBuildException(me2);
            }
        }
    }

    public void buildMessageOnActivityResult(int requestCode, @NonNull Intent userInteractionResult) {
        if (this.currentProcessedMimeMessage == null) {
            throw new AssertionError("build message from activity result must not be called individually");
        }
        startOrContinueBuildMessage(userInteractionResult);
    }

    private void startOrContinueBuildMessage(@Nullable Intent pgpApiIntent) {
        boolean shouldEncrypt;
        boolean z;
        boolean z2 = false;
        try {
            boolean shouldSign = this.cryptoStatus.isSigningEnabled();
            if (!this.cryptoStatus.isEncryptionEnabled() || this.opportunisticSkipEncryption) {
                shouldEncrypt = false;
            } else {
                shouldEncrypt = true;
            }
            boolean isPgpInlineMode = this.cryptoStatus.isPgpInlineModeEnabled();
            if (shouldSign || shouldEncrypt) {
                boolean isSimpleTextMessage = MimeUtility.isSameMimeType(ContentTypeField.TYPE_TEXT_PLAIN, this.currentProcessedMimeMessage.getMimeType());
                if (!isPgpInlineMode || isSimpleTextMessage) {
                    if (pgpApiIntent == null) {
                        pgpApiIntent = buildOpenPgpApiIntent(shouldSign, shouldEncrypt, isPgpInlineMode);
                    }
                    if (shouldEncrypt || isPgpInlineMode) {
                        z = true;
                    } else {
                        z = false;
                    }
                    if (shouldEncrypt || !isPgpInlineMode) {
                        z2 = true;
                    }
                    PendingIntent returnedPendingIntent = launchOpenPgpApiIntent(pgpApiIntent, z, z2, isPgpInlineMode);
                    if (returnedPendingIntent != null) {
                        queueMessageBuildPendingIntent(returnedPendingIntent, 1);
                    } else if (!this.opportunisticSkipEncryption || this.opportunisticSecondPass) {
                        queueMessageBuildSuccess(this.currentProcessedMimeMessage);
                    } else {
                        this.opportunisticSecondPass = true;
                        startOrContinueBuildMessage(null);
                    }
                } else {
                    throw new MessagingException("Attachments are not supported in PGP/INLINE format!");
                }
            }
        } catch (MessagingException me2) {
            queueMessageBuildException(me2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    @NonNull
    private Intent buildOpenPgpApiIntent(boolean shouldSign, boolean shouldEncrypt, boolean isPgpInlineMode) throws MessagingException {
        Intent pgpApiIntent;
        if (!shouldEncrypt) {
            pgpApiIntent = new Intent(isPgpInlineMode ? OpenPgpApi.ACTION_SIGN : OpenPgpApi.ACTION_DETACHED_SIGN);
        } else if (!shouldSign) {
            throw new IllegalStateException("encrypt-only is not supported at this point and should never happen!");
        } else {
            pgpApiIntent = new Intent(OpenPgpApi.ACTION_SIGN_AND_ENCRYPT);
            long[] encryptKeyIds = this.cryptoStatus.getEncryptKeyIds();
            if (encryptKeyIds != null) {
                pgpApiIntent.putExtra("key_ids", encryptKeyIds);
            }
            if (!isDraft()) {
                String[] encryptRecipientAddresses = this.cryptoStatus.getRecipientAddresses();
                if (!(encryptRecipientAddresses != null && encryptRecipientAddresses.length > 0)) {
                    throw new MessagingException("encryption is enabled, but no recipient specified!");
                }
                pgpApiIntent.putExtra(OpenPgpApi.EXTRA_USER_IDS, encryptRecipientAddresses);
                pgpApiIntent.putExtra(OpenPgpApi.EXTRA_ENCRYPT_OPPORTUNISTIC, this.cryptoStatus.isEncryptionOpportunistic());
            }
        }
        if (shouldSign) {
            pgpApiIntent.putExtra(OpenPgpApi.EXTRA_SIGN_KEY_ID, this.cryptoStatus.getSigningKeyId());
        }
        pgpApiIntent.putExtra(OpenPgpApi.EXTRA_REQUEST_ASCII_ARMOR, true);
        return pgpApiIntent;
    }

    private PendingIntent launchOpenPgpApiIntent(@NonNull Intent openPgpIntent, boolean captureOutputPart, boolean capturedOutputPartIs7Bit, boolean writeBodyContentOnly) throws MessagingException {
        MimeBodyPart bodyPart = this.currentProcessedMimeMessage.toBodyPart();
        String[] contentType = this.currentProcessedMimeMessage.getHeader("Content-Type");
        if (contentType.length > 0) {
            bodyPart.setHeader("Content-Type", contentType[0]);
        }
        OpenPgpApi.OpenPgpDataSource dataSource = createOpenPgpDataSourceFromBodyPart(bodyPart, writeBodyContentOnly);
        BinaryTempFileBody pgpResultTempBody = null;
        OutputStream outputStream = null;
        if (captureOutputPart) {
            try {
                BinaryTempFileBody pgpResultTempBody2 = new BinaryTempFileBody(capturedOutputPartIs7Bit ? MimeUtil.ENC_7BIT : MimeUtil.ENC_8BIT);
                try {
                    outputStream = pgpResultTempBody2.getOutputStream();
                    pgpResultTempBody = pgpResultTempBody2;
                } catch (IOException e) {
                    e = e;
                    throw new MessagingException("could not allocate temp file for storage!", e);
                }
            } catch (IOException e2) {
                e = e2;
                throw new MessagingException("could not allocate temp file for storage!", e);
            }
        }
        Intent result = this.openPgpApi.executeApi(openPgpIntent, dataSource, outputStream);
        switch (result.getIntExtra(OpenPgpApi.RESULT_CODE, 0)) {
            case 0:
                OpenPgpError error = (OpenPgpError) result.getParcelableExtra(OpenPgpApi.RESULT_ERROR);
                if (error == null) {
                    throw new MessagingException("internal openpgp api error");
                }
                if (error.getErrorId() == 4) {
                    skipEncryptingMessage();
                    return null;
                }
                throw new MessagingException(error.getMessage());
            case 1:
                mimeBuildMessage(result, bodyPart, pgpResultTempBody);
                return null;
            case 2:
                PendingIntent returnedPendingIntent = (PendingIntent) result.getParcelableExtra(OpenPgpApi.RESULT_INTENT);
                if (returnedPendingIntent != null) {
                    return returnedPendingIntent;
                }
                throw new MessagingException("openpgp api needs user interaction, but returned no pendingintent!");
            default:
                throw new IllegalStateException("unreachable code segment reached");
        }
    }

    @NonNull
    private OpenPgpApi.OpenPgpDataSource createOpenPgpDataSourceFromBodyPart(final MimeBodyPart bodyPart, final boolean writeBodyContentOnly) throws MessagingException {
        return new OpenPgpApi.OpenPgpDataSource() {
            public void writeTo(OutputStream os) throws IOException {
                try {
                    if (writeBodyContentOnly) {
                        IOUtils.copy(bodyPart.getBody().getInputStream(), os);
                    } else {
                        bodyPart.writeTo(os);
                    }
                } catch (MessagingException e) {
                    throw new IOException(e);
                }
            }
        };
    }

    private void mimeBuildMessage(@NonNull Intent result, @NonNull MimeBodyPart bodyPart, @Nullable BinaryTempFileBody pgpResultTempBody) throws MessagingException {
        if (pgpResultTempBody == null) {
            if (this.cryptoStatus.isPgpInlineModeEnabled() || (this.cryptoStatus.isEncryptionEnabled() && !this.opportunisticSkipEncryption)) {
                throw new AssertionError("encryption or pgp/inline is enabled, but no output part!");
            }
            mimeBuildSignedMessage(bodyPart, result);
        } else if (this.cryptoStatus.isPgpInlineModeEnabled()) {
            mimeBuildInlineMessage(pgpResultTempBody);
        } else {
            mimeBuildEncryptedMessage(pgpResultTempBody);
        }
    }

    private void mimeBuildSignedMessage(@NonNull BodyPart signedBodyPart, Intent result) throws MessagingException {
        if (!this.cryptoStatus.isSigningEnabled()) {
            throw new IllegalStateException("call to mimeBuildSignedMessage while signing isn't enabled!");
        }
        byte[] signedData = result.getByteArrayExtra("detached_signature");
        if (signedData == null) {
            throw new MessagingException("didn't find expected RESULT_DETACHED_SIGNATURE in api call result");
        }
        MimeMultipart multipartSigned = createMimeMultipart();
        multipartSigned.setSubType("signed");
        multipartSigned.addBodyPart(signedBodyPart);
        multipartSigned.addBodyPart(new MimeBodyPart(new BinaryMemoryBody(signedData, MimeUtil.ENC_7BIT), "application/pgp-signature"));
        MimeMessageHelper.setBody(this.currentProcessedMimeMessage, multipartSigned);
        String contentType = String.format("multipart/signed; boundary=\"%s\";\r\n  protocol=\"application/pgp-signature\"", multipartSigned.getBoundary());
        if (result.hasExtra(OpenPgpApi.RESULT_SIGNATURE_MICALG)) {
            String micAlgParameter = result.getStringExtra(OpenPgpApi.RESULT_SIGNATURE_MICALG);
            contentType = contentType + String.format("; micalg=\"%s\"", micAlgParameter);
        } else {
            Log.e("k9", "missing micalg parameter for pgp multipart/signed!");
        }
        this.currentProcessedMimeMessage.setHeader("Content-Type", contentType);
    }

    private void mimeBuildEncryptedMessage(@NonNull Body encryptedBodyPart) throws MessagingException {
        if (!this.cryptoStatus.isEncryptionEnabled()) {
            throw new IllegalStateException("call to mimeBuildEncryptedMessage while encryption isn't enabled!");
        }
        MimeMultipart multipartEncrypted = createMimeMultipart();
        multipartEncrypted.setSubType("encrypted");
        multipartEncrypted.addBodyPart(new MimeBodyPart(new TextBody("Version: 1"), "application/pgp-encrypted"));
        multipartEncrypted.addBodyPart(new MimeBodyPart(encryptedBodyPart, MimeUtility.DEFAULT_ATTACHMENT_MIME_TYPE));
        MimeMessageHelper.setBody(this.currentProcessedMimeMessage, multipartEncrypted);
        this.currentProcessedMimeMessage.setHeader("Content-Type", String.format("multipart/encrypted; boundary=\"%s\";\r\n  protocol=\"application/pgp-encrypted\"", multipartEncrypted.getBoundary()));
    }

    private void mimeBuildInlineMessage(@NonNull Body inlineBodyPart) throws MessagingException {
        if (!this.cryptoStatus.isPgpInlineModeEnabled()) {
            throw new IllegalStateException("call to mimeBuildInlineMessage while pgp/inline isn't enabled!");
        }
        if (!this.cryptoStatus.isEncryptionEnabled()) {
            inlineBodyPart.setEncoding(MimeUtil.ENC_QUOTED_PRINTABLE);
        }
        MimeMessageHelper.setBody(this.currentProcessedMimeMessage, inlineBodyPart);
    }

    private void skipEncryptingMessage() throws MessagingException {
        if (!this.cryptoStatus.isEncryptionOpportunistic()) {
            throw new AssertionError("Got opportunistic error, but encryption wasn't supposed to be opportunistic!");
        }
        this.opportunisticSkipEncryption = true;
    }

    public void setCryptoStatus(ComposeCryptoStatus cryptoStatus2) {
        this.cryptoStatus = cryptoStatus2;
    }
}
