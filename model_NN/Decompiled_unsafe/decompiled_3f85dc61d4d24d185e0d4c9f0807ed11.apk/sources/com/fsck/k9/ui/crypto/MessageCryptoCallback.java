package com.fsck.k9.ui.crypto;

import android.content.Intent;
import android.content.IntentSender;

public interface MessageCryptoCallback {
    void onCryptoHelperProgress(int i, int i2);

    void onCryptoOperationsFinished(MessageCryptoAnnotations messageCryptoAnnotations);

    void startPendingIntentForCryptoHelper(IntentSender intentSender, int i, Intent intent, int i2, int i3, int i4);
}
