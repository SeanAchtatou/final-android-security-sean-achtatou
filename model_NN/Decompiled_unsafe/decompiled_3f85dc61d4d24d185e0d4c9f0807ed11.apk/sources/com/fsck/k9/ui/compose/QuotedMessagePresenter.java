package com.fsck.k9.ui.compose;

import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import com.fsck.k9.Account;
import com.fsck.k9.K9;
import com.fsck.k9.activity.MessageCompose;
import com.fsck.k9.helper.HtmlConverter;
import com.fsck.k9.helper.QuotedMessageHelper;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.Part;
import com.fsck.k9.mail.internet.MessageExtractor;
import com.fsck.k9.mail.internet.MimeUtility;
import com.fsck.k9.mailstore.AttachmentResolver;
import com.fsck.k9.mailstore.MessageViewInfo;
import com.fsck.k9.message.IdentityField;
import com.fsck.k9.message.InsertableHtmlContent;
import com.fsck.k9.message.MessageBuilder;
import com.fsck.k9.message.QuotedTextMode;
import com.fsck.k9.message.SimpleMessageFormat;
import com.fsck.k9.remotecontrol.K9RemoteControl;
import java.util.Map;
import org.apache.james.mime4j.dom.field.ContentTypeField;

public class QuotedMessagePresenter {
    private static final String STATE_KEY_FORCE_PLAIN_TEXT = "state:forcePlainText";
    private static final String STATE_KEY_HTML_QUOTE = "state:htmlQuote";
    private static final String STATE_KEY_QUOTED_TEXT_FORMAT = "state:quotedTextFormat";
    private static final String STATE_KEY_QUOTED_TEXT_MODE = "state:quotedTextShown";
    private static final int UNKNOWN_LENGTH = 0;
    private Account account;
    private boolean forcePlainText;
    private final MessageCompose messageCompose;
    private Account.QuoteStyle quoteStyle;
    private InsertableHtmlContent quotedHtmlContent;
    private SimpleMessageFormat quotedTextFormat;
    private QuotedTextMode quotedTextMode = QuotedTextMode.NONE;
    private final Resources resources;
    private final QuotedMessageMvpView view;

    public QuotedMessagePresenter(MessageCompose messageCompose2, QuotedMessageMvpView quotedMessageMvpView, Account account2) {
        this.messageCompose = messageCompose2;
        this.resources = messageCompose2.getResources();
        this.view = quotedMessageMvpView;
        onSwitchAccount(account2);
        this.quoteStyle = account2.getQuoteStyle();
        quotedMessageMvpView.setOnClickPresenter(this);
    }

    public void onSwitchAccount(Account account2) {
        this.account = account2;
    }

    public void showOrHideQuotedText(QuotedTextMode mode) {
        this.quotedTextMode = mode;
        this.view.showOrHideQuotedText(mode, this.quotedTextFormat);
    }

    public void populateUIWithQuotedMessage(MessageViewInfo messageViewInfo, boolean showQuotedText, MessageCompose.Action action) throws MessagingException {
        Account.MessageFormat origMessageFormat = this.account.getMessageFormat();
        if (this.forcePlainText || origMessageFormat == Account.MessageFormat.TEXT) {
            this.quotedTextFormat = SimpleMessageFormat.TEXT;
        } else if (origMessageFormat == Account.MessageFormat.AUTO) {
            this.quotedTextFormat = MimeUtility.findFirstPartByMimeType(messageViewInfo.rootPart, "text/html") == null ? SimpleMessageFormat.TEXT : SimpleMessageFormat.HTML;
        } else {
            this.quotedTextFormat = SimpleMessageFormat.HTML;
        }
        String content = QuotedMessageHelper.getBodyTextFromMessage(messageViewInfo.rootPart, this.quotedTextFormat);
        if (this.quotedTextFormat == SimpleMessageFormat.HTML) {
            if (this.account.isStripSignature() && (action == MessageCompose.Action.REPLY || action == MessageCompose.Action.REPLY_ALL)) {
                content = QuotedMessageHelper.stripSignatureForHtmlMessage(content);
            }
            this.quotedHtmlContent = QuotedMessageHelper.quoteOriginalHtmlMessage(this.resources, messageViewInfo.message, content, this.quoteStyle);
            this.view.setQuotedHtml(this.quotedHtmlContent.getQuotedContent(), AttachmentResolver.createFromPart(messageViewInfo.rootPart));
            this.view.setQuotedText(QuotedMessageHelper.quoteOriginalTextMessage(this.resources, messageViewInfo.message, QuotedMessageHelper.getBodyTextFromMessage(messageViewInfo.rootPart, SimpleMessageFormat.TEXT), this.quoteStyle, this.account.getQuotePrefix()));
        } else if (this.quotedTextFormat == SimpleMessageFormat.TEXT) {
            if (this.account.isStripSignature() && (action == MessageCompose.Action.REPLY || action == MessageCompose.Action.REPLY_ALL)) {
                content = QuotedMessageHelper.stripSignatureForTextMessage(content);
            }
            this.view.setQuotedText(QuotedMessageHelper.quoteOriginalTextMessage(this.resources, messageViewInfo.message, content, this.quoteStyle, this.account.getQuotePrefix()));
        }
        if (showQuotedText) {
            showOrHideQuotedText(QuotedTextMode.SHOW);
        } else {
            showOrHideQuotedText(QuotedTextMode.HIDE);
        }
    }

    public void builderSetProperties(MessageBuilder builder) {
        builder.setQuoteStyle(this.quoteStyle).setQuotedText(this.view.getQuotedText()).setQuotedTextMode(this.quotedTextMode).setQuotedHtmlContent(this.quotedHtmlContent).setReplyAfterQuote(this.account.isReplyAfterQuote());
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(STATE_KEY_QUOTED_TEXT_MODE, this.quotedTextMode);
        outState.putSerializable(STATE_KEY_HTML_QUOTE, this.quotedHtmlContent);
        outState.putSerializable(STATE_KEY_QUOTED_TEXT_FORMAT, this.quotedTextFormat);
        outState.putBoolean(STATE_KEY_FORCE_PLAIN_TEXT, this.forcePlainText);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        this.quotedHtmlContent = (InsertableHtmlContent) savedInstanceState.getSerializable(STATE_KEY_HTML_QUOTE);
        if (!(this.quotedHtmlContent == null || this.quotedHtmlContent.getQuotedContent() == null)) {
            this.view.setQuotedHtml(this.quotedHtmlContent.getQuotedContent(), null);
        }
        this.quotedTextFormat = (SimpleMessageFormat) savedInstanceState.getSerializable(STATE_KEY_QUOTED_TEXT_FORMAT);
        this.forcePlainText = savedInstanceState.getBoolean(STATE_KEY_FORCE_PLAIN_TEXT);
        showOrHideQuotedText((QuotedTextMode) savedInstanceState.getSerializable(STATE_KEY_QUOTED_TEXT_MODE));
    }

    public void processMessageToForward(MessageViewInfo messageViewInfo) throws MessagingException {
        this.quoteStyle = Account.QuoteStyle.HEADER;
        populateUIWithQuotedMessage(messageViewInfo, true, MessageCompose.Action.FORWARD);
    }

    public void initFromReplyToMessage(MessageViewInfo messageViewInfo, MessageCompose.Action action) throws MessagingException {
        populateUIWithQuotedMessage(messageViewInfo, this.account.isDefaultQuotedTextShown(), action);
    }

    public void processDraftMessage(MessageViewInfo messageViewInfo, Map<IdentityField, String> k9identity) {
        Account.QuoteStyle quoteStyle2;
        String showQuotedTextMode;
        QuotedTextMode quotedMode;
        if (k9identity.get(IdentityField.QUOTE_STYLE) != null) {
            quoteStyle2 = Account.QuoteStyle.valueOf(k9identity.get(IdentityField.QUOTE_STYLE));
        } else {
            quoteStyle2 = this.account.getQuoteStyle();
        }
        this.quoteStyle = quoteStyle2;
        int cursorPosition = 0;
        if (k9identity.containsKey(IdentityField.CURSOR_POSITION)) {
            try {
                cursorPosition = Integer.parseInt(k9identity.get(IdentityField.CURSOR_POSITION));
            } catch (Exception e) {
                Log.e("k9", "Could not parse cursor position for MessageCompose; continuing.", e);
            }
        }
        if (k9identity.containsKey(IdentityField.QUOTED_TEXT_MODE)) {
            showQuotedTextMode = k9identity.get(IdentityField.QUOTED_TEXT_MODE);
        } else {
            showQuotedTextMode = K9RemoteControl.K9_FOLDERS_NONE;
        }
        int bodyLength = k9identity.get(IdentityField.LENGTH) != null ? Integer.valueOf(k9identity.get(IdentityField.LENGTH)).intValue() : 0;
        int bodyOffset = k9identity.get(IdentityField.OFFSET) != null ? Integer.valueOf(k9identity.get(IdentityField.OFFSET)).intValue() : 0;
        Integer bodyFooterOffset = k9identity.get(IdentityField.FOOTER_OFFSET) != null ? Integer.valueOf(k9identity.get(IdentityField.FOOTER_OFFSET)) : null;
        Integer bodyPlainLength = k9identity.get(IdentityField.PLAIN_LENGTH) != null ? Integer.valueOf(k9identity.get(IdentityField.PLAIN_LENGTH)) : null;
        Integer bodyPlainOffset = k9identity.get(IdentityField.PLAIN_OFFSET) != null ? Integer.valueOf(k9identity.get(IdentityField.PLAIN_OFFSET)) : null;
        try {
            quotedMode = QuotedTextMode.valueOf(showQuotedTextMode);
        } catch (Exception e2) {
            quotedMode = QuotedTextMode.NONE;
        }
        String messageFormatString = k9identity.get(IdentityField.MESSAGE_FORMAT);
        Account.MessageFormat messageFormat = null;
        if (messageFormatString != null) {
            try {
                messageFormat = Account.MessageFormat.valueOf(messageFormatString);
            } catch (Exception e3) {
            }
        }
        if (messageFormat == null) {
            this.view.setMessageContentCharacters(QuotedMessageHelper.getBodyTextFromMessage(messageViewInfo.message, SimpleMessageFormat.TEXT));
            this.forcePlainText = true;
            showOrHideQuotedText(quotedMode);
            return;
        }
        if (messageFormat == Account.MessageFormat.HTML) {
            Part part = MimeUtility.findFirstPartByMimeType(messageViewInfo.message, "text/html");
            if (part != null) {
                this.quotedTextFormat = SimpleMessageFormat.HTML;
                String text = MessageExtractor.getTextFromPart(part);
                if (K9.DEBUG) {
                    Log.d("k9", "Loading message with offset " + bodyOffset + ", length " + bodyLength + ". Text length is " + text.length() + ".");
                }
                if (bodyOffset + bodyLength > text.length()) {
                    Log.d("k9", "The identity field from the draft contains an invalid LENGTH/OFFSET");
                    bodyOffset = 0;
                    bodyLength = 0;
                }
                this.view.setMessageContentCharacters(HtmlConverter.htmlToText(text.substring(bodyOffset, bodyOffset + bodyLength)));
                StringBuilder quotedHTML = new StringBuilder();
                quotedHTML.append(text.substring(0, bodyOffset));
                quotedHTML.append(text.substring(bodyOffset + bodyLength));
                if (quotedHTML.length() > 0) {
                    this.quotedHtmlContent = new InsertableHtmlContent();
                    this.quotedHtmlContent.setQuotedContent(quotedHTML);
                    this.quotedHtmlContent.setHeaderInsertionPoint(bodyOffset);
                    if (bodyFooterOffset != null) {
                        this.quotedHtmlContent.setFooterInsertionPoint(bodyFooterOffset.intValue());
                    } else {
                        this.quotedHtmlContent.setFooterInsertionPoint(bodyOffset);
                    }
                    this.view.setQuotedHtml(this.quotedHtmlContent.getQuotedContent(), AttachmentResolver.createFromPart(messageViewInfo.rootPart));
                }
            }
            if (!(bodyPlainOffset == null || bodyPlainLength == null)) {
                processSourceMessageText(messageViewInfo.rootPart, bodyPlainOffset.intValue(), bodyPlainLength.intValue(), false);
            }
        } else if (messageFormat == Account.MessageFormat.TEXT) {
            this.quotedTextFormat = SimpleMessageFormat.TEXT;
            processSourceMessageText(messageViewInfo.rootPart, bodyOffset, bodyLength, true);
        } else {
            Log.e("k9", "Unhandled message format.");
        }
        try {
            this.view.setMessageContentCursorPosition(cursorPosition);
        } catch (Exception e4) {
            Log.e("k9", "Could not set cursor position in MessageCompose; ignoring.", e4);
        }
        showOrHideQuotedText(quotedMode);
    }

    private void processSourceMessageText(Part rootMessagePart, int bodyOffset, int bodyLength, boolean viewMessageContent) {
        Part textPart = MimeUtility.findFirstPartByMimeType(rootMessagePart, ContentTypeField.TYPE_TEXT_PLAIN);
        if (textPart != null) {
            String messageText = MessageExtractor.getTextFromPart(textPart);
            if (K9.DEBUG) {
                Log.d("k9", "Loading message with offset " + bodyOffset + ", length " + bodyLength + ". Text length is " + messageText.length() + ".");
            }
            if (bodyLength != 0) {
                try {
                    StringBuilder quotedText = new StringBuilder();
                    if (bodyOffset == 0 && messageText.substring(bodyLength, bodyLength + 4).equals("\r\n\r\n")) {
                        quotedText.append(messageText.substring(bodyLength + 4));
                    } else if (bodyOffset + bodyLength != messageText.length() || !messageText.substring(bodyOffset - 2, bodyOffset).equals("\r\n")) {
                        quotedText.append(messageText.substring(0, bodyOffset));
                        quotedText.append(messageText.substring(bodyOffset + bodyLength));
                    } else {
                        quotedText.append(messageText.substring(0, bodyOffset - 2));
                    }
                    this.view.setQuotedText(quotedText.toString());
                    messageText = messageText.substring(bodyOffset, bodyOffset + bodyLength);
                } catch (IndexOutOfBoundsException e) {
                    Log.d("k9", "The identity field from the draft contains an invalid bodyOffset/bodyLength");
                }
            }
            if (viewMessageContent) {
                this.view.setMessageContentCharacters(messageText);
            }
        }
    }

    public void onClickShowQuotedText() {
        showOrHideQuotedText(QuotedTextMode.SHOW);
        this.messageCompose.updateMessageFormat();
        this.messageCompose.saveDraftEventually();
    }

    public void onClickDeleteQuotedText() {
        showOrHideQuotedText(QuotedTextMode.HIDE);
        this.messageCompose.updateMessageFormat();
        this.messageCompose.saveDraftEventually();
    }

    public void onClickEditQuotedText() {
        this.forcePlainText = true;
        this.messageCompose.loadQuotedTextForEdit();
    }

    public boolean includeQuotedText() {
        return this.quotedTextMode == QuotedTextMode.SHOW;
    }

    public boolean isForcePlainText() {
        return this.forcePlainText;
    }

    public boolean isQuotedTextText() {
        return this.quotedTextFormat == SimpleMessageFormat.TEXT;
    }
}
