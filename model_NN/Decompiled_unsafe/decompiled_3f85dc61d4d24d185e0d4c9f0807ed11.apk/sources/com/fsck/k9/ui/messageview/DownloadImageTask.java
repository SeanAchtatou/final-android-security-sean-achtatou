package com.fsck.k9.ui.messageview;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
import com.fsck.k9.K9;
import com.fsck.k9.R;
import com.fsck.k9.helper.FileHelper;
import com.fsck.k9.helper.UrlEncodingHelper;
import com.fsck.k9.mail.internet.MimeUtility;
import com.fsck.k9.provider.AttachmentProvider;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import org.apache.commons.io.IOUtils;

@Deprecated
class DownloadImageTask extends AsyncTask<String, Void, String> {
    private static final String[] ATTACHMENT_PROJECTION = {AttachmentProvider.AttachmentProviderColumns._ID, AttachmentProvider.AttachmentProviderColumns.DISPLAY_NAME};
    private static final String DEFAULT_FILE_NAME = "saved_image";
    private static final int DISPLAY_NAME_INDEX = 1;
    private final Context context;

    public DownloadImageTask(Context context2) {
        this.context = context2.getApplicationContext();
    }

    /* access modifiers changed from: protected */
    public String doInBackground(String... params) {
        String url = params[0];
        try {
            if (url.startsWith("http")) {
                return downloadAndStoreImage(url);
            }
            return fetchAndStoreImage(url);
        } catch (Exception e) {
            Log.e("k9", "Error while downloading image", e);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(String fileName) {
        boolean errorSavingFile;
        String text;
        if (fileName == null) {
            errorSavingFile = true;
        } else {
            errorSavingFile = false;
        }
        if (errorSavingFile) {
            text = this.context.getString(R.string.image_saving_failed);
        } else {
            text = this.context.getString(R.string.image_saved_as, fileName);
        }
        Toast.makeText(this.context, text, 1).show();
    }

    private String downloadAndStoreImage(String urlString) throws IOException {
        URL url = new URL(urlString);
        URLConnection conn = url.openConnection();
        InputStream in = conn.getInputStream();
        try {
            String fileName = getFileNameFromUrl(url);
            return writeFileToStorage(getFileNameWithExtension(fileName, getMimeType(conn, fileName)), in);
        } finally {
            in.close();
        }
    }

    private String getFileNameFromUrl(URL url) {
        String path = url.getPath();
        int start = path.lastIndexOf("/");
        if (start == -1 || start + 1 >= path.length()) {
            return DEFAULT_FILE_NAME;
        }
        return UrlEncodingHelper.decodeUtf8(path.substring(start + 1));
    }

    private String getMimeType(URLConnection conn, String fileName) {
        if (fileName.indexOf(46) == -1) {
            return conn.getContentType();
        }
        return null;
    }

    private String fetchAndStoreImage(String urlString) throws IOException {
        ContentResolver contentResolver = this.context.getContentResolver();
        Uri uri = Uri.parse(urlString);
        String fileName = getFileNameFromContentProvider(contentResolver, uri);
        String mimeType = getMimeType(contentResolver, uri, fileName);
        InputStream in = contentResolver.openInputStream(uri);
        try {
            return writeFileToStorage(getFileNameWithExtension(fileName, mimeType), in);
        } finally {
            in.close();
        }
    }

    private String getMimeType(ContentResolver contentResolver, Uri uri, String fileName) {
        if (fileName.indexOf(46) == -1) {
            return contentResolver.getType(uri);
        }
        return null;
    }

    private String getFileNameFromContentProvider(ContentResolver contentResolver, Uri uri) {
        String displayName = DEFAULT_FILE_NAME;
        Cursor cursor = contentResolver.query(uri, ATTACHMENT_PROJECTION, null, null, null);
        if (cursor != null) {
            try {
                if (cursor.moveToNext() && !cursor.isNull(1)) {
                    displayName = cursor.getString(1);
                }
            } finally {
                cursor.close();
            }
        }
        return displayName;
    }

    private String getFileNameWithExtension(String fileName, String mimeType) {
        String extensionFromMimeType;
        if (fileName.indexOf(46) != -1) {
            return fileName;
        }
        String extension = "jpeg";
        if (!(mimeType == null || (extensionFromMimeType = MimeUtility.getExtensionByMimeType(mimeType)) == null)) {
            extension = extensionFromMimeType;
        }
        return fileName + "." + extension;
    }

    /* JADX INFO: finally extract failed */
    private String writeFileToStorage(String fileName, InputStream in) throws IOException {
        File file = FileHelper.createUniqueFile(new File(K9.getAttachmentDefaultPath()), FileHelper.sanitizeFilename(fileName));
        FileOutputStream out = new FileOutputStream(file);
        try {
            IOUtils.copy(in, out);
            out.flush();
            out.close();
            return file.getName();
        } catch (Throwable th) {
            out.close();
            throw th;
        }
    }
}
