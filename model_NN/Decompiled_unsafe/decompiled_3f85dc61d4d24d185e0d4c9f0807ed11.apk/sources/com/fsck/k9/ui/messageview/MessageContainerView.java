package com.fsck.k9.ui.messageview;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.fsck.k9.R;
import com.fsck.k9.helper.ClipboardManager;
import com.fsck.k9.helper.Contacts;
import com.fsck.k9.helper.HtmlConverter;
import com.fsck.k9.helper.Utility;
import com.fsck.k9.mail.Address;
import com.fsck.k9.mailstore.AttachmentResolver;
import com.fsck.k9.mailstore.AttachmentViewInfo;
import com.fsck.k9.mailstore.MessageViewInfo;
import com.fsck.k9.view.MessageHeader;
import com.fsck.k9.view.MessageWebView;
import java.util.HashMap;
import java.util.Map;
import org.apache.james.mime4j.dom.field.ContentTypeField;

public class MessageContainerView extends LinearLayout implements MessageHeader.OnLayoutChangedListener, View.OnCreateContextMenuListener {
    private static final int MENU_ITEM_EMAIL_COPY = 3;
    private static final int MENU_ITEM_EMAIL_SAVE = 2;
    private static final int MENU_ITEM_EMAIL_SEND = 1;
    private static final int MENU_ITEM_IMAGE_COPY = 3;
    private static final int MENU_ITEM_IMAGE_SAVE = 2;
    private static final int MENU_ITEM_IMAGE_VIEW = 1;
    private static final int MENU_ITEM_LINK_COPY = 3;
    private static final int MENU_ITEM_LINK_SHARE = 2;
    private static final int MENU_ITEM_LINK_VIEW = 1;
    private static final int MENU_ITEM_PHONE_CALL = 1;
    private static final int MENU_ITEM_PHONE_COPY = 3;
    private static final int MENU_ITEM_PHONE_SAVE = 2;
    private AttachmentViewCallback attachmentCallback;
    private Map<AttachmentViewInfo, AttachmentView> attachments = new HashMap();
    private AttachmentResolver currentAttachmentResolver;
    private String currentHtmlText;
    private boolean hasHiddenExternalImages;
    private LinearLayout mAttachments;
    private View mAttachmentsContainer;
    /* access modifiers changed from: private */
    public ClipboardManager mClipboardManager;
    private LayoutInflater mInflater;
    private MessageWebView mMessageContentView;
    private SavedState mSavedState;
    private boolean showingPictures;
    private TextView unsignedText;
    private View unsignedTextContainer;

    interface OnRenderingFinishedListener {
        void onLoadFinished();
    }

    public void onFinishInflate() {
        super.onFinishInflate();
        this.mAttachmentsContainer = findViewById(R.id.attachments_container);
        this.mAttachments = (LinearLayout) findViewById(R.id.attachments);
        this.unsignedTextContainer = findViewById(R.id.message_unsigned_container);
        this.unsignedText = (TextView) findViewById(R.id.message_unsigned_text);
        this.showingPictures = false;
        Context context = getContext();
        this.mInflater = LayoutInflater.from(context);
        this.mClipboardManager = ClipboardManager.getInstance(context);
    }

    public void setWebView(ViewGroup parent) {
        this.mMessageContentView = (MessageWebView) parent.findViewById(R.id.message_content);
        if (!isInEditMode()) {
            this.mMessageContentView.configure();
        }
        this.mMessageContentView.setOnCreateContextMenuListener(this);
        this.mMessageContentView.setVisibility(0);
    }

    public void setAtachmentsView(ViewGroup parent) {
        this.mAttachmentsContainer = parent.findViewById(R.id.attachments_container);
        this.mAttachments = (LinearLayout) parent.findViewById(R.id.attachments);
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        String string;
        super.onCreateContextMenu(menu);
        WebView.HitTestResult result = ((WebView) v).getHitTestResult();
        if (result != null) {
            int type = result.getType();
            Context context = getContext();
            switch (type) {
                case 2:
                    final String phoneNumber = result.getExtra();
                    MenuItem.OnMenuItemClickListener listener = new MenuItem.OnMenuItemClickListener() {
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case 1:
                                    MessageContainerView.this.startActivityIfAvailable(MessageContainerView.this.getContext(), new Intent("android.intent.action.VIEW", Uri.parse("tel:" + phoneNumber)));
                                    return true;
                                case 2:
                                    Contacts.getInstance(MessageContainerView.this.getContext()).addPhoneContact(phoneNumber);
                                    return true;
                                case 3:
                                    MessageContainerView.this.mClipboardManager.setText(MessageContainerView.this.getContext().getString(R.string.webview_contextmenu_phone_clipboard_label), phoneNumber);
                                    return true;
                                default:
                                    return true;
                            }
                        }
                    };
                    menu.setHeaderTitle(phoneNumber);
                    menu.add(0, 1, 0, context.getString(R.string.webview_contextmenu_phone_call_action)).setOnMenuItemClickListener(listener);
                    menu.add(0, 2, 1, context.getString(R.string.webview_contextmenu_phone_save_action)).setOnMenuItemClickListener(listener);
                    menu.add(0, 3, 2, context.getString(R.string.webview_contextmenu_phone_copy_action)).setOnMenuItemClickListener(listener);
                    return;
                case 3:
                case 6:
                default:
                    return;
                case 4:
                    final String email = result.getExtra();
                    MenuItem.OnMenuItemClickListener listener2 = new MenuItem.OnMenuItemClickListener() {
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case 1:
                                    MessageContainerView.this.startActivityIfAvailable(MessageContainerView.this.getContext(), new Intent("android.intent.action.VIEW", Uri.parse("mailto:" + email)));
                                    return true;
                                case 2:
                                    Contacts.getInstance(MessageContainerView.this.getContext()).createContact(new Address(email));
                                    return true;
                                case 3:
                                    MessageContainerView.this.mClipboardManager.setText(MessageContainerView.this.getContext().getString(R.string.webview_contextmenu_email_clipboard_label), email);
                                    return true;
                                default:
                                    return true;
                            }
                        }
                    };
                    menu.setHeaderTitle(email);
                    menu.add(0, 1, 0, context.getString(R.string.webview_contextmenu_email_send_action)).setOnMenuItemClickListener(listener2);
                    menu.add(0, 2, 1, context.getString(R.string.webview_contextmenu_email_save_action)).setOnMenuItemClickListener(listener2);
                    menu.add(0, 3, 2, context.getString(R.string.webview_contextmenu_email_copy_action)).setOnMenuItemClickListener(listener2);
                    return;
                case 5:
                case 8:
                    final String url = getUriForExternalAccess(result.getExtra());
                    if (url != null) {
                        final boolean externalImage = url.startsWith("http");
                        MenuItem.OnMenuItemClickListener listener3 = new MenuItem.OnMenuItemClickListener() {
                            public boolean onMenuItemClick(MenuItem item) {
                                switch (item.getItemId()) {
                                    case 1:
                                        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(url));
                                        if (!externalImage) {
                                            intent.addFlags(1);
                                        }
                                        MessageContainerView.this.startActivityIfAvailable(MessageContainerView.this.getContext(), intent);
                                        break;
                                    case 2:
                                        new DownloadImageTask(MessageContainerView.this.getContext()).execute(url);
                                        break;
                                    case 3:
                                        MessageContainerView.this.mClipboardManager.setText(MessageContainerView.this.getContext().getString(R.string.webview_contextmenu_image_clipboard_label), url);
                                        break;
                                }
                                return true;
                            }
                        };
                        if (!externalImage) {
                            url = context.getString(R.string.webview_contextmenu_image_title);
                        }
                        menu.setHeaderTitle(url);
                        menu.add(0, 1, 0, context.getString(R.string.webview_contextmenu_image_view_action)).setOnMenuItemClickListener(listener3);
                        if (externalImage) {
                            string = context.getString(R.string.webview_contextmenu_image_download_action);
                        } else {
                            string = context.getString(R.string.webview_contextmenu_image_save_action);
                        }
                        menu.add(0, 2, 1, string).setOnMenuItemClickListener(listener3);
                        if (externalImage) {
                            menu.add(0, 3, 2, context.getString(R.string.webview_contextmenu_image_copy_action)).setOnMenuItemClickListener(listener3);
                            return;
                        }
                        return;
                    }
                    return;
                case 7:
                    final String url2 = result.getExtra();
                    MenuItem.OnMenuItemClickListener listener4 = new MenuItem.OnMenuItemClickListener() {
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case 1:
                                    MessageContainerView.this.startActivityIfAvailable(MessageContainerView.this.getContext(), new Intent("android.intent.action.VIEW", Uri.parse(url2)));
                                    return true;
                                case 2:
                                    Intent intent = new Intent("android.intent.action.SEND");
                                    intent.setType(ContentTypeField.TYPE_TEXT_PLAIN);
                                    intent.putExtra("android.intent.extra.TEXT", url2);
                                    MessageContainerView.this.startActivityIfAvailable(MessageContainerView.this.getContext(), intent);
                                    return true;
                                case 3:
                                    MessageContainerView.this.mClipboardManager.setText(MessageContainerView.this.getContext().getString(R.string.webview_contextmenu_link_clipboard_label), url2);
                                    return true;
                                default:
                                    return true;
                            }
                        }
                    };
                    menu.setHeaderTitle(url2);
                    menu.add(0, 1, 0, context.getString(R.string.webview_contextmenu_link_view_action)).setOnMenuItemClickListener(listener4);
                    menu.add(0, 2, 1, context.getString(R.string.webview_contextmenu_link_share_action)).setOnMenuItemClickListener(listener4);
                    menu.add(0, 3, 2, context.getString(R.string.webview_contextmenu_link_copy_action)).setOnMenuItemClickListener(listener4);
                    return;
            }
        }
    }

    private String getUriForExternalAccess(String url) {
        if (!url.startsWith("cid:")) {
            return url;
        }
        AttachmentViewInfo attachment = getAttachmentByContentId(Uri.parse(url).getSchemeSpecificPart());
        if (attachment == null) {
            return null;
        }
        return attachment.internalUri.toString();
    }

    private AttachmentViewInfo getAttachmentByContentId(String cid) {
        for (AttachmentViewInfo attachment : this.attachments.keySet()) {
            if (cid.equals(attachment.part.getContentId())) {
                return attachment;
            }
        }
        return null;
    }

    /* access modifiers changed from: private */
    public void startActivityIfAvailable(Context context, Intent intent) {
        try {
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(context, (int) R.string.error_activity_not_found, 1).show();
        }
    }

    public MessageContainerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public boolean isShowingPictures() {
        return this.showingPictures;
    }

    private void setLoadPictures(boolean enable) {
        this.mMessageContentView.blockNetworkData(!enable);
        this.showingPictures = enable;
    }

    public void showPictures() {
        setLoadPictures(true);
        refreshDisplayedContent();
    }

    public void enableAttachmentButtons() {
        for (AttachmentView attachmentView : this.attachments.values()) {
            attachmentView.enableButtons();
        }
    }

    public void disableAttachmentButtons() {
        for (AttachmentView attachmentView : this.attachments.values()) {
            attachmentView.disableButtons();
        }
    }

    public void displayMessageViewContainer(MessageViewInfo messageViewInfo, final OnRenderingFinishedListener onRenderingFinishedListener, boolean automaticallyLoadPictures, AttachmentViewCallback attachmentCallback2) {
        this.attachmentCallback = attachmentCallback2;
        resetView();
        renderAttachments(messageViewInfo);
        if (this.mSavedState != null) {
            if (this.mSavedState.showingPictures) {
                setLoadPictures(true);
            }
            this.mSavedState = null;
        }
        String textToDisplay = messageViewInfo.text;
        if (textToDisplay != null && !isShowingPictures() && Utility.hasExternalImages(textToDisplay)) {
            if (automaticallyLoadPictures) {
                setLoadPictures(true);
            } else {
                this.hasHiddenExternalImages = true;
            }
        }
        if (textToDisplay == null) {
            textToDisplay = HtmlConverter.wrapStatusMessage(getContext().getString(R.string.webview_empty_message));
        }
        displayHtmlContentWithInlineAttachments(textToDisplay, messageViewInfo.attachmentResolver, new MessageWebView.OnPageFinishedListener() {
            public void onPageFinished() {
                onRenderingFinishedListener.onLoadFinished();
            }
        });
        if (!TextUtils.isEmpty(messageViewInfo.extraText)) {
            this.unsignedTextContainer.setVisibility(0);
            this.unsignedText.setText(messageViewInfo.extraText);
        }
    }

    public boolean hasHiddenExternalImages() {
        return this.hasHiddenExternalImages;
    }

    private void displayHtmlContentWithInlineAttachments(String htmlText, AttachmentResolver attachmentResolver, MessageWebView.OnPageFinishedListener onPageFinishedListener) {
        this.currentHtmlText = htmlText;
        this.currentAttachmentResolver = attachmentResolver;
        this.mMessageContentView.displayHtmlContentWithInlineAttachments(htmlText, attachmentResolver, onPageFinishedListener);
    }

    private void refreshDisplayedContent() {
        this.mMessageContentView.displayHtmlContentWithInlineAttachments(this.currentHtmlText, this.currentAttachmentResolver, null);
    }

    private void clearDisplayedContent() {
        this.mMessageContentView.displayHtmlContentWithInlineAttachments("", null, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.widget.LinearLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public void renderAttachments(MessageViewInfo messageViewInfo) {
        if (messageViewInfo.attachments != null) {
            for (AttachmentViewInfo attachment : messageViewInfo.attachments) {
                AttachmentView view = (AttachmentView) this.mInflater.inflate((int) R.layout.message_view_attachment, (ViewGroup) this.mAttachments, false);
                view.setCallback(this.attachmentCallback);
                view.setAttachment(attachment);
                this.attachments.put(attachment, view);
                this.mAttachments.addView(view);
            }
        }
        if (messageViewInfo.extraAttachments != null) {
            for (AttachmentViewInfo attachment2 : messageViewInfo.extraAttachments) {
                LockedAttachmentView view2 = (LockedAttachmentView) this.mInflater.inflate((int) R.layout.message_view_attachment_locked, (ViewGroup) this.mAttachments, false);
                view2.setCallback(this.attachmentCallback);
                view2.setAttachment(attachment2);
                this.mAttachments.addView(view2);
            }
        }
    }

    public void zoom(KeyEvent event) {
        if (event.isShiftPressed()) {
            this.mMessageContentView.zoomIn();
        } else {
            this.mMessageContentView.zoomOut();
        }
    }

    public void beginSelectingText() {
        this.mMessageContentView.emulateShiftHeld();
    }

    public void resetView() {
        setLoadPictures(false);
        this.mAttachments.removeAllViews();
        this.currentHtmlText = null;
        this.currentAttachmentResolver = null;
        clearDisplayedContent();
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.attachmentViewVisible = this.mAttachmentsContainer != null && this.mAttachmentsContainer.getVisibility() == 0;
        savedState.showingPictures = this.showingPictures;
        return savedState;
    }

    public void onRestoreInstanceState(Parcelable state) {
        if (!(state instanceof SavedState)) {
            super.onRestoreInstanceState(state);
            return;
        }
        SavedState savedState = (SavedState) state;
        super.onRestoreInstanceState(savedState.getSuperState());
        this.mSavedState = savedState;
    }

    public void onLayoutChanged() {
        if (this.mMessageContentView != null) {
            this.mMessageContentView.invalidate();
        }
    }

    public void enableAttachmentButtons(AttachmentViewInfo attachment) {
        getAttachmentView(attachment).enableButtons();
    }

    public void disableAttachmentButtons(AttachmentViewInfo attachment) {
        getAttachmentView(attachment).disableButtons();
    }

    public void refreshAttachmentThumbnail(AttachmentViewInfo attachment) {
        getAttachmentView(attachment).refreshThumbnail();
    }

    private AttachmentView getAttachmentView(AttachmentViewInfo attachment) {
        return this.attachments.get(attachment);
    }

    static class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }

            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };
        boolean attachmentViewVisible;
        boolean showingPictures;

        SavedState(Parcelable superState) {
            super(superState);
        }

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        private SavedState(Parcel in) {
            super(in);
            boolean z;
            boolean z2 = true;
            if (in.readInt() != 0) {
                z = true;
            } else {
                z = false;
            }
            this.attachmentViewVisible = z;
            this.showingPictures = in.readInt() == 0 ? false : z2;
        }

        public void writeToParcel(Parcel out, int flags) {
            int i;
            int i2 = 1;
            super.writeToParcel(out, flags);
            if (this.attachmentViewVisible) {
                i = 1;
            } else {
                i = 0;
            }
            out.writeInt(i);
            if (!this.showingPictures) {
                i2 = 0;
            }
            out.writeInt(i2);
        }
    }
}
