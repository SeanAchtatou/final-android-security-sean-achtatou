package com.fsck.k9.ui.crypto;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;
import com.fsck.k9.K9;
import com.fsck.k9.crypto.MessageDecryptVerifier;
import com.fsck.k9.mail.Address;
import com.fsck.k9.mail.Body;
import com.fsck.k9.mail.BodyPart;
import com.fsck.k9.mail.Flag;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.Multipart;
import com.fsck.k9.mail.Part;
import com.fsck.k9.mail.internet.MessageExtractor;
import com.fsck.k9.mail.internet.MimeBodyPart;
import com.fsck.k9.mail.internet.MimeMultipart;
import com.fsck.k9.mail.internet.SizeAware;
import com.fsck.k9.mail.internet.TextBody;
import com.fsck.k9.mailstore.CryptoResultAnnotation;
import com.fsck.k9.mailstore.LocalMessage;
import com.fsck.k9.mailstore.MessageHelper;
import com.fsck.k9.mailstore.MimePartStreamParser;
import com.fsck.k9.provider.DecryptedFileProvider;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;
import org.apache.commons.io.IOUtils;
import org.apache.james.mime4j.dom.field.ContentTypeField;
import org.openintents.openpgp.IOpenPgpService2;
import org.openintents.openpgp.OpenPgpDecryptionResult;
import org.openintents.openpgp.OpenPgpError;
import org.openintents.openpgp.OpenPgpSignatureResult;
import org.openintents.openpgp.util.OpenPgpApi;
import org.openintents.openpgp.util.OpenPgpServiceConnection;
import org.openintents.openpgp.util.OpenPgpUtils;

public class MessageCryptoHelper {
    private static final int INVALID_OPENPGP_RESULT_CODE = -1;
    private static final MimeBodyPart NO_REPLACEMENT_PART = null;
    public static final int PROGRESS_SIZE_THRESHOLD = 4096;
    public static final int REQUEST_CODE_USER_INTERACTION = 124;
    private OpenPgpDecryptionResult cachedDecryptionResult;
    @Nullable
    private MessageCryptoCallback callback;
    private final Object callbackLock = new Object();
    /* access modifiers changed from: private */
    public OpenPgpApi.CancelableBackgroundOperation cancelableBackgroundOperation;
    /* access modifiers changed from: private */
    public final Context context;
    /* access modifiers changed from: private */
    public CryptoPart currentCryptoPart;
    /* access modifiers changed from: private */
    public Intent currentCryptoResult;
    private LocalMessage currentMessage;
    private boolean isCancelled;
    private MessageCryptoAnnotations messageAnnotations;
    /* access modifiers changed from: private */
    public OpenPgpApi openPgpApi;
    private final String openPgpProviderPackage;
    private OpenPgpServiceConnection openPgpServiceConnection;
    private Deque<CryptoPart> partsToDecryptOrVerify = new ArrayDeque();
    private PendingIntent queuedPendingIntent;
    private MessageCryptoAnnotations queuedResult;
    private boolean secondPassStarted;
    private Intent userInteractionResultIntent;

    private enum CryptoPartType {
        PGP_INLINE,
        PGP_ENCRYPTED,
        PGP_SIGNED
    }

    public MessageCryptoHelper(Context context2, String openPgpProviderPackage2) {
        this.context = context2.getApplicationContext();
        this.openPgpProviderPackage = openPgpProviderPackage2;
        if (openPgpProviderPackage2 == null || "".equals(openPgpProviderPackage2)) {
            throw new IllegalStateException("MessageCryptoHelper must only be called with a openpgp provider!");
        }
    }

    public void asyncStartOrResumeProcessingMessage(LocalMessage message, MessageCryptoCallback callback2, OpenPgpDecryptionResult cachedDecryptionResult2) {
        if (this.currentMessage != null) {
            reattachCallback(message, callback2);
            return;
        }
        this.messageAnnotations = new MessageCryptoAnnotations();
        this.currentMessage = message;
        this.cachedDecryptionResult = cachedDecryptionResult2;
        this.callback = callback2;
        runFirstPass();
    }

    private void runFirstPass() {
        processFoundEncryptedParts(MessageDecryptVerifier.findEncryptedParts(this.currentMessage));
        decryptOrVerifyNextPart();
    }

    private void runSecondPass() {
        processFoundSignedParts(MessageDecryptVerifier.findSignedParts(this.currentMessage, this.messageAnnotations));
        addFoundInlinePgpParts(MessageDecryptVerifier.findPgpInlineParts(this.currentMessage));
        decryptOrVerifyNextPart();
    }

    private void processFoundEncryptedParts(List<Part> foundParts) {
        for (Part part : foundParts) {
            if (!MessageHelper.isCompletePartAvailable(part)) {
                addErrorAnnotation(part, CryptoResultAnnotation.CryptoError.OPENPGP_ENCRYPTED_BUT_INCOMPLETE, MessageHelper.createEmptyPart());
            } else if (MessageDecryptVerifier.isPgpMimeEncryptedOrSignedPart(part)) {
                this.partsToDecryptOrVerify.add(new CryptoPart(CryptoPartType.PGP_ENCRYPTED, part));
            } else {
                addErrorAnnotation(part, CryptoResultAnnotation.CryptoError.ENCRYPTED_BUT_UNSUPPORTED, MessageHelper.createEmptyPart());
            }
        }
    }

    private void processFoundSignedParts(List<Part> foundParts) {
        for (Part part : foundParts) {
            if (!MessageHelper.isCompletePartAvailable(part)) {
                addErrorAnnotation(part, CryptoResultAnnotation.CryptoError.OPENPGP_SIGNED_BUT_INCOMPLETE, getMultipartSignedContentPartIfAvailable(part));
            } else if (MessageDecryptVerifier.isPgpMimeEncryptedOrSignedPart(part)) {
                this.partsToDecryptOrVerify.add(new CryptoPart(CryptoPartType.PGP_SIGNED, part));
            } else {
                addErrorAnnotation(part, CryptoResultAnnotation.CryptoError.SIGNED_BUT_UNSUPPORTED, getMultipartSignedContentPartIfAvailable(part));
            }
        }
    }

    private void addErrorAnnotation(Part part, CryptoResultAnnotation.CryptoError error, MimeBodyPart replacementPart) {
        this.messageAnnotations.put(part, CryptoResultAnnotation.createErrorAnnotation(error, replacementPart));
    }

    private void addFoundInlinePgpParts(List<Part> foundParts) {
        for (Part part : foundParts) {
            if (this.currentMessage.getFlags().contains(Flag.X_DOWNLOADED_FULL)) {
                this.partsToDecryptOrVerify.add(new CryptoPart(CryptoPartType.PGP_INLINE, part));
            } else if (MessageDecryptVerifier.isPartPgpInlineEncrypted(part)) {
                addErrorAnnotation(part, CryptoResultAnnotation.CryptoError.OPENPGP_ENCRYPTED_BUT_INCOMPLETE, NO_REPLACEMENT_PART);
            } else {
                addErrorAnnotation(part, CryptoResultAnnotation.CryptoError.OPENPGP_SIGNED_BUT_INCOMPLETE, extractClearsignedTextReplacementPart(part));
            }
        }
    }

    /* access modifiers changed from: private */
    public void decryptOrVerifyNextPart() {
        if (!this.isCancelled) {
            if (this.partsToDecryptOrVerify.isEmpty()) {
                runSecondPassOrReturnResultToFragment();
            } else {
                startDecryptingOrVerifyingPart(this.partsToDecryptOrVerify.peekFirst());
            }
        }
    }

    private void startDecryptingOrVerifyingPart(CryptoPart cryptoPart) {
        if (!isBoundToCryptoProviderService()) {
            connectToCryptoProviderService();
        } else {
            decryptOrVerifyPart(cryptoPart);
        }
    }

    private boolean isBoundToCryptoProviderService() {
        return this.openPgpApi != null;
    }

    private void connectToCryptoProviderService() {
        this.openPgpServiceConnection = new OpenPgpServiceConnection(this.context, this.openPgpProviderPackage, new OpenPgpServiceConnection.OnBound() {
            public void onBound(IOpenPgpService2 service) {
                OpenPgpApi unused = MessageCryptoHelper.this.openPgpApi = new OpenPgpApi(MessageCryptoHelper.this.context, service);
                MessageCryptoHelper.this.decryptOrVerifyNextPart();
            }

            public void onError(Exception e) {
                Log.e("k9", "Couldn't connect to OpenPgpService", e);
            }
        });
        this.openPgpServiceConnection.bindToService();
    }

    private void decryptOrVerifyPart(CryptoPart cryptoPart) {
        this.currentCryptoPart = cryptoPart;
        Intent decryptIntent = this.userInteractionResultIntent;
        this.userInteractionResultIntent = null;
        if (decryptIntent == null) {
            decryptIntent = getDecryptionIntent();
        }
        decryptVerify(decryptIntent);
    }

    @NonNull
    private Intent getDecryptionIntent() {
        Intent decryptIntent = new Intent(OpenPgpApi.ACTION_DECRYPT_VERIFY);
        Address[] from = this.currentMessage.getFrom();
        if (from.length > 0) {
            decryptIntent.putExtra(OpenPgpApi.EXTRA_SENDER_ADDRESS, from[0].getAddress());
        }
        decryptIntent.putExtra(OpenPgpApi.EXTRA_DECRYPTION_RESULT, this.cachedDecryptionResult);
        return decryptIntent;
    }

    private void decryptVerify(Intent intent) {
        try {
            CryptoPartType cryptoPartType = this.currentCryptoPart.type;
            switch (cryptoPartType) {
                case PGP_SIGNED:
                    callAsyncDetachedVerify(intent);
                    return;
                case PGP_ENCRYPTED:
                    callAsyncDecrypt(intent);
                    return;
                case PGP_INLINE:
                    callAsyncInlineOperation(intent);
                    return;
                default:
                    throw new IllegalStateException("Unknown crypto part type: " + cryptoPartType);
            }
        } catch (IOException e) {
            Log.e("k9", "IOException", e);
        } catch (MessagingException e2) {
            Log.e("k9", "MessagingException", e2);
        }
    }

    private void callAsyncInlineOperation(Intent intent) throws IOException {
        this.cancelableBackgroundOperation = this.openPgpApi.executeApiAsync(intent, getDataSourceForEncryptedOrInlineData(), getDataSinkForDecryptedInlineData(), new OpenPgpApi.IOpenPgpSinkResultCallback<MimeBodyPart>() {
            public void onProgress(int current, int max) {
                Log.d("k9", "received progress status: " + current + " / " + max);
                MessageCryptoHelper.this.callbackProgress(current, max);
            }

            public void onReturn(Intent result, MimeBodyPart bodyPart) {
                OpenPgpApi.CancelableBackgroundOperation unused = MessageCryptoHelper.this.cancelableBackgroundOperation = null;
                Intent unused2 = MessageCryptoHelper.this.currentCryptoResult = result;
                MessageCryptoHelper.this.onCryptoOperationReturned(bodyPart);
            }
        });
    }

    public void cancelIfRunning() {
        detachCallback();
        this.isCancelled = true;
        if (this.cancelableBackgroundOperation != null) {
            this.cancelableBackgroundOperation.cancelOperation();
        }
    }

    private OpenPgpApi.OpenPgpDataSink<MimeBodyPart> getDataSinkForDecryptedInlineData() {
        return new OpenPgpApi.OpenPgpDataSink<MimeBodyPart>() {
            public MimeBodyPart processData(InputStream is) throws IOException {
                try {
                    ByteArrayOutputStream decryptedByteOutputStream = new ByteArrayOutputStream();
                    IOUtils.copy(is, decryptedByteOutputStream);
                    return new MimeBodyPart(new TextBody(new String(decryptedByteOutputStream.toByteArray())), ContentTypeField.TYPE_TEXT_PLAIN);
                } catch (MessagingException e) {
                    Log.e("k9", "MessagingException", e);
                    return null;
                }
            }
        };
    }

    private void callAsyncDecrypt(Intent intent) throws IOException {
        this.cancelableBackgroundOperation = this.openPgpApi.executeApiAsync(intent, getDataSourceForEncryptedOrInlineData(), getDataSinkForDecryptedData(), new OpenPgpApi.IOpenPgpSinkResultCallback<MimeBodyPart>() {
            public void onReturn(Intent result, MimeBodyPart decryptedPart) {
                OpenPgpApi.CancelableBackgroundOperation unused = MessageCryptoHelper.this.cancelableBackgroundOperation = null;
                Intent unused2 = MessageCryptoHelper.this.currentCryptoResult = result;
                MessageCryptoHelper.this.onCryptoOperationReturned(decryptedPart);
            }

            public void onProgress(int current, int max) {
                Log.d("k9", "received progress status: " + current + " / " + max);
                MessageCryptoHelper.this.callbackProgress(current, max);
            }
        });
    }

    private void callAsyncDetachedVerify(Intent intent) throws IOException, MessagingException {
        OpenPgpApi.OpenPgpDataSource dataSource = getDataSourceForSignedData();
        intent.putExtra("detached_signature", MessageDecryptVerifier.getSignatureData(this.currentCryptoPart.part));
        this.openPgpApi.executeApiAsync(intent, dataSource, new OpenPgpApi.IOpenPgpSinkResultCallback<Void>() {
            public void onReturn(Intent result, Void dummy) {
                OpenPgpApi.CancelableBackgroundOperation unused = MessageCryptoHelper.this.cancelableBackgroundOperation = null;
                Intent unused2 = MessageCryptoHelper.this.currentCryptoResult = result;
                MessageCryptoHelper.this.onCryptoOperationReturned(null);
            }

            public void onProgress(int current, int max) {
                Log.d("k9", "received progress status: " + current + " / " + max);
                MessageCryptoHelper.this.callbackProgress(current, max);
            }
        });
    }

    private OpenPgpApi.OpenPgpDataSource getDataSourceForSignedData() throws IOException {
        return new OpenPgpApi.OpenPgpDataSource() {
            public void writeTo(OutputStream os) throws IOException {
                try {
                    BodyPart signatureBodyPart = ((Multipart) MessageCryptoHelper.this.currentCryptoPart.part.getBody()).getBodyPart(0);
                    Log.d("k9", "signed data type: " + signatureBodyPart.getMimeType());
                    signatureBodyPart.writeTo(os);
                } catch (MessagingException e) {
                    Log.e("k9", "Exception while writing message to crypto provider", e);
                }
            }
        };
    }

    private OpenPgpApi.OpenPgpDataSource getDataSourceForEncryptedOrInlineData() throws IOException {
        return new OpenPgpApi.OpenPgpDataSource() {
            public Long getSizeForProgress() {
                Body body;
                Part part = MessageCryptoHelper.this.currentCryptoPart.part;
                CryptoPartType cryptoPartType = MessageCryptoHelper.this.currentCryptoPart.type;
                if (cryptoPartType == CryptoPartType.PGP_ENCRYPTED) {
                    body = ((Multipart) part.getBody()).getBodyPart(1).getBody();
                } else if (cryptoPartType == CryptoPartType.PGP_INLINE) {
                    body = part.getBody();
                } else {
                    throw new IllegalStateException("part to stream must be encrypted or inline!");
                }
                if (body instanceof SizeAware) {
                    long bodySize = ((SizeAware) body).getSize();
                    if (bodySize > PlaybackStateCompat.ACTION_SKIP_TO_QUEUE_ITEM) {
                        return Long.valueOf(bodySize);
                    }
                }
                return null;
            }

            @WorkerThread
            public void writeTo(OutputStream os) throws IOException {
                try {
                    Part part = MessageCryptoHelper.this.currentCryptoPart.part;
                    CryptoPartType cryptoPartType = MessageCryptoHelper.this.currentCryptoPart.type;
                    if (cryptoPartType == CryptoPartType.PGP_ENCRYPTED) {
                        ((Multipart) part.getBody()).getBodyPart(1).getBody().writeTo(os);
                    } else if (cryptoPartType == CryptoPartType.PGP_INLINE) {
                        os.write(MessageExtractor.getTextFromPart(part).getBytes());
                    } else {
                        throw new IllegalStateException("part to stream must be encrypted or inline!");
                    }
                } catch (MessagingException e) {
                    Log.e("k9", "MessagingException while writing message to crypto provider", e);
                }
            }
        };
    }

    private OpenPgpApi.OpenPgpDataSink<MimeBodyPart> getDataSinkForDecryptedData() throws IOException {
        return new OpenPgpApi.OpenPgpDataSink<MimeBodyPart>() {
            @WorkerThread
            public MimeBodyPart processData(InputStream is) throws IOException {
                try {
                    return MimePartStreamParser.parse(DecryptedFileProvider.getFileFactory(MessageCryptoHelper.this.context), is);
                } catch (MessagingException e) {
                    Log.e("k9", "Something went wrong while parsing the decrypted MIME part", e);
                    return null;
                }
            }
        };
    }

    /* access modifiers changed from: private */
    public void onCryptoOperationReturned(MimeBodyPart decryptedPart) {
        if (this.currentCryptoResult == null) {
            Log.e("k9", "Internal error: we should have a result here!");
            return;
        }
        try {
            handleCryptoOperationResult(decryptedPart);
        } finally {
            this.currentCryptoResult = null;
        }
    }

    private void handleCryptoOperationResult(MimeBodyPart outputPart) {
        int resultCode = this.currentCryptoResult.getIntExtra(OpenPgpApi.RESULT_CODE, -1);
        if (K9.DEBUG) {
            Log.d("k9", "OpenPGP API decryptVerify result code: " + resultCode);
        }
        switch (resultCode) {
            case -1:
                Log.e("k9", "Internal error: no result code!");
                return;
            case 0:
                handleCryptoOperationError();
                return;
            case 1:
                handleCryptoOperationSuccess(outputPart);
                return;
            case 2:
                handleUserInteractionRequest();
                return;
            default:
                return;
        }
    }

    private void handleUserInteractionRequest() {
        PendingIntent pendingIntent = (PendingIntent) this.currentCryptoResult.getParcelableExtra(OpenPgpApi.RESULT_INTENT);
        if (pendingIntent == null) {
            throw new AssertionError("Expecting PendingIntent on USER_INTERACTION_REQUIRED!");
        }
        callbackPendingIntent(pendingIntent);
    }

    private void handleCryptoOperationError() {
        OpenPgpError error = (OpenPgpError) this.currentCryptoResult.getParcelableExtra(OpenPgpApi.RESULT_ERROR);
        if (K9.DEBUG) {
            Log.w("k9", "OpenPGP API error: " + error.getMessage());
        }
        onCryptoOperationFailed(error);
    }

    private void handleCryptoOperationSuccess(MimeBodyPart outputPart) {
        onCryptoOperationSuccess(CryptoResultAnnotation.createOpenPgpResultAnnotation((OpenPgpDecryptionResult) this.currentCryptoResult.getParcelableExtra(OpenPgpApi.RESULT_DECRYPTION), (OpenPgpSignatureResult) this.currentCryptoResult.getParcelableExtra(OpenPgpApi.RESULT_SIGNATURE), (PendingIntent) this.currentCryptoResult.getParcelableExtra(OpenPgpApi.RESULT_INTENT), outputPart));
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!this.isCancelled) {
            if (requestCode != 124) {
                throw new IllegalStateException("got an activity result that wasn't meant for us. this is a bug!");
            } else if (resultCode == -1) {
                this.userInteractionResultIntent = data;
                decryptOrVerifyNextPart();
            } else {
                onCryptoOperationCanceled();
            }
        }
    }

    private void onCryptoOperationSuccess(CryptoResultAnnotation resultAnnotation) {
        addCryptoResultAnnotationToMessage(resultAnnotation);
        onCryptoFinished();
    }

    private void propagateEncapsulatedSignedPart(CryptoResultAnnotation resultAnnotation, Part part) {
        Part encapsulatingPart = this.messageAnnotations.findKeyForAnnotationWithReplacementPart(part);
        CryptoResultAnnotation encapsulatingPartAnnotation = this.messageAnnotations.get(encapsulatingPart);
        if (encapsulatingPart != null && resultAnnotation.hasSignatureResult()) {
            this.messageAnnotations.put(encapsulatingPart, encapsulatingPartAnnotation.withEncapsulatedResult(resultAnnotation));
        }
    }

    private void onCryptoOperationCanceled() {
        addCryptoResultAnnotationToMessage(CryptoResultAnnotation.createOpenPgpCanceledAnnotation());
        onCryptoFinished();
    }

    private void onCryptoOperationFailed(OpenPgpError error) {
        addCryptoResultAnnotationToMessage(CryptoResultAnnotation.createOpenPgpErrorAnnotation(error));
        onCryptoFinished();
    }

    private void addCryptoResultAnnotationToMessage(CryptoResultAnnotation resultAnnotation) {
        Part part = this.currentCryptoPart.part;
        this.messageAnnotations.put(part, resultAnnotation);
        propagateEncapsulatedSignedPart(resultAnnotation, part);
    }

    private void onCryptoFinished() {
        this.currentCryptoPart = null;
        this.partsToDecryptOrVerify.removeFirst();
        decryptOrVerifyNextPart();
    }

    private void runSecondPassOrReturnResultToFragment() {
        if (this.secondPassStarted) {
            callbackReturnResult();
            return;
        }
        this.secondPassStarted = true;
        runSecondPass();
    }

    private void cleanupAfterProcessingFinished() {
        this.partsToDecryptOrVerify = null;
        this.openPgpApi = null;
        if (this.openPgpServiceConnection != null) {
            this.openPgpServiceConnection.unbindFromService();
        }
        this.openPgpServiceConnection = null;
    }

    public void detachCallback() {
        synchronized (this.callbackLock) {
            this.callback = null;
        }
    }

    private void reattachCallback(LocalMessage message, MessageCryptoCallback callback2) {
        if (!message.equals(this.currentMessage)) {
            throw new AssertionError("Callback may only be reattached for the same message!");
        }
        synchronized (this.callbackLock) {
            if (this.queuedResult != null) {
                Log.d("k9", "Returning cached result to reattached callback");
            }
            this.callback = callback2;
            deliverResult();
        }
    }

    private void callbackPendingIntent(PendingIntent pendingIntent) {
        synchronized (this.callbackLock) {
            this.queuedPendingIntent = pendingIntent;
            deliverResult();
        }
    }

    private void callbackReturnResult() {
        synchronized (this.callbackLock) {
            cleanupAfterProcessingFinished();
            this.queuedResult = this.messageAnnotations;
            this.messageAnnotations = null;
            deliverResult();
        }
    }

    /* access modifiers changed from: private */
    public void callbackProgress(int current, int max) {
        synchronized (this.callbackLock) {
            if (this.callback != null) {
                this.callback.onCryptoHelperProgress(current, max);
            }
        }
    }

    private void deliverResult() {
        if (!this.isCancelled) {
            if (this.callback == null) {
                Log.d("k9", "Keeping crypto helper result in queue for later delivery");
            } else if (this.queuedResult != null) {
                this.callback.onCryptoOperationsFinished(this.queuedResult);
            } else if (this.queuedPendingIntent != null) {
                this.callback.startPendingIntentForCryptoHelper(this.queuedPendingIntent.getIntentSender(), REQUEST_CODE_USER_INTERACTION, null, 0, 0, 0);
                this.queuedPendingIntent = null;
            }
        }
    }

    private static class CryptoPart {
        public final Part part;
        public final CryptoPartType type;

        CryptoPart(CryptoPartType type2, Part part2) {
            this.type = type2;
            this.part = part2;
        }
    }

    @Nullable
    private static MimeBodyPart getMultipartSignedContentPartIfAvailable(Part part) {
        MimeBodyPart replacementPart = NO_REPLACEMENT_PART;
        if (!(part.getBody() instanceof MimeMultipart)) {
            return replacementPart;
        }
        MimeMultipart multipart = (MimeMultipart) part.getBody();
        if (multipart.getCount() >= 1) {
            return (MimeBodyPart) multipart.getBodyPart(0);
        }
        return replacementPart;
    }

    private static MimeBodyPart extractClearsignedTextReplacementPart(Part part) {
        try {
            String replacementText = OpenPgpUtils.extractClearsignedMessage(MessageExtractor.getTextFromPart(part));
            if (replacementText != null) {
                return new MimeBodyPart(new TextBody(replacementText), ContentTypeField.TYPE_TEXT_PLAIN);
            }
            Log.e("k9", "failed to extract clearsigned text for replacement part");
            return NO_REPLACEMENT_PART;
        } catch (MessagingException e) {
            Log.e("k9", "failed to create clearsigned text replacement part", e);
            return NO_REPLACEMENT_PART;
        }
    }
}
