package com.fsck.k9.ui.messageview;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.DownloadManager;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;
import atomicgonza.Views;
import com.fsck.k9.Account;
import com.fsck.k9.K9;
import com.fsck.k9.Preferences;
import com.fsck.k9.R;
import com.fsck.k9.activity.ChooseFolder;
import com.fsck.k9.activity.MessageLoaderHelper;
import com.fsck.k9.activity.MessageReference;
import com.fsck.k9.controller.MessagingController;
import com.fsck.k9.fragment.ConfirmationDialogFragment;
import com.fsck.k9.fragment.ProgressDialogFragment;
import com.fsck.k9.helper.FileBrowserHelper;
import com.fsck.k9.mail.Flag;
import com.fsck.k9.mailstore.AttachmentViewInfo;
import com.fsck.k9.mailstore.LocalMessage;
import com.fsck.k9.mailstore.MessageViewInfo;
import com.fsck.k9.ui.messageview.CryptoInfoDialog;
import com.fsck.k9.ui.messageview.MessageCryptoPresenter;
import com.fsck.k9.view.MessageCryptoDisplayStatus;
import com.fsck.k9.view.MessageHeader;
import java.io.File;
import java.util.Collections;
import java.util.Locale;

public class MessageViewFragment extends Fragment implements ConfirmationDialogFragment.ConfirmationDialogFragmentListener, AttachmentViewCallback, CryptoInfoDialog.OnClickShowCryptoKeyListener {
    private static final int ACTIVITY_CHOOSE_DIRECTORY = 3;
    private static final int ACTIVITY_CHOOSE_FOLDER_COPY = 2;
    private static final int ACTIVITY_CHOOSE_FOLDER_MOVE = 1;
    private static final String ARG_REFERENCE = "reference";
    public static final int REQUEST_MASK_CRYPTO_PRESENTER = 512;
    public static final int REQUEST_MASK_LOADER_HELPER = 256;
    private AttachmentViewInfo currentAttachmentViewInfo;
    private DownloadManager downloadManager;
    private Handler handler = new Handler();
    private Account mAccount;
    private Context mContext;
    private MessagingController mController;
    private String mDstFolder;
    private MessageViewFragmentListener mFragmentListener;
    private boolean mInitialized = false;
    /* access modifiers changed from: private */
    public LocalMessage mMessage;
    private MessageReference mMessageReference;
    /* access modifiers changed from: private */
    public MessageTopView mMessageView;
    private MessageCryptoPresenter.MessageCryptoMvpView messageCryptoMvpView = new MessageCryptoPresenter.MessageCryptoMvpView() {
        public void redisplayMessage() {
            MessageViewFragment.this.messageLoaderHelper.asyncReloadMessage();
        }

        public void startPendingIntentForCryptoPresenter(IntentSender si, Integer requestCode, Intent fillIntent, int flagsMask, int flagValues, int extraFlags) throws IntentSender.SendIntentException {
            if (requestCode == null) {
                MessageViewFragment.this.getActivity().startIntentSender(si, fillIntent, flagsMask, flagValues, extraFlags);
                return;
            }
            IntentSender intentSender = si;
            MessageViewFragment.this.getActivity().startIntentSenderForResult(intentSender, Integer.valueOf(requestCode.intValue() | 512).intValue(), fillIntent, flagsMask, flagValues, extraFlags);
        }

        public void showCryptoInfoDialog(MessageCryptoDisplayStatus displayStatus) {
            CryptoInfoDialog dialog = CryptoInfoDialog.newInstance(displayStatus);
            dialog.setTargetFragment(MessageViewFragment.this, 0);
            dialog.show(MessageViewFragment.this.getFragmentManager(), "crypto_info_dialog");
        }

        public void restartMessageCryptoProcessing() {
            MessageViewFragment.this.mMessageView.setToLoadingState();
            MessageViewFragment.this.messageLoaderHelper.asyncRestartMessageCryptoProcessing();
        }
    };
    private MessageCryptoPresenter messageCryptoPresenter;
    private MessageLoaderHelper.MessageLoaderCallbacks messageLoaderCallbacks = new MessageLoaderHelper.MessageLoaderCallbacks() {
        public void onMessageDataLoadFinished(LocalMessage message) {
            LocalMessage unused = MessageViewFragment.this.mMessage = message;
            MessageViewFragment.this.displayHeaderForLoadingMessage(message);
            MessageViewFragment.this.mMessageView.setToLoadingState();
        }

        public void onMessageDataLoadFailed() {
            Toast.makeText(MessageViewFragment.this.getActivity(), (int) R.string.status_loading_error, 1).show();
            if (K9.DEBUG) {
                Log.e("AtomicGonza", "okAG DATA LOAD FAILED " + new Exception().getStackTrace()[0].toString());
            }
        }

        public void onMessageViewInfoLoadFinished(MessageViewInfo messageViewInfo) {
            MessageViewFragment.this.showMessage(messageViewInfo);
            Views.show(MessageViewFragment.this.mMessageView.findViewById(R.id.footer_message_controls));
            Views.gone(MessageViewFragment.this.mMessageView.findViewById(R.id.message_progress_footer));
            if (K9.DEBUG) {
                Log.e("AtomicGonza", "okAG LOAD FINISHED " + new Exception().getStackTrace()[0].toString());
            }
        }

        public void onMessageViewInfoLoadFailed(MessageViewInfo messageViewInfo) {
            MessageViewFragment.this.showMessage(messageViewInfo);
            if (K9.DEBUG) {
                Log.e("AtomicGonza", "okAG loadFailed " + new Exception().getStackTrace()[0].toString());
            }
        }

        public void setLoadingProgress(int current, int max) {
            MessageViewFragment.this.mMessageView.setLoadingProgress(current, max);
        }

        public void onDownloadErrorMessageNotFound() {
            MessageViewFragment.this.mMessageView.enableDownloadButton();
            MessageViewFragment.this.getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(MessageViewFragment.this.getActivity(), (int) R.string.status_invalid_id_error, 1).show();
                }
            });
        }

        public void onDownloadErrorNetworkError() {
            MessageViewFragment.this.mMessageView.enableDownloadButton();
            MessageViewFragment.this.getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(MessageViewFragment.this.getActivity(), (int) R.string.status_network_error, 1).show();
                }
            });
            if (K9.DEBUG) {
                Log.e("AtomicGonza", "okAG NETWORK ERROR " + new Exception().getStackTrace()[0].toString());
            }
        }

        public void startIntentSenderForMessageLoaderHelper(IntentSender si, int requestCode, Intent fillIntent, int flagsMask, int flagValues, int extraFlags) {
            try {
                MessageViewFragment.this.getActivity().startIntentSenderForResult(si, requestCode | 256, fillIntent, flagsMask, flagValues, extraFlags);
            } catch (IntentSender.SendIntentException e) {
                Log.e("k9", "Irrecoverable error calling PendingIntent!", e);
            }
        }
    };
    /* access modifiers changed from: private */
    public MessageLoaderHelper messageLoaderHelper;

    public interface MessageViewFragmentListener {
        void disableDeleteAction();

        void displayMessageSubject(String str);

        void messageHeaderViewAvailable(MessageHeader messageHeader);

        void onForward(MessageReference messageReference, Parcelable parcelable);

        void onReply(MessageReference messageReference, Parcelable parcelable);

        void onReplyAll(MessageReference messageReference, Parcelable parcelable);

        void setProgress(boolean z);

        void showNextMessageOrReturn();

        void updateMenu();
    }

    public static MessageViewFragment newInstance(MessageReference reference) {
        MessageViewFragment fragment = new MessageViewFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_REFERENCE, reference);
        fragment.setArguments(args);
        return fragment;
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.mContext = activity.getApplicationContext();
        try {
            this.mFragmentListener = (MessageViewFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.getClass() + " must implement MessageViewFragmentListener");
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        Context context = getActivity().getApplicationContext();
        this.mController = MessagingController.getInstance(context);
        this.downloadManager = (DownloadManager) context.getSystemService("download");
        this.messageCryptoPresenter = new MessageCryptoPresenter(savedInstanceState, this.messageCryptoMvpView);
        this.messageLoaderHelper = new MessageLoaderHelper(context, getLoaderManager(), getFragmentManager(), this.messageLoaderCallbacks);
        this.mInitialized = true;
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        this.messageCryptoPresenter.onSaveInstanceState(outState);
    }

    public void onDestroy() {
        super.onDestroy();
        Activity activity = getActivity();
        if (activity != null && activity.isChangingConfigurations()) {
            this.messageLoaderHelper.onDestroyChangingConfigurations();
        } else {
            this.messageLoaderHelper.onDestroy();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = LayoutInflater.from(new ContextThemeWrapper(inflater.getContext(), K9.getK9ThemeResourceId(K9.getK9MessageViewTheme()))).inflate((int) R.layout.message, container, false);
        this.mMessageView = (MessageTopView) view.findViewById(R.id.message_view);
        this.mMessageView.setAttachmentCallback(this);
        this.mMessageView.setMessageCryptoPresenter(this.messageCryptoPresenter);
        this.mMessageView.setOnToggleFlagClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MessageViewFragment.this.onToggleFlagged();
            }
        });
        this.mMessageView.setOnDownloadButtonClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MessageViewFragment.this.mMessageView.disableDownloadButton();
                MessageViewFragment.this.messageLoaderHelper.downloadCompleteMessage();
                MessageViewFragment.this.mMessageView.findViewById(R.id.message_progress_footer).setVisibility(0);
                v.setVisibility(8);
            }
        });
        this.mFragmentListener.messageHeaderViewAvailable(this.mMessageView.getMessageHeaderView());
        this.mMessageView.findViewById(R.id.buttonFooterReply).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MessageViewFragment.this.onReply();
            }
        });
        this.mMessageView.findViewById(R.id.buttonfooterForward).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MessageViewFragment.this.onForward();
            }
        });
        this.mMessageView.findViewById(R.id.buttonfooterReplyAll).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MessageViewFragment.this.onReplyAll();
            }
        });
        return view;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        displayMessage((MessageReference) getArguments().getParcelable(ARG_REFERENCE));
    }

    private void displayMessage(MessageReference messageReference) {
        this.mMessageReference = messageReference;
        if (K9.DEBUG) {
            Log.d("k9", "MessageView displaying message " + this.mMessageReference);
        }
        this.mAccount = Preferences.getPreferences(getApplicationContext()).getAccount(this.mMessageReference.getAccountUuid());
        Views.show(this.mMessageView.findViewById(R.id.message_progress_footer));
        this.messageLoaderHelper.asyncStartOrResumeLoadingMessage(messageReference, null);
        this.mFragmentListener.updateMenu();
    }

    private void hideKeyboard() {
        Activity activity = getActivity();
        if (activity != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService("input_method");
            View decorView = activity.getWindow().getDecorView();
            if (decorView != null) {
                imm.hideSoftInputFromWindow(decorView.getApplicationWindowToken(), 0);
            }
        }
    }

    private void showUnableToDecodeError() {
        Toast.makeText(getActivity().getApplicationContext(), (int) R.string.message_view_toast_unable_to_display_message, 0).show();
    }

    /* access modifiers changed from: private */
    public void showMessage(MessageViewInfo messageViewInfo) {
        hideKeyboard();
        if (!this.messageCryptoPresenter.maybeHandleShowMessage(this.mMessageView, this.mAccount, messageViewInfo)) {
            this.mMessageView.showMessage(this.mAccount, messageViewInfo);
            if (this.mAccount.isOpenPgpProviderConfigured()) {
                this.mMessageView.getMessageHeaderView().setCryptoStatusDisabled();
            } else {
                this.mMessageView.getMessageHeaderView().hideCryptoStatus();
            }
        }
    }

    /* access modifiers changed from: private */
    public void displayHeaderForLoadingMessage(LocalMessage message) {
        this.mMessageView.setHeaders(message, this.mAccount);
        if (this.mAccount.isOpenPgpProviderConfigured()) {
            this.mMessageView.getMessageHeaderView().setCryptoStatusLoading();
        }
        displayMessageSubject(getSubjectForMessage(message));
        this.mFragmentListener.updateMenu();
    }

    public void onDelete() {
        if (K9.confirmDelete() || (K9.confirmDeleteStarred() && this.mMessage.isSet(Flag.FLAGGED))) {
            showDialog(R.id.dialog_confirm_delete);
        } else {
            delete();
        }
    }

    public void onToggleAllHeadersView() {
        this.mMessageView.getMessageHeaderView().onShowAdditionalHeaders();
    }

    public boolean allHeadersVisible() {
        return this.mMessageView.getMessageHeaderView().additionalHeadersVisible();
    }

    private void delete() {
        if (this.mMessage != null) {
            this.mFragmentListener.disableDeleteAction();
            LocalMessage localMessage = this.mMessage;
            this.mFragmentListener.showNextMessageOrReturn();
            this.mController.deleteMessage(this.mMessageReference, null);
        }
    }

    public void onRefile(String dstFolder) {
        if (this.mController.isMoveCapable(this.mAccount)) {
            if (!this.mController.isMoveCapable(this.mMessageReference)) {
                Toast.makeText(getActivity(), (int) R.string.move_copy_cannot_copy_unsynced_message, 1).show();
            } else if (K9.FOLDER_NONE.equalsIgnoreCase(dstFolder)) {
            } else {
                if (!this.mAccount.getSpamFolderName().equals(dstFolder) || !K9.confirmSpam()) {
                    refileMessage(dstFolder);
                    return;
                }
                this.mDstFolder = dstFolder;
                showDialog(R.id.dialog_confirm_spam);
            }
        }
    }

    private void refileMessage(String dstFolder) {
        String srcFolder = this.mMessageReference.getFolderName();
        MessageReference messageToMove = this.mMessageReference;
        this.mFragmentListener.showNextMessageOrReturn();
        this.mController.moveMessage(this.mAccount, srcFolder, messageToMove, dstFolder);
    }

    public void onReply() {
        if (this.mMessage != null) {
            this.mFragmentListener.onReply(this.mMessage.makeMessageReference(), this.messageCryptoPresenter.getDecryptionResultForReply());
        }
    }

    public void onReplyAll() {
        if (this.mMessage != null) {
            this.mFragmentListener.onReplyAll(this.mMessage.makeMessageReference(), this.messageCryptoPresenter.getDecryptionResultForReply());
        }
    }

    public void onForward() {
        if (this.mMessage != null) {
            this.mFragmentListener.onForward(this.mMessage.makeMessageReference(), this.messageCryptoPresenter.getDecryptionResultForReply());
        }
    }

    public void onToggleFlagged() {
        if (this.mMessage != null) {
            this.mController.setFlag(this.mAccount, this.mMessage.getFolder().getName(), Collections.singletonList(this.mMessage), Flag.FLAGGED, !this.mMessage.isSet(Flag.FLAGGED));
            this.mMessageView.setHeaders(this.mMessage, this.mAccount);
        }
    }

    public void onMove() {
        if (this.mController.isMoveCapable(this.mAccount) && this.mMessage != null) {
            if (!this.mController.isMoveCapable(this.mMessageReference)) {
                Toast.makeText(getActivity(), (int) R.string.move_copy_cannot_copy_unsynced_message, 1).show();
            } else {
                startRefileActivity(1);
            }
        }
    }

    public void onCopy() {
        if (this.mController.isCopyCapable(this.mAccount) && this.mMessage != null) {
            if (!this.mController.isCopyCapable(this.mMessageReference)) {
                Toast.makeText(getActivity(), (int) R.string.move_copy_cannot_copy_unsynced_message, 1).show();
            } else {
                startRefileActivity(2);
            }
        }
    }

    public void onArchive() {
        onRefile(this.mAccount.getArchiveFolderName());
    }

    public void onSpam() {
        onRefile(this.mAccount.getSpamFolderName());
    }

    public void onSelectText() {
    }

    private void startRefileActivity(int activity) {
        Intent intent = new Intent(getActivity(), ChooseFolder.class);
        intent.putExtra(ChooseFolder.EXTRA_ACCOUNT, this.mAccount.getUuid());
        intent.putExtra(ChooseFolder.EXTRA_CUR_FOLDER, this.mMessageReference.getFolderName());
        intent.putExtra(ChooseFolder.EXTRA_SEL_FOLDER, this.mAccount.getLastSelectedFolderName());
        intent.putExtra(ChooseFolder.EXTRA_MESSAGE, this.mMessageReference);
        startActivityForResult(intent, activity);
    }

    public void onPendingIntentResult(int requestCode, int resultCode, Intent data) {
        if ((requestCode & 256) == 256) {
            this.messageLoaderHelper.onActivityResult(requestCode ^ 256, resultCode, data);
        } else if ((requestCode & 512) == 512) {
            this.messageCryptoPresenter.onActivityResult(requestCode ^ 512, resultCode, data);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Uri fileUri;
        String filePath;
        if (resultCode == -1) {
            switch (requestCode) {
                case 1:
                case 2:
                    if (data != null) {
                        String destFolderName = data.getStringExtra(ChooseFolder.EXTRA_NEW_FOLDER);
                        MessageReference ref = (MessageReference) data.getParcelableExtra(ChooseFolder.EXTRA_MESSAGE);
                        if (this.mMessageReference.equals(ref)) {
                            this.mAccount.setLastSelectedFolderName(destFolderName);
                            switch (requestCode) {
                                case 1:
                                    this.mFragmentListener.showNextMessageOrReturn();
                                    moveMessage(ref, destFolderName);
                                    return;
                                case 2:
                                    copyMessage(ref, destFolderName);
                                    return;
                                default:
                                    return;
                            }
                        } else {
                            return;
                        }
                    } else {
                        return;
                    }
                case 3:
                    if (data != null && (fileUri = data.getData()) != null && (filePath = fileUri.getPath()) != null) {
                        getAttachmentController(this.currentAttachmentViewInfo).saveAttachmentTo(filePath);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public void onSendAlternate() {
        if (this.mMessage != null) {
            this.mController.sendAlternate(getActivity(), this.mAccount, this.mMessage);
        }
    }

    public void onToggleRead() {
        if (this.mMessage != null) {
            this.mController.setFlag(this.mAccount, this.mMessage.getFolder().getName(), Collections.singletonList(this.mMessage), Flag.SEEN, !this.mMessage.isSet(Flag.SEEN));
            this.mMessageView.setHeaders(this.mMessage, this.mAccount);
            displayMessageSubject(this.mMessage.getSubject());
            this.mFragmentListener.updateMenu();
        }
    }

    private void setProgress(boolean enable) {
        if (this.mFragmentListener != null) {
            this.mFragmentListener.setProgress(enable);
        }
    }

    private void displayMessageSubject(String subject) {
        if (this.mFragmentListener != null) {
            this.mFragmentListener.displayMessageSubject(subject);
        }
    }

    private String getSubjectForMessage(LocalMessage message) {
        String subject = message.getSubject();
        if (TextUtils.isEmpty(subject)) {
            return this.mContext.getString(R.string.general_no_subject);
        }
        return subject;
    }

    public void moveMessage(MessageReference reference, String destFolderName) {
        this.mController.moveMessage(this.mAccount, this.mMessageReference.getFolderName(), reference, destFolderName);
    }

    public void copyMessage(MessageReference reference, String destFolderName) {
        this.mController.copyMessage(this.mAccount, this.mMessageReference.getFolderName(), reference, destFolderName);
    }

    private void showDialog(int dialogId) {
        DialogFragment fragment;
        switch (dialogId) {
            case R.id.dialog_attachment_progress:
                fragment = ProgressDialogFragment.newInstance(null, getString(R.string.dialog_attachment_progress_title));
                break;
            case R.id.dialog_confirm_delete:
                fragment = ConfirmationDialogFragment.newInstance(dialogId, getString(R.string.dialog_confirm_delete_title), getString(R.string.dialog_confirm_delete_message), getString(R.string.dialog_confirm_delete_confirm_button), getString(R.string.dialog_confirm_delete_cancel_button));
                break;
            case R.id.dialog_confirm_mark_all_as_read:
            default:
                throw new RuntimeException("Called showDialog(int) with unknown dialog id.");
            case R.id.dialog_confirm_spam:
                fragment = ConfirmationDialogFragment.newInstance(dialogId, getString(R.string.dialog_confirm_spam_title), getResources().getQuantityString(exts.whats.R.dimen.abc_action_bar_stacked_tab_max_width, 1), getString(R.string.dialog_confirm_spam_confirm_button), getString(R.string.dialog_confirm_spam_cancel_button));
                break;
        }
        fragment.setTargetFragment(this, dialogId);
        fragment.show(getFragmentManager(), getDialogTag(dialogId));
    }

    /* access modifiers changed from: private */
    public void removeDialog(int dialogId) {
        FragmentManager fm = getFragmentManager();
        if (fm != null && !isRemoving() && !isDetached()) {
            fm.executePendingTransactions();
            DialogFragment fragment = (DialogFragment) fm.findFragmentByTag(getDialogTag(dialogId));
            if (fragment != null) {
                fragment.dismiss();
            }
        }
    }

    private String getDialogTag(int dialogId) {
        return String.format(Locale.US, "dialog-%d", Integer.valueOf(dialogId));
    }

    public void zoom(KeyEvent event) {
    }

    public void doPositiveClick(int dialogId) {
        switch (dialogId) {
            case R.id.dialog_confirm_delete:
                delete();
                return;
            case R.id.dialog_confirm_mark_all_as_read:
            default:
                return;
            case R.id.dialog_confirm_spam:
                refileMessage(this.mDstFolder);
                this.mDstFolder = null;
                return;
        }
    }

    public void doNegativeClick(int dialogId) {
    }

    public void dialogCancelled(int dialogId) {
    }

    public MessageReference getMessageReference() {
        return this.mMessageReference;
    }

    public boolean isMessageRead() {
        if (this.mMessage != null) {
            return this.mMessage.isSet(Flag.SEEN);
        }
        return false;
    }

    public boolean isCopyCapable() {
        return this.mController.isCopyCapable(this.mAccount);
    }

    public boolean isMoveCapable() {
        return this.mController.isMoveCapable(this.mAccount);
    }

    public boolean canMessageBeArchived() {
        return !this.mMessageReference.getFolderName().equals(this.mAccount.getArchiveFolderName()) && this.mAccount.hasArchiveFolder();
    }

    public boolean canMessageBeMovedToSpam() {
        return !this.mMessageReference.getFolderName().equals(this.mAccount.getSpamFolderName()) && this.mAccount.hasSpamFolder();
    }

    public void updateTitle() {
        if (this.mMessage != null) {
            displayMessageSubject(this.mMessage.getSubject());
        }
    }

    public Context getApplicationContext() {
        return this.mContext;
    }

    public void disableAttachmentButtons(AttachmentViewInfo attachment) {
    }

    public void enableAttachmentButtons(AttachmentViewInfo attachment) {
    }

    public void runOnMainThread(Runnable runnable) {
        this.handler.post(runnable);
    }

    public void showAttachmentLoadingDialog() {
        showDialog(R.id.dialog_attachment_progress);
    }

    public void hideAttachmentLoadingDialogOnMainThread() {
        this.handler.post(new Runnable() {
            public void run() {
                MessageViewFragment.this.removeDialog(R.id.dialog_attachment_progress);
            }
        });
    }

    public void refreshAttachmentThumbnail(AttachmentViewInfo attachment) {
    }

    public void onClickShowCryptoKey() {
        this.messageCryptoPresenter.onClickShowCryptoKey();
    }

    public boolean isInitialized() {
        return this.mInitialized;
    }

    public void onViewAttachment(AttachmentViewInfo attachment) {
        getAttachmentController(attachment).viewAttachment();
    }

    public void onSaveAttachment(AttachmentViewInfo attachment) {
        getAttachmentController(attachment).saveAttachment();
    }

    public void onSaveAttachmentToUserProvidedDirectory(final AttachmentViewInfo attachment) {
        this.currentAttachmentViewInfo = attachment;
        FileBrowserHelper.getInstance().showFileBrowserActivity(this, (File) null, 3, new FileBrowserHelper.FileBrowserFailOverCallback() {
            public void onPathEntered(String path) {
                MessageViewFragment.this.getAttachmentController(attachment).saveAttachmentTo(path);
            }

            public void onCancel() {
            }
        });
    }

    /* access modifiers changed from: private */
    public AttachmentController getAttachmentController(AttachmentViewInfo attachment) {
        return new AttachmentController(this.mController, this.downloadManager, this, attachment);
    }
}
