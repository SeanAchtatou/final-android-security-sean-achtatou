package com.fsck.k9.ui.messageview;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.util.Log;
import com.fsck.k9.Account;
import com.fsck.k9.R;
import com.fsck.k9.mailstore.CryptoResultAnnotation;
import com.fsck.k9.mailstore.MessageViewInfo;
import com.fsck.k9.view.MessageCryptoDisplayStatus;

public class MessageCryptoPresenter implements OnCryptoClickListener {
    public static final int REQUEST_CODE_UNKNOWN_KEY = 123;
    private CryptoResultAnnotation cryptoResultAnnotation;
    private final MessageCryptoMvpView messageCryptoMvpView;
    private boolean overrideCryptoWarning;

    public interface MessageCryptoMvpView {
        void redisplayMessage();

        void restartMessageCryptoProcessing();

        void showCryptoInfoDialog(MessageCryptoDisplayStatus messageCryptoDisplayStatus);

        void startPendingIntentForCryptoPresenter(IntentSender intentSender, Integer num, Intent intent, int i, int i2, int i3) throws IntentSender.SendIntentException;
    }

    public MessageCryptoPresenter(Bundle savedInstanceState, MessageCryptoMvpView messageCryptoMvpView2) {
        this.messageCryptoMvpView = messageCryptoMvpView2;
        if (savedInstanceState != null) {
            this.overrideCryptoWarning = savedInstanceState.getBoolean("overrideCryptoWarning");
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean("overrideCryptoWarning", this.overrideCryptoWarning);
    }

    public boolean maybeHandleShowMessage(MessageTopView messageView, Account account, MessageViewInfo messageViewInfo) {
        this.cryptoResultAnnotation = messageViewInfo.cryptoResultAnnotation;
        MessageCryptoDisplayStatus displayStatus = MessageCryptoDisplayStatus.fromResultAnnotation(messageViewInfo.cryptoResultAnnotation);
        if (displayStatus == MessageCryptoDisplayStatus.DISABLED) {
            return false;
        }
        messageView.getMessageHeaderView().setCryptoStatus(displayStatus);
        switch (displayStatus) {
            case UNENCRYPTED_SIGN_REVOKED:
            case ENCRYPTED_SIGN_REVOKED:
                showMessageCryptoWarning(messageView, account, messageViewInfo, R.string.messageview_crypto_warning_revoked);
                break;
            case UNENCRYPTED_SIGN_EXPIRED:
            case ENCRYPTED_SIGN_EXPIRED:
                showMessageCryptoWarning(messageView, account, messageViewInfo, R.string.messageview_crypto_warning_expired);
                break;
            case UNENCRYPTED_SIGN_INSECURE:
            case ENCRYPTED_SIGN_INSECURE:
                showMessageCryptoWarning(messageView, account, messageViewInfo, R.string.messageview_crypto_warning_insecure);
                break;
            case UNENCRYPTED_SIGN_ERROR:
            case ENCRYPTED_SIGN_ERROR:
                showMessageCryptoWarning(messageView, account, messageViewInfo, R.string.messageview_crypto_warning_error);
                break;
            case CANCELLED:
                messageView.showMessageCryptoCancelledView(messageViewInfo, getOpenPgpApiProviderIcon(messageView.getContext(), account));
                break;
            case INCOMPLETE_ENCRYPTED:
                messageView.showMessageEncryptedButIncomplete(messageViewInfo, getOpenPgpApiProviderIcon(messageView.getContext(), account));
                break;
            case ENCRYPTED_ERROR:
            case UNSUPPORTED_ENCRYPTED:
                messageView.showMessageCryptoErrorView(messageViewInfo, getOpenPgpApiProviderIcon(messageView.getContext(), account));
                break;
            case INCOMPLETE_SIGNED:
            case UNSUPPORTED_SIGNED:
            default:
                messageView.showMessage(account, messageViewInfo);
                break;
            case LOADING:
                throw new IllegalStateException("Displaying message while in loading state!");
        }
        return true;
    }

    private void showMessageCryptoWarning(MessageTopView messageView, Account account, MessageViewInfo messageViewInfo, @StringRes int warningStringRes) {
        if (this.overrideCryptoWarning) {
            messageView.showMessage(account, messageViewInfo);
        } else {
            messageView.showMessageCryptoWarning(messageViewInfo, getOpenPgpApiProviderIcon(messageView.getContext(), account), warningStringRes);
        }
    }

    public void onCryptoClick() {
        if (this.cryptoResultAnnotation != null) {
            MessageCryptoDisplayStatus displayStatus = MessageCryptoDisplayStatus.fromResultAnnotation(this.cryptoResultAnnotation);
            switch (displayStatus) {
                case LOADING:
                    return;
                case UNENCRYPTED_SIGN_UNKNOWN:
                    launchPendingIntent(this.cryptoResultAnnotation);
                    return;
                default:
                    displayCryptoInfoDialog(displayStatus);
                    return;
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode != 123) {
            throw new IllegalStateException("got an activity result that wasn't meant for us. this is a bug!");
        } else if (resultCode == -1) {
            this.messageCryptoMvpView.restartMessageCryptoProcessing();
        }
    }

    private void displayCryptoInfoDialog(MessageCryptoDisplayStatus displayStatus) {
        this.messageCryptoMvpView.showCryptoInfoDialog(displayStatus);
    }

    private void launchPendingIntent(CryptoResultAnnotation cryptoResultAnnotation2) {
        try {
            PendingIntent pendingIntent = cryptoResultAnnotation2.getOpenPgpPendingIntent();
            if (pendingIntent != null) {
                this.messageCryptoMvpView.startPendingIntentForCryptoPresenter(pendingIntent.getIntentSender(), Integer.valueOf((int) REQUEST_CODE_UNKNOWN_KEY), null, 0, 0, 0);
            }
        } catch (IntentSender.SendIntentException e) {
            Log.e("k9", "SendIntentException", e);
        }
    }

    public void onClickShowCryptoKey() {
        try {
            PendingIntent pendingIntent = this.cryptoResultAnnotation.getOpenPgpPendingIntent();
            if (pendingIntent != null) {
                this.messageCryptoMvpView.startPendingIntentForCryptoPresenter(pendingIntent.getIntentSender(), null, null, 0, 0, 0);
            }
        } catch (IntentSender.SendIntentException e) {
            Log.e("k9", "SendIntentException", e);
        }
    }

    public void onClickRetryCryptoOperation() {
        this.messageCryptoMvpView.restartMessageCryptoProcessing();
    }

    public void onClickShowMessageOverrideWarning() {
        this.overrideCryptoWarning = true;
        this.messageCryptoMvpView.redisplayMessage();
    }

    public Parcelable getDecryptionResultForReply() {
        if (this.cryptoResultAnnotation == null || !this.cryptoResultAnnotation.isOpenPgpResult()) {
            return null;
        }
        return this.cryptoResultAnnotation.getOpenPgpDecryptionResult();
    }

    @Nullable
    private static Drawable getOpenPgpApiProviderIcon(Context context, Account account) {
        try {
            String openPgpProvider = account.getOpenPgpProvider();
            if ("".equals(openPgpProvider)) {
                return null;
            }
            return context.getPackageManager().getApplicationIcon(openPgpProvider);
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }
}
