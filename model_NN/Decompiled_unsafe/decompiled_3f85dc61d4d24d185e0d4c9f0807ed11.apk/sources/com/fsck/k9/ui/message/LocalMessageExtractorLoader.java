package com.fsck.k9.ui.message;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.util.Log;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mailstore.LocalMessage;
import com.fsck.k9.mailstore.MessageViewInfo;
import com.fsck.k9.mailstore.MessageViewInfoExtractor;
import com.fsck.k9.ui.crypto.MessageCryptoAnnotations;

public class LocalMessageExtractorLoader extends AsyncTaskLoader<MessageViewInfo> {
    private static final MessageViewInfoExtractor messageViewInfoExtractor = MessageViewInfoExtractor.getInstance();
    @Nullable
    private MessageCryptoAnnotations annotations;
    private final Message message;
    private MessageViewInfo messageViewInfo;

    public LocalMessageExtractorLoader(Context context, Message message2, @Nullable MessageCryptoAnnotations annotations2) {
        super(context);
        this.message = message2;
        this.annotations = annotations2;
    }

    /* access modifiers changed from: protected */
    public void onStartLoading() {
        if (this.messageViewInfo != null) {
            super.deliverResult((Object) this.messageViewInfo);
        }
        if (takeContentChanged() || this.messageViewInfo == null) {
            forceLoad();
        }
    }

    public void deliverResult(MessageViewInfo messageViewInfo2) {
        this.messageViewInfo = messageViewInfo2;
        super.deliverResult((Object) messageViewInfo2);
    }

    @WorkerThread
    public MessageViewInfo loadInBackground() {
        try {
            return messageViewInfoExtractor.extractMessageForView(this.message, this.annotations);
        } catch (Exception e) {
            Log.e("k9", "Error while decoding message", e);
            return null;
        }
    }

    public boolean isCreatedFor(LocalMessage localMessage, MessageCryptoAnnotations messageCryptoAnnotations) {
        return this.annotations == messageCryptoAnnotations && this.message.equals(localMessage);
    }
}
