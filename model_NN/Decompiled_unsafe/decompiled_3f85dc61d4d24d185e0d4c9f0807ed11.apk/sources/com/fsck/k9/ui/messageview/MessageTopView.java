package com.fsck.k9.ui.messageview;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.StringRes;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.fsck.k9.Account;
import com.fsck.k9.R;
import com.fsck.k9.helper.Contacts;
import com.fsck.k9.mail.Address;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mailstore.MessageViewInfo;
import com.fsck.k9.ui.messageview.MessageContainerView;
import com.fsck.k9.view.MessageHeader;
import com.fsck.k9.view.ThemeUtils;
import com.fsck.k9.view.ToolableViewAnimator;
import org.openintents.openpgp.OpenPgpError;

public class MessageTopView extends LinearLayout {
    public static final int PROGRESS_MAX = 1000;
    public static final int PROGRESS_MAX_WITH_MARGIN = 950;
    public static final int PROGRESS_STEP_DURATION = 180;
    private AttachmentViewCallback attachmentCallback;
    private ViewGroup containerView;
    private boolean isShowingProgress;
    private TextView mDownloadRemainder;
    private MessageHeader mHeaderContainer;
    private LayoutInflater mInflater;
    /* access modifiers changed from: private */
    public MessageCryptoPresenter messageCryptoPresenter;
    private ProgressBar progressBar;
    private TextView progressText;
    private Button showPicturesButton;
    /* access modifiers changed from: private */
    public ToolableViewAnimator viewAnimator;

    public MessageTopView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void onFinishInflate() {
        super.onFinishInflate();
        this.mHeaderContainer = (MessageHeader) findViewById(R.id.header_container);
        this.mInflater = LayoutInflater.from(getContext());
        this.viewAnimator = (ToolableViewAnimator) findViewById(R.id.message_layout_animator);
        this.progressBar = (ProgressBar) findViewById(R.id.message_progress);
        this.progressText = (TextView) findViewById(R.id.message_progress_text);
        this.mDownloadRemainder = (TextView) findViewById(R.id.download_remainder);
        this.mDownloadRemainder.setVisibility(8);
        this.showPicturesButton = (Button) findViewById(R.id.show_pictures);
        setShowPicturesButtonListener();
        this.containerView = (ViewGroup) findViewById(R.id.message_container);
        hideHeaderView();
    }

    private void setShowPicturesButtonListener() {
        this.showPicturesButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MessageTopView.this.showPicturesInAllContainerViews();
            }
        });
    }

    /* access modifiers changed from: private */
    public void showPicturesInAllContainerViews() {
        View messageContainerViewCandidate = this.containerView.getChildAt(0);
        if (messageContainerViewCandidate instanceof MessageContainerView) {
            ((MessageContainerView) messageContainerViewCandidate).showPictures();
        }
        hideShowPicturesButton();
    }

    private void resetAndPrepareMessageView(MessageViewInfo messageViewInfo) {
        this.mDownloadRemainder.setVisibility(8);
        this.containerView.removeAllViews();
        setShowDownloadButton(messageViewInfo);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public void showMessage(Account account, MessageViewInfo messageViewInfo) {
        resetAndPrepareMessageView(messageViewInfo);
        boolean automaticallyLoadPictures = shouldAutomaticallyLoadPictures(account.getShowPictures(), messageViewInfo.message);
        MessageContainerView view = (MessageContainerView) this.mInflater.inflate((int) R.layout.message_container, this.containerView, false);
        this.containerView.addView(view);
        view.setWebView(this);
        view.setAtachmentsView(this);
        view.displayMessageViewContainer(messageViewInfo, new MessageContainerView.OnRenderingFinishedListener() {
            public void onLoadFinished() {
                MessageTopView.this.displayViewOnLoadFinished(true);
            }
        }, automaticallyLoadPictures, this.attachmentCallback);
        if (view.hasHiddenExternalImages()) {
            showShowPicturesButton();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public void showMessageCryptoWarning(MessageViewInfo messageViewInfo, Drawable providerIcon, @StringRes int warningTextRes) {
        resetAndPrepareMessageView(messageViewInfo);
        View view = this.mInflater.inflate((int) R.layout.message_content_crypto_warning, this.containerView, false);
        setCryptoProviderIcon(providerIcon, view);
        view.findViewById(R.id.crypto_warning_override).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MessageTopView.this.messageCryptoPresenter.onClickShowMessageOverrideWarning();
            }
        });
        ((TextView) view.findViewById(R.id.crypto_warning_text)).setText(warningTextRes);
        this.containerView.addView(view);
        displayViewOnLoadFinished(false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public void showMessageEncryptedButIncomplete(MessageViewInfo messageViewInfo, Drawable providerIcon) {
        resetAndPrepareMessageView(messageViewInfo);
        View view = this.mInflater.inflate((int) R.layout.message_content_crypto_incomplete, this.containerView, false);
        setCryptoProviderIcon(providerIcon, view);
        this.containerView.addView(view);
        displayViewOnLoadFinished(false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public void showMessageCryptoErrorView(MessageViewInfo messageViewInfo, Drawable providerIcon) {
        resetAndPrepareMessageView(messageViewInfo);
        View view = this.mInflater.inflate((int) R.layout.message_content_crypto_error, this.containerView, false);
        setCryptoProviderIcon(providerIcon, view);
        TextView cryptoErrorText = (TextView) view.findViewById(R.id.crypto_error_text);
        OpenPgpError openPgpError = messageViewInfo.cryptoResultAnnotation.getOpenPgpError();
        if (openPgpError != null) {
            cryptoErrorText.setText(openPgpError.getMessage());
        }
        this.containerView.addView(view);
        displayViewOnLoadFinished(false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public void showMessageCryptoCancelledView(MessageViewInfo messageViewInfo, Drawable providerIcon) {
        resetAndPrepareMessageView(messageViewInfo);
        View view = this.mInflater.inflate((int) R.layout.message_content_crypto_cancelled, this.containerView, false);
        setCryptoProviderIcon(providerIcon, view);
        view.findViewById(R.id.crypto_cancelled_retry).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MessageTopView.this.messageCryptoPresenter.onClickRetryCryptoOperation();
            }
        });
        this.containerView.addView(view);
        displayViewOnLoadFinished(false);
    }

    private void setCryptoProviderIcon(Drawable openPgpApiProviderIcon, View view) {
        ImageView cryptoProviderIcon = (ImageView) view.findViewById(R.id.crypto_error_icon);
        if (openPgpApiProviderIcon != null) {
            cryptoProviderIcon.setImageDrawable(openPgpApiProviderIcon);
            return;
        }
        cryptoProviderIcon.setImageResource(R.drawable.status_lock_error);
        cryptoProviderIcon.setColorFilter(ThemeUtils.getStyledColor(getContext(), (int) R.attr.openpgp_red));
    }

    public MessageHeader getMessageHeaderView() {
        return this.mHeaderContainer;
    }

    public void setHeaders(Message message, Account account) {
        this.mHeaderContainer.populate(message, account);
        this.mHeaderContainer.setVisibility(0);
    }

    public void setOnToggleFlagClickListener(View.OnClickListener listener) {
        this.mHeaderContainer.setOnFlagListener(listener);
    }

    public void showAllHeaders() {
        this.mHeaderContainer.onShowAdditionalHeaders();
    }

    public boolean additionalHeadersVisible() {
        return this.mHeaderContainer.additionalHeadersVisible();
    }

    private void hideHeaderView() {
        this.mHeaderContainer.setVisibility(8);
    }

    public void setOnDownloadButtonClickListener(View.OnClickListener listener) {
        this.mDownloadRemainder.setOnClickListener(listener);
    }

    public void setAttachmentCallback(AttachmentViewCallback callback) {
        this.attachmentCallback = callback;
    }

    public void setMessageCryptoPresenter(MessageCryptoPresenter messageCryptoPresenter2) {
        this.messageCryptoPresenter = messageCryptoPresenter2;
        this.mHeaderContainer.setOnCryptoClickListener(messageCryptoPresenter2);
    }

    public void enableDownloadButton() {
        this.mDownloadRemainder.setEnabled(true);
    }

    public void disableDownloadButton() {
        this.mDownloadRemainder.setEnabled(false);
    }

    private void setShowDownloadButton(MessageViewInfo messageViewInfo) {
        if (messageViewInfo.isMessageIncomplete) {
            this.mDownloadRemainder.setEnabled(true);
            this.mDownloadRemainder.setVisibility(0);
            return;
        }
        this.mDownloadRemainder.setVisibility(8);
    }

    private void showShowPicturesButton() {
        this.showPicturesButton.setVisibility(0);
    }

    private void hideShowPicturesButton() {
        this.showPicturesButton.setVisibility(8);
    }

    private boolean shouldAutomaticallyLoadPictures(Account.ShowPictures showPicturesSetting, Message message) {
        return showPicturesSetting == Account.ShowPictures.ALWAYS || shouldShowPicturesFromSender(showPicturesSetting, message);
    }

    private boolean shouldShowPicturesFromSender(Account.ShowPictures showPicturesSetting, Message message) {
        String senderEmailAddress;
        if (showPicturesSetting == Account.ShowPictures.ONLY_FROM_CONTACTS && (senderEmailAddress = getSenderEmailAddress(message)) != null) {
            return Contacts.getInstance(getContext()).isInContacts(senderEmailAddress);
        }
        return false;
    }

    private String getSenderEmailAddress(Message message) {
        Address[] from = message.getFrom();
        if (from == null || from.length == 0) {
            return null;
        }
        return from[0].getAddress();
    }

    public void displayViewOnLoadFinished(boolean finishProgressBar) {
        if (!finishProgressBar || !this.isShowingProgress) {
            this.viewAnimator.setDisplayedChild(2);
            return;
        }
        ObjectAnimator animator = ObjectAnimator.ofInt(this.progressBar, "progress", this.progressBar.getProgress(), 1000);
        animator.addListener(new AnimatorListenerAdapter() {
            public void onAnimationEnd(Animator animator) {
                MessageTopView.this.viewAnimator.setDisplayedChild(2);
            }
        });
        animator.setDuration(180L);
        animator.start();
    }

    public void setToLoadingState() {
        this.viewAnimator.setDisplayedChild(0);
        this.progressBar.setProgress(0);
        this.isShowingProgress = false;
    }

    public void setLoadingProgress(int progress, int max) {
        if (!this.isShowingProgress) {
            this.viewAnimator.setDisplayedChild(1);
            this.isShowingProgress = true;
            return;
        }
        int newPosition = (int) ((((float) progress) / ((float) max)) * 950.0f);
        int currentPosition = this.progressBar.getProgress();
        if (newPosition > currentPosition) {
            ObjectAnimator.ofInt(this.progressBar, "progress", currentPosition, newPosition).setDuration(180L).start();
            return;
        }
        this.progressBar.setProgress(newPosition);
    }
}
