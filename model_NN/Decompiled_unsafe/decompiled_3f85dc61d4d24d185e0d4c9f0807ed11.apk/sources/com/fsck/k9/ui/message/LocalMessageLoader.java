package com.fsck.k9.ui.message;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.util.Log;
import com.fsck.k9.Account;
import com.fsck.k9.activity.MessageReference;
import com.fsck.k9.controller.MessagingController;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mailstore.LocalMessage;

public class LocalMessageLoader extends AsyncTaskLoader<LocalMessage> {
    private final Account account;
    private final MessagingController controller;
    private LocalMessage message;
    private final MessageReference messageReference;

    public LocalMessageLoader(Context context, MessagingController controller2, Account account2, MessageReference messageReference2) {
        super(context);
        this.controller = controller2;
        this.account = account2;
        this.messageReference = messageReference2;
    }

    /* access modifiers changed from: protected */
    public void onStartLoading() {
        if (this.message != null) {
            super.deliverResult((Object) this.message);
        }
        if (takeContentChanged() || this.message == null) {
            forceLoad();
        }
    }

    public void deliverResult(LocalMessage message2) {
        this.message = message2;
        super.deliverResult((Object) message2);
    }

    public LocalMessage loadInBackground() {
        try {
            return loadMessageFromDatabase();
        } catch (Exception e) {
            Log.e("k9", "Error while loading message from database", e);
            return null;
        }
    }

    private LocalMessage loadMessageFromDatabase() throws MessagingException {
        return this.controller.loadMessage(this.account, this.messageReference.getFolderName(), this.messageReference.getUid());
    }

    public boolean isCreatedFor(MessageReference messageReference2) {
        return this.messageReference.equals(messageReference2);
    }
}
