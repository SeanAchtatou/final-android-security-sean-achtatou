package com.fsck.k9.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;
import org.apache.commons.io.IOUtils;

public class EolConvertingEditText extends EditText {
    public EolConvertingEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public String getCharacters() {
        return getText().toString().replace(IOUtils.LINE_SEPARATOR_UNIX, "\r\n");
    }

    public void setCharacters(CharSequence text) {
        setText(text.toString().replace("\r\n", IOUtils.LINE_SEPARATOR_UNIX));
    }
}
