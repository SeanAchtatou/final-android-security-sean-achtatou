package com.fsck.k9.ui.messageview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewStub;
import com.fsck.k9.R;
import com.fsck.k9.mailstore.AttachmentViewInfo;
import com.fsck.k9.view.ToolableViewAnimator;

public class LockedAttachmentView extends ToolableViewAnimator implements View.OnClickListener {
    private AttachmentViewInfo attachment;
    private AttachmentViewCallback attachmentCallback;
    private ViewStub attachmentViewStub;

    public LockedAttachmentView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public LockedAttachmentView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LockedAttachmentView(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        if (!isInEditMode()) {
            findViewById(R.id.locked_button).setOnClickListener(this);
            this.attachmentViewStub = (ViewStub) findViewById(R.id.attachment_stub);
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.locked_button:
                showUnlockedView();
                return;
            default:
                return;
        }
    }

    private void showUnlockedView() {
        if (this.attachmentViewStub == null) {
            throw new IllegalStateException("Cannot display unlocked attachment!");
        }
        AttachmentView attachmentView = (AttachmentView) this.attachmentViewStub.inflate();
        attachmentView.setAttachment(this.attachment);
        attachmentView.setCallback(this.attachmentCallback);
        this.attachmentViewStub = null;
        setDisplayedChild(1);
    }

    public void setAttachment(AttachmentViewInfo attachment2) {
        this.attachment = attachment2;
    }

    public void setCallback(AttachmentViewCallback attachmentCallback2) {
        this.attachmentCallback = attachmentCallback2;
    }
}
