package com.fsck.k9.ui.crypto;

import com.fsck.k9.mail.Part;
import com.fsck.k9.mailstore.CryptoResultAnnotation;
import java.util.HashMap;
import java.util.Map;

public class MessageCryptoAnnotations {
    private HashMap<Part, CryptoResultAnnotation> annotations = new HashMap<>();

    MessageCryptoAnnotations() {
    }

    /* access modifiers changed from: package-private */
    public void put(Part part, CryptoResultAnnotation annotation) {
        this.annotations.put(part, annotation);
    }

    public CryptoResultAnnotation get(Part part) {
        return this.annotations.get(part);
    }

    public boolean has(Part part) {
        return this.annotations.containsKey(part);
    }

    public Part findKeyForAnnotationWithReplacementPart(Part part) {
        for (Map.Entry<Part, CryptoResultAnnotation> entry : this.annotations.entrySet()) {
            if (part == ((CryptoResultAnnotation) entry.getValue()).getReplacementData()) {
                return (Part) entry.getKey();
            }
        }
        return null;
    }
}
