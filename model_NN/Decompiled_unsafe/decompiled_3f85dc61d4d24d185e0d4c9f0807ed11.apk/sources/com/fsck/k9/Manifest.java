package com.fsck.k9;

public final class Manifest {

    public static final class permission {
        public static final String DELETE_MESSAGES = "yahoo.mail.app.permission.DELETE_MESSAGES";
        public static final String READ_ATTACHMENT = "yahoo.mail.app.permission.READ_ATTACHMENT";
        public static final String READ_MESSAGES = "yahoo.mail.app.permission.READ_MESSAGES";
        public static final String REMOTE_CONTROL = "yahoo.mail.app.permission.REMOTE_CONTROL";
    }
}
