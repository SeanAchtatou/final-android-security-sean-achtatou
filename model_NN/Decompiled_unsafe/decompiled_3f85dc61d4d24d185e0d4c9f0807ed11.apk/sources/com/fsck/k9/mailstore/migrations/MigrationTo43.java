package com.fsck.k9.mailstore.migrations;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.fsck.k9.Account;
import com.fsck.k9.R;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mailstore.LocalFolder;
import com.fsck.k9.mailstore.LocalStore;
import com.fsck.k9.preferences.SettingsExporter;
import java.util.List;

class MigrationTo43 {
    MigrationTo43() {
    }

    public static void fixOutboxFolders(SQLiteDatabase db, MigrationsHelper migrationsHelper) {
        try {
            LocalStore localStore = migrationsHelper.getLocalStore();
            Account account = migrationsHelper.getAccount();
            Context context = migrationsHelper.getContext();
            if (new LocalFolder(localStore, "OUTBOX").exists()) {
                ContentValues cv = new ContentValues();
                cv.put("name", Account.OUTBOX);
                db.update(SettingsExporter.FOLDERS_ELEMENT, cv, "name = ?", new String[]{"OUTBOX"});
                Log.i("k9", "Renamed folder OUTBOX to K9MAIL_INTERNAL_OUTBOX");
            }
            LocalFolder obsoleteOutbox = new LocalFolder(localStore, context.getString(R.string.special_mailbox_name_outbox));
            if (obsoleteOutbox.exists()) {
                List<? extends Message> messages = obsoleteOutbox.getMessages(null, false);
                if (messages.size() > 0) {
                    obsoleteOutbox.moveMessages(messages, new LocalFolder(localStore, account.getDraftsFolderName()));
                }
                obsoleteOutbox.delete();
                obsoleteOutbox.delete(true);
            }
        } catch (Exception e) {
            Log.e("k9", "Error trying to fix the outbox folders", e);
        }
    }
}
