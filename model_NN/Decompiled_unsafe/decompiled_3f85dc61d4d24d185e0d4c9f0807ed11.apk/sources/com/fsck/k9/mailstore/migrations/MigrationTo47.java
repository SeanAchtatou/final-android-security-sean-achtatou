package com.fsck.k9.mailstore.migrations;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

class MigrationTo47 {
    MigrationTo47() {
    }

    /* JADX INFO: finally extract failed */
    public static void createThreadsTable(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS threads");
        db.execSQL("CREATE TABLE threads (id INTEGER PRIMARY KEY, message_id INTEGER, root INTEGER, parent INTEGER)");
        db.execSQL("DROP INDEX IF EXISTS threads_message_id");
        db.execSQL("CREATE INDEX IF NOT EXISTS threads_message_id ON threads (message_id)");
        db.execSQL("DROP INDEX IF EXISTS threads_root");
        db.execSQL("CREATE INDEX IF NOT EXISTS threads_root ON threads (root)");
        db.execSQL("DROP INDEX IF EXISTS threads_parent");
        db.execSQL("CREATE INDEX IF NOT EXISTS threads_parent ON threads (parent)");
        db.execSQL("INSERT INTO threads (message_id) SELECT id FROM messages");
        Cursor cursor = db.query("messages", new String[]{"id", "thread_root", "thread_parent"}, null, null, null, null, null);
        try {
            ContentValues cv = new ContentValues();
            while (cursor.moveToNext()) {
                cv.clear();
                long messageId = cursor.getLong(0);
                if (!cursor.isNull(1)) {
                    db.execSQL("UPDATE threads SET root = (SELECT t.id FROM threads t WHERE t.message_id = ?) WHERE message_id = ?", new String[]{Long.toString(cursor.getLong(1)), Long.toString(messageId)});
                }
                if (!cursor.isNull(2)) {
                    db.execSQL("UPDATE threads SET parent = (SELECT t.id FROM threads t WHERE t.message_id = ?) WHERE message_id = ?", new String[]{Long.toString(cursor.getLong(2)), Long.toString(messageId)});
                }
            }
            cursor.close();
            db.execSQL("DROP INDEX IF EXISTS msg_thread_root");
            db.execSQL("DROP INDEX IF EXISTS msg_thread_parent");
            ContentValues cv2 = new ContentValues();
            cv2.putNull("thread_root");
            cv2.putNull("thread_parent");
            db.update("messages", cv2, null, null);
        } catch (Throwable th) {
            cursor.close();
            throw th;
        }
    }
}
