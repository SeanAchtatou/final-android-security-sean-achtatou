package com.fsck.k9.mailstore.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import org.apache.commons.io.output.ThresholdingOutputStream;

public class DeferredFileOutputStream extends ThresholdingOutputStream {
    private OutputStream currentOutputStream;
    private final FileFactory fileFactory;
    private File outputFile;

    public DeferredFileOutputStream(int threshold, FileFactory fileFactory2) {
        super(threshold);
        this.fileFactory = fileFactory2;
        this.currentOutputStream = new ByteArrayOutputStream(threshold < 1024 ? 256 : threshold / 4);
    }

    /* access modifiers changed from: protected */
    public OutputStream getStream() throws IOException {
        return this.currentOutputStream;
    }

    private boolean isMemoryBacked() {
        return this.currentOutputStream instanceof ByteArrayOutputStream;
    }

    /* access modifiers changed from: protected */
    public void thresholdReached() throws IOException {
        if (this.outputFile != null) {
            throw new IllegalStateException("thresholdReached must not be called if we already have an output file!");
        } else if (!isMemoryBacked()) {
            throw new IllegalStateException("currentOutputStream must be memory-based at this point!");
        } else {
            this.outputFile = this.fileFactory.createFile();
            this.currentOutputStream = new FileOutputStream(this.outputFile);
            ((ByteArrayOutputStream) this.currentOutputStream).writeTo(this.currentOutputStream);
        }
    }

    public byte[] getData() {
        if (isMemoryBacked()) {
            return ((ByteArrayOutputStream) this.currentOutputStream).toByteArray();
        }
        throw new IllegalStateException("getData must only be called in memory-backed state!");
    }

    public File getFile() {
        if (!isMemoryBacked()) {
            return this.outputFile;
        }
        throw new IllegalStateException("getFile must only be called in file-backed state!");
    }
}
