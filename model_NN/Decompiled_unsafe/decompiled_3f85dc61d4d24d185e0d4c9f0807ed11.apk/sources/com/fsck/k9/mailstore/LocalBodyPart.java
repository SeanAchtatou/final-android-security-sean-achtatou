package com.fsck.k9.mailstore;

import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.internet.MimeBodyPart;

public class LocalBodyPart extends MimeBodyPart implements LocalPart {
    private final String accountUuid;
    private final LocalMessage message;
    private final long messagePartId;
    private final long size;

    public LocalBodyPart(String accountUuid2, LocalMessage message2, long messagePartId2, long size2) throws MessagingException {
        this.accountUuid = accountUuid2;
        this.message = message2;
        this.messagePartId = messagePartId2;
        this.size = size2;
    }

    public String getAccountUuid() {
        return this.accountUuid;
    }

    public long getId() {
        return this.messagePartId;
    }

    public long getSize() {
        return this.size;
    }

    public LocalMessage getMessage() {
        return this.message;
    }
}
