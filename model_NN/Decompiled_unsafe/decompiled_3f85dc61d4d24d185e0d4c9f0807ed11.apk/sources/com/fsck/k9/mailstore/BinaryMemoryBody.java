package com.fsck.k9.mailstore;

import com.fsck.k9.mail.Body;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.internet.RawDataBody;
import com.fsck.k9.mail.internet.SizeAware;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class BinaryMemoryBody implements Body, RawDataBody, SizeAware {
    private final byte[] data;
    private final String encoding;

    public BinaryMemoryBody(byte[] data2, String encoding2) {
        this.data = data2;
        this.encoding = encoding2;
    }

    public String getEncoding() {
        return this.encoding;
    }

    public InputStream getInputStream() throws MessagingException {
        return new ByteArrayInputStream(this.data);
    }

    public void setEncoding(String encoding2) throws MessagingException {
        throw new RuntimeException("nope");
    }

    public void writeTo(OutputStream out) throws IOException, MessagingException {
        out.write(this.data);
    }

    public long getSize() {
        return (long) this.data.length;
    }
}
