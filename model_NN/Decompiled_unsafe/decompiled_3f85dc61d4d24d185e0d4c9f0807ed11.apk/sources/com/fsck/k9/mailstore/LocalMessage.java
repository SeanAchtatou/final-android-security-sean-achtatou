package com.fsck.k9.mailstore;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.fsck.k9.Account;
import com.fsck.k9.activity.MessageReference;
import com.fsck.k9.mail.Address;
import com.fsck.k9.mail.Flag;
import com.fsck.k9.mail.Folder;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.internet.MimeMessage;
import com.fsck.k9.mail.message.MessageHeaderParser;
import com.fsck.k9.mailstore.LockableDatabase;
import com.fsck.k9.message.extractors.PreviewResult;
import com.fsck.k9.provider.EmailProvider;
import java.io.ByteArrayInputStream;
import java.util.Date;

public class LocalMessage extends MimeMessage {
    /* access modifiers changed from: private */
    public final LocalStore localStore;
    private int mAttachmentCount;
    /* access modifiers changed from: private */
    public long mId;
    private String mPreview = "";
    protected MessageReference mReference;
    private long mRootId;
    private String mSubject;
    private long mThreadId;
    /* access modifiers changed from: private */
    public long messagePartId;
    private String mimeType;
    private PreviewResult.PreviewType previewType;

    private LocalMessage(LocalStore localStore2) {
        this.localStore = localStore2;
    }

    LocalMessage(LocalStore localStore2, String uid, Folder folder) {
        this.localStore = localStore2;
        this.mUid = uid;
        this.mFolder = folder;
    }

    /* access modifiers changed from: package-private */
    public void populateFromGetMessageCursor(Cursor cursor) throws MessagingException {
        String subject = cursor.getString(0);
        if (subject == null) {
            subject = "";
        }
        setSubject(subject);
        Address[] from = Address.unpack(cursor.getString(1));
        if (from.length > 0) {
            setFrom(from[0]);
        }
        setInternalSentDate(new Date(cursor.getLong(2)));
        setUid(cursor.getString(3));
        String flagList = cursor.getString(4);
        if (flagList != null && flagList.length() > 0) {
            String[] flags = flagList.split(",");
            int length = flags.length;
            for (int i = 0; i < length; i++) {
                String flag = flags[i];
                try {
                    setFlagInternal(Flag.valueOf(flag), true);
                } catch (Exception e) {
                    if (!"X_BAD_FLAG".equals(flag)) {
                        Log.w("k9", "Unable to parse flag " + flag);
                    }
                }
            }
        }
        this.mId = cursor.getLong(5);
        setRecipients(Message.RecipientType.TO, Address.unpack(cursor.getString(6)));
        setRecipients(Message.RecipientType.CC, Address.unpack(cursor.getString(7)));
        setRecipients(Message.RecipientType.BCC, Address.unpack(cursor.getString(8)));
        setReplyTo(Address.unpack(cursor.getString(9)));
        this.mAttachmentCount = cursor.getInt(10);
        setInternalDate(new Date(cursor.getLong(11)));
        setMessageId(cursor.getString(12));
        this.previewType = DatabasePreviewType.fromDatabaseValue(cursor.getString(24)).getPreviewType();
        if (this.previewType == PreviewResult.PreviewType.TEXT) {
            this.mPreview = cursor.getString(14);
        } else {
            this.mPreview = "";
        }
        if (this.mFolder == null) {
            LocalFolder f = new LocalFolder(this.localStore, (long) cursor.getInt(13));
            f.open(0);
            this.mFolder = f;
        }
        this.mThreadId = cursor.isNull(15) ? -1 : cursor.getLong(15);
        this.mRootId = cursor.isNull(16) ? -1 : cursor.getLong(16);
        boolean deleted = cursor.getInt(17) == 1;
        boolean read = cursor.getInt(18) == 1;
        boolean flagged = cursor.getInt(19) == 1;
        boolean answered = cursor.getInt(20) == 1;
        boolean forwarded = cursor.getInt(21) == 1;
        setFlagInternal(Flag.DELETED, deleted);
        setFlagInternal(Flag.SEEN, read);
        setFlagInternal(Flag.FLAGGED, flagged);
        setFlagInternal(Flag.ANSWERED, answered);
        setFlagInternal(Flag.FORWARDED, forwarded);
        this.messagePartId = cursor.getLong(22);
        this.mimeType = cursor.getString(23);
        byte[] header = cursor.getBlob(25);
        if (header != null) {
            MessageHeaderParser.parse(this, new ByteArrayInputStream(header));
        } else {
            Log.d("k9", "No headers available for this message!");
        }
    }

    public long getMessagePartId() {
        return this.messagePartId;
    }

    public String getMimeType() {
        return this.mimeType;
    }

    public PreviewResult.PreviewType getPreviewType() {
        return this.previewType;
    }

    public String getPreview() {
        return this.mPreview;
    }

    public String getSubject() {
        return this.mSubject;
    }

    public void setSubject(String subject) {
        this.mSubject = subject;
    }

    public void setMessageId(String messageId) {
        this.mMessageId = messageId;
    }

    public void setUid(String uid) {
        super.setUid(uid);
        this.mReference = null;
    }

    public boolean hasAttachments() {
        return this.mAttachmentCount > 0;
    }

    public int getAttachmentCount() {
        return this.mAttachmentCount;
    }

    public void setFrom(Address from) {
        this.mFrom = new Address[]{from};
    }

    public void setReplyTo(Address[] replyTo) {
        if (replyTo == null || replyTo.length == 0) {
            this.mReplyTo = null;
        } else {
            this.mReplyTo = replyTo;
        }
    }

    public void setRecipients(Message.RecipientType type, Address[] addresses) {
        if (type == Message.RecipientType.TO) {
            if (addresses == null || addresses.length == 0) {
                this.mTo = null;
            } else {
                this.mTo = addresses;
            }
        } else if (type == Message.RecipientType.CC) {
            if (addresses == null || addresses.length == 0) {
                this.mCc = null;
            } else {
                this.mCc = addresses;
            }
        } else if (type != Message.RecipientType.BCC) {
            throw new IllegalArgumentException("Unrecognized recipient type.");
        } else if (addresses == null || addresses.length == 0) {
            this.mBcc = null;
        } else {
            this.mBcc = addresses;
        }
    }

    public void setFlagInternal(Flag flag, boolean set) throws MessagingException {
        super.setFlag(flag, set);
    }

    public long getId() {
        return this.mId;
    }

    public void setFlag(final Flag flag, final boolean set) throws MessagingException {
        try {
            this.localStore.database.execute(true, new LockableDatabase.DbCallback<Void>() {
                public Void doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException, UnavailableStorageException {
                    int i;
                    int i2;
                    int i3;
                    int i4;
                    try {
                        if (flag == Flag.DELETED && set) {
                            LocalMessage.this.delete();
                        }
                        LocalMessage.super.setFlag(flag, set);
                        ContentValues cv = new ContentValues();
                        LocalStore unused = LocalMessage.this.localStore;
                        cv.put(EmailProvider.MessageColumns.FLAGS, LocalStore.serializeFlags(LocalMessage.this.getFlags()));
                        if (LocalMessage.this.isSet(Flag.SEEN)) {
                            i = 1;
                        } else {
                            i = 0;
                        }
                        cv.put(EmailProvider.MessageColumns.READ, Integer.valueOf(i));
                        if (LocalMessage.this.isSet(Flag.FLAGGED)) {
                            i2 = 1;
                        } else {
                            i2 = 0;
                        }
                        cv.put(EmailProvider.MessageColumns.FLAGGED, Integer.valueOf(i2));
                        if (LocalMessage.this.isSet(Flag.ANSWERED)) {
                            i3 = 1;
                        } else {
                            i3 = 0;
                        }
                        cv.put(EmailProvider.MessageColumns.ANSWERED, Integer.valueOf(i3));
                        if (LocalMessage.this.isSet(Flag.FORWARDED)) {
                            i4 = 1;
                        } else {
                            i4 = 0;
                        }
                        cv.put(EmailProvider.MessageColumns.FORWARDED, Integer.valueOf(i4));
                        db.update("messages", cv, "id = ?", new String[]{Long.toString(LocalMessage.this.mId)});
                        return null;
                    } catch (MessagingException e) {
                        throw new LockableDatabase.WrappedException(e);
                    }
                }
            });
            this.localStore.notifyChange();
        } catch (LockableDatabase.WrappedException e) {
            throw ((MessagingException) e.getCause());
        }
    }

    /* access modifiers changed from: private */
    public void delete() throws MessagingException {
        try {
            this.localStore.database.execute(true, new LockableDatabase.DbCallback<Void>() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
                 arg types: [java.lang.String, int]
                 candidates:
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
                public Void doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException, UnavailableStorageException {
                    ContentValues cv = new ContentValues();
                    cv.put(EmailProvider.InternalMessageColumns.DELETED, (Integer) 1);
                    cv.put("empty", (Integer) 1);
                    cv.putNull("subject");
                    cv.putNull(EmailProvider.MessageColumns.SENDER_LIST);
                    cv.putNull("date");
                    cv.putNull(EmailProvider.MessageColumns.TO_LIST);
                    cv.putNull(EmailProvider.MessageColumns.CC_LIST);
                    cv.putNull(EmailProvider.MessageColumns.BCC_LIST);
                    cv.putNull("preview");
                    cv.putNull(EmailProvider.MessageColumns.REPLY_TO_LIST);
                    cv.putNull("message_part_id");
                    db.update("messages", cv, "id = ?", new String[]{Long.toString(LocalMessage.this.mId)});
                    try {
                        ((LocalFolder) LocalMessage.this.mFolder).deleteMessagePartsAndDataFromDisk(LocalMessage.this.messagePartId);
                        return null;
                    } catch (MessagingException e) {
                        throw new LockableDatabase.WrappedException(e);
                    }
                }
            });
            this.localStore.notifyChange();
        } catch (LockableDatabase.WrappedException e) {
            throw ((MessagingException) e.getCause());
        }
    }

    public void debugClearLocalData() throws MessagingException {
        throw new AssertionError("method must only be used in debug build!");
    }

    public void destroy() throws MessagingException {
        try {
            this.localStore.database.execute(true, new LockableDatabase.DbCallback<Void>() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
                 arg types: [java.lang.String, int]
                 candidates:
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
                  ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
                public Void doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException, UnavailableStorageException {
                    try {
                        LocalFolder localFolder = (LocalFolder) LocalMessage.this.mFolder;
                        localFolder.deleteMessagePartsAndDataFromDisk(LocalMessage.this.messagePartId);
                        LocalMessage.this.deleteFulltextIndexEntry(db, LocalMessage.this.mId);
                        if (LocalMessage.this.hasThreadChildren(db, LocalMessage.this.mId)) {
                            ContentValues cv = new ContentValues();
                            cv.put("id", Long.valueOf(LocalMessage.this.mId));
                            cv.put(EmailProvider.MessageColumns.FOLDER_ID, Long.valueOf(localFolder.getId()));
                            cv.put(EmailProvider.InternalMessageColumns.DELETED, (Integer) 0);
                            cv.put("message_id", LocalMessage.this.getMessageId());
                            cv.put("empty", (Integer) 1);
                            db.replace("messages", null, cv);
                        } else {
                            long currentId = LocalMessage.this.getEmptyThreadParent(db, LocalMessage.this.mId);
                            LocalMessage.this.deleteMessageRow(db, LocalMessage.this.mId);
                            while (currentId != -1 && !LocalMessage.this.hasThreadChildren(db, currentId)) {
                                long newId = LocalMessage.this.getEmptyThreadParent(db, currentId);
                                LocalMessage.this.deleteMessageRow(db, currentId);
                                currentId = newId;
                            }
                        }
                        return null;
                    } catch (MessagingException e) {
                        throw new LockableDatabase.WrappedException(e);
                    }
                }
            });
            this.localStore.notifyChange();
        } catch (LockableDatabase.WrappedException e) {
            throw ((MessagingException) e.getCause());
        }
    }

    /* access modifiers changed from: private */
    public long getEmptyThreadParent(SQLiteDatabase db, long messageId) {
        Cursor cursor = db.rawQuery("SELECT m.id FROM threads t1 JOIN threads t2 ON (t1.parent = t2.id) LEFT JOIN messages m ON (t2.message_id = m.id) WHERE t1.message_id = ? AND m.empty = 1", new String[]{Long.toString(messageId)});
        try {
            return (!cursor.moveToFirst() || cursor.isNull(0)) ? -1 : cursor.getLong(0);
        } finally {
            cursor.close();
        }
    }

    /* access modifiers changed from: private */
    public boolean hasThreadChildren(SQLiteDatabase db, long messageId) {
        boolean z = true;
        Cursor cursor = db.rawQuery("SELECT COUNT(t2.id) FROM threads t1 JOIN threads t2 ON (t2.parent = t1.id) WHERE t1.message_id = ?", new String[]{Long.toString(messageId)});
        try {
            if (!cursor.moveToFirst() || cursor.isNull(0) || cursor.getLong(0) <= 0) {
                z = false;
            }
            return z;
        } finally {
            cursor.close();
        }
    }

    /* access modifiers changed from: private */
    public void deleteFulltextIndexEntry(SQLiteDatabase db, long messageId) {
        db.delete("messages_fulltext", "docid = ?", new String[]{Long.toString(messageId)});
    }

    /* access modifiers changed from: private */
    public void deleteMessageRow(SQLiteDatabase db, long messageId) {
        String[] idArg = {Long.toString(messageId)};
        db.delete("messages", "id = ?", idArg);
        db.delete("threads", "message_id = ?", idArg);
    }

    public LocalMessage clone() {
        LocalMessage message = new LocalMessage(this.localStore);
        super.copy((MimeMessage) message);
        message.mId = this.mId;
        message.mAttachmentCount = this.mAttachmentCount;
        message.mSubject = this.mSubject;
        message.mPreview = this.mPreview;
        return message;
    }

    public long getThreadId() {
        return this.mThreadId;
    }

    public long getRootId() {
        return this.mRootId;
    }

    public Account getAccount() {
        return this.localStore.getAccount();
    }

    public MessageReference makeMessageReference() {
        if (this.mReference == null) {
            this.mReference = new MessageReference(getFolder().getAccountUuid(), getFolder().getName(), this.mUid, null);
        }
        return this.mReference;
    }

    /* access modifiers changed from: protected */
    public void copy(MimeMessage destination) {
        super.copy(destination);
        if (destination instanceof LocalMessage) {
            ((LocalMessage) destination).mReference = this.mReference;
        }
    }

    public LocalFolder getFolder() {
        return (LocalFolder) super.getFolder();
    }

    public String getUri() {
        return "email://messages/" + getAccount().getAccountNumber() + "/" + getFolder().getName() + "/" + getUid();
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        String thisAccountUuid = getAccountUuid();
        String thatAccountUuid = ((LocalMessage) o).getAccountUuid();
        if (thisAccountUuid != null) {
            return thisAccountUuid.equals(thatAccountUuid);
        }
        if (thatAccountUuid != null) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (super.hashCode() * 31) + (getAccountUuid() != null ? getAccountUuid().hashCode() : 0);
    }

    private String getAccountUuid() {
        return getAccount().getUuid();
    }

    public boolean isBodyMissing() {
        return getBody() == null;
    }
}
