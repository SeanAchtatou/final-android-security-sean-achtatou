package com.fsck.k9.mailstore;

public interface LocalPart {
    String getAccountUuid();

    long getId();

    LocalMessage getMessage();

    long getSize();
}
