package com.fsck.k9.mailstore;

import android.app.PendingIntent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.fsck.k9.mail.internet.MimeBodyPart;
import org.openintents.openpgp.OpenPgpDecryptionResult;
import org.openintents.openpgp.OpenPgpError;
import org.openintents.openpgp.OpenPgpSignatureResult;

public final class CryptoResultAnnotation {
    private final CryptoResultAnnotation encapsulatedResult;
    @NonNull
    private final CryptoError errorType;
    private final OpenPgpDecryptionResult openPgpDecryptionResult;
    private final OpenPgpError openPgpError;
    private final PendingIntent openPgpPendingIntent;
    private final OpenPgpSignatureResult openPgpSignatureResult;
    private final MimeBodyPart replacementData;

    public enum CryptoError {
        OPENPGP_OK,
        OPENPGP_UI_CANCELED,
        OPENPGP_API_RETURNED_ERROR,
        OPENPGP_SIGNED_BUT_INCOMPLETE,
        OPENPGP_ENCRYPTED_BUT_INCOMPLETE,
        SIGNED_BUT_UNSUPPORTED,
        ENCRYPTED_BUT_UNSUPPORTED
    }

    private CryptoResultAnnotation(@NonNull CryptoError errorType2, MimeBodyPart replacementData2, OpenPgpDecryptionResult openPgpDecryptionResult2, OpenPgpSignatureResult openPgpSignatureResult2, PendingIntent openPgpPendingIntent2, OpenPgpError openPgpError2) {
        this.errorType = errorType2;
        this.replacementData = replacementData2;
        this.openPgpDecryptionResult = openPgpDecryptionResult2;
        this.openPgpSignatureResult = openPgpSignatureResult2;
        this.openPgpPendingIntent = openPgpPendingIntent2;
        this.openPgpError = openPgpError2;
        this.encapsulatedResult = null;
    }

    private CryptoResultAnnotation(CryptoResultAnnotation annotation, CryptoResultAnnotation encapsulatedResult2) {
        if (annotation.encapsulatedResult != null) {
            throw new AssertionError("cannot replace an encapsulated result, this is a bug!");
        }
        this.errorType = annotation.errorType;
        this.replacementData = annotation.replacementData;
        this.openPgpDecryptionResult = annotation.openPgpDecryptionResult;
        this.openPgpSignatureResult = annotation.openPgpSignatureResult;
        this.openPgpPendingIntent = annotation.openPgpPendingIntent;
        this.openPgpError = annotation.openPgpError;
        this.encapsulatedResult = encapsulatedResult2;
    }

    public static CryptoResultAnnotation createOpenPgpResultAnnotation(OpenPgpDecryptionResult decryptionResult, OpenPgpSignatureResult signatureResult, PendingIntent pendingIntent, MimeBodyPart replacementPart) {
        return new CryptoResultAnnotation(CryptoError.OPENPGP_OK, replacementPart, decryptionResult, signatureResult, pendingIntent, null);
    }

    public static CryptoResultAnnotation createErrorAnnotation(CryptoError error, MimeBodyPart replacementData2) {
        if (error != CryptoError.OPENPGP_OK) {
            return new CryptoResultAnnotation(error, replacementData2, null, null, null, null);
        }
        throw new AssertionError("CryptoError must be actual error state!");
    }

    public static CryptoResultAnnotation createOpenPgpCanceledAnnotation() {
        return new CryptoResultAnnotation(CryptoError.OPENPGP_UI_CANCELED, null, null, null, null, null);
    }

    public static CryptoResultAnnotation createOpenPgpErrorAnnotation(OpenPgpError error) {
        return new CryptoResultAnnotation(CryptoError.OPENPGP_API_RETURNED_ERROR, null, null, null, null, error);
    }

    public boolean isOpenPgpResult() {
        return (this.openPgpDecryptionResult == null || this.openPgpSignatureResult == null) ? false : true;
    }

    public boolean hasSignatureResult() {
        return (this.openPgpSignatureResult == null || this.openPgpSignatureResult.getResult() == -1) ? false : true;
    }

    @Nullable
    public OpenPgpDecryptionResult getOpenPgpDecryptionResult() {
        return this.openPgpDecryptionResult;
    }

    @Nullable
    public OpenPgpSignatureResult getOpenPgpSignatureResult() {
        return this.openPgpSignatureResult;
    }

    @Nullable
    public PendingIntent getOpenPgpPendingIntent() {
        return this.openPgpPendingIntent;
    }

    @Nullable
    public OpenPgpError getOpenPgpError() {
        return this.openPgpError;
    }

    @NonNull
    public CryptoError getErrorType() {
        return this.errorType;
    }

    public boolean hasReplacementData() {
        return this.replacementData != null;
    }

    @Nullable
    public MimeBodyPart getReplacementData() {
        return this.replacementData;
    }

    @NonNull
    public CryptoResultAnnotation withEncapsulatedResult(CryptoResultAnnotation resultAnnotation) {
        return new CryptoResultAnnotation(this, resultAnnotation);
    }

    public boolean hasEncapsulatedResult() {
        return this.encapsulatedResult != null;
    }

    public CryptoResultAnnotation getEncapsulatedResult() {
        return this.encapsulatedResult;
    }
}
