package com.fsck.k9.mailstore;

import android.support.annotation.Nullable;
import android.util.Log;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.internet.RawDataBody;
import com.fsck.k9.mail.internet.SizeAware;
import com.fsck.k9.mailstore.util.DeferredFileOutputStream;
import com.fsck.k9.mailstore.util.FileFactory;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.apache.commons.io.IOUtils;

public class DeferredFileBody implements RawDataBody, SizeAware {
    public static final int DEFAULT_MEMORY_BACKED_THRESHOLD = 8192;
    /* access modifiers changed from: private */
    @Nullable
    public byte[] data;
    private final String encoding;
    /* access modifiers changed from: private */
    public File file;
    private final FileFactory fileFactory;
    private final int memoryBackedThreshold;

    public DeferredFileBody(FileFactory fileFactory2, String transferEncoding) {
        this(8192, fileFactory2, transferEncoding);
    }

    DeferredFileBody(int memoryBackedThreshold2, FileFactory fileFactory2, String transferEncoding) {
        this.fileFactory = fileFactory2;
        this.memoryBackedThreshold = memoryBackedThreshold2;
        this.encoding = transferEncoding;
    }

    public OutputStream getOutputStream() throws IOException {
        return new DeferredFileOutputStream(this.memoryBackedThreshold, this.fileFactory) {
            public void close() throws IOException {
                super.close();
                if (isThresholdExceeded()) {
                    File unused = DeferredFileBody.this.file = getFile();
                } else {
                    byte[] unused2 = DeferredFileBody.this.data = getData();
                }
            }
        };
    }

    public InputStream getInputStream() throws MessagingException {
        try {
            if (this.file != null) {
                Log.d("k9", "Decrypted data is file-backed.");
                return new BufferedInputStream(new FileInputStream(this.file));
            } else if (this.data != null) {
                Log.d("k9", "Decrypted data is memory-backed.");
                return new ByteArrayInputStream(this.data);
            } else {
                throw new IllegalStateException("Data must be fully written before it can be read!");
            }
        } catch (IOException ioe) {
            throw new MessagingException("Unable to open body", ioe);
        }
    }

    public long getSize() {
        if (this.file != null) {
            return this.file.length();
        }
        if (this.data != null) {
            return (long) this.data.length;
        }
        throw new IllegalStateException("Data must be fully written before it can be read!");
    }

    public File getFile() throws IOException {
        if (this.file == null) {
            writeMemoryToFile();
        }
        return this.file;
    }

    private void writeMemoryToFile() throws IOException {
        if (this.file != null) {
            throw new IllegalStateException("Body is already file-backed!");
        } else if (this.data == null) {
            throw new IllegalStateException("Data must be fully written before it can be read!");
        } else {
            Log.d("k9", "Writing body to file for attachment access");
            this.file = this.fileFactory.createFile();
            FileOutputStream fos = new FileOutputStream(this.file);
            fos.write(this.data);
            fos.close();
            this.data = null;
        }
    }

    public void setEncoding(String encoding2) throws MessagingException {
        throw new UnsupportedOperationException("Cannot re-encode a DecryptedTempFileBody!");
    }

    public void writeTo(OutputStream out) throws IOException, MessagingException {
        IOUtils.copy(getInputStream(), out);
    }

    public String getEncoding() {
        return this.encoding;
    }
}
