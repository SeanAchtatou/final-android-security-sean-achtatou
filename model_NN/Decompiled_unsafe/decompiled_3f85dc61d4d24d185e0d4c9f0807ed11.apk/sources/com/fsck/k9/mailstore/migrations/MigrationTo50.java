package com.fsck.k9.mailstore.migrations;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import com.fsck.k9.mail.Folder;
import com.fsck.k9.preferences.SettingsExporter;

class MigrationTo50 {
    MigrationTo50() {
    }

    public static void foldersAddNotifyClassColumn(SQLiteDatabase db, MigrationsHelper migrationsHelper) {
        try {
            db.execSQL("ALTER TABLE folders ADD notify_class TEXT default '" + Folder.FolderClass.INHERITED.name() + "'");
        } catch (SQLiteException e) {
            if (!e.getMessage().startsWith("duplicate column name:")) {
                throw e;
            }
        }
        ContentValues cv = new ContentValues();
        cv.put("notify_class", Folder.FolderClass.FIRST_CLASS.name());
        db.update(SettingsExporter.FOLDERS_ELEMENT, cv, "name = ?", new String[]{migrationsHelper.getAccount().getInboxFolderName()});
    }
}
