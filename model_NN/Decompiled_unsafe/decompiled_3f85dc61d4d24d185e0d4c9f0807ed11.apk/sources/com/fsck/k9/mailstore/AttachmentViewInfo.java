package com.fsck.k9.mailstore;

import android.net.Uri;
import com.fsck.k9.mail.Part;

public class AttachmentViewInfo {
    public static final long UNKNOWN_SIZE = -1;
    public final String displayName;
    public final boolean inlineAttachment;
    public final Uri internalUri;
    public final boolean isContentAvailable;
    public final String mimeType;
    public final Part part;
    public final long size;

    public AttachmentViewInfo(String mimeType2, String displayName2, long size2, Uri internalUri2, boolean inlineAttachment2, Part part2, boolean isContentAvailable2) {
        this.mimeType = mimeType2;
        this.displayName = displayName2;
        this.size = size2;
        this.internalUri = internalUri2;
        this.inlineAttachment = inlineAttachment2;
        this.part = part2;
        this.isContentAvailable = isContentAvailable2;
    }
}
