package com.fsck.k9.mailstore;

import android.content.Context;
import android.support.annotation.WorkerThread;
import com.fsck.k9.Globals;
import com.fsck.k9.R;
import com.fsck.k9.helper.HtmlConverter;
import com.fsck.k9.helper.HtmlSanitizer;
import com.fsck.k9.mail.Address;
import com.fsck.k9.mail.Flag;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.Part;
import com.fsck.k9.mail.internet.MessageExtractor;
import com.fsck.k9.mail.internet.MimeUtility;
import com.fsck.k9.mail.internet.Viewable;
import com.fsck.k9.message.extractors.AttachmentInfoExtractor;
import com.fsck.k9.ui.crypto.MessageCryptoAnnotations;
import com.fsck.k9.ui.crypto.MessageCryptoSplitter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.apache.james.mime4j.dom.field.ContentDispositionField;

public class MessageViewInfoExtractor {
    private static final String FILENAME_PREFIX = "----- ";
    private static final int FILENAME_PREFIX_LENGTH = FILENAME_PREFIX.length();
    private static final String FILENAME_SUFFIX = " ";
    private static final int FILENAME_SUFFIX_LENGTH = FILENAME_SUFFIX.length();
    private static final String TEXT_DIVIDER = "------------------------------------------------------------------------";
    private static final int TEXT_DIVIDER_LENGTH = TEXT_DIVIDER.length();
    private final AttachmentInfoExtractor attachmentInfoExtractor;
    private final Context context;
    private final HtmlSanitizer htmlSanitizer;

    public static MessageViewInfoExtractor getInstance() {
        return new MessageViewInfoExtractor(Globals.getContext(), AttachmentInfoExtractor.getInstance(), HtmlSanitizer.getInstance());
    }

    MessageViewInfoExtractor(Context context2, AttachmentInfoExtractor attachmentInfoExtractor2, HtmlSanitizer htmlSanitizer2) {
        this.context = context2;
        this.attachmentInfoExtractor = attachmentInfoExtractor2;
        this.htmlSanitizer = htmlSanitizer2;
    }

    @WorkerThread
    public MessageViewInfo extractMessageForView(Message message, MessageCryptoAnnotations annotations) throws MessagingException {
        Part rootPart;
        CryptoResultAnnotation cryptoResultAnnotation;
        List<Part> extraParts;
        MessageCryptoSplitter.CryptoMessageParts cryptoMessageParts = MessageCryptoSplitter.split(message, annotations);
        if (cryptoMessageParts != null) {
            rootPart = cryptoMessageParts.contentPart;
            cryptoResultAnnotation = cryptoMessageParts.contentCryptoAnnotation;
            extraParts = cryptoMessageParts.extraParts;
        } else {
            rootPart = message;
            cryptoResultAnnotation = null;
            extraParts = null;
        }
        List<AttachmentViewInfo> attachmentInfos = new ArrayList<>();
        ViewableExtractedText viewable = extractViewableAndAttachments(Collections.singletonList(rootPart), attachmentInfos);
        List<AttachmentViewInfo> extraAttachmentInfos = new ArrayList<>();
        String extraViewableText = null;
        if (extraParts != null) {
            extraViewableText = extractViewableAndAttachments(extraParts, extraAttachmentInfos).text;
        }
        return MessageViewInfo.createWithExtractedContent(message, !message.isSet(Flag.X_DOWNLOADED_FULL) || MessageExtractor.hasMissingParts(message), rootPart, viewable.html, attachmentInfos, cryptoResultAnnotation, AttachmentResolver.createFromPart(rootPart), extraViewableText, extraAttachmentInfos);
    }

    private ViewableExtractedText extractViewableAndAttachments(List<Part> parts, List<AttachmentViewInfo> attachmentInfos) throws MessagingException {
        ArrayList<Viewable> viewableParts = new ArrayList<>();
        ArrayList<Part> attachments = new ArrayList<>();
        for (Part part : parts) {
            MessageExtractor.findViewablesAndAttachments(part, viewableParts, attachments);
        }
        attachmentInfos.addAll(this.attachmentInfoExtractor.extractAttachmentInfoForView(attachments));
        return extractTextFromViewables(viewableParts);
    }

    /* access modifiers changed from: package-private */
    public ViewableExtractedText extractTextFromViewables(List<Viewable> viewables) throws MessagingException {
        boolean hideDivider = true;
        try {
            StringBuilder text = new StringBuilder();
            StringBuilder html = new StringBuilder();
            for (Viewable viewable : viewables) {
                if (viewable instanceof Viewable.Textual) {
                    text.append((CharSequence) buildText(viewable, !hideDivider));
                    html.append((CharSequence) buildHtml(viewable, !hideDivider));
                    hideDivider = false;
                } else if (viewable instanceof Viewable.MessageHeader) {
                    Viewable.MessageHeader header = (Viewable.MessageHeader) viewable;
                    Part containerPart = header.getContainerPart();
                    Message innerMessage = header.getMessage();
                    addTextDivider(text, containerPart, !hideDivider);
                    addMessageHeaderText(text, innerMessage);
                    addHtmlDivider(html, containerPart, !hideDivider);
                    addMessageHeaderHtml(html, innerMessage);
                    hideDivider = true;
                } else if (viewable instanceof Viewable.Alternative) {
                    Viewable.Alternative alternative = (Viewable.Alternative) viewable;
                    List<Viewable> textAlternative = alternative.getText().isEmpty() ? alternative.getHtml() : alternative.getText();
                    List<Viewable> htmlAlternative = alternative.getHtml().isEmpty() ? alternative.getText() : alternative.getHtml();
                    boolean divider = !hideDivider;
                    for (Viewable textViewable : textAlternative) {
                        text.append((CharSequence) buildText(textViewable, divider));
                        divider = true;
                    }
                    boolean divider2 = !hideDivider;
                    for (Viewable htmlViewable : htmlAlternative) {
                        html.append((CharSequence) buildHtml(htmlViewable, divider2));
                        divider2 = true;
                    }
                    hideDivider = false;
                }
            }
            return new ViewableExtractedText(text.toString(), this.htmlSanitizer.sanitize(HtmlConverter.wrapMessageContent(html)));
        } catch (Exception e) {
            throw new MessagingException("Couldn't extract viewable parts", e);
        }
    }

    private StringBuilder buildHtml(Viewable viewable, boolean prependDivider) {
        StringBuilder html = new StringBuilder();
        if (viewable instanceof Viewable.Textual) {
            Part part = ((Viewable.Textual) viewable).getPart();
            addHtmlDivider(html, part, prependDivider);
            String t = MessageExtractor.getTextFromPart(part);
            if (t == null) {
                t = "";
            } else if (viewable instanceof Viewable.Text) {
                t = HtmlConverter.textToHtml(t);
            }
            html.append(t);
        } else if (viewable instanceof Viewable.Alternative) {
            Viewable.Alternative alternative = (Viewable.Alternative) viewable;
            boolean divider = prependDivider;
            for (Viewable htmlViewable : alternative.getHtml().isEmpty() ? alternative.getText() : alternative.getHtml()) {
                html.append((CharSequence) buildHtml(htmlViewable, divider));
                divider = true;
            }
        }
        return html;
    }

    private StringBuilder buildText(Viewable viewable, boolean prependDivider) {
        StringBuilder text = new StringBuilder();
        if (viewable instanceof Viewable.Textual) {
            Part part = ((Viewable.Textual) viewable).getPart();
            addTextDivider(text, part, prependDivider);
            String t = MessageExtractor.getTextFromPart(part);
            if (t == null) {
                t = "";
            } else if (viewable instanceof Viewable.Html) {
                t = HtmlConverter.htmlToText(t);
            }
            text.append(t);
        } else if (viewable instanceof Viewable.Alternative) {
            Viewable.Alternative alternative = (Viewable.Alternative) viewable;
            boolean divider = prependDivider;
            for (Viewable textViewable : alternative.getText().isEmpty() ? alternative.getHtml() : alternative.getText()) {
                text.append((CharSequence) buildText(textViewable, divider));
                divider = true;
            }
        }
        return text;
    }

    private void addHtmlDivider(StringBuilder html, Part part, boolean prependDivider) {
        if (prependDivider) {
            String filename = getPartName(part);
            html.append("<p style=\"margin-top: 2.5em; margin-bottom: 1em; border-bottom: 1px solid #000\">");
            html.append(filename);
            html.append("</p>");
        }
    }

    private static String getPartName(Part part) {
        String name;
        String disposition = part.getDisposition();
        if (disposition == null || (name = MimeUtility.getHeaderParameter(disposition, ContentDispositionField.PARAM_FILENAME)) == null) {
            return "";
        }
        return name;
    }

    private void addTextDivider(StringBuilder text, Part part, boolean prependDivider) {
        if (prependDivider) {
            String filename = getPartName(part);
            text.append("\r\n\r\n");
            int len = filename.length();
            if (len > 0) {
                if (len > (TEXT_DIVIDER_LENGTH - FILENAME_PREFIX_LENGTH) - FILENAME_SUFFIX_LENGTH) {
                    filename = filename.substring(0, ((TEXT_DIVIDER_LENGTH - FILENAME_PREFIX_LENGTH) - FILENAME_SUFFIX_LENGTH) - 3) + "...";
                }
                text.append(FILENAME_PREFIX);
                text.append(filename);
                text.append(FILENAME_SUFFIX);
                text.append(TEXT_DIVIDER.substring(0, ((TEXT_DIVIDER_LENGTH - FILENAME_PREFIX_LENGTH) - filename.length()) - FILENAME_SUFFIX_LENGTH));
            } else {
                text.append(TEXT_DIVIDER);
            }
            text.append("\r\n\r\n");
        }
    }

    private void addMessageHeaderText(StringBuilder text, Message message) throws MessagingException {
        Address[] from = message.getFrom();
        if (from != null && from.length > 0) {
            text.append(this.context.getString(R.string.message_compose_quote_header_from));
            text.append(' ');
            text.append(Address.toString(from));
            text.append("\r\n");
        }
        Address[] to = message.getRecipients(Message.RecipientType.TO);
        if (to != null && to.length > 0) {
            text.append(this.context.getString(R.string.message_compose_quote_header_to));
            text.append(' ');
            text.append(Address.toString(to));
            text.append("\r\n");
        }
        Address[] cc = message.getRecipients(Message.RecipientType.CC);
        if (cc != null && cc.length > 0) {
            text.append(this.context.getString(R.string.message_compose_quote_header_cc));
            text.append(' ');
            text.append(Address.toString(cc));
            text.append("\r\n");
        }
        Date date = message.getSentDate();
        if (date != null) {
            text.append(this.context.getString(R.string.message_compose_quote_header_send_date));
            text.append(' ');
            text.append(date.toString());
            text.append("\r\n");
        }
        String subject = message.getSubject();
        text.append(this.context.getString(R.string.message_compose_quote_header_subject));
        text.append(' ');
        if (subject == null) {
            text.append(this.context.getString(R.string.general_no_subject));
        } else {
            text.append(subject);
        }
        text.append("\r\n\r\n");
    }

    private void addMessageHeaderHtml(StringBuilder html, Message message) throws MessagingException {
        html.append("<table style=\"border: 0\">");
        Address[] from = message.getFrom();
        if (from != null && from.length > 0) {
            addTableRow(html, this.context.getString(R.string.message_compose_quote_header_from), Address.toString(from));
        }
        Address[] to = message.getRecipients(Message.RecipientType.TO);
        if (to != null && to.length > 0) {
            addTableRow(html, this.context.getString(R.string.message_compose_quote_header_to), Address.toString(to));
        }
        Address[] cc = message.getRecipients(Message.RecipientType.CC);
        if (cc != null && cc.length > 0) {
            addTableRow(html, this.context.getString(R.string.message_compose_quote_header_cc), Address.toString(cc));
        }
        Date date = message.getSentDate();
        if (date != null) {
            addTableRow(html, this.context.getString(R.string.message_compose_quote_header_send_date), date.toString());
        }
        String subject = message.getSubject();
        String string = this.context.getString(R.string.message_compose_quote_header_subject);
        if (subject == null) {
            subject = this.context.getString(R.string.general_no_subject);
        }
        addTableRow(html, string, subject);
        html.append("</table>");
    }

    private static void addTableRow(StringBuilder html, String header, String value) {
        html.append("<tr><th style=\"text-align: left; vertical-align: top;\">");
        html.append(header);
        html.append("</th>");
        html.append("<td>");
        html.append(value);
        html.append("</td></tr>");
    }

    static class ViewableExtractedText {
        public final String html;
        public final String text;

        public ViewableExtractedText(String text2, String html2) {
            this.text = text2;
            this.html = html2;
        }
    }
}
