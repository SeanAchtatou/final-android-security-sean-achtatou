package com.fsck.k9.mailstore.migrations;

import android.content.Context;
import com.fsck.k9.Account;
import com.fsck.k9.mail.Flag;
import com.fsck.k9.mailstore.LocalStore;
import com.fsck.k9.preferences.Storage;
import java.util.List;

public interface MigrationsHelper {
    Account getAccount();

    Context getContext();

    LocalStore getLocalStore();

    Storage getStorage();

    String serializeFlags(List<Flag> list);
}
