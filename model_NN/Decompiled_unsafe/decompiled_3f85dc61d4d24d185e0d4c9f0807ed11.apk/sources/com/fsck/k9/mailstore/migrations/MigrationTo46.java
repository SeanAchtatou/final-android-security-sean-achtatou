package com.fsck.k9.mailstore.migrations;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.fsck.k9.mail.Flag;
import com.fsck.k9.provider.EmailProvider;
import java.util.ArrayList;
import java.util.List;

class MigrationTo46 {
    MigrationTo46() {
    }

    /* JADX INFO: finally extract failed */
    public static void addMessagesFlagColumns(SQLiteDatabase db, MigrationsHelper migrationsHelper) {
        db.execSQL("ALTER TABLE messages ADD read INTEGER default 0");
        db.execSQL("ALTER TABLE messages ADD flagged INTEGER default 0");
        db.execSQL("ALTER TABLE messages ADD answered INTEGER default 0");
        db.execSQL("ALTER TABLE messages ADD forwarded INTEGER default 0");
        String[] projection = {"id", EmailProvider.MessageColumns.FLAGS};
        ContentValues cv = new ContentValues();
        List<Flag> extraFlags = new ArrayList<>();
        Cursor cursor = db.query("messages", projection, null, null, null, null, null);
        while (cursor.moveToNext()) {
            try {
                long id = cursor.getLong(0);
                String flagList = cursor.getString(1);
                boolean read = false;
                boolean flagged = false;
                boolean answered = false;
                boolean forwarded = false;
                if (flagList != null && flagList.length() > 0) {
                    for (String flagStr : flagList.split(",")) {
                        try {
                            Flag flag = Flag.valueOf(flagStr);
                            switch (flag) {
                                case ANSWERED:
                                    answered = true;
                                    break;
                                case FLAGGED:
                                    flagged = true;
                                    break;
                                case FORWARDED:
                                    forwarded = true;
                                    break;
                                case SEEN:
                                    read = true;
                                    break;
                                case DRAFT:
                                case RECENT:
                                case X_DESTROYED:
                                case X_DOWNLOADED_FULL:
                                case X_DOWNLOADED_PARTIAL:
                                case X_REMOTE_COPY_STARTED:
                                case X_SEND_FAILED:
                                case X_SEND_IN_PROGRESS:
                                    extraFlags.add(flag);
                                    break;
                            }
                        } catch (Exception e) {
                        }
                    }
                }
                cv.put(EmailProvider.MessageColumns.FLAGS, migrationsHelper.serializeFlags(extraFlags));
                cv.put(EmailProvider.MessageColumns.READ, Boolean.valueOf(read));
                cv.put(EmailProvider.MessageColumns.FLAGGED, Boolean.valueOf(flagged));
                cv.put(EmailProvider.MessageColumns.ANSWERED, Boolean.valueOf(answered));
                cv.put(EmailProvider.MessageColumns.FORWARDED, Boolean.valueOf(forwarded));
                db.update("messages", cv, "id = ?", new String[]{Long.toString(id)});
                cv.clear();
                extraFlags.clear();
            } catch (Throwable th) {
                cursor.close();
                throw th;
            }
        }
        cursor.close();
        db.execSQL("CREATE INDEX IF NOT EXISTS msg_read ON messages (read)");
        db.execSQL("CREATE INDEX IF NOT EXISTS msg_flagged ON messages (flagged)");
    }
}
