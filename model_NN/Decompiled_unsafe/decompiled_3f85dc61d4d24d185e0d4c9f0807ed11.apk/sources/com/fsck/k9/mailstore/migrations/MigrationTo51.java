package com.fsck.k9.mailstore.migrations;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import com.fsck.k9.Account;
import com.fsck.k9.K9;
import com.fsck.k9.mail.Flag;
import com.fsck.k9.mail.internet.MimeHeader;
import com.fsck.k9.mail.internet.MimeUtility;
import com.fsck.k9.preferences.SettingsExporter;
import com.fsck.k9.provider.EmailProvider;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;
import org.apache.james.mime4j.codec.QuotedPrintableOutputStream;
import org.apache.james.mime4j.dom.field.ContentDispositionField;
import org.apache.james.mime4j.dom.field.ContentTypeField;
import org.apache.james.mime4j.util.MimeUtil;

class MigrationTo51 {
    private static final int DATA_LOCATION__IN_DATABASE = 1;
    private static final int DATA_LOCATION__MISSING = 0;
    private static final int DATA_LOCATION__ON_DISK = 2;
    private static final int MESSAGE_PART_TYPE__HIDDEN_ATTACHMENT = 6;
    private static final int MESSAGE_PART_TYPE__UNKNOWN = 0;

    MigrationTo51() {
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x00ff A[Catch:{ IOException -> 0x0140, all -> 0x014c }] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0158  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void db51MigrateMessageFormat(android.database.sqlite.SQLiteDatabase r36, com.fsck.k9.mailstore.migrations.MigrationsHelper r37) {
        /*
            renameOldMessagesTableAndCreateNew(r36)
            copyMessageMetadataToNewTable(r36)
            com.fsck.k9.Account r26 = r37.getAccount()
            android.app.Application r4 = com.fsck.k9.K9.app
            com.fsck.k9.mailstore.StorageManager r4 = com.fsck.k9.mailstore.StorageManager.getInstance(r4)
            java.lang.String r5 = r26.getUuid()
            java.lang.String r8 = r26.getLocalStorageProviderId()
            java.io.File r19 = r4.getAttachmentDirectory(r5, r8)
            r0 = r26
            r1 = r19
            java.io.File r18 = renameOldAttachmentDirAndCreateNew(r0, r1)
            java.lang.String r5 = "messages_old"
            r4 = 6
            java.lang.String[] r6 = new java.lang.String[r4]
            r4 = 0
            java.lang.String r8 = "id"
            r6[r4] = r8
            r4 = 1
            java.lang.String r8 = "flags"
            r6[r4] = r8
            r4 = 2
            java.lang.String r8 = "html_content"
            r6[r4] = r8
            r4 = 3
            java.lang.String r8 = "text_content"
            r6[r4] = r8
            r4 = 4
            java.lang.String r8 = "mime_type"
            r6[r4] = r8
            r4 = 5
            java.lang.String r8 = "attachment_count"
            r6[r4] = r8
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 0
            r4 = r36
            android.database.Cursor r35 = r4.query(r5, r6, r7, r8, r9, r10, r11)
            java.lang.String r4 = "k9"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x014c }
            r5.<init>()     // Catch:{ all -> 0x014c }
            java.lang.String r8 = "migrating "
            java.lang.StringBuilder r5 = r5.append(r8)     // Catch:{ all -> 0x014c }
            int r8 = r35.getCount()     // Catch:{ all -> 0x014c }
            java.lang.StringBuilder r5 = r5.append(r8)     // Catch:{ all -> 0x014c }
            java.lang.String r8 = " messages"
            java.lang.StringBuilder r5 = r5.append(r8)     // Catch:{ all -> 0x014c }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x014c }
            android.util.Log.d(r4, r5)     // Catch:{ all -> 0x014c }
            android.content.ContentValues r28 = new android.content.ContentValues     // Catch:{ all -> 0x014c }
            r28.<init>()     // Catch:{ all -> 0x014c }
        L_0x0079:
            boolean r4 = r35.moveToNext()     // Catch:{ all -> 0x014c }
            if (r4 == 0) goto L_0x016b
            r4 = 0
            r0 = r35
            long r6 = r0.getLong(r4)     // Catch:{ all -> 0x014c }
            r4 = 1
            r0 = r35
            java.lang.String r33 = r0.getString(r4)     // Catch:{ all -> 0x014c }
            r4 = 2
            r0 = r35
            java.lang.String r13 = r0.getString(r4)     // Catch:{ all -> 0x014c }
            r4 = 3
            r0 = r35
            java.lang.String r14 = r0.getString(r4)     // Catch:{ all -> 0x014c }
            r4 = 4
            r0 = r35
            java.lang.String r15 = r0.getString(r4)     // Catch:{ all -> 0x014c }
            r4 = 5
            r0 = r35
            int r27 = r0.getInt(r4)     // Catch:{ all -> 0x014c }
            r0 = r36
            r1 = r33
            r2 = r37
            updateFlagsForMessage(r0, r6, r1, r2)     // Catch:{ IOException -> 0x0140 }
            r0 = r36
            com.fsck.k9.mail.internet.MimeHeader r10 = loadHeaderFromHeadersTable(r0, r6)     // Catch:{ IOException -> 0x0140 }
            com.fsck.k9.mailstore.migrations.MigrationTo51$MimeStructureState r11 = com.fsck.k9.mailstore.migrations.MigrationTo51.MimeStructureState.getNewRootState()     // Catch:{ IOException -> 0x0140 }
            r34 = 0
            r4 = 2
            r0 = r27
            if (r0 != r4) goto L_0x0151
            java.lang.String r4 = "multipart/encrypted"
            boolean r4 = org.apache.james.mime4j.util.MimeUtil.isSameMimeType(r15, r4)     // Catch:{ IOException -> 0x0140 }
            if (r4 == 0) goto L_0x0151
            r30 = 1
        L_0x00cd:
            if (r30 == 0) goto L_0x00df
            r5 = r36
            r8 = r18
            r9 = r19
            com.fsck.k9.mailstore.migrations.MigrationTo51$MimeStructureState r32 = migratePgpMimeEncryptedContent(r5, r6, r8, r9, r10, r11)     // Catch:{ IOException -> 0x0140 }
            if (r32 == 0) goto L_0x00df
            r11 = r32
            r34 = 1
        L_0x00df:
            if (r34 != 0) goto L_0x0109
            if (r27 != 0) goto L_0x0155
            r4 = 3
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ IOException -> 0x0140 }
            r5 = 0
            java.lang.String r8 = "text/plain"
            r4[r5] = r8     // Catch:{ IOException -> 0x0140 }
            r5 = 1
            java.lang.String r8 = "text/html"
            r4[r5] = r8     // Catch:{ IOException -> 0x0140 }
            r5 = 2
            java.lang.String r8 = "multipart/alternative"
            r4[r5] = r8     // Catch:{ IOException -> 0x0140 }
            boolean r4 = com.fsck.k9.helper.Utility.isAnyMimeType(r15, r4)     // Catch:{ IOException -> 0x0140 }
            if (r4 == 0) goto L_0x0155
            r31 = 1
        L_0x00fd:
            if (r31 == 0) goto L_0x0158
            r12 = r36
            r16 = r10
            r17 = r11
            com.fsck.k9.mailstore.migrations.MigrationTo51$MimeStructureState r11 = migrateSimpleMailContent(r12, r13, r14, r15, r16, r17)     // Catch:{ IOException -> 0x0140 }
        L_0x0109:
            r28.clear()     // Catch:{ IOException -> 0x0140 }
            java.lang.String r4 = "mime_type"
            r0 = r28
            r0.put(r4, r15)     // Catch:{ IOException -> 0x0140 }
            java.lang.String r4 = "message_part_id"
            java.lang.Long r5 = r11.rootPartId     // Catch:{ IOException -> 0x0140 }
            r0 = r28
            r0.put(r4, r5)     // Catch:{ IOException -> 0x0140 }
            java.lang.String r4 = "attachment_count"
            java.lang.Integer r5 = java.lang.Integer.valueOf(r27)     // Catch:{ IOException -> 0x0140 }
            r0 = r28
            r0.put(r4, r5)     // Catch:{ IOException -> 0x0140 }
            java.lang.String r4 = "messages"
            java.lang.String r5 = "id = ?"
            r8 = 1
            java.lang.String[] r8 = new java.lang.String[r8]     // Catch:{ IOException -> 0x0140 }
            r9 = 0
            java.lang.String r12 = java.lang.Long.toString(r6)     // Catch:{ IOException -> 0x0140 }
            r8[r9] = r12     // Catch:{ IOException -> 0x0140 }
            r0 = r36
            r1 = r28
            r0.update(r4, r1, r5, r8)     // Catch:{ IOException -> 0x0140 }
            goto L_0x0079
        L_0x0140:
            r29 = move-exception
            java.lang.String r4 = "k9"
            java.lang.String r5 = "error inserting into database"
            r0 = r29
            android.util.Log.e(r4, r5, r0)     // Catch:{ all -> 0x014c }
            goto L_0x0079
        L_0x014c:
            r4 = move-exception
            r35.close()
            throw r4
        L_0x0151:
            r30 = 0
            goto L_0x00cd
        L_0x0155:
            r31 = 0
            goto L_0x00fd
        L_0x0158:
            java.lang.String r15 = "multipart/mixed"
            r17 = r36
            r20 = r6
            r22 = r13
            r23 = r14
            r24 = r10
            r25 = r11
            com.fsck.k9.mailstore.migrations.MigrationTo51$MimeStructureState r11 = migrateComplexMailContent(r17, r18, r19, r20, r22, r23, r24, r25)     // Catch:{ IOException -> 0x0140 }
            goto L_0x0109
        L_0x016b:
            r35.close()
            cleanUpOldAttachmentDirectory(r18)
            dropOldMessagesTable(r36)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fsck.k9.mailstore.migrations.MigrationTo51.db51MigrateMessageFormat(android.database.sqlite.SQLiteDatabase, com.fsck.k9.mailstore.migrations.MigrationsHelper):void");
    }

    @NonNull
    private static File renameOldAttachmentDirAndCreateNew(Account account, File attachmentDirNew) {
        File attachmentDirOld = new File(attachmentDirNew.getParent(), account.getUuid() + ".old_attach-" + System.currentTimeMillis());
        if (!attachmentDirNew.renameTo(attachmentDirOld)) {
            Log.e("k9", "Error moving attachment dir! All attachments might be lost!");
        }
        if (!attachmentDirNew.mkdir()) {
            Log.e("k9", "Error creating new attachment dir!");
        }
        return attachmentDirOld;
    }

    private static void dropOldMessagesTable(SQLiteDatabase db) {
        Log.d("k9", "Migration succeeded, dropping old tables.");
        db.execSQL("DROP TABLE messages_old");
        db.execSQL("DROP TABLE attachments");
        db.execSQL("DROP TABLE headers");
    }

    private static void cleanUpOldAttachmentDirectory(File attachmentDirOld) {
        for (File file : attachmentDirOld.listFiles()) {
            Log.d("k9", "deleting stale attachment file: " + file.getName());
            if (file.exists() && !file.delete()) {
                Log.d("k9", "Failed to delete stale attachement file: " + file.getAbsolutePath());
            }
        }
        Log.d("k9", "deleting old attachment directory");
        if (attachmentDirOld.exists() && !attachmentDirOld.delete()) {
            Log.d("k9", "Failed to delete old attachement directory: " + attachmentDirOld.getAbsolutePath());
        }
    }

    private static void copyMessageMetadataToNewTable(SQLiteDatabase db) {
        db.execSQL("INSERT INTO messages (id, deleted, folder_id, uid, subject, date, sender_list, to_list, cc_list, bcc_list, reply_to_list, attachment_count, internal_date, message_id, preview, mime_type, normalized_subject_hash, empty, read, flagged, answered) SELECT id, deleted, folder_id, uid, subject, date, sender_list, to_list, cc_list, bcc_list, reply_to_list, attachment_count, internal_date, message_id, preview, mime_type, normalized_subject_hash, empty, read, flagged, answered FROM messages_old");
    }

    private static void renameOldMessagesTableAndCreateNew(SQLiteDatabase db) {
        db.execSQL("ALTER TABLE messages RENAME TO messages_old");
        db.execSQL("CREATE TABLE messages (id INTEGER PRIMARY KEY, deleted INTEGER default 0, folder_id INTEGER, uid TEXT, subject TEXT, date INTEGER, flags TEXT, sender_list TEXT, to_list TEXT, cc_list TEXT, bcc_list TEXT, reply_to_list TEXT, attachment_count INTEGER, internal_date INTEGER, message_id TEXT, preview TEXT, mime_type TEXT, normalized_subject_hash INTEGER, empty INTEGER default 0, read INTEGER default 0, flagged INTEGER default 0, answered INTEGER default 0, forwarded INTEGER default 0, message_part_id INTEGER)");
        db.execSQL("CREATE TABLE message_parts (id INTEGER PRIMARY KEY, type INTEGER NOT NULL, root INTEGER, parent INTEGER NOT NULL, seq INTEGER NOT NULL, mime_type TEXT, decoded_body_size INTEGER, display_name TEXT, header TEXT, encoding TEXT, charset TEXT, data_location INTEGER NOT NULL, data BLOB, preamble TEXT, epilogue TEXT, boundary TEXT, content_id TEXT, server_extra TEXT)");
        db.execSQL("CREATE TRIGGER set_message_part_root AFTER INSERT ON message_parts BEGIN UPDATE message_parts SET root=id WHERE root IS NULL AND ROWID = NEW.ROWID; END");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    @Nullable
    private static MimeStructureState migratePgpMimeEncryptedContent(SQLiteDatabase db, long messageId, File attachmentDirOld, File attachmentDirNew, MimeHeader mimeHeader, MimeStructureState structureState) {
        Log.d("k9", "Attempting to migrate multipart/encrypted as pgp/mime");
        Cursor cursor = db.query("attachments", new String[]{"id", ContentDispositionField.PARAM_SIZE, "name", EmailProvider.InternalMessageColumns.MIME_TYPE, "store_data", "content_uri", "content_id", "content_disposition"}, "message_id = ?", new String[]{Long.toString(messageId)}, null, null, "(mime_type LIKE 'application/pgp-encrypted') DESC");
        try {
            if (cursor.getCount() != 2) {
                Log.e("k9", "Found multipart/encrypted but bad number of attachments, handling as regular mail");
                return null;
            }
            cursor.moveToFirst();
            long firstPartId = cursor.getLong(0);
            int firstPartSize = cursor.getInt(1);
            String firstPartName = cursor.getString(2);
            String firstPartMimeType = cursor.getString(3);
            String firstPartStoreData = cursor.getString(4);
            String firstPartContentUriString = cursor.getString(5);
            if (!MimeUtil.isSameMimeType(firstPartMimeType, "application/pgp-encrypted")) {
                Log.e("k9", "First part in multipart/encrypted wasn't application/pgp-encrypted, not handling as pgp/mime");
                cursor.close();
                return null;
            }
            cursor.moveToNext();
            long secondPartId = cursor.getLong(0);
            int secondPartSize = cursor.getInt(1);
            String secondPartName = cursor.getString(2);
            String secondPartMimeType = cursor.getString(3);
            String secondPartStoreData = cursor.getString(4);
            String secondPartContentUriString = cursor.getString(5);
            if (!MimeUtil.isSameMimeType(secondPartMimeType, MimeUtility.DEFAULT_ATTACHMENT_MIME_TYPE)) {
                Log.e("k9", "First part in multipart/encrypted wasn't application/octet-stream, not handling as pgp/mime");
                cursor.close();
                return null;
            }
            String boundary = MimeUtility.getHeaderParameter(mimeHeader.getFirstHeader("Content-Type"), ContentTypeField.PARAM_BOUNDARY);
            if (TextUtils.isEmpty(boundary)) {
                boundary = MimeUtil.createUniqueBoundary();
            }
            MimeHeader mimeHeader2 = mimeHeader;
            mimeHeader2.setHeader("Content-Type", String.format("multipart/encrypted; boundary=\"%s\"; protocol=\"application/pgp-encrypted\"", boundary));
            ContentValues cv = new ContentValues();
            cv.put(SettingsExporter.TYPE_ATTRIBUTE, (Integer) 0);
            cv.put("data_location", (Integer) 1);
            cv.put(EmailProvider.InternalMessageColumns.MIME_TYPE, "multipart/encrypted");
            cv.put("header", mimeHeader.toString());
            cv.put(ContentTypeField.PARAM_BOUNDARY, boundary);
            structureState.applyValues(cv);
            MimeStructureState structureState2 = insertMimeAttachmentPart(db, attachmentDirOld, attachmentDirNew, insertMimeAttachmentPart(db, attachmentDirOld, attachmentDirNew, structureState.nextMultipartChild(db.insertOrThrow("message_parts", null, cv)), firstPartId, firstPartSize, firstPartName, "application/pgp-encrypted", firstPartStoreData, firstPartContentUriString, null, null), secondPartId, secondPartSize, secondPartName, MimeUtility.DEFAULT_ATTACHMENT_MIME_TYPE, secondPartStoreData, secondPartContentUriString, null, null);
            cursor.close();
            return structureState2;
        } finally {
            cursor.close();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private static MimeStructureState migrateComplexMailContent(SQLiteDatabase db, File attachmentDirOld, File attachmentDirNew, long messageId, String htmlContent, String textContent, MimeHeader mimeHeader, MimeStructureState structureState) throws IOException {
        Log.d("k9", "Processing mail with complex data structure as multipart/mixed");
        String boundary = MimeUtility.getHeaderParameter(mimeHeader.getFirstHeader("Content-Type"), ContentTypeField.PARAM_BOUNDARY);
        if (TextUtils.isEmpty(boundary)) {
            boundary = MimeUtil.createUniqueBoundary();
        }
        MimeHeader mimeHeader2 = mimeHeader;
        mimeHeader2.setHeader("Content-Type", String.format("multipart/mixed; boundary=\"%s\";", boundary));
        ContentValues cv = new ContentValues();
        cv.put(SettingsExporter.TYPE_ATTRIBUTE, (Integer) 0);
        cv.put("data_location", (Integer) 1);
        cv.put(EmailProvider.InternalMessageColumns.MIME_TYPE, "multipart/mixed");
        cv.put("header", mimeHeader.toString());
        cv.put(ContentTypeField.PARAM_BOUNDARY, boundary);
        structureState.applyValues(cv);
        MimeStructureState structureState2 = structureState.nextMultipartChild(db.insertOrThrow("message_parts", null, cv));
        if (htmlContent != null) {
            htmlContent = replaceContentUriWithContentIdInHtmlPart(db, messageId, htmlContent);
        }
        if (textContent != null && htmlContent != null) {
            structureState2 = insertBodyAsMultipartAlternative(db, structureState2, null, textContent, htmlContent).popParent();
        } else if (textContent != null) {
            structureState2 = insertTextualPartIntoDatabase(db, structureState2, null, textContent, false);
        } else if (htmlContent != null) {
            structureState2 = insertTextualPartIntoDatabase(db, structureState2, null, htmlContent, true);
        }
        return insertAttachments(db, attachmentDirOld, attachmentDirNew, messageId, structureState2);
    }

    private static String replaceContentUriWithContentIdInHtmlPart(SQLiteDatabase db, long messageId, String htmlContent) {
        Cursor cursor = db.query("attachments", new String[]{"content_uri", "content_id"}, "content_id IS NOT NULL AND message_id = ?", new String[]{Long.toString(messageId)}, null, null, null);
        while (cursor.moveToNext()) {
            try {
                htmlContent = htmlContent.replaceAll(Pattern.quote(cursor.getString(0)), "cid:" + cursor.getString(1));
            } finally {
                cursor.close();
            }
        }
        return htmlContent;
    }

    private static MimeStructureState migrateSimpleMailContent(SQLiteDatabase db, String htmlContent, String textContent, String mimeType, MimeHeader mimeHeader, MimeStructureState structureState) throws IOException {
        Log.d("k9", "Processing mail with simple structure");
        if (MimeUtil.isSameMimeType(mimeType, ContentTypeField.TYPE_TEXT_PLAIN)) {
            return insertTextualPartIntoDatabase(db, structureState, mimeHeader, textContent, false);
        }
        if (MimeUtil.isSameMimeType(mimeType, "text/html")) {
            return insertTextualPartIntoDatabase(db, structureState, mimeHeader, htmlContent, true);
        }
        if (MimeUtil.isSameMimeType(mimeType, "multipart/alternative")) {
            return insertBodyAsMultipartAlternative(db, structureState, mimeHeader, textContent, htmlContent);
        }
        throw new IllegalStateException("migrateSimpleMailContent cannot handle mimeType " + mimeType);
    }

    private static MimeStructureState insertAttachments(SQLiteDatabase db, File attachmentDirOld, File attachmentDirNew, long messageId, MimeStructureState structureState) {
        Cursor cursor = db.query("attachments", new String[]{"id", ContentDispositionField.PARAM_SIZE, "name", EmailProvider.InternalMessageColumns.MIME_TYPE, "store_data", "content_uri", "content_id", "content_disposition"}, "message_id = ?", new String[]{Long.toString(messageId)}, null, null, null);
        while (cursor.moveToNext()) {
            try {
                structureState = insertMimeAttachmentPart(db, attachmentDirOld, attachmentDirNew, structureState, cursor.getLong(0), cursor.getInt(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(7));
            } finally {
                cursor.close();
            }
        }
        return structureState;
    }

    private static MimeStructureState insertMimeAttachmentPart(SQLiteDatabase db, File attachmentDirOld, File attachmentDirNew, MimeStructureState structureState, long id, int size, String name, String mimeType, String storeData, String contentUriString, String contentId, String contentDisposition) {
        File attachmentFileToMove;
        if (K9.DEBUG) {
            Log.d("k9", "processing attachment " + id + ", " + name + ", " + mimeType + ", " + storeData + ", " + contentUriString);
        }
        if (contentDisposition == null) {
            contentDisposition = ContentDispositionField.DISPOSITION_TYPE_ATTACHMENT;
        }
        MimeHeader mimeHeader = new MimeHeader();
        mimeHeader.setHeader("Content-Type", String.format("%s;\r\n name=\"%s\"", mimeType, name));
        mimeHeader.setHeader("Content-Disposition", String.format(Locale.US, "%s;\r\n filename=\"%s\";\r\n size=%d", contentDisposition, name, Integer.valueOf(size)));
        if (contentId != null) {
            mimeHeader.setHeader("Content-ID", contentId);
        }
        if (contentUriString != null) {
            try {
                String attachmentId = Uri.parse(contentUriString).getPathSegments().get(1);
                boolean isMatchingAttachmentId = Long.parseLong(attachmentId) == id;
                File attachmentFile = new File(attachmentDirOld, attachmentId);
                boolean isExistingAttachmentFile = attachmentFile.exists();
                if (!isMatchingAttachmentId) {
                    Log.e("k9", "mismatched attachment id. mark as missing");
                    attachmentFileToMove = null;
                } else if (!isExistingAttachmentFile) {
                    Log.e("k9", "attached file doesn't exist. mark as missing");
                    attachmentFileToMove = null;
                } else {
                    attachmentFileToMove = attachmentFile;
                }
            } catch (Exception e) {
                attachmentFileToMove = null;
            }
        } else {
            attachmentFileToMove = null;
        }
        if (K9.DEBUG && attachmentFileToMove == null) {
            Log.d("k9", "matching attachment is in local cache");
        }
        int messageType = !TextUtils.isEmpty(contentId) && ContentDispositionField.DISPOSITION_TYPE_INLINE.equalsIgnoreCase(contentDisposition) ? 6 : 0;
        ContentValues cv = new ContentValues();
        cv.put(SettingsExporter.TYPE_ATTRIBUTE, Integer.valueOf(messageType));
        cv.put(EmailProvider.InternalMessageColumns.MIME_TYPE, mimeType);
        cv.put("decoded_body_size", Integer.valueOf(size));
        cv.put("display_name", name);
        cv.put("header", mimeHeader.toString());
        cv.put("encoding", MimeUtil.ENC_BINARY);
        cv.put("data_location", Integer.valueOf(attachmentFileToMove != null ? 2 : 0));
        cv.put("content_id", contentId);
        cv.put("server_extra", storeData);
        structureState.applyValues(cv);
        long partId = db.insertOrThrow("message_parts", null, cv);
        MimeStructureState structureState2 = structureState.nextChild(partId);
        if (attachmentFileToMove != null && !attachmentFileToMove.renameTo(new File(attachmentDirNew, Long.toString(partId)))) {
            Log.e("k9", "Moving attachment to new dir failed!");
        }
        return structureState2;
    }

    private static void updateFlagsForMessage(SQLiteDatabase db, long messageId, String messageFlags, MigrationsHelper migrationsHelper) {
        List<Flag> extraFlags = new ArrayList<>();
        if (messageFlags != null && messageFlags.length() > 0) {
            for (String flagStr : messageFlags.split(",")) {
                try {
                    extraFlags.add(Flag.valueOf(flagStr));
                } catch (Exception e) {
                }
            }
        }
        extraFlags.add(Flag.X_MIGRATED_FROM_V50);
        db.execSQL("UPDATE messages SET flags = ? WHERE id = ?", new Object[]{migrationsHelper.serializeFlags(extraFlags), Long.valueOf(messageId)});
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private static MimeStructureState insertBodyAsMultipartAlternative(SQLiteDatabase db, MimeStructureState structureState, MimeHeader mimeHeader, String textContent, String htmlContent) throws IOException {
        int dataLocation;
        if (mimeHeader == null) {
            mimeHeader = new MimeHeader();
        }
        String boundary = MimeUtility.getHeaderParameter(mimeHeader.getFirstHeader("Content-Type"), ContentTypeField.PARAM_BOUNDARY);
        if (TextUtils.isEmpty(boundary)) {
            boundary = MimeUtil.createUniqueBoundary();
        }
        mimeHeader.setHeader("Content-Type", String.format("multipart/alternative; boundary=\"%s\";", boundary));
        if (textContent == null && htmlContent == null) {
            dataLocation = 0;
        } else {
            dataLocation = 1;
        }
        ContentValues cv = new ContentValues();
        cv.put(SettingsExporter.TYPE_ATTRIBUTE, (Integer) 0);
        cv.put("data_location", Integer.valueOf(dataLocation));
        cv.put(EmailProvider.InternalMessageColumns.MIME_TYPE, "multipart/alternative");
        cv.put("header", mimeHeader.toString());
        cv.put(ContentTypeField.PARAM_BOUNDARY, boundary);
        structureState.applyValues(cv);
        MimeStructureState structureState2 = structureState.nextMultipartChild(db.insertOrThrow("message_parts", null, cv));
        if (textContent != null) {
            structureState2 = insertTextualPartIntoDatabase(db, structureState2, null, textContent, false);
        }
        if (htmlContent != null) {
            return insertTextualPartIntoDatabase(db, structureState2, null, htmlContent, true);
        }
        return structureState2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private static MimeStructureState insertTextualPartIntoDatabase(SQLiteDatabase db, MimeStructureState structureState, MimeHeader mimeHeader, String content, boolean isHtml) throws IOException {
        int dataLocation;
        byte[] contentBytes;
        int decodedBodySize;
        if (mimeHeader == null) {
            mimeHeader = new MimeHeader();
        }
        mimeHeader.setHeader("Content-Type", isHtml ? "text/html; charset=\"utf-8\"" : "text/plain; charset=\"utf-8\"");
        mimeHeader.setHeader("Content-Transfer-Encoding", MimeUtil.ENC_QUOTED_PRINTABLE);
        if (content != null) {
            ByteArrayOutputStream contentOutputStream = new ByteArrayOutputStream();
            QuotedPrintableOutputStream quotedPrintableOutputStream = new QuotedPrintableOutputStream(contentOutputStream, false);
            quotedPrintableOutputStream.write(content.getBytes());
            quotedPrintableOutputStream.flush();
            dataLocation = 1;
            contentBytes = contentOutputStream.toByteArray();
            decodedBodySize = content.length();
        } else {
            dataLocation = 0;
            contentBytes = null;
            decodedBodySize = 0;
        }
        ContentValues cv = new ContentValues();
        cv.put(SettingsExporter.TYPE_ATTRIBUTE, (Integer) 0);
        cv.put("data_location", Integer.valueOf(dataLocation));
        cv.put(EmailProvider.InternalMessageColumns.MIME_TYPE, isHtml ? "text/html" : ContentTypeField.TYPE_TEXT_PLAIN);
        cv.put("header", mimeHeader.toString());
        cv.put("data", contentBytes);
        cv.put("decoded_body_size", Integer.valueOf(decodedBodySize));
        cv.put("encoding", MimeUtil.ENC_QUOTED_PRINTABLE);
        cv.put("charset", "utf-8");
        structureState.applyValues(cv);
        return structureState.nextChild(db.insertOrThrow("message_parts", null, cv));
    }

    private static MimeHeader loadHeaderFromHeadersTable(SQLiteDatabase db, long messageId) {
        Cursor headersCursor = db.query("headers", new String[]{"name", SettingsExporter.VALUE_ELEMENT}, "message_id = ?", new String[]{Long.toString(messageId)}, null, null, null);
        try {
            MimeHeader mimeHeader = new MimeHeader();
            while (headersCursor.moveToNext()) {
                mimeHeader.addHeader(headersCursor.getString(0), headersCursor.getString(1));
            }
            return mimeHeader;
        } finally {
            headersCursor.close();
        }
    }

    static class MimeStructureState {
        private boolean isStateAdvanced;
        private boolean isValuesApplied;
        private final int nextOrder;
        private final long parentId;
        private final Long prevParentId;
        /* access modifiers changed from: private */
        public final Long rootPartId;

        private MimeStructureState(Long rootPartId2, Long prevParentId2, long parentId2, int nextOrder2) {
            this.rootPartId = rootPartId2;
            this.prevParentId = prevParentId2;
            this.parentId = parentId2;
            this.nextOrder = nextOrder2;
        }

        public static MimeStructureState getNewRootState() {
            return new MimeStructureState(null, null, -1, 0);
        }

        public MimeStructureState nextChild(long newPartId) {
            if (!this.isValuesApplied || this.isStateAdvanced) {
                throw new IllegalStateException("next* methods must only be called once");
            }
            this.isStateAdvanced = true;
            if (this.rootPartId == null) {
                return new MimeStructureState(Long.valueOf(newPartId), null, -1, this.nextOrder + 1);
            }
            return new MimeStructureState(this.rootPartId, this.prevParentId, this.parentId, this.nextOrder + 1);
        }

        public MimeStructureState nextMultipartChild(long newPartId) {
            if (!this.isValuesApplied || this.isStateAdvanced) {
                throw new IllegalStateException("next* methods must only be called once");
            }
            this.isStateAdvanced = true;
            if (this.rootPartId == null) {
                return new MimeStructureState(Long.valueOf(newPartId), Long.valueOf(this.parentId), newPartId, this.nextOrder + 1);
            }
            return new MimeStructureState(this.rootPartId, Long.valueOf(this.parentId), newPartId, this.nextOrder + 1);
        }

        public void applyValues(ContentValues cv) {
            if (this.isValuesApplied || this.isStateAdvanced) {
                throw new IllegalStateException("applyValues must be called exactly once, after a call to next*");
            } else if (this.rootPartId == null || this.parentId != -1) {
                this.isValuesApplied = true;
                if (this.rootPartId != null) {
                    cv.put(EmailProvider.ThreadColumns.ROOT, this.rootPartId);
                }
                cv.put(EmailProvider.ThreadColumns.PARENT, Long.valueOf(this.parentId));
                cv.put("seq", Integer.valueOf(this.nextOrder));
            } else {
                throw new IllegalStateException("applyValues must not be called after a root nextChild call");
            }
        }

        public MimeStructureState popParent() {
            if (this.prevParentId != null) {
                return new MimeStructureState(this.rootPartId, null, this.prevParentId.longValue(), this.nextOrder);
            }
            throw new IllegalStateException("popParent must only be called if parent depth is >= 2");
        }
    }
}
