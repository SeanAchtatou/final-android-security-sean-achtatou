package com.fsck.k9.mailstore;

import com.fsck.k9.mail.MessagingException;

public class UnavailableStorageException extends MessagingException {
    private static final long serialVersionUID = 1348267375054620792L;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fsck.k9.mailstore.UnavailableStorageException.<init>(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.fsck.k9.mailstore.UnavailableStorageException.<init>(java.lang.String, java.lang.Throwable):void
      com.fsck.k9.mailstore.UnavailableStorageException.<init>(java.lang.String, boolean):void */
    public UnavailableStorageException(String message) {
        this(message, true);
    }

    public UnavailableStorageException(String message, boolean perm) {
        super(message, perm);
    }

    public UnavailableStorageException(String message, Throwable throwable) {
        this(message, true, throwable);
    }

    public UnavailableStorageException(String message, boolean perm, Throwable throwable) {
        super(message, perm, throwable);
    }
}
