package com.fsck.k9.mailstore;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.v4.media.session.PlaybackStateCompat;
import android.support.v4.os.EnvironmentCompat;
import android.text.TextUtils;
import android.util.Log;
import com.fsck.k9.Account;
import com.fsck.k9.K9;
import com.fsck.k9.Preferences;
import com.fsck.k9.activity.MessageReference;
import com.fsck.k9.activity.Search;
import com.fsck.k9.controller.MessagingController;
import com.fsck.k9.helper.Utility;
import com.fsck.k9.mail.Address;
import com.fsck.k9.mail.Body;
import com.fsck.k9.mail.BoundaryGenerator;
import com.fsck.k9.mail.FetchProfile;
import com.fsck.k9.mail.Flag;
import com.fsck.k9.mail.Folder;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.MessageRetrievalListener;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.Multipart;
import com.fsck.k9.mail.Part;
import com.fsck.k9.mail.filter.CountingOutputStream;
import com.fsck.k9.mail.internet.BinaryTempFileBody;
import com.fsck.k9.mail.internet.MimeMessage;
import com.fsck.k9.mail.internet.MimeUtility;
import com.fsck.k9.mail.internet.SizeAware;
import com.fsck.k9.mail.message.MessageHeaderParser;
import com.fsck.k9.mailstore.LockableDatabase;
import com.fsck.k9.message.extractors.AttachmentInfoExtractor;
import com.fsck.k9.message.extractors.PreviewResult;
import com.fsck.k9.preferences.SettingsExporter;
import com.fsck.k9.preferences.Storage;
import com.fsck.k9.preferences.StorageEditor;
import com.fsck.k9.provider.EmailProvider;
import com.fsck.k9.remotecontrol.K9RemoteControl;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.UUID;
import org.apache.commons.io.IOUtils;
import org.apache.james.mime4j.dom.field.ContentTypeField;
import org.apache.james.mime4j.util.MimeUtil;

public class LocalFolder extends Folder<LocalMessage> implements Serializable {
    static final long INVALID_MESSAGE_PART_ID = -1;
    private static final int MAX_BODY_SIZE_FOR_DATABASE = 16384;
    private static final AttachmentInfoExtractor attachmentInfoExtractor = AttachmentInfoExtractor.getInstance();
    private static final long serialVersionUID = -1973296520918624767L;
    /* access modifiers changed from: private */
    public final LocalStore localStore;
    /* access modifiers changed from: private */
    public Folder.FolderClass mDisplayClass = Folder.FolderClass.NO_CLASS;
    /* access modifiers changed from: private */
    public long mFolderId = -1;
    /* access modifiers changed from: private */
    public boolean mInTopGroup = false;
    /* access modifiers changed from: private */
    public boolean mIntegrate = false;
    private Integer mLastUid = null;
    /* access modifiers changed from: private */
    public String mName = null;
    /* access modifiers changed from: private */
    public Folder.FolderClass mNotifyClass = Folder.FolderClass.INHERITED;
    /* access modifiers changed from: private */
    public Folder.FolderClass mPushClass = Folder.FolderClass.SECOND_CLASS;
    private String mPushState = null;
    /* access modifiers changed from: private */
    public Folder.FolderClass mSyncClass = Folder.FolderClass.INHERITED;
    private int mVisibleLimit = -1;
    private MoreMessages moreMessages = MoreMessages.UNKNOWN;
    private String prefId = null;

    public LocalFolder(LocalStore localStore2, String name) {
        this.localStore = localStore2;
        this.mName = name;
        if (getAccount().getInboxFolderName().equals(getName())) {
            this.mSyncClass = Folder.FolderClass.FIRST_CLASS;
            this.mPushClass = Folder.FolderClass.FIRST_CLASS;
            this.mInTopGroup = true;
        }
    }

    public LocalFolder(LocalStore localStore2, long id) {
        this.localStore = localStore2;
        this.mFolderId = id;
    }

    public long getId() {
        return this.mFolderId;
    }

    public String getAccountUuid() {
        return getAccount().getUuid();
    }

    public boolean getSignatureUse() {
        return getAccount().getSignatureUse();
    }

    public void setLastSelectedFolderName(String destFolderName) {
        getAccount().setLastSelectedFolderName(destFolderName);
    }

    public boolean syncRemoteDeletions() {
        return getAccount().syncRemoteDeletions();
    }

    public void open(final int mode) throws MessagingException {
        if (!isOpen() || !(getMode() == mode || mode == 1)) {
            if (isOpen()) {
                close();
            }
            try {
                this.localStore.database.execute(false, new LockableDatabase.DbCallback<Void>() {
                    public Void doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException {
                        Cursor cursor;
                        try {
                            if (LocalFolder.this.mName != null) {
                                cursor = db.rawQuery("SELECT folders.id, name, visible_limit, last_updated, status, push_state, last_pushed, integrate, top_group, poll_class, push_class, display_class, notify_class, more_messages FROM folders " + "where folders.name = ?", new String[]{LocalFolder.this.mName});
                            } else {
                                cursor = db.rawQuery("SELECT folders.id, name, visible_limit, last_updated, status, push_state, last_pushed, integrate, top_group, poll_class, push_class, display_class, notify_class, more_messages FROM folders " + "where folders.id = ?", new String[]{Long.toString(LocalFolder.this.mFolderId)});
                            }
                            if (!cursor.moveToFirst() || cursor.isNull(0)) {
                                Log.w("k9", "Creating folder " + LocalFolder.this.getName() + " with existing id " + LocalFolder.this.getId());
                                LocalFolder.this.create(Folder.FolderType.HOLDS_MESSAGES);
                                LocalFolder.this.open(mode);
                            } else if (cursor.getInt(0) > 0) {
                                LocalFolder.this.open(cursor);
                            }
                            Utility.closeQuietly(cursor);
                            return null;
                        } catch (MessagingException e) {
                            throw new LockableDatabase.WrappedException(e);
                        } catch (Throwable th) {
                            Utility.closeQuietly(null);
                            throw th;
                        }
                    }
                });
            } catch (LockableDatabase.WrappedException e) {
                throw ((MessagingException) e.getCause());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void open(Cursor cursor) throws MessagingException {
        boolean z;
        boolean z2 = true;
        this.mFolderId = (long) cursor.getInt(0);
        this.mName = cursor.getString(1);
        this.mVisibleLimit = cursor.getInt(2);
        this.mPushState = cursor.getString(5);
        super.setStatus(cursor.getString(4));
        super.setLastChecked(cursor.getLong(3));
        super.setLastPush(cursor.getLong(6));
        if (cursor.getInt(8) == 1) {
            z = true;
        } else {
            z = false;
        }
        this.mInTopGroup = z;
        if (cursor.getInt(7) != 1) {
            z2 = false;
        }
        this.mIntegrate = z2;
        String noClass = Folder.FolderClass.NO_CLASS.toString();
        String displayClass = cursor.getString(11);
        if (displayClass == null) {
            displayClass = noClass;
        }
        this.mDisplayClass = Folder.FolderClass.valueOf(displayClass);
        String notifyClass = cursor.getString(12);
        if (notifyClass == null) {
            notifyClass = noClass;
        }
        this.mNotifyClass = Folder.FolderClass.valueOf(notifyClass);
        String pushClass = cursor.getString(10);
        if (pushClass == null) {
            pushClass = noClass;
        }
        this.mPushClass = Folder.FolderClass.valueOf(pushClass);
        String syncClass = cursor.getString(9);
        if (syncClass != null) {
            noClass = syncClass;
        }
        this.mSyncClass = Folder.FolderClass.valueOf(noClass);
        this.moreMessages = MoreMessages.fromDatabaseName(cursor.getString(13));
    }

    public boolean isOpen() {
        return (this.mFolderId == -1 || this.mName == null) ? false : true;
    }

    public int getMode() {
        return 0;
    }

    public String getName() {
        return this.mName;
    }

    public boolean exists() throws MessagingException {
        return ((Boolean) this.localStore.database.execute(false, new LockableDatabase.DbCallback<Boolean>() {
            public Boolean doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException {
                boolean z;
                boolean z2 = true;
                Cursor cursor = null;
                try {
                    cursor = db.rawQuery("SELECT id FROM folders where folders.name = ?", new String[]{LocalFolder.this.getName()});
                    if (cursor.moveToFirst()) {
                        if (cursor.getInt(0) <= 0) {
                            z2 = false;
                        }
                        z = Boolean.valueOf(z2);
                    } else {
                        z = false;
                        Utility.closeQuietly(cursor);
                    }
                    return z;
                } finally {
                    Utility.closeQuietly(cursor);
                }
            }
        })).booleanValue();
    }

    public boolean create(Folder.FolderType type) throws MessagingException {
        return create(type, getAccount().getDisplayCount());
    }

    public boolean create(Folder.FolderType type, int visibleLimit) throws MessagingException {
        if (exists()) {
            throw new MessagingException("Folder " + this.mName + " already exists.");
        }
        List<LocalFolder> foldersToCreate = new ArrayList<>(1);
        foldersToCreate.add(this);
        this.localStore.createFolders(foldersToCreate, visibleLimit);
        return true;
    }

    class PreferencesHolder {
        Folder.FolderClass displayClass = LocalFolder.this.mDisplayClass;
        boolean inTopGroup = LocalFolder.this.mInTopGroup;
        boolean integrate = LocalFolder.this.mIntegrate;
        Folder.FolderClass notifyClass = LocalFolder.this.mNotifyClass;
        Folder.FolderClass pushClass = LocalFolder.this.mPushClass;
        Folder.FolderClass syncClass = LocalFolder.this.mSyncClass;

        PreferencesHolder() {
        }
    }

    public void close() {
        this.mFolderId = -1;
    }

    public int getMessageCount() throws MessagingException {
        try {
            return ((Integer) this.localStore.database.execute(false, new LockableDatabase.DbCallback<Integer>() {
                public Integer doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException {
                    try {
                        LocalFolder.this.open(0);
                        Cursor cursor = null;
                        try {
                            cursor = db.rawQuery("SELECT COUNT(id) FROM messages WHERE empty = 0 AND deleted = 0 and folder_id = ?", new String[]{Long.toString(LocalFolder.this.mFolderId)});
                            cursor.moveToFirst();
                            return Integer.valueOf(cursor.getInt(0));
                        } finally {
                            Utility.closeQuietly(cursor);
                        }
                    } catch (MessagingException e) {
                        throw new LockableDatabase.WrappedException(e);
                    }
                }
            })).intValue();
        } catch (LockableDatabase.WrappedException e) {
            throw ((MessagingException) e.getCause());
        }
    }

    public int getUnreadMessageCount() throws MessagingException {
        if (this.mFolderId == -1) {
            open(0);
        }
        try {
            return ((Integer) this.localStore.database.execute(false, new LockableDatabase.DbCallback<Integer>() {
                /* JADX INFO: finally extract failed */
                public Integer doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException {
                    int unreadMessageCount = 0;
                    Cursor cursor = db.query("messages", new String[]{"COUNT(id)"}, "folder_id = ? AND empty = 0 AND deleted = 0 AND read=0", new String[]{Long.toString(LocalFolder.this.mFolderId)}, null, null, null);
                    try {
                        if (cursor.moveToFirst()) {
                            unreadMessageCount = cursor.getInt(0);
                        }
                        cursor.close();
                        return Integer.valueOf(unreadMessageCount);
                    } catch (Throwable th) {
                        cursor.close();
                        throw th;
                    }
                }
            })).intValue();
        } catch (LockableDatabase.WrappedException e) {
            throw ((MessagingException) e.getCause());
        }
    }

    public int getFlaggedMessageCount() throws MessagingException {
        if (this.mFolderId == -1) {
            open(0);
        }
        try {
            return ((Integer) this.localStore.database.execute(false, new LockableDatabase.DbCallback<Integer>() {
                /* JADX INFO: finally extract failed */
                public Integer doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException {
                    int flaggedMessageCount = 0;
                    Cursor cursor = db.query("messages", new String[]{"COUNT(id)"}, "folder_id = ? AND empty = 0 AND deleted = 0 AND flagged = 1", new String[]{Long.toString(LocalFolder.this.mFolderId)}, null, null, null);
                    try {
                        if (cursor.moveToFirst()) {
                            flaggedMessageCount = cursor.getInt(0);
                        }
                        cursor.close();
                        return Integer.valueOf(flaggedMessageCount);
                    } catch (Throwable th) {
                        cursor.close();
                        throw th;
                    }
                }
            })).intValue();
        } catch (LockableDatabase.WrappedException e) {
            throw ((MessagingException) e.getCause());
        }
    }

    public void setLastChecked(long lastChecked) throws MessagingException {
        try {
            open(0);
            super.setLastChecked(lastChecked);
            updateFolderColumn(EmailProvider.FolderColumns.LAST_UPDATED, Long.valueOf(lastChecked));
        } catch (MessagingException e) {
            throw new LockableDatabase.WrappedException(e);
        }
    }

    public void setLastPush(long lastChecked) throws MessagingException {
        try {
            open(0);
            super.setLastPush(lastChecked);
            updateFolderColumn(EmailProvider.FolderColumns.LAST_PUSHED, Long.valueOf(lastChecked));
        } catch (MessagingException e) {
            throw new LockableDatabase.WrappedException(e);
        }
    }

    public int getVisibleLimit() throws MessagingException {
        open(0);
        return this.mVisibleLimit;
    }

    public void purgeToVisibleLimit(MessageRemovalListener listener) throws MessagingException {
        if (!Search.isActive() && this.mVisibleLimit != 0) {
            open(0);
            List<? extends Message> messages = getMessages(null, false);
            for (int i = this.mVisibleLimit; i < messages.size(); i++) {
                if (listener != null) {
                    listener.messageRemoved(messages.get(i));
                }
                messages.get(i).destroy();
            }
        }
    }

    public void setVisibleLimit(int visibleLimit) throws MessagingException {
        updateMoreMessagesOnVisibleLimitChange(visibleLimit, this.mVisibleLimit);
        this.mVisibleLimit = visibleLimit;
        updateFolderColumn(EmailProvider.FolderColumns.VISIBLE_LIMIT, Integer.valueOf(this.mVisibleLimit));
    }

    private void updateMoreMessagesOnVisibleLimitChange(int newVisibleLimit, int oldVisibleLimit) throws MessagingException {
        boolean growVisibleLimit;
        boolean shrinkVisibleLimit;
        boolean moreMessagesWereAvailable;
        if (newVisibleLimit > oldVisibleLimit) {
            growVisibleLimit = true;
        } else {
            growVisibleLimit = false;
        }
        if (newVisibleLimit < oldVisibleLimit) {
            shrinkVisibleLimit = true;
        } else {
            shrinkVisibleLimit = false;
        }
        if (getMoreMessages() == MoreMessages.TRUE) {
            moreMessagesWereAvailable = true;
        } else {
            moreMessagesWereAvailable = false;
        }
        if (growVisibleLimit || (shrinkVisibleLimit && !moreMessagesWereAvailable)) {
            setMoreMessages(MoreMessages.UNKNOWN);
        }
    }

    public void setStatus(String status) throws MessagingException {
        updateFolderColumn("status", status);
    }

    public void setPushState(String pushState) throws MessagingException {
        this.mPushState = pushState;
        updateFolderColumn(EmailProvider.FolderColumns.PUSH_STATE, pushState);
    }

    private void updateFolderColumn(final String column, final Object value) throws MessagingException {
        try {
            this.localStore.database.execute(false, new LockableDatabase.DbCallback<Void>() {
                public Void doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException {
                    try {
                        LocalFolder.this.open(0);
                        db.execSQL("UPDATE folders SET " + column + " = ? WHERE id = ?", new Object[]{value, Long.valueOf(LocalFolder.this.mFolderId)});
                        return null;
                    } catch (MessagingException e) {
                        throw new LockableDatabase.WrappedException(e);
                    }
                }
            });
        } catch (LockableDatabase.WrappedException e) {
            throw ((MessagingException) e.getCause());
        }
    }

    public String getPushState() {
        return this.mPushState;
    }

    public Folder.FolderClass getDisplayClass() {
        return this.mDisplayClass;
    }

    public Folder.FolderClass getSyncClass() {
        return Folder.FolderClass.INHERITED == this.mSyncClass ? getDisplayClass() : this.mSyncClass;
    }

    public Folder.FolderClass getRawSyncClass() {
        return this.mSyncClass;
    }

    public Folder.FolderClass getNotifyClass() {
        return Folder.FolderClass.INHERITED == this.mNotifyClass ? getPushClass() : this.mNotifyClass;
    }

    public Folder.FolderClass getRawNotifyClass() {
        return this.mNotifyClass;
    }

    public Folder.FolderClass getPushClass() {
        return Folder.FolderClass.INHERITED == this.mPushClass ? getSyncClass() : this.mPushClass;
    }

    public Folder.FolderClass getRawPushClass() {
        return this.mPushClass;
    }

    public void setDisplayClass(Folder.FolderClass displayClass) throws MessagingException {
        this.mDisplayClass = displayClass;
        updateFolderColumn(EmailProvider.FolderColumns.DISPLAY_CLASS, this.mDisplayClass.name());
    }

    public void setSyncClass(Folder.FolderClass syncClass) throws MessagingException {
        this.mSyncClass = syncClass;
        updateFolderColumn(EmailProvider.FolderColumns.POLL_CLASS, this.mSyncClass.name());
    }

    public void setPushClass(Folder.FolderClass pushClass) throws MessagingException {
        this.mPushClass = pushClass;
        updateFolderColumn(EmailProvider.FolderColumns.PUSH_CLASS, this.mPushClass.name());
    }

    public void setNotifyClass(Folder.FolderClass notifyClass) throws MessagingException {
        this.mNotifyClass = notifyClass;
        updateFolderColumn("notify_class", this.mNotifyClass.name());
    }

    public boolean isIntegrate() {
        return this.mIntegrate;
    }

    public void setIntegrate(boolean integrate) throws MessagingException {
        this.mIntegrate = integrate;
        updateFolderColumn("integrate", Integer.valueOf(this.mIntegrate ? 1 : 0));
    }

    public boolean hasMoreMessages() {
        return this.moreMessages != MoreMessages.FALSE;
    }

    public MoreMessages getMoreMessages() {
        return this.moreMessages;
    }

    public void setMoreMessages(MoreMessages moreMessages2) throws MessagingException {
        this.moreMessages = moreMessages2;
        updateFolderColumn("more_messages", moreMessages2.getDatabaseName());
    }

    private String getPrefId(String name) {
        if (this.prefId == null) {
            this.prefId = this.localStore.uUid + "." + name;
        }
        return this.prefId;
    }

    private String getPrefId() throws MessagingException {
        open(0);
        return getPrefId(this.mName);
    }

    public void delete() throws MessagingException {
        String id = getPrefId();
        StorageEditor editor = this.localStore.getStorage().edit();
        editor.remove(id + ".displayMode");
        editor.remove(id + ".syncMode");
        editor.remove(id + ".pushMode");
        editor.remove(id + ".inTopGroup");
        editor.remove(id + ".integrate");
        editor.commit();
    }

    public void save() throws MessagingException {
        StorageEditor editor = this.localStore.getStorage().edit();
        save(editor);
        editor.commit();
    }

    public void save(StorageEditor editor) throws MessagingException {
        String id = getPrefId();
        if (this.mDisplayClass != Folder.FolderClass.NO_CLASS || getAccount().getInboxFolderName().equals(getName())) {
            editor.putString(id + ".displayMode", this.mDisplayClass.name());
        } else {
            editor.remove(id + ".displayMode");
        }
        if (this.mSyncClass != Folder.FolderClass.INHERITED || getAccount().getInboxFolderName().equals(getName())) {
            editor.putString(id + ".syncMode", this.mSyncClass.name());
        } else {
            editor.remove(id + ".syncMode");
        }
        if (this.mNotifyClass != Folder.FolderClass.INHERITED || getAccount().getInboxFolderName().equals(getName())) {
            editor.putString(id + ".notifyMode", this.mNotifyClass.name());
        } else {
            editor.remove(id + ".notifyMode");
        }
        if (this.mPushClass != Folder.FolderClass.SECOND_CLASS || getAccount().getInboxFolderName().equals(getName())) {
            editor.putString(id + ".pushMode", this.mPushClass.name());
        } else {
            editor.remove(id + ".pushMode");
        }
        editor.putBoolean(id + ".inTopGroup", this.mInTopGroup);
        editor.putBoolean(id + ".integrate", this.mIntegrate);
    }

    public void refresh(String name, PreferencesHolder prefHolder) {
        String id = getPrefId(name);
        Storage storage = this.localStore.getStorage();
        try {
            prefHolder.displayClass = Folder.FolderClass.valueOf(storage.getString(id + ".displayMode", prefHolder.displayClass.name()));
        } catch (Exception e) {
            Log.e("k9", "Unable to load displayMode for " + getName(), e);
        }
        if (prefHolder.displayClass == Folder.FolderClass.NONE) {
            prefHolder.displayClass = Folder.FolderClass.NO_CLASS;
        }
        try {
            prefHolder.syncClass = Folder.FolderClass.valueOf(storage.getString(id + ".syncMode", prefHolder.syncClass.name()));
        } catch (Exception e2) {
            Log.e("k9", "Unable to load syncMode for " + getName(), e2);
        }
        if (prefHolder.syncClass == Folder.FolderClass.NONE) {
            prefHolder.syncClass = Folder.FolderClass.INHERITED;
        }
        try {
            prefHolder.notifyClass = Folder.FolderClass.valueOf(storage.getString(id + ".notifyMode", prefHolder.notifyClass.name()));
        } catch (Exception e3) {
            Log.e("k9", "Unable to load notifyMode for " + getName(), e3);
        }
        if (prefHolder.notifyClass == Folder.FolderClass.NONE) {
            prefHolder.notifyClass = Folder.FolderClass.INHERITED;
        }
        try {
            prefHolder.pushClass = Folder.FolderClass.valueOf(storage.getString(id + ".pushMode", prefHolder.pushClass.name()));
        } catch (Exception e4) {
            Log.e("k9", "Unable to load pushMode for " + getName(), e4);
        }
        if (prefHolder.pushClass == Folder.FolderClass.NONE) {
            prefHolder.pushClass = Folder.FolderClass.INHERITED;
        }
        prefHolder.inTopGroup = storage.getBoolean(id + ".inTopGroup", prefHolder.inTopGroup);
        prefHolder.integrate = storage.getBoolean(id + ".integrate", prefHolder.integrate);
    }

    public void fetch(final List<LocalMessage> messages, final FetchProfile fp, MessageRetrievalListener<LocalMessage> messageRetrievalListener) throws MessagingException {
        try {
            this.localStore.database.execute(false, new LockableDatabase.DbCallback<Void>() {
                public Void doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException {
                    try {
                        LocalFolder.this.open(0);
                        if (!fp.contains(FetchProfile.Item.BODY)) {
                            return null;
                        }
                        for (Message message : messages) {
                            LocalFolder.this.loadMessageParts(db, (LocalMessage) message);
                        }
                        return null;
                    } catch (MessagingException e) {
                        throw new LockableDatabase.WrappedException(e);
                    }
                }
            });
        } catch (LockableDatabase.WrappedException e) {
            throw ((MessagingException) e.getCause());
        }
    }

    /* access modifiers changed from: private */
    public void loadMessageParts(SQLiteDatabase db, LocalMessage message) throws MessagingException {
        Map<Long, Part> partById = new HashMap<>();
        Cursor cursor = db.query("message_parts", new String[]{"id", SettingsExporter.TYPE_ATTRIBUTE, EmailProvider.ThreadColumns.PARENT, EmailProvider.InternalMessageColumns.MIME_TYPE, "decoded_body_size", "display_name", "header", "encoding", "charset", "data_location", "data", "preamble", "epilogue", ContentTypeField.PARAM_BOUNDARY, "content_id", "server_extra"}, "root = ?", new String[]{String.valueOf(message.getMessagePartId())}, null, null, "seq");
        while (cursor.moveToNext()) {
            try {
                loadMessagePart(message, partById, cursor);
            } finally {
                cursor.close();
            }
        }
    }

    /* JADX WARN: Type inference failed for: r5v0, types: [com.fsck.k9.mail.BodyPart, com.fsck.k9.mailstore.LocalBodyPart] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void loadMessagePart(com.fsck.k9.mailstore.LocalMessage r30, java.util.Map<java.lang.Long, com.fsck.k9.mail.Part> r31, android.database.Cursor r32) throws com.fsck.k9.mail.MessagingException {
        /*
            r29 = this;
            r6 = 0
            r0 = r32
            long r8 = r0.getLong(r6)
            r6 = 2
            r0 = r32
            long r22 = r0.getLong(r6)
            r6 = 3
            r0 = r32
            java.lang.String r20 = r0.getString(r6)
            r6 = 4
            r0 = r32
            long r10 = r0.getLong(r6)
            r6 = 6
            r0 = r32
            byte[] r18 = r0.getBlob(r6)
            r6 = 9
            r0 = r32
            int r14 = r0.getInt(r6)
            r6 = 15
            r0 = r32
            java.lang.String r28 = r0.getString(r6)
            long r6 = r30.getMessagePartId()
            int r6 = (r8 > r6 ? 1 : (r8 == r6 ? 0 : -1))
            if (r6 != 0) goto L_0x008c
            r26 = r30
        L_0x003d:
            java.lang.Long r6 = java.lang.Long.valueOf(r8)
            r0 = r31
            r1 = r26
            r0.put(r6, r1)
            r0 = r26
            r1 = r28
            r0.setServerExtra(r1)
            boolean r6 = com.fsck.k9.mail.internet.MimeUtility.isMultipart(r20)
            if (r6 == 0) goto L_0x00ea
            r6 = 11
            r0 = r32
            byte[] r27 = r0.getBlob(r6)
            r6 = 12
            r0 = r32
            byte[] r16 = r0.getBlob(r6)
            r6 = 13
            r0 = r32
            java.lang.String r12 = r0.getString(r6)
            com.fsck.k9.mail.internet.MimeMultipart r21 = new com.fsck.k9.mail.internet.MimeMultipart
            r0 = r21
            r1 = r20
            r0.<init>(r1, r12)
            r0 = r26
            r1 = r21
            r0.setBody(r1)
            r0 = r21
            r1 = r27
            r0.setPreamble(r1)
            r0 = r21
            r1 = r16
            r0.setEpilogue(r1)
        L_0x008b:
            return
        L_0x008c:
            java.lang.Long r6 = java.lang.Long.valueOf(r22)
            r0 = r31
            java.lang.Object r25 = r0.get(r6)
            com.fsck.k9.mail.Part r25 = (com.fsck.k9.mail.Part) r25
            if (r25 != 0) goto L_0x00a2
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r7 = "Parent part not found"
            r6.<init>(r7)
            throw r6
        L_0x00a2:
            java.lang.String r24 = r25.getMimeType()
            boolean r6 = com.fsck.k9.mail.internet.MimeUtility.isMultipart(r24)
            if (r6 == 0) goto L_0x00cd
            com.fsck.k9.mailstore.LocalBodyPart r5 = new com.fsck.k9.mailstore.LocalBodyPart
            java.lang.String r6 = r29.getAccountUuid()
            r7 = r30
            r5.<init>(r6, r7, r8, r10)
            com.fsck.k9.mail.Body r6 = r25.getBody()
            com.fsck.k9.mail.Multipart r6 = (com.fsck.k9.mail.Multipart) r6
            r6.addBodyPart(r5)
            r26 = r5
        L_0x00c2:
            r0 = r29
            r1 = r26
            r2 = r18
            r0.parseHeaderBytes(r1, r2)
            goto L_0x003d
        L_0x00cd:
            boolean r6 = com.fsck.k9.mail.internet.MimeUtility.isMessage(r24)
            if (r6 == 0) goto L_0x00e2
            com.fsck.k9.mail.internet.MimeMessage r19 = new com.fsck.k9.mail.internet.MimeMessage
            r19.<init>()
            r0 = r25
            r1 = r19
            r0.setBody(r1)
            r26 = r19
            goto L_0x00c2
        L_0x00e2:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r7 = "Parent is neither a multipart nor a message"
            r6.<init>(r7)
            throw r6
        L_0x00ea:
            r6 = 1
            if (r14 != r6) goto L_0x0107
            r6 = 7
            r0 = r32
            java.lang.String r15 = r0.getString(r6)
            r6 = 10
            r0 = r32
            byte[] r13 = r0.getBlob(r6)
            com.fsck.k9.mailstore.BinaryMemoryBody r4 = new com.fsck.k9.mailstore.BinaryMemoryBody
            r4.<init>(r13, r15)
            r0 = r26
            r0.setBody(r4)
            goto L_0x008b
        L_0x0107:
            r6 = 2
            if (r14 != r6) goto L_0x008b
            r6 = 7
            r0 = r32
            java.lang.String r15 = r0.getString(r6)
            r0 = r29
            com.fsck.k9.mailstore.LocalStore r6 = r0.localStore
            java.lang.String r7 = java.lang.Long.toString(r8)
            java.io.File r17 = r6.getAttachmentFile(r7)
            boolean r6 = r17.exists()
            if (r6 == 0) goto L_0x008b
            com.fsck.k9.mailstore.FileBackedBody r4 = new com.fsck.k9.mailstore.FileBackedBody
            r0 = r17
            r4.<init>(r0, r15)
            r0 = r26
            r0.setBody(r4)
            goto L_0x008b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fsck.k9.mailstore.LocalFolder.loadMessagePart(com.fsck.k9.mailstore.LocalMessage, java.util.Map, android.database.Cursor):void");
    }

    private void parseHeaderBytes(Part part, byte[] header) throws MessagingException {
        MessageHeaderParser.parse(part, new ByteArrayInputStream(header));
    }

    public List<LocalMessage> getMessages(int start, int end, Date earliestDate, MessageRetrievalListener<LocalMessage> messageRetrievalListener) throws MessagingException {
        open(0);
        throw new MessagingException("LocalStore.getMessages(int, int, MessageRetrievalListener) not yet implemented");
    }

    public boolean areMoreMessagesAvailable(int indexOfOldestMessage, Date earliestDate) throws IOException, MessagingException {
        throw new IllegalStateException("Not implemented");
    }

    public String getMessageUidById(final long id) throws MessagingException {
        try {
            return (String) this.localStore.database.execute(false, new LockableDatabase.DbCallback<String>() {
                public String doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException, UnavailableStorageException {
                    Cursor cursor;
                    String string;
                    try {
                        LocalFolder.this.open(0);
                        cursor = null;
                        cursor = db.rawQuery("SELECT uid FROM messages WHERE id = ? AND folder_id = ?", new String[]{Long.toString(id), Long.toString(LocalFolder.this.mFolderId)});
                        if (!cursor.moveToNext()) {
                            string = null;
                            Utility.closeQuietly(cursor);
                        } else {
                            string = cursor.getString(0);
                            Utility.closeQuietly(cursor);
                        }
                        return string;
                    } catch (MessagingException e) {
                        throw new LockableDatabase.WrappedException(e);
                    } catch (Throwable th) {
                        Utility.closeQuietly(cursor);
                        throw th;
                    }
                }
            });
        } catch (LockableDatabase.WrappedException e) {
            throw ((MessagingException) e.getCause());
        }
    }

    public LocalMessage getMessage(final String uid) throws MessagingException {
        try {
            return (LocalMessage) this.localStore.database.execute(false, new LockableDatabase.DbCallback<LocalMessage>() {
                public LocalMessage doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException, UnavailableStorageException {
                    Cursor cursor;
                    try {
                        LocalFolder.this.open(0);
                        LocalMessage message = new LocalMessage(LocalFolder.this.localStore, uid, LocalFolder.this);
                        cursor = null;
                        cursor = db.rawQuery("SELECT " + LocalStore.GET_MESSAGES_COLS + "FROM messages " + "LEFT JOIN message_parts ON (message_parts.id = messages" + ".message_part_id) " + "LEFT JOIN threads ON (threads.message_id = messages.id) " + "WHERE uid = ? AND folder_id = ?", new String[]{message.getUid(), Long.toString(LocalFolder.this.mFolderId)});
                        if (!cursor.moveToNext()) {
                            message = null;
                            Utility.closeQuietly(cursor);
                        } else {
                            message.populateFromGetMessageCursor(cursor);
                            Utility.closeQuietly(cursor);
                        }
                        return message;
                    } catch (MessagingException e) {
                        throw new LockableDatabase.WrappedException(e);
                    } catch (Throwable th) {
                        Utility.closeQuietly(cursor);
                        throw th;
                    }
                }
            });
        } catch (LockableDatabase.WrappedException e) {
            throw ((MessagingException) e.getCause());
        }
    }

    public List<LocalMessage> getMessages(MessageRetrievalListener<LocalMessage> listener) throws MessagingException {
        return getMessages(listener, true);
    }

    public List<LocalMessage> getMessages(final MessageRetrievalListener<LocalMessage> listener, final boolean includeDeleted) throws MessagingException {
        try {
            return (List) this.localStore.database.execute(false, new LockableDatabase.DbCallback<List<LocalMessage>>() {
                public List<LocalMessage> doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException, UnavailableStorageException {
                    String str;
                    try {
                        LocalFolder.this.open(0);
                        LocalStore access$1100 = LocalFolder.this.localStore;
                        MessageRetrievalListener messageRetrievalListener = listener;
                        LocalFolder localFolder = LocalFolder.this;
                        StringBuilder append = new StringBuilder().append("SELECT ").append(LocalStore.GET_MESSAGES_COLS).append("FROM messages ").append("LEFT JOIN message_parts ON (message_parts.id ").append("= messages.message_part_id) ").append("LEFT JOIN threads ON (threads.message_id = ").append("messages.id) ").append("WHERE empty = 0 AND ");
                        if (includeDeleted) {
                            str = "";
                        } else {
                            str = "deleted = 0 AND ";
                        }
                        return access$1100.getMessages(messageRetrievalListener, localFolder, append.append(str).append("folder_id = ? ORDER BY date DESC").toString(), new String[]{Long.toString(LocalFolder.this.mFolderId)});
                    } catch (MessagingException e) {
                        throw new LockableDatabase.WrappedException(e);
                    }
                }
            });
        } catch (LockableDatabase.WrappedException e) {
            throw ((MessagingException) e.getCause());
        }
    }

    public List<LocalMessage> getMessagesByUids(@NonNull List<String> uids) throws MessagingException {
        open(0);
        List<LocalMessage> messages = new ArrayList<>();
        for (String uid : uids) {
            LocalMessage message = getMessage(uid);
            if (message != null) {
                messages.add(message);
            }
        }
        return messages;
    }

    public List<LocalMessage> getMessagesByReference(@NonNull List<MessageReference> messageReferences) throws MessagingException {
        open(0);
        String accountUuid = getAccountUuid();
        String folderName = getName();
        List<LocalMessage> messages = new ArrayList<>();
        for (MessageReference messageReference : messageReferences) {
            if (!accountUuid.equals(messageReference.getAccountUuid())) {
                throw new IllegalArgumentException("all message references must belong to this Account!");
            } else if (!folderName.equals(messageReference.getFolderName())) {
                throw new IllegalArgumentException("all message references must belong to this LocalFolder!");
            } else {
                LocalMessage message = getMessage(messageReference.getUid());
                if (message != null) {
                    messages.add(message);
                }
            }
        }
        return messages;
    }

    public Map<String, String> copyMessages(List<? extends Message> msgs, Folder folder) throws MessagingException {
        if (folder instanceof LocalFolder) {
            return ((LocalFolder) folder).appendMessages(msgs, true);
        }
        throw new MessagingException("copyMessages called with incorrect Folder");
    }

    public Map<String, String> moveMessages(final List<? extends Message> msgs, Folder destFolder) throws MessagingException {
        if (!(destFolder instanceof LocalFolder)) {
            throw new MessagingException("moveMessages called with non-LocalFolder");
        }
        final LocalFolder lDestFolder = (LocalFolder) destFolder;
        final Map<String, String> uidMap = new HashMap<>();
        try {
            this.localStore.database.execute(false, new LockableDatabase.DbCallback<Void>() {
                public Void doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException, UnavailableStorageException {
                    long newId;
                    try {
                        lDestFolder.open(0);
                        for (Message message : msgs) {
                            LocalMessage lMessage = (LocalMessage) message;
                            String oldUID = message.getUid();
                            if (K9.DEBUG) {
                                Log.d("k9", "Updating folder_id to " + lDestFolder.getId() + " for message with UID " + message.getUid() + ", id " + lMessage.getId() + " currently in folder " + LocalFolder.this.getName());
                            }
                            String newUid = K9.LOCAL_UID_PREFIX + UUID.randomUUID().toString();
                            message.setUid(newUid);
                            uidMap.put(oldUID, newUid);
                            ThreadInfo threadInfo = lDestFolder.doMessageThreading(db, message);
                            long msgId = lMessage.getId();
                            String[] idArg = {Long.toString(msgId)};
                            ContentValues cv = new ContentValues();
                            cv.put(EmailProvider.MessageColumns.FOLDER_ID, Long.valueOf(lDestFolder.getId()));
                            cv.put(EmailProvider.MessageColumns.UID, newUid);
                            db.update("messages", cv, "id = ?", idArg);
                            cv.clear();
                            cv.put("message_id", Long.valueOf(msgId));
                            if (threadInfo.threadId == -1) {
                                if (threadInfo.rootId != -1) {
                                    cv.put(EmailProvider.ThreadColumns.ROOT, Long.valueOf(threadInfo.rootId));
                                }
                                if (threadInfo.parentId != -1) {
                                    cv.put(EmailProvider.ThreadColumns.PARENT, Long.valueOf(threadInfo.parentId));
                                }
                                db.insert("threads", null, cv);
                            } else {
                                db.update("threads", cv, "id = ?", new String[]{Long.toString(threadInfo.threadId)});
                            }
                            LocalFolder.this.open(0);
                            cv.clear();
                            cv.put(EmailProvider.MessageColumns.UID, oldUID);
                            cv.putNull(EmailProvider.MessageColumns.FLAGS);
                            cv.put(EmailProvider.MessageColumns.READ, (Integer) 1);
                            cv.put(EmailProvider.InternalMessageColumns.DELETED, (Integer) 1);
                            cv.put(EmailProvider.MessageColumns.FOLDER_ID, Long.valueOf(LocalFolder.this.mFolderId));
                            cv.put("empty", (Integer) null);
                            String messageId = message.getMessageId();
                            if (messageId != null) {
                                cv.put("message_id", messageId);
                            }
                            if (threadInfo.msgId != -1) {
                                newId = threadInfo.msgId;
                                db.update("messages", cv, "id = ?", new String[]{Long.toString(newId)});
                            } else {
                                newId = db.insert("messages", null, cv);
                            }
                            cv.clear();
                            cv.put("message_id", Long.valueOf(newId));
                            db.update("threads", cv, "id = ?", new String[]{Long.toString(lMessage.getThreadId())});
                        }
                        return null;
                    } catch (MessagingException e) {
                        throw new LockableDatabase.WrappedException(e);
                    }
                }
            });
            this.localStore.notifyChange();
            return uidMap;
        } catch (LockableDatabase.WrappedException e) {
            throw ((MessagingException) e.getCause());
        }
    }

    public LocalMessage storeSmallMessage(final Message message, final Runnable runnable) throws MessagingException {
        return (LocalMessage) this.localStore.database.execute(true, new LockableDatabase.DbCallback<LocalMessage>() {
            public LocalMessage doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException, UnavailableStorageException {
                try {
                    LocalFolder.this.appendMessages(Collections.singletonList(message));
                    LocalMessage result = LocalFolder.this.getMessage(message.getUid());
                    runnable.run();
                    result.setFlag(Flag.X_DOWNLOADED_FULL, true);
                    return result;
                } catch (MessagingException e) {
                    throw new LockableDatabase.WrappedException(e);
                }
            }
        });
    }

    public Map<String, String> appendMessages(List<? extends Message> messages) throws MessagingException {
        return appendMessages(messages, false);
    }

    public void destroyMessages(final List<? extends Message> messages) {
        try {
            this.localStore.database.execute(true, new LockableDatabase.DbCallback<Void>() {
                public Void doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException, UnavailableStorageException {
                    for (Message message : messages) {
                        try {
                            message.destroy();
                        } catch (MessagingException e) {
                            throw new LockableDatabase.WrappedException(e);
                        }
                    }
                    return null;
                }
            });
        } catch (MessagingException e) {
            throw new LockableDatabase.WrappedException(e);
        }
    }

    private ThreadInfo getThreadInfo(SQLiteDatabase db, String messageId, boolean onlyEmpty) {
        if (messageId == null) {
            return null;
        }
        Cursor cursor = db.rawQuery("SELECT t.id, t.message_id, t.root, t.parent FROM messages m LEFT JOIN threads t ON (t.message_id = m.id) WHERE m.folder_id = ? AND m.message_id = ? " + (onlyEmpty ? "AND m.empty = 1 " : "") + "ORDER BY m.id LIMIT 1", new String[]{Long.toString(this.mFolderId), messageId});
        if (cursor != null) {
            try {
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    return new ThreadInfo(cursor.getLong(0), cursor.getLong(1), messageId, cursor.isNull(2) ? -1 : cursor.getLong(2), cursor.isNull(3) ? -1 : cursor.getLong(3));
                }
                cursor.close();
            } finally {
                cursor.close();
            }
        }
        return null;
    }

    private Map<String, String> appendMessages(final List<? extends Message> messages, final boolean copy) throws MessagingException {
        open(0);
        try {
            final Map<String, String> uidMap = new HashMap<>();
            this.localStore.database.execute(true, new LockableDatabase.DbCallback<Void>() {
                public Void doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException, UnavailableStorageException {
                    try {
                        for (Message message : messages) {
                            LocalFolder.this.saveMessage(db, message, copy, uidMap);
                        }
                        return null;
                    } catch (MessagingException e) {
                        throw new LockableDatabase.WrappedException(e);
                    }
                }
            });
            this.localStore.notifyChange();
            return uidMap;
        } catch (LockableDatabase.WrappedException e) {
            throw ((MessagingException) e.getCause());
        }
    }

    /* access modifiers changed from: protected */
    public void saveMessage(SQLiteDatabase db, Message message, boolean copy, Map<String, String> uidMap) throws MessagingException {
        long time;
        long msgId;
        if (!(message instanceof MimeMessage)) {
            throw new Error("LocalStore can only store Messages that extend MimeMessage");
        }
        long oldMessageId = -1;
        String uid = message.getUid();
        if (uid == null || copy) {
            String randomLocalUid = K9.LOCAL_UID_PREFIX + UUID.randomUUID().toString();
            if (copy) {
                uidMap.put(uid, randomLocalUid);
            } else {
                message.setUid(randomLocalUid);
            }
            uid = randomLocalUid;
        } else {
            LocalMessage oldMessage = getMessage(uid);
            if (oldMessage != null) {
                oldMessageId = oldMessage.getId();
                deleteMessagePartsAndDataFromDisk(oldMessage.getMessagePartId());
            }
        }
        long rootId = -1;
        long parentId = -1;
        if (oldMessageId == -1) {
            ThreadInfo threadInfo = doMessageThreading(db, message);
            oldMessageId = threadInfo.msgId;
            rootId = threadInfo.rootId;
            parentId = threadInfo.parentId;
        }
        try {
            PreviewResult previewResult = this.localStore.getMessagePreviewCreator().createPreview(message);
            DatabasePreviewType databasePreviewType = DatabasePreviewType.fromPreviewType(previewResult.getPreviewType());
            String fulltext = this.localStore.getMessageFulltextCreator().createFulltext(message);
            int attachmentCount = this.localStore.getAttachmentCounter().getAttachmentCount(message);
            long rootMessagePartId = saveMessageParts(db, message);
            ContentValues cv = new ContentValues();
            cv.put("message_part_id", Long.valueOf(rootMessagePartId));
            cv.put(EmailProvider.MessageColumns.UID, uid);
            cv.put("subject", message.getSubject());
            cv.put(EmailProvider.MessageColumns.SENDER_LIST, Address.pack(message.getFrom()));
            cv.put("date", Long.valueOf(message.getSentDate() == null ? System.currentTimeMillis() : message.getSentDate().getTime()));
            LocalStore localStore2 = this.localStore;
            cv.put(EmailProvider.MessageColumns.FLAGS, LocalStore.serializeFlags(message.getFlags()));
            cv.put(EmailProvider.InternalMessageColumns.DELETED, Integer.valueOf(message.isSet(Flag.DELETED) ? 1 : 0));
            cv.put(EmailProvider.MessageColumns.READ, Integer.valueOf(message.isSet(Flag.SEEN) ? 1 : 0));
            cv.put(EmailProvider.MessageColumns.FLAGGED, Integer.valueOf(message.isSet(Flag.FLAGGED) ? 1 : 0));
            cv.put(EmailProvider.MessageColumns.ANSWERED, Integer.valueOf(message.isSet(Flag.ANSWERED) ? 1 : 0));
            cv.put(EmailProvider.MessageColumns.FORWARDED, Integer.valueOf(message.isSet(Flag.FORWARDED) ? 1 : 0));
            cv.put(EmailProvider.MessageColumns.FOLDER_ID, Long.valueOf(this.mFolderId));
            cv.put(EmailProvider.MessageColumns.TO_LIST, Address.pack(message.getRecipients(Message.RecipientType.TO)));
            cv.put(EmailProvider.MessageColumns.CC_LIST, Address.pack(message.getRecipients(Message.RecipientType.CC)));
            cv.put(EmailProvider.MessageColumns.BCC_LIST, Address.pack(message.getRecipients(Message.RecipientType.BCC)));
            cv.put(EmailProvider.MessageColumns.REPLY_TO_LIST, Address.pack(message.getReplyTo()));
            cv.put(EmailProvider.MessageColumns.ATTACHMENT_COUNT, Integer.valueOf(attachmentCount));
            if (message.getInternalDate() == null) {
                time = System.currentTimeMillis();
            } else {
                time = message.getInternalDate().getTime();
            }
            cv.put(EmailProvider.MessageColumns.INTERNAL_DATE, Long.valueOf(time));
            cv.put(EmailProvider.InternalMessageColumns.MIME_TYPE, message.getMimeType());
            cv.put("empty", (Integer) null);
            cv.put(EmailProvider.MessageColumns.PREVIEW_TYPE, databasePreviewType.getDatabaseValue());
            if (previewResult.isPreviewTextAvailable()) {
                cv.put("preview", previewResult.getPreviewText());
            } else {
                cv.putNull("preview");
            }
            String messageId = message.getMessageId();
            if (messageId != null) {
                cv.put("message_id", messageId);
            }
            if (oldMessageId == -1) {
                msgId = db.insert("messages", EmailProvider.MessageColumns.UID, cv);
                cv.clear();
                cv.put("message_id", Long.valueOf(msgId));
                if (rootId != -1) {
                    cv.put(EmailProvider.ThreadColumns.ROOT, Long.valueOf(rootId));
                }
                if (parentId != -1) {
                    cv.put(EmailProvider.ThreadColumns.PARENT, Long.valueOf(parentId));
                }
                db.insert("threads", null, cv);
            } else {
                msgId = oldMessageId;
                db.update("messages", cv, "id = ?", new String[]{Long.toString(oldMessageId)});
            }
            if (fulltext != null) {
                cv.clear();
                cv.put("docid", Long.valueOf(msgId));
                cv.put("fulltext", fulltext);
                db.replace("messages_fulltext", null, cv);
            }
        } catch (Exception e) {
            throw new MessagingException("Error appending message: " + message.getSubject(), e);
        }
    }

    private long saveMessageParts(SQLiteDatabase db, Message message) throws IOException, MessagingException {
        long rootMessagePartId = saveMessagePart(db, new PartContainer(-1, message), -1, 0);
        Stack<PartContainer> partsToSave = new Stack<>();
        addChildrenToStack(partsToSave, message, rootMessagePartId);
        int order = 1;
        while (!partsToSave.isEmpty()) {
            PartContainer partContainer = (PartContainer) partsToSave.pop();
            long messagePartId = saveMessagePart(db, partContainer, rootMessagePartId, order);
            order++;
            addChildrenToStack(partsToSave, partContainer.part, messagePartId);
        }
        return rootMessagePartId;
    }

    private long saveMessagePart(SQLiteDatabase db, PartContainer partContainer, long rootMessagePartId, int order) throws IOException, MessagingException {
        Part part = partContainer.part;
        ContentValues cv = new ContentValues();
        if (rootMessagePartId != -1) {
            cv.put(EmailProvider.ThreadColumns.ROOT, Long.valueOf(rootMessagePartId));
        }
        cv.put(EmailProvider.ThreadColumns.PARENT, Long.valueOf(partContainer.parent));
        cv.put("seq", Integer.valueOf(order));
        cv.put("server_extra", part.getServerExtra());
        return updateOrInsertMessagePart(db, cv, part, -1);
    }

    private void renameTemporaryFile(File file, String messagePartId) {
        File destination = this.localStore.getAttachmentFile(messagePartId);
        if (!file.renameTo(destination)) {
            Log.w("k9", "Couldn't rename temporary file " + file.getAbsolutePath() + " to " + destination.getAbsolutePath());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* access modifiers changed from: private */
    public long updateOrInsertMessagePart(SQLiteDatabase db, ContentValues cv, Part part, long existingMessagePartId) throws IOException, MessagingException {
        long messagePartId;
        byte[] headerBytes = getHeaderBytes(part);
        cv.put(EmailProvider.InternalMessageColumns.MIME_TYPE, part.getMimeType());
        cv.put("header", headerBytes);
        cv.put(SettingsExporter.TYPE_ATTRIBUTE, (Integer) 0);
        File file = null;
        Body body = part.getBody();
        if (body instanceof Multipart) {
            multipartToContentValues(cv, (Multipart) body);
        } else if (body == null) {
            missingPartToContentValues(cv, part);
        } else if (body instanceof Message) {
            messageMarkerToContentValues(cv);
        } else {
            file = leafPartToContentValues(cv, part, body);
        }
        if (existingMessagePartId != -1) {
            messagePartId = existingMessagePartId;
            db.update("message_parts", cv, "id = ?", new String[]{Long.toString(messagePartId)});
        } else {
            messagePartId = db.insertOrThrow("message_parts", null, cv);
        }
        if (file != null) {
            renameTemporaryFile(file, Long.toString(messagePartId));
        }
        return messagePartId;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private void multipartToContentValues(ContentValues cv, Multipart multipart) {
        cv.put("data_location", (Integer) 1);
        cv.put("preamble", multipart.getPreamble());
        cv.put("epilogue", multipart.getEpilogue());
        cv.put(ContentTypeField.PARAM_BOUNDARY, multipart.getBoundary());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private void missingPartToContentValues(ContentValues cv, Part part) throws MessagingException {
        AttachmentViewInfo attachment = attachmentInfoExtractor.extractAttachmentInfoForDatabase(part);
        cv.put("display_name", attachment.displayName);
        cv.put("data_location", (Integer) 0);
        cv.put("decoded_body_size", Long.valueOf(attachment.size));
        if (MimeUtility.isMultipart(part.getMimeType())) {
            cv.put(ContentTypeField.PARAM_BOUNDARY, BoundaryGenerator.getInstance().generateBoundary());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private void messageMarkerToContentValues(ContentValues cv) throws MessagingException {
        cv.put("data_location", (Integer) 3);
    }

    private File leafPartToContentValues(ContentValues cv, Part part, Body body) throws MessagingException, IOException {
        int dataLocation;
        cv.put("display_name", attachmentInfoExtractor.extractAttachmentInfoForDatabase(part).displayName);
        String encoding = getTransferEncoding(part);
        if (!(body instanceof SizeAware)) {
            throw new IllegalStateException("Body needs to implement SizeAware");
        }
        long fileSize = ((SizeAware) body).getSize();
        File file = null;
        if (fileSize > PlaybackStateCompat.ACTION_PREPARE) {
            dataLocation = 2;
            file = writeBodyToDiskIfNecessary(part);
            cv.put("decoded_body_size", Long.valueOf(decodeAndCountBytes(file, encoding, fileSize)));
        } else {
            dataLocation = 1;
            byte[] bodyData = getBodyBytes(body);
            cv.put("data", bodyData);
            cv.put("decoded_body_size", Long.valueOf(decodeAndCountBytes(bodyData, encoding, (long) bodyData.length)));
        }
        cv.put("data_location", Integer.valueOf(dataLocation));
        cv.put("encoding", encoding);
        cv.put("content_id", part.getContentId());
        return file;
    }

    private File writeBodyToDiskIfNecessary(Part part) throws MessagingException, IOException {
        Body body = part.getBody();
        if (body instanceof BinaryTempFileBody) {
            return ((BinaryTempFileBody) body).getFile();
        }
        return writeBodyToDisk(body);
    }

    private File writeBodyToDisk(Body body) throws IOException, MessagingException {
        File file = File.createTempFile("body", null, BinaryTempFileBody.getTempDirectory());
        OutputStream out = new FileOutputStream(file);
        try {
            body.writeTo(out);
            return file;
        } finally {
            out.close();
        }
    }

    private long decodeAndCountBytes(byte[] bodyData, String encoding, long fallbackValue) {
        return decodeAndCountBytes(new ByteArrayInputStream(bodyData), encoding, fallbackValue);
    }

    private long decodeAndCountBytes(File file, String encoding, long fallbackValue) throws MessagingException, IOException {
        InputStream inputStream = new FileInputStream(file);
        try {
            return decodeAndCountBytes(inputStream, encoding, fallbackValue);
        } finally {
            inputStream.close();
        }
    }

    private long decodeAndCountBytes(InputStream rawInputStream, String encoding, long fallbackValue) {
        InputStream decodingInputStream = this.localStore.getDecodingInputStream(rawInputStream, encoding);
        try {
            CountingOutputStream countingOutputStream = new CountingOutputStream();
            try {
                IOUtils.copy(decodingInputStream, countingOutputStream);
                fallbackValue = countingOutputStream.getCount();
                try {
                    decodingInputStream.close();
                } catch (IOException e) {
                }
            } catch (IOException e2) {
            }
            return fallbackValue;
        } finally {
            try {
                decodingInputStream.close();
            } catch (IOException e3) {
            }
        }
    }

    private byte[] getHeaderBytes(Part part) throws IOException, MessagingException {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        part.writeHeaderTo(output);
        return output.toByteArray();
    }

    private byte[] getBodyBytes(Body body) throws IOException, MessagingException {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        body.writeTo(output);
        return output.toByteArray();
    }

    private String getTransferEncoding(Part part) {
        String[] contentTransferEncoding = part.getHeader("Content-Transfer-Encoding");
        if (contentTransferEncoding.length > 0) {
            return contentTransferEncoding[0].toLowerCase(Locale.US);
        }
        return MimeUtil.ENC_7BIT;
    }

    private void addChildrenToStack(Stack<PartContainer> stack, Part part, long parentMessageId) {
        Body body = part.getBody();
        if (body instanceof Multipart) {
            Multipart multipart = (Multipart) body;
            for (int i = multipart.getCount() - 1; i >= 0; i--) {
                stack.push(new PartContainer(parentMessageId, multipart.getBodyPart(i)));
            }
        } else if (body instanceof Message) {
            stack.push(new PartContainer(parentMessageId, (Message) body));
        }
    }

    private static class PartContainer {
        public final long parent;
        public final Part part;

        PartContainer(long parent2, Part part2) {
            this.parent = parent2;
            this.part = part2;
        }
    }

    public void addPartToMessage(final LocalMessage message, final Part part) throws MessagingException {
        open(0);
        this.localStore.database.execute(false, new LockableDatabase.DbCallback<Void>() {
            public Void doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException, UnavailableStorageException {
                Cursor cursor = db.query("message_parts", new String[]{"id"}, "root = ? AND server_extra = ?", new String[]{Long.toString(message.getMessagePartId()), part.getServerExtra()}, null, null, null);
                try {
                    if (!cursor.moveToFirst()) {
                        throw new IllegalStateException("Message part not found");
                    }
                    try {
                        long unused = LocalFolder.this.updateOrInsertMessagePart(db, new ContentValues(), part, cursor.getLong(0));
                    } catch (Exception e) {
                        Log.e("k9", "Error writing message part", e);
                    }
                    return null;
                } finally {
                    cursor.close();
                }
            }
        });
        this.localStore.notifyChange();
    }

    public void changeUid(final LocalMessage message) throws MessagingException {
        open(0);
        final ContentValues cv = new ContentValues();
        cv.put(EmailProvider.MessageColumns.UID, message.getUid());
        this.localStore.database.execute(false, new LockableDatabase.DbCallback<Void>() {
            public Void doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException, UnavailableStorageException {
                db.update("messages", cv, "id = ?", new String[]{Long.toString(message.getId())});
                return null;
            }
        });
        this.localStore.notifyChange();
    }

    public void setFlags(final List<? extends Message> messages, final Set<Flag> flags, final boolean value) throws MessagingException {
        open(0);
        try {
            this.localStore.database.execute(true, new LockableDatabase.DbCallback<Void>() {
                public Void doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException, UnavailableStorageException {
                    for (Message message : messages) {
                        try {
                            message.setFlags(flags, value);
                        } catch (MessagingException e) {
                            Log.e("k9", "Something went wrong while setting flag", e);
                        }
                    }
                    return null;
                }
            });
        } catch (LockableDatabase.WrappedException e) {
            throw ((MessagingException) e.getCause());
        }
    }

    public void setFlags(Set<Flag> flags, boolean value) throws MessagingException {
        open(0);
        for (LocalMessage message : getMessages(null)) {
            message.setFlags(flags, value);
        }
    }

    public String getUidFromMessageId(Message message) throws MessagingException {
        throw new MessagingException("Cannot call getUidFromMessageId on LocalFolder");
    }

    public void clearMessagesOlderThan(long cutoff) throws MessagingException {
        open(1);
        Iterator<LocalMessage> it = this.localStore.getMessages(null, this, "SELECT " + LocalStore.GET_MESSAGES_COLS + "FROM messages " + "LEFT JOIN message_parts ON (message_parts.id = messages" + ".message_part_id) " + "LEFT JOIN threads ON (threads.message_id = messages.id) " + "WHERE empty = 0 AND (folder_id = ? and date < ?)", new String[]{Long.toString(this.mFolderId), Long.toString(cutoff)}).iterator();
        while (it.hasNext()) {
            it.next().destroy();
        }
        this.localStore.notifyChange();
    }

    public void clearAllMessages() throws MessagingException {
        final String[] folderIdArg = {Long.toString(this.mFolderId)};
        open(1);
        try {
            this.localStore.database.execute(false, new LockableDatabase.DbCallback<Void>() {
                public Void doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException {
                    Cursor cursor;
                    try {
                        SQLiteDatabase sQLiteDatabase = db;
                        cursor = sQLiteDatabase.query("messages", new String[]{"message_part_id"}, "folder_id = ? AND empty = 0", folderIdArg, null, null, null);
                        while (cursor.moveToNext()) {
                            LocalFolder.this.deleteMessageDataFromDisk(cursor.getLong(0));
                        }
                        cursor.close();
                        db.execSQL("DELETE FROM threads WHERE message_id IN (SELECT id FROM messages WHERE folder_id = ?)", folderIdArg);
                        db.execSQL("DELETE FROM messages WHERE folder_id = ?", folderIdArg);
                        LocalFolder.this.setMoreMessages(MoreMessages.UNKNOWN);
                        return null;
                    } catch (MessagingException e) {
                        throw new LockableDatabase.WrappedException(e);
                    } catch (Throwable th) {
                        cursor.close();
                        throw th;
                    }
                }
            });
            this.localStore.notifyChange();
            setPushState(null);
            setLastPush(0);
            setLastChecked(0);
            setVisibleLimit(getAccount().getDisplayCount());
        } catch (LockableDatabase.WrappedException e) {
            throw ((MessagingException) e.getCause());
        }
    }

    public void delete(boolean recurse) throws MessagingException {
        try {
            this.localStore.database.execute(false, new LockableDatabase.DbCallback<Void>() {
                public Void doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException, UnavailableStorageException {
                    try {
                        LocalFolder.this.open(1);
                        for (LocalMessage message : LocalFolder.this.getMessages(null)) {
                            LocalFolder.this.deleteMessageDataFromDisk(message.getMessagePartId());
                        }
                        db.execSQL("DELETE FROM folders WHERE id = ?", new Object[]{Long.toString(LocalFolder.this.mFolderId)});
                        return null;
                    } catch (MessagingException e) {
                        throw new LockableDatabase.WrappedException(e);
                    }
                }
            });
        } catch (LockableDatabase.WrappedException e) {
            throw ((MessagingException) e.getCause());
        }
    }

    public boolean equals(Object o) {
        if (o instanceof LocalFolder) {
            return ((LocalFolder) o).mName.equals(this.mName);
        }
        return super.equals(o);
    }

    public int hashCode() {
        return this.mName.hashCode();
    }

    /* access modifiers changed from: package-private */
    public void deleteMessagePartsAndDataFromDisk(long rootMessagePartId) throws MessagingException {
        deleteMessageDataFromDisk(rootMessagePartId);
        deleteMessageParts(rootMessagePartId);
    }

    private void deleteMessageParts(final long rootMessagePartId) throws MessagingException {
        this.localStore.database.execute(false, new LockableDatabase.DbCallback<Void>() {
            public Void doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException, UnavailableStorageException {
                db.delete("message_parts", "root = ?", new String[]{Long.toString(rootMessagePartId)});
                return null;
            }
        });
    }

    /* access modifiers changed from: private */
    public void deleteMessageDataFromDisk(final long rootMessagePartId) throws MessagingException {
        this.localStore.database.execute(false, new LockableDatabase.DbCallback<Void>() {
            public Void doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException, UnavailableStorageException {
                LocalFolder.this.deleteMessagePartsFromDisk(db, rootMessagePartId);
                return null;
            }
        });
    }

    /* access modifiers changed from: private */
    public void deleteMessagePartsFromDisk(SQLiteDatabase db, long rootMessagePartId) {
        Cursor cursor = db.query("message_parts", new String[]{"id"}, "root = ? AND data_location = 2", new String[]{Long.toString(rootMessagePartId)}, null, null, null);
        while (cursor.moveToNext()) {
            try {
                File file = this.localStore.getAttachmentFile(cursor.getString(0));
                if (file.exists() && !file.delete() && K9.DEBUG) {
                    Log.d("k9", "Couldn't delete message part file: " + file.getAbsolutePath());
                }
            } finally {
                cursor.close();
            }
        }
    }

    public boolean isInTopGroup() {
        return this.mInTopGroup;
    }

    public void setInTopGroup(boolean inTopGroup) throws MessagingException {
        this.mInTopGroup = inTopGroup;
        updateFolderColumn(EmailProvider.FolderColumns.TOP_GROUP, Integer.valueOf(this.mInTopGroup ? 1 : 0));
    }

    public Integer getLastUid() {
        return this.mLastUid;
    }

    public void updateLastUid() throws MessagingException {
        Integer lastUid = (Integer) this.localStore.database.execute(false, new LockableDatabase.DbCallback<Integer>() {
            public Integer doDbWork(SQLiteDatabase db) {
                Cursor cursor = null;
                try {
                    LocalFolder.this.open(1);
                    cursor = db.rawQuery("SELECT MAX(uid) FROM messages WHERE folder_id=?", new String[]{Long.toString(LocalFolder.this.mFolderId)});
                    if (cursor.getCount() > 0) {
                        cursor.moveToFirst();
                        return Integer.valueOf(cursor.getInt(0));
                    }
                    Utility.closeQuietly(cursor);
                    return null;
                } catch (Exception e) {
                    Log.e("k9", "Unable to updateLastUid: ", e);
                } finally {
                    Utility.closeQuietly(cursor);
                }
            }
        });
        if (K9.DEBUG) {
            Log.d("k9", "Updated last UID for folder " + this.mName + " to " + lastUid);
        }
        this.mLastUid = lastUid;
    }

    public Long getOldestMessageDate() throws MessagingException {
        return (Long) this.localStore.database.execute(false, new LockableDatabase.DbCallback<Long>() {
            public Long doDbWork(SQLiteDatabase db) {
                Cursor cursor = null;
                try {
                    LocalFolder.this.open(1);
                    cursor = db.rawQuery("SELECT MIN(date) FROM messages WHERE folder_id=?", new String[]{Long.toString(LocalFolder.this.mFolderId)});
                    if (cursor.getCount() > 0) {
                        cursor.moveToFirst();
                        return Long.valueOf(cursor.getLong(0));
                    }
                    Utility.closeQuietly(cursor);
                    return null;
                } catch (Exception e) {
                    Log.e("k9", "Unable to fetch oldest message date: ", e);
                } finally {
                    Utility.closeQuietly(cursor);
                }
            }
        });
    }

    /* JADX INFO: additional move instructions added (4) to help type inference */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* access modifiers changed from: private */
    public ThreadInfo doMessageThreading(SQLiteDatabase db, Message message) throws MessagingException {
        long threadId;
        long msgId;
        String messageId = message.getMessageId();
        ThreadInfo msgThreadInfo = getThreadInfo(db, messageId, true);
        String[] referencesArray = message.getHeader("References");
        List<String> messageIds = null;
        if (referencesArray.length > 0) {
            messageIds = Utility.extractMessageIds(referencesArray[0]);
        }
        String[] inReplyToArray = message.getHeader("In-Reply-To");
        ArrayList<String> arrayList = messageIds;
        if (inReplyToArray.length > 0) {
            String inReplyTo = Utility.extractMessageId(inReplyToArray[0]);
            arrayList = messageIds;
            if (inReplyTo != null) {
                if (messageIds == null) {
                    ArrayList arrayList2 = new ArrayList(1);
                    arrayList2.add(inReplyTo);
                    arrayList = arrayList2;
                } else {
                    boolean contains = messageIds.contains(inReplyTo);
                    arrayList = messageIds;
                    if (!contains) {
                        messageIds.add(inReplyTo);
                        arrayList = messageIds;
                    }
                }
            }
        }
        if (arrayList == null) {
            if (msgThreadInfo == null) {
                msgThreadInfo = new ThreadInfo(-1, -1, messageId, -1, -1);
            }
            return msgThreadInfo;
        }
        long parentId = -1;
        long rootId = -1;
        for (String reference : arrayList) {
            ThreadInfo threadInfo = getThreadInfo(db, reference, false);
            if (threadInfo == null) {
                ContentValues cv = new ContentValues();
                cv.put("message_id", reference);
                cv.put(EmailProvider.MessageColumns.FOLDER_ID, Long.valueOf(this.mFolderId));
                cv.put("empty", (Integer) 1);
                long newMsgId = db.insert("messages", null, cv);
                cv.clear();
                cv.put("message_id", Long.valueOf(newMsgId));
                if (rootId != -1) {
                    cv.put(EmailProvider.ThreadColumns.ROOT, Long.valueOf(rootId));
                }
                if (parentId != -1) {
                    cv.put(EmailProvider.ThreadColumns.PARENT, Long.valueOf(parentId));
                }
                parentId = db.insert("threads", null, cv);
                if (rootId == -1) {
                    rootId = parentId;
                }
            } else {
                if (rootId == -1 || threadInfo.rootId != -1 || rootId == threadInfo.threadId) {
                    rootId = threadInfo.rootId == -1 ? threadInfo.threadId : threadInfo.rootId;
                } else {
                    ContentValues cv2 = new ContentValues();
                    cv2.put(EmailProvider.ThreadColumns.ROOT, Long.valueOf(rootId));
                    db.update("threads", cv2, "root = ?", new String[]{Long.toString(threadInfo.threadId)});
                    cv2.put(EmailProvider.ThreadColumns.PARENT, Long.valueOf(parentId));
                    db.update("threads", cv2, "id = ?", new String[]{Long.toString(threadInfo.threadId)});
                }
                parentId = threadInfo.threadId;
            }
        }
        if (msgThreadInfo != null) {
            threadId = msgThreadInfo.threadId;
            msgId = msgThreadInfo.msgId;
        } else {
            threadId = -1;
            msgId = -1;
        }
        return new ThreadInfo(threadId, msgId, messageId, rootId, parentId);
    }

    public List<Message> extractNewMessages(final List<Message> messages) throws MessagingException {
        try {
            return (List) this.localStore.database.execute(false, new LockableDatabase.DbCallback<List<Message>>() {
                /* JADX INFO: finally extract failed */
                public List<Message> doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException {
                    try {
                        LocalFolder.this.open(0);
                        ArrayList arrayList = new ArrayList();
                        ArrayList arrayList2 = new ArrayList();
                        Set<String> existingMessages = new HashSet<>();
                        int start = 0;
                        while (start < messages.size()) {
                            StringBuilder selection = new StringBuilder();
                            selection.append("folder_id = ? AND UID IN (");
                            arrayList2.add(Long.toString(LocalFolder.this.mFolderId));
                            int count = Math.min(messages.size() - start, 500);
                            int end = start + count;
                            for (int i = start; i < end; i++) {
                                if (i > start) {
                                    selection.append(",?");
                                } else {
                                    selection.append("?");
                                }
                                arrayList2.add(((Message) messages.get(i)).getUid());
                            }
                            selection.append(")");
                            SQLiteDatabase sQLiteDatabase = db;
                            Cursor cursor = sQLiteDatabase.query("messages", LocalStore.UID_CHECK_PROJECTION, selection.toString(), (String[]) arrayList2.toArray(LocalStore.EMPTY_STRING_ARRAY), null, null, null);
                            while (cursor.moveToNext()) {
                                try {
                                    existingMessages.add(cursor.getString(0));
                                } catch (Throwable th) {
                                    Utility.closeQuietly(cursor);
                                    throw th;
                                }
                            }
                            Utility.closeQuietly(cursor);
                            int end2 = start + count;
                            for (int i2 = start; i2 < end2; i2++) {
                                Message message = (Message) messages.get(i2);
                                if (!existingMessages.contains(message.getUid())) {
                                    arrayList.add(message);
                                }
                            }
                            existingMessages.clear();
                            arrayList2.clear();
                            start += count;
                        }
                        return arrayList;
                    } catch (MessagingException e) {
                        throw new LockableDatabase.WrappedException(e);
                    }
                }
            });
        } catch (LockableDatabase.WrappedException e) {
            throw ((MessagingException) e.getCause());
        }
    }

    private Account getAccount() {
        return this.localStore.getAccount();
    }

    static class MessagePartType {
        static final int ALTERNATIVE_HTML = 2;
        static final int ALTERNATIVE_PLAIN = 1;
        static final int ATTACHMENT = 5;
        static final int HIDDEN_ATTACHMENT = 6;
        static final int RELATED = 4;
        static final int TEXT = 3;
        static final int UNKNOWN = 0;

        MessagePartType() {
        }
    }

    static class DataLocation {
        static final int CHILD_PART_CONTAINS_DATA = 3;
        static final int IN_DATABASE = 1;
        static final int MISSING = 0;
        static final int ON_DISK = 2;

        DataLocation() {
        }
    }

    public enum MoreMessages {
        UNKNOWN(EnvironmentCompat.MEDIA_UNKNOWN),
        FALSE(K9RemoteControl.K9_DISABLED),
        TRUE("true");
        
        private final String databaseName;

        private MoreMessages(String databaseName2) {
            this.databaseName = databaseName2;
        }

        public static MoreMessages fromDatabaseName(String databaseName2) {
            for (MoreMessages value : values()) {
                if (value.databaseName.equals(databaseName2)) {
                    return value;
                }
            }
            throw new IllegalArgumentException("Unknown value: " + databaseName2);
        }

        public String getDatabaseName() {
            return this.databaseName;
        }
    }

    /* access modifiers changed from: protected */
    public void saveMessageSearch(String search, SQLiteDatabase db, Message message, boolean copy, Map<String, String> uidMap) throws MessagingException {
        long time;
        long msgId;
        if (!(message instanceof MimeMessage)) {
            throw new Error("LocalStore can only store Messages that extend MimeMessage");
        }
        long oldMessageId = -1;
        String uid = message.getUid();
        if (uid == null || copy) {
            String randomLocalUid = K9.LOCAL_UID_PREFIX + UUID.randomUUID().toString();
            if (copy) {
                uidMap.put(uid, randomLocalUid);
            } else {
                message.setUid(randomLocalUid);
            }
            uid = randomLocalUid;
        } else {
            LocalMessage oldMessage = getMessage(uid);
            if (oldMessage != null) {
                oldMessageId = oldMessage.getId();
                deleteMessagePartsAndDataFromDisk(oldMessage.getMessagePartId());
            }
        }
        long rootId = -1;
        long parentId = -1;
        if (oldMessageId == -1) {
            ThreadInfo threadInfo = doMessageThreading(db, message);
            oldMessageId = threadInfo.msgId;
            rootId = threadInfo.rootId;
            parentId = threadInfo.parentId;
        }
        try {
            PreviewResult previewResult = this.localStore.getMessagePreviewCreator().createPreview(message);
            DatabasePreviewType databasePreviewType = DatabasePreviewType.fromPreviewType(previewResult.getPreviewType());
            String fulltext = this.localStore.getMessageFulltextCreator().createFulltext(message);
            int attachmentCount = this.localStore.getAttachmentCounter().getAttachmentCount(message);
            long rootMessagePartId = saveMessageParts(db, message);
            ContentValues cv = new ContentValues();
            cv.put("message_part_id", Long.valueOf(rootMessagePartId));
            cv.put(EmailProvider.MessageColumns.UID, uid);
            cv.put("subject", message.getSubject());
            cv.put(EmailProvider.MessageColumns.SENDER_LIST, Address.pack(message.getFrom()));
            cv.put("date", Long.valueOf(message.getSentDate() == null ? System.currentTimeMillis() : message.getSentDate().getTime()));
            LocalStore localStore2 = this.localStore;
            cv.put(EmailProvider.MessageColumns.FLAGS, LocalStore.serializeFlags(message.getFlags()));
            cv.put(EmailProvider.InternalMessageColumns.DELETED, Integer.valueOf(message.isSet(Flag.DELETED) ? 1 : 0));
            cv.put(EmailProvider.MessageColumns.READ, Integer.valueOf(message.isSet(Flag.SEEN) ? 1 : 0));
            cv.put(EmailProvider.MessageColumns.FLAGGED, Integer.valueOf(message.isSet(Flag.FLAGGED) ? 1 : 0));
            cv.put(EmailProvider.MessageColumns.ANSWERED, Integer.valueOf(message.isSet(Flag.ANSWERED) ? 1 : 0));
            cv.put(EmailProvider.MessageColumns.FORWARDED, Integer.valueOf(message.isSet(Flag.FORWARDED) ? 1 : 0));
            cv.put(EmailProvider.MessageColumns.FOLDER_ID, Long.valueOf(this.mFolderId));
            cv.put(EmailProvider.MessageColumns.TO_LIST, Address.pack(message.getRecipients(Message.RecipientType.TO)));
            cv.put(EmailProvider.MessageColumns.CC_LIST, Address.pack(message.getRecipients(Message.RecipientType.CC)));
            cv.put(EmailProvider.MessageColumns.BCC_LIST, Address.pack(message.getRecipients(Message.RecipientType.BCC)));
            cv.put(EmailProvider.MessageColumns.REPLY_TO_LIST, Address.pack(message.getReplyTo()));
            if (TextUtils.isEmpty(search)) {
                cv.put(EmailProvider.MessageColumns.ATTACHMENT_COUNT, Integer.valueOf(attachmentCount));
            } else {
                cv.put(EmailProvider.MessageColumns.ATTACHMENT_COUNT, (Integer) null);
            }
            if (message.getInternalDate() == null) {
                time = System.currentTimeMillis();
            } else {
                time = message.getInternalDate().getTime();
            }
            cv.put(EmailProvider.MessageColumns.INTERNAL_DATE, Long.valueOf(time));
            cv.put(EmailProvider.InternalMessageColumns.MIME_TYPE, message.getMimeType());
            cv.put("empty", (Integer) null);
            cv.put(EmailProvider.MessageColumns.PREVIEW_TYPE, databasePreviewType.getDatabaseValue());
            if (previewResult.isPreviewTextAvailable()) {
                cv.put("preview", previewResult.getPreviewText());
            } else {
                cv.putNull("preview");
            }
            String messageId = message.getMessageId();
            if (messageId != null) {
                cv.put("message_id", messageId);
            }
            if (oldMessageId == -1) {
                msgId = db.insert("messages", EmailProvider.MessageColumns.UID, cv);
                cv.clear();
                cv.put("message_id", Long.valueOf(msgId));
                if (rootId != -1) {
                    cv.put(EmailProvider.ThreadColumns.ROOT, Long.valueOf(rootId));
                }
                if (parentId != -1) {
                    cv.put(EmailProvider.ThreadColumns.PARENT, Long.valueOf(parentId));
                }
                db.insert("threads", null, cv);
            } else {
                msgId = oldMessageId;
                db.update("messages", cv, "id = ?", new String[]{Long.toString(oldMessageId)});
            }
            if (fulltext != null) {
                cv.clear();
                cv.put("docid", Long.valueOf(msgId));
                cv.put("fulltext", fulltext);
                db.replace("messages_fulltext", null, cv);
                return;
            }
            cv.clear();
            cv.put("docid", Long.valueOf(msgId));
            cv.put("fulltext", search);
            db.replace("messages_fulltext", null, cv);
        } catch (Exception e) {
            throw new MessagingException("Error appending message: " + message.getSubject(), e);
        }
    }

    private Map<String, String> appendMessagesSearch(String search, List<? extends Message> messages, boolean copy) throws MessagingException {
        open(0);
        try {
            final Map<String, String> uidMap = new HashMap<>();
            final List<? extends Message> list = messages;
            final String str = search;
            final boolean z = copy;
            this.localStore.database.execute(true, new LockableDatabase.DbCallback<Void>() {
                public Void doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException, UnavailableStorageException {
                    try {
                        for (Message message : list) {
                            LocalFolder.this.saveMessageSearch(str, db, message, z, uidMap);
                        }
                    } catch (MessagingException e) {
                        if (K9.DEBUG) {
                            Log.e("AtomicGonza", "okAG " + e.getMessage() + " " + new Exception().getStackTrace()[0].toString());
                        }
                        MessagingController.addErrorMessage(Preferences.getPreferences(K9.app).getDefaultAccount(), null, e, K9.app);
                    }
                    return null;
                }
            });
            this.localStore.notifyChange();
            return uidMap;
        } catch (LockableDatabase.WrappedException e) {
            throw ((MessagingException) e.getCause());
        }
    }

    public Map<String, String> appendMessagesSearch(String search, List<? extends Message> messages) throws MessagingException {
        return appendMessagesSearch(search, messages, false);
    }
}
