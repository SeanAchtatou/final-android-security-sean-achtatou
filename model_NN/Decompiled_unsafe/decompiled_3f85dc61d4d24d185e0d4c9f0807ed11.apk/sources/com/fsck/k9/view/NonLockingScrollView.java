package com.fsck.k9.view;

import android.content.Context;
import android.graphics.Rect;
import android.support.v4.widget.NestedScrollView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.webkit.WebView;
import java.util.ArrayList;
import java.util.List;

public class NonLockingScrollView extends NestedScrollView {
    private final List<View> mChildrenNeedingAllTouches = new ArrayList();
    private boolean mInCustomDrag = false;
    private boolean mSkipWebViewScroll = true;
    private final Rect sHitFrame = new Rect();

    public NonLockingScrollView(Context context) {
        super(context);
    }

    public NonLockingScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NonLockingScrollView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public boolean onInterceptTouchEvent(MotionEvent ev) {
        boolean isUp;
        if (getActionMasked(ev) == 1) {
            isUp = true;
        } else {
            isUp = false;
        }
        if (isUp && this.mInCustomDrag) {
            this.mInCustomDrag = false;
            onTouchEvent(ev);
            return true;
        } else if (!this.mInCustomDrag && !isEventOverChild(ev, this.mChildrenNeedingAllTouches)) {
            return super.onInterceptTouchEvent(ev);
        } else {
            this.mInCustomDrag = super.onInterceptTouchEvent(ev);
            if (this.mInCustomDrag) {
                onTouchEvent(ev);
            }
            return false;
        }
    }

    private int getActionMasked(MotionEvent ev) {
        return ev.getAction() & 255;
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        excludeChildrenFromInterceptions(this);
    }

    private void excludeChildrenFromInterceptions(View node) {
        if (node instanceof WebView) {
            this.mChildrenNeedingAllTouches.add(node);
        } else if (node instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) node;
            int childCount = viewGroup.getChildCount();
            for (int i = 0; i < childCount; i++) {
                excludeChildrenFromInterceptions(viewGroup.getChildAt(i));
            }
        }
    }

    private boolean isEventOverChild(MotionEvent ev, List<View> children) {
        int actionIndex = ev.getActionIndex();
        float x = ev.getX(actionIndex) + ((float) getScrollX());
        float y = ev.getY(actionIndex) + ((float) getScrollY());
        for (View child : children) {
            if (canViewReceivePointerEvents(child)) {
                child.getHitRect(this.sHitFrame);
                if (this.sHitFrame.contains((int) x, (int) y)) {
                    return true;
                }
            }
        }
        return false;
    }

    private static boolean canViewReceivePointerEvents(View child) {
        return child.getVisibility() == 0 || child.getAnimation() != null;
    }

    public void requestChildFocus(View child, View focused) {
        if (!this.mSkipWebViewScroll || !(focused instanceof MessageWebView) || !focused.getGlobalVisibleRect(new Rect())) {
            super.requestChildFocus(child, focused);
            return;
        }
        this.mSkipWebViewScroll = false;
        super.requestChildFocus(child, child);
        ViewParent parent = getParent();
        if (parent != null) {
            parent.requestChildFocus(this, focused);
        }
    }
}
