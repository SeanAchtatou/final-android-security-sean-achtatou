package com.fsck.k9.view;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.widget.ListView;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

public class K9PullToRefreshListView extends PullToRefreshListView {
    public K9PullToRefreshListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /* access modifiers changed from: protected */
    public ListView createListView(Context context, AttributeSet attrs) {
        return new ListView(context, attrs) {
            public void onRestoreInstanceState(Parcelable state) {
                super.onRestoreInstanceState(state);
                layoutChildren();
            }

            /* access modifiers changed from: protected */
            public void dispatchDraw(Canvas canvas) {
                if (getAdapter() != null && getAdapter().getCount() < getChildCount()) {
                    layoutChildren();
                }
                super.dispatchDraw(canvas);
            }
        };
    }
}
