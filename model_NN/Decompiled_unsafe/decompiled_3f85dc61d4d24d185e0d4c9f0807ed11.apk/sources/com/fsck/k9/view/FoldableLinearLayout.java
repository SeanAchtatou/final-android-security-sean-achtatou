package com.fsck.k9.view;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.fsck.k9.R;

public class FoldableLinearLayout extends LinearLayout {
    /* access modifiers changed from: private */
    public LinearLayout mFoldableContainer = null;
    private ImageView mFoldableIcon;
    private View mFoldableLayout = null;
    private TextView mFoldableTextView = null;
    private String mFoldedLabel;
    private boolean mHasMigrated = false;
    private int mIconActionCollapseId;
    private int mIconActionExpandId;
    /* access modifiers changed from: private */
    public boolean mIsFolded = true;
    private Integer mShortAnimationDuration = null;
    private String mUnFoldedLabel;

    public FoldableLinearLayout(Context context) {
        super(context);
        processAttributes(context, null);
    }

    public FoldableLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        processAttributes(context, attrs);
    }

    public FoldableLinearLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs);
        processAttributes(context, attrs);
    }

    private void processAttributes(Context context, AttributeSet attrs) {
        Resources.Theme theme = context.getTheme();
        TypedValue outValue = new TypedValue();
        if (theme.resolveAttribute(R.attr.iconActionCollapse, outValue, true)) {
            this.mIconActionCollapseId = outValue.resourceId;
        }
        if (theme.resolveAttribute(R.attr.iconActionExpand, outValue, true)) {
            this.mIconActionExpandId = outValue.resourceId;
        }
        if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.FoldableLinearLayout, 0, 0);
            this.mFoldedLabel = a.getString(0);
            this.mUnFoldedLabel = a.getString(1);
            a.recycle();
        }
        this.mFoldedLabel = this.mFoldedLabel == null ? "No text!" : this.mFoldedLabel;
        this.mUnFoldedLabel = this.mUnFoldedLabel == null ? "No text!" : this.mUnFoldedLabel;
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        if (!this.mHasMigrated) {
            migrateChildrenToContainer();
            this.mHasMigrated = true;
        }
        initialiseInnerViews();
        super.onFinishInflate();
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        boolean unused = savedState.mFolded = this.mIsFolded;
        return savedState;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable state) {
        if (state instanceof SavedState) {
            SavedState savedState = (SavedState) state;
            super.onRestoreInstanceState(savedState.getSuperState());
            this.mIsFolded = savedState.mFolded;
            updateFoldedState(this.mIsFolded, false);
            return;
        }
        super.onRestoreInstanceState(state);
    }

    static class SavedState extends View.BaseSavedState {
        static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            public SavedState createFromParcel(Parcel source) {
                return new SavedState(source);
            }

            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };
        /* access modifiers changed from: private */
        public boolean mFolded;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        private SavedState(Parcel parcel) {
            super(parcel);
            boolean z = true;
            this.mFolded = parcel.readInt() != 1 ? false : z;
        }

        private SavedState(Parcelable superState) {
            super(superState);
        }

        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            dest.writeInt(this.mFolded ? 1 : 0);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.fsck.k9.view.FoldableLinearLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void migrateChildrenToContainer() {
        int childNum = getChildCount();
        View[] children = new View[childNum];
        for (int i = 0; i < childNum; i++) {
            children[i] = getChildAt(i);
        }
        if (children[0].getId() == 2131755261) {
        }
        detachAllViewsFromParent();
        this.mFoldableLayout = ((LayoutInflater) getContext().getSystemService("layout_inflater")).inflate((int) R.layout.foldable_linearlayout, (ViewGroup) this, true);
        this.mFoldableContainer = (LinearLayout) this.mFoldableLayout.findViewById(R.id.foldableContainer);
        for (int i2 = 0; i2 < childNum; i2++) {
            addView(children[i2]);
        }
    }

    private void initialiseInnerViews() {
        this.mFoldableIcon = (ImageView) this.mFoldableLayout.findViewById(R.id.foldableIcon);
        this.mFoldableTextView = (TextView) this.mFoldableLayout.findViewById(R.id.foldableText);
        this.mFoldableTextView.setText(this.mFoldedLabel);
        this.mShortAnimationDuration = Integer.valueOf(getResources().getInteger(17694720));
        ((LinearLayout) this.mFoldableLayout.findViewById(R.id.foldableControl)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                boolean unused = FoldableLinearLayout.this.mIsFolded = !FoldableLinearLayout.this.mIsFolded;
                FoldableLinearLayout.this.updateFoldedState(FoldableLinearLayout.this.mIsFolded, true);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void updateFoldedState(boolean newStateIsFolded, boolean animate) {
        if (newStateIsFolded) {
            this.mFoldableIcon.setImageResource(this.mIconActionExpandId);
            if (animate) {
                AlphaAnimation animation = new AlphaAnimation(1.0f, 0.0f);
                animation.setDuration((long) this.mShortAnimationDuration.intValue());
                animation.setAnimationListener(new Animation.AnimationListener() {
                    public void onAnimationStart(Animation animation) {
                    }

                    public void onAnimationEnd(Animation animation) {
                        FoldableLinearLayout.this.mFoldableContainer.setVisibility(4);
                    }

                    public void onAnimationRepeat(Animation animation) {
                    }
                });
                this.mFoldableContainer.startAnimation(animation);
            } else {
                this.mFoldableContainer.setVisibility(4);
            }
            this.mFoldableTextView.setText(this.mFoldedLabel);
            return;
        }
        this.mFoldableIcon.setImageResource(this.mIconActionCollapseId);
        this.mFoldableContainer.setVisibility(0);
        if (animate) {
            AlphaAnimation animation2 = new AlphaAnimation(0.0f, 1.0f);
            animation2.setDuration((long) this.mShortAnimationDuration.intValue());
            this.mFoldableContainer.startAnimation(animation2);
        }
        this.mFoldableTextView.setText(this.mUnFoldedLabel);
    }

    public void addView(View child) {
        if (this.mFoldableContainer != null) {
            this.mFoldableContainer.addView(child);
        }
    }
}
