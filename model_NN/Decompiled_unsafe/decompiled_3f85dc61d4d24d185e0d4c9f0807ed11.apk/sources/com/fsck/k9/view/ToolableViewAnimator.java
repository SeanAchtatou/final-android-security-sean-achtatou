package com.fsck.k9.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ViewAnimator;
import com.fsck.k9.R;

public class ToolableViewAnimator extends ViewAnimator {
    private int mInitChild = -1;

    public ToolableViewAnimator(Context context) {
        super(context);
    }

    public ToolableViewAnimator(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (isInEditMode()) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ToolableViewAnimator);
            this.mInitChild = a.getInt(0, -1);
            a.recycle();
        }
    }

    public ToolableViewAnimator(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs);
        if (isInEditMode()) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ToolableViewAnimator, defStyleAttr, 0);
            this.mInitChild = a.getInt(0, -1);
            a.recycle();
        }
    }

    public void addView(@NonNull View child, int index, ViewGroup.LayoutParams params) {
        if (isInEditMode()) {
            int i = this.mInitChild;
            this.mInitChild = i - 1;
            if (i > 0) {
                return;
            }
        }
        super.addView(child, index, params);
    }

    public void setDisplayedChild(int whichChild) {
        if (whichChild != getDisplayedChild()) {
            super.setDisplayedChild(whichChild);
        }
    }

    public void setDisplayedChild(int whichChild, boolean animate) {
        if (animate) {
            setDisplayedChild(whichChild);
            return;
        }
        Animation savedInAnim = getInAnimation();
        Animation savedOutAnim = getOutAnimation();
        setInAnimation(null);
        setOutAnimation(null);
        setDisplayedChild(whichChild);
        setInAnimation(savedInAnim);
        setOutAnimation(savedOutAnim);
    }
}
