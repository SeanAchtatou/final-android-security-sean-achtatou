package com.fsck.k9.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.fsck.k9.R;

public class MessageCryptoStatusView extends FrameLayout {
    private ImageView iconCombinedFirst;
    private ImageView iconCombinedSecond;
    private ImageView iconDotsBackground;
    private ImageView iconSingle;

    public MessageCryptoStatusView(Context context) {
        super(context);
    }

    public MessageCryptoStatusView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MessageCryptoStatusView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        this.iconSingle = (ImageView) findViewById(R.id.crypto_status_single);
        this.iconCombinedFirst = (ImageView) findViewById(R.id.crypto_status_combined_1);
        this.iconCombinedSecond = (ImageView) findViewById(R.id.crypto_status_combined_2);
        this.iconDotsBackground = (ImageView) findViewById(R.id.crypto_status_dots_bg);
    }

    public void setCryptoDisplayStatus(MessageCryptoDisplayStatus displayStatus) {
        int color = ThemeUtils.getStyledColor(getContext(), displayStatus.colorAttr);
        if (displayStatus.statusDotsRes != null) {
            this.iconCombinedFirst.setVisibility(0);
            this.iconCombinedSecond.setVisibility(0);
            this.iconDotsBackground.setVisibility(0);
            this.iconSingle.setVisibility(8);
            this.iconCombinedFirst.setImageResource(displayStatus.statusIconRes);
            this.iconCombinedFirst.setColorFilter(color);
            this.iconCombinedSecond.setImageResource(displayStatus.statusDotsRes.intValue());
            this.iconCombinedSecond.setColorFilter(color);
            return;
        }
        this.iconCombinedFirst.setVisibility(8);
        this.iconCombinedSecond.setVisibility(8);
        this.iconDotsBackground.setVisibility(8);
        this.iconSingle.setVisibility(0);
        this.iconSingle.setImageResource(displayStatus.statusIconRes);
        this.iconSingle.setColorFilter(color);
    }
}
