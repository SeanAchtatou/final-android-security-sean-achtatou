package com.fsck.k9.view;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.text.style.StyleSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.QuickContactBadge;
import android.widget.TextView;
import android.widget.Toast;
import com.fsck.k9.Account;
import com.fsck.k9.FontSizes;
import com.fsck.k9.K9;
import com.fsck.k9.R;
import com.fsck.k9.activity.misc.ContactPictureLoader;
import com.fsck.k9.helper.ClipboardManager;
import com.fsck.k9.helper.ContactPicture;
import com.fsck.k9.helper.Contacts;
import com.fsck.k9.helper.MessageHelper;
import com.fsck.k9.helper.Utility;
import com.fsck.k9.mail.Address;
import com.fsck.k9.mail.Flag;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.internet.MimeUtility;
import com.fsck.k9.ui.messageview.OnCryptoClickListener;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.io.IOUtils;

public class MessageHeader extends LinearLayout implements View.OnClickListener, View.OnLongClickListener {
    private int defaultSubjectColor;
    private Account mAccount;
    private TextView mAdditionalHeadersView;
    private View mAnsweredIcon;
    private TextView mCcLabel;
    private TextView mCcView;
    private View mChip;
    private QuickContactBadge mContactBadge;
    private Contacts mContacts;
    private ContactPictureLoader mContactsPictureLoader;
    private Context mContext;
    private MessageCryptoStatusView mCryptoStatusIcon;
    private TextView mDateView;
    private CheckBox mFlagged;
    private FontSizes mFontSizes = K9.getFontSizes();
    private View mForwardedIcon;
    private TextView mFromView;
    private Message mMessage;
    private MessageHelper mMessageHelper;
    private OnLayoutChangedListener mOnLayoutChangedListener;
    private SavedState mSavedState;
    private TextView mSubjectView;
    private TextView mToLabel;
    private TextView mToView;
    private OnCryptoClickListener onCryptoClickListener;

    public interface OnLayoutChangedListener {
        void onLayoutChanged();
    }

    private static class HeaderEntry {
        public String label;
        public String value;

        public HeaderEntry(String label2, String value2) {
            this.label = label2;
            this.value = value2;
        }
    }

    public MessageHeader(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        this.mContacts = Contacts.getInstance(this.mContext);
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        this.mAnsweredIcon = findViewById(R.id.answered);
        this.mForwardedIcon = findViewById(R.id.forwarded);
        this.mFromView = (TextView) findViewById(R.id.from);
        this.mToView = (TextView) findViewById(R.id.to);
        this.mToLabel = (TextView) findViewById(R.id.to_label);
        this.mCcView = (TextView) findViewById(R.id.cc);
        this.mCcLabel = (TextView) findViewById(R.id.cc_label);
        this.mContactBadge = (QuickContactBadge) findViewById(R.id.contact_badge);
        this.mSubjectView = (TextView) findViewById(R.id.subject);
        this.mAdditionalHeadersView = (TextView) findViewById(R.id.additional_headers_view);
        this.mChip = findViewById(R.id.chip);
        this.mDateView = (TextView) findViewById(R.id.date);
        this.mFlagged = (CheckBox) findViewById(R.id.flagged);
        this.defaultSubjectColor = this.mSubjectView.getCurrentTextColor();
        this.mFontSizes.setViewTextSize(this.mSubjectView, this.mFontSizes.getMessageViewSubject());
        this.mFontSizes.setViewTextSize(this.mDateView, this.mFontSizes.getMessageViewDate());
        this.mFontSizes.setViewTextSize(this.mAdditionalHeadersView, this.mFontSizes.getMessageViewAdditionalHeaders());
        this.mFontSizes.setViewTextSize(this.mFromView, this.mFontSizes.getMessageViewSender());
        this.mFontSizes.setViewTextSize(this.mToView, this.mFontSizes.getMessageViewTo());
        this.mFontSizes.setViewTextSize(this.mToLabel, this.mFontSizes.getMessageViewTo());
        this.mFontSizes.setViewTextSize(this.mCcView, this.mFontSizes.getMessageViewCC());
        this.mFontSizes.setViewTextSize(this.mCcLabel, this.mFontSizes.getMessageViewCC());
        this.mFromView.setOnClickListener(this);
        this.mToView.setOnClickListener(this);
        this.mCcView.setOnClickListener(this);
        this.mFromView.setOnLongClickListener(this);
        this.mToView.setOnLongClickListener(this);
        this.mCcView.setOnLongClickListener(this);
        this.mCryptoStatusIcon = (MessageCryptoStatusView) findViewById(R.id.crypto_status_icon);
        this.mCryptoStatusIcon.setOnClickListener(this);
        this.mMessageHelper = MessageHelper.getInstance(this.mContext);
        hideAdditionalHeaders();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.to:
            case R.id.cc:
                expand((TextView) view, ((TextView) view).getEllipsize() != null);
                layoutChanged();
                return;
            case R.id.crypto_status_icon:
                this.onCryptoClickListener.onCryptoClick();
                return;
            case R.id.from:
                onAddSenderToContacts();
                return;
            default:
                return;
        }
    }

    public boolean onLongClick(View view) {
        switch (view.getId()) {
            case R.id.to:
                onAddRecipientsToClipboard(Message.RecipientType.TO);
                return true;
            case R.id.cc:
                onAddRecipientsToClipboard(Message.RecipientType.CC);
                return true;
            case R.id.from:
                onAddAddressesToClipboard(this.mMessage.getFrom());
                return true;
            default:
                return true;
        }
    }

    private void onAddSenderToContacts() {
        if (this.mMessage != null) {
            try {
                this.mContacts.createContact(this.mMessage.getFrom()[0]);
            } catch (Exception e) {
                Log.e("k9", "Couldn't create contact", e);
            }
        }
    }

    public String createMessage(int addressesCount) {
        return this.mContext.getResources().getQuantityString(exts.whats.R.dimen.abc_action_bar_subtitle_top_margin, addressesCount);
    }

    private void onAddAddressesToClipboard(Address[] addresses) {
        ClipboardManager.getInstance(this.mContext).setText("addresses", Address.toString(addresses));
        Toast.makeText(this.mContext, createMessage(addresses.length), 1).show();
    }

    private void onAddRecipientsToClipboard(Message.RecipientType recipientType) {
        onAddAddressesToClipboard(this.mMessage.getRecipients(recipientType));
    }

    public void setOnFlagListener(View.OnClickListener listener) {
        this.mFlagged.setOnClickListener(listener);
    }

    public boolean additionalHeadersVisible() {
        return this.mAdditionalHeadersView != null && this.mAdditionalHeadersView.getVisibility() == 0;
    }

    private void hideAdditionalHeaders() {
        this.mAdditionalHeadersView.setVisibility(8);
        this.mAdditionalHeadersView.setText("");
    }

    private void showAdditionalHeaders() {
        Integer messageToShow = null;
        try {
            List<HeaderEntry> additionalHeaders = getAdditionalHeaders(this.mMessage);
            if (!additionalHeaders.isEmpty()) {
                populateAdditionalHeadersView(additionalHeaders);
                this.mAdditionalHeadersView.setVisibility(0);
            } else {
                messageToShow = Integer.valueOf((int) R.string.message_no_additional_headers_available);
            }
        } catch (Exception e) {
            messageToShow = Integer.valueOf((int) R.string.message_additional_headers_retrieval_failed);
        }
        if (messageToShow != null) {
            Toast toast = Toast.makeText(this.mContext, messageToShow.intValue(), 1);
            toast.setGravity(17, 0, 0);
            toast.show();
        }
    }

    public void populate(Message message, Account account) {
        Contacts contacts = K9.showContactName() ? this.mContacts : null;
        CharSequence from = MessageHelper.toFriendly(message.getFrom(), contacts);
        CharSequence to = MessageHelper.toFriendly(message.getRecipients(Message.RecipientType.TO), contacts);
        CharSequence cc = MessageHelper.toFriendly(message.getRecipients(Message.RecipientType.CC), contacts);
        Address[] fromAddrs = message.getFrom();
        Address[] toAddrs = message.getRecipients(Message.RecipientType.TO);
        Address[] ccAddrs = message.getRecipients(Message.RecipientType.CC);
        Address counterpartyAddress = null;
        if (this.mMessageHelper.toMe(account, fromAddrs)) {
            if (toAddrs.length > 0) {
                counterpartyAddress = toAddrs[0];
            } else if (ccAddrs.length > 0) {
                counterpartyAddress = ccAddrs[0];
            }
        } else if (fromAddrs.length > 0) {
            counterpartyAddress = fromAddrs[0];
        }
        if (this.mMessage == null || this.mMessage.getId() != message.getId()) {
        }
        this.mMessage = message;
        this.mAccount = account;
        if (K9.showContactPicture()) {
            this.mContactBadge.setVisibility(0);
            this.mContactsPictureLoader = ContactPicture.getContactPictureLoader(this.mContext);
        } else {
            this.mContactBadge.setVisibility(8);
        }
        String subject = message.getSubject();
        if (TextUtils.isEmpty(subject)) {
            this.mSubjectView.setText(this.mContext.getText(R.string.general_no_subject));
        } else {
            this.mSubjectView.setText(subject);
        }
        this.mSubjectView.setTextColor(-16777216 | this.defaultSubjectColor);
        this.mDateView.setText(DateUtils.formatDateTime(this.mContext, message.getSentDate().getTime(), 524309));
        if (K9.showContactPicture()) {
            if (counterpartyAddress != null) {
                Utility.setContactForBadge(this.mContactBadge, counterpartyAddress);
                this.mContactsPictureLoader.loadContactPicture(counterpartyAddress, this.mContactBadge);
            } else {
                this.mContactBadge.setImageResource(R.drawable.ic_contact_picture);
            }
        }
        this.mFromView.setText(from);
        updateAddressField(this.mToView, to, this.mToLabel);
        updateAddressField(this.mCcView, cc, this.mCcLabel);
        this.mAnsweredIcon.setVisibility(message.isSet(Flag.ANSWERED) ? 0 : 8);
        this.mForwardedIcon.setVisibility(message.isSet(Flag.FORWARDED) ? 0 : 8);
        this.mFlagged.setChecked(message.isSet(Flag.FLAGGED));
        this.mChip.setBackgroundColor(this.mAccount.getChipColor());
        setVisibility(0);
        if (this.mSavedState != null) {
            if (this.mSavedState.additionalHeadersVisible) {
                showAdditionalHeaders();
            }
            this.mSavedState = null;
            return;
        }
        hideAdditionalHeaders();
    }

    public void hideCryptoStatus() {
        this.mCryptoStatusIcon.setVisibility(8);
    }

    public void setCryptoStatusLoading() {
        this.mCryptoStatusIcon.setVisibility(0);
        this.mCryptoStatusIcon.setEnabled(false);
        this.mCryptoStatusIcon.setCryptoDisplayStatus(MessageCryptoDisplayStatus.LOADING);
    }

    public void setCryptoStatusDisabled() {
        this.mCryptoStatusIcon.setVisibility(0);
        this.mCryptoStatusIcon.setEnabled(false);
        this.mCryptoStatusIcon.setCryptoDisplayStatus(MessageCryptoDisplayStatus.DISABLED);
    }

    public void setCryptoStatus(MessageCryptoDisplayStatus displayStatus) {
        this.mCryptoStatusIcon.setVisibility(0);
        this.mCryptoStatusIcon.setEnabled(true);
        this.mCryptoStatusIcon.setCryptoDisplayStatus(displayStatus);
    }

    public void onShowAdditionalHeaders() {
        if (this.mAdditionalHeadersView.getVisibility() == 0) {
            hideAdditionalHeaders();
            expand(this.mToView, false);
            expand(this.mCcView, false);
        } else {
            showAdditionalHeaders();
            expand(this.mToView, true);
            expand(this.mCcView, true);
        }
        layoutChanged();
    }

    private void updateAddressField(TextView v, CharSequence text, View label) {
        boolean hasText;
        int i;
        int i2 = 0;
        if (!TextUtils.isEmpty(text)) {
            hasText = true;
        } else {
            hasText = false;
        }
        v.setText(text);
        if (hasText) {
            i = 0;
        } else {
            i = 8;
        }
        v.setVisibility(i);
        if (!hasText) {
            i2 = 8;
        }
        label.setVisibility(i2);
    }

    private void expand(TextView v, boolean expand) {
        if (expand) {
            v.setMaxLines(Integer.MAX_VALUE);
            v.setEllipsize(null);
            return;
        }
        v.setMaxLines(2);
        v.setEllipsize(TextUtils.TruncateAt.END);
    }

    private List<HeaderEntry> getAdditionalHeaders(Message message) throws MessagingException {
        List<HeaderEntry> additionalHeaders = new LinkedList<>();
        for (String headerName : new LinkedHashSet<>(message.getHeaderNames())) {
            for (String headerValue : message.getHeader(headerName)) {
                additionalHeaders.add(new HeaderEntry(headerName, headerValue));
            }
        }
        return additionalHeaders;
    }

    private void populateAdditionalHeadersView(List<HeaderEntry> additionalHeaders) {
        SpannableStringBuilder sb = new SpannableStringBuilder();
        boolean first = true;
        for (HeaderEntry additionalHeader : additionalHeaders) {
            if (!first) {
                sb.append((CharSequence) IOUtils.LINE_SEPARATOR_UNIX);
            } else {
                first = false;
            }
            StyleSpan boldSpan = new StyleSpan(1);
            SpannableString label = new SpannableString(additionalHeader.label + ": ");
            label.setSpan(boldSpan, 0, label.length(), 0);
            sb.append((CharSequence) label);
            sb.append((CharSequence) MimeUtility.unfoldAndDecode(additionalHeader.value));
        }
        this.mAdditionalHeadersView.setText(sb);
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.additionalHeadersVisible = additionalHeadersVisible();
        return savedState;
    }

    public void onRestoreInstanceState(Parcelable state) {
        if (!(state instanceof SavedState)) {
            super.onRestoreInstanceState(state);
            return;
        }
        SavedState savedState = (SavedState) state;
        super.onRestoreInstanceState(savedState.getSuperState());
        this.mSavedState = savedState;
    }

    static class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }

            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };
        boolean additionalHeadersVisible;

        SavedState(Parcelable superState) {
            super(superState);
        }

        private SavedState(Parcel in) {
            super(in);
            this.additionalHeadersVisible = in.readInt() != 0;
        }

        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeInt(this.additionalHeadersVisible ? 1 : 0);
        }
    }

    public void setOnLayoutChangedListener(OnLayoutChangedListener listener) {
        this.mOnLayoutChangedListener = listener;
    }

    private void layoutChanged() {
        if (this.mOnLayoutChangedListener != null) {
            this.mOnLayoutChangedListener.onLayoutChanged();
        }
    }

    public void showSubjectLine() {
        this.mSubjectView.setVisibility(0);
    }

    public void setOnCryptoClickListener(OnCryptoClickListener onCryptoClickListener2) {
        this.onCryptoClickListener = onCryptoClickListener2;
    }
}
