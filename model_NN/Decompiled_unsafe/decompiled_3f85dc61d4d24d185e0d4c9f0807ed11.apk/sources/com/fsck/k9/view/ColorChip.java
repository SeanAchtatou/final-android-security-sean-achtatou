package com.fsck.k9.view;

import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.PathShape;

public class ColorChip {
    public static final Path CIRCULAR = new Path();
    public static final Path LEFT_POINTING = new Path();
    public static final Path RIGHT_NOTCH = new Path();
    public static final Path RIGHT_POINTING = new Path();
    public static final Path STAR = new Path();
    private ShapeDrawable mDrawable;

    static {
        CIRCULAR.addCircle(160.0f, 160.0f, 70.0f, Path.Direction.CW);
        CIRCULAR.close();
        RIGHT_POINTING.addArc(new RectF(80.0f, 80.0f, 240.0f, 240.0f), 90.0f, 180.0f);
        RIGHT_POINTING.arcTo(new RectF(160.0f, 0.0f, 320.0f, 160.0f), 180.0f, -90.0f);
        RIGHT_POINTING.arcTo(new RectF(160.0f, 160.0f, 320.0f, 320.0f), 270.0f, -90.0f);
        RIGHT_POINTING.close();
        RIGHT_NOTCH.addArc(new RectF(80.0f, 80.0f, 240.0f, 240.0f), 90.0f, 180.0f);
        RIGHT_NOTCH.arcTo(new RectF(160.0f, 0.0f, 320.0f, 160.0f), 180.0f, -90.0f);
        RIGHT_NOTCH.arcTo(new RectF(160.0f, 160.0f, 320.0f, 320.0f), 270.0f, -90.0f);
        RIGHT_NOTCH.close();
        LEFT_POINTING.addArc(new RectF(80.0f, 80.0f, 240.0f, 240.0f), 90.0f, -180.0f);
        LEFT_POINTING.arcTo(new RectF(0.0f, 0.0f, 160.0f, 160.0f), 0.0f, 90.0f);
        LEFT_POINTING.arcTo(new RectF(0.0f, 160.0f, 160.0f, 320.0f), 270.0f, 90.0f);
        LEFT_POINTING.close();
        STAR.moveTo(140.0f, 60.0f);
        STAR.lineTo(170.0f, 110.0f);
        STAR.lineTo(220.0f, 120.0f);
        STAR.lineTo(180.0f, 160.0f);
        STAR.lineTo(200.0f, 220.0f);
        STAR.lineTo(140.0f, 190.0f);
        STAR.lineTo(80.0f, 220.0f);
        STAR.lineTo(100.0f, 160.0f);
        STAR.lineTo(60.0f, 120.0f);
        STAR.lineTo(110.0f, 110.0f);
        STAR.lineTo(140.0f, 60.0f);
        STAR.close();
    }

    public ColorChip(int color, boolean messageRead, Path shape) {
        if (shape.equals(STAR)) {
            this.mDrawable = new ShapeDrawable(new PathShape(shape, 280.0f, 280.0f));
        } else {
            this.mDrawable = new ShapeDrawable(new PathShape(shape, 320.0f, 320.0f));
        }
        if (messageRead) {
            this.mDrawable.getPaint().setStyle(Paint.Style.STROKE);
        } else {
            this.mDrawable.getPaint().setStyle(Paint.Style.FILL_AND_STROKE);
        }
        this.mDrawable.getPaint().setStrokeWidth(20.0f);
        this.mDrawable.getPaint().setColor(color);
    }

    public ShapeDrawable drawable() {
        return this.mDrawable;
    }
}
