package com.fsck.k9.view;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import com.fsck.k9.R;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;

public class HighlightDialogFragment extends DialogFragment {
    public static final String ARG_HIGHLIGHT_VIEW = "highlighted_view";
    public static final float BACKGROUND_DIM_AMOUNT = 0.25f;
    private ShowcaseView showcaseView;

    /* access modifiers changed from: protected */
    public void highlightViewInBackground() {
        if (getArguments().containsKey(ARG_HIGHLIGHT_VIEW)) {
            Activity activity = getActivity();
            if (activity == null) {
                throw new IllegalStateException("fragment must be attached to set highlight!");
            }
            if (!(this.showcaseView != null && this.showcaseView.isShowing())) {
                this.showcaseView = new ShowcaseView.Builder(activity).setTarget(new ViewTarget(getArguments().getInt(ARG_HIGHLIGHT_VIEW), activity)).hideOnTouchOutside().blockAllTouches().withMaterialShowcase().setStyle(R.style.ShowcaseTheme).build();
                this.showcaseView.hideButton();
            }
        }
    }

    public void onStart() {
        super.onStart();
        hideKeyboard();
        highlightViewInBackground();
        setDialogBackgroundDim();
    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        hideShowcaseView();
    }

    private void setDialogBackgroundDim() {
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setDimAmount(0.25f);
        }
    }

    private void hideKeyboard() {
        View v;
        Activity activity = getActivity();
        if (activity != null && (v = activity.getCurrentFocus()) != null) {
            ((InputMethodManager) activity.getSystemService("input_method")).hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
    }

    private void hideShowcaseView() {
        if (this.showcaseView != null && this.showcaseView.isShowing()) {
            this.showcaseView.hide();
        }
        this.showcaseView = null;
    }
}
