package com.fsck.k9.view;

import android.annotation.SuppressLint;
import android.app.LoaderManager;
import android.content.Context;
import android.content.Loader;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.ListPopupWindow;
import android.widget.TextView;
import com.fsck.k9.R;
import com.fsck.k9.activity.AlternateRecipientAdapter;
import com.fsck.k9.activity.compose.RecipientAdapter;
import com.fsck.k9.activity.compose.RecipientLoader;
import com.fsck.k9.mail.Address;
import com.tokenautocomplete.TokenCompleteTextView;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.List;
import org.apache.james.mime4j.util.CharsetUtil;

public class RecipientSelectView extends TokenCompleteTextView<Recipient> implements LoaderManager.LoaderCallbacks<List<Recipient>>, AlternateRecipientAdapter.AlternateRecipientListener {
    private static final String ARG_QUERY = "query";
    private static final int LOADER_ID_ALTERNATES = 1;
    private static final int LOADER_ID_FILTERING = 0;
    private static final int MINIMUM_LENGTH_FOR_FILTERING = 2;
    private RecipientAdapter adapter;
    private AlternateRecipientAdapter alternatesAdapter;
    private ListPopupWindow alternatesPopup;
    private Recipient alternatesPopupRecipient;
    @Nullable
    private String cryptoProvider;
    private TokenListener<Recipient> listener;
    @Nullable
    private LoaderManager loaderManager;

    public interface TokenListener<T> extends TokenCompleteTextView.TokenListener<T> {
        void onTokenChanged(T t);
    }

    public /* bridge */ /* synthetic */ void onLoadFinished(Loader loader, Object obj) {
        onLoadFinished((Loader<List<Recipient>>) loader, (List<Recipient>) ((List) obj));
    }

    public RecipientSelectView(Context context) {
        super(context);
        initView(context);
    }

    public RecipientSelectView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public RecipientSelectView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initView(context);
    }

    private void initView(Context context) {
        this.alternatesPopup = new ListPopupWindow(context);
        this.alternatesAdapter = new AlternateRecipientAdapter(context, this);
        this.alternatesPopup.setAdapter(this.alternatesAdapter);
        allowDuplicates(false);
        performBestGuess(true);
        this.adapter = new RecipientAdapter(context);
        setAdapter(this.adapter);
        setLongClickable(true);
    }

    /* access modifiers changed from: protected */
    public View getViewForObject(Recipient recipient) {
        View view = inflateLayout();
        view.setTag(new RecipientTokenViewHolder(view));
        bindObjectView(recipient, view);
        return view;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    @SuppressLint({"InflateParams"})
    private View inflateLayout() {
        return LayoutInflater.from(getContext()).inflate((int) R.layout.recipient_token_item, (ViewGroup) null, false);
    }

    private void bindObjectView(Recipient recipient, View view) {
        boolean hasCryptoProvider;
        RecipientTokenViewHolder holder = (RecipientTokenViewHolder) view.getTag();
        holder.vName.setText(recipient.getDisplayNameOrAddress());
        RecipientAdapter.setContactPhotoOrPlaceholder(getContext(), holder.vContactPhoto, recipient);
        if (this.cryptoProvider != null) {
            hasCryptoProvider = true;
        } else {
            hasCryptoProvider = false;
        }
        if (!hasCryptoProvider) {
            holder.cryptoStatusRed.setVisibility(8);
            holder.cryptoStatusOrange.setVisibility(8);
            holder.cryptoStatusGreen.setVisibility(8);
        } else if (recipient.cryptoStatus == RecipientCryptoStatus.UNAVAILABLE) {
            holder.cryptoStatusRed.setVisibility(0);
            holder.cryptoStatusOrange.setVisibility(8);
            holder.cryptoStatusGreen.setVisibility(8);
        } else if (recipient.cryptoStatus == RecipientCryptoStatus.AVAILABLE_UNTRUSTED) {
            holder.cryptoStatusRed.setVisibility(8);
            holder.cryptoStatusOrange.setVisibility(0);
            holder.cryptoStatusGreen.setVisibility(8);
        } else if (recipient.cryptoStatus == RecipientCryptoStatus.AVAILABLE_TRUSTED) {
            holder.cryptoStatusRed.setVisibility(8);
            holder.cryptoStatusOrange.setVisibility(8);
            holder.cryptoStatusGreen.setVisibility(0);
        }
    }

    public boolean onTouchEvent(@NonNull MotionEvent event) {
        int offset;
        int action = event.getActionMasked();
        Editable text = getText();
        if (!(text == null || action != 1 || (offset = getOffsetForPosition(event.getX(), event.getY())) == -1)) {
            TokenCompleteTextView<Recipient>.TokenImageSpan[] links = (TokenCompleteTextView.TokenImageSpan[]) text.getSpans(offset, offset, RecipientTokenSpan.class);
            if (links.length > 0) {
                showAlternates((Recipient) links[0].getToken());
                return true;
            }
        }
        return super.onTouchEvent(event);
    }

    /* access modifiers changed from: protected */
    public Recipient defaultObject(String completionText) {
        Address[] parsedAddresses = Address.parse(completionText);
        if (!CharsetUtil.isASCII(completionText)) {
            setError(getContext().getString(R.string.recipient_error_non_ascii));
            return null;
        } else if (parsedAddresses.length != 0 && parsedAddresses[0].getAddress() != null) {
            return new Recipient(parsedAddresses[0]);
        } else {
            setError(getContext().getString(R.string.recipient_error_parse_failed));
            return null;
        }
    }

    public boolean isEmpty() {
        return getObjects().isEmpty();
    }

    public void setLoaderManager(@Nullable LoaderManager loaderManager2) {
        this.loaderManager = loaderManager2;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.loaderManager != null) {
            this.loaderManager.destroyLoader(1);
            this.loaderManager.destroyLoader(0);
            this.loaderManager = null;
        }
    }

    public void onFocusChanged(boolean hasFocus, int direction, Rect previous) {
        super.onFocusChanged(hasFocus, direction, previous);
        if (hasFocus) {
            displayKeyboard();
        }
    }

    private void displayKeyboard() {
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService("input_method");
        if (imm != null) {
            imm.showSoftInput(this, 1);
        }
    }

    public void showDropDown() {
        if (this.adapter != null) {
            super.showDropDown();
        }
    }

    public void performCompletion() {
        Object defaultObject;
        if (getListSelection() != -1 || !enoughToFilter()) {
            super.performCompletion();
            return;
        }
        if (getAdapter().getCount() > 0) {
            defaultObject = getAdapter().getItem(0);
        } else {
            defaultObject = defaultObject(currentCompletionText());
        }
        if (defaultObject != null) {
            replaceText(convertSelectionToString(defaultObject));
        }
    }

    /* access modifiers changed from: protected */
    public void performFiltering(@NonNull CharSequence text, int start, int end, int keyCode) {
        if (this.loaderManager != null) {
            String query = text.subSequence(start, end).toString();
            if (TextUtils.isEmpty(query) || query.length() < 2) {
                this.loaderManager.destroyLoader(0);
                return;
            }
            Bundle args = new Bundle();
            args.putString(ARG_QUERY, query);
            this.loaderManager.restartLoader(0, args, this);
        }
    }

    public void setCryptoProvider(@Nullable String cryptoProvider2) {
        this.cryptoProvider = cryptoProvider2;
    }

    public void addRecipients(Recipient... recipients) {
        for (Recipient recipient : recipients) {
            addObject(recipient);
        }
    }

    public Address[] getAddresses() {
        List<Recipient> recipients = getObjects();
        Address[] address = new Address[recipients.size()];
        for (int i = 0; i < address.length; i++) {
            address[i] = ((Recipient) recipients.get(i)).address;
        }
        return address;
    }

    private void showAlternates(Recipient recipient) {
        if (this.loaderManager != null) {
            ((InputMethodManager) getContext().getSystemService("input_method")).hideSoftInputFromWindow(getWindowToken(), 0);
            this.alternatesPopupRecipient = recipient;
            this.loaderManager.restartLoader(1, null, this);
        }
    }

    public void postShowAlternatesPopup(final List<Recipient> data) {
        new Handler().post(new Runnable() {
            public void run() {
                RecipientSelectView.this.showAlternatesPopup(data);
            }
        });
    }

    public void showAlternatesPopup(List<Recipient> data) {
        if (this.loaderManager != null) {
            this.alternatesPopup.setAnchorView(getRootView().findViewById(getDropDownAnchor()));
            this.alternatesPopup.setWidth(getDropDownWidth());
            this.alternatesAdapter.setCurrentRecipient(this.alternatesPopupRecipient);
            this.alternatesAdapter.setAlternateRecipientInfo(data);
            this.alternatesPopup.show();
            this.alternatesPopup.getListView().setChoiceMode(1);
        }
    }

    public Loader<List<Recipient>> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case 0:
                String query = (args == null || !args.containsKey(ARG_QUERY)) ? "" : args.getString(ARG_QUERY);
                this.adapter.setHighlight(query);
                return new RecipientLoader(getContext(), this.cryptoProvider, query);
            case 1:
                Uri contactLookupUri = this.alternatesPopupRecipient.getContactLookupUri();
                if (contactLookupUri != null) {
                    return new RecipientLoader(getContext(), this.cryptoProvider, contactLookupUri, true);
                }
                return new RecipientLoader(getContext(), this.cryptoProvider, this.alternatesPopupRecipient.address);
            default:
                throw new IllegalStateException("Unknown Loader ID: " + id);
        }
    }

    public void onLoadFinished(Loader<List<Recipient>> loader, List<Recipient> data) {
        if (this.loaderManager != null) {
            switch (loader.getId()) {
                case 0:
                    this.adapter.setRecipients(data);
                    return;
                case 1:
                    postShowAlternatesPopup(data);
                    this.loaderManager.destroyLoader(1);
                    return;
                default:
                    return;
            }
        }
    }

    public void onLoaderReset(Loader<List<Recipient>> loader) {
        if (loader.getId() == 0) {
            this.adapter.setHighlight(null);
            this.adapter.setRecipients(null);
        }
    }

    public boolean tryPerformCompletion() {
        if (!hasUncompletedText()) {
            return false;
        }
        int previousNumRecipients = getTokenCount();
        performCompletion();
        if (previousNumRecipients != getTokenCount()) {
            return true;
        }
        return false;
    }

    private int getTokenCount() {
        return getObjects().size();
    }

    public boolean hasUncompletedText() {
        String currentCompletionText = currentCompletionText();
        return !TextUtils.isEmpty(currentCompletionText) && !isPlaceholderText(currentCompletionText);
    }

    private static boolean isPlaceholderText(String currentCompletionText) {
        return currentCompletionText.startsWith("+") && currentCompletionText.substring(1).matches("[0-9]+");
    }

    public void onRecipientRemove(Recipient currentRecipient) {
        this.alternatesPopup.dismiss();
        removeObject(currentRecipient);
    }

    public void onRecipientChange(Recipient currentRecipient, Recipient alternateAddress) {
        this.alternatesPopup.dismiss();
        currentRecipient.address = alternateAddress.address;
        currentRecipient.addressLabel = alternateAddress.addressLabel;
        RecipientCryptoStatus unused = currentRecipient.cryptoStatus = alternateAddress.cryptoStatus;
        View recipientTokenView = getTokenViewForRecipient(currentRecipient);
        if (recipientTokenView == null) {
            Log.e("k9", "Tried to refresh invalid view token!");
            return;
        }
        bindObjectView(currentRecipient, recipientTokenView);
        if (this.listener != null) {
            this.listener.onTokenChanged(currentRecipient);
        }
        invalidate();
    }

    /* access modifiers changed from: protected */
    public TokenCompleteTextView<Recipient>.TokenImageSpan buildSpanForObject(Recipient obj) {
        if (obj == null) {
            return null;
        }
        return new RecipientTokenSpan(getViewForObject(obj), obj, (int) maxTextWidth());
    }

    private View getTokenViewForRecipient(Recipient currentRecipient) {
        Editable text = getText();
        if (text == null) {
            return null;
        }
        for (RecipientTokenSpan recipientSpan : (RecipientTokenSpan[]) text.getSpans(0, text.length(), RecipientTokenSpan.class)) {
            if (((Recipient) recipientSpan.getToken()).equals(currentRecipient)) {
                return recipientSpan.view;
            }
        }
        return null;
    }

    public void setTokenListener(TokenListener<Recipient> listener2) {
        super.setTokenListener((TokenCompleteTextView.TokenListener) listener2);
        this.listener = listener2;
    }

    public enum RecipientCryptoStatus {
        UNDEFINED,
        UNAVAILABLE,
        AVAILABLE_UNTRUSTED,
        AVAILABLE_TRUSTED;

        public boolean isAvailable() {
            return this == AVAILABLE_TRUSTED || this == AVAILABLE_UNTRUSTED;
        }
    }

    private class RecipientTokenSpan extends TokenCompleteTextView<Recipient>.TokenImageSpan {
        /* access modifiers changed from: private */
        public final View view;

        public RecipientTokenSpan(View view2, Recipient recipient, int token) {
            super(view2, recipient, token);
            this.view = view2;
        }
    }

    private static class RecipientTokenViewHolder {
        public final View cryptoStatusGreen;
        public final View cryptoStatusOrange;
        public final View cryptoStatusRed;
        public final ImageView vContactPhoto;
        public final TextView vName;

        RecipientTokenViewHolder(View view) {
            this.vName = (TextView) view.findViewById(16908308);
            this.vContactPhoto = (ImageView) view.findViewById(R.id.contact_photo);
            this.cryptoStatusRed = view.findViewById(R.id.contact_crypto_status_red);
            this.cryptoStatusOrange = view.findViewById(R.id.contact_crypto_status_orange);
            this.cryptoStatusGreen = view.findViewById(R.id.contact_crypto_status_green);
        }
    }

    public static class Recipient implements Serializable {
        @NonNull
        public Address address;
        public String addressLabel;
        @Nullable
        public final Long contactId;
        public final String contactLookupKey;
        /* access modifiers changed from: private */
        @NonNull
        public RecipientCryptoStatus cryptoStatus;
        @Nullable
        public transient Uri photoThumbnailUri;

        public Recipient(@NonNull Address address2) {
            this.address = address2;
            this.contactId = null;
            this.cryptoStatus = RecipientCryptoStatus.UNDEFINED;
            this.contactLookupKey = null;
        }

        public Recipient(String name, String email, String addressLabel2, long contactId2, String lookupKey) {
            this.address = new Address(email, name);
            this.contactId = Long.valueOf(contactId2);
            this.addressLabel = addressLabel2;
            this.cryptoStatus = RecipientCryptoStatus.UNDEFINED;
            this.contactLookupKey = lookupKey;
        }

        public String getDisplayNameOrAddress() {
            String displayName = getDisplayName();
            return displayName != null ? displayName : this.address.getAddress();
        }

        public boolean isValidEmailAddress() {
            return this.address.getAddress() != null;
        }

        public String getDisplayNameOrUnknown(Context context) {
            String displayName = getDisplayName();
            return displayName != null ? displayName : context.getString(R.string.unknown_recipient);
        }

        public String getNameOrUnknown(Context context) {
            String name = this.address.getPersonal();
            return name != null ? name : context.getString(R.string.unknown_recipient);
        }

        private String getDisplayName() {
            if (TextUtils.isEmpty(this.address.getPersonal())) {
                return null;
            }
            String displayName = this.address.getPersonal();
            if (this.addressLabel != null) {
                return displayName + " (" + this.addressLabel + ")";
            }
            return displayName;
        }

        @NonNull
        public RecipientCryptoStatus getCryptoStatus() {
            return this.cryptoStatus;
        }

        public void setCryptoStatus(@NonNull RecipientCryptoStatus cryptoStatus2) {
            this.cryptoStatus = cryptoStatus2;
        }

        @Nullable
        public Uri getContactLookupUri() {
            if (this.contactId == null) {
                return null;
            }
            return ContactsContract.Contacts.getLookupUri(this.contactId.longValue(), this.contactLookupKey);
        }

        public boolean equals(Object o) {
            return (o instanceof Recipient) && this.address.equals(((Recipient) o).address);
        }

        private void writeObject(ObjectOutputStream oos) throws IOException {
            oos.defaultWriteObject();
            if (this.photoThumbnailUri != null) {
                oos.writeInt(1);
                oos.writeUTF(this.photoThumbnailUri.toString());
                return;
            }
            oos.writeInt(0);
        }

        private void readObject(ObjectInputStream ois) throws ClassNotFoundException, IOException {
            ois.defaultReadObject();
            if (ois.readInt() != 0) {
                this.photoThumbnailUri = Uri.parse(ois.readUTF());
            }
        }
    }
}
