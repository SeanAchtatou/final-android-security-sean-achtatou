package com.fsck.k9.view;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.webkit.WebView;
import com.fsck.k9.Clock;
import com.fsck.k9.K9;
import com.fsck.k9.Throttle;
import com.fsck.k9.helper.Utility;

public class RigidWebView extends WebView {
    private static final int MAX_RESIZE_INTERVAL = 300;
    private static final int MIN_RESIZE_INTERVAL = 200;
    private static final boolean NO_THROTTLE = (Build.VERSION.SDK_INT >= 21);
    private final Clock mClock = Clock.INSTANCE;
    private boolean mIgnoreNext;
    private long mLastSizeChangeTime = -1;
    private int mRealHeight;
    private int mRealWidth;
    private final Throttle mThrottle = new Throttle(getClass().getName(), new Runnable() {
        public void run() {
            RigidWebView.this.performSizeChangeDelayed();
        }
    }, Utility.getMainThreadHandler(), 200, MAX_RESIZE_INTERVAL);

    public RigidWebView(Context context) {
        super(context);
    }

    public RigidWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RigidWebView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int ow, int oh) {
        boolean recentlySized;
        if (NO_THROTTLE) {
            super.onSizeChanged(w, h, ow, oh);
            return;
        }
        this.mRealWidth = w;
        this.mRealHeight = h;
        if (this.mClock.getTime() - this.mLastSizeChangeTime < 200) {
            recentlySized = true;
        } else {
            recentlySized = false;
        }
        if (this.mIgnoreNext) {
            this.mIgnoreNext = false;
            if (recentlySized) {
                if (K9.DEBUG) {
                    Log.w("k9", "Supressing size change in RigidWebView");
                    return;
                }
                return;
            }
        }
        if (recentlySized) {
            this.mThrottle.onEvent();
        } else {
            performSizeChange(ow, oh);
        }
    }

    private void performSizeChange(int ow, int oh) {
        super.onSizeChanged(this.mRealWidth, this.mRealHeight, ow, oh);
        this.mLastSizeChangeTime = this.mClock.getTime();
    }

    /* access modifiers changed from: private */
    public void performSizeChangeDelayed() {
        this.mIgnoreNext = true;
        performSizeChange(getWidth(), getHeight());
    }
}
