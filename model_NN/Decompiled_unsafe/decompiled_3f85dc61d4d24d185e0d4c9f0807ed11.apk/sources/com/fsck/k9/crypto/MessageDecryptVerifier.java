package com.fsck.k9.crypto;

import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.fsck.k9.mail.Body;
import com.fsck.k9.mail.BodyPart;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.Multipart;
import com.fsck.k9.mail.Part;
import com.fsck.k9.mail.internet.MessageExtractor;
import com.fsck.k9.mail.internet.MimeBodyPart;
import com.fsck.k9.mail.internet.MimeUtility;
import com.fsck.k9.ui.crypto.MessageCryptoAnnotations;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class MessageDecryptVerifier {
    private static final String APPLICATION_PGP = "application/pgp";
    private static final String APPLICATION_PGP_ENCRYPTED = "application/pgp-encrypted";
    private static final String APPLICATION_PGP_SIGNATURE = "application/pgp-signature";
    private static final String MULTIPART_ENCRYPTED = "multipart/encrypted";
    private static final String MULTIPART_SIGNED = "multipart/signed";
    public static final String PGP_INLINE_SIGNED_START_MARKER = "-----BEGIN PGP SIGNED MESSAGE-----";
    public static final String PGP_INLINE_START_MARKER = "-----BEGIN PGP MESSAGE-----";
    private static final String PROTOCOL_PARAMETER = "protocol";
    public static final int TEXT_LENGTH_FOR_INLINE_CHECK = 36;
    private static final String TEXT_PLAIN = "text/plain";

    public static Part findPrimaryEncryptedOrSignedPart(Part part, List<Part> outputExtraParts) {
        if (isPartEncryptedOrSigned(part)) {
            return part;
        }
        Body body = part.getBody();
        if (part.isMimeType("multipart/mixed") && (body instanceof Multipart)) {
            Multipart multipart = (Multipart) body;
            Part firstBodyPart = multipart.getBodyPart(0);
            if (isPartEncryptedOrSigned(firstBodyPart)) {
                if (outputExtraParts != null) {
                    for (int i = 1; i < multipart.getCount(); i++) {
                        outputExtraParts.add(multipart.getBodyPart(i));
                    }
                }
                return firstBodyPart;
            }
        }
        return null;
    }

    public static List<Part> findEncryptedParts(Part startPart) {
        List<Part> encryptedParts = new ArrayList<>();
        Stack<Part> partsToCheck = new Stack<>();
        partsToCheck.push(startPart);
        while (!partsToCheck.isEmpty()) {
            Part part = (Part) partsToCheck.pop();
            Body body = part.getBody();
            if (isPartMultipartEncrypted(part)) {
                encryptedParts.add(part);
            } else if (body instanceof Multipart) {
                Multipart multipart = (Multipart) body;
                for (int i = multipart.getCount() - 1; i >= 0; i--) {
                    partsToCheck.push(multipart.getBodyPart(i));
                }
            }
        }
        return encryptedParts;
    }

    public static List<Part> findSignedParts(Part startPart, MessageCryptoAnnotations messageCryptoAnnotations) {
        MimeBodyPart replacementData;
        List<Part> signedParts = new ArrayList<>();
        Stack<Part> partsToCheck = new Stack<>();
        partsToCheck.push(startPart);
        while (!partsToCheck.isEmpty()) {
            Part part = (Part) partsToCheck.pop();
            if (messageCryptoAnnotations.has(part) && (replacementData = messageCryptoAnnotations.get(part).getReplacementData()) != null) {
                part = replacementData;
            }
            Body body = part.getBody();
            if (isPartMultipartSigned(part)) {
                signedParts.add(part);
            } else if (body instanceof Multipart) {
                Multipart multipart = (Multipart) body;
                for (int i = multipart.getCount() - 1; i >= 0; i--) {
                    partsToCheck.push(multipart.getBodyPart(i));
                }
            }
        }
        return signedParts;
    }

    public static List<Part> findPgpInlineParts(Part startPart) {
        List<Part> inlineParts = new ArrayList<>();
        Stack<Part> partsToCheck = new Stack<>();
        partsToCheck.push(startPart);
        while (!partsToCheck.isEmpty()) {
            Part part = (Part) partsToCheck.pop();
            Body body = part.getBody();
            if (isPartPgpInlineEncryptedOrSigned(part)) {
                inlineParts.add(part);
            } else if (body instanceof Multipart) {
                Multipart multipart = (Multipart) body;
                for (int i = multipart.getCount() - 1; i >= 0; i--) {
                    partsToCheck.push(multipart.getBodyPart(i));
                }
            }
        }
        return inlineParts;
    }

    public static byte[] getSignatureData(Part part) throws IOException, MessagingException {
        if (isPartMultipartSigned(part)) {
            Body body = part.getBody();
            if (body instanceof Multipart) {
                BodyPart signatureBody = ((Multipart) body).getBodyPart(1);
                if (MimeUtility.isSameMimeType(signatureBody.getMimeType(), APPLICATION_PGP_SIGNATURE)) {
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    signatureBody.getBody().writeTo(bos);
                    return bos.toByteArray();
                }
            }
        }
        return null;
    }

    private static boolean isPartEncryptedOrSigned(Part part) {
        return isPartMultipartEncrypted(part) || isPartMultipartSigned(part) || isPartPgpInlineEncryptedOrSigned(part);
    }

    private static boolean isPartMultipartSigned(Part part) {
        return MimeUtility.isSameMimeType(part.getMimeType(), MULTIPART_SIGNED);
    }

    private static boolean isPartMultipartEncrypted(Part part) {
        return MimeUtility.isSameMimeType(part.getMimeType(), MULTIPART_ENCRYPTED);
    }

    public static boolean isPgpMimeEncryptedOrSignedPart(Part part) {
        boolean isPgpEncrypted;
        boolean isPgpSigned;
        String protocolParameter = MimeUtility.getHeaderParameter(part.getContentType(), PROTOCOL_PARAMETER);
        if (!MimeUtility.isSameMimeType(part.getMimeType(), MULTIPART_ENCRYPTED) || !APPLICATION_PGP_ENCRYPTED.equalsIgnoreCase(protocolParameter)) {
            isPgpEncrypted = false;
        } else {
            isPgpEncrypted = true;
        }
        if (!MimeUtility.isSameMimeType(part.getMimeType(), MULTIPART_SIGNED) || !APPLICATION_PGP_SIGNATURE.equalsIgnoreCase(protocolParameter)) {
            isPgpSigned = false;
        } else {
            isPgpSigned = true;
        }
        if (isPgpEncrypted || isPgpSigned) {
            return true;
        }
        return false;
    }

    private static boolean isPartPgpInlineEncryptedOrSigned(Part part) {
        if (!part.isMimeType("text/plain") && !part.isMimeType(APPLICATION_PGP)) {
            return false;
        }
        String text = MessageExtractor.getTextFromPart(part, 36);
        if (TextUtils.isEmpty(text)) {
            return false;
        }
        if (text.startsWith(PGP_INLINE_START_MARKER) || text.startsWith("-----BEGIN PGP SIGNED MESSAGE-----")) {
            return true;
        }
        return false;
    }

    public static boolean isPartPgpInlineEncrypted(@Nullable Part part) {
        if (part == null) {
            return false;
        }
        if (!part.isMimeType("text/plain") && !part.isMimeType(APPLICATION_PGP)) {
            return false;
        }
        String text = MessageExtractor.getTextFromPart(part, 36);
        if (TextUtils.isEmpty(text) || !text.startsWith(PGP_INLINE_START_MARKER)) {
            return false;
        }
        return true;
    }
}
