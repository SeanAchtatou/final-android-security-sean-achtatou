package com.fsck.k9.controller;

import com.fsck.k9.mail.Message;
import java.util.Comparator;

public class UidReverseComparator implements Comparator<Message> {
    public int compare(Message messageLeft, Message messageRight) {
        Long uidLeft = getUidForMessage(messageLeft);
        Long uidRight = getUidForMessage(messageRight);
        if (uidLeft == null && uidRight == null) {
            return 0;
        }
        if (uidLeft == null) {
            return 1;
        }
        if (uidRight == null) {
            return -1;
        }
        return uidRight.compareTo(uidLeft);
    }

    private Long getUidForMessage(Message message) {
        try {
            return Long.valueOf(Long.parseLong(message.getUid()));
        } catch (NullPointerException | NumberFormatException e) {
            return null;
        }
    }
}
