package com.fsck.k9.controller;

public class UnavailableAccountException extends RuntimeException {
    private static final long serialVersionUID = -1827283277120501465L;

    public UnavailableAccountException() {
        super("please try again later");
    }

    public UnavailableAccountException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public UnavailableAccountException(String detailMessage) {
        super(detailMessage);
    }

    public UnavailableAccountException(Throwable throwable) {
        super(throwable);
    }
}
