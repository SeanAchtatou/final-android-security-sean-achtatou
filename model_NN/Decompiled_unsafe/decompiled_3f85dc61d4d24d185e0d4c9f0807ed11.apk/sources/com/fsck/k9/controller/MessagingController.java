package com.fsck.k9.controller;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Process;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;
import atomicgonza.AccountUtils;
import atomicgonza.App;
import atomicgonza.GmailUtils;
import com.fsck.k9.Account;
import com.fsck.k9.AccountStats;
import com.fsck.k9.K9;
import com.fsck.k9.Preferences;
import com.fsck.k9.R;
import com.fsck.k9.activity.MessageReference;
import com.fsck.k9.activity.setup.AccountSetupCheckSettings;
import com.fsck.k9.cache.EmailProviderCache;
import com.fsck.k9.helper.Contacts;
import com.fsck.k9.mail.Address;
import com.fsck.k9.mail.CertificateValidationException;
import com.fsck.k9.mail.FetchProfile;
import com.fsck.k9.mail.Flag;
import com.fsck.k9.mail.Folder;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.MessageRetrievalListener;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.Part;
import com.fsck.k9.mail.Pusher;
import com.fsck.k9.mail.Store;
import com.fsck.k9.mail.internet.MessageExtractor;
import com.fsck.k9.mail.internet.MimeMessage;
import com.fsck.k9.mail.internet.MimeMessageHelper;
import com.fsck.k9.mail.internet.MimeUtility;
import com.fsck.k9.mail.internet.TextBody;
import com.fsck.k9.mail.power.TracingPowerManager;
import com.fsck.k9.mail.store.pop3.Pop3Store;
import com.fsck.k9.mailstore.LocalFolder;
import com.fsck.k9.mailstore.LocalMessage;
import com.fsck.k9.mailstore.LocalStore;
import com.fsck.k9.mailstore.MessageRemovalListener;
import com.fsck.k9.mailstore.UnavailableStorageException;
import com.fsck.k9.notification.NotificationController;
import com.fsck.k9.provider.EmailProvider;
import com.fsck.k9.search.LocalSearch;
import com.fsck.k9.search.SearchAccount;
import com.fsck.k9.search.SearchSpecification;
import com.fsck.k9.search.SqlQueryBuilder;
import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import org.apache.james.mime4j.dom.field.ContentTypeField;

public class MessagingController {
    /* access modifiers changed from: private */
    public static final String[] EMPTY_STRING_ARRAY = new String[0];
    public static final long INVALID_MESSAGE_ID = -1;
    private static final String PENDING_COMMAND_APPEND = "com.fsck.k9.MessagingController.append";
    private static final String PENDING_COMMAND_EMPTY_TRASH = "com.fsck.k9.MessagingController.emptyTrash";
    private static final String PENDING_COMMAND_EXPUNGE = "com.fsck.k9.MessagingController.expunge";
    private static final String PENDING_COMMAND_MARK_ALL_AS_READ = "com.fsck.k9.MessagingController.markAllAsRead";
    private static final String PENDING_COMMAND_MOVE_OR_COPY = "com.fsck.k9.MessagingController.moveOrCopy";
    private static final String PENDING_COMMAND_MOVE_OR_COPY_BULK = "com.fsck.k9.MessagingController.moveOrCopyBulk";
    private static final String PENDING_COMMAND_MOVE_OR_COPY_BULK_NEW = "com.fsck.k9.MessagingController.moveOrCopyBulkNew";
    private static final String PENDING_COMMAND_SET_FLAG = "com.fsck.k9.MessagingController.setFlag";
    private static final String PENDING_COMMAND_SET_FLAG_BULK = "com.fsck.k9.MessagingController.setFlagBulk";
    private static final Set<Flag> SYNC_FLAGS = EnumSet.of(Flag.SEEN, Flag.FLAGGED, Flag.ANSWERED, Flag.FORWARDED);
    private static MessagingController inst = null;
    private static AtomicBoolean loopCatch = new AtomicBoolean();
    /* access modifiers changed from: private */
    public static AtomicInteger sequencing = new AtomicInteger(0);
    private MessagingListener checkMailListener = null;
    private final Contacts contacts;
    /* access modifiers changed from: private */
    public final Context context;
    private final Thread controllerThread;
    private final Boolean isFetchingMessageBody = false;
    public boolean isFetchingSearchList = false;
    public boolean isRemoteSearchRequested = false;
    private final Set<MessagingListener> listeners = new CopyOnWriteArraySet();
    private final MemorizingMessagingListener memorizingMessagingListener = new MemorizingMessagingListener();
    /* access modifiers changed from: private */
    public final NotificationController notificationController;
    private final ConcurrentHashMap<Account, Pusher> pushers = new ConcurrentHashMap<>();
    /* access modifiers changed from: private */
    public final BlockingQueue<Command> queuedCommands = new PriorityBlockingQueue();
    private final ConcurrentHashMap<String, AtomicInteger> sendCount = new ConcurrentHashMap<>();
    public boolean stopFetchingSearchedMessages = false;
    private volatile boolean stopped = false;
    private final ExecutorService threadPool = Executors.newCachedThreadPool();

    private interface MessageActor {
        void act(Account account, LocalFolder localFolder, List<LocalMessage> list);
    }

    public static synchronized MessagingController getInstance(Context context2) {
        MessagingController messagingController;
        synchronized (MessagingController.class) {
            if (inst == null) {
                Context appContext = context2.getApplicationContext();
                inst = new MessagingController(appContext, NotificationController.newInstance(appContext), Contacts.getInstance(context2));
            }
            messagingController = inst;
        }
        return messagingController;
    }

    MessagingController(Context context2, NotificationController notificationController2, Contacts contacts2) {
        this.context = context2;
        this.notificationController = notificationController2;
        this.contacts = contacts2;
        this.controllerThread = new Thread(new Runnable() {
            public void run() {
                MessagingController.this.runInBackground();
            }
        });
        this.controllerThread.setName("MessagingController");
        this.controllerThread.start();
        addListener(this.memorizingMessagingListener);
    }

    /* access modifiers changed from: package-private */
    public void stop() throws InterruptedException {
        this.stopped = true;
        this.controllerThread.interrupt();
        this.controllerThread.join(1000);
    }

    /* access modifiers changed from: private */
    public void runInBackground() {
        Process.setThreadPriority(10);
        while (!this.stopped) {
            String commandDescription = null;
            try {
                final Command command = this.queuedCommands.take();
                if (command != null) {
                    commandDescription = command.description;
                    if (K9.DEBUG) {
                        Log.i("k9", "Running command '" + command.description + "', seq = " + command.sequence + "(" + (command.isForegroundPriority ? "foreground" : "background") + "priority)");
                    }
                    try {
                        command.runnable.run();
                    } catch (UnavailableAccountException e) {
                        new Thread() {
                            public void run() {
                                try {
                                    sleep(30000);
                                    MessagingController.this.queuedCommands.put(command);
                                } catch (InterruptedException e) {
                                    Log.e("k9", "interrupted while putting a pending command for an unavailable account back into the queue. THIS SHOULD NEVER HAPPEN.");
                                }
                            }
                        }.start();
                    }
                    if (K9.DEBUG) {
                        Log.i("k9", " Command '" + command.description + "' completed");
                    }
                    for (MessagingListener l : getListeners(command.listener)) {
                        l.controllerCommandCompleted(!this.queuedCommands.isEmpty());
                    }
                }
            } catch (Exception e2) {
                Log.e("k9", "Error running command '" + commandDescription + "'", e2);
            }
        }
    }

    private void put(String description, MessagingListener listener, Runnable runnable) {
        putCommand(this.queuedCommands, description, listener, runnable, true);
    }

    /* access modifiers changed from: private */
    public void putBackground(String description, MessagingListener listener, Runnable runnable) {
        putCommand(this.queuedCommands, description, listener, runnable, false);
    }

    private void putCommand(BlockingQueue<Command> queue, String description, MessagingListener listener, Runnable runnable, boolean isForeground) {
        int retries = 10;
        Exception e = null;
        while (true) {
            int retries2 = retries;
            retries = retries2 - 1;
            if (retries2 > 0) {
                try {
                    Command command = new Command();
                    command.listener = listener;
                    command.runnable = runnable;
                    command.description = description;
                    command.isForegroundPriority = isForeground;
                    queue.put(command);
                    return;
                } catch (InterruptedException ie) {
                    SystemClock.sleep(200);
                    e = ie;
                }
            } else {
                throw new Error(e);
            }
        }
    }

    public void addListener(MessagingListener listener) {
        this.listeners.add(listener);
        refreshListener(listener);
    }

    public void refreshListener(MessagingListener listener) {
        if (listener != null) {
            this.memorizingMessagingListener.refreshOther(listener);
        }
    }

    public void removeListener(MessagingListener listener) {
        this.listeners.remove(listener);
    }

    public Set<MessagingListener> getListeners() {
        return this.listeners;
    }

    public Set<MessagingListener> getListeners(MessagingListener listener) {
        if (listener == null) {
            return this.listeners;
        }
        Set<MessagingListener> listeners2 = new HashSet<>(this.listeners);
        listeners2.add(listener);
        return listeners2;
    }

    /* access modifiers changed from: private */
    public void suppressMessages(Account account, List<LocalMessage> messages) {
        EmailProviderCache.getCache(account.getUuid(), this.context).hideMessages(messages);
    }

    private void unsuppressMessages(Account account, List<? extends Message> messages) {
        EmailProviderCache.getCache(account.getUuid(), this.context).unhideMessages(messages);
    }

    /* access modifiers changed from: private */
    public boolean isMessageSuppressed(LocalMessage message) {
        long messageId = message.getId();
        return EmailProviderCache.getCache(message.getFolder().getAccountUuid(), this.context).isMessageHidden(Long.valueOf(messageId), message.getFolder().getId());
    }

    private void setFlagInCache(Account account, List<Long> messageIds, Flag flag, boolean newState) {
        EmailProviderCache.getCache(account.getUuid(), this.context).setValueForMessages(messageIds, LocalStore.getColumnNameForFlag(flag), Integer.toString(newState ? 1 : 0));
    }

    private void removeFlagFromCache(Account account, List<Long> messageIds, Flag flag) {
        EmailProviderCache.getCache(account.getUuid(), this.context).removeValueForMessages(messageIds, LocalStore.getColumnNameForFlag(flag));
    }

    private void setFlagForThreadsInCache(Account account, List<Long> threadRootIds, Flag flag, boolean newState) {
        EmailProviderCache.getCache(account.getUuid(), this.context).setValueForThreads(threadRootIds, LocalStore.getColumnNameForFlag(flag), Integer.toString(newState ? 1 : 0));
    }

    private void removeFlagForThreadsFromCache(Account account, List<Long> messageIds, Flag flag) {
        EmailProviderCache.getCache(account.getUuid(), this.context).removeValueForThreads(messageIds, LocalStore.getColumnNameForFlag(flag));
    }

    public void listFolders(final Account account, final boolean refreshRemote, final MessagingListener listener) {
        this.threadPool.execute(new Runnable() {
            public void run() {
                MessagingController.this.listFoldersSynchronous(account, refreshRemote, listener);
            }
        });
    }

    /*  JADX ERROR: StackOverflow in pass: MarkFinallyVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    public void listFoldersSynchronous(com.fsck.k9.Account r9, boolean r10, com.fsck.k9.controller.MessagingListener r11) {
        /*
            r8 = this;
            java.util.Set r5 = r8.getListeners(r11)
            java.util.Iterator r5 = r5.iterator()
        L_0x0008:
            boolean r6 = r5.hasNext()
            if (r6 == 0) goto L_0x0018
            java.lang.Object r1 = r5.next()
            com.fsck.k9.controller.MessagingListener r1 = (com.fsck.k9.controller.MessagingListener) r1
            r1.listFoldersStarted(r9)
            goto L_0x0008
        L_0x0018:
            r3 = 0
            android.content.Context r5 = r8.context
            boolean r5 = r9.isAvailable(r5)
            if (r5 != 0) goto L_0x0040
            java.lang.String r5 = "k9"
            java.lang.String r6 = "not listing folders of unavailable account"
            android.util.Log.i(r5, r6)
        L_0x0028:
            java.util.Set r5 = r8.getListeners(r11)
            java.util.Iterator r5 = r5.iterator()
        L_0x0030:
            boolean r6 = r5.hasNext()
            if (r6 == 0) goto L_0x00e7
            java.lang.Object r1 = r5.next()
            com.fsck.k9.controller.MessagingListener r1 = (com.fsck.k9.controller.MessagingListener) r1
            r1.listFoldersFinished(r9)
            goto L_0x0030
        L_0x0040:
            com.fsck.k9.mailstore.LocalStore r4 = r9.getLocalStore()     // Catch:{ Exception -> 0x0082 }
            r5 = 0
            java.util.List r3 = r4.getPersonalNamespaces(r5)     // Catch:{ Exception -> 0x0082 }
            if (r10 != 0) goto L_0x0051
            boolean r5 = r3.isEmpty()     // Catch:{ Exception -> 0x0082 }
            if (r5 == 0) goto L_0x006a
        L_0x0051:
            r8.doRefreshRemote(r9, r11)     // Catch:{ Exception -> 0x0082 }
            if (r3 == 0) goto L_0x00e7
            java.util.Iterator r5 = r3.iterator()
        L_0x005a:
            boolean r6 = r5.hasNext()
            if (r6 == 0) goto L_0x00e7
            java.lang.Object r2 = r5.next()
            com.fsck.k9.mail.Folder r2 = (com.fsck.k9.mail.Folder) r2
            closeFolder(r2)
            goto L_0x005a
        L_0x006a:
            java.util.Set r5 = r8.getListeners(r11)     // Catch:{ Exception -> 0x0082 }
            java.util.Iterator r5 = r5.iterator()     // Catch:{ Exception -> 0x0082 }
        L_0x0072:
            boolean r6 = r5.hasNext()     // Catch:{ Exception -> 0x0082 }
            if (r6 == 0) goto L_0x00b6
            java.lang.Object r1 = r5.next()     // Catch:{ Exception -> 0x0082 }
            com.fsck.k9.controller.MessagingListener r1 = (com.fsck.k9.controller.MessagingListener) r1     // Catch:{ Exception -> 0x0082 }
            r1.listFolders(r9, r3)     // Catch:{ Exception -> 0x0082 }
            goto L_0x0072
        L_0x0082:
            r0 = move-exception
            java.util.Set r5 = r8.getListeners(r11)     // Catch:{ all -> 0x009f }
            java.util.Iterator r5 = r5.iterator()     // Catch:{ all -> 0x009f }
        L_0x008b:
            boolean r6 = r5.hasNext()     // Catch:{ all -> 0x009f }
            if (r6 == 0) goto L_0x00cc
            java.lang.Object r1 = r5.next()     // Catch:{ all -> 0x009f }
            com.fsck.k9.controller.MessagingListener r1 = (com.fsck.k9.controller.MessagingListener) r1     // Catch:{ all -> 0x009f }
            java.lang.String r6 = r0.getMessage()     // Catch:{ all -> 0x009f }
            r1.listFoldersFailed(r9, r6)     // Catch:{ all -> 0x009f }
            goto L_0x008b
        L_0x009f:
            r5 = move-exception
            if (r3 == 0) goto L_0x00e6
            java.util.Iterator r6 = r3.iterator()
        L_0x00a6:
            boolean r7 = r6.hasNext()
            if (r7 == 0) goto L_0x00e6
            java.lang.Object r2 = r6.next()
            com.fsck.k9.mail.Folder r2 = (com.fsck.k9.mail.Folder) r2
            closeFolder(r2)
            goto L_0x00a6
        L_0x00b6:
            if (r3 == 0) goto L_0x0028
            java.util.Iterator r5 = r3.iterator()
        L_0x00bc:
            boolean r6 = r5.hasNext()
            if (r6 == 0) goto L_0x0028
            java.lang.Object r2 = r5.next()
            com.fsck.k9.mail.Folder r2 = (com.fsck.k9.mail.Folder) r2
            closeFolder(r2)
            goto L_0x00bc
        L_0x00cc:
            r5 = 0
            r8.addErrorMessage(r9, r5, r0)     // Catch:{ all -> 0x009f }
            if (r3 == 0) goto L_0x00e7
            java.util.Iterator r5 = r3.iterator()
        L_0x00d6:
            boolean r6 = r5.hasNext()
            if (r6 == 0) goto L_0x00e7
            java.lang.Object r2 = r5.next()
            com.fsck.k9.mail.Folder r2 = (com.fsck.k9.mail.Folder) r2
            closeFolder(r2)
            goto L_0x00d6
        L_0x00e6:
            throw r5
        L_0x00e7:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fsck.k9.controller.MessagingController.listFoldersSynchronous(com.fsck.k9.Account, boolean, com.fsck.k9.controller.MessagingListener):void");
    }

    private void doRefreshRemote(final Account account, final MessagingListener listener) {
        put("doRefreshRemote", listener, new Runnable() {
            public void run() {
                MessagingController.this.refreshRemoteSynchronous(account, listener);
            }
        });
    }

    /*  JADX ERROR: StackOverflow in pass: MarkFinallyVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    void refreshRemoteSynchronous(com.fsck.k9.Account r18, com.fsck.k9.controller.MessagingListener r19) {
        /*
            r17 = this;
            r8 = 0
            com.fsck.k9.mail.Store r13 = r18.getRemoteStore()     // Catch:{ Exception -> 0x003a }
            r14 = 0
            java.util.List r12 = r13.getPersonalNamespaces(r14)     // Catch:{ Exception -> 0x003a }
            com.fsck.k9.mailstore.LocalStore r9 = r18.getLocalStore()     // Catch:{ Exception -> 0x003a }
            java.util.HashSet r11 = new java.util.HashSet     // Catch:{ Exception -> 0x003a }
            r11.<init>()     // Catch:{ Exception -> 0x003a }
            java.util.LinkedList r3 = new java.util.LinkedList     // Catch:{ Exception -> 0x003a }
            r3.<init>()     // Catch:{ Exception -> 0x003a }
            r14 = 0
            java.util.List r8 = r9.getPersonalNamespaces(r14)     // Catch:{ Exception -> 0x003a }
            java.util.HashSet r7 = new java.util.HashSet     // Catch:{ Exception -> 0x003a }
            r7.<init>()     // Catch:{ Exception -> 0x003a }
            java.util.Iterator r14 = r8.iterator()     // Catch:{ Exception -> 0x003a }
        L_0x0026:
            boolean r15 = r14.hasNext()     // Catch:{ Exception -> 0x003a }
            if (r15 == 0) goto L_0x0072
            java.lang.Object r5 = r14.next()     // Catch:{ Exception -> 0x003a }
            com.fsck.k9.mail.Folder r5 = (com.fsck.k9.mail.Folder) r5     // Catch:{ Exception -> 0x003a }
            java.lang.String r15 = r5.getName()     // Catch:{ Exception -> 0x003a }
            r7.add(r15)     // Catch:{ Exception -> 0x003a }
            goto L_0x0026
        L_0x003a:
            r2 = move-exception
            r0 = r17
            r1 = r19
            java.util.Set r14 = r0.getListeners(r1)     // Catch:{ all -> 0x005b }
            java.util.Iterator r14 = r14.iterator()     // Catch:{ all -> 0x005b }
        L_0x0047:
            boolean r15 = r14.hasNext()     // Catch:{ all -> 0x005b }
            if (r15 == 0) goto L_0x0135
            java.lang.Object r4 = r14.next()     // Catch:{ all -> 0x005b }
            com.fsck.k9.controller.MessagingListener r4 = (com.fsck.k9.controller.MessagingListener) r4     // Catch:{ all -> 0x005b }
            java.lang.String r15 = ""
            r0 = r18
            r4.listFoldersFailed(r0, r15)     // Catch:{ all -> 0x005b }
            goto L_0x0047
        L_0x005b:
            r14 = move-exception
            if (r8 == 0) goto L_0x0153
            java.util.Iterator r15 = r8.iterator()
        L_0x0062:
            boolean r16 = r15.hasNext()
            if (r16 == 0) goto L_0x0153
            java.lang.Object r5 = r15.next()
            com.fsck.k9.mail.Folder r5 = (com.fsck.k9.mail.Folder) r5
            closeFolder(r5)
            goto L_0x0062
        L_0x0072:
            java.util.Iterator r14 = r12.iterator()     // Catch:{ Exception -> 0x003a }
        L_0x0076:
            boolean r15 = r14.hasNext()     // Catch:{ Exception -> 0x003a }
            if (r15 == 0) goto L_0x009f
            java.lang.Object r10 = r14.next()     // Catch:{ Exception -> 0x003a }
            com.fsck.k9.mail.Folder r10 = (com.fsck.k9.mail.Folder) r10     // Catch:{ Exception -> 0x003a }
            java.lang.String r15 = r10.getName()     // Catch:{ Exception -> 0x003a }
            boolean r15 = r7.contains(r15)     // Catch:{ Exception -> 0x003a }
            if (r15 != 0) goto L_0x0097
            java.lang.String r15 = r10.getName()     // Catch:{ Exception -> 0x003a }
            com.fsck.k9.mailstore.LocalFolder r5 = r9.getFolder(r15)     // Catch:{ Exception -> 0x003a }
            r3.add(r5)     // Catch:{ Exception -> 0x003a }
        L_0x0097:
            java.lang.String r15 = r10.getName()     // Catch:{ Exception -> 0x003a }
            r11.add(r15)     // Catch:{ Exception -> 0x003a }
            goto L_0x0076
        L_0x009f:
            int r14 = r18.getDisplayCount()     // Catch:{ Exception -> 0x003a }
            r9.createFolders(r3, r14)     // Catch:{ Exception -> 0x003a }
            r14 = 0
            java.util.List r8 = r9.getPersonalNamespaces(r14)     // Catch:{ Exception -> 0x003a }
            java.util.Iterator r14 = r8.iterator()     // Catch:{ Exception -> 0x003a }
        L_0x00af:
            boolean r15 = r14.hasNext()     // Catch:{ Exception -> 0x003a }
            if (r15 == 0) goto L_0x00de
            java.lang.Object r5 = r14.next()     // Catch:{ Exception -> 0x003a }
            com.fsck.k9.mail.Folder r5 = (com.fsck.k9.mail.Folder) r5     // Catch:{ Exception -> 0x003a }
            java.lang.String r6 = r5.getName()     // Catch:{ Exception -> 0x003a }
            java.lang.String r15 = "-NONE-"
            boolean r15 = r15.equals(r6)     // Catch:{ Exception -> 0x003a }
            if (r15 == 0) goto L_0x00cb
            r15 = 0
            r5.delete(r15)     // Catch:{ Exception -> 0x003a }
        L_0x00cb:
            r0 = r18
            boolean r15 = r0.isSpecialFolder(r6)     // Catch:{ Exception -> 0x003a }
            if (r15 != 0) goto L_0x00af
            boolean r15 = r11.contains(r6)     // Catch:{ Exception -> 0x003a }
            if (r15 != 0) goto L_0x00af
            r15 = 0
            r5.delete(r15)     // Catch:{ Exception -> 0x003a }
            goto L_0x00af
        L_0x00de:
            r14 = 0
            java.util.List r8 = r9.getPersonalNamespaces(r14)     // Catch:{ Exception -> 0x003a }
            r0 = r17
            r1 = r19
            java.util.Set r14 = r0.getListeners(r1)     // Catch:{ Exception -> 0x003a }
            java.util.Iterator r14 = r14.iterator()     // Catch:{ Exception -> 0x003a }
        L_0x00ef:
            boolean r15 = r14.hasNext()     // Catch:{ Exception -> 0x003a }
            if (r15 == 0) goto L_0x0101
            java.lang.Object r4 = r14.next()     // Catch:{ Exception -> 0x003a }
            com.fsck.k9.controller.MessagingListener r4 = (com.fsck.k9.controller.MessagingListener) r4     // Catch:{ Exception -> 0x003a }
            r0 = r18
            r4.listFolders(r0, r8)     // Catch:{ Exception -> 0x003a }
            goto L_0x00ef
        L_0x0101:
            r0 = r17
            r1 = r19
            java.util.Set r14 = r0.getListeners(r1)     // Catch:{ Exception -> 0x003a }
            java.util.Iterator r14 = r14.iterator()     // Catch:{ Exception -> 0x003a }
        L_0x010d:
            boolean r15 = r14.hasNext()     // Catch:{ Exception -> 0x003a }
            if (r15 == 0) goto L_0x011f
            java.lang.Object r4 = r14.next()     // Catch:{ Exception -> 0x003a }
            com.fsck.k9.controller.MessagingListener r4 = (com.fsck.k9.controller.MessagingListener) r4     // Catch:{ Exception -> 0x003a }
            r0 = r18
            r4.listFoldersFinished(r0)     // Catch:{ Exception -> 0x003a }
            goto L_0x010d
        L_0x011f:
            if (r8 == 0) goto L_0x0154
            java.util.Iterator r14 = r8.iterator()
        L_0x0125:
            boolean r15 = r14.hasNext()
            if (r15 == 0) goto L_0x0154
            java.lang.Object r5 = r14.next()
            com.fsck.k9.mail.Folder r5 = (com.fsck.k9.mail.Folder) r5
            closeFolder(r5)
            goto L_0x0125
        L_0x0135:
            r14 = 0
            r0 = r17
            r1 = r18
            r0.addErrorMessage(r1, r14, r2)     // Catch:{ all -> 0x005b }
            if (r8 == 0) goto L_0x0154
            java.util.Iterator r14 = r8.iterator()
        L_0x0143:
            boolean r15 = r14.hasNext()
            if (r15 == 0) goto L_0x0154
            java.lang.Object r5 = r14.next()
            com.fsck.k9.mail.Folder r5 = (com.fsck.k9.mail.Folder) r5
            closeFolder(r5)
            goto L_0x0143
        L_0x0153:
            throw r14
        L_0x0154:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fsck.k9.controller.MessagingController.refreshRemoteSynchronous(com.fsck.k9.Account, com.fsck.k9.controller.MessagingListener):void");
    }

    public void searchLocalMessages(final LocalSearch search, final MessagingListener listener) {
        this.threadPool.execute(new Runnable() {
            public void run() {
                MessagingController.this.searchLocalMessagesSynchronous(search, listener);
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void searchLocalMessagesSynchronous(LocalSearch search, final MessagingListener listener) {
        final AccountStats stats = new AccountStats();
        Set<String> uuidSet = new HashSet<>(Arrays.asList(search.getAccountUuids()));
        List<Account> accounts = Preferences.getPreferences(this.context).getAccounts();
        boolean allAccounts = uuidSet.contains(SearchSpecification.ALL_ACCOUNTS);
        for (final Account account : accounts) {
            if (allAccounts || uuidSet.contains(account.getUuid())) {
                MessageRetrievalListener<LocalMessage> retrievalListener = new MessageRetrievalListener<LocalMessage>() {
                    public void messageStarted(String message, int number, int ofTotal) {
                    }

                    public void messagesFinished(int number) {
                    }

                    public void messageFinished(LocalMessage message, int number, int ofTotal) {
                        int i;
                        int i2 = 1;
                        if (!MessagingController.this.isMessageSuppressed(message)) {
                            List<LocalMessage> messages = new ArrayList<>();
                            messages.add(message);
                            AccountStats accountStats = stats;
                            int i3 = accountStats.unreadMessageCount;
                            if (!message.isSet(Flag.SEEN)) {
                                i = 1;
                            } else {
                                i = 0;
                            }
                            accountStats.unreadMessageCount = i + i3;
                            AccountStats accountStats2 = stats;
                            int i4 = accountStats2.flaggedMessageCount;
                            if (!message.isSet(Flag.FLAGGED)) {
                                i2 = 0;
                            }
                            accountStats2.flaggedMessageCount = i2 + i4;
                            if (listener != null) {
                                listener.listLocalMessagesAddMessages(account, null, messages);
                            }
                        }
                    }
                };
                if (listener != null) {
                    listener.listLocalMessagesStarted(account, null);
                }
                try {
                    account.getLocalStore().searchForMessages(retrievalListener, search);
                    if (listener != null) {
                        listener.listLocalMessagesFinished(account, null);
                    }
                } catch (Exception e) {
                    if (listener != null) {
                        listener.listLocalMessagesFailed(account, null, e.getMessage());
                    }
                    addErrorMessage(account, (String) null, e);
                    if (listener != null) {
                        listener.listLocalMessagesFinished(account, null);
                    }
                } catch (Throwable th) {
                    if (listener != null) {
                        listener.listLocalMessagesFinished(account, null);
                    }
                    throw th;
                }
            }
        }
        if (listener != null) {
            listener.searchStats(stats);
        }
    }

    public Future<?> searchRemoteMessages(String acctUuid, String folderName, String query, Set<Flag> requiredFlags, Set<Flag> forbiddenFlags, MessagingListener listener) {
        if (K9.DEBUG) {
            Log.i("k9", "searchRemoteMessages (acct=" + acctUuid + ", folderName = " + folderName + ", query = " + query + ")");
        }
        final String str = acctUuid;
        final String str2 = folderName;
        final String str3 = query;
        final Set<Flag> set = requiredFlags;
        final Set<Flag> set2 = forbiddenFlags;
        final MessagingListener messagingListener = listener;
        return this.threadPool.submit(new Runnable() {
            public void run() {
                MessagingController.this.searchRemoteMessagesSynchronous(str, str2, str3, set, set2, messagingListener);
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void searchRemoteMessagesSynchronous(String acctUuid, String folderName, String query, Set<Flag> requiredFlags, Set<Flag> forbiddenFlags, MessagingListener listener) {
        Account acct = Preferences.getPreferences(this.context).getAccount(acctUuid);
        if (listener != null) {
            listener.remoteSearchStarted(folderName);
        }
        List arrayList = new ArrayList();
        try {
            Store remoteStore = acct.getRemoteStore();
            LocalStore localStore = acct.getLocalStore();
            if (remoteStore == null || localStore == null) {
                throw new MessagingException("Could not get store");
            }
            Folder remoteFolder = remoteStore.getFolder(folderName);
            LocalFolder localFolder = localStore.getFolder(folderName);
            if (remoteFolder == null || localFolder == null) {
                throw new MessagingException("Folder not found");
            }
            Log.e("AtomicGonza", "okAG Search REMOTE " + new Exception().getStackTrace()[0].toString());
            List<? extends Message> search = remoteFolder.search(query, requiredFlags, forbiddenFlags);
            if (listener != null) {
                listener.remoteSearchServerQueryComplete(folderName, search.size(), acct.getRemoteSearchNumResults());
            }
            Collections.sort(search, AccountUtils.getSearchComparator(acct.getEmail()));
            int resultLimit = acct.getRemoteSearchNumResults();
            if (resultLimit > 0 && search.size() > resultLimit) {
                arrayList = search.subList(resultLimit, search.size());
                search = search.subList(0, resultLimit);
            }
            int results = search.size();
            if (!remoteFolder.isOpen()) {
                remoteFolder.open(0);
            }
            loadSearchResultsSynchronous(search, localFolder, remoteFolder, listener);
            if (listener != null) {
                listener.remoteSearchFinished(folderName, results, acct.getRemoteSearchNumResults(), arrayList);
            }
        } catch (Exception e) {
            if (Thread.currentThread().isInterrupted()) {
                Log.i("k9", "Caught exception on aborted remote search; safe to ignore.", e);
            } else {
                Log.e("k9", "Could not complete remote search", e);
                if (listener != null) {
                    listener.remoteSearchFailed(null, e.getMessage());
                }
                addErrorMessage(acct, (String) null, e);
            }
            if (listener != null) {
                listener.remoteSearchFinished(folderName, 0, acct.getRemoteSearchNumResults(), arrayList);
            }
        } catch (Throwable th) {
            if (listener != null) {
                listener.remoteSearchFinished(folderName, 0, acct.getRemoteSearchNumResults(), arrayList);
            }
            throw th;
        }
    }

    public void loadSearchResults(Account account, String folderName, List<Message> messages, MessagingListener listener) {
        final MessagingListener messagingListener = listener;
        final Account account2 = account;
        final String str = folderName;
        final List<Message> list = messages;
        this.threadPool.execute(new Runnable() {
            /* JADX INFO: finally extract failed */
            public void run() {
                if (messagingListener != null) {
                    messagingListener.enableProgressIndicator(true);
                    messagingListener.remoteSearchStartLoadMoreMessages();
                }
                try {
                    Store remoteStore = account2.getRemoteStore();
                    LocalStore localStore = account2.getLocalStore();
                    if (remoteStore == null || localStore == null) {
                        throw new MessagingException("Could not get store");
                    }
                    Folder remoteFolder = remoteStore.getFolder(str);
                    LocalFolder localFolder = localStore.getFolder(str);
                    if (remoteFolder == null || localFolder == null) {
                        throw new MessagingException("Folder not found");
                    }
                    if (!remoteFolder.isOpen()) {
                        if (K9.DEBUG) {
                            Log.e("AtomicGonza", "okAG RemoteFolder not open, opening... " + new Exception().getStackTrace()[0].toString());
                        }
                        remoteFolder.open(1);
                    }
                    Collections.sort(list, AccountUtils.getSearchComparator(account2.getEmail()));
                    MessagingController.this.loadSearchResultsSynchronous(list, localFolder, remoteFolder, messagingListener);
                    if (messagingListener != null) {
                        messagingListener.enableProgressIndicator(false);
                    }
                } catch (MessagingException e) {
                    Log.e("AtomicGonza", "okAG " + e.getMessage() + " " + new Exception().getStackTrace()[0].toString());
                    MessagingController.this.addErrorMessage(account2, (String) null, e);
                    if (messagingListener != null) {
                        messagingListener.remoteSearchFailed(null, e.getMessage());
                    }
                    if (messagingListener != null) {
                        messagingListener.enableProgressIndicator(false);
                    }
                } catch (Throwable th) {
                    if (messagingListener != null) {
                        messagingListener.enableProgressIndicator(false);
                    }
                    throw th;
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void loadSearchResultsSynchronous(List<Message> messages, LocalFolder localFolder, Folder remoteFolder, MessagingListener listener) throws MessagingException {
        FetchProfile header = new FetchProfile();
        header.add(FetchProfile.Item.FLAGS);
        header.add(FetchProfile.Item.ENVELOPE);
        new FetchProfile().add(FetchProfile.Item.STRUCTURE);
        int i = 0;
        while (true) {
            try {
                if (i >= messages.size()) {
                    break;
                }
                Message message = messages.get(i);
                if (this.stopFetchingSearchedMessages) {
                    this.stopFetchingSearchedMessages = false;
                    break;
                }
                this.isFetchingSearchList = true;
                if (localFolder.getMessage(message.getUid()) == null) {
                    synchronized (this.isFetchingMessageBody) {
                        if (K9.DEBUG) {
                            Log.e("AtomicGonza", "okAG  fetching header message... " + new Exception().getStackTrace()[0].toString());
                        }
                        remoteFolder.fetch(Collections.singletonList(message), header, null);
                        if (listener.getSearchQueryIfExist() != null) {
                            localFolder.appendMessagesSearch(listener.getSearchQueryIfExist(), Collections.singletonList(message));
                            this.isRemoteSearchRequested = true;
                        } else {
                            localFolder.appendMessages(Collections.singletonList(message));
                        }
                    }
                }
                if (listener != null) {
                    listener.remoteSearchFetchedMessage(i, messages.size());
                }
                i++;
            } catch (Exception e) {
                if (K9.DEBUG) {
                    Log.e("AtomicGonza", "okAG " + e.getMessage() + " " + new Exception().getStackTrace()[0].toString());
                }
                this.isFetchingSearchList = false;
                addErrorMessage(Preferences.getPreferences(this.context).getAccount(localFolder.getAccountUuid()), (String) null, e);
                throw e;
            }
        }
        this.isFetchingSearchList = false;
    }

    public void loadMoreMessages(Account account, String folder, MessagingListener listener) {
        try {
            LocalFolder localFolder = account.getLocalStore().getFolder(folder);
            if (localFolder.getVisibleLimit() > 0) {
                localFolder.setVisibleLimit(localFolder.getVisibleLimit() + account.getDisplayCount());
            }
            synchronizeMailbox(account, folder, listener, null);
        } catch (MessagingException me2) {
            addErrorMessage(account, (String) null, me2);
            throw new RuntimeException("Unable to set visible limit on folder", me2);
        }
    }

    public void synchronizeMailbox(Account account, String folder, MessagingListener listener, Folder providedRemoteFolder) {
        final Account account2 = account;
        final String str = folder;
        final MessagingListener messagingListener = listener;
        final Folder folder2 = providedRemoteFolder;
        putBackground("synchronizeMailbox", listener, new Runnable() {
            public void run() {
                MessagingController.this.synchronizeMailboxSynchronous(account2, str, messagingListener, folder2);
            }
        });
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:174:0x0666, code lost:
        if (r51 == null) goto L_0x0668;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:175:0x0668, code lost:
        closeFolder(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:176:0x066b, code lost:
        closeFolder(r38);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:209:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0164, code lost:
        r16 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:?, code lost:
        handleAuthenticationFailure(r48, true);
        r29 = "Error getting token Error: " + r16.getMessage() + " at MessagingController.java 1118";
        addErrorMessage(r48, r29, r29);
        r6 = getListeners(r50).iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x01a3, code lost:
        if (r6.hasNext() != false) goto L_0x01a5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x01a5, code lost:
        r6.next().synchronizeMailboxFailed(r48, r49, "Authentication failure");
     */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0164 A[ExcHandler: AuthenticationFailedException (r16v1 'e' com.fsck.k9.mail.AuthenticationFailedException A[CUSTOM_DECLARE]), PHI: r8 r38 
      PHI: (r8v3 'remoteFolder' com.fsck.k9.mail.Folder) = (r8v0 'remoteFolder' com.fsck.k9.mail.Folder), (r8v0 'remoteFolder' com.fsck.k9.mail.Folder), (r8v0 'remoteFolder' com.fsck.k9.mail.Folder), (r8v0 'remoteFolder' com.fsck.k9.mail.Folder), (r8v0 'remoteFolder' com.fsck.k9.mail.Folder), (r8v4 'remoteFolder' com.fsck.k9.mail.Folder), (r8v0 'remoteFolder' com.fsck.k9.mail.Folder) binds: [B:24:0x00e8, B:28:0x0108, B:52:0x01c9, B:30:0x010b, B:90:0x02fc, B:99:0x033e, B:29:?] A[DONT_GENERATE, DONT_INLINE]
      PHI: (r38v3 'tLocalFolder' com.fsck.k9.mailstore.LocalFolder) = (r38v0 'tLocalFolder' com.fsck.k9.mailstore.LocalFolder), (r38v0 'tLocalFolder' com.fsck.k9.mailstore.LocalFolder), (r38v0 'tLocalFolder' com.fsck.k9.mailstore.LocalFolder), (r38v0 'tLocalFolder' com.fsck.k9.mailstore.LocalFolder), (r38v4 'tLocalFolder' com.fsck.k9.mailstore.LocalFolder), (r38v4 'tLocalFolder' com.fsck.k9.mailstore.LocalFolder), (r38v0 'tLocalFolder' com.fsck.k9.mailstore.LocalFolder) binds: [B:24:0x00e8, B:28:0x0108, B:52:0x01c9, B:30:0x010b, B:90:0x02fc, B:99:0x033e, B:29:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:28:0x0108] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void synchronizeMailboxSynchronous(com.fsck.k9.Account r48, java.lang.String r49, com.fsck.k9.controller.MessagingListener r50, com.fsck.k9.mail.Folder r51) {
        /*
            r47 = this;
            if (r48 == 0) goto L_0x005a
            java.lang.String r24 = r48.getEmail()
            boolean r6 = atomicgonza.GmailUtils.isGmailAccount(r24)
            if (r6 == 0) goto L_0x005a
            r0 = r47
            android.content.Context r6 = r0.context
            android.content.SharedPreferences r31 = atomicgonza.App.getPrefToken(r6)
            r0 = r31
            r1 = r24
            long r40 = atomicgonza.App.getTimeExpireTokenGmailAccount(r0, r1)
            r6 = 3600000(0x36ee80, double:1.7786363E-317)
            int r6 = (r40 > r6 ? 1 : (r40 == r6 ? 0 : -1))
            if (r6 <= 0) goto L_0x005a
            long r6 = java.lang.System.currentTimeMillis()
            int r6 = (r40 > r6 ? 1 : (r40 == r6 ? 0 : -1))
            if (r6 >= 0) goto L_0x005a
            r0 = r31
            r1 = r24
            java.lang.String r12 = atomicgonza.App.getRefreshTokenGmailAccount(r0, r1)
            r0 = r47
            android.content.Context r0 = r0.context
            r44 = r0
            r45 = 1
            r46 = 0
            com.fsck.k9.controller.MessagingController$10 r6 = new com.fsck.k9.controller.MessagingController$10
            r7 = r47
            r8 = r48
            r9 = r50
            r10 = r49
            r11 = r51
            r6.<init>(r8, r9, r10, r11)
            r7 = r12
            r8 = r44
            r9 = r48
            r10 = r45
            r11 = r46
            r12 = r6
            atomicgonza.GmailUtils.queryAccessToken(r7, r8, r9, r10, r11, r12)
        L_0x0059:
            return
        L_0x005a:
            r8 = 0
            r38 = 0
            boolean r6 = com.fsck.k9.K9.DEBUG
            if (r6 == 0) goto L_0x0089
            java.lang.String r6 = "k9"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r11 = "Synchronizing folder "
            java.lang.StringBuilder r7 = r7.append(r11)
            java.lang.String r11 = r48.getDescription()
            java.lang.StringBuilder r7 = r7.append(r11)
            java.lang.String r11 = ":"
            java.lang.StringBuilder r7 = r7.append(r11)
            r0 = r49
            java.lang.StringBuilder r7 = r7.append(r0)
            java.lang.String r7 = r7.toString()
            android.util.Log.i(r6, r7)
        L_0x0089:
            r0 = r47
            r1 = r50
            java.util.Set r6 = r0.getListeners(r1)
            java.util.Iterator r6 = r6.iterator()
        L_0x0095:
            boolean r7 = r6.hasNext()
            if (r7 == 0) goto L_0x00ab
            java.lang.Object r19 = r6.next()
            com.fsck.k9.controller.MessagingListener r19 = (com.fsck.k9.controller.MessagingListener) r19
            r0 = r19
            r1 = r48
            r2 = r49
            r0.synchronizeMailboxStarted(r1, r2)
            goto L_0x0095
        L_0x00ab:
            java.lang.String r6 = r48.getOutboxFolderName()
            r0 = r49
            boolean r6 = r0.equals(r6)
            if (r6 != 0) goto L_0x00c3
            java.lang.String r6 = r48.getErrorFolderName()
            r0 = r49
            boolean r6 = r0.equals(r6)
            if (r6 == 0) goto L_0x00e7
        L_0x00c3:
            r0 = r47
            r1 = r50
            java.util.Set r6 = r0.getListeners(r1)
            java.util.Iterator r6 = r6.iterator()
        L_0x00cf:
            boolean r7 = r6.hasNext()
            if (r7 == 0) goto L_0x0059
            java.lang.Object r19 = r6.next()
            com.fsck.k9.controller.MessagingListener r19 = (com.fsck.k9.controller.MessagingListener) r19
            r7 = 0
            r11 = 0
            r0 = r19
            r1 = r48
            r2 = r49
            r0.synchronizeMailboxFinished(r1, r2, r7, r11)
            goto L_0x00cf
        L_0x00e7:
            r13 = 0
            boolean r6 = com.fsck.k9.K9.DEBUG     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            if (r6 == 0) goto L_0x0108
            java.lang.String r6 = "k9"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r7.<init>()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r11 = "SYNC: About to process pending commands for account "
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r11 = r48.getDescription()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r7 = r7.toString()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            android.util.Log.d(r6, r7)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
        L_0x0108:
            r47.processPendingCommandsSynchronous(r48)     // Catch:{ Exception -> 0x01c1, AuthenticationFailedException -> 0x0164 }
        L_0x010b:
            boolean r6 = com.fsck.k9.K9.DEBUG     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            if (r6 == 0) goto L_0x0129
            java.lang.String r6 = "k9"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r7.<init>()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r11 = "SYNC: About to get local folder "
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r0 = r49
            java.lang.StringBuilder r7 = r7.append(r0)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r7 = r7.toString()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            android.util.Log.v(r6, r7)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
        L_0x0129:
            com.fsck.k9.mailstore.LocalStore r22 = r48.getLocalStore()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r0 = r22
            r1 = r49
            com.fsck.k9.mailstore.LocalFolder r38 = r0.getFolder(r1)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r9 = r38
            r6 = 0
            r9.open(r6)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r9.updateLastUid()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r6 = 0
            java.util.List r21 = r9.getMessages(r6)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.util.HashMap r23 = new java.util.HashMap     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r23.<init>()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.util.Iterator r6 = r21.iterator()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
        L_0x014c:
            boolean r7 = r6.hasNext()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            if (r7 == 0) goto L_0x01d9
            java.lang.Object r26 = r6.next()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            com.fsck.k9.mail.Message r26 = (com.fsck.k9.mail.Message) r26     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r7 = r26.getUid()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r0 = r23
            r1 = r26
            r0.put(r7, r1)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            goto L_0x014c
        L_0x0164:
            r16 = move-exception
            r6 = 1
            r0 = r47
            r1 = r48
            r0.handleAuthenticationFailure(r1, r6)     // Catch:{ all -> 0x01b7 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x01b7 }
            r6.<init>()     // Catch:{ all -> 0x01b7 }
            java.lang.String r7 = "Error getting token Error: "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x01b7 }
            java.lang.String r7 = r16.getMessage()     // Catch:{ all -> 0x01b7 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x01b7 }
            java.lang.String r7 = " at MessagingController.java 1118"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x01b7 }
            java.lang.String r29 = r6.toString()     // Catch:{ all -> 0x01b7 }
            r0 = r48
            r1 = r29
            r2 = r29
            addErrorMessage(r0, r1, r2)     // Catch:{ all -> 0x01b7 }
            r0 = r47
            r1 = r50
            java.util.Set r6 = r0.getListeners(r1)     // Catch:{ all -> 0x01b7 }
            java.util.Iterator r6 = r6.iterator()     // Catch:{ all -> 0x01b7 }
        L_0x019f:
            boolean r7 = r6.hasNext()     // Catch:{ all -> 0x01b7 }
            if (r7 == 0) goto L_0x0666
            java.lang.Object r19 = r6.next()     // Catch:{ all -> 0x01b7 }
            com.fsck.k9.controller.MessagingListener r19 = (com.fsck.k9.controller.MessagingListener) r19     // Catch:{ all -> 0x01b7 }
            java.lang.String r7 = "Authentication failure"
            r0 = r19
            r1 = r48
            r2 = r49
            r0.synchronizeMailboxFailed(r1, r2, r7)     // Catch:{ all -> 0x01b7 }
            goto L_0x019f
        L_0x01b7:
            r6 = move-exception
            if (r51 != 0) goto L_0x01bd
            closeFolder(r8)
        L_0x01bd:
            closeFolder(r38)
            throw r6
        L_0x01c1:
            r16 = move-exception
            r6 = 0
            r0 = r47
            r1 = r48
            r2 = r16
            r0.addErrorMessage(r1, r6, r2)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r6 = "k9"
            java.lang.String r7 = "Failure processing command, but allow message sync attempt"
            r0 = r16
            android.util.Log.e(r6, r7, r0)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r13 = r16
            goto L_0x010b
        L_0x01d9:
            if (r51 == 0) goto L_0x02fc
            boolean r6 = com.fsck.k9.K9.DEBUG     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            if (r6 == 0) goto L_0x01f9
            java.lang.String r6 = "k9"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r7.<init>()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r11 = "SYNC: using providedRemoteFolder "
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r0 = r49
            java.lang.StringBuilder r7 = r7.append(r0)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r7 = r7.toString()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            android.util.Log.v(r6, r7)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
        L_0x01f9:
            r8 = r51
        L_0x01fb:
            r0 = r47
            com.fsck.k9.notification.NotificationController r6 = r0.notificationController     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r7 = 1
            r0 = r48
            r6.clearAuthenticationErrorNotification(r0, r7)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            int r33 = r8.getMessageCount()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            int r43 = r9.getVisibleLimit()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            if (r43 >= 0) goto L_0x0211
            r43 = 25
        L_0x0211:
            java.util.ArrayList r10 = new java.util.ArrayList     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r10.<init>()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.util.HashMap r36 = new java.util.HashMap     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r36.<init>()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            boolean r6 = com.fsck.k9.K9.DEBUG     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            if (r6 == 0) goto L_0x0245
            java.lang.String r6 = "k9"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r7.<init>()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r11 = "SYNC: Remote message count for folder "
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r0 = r49
            java.lang.StringBuilder r7 = r7.append(r0)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r11 = " is "
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r0 = r33
            java.lang.StringBuilder r7 = r7.append(r0)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r7 = r7.toString()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            android.util.Log.v(r6, r7)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
        L_0x0245:
            java.util.Date r17 = r48.getEarliestPollDate()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r34 = 1
            if (r33 <= 0) goto L_0x0466
            if (r43 <= 0) goto L_0x0399
            r6 = 0
            int r7 = r33 - r43
            int r6 = java.lang.Math.max(r6, r7)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            int r34 = r6 + 1
        L_0x0258:
            boolean r6 = com.fsck.k9.K9.DEBUG     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            if (r6 == 0) goto L_0x028e
            java.lang.String r6 = "k9"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r7.<init>()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r11 = "SYNC: About to get messages "
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r0 = r34
            java.lang.StringBuilder r7 = r7.append(r0)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r11 = " through "
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r0 = r33
            java.lang.StringBuilder r7 = r7.append(r0)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r11 = " for folder "
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r0 = r49
            java.lang.StringBuilder r7 = r7.append(r0)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r7 = r7.toString()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            android.util.Log.v(r6, r7)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
        L_0x028e:
            java.util.concurrent.atomic.AtomicInteger r18 = new java.util.concurrent.atomic.AtomicInteger     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r6 = 0
            r0 = r18
            r0.<init>(r6)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r0 = r47
            r1 = r50
            java.util.Set r6 = r0.getListeners(r1)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.util.Iterator r6 = r6.iterator()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
        L_0x02a2:
            boolean r7 = r6.hasNext()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            if (r7 == 0) goto L_0x039d
            java.lang.Object r19 = r6.next()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            com.fsck.k9.controller.MessagingListener r19 = (com.fsck.k9.controller.MessagingListener) r19     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r0 = r19
            r1 = r48
            r2 = r49
            r0.synchronizeMailboxHeadersStarted(r1, r2)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            goto L_0x02a2
        L_0x02b8:
            r16 = move-exception
            java.lang.String r6 = "k9"
            java.lang.String r7 = "synchronizeMailbox"
            r0 = r16
            android.util.Log.e(r6, r7, r0)     // Catch:{ all -> 0x01b7 }
            java.lang.String r37 = getRootCauseMessage(r16)     // Catch:{ all -> 0x01b7 }
            if (r38 == 0) goto L_0x02d8
            r0 = r38
            r1 = r37
            r0.setStatus(r1)     // Catch:{ MessagingException -> 0x0670 }
            long r6 = java.lang.System.currentTimeMillis()     // Catch:{ MessagingException -> 0x0670 }
            r0 = r38
            r0.setLastChecked(r6)     // Catch:{ MessagingException -> 0x0670 }
        L_0x02d8:
            r0 = r47
            r1 = r50
            java.util.Set r6 = r0.getListeners(r1)     // Catch:{ all -> 0x01b7 }
            java.util.Iterator r6 = r6.iterator()     // Catch:{ all -> 0x01b7 }
        L_0x02e4:
            boolean r7 = r6.hasNext()     // Catch:{ all -> 0x01b7 }
            if (r7 == 0) goto L_0x069f
            java.lang.Object r19 = r6.next()     // Catch:{ all -> 0x01b7 }
            com.fsck.k9.controller.MessagingListener r19 = (com.fsck.k9.controller.MessagingListener) r19     // Catch:{ all -> 0x01b7 }
            r0 = r19
            r1 = r48
            r2 = r49
            r3 = r37
            r0.synchronizeMailboxFailed(r1, r2, r3)     // Catch:{ all -> 0x01b7 }
            goto L_0x02e4
        L_0x02fc:
            com.fsck.k9.mail.Store r35 = r48.getRemoteStore()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            boolean r6 = com.fsck.k9.K9.DEBUG     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            if (r6 == 0) goto L_0x031e
            java.lang.String r6 = "k9"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r7.<init>()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r11 = "SYNC: About to get remote folder "
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r0 = r49
            java.lang.StringBuilder r7 = r7.append(r0)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r7 = r7.toString()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            android.util.Log.v(r6, r7)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
        L_0x031e:
            r0 = r35
            r1 = r49
            com.fsck.k9.mail.Folder r8 = r0.getFolder(r1)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r0 = r47
            r1 = r48
            r2 = r49
            r3 = r50
            boolean r6 = r0.verifyOrCreateRemoteSpecialFolder(r1, r2, r8, r3)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            if (r6 != 0) goto L_0x033e
            if (r51 != 0) goto L_0x0339
            closeFolder(r8)
        L_0x0339:
            closeFolder(r38)
            goto L_0x0059
        L_0x033e:
            boolean r6 = com.fsck.k9.K9.DEBUG     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            if (r6 == 0) goto L_0x035c
            java.lang.String r6 = "k9"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r7.<init>()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r11 = "SYNC: About to open remote folder "
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r0 = r49
            java.lang.StringBuilder r7 = r7.append(r0)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r7 = r7.toString()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            android.util.Log.v(r6, r7)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
        L_0x035c:
            r6 = 0
            r8.open(r6)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            com.fsck.k9.Account$Expunge r6 = com.fsck.k9.Account.Expunge.EXPUNGE_ON_POLL     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            com.fsck.k9.Account$Expunge r7 = r48.getExpungePolicy()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            if (r6 != r7) goto L_0x01fb
            boolean r6 = com.fsck.k9.K9.DEBUG     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            if (r6 == 0) goto L_0x0394
            java.lang.String r6 = "k9"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r7.<init>()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r11 = "SYNC: Expunging folder "
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r11 = r48.getDescription()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r11 = ":"
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r0 = r49
            java.lang.StringBuilder r7 = r7.append(r0)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r7 = r7.toString()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            android.util.Log.d(r6, r7)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
        L_0x0394:
            r8.expunge()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            goto L_0x01fb
        L_0x0399:
            r34 = 1
            goto L_0x0258
        L_0x039d:
            r6 = 0
            r0 = r34
            r1 = r33
            r2 = r17
            java.util.List r32 = r8.getMessages(r0, r1, r2, r6)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            int r27 = r32.size()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.util.Iterator r6 = r32.iterator()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
        L_0x03b0:
            boolean r7 = r6.hasNext()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            if (r7 == 0) goto L_0x0410
            java.lang.Object r39 = r6.next()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            com.fsck.k9.mail.Message r39 = (com.fsck.k9.mail.Message) r39     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r18.incrementAndGet()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r0 = r47
            r1 = r50
            java.util.Set r7 = r0.getListeners(r1)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.util.Iterator r7 = r7.iterator()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
        L_0x03cb:
            boolean r11 = r7.hasNext()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            if (r11 == 0) goto L_0x03e7
            java.lang.Object r19 = r7.next()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            com.fsck.k9.controller.MessagingListener r19 = (com.fsck.k9.controller.MessagingListener) r19     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            int r11 = r18.get()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r0 = r19
            r1 = r48
            r2 = r49
            r3 = r27
            r0.synchronizeMailboxHeadersProgress(r1, r2, r11, r3)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            goto L_0x03cb
        L_0x03e7:
            java.lang.String r7 = r39.getUid()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r0 = r23
            java.lang.Object r20 = r0.get(r7)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            com.fsck.k9.mail.Message r20 = (com.fsck.k9.mail.Message) r20     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            if (r20 == 0) goto L_0x03ff
            r0 = r20
            r1 = r17
            boolean r7 = r0.olderThan(r1)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            if (r7 != 0) goto L_0x03b0
        L_0x03ff:
            r0 = r39
            r10.add(r0)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r7 = r39.getUid()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r0 = r36
            r1 = r39
            r0.put(r7, r1)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            goto L_0x03b0
        L_0x0410:
            boolean r6 = com.fsck.k9.K9.DEBUG     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            if (r6 == 0) goto L_0x043c
            java.lang.String r6 = "k9"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r7.<init>()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r11 = "SYNC: Got "
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            int r11 = r36.size()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r11 = " messages for folder "
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r0 = r49
            java.lang.StringBuilder r7 = r7.append(r0)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r7 = r7.toString()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            android.util.Log.v(r6, r7)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
        L_0x043c:
            r0 = r47
            r1 = r50
            java.util.Set r6 = r0.getListeners(r1)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.util.Iterator r6 = r6.iterator()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
        L_0x0448:
            boolean r7 = r6.hasNext()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            if (r7 == 0) goto L_0x048f
            java.lang.Object r19 = r6.next()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            com.fsck.k9.controller.MessagingListener r19 = (com.fsck.k9.controller.MessagingListener) r19     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            int r7 = r18.get()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            int r11 = r36.size()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r0 = r19
            r1 = r48
            r2 = r49
            r0.synchronizeMailboxHeadersFinished(r1, r2, r7, r11)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            goto L_0x0448
        L_0x0466:
            if (r33 >= 0) goto L_0x048f
            java.lang.Exception r6 = new java.lang.Exception     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r7.<init>()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r11 = "Message count "
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r0 = r33
            java.lang.StringBuilder r7 = r7.append(r0)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r11 = " for folder "
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r0 = r49
            java.lang.StringBuilder r7 = r7.append(r0)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r7 = r7.toString()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r6.<init>(r7)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            throw r6     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
        L_0x048f:
            com.fsck.k9.mailstore.LocalFolder$MoreMessages r28 = r9.getMoreMessages()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            boolean r6 = r48.syncRemoteDeletions()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            if (r6 == 0) goto L_0x04fd
            java.util.ArrayList r15 = new java.util.ArrayList     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r15.<init>()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.util.Iterator r6 = r21.iterator()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
        L_0x04a2:
            boolean r7 = r6.hasNext()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            if (r7 == 0) goto L_0x04c0
            java.lang.Object r20 = r6.next()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            com.fsck.k9.mail.Message r20 = (com.fsck.k9.mail.Message) r20     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r7 = r20.getUid()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r0 = r36
            java.lang.Object r7 = r0.get(r7)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            if (r7 != 0) goto L_0x04a2
            r0 = r20
            r15.add(r0)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            goto L_0x04a2
        L_0x04c0:
            boolean r6 = r15.isEmpty()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            if (r6 != 0) goto L_0x04fd
            com.fsck.k9.mailstore.LocalFolder$MoreMessages r28 = com.fsck.k9.mailstore.LocalFolder.MoreMessages.UNKNOWN     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r9.destroyMessages(r15)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.util.Iterator r6 = r15.iterator()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
        L_0x04cf:
            boolean r7 = r6.hasNext()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            if (r7 == 0) goto L_0x04fd
            java.lang.Object r14 = r6.next()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            com.fsck.k9.mail.Message r14 = (com.fsck.k9.mail.Message) r14     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r0 = r47
            r1 = r50
            java.util.Set r7 = r0.getListeners(r1)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.util.Iterator r7 = r7.iterator()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
        L_0x04e7:
            boolean r11 = r7.hasNext()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            if (r11 == 0) goto L_0x04cf
            java.lang.Object r19 = r7.next()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            com.fsck.k9.controller.MessagingListener r19 = (com.fsck.k9.controller.MessagingListener) r19     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r0 = r19
            r1 = r48
            r2 = r49
            r0.synchronizeMailboxRemovedMessage(r1, r2, r14)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            goto L_0x04e7
        L_0x04fd:
            r21 = 0
            com.fsck.k9.mailstore.LocalFolder$MoreMessages r6 = com.fsck.k9.mailstore.LocalFolder.MoreMessages.UNKNOWN     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r0 = r28
            if (r0 != r6) goto L_0x050e
            r0 = r47
            r1 = r17
            r2 = r34
            r0.updateMoreMessages(r8, r9, r1, r2)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
        L_0x050e:
            java.lang.String r6 = r48.getEmail()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.util.Comparator r6 = atomicgonza.AccountUtils.getDownloadComparator(r6)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.util.Collections.sort(r10, r6)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r11 = 0
            r12 = 1
            r6 = r47
            r7 = r48
            int r30 = r6.downloadMessages(r7, r8, r9, r10, r11, r12)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            int r42 = r9.getUnreadMessageCount()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.util.Set r6 = r47.getListeners()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.util.Iterator r6 = r6.iterator()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
        L_0x052f:
            boolean r7 = r6.hasNext()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            if (r7 == 0) goto L_0x0547
            java.lang.Object r19 = r6.next()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            com.fsck.k9.controller.MessagingListener r19 = (com.fsck.k9.controller.MessagingListener) r19     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r0 = r19
            r1 = r48
            r2 = r49
            r3 = r42
            r0.folderStatusChanged(r1, r2, r3)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            goto L_0x052f
        L_0x0547:
            long r6 = java.lang.System.currentTimeMillis()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r9.setLastChecked(r6)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r6 = 0
            r9.setStatus(r6)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            boolean r6 = com.fsck.k9.K9.DEBUG     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            if (r6 == 0) goto L_0x059f
            java.lang.String r6 = "k9"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r7.<init>()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r11 = "Done synchronizing folder "
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r11 = r48.getDescription()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r11 = ":"
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r0 = r49
            java.lang.StringBuilder r7 = r7.append(r0)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r11 = " @ "
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.util.Date r11 = new java.util.Date     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r11.<init>()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r11 = " with "
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r0 = r30
            java.lang.StringBuilder r7 = r7.append(r0)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r11 = " new messages"
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r7 = r7.toString()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            android.util.Log.d(r6, r7)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
        L_0x059f:
            r0 = r47
            r1 = r50
            java.util.Set r6 = r0.getListeners(r1)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.util.Iterator r6 = r6.iterator()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
        L_0x05ab:
            boolean r7 = r6.hasNext()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            if (r7 == 0) goto L_0x05c5
            java.lang.Object r19 = r6.next()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            com.fsck.k9.controller.MessagingListener r19 = (com.fsck.k9.controller.MessagingListener) r19     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r0 = r19
            r1 = r48
            r2 = r49
            r3 = r33
            r4 = r30
            r0.synchronizeMailboxFinished(r1, r2, r3, r4)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            goto L_0x05ab
        L_0x05c5:
            if (r13 == 0) goto L_0x0630
            java.lang.String r37 = getRootCauseMessage(r13)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r6 = "k9"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r7.<init>()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r11 = "Root cause failure in "
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r11 = r48.getDescription()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r11 = ":"
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r11 = r38.getName()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r11 = " was '"
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r0 = r37
            java.lang.StringBuilder r7 = r7.append(r0)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r11 = "'"
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r7 = r7.toString()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            android.util.Log.e(r6, r7)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r0 = r37
            r9.setStatus(r0)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r0 = r47
            r1 = r50
            java.util.Set r6 = r0.getListeners(r1)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.util.Iterator r6 = r6.iterator()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
        L_0x0618:
            boolean r7 = r6.hasNext()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            if (r7 == 0) goto L_0x0630
            java.lang.Object r19 = r6.next()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            com.fsck.k9.controller.MessagingListener r19 = (com.fsck.k9.controller.MessagingListener) r19     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r0 = r19
            r1 = r48
            r2 = r49
            r3 = r37
            r0.synchronizeMailboxFailed(r1, r2, r3)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            goto L_0x0618
        L_0x0630:
            boolean r6 = com.fsck.k9.K9.DEBUG     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            if (r6 == 0) goto L_0x065c
            java.lang.String r6 = "k9"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r7.<init>()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r11 = "Done synchronizing folder "
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r11 = r48.getDescription()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r11 = ":"
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            r0 = r49
            java.lang.StringBuilder r7 = r7.append(r0)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            java.lang.String r7 = r7.toString()     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
            android.util.Log.i(r6, r7)     // Catch:{ AuthenticationFailedException -> 0x0164, Exception -> 0x02b8 }
        L_0x065c:
            if (r51 != 0) goto L_0x0661
            closeFolder(r8)
        L_0x0661:
            closeFolder(r38)
            goto L_0x0059
        L_0x0666:
            if (r51 != 0) goto L_0x066b
            closeFolder(r8)
        L_0x066b:
            closeFolder(r38)
            goto L_0x0059
        L_0x0670:
            r25 = move-exception
            java.lang.String r6 = "k9"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x01b7 }
            r7.<init>()     // Catch:{ all -> 0x01b7 }
            java.lang.String r11 = "Could not set last checked on folder "
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ all -> 0x01b7 }
            java.lang.String r11 = r48.getDescription()     // Catch:{ all -> 0x01b7 }
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ all -> 0x01b7 }
            java.lang.String r11 = ":"
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ all -> 0x01b7 }
            java.lang.String r11 = r38.getName()     // Catch:{ all -> 0x01b7 }
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ all -> 0x01b7 }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x01b7 }
            r0 = r16
            android.util.Log.e(r6, r7, r0)     // Catch:{ all -> 0x01b7 }
            goto L_0x02d8
        L_0x069f:
            r6 = 1
            r0 = r47
            r1 = r48
            r2 = r16
            r0.notifyUserIfCertificateProblem(r1, r2, r6)     // Catch:{ all -> 0x01b7 }
            r6 = 0
            r0 = r47
            r1 = r48
            r2 = r16
            r0.addErrorMessage(r1, r6, r2)     // Catch:{ all -> 0x01b7 }
            java.lang.String r6 = "k9"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x01b7 }
            r7.<init>()     // Catch:{ all -> 0x01b7 }
            java.lang.String r11 = "Failed synchronizing folder "
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ all -> 0x01b7 }
            java.lang.String r11 = r48.getDescription()     // Catch:{ all -> 0x01b7 }
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ all -> 0x01b7 }
            java.lang.String r11 = ":"
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ all -> 0x01b7 }
            r0 = r49
            java.lang.StringBuilder r7 = r7.append(r0)     // Catch:{ all -> 0x01b7 }
            java.lang.String r11 = " @ "
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ all -> 0x01b7 }
            java.util.Date r11 = new java.util.Date     // Catch:{ all -> 0x01b7 }
            r11.<init>()     // Catch:{ all -> 0x01b7 }
            java.lang.StringBuilder r7 = r7.append(r11)     // Catch:{ all -> 0x01b7 }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x01b7 }
            android.util.Log.e(r6, r7)     // Catch:{ all -> 0x01b7 }
            if (r51 != 0) goto L_0x06ef
            closeFolder(r8)
        L_0x06ef:
            closeFolder(r38)
            goto L_0x0059
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fsck.k9.controller.MessagingController.synchronizeMailboxSynchronous(com.fsck.k9.Account, java.lang.String, com.fsck.k9.controller.MessagingListener, com.fsck.k9.mail.Folder):void");
    }

    /* access modifiers changed from: package-private */
    public void handleAuthenticationFailure(Account account, boolean incoming) {
        this.notificationController.showAuthenticationErrorNotification(account, incoming);
    }

    /* access modifiers changed from: package-private */
    public void clearAuthenticationFailure(Account account, boolean incoming) {
        this.notificationController.clearAuthenticationErrorNotification(account, incoming);
    }

    private void updateMoreMessages(Folder remoteFolder, LocalFolder localFolder, Date earliestDate, int remoteStart) throws MessagingException, IOException {
        if (remoteStart == 1) {
            localFolder.setMoreMessages(LocalFolder.MoreMessages.FALSE);
        } else {
            localFolder.setMoreMessages(remoteFolder.areMoreMessagesAvailable(remoteStart, earliestDate) ? LocalFolder.MoreMessages.TRUE : LocalFolder.MoreMessages.FALSE);
        }
    }

    /* access modifiers changed from: private */
    public static void closeFolder(Folder f) {
        if (f != null) {
            f.close();
        }
    }

    private boolean verifyOrCreateRemoteSpecialFolder(Account account, String folder, Folder remoteFolder, MessagingListener listener) throws MessagingException {
        if ((!folder.equals(account.getTrashFolderName()) && !folder.equals(account.getSentFolderName()) && !folder.equals(account.getDraftsFolderName())) || remoteFolder.exists() || remoteFolder.create(Folder.FolderType.HOLDS_MESSAGES)) {
            return true;
        }
        for (MessagingListener l : getListeners(listener)) {
            l.synchronizeMailboxFinished(account, folder, 0, 0);
        }
        if (!K9.DEBUG) {
            return false;
        }
        Log.i("k9", "Done synchronizing folder " + folder);
        return false;
    }

    /* access modifiers changed from: private */
    public int downloadMessages(Account account, Folder remoteFolder, LocalFolder localFolder, List<Message> inputMessages, boolean flagSyncOnly, boolean purgeToVisibleLimit) throws MessagingException {
        Date earliestDate = account.getEarliestPollDate();
        Date downloadStarted = new Date();
        if (earliestDate != null && K9.DEBUG) {
            Log.d("k9", "Only syncing messages after " + earliestDate);
        }
        final String folder = remoteFolder.getName();
        int unreadBeforeStart = 0;
        try {
            unreadBeforeStart = account.getStats(this.context).unreadMessageCount;
        } catch (MessagingException e) {
            Log.e("k9", "Unable to getUnreadMessageCount for account: " + account, e);
        }
        ArrayList arrayList = new ArrayList();
        List<Message> unsyncedMessages = new ArrayList<>();
        AtomicInteger atomicInteger = new AtomicInteger(0);
        ArrayList<Message> arrayList2 = new ArrayList<>(inputMessages);
        Collections.sort(arrayList2, AccountUtils.getDownloadComparator(account.getEmail()));
        for (Message message : arrayList2) {
            evaluateMessageForDownload(message, folder, localFolder, remoteFolder, account, unsyncedMessages, arrayList, flagSyncOnly);
        }
        AtomicInteger atomicInteger2 = new AtomicInteger(0);
        int todo = unsyncedMessages.size() + arrayList.size();
        for (MessagingListener l : getListeners()) {
            l.synchronizeMailboxProgress(account, folder, atomicInteger2.get(), todo);
        }
        if (K9.DEBUG) {
            Log.d("k9", "SYNC: Have " + unsyncedMessages.size() + " unsynced messages");
        }
        arrayList2.clear();
        ArrayList arrayList3 = new ArrayList();
        ArrayList arrayList4 = new ArrayList();
        if (!unsyncedMessages.isEmpty()) {
            Collections.sort(unsyncedMessages, AccountUtils.getDownloadComparator(account.getEmail()));
            int visibleLimit = localFolder.getVisibleLimit();
            int listSize = unsyncedMessages.size();
            if (visibleLimit > 0 && listSize > visibleLimit) {
                unsyncedMessages = unsyncedMessages.subList(0, visibleLimit);
            }
            FetchProfile fp = new FetchProfile();
            if (remoteFolder.supportsFetchingFlags()) {
                fp.add(FetchProfile.Item.FLAGS);
            }
            fp.add(FetchProfile.Item.ENVELOPE);
            if (K9.DEBUG) {
                Log.d("k9", "SYNC: About to fetch " + unsyncedMessages.size() + " unsynced messages for folder " + folder);
            }
            Collections.sort(unsyncedMessages, AccountUtils.getDownloadComparator(account.getEmail()));
            fetchUnsyncedMessages(account, remoteFolder, unsyncedMessages, arrayList4, arrayList3, atomicInteger2, todo, fp);
            String updatedPushState = localFolder.getPushState();
            for (Message message2 : unsyncedMessages) {
                String newPushState = remoteFolder.getNewPushState(updatedPushState, message2);
                if (newPushState != null) {
                    updatedPushState = newPushState;
                }
            }
            localFolder.setPushState(updatedPushState);
            if (K9.DEBUG) {
                Log.d("k9", "SYNC: Synced unsynced messages for folder " + folder);
            }
        }
        if (K9.DEBUG) {
            Log.d("k9", "SYNC: Have " + arrayList3.size() + " large messages and " + arrayList4.size() + " small messages out of " + unsyncedMessages.size() + " unsynced messages");
        }
        unsyncedMessages.clear();
        FetchProfile fp2 = new FetchProfile();
        fp2.add(FetchProfile.Item.BODY);
        Collections.sort(arrayList4, AccountUtils.getDownloadComparator(account.getEmail()));
        downloadSmallMessages(account, remoteFolder, localFolder, arrayList4, atomicInteger2, unreadBeforeStart, atomicInteger, todo, fp2);
        arrayList4.clear();
        FetchProfile fp3 = new FetchProfile();
        fp3.add(FetchProfile.Item.STRUCTURE);
        downloadLargeMessages(account, remoteFolder, localFolder, arrayList3, atomicInteger2, unreadBeforeStart, atomicInteger, todo, fp3);
        arrayList3.clear();
        refreshLocalMessageFlags(account, remoteFolder, localFolder, arrayList, atomicInteger2, todo);
        if (K9.DEBUG) {
            Log.d("k9", "SYNC: Synced remote messages for folder " + folder + ", " + atomicInteger.get() + " new messages");
        }
        if (purgeToVisibleLimit) {
            final Account account2 = account;
            localFolder.purgeToVisibleLimit(new MessageRemovalListener() {
                public void messageRemoved(Message message) {
                    for (MessagingListener l : MessagingController.this.getListeners()) {
                        l.synchronizeMailboxRemovedMessage(account2, folder, message);
                    }
                }
            });
        }
        Long oldestMessageTime = localFolder.getOldestMessageDate();
        if (oldestMessageTime != null) {
            Date date = new Date(oldestMessageTime.longValue());
            if (date.before(downloadStarted)) {
                if (date.after(new Date(account.getLatestOldMessageSeenTime()))) {
                    account.setLatestOldMessageSeenTime(date.getTime());
                    account.save(Preferences.getPreferences(this.context));
                }
            }
        }
        return atomicInteger.get();
    }

    private void evaluateMessageForDownload(Message message, String folder, LocalFolder localFolder, Folder remoteFolder, Account account, List<Message> unsyncedMessages, List<Message> syncFlagMessages, boolean flagSyncOnly) throws MessagingException {
        if (message.isSet(Flag.DELETED)) {
            syncFlagMessages.add(message);
            return;
        }
        Message localMessage = localFolder.getMessage(message.getUid());
        if (localMessage == null) {
            if (flagSyncOnly) {
                return;
            }
            if (message.isSet(Flag.X_DOWNLOADED_FULL) || message.isSet(Flag.X_DOWNLOADED_PARTIAL)) {
                if (K9.DEBUG) {
                    Log.v("k9", "Message with uid " + message.getUid() + " is partially or fully downloaded");
                }
                localFolder.appendMessages(Collections.singletonList(message));
                Message localMessage2 = localFolder.getMessage(message.getUid());
                localMessage2.setFlag(Flag.X_DOWNLOADED_FULL, message.isSet(Flag.X_DOWNLOADED_FULL));
                localMessage2.setFlag(Flag.X_DOWNLOADED_PARTIAL, message.isSet(Flag.X_DOWNLOADED_PARTIAL));
                for (MessagingListener l : getListeners()) {
                    l.synchronizeMailboxAddOrUpdateMessage(account, folder, localMessage2);
                    if (!localMessage2.isSet(Flag.SEEN)) {
                        l.synchronizeMailboxNewMessage(account, folder, localMessage2);
                    }
                }
                return;
            }
            if (K9.DEBUG) {
                Log.v("k9", "Message with uid " + message.getUid() + " has not yet been downloaded");
            }
            unsyncedMessages.add(message);
        } else if (!localMessage.isSet(Flag.DELETED)) {
            if (K9.DEBUG) {
                Log.v("k9", "Message with uid " + message.getUid() + " is present in the local store");
            }
            if (localMessage.isSet(Flag.X_DOWNLOADED_FULL) || localMessage.isSet(Flag.X_DOWNLOADED_PARTIAL)) {
                String newPushState = remoteFolder.getNewPushState(localFolder.getPushState(), message);
                if (newPushState != null) {
                    localFolder.setPushState(newPushState);
                }
                syncFlagMessages.add(message);
                return;
            }
            if (K9.DEBUG) {
                Log.v("k9", "Message with uid " + message.getUid() + " is not downloaded, even partially; trying again");
            }
            unsyncedMessages.add(message);
        }
    }

    private <T extends Message> void fetchUnsyncedMessages(Account account, Folder<T> remoteFolder, List<T> unsyncedMessages, List<Message> smallMessages, List<Message> largeMessages, AtomicInteger progress, int todo, FetchProfile fp) throws MessagingException {
        final String folder = remoteFolder.getName();
        final Date earliestDate = account.getEarliestPollDate();
        final Account account2 = account;
        final AtomicInteger atomicInteger = progress;
        final int i = todo;
        final List<Message> list = largeMessages;
        final List<Message> list2 = smallMessages;
        remoteFolder.fetch(unsyncedMessages, fp, new MessageRetrievalListener<T>() {
            /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
                jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
                	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
                	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
                	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
                	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
                	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
                	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
                */
            public void messageFinished(T r8, int r9, int r10) {
                /*
                    r7 = this;
                    com.fsck.k9.mail.Flag r2 = com.fsck.k9.mail.Flag.DELETED     // Catch:{ Exception -> 0x0081 }
                    boolean r2 = r8.isSet(r2)     // Catch:{ Exception -> 0x0081 }
                    if (r2 != 0) goto L_0x0010
                    java.util.Date r2 = r3     // Catch:{ Exception -> 0x0081 }
                    boolean r2 = r8.olderThan(r2)     // Catch:{ Exception -> 0x0081 }
                    if (r2 == 0) goto L_0x00c1
                L_0x0010:
                    boolean r2 = com.fsck.k9.K9.DEBUG     // Catch:{ Exception -> 0x0081 }
                    if (r2 == 0) goto L_0x0056
                    com.fsck.k9.mail.Flag r2 = com.fsck.k9.mail.Flag.DELETED     // Catch:{ Exception -> 0x0081 }
                    boolean r2 = r8.isSet(r2)     // Catch:{ Exception -> 0x0081 }
                    if (r2 == 0) goto L_0x0092
                    java.lang.String r2 = "k9"
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0081 }
                    r3.<init>()     // Catch:{ Exception -> 0x0081 }
                    java.lang.String r4 = "Newly downloaded message "
                    java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0081 }
                    com.fsck.k9.Account r4 = r4     // Catch:{ Exception -> 0x0081 }
                    java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0081 }
                    java.lang.String r4 = ":"
                    java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0081 }
                    java.lang.String r4 = r5     // Catch:{ Exception -> 0x0081 }
                    java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0081 }
                    java.lang.String r4 = ":"
                    java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0081 }
                    java.lang.String r4 = r8.getUid()     // Catch:{ Exception -> 0x0081 }
                    java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0081 }
                    java.lang.String r4 = " was marked deleted on server, skipping"
                    java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0081 }
                    java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0081 }
                    android.util.Log.v(r2, r3)     // Catch:{ Exception -> 0x0081 }
                L_0x0056:
                    java.util.concurrent.atomic.AtomicInteger r2 = r6     // Catch:{ Exception -> 0x0081 }
                    r2.incrementAndGet()     // Catch:{ Exception -> 0x0081 }
                    com.fsck.k9.controller.MessagingController r2 = com.fsck.k9.controller.MessagingController.this     // Catch:{ Exception -> 0x0081 }
                    java.util.Set r2 = r2.getListeners()     // Catch:{ Exception -> 0x0081 }
                    java.util.Iterator r2 = r2.iterator()     // Catch:{ Exception -> 0x0081 }
                L_0x0065:
                    boolean r3 = r2.hasNext()     // Catch:{ Exception -> 0x0081 }
                    if (r3 == 0) goto L_0x0091
                    java.lang.Object r1 = r2.next()     // Catch:{ Exception -> 0x0081 }
                    com.fsck.k9.controller.MessagingListener r1 = (com.fsck.k9.controller.MessagingListener) r1     // Catch:{ Exception -> 0x0081 }
                    com.fsck.k9.Account r3 = r4     // Catch:{ Exception -> 0x0081 }
                    java.lang.String r4 = r5     // Catch:{ Exception -> 0x0081 }
                    java.util.concurrent.atomic.AtomicInteger r5 = r6     // Catch:{ Exception -> 0x0081 }
                    int r5 = r5.get()     // Catch:{ Exception -> 0x0081 }
                    int r6 = r7     // Catch:{ Exception -> 0x0081 }
                    r1.synchronizeMailboxProgress(r3, r4, r5, r6)     // Catch:{ Exception -> 0x0081 }
                    goto L_0x0065
                L_0x0081:
                    r0 = move-exception
                    java.lang.String r2 = "k9"
                    java.lang.String r3 = "Error while storing downloaded message."
                    android.util.Log.e(r2, r3, r0)
                    com.fsck.k9.controller.MessagingController r2 = com.fsck.k9.controller.MessagingController.this
                    com.fsck.k9.Account r3 = r4
                    r4 = 0
                    r2.addErrorMessage(r3, r4, r0)
                L_0x0091:
                    return
                L_0x0092:
                    java.lang.String r2 = "k9"
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0081 }
                    r3.<init>()     // Catch:{ Exception -> 0x0081 }
                    java.lang.String r4 = "Newly downloaded message "
                    java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0081 }
                    java.lang.String r4 = r8.getUid()     // Catch:{ Exception -> 0x0081 }
                    java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0081 }
                    java.lang.String r4 = " is older than "
                    java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0081 }
                    java.util.Date r4 = r3     // Catch:{ Exception -> 0x0081 }
                    java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0081 }
                    java.lang.String r4 = ", skipping"
                    java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0081 }
                    java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0081 }
                    android.util.Log.d(r2, r3)     // Catch:{ Exception -> 0x0081 }
                    goto L_0x0056
                L_0x00c1:
                    com.fsck.k9.Account r2 = r4     // Catch:{ Exception -> 0x0081 }
                    int r2 = r2.getMaximumAutoDownloadMessageSize()     // Catch:{ Exception -> 0x0081 }
                    if (r2 <= 0) goto L_0x00db
                    int r2 = r8.getSize()     // Catch:{ Exception -> 0x0081 }
                    com.fsck.k9.Account r3 = r4     // Catch:{ Exception -> 0x0081 }
                    int r3 = r3.getMaximumAutoDownloadMessageSize()     // Catch:{ Exception -> 0x0081 }
                    if (r2 <= r3) goto L_0x00db
                    java.util.List r2 = r8     // Catch:{ Exception -> 0x0081 }
                    r2.add(r8)     // Catch:{ Exception -> 0x0081 }
                    goto L_0x0091
                L_0x00db:
                    java.util.List r2 = r9     // Catch:{ Exception -> 0x0081 }
                    r2.add(r8)     // Catch:{ Exception -> 0x0081 }
                    goto L_0x0091
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fsck.k9.controller.MessagingController.AnonymousClass12.messageFinished(com.fsck.k9.mail.Message, int, int):void");
            }

            public void messageStarted(String uid, int number, int ofTotal) {
            }

            public void messagesFinished(int total) {
            }
        });
    }

    /* access modifiers changed from: private */
    public boolean shouldImportMessage(Account account, Message message, Date earliestDate) {
        if (!account.isSearchByDateCapable() || !message.olderThan(earliestDate)) {
            return true;
        }
        if (K9.DEBUG) {
            Log.d("k9", "Message " + message.getUid() + " is older than " + earliestDate + ", hence not saving");
        }
        return false;
    }

    private <T extends Message> void downloadSmallMessages(Account account, Folder<T> remoteFolder, LocalFolder localFolder, List<T> smallMessages, AtomicInteger progress, int unreadBeforeStart, AtomicInteger newMessages, int todo, FetchProfile fp) throws MessagingException {
        final String folder = remoteFolder.getName();
        final Date earliestDate = account.getEarliestPollDate();
        if (K9.DEBUG) {
            Log.d("k9", "SYNC: Fetching " + smallMessages.size() + " small messages for folder " + folder);
        }
        for (int i = 0; i < smallMessages.size(); i++) {
            final Account account2 = account;
            final AtomicInteger atomicInteger = progress;
            final LocalFolder localFolder2 = localFolder;
            final AtomicInteger atomicInteger2 = newMessages;
            final int i2 = todo;
            final int i3 = unreadBeforeStart;
            remoteFolder.fetch(Collections.singletonList(smallMessages.get(i)), fp, new MessageRetrievalListener<T>() {
                public void messageFinished(T message, int number, int ofTotal) {
                    try {
                        if (!MessagingController.this.shouldImportMessage(account2, message, earliestDate)) {
                            atomicInteger.incrementAndGet();
                            return;
                        }
                        Log.e("AtomicGonza", "okAG MessageFinished " + message.getUid() + " " + new Exception().getStackTrace()[0].toString());
                        LocalMessage localMessage = localFolder2.storeSmallMessage(message, new Runnable() {
                            public void run() {
                                atomicInteger.incrementAndGet();
                            }
                        });
                        if (!localMessage.isSet(Flag.SEEN)) {
                            atomicInteger2.incrementAndGet();
                        }
                        if (K9.DEBUG) {
                            Log.v("k9", "About to notify listeners that we got a new small message " + account2 + ":" + folder + ":" + message.getUid());
                        }
                        for (MessagingListener l : MessagingController.this.getListeners()) {
                            l.synchronizeMailboxAddOrUpdateMessage(account2, folder, localMessage);
                            l.synchronizeMailboxProgress(account2, folder, atomicInteger.get(), i2);
                            if (!localMessage.isSet(Flag.SEEN)) {
                                l.synchronizeMailboxNewMessage(account2, folder, localMessage);
                            }
                        }
                        if (MessagingController.this.shouldNotifyForMessage(account2, localFolder2, message)) {
                            MessagingController.this.notificationController.addNewMailNotification(account2, localMessage, i3);
                        }
                    } catch (MessagingException me2) {
                        MessagingController.this.addErrorMessage(account2, (String) null, me2);
                        Log.e("k9", "SYNC: fetch small messages", me2);
                    }
                }

                public void messageStarted(String uid, int number, int ofTotal) {
                }

                public void messagesFinished(int total) {
                }
            });
        }
        if (K9.DEBUG) {
            Log.d("k9", "SYNC: Done fetching small messages for folder " + folder);
        }
    }

    /* JADX INFO: Multiple debug info for r6v1 com.fsck.k9.mailstore.LocalMessage: [D('localMessage' com.fsck.k9.mail.Message), D('localMessage' com.fsck.k9.mailstore.LocalMessage)] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private <T extends com.fsck.k9.mail.Message> void downloadLargeMessages(com.fsck.k9.Account r16, com.fsck.k9.mail.Folder<T> r17, com.fsck.k9.mailstore.LocalFolder r18, java.util.List<T> r19, java.util.concurrent.atomic.AtomicInteger r20, int r21, java.util.concurrent.atomic.AtomicInteger r22, int r23, com.fsck.k9.mail.FetchProfile r24) throws com.fsck.k9.mail.MessagingException {
        /*
            r15 = this;
            java.lang.String r4 = r17.getName()
            java.util.Date r3 = r16.getEarliestPollDate()
            boolean r10 = com.fsck.k9.K9.DEBUG
            if (r10 == 0) goto L_0x0024
            java.lang.String r10 = "k9"
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r12 = "SYNC: Fetching large messages for folder "
            java.lang.StringBuilder r11 = r11.append(r12)
            java.lang.StringBuilder r11 = r11.append(r4)
            java.lang.String r11 = r11.toString()
            android.util.Log.d(r10, r11)
        L_0x0024:
            r10 = 0
            r0 = r17
            r1 = r19
            r2 = r24
            r0.fetch(r1, r2, r10)
            java.lang.String r10 = r16.getEmail()
            java.util.Comparator r10 = atomicgonza.AccountUtils.getDownloadComparator(r10)
            r0 = r19
            java.util.Collections.sort(r0, r10)
            java.util.Iterator r10 = r19.iterator()
        L_0x003f:
            boolean r11 = r10.hasNext()
            if (r11 == 0) goto L_0x01c9
            java.lang.Object r7 = r10.next()
            com.fsck.k9.mail.Message r7 = (com.fsck.k9.mail.Message) r7
            r0 = r16
            boolean r11 = r15.shouldImportMessage(r0, r7, r3)
            if (r11 != 0) goto L_0x0057
            r20.incrementAndGet()
            goto L_0x003f
        L_0x0057:
            com.fsck.k9.mail.Body r11 = r7.getBody()
            if (r11 != 0) goto L_0x0156
            com.fsck.k9.mail.FetchProfile r24 = new com.fsck.k9.mail.FetchProfile
            r24.<init>()
            com.fsck.k9.mail.FetchProfile$Item r11 = com.fsck.k9.mail.FetchProfile.Item.BODY_SANE
            r0 = r24
            r0.add(r11)
            java.util.List r11 = java.util.Collections.singletonList(r7)
            r12 = 0
            r0 = r17
            r1 = r24
            r0.fetch(r11, r1, r12)
            java.util.List r11 = java.util.Collections.singletonList(r7)
            r0 = r18
            r0.appendMessages(r11)
            java.lang.String r11 = "AtomicGonza"
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            java.lang.String r13 = "okAG Appending message with body "
            java.lang.StringBuilder r12 = r12.append(r13)
            java.lang.Exception r13 = new java.lang.Exception
            r13.<init>()
            java.lang.StackTraceElement[] r13 = r13.getStackTrace()
            r14 = 0
            r13 = r13[r14]
            java.lang.String r13 = r13.toString()
            java.lang.StringBuilder r12 = r12.append(r13)
            java.lang.String r12 = r12.toString()
            android.util.Log.e(r11, r12)
            java.lang.String r11 = r7.getUid()
            r0 = r18
            com.fsck.k9.mailstore.LocalMessage r6 = r0.getMessage(r11)
            com.fsck.k9.mail.Flag r11 = com.fsck.k9.mail.Flag.X_DOWNLOADED_FULL
            boolean r11 = r7.isSet(r11)
            if (r11 != 0) goto L_0x00ce
            int r11 = r16.getMaximumAutoDownloadMessageSize()
            if (r11 == 0) goto L_0x00c8
            int r11 = r7.getSize()
            int r12 = r16.getMaximumAutoDownloadMessageSize()
            if (r11 >= r12) goto L_0x014e
        L_0x00c8:
            com.fsck.k9.mail.Flag r11 = com.fsck.k9.mail.Flag.X_DOWNLOADED_FULL
            r12 = 1
            r6.setFlag(r11, r12)
        L_0x00ce:
            boolean r11 = com.fsck.k9.K9.DEBUG
            if (r11 == 0) goto L_0x0104
            java.lang.String r11 = "k9"
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            java.lang.String r13 = "About to notify listeners that we got a new large message "
            java.lang.StringBuilder r12 = r12.append(r13)
            r0 = r16
            java.lang.StringBuilder r12 = r12.append(r0)
            java.lang.String r13 = ":"
            java.lang.StringBuilder r12 = r12.append(r13)
            java.lang.StringBuilder r12 = r12.append(r4)
            java.lang.String r13 = ":"
            java.lang.StringBuilder r12 = r12.append(r13)
            java.lang.String r13 = r7.getUid()
            java.lang.StringBuilder r12 = r12.append(r13)
            java.lang.String r12 = r12.toString()
            android.util.Log.v(r11, r12)
        L_0x0104:
            r20.incrementAndGet()
            java.lang.String r11 = r7.getUid()
            r0 = r18
            com.fsck.k9.mailstore.LocalMessage r6 = r0.getMessage(r11)
            com.fsck.k9.mail.Flag r11 = com.fsck.k9.mail.Flag.SEEN
            boolean r11 = r6.isSet(r11)
            if (r11 != 0) goto L_0x011c
            r22.incrementAndGet()
        L_0x011c:
            java.util.Set r11 = r15.getListeners()
            java.util.Iterator r11 = r11.iterator()
        L_0x0124:
            boolean r12 = r11.hasNext()
            if (r12 == 0) goto L_0x01b4
            java.lang.Object r5 = r11.next()
            com.fsck.k9.controller.MessagingListener r5 = (com.fsck.k9.controller.MessagingListener) r5
            r0 = r16
            r5.synchronizeMailboxAddOrUpdateMessage(r0, r4, r6)
            int r12 = r20.get()
            r0 = r16
            r1 = r23
            r5.synchronizeMailboxProgress(r0, r4, r12, r1)
            com.fsck.k9.mail.Flag r12 = com.fsck.k9.mail.Flag.SEEN
            boolean r12 = r6.isSet(r12)
            if (r12 != 0) goto L_0x0124
            r0 = r16
            r5.synchronizeMailboxNewMessage(r0, r4, r6)
            goto L_0x0124
        L_0x014e:
            com.fsck.k9.mail.Flag r11 = com.fsck.k9.mail.Flag.X_DOWNLOADED_PARTIAL
            r12 = 1
            r6.setFlag(r11, r12)
            goto L_0x00ce
        L_0x0156:
            java.util.Set r9 = com.fsck.k9.mail.internet.MessageExtractor.collectTextParts(r7)
            java.util.Iterator r11 = r9.iterator()
        L_0x015e:
            boolean r12 = r11.hasNext()
            if (r12 == 0) goto L_0x0171
            java.lang.Object r8 = r11.next()
            com.fsck.k9.mail.Part r8 = (com.fsck.k9.mail.Part) r8
            r12 = 0
            r0 = r17
            r0.fetchPart(r7, r8, r12)
            goto L_0x015e
        L_0x0171:
            java.util.List r11 = java.util.Collections.singletonList(r7)
            r0 = r18
            r0.appendMessages(r11)
            java.lang.String r11 = "AtomicGonza"
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            java.lang.String r13 = "okAG Appending message with NO body "
            java.lang.StringBuilder r12 = r12.append(r13)
            java.lang.Exception r13 = new java.lang.Exception
            r13.<init>()
            java.lang.StackTraceElement[] r13 = r13.getStackTrace()
            r14 = 0
            r13 = r13[r14]
            java.lang.String r13 = r13.toString()
            java.lang.StringBuilder r12 = r12.append(r13)
            java.lang.String r12 = r12.toString()
            android.util.Log.e(r11, r12)
            java.lang.String r11 = r7.getUid()
            r0 = r18
            com.fsck.k9.mailstore.LocalMessage r6 = r0.getMessage(r11)
            com.fsck.k9.mail.Flag r11 = com.fsck.k9.mail.Flag.X_DOWNLOADED_PARTIAL
            r12 = 1
            r6.setFlag(r11, r12)
            goto L_0x00ce
        L_0x01b4:
            r0 = r16
            r1 = r18
            boolean r11 = r15.shouldNotifyForMessage(r0, r1, r7)
            if (r11 == 0) goto L_0x003f
            com.fsck.k9.notification.NotificationController r11 = r15.notificationController
            r0 = r16
            r1 = r21
            r11.addNewMailNotification(r0, r6, r1)
            goto L_0x003f
        L_0x01c9:
            boolean r10 = com.fsck.k9.K9.DEBUG
            if (r10 == 0) goto L_0x01e5
            java.lang.String r10 = "k9"
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r12 = "SYNC: Done fetching large messages for folder "
            java.lang.StringBuilder r11 = r11.append(r12)
            java.lang.StringBuilder r11 = r11.append(r4)
            java.lang.String r11 = r11.toString()
            android.util.Log.d(r10, r11)
        L_0x01e5:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fsck.k9.controller.MessagingController.downloadLargeMessages(com.fsck.k9.Account, com.fsck.k9.mail.Folder, com.fsck.k9.mailstore.LocalFolder, java.util.List, java.util.concurrent.atomic.AtomicInteger, int, java.util.concurrent.atomic.AtomicInteger, int, com.fsck.k9.mail.FetchProfile):void");
    }

    private void refreshLocalMessageFlags(Account account, Folder remoteFolder, LocalFolder localFolder, List<Message> syncFlagMessages, AtomicInteger progress, int todo) throws MessagingException {
        String folder = remoteFolder.getName();
        if (remoteFolder.supportsFetchingFlags()) {
            if (K9.DEBUG) {
                Log.d("k9", "SYNC: About to sync flags for " + syncFlagMessages.size() + " remote messages for folder " + folder);
            }
            FetchProfile fp = new FetchProfile();
            fp.add(FetchProfile.Item.FLAGS);
            List<Message> undeletedMessages = new LinkedList<>();
            for (Message message : syncFlagMessages) {
                if (!message.isSet(Flag.DELETED)) {
                    undeletedMessages.add(message);
                }
            }
            remoteFolder.fetch(undeletedMessages, fp, null);
            for (Message remoteMessage : syncFlagMessages) {
                LocalMessage localMessage = localFolder.getMessage(remoteMessage.getUid());
                if (syncFlags(localMessage, remoteMessage)) {
                    boolean shouldBeNotifiedOf = false;
                    if (localMessage.isSet(Flag.DELETED) || isMessageSuppressed(localMessage)) {
                        for (MessagingListener l : getListeners()) {
                            l.synchronizeMailboxRemovedMessage(account, folder, localMessage);
                        }
                    } else {
                        for (MessagingListener l2 : getListeners()) {
                            l2.synchronizeMailboxAddOrUpdateMessage(account, folder, localMessage);
                        }
                        if (shouldNotifyForMessage(account, localFolder, localMessage)) {
                            shouldBeNotifiedOf = true;
                        }
                    }
                    if (!shouldBeNotifiedOf) {
                        this.notificationController.removeNewMailNotification(account, localMessage.makeMessageReference());
                    }
                }
                progress.incrementAndGet();
                for (MessagingListener l3 : getListeners()) {
                    l3.synchronizeMailboxProgress(account, folder, progress.get(), todo);
                }
            }
        }
    }

    private boolean syncFlags(LocalMessage localMessage, Message remoteMessage) throws MessagingException {
        boolean messageChanged = false;
        if (localMessage == null || localMessage.isSet(Flag.DELETED)) {
            return false;
        }
        if (!remoteMessage.isSet(Flag.DELETED)) {
            for (Flag flag : SYNC_FLAGS) {
                if (remoteMessage.isSet(flag) != localMessage.isSet(flag)) {
                    localMessage.setFlag(flag, remoteMessage.isSet(flag));
                    messageChanged = true;
                }
            }
        } else if (localMessage.getFolder().syncRemoteDeletions()) {
            localMessage.setFlag(Flag.DELETED, true);
            messageChanged = true;
        }
        return messageChanged;
    }

    /* access modifiers changed from: private */
    public static String getRootCauseMessage(Throwable t) {
        Throwable nextCause;
        Throwable rootCause = t;
        do {
            nextCause = rootCause.getCause();
            if (nextCause != null) {
                rootCause = nextCause;
                continue;
            }
        } while (nextCause != null);
        if (rootCause instanceof MessagingException) {
            return rootCause.getMessage();
        }
        if (rootCause.getLocalizedMessage() != null) {
            return rootCause.getClass().getSimpleName() + ": " + rootCause.getLocalizedMessage();
        }
        return rootCause.getClass().getSimpleName();
    }

    /* access modifiers changed from: private */
    public void queuePendingCommand(Account account, LocalStore.PendingCommand command) {
        try {
            account.getLocalStore().addPendingCommand(command);
        } catch (Exception e) {
            addErrorMessage(account, (String) null, e);
            throw new RuntimeException("Unable to enqueue pending command", e);
        }
    }

    /* access modifiers changed from: private */
    public void processPendingCommands(final Account account) {
        putBackground("processPendingCommands", null, new Runnable() {
            public void run() {
                try {
                    MessagingController.this.processPendingCommandsSynchronous(account);
                } catch (UnavailableStorageException e) {
                    Log.i("k9", "Failed to process pending command because storage is not available - trying again later.");
                    throw new UnavailableAccountException(e);
                } catch (MessagingException me2) {
                    Log.e("k9", "processPendingCommands", me2);
                    MessagingController.this.addErrorMessage(account, (String) null, me2);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    /*  JADX ERROR: StackOverflow in pass: MarkFinallyVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    public void processPendingCommandsSynchronous(com.fsck.k9.Account r15) throws com.fsck.k9.mail.MessagingException {
        /*
            r14 = this;
            r12 = 0
            com.fsck.k9.mailstore.LocalStore r5 = r15.getLocalStore()
            java.util.List r2 = r5.getPendingCommands()
            r8 = 0
            int r9 = r2.size()
            if (r9 != 0) goto L_0x0011
        L_0x0010:
            return
        L_0x0011:
            java.util.Set r10 = r14.getListeners()
            java.util.Iterator r10 = r10.iterator()
        L_0x0019:
            boolean r11 = r10.hasNext()
            if (r11 == 0) goto L_0x002c
            java.lang.Object r4 = r10.next()
            com.fsck.k9.controller.MessagingListener r4 = (com.fsck.k9.controller.MessagingListener) r4
            r4.pendingCommandsProcessing(r15)
            r4.synchronizeMailboxProgress(r15, r12, r8, r9)
            goto L_0x0019
        L_0x002c:
            r7 = 0
            java.util.Iterator r10 = r2.iterator()     // Catch:{ MessagingException -> 0x0085 }
        L_0x0031:
            boolean r11 = r10.hasNext()     // Catch:{ MessagingException -> 0x0085 }
            if (r11 == 0) goto L_0x01f8
            java.lang.Object r0 = r10.next()     // Catch:{ MessagingException -> 0x0085 }
            com.fsck.k9.mailstore.LocalStore$PendingCommand r0 = (com.fsck.k9.mailstore.LocalStore.PendingCommand) r0     // Catch:{ MessagingException -> 0x0085 }
            r7 = r0
            boolean r11 = com.fsck.k9.K9.DEBUG     // Catch:{ MessagingException -> 0x0085 }
            if (r11 == 0) goto L_0x0060
            java.lang.String r11 = "k9"
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ MessagingException -> 0x0085 }
            r12.<init>()     // Catch:{ MessagingException -> 0x0085 }
            java.lang.String r13 = "Processing pending command '"
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ MessagingException -> 0x0085 }
            java.lang.StringBuilder r12 = r12.append(r0)     // Catch:{ MessagingException -> 0x0085 }
            java.lang.String r13 = "'"
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ MessagingException -> 0x0085 }
            java.lang.String r12 = r12.toString()     // Catch:{ MessagingException -> 0x0085 }
            android.util.Log.d(r11, r12)     // Catch:{ MessagingException -> 0x0085 }
        L_0x0060:
            java.lang.String r11 = r0.command     // Catch:{ MessagingException -> 0x0085 }
            java.lang.String r12 = "\\."
            java.lang.String[] r3 = r11.split(r12)     // Catch:{ MessagingException -> 0x0085 }
            int r11 = r3.length     // Catch:{ MessagingException -> 0x0085 }
            int r11 = r11 + -1
            r1 = r3[r11]     // Catch:{ MessagingException -> 0x0085 }
            java.util.Set r11 = r14.getListeners()     // Catch:{ MessagingException -> 0x0085 }
            java.util.Iterator r11 = r11.iterator()     // Catch:{ MessagingException -> 0x0085 }
        L_0x0075:
            boolean r12 = r11.hasNext()     // Catch:{ MessagingException -> 0x0085 }
            if (r12 == 0) goto L_0x00c6
            java.lang.Object r4 = r11.next()     // Catch:{ MessagingException -> 0x0085 }
            com.fsck.k9.controller.MessagingListener r4 = (com.fsck.k9.controller.MessagingListener) r4     // Catch:{ MessagingException -> 0x0085 }
            r4.pendingCommandStarted(r15, r1)     // Catch:{ MessagingException -> 0x0085 }
            goto L_0x0075
        L_0x0085:
            r6 = move-exception
            r10 = 1
            r14.notifyUserIfCertificateProblem(r15, r6, r10)     // Catch:{ all -> 0x00ad }
            r10 = 0
            r14.addErrorMessage(r15, r10, r6)     // Catch:{ all -> 0x00ad }
            java.lang.String r10 = "k9"
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ad }
            r11.<init>()     // Catch:{ all -> 0x00ad }
            java.lang.String r12 = "Could not process command '"
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ all -> 0x00ad }
            java.lang.StringBuilder r11 = r11.append(r7)     // Catch:{ all -> 0x00ad }
            java.lang.String r12 = "'"
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ all -> 0x00ad }
            java.lang.String r11 = r11.toString()     // Catch:{ all -> 0x00ad }
            android.util.Log.e(r10, r11, r6)     // Catch:{ all -> 0x00ad }
            throw r6     // Catch:{ all -> 0x00ad }
        L_0x00ad:
            r10 = move-exception
            java.util.Set r11 = r14.getListeners()
            java.util.Iterator r11 = r11.iterator()
        L_0x00b6:
            boolean r12 = r11.hasNext()
            if (r12 == 0) goto L_0x0210
            java.lang.Object r4 = r11.next()
            com.fsck.k9.controller.MessagingListener r4 = (com.fsck.k9.controller.MessagingListener) r4
            r4.pendingCommandsFinished(r15)
            goto L_0x00b6
        L_0x00c6:
            java.lang.String r11 = "com.fsck.k9.MessagingController.append"
            java.lang.String r12 = r0.command     // Catch:{ MessagingException -> 0x0124 }
            boolean r11 = r11.equals(r12)     // Catch:{ MessagingException -> 0x0124 }
            if (r11 == 0) goto L_0x0116
            r14.processPendingAppend(r0, r15)     // Catch:{ MessagingException -> 0x0124 }
        L_0x00d3:
            r5.removePendingCommand(r0)     // Catch:{ MessagingException -> 0x0124 }
            boolean r11 = com.fsck.k9.K9.DEBUG     // Catch:{ MessagingException -> 0x0124 }
            if (r11 == 0) goto L_0x00f8
            java.lang.String r11 = "k9"
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ MessagingException -> 0x0124 }
            r12.<init>()     // Catch:{ MessagingException -> 0x0124 }
            java.lang.String r13 = "Done processing pending command '"
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ MessagingException -> 0x0124 }
            java.lang.StringBuilder r12 = r12.append(r0)     // Catch:{ MessagingException -> 0x0124 }
            java.lang.String r13 = "'"
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ MessagingException -> 0x0124 }
            java.lang.String r12 = r12.toString()     // Catch:{ MessagingException -> 0x0124 }
            android.util.Log.d(r11, r12)     // Catch:{ MessagingException -> 0x0124 }
        L_0x00f8:
            int r8 = r8 + 1
            java.util.Set r11 = r14.getListeners()     // Catch:{ MessagingException -> 0x0085 }
            java.util.Iterator r11 = r11.iterator()     // Catch:{ MessagingException -> 0x0085 }
        L_0x0102:
            boolean r12 = r11.hasNext()     // Catch:{ MessagingException -> 0x0085 }
            if (r12 == 0) goto L_0x0031
            java.lang.Object r4 = r11.next()     // Catch:{ MessagingException -> 0x0085 }
            com.fsck.k9.controller.MessagingListener r4 = (com.fsck.k9.controller.MessagingListener) r4     // Catch:{ MessagingException -> 0x0085 }
            r12 = 0
            r4.synchronizeMailboxProgress(r15, r12, r8, r9)     // Catch:{ MessagingException -> 0x0085 }
            r4.pendingCommandCompleted(r15, r1)     // Catch:{ MessagingException -> 0x0085 }
            goto L_0x0102
        L_0x0116:
            java.lang.String r11 = "com.fsck.k9.MessagingController.setFlagBulk"
            java.lang.String r12 = r0.command     // Catch:{ MessagingException -> 0x0124 }
            boolean r11 = r11.equals(r12)     // Catch:{ MessagingException -> 0x0124 }
            if (r11 == 0) goto L_0x016e
            r14.processPendingSetFlag(r0, r15)     // Catch:{ MessagingException -> 0x0124 }
            goto L_0x00d3
        L_0x0124:
            r6 = move-exception
            boolean r11 = r6.isPermanentFailure()     // Catch:{ all -> 0x017d }
            if (r11 == 0) goto L_0x01f6
            r11 = 0
            r14.addErrorMessage(r15, r11, r6)     // Catch:{ all -> 0x017d }
            java.lang.String r11 = "k9"
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ all -> 0x017d }
            r12.<init>()     // Catch:{ all -> 0x017d }
            java.lang.String r13 = "Failure of command '"
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ all -> 0x017d }
            java.lang.StringBuilder r12 = r12.append(r0)     // Catch:{ all -> 0x017d }
            java.lang.String r13 = "' was permanent, removing command from queue"
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ all -> 0x017d }
            java.lang.String r12 = r12.toString()     // Catch:{ all -> 0x017d }
            android.util.Log.e(r11, r12)     // Catch:{ all -> 0x017d }
            r5.removePendingCommand(r7)     // Catch:{ all -> 0x017d }
            int r8 = r8 + 1
            java.util.Set r11 = r14.getListeners()     // Catch:{ MessagingException -> 0x0085 }
            java.util.Iterator r11 = r11.iterator()     // Catch:{ MessagingException -> 0x0085 }
        L_0x015a:
            boolean r12 = r11.hasNext()     // Catch:{ MessagingException -> 0x0085 }
            if (r12 == 0) goto L_0x0031
            java.lang.Object r4 = r11.next()     // Catch:{ MessagingException -> 0x0085 }
            com.fsck.k9.controller.MessagingListener r4 = (com.fsck.k9.controller.MessagingListener) r4     // Catch:{ MessagingException -> 0x0085 }
            r12 = 0
            r4.synchronizeMailboxProgress(r15, r12, r8, r9)     // Catch:{ MessagingException -> 0x0085 }
            r4.pendingCommandCompleted(r15, r1)     // Catch:{ MessagingException -> 0x0085 }
            goto L_0x015a
        L_0x016e:
            java.lang.String r11 = "com.fsck.k9.MessagingController.setFlag"
            java.lang.String r12 = r0.command     // Catch:{ MessagingException -> 0x0124 }
            boolean r11 = r11.equals(r12)     // Catch:{ MessagingException -> 0x0124 }
            if (r11 == 0) goto L_0x019c
            r14.processPendingSetFlagOld(r0, r15)     // Catch:{ MessagingException -> 0x0124 }
            goto L_0x00d3
        L_0x017d:
            r10 = move-exception
            int r8 = r8 + 1
            java.util.Set r11 = r14.getListeners()     // Catch:{ MessagingException -> 0x0085 }
            java.util.Iterator r11 = r11.iterator()     // Catch:{ MessagingException -> 0x0085 }
        L_0x0188:
            boolean r12 = r11.hasNext()     // Catch:{ MessagingException -> 0x0085 }
            if (r12 == 0) goto L_0x01f7
            java.lang.Object r4 = r11.next()     // Catch:{ MessagingException -> 0x0085 }
            com.fsck.k9.controller.MessagingListener r4 = (com.fsck.k9.controller.MessagingListener) r4     // Catch:{ MessagingException -> 0x0085 }
            r12 = 0
            r4.synchronizeMailboxProgress(r15, r12, r8, r9)     // Catch:{ MessagingException -> 0x0085 }
            r4.pendingCommandCompleted(r15, r1)     // Catch:{ MessagingException -> 0x0085 }
            goto L_0x0188
        L_0x019c:
            java.lang.String r11 = "com.fsck.k9.MessagingController.markAllAsRead"
            java.lang.String r12 = r0.command     // Catch:{ MessagingException -> 0x0124 }
            boolean r11 = r11.equals(r12)     // Catch:{ MessagingException -> 0x0124 }
            if (r11 == 0) goto L_0x01ab
            r14.processPendingMarkAllAsRead(r0, r15)     // Catch:{ MessagingException -> 0x0124 }
            goto L_0x00d3
        L_0x01ab:
            java.lang.String r11 = "com.fsck.k9.MessagingController.moveOrCopyBulk"
            java.lang.String r12 = r0.command     // Catch:{ MessagingException -> 0x0124 }
            boolean r11 = r11.equals(r12)     // Catch:{ MessagingException -> 0x0124 }
            if (r11 == 0) goto L_0x01ba
            r14.processPendingMoveOrCopyOld2(r0, r15)     // Catch:{ MessagingException -> 0x0124 }
            goto L_0x00d3
        L_0x01ba:
            java.lang.String r11 = "com.fsck.k9.MessagingController.moveOrCopyBulkNew"
            java.lang.String r12 = r0.command     // Catch:{ MessagingException -> 0x0124 }
            boolean r11 = r11.equals(r12)     // Catch:{ MessagingException -> 0x0124 }
            if (r11 == 0) goto L_0x01c9
            r14.processPendingMoveOrCopy(r0, r15)     // Catch:{ MessagingException -> 0x0124 }
            goto L_0x00d3
        L_0x01c9:
            java.lang.String r11 = "com.fsck.k9.MessagingController.moveOrCopy"
            java.lang.String r12 = r0.command     // Catch:{ MessagingException -> 0x0124 }
            boolean r11 = r11.equals(r12)     // Catch:{ MessagingException -> 0x0124 }
            if (r11 == 0) goto L_0x01d8
            r14.processPendingMoveOrCopyOld(r0, r15)     // Catch:{ MessagingException -> 0x0124 }
            goto L_0x00d3
        L_0x01d8:
            java.lang.String r11 = "com.fsck.k9.MessagingController.emptyTrash"
            java.lang.String r12 = r0.command     // Catch:{ MessagingException -> 0x0124 }
            boolean r11 = r11.equals(r12)     // Catch:{ MessagingException -> 0x0124 }
            if (r11 == 0) goto L_0x01e7
            r14.processPendingEmptyTrash(r0, r15)     // Catch:{ MessagingException -> 0x0124 }
            goto L_0x00d3
        L_0x01e7:
            java.lang.String r11 = "com.fsck.k9.MessagingController.expunge"
            java.lang.String r12 = r0.command     // Catch:{ MessagingException -> 0x0124 }
            boolean r11 = r11.equals(r12)     // Catch:{ MessagingException -> 0x0124 }
            if (r11 == 0) goto L_0x00d3
            r14.processPendingExpunge(r0, r15)     // Catch:{ MessagingException -> 0x0124 }
            goto L_0x00d3
        L_0x01f6:
            throw r6     // Catch:{ all -> 0x017d }
        L_0x01f7:
            throw r10     // Catch:{ MessagingException -> 0x0085 }
        L_0x01f8:
            java.util.Set r10 = r14.getListeners()
            java.util.Iterator r10 = r10.iterator()
        L_0x0200:
            boolean r11 = r10.hasNext()
            if (r11 == 0) goto L_0x0010
            java.lang.Object r4 = r10.next()
            com.fsck.k9.controller.MessagingListener r4 = (com.fsck.k9.controller.MessagingListener) r4
            r4.pendingCommandsFinished(r15)
            goto L_0x0200
        L_0x0210:
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fsck.k9.controller.MessagingController.processPendingCommandsSynchronous(com.fsck.k9.Account):void");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void processPendingAppend(com.fsck.k9.mailstore.LocalStore.PendingCommand r20, com.fsck.k9.Account r21) throws com.fsck.k9.mail.MessagingException {
        /*
            r19 = this;
            r12 = 0
            r6 = 0
            r0 = r20
            java.lang.String[] r0 = r0.arguments     // Catch:{ all -> 0x012a }
            r16 = r0
            r17 = 0
            r2 = r16[r17]     // Catch:{ all -> 0x012a }
            r0 = r20
            java.lang.String[] r0 = r0.arguments     // Catch:{ all -> 0x012a }
            r16 = r0
            r17 = 1
            r15 = r16[r17]     // Catch:{ all -> 0x012a }
            java.lang.String r16 = r21.getErrorFolderName()     // Catch:{ all -> 0x012a }
            r0 = r16
            boolean r16 = r0.equals(r2)     // Catch:{ all -> 0x012a }
            if (r16 == 0) goto L_0x0029
            closeFolder(r12)
            closeFolder(r6)
        L_0x0028:
            return
        L_0x0029:
            com.fsck.k9.mailstore.LocalStore r8 = r21.getLocalStore()     // Catch:{ all -> 0x012a }
            com.fsck.k9.mailstore.LocalFolder r6 = r8.getFolder(r2)     // Catch:{ all -> 0x012a }
            com.fsck.k9.mailstore.LocalMessage r7 = r6.getMessage(r15)     // Catch:{ all -> 0x012a }
            if (r7 != 0) goto L_0x003e
            closeFolder(r12)
            closeFolder(r6)
            goto L_0x0028
        L_0x003e:
            com.fsck.k9.mail.Store r14 = r21.getRemoteStore()     // Catch:{ all -> 0x012a }
            com.fsck.k9.mail.Folder r12 = r14.getFolder(r2)     // Catch:{ all -> 0x012a }
            boolean r16 = r12.exists()     // Catch:{ all -> 0x012a }
            if (r16 != 0) goto L_0x005d
            com.fsck.k9.mail.Folder$FolderType r16 = com.fsck.k9.mail.Folder.FolderType.HOLDS_MESSAGES     // Catch:{ all -> 0x012a }
            r0 = r16
            boolean r16 = r12.create(r0)     // Catch:{ all -> 0x012a }
            if (r16 != 0) goto L_0x005d
            closeFolder(r12)
            closeFolder(r6)
            goto L_0x0028
        L_0x005d:
            r16 = 0
            r0 = r16
            r12.open(r0)     // Catch:{ all -> 0x012a }
            int r16 = r12.getMode()     // Catch:{ all -> 0x012a }
            if (r16 == 0) goto L_0x0071
            closeFolder(r12)
            closeFolder(r6)
            goto L_0x0028
        L_0x0071:
            r13 = 0
            java.lang.String r16 = r7.getUid()     // Catch:{ all -> 0x012a }
            java.lang.String r17 = "K9LOCAL:"
            boolean r16 = r16.startsWith(r17)     // Catch:{ all -> 0x012a }
            if (r16 != 0) goto L_0x0088
            java.lang.String r16 = r7.getUid()     // Catch:{ all -> 0x012a }
            r0 = r16
            com.fsck.k9.mail.Message r13 = r12.getMessage(r0)     // Catch:{ all -> 0x012a }
        L_0x0088:
            if (r13 != 0) goto L_0x0195
            com.fsck.k9.mail.Flag r16 = com.fsck.k9.mail.Flag.X_REMOTE_COPY_STARTED     // Catch:{ all -> 0x012a }
            r0 = r16
            boolean r16 = r7.isSet(r0)     // Catch:{ all -> 0x012a }
            if (r16 == 0) goto L_0x0141
            java.lang.String r16 = "k9"
            java.lang.StringBuilder r17 = new java.lang.StringBuilder     // Catch:{ all -> 0x012a }
            r17.<init>()     // Catch:{ all -> 0x012a }
            java.lang.String r18 = "Local message with uid "
            java.lang.StringBuilder r17 = r17.append(r18)     // Catch:{ all -> 0x012a }
            java.lang.String r18 = r7.getUid()     // Catch:{ all -> 0x012a }
            java.lang.StringBuilder r17 = r17.append(r18)     // Catch:{ all -> 0x012a }
            java.lang.String r18 = " has flag "
            java.lang.StringBuilder r17 = r17.append(r18)     // Catch:{ all -> 0x012a }
            com.fsck.k9.mail.Flag r18 = com.fsck.k9.mail.Flag.X_REMOTE_COPY_STARTED     // Catch:{ all -> 0x012a }
            java.lang.StringBuilder r17 = r17.append(r18)     // Catch:{ all -> 0x012a }
            java.lang.String r18 = " already set, checking for remote message with "
            java.lang.StringBuilder r17 = r17.append(r18)     // Catch:{ all -> 0x012a }
            java.lang.String r18 = " same message id"
            java.lang.StringBuilder r17 = r17.append(r18)     // Catch:{ all -> 0x012a }
            java.lang.String r17 = r17.toString()     // Catch:{ all -> 0x012a }
            android.util.Log.w(r16, r17)     // Catch:{ all -> 0x012a }
            java.lang.String r10 = r12.getUidFromMessageId(r7)     // Catch:{ all -> 0x012a }
            if (r10 == 0) goto L_0x013a
            java.lang.String r16 = "k9"
            java.lang.StringBuilder r17 = new java.lang.StringBuilder     // Catch:{ all -> 0x012a }
            r17.<init>()     // Catch:{ all -> 0x012a }
            java.lang.String r18 = "Local message has flag "
            java.lang.StringBuilder r17 = r17.append(r18)     // Catch:{ all -> 0x012a }
            com.fsck.k9.mail.Flag r18 = com.fsck.k9.mail.Flag.X_REMOTE_COPY_STARTED     // Catch:{ all -> 0x012a }
            java.lang.StringBuilder r17 = r17.append(r18)     // Catch:{ all -> 0x012a }
            java.lang.String r18 = " already set, and there is a remote message with "
            java.lang.StringBuilder r17 = r17.append(r18)     // Catch:{ all -> 0x012a }
            java.lang.String r18 = " uid "
            java.lang.StringBuilder r17 = r17.append(r18)     // Catch:{ all -> 0x012a }
            r0 = r17
            java.lang.StringBuilder r17 = r0.append(r10)     // Catch:{ all -> 0x012a }
            java.lang.String r18 = ", assuming message was already copied and aborting this copy"
            java.lang.StringBuilder r17 = r17.append(r18)     // Catch:{ all -> 0x012a }
            java.lang.String r17 = r17.toString()     // Catch:{ all -> 0x012a }
            android.util.Log.w(r16, r17)     // Catch:{ all -> 0x012a }
            java.lang.String r9 = r7.getUid()     // Catch:{ all -> 0x012a }
            r7.setUid(r10)     // Catch:{ all -> 0x012a }
            r6.changeUid(r7)     // Catch:{ all -> 0x012a }
            java.util.Set r16 = r19.getListeners()     // Catch:{ all -> 0x012a }
            java.util.Iterator r16 = r16.iterator()     // Catch:{ all -> 0x012a }
        L_0x0112:
            boolean r17 = r16.hasNext()     // Catch:{ all -> 0x012a }
            if (r17 == 0) goto L_0x0132
            java.lang.Object r4 = r16.next()     // Catch:{ all -> 0x012a }
            com.fsck.k9.controller.MessagingListener r4 = (com.fsck.k9.controller.MessagingListener) r4     // Catch:{ all -> 0x012a }
            java.lang.String r17 = r7.getUid()     // Catch:{ all -> 0x012a }
            r0 = r21
            r1 = r17
            r4.messageUidChanged(r0, r2, r9, r1)     // Catch:{ all -> 0x012a }
            goto L_0x0112
        L_0x012a:
            r16 = move-exception
            closeFolder(r12)
            closeFolder(r6)
            throw r16
        L_0x0132:
            closeFolder(r12)
            closeFolder(r6)
            goto L_0x0028
        L_0x013a:
            java.lang.String r16 = "k9"
            java.lang.String r17 = "No remote message with message-id found, proceeding with append"
            android.util.Log.w(r16, r17)     // Catch:{ all -> 0x012a }
        L_0x0141:
            com.fsck.k9.mail.FetchProfile r3 = new com.fsck.k9.mail.FetchProfile     // Catch:{ all -> 0x012a }
            r3.<init>()     // Catch:{ all -> 0x012a }
            com.fsck.k9.mail.FetchProfile$Item r16 = com.fsck.k9.mail.FetchProfile.Item.BODY     // Catch:{ all -> 0x012a }
            r0 = r16
            r3.add(r0)     // Catch:{ all -> 0x012a }
            java.util.List r16 = java.util.Collections.singletonList(r7)     // Catch:{ all -> 0x012a }
            r17 = 0
            r0 = r16
            r1 = r17
            r6.fetch(r0, r3, r1)     // Catch:{ all -> 0x012a }
            java.lang.String r9 = r7.getUid()     // Catch:{ all -> 0x012a }
            com.fsck.k9.mail.Flag r16 = com.fsck.k9.mail.Flag.X_REMOTE_COPY_STARTED     // Catch:{ all -> 0x012a }
            r17 = 1
            r0 = r16
            r1 = r17
            r7.setFlag(r0, r1)     // Catch:{ all -> 0x012a }
            java.util.List r16 = java.util.Collections.singletonList(r7)     // Catch:{ all -> 0x012a }
            r0 = r16
            r12.appendMessages(r0)     // Catch:{ all -> 0x012a }
            r6.changeUid(r7)     // Catch:{ all -> 0x012a }
            java.util.Set r16 = r19.getListeners()     // Catch:{ all -> 0x012a }
            java.util.Iterator r16 = r16.iterator()     // Catch:{ all -> 0x012a }
        L_0x017d:
            boolean r17 = r16.hasNext()     // Catch:{ all -> 0x012a }
            if (r17 == 0) goto L_0x01c1
            java.lang.Object r4 = r16.next()     // Catch:{ all -> 0x012a }
            com.fsck.k9.controller.MessagingListener r4 = (com.fsck.k9.controller.MessagingListener) r4     // Catch:{ all -> 0x012a }
            java.lang.String r17 = r7.getUid()     // Catch:{ all -> 0x012a }
            r0 = r21
            r1 = r17
            r4.messageUidChanged(r0, r2, r9, r1)     // Catch:{ all -> 0x012a }
            goto L_0x017d
        L_0x0195:
            com.fsck.k9.mail.FetchProfile r3 = new com.fsck.k9.mail.FetchProfile     // Catch:{ all -> 0x012a }
            r3.<init>()     // Catch:{ all -> 0x012a }
            com.fsck.k9.mail.FetchProfile$Item r16 = com.fsck.k9.mail.FetchProfile.Item.ENVELOPE     // Catch:{ all -> 0x012a }
            r0 = r16
            r3.add(r0)     // Catch:{ all -> 0x012a }
            java.util.List r16 = java.util.Collections.singletonList(r13)     // Catch:{ all -> 0x012a }
            r17 = 0
            r0 = r16
            r1 = r17
            r12.fetch(r0, r3, r1)     // Catch:{ all -> 0x012a }
            java.util.Date r5 = r7.getInternalDate()     // Catch:{ all -> 0x012a }
            java.util.Date r11 = r13.getInternalDate()     // Catch:{ all -> 0x012a }
            if (r11 == 0) goto L_0x01c9
            int r16 = r11.compareTo(r5)     // Catch:{ all -> 0x012a }
            if (r16 <= 0) goto L_0x01c9
            r7.destroy()     // Catch:{ all -> 0x012a }
        L_0x01c1:
            closeFolder(r12)
            closeFolder(r6)
            goto L_0x0028
        L_0x01c9:
            com.fsck.k9.mail.FetchProfile r3 = new com.fsck.k9.mail.FetchProfile     // Catch:{ all -> 0x012a }
            r3.<init>()     // Catch:{ all -> 0x012a }
            com.fsck.k9.mail.FetchProfile$Item r16 = com.fsck.k9.mail.FetchProfile.Item.BODY     // Catch:{ all -> 0x012a }
            r0 = r16
            r3.add(r0)     // Catch:{ all -> 0x012a }
            java.util.List r16 = java.util.Collections.singletonList(r7)     // Catch:{ all -> 0x012a }
            r17 = 0
            r0 = r16
            r1 = r17
            r6.fetch(r0, r3, r1)     // Catch:{ all -> 0x012a }
            java.lang.String r9 = r7.getUid()     // Catch:{ all -> 0x012a }
            com.fsck.k9.mail.Flag r16 = com.fsck.k9.mail.Flag.X_REMOTE_COPY_STARTED     // Catch:{ all -> 0x012a }
            r17 = 1
            r0 = r16
            r1 = r17
            r7.setFlag(r0, r1)     // Catch:{ all -> 0x012a }
            java.util.List r16 = java.util.Collections.singletonList(r7)     // Catch:{ all -> 0x012a }
            r0 = r16
            r12.appendMessages(r0)     // Catch:{ all -> 0x012a }
            r6.changeUid(r7)     // Catch:{ all -> 0x012a }
            java.util.Set r16 = r19.getListeners()     // Catch:{ all -> 0x012a }
            java.util.Iterator r16 = r16.iterator()     // Catch:{ all -> 0x012a }
        L_0x0205:
            boolean r17 = r16.hasNext()     // Catch:{ all -> 0x012a }
            if (r17 == 0) goto L_0x021d
            java.lang.Object r4 = r16.next()     // Catch:{ all -> 0x012a }
            com.fsck.k9.controller.MessagingListener r4 = (com.fsck.k9.controller.MessagingListener) r4     // Catch:{ all -> 0x012a }
            java.lang.String r17 = r7.getUid()     // Catch:{ all -> 0x012a }
            r0 = r21
            r1 = r17
            r4.messageUidChanged(r0, r2, r9, r1)     // Catch:{ all -> 0x012a }
            goto L_0x0205
        L_0x021d:
            if (r11 == 0) goto L_0x01c1
            com.fsck.k9.mail.Flag r16 = com.fsck.k9.mail.Flag.DELETED     // Catch:{ all -> 0x012a }
            r17 = 1
            r0 = r16
            r1 = r17
            r13.setFlag(r0, r1)     // Catch:{ all -> 0x012a }
            com.fsck.k9.Account$Expunge r16 = com.fsck.k9.Account.Expunge.EXPUNGE_IMMEDIATELY     // Catch:{ all -> 0x012a }
            com.fsck.k9.Account$Expunge r17 = r21.getExpungePolicy()     // Catch:{ all -> 0x012a }
            r0 = r16
            r1 = r17
            if (r0 != r1) goto L_0x01c1
            r12.expunge()     // Catch:{ all -> 0x012a }
            goto L_0x01c1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fsck.k9.controller.MessagingController.processPendingAppend(com.fsck.k9.mailstore.LocalStore$PendingCommand, com.fsck.k9.Account):void");
    }

    private void queueMoveOrCopy(Account account, String srcFolder, String destFolder, boolean isCopy, String[] uids) {
        if (!account.getErrorFolderName().equals(srcFolder)) {
            LocalStore.PendingCommand command = new LocalStore.PendingCommand();
            command.command = PENDING_COMMAND_MOVE_OR_COPY_BULK_NEW;
            command.arguments = new String[(uids.length + 4)];
            command.arguments[0] = srcFolder;
            command.arguments[1] = destFolder;
            command.arguments[2] = Boolean.toString(isCopy);
            command.arguments[3] = Boolean.toString(false);
            System.arraycopy(uids, 0, command.arguments, 4, uids.length);
            queuePendingCommand(account, command);
        }
    }

    private void queueMoveOrCopy(Account account, String srcFolder, String destFolder, boolean isCopy, String[] uids, Map<String, String> uidMap) {
        if (uidMap == null || uidMap.isEmpty()) {
            queueMoveOrCopy(account, srcFolder, destFolder, isCopy, uids);
        } else if (!account.getErrorFolderName().equals(srcFolder)) {
            LocalStore.PendingCommand command = new LocalStore.PendingCommand();
            command.command = PENDING_COMMAND_MOVE_OR_COPY_BULK_NEW;
            command.arguments = new String[(uidMap.keySet().size() + 4 + uidMap.values().size())];
            command.arguments[0] = srcFolder;
            command.arguments[1] = destFolder;
            command.arguments[2] = Boolean.toString(isCopy);
            command.arguments[3] = Boolean.toString(true);
            System.arraycopy(uidMap.keySet().toArray(EMPTY_STRING_ARRAY), 0, command.arguments, 4, uidMap.keySet().size());
            System.arraycopy(uidMap.values().toArray(EMPTY_STRING_ARRAY), 0, command.arguments, uidMap.keySet().size() + 4, uidMap.values().size());
            queuePendingCommand(account, command);
        }
    }

    private void processPendingMoveOrCopyOld2(LocalStore.PendingCommand command, Account account) throws MessagingException {
        LocalStore.PendingCommand newCommand = new LocalStore.PendingCommand();
        int len = command.arguments.length;
        newCommand.command = PENDING_COMMAND_MOVE_OR_COPY_BULK_NEW;
        newCommand.arguments = new String[(len + 1)];
        newCommand.arguments[0] = command.arguments[0];
        newCommand.arguments[1] = command.arguments[1];
        newCommand.arguments[2] = command.arguments[2];
        newCommand.arguments[3] = Boolean.toString(false);
        System.arraycopy(command.arguments, 3, newCommand.arguments, 4, len - 3);
        processPendingMoveOrCopy(newCommand, account);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fsck.k9.mail.MessagingException.<init>(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.fsck.k9.mail.MessagingException.<init>(java.lang.String, java.lang.Throwable):void
      com.fsck.k9.mail.MessagingException.<init>(java.lang.String, boolean):void */
    private void processPendingMoveOrCopy(LocalStore.PendingCommand command, Account account) throws MessagingException {
        Folder<? extends Message> folder = null;
        Folder<? extends Message> folder2 = null;
        try {
            String srcFolder = command.arguments[0];
            if (!account.getErrorFolderName().equals(srcFolder)) {
                String destFolder = command.arguments[1];
                String isCopyS = command.arguments[2];
                String hasNewUidsS = command.arguments[3];
                boolean hasNewUids = false;
                if (hasNewUidsS != null) {
                    hasNewUids = Boolean.parseBoolean(hasNewUidsS);
                }
                Store remoteStore = account.getRemoteStore();
                folder = remoteStore.getFolder(srcFolder);
                LocalFolder localDestFolder = (LocalFolder) account.getLocalStore().getFolder(destFolder);
                ArrayList arrayList = new ArrayList();
                HashMap hashMap = new HashMap();
                if (hasNewUids) {
                    int offset = (command.arguments.length - 4) / 2;
                    for (int i = 4; i < offset + 4; i++) {
                        hashMap.put(command.arguments[i], command.arguments[i + offset]);
                        String uid = command.arguments[i];
                        if (!uid.startsWith(K9.LOCAL_UID_PREFIX)) {
                            arrayList.add(folder.getMessage(uid));
                        }
                    }
                } else {
                    for (int i2 = 4; i2 < command.arguments.length; i2++) {
                        String uid2 = command.arguments[i2];
                        if (!uid2.startsWith(K9.LOCAL_UID_PREFIX)) {
                            arrayList.add(folder.getMessage(uid2));
                        }
                    }
                }
                boolean isCopy = false;
                if (isCopyS != null) {
                    isCopy = Boolean.parseBoolean(isCopyS);
                }
                if (!folder.exists()) {
                    throw new MessagingException("processingPendingMoveOrCopy: remoteFolder " + srcFolder + " does not exist", true);
                }
                folder.open(0);
                if (folder.getMode() != 0) {
                    throw new MessagingException("processingPendingMoveOrCopy: could not open remoteSrcFolder " + srcFolder + " read/write", true);
                }
                if (K9.DEBUG) {
                    Log.d("k9", "processingPendingMoveOrCopy: source folder = " + srcFolder + ", " + arrayList.size() + " messages, destination folder = " + destFolder + ", isCopy = " + isCopy);
                }
                Map<String, String> remoteUidMap = null;
                if (isCopy || !destFolder.equals(account.getTrashFolderName())) {
                    folder2 = remoteStore.getFolder(destFolder);
                    if (isCopy) {
                        remoteUidMap = folder.copyMessages(arrayList, folder2);
                    } else {
                        remoteUidMap = folder.moveMessages(arrayList, folder2);
                    }
                } else {
                    if (K9.DEBUG) {
                        Log.d("k9", "processingPendingMoveOrCopy doing special case for deleting message");
                    }
                    String destFolderName = destFolder;
                    if (K9.FOLDER_NONE.equals(destFolderName)) {
                        destFolderName = null;
                    }
                    folder.delete(arrayList, destFolderName);
                }
                if (!isCopy && Account.Expunge.EXPUNGE_IMMEDIATELY == account.getExpungePolicy()) {
                    if (K9.DEBUG) {
                        Log.i("k9", "processingPendingMoveOrCopy expunging folder " + account.getDescription() + ":" + srcFolder);
                    }
                    folder.expunge();
                }
                if (!hashMap.isEmpty() && remoteUidMap != null && !remoteUidMap.isEmpty()) {
                    for (Map.Entry<String, String> entry : remoteUidMap.entrySet()) {
                        String localDestUid = (String) hashMap.get((String) entry.getKey());
                        String newUid = (String) entry.getValue();
                        Message localDestMessage = localDestFolder.getMessage(localDestUid);
                        if (localDestMessage != null) {
                            localDestMessage.setUid(newUid);
                            localDestFolder.changeUid((LocalMessage) localDestMessage);
                            for (MessagingListener l : getListeners()) {
                                l.messageUidChanged(account, destFolder, localDestUid, newUid);
                            }
                        }
                    }
                }
                closeFolder(folder);
                closeFolder(folder2);
            }
        } finally {
            closeFolder(folder);
            closeFolder(folder2);
        }
    }

    private void queueSetFlag(Account account, String folderName, String newState, String flag, String[] uids) {
        final String[] strArr = uids;
        final String str = folderName;
        final String str2 = newState;
        final String str3 = flag;
        final Account account2 = account;
        putBackground("queueSetFlag " + account.getDescription() + ":" + folderName, null, new Runnable() {
            public void run() {
                LocalStore.PendingCommand command = new LocalStore.PendingCommand();
                command.command = MessagingController.PENDING_COMMAND_SET_FLAG_BULK;
                command.arguments = new String[(strArr.length + 3)];
                command.arguments[0] = str;
                command.arguments[1] = str2;
                command.arguments[2] = str3;
                System.arraycopy(strArr, 0, command.arguments, 3, strArr.length);
                MessagingController.this.queuePendingCommand(account2, command);
                MessagingController.this.processPendingCommands(account2);
            }
        });
    }

    private void processPendingSetFlag(LocalStore.PendingCommand command, Account account) throws MessagingException {
        String folder = command.arguments[0];
        if (!account.getErrorFolderName().equals(folder)) {
            boolean newState = Boolean.parseBoolean(command.arguments[1]);
            Flag flag = Flag.valueOf(command.arguments[2]);
            Folder remoteFolder = account.getRemoteStore().getFolder(folder);
            if (remoteFolder.exists() && remoteFolder.isFlagSupported(flag)) {
                try {
                    remoteFolder.open(0);
                    if (remoteFolder.getMode() == 0) {
                        List<Message> messages = new ArrayList<>();
                        for (int i = 3; i < command.arguments.length; i++) {
                            String uid = command.arguments[i];
                            if (!uid.startsWith(K9.LOCAL_UID_PREFIX)) {
                                messages.add(remoteFolder.getMessage(uid));
                            }
                        }
                        if (messages.isEmpty()) {
                            closeFolder(remoteFolder);
                            return;
                        }
                        remoteFolder.setFlags(messages, Collections.singleton(flag), newState);
                        closeFolder(remoteFolder);
                    }
                } finally {
                    closeFolder(remoteFolder);
                }
            }
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void processPendingSetFlagOld(com.fsck.k9.mailstore.LocalStore.PendingCommand r11, com.fsck.k9.Account r12) throws com.fsck.k9.mail.MessagingException {
        /*
            r10 = this;
            r8 = 0
            java.lang.String[] r7 = r11.arguments
            r1 = r7[r8]
            java.lang.String[] r7 = r11.arguments
            r8 = 1
            r6 = r7[r8]
            java.lang.String r7 = r12.getErrorFolderName()
            boolean r7 = r7.equals(r1)
            if (r7 == 0) goto L_0x0015
        L_0x0014:
            return
        L_0x0015:
            boolean r7 = com.fsck.k9.K9.DEBUG
            if (r7 == 0) goto L_0x003b
            java.lang.String r7 = "k9"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "processPendingSetFlagOld: folder = "
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.StringBuilder r8 = r8.append(r1)
            java.lang.String r9 = ", uid = "
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.StringBuilder r8 = r8.append(r6)
            java.lang.String r8 = r8.toString()
            android.util.Log.d(r7, r8)
        L_0x003b:
            java.lang.String[] r7 = r11.arguments
            r8 = 2
            r7 = r7[r8]
            boolean r2 = java.lang.Boolean.parseBoolean(r7)
            java.lang.String[] r7 = r11.arguments
            r8 = 3
            r7 = r7[r8]
            com.fsck.k9.mail.Flag r0 = com.fsck.k9.mail.Flag.valueOf(r7)
            r3 = 0
            com.fsck.k9.mail.Store r5 = r12.getRemoteStore()     // Catch:{ all -> 0x0088 }
            com.fsck.k9.mail.Folder r3 = r5.getFolder(r1)     // Catch:{ all -> 0x0088 }
            boolean r7 = r3.exists()     // Catch:{ all -> 0x0088 }
            if (r7 != 0) goto L_0x0060
            closeFolder(r3)
            goto L_0x0014
        L_0x0060:
            r7 = 0
            r3.open(r7)     // Catch:{ all -> 0x0088 }
            int r7 = r3.getMode()     // Catch:{ all -> 0x0088 }
            if (r7 == 0) goto L_0x006e
            closeFolder(r3)
            goto L_0x0014
        L_0x006e:
            r4 = 0
            java.lang.String r7 = "K9LOCAL:"
            boolean r7 = r6.startsWith(r7)     // Catch:{ all -> 0x0088 }
            if (r7 != 0) goto L_0x007b
            com.fsck.k9.mail.Message r4 = r3.getMessage(r6)     // Catch:{ all -> 0x0088 }
        L_0x007b:
            if (r4 != 0) goto L_0x0081
            closeFolder(r3)
            goto L_0x0014
        L_0x0081:
            r4.setFlag(r0, r2)     // Catch:{ all -> 0x0088 }
            closeFolder(r3)
            goto L_0x0014
        L_0x0088:
            r7 = move-exception
            closeFolder(r3)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fsck.k9.controller.MessagingController.processPendingSetFlagOld(com.fsck.k9.mailstore.LocalStore$PendingCommand, com.fsck.k9.Account):void");
    }

    /* access modifiers changed from: private */
    public void queueExpunge(final Account account, final String folderName) {
        putBackground("queueExpunge " + account.getDescription() + ":" + folderName, null, new Runnable() {
            public void run() {
                LocalStore.PendingCommand command = new LocalStore.PendingCommand();
                command.command = MessagingController.PENDING_COMMAND_EXPUNGE;
                command.arguments = new String[1];
                command.arguments[0] = folderName;
                MessagingController.this.queuePendingCommand(account, command);
                MessagingController.this.processPendingCommands(account);
            }
        });
    }

    private void processPendingExpunge(LocalStore.PendingCommand command, Account account) throws MessagingException {
        String folder = command.arguments[0];
        if (!account.getErrorFolderName().equals(folder)) {
            if (K9.DEBUG) {
                Log.d("k9", "processPendingExpunge: folder = " + folder);
            }
            Folder remoteFolder = account.getRemoteStore().getFolder(folder);
            try {
                if (remoteFolder.exists()) {
                    remoteFolder.open(0);
                    if (remoteFolder.getMode() != 0) {
                        closeFolder(remoteFolder);
                        return;
                    }
                    remoteFolder.expunge();
                    if (K9.DEBUG) {
                        Log.d("k9", "processPendingExpunge: complete for folder = " + folder);
                    }
                    closeFolder(remoteFolder);
                }
            } finally {
                closeFolder(remoteFolder);
            }
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fsck.k9.mail.MessagingException.<init>(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.fsck.k9.mail.MessagingException.<init>(java.lang.String, java.lang.Throwable):void
      com.fsck.k9.mail.MessagingException.<init>(java.lang.String, boolean):void */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void processPendingMoveOrCopyOld(com.fsck.k9.mailstore.LocalStore.PendingCommand r13, com.fsck.k9.Account r14) throws com.fsck.k9.mail.MessagingException {
        /*
            r12 = this;
            java.lang.String[] r9 = r13.arguments
            r10 = 0
            r7 = r9[r10]
            java.lang.String[] r9 = r13.arguments
            r10 = 1
            r8 = r9[r10]
            java.lang.String[] r9 = r13.arguments
            r10 = 2
            r0 = r9[r10]
            java.lang.String[] r9 = r13.arguments
            r10 = 3
            r2 = r9[r10]
            r1 = 0
            if (r2 == 0) goto L_0x001b
            boolean r1 = java.lang.Boolean.parseBoolean(r2)
        L_0x001b:
            java.lang.String r9 = r14.getErrorFolderName()
            boolean r9 = r9.equals(r7)
            if (r9 == 0) goto L_0x0026
        L_0x0025:
            return
        L_0x0026:
            com.fsck.k9.mail.Store r6 = r14.getRemoteStore()
            com.fsck.k9.mail.Folder r5 = r6.getFolder(r7)
            com.fsck.k9.mail.Folder r3 = r6.getFolder(r0)
            boolean r9 = r5.exists()
            if (r9 != 0) goto L_0x0058
            com.fsck.k9.mail.MessagingException r9 = new com.fsck.k9.mail.MessagingException
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = "processPendingMoveOrCopyOld: remoteFolder "
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.StringBuilder r10 = r10.append(r7)
            java.lang.String r11 = " does not exist"
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.String r10 = r10.toString()
            r11 = 1
            r9.<init>(r10, r11)
            throw r9
        L_0x0058:
            r9 = 0
            r5.open(r9)
            int r9 = r5.getMode()
            if (r9 == 0) goto L_0x0082
            com.fsck.k9.mail.MessagingException r9 = new com.fsck.k9.mail.MessagingException
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = "processPendingMoveOrCopyOld: could not open remoteSrcFolder "
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.StringBuilder r10 = r10.append(r7)
            java.lang.String r11 = " read/write"
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.String r10 = r10.toString()
            r11 = 1
            r9.<init>(r10, r11)
            throw r9
        L_0x0082:
            r4 = 0
            java.lang.String r9 = "K9LOCAL:"
            boolean r9 = r8.startsWith(r9)
            if (r9 != 0) goto L_0x008f
            com.fsck.k9.mail.Message r4 = r5.getMessage(r8)
        L_0x008f:
            if (r4 != 0) goto L_0x00b1
            com.fsck.k9.mail.MessagingException r9 = new com.fsck.k9.mail.MessagingException
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = "processPendingMoveOrCopyOld: remoteMessage "
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.StringBuilder r10 = r10.append(r8)
            java.lang.String r11 = " does not exist"
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.String r10 = r10.toString()
            r11 = 1
            r9.<init>(r10, r11)
            throw r9
        L_0x00b1:
            boolean r9 = com.fsck.k9.K9.DEBUG
            if (r9 == 0) goto L_0x00eb
            java.lang.String r9 = "k9"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = "processPendingMoveOrCopyOld: source folder = "
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.StringBuilder r10 = r10.append(r7)
            java.lang.String r11 = ", uid = "
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.StringBuilder r10 = r10.append(r8)
            java.lang.String r11 = ", destination folder = "
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.StringBuilder r10 = r10.append(r0)
            java.lang.String r11 = ", isCopy = "
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.StringBuilder r10 = r10.append(r1)
            java.lang.String r10 = r10.toString()
            android.util.Log.d(r9, r10)
        L_0x00eb:
            if (r1 != 0) goto L_0x010e
            java.lang.String r9 = r14.getTrashFolderName()
            boolean r9 = r0.equals(r9)
            if (r9 == 0) goto L_0x010e
            boolean r9 = com.fsck.k9.K9.DEBUG
            if (r9 == 0) goto L_0x0102
            java.lang.String r9 = "k9"
            java.lang.String r10 = "processPendingMoveOrCopyOld doing special case for deleting message"
            android.util.Log.d(r9, r10)
        L_0x0102:
            java.lang.String r9 = r14.getTrashFolderName()
            r4.delete(r9)
            r5.close()
            goto L_0x0025
        L_0x010e:
            r9 = 0
            r3.open(r9)
            int r9 = r3.getMode()
            if (r9 == 0) goto L_0x0138
            com.fsck.k9.mail.MessagingException r9 = new com.fsck.k9.mail.MessagingException
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = "processPendingMoveOrCopyOld: could not open remoteDestFolder "
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.StringBuilder r10 = r10.append(r7)
            java.lang.String r11 = " read/write"
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.String r10 = r10.toString()
            r11 = 1
            r9.<init>(r10, r11)
            throw r9
        L_0x0138:
            if (r1 == 0) goto L_0x0149
            java.util.List r9 = java.util.Collections.singletonList(r4)
            r5.copyMessages(r9, r3)
        L_0x0141:
            r5.close()
            r3.close()
            goto L_0x0025
        L_0x0149:
            java.util.List r9 = java.util.Collections.singletonList(r4)
            r5.moveMessages(r9, r3)
            goto L_0x0141
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fsck.k9.controller.MessagingController.processPendingMoveOrCopyOld(com.fsck.k9.mailstore.LocalStore$PendingCommand, com.fsck.k9.Account):void");
    }

    /* JADX WARN: Type inference failed for: r10v3, types: [com.fsck.k9.mail.Folder] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void processPendingMarkAllAsRead(com.fsck.k9.mailstore.LocalStore.PendingCommand r14, com.fsck.k9.Account r15) throws com.fsck.k9.mail.MessagingException {
        /*
            r13 = this;
            r11 = 0
            java.lang.String[] r10 = r14.arguments
            r1 = r10[r11]
            r7 = 0
            r3 = 0
            com.fsck.k9.mailstore.LocalStore r4 = r15.getLocalStore()     // Catch:{ UnsupportedOperationException -> 0x0053 }
            com.fsck.k9.mail.Folder r10 = r4.getFolder(r1)     // Catch:{ UnsupportedOperationException -> 0x0053 }
            r0 = r10
            com.fsck.k9.mailstore.LocalFolder r0 = (com.fsck.k9.mailstore.LocalFolder) r0     // Catch:{ UnsupportedOperationException -> 0x0053 }
            r3 = r0
            r10 = 0
            r3.open(r10)     // Catch:{ UnsupportedOperationException -> 0x0053 }
            r10 = 0
            r11 = 0
            java.util.List r6 = r3.getMessages(r10, r11)     // Catch:{ UnsupportedOperationException -> 0x0053 }
            java.util.Iterator r10 = r6.iterator()     // Catch:{ UnsupportedOperationException -> 0x0053 }
        L_0x0021:
            boolean r11 = r10.hasNext()     // Catch:{ UnsupportedOperationException -> 0x0053 }
            if (r11 == 0) goto L_0x0062
            java.lang.Object r5 = r10.next()     // Catch:{ UnsupportedOperationException -> 0x0053 }
            com.fsck.k9.mail.Message r5 = (com.fsck.k9.mail.Message) r5     // Catch:{ UnsupportedOperationException -> 0x0053 }
            com.fsck.k9.mail.Flag r11 = com.fsck.k9.mail.Flag.SEEN     // Catch:{ UnsupportedOperationException -> 0x0053 }
            boolean r11 = r5.isSet(r11)     // Catch:{ UnsupportedOperationException -> 0x0053 }
            if (r11 != 0) goto L_0x0021
            com.fsck.k9.mail.Flag r11 = com.fsck.k9.mail.Flag.SEEN     // Catch:{ UnsupportedOperationException -> 0x0053 }
            r12 = 1
            r5.setFlag(r11, r12)     // Catch:{ UnsupportedOperationException -> 0x0053 }
            java.util.Set r11 = r13.getListeners()     // Catch:{ UnsupportedOperationException -> 0x0053 }
            java.util.Iterator r11 = r11.iterator()     // Catch:{ UnsupportedOperationException -> 0x0053 }
        L_0x0043:
            boolean r12 = r11.hasNext()     // Catch:{ UnsupportedOperationException -> 0x0053 }
            if (r12 == 0) goto L_0x0021
            java.lang.Object r2 = r11.next()     // Catch:{ UnsupportedOperationException -> 0x0053 }
            com.fsck.k9.controller.MessagingListener r2 = (com.fsck.k9.controller.MessagingListener) r2     // Catch:{ UnsupportedOperationException -> 0x0053 }
            r2.listLocalMessagesUpdateMessage(r15, r1, r5)     // Catch:{ UnsupportedOperationException -> 0x0053 }
            goto L_0x0043
        L_0x0053:
            r9 = move-exception
            java.lang.String r10 = "k9"
            java.lang.String r11 = "Could not mark all server-side as read because store doesn't support operation"
            android.util.Log.w(r10, r11, r9)     // Catch:{ all -> 0x007b }
            closeFolder(r3)
            closeFolder(r7)
        L_0x0061:
            return
        L_0x0062:
            java.util.Set r10 = r13.getListeners()     // Catch:{ UnsupportedOperationException -> 0x0053 }
            java.util.Iterator r10 = r10.iterator()     // Catch:{ UnsupportedOperationException -> 0x0053 }
        L_0x006a:
            boolean r11 = r10.hasNext()     // Catch:{ UnsupportedOperationException -> 0x0053 }
            if (r11 == 0) goto L_0x0083
            java.lang.Object r2 = r10.next()     // Catch:{ UnsupportedOperationException -> 0x0053 }
            com.fsck.k9.controller.MessagingListener r2 = (com.fsck.k9.controller.MessagingListener) r2     // Catch:{ UnsupportedOperationException -> 0x0053 }
            r11 = 0
            r2.folderStatusChanged(r15, r1, r11)     // Catch:{ UnsupportedOperationException -> 0x0053 }
            goto L_0x006a
        L_0x007b:
            r10 = move-exception
            closeFolder(r3)
            closeFolder(r7)
            throw r10
        L_0x0083:
            java.lang.String r10 = r15.getErrorFolderName()     // Catch:{ UnsupportedOperationException -> 0x0053 }
            boolean r10 = r10.equals(r1)     // Catch:{ UnsupportedOperationException -> 0x0053 }
            if (r10 == 0) goto L_0x0094
            closeFolder(r3)
            closeFolder(r7)
            goto L_0x0061
        L_0x0094:
            com.fsck.k9.mail.Store r8 = r15.getRemoteStore()     // Catch:{ UnsupportedOperationException -> 0x0053 }
            com.fsck.k9.mail.Folder r7 = r8.getFolder(r1)     // Catch:{ UnsupportedOperationException -> 0x0053 }
            boolean r10 = r7.exists()     // Catch:{ UnsupportedOperationException -> 0x0053 }
            if (r10 == 0) goto L_0x00aa
            com.fsck.k9.mail.Flag r10 = com.fsck.k9.mail.Flag.SEEN     // Catch:{ UnsupportedOperationException -> 0x0053 }
            boolean r10 = r7.isFlagSupported(r10)     // Catch:{ UnsupportedOperationException -> 0x0053 }
            if (r10 != 0) goto L_0x00b1
        L_0x00aa:
            closeFolder(r3)
            closeFolder(r7)
            goto L_0x0061
        L_0x00b1:
            r10 = 0
            r7.open(r10)     // Catch:{ UnsupportedOperationException -> 0x0053 }
            int r10 = r7.getMode()     // Catch:{ UnsupportedOperationException -> 0x0053 }
            if (r10 == 0) goto L_0x00c2
            closeFolder(r3)
            closeFolder(r7)
            goto L_0x0061
        L_0x00c2:
            com.fsck.k9.mail.Flag r10 = com.fsck.k9.mail.Flag.SEEN     // Catch:{ UnsupportedOperationException -> 0x0053 }
            java.util.Set r10 = java.util.Collections.singleton(r10)     // Catch:{ UnsupportedOperationException -> 0x0053 }
            r11 = 1
            r7.setFlags(r10, r11)     // Catch:{ UnsupportedOperationException -> 0x0053 }
            r7.close()     // Catch:{ UnsupportedOperationException -> 0x0053 }
            closeFolder(r3)
            closeFolder(r7)
            goto L_0x0061
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fsck.k9.controller.MessagingController.processPendingMarkAllAsRead(com.fsck.k9.mailstore.LocalStore$PendingCommand, com.fsck.k9.Account):void");
    }

    public void addErrorMessage(Account account, String subject, Throwable t) {
        addErrorMessage(account, subject, t, this.context);
    }

    public static void addErrorMessage(Account account, String subject, Throwable t, Context ctx) {
        if (K9.DEBUG && t != null) {
            try {
                CharArrayWriter baos = new CharArrayWriter(t.getStackTrace().length * 10);
                PrintWriter ps = new PrintWriter(baos);
                try {
                    ps.format("K9-Mail version: %s\r\n", ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0).versionName);
                } catch (Exception e) {
                }
                ps.format("Device make: %s\r\n", Build.MANUFACTURER);
                ps.format("Device model: %s\r\n", Build.MODEL);
                ps.format("Android version: %s\r\n\r\n", Build.VERSION.RELEASE);
                t.printStackTrace(ps);
                ps.close();
                if (subject == null) {
                    subject = getRootCauseMessage(t);
                }
                addErrorMessage(account, subject, baos.toString());
            } catch (Throwable it) {
                Log.e("k9", "Could not save error message to " + account.getErrorFolderName(), it);
            }
        }
    }

    /* access modifiers changed from: private */
    public static void addErrorMessage(Account account, String subject, String body) {
        if (K9.DEBUG && loopCatch.compareAndSet(false, true)) {
            if (body != null) {
                try {
                    if (body.length() >= 1) {
                        LocalFolder localFolder = (LocalFolder) account.getLocalStore().getFolder(account.getErrorFolderName());
                        MimeMessage message = new MimeMessage();
                        MimeMessageHelper.setBody(message, new TextBody(body));
                        message.setFlag(Flag.X_DOWNLOADED_FULL, true);
                        message.setSubject(subject);
                        long nowTime = System.currentTimeMillis();
                        Date nowDate = new Date(nowTime);
                        message.setInternalDate(nowDate);
                        message.addSentDate(nowDate, K9.hideTimeZone());
                        message.setFrom(new Address(account.getEmail(), "K9mail internal"));
                        localFolder.appendMessages(Collections.singletonList(message));
                        localFolder.clearMessagesOlderThan(nowTime - 900000);
                        return;
                    }
                } catch (Throwable it) {
                    Log.e("k9", "Could not save error message to " + account.getErrorFolderName(), it);
                    return;
                } finally {
                    loopCatch.set(false);
                }
            }
            loopCatch.set(false);
        }
    }

    public void markAllMessagesRead(Account account, String folder) {
        if (K9.DEBUG) {
            Log.i("k9", "Marking all messages in " + account.getDescription() + ":" + folder + " as read");
        }
        LocalStore.PendingCommand command = new LocalStore.PendingCommand();
        command.command = PENDING_COMMAND_MARK_ALL_AS_READ;
        command.arguments = new String[]{folder};
        queuePendingCommand(account, command);
        processPendingCommands(account);
    }

    public void setFlag(Account account, List<Long> messageIds, Flag flag, boolean newState) {
        setFlagInCache(account, messageIds, flag, newState);
        final Account account2 = account;
        final List<Long> list = messageIds;
        final Flag flag2 = flag;
        final boolean z = newState;
        this.threadPool.execute(new Runnable() {
            public void run() {
                MessagingController.this.setFlagSynchronous(account2, list, flag2, z, false);
            }
        });
    }

    public void setFlagForThreads(Account account, List<Long> threadRootIds, Flag flag, boolean newState) {
        setFlagForThreadsInCache(account, threadRootIds, flag, newState);
        final Account account2 = account;
        final List<Long> list = threadRootIds;
        final Flag flag2 = flag;
        final boolean z = newState;
        this.threadPool.execute(new Runnable() {
            public void run() {
                MessagingController.this.setFlagSynchronous(account2, list, flag2, z, true);
            }
        });
    }

    /* access modifiers changed from: private */
    public void setFlagSynchronous(Account account, List<Long> ids, Flag flag, boolean newState, boolean threadedList) {
        try {
            LocalStore localStore = account.getLocalStore();
            if (threadedList) {
                try {
                    localStore.setFlagForThreads(ids, flag, newState);
                    removeFlagForThreadsFromCache(account, ids, flag);
                } catch (MessagingException e) {
                    Log.e("k9", "Couldn't set flags in local database", e);
                }
            } else {
                localStore.setFlag(ids, flag, newState);
                removeFlagFromCache(account, ids, flag);
            }
            try {
                for (Map.Entry<String, List<String>> entry : localStore.getFoldersAndUids(ids, threadedList).entrySet()) {
                    String folderName = (String) entry.getKey();
                    try {
                        int unreadMessageCount = localStore.getFolder(folderName).getUnreadMessageCount();
                        for (MessagingListener l : getListeners()) {
                            l.folderStatusChanged(account, folderName, unreadMessageCount);
                        }
                    } catch (MessagingException e2) {
                        Log.w("k9", "Couldn't get unread count for folder: " + folderName, e2);
                    }
                    if (!account.getErrorFolderName().equals(folderName)) {
                        List<String> value = entry.getValue();
                        String bool = Boolean.toString(newState);
                        String flag2 = flag.toString();
                        Account account2 = account;
                        queueSetFlag(account2, folderName, bool, flag2, (String[]) value.toArray(new String[value.size()]));
                        processPendingCommands(account);
                    }
                }
            } catch (MessagingException e3) {
                Log.e("k9", "Couldn't get folder name and UID of messages", e3);
            }
        } catch (MessagingException e4) {
            Log.e("k9", "Couldn't get LocalStore instance", e4);
        }
    }

    public void setFlag(Account account, String folderName, List<? extends Message> messages, Flag flag, boolean newState) {
        Folder localFolder = null;
        try {
            localFolder = account.getLocalStore().getFolder(folderName);
            localFolder.open(0);
            if (flag == Flag.FLAGGED && !newState && account.getOutboxFolderName().equals(folderName)) {
                for (Message message : messages) {
                    String uid = message.getUid();
                    if (uid != null) {
                        this.sendCount.remove(uid);
                    }
                }
            }
            localFolder.setFlags(messages, Collections.singleton(flag), newState);
            int unreadMessageCount = localFolder.getUnreadMessageCount();
            for (MessagingListener l : getListeners()) {
                l.folderStatusChanged(account, folderName, unreadMessageCount);
            }
            if (account.getErrorFolderName().equals(folderName)) {
                closeFolder(localFolder);
                return;
            }
            String[] uids = new String[messages.size()];
            int end = uids.length;
            for (int i = 0; i < end; i++) {
                uids[i] = ((Message) messages.get(i)).getUid();
            }
            queueSetFlag(account, folderName, Boolean.toString(newState), flag.toString(), uids);
            processPendingCommands(account);
            closeFolder(localFolder);
        } catch (MessagingException me2) {
            addErrorMessage(account, (String) null, me2);
            throw new RuntimeException(me2);
        } catch (Throwable th) {
            closeFolder(localFolder);
            throw th;
        }
    }

    public void setFlag(Account account, String folderName, String uid, Flag flag, boolean newState) {
        try {
            Folder localFolder = account.getLocalStore().getFolder(folderName);
            localFolder.open(0);
            Message message = localFolder.getMessage(uid);
            if (message != null) {
                setFlag(account, folderName, Collections.singletonList(message), flag, newState);
            }
            closeFolder(localFolder);
        } catch (MessagingException me2) {
            addErrorMessage(account, (String) null, me2);
            throw new RuntimeException(me2);
        } catch (Throwable th) {
            closeFolder(null);
            throw th;
        }
    }

    public void clearAllPending(Account account) {
        try {
            Log.w("k9", "Clearing pending commands!");
            account.getLocalStore().removePendingCommands();
        } catch (MessagingException me2) {
            Log.e("k9", "Unable to clear pending command", me2);
            addErrorMessage(account, (String) null, me2);
        }
    }

    public void loadMessageRemotePartial(Account account, String folder, String uid, MessagingListener listener) {
        final Account account2 = account;
        final String str = folder;
        final String str2 = uid;
        final MessagingListener messagingListener = listener;
        put("loadMessageRemotePartial", listener, new Runnable() {
            public void run() {
                boolean unused = MessagingController.this.loadMessageRemoteSynchronous(account2, str, str2, messagingListener, true);
            }
        });
    }

    public void loadMessageRemote(Account account, String folder, String uid, MessagingListener listener) {
        final Account account2 = account;
        final String str = folder;
        final String str2 = uid;
        final MessagingListener messagingListener = listener;
        put("loadMessageRemote", listener, new Runnable() {
            public void run() {
                boolean unused = MessagingController.this.loadMessageRemoteSynchronous(account2, str, str2, messagingListener, false);
            }
        });
    }

    /* access modifiers changed from: private */
    public boolean loadMessageRemoteSynchronous(Account account, String folder, String uid, MessagingListener listener, boolean loadPartialFromSearch) {
        boolean z;
        String str;
        String str2;
        Folder remoteFolder = null;
        LocalFolder localFolder = null;
        try {
            localFolder = account.getLocalStore().getFolder(folder);
            localFolder.open(0);
            LocalMessage message = localFolder.getMessage(uid);
            if (uid.startsWith(K9.LOCAL_UID_PREFIX)) {
                Log.w("k9", "Message has local UID so cannot download fully.");
                Toast.makeText(this.context, "Message has local UID so cannot download fully", 1).show();
                message.setFlag(Flag.X_DOWNLOADED_FULL, true);
                message.setFlag(Flag.X_DOWNLOADED_PARTIAL, false);
            }
            if (this.isFetchingSearchList) {
                this.stopFetchingSearchedMessages = true;
            }
            synchronized (this.isFetchingMessageBody) {
                if (K9.DEBUG) {
                    Log.e("AtomicGonza", "okAG fetching Body Message " + new Exception().getStackTrace()[0].toString());
                }
                remoteFolder = account.getRemoteStore().getFolder(folder);
                remoteFolder.open(0);
                Message remoteMessage = remoteFolder.getMessage(uid);
                if (loadPartialFromSearch) {
                    downloadMessages(account, remoteFolder, localFolder, Collections.singletonList(remoteMessage), false, false);
                } else {
                    FetchProfile fp = new FetchProfile();
                    fp.add(FetchProfile.Item.BODY);
                    remoteFolder.fetch(Collections.singletonList(remoteMessage), fp, null);
                    localFolder.appendMessages(Collections.singletonList(remoteMessage));
                }
                LocalMessage message2 = localFolder.getMessage(uid);
                if (!loadPartialFromSearch) {
                    message2.setFlag(Flag.X_DOWNLOADED_FULL, true);
                }
                if (account.isMarkMessageAsReadOnView()) {
                    message2.setFlag(Flag.SEEN, true);
                }
                for (MessagingListener l : getListeners(listener)) {
                    l.loadMessageRemoteFinished(account, folder, uid);
                }
            }
            z = true;
            if (K9.DEBUG) {
                Log.e("AtomicGonza", "okAG Closing folder after fetching body " + new Exception().getStackTrace()[0].toString());
            }
            closeFolder(remoteFolder);
            closeFolder(localFolder);
        } catch (Exception e) {
            try {
                for (MessagingListener l2 : getListeners(listener)) {
                    l2.loadMessageRemoteFailed(account, folder, uid, e);
                }
                notifyUserIfCertificateProblem(account, e, true);
                addErrorMessage(account, (String) null, e);
                z = false;
            } finally {
                if (K9.DEBUG) {
                    str = "AtomicGonza";
                    str2 = "okAG Closing folder after fetching body ";
                    Log.e(str, str2 + new Exception().getStackTrace()[0].toString());
                }
                closeFolder(remoteFolder);
                closeFolder(localFolder);
            }
        }
        return z;
    }

    public LocalMessage loadMessage(Account account, String folderName, String uid) throws MessagingException {
        LocalFolder localFolder = account.getLocalStore().getFolder(folderName);
        localFolder.open(0);
        LocalMessage message = localFolder.getMessage(uid);
        if (message == null || message.getId() == 0) {
            throw new IllegalArgumentException("Message not found: folder=" + folderName + ", uid=" + uid);
        }
        FetchProfile fp = new FetchProfile();
        fp.add(FetchProfile.Item.BODY);
        localFolder.fetch(Collections.singletonList(message), fp, null);
        localFolder.close();
        this.notificationController.removeNewMailNotification(account, message.makeMessageReference());
        markMessageAsReadOnView(account, message);
        return message;
    }

    private void markMessageAsReadOnView(Account account, LocalMessage message) throws MessagingException {
        if (account.isMarkMessageAsReadOnView() && !message.isSet(Flag.SEEN)) {
            setFlag(account, Collections.singletonList(Long.valueOf(message.getId())), Flag.SEEN, true);
            message.setFlagInternal(Flag.SEEN, true);
        }
    }

    public void loadAttachment(Account account, LocalMessage message, Part part, MessagingListener listener) {
        final LocalMessage localMessage = message;
        final Account account2 = account;
        final Part part2 = part;
        final MessagingListener messagingListener = listener;
        put("loadAttachment", listener, new Runnable() {
            public void run() {
                Folder remoteFolder = null;
                LocalFolder localFolder = null;
                try {
                    String folderName = localMessage.getFolder().getName();
                    localFolder = account2.getLocalStore().getFolder(folderName);
                    remoteFolder = account2.getRemoteStore().getFolder(folderName);
                    remoteFolder.open(0);
                    remoteFolder.fetchPart(remoteFolder.getMessage(localMessage.getUid()), part2, null);
                    localFolder.addPartToMessage(localMessage, part2);
                    for (MessagingListener l : MessagingController.this.getListeners(messagingListener)) {
                        l.loadAttachmentFinished(account2, localMessage, part2);
                    }
                } catch (MessagingException me2) {
                    if (K9.DEBUG) {
                        Log.v("k9", "Exception loading attachment", me2);
                    }
                    for (MessagingListener l2 : MessagingController.this.getListeners(messagingListener)) {
                        l2.loadAttachmentFailed(account2, localMessage, part2, me2.getMessage());
                    }
                    MessagingController.this.notifyUserIfCertificateProblem(account2, me2, true);
                    MessagingController.this.addErrorMessage(account2, (String) null, me2);
                } finally {
                    MessagingController.closeFolder(localFolder);
                    MessagingController.closeFolder(remoteFolder);
                }
            }
        });
    }

    public void sendMessage(Account account, Message message, MessagingListener listener) {
        try {
            LocalFolder localFolder = account.getLocalStore().getFolder(account.getOutboxFolderName());
            localFolder.open(0);
            localFolder.appendMessages(Collections.singletonList(message));
            localFolder.getMessage(message.getUid()).setFlag(Flag.X_DOWNLOADED_FULL, true);
            localFolder.close();
            sendPendingMessages(account, listener);
        } catch (Exception e) {
            addErrorMessage(account, (String) null, e);
        }
    }

    public void sendPendingMessages(MessagingListener listener) {
        for (Account account : Preferences.getPreferences(this.context).getAvailableAccounts()) {
            sendPendingMessages(account, listener);
        }
    }

    public void sendPendingMessages(final Account account, final MessagingListener listener) {
        if (account != null) {
            String mail = account.getEmail();
            if (GmailUtils.isGmailAccount(mail)) {
                SharedPreferences pref = App.getPrefToken(this.context);
                long timeExpireToken = App.getTimeExpireTokenGmailAccount(pref, mail);
                if (timeExpireToken > 3600000 && timeExpireToken < System.currentTimeMillis()) {
                    GmailUtils.queryAccessToken(App.getRefreshTokenGmailAccount(pref, mail), this.context, account, true, false, new GmailUtils.CallBackAccessToken() {
                        public void sucess() {
                            MessagingController.this.addErrorMessage(account, "Sucess getting token at MessagingController.java 3129 ", new Exception());
                            MessagingController.this.sendPendingMessages(account, listener);
                        }

                        public void error(String msg) {
                            MessagingController.this.handleAuthenticationFailure(account, true);
                            String msg2 = "Error getting token Error: " + msg + " at MessagingController.java 3138";
                            MessagingController.addErrorMessage(account, msg2, msg2);
                        }
                    });
                    return;
                }
            }
        }
        putBackground("sendPendingMessages", listener, new Runnable() {
            public void run() {
                if (!account.isAvailable(MessagingController.this.context)) {
                    throw new UnavailableAccountException();
                } else if (MessagingController.this.messagesPendingSend(account)) {
                    MessagingController.this.showSendingNotificationIfNecessary(account);
                    try {
                        MessagingController.this.sendPendingMessagesSynchronous(account);
                    } finally {
                        MessagingController.this.clearSendingNotificationIfNecessary(account);
                    }
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void showSendingNotificationIfNecessary(Account account) {
        if (account.isShowOngoing()) {
            this.notificationController.showSendingNotification(account);
        }
    }

    /* access modifiers changed from: private */
    public void clearSendingNotificationIfNecessary(Account account) {
        if (account.isShowOngoing()) {
            this.notificationController.clearSendingNotification(account);
        }
    }

    /* access modifiers changed from: private */
    public boolean messagesPendingSend(Account account) {
        Folder localFolder = null;
        try {
            localFolder = account.getLocalStore().getFolder(account.getOutboxFolderName());
            if (!localFolder.exists()) {
                return false;
            }
            localFolder.open(0);
            if (localFolder.getMessageCount() > 0) {
                closeFolder(localFolder);
                return true;
            }
            closeFolder(localFolder);
            return false;
        } catch (Exception e) {
            Log.e("k9", "Exception while checking for unsent messages", e);
            return false;
        } finally {
            closeFolder(localFolder);
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:120:?, code lost:
        addErrorMessage(r27, (java.lang.String) null, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x03d5, code lost:
        if (r15 == null) goto L_0x03d7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x03d7, code lost:
        r0.notificationController.clearSendFailedNotification(r27);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x03e0, code lost:
        closeFolder(r7);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003e, code lost:
        r9 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:140:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        android.util.Log.i("k9", "Failed to send pending messages because storage is not available - trying again later.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x004b, code lost:
        throw new com.fsck.k9.controller.UnavailableAccountException(r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x008d, code lost:
        r9 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        r4 = getListeners().iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x009a, code lost:
        if (r4.hasNext() != false) goto L_0x009c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x009c, code lost:
        r4.next().sendPendingMessagesFailed(r27);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x01c0, code lost:
        r9 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x01c1, code lost:
        r15 = r9;
        r10 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:?, code lost:
        android.util.Log.e("k9", "Failed to fetch message for sending", r9);
        addErrorMessage(r27, "Failed to fetch message for sending", r9);
        notifySynchronizeMailboxFailed(r27, r7, r9);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003e A[ExcHandler: UnavailableStorageException (r9v1 'e' com.fsck.k9.mailstore.UnavailableStorageException A[CUSTOM_DECLARE]), PHI: r7 r15 
      PHI: (r7v3 'localFolder' com.fsck.k9.mailstore.LocalFolder) = (r7v0 'localFolder' com.fsck.k9.mailstore.LocalFolder), (r7v4 'localFolder' com.fsck.k9.mailstore.LocalFolder), (r7v4 'localFolder' com.fsck.k9.mailstore.LocalFolder), (r7v4 'localFolder' com.fsck.k9.mailstore.LocalFolder), (r7v4 'localFolder' com.fsck.k9.mailstore.LocalFolder), (r7v4 'localFolder' com.fsck.k9.mailstore.LocalFolder), (r7v4 'localFolder' com.fsck.k9.mailstore.LocalFolder), (r7v4 'localFolder' com.fsck.k9.mailstore.LocalFolder), (r7v4 'localFolder' com.fsck.k9.mailstore.LocalFolder), (r7v4 'localFolder' com.fsck.k9.mailstore.LocalFolder), (r7v4 'localFolder' com.fsck.k9.mailstore.LocalFolder), (r7v4 'localFolder' com.fsck.k9.mailstore.LocalFolder), (r7v4 'localFolder' com.fsck.k9.mailstore.LocalFolder), (r7v4 'localFolder' com.fsck.k9.mailstore.LocalFolder), (r7v4 'localFolder' com.fsck.k9.mailstore.LocalFolder), (r7v4 'localFolder' com.fsck.k9.mailstore.LocalFolder), (r7v4 'localFolder' com.fsck.k9.mailstore.LocalFolder), (r7v4 'localFolder' com.fsck.k9.mailstore.LocalFolder), (r7v4 'localFolder' com.fsck.k9.mailstore.LocalFolder), (r7v4 'localFolder' com.fsck.k9.mailstore.LocalFolder), (r7v4 'localFolder' com.fsck.k9.mailstore.LocalFolder), (r7v4 'localFolder' com.fsck.k9.mailstore.LocalFolder), (r7v4 'localFolder' com.fsck.k9.mailstore.LocalFolder), (r7v4 'localFolder' com.fsck.k9.mailstore.LocalFolder), (r7v4 'localFolder' com.fsck.k9.mailstore.LocalFolder) binds: [B:1:0x0003, B:2:?, B:7:0x0024, B:23:0x005d, B:34:0x00a8, B:104:0x038c, B:116:0x03c5, B:117:?, B:44:0x011d, B:56:0x01c7, B:57:?, B:58:0x01dc, B:59:?, B:61:0x01e6, B:102:0x0387, B:103:?, B:90:0x02be, B:91:?, B:79:0x0297, B:80:?, B:67:0x01fe, B:68:?, B:69:0x0230, B:81:0x02a3, B:92:0x02cb] A[DONT_GENERATE, DONT_INLINE]
      PHI: (r15v3 'lastFailure' java.lang.Exception) = (r15v0 'lastFailure' java.lang.Exception), (r15v0 'lastFailure' java.lang.Exception), (r15v0 'lastFailure' java.lang.Exception), (r15v0 'lastFailure' java.lang.Exception), (r15v0 'lastFailure' java.lang.Exception), (r15v4 'lastFailure' java.lang.Exception), (r15v4 'lastFailure' java.lang.Exception), (r15v4 'lastFailure' java.lang.Exception), (r15v4 'lastFailure' java.lang.Exception), (r15v6 'lastFailure' java.lang.Exception), (r15v6 'lastFailure' java.lang.Exception), (r15v4 'lastFailure' java.lang.Exception), (r15v4 'lastFailure' java.lang.Exception), (r15v4 'lastFailure' java.lang.Exception), (r15v7 'lastFailure' java.lang.Exception), (r15v7 'lastFailure' java.lang.Exception), (r15v8 'lastFailure' java.lang.Exception), (r15v8 'lastFailure' java.lang.Exception), (r15v9 'lastFailure' java.lang.Exception), (r15v9 'lastFailure' java.lang.Exception), (r15v10 'lastFailure' java.lang.Exception), (r15v10 'lastFailure' java.lang.Exception), (r15v4 'lastFailure' java.lang.Exception), (r15v4 'lastFailure' java.lang.Exception), (r15v4 'lastFailure' java.lang.Exception) binds: [B:1:0x0003, B:2:?, B:7:0x0024, B:23:0x005d, B:34:0x00a8, B:104:0x038c, B:116:0x03c5, B:117:?, B:44:0x011d, B:56:0x01c7, B:57:?, B:58:0x01dc, B:59:?, B:61:0x01e6, B:102:0x0387, B:103:?, B:90:0x02be, B:91:?, B:79:0x0297, B:80:?, B:67:0x01fe, B:68:?, B:69:0x0230, B:81:0x02a3, B:92:0x02cb] A[DONT_GENERATE, DONT_INLINE], Splitter:B:1:0x0003] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void sendPendingMessagesSynchronous(com.fsck.k9.Account r27) {
        /*
            r26 = this;
            r7 = 0
            r15 = 0
            r10 = 0
            com.fsck.k9.mailstore.LocalStore r6 = r27.getLocalStore()     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            java.lang.String r4 = r27.getOutboxFolderName()     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            com.fsck.k9.mailstore.LocalFolder r7 = r6.getFolder(r4)     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            boolean r4 = r7.exists()     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            if (r4 != 0) goto L_0x0024
            if (r15 != 0) goto L_0x0020
            r0 = r26
            com.fsck.k9.notification.NotificationController r4 = r0.notificationController
            r0 = r27
            r4.clearSendFailedNotification(r0)
        L_0x0020:
            closeFolder(r7)
        L_0x0023:
            return
        L_0x0024:
            java.util.Set r4 = r26.getListeners()     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            java.util.Iterator r4 = r4.iterator()     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
        L_0x002c:
            boolean r5 = r4.hasNext()     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            if (r5 == 0) goto L_0x005c
            java.lang.Object r14 = r4.next()     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            com.fsck.k9.controller.MessagingListener r14 = (com.fsck.k9.controller.MessagingListener) r14     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            r0 = r27
            r14.sendPendingMessagesStarted(r0)     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            goto L_0x002c
        L_0x003e:
            r9 = move-exception
            java.lang.String r4 = "k9"
            java.lang.String r5 = "Failed to send pending messages because storage is not available - trying again later."
            android.util.Log.i(r4, r5)     // Catch:{ all -> 0x004c }
            com.fsck.k9.controller.UnavailableAccountException r4 = new com.fsck.k9.controller.UnavailableAccountException     // Catch:{ all -> 0x004c }
            r4.<init>(r9)     // Catch:{ all -> 0x004c }
            throw r4     // Catch:{ all -> 0x004c }
        L_0x004c:
            r4 = move-exception
            if (r15 != 0) goto L_0x0058
            r0 = r26
            com.fsck.k9.notification.NotificationController r5 = r0.notificationController
            r0 = r27
            r5.clearSendFailedNotification(r0)
        L_0x0058:
            closeFolder(r7)
            throw r4
        L_0x005c:
            r4 = 0
            r7.open(r4)     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            r4 = 0
            java.util.List r16 = r7.getMessages(r4)     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            r20 = 0
            int r21 = r16.size()     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            java.util.Set r4 = r26.getListeners()     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            java.util.Iterator r4 = r4.iterator()     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
        L_0x0073:
            boolean r5 = r4.hasNext()     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            if (r5 == 0) goto L_0x00a8
            java.lang.Object r14 = r4.next()     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            com.fsck.k9.controller.MessagingListener r14 = (com.fsck.k9.controller.MessagingListener) r14     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            java.lang.String r5 = r27.getSentFolderName()     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            r0 = r27
            r1 = r20
            r2 = r21
            r14.synchronizeMailboxProgress(r0, r5, r1, r2)     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            goto L_0x0073
        L_0x008d:
            r9 = move-exception
            java.util.Set r4 = r26.getListeners()     // Catch:{ all -> 0x004c }
            java.util.Iterator r4 = r4.iterator()     // Catch:{ all -> 0x004c }
        L_0x0096:
            boolean r5 = r4.hasNext()     // Catch:{ all -> 0x004c }
            if (r5 == 0) goto L_0x03cd
            java.lang.Object r14 = r4.next()     // Catch:{ all -> 0x004c }
            com.fsck.k9.controller.MessagingListener r14 = (com.fsck.k9.controller.MessagingListener) r14     // Catch:{ all -> 0x004c }
            r0 = r27
            r14.sendPendingMessagesFailed(r0)     // Catch:{ all -> 0x004c }
            goto L_0x0096
        L_0x00a8:
            com.fsck.k9.mail.FetchProfile r13 = new com.fsck.k9.mail.FetchProfile     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            r13.<init>()     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            com.fsck.k9.mail.FetchProfile$Item r4 = com.fsck.k9.mail.FetchProfile.Item.ENVELOPE     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            r13.add(r4)     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            com.fsck.k9.mail.FetchProfile$Item r4 = com.fsck.k9.mail.FetchProfile.Item.BODY     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            r13.add(r4)     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            boolean r4 = com.fsck.k9.K9.DEBUG     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            if (r4 == 0) goto L_0x00f5
            java.lang.String r4 = "k9"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            r5.<init>()     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            java.lang.String r23 = "Scanning folder '"
            r0 = r23
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            java.lang.String r23 = r27.getOutboxFolderName()     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            r0 = r23
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            java.lang.String r23 = "' ("
            r0 = r23
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            long r24 = r7.getId()     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            r0 = r24
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            java.lang.String r23 = ") for messages to send"
            r0 = r23
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            java.lang.String r5 = r5.toString()     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            android.util.Log.i(r4, r5)     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
        L_0x00f5:
            android.app.Application r4 = com.fsck.k9.K9.app     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            com.fsck.k9.mail.oauth.OAuth2TokenProvider r5 = com.fsck.k9.Globals.getOAuth2TokenProvider()     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            r0 = r27
            com.fsck.k9.mail.Transport r22 = com.fsck.k9.mail.Transport.getInstance(r4, r0, r5)     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            java.util.Iterator r23 = r16.iterator()     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
        L_0x0105:
            boolean r4 = r23.hasNext()     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            if (r4 == 0) goto L_0x038c
            java.lang.Object r8 = r23.next()     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            com.fsck.k9.mailstore.LocalMessage r8 = (com.fsck.k9.mailstore.LocalMessage) r8     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            com.fsck.k9.mail.Flag r4 = com.fsck.k9.mail.Flag.DELETED     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            boolean r4 = r8.isSet(r4)     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            if (r4 == 0) goto L_0x011d
            r8.destroy()     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            goto L_0x0105
        L_0x011d:
            java.util.concurrent.atomic.AtomicInteger r12 = new java.util.concurrent.atomic.AtomicInteger     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            r4 = 0
            r12.<init>(r4)     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            r0 = r26
            java.util.concurrent.ConcurrentHashMap<java.lang.String, java.util.concurrent.atomic.AtomicInteger> r4 = r0.sendCount     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            java.lang.String r5 = r8.getUid()     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            java.lang.Object r19 = r4.putIfAbsent(r5, r12)     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            java.util.concurrent.atomic.AtomicInteger r19 = (java.util.concurrent.atomic.AtomicInteger) r19     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            if (r19 == 0) goto L_0x0135
            r12 = r19
        L_0x0135:
            boolean r4 = com.fsck.k9.K9.DEBUG     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            if (r4 == 0) goto L_0x016b
            java.lang.String r4 = "k9"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            r5.<init>()     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            java.lang.String r24 = "Send count for message "
            r0 = r24
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            java.lang.String r24 = r8.getUid()     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            r0 = r24
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            java.lang.String r24 = " is "
            r0 = r24
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            int r24 = r12.get()     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            r0 = r24
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            android.util.Log.i(r4, r5)     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
        L_0x016b:
            int r4 = r12.incrementAndGet()     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            r5 = 5
            if (r4 <= r5) goto L_0x01dc
            java.lang.String r4 = "k9"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            r5.<init>()     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            java.lang.String r24 = "Send count for message "
            r0 = r24
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            java.lang.String r24 = r8.getUid()     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            r0 = r24
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            java.lang.String r24 = " can't be delivered after "
            r0 = r24
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            r24 = 5
            r0 = r24
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            java.lang.String r24 = " attempts.  Giving up until the user restarts the device"
            r0 = r24
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            android.util.Log.e(r4, r5)     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            r0 = r26
            com.fsck.k9.notification.NotificationController r4 = r0.notificationController     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            com.fsck.k9.mail.MessagingException r5 = new com.fsck.k9.mail.MessagingException     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            java.lang.String r24 = r8.getSubject()     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            r0 = r24
            r5.<init>(r0)     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            r0 = r27
            r4.showSendFailedNotification(r0, r5)     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            goto L_0x0105
        L_0x01c0:
            r9 = move-exception
            r15 = r9
            r10 = 0
            java.lang.String r4 = "k9"
            java.lang.String r5 = "Failed to fetch message for sending"
            android.util.Log.e(r4, r5, r9)     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            java.lang.String r4 = "Failed to fetch message for sending"
            r0 = r26
            r1 = r27
            r0.addErrorMessage(r1, r4, r9)     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            r0 = r26
            r1 = r27
            r0.notifySynchronizeMailboxFailed(r1, r7, r9)     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            goto L_0x0105
        L_0x01dc:
            java.util.List r4 = java.util.Collections.singletonList(r8)     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            r5 = 0
            r7.fetch(r4, r13, r5)     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            java.lang.String r4 = "X-K9mail-Identity"
            java.lang.String[] r4 = r8.getHeader(r4)     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            int r4 = r4.length     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            if (r4 <= 0) goto L_0x0230
            java.lang.String r4 = "k9"
            java.lang.String r5 = "The user has set the Outbox and Drafts folder to the same thing. This message appears to be a draft, so K-9 will not send it"
            android.util.Log.v(r4, r5)     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            goto L_0x0105
        L_0x01f6:
            r9 = move-exception
            r15 = r9
            r10 = 0
            r4 = 0
            r0 = r26
            r1 = r27
            r0.handleAuthenticationFailure(r1, r4)     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            r4.<init>()     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            java.lang.String r5 = "Exception sendPendingMessagesSynchronous: "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            java.lang.String r5 = r9.getMessage()     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            java.lang.String r5 = " at MessagingControler.java 3230"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            java.lang.String r18 = r4.toString()     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            r0 = r27
            r1 = r18
            r2 = r18
            addErrorMessage(r0, r1, r2)     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            r4 = r26
            r5 = r27
            r4.handleSendFailure(r5, r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            goto L_0x0105
        L_0x0230:
            com.fsck.k9.mail.Flag r4 = com.fsck.k9.mail.Flag.X_SEND_IN_PROGRESS     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            r5 = 1
            r8.setFlag(r4, r5)     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            boolean r4 = com.fsck.k9.K9.DEBUG     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            if (r4 == 0) goto L_0x025a
            java.lang.String r4 = "k9"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            r5.<init>()     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            java.lang.String r24 = "Sending message with UID "
            r0 = r24
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            java.lang.String r24 = r8.getUid()     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            r0 = r24
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            java.lang.String r5 = r5.toString()     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            android.util.Log.i(r4, r5)     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
        L_0x025a:
            r0 = r22
            r0.sendMessage(r8)     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            com.fsck.k9.mail.Flag r4 = com.fsck.k9.mail.Flag.X_SEND_IN_PROGRESS     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            r5 = 0
            r8.setFlag(r4, r5)     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            com.fsck.k9.mail.Flag r4 = com.fsck.k9.mail.Flag.SEEN     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            r5 = 1
            r8.setFlag(r4, r5)     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            int r20 = r20 + 1
            java.util.Set r4 = r26.getListeners()     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            java.util.Iterator r4 = r4.iterator()     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
        L_0x0275:
            boolean r5 = r4.hasNext()     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            if (r5 == 0) goto L_0x02a3
            java.lang.Object r14 = r4.next()     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            com.fsck.k9.controller.MessagingListener r14 = (com.fsck.k9.controller.MessagingListener) r14     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            java.lang.String r5 = r27.getSentFolderName()     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            r0 = r27
            r1 = r20
            r2 = r21
            r14.synchronizeMailboxProgress(r0, r5, r1, r2)     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            goto L_0x0275
        L_0x028f:
            r9 = move-exception
            r15 = r9
            r10 = 0
            r4 = 0
            r0 = r26
            r1 = r27
            r0.notifyUserIfCertificateProblem(r1, r9, r4)     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            r4 = r26
            r5 = r27
            r4.handleSendFailure(r5, r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            goto L_0x0105
        L_0x02a3:
            boolean r4 = r27.hasSentFolder()     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            if (r4 != 0) goto L_0x02cb
            boolean r4 = com.fsck.k9.K9.DEBUG     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            if (r4 == 0) goto L_0x02b4
            java.lang.String r4 = "k9"
            java.lang.String r5 = "Account does not have a sent mail folder; deleting sent message"
            android.util.Log.i(r4, r5)     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
        L_0x02b4:
            com.fsck.k9.mail.Flag r4 = com.fsck.k9.mail.Flag.DELETED     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            r5 = 1
            r8.setFlag(r4, r5)     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            goto L_0x0105
        L_0x02bc:
            r9 = move-exception
            r15 = r9
            boolean r10 = r9.isPermanentFailure()     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            r4 = r26
            r5 = r27
            r4.handleSendFailure(r5, r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            goto L_0x0105
        L_0x02cb:
            java.lang.String r4 = r27.getSentFolderName()     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            com.fsck.k9.mailstore.LocalFolder r17 = r6.getFolder(r4)     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            boolean r4 = com.fsck.k9.K9.DEBUG     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            if (r4 == 0) goto L_0x0311
            java.lang.String r4 = "k9"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            r5.<init>()     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            java.lang.String r24 = "Moving sent message to folder '"
            r0 = r24
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            java.lang.String r24 = r27.getSentFolderName()     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            r0 = r24
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            java.lang.String r24 = "' ("
            r0 = r24
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            long r24 = r17.getId()     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            r0 = r24
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            java.lang.String r24 = ") "
            r0 = r24
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            java.lang.String r5 = r5.toString()     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            android.util.Log.i(r4, r5)     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
        L_0x0311:
            java.util.List r4 = java.util.Collections.singletonList(r8)     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            r0 = r17
            r7.moveMessages(r4, r0)     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            boolean r4 = com.fsck.k9.K9.DEBUG     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            if (r4 == 0) goto L_0x0358
            java.lang.String r4 = "k9"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            r5.<init>()     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            java.lang.String r24 = "Moved sent message to folder '"
            r0 = r24
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            java.lang.String r24 = r27.getSentFolderName()     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            r0 = r24
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            java.lang.String r24 = "' ("
            r0 = r24
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            long r24 = r17.getId()     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            r0 = r24
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            java.lang.String r24 = ") "
            r0 = r24
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            java.lang.String r5 = r5.toString()     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            android.util.Log.i(r4, r5)     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
        L_0x0358:
            com.fsck.k9.mailstore.LocalStore$PendingCommand r11 = new com.fsck.k9.mailstore.LocalStore$PendingCommand     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            r11.<init>()     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            java.lang.String r4 = "com.fsck.k9.MessagingController.append"
            r11.command = r4     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            r4 = 2
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            r5 = 0
            java.lang.String r24 = r17.getName()     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            r4[r5] = r24     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            r5 = 1
            java.lang.String r24 = r8.getUid()     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            r4[r5] = r24     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            r11.arguments = r4     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            r0 = r26
            r1 = r27
            r0.queuePendingCommand(r1, r11)     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            r26.processPendingCommands(r27)     // Catch:{ AuthenticationFailedException -> 0x01f6, CertificateValidationException -> 0x028f, MessagingException -> 0x02bc, Exception -> 0x0380, UnavailableStorageException -> 0x003e }
            goto L_0x0105
        L_0x0380:
            r9 = move-exception
            r15 = r9
            r10 = 1
            r4 = r26
            r5 = r27
            r4.handleSendFailure(r5, r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x01c0, UnavailableStorageException -> 0x003e }
            goto L_0x0105
        L_0x038c:
            java.util.Set r4 = r26.getListeners()     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            java.util.Iterator r4 = r4.iterator()     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
        L_0x0394:
            boolean r5 = r4.hasNext()     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            if (r5 == 0) goto L_0x03a6
            java.lang.Object r14 = r4.next()     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            com.fsck.k9.controller.MessagingListener r14 = (com.fsck.k9.controller.MessagingListener) r14     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            r0 = r27
            r14.sendPendingMessagesCompleted(r0)     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            goto L_0x0394
        L_0x03a6:
            if (r15 == 0) goto L_0x03b3
            if (r10 == 0) goto L_0x03c3
            r0 = r26
            com.fsck.k9.notification.NotificationController r4 = r0.notificationController     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            r0 = r27
            r4.showSendFailedNotification(r0, r15)     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
        L_0x03b3:
            if (r15 != 0) goto L_0x03be
            r0 = r26
            com.fsck.k9.notification.NotificationController r4 = r0.notificationController
            r0 = r27
            r4.clearSendFailedNotification(r0)
        L_0x03be:
            closeFolder(r7)
            goto L_0x0023
        L_0x03c3:
            r0 = r26
            com.fsck.k9.notification.NotificationController r4 = r0.notificationController     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            r0 = r27
            r4.showSendFailedNotification(r0, r15)     // Catch:{ UnavailableStorageException -> 0x003e, Exception -> 0x008d }
            goto L_0x03b3
        L_0x03cd:
            r4 = 0
            r0 = r26
            r1 = r27
            r0.addErrorMessage(r1, r4, r9)     // Catch:{ all -> 0x004c }
            if (r15 != 0) goto L_0x03e0
            r0 = r26
            com.fsck.k9.notification.NotificationController r4 = r0.notificationController
            r0 = r27
            r4.clearSendFailedNotification(r0)
        L_0x03e0:
            closeFolder(r7)
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fsck.k9.controller.MessagingController.sendPendingMessagesSynchronous(com.fsck.k9.Account):void");
    }

    private void handleSendFailure(Account account, Store localStore, Folder localFolder, Message message, Exception exception, boolean permanentFailure) throws MessagingException {
        Log.e("k9", "Failed to send message", exception);
        if (permanentFailure) {
            moveMessageToDraftsFolder(account, localFolder, localStore, message);
        }
        addErrorMessage(account, "Failed to send message", exception);
        message.setFlag(Flag.X_SEND_FAILED, true);
        notifySynchronizeMailboxFailed(account, localFolder, exception);
    }

    private void moveMessageToDraftsFolder(Account account, Folder localFolder, Store localStore, Message message) throws MessagingException {
        localFolder.moveMessages(Collections.singletonList(message), (LocalFolder) localStore.getFolder(account.getDraftsFolderName()));
    }

    private void notifySynchronizeMailboxFailed(Account account, Folder localFolder, Exception exception) {
        String folderName = localFolder.getName();
        String errorMessage = getRootCauseMessage(exception);
        for (MessagingListener listener : getListeners()) {
            listener.synchronizeMailboxFailed(account, folderName, errorMessage);
        }
    }

    public void getAccountStats(final Context context2, final Account account, final MessagingListener listener) {
        this.threadPool.execute(new Runnable() {
            public void run() {
                try {
                    listener.accountStatusChanged(account, account.getStats(context2));
                } catch (Exception me2) {
                    Log.e("k9", "Count not get unread count for account ");
                    Log.e("AtomicGonza", "okAG Exception " + me2.getMessage() + " " + me2.getStackTrace()[0].toString());
                }
            }
        });
    }

    public void getSearchAccountStats(final SearchAccount searchAccount, final MessagingListener listener) {
        this.threadPool.execute(new Runnable() {
            public void run() {
                MessagingController.this.getSearchAccountStatsSynchronous(searchAccount, listener);
            }
        });
    }

    public AccountStats getSearchAccountStatsSynchronous(SearchAccount searchAccount, MessagingListener listener) {
        List<Account> accounts;
        Preferences preferences = Preferences.getPreferences(this.context);
        LocalSearch search = searchAccount.getRelatedSearch();
        String[] accountUuids = search.getAccountUuids();
        if (search.searchAllAccounts()) {
            accounts = preferences.getAccounts();
        } else {
            accounts = new ArrayList<>(accountUuids.length);
            int len = accountUuids.length;
            for (int i = 0; i < len; i++) {
                accounts.set(i, preferences.getAccount(accountUuids[i]));
            }
        }
        ContentResolver cr = this.context.getContentResolver();
        int unreadMessageCount = 0;
        int flaggedMessageCount = 0;
        String[] projection = {"unread_count", "flagged_count"};
        for (Account account : accounts) {
            StringBuilder query = new StringBuilder();
            ArrayList arrayList = new ArrayList();
            SqlQueryBuilder.buildWhereClause(account, search.getConditions(), query, arrayList);
            Uri uri = Uri.withAppendedPath(EmailProvider.CONTENT_URI, "account/" + account.getUuid() + "/stats");
            Cursor cursor = cr.query(uri, projection, query.toString(), (String[]) arrayList.toArray(new String[arrayList.size()]), null);
            if (cursor != null) {
                try {
                    if (cursor.moveToFirst()) {
                        unreadMessageCount += cursor.getInt(0);
                        flaggedMessageCount += cursor.getInt(1);
                    }
                } catch (Throwable th) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            if (cursor != null) {
                cursor.close();
            }
        }
        AccountStats stats = new AccountStats();
        stats.unreadMessageCount = unreadMessageCount;
        stats.flaggedMessageCount = flaggedMessageCount;
        if (listener != null) {
            listener.accountStatusChanged(searchAccount, stats);
        }
        return stats;
    }

    public void getFolderUnreadMessageCount(final Account account, final String folderName, final MessagingListener l) {
        put("getFolderUnread:" + account.getDescription() + ":" + folderName, l, new Runnable() {
            public void run() {
                int unreadMessageCount = 0;
                try {
                    unreadMessageCount = account.getLocalStore().getFolder(folderName).getUnreadMessageCount();
                } catch (MessagingException me2) {
                    Log.e("k9", "Count not get unread count for account " + account.getDescription(), me2);
                }
                l.folderStatusChanged(account, folderName, unreadMessageCount);
            }
        });
    }

    public boolean isMoveCapable(MessageReference messageReference) {
        return !messageReference.getUid().startsWith(K9.LOCAL_UID_PREFIX);
    }

    public boolean isCopyCapable(MessageReference message) {
        return isMoveCapable(message);
    }

    public boolean isMoveCapable(Account account) {
        try {
            Store localStore = account.getLocalStore();
            Store remoteStore = account.getRemoteStore();
            if (!localStore.isMoveCapable() || !remoteStore.isMoveCapable()) {
                return false;
            }
            return true;
        } catch (MessagingException me2) {
            Log.e("k9", "Exception while ascertaining move capability", me2);
            return false;
        }
    }

    public boolean isCopyCapable(Account account) {
        try {
            Store localStore = account.getLocalStore();
            Store remoteStore = account.getRemoteStore();
            if (!localStore.isCopyCapable() || !remoteStore.isCopyCapable()) {
                return false;
            }
            return true;
        } catch (MessagingException me2) {
            Log.e("k9", "Exception while ascertaining copy capability", me2);
            return false;
        }
    }

    public void moveMessages(Account srcAccount, final String srcFolder, List<MessageReference> messageReferences, final String destFolder) {
        actOnMessageGroup(srcAccount, srcFolder, messageReferences, new MessageActor() {
            public void act(final Account account, LocalFolder messageFolder, final List<LocalMessage> messages) {
                MessagingController.this.suppressMessages(account, messages);
                MessagingController.this.putBackground("moveMessages", null, new Runnable() {
                    public void run() {
                        MessagingController.this.moveOrCopyMessageSynchronous(account, srcFolder, messages, destFolder, false);
                    }
                });
            }
        });
    }

    public void moveMessagesInThread(Account srcAccount, final String srcFolder, List<MessageReference> messageReferences, final String destFolder) {
        actOnMessageGroup(srcAccount, srcFolder, messageReferences, new MessageActor() {
            public void act(final Account account, LocalFolder messageFolder, final List<LocalMessage> messages) {
                MessagingController.this.suppressMessages(account, messages);
                MessagingController.this.putBackground("moveMessagesInThread", null, new Runnable() {
                    public void run() {
                        try {
                            MessagingController.this.moveOrCopyMessageSynchronous(account, srcFolder, MessagingController.collectMessagesInThreads(account, messages), destFolder, false);
                        } catch (MessagingException e) {
                            MessagingController.this.addErrorMessage(account, "Exception while moving messages", e);
                        }
                    }
                });
            }
        });
    }

    public void moveMessage(Account account, String srcFolder, MessageReference message, String destFolder) {
        moveMessages(account, srcFolder, Collections.singletonList(message), destFolder);
    }

    public void copyMessages(final Account srcAccount, final String srcFolder, List<MessageReference> messageReferences, final String destFolder) {
        actOnMessageGroup(srcAccount, srcFolder, messageReferences, new MessageActor() {
            public void act(Account account, LocalFolder messageFolder, final List<LocalMessage> messages) {
                MessagingController.this.putBackground("copyMessages", null, new Runnable() {
                    public void run() {
                        MessagingController.this.moveOrCopyMessageSynchronous(srcAccount, srcFolder, messages, destFolder, true);
                    }
                });
            }
        });
    }

    public void copyMessagesInThread(Account srcAccount, final String srcFolder, List<MessageReference> messageReferences, final String destFolder) {
        actOnMessageGroup(srcAccount, srcFolder, messageReferences, new MessageActor() {
            public void act(final Account account, LocalFolder messageFolder, final List<LocalMessage> messages) {
                MessagingController.this.putBackground("copyMessagesInThread", null, new Runnable() {
                    public void run() {
                        try {
                            MessagingController.this.moveOrCopyMessageSynchronous(account, srcFolder, MessagingController.collectMessagesInThreads(account, messages), destFolder, true);
                        } catch (MessagingException e) {
                            MessagingController.this.addErrorMessage(account, "Exception while copying messages", e);
                        }
                    }
                });
            }
        });
    }

    public void copyMessage(Account account, String srcFolder, MessageReference message, String destFolder) {
        copyMessages(account, srcFolder, Collections.singletonList(message), destFolder);
    }

    /* access modifiers changed from: private */
    public void moveOrCopyMessageSynchronous(Account account, String srcFolder, List<? extends Message> inMessages, String destFolder, boolean isCopy) {
        Map<String, String> uidMap;
        try {
            LocalStore localStore = account.getLocalStore();
            Store remoteStore = account.getRemoteStore();
            if (!isCopy && (!remoteStore.isMoveCapable() || !localStore.isMoveCapable())) {
                return;
            }
            if (!isCopy || (remoteStore.isCopyCapable() && localStore.isCopyCapable())) {
                LocalFolder localSrcFolder = localStore.getFolder(srcFolder);
                Folder localDestFolder = localStore.getFolder(destFolder);
                boolean unreadCountAffected = false;
                LinkedList linkedList = new LinkedList();
                for (Message message : inMessages) {
                    String uid = message.getUid();
                    if (!uid.startsWith(K9.LOCAL_UID_PREFIX)) {
                        linkedList.add(uid);
                    }
                    if (!unreadCountAffected) {
                        if (!message.isSet(Flag.SEEN)) {
                            unreadCountAffected = true;
                        }
                    }
                }
                List<LocalMessage> messages = localSrcFolder.getMessagesByUids(linkedList);
                if (messages.size() > 0) {
                    HashMap hashMap = new HashMap();
                    for (Message message2 : messages) {
                        hashMap.put(message2.getUid(), message2);
                    }
                    if (K9.DEBUG) {
                        Log.i("k9", "moveOrCopyMessageSynchronous: source folder = " + srcFolder + ", " + messages.size() + " messages, " + ", destination folder = " + destFolder + ", isCopy = " + isCopy);
                    }
                    if (isCopy) {
                        FetchProfile fp = new FetchProfile();
                        fp.add(FetchProfile.Item.ENVELOPE);
                        fp.add(FetchProfile.Item.BODY);
                        localSrcFolder.fetch(messages, fp, null);
                        uidMap = localSrcFolder.copyMessages(messages, localDestFolder);
                        if (unreadCountAffected) {
                            int unreadMessageCount = localDestFolder.getUnreadMessageCount();
                            for (MessagingListener l : getListeners()) {
                                l.folderStatusChanged(account, destFolder, unreadMessageCount);
                            }
                        }
                    } else {
                        uidMap = localSrcFolder.moveMessages(messages, localDestFolder);
                        for (Map.Entry<String, Message> entry : hashMap.entrySet()) {
                            String origUid = (String) entry.getKey();
                            Message message3 = (Message) entry.getValue();
                            for (MessagingListener l2 : getListeners()) {
                                l2.messageUidChanged(account, srcFolder, origUid, message3.getUid());
                            }
                        }
                        unsuppressMessages(account, messages);
                        if (unreadCountAffected) {
                            int unreadMessageCountSrc = localSrcFolder.getUnreadMessageCount();
                            int unreadMessageCountDest = localDestFolder.getUnreadMessageCount();
                            for (MessagingListener l3 : getListeners()) {
                                l3.folderStatusChanged(account, srcFolder, unreadMessageCountSrc);
                                l3.folderStatusChanged(account, destFolder, unreadMessageCountDest);
                            }
                        }
                    }
                    Set<String> origUidKeys = hashMap.keySet();
                    Account account2 = account;
                    String str = srcFolder;
                    String str2 = destFolder;
                    boolean z = isCopy;
                    queueMoveOrCopy(account2, str, str2, z, (String[]) origUidKeys.toArray(new String[origUidKeys.size()]), uidMap);
                }
                processPendingCommands(account);
            }
        } catch (UnavailableStorageException e) {
            Log.i("k9", "Failed to move/copy message because storage is not available - trying again later.");
            throw new UnavailableAccountException(e);
        } catch (MessagingException me2) {
            addErrorMessage(account, (String) null, me2);
            throw new RuntimeException("Error moving message", me2);
        }
    }

    public void expunge(final Account account, final String folder) {
        putBackground("expunge", null, new Runnable() {
            public void run() {
                MessagingController.this.queueExpunge(account, folder);
            }
        });
    }

    public void deleteDraft(Account account, long id) {
        LocalFolder localFolder = null;
        try {
            localFolder = account.getLocalStore().getFolder(account.getDraftsFolderName());
            localFolder.open(0);
            String uid = localFolder.getMessageUidById(id);
            if (uid != null) {
                deleteMessage(new MessageReference(account.getUuid(), account.getDraftsFolderName(), uid, null), null);
            }
        } catch (MessagingException me2) {
            addErrorMessage(account, (String) null, me2);
        } finally {
            closeFolder(localFolder);
        }
    }

    public void deleteThreads(List<MessageReference> messages) {
        actOnMessagesGroupedByAccountAndFolder(messages, new MessageActor() {
            public void act(final Account account, final LocalFolder messageFolder, final List<LocalMessage> accountMessages) {
                MessagingController.this.suppressMessages(account, accountMessages);
                MessagingController.this.putBackground("deleteThreads", null, new Runnable() {
                    public void run() {
                        MessagingController.this.deleteThreadsSynchronous(account, messageFolder.getName(), accountMessages);
                    }
                });
            }
        });
    }

    /* access modifiers changed from: private */
    public void deleteThreadsSynchronous(Account account, String folderName, List<? extends Message> messages) {
        try {
            deleteMessagesSynchronous(account, folderName, collectMessagesInThreads(account, messages), null);
        } catch (MessagingException e) {
            Log.e("k9", "Something went wrong while deleting threads", e);
        }
    }

    /* access modifiers changed from: private */
    public static List<Message> collectMessagesInThreads(Account account, List<? extends Message> messages) throws MessagingException {
        long threadId;
        LocalStore localStore = account.getLocalStore();
        List<Message> messagesInThreads = new ArrayList<>();
        for (Message message : messages) {
            LocalMessage localMessage = (LocalMessage) message;
            long rootId = localMessage.getRootId();
            if (rootId == -1) {
                threadId = localMessage.getThreadId();
            } else {
                threadId = rootId;
            }
            messagesInThreads.addAll(localStore.getMessagesInThread(threadId));
        }
        return messagesInThreads;
    }

    public void deleteMessage(MessageReference message, MessagingListener listener) {
        deleteMessages(Collections.singletonList(message), listener);
    }

    public void deleteMessages(List<MessageReference> messages, final MessagingListener listener) {
        actOnMessagesGroupedByAccountAndFolder(messages, new MessageActor() {
            public void act(final Account account, final LocalFolder messageFolder, final List<LocalMessage> accountMessages) {
                MessagingController.this.suppressMessages(account, accountMessages);
                MessagingController.this.putBackground("deleteMessages", null, new Runnable() {
                    public void run() {
                        MessagingController.this.deleteMessagesSynchronous(account, messageFolder.getName(), accountMessages, listener);
                    }
                });
            }
        });
    }

    @SuppressLint({"NewApi"})
    public void debugClearMessagesLocally(List<MessageReference> list) {
        throw new AssertionError("method must only be used in debug build!");
    }

    /* access modifiers changed from: private */
    public void deleteMessagesSynchronous(Account account, String folder, List<? extends Message> messages, MessagingListener listener) {
        Folder localFolder = null;
        Folder localTrashFolder = null;
        String[] uids = getUidsFromMessages(messages);
        try {
            for (Message message : messages) {
                for (MessagingListener l : getListeners(listener)) {
                    l.messageDeleted(account, folder, message);
                }
            }
            Store localStore = account.getLocalStore();
            localFolder = localStore.getFolder(folder);
            Map<String, String> uidMap = null;
            if (folder.equals(account.getTrashFolderName()) || !account.hasTrashFolder()) {
                if (K9.DEBUG) {
                    Log.d("k9", "Deleting messages in trash folder or trash set to -None-, not copying");
                }
                localFolder.setFlags(messages, Collections.singleton(Flag.DELETED), true);
            } else {
                localTrashFolder = localStore.getFolder(account.getTrashFolderName());
                if (!localTrashFolder.exists()) {
                    localTrashFolder.create(Folder.FolderType.HOLDS_MESSAGES);
                }
                if (localTrashFolder.exists()) {
                    if (K9.DEBUG) {
                        Log.d("k9", "Deleting messages in normal folder, moving");
                    }
                    uidMap = localFolder.moveMessages(messages, localTrashFolder);
                }
            }
            for (MessagingListener l2 : getListeners()) {
                l2.folderStatusChanged(account, folder, localFolder.getUnreadMessageCount());
                if (localTrashFolder != null) {
                    l2.folderStatusChanged(account, account.getTrashFolderName(), localTrashFolder.getUnreadMessageCount());
                }
            }
            if (K9.DEBUG) {
                Log.d("k9", "Delete policy for account " + account.getDescription() + " is " + account.getDeletePolicy());
            }
            if (folder.equals(account.getOutboxFolderName())) {
                for (Message message2 : messages) {
                    LocalStore.PendingCommand command = new LocalStore.PendingCommand();
                    command.command = PENDING_COMMAND_APPEND;
                    command.arguments = new String[]{account.getTrashFolderName(), message2.getUid()};
                    queuePendingCommand(account, command);
                }
                processPendingCommands(account);
            } else if (account.getDeletePolicy() == Account.DeletePolicy.ON_DELETE) {
                if (folder.equals(account.getTrashFolderName())) {
                    queueSetFlag(account, folder, Boolean.toString(true), Flag.DELETED.toString(), uids);
                } else {
                    queueMoveOrCopy(account, folder, account.getTrashFolderName(), false, uids, uidMap);
                }
                processPendingCommands(account);
            } else if (account.getDeletePolicy() == Account.DeletePolicy.MARK_AS_READ) {
                queueSetFlag(account, folder, Boolean.toString(true), Flag.SEEN.toString(), uids);
                processPendingCommands(account);
            } else if (K9.DEBUG) {
                Log.d("k9", "Delete policy " + account.getDeletePolicy() + " prevents delete from server");
            }
            unsuppressMessages(account, messages);
            closeFolder(localFolder);
            closeFolder(localTrashFolder);
        } catch (UnavailableStorageException e) {
            Log.i("k9", "Failed to delete message because storage is not available - trying again later.");
            throw new UnavailableAccountException(e);
        } catch (MessagingException me2) {
            addErrorMessage(account, (String) null, me2);
            throw new RuntimeException("Error deleting message from local store.", me2);
        } catch (Throwable th) {
            closeFolder(localFolder);
            closeFolder(localTrashFolder);
            throw th;
        }
    }

    private static String[] getUidsFromMessages(List<? extends Message> messages) {
        String[] uids = new String[messages.size()];
        for (int i = 0; i < messages.size(); i++) {
            uids[i] = ((Message) messages.get(i)).getUid();
        }
        return uids;
    }

    private void processPendingEmptyTrash(LocalStore.PendingCommand command, Account account) throws MessagingException {
        Folder remoteFolder = account.getRemoteStore().getFolder(account.getTrashFolderName());
        try {
            if (remoteFolder.exists()) {
                remoteFolder.open(0);
                remoteFolder.setFlags(Collections.singleton(Flag.DELETED), true);
                if (Account.Expunge.EXPUNGE_IMMEDIATELY == account.getExpungePolicy()) {
                    remoteFolder.expunge();
                }
                synchronizeFolder(account, remoteFolder, true, 0, null);
                compact(account, null);
            }
        } finally {
            closeFolder(remoteFolder);
        }
    }

    public void emptyTrash(final Account account, MessagingListener listener) {
        putBackground("emptyTrash", listener, new Runnable() {
            /* JADX WARN: Type inference failed for: r7v8, types: [com.fsck.k9.mail.Folder] */
            /* JADX WARNING: Multi-variable type inference failed */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r10 = this;
                    r5 = 0
                    com.fsck.k9.Account r7 = r3     // Catch:{ UnavailableStorageException -> 0x0042, Exception -> 0x0060 }
                    com.fsck.k9.mailstore.LocalStore r6 = r7.getLocalStore()     // Catch:{ UnavailableStorageException -> 0x0042, Exception -> 0x0060 }
                    com.fsck.k9.Account r7 = r3     // Catch:{ UnavailableStorageException -> 0x0042, Exception -> 0x0060 }
                    java.lang.String r7 = r7.getTrashFolderName()     // Catch:{ UnavailableStorageException -> 0x0042, Exception -> 0x0060 }
                    com.fsck.k9.mail.Folder r7 = r6.getFolder(r7)     // Catch:{ UnavailableStorageException -> 0x0042, Exception -> 0x0060 }
                    r0 = r7
                    com.fsck.k9.mailstore.LocalFolder r0 = (com.fsck.k9.mailstore.LocalFolder) r0     // Catch:{ UnavailableStorageException -> 0x0042, Exception -> 0x0060 }
                    r5 = r0
                    r7 = 0
                    r5.open(r7)     // Catch:{ UnavailableStorageException -> 0x0042, Exception -> 0x0060 }
                    com.fsck.k9.controller.MessagingController r7 = com.fsck.k9.controller.MessagingController.this     // Catch:{ UnavailableStorageException -> 0x0042, Exception -> 0x0060 }
                    com.fsck.k9.Account r8 = r3     // Catch:{ UnavailableStorageException -> 0x0042, Exception -> 0x0060 }
                    boolean r3 = r7.isTrashLocalOnly(r8)     // Catch:{ UnavailableStorageException -> 0x0042, Exception -> 0x0060 }
                    if (r3 == 0) goto L_0x0055
                    r5.clearAllMessages()     // Catch:{ UnavailableStorageException -> 0x0042, Exception -> 0x0060 }
                L_0x0026:
                    com.fsck.k9.controller.MessagingController r7 = com.fsck.k9.controller.MessagingController.this     // Catch:{ UnavailableStorageException -> 0x0042, Exception -> 0x0060 }
                    java.util.Set r7 = r7.getListeners()     // Catch:{ UnavailableStorageException -> 0x0042, Exception -> 0x0060 }
                    java.util.Iterator r7 = r7.iterator()     // Catch:{ UnavailableStorageException -> 0x0042, Exception -> 0x0060 }
                L_0x0030:
                    boolean r8 = r7.hasNext()     // Catch:{ UnavailableStorageException -> 0x0042, Exception -> 0x0060 }
                    if (r8 == 0) goto L_0x0074
                    java.lang.Object r4 = r7.next()     // Catch:{ UnavailableStorageException -> 0x0042, Exception -> 0x0060 }
                    com.fsck.k9.controller.MessagingListener r4 = (com.fsck.k9.controller.MessagingListener) r4     // Catch:{ UnavailableStorageException -> 0x0042, Exception -> 0x0060 }
                    com.fsck.k9.Account r8 = r3     // Catch:{ UnavailableStorageException -> 0x0042, Exception -> 0x0060 }
                    r4.emptyTrashCompleted(r8)     // Catch:{ UnavailableStorageException -> 0x0042, Exception -> 0x0060 }
                    goto L_0x0030
                L_0x0042:
                    r2 = move-exception
                    java.lang.String r7 = "k9"
                    java.lang.String r8 = "Failed to empty trash because storage is not available - trying again later."
                    android.util.Log.i(r7, r8)     // Catch:{ all -> 0x0050 }
                    com.fsck.k9.controller.UnavailableAccountException r7 = new com.fsck.k9.controller.UnavailableAccountException     // Catch:{ all -> 0x0050 }
                    r7.<init>(r2)     // Catch:{ all -> 0x0050 }
                    throw r7     // Catch:{ all -> 0x0050 }
                L_0x0050:
                    r7 = move-exception
                    com.fsck.k9.controller.MessagingController.closeFolder(r5)
                    throw r7
                L_0x0055:
                    com.fsck.k9.mail.Flag r7 = com.fsck.k9.mail.Flag.DELETED     // Catch:{ UnavailableStorageException -> 0x0042, Exception -> 0x0060 }
                    java.util.Set r7 = java.util.Collections.singleton(r7)     // Catch:{ UnavailableStorageException -> 0x0042, Exception -> 0x0060 }
                    r8 = 1
                    r5.setFlags(r7, r8)     // Catch:{ UnavailableStorageException -> 0x0042, Exception -> 0x0060 }
                    goto L_0x0026
                L_0x0060:
                    r2 = move-exception
                    java.lang.String r7 = "k9"
                    java.lang.String r8 = "emptyTrash failed"
                    android.util.Log.e(r7, r8, r2)     // Catch:{ all -> 0x0050 }
                    com.fsck.k9.controller.MessagingController r7 = com.fsck.k9.controller.MessagingController.this     // Catch:{ all -> 0x0050 }
                    com.fsck.k9.Account r8 = r3     // Catch:{ all -> 0x0050 }
                    r9 = 0
                    r7.addErrorMessage(r8, r9, r2)     // Catch:{ all -> 0x0050 }
                    com.fsck.k9.controller.MessagingController.closeFolder(r5)
                L_0x0073:
                    return
                L_0x0074:
                    if (r3 != 0) goto L_0x0093
                    com.fsck.k9.mailstore.LocalStore$PendingCommand r1 = new com.fsck.k9.mailstore.LocalStore$PendingCommand     // Catch:{ UnavailableStorageException -> 0x0042, Exception -> 0x0060 }
                    r1.<init>()     // Catch:{ UnavailableStorageException -> 0x0042, Exception -> 0x0060 }
                    java.lang.String r7 = "com.fsck.k9.MessagingController.emptyTrash"
                    r1.command = r7     // Catch:{ UnavailableStorageException -> 0x0042, Exception -> 0x0060 }
                    java.lang.String[] r7 = com.fsck.k9.controller.MessagingController.EMPTY_STRING_ARRAY     // Catch:{ UnavailableStorageException -> 0x0042, Exception -> 0x0060 }
                    r1.arguments = r7     // Catch:{ UnavailableStorageException -> 0x0042, Exception -> 0x0060 }
                    com.fsck.k9.controller.MessagingController r7 = com.fsck.k9.controller.MessagingController.this     // Catch:{ UnavailableStorageException -> 0x0042, Exception -> 0x0060 }
                    com.fsck.k9.Account r8 = r3     // Catch:{ UnavailableStorageException -> 0x0042, Exception -> 0x0060 }
                    r7.queuePendingCommand(r8, r1)     // Catch:{ UnavailableStorageException -> 0x0042, Exception -> 0x0060 }
                    com.fsck.k9.controller.MessagingController r7 = com.fsck.k9.controller.MessagingController.this     // Catch:{ UnavailableStorageException -> 0x0042, Exception -> 0x0060 }
                    com.fsck.k9.Account r8 = r3     // Catch:{ UnavailableStorageException -> 0x0042, Exception -> 0x0060 }
                    r7.processPendingCommands(r8)     // Catch:{ UnavailableStorageException -> 0x0042, Exception -> 0x0060 }
                L_0x0093:
                    com.fsck.k9.controller.MessagingController.closeFolder(r5)
                    goto L_0x0073
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fsck.k9.controller.MessagingController.AnonymousClass35.run():void");
            }
        });
    }

    /* access modifiers changed from: private */
    public boolean isTrashLocalOnly(Account account) throws MessagingException {
        return account.getRemoteStore() instanceof Pop3Store;
    }

    public void sendAlternate(Context context2, Account account, LocalMessage message) {
        if (K9.DEBUG) {
            Log.d("k9", "Got message " + account.getDescription() + ":" + message.getFolder() + ":" + message.getUid() + " for sendAlternate");
        }
        Intent msg = new Intent("android.intent.action.SEND");
        String quotedText = null;
        Part part = MimeUtility.findFirstPartByMimeType(message, ContentTypeField.TYPE_TEXT_PLAIN);
        if (part == null) {
            part = MimeUtility.findFirstPartByMimeType(message, "text/html");
        }
        if (part != null) {
            quotedText = MessageExtractor.getTextFromPart(part);
        }
        if (quotedText != null) {
            msg.putExtra("android.intent.extra.TEXT", quotedText);
        }
        msg.putExtra("android.intent.extra.SUBJECT", message.getSubject());
        Address[] from = message.getFrom();
        String[] senders = new String[from.length];
        for (int i = 0; i < from.length; i++) {
            senders[i] = from[i].toString();
        }
        msg.putExtra(K9.Intents.Share.EXTRA_FROM, senders);
        Address[] to = message.getRecipients(Message.RecipientType.TO);
        String[] recipientsTo = new String[to.length];
        for (int i2 = 0; i2 < to.length; i2++) {
            recipientsTo[i2] = to[i2].toString();
        }
        msg.putExtra("android.intent.extra.EMAIL", recipientsTo);
        Address[] cc = message.getRecipients(Message.RecipientType.CC);
        String[] recipientsCc = new String[cc.length];
        for (int i3 = 0; i3 < cc.length; i3++) {
            recipientsCc[i3] = cc[i3].toString();
        }
        msg.putExtra("android.intent.extra.CC", recipientsCc);
        msg.setType(ContentTypeField.TYPE_TEXT_PLAIN);
        context2.startActivity(Intent.createChooser(msg, context2.getString(R.string.send_alternate_chooser_title)));
    }

    public void checkMail(Context context2, Account account, boolean ignoreLastCheckedTime, boolean useManualWakeLock, MessagingListener listener) {
        TracingPowerManager.TracingWakeLock twakeLock = null;
        if (useManualWakeLock) {
            twakeLock = TracingPowerManager.getPowerManager(context2).newWakeLock(1, "K9 MessagingController.checkMail");
            twakeLock.setReferenceCounted(false);
            twakeLock.acquire(120000);
        }
        final TracingPowerManager.TracingWakeLock wakeLock = twakeLock;
        for (MessagingListener l : getListeners()) {
            l.checkMailStarted(context2, account);
        }
        final Context context3 = context2;
        final Account account2 = account;
        final boolean z = ignoreLastCheckedTime;
        final MessagingListener messagingListener = listener;
        putBackground("checkMail", listener, new Runnable() {
            public void run() {
                Collection<Account> accounts;
                try {
                    if (K9.DEBUG) {
                        Log.i("k9", "Starting mail check");
                    }
                    Preferences prefs = Preferences.getPreferences(context3);
                    if (account2 != null) {
                        accounts = new ArrayList<>(1);
                        accounts.add(account2);
                    } else {
                        accounts = prefs.getAvailableAccounts();
                    }
                    for (Account account : accounts) {
                        MessagingController.this.checkMailForAccount(context3, account, z, messagingListener);
                    }
                } catch (Exception e) {
                    Log.e("k9", "Unable to synchronize mail", e);
                    MessagingController.this.addErrorMessage(account2, (String) null, e);
                }
                MessagingController.this.putBackground("finalize sync", null, new Runnable() {
                    public void run() {
                        if (K9.DEBUG) {
                            Log.i("k9", "Finished mail sync");
                        }
                        if (wakeLock != null) {
                            wakeLock.release();
                        }
                        for (MessagingListener l : MessagingController.this.getListeners()) {
                            l.checkMailFinished(context3, account2);
                        }
                    }
                });
            }
        });
    }

    /* access modifiers changed from: private */
    public void checkMailForAccount(Context context2, Account account, boolean ignoreLastCheckedTime, MessagingListener listener) {
        String str;
        if (account.isAvailable(context2)) {
            long accountInterval = (long) (account.getAutomaticCheckIntervalMinutes() * 60 * 1000);
            if (ignoreLastCheckedTime || accountInterval > 0) {
                if (K9.DEBUG) {
                    Log.i("k9", "Synchronizing account " + account.getDescription());
                }
                account.setRingNotified(false);
                sendPendingMessages(account, listener);
                try {
                    Account.FolderMode aDisplayMode = account.getFolderDisplayMode();
                    Account.FolderMode aSyncMode = account.getFolderSyncMode();
                    for (Folder folder : account.getLocalStore().getPersonalNamespaces(false)) {
                        folder.open(0);
                        Folder.FolderClass fDisplayClass = folder.getDisplayClass();
                        Folder.FolderClass fSyncClass = folder.getSyncClass();
                        if (!modeMismatch(aDisplayMode, fDisplayClass) && !modeMismatch(aSyncMode, fSyncClass)) {
                            synchronizeFolder(account, folder, ignoreLastCheckedTime, accountInterval, listener);
                        }
                    }
                } catch (MessagingException e) {
                    Log.e("k9", "Unable to synchronize account " + account.getName(), e);
                    addErrorMessage(account, (String) null, e);
                } finally {
                    str = "clear notification flag for ";
                    final Account account2 = account;
                    final Context context3 = context2;
                    putBackground(str + account.getDescription(), null, new Runnable() {
                        public void run() {
                            if (K9.DEBUG) {
                                Log.v("k9", "Clearing notification flag for " + account2.getDescription());
                            }
                            account2.setRingNotified(false);
                            try {
                                AccountStats stats = account2.getStats(context3);
                                if (stats == null || stats.unreadMessageCount == 0) {
                                    MessagingController.this.notificationController.clearNewMailNotifications(account2);
                                }
                            } catch (MessagingException e) {
                                Log.e("k9", "Unable to getUnreadMessageCount for account: " + account2, e);
                            }
                        }
                    });
                }
            } else if (K9.DEBUG) {
                Log.i("k9", "Skipping synchronizing account " + account.getDescription());
            }
        } else if (K9.DEBUG) {
            Log.i("k9", "Skipping synchronizing unavailable account " + account.getDescription());
        }
    }

    private void synchronizeFolder(Account account, Folder folder, boolean ignoreLastCheckedTime, long accountInterval, MessagingListener listener) {
        if (K9.DEBUG) {
            Log.v("k9", "Folder " + folder.getName() + " was last synced @ " + new Date(folder.getLastChecked()));
        }
        if (ignoreLastCheckedTime || folder.getLastChecked() <= System.currentTimeMillis() - accountInterval) {
            final Account account2 = account;
            final Folder folder2 = folder;
            final boolean z = ignoreLastCheckedTime;
            final long j = accountInterval;
            final MessagingListener messagingListener = listener;
            putBackground("sync" + folder.getName(), null, new Runnable() {
                public void run() {
                    LocalFolder tLocalFolder = null;
                    try {
                        tLocalFolder = account2.getLocalStore().getFolder(folder2.getName());
                        tLocalFolder.open(0);
                        if (z || tLocalFolder.getLastChecked() <= System.currentTimeMillis() - j) {
                            MessagingController.this.showFetchingMailNotificationIfNecessary(account2, folder2);
                            MessagingController.this.synchronizeMailboxSynchronous(account2, folder2.getName(), messagingListener, null);
                            MessagingController.this.clearFetchingMailNotificationIfNecessary(account2);
                            MessagingController.closeFolder(tLocalFolder);
                            return;
                        }
                        if (K9.DEBUG) {
                            Log.v("k9", "Not running Command for folder " + folder2.getName() + ", previously synced @ " + new Date(folder2.getLastChecked()) + " which would be too recent for the account period");
                        }
                        MessagingController.closeFolder(tLocalFolder);
                    } catch (Exception e) {
                        try {
                            Log.e("k9", "Exception while processing folder " + account2.getDescription() + ":" + folder2.getName(), e);
                            MessagingController.this.addErrorMessage(account2, (String) null, e);
                        } finally {
                            MessagingController.closeFolder(tLocalFolder);
                        }
                    } catch (Throwable th) {
                        MessagingController.this.clearFetchingMailNotificationIfNecessary(account2);
                        throw th;
                    }
                }
            });
        } else if (K9.DEBUG) {
            Log.v("k9", "Not syncing folder " + folder.getName() + ", previously synced @ " + new Date(folder.getLastChecked()) + " which would be too recent for the account period");
        }
    }

    /* access modifiers changed from: private */
    public void showFetchingMailNotificationIfNecessary(Account account, Folder folder) {
        if (account.isShowOngoing()) {
            this.notificationController.showFetchingMailNotification(account, folder);
        }
    }

    /* access modifiers changed from: private */
    public void clearFetchingMailNotificationIfNecessary(Account account) {
        if (account.isShowOngoing()) {
            this.notificationController.clearFetchingMailNotification(account);
        }
    }

    public void compact(final Account account, final MessagingListener ml) {
        putBackground("compact:" + account.getDescription(), ml, new Runnable() {
            public void run() {
                try {
                    LocalStore localStore = account.getLocalStore();
                    long oldSize = localStore.getSize();
                    localStore.compact();
                    long newSize = localStore.getSize();
                    for (MessagingListener l : MessagingController.this.getListeners(ml)) {
                        l.accountSizeChanged(account, oldSize, newSize);
                    }
                } catch (UnavailableStorageException e) {
                    Log.i("k9", "Failed to compact account because storage is not available - trying again later.");
                    throw new UnavailableAccountException(e);
                } catch (Exception e2) {
                    Log.e("k9", "Failed to compact account " + account.getDescription(), e2);
                }
            }
        });
    }

    public void clear(final Account account, final MessagingListener ml) {
        putBackground("clear:" + account.getDescription(), ml, new Runnable() {
            public void run() {
                try {
                    LocalStore localStore = account.getLocalStore();
                    long oldSize = localStore.getSize();
                    localStore.clear();
                    localStore.resetVisibleLimits(account.getDisplayCount());
                    long newSize = localStore.getSize();
                    AccountStats stats = new AccountStats();
                    stats.size = newSize;
                    stats.unreadMessageCount = 0;
                    stats.flaggedMessageCount = 0;
                    for (MessagingListener l : MessagingController.this.getListeners(ml)) {
                        l.accountSizeChanged(account, oldSize, newSize);
                        l.accountStatusChanged(account, stats);
                    }
                } catch (UnavailableStorageException e) {
                    Log.i("k9", "Failed to clear account because storage is not available - trying again later.");
                    throw new UnavailableAccountException(e);
                } catch (Exception e2) {
                    Log.e("k9", "Failed to clear account " + account.getDescription(), e2);
                }
            }
        });
    }

    public void recreate(final Account account, final MessagingListener ml) {
        putBackground("recreate:" + account.getDescription(), ml, new Runnable() {
            public void run() {
                try {
                    LocalStore localStore = account.getLocalStore();
                    long oldSize = localStore.getSize();
                    localStore.recreate();
                    localStore.resetVisibleLimits(account.getDisplayCount());
                    long newSize = localStore.getSize();
                    AccountStats stats = new AccountStats();
                    stats.size = newSize;
                    stats.unreadMessageCount = 0;
                    stats.flaggedMessageCount = 0;
                    for (MessagingListener l : MessagingController.this.getListeners(ml)) {
                        l.accountSizeChanged(account, oldSize, newSize);
                        l.accountStatusChanged(account, stats);
                    }
                } catch (UnavailableStorageException e) {
                    Log.i("k9", "Failed to recreate an account because storage is not available - trying again later.");
                    throw new UnavailableAccountException(e);
                } catch (Exception e2) {
                    Log.e("k9", "Failed to recreate account " + account.getDescription(), e2);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public boolean shouldNotifyForMessage(Account account, LocalFolder localFolder, Message message) {
        if (account.getName() == null || !account.isNotifyNewMail() || message.isSet(Flag.SEEN)) {
            return false;
        }
        Account.FolderMode aDisplayMode = account.getFolderDisplayMode();
        Account.FolderMode aNotifyMode = account.getFolderNotifyNewMailMode();
        Folder.FolderClass fDisplayClass = localFolder.getDisplayClass();
        Folder.FolderClass fNotifyClass = localFolder.getNotifyClass();
        if (modeMismatch(aDisplayMode, fDisplayClass) || modeMismatch(aNotifyMode, fNotifyClass)) {
            return false;
        }
        if (account.getStoreUri().startsWith("pop3") && message.olderThan(new Date(account.getLatestOldMessageSeenTime()))) {
            return false;
        }
        Folder folder = message.getFolder();
        if (folder != null) {
            String folderName = folder.getName();
            if (!account.getInboxFolderName().equals(folderName) && (account.getTrashFolderName().equals(folderName) || account.getDraftsFolderName().equals(folderName) || account.getSpamFolderName().equals(folderName) || account.getSentFolderName().equals(folderName))) {
                return false;
            }
        }
        if (!(message.getUid() == null || localFolder.getLastUid() == null)) {
            try {
                Integer messageUid = Integer.valueOf(Integer.parseInt(message.getUid()));
                if (messageUid.intValue() <= localFolder.getLastUid().intValue()) {
                    if (!K9.DEBUG) {
                        return false;
                    }
                    Log.d("k9", "Message uid is " + messageUid + ", max message uid is " + localFolder.getLastUid() + ".  Skipping notification.");
                    return false;
                }
            } catch (NumberFormatException e) {
            }
        }
        if (account.isAnIdentity(message.getFrom()) && !account.isNotifySelfNewMail()) {
            return false;
        }
        if (!account.isNotifyContactsMailOnly() || this.contacts.isAnyInContacts(message.getFrom())) {
            return true;
        }
        return false;
    }

    public void deleteAccount(Account account) {
        this.notificationController.clearNewMailNotifications(account);
        this.memorizingMessagingListener.removeAccount(account);
    }

    public Message saveDraft(Account account, Message message, long existingDraftId, boolean saveRemotely) {
        Message localMessage = null;
        try {
            LocalFolder localFolder = account.getLocalStore().getFolder(account.getDraftsFolderName());
            localFolder.open(0);
            if (existingDraftId != -1) {
                message.setUid(localFolder.getMessageUidById(existingDraftId));
            }
            localFolder.appendMessages(Collections.singletonList(message));
            localMessage = localFolder.getMessage(message.getUid());
            localMessage.setFlag(Flag.X_DOWNLOADED_FULL, true);
            if (saveRemotely) {
                LocalStore.PendingCommand command = new LocalStore.PendingCommand();
                command.command = PENDING_COMMAND_APPEND;
                command.arguments = new String[]{localFolder.getName(), localMessage.getUid()};
                queuePendingCommand(account, command);
                processPendingCommands(account);
            }
        } catch (MessagingException e) {
            Log.e("k9", "Unable to save message as draft.", e);
            addErrorMessage(account, (String) null, e);
        }
        return localMessage;
    }

    public long getId(Message message) {
        if (message instanceof LocalMessage) {
            return message.getId();
        }
        Log.w("k9", "MessagingController.getId() called without a LocalMessage");
        return -1;
    }

    private boolean modeMismatch(Account.FolderMode aMode, Folder.FolderClass fMode) {
        if (aMode == Account.FolderMode.NONE || ((aMode == Account.FolderMode.FIRST_CLASS && fMode != Folder.FolderClass.FIRST_CLASS) || ((aMode == Account.FolderMode.FIRST_AND_SECOND_CLASS && fMode != Folder.FolderClass.FIRST_CLASS && fMode != Folder.FolderClass.SECOND_CLASS) || (aMode == Account.FolderMode.NOT_SECOND_CLASS && fMode == Folder.FolderClass.SECOND_CLASS)))) {
            return true;
        }
        return false;
    }

    private static class Command implements Comparable<Command> {
        public String description;
        boolean isForegroundPriority;
        public MessagingListener listener;
        public Runnable runnable;
        int sequence;

        private Command() {
            this.sequence = MessagingController.sequencing.getAndIncrement();
        }

        public int compareTo(@NonNull Command other) {
            if (other.isForegroundPriority && !this.isForegroundPriority) {
                return 1;
            }
            if (other.isForegroundPriority || !this.isForegroundPriority) {
                return this.sequence - other.sequence;
            }
            return -1;
        }
    }

    public MessagingListener getCheckMailListener() {
        return this.checkMailListener;
    }

    public void setCheckMailListener(MessagingListener checkMailListener2) {
        if (this.checkMailListener != null) {
            removeListener(this.checkMailListener);
        }
        this.checkMailListener = checkMailListener2;
        if (this.checkMailListener != null) {
            addListener(this.checkMailListener);
        }
    }

    public Collection<Pusher> getPushers() {
        return this.pushers.values();
    }

    public boolean setupPushing(Account account) {
        if (account != null) {
            String mail = account.getEmail();
            if (GmailUtils.isGmailAccount(mail)) {
                SharedPreferences pref = App.getPrefToken(this.context);
                long timeExpireToken = App.getTimeExpireTokenGmailAccount(pref, mail);
                if (timeExpireToken > 3600000 && timeExpireToken < System.currentTimeMillis()) {
                    final Account account2 = account;
                    GmailUtils.queryAccessToken(App.getRefreshTokenGmailAccount(pref, mail), this.context, account, true, false, new GmailUtils.CallBackAccessToken() {
                        public void sucess() {
                            MessagingController.this.addErrorMessage(account2, "Sucess getting token at MessagingController.java 4474 ", new Exception());
                            MessagingController.this.setupPushing(account2);
                        }

                        public void error(String msg) {
                            MessagingController.this.handleAuthenticationFailure(account2, true);
                            String msg2 = "Error getting token Error: " + msg + " at MessagingController.java 4490";
                            MessagingController.addErrorMessage(account2, msg2, msg2);
                        }
                    });
                    return true;
                }
            }
        }
        try {
            Pusher previousPusher = this.pushers.remove(account);
            if (previousPusher != null) {
                previousPusher.stop();
            }
            Account.FolderMode aDisplayMode = account.getFolderDisplayMode();
            Account.FolderMode aPushMode = account.getFolderPushMode();
            List<String> names = new ArrayList<>();
            for (Folder folder : account.getLocalStore().getPersonalNamespaces(false)) {
                if (!folder.getName().equals(account.getErrorFolderName()) && !folder.getName().equals(account.getOutboxFolderName())) {
                    folder.open(0);
                    Folder.FolderClass fDisplayClass = folder.getDisplayClass();
                    Folder.FolderClass fPushClass = folder.getPushClass();
                    if (!modeMismatch(aDisplayMode, fDisplayClass) && !modeMismatch(aPushMode, fPushClass)) {
                        if (K9.DEBUG) {
                            Log.i("k9", "Starting pusher for " + account.getDescription() + ":" + folder.getName());
                        }
                        names.add(folder.getName());
                    }
                }
            }
            if (!names.isEmpty()) {
                MessagingControllerPushReceiver messagingControllerPushReceiver = new MessagingControllerPushReceiver(this.context, account, this);
                int maxPushFolders = account.getMaxPushFolders();
                if (names.size() > maxPushFolders) {
                    if (K9.DEBUG) {
                        Log.i("k9", "Count of folders to push for account " + account.getDescription() + " is " + names.size() + ", greater than limit of " + maxPushFolders + ", truncating");
                    }
                    names = names.subList(0, maxPushFolders);
                }
                try {
                    Store store = account.getRemoteStore();
                    if (!store.isPushCapable()) {
                        if (K9.DEBUG) {
                            Log.i("k9", "Account " + account.getDescription() + " is not push capable, skipping");
                        }
                        return false;
                    }
                    Pusher pusher = store.getPusher(messagingControllerPushReceiver);
                    if (pusher != null && this.pushers.putIfAbsent(account, pusher) == null) {
                        pusher.start(names);
                    }
                    return true;
                } catch (Exception e) {
                    Log.e("k9", "Could not get remote store", e);
                    return false;
                }
            } else {
                if (K9.DEBUG) {
                    Log.i("k9", "No folders are configured for pushing in account " + account.getDescription());
                }
                return false;
            }
        } catch (Exception e2) {
            Log.e("k9", "Got exception while setting up pushing", e2);
            return false;
        }
    }

    public void stopAllPushing() {
        if (K9.DEBUG) {
            Log.i("k9", "Stopping all pushers");
        }
        Iterator<Pusher> iter = this.pushers.values().iterator();
        while (iter.hasNext()) {
            iter.remove();
            iter.next().stop();
        }
    }

    public void messagesArrived(Account account, Folder remoteFolder, List<Message> messages, boolean flagSyncOnly) {
        if (K9.DEBUG) {
            Log.i("k9", "Got new pushed email messages for account " + account.getDescription() + ", folder " + remoteFolder.getName());
        }
        final CountDownLatch latch = new CountDownLatch(1);
        final Account account2 = account;
        final Folder folder = remoteFolder;
        final List<Message> list = messages;
        final boolean z = flagSyncOnly;
        putBackground("Push messageArrived of account " + account.getDescription() + ", folder " + remoteFolder.getName(), null, new Runnable() {
            public void run() {
                LocalFolder localFolder = null;
                try {
                    localFolder = account2.getLocalStore().getFolder(folder.getName());
                    localFolder.open(0);
                    account2.setRingNotified(false);
                    int newCount = MessagingController.this.downloadMessages(account2, folder, localFolder, list, z, true);
                    int unreadMessageCount = localFolder.getUnreadMessageCount();
                    localFolder.setLastPush(System.currentTimeMillis());
                    localFolder.setStatus(null);
                    if (K9.DEBUG) {
                        Log.i("k9", "messagesArrived newCount = " + newCount + ", unread count = " + unreadMessageCount);
                    }
                    if (unreadMessageCount == 0) {
                        MessagingController.this.notificationController.clearNewMailNotifications(account2);
                    }
                    for (MessagingListener l : MessagingController.this.getListeners()) {
                        l.folderStatusChanged(account2, folder.getName(), unreadMessageCount);
                    }
                } catch (Exception e) {
                    String errorMessage = "Push failed: " + MessagingController.getRootCauseMessage(e);
                    try {
                        localFolder.setStatus(errorMessage);
                    } catch (Exception se) {
                        Log.e("k9", "Unable to set failed status on localFolder", se);
                    }
                    for (MessagingListener l2 : MessagingController.this.getListeners()) {
                        l2.synchronizeMailboxFailed(account2, folder.getName(), errorMessage);
                    }
                    MessagingController.this.addErrorMessage(account2, (String) null, e);
                } finally {
                    MessagingController.closeFolder(localFolder);
                    latch.countDown();
                }
            }
        });
        try {
            latch.await();
        } catch (Exception e) {
            Log.e("k9", "Interrupted while awaiting latch release", e);
        }
        if (K9.DEBUG) {
            Log.i("k9", "MessagingController.messagesArrivedLatch released");
        }
    }

    public void systemStatusChanged() {
        for (MessagingListener l : getListeners()) {
            l.systemStatusChanged();
        }
    }

    public void cancelNotificationsForAccount(Account account) {
        this.notificationController.clearNewMailNotifications(account);
    }

    public void cancelNotificationForMessage(Account account, MessageReference messageReference) {
        this.notificationController.removeNewMailNotification(account, messageReference);
    }

    public void clearCertificateErrorNotifications(Account account, AccountSetupCheckSettings.CheckDirection direction) {
        this.notificationController.clearCertificateErrorNotifications(account, direction == AccountSetupCheckSettings.CheckDirection.INCOMING);
    }

    public void notifyUserIfCertificateProblem(Account account, Exception exception, boolean incoming) {
        if ((exception instanceof CertificateValidationException) && ((CertificateValidationException) exception).needsUserAttention()) {
            this.notificationController.showCertificateErrorNotification(account, incoming);
        }
    }

    private void actOnMessagesGroupedByAccountAndFolder(List<MessageReference> messages, MessageActor actor) {
        for (Map.Entry<String, Map<String, List<MessageReference>>> entry : groupMessagesByAccountAndFolder(messages).entrySet()) {
            Account account = Preferences.getPreferences(this.context).getAccount((String) entry.getKey());
            for (Map.Entry<String, List<MessageReference>> folderEntry : entry.getValue().entrySet()) {
                actOnMessageGroup(account, (String) folderEntry.getKey(), folderEntry.getValue(), actor);
            }
        }
    }

    @NonNull
    private Map<String, Map<String, List<MessageReference>>> groupMessagesByAccountAndFolder(List<MessageReference> messages) {
        Map<String, Map<String, List<MessageReference>>> accountMap = new HashMap<>();
        for (MessageReference message : messages) {
            if (message != null) {
                String accountUuid = message.getAccountUuid();
                String folderName = message.getFolderName();
                Map<String, List<MessageReference>> folderMap = accountMap.get(accountUuid);
                if (folderMap == null) {
                    folderMap = new HashMap<>();
                    accountMap.put(accountUuid, folderMap);
                }
                List<MessageReference> messageList = folderMap.get(folderName);
                if (messageList == null) {
                    messageList = new LinkedList<>();
                    folderMap.put(folderName, messageList);
                }
                messageList.add(message);
            }
        }
        return accountMap;
    }

    private void actOnMessageGroup(Account account, String folderName, List<MessageReference> messageReferences, MessageActor actor) {
        try {
            LocalFolder messageFolder = account.getLocalStore().getFolder(folderName);
            actor.act(account, messageFolder, messageFolder.getMessagesByReference(messageReferences));
        } catch (MessagingException e) {
            Log.e("k9", "Error loading account?!", e);
        }
    }
}
