package com.fsck.k9.fragment;

import android.database.Cursor;
import com.fsck.k9.provider.AttachmentProvider;
import java.util.Comparator;
import java.util.List;

public class MessageListFragmentComparators {

    public static class ReverseComparator<T> implements Comparator<T> {
        private Comparator<T> mDelegate;

        public ReverseComparator(Comparator<T> delegate) {
            this.mDelegate = delegate;
        }

        public int compare(T object1, T object2) {
            return this.mDelegate.compare(object2, object1);
        }
    }

    public static class ComparatorChain<T> implements Comparator<T> {
        private List<Comparator<T>> mChain;

        public ComparatorChain(List<Comparator<T>> chain) {
            this.mChain = chain;
        }

        /* JADX WARNING: Removed duplicated region for block: B:3:0x000d  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public int compare(T r5, T r6) {
            /*
                r4 = this;
                r1 = 0
                java.util.List<java.util.Comparator<T>> r2 = r4.mChain
                java.util.Iterator r2 = r2.iterator()
            L_0x0007:
                boolean r3 = r2.hasNext()
                if (r3 == 0) goto L_0x0019
                java.lang.Object r0 = r2.next()
                java.util.Comparator r0 = (java.util.Comparator) r0
                int r1 = r0.compare(r5, r6)
                if (r1 == 0) goto L_0x0007
            L_0x0019:
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fsck.k9.fragment.MessageListFragmentComparators.ComparatorChain.compare(java.lang.Object, java.lang.Object):int");
        }
    }

    public static class ReverseIdComparator implements Comparator<Cursor> {
        private int mIdColumn = -1;

        public int compare(Cursor cursor1, Cursor cursor2) {
            if (this.mIdColumn == -1) {
                this.mIdColumn = cursor1.getColumnIndex(AttachmentProvider.AttachmentProviderColumns._ID);
            }
            if (cursor1.getLong(this.mIdColumn) > cursor2.getLong(this.mIdColumn)) {
                return -1;
            }
            return 1;
        }
    }

    public static class AttachmentComparator implements Comparator<Cursor> {
        public int compare(Cursor cursor1, Cursor cursor2) {
            int o1HasAttachment;
            int o2HasAttachment;
            if (cursor1.getInt(12) > 0) {
                o1HasAttachment = 0;
            } else {
                o1HasAttachment = 1;
            }
            if (cursor2.getInt(12) > 0) {
                o2HasAttachment = 0;
            } else {
                o2HasAttachment = 1;
            }
            return o1HasAttachment - o2HasAttachment;
        }
    }

    public static class FlaggedComparator implements Comparator<Cursor> {
        public int compare(Cursor cursor1, Cursor cursor2) {
            int o1IsFlagged;
            int o2IsFlagged;
            if (cursor1.getInt(9) == 1) {
                o1IsFlagged = 0;
            } else {
                o1IsFlagged = 1;
            }
            if (cursor2.getInt(9) == 1) {
                o2IsFlagged = 0;
            } else {
                o2IsFlagged = 1;
            }
            return o1IsFlagged - o2IsFlagged;
        }
    }

    public static class UnreadComparator implements Comparator<Cursor> {
        public int compare(Cursor cursor1, Cursor cursor2) {
            return cursor1.getInt(8) - cursor2.getInt(8);
        }
    }

    public static class DateComparator implements Comparator<Cursor> {
        public int compare(Cursor cursor1, Cursor cursor2) {
            long o1Date = cursor1.getLong(4);
            long o2Date = cursor2.getLong(4);
            if (o1Date < o2Date) {
                return -1;
            }
            if (o1Date == o2Date) {
                return 0;
            }
            return 1;
        }
    }

    public static class ArrivalComparator implements Comparator<Cursor> {
        public int compare(Cursor cursor1, Cursor cursor2) {
            long o1Date = cursor1.getLong(2);
            long o2Date = cursor2.getLong(2);
            if (o1Date == o2Date) {
                return 0;
            }
            if (o1Date < o2Date) {
                return -1;
            }
            return 1;
        }
    }

    public static class SubjectComparator implements Comparator<Cursor> {
        public int compare(Cursor cursor1, Cursor cursor2) {
            String subject1 = cursor1.getString(3);
            String subject2 = cursor2.getString(3);
            if (subject1 == null) {
                return subject2 == null ? 0 : -1;
            }
            if (subject2 == null) {
                return 1;
            }
            return subject1.compareToIgnoreCase(subject2);
        }
    }

    public static class SenderComparator implements Comparator<Cursor> {
        public int compare(Cursor cursor1, Cursor cursor2) {
            String sender1 = MessageListFragment.getSenderAddressFromCursor(cursor1);
            String sender2 = MessageListFragment.getSenderAddressFromCursor(cursor2);
            if (sender1 == null && sender2 == null) {
                return 0;
            }
            if (sender1 == null) {
                return 1;
            }
            if (sender2 == null) {
                return -1;
            }
            return sender1.compareToIgnoreCase(sender2);
        }
    }
}
