package com.fsck.k9.fragment;

import android.content.Context;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

public class AkRecyclerView extends RecyclerView {
    public AkRecyclerView(Context context) {
        super(context);
    }

    public AkRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public AkRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable state) {
        super.onRestoreInstanceState(state);
    }

    public void onRestoreInstanceState(Parcelable state, boolean unUsedParam) {
        super.onRestoreInstanceState(state);
    }

    public Parcelable onSaveInstanceState(boolean unUsedParam) {
        return super.onSaveInstanceState();
    }
}
