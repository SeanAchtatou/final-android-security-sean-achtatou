package com.fsck.k9.provider;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.util.Log;
import com.fsck.k9.K9;
import com.fsck.k9.mailstore.util.FileFactory;
import com.fsck.k9.provider.EmailProvider;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Locale;
import org.apache.james.mime4j.codec.Base64InputStream;
import org.apache.james.mime4j.codec.QuotedPrintableInputStream;
import org.apache.james.mime4j.util.MimeUtil;
import org.openintents.openpgp.util.ParcelFileDescriptorUtil;

public class DecryptedFileProvider extends FileProvider {
    private static final String AUTHORITY = "yahoo.mail.app.decryptedfileprovider";
    private static final String DECRYPTED_CACHE_DIRECTORY = "decrypted";
    private static final long FILE_DELETE_THRESHOLD_MILLISECONDS = 180000;
    private static DecryptedFileProviderCleanupReceiver cleanupReceiver = null;
    private static final Object cleanupReceiverMonitor = new Object();

    public static FileFactory getFileFactory(Context context) {
        final Context applicationContext = context.getApplicationContext();
        return new FileFactory() {
            public File createFile() throws IOException {
                DecryptedFileProvider.registerFileCleanupReceiver(applicationContext);
                return File.createTempFile("decrypted-", null, DecryptedFileProvider.getDecryptedTempDirectory(applicationContext));
            }
        };
    }

    @Nullable
    public static Uri getUriForProvidedFile(@NonNull Context context, File file, @Nullable String encoding, @Nullable String mimeType) throws IOException {
        try {
            Uri.Builder uriBuilder = FileProvider.getUriForFile(context, AUTHORITY, file).buildUpon();
            if (mimeType != null) {
                uriBuilder.appendQueryParameter(EmailProvider.InternalMessageColumns.MIME_TYPE, mimeType);
            }
            if (encoding != null) {
                uriBuilder.appendQueryParameter("encoding", encoding);
            }
            return uriBuilder.build();
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

    public static boolean deleteOldTemporaryFiles(Context context) {
        File tempDirectory = getDecryptedTempDirectory(context);
        boolean allFilesDeleted = true;
        long deletionThreshold = new Date().getTime() - FILE_DELETE_THRESHOLD_MILLISECONDS;
        for (File tempFile : tempDirectory.listFiles()) {
            long lastModified = tempFile.lastModified();
            if (lastModified >= deletionThreshold) {
                if (K9.DEBUG) {
                    Log.e("k9", "Not deleting temp file (for another " + String.format(Locale.ENGLISH, "%.2f", Double.valueOf(((double) ((lastModified - deletionThreshold) / 1000)) / 60.0d)) + " minutes)");
                }
                allFilesDeleted = false;
            } else if (!tempFile.delete()) {
                Log.e("k9", "Failed to delete temporary file");
                allFilesDeleted = false;
            }
        }
        return allFilesDeleted;
    }

    /* access modifiers changed from: private */
    public static File getDecryptedTempDirectory(Context context) {
        File directory = new File(context.getCacheDir(), DECRYPTED_CACHE_DIRECTORY);
        if (!directory.exists() && !directory.mkdir()) {
            Log.e("k9", "Error creating directory: " + directory.getAbsolutePath());
        }
        return directory;
    }

    public String getType(Uri uri) {
        return uri.getQueryParameter(EmailProvider.InternalMessageColumns.MIME_TYPE);
    }

    public int delete(Uri uri, String selection, String[] selectionArgs) {
        throw new UnsupportedOperationException();
    }

    public ParcelFileDescriptor openFile(Uri uri, String mode) throws FileNotFoundException {
        InputStream decodedInputStream;
        ParcelFileDescriptor pfd = super.openFile(uri, "r");
        String encoding = uri.getQueryParameter("encoding");
        if (MimeUtil.isBase64Encoding(encoding)) {
            decodedInputStream = new Base64InputStream(new ParcelFileDescriptor.AutoCloseInputStream(pfd));
        } else if (MimeUtil.isQuotedPrintableEncoded(encoding)) {
            decodedInputStream = new QuotedPrintableInputStream(new ParcelFileDescriptor.AutoCloseInputStream(pfd));
        } else if (!K9.DEBUG || TextUtils.isEmpty(encoding)) {
            return pfd;
        } else {
            Log.e("k9", "unsupported encoding, returning raw stream");
            return pfd;
        }
        try {
            return ParcelFileDescriptorUtil.pipeFrom(decodedInputStream);
        } catch (IOException e) {
            throw new FileNotFoundException();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0005, code lost:
        r0 = getContext();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onTrimMemory(int r4) {
        /*
            r3 = this;
            r1 = 80
            if (r4 >= r1) goto L_0x0005
        L_0x0004:
            return
        L_0x0005:
            android.content.Context r0 = r3.getContext()
            if (r0 == 0) goto L_0x0004
            com.fsck.k9.provider.DecryptedFileProvider$2 r1 = new com.fsck.k9.provider.DecryptedFileProvider$2
            r1.<init>(r0)
            r2 = 0
            java.lang.Void[] r2 = new java.lang.Void[r2]
            r1.execute(r2)
            unregisterFileCleanupReceiver(r0)
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fsck.k9.provider.DecryptedFileProvider.onTrimMemory(int):void");
    }

    /* access modifiers changed from: private */
    public static void unregisterFileCleanupReceiver(Context context) {
        synchronized (cleanupReceiverMonitor) {
            if (cleanupReceiver != null) {
                if (K9.DEBUG) {
                    Log.d("k9", "Unregistering temp file cleanup receiver");
                }
                context.unregisterReceiver(cleanupReceiver);
                cleanupReceiver = null;
            }
        }
    }

    /* access modifiers changed from: private */
    public static void registerFileCleanupReceiver(Context context) {
        synchronized (cleanupReceiverMonitor) {
            if (cleanupReceiver == null) {
                if (K9.DEBUG) {
                    Log.d("k9", "Registering temp file cleanup receiver");
                }
                cleanupReceiver = new DecryptedFileProviderCleanupReceiver();
                IntentFilter intentFilter = new IntentFilter();
                intentFilter.addAction("android.intent.action.SCREEN_OFF");
                context.registerReceiver(cleanupReceiver, intentFilter);
            }
        }
    }

    private static class DecryptedFileProviderCleanupReceiver extends BroadcastReceiver {
        private DecryptedFileProviderCleanupReceiver() {
        }

        @MainThread
        public void onReceive(Context context, Intent intent) {
            if (!"android.intent.action.SCREEN_OFF".equals(intent.getAction())) {
                throw new IllegalArgumentException("onReceive called with action that isn't screen off!");
            }
            if (K9.DEBUG) {
                Log.d("k9", "Cleaning up temp files");
            }
            if (DecryptedFileProvider.deleteOldTemporaryFiles(context)) {
                DecryptedFileProvider.unregisterFileCleanupReceiver(context);
            }
        }
    }
}
