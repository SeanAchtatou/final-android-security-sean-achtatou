package com.fsck.k9.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import com.fsck.k9.Account;
import com.fsck.k9.K9;
import com.fsck.k9.Preferences;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.internet.MimeUtility;
import com.fsck.k9.mailstore.LocalStore;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import org.openintents.openpgp.util.ParcelFileDescriptorUtil;

public class AttachmentProvider extends ContentProvider {
    private static final String AUTHORITY = "yahoo.mail.app.attachmentprovider";
    public static final Uri CONTENT_URI = Uri.parse("content://yahoo.mail.app.attachmentprovider");
    private static final String[] DEFAULT_PROJECTION = {AttachmentProviderColumns._ID, AttachmentProviderColumns.DATA};

    public static class AttachmentProviderColumns {
        public static final String DATA = "_data";
        public static final String DISPLAY_NAME = "_display_name";
        public static final String SIZE = "_size";
        public static final String _ID = "_id";
    }

    public static Uri getAttachmentUri(String accountUuid, long id) {
        return CONTENT_URI.buildUpon().appendPath(accountUuid).appendPath(Long.toString(id)).build();
    }

    public boolean onCreate() {
        return true;
    }

    public String getType(@NonNull Uri uri) {
        List<String> segments = uri.getPathSegments();
        return getType(segments.get(0), segments.get(1), segments.size() < 3 ? null : segments.get(2));
    }

    public ParcelFileDescriptor openFile(@NonNull Uri uri, @NonNull String mode) throws FileNotFoundException {
        List<String> segments = uri.getPathSegments();
        ParcelFileDescriptor parcelFileDescriptor = openAttachment(segments.get(0), segments.get(1));
        if (parcelFileDescriptor != null) {
            return parcelFileDescriptor;
        }
        throw new FileNotFoundException("Attachment missing or cannot be opened!");
    }

    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        String[] columnNames;
        if (projection == null) {
            columnNames = DEFAULT_PROJECTION;
        } else {
            columnNames = projection;
        }
        List<String> segments = uri.getPathSegments();
        String accountUuid = segments.get(0);
        String id = segments.get(1);
        try {
            LocalStore.AttachmentInfo attachmentInfo = LocalStore.getInstance(Preferences.getPreferences(getContext()).getAccount(accountUuid), getContext()).getAttachmentInfo(id);
            if (attachmentInfo == null) {
                if (K9.DEBUG) {
                    Log.d("k9", "No attachment info for ID: " + id);
                }
                return null;
            }
            MatrixCursor ret = new MatrixCursor(columnNames);
            Object[] values = new Object[columnNames.length];
            int count = columnNames.length;
            for (int i = 0; i < count; i++) {
                String column = columnNames[i];
                if (AttachmentProviderColumns._ID.equals(column)) {
                    values[i] = id;
                } else if (AttachmentProviderColumns.DATA.equals(column)) {
                    values[i] = uri.toString();
                } else if (AttachmentProviderColumns.DISPLAY_NAME.equals(column)) {
                    values[i] = attachmentInfo.name;
                } else if (AttachmentProviderColumns.SIZE.equals(column)) {
                    values[i] = Long.valueOf(attachmentInfo.size);
                }
            }
            ret.addRow(values);
            return ret;
        } catch (MessagingException e) {
            Log.e("k9", "Unable to retrieve attachment info from local store for ID: " + id, e);
            return null;
        }
    }

    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        throw new UnsupportedOperationException();
    }

    public int delete(@NonNull Uri uri, String arg1, String[] arg2) {
        throw new UnsupportedOperationException();
    }

    public Uri insert(@NonNull Uri uri, ContentValues values) {
        throw new UnsupportedOperationException();
    }

    private String getType(String accountUuid, String id, String mimeType) {
        Account account = Preferences.getPreferences(getContext()).getAccount(accountUuid);
        try {
            LocalStore.AttachmentInfo attachmentInfo = LocalStore.getInstance(account, getContext()).getAttachmentInfo(id);
            if (mimeType != null) {
                return mimeType;
            }
            return attachmentInfo.type;
        } catch (MessagingException e) {
            Log.e("k9", "Unable to retrieve LocalStore for " + account, e);
            return MimeUtility.DEFAULT_ATTACHMENT_MIME_TYPE;
        }
    }

    @Nullable
    private ParcelFileDescriptor openAttachment(String accountUuid, String attachmentId) {
        try {
            InputStream inputStream = getAttachmentInputStream(accountUuid, attachmentId);
            if (inputStream != null) {
                return ParcelFileDescriptorUtil.pipeFrom(inputStream);
            }
            Log.e("k9", "Error getting InputStream for attachment (part doesn't exist?)");
            return null;
        } catch (MessagingException e) {
            Log.e("k9", "Error getting InputStream for attachment", e);
            return null;
        } catch (IOException e2) {
            Log.e("k9", "Error creating ParcelFileDescriptor", e2);
            return null;
        }
    }

    @Nullable
    private InputStream getAttachmentInputStream(String accountUuid, String attachmentId) throws MessagingException {
        return LocalStore.getInstance(Preferences.getPreferences(getContext()).getAccount(accountUuid), getContext()).getAttachmentInputStream(attachmentId);
    }
}
