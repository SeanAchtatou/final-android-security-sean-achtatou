package com.fsck.k9.provider;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import atomicgonza.AccountUtils;
import com.fsck.k9.activity.UnreadWidgetConfiguration;

public class UnreadWidgetProvider extends AppWidgetProvider {
    private static final int MAX_COUNT = 9999;

    public static void updateUnreadCount(Context context) {
        Context appContext = context.getApplicationContext();
        int[] widgetIds = AppWidgetManager.getInstance(appContext).getAppWidgetIds(new ComponentName(appContext, UnreadWidgetProvider.class));
        Intent intent = new Intent(context, UnreadWidgetProvider.class);
        intent.setAction("android.appwidget.action.APPWIDGET_UPDATE");
        intent.putExtra("appWidgetIds", widgetIds);
        context.sendBroadcast(intent);
        AccountUtils.updateUnreadWidget(context);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v0, resolved type: com.fsck.k9.search.SearchAccount} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v0, resolved type: com.fsck.k9.Account} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v1, resolved type: com.fsck.k9.search.SearchAccount} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v2, resolved type: com.fsck.k9.search.SearchAccount} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v3, resolved type: com.fsck.k9.search.SearchAccount} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void updateWidget(android.content.Context r22, android.appwidget.AppWidgetManager r23, int r24, java.lang.String r25) {
        /*
            android.widget.RemoteViews r13 = new android.widget.RemoteViews
            java.lang.String r18 = r22.getPackageName()
            r19 = 2130968702(0x7f04007e, float:1.7546065E38)
            r0 = r18
            r1 = r19
            r13.<init>(r0, r1)
            r17 = 0
            r18 = 2131231029(0x7f080135, float:1.8078127E38)
            r0 = r22
            r1 = r18
            java.lang.String r6 = r0.getString(r1)
            r7 = 0
            r5 = 0
            r16 = 0
            r15 = 0
            java.lang.String r18 = "unified_inbox"
            r0 = r18
            r1 = r25
            boolean r18 = r0.equals(r1)     // Catch:{ Exception -> 0x00fc }
            if (r18 == 0) goto L_0x00b6
            com.fsck.k9.search.SearchAccount r15 = com.fsck.k9.search.SearchAccount.createUnifiedInboxAccount(r22)     // Catch:{ Exception -> 0x00fc }
        L_0x0032:
            if (r15 == 0) goto L_0x00c8
            r5 = r15
            com.fsck.k9.controller.MessagingController r8 = com.fsck.k9.controller.MessagingController.getInstance(r22)     // Catch:{ Exception -> 0x00fc }
            r18 = 0
            r0 = r18
            com.fsck.k9.AccountStats r16 = r8.getSearchAccountStatsSynchronous(r15, r0)     // Catch:{ Exception -> 0x00fc }
            com.fsck.k9.search.LocalSearch r18 = r15.getRelatedSearch()     // Catch:{ Exception -> 0x00fc }
            r19 = 0
            r20 = 1
            r21 = 1
            r0 = r22
            r1 = r18
            r2 = r19
            r3 = r20
            r4 = r21
            android.content.Intent r7 = com.fsck.k9.activity.MessageList.intentDisplaySearch(r0, r1, r2, r3, r4)     // Catch:{ Exception -> 0x00fc }
        L_0x0059:
            if (r5 == 0) goto L_0x005f
            java.lang.String r6 = r5.getDescription()     // Catch:{ Exception -> 0x00fc }
        L_0x005f:
            if (r16 == 0) goto L_0x0067
            r0 = r16
            int r0 = r0.unreadMessageCount     // Catch:{ Exception -> 0x00fc }
            r17 = r0
        L_0x0067:
            if (r17 > 0) goto L_0x013e
            r18 = 2131755427(0x7f1001a3, float:1.9141733E38)
            r19 = 8
            r0 = r18
            r1 = r19
            r13.setViewVisibility(r0, r1)
        L_0x0075:
            r18 = 2131755160(0x7f100098, float:1.9141191E38)
            r0 = r18
            r13.setTextViewText(r0, r6)
            if (r7 != 0) goto L_0x0093
            android.content.Intent r7 = new android.content.Intent
            java.lang.Class<com.fsck.k9.activity.UnreadWidgetConfiguration> r18 = com.fsck.k9.activity.UnreadWidgetConfiguration.class
            r0 = r22
            r1 = r18
            r7.<init>(r0, r1)
            java.lang.String r18 = "appWidgetId"
            r0 = r18
            r1 = r24
            r7.putExtra(r0, r1)
        L_0x0093:
            r18 = 268435456(0x10000000, float:2.5243549E-29)
            r0 = r18
            r7.addFlags(r0)
            r18 = 0
            r0 = r22
            r1 = r24
            r2 = r18
            android.app.PendingIntent r11 = android.app.PendingIntent.getActivity(r0, r1, r7, r2)
            r18 = 2131755426(0x7f1001a2, float:1.914173E38)
            r0 = r18
            r13.setOnClickPendingIntent(r0, r11)
            r0 = r23
            r1 = r24
            r0.updateAppWidget(r1, r13)
            return
        L_0x00b6:
            java.lang.String r18 = "all_messages"
            r0 = r18
            r1 = r25
            boolean r18 = r0.equals(r1)     // Catch:{ Exception -> 0x00fc }
            if (r18 == 0) goto L_0x0032
            com.fsck.k9.search.SearchAccount r15 = com.fsck.k9.search.SearchAccount.createAllMessagesAccount(r22)     // Catch:{ Exception -> 0x00fc }
            goto L_0x0032
        L_0x00c8:
            com.fsck.k9.Preferences r18 = com.fsck.k9.Preferences.getPreferences(r22)     // Catch:{ Exception -> 0x00fc }
            r0 = r18
            r1 = r25
            com.fsck.k9.Account r12 = r0.getAccount(r1)     // Catch:{ Exception -> 0x00fc }
            if (r12 == 0) goto L_0x0059
            r5 = r12
            r0 = r22
            com.fsck.k9.AccountStats r16 = r12.getStats(r0)     // Catch:{ Exception -> 0x00fc }
            java.lang.String r18 = "-NONE-"
            java.lang.String r19 = r12.getAutoExpandFolderName()     // Catch:{ Exception -> 0x00fc }
            boolean r18 = r18.equals(r19)     // Catch:{ Exception -> 0x00fc }
            if (r18 == 0) goto L_0x010e
            r18 = 0
            r0 = r22
            r1 = r18
            android.content.Intent r7 = com.fsck.k9.activity.FolderList.actionHandleAccountIntent(r0, r12, r1)     // Catch:{ Exception -> 0x00fc }
        L_0x00f3:
            r18 = 131072(0x20000, float:1.83671E-40)
            r0 = r18
            r7.addFlags(r0)     // Catch:{ Exception -> 0x00fc }
            goto L_0x0059
        L_0x00fc:
            r10 = move-exception
            boolean r18 = com.fsck.k9.K9.DEBUG
            if (r18 == 0) goto L_0x0067
            java.lang.String r18 = "k9"
            java.lang.String r19 = "Error getting widget configuration"
            r0 = r18
            r1 = r19
            android.util.Log.e(r0, r1, r10)
            goto L_0x0067
        L_0x010e:
            com.fsck.k9.search.LocalSearch r14 = new com.fsck.k9.search.LocalSearch     // Catch:{ Exception -> 0x00fc }
            java.lang.String r18 = r12.getAutoExpandFolderName()     // Catch:{ Exception -> 0x00fc }
            r0 = r18
            r14.<init>(r0)     // Catch:{ Exception -> 0x00fc }
            java.lang.String r18 = r12.getAutoExpandFolderName()     // Catch:{ Exception -> 0x00fc }
            r0 = r18
            r14.addAllowedFolder(r0)     // Catch:{ Exception -> 0x00fc }
            java.lang.String r18 = r5.getUuid()     // Catch:{ Exception -> 0x00fc }
            r0 = r18
            r14.addAccountUuid(r0)     // Catch:{ Exception -> 0x00fc }
            r18 = 0
            r19 = 1
            r20 = 1
            r0 = r22
            r1 = r18
            r2 = r19
            r3 = r20
            android.content.Intent r7 = com.fsck.k9.activity.MessageList.intentDisplaySearch(r0, r14, r1, r2, r3)     // Catch:{ Exception -> 0x00fc }
            goto L_0x00f3
        L_0x013e:
            r18 = 2131755427(0x7f1001a3, float:1.9141733E38)
            r19 = 0
            r0 = r18
            r1 = r19
            r13.setViewVisibility(r0, r1)
            r18 = 9999(0x270f, float:1.4012E-41)
            r0 = r17
            r1 = r18
            if (r0 > r1) goto L_0x0160
            java.lang.String r9 = java.lang.String.valueOf(r17)
        L_0x0156:
            r18 = 2131755427(0x7f1001a3, float:1.9141733E38)
            r0 = r18
            r13.setTextViewText(r0, r9)
            goto L_0x0075
        L_0x0160:
            java.lang.StringBuilder r18 = new java.lang.StringBuilder
            r18.<init>()
            r19 = 9999(0x270f, float:1.4012E-41)
            java.lang.String r19 = java.lang.String.valueOf(r19)
            java.lang.StringBuilder r18 = r18.append(r19)
            java.lang.String r19 = "+"
            java.lang.StringBuilder r18 = r18.append(r19)
            java.lang.String r9 = r18.toString()
            goto L_0x0156
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fsck.k9.provider.UnreadWidgetProvider.updateWidget(android.content.Context, android.appwidget.AppWidgetManager, int, java.lang.String):void");
    }

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (int widgetId : appWidgetIds) {
            updateWidget(context, appWidgetManager, widgetId, UnreadWidgetConfiguration.getAccountUuid(context, widgetId));
        }
    }

    public void onDeleted(Context context, int[] appWidgetIds) {
        for (int appWidgetId : appWidgetIds) {
            UnreadWidgetConfiguration.deleteWidgetConfiguration(context, appWidgetId);
        }
    }
}
