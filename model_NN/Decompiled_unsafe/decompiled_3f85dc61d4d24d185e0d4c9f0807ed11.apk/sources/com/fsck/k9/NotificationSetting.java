package com.fsck.k9;

public class NotificationSetting {
    private boolean mLed;
    private int mLedColor;
    private boolean mRing;
    private String mRingtoneUri;
    private boolean mVibrate;
    private int mVibratePattern;
    private int mVibrateTimes;

    public synchronized void setRing(boolean ring) {
        this.mRing = ring;
    }

    public synchronized boolean shouldRing() {
        return this.mRing;
    }

    public synchronized String getRingtone() {
        return this.mRingtoneUri;
    }

    public synchronized void setRingtone(String ringtoneUri) {
        this.mRingtoneUri = ringtoneUri;
    }

    public synchronized boolean isLed() {
        return this.mLed;
    }

    public synchronized void setLed(boolean led) {
        this.mLed = led;
    }

    public synchronized int getLedColor() {
        return this.mLedColor;
    }

    public synchronized void setLedColor(int color) {
        this.mLedColor = color;
    }

    public synchronized boolean shouldVibrate() {
        return this.mVibrate;
    }

    public synchronized void setVibrate(boolean vibrate) {
        this.mVibrate = vibrate;
    }

    public synchronized int getVibratePattern() {
        return this.mVibratePattern;
    }

    public synchronized int getVibrateTimes() {
        return this.mVibrateTimes;
    }

    public synchronized void setVibratePattern(int pattern) {
        this.mVibratePattern = pattern;
    }

    public synchronized void setVibrateTimes(int times) {
        this.mVibrateTimes = times;
    }

    public long[] getVibration() {
        return getVibration(this.mVibratePattern, this.mVibrateTimes);
    }

    public static long[] getVibration(int pattern, int times) {
        long[] pattern1 = {100, 200};
        long[] pattern2 = {100, 500};
        long[] pattern3 = {200, 200};
        long[] pattern4 = {200, 500};
        long[] pattern5 = {500, 500};
        long[] selectedPattern = {300, 200};
        switch (pattern) {
            case 1:
                selectedPattern = pattern1;
                break;
            case 2:
                selectedPattern = pattern2;
                break;
            case 3:
                selectedPattern = pattern3;
                break;
            case 4:
                selectedPattern = pattern4;
                break;
            case 5:
                selectedPattern = pattern5;
                break;
        }
        long[] repeatedPattern = new long[(selectedPattern.length * times)];
        for (int n = 0; n < times; n++) {
            System.arraycopy(selectedPattern, 0, repeatedPattern, selectedPattern.length * n, selectedPattern.length);
        }
        repeatedPattern[0] = 0;
        return repeatedPattern;
    }
}
