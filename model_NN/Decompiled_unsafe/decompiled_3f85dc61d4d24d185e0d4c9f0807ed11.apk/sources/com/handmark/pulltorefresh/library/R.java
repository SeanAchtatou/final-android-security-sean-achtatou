package com.handmark.pulltorefresh.library;

public final class R {

    public static final class anim {
        public static final int slide_in_from_bottom = 2131034135;
        public static final int slide_in_from_top = 2131034136;
        public static final int slide_out_to_bottom = 2131034141;
        public static final int slide_out_to_top = 2131034142;
    }

    public static final class attr {
        public static final int ptrAdapterViewBackground = 2130772287;
        public static final int ptrAnimationStyle = 2130772283;
        public static final int ptrDrawable = 2130772277;
        public static final int ptrDrawableBottom = 2130772289;
        public static final int ptrDrawableEnd = 2130772279;
        public static final int ptrDrawableStart = 2130772278;
        public static final int ptrDrawableTop = 2130772288;
        public static final int ptrHeaderBackground = 2130772272;
        public static final int ptrHeaderSubTextColor = 2130772274;
        public static final int ptrHeaderTextAppearance = 2130772281;
        public static final int ptrHeaderTextColor = 2130772273;
        public static final int ptrListViewExtrasEnabled = 2130772285;
        public static final int ptrMode = 2130772275;
        public static final int ptrOverScroll = 2130772280;
        public static final int ptrRefreshableViewBackground = 2130772271;
        public static final int ptrRotateDrawableWhilePulling = 2130772286;
        public static final int ptrScrollingWhileRefreshingEnabled = 2130772284;
        public static final int ptrShowIndicator = 2130772276;
        public static final int ptrSubHeaderTextAppearance = 2130772282;
    }

    public static final class dimen {
        public static final int header_footer_left_right_padding = 2131361909;
        public static final int header_footer_top_bottom_padding = 2131361910;
        public static final int indicator_corner_radius = 2131361918;
        public static final int indicator_internal_padding = 2131361919;
        public static final int indicator_right_padding = 2131361920;
    }

    public static final class drawable {
        public static final int default_ptr_flip = 2130837627;
        public static final int default_ptr_rotate = 2130837628;
        public static final int indicator_arrow = 2130837748;
        public static final int indicator_bg_bottom = 2130837749;
        public static final int indicator_bg_top = 2130837750;
    }

    public static final class id {
        public static final int both = 2131755082;
        public static final int disabled = 2131755083;
        public static final int fl_inner = 2131755400;
        public static final int flip = 2131755089;
        public static final int gridview = 2131755017;
        public static final int manualOnly = 2131755084;
        public static final int pullDownFromTop = 2131755085;
        public static final int pullFromEnd = 2131755086;
        public static final int pullFromStart = 2131755087;
        public static final int pullUpFromBottom = 2131755088;
        public static final int pull_to_refresh_image = 2131755401;
        public static final int pull_to_refresh_progress = 2131755402;
        public static final int pull_to_refresh_sub_text = 2131755404;
        public static final int pull_to_refresh_text = 2131755403;
        public static final int rotate = 2131755090;
        public static final int scrollview = 2131755022;
        public static final int webview = 2131755031;
    }

    public static final class layout {
        public static final int pull_to_refresh_header_horizontal = 2130968687;
        public static final int pull_to_refresh_header_vertical = 2130968688;
    }

    public static final class string {
        public static final int pull_to_refresh_from_bottom_pull_label = 2131231620;
        public static final int pull_to_refresh_from_bottom_refreshing_label = 2131231621;
        public static final int pull_to_refresh_from_bottom_release_label = 2131231622;
        public static final int pull_to_refresh_pull_label = 2131230742;
        public static final int pull_to_refresh_refreshing_label = 2131230743;
        public static final int pull_to_refresh_release_label = 2131230744;
    }

    public static final class styleable {
        public static final int[] PullToRefresh = {2130772271, 2130772272, 2130772273, 2130772274, 2130772275, 2130772276, 2130772277, 2130772278, 2130772279, 2130772280, 2130772281, 2130772282, 2130772283, 2130772284, 2130772285, 2130772286, 2130772287, 2130772288, 2130772289};
        public static final int PullToRefresh_ptrAdapterViewBackground = 16;
        public static final int PullToRefresh_ptrAnimationStyle = 12;
        public static final int PullToRefresh_ptrDrawable = 6;
        public static final int PullToRefresh_ptrDrawableBottom = 18;
        public static final int PullToRefresh_ptrDrawableEnd = 8;
        public static final int PullToRefresh_ptrDrawableStart = 7;
        public static final int PullToRefresh_ptrDrawableTop = 17;
        public static final int PullToRefresh_ptrHeaderBackground = 1;
        public static final int PullToRefresh_ptrHeaderSubTextColor = 3;
        public static final int PullToRefresh_ptrHeaderTextAppearance = 10;
        public static final int PullToRefresh_ptrHeaderTextColor = 2;
        public static final int PullToRefresh_ptrListViewExtrasEnabled = 14;
        public static final int PullToRefresh_ptrMode = 4;
        public static final int PullToRefresh_ptrOverScroll = 9;
        public static final int PullToRefresh_ptrRefreshableViewBackground = 0;
        public static final int PullToRefresh_ptrRotateDrawableWhilePulling = 15;
        public static final int PullToRefresh_ptrScrollingWhileRefreshingEnabled = 13;
        public static final int PullToRefresh_ptrShowIndicator = 5;
        public static final int PullToRefresh_ptrSubHeaderTextAppearance = 11;
    }
}
