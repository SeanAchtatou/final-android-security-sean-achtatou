package com.bumptech.glide.util;

import java.util.LinkedHashMap;
import java.util.Map;

public class LruCache<T, Y> {
    private final LinkedHashMap<T, Y> cache = new LinkedHashMap<>(100, 0.75f, true);
    private int currentSize = 0;
    private final int initialMaxSize;
    private int maxSize;

    public LruCache(int size) {
        this.initialMaxSize = size;
        this.maxSize = size;
    }

    public void setSizeMultiplier(float multiplier) {
        if (multiplier < 0.0f) {
            throw new IllegalArgumentException("Multiplier must be >= 0");
        }
        this.maxSize = Math.round(((float) this.initialMaxSize) * multiplier);
        evict();
    }

    /* access modifiers changed from: protected */
    public int getSize(Object obj) {
        return 1;
    }

    /* access modifiers changed from: protected */
    public void onItemEvicted(Object obj, Object obj2) {
    }

    public int getMaxSize() {
        return this.maxSize;
    }

    public int getCurrentSize() {
        return this.currentSize;
    }

    public boolean contains(T key) {
        return this.cache.containsKey(key);
    }

    public Y get(T key) {
        return this.cache.get(key);
    }

    public Y put(Object obj, Object obj2) {
        if (getSize(obj2) >= this.maxSize) {
            onItemEvicted(obj, obj2);
            return null;
        }
        Y result = this.cache.put(obj, obj2);
        if (obj2 != null) {
            this.currentSize += getSize(obj2);
        }
        if (result != null) {
            this.currentSize -= getSize(result);
        }
        evict();
        return result;
    }

    public Y remove(Object obj) {
        Y value = this.cache.remove(obj);
        if (value != null) {
            this.currentSize -= getSize(value);
        }
        return value;
    }

    public void clearMemory() {
        trimToSize(0);
    }

    /* access modifiers changed from: protected */
    public void trimToSize(int size) {
        while (this.currentSize > size) {
            Map.Entry<T, Y> last = this.cache.entrySet().iterator().next();
            Y toRemove = last.getValue();
            this.currentSize -= getSize(toRemove);
            T key = last.getKey();
            this.cache.remove(key);
            onItemEvicted(key, toRemove);
        }
    }

    private void evict() {
        trimToSize(this.maxSize);
    }
}
