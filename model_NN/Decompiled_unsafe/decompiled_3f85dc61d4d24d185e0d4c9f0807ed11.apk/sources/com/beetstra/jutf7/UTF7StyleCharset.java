package com.beetstra.jutf7;

import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.util.Arrays;
import java.util.List;

abstract class UTF7StyleCharset extends Charset {
    private static final List CONTAINED = Arrays.asList("US-ASCII", "ISO-8859-1", "UTF-8", "UTF-16", "UTF-16LE", "UTF-16BE");
    Base64Util base64;
    final boolean strict;

    /* access modifiers changed from: package-private */
    public abstract boolean canEncodeDirectly(char c);

    /* access modifiers changed from: package-private */
    public abstract byte shift();

    /* access modifiers changed from: package-private */
    public abstract byte unshift();

    protected UTF7StyleCharset(String canonicalName, String[] aliases, String alphabet, boolean strict2) {
        super(canonicalName, aliases);
        this.base64 = new Base64Util(alphabet);
        this.strict = strict2;
    }

    public boolean contains(Charset cs) {
        return CONTAINED.contains(cs.name());
    }

    public CharsetDecoder newDecoder() {
        return new UTF7StyleCharsetDecoder(this, this.base64, this.strict);
    }

    public CharsetEncoder newEncoder() {
        return new UTF7StyleCharsetEncoder(this, this.base64, this.strict);
    }
}
