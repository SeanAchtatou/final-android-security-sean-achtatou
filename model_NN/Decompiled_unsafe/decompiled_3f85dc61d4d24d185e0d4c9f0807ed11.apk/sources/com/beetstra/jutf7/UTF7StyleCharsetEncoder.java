package com.beetstra.jutf7;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.CoderResult;

class UTF7StyleCharsetEncoder extends CharsetEncoder {
    private static final float AVG_BYTES_PER_CHAR = 1.5f;
    private static final float MAX_BYTES_PER_CHAR = 5.0f;
    static boolean useUglyHackToForceCallToFlushInJava5;
    private final Base64Util base64;
    private boolean base64mode;
    private int bitsToOutput;
    private final UTF7StyleCharset cs;
    private int sextet;
    private final byte shift;
    private final boolean strict;
    private final byte unshift;

    static {
        String version = System.getProperty("java.specification.version");
        String vendor = System.getProperty("java.vm.vendor");
        useUglyHackToForceCallToFlushInJava5 = "1.4".equals(version) || "1.5".equals(version);
        useUglyHackToForceCallToFlushInJava5 &= "Sun Microsystems Inc.".equals(vendor);
    }

    UTF7StyleCharsetEncoder(UTF7StyleCharset cs2, Base64Util base642, boolean strict2) {
        super(cs2, AVG_BYTES_PER_CHAR, MAX_BYTES_PER_CHAR);
        this.cs = cs2;
        this.base64 = base642;
        this.strict = strict2;
        this.shift = cs2.shift();
        this.unshift = cs2.unshift();
    }

    /* access modifiers changed from: protected */
    public void implReset() {
        this.base64mode = false;
        this.sextet = 0;
        this.bitsToOutput = 0;
    }

    /* access modifiers changed from: protected */
    public CoderResult implFlush(ByteBuffer out) {
        if (this.base64mode) {
            if (out.remaining() < 2) {
                return CoderResult.OVERFLOW;
            }
            if (this.bitsToOutput != 0) {
                out.put(this.base64.getChar(this.sextet));
            }
            out.put(this.unshift);
        }
        return CoderResult.UNDERFLOW;
    }

    /* access modifiers changed from: protected */
    public CoderResult encodeLoop(CharBuffer in, ByteBuffer out) {
        while (in.hasRemaining()) {
            if (out.remaining() < 4) {
                return CoderResult.OVERFLOW;
            }
            char ch = in.get();
            if (this.cs.canEncodeDirectly(ch)) {
                unshift(out, ch);
                out.put((byte) ch);
            } else if (this.base64mode || ch != this.shift) {
                encodeBase64(ch, out);
            } else {
                out.put(this.shift);
                out.put(this.unshift);
            }
        }
        if (!this.base64mode || !useUglyHackToForceCallToFlushInJava5 || ((float) out.limit()) == MAX_BYTES_PER_CHAR * ((float) in.limit())) {
            return CoderResult.UNDERFLOW;
        }
        return CoderResult.OVERFLOW;
    }

    private void unshift(ByteBuffer out, char ch) {
        if (this.base64mode) {
            if (this.bitsToOutput != 0) {
                out.put(this.base64.getChar(this.sextet));
            }
            if (this.base64.contains(ch) || ch == this.unshift || this.strict) {
                out.put(this.unshift);
            }
            this.base64mode = false;
            this.sextet = 0;
            this.bitsToOutput = 0;
        }
    }

    private void encodeBase64(char ch, ByteBuffer out) {
        if (!this.base64mode) {
            out.put(this.shift);
        }
        this.base64mode = true;
        this.bitsToOutput += 16;
        while (this.bitsToOutput >= 6) {
            this.bitsToOutput -= 6;
            this.sextet += ch >> this.bitsToOutput;
            this.sextet &= 63;
            out.put(this.base64.getChar(this.sextet));
            this.sextet = 0;
        }
        this.sextet = (ch << (6 - this.bitsToOutput)) & 63;
    }
}
