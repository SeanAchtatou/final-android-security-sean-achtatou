package com.jcraft.jzlib;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class ZInputStream extends FilterInputStream {
    protected byte[] buf;
    protected byte[] buf1;
    protected int bufsize;
    protected boolean compress;
    protected int flush;
    protected InputStream in;
    private boolean nomoreinput;
    protected ZStream z;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.jcraft.jzlib.ZInputStream.<init>(java.io.InputStream, boolean):void
     arg types: [java.io.InputStream, int]
     candidates:
      com.jcraft.jzlib.ZInputStream.<init>(java.io.InputStream, int):void
      com.jcraft.jzlib.ZInputStream.<init>(java.io.InputStream, boolean):void */
    public ZInputStream(InputStream in2) {
        this(in2, false);
    }

    public ZInputStream(InputStream in2, boolean nowrap) {
        super(in2);
        this.z = new ZStream();
        this.bufsize = 512;
        this.flush = 0;
        this.buf = new byte[this.bufsize];
        this.buf1 = new byte[1];
        this.in = null;
        this.nomoreinput = false;
        this.in = in2;
        this.z.inflateInit(nowrap);
        this.compress = false;
        this.z.next_in = this.buf;
        this.z.next_in_index = 0;
        this.z.avail_in = 0;
    }

    public ZInputStream(InputStream in2, int level) {
        super(in2);
        this.z = new ZStream();
        this.bufsize = 512;
        this.flush = 0;
        this.buf = new byte[this.bufsize];
        this.buf1 = new byte[1];
        this.in = null;
        this.nomoreinput = false;
        this.in = in2;
        this.z.deflateInit(level);
        this.compress = true;
        this.z.next_in = this.buf;
        this.z.next_in_index = 0;
        this.z.avail_in = 0;
    }

    public int read() throws IOException {
        if (read(this.buf1, 0, 1) == -1) {
            return -1;
        }
        return this.buf1[0] & 255;
    }

    public int read(byte[] b, int off, int len) throws IOException {
        int err;
        if (len == 0) {
            return 0;
        }
        this.z.next_out = b;
        this.z.next_out_index = off;
        this.z.avail_out = len;
        do {
            if (this.z.avail_in == 0 && !this.nomoreinput) {
                this.z.next_in_index = 0;
                this.z.avail_in = this.in.read(this.buf, 0, this.bufsize);
                if (this.z.avail_in == -1) {
                    this.z.avail_in = 0;
                    this.nomoreinput = true;
                }
            }
            if (this.compress) {
                err = this.z.deflate(this.flush);
            } else {
                err = this.z.inflate(this.flush);
            }
            if (this.nomoreinput && err == -5) {
                return -1;
            }
            if (err == 0 || err == 1) {
                if ((!this.nomoreinput && err != 1) || this.z.avail_out != len) {
                    if (this.z.avail_out != len) {
                        break;
                    }
                } else {
                    return -1;
                }
            } else {
                throw new ZStreamException(new StringBuffer().append(this.compress ? "de" : "in").append("flating: ").append(this.z.msg).toString());
            }
        } while (err == 0);
        return len - this.z.avail_out;
    }

    public long skip(long n) throws IOException {
        int len = 512;
        if (n < ((long) 512)) {
            len = (int) n;
        }
        return (long) read(new byte[len]);
    }

    public int getFlushMode() {
        return this.flush;
    }

    public void setFlushMode(int flush2) {
        this.flush = flush2;
    }

    public long getTotalIn() {
        return this.z.total_in;
    }

    public long getTotalOut() {
        return this.z.total_out;
    }

    public void close() throws IOException {
        this.in.close();
    }
}
