package com.larswerkman.colorpicker;

public final class R {

    public static final class attr {
        public static final int pointer_size = 2130772154;
        public static final int wheel_size = 2130772153;
    }

    public static final class styleable {
        public static final int[] ColorPicker = {2130772153, 2130772154};
        public static final int ColorPicker_pointer_size = 1;
        public static final int ColorPicker_wheel_size = 0;
    }
}
