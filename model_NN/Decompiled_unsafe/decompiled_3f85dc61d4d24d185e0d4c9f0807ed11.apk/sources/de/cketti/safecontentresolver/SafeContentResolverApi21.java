package de.cketti.safecontentresolver;

import android.content.Context;
import android.support.annotation.NonNull;
import android.system.ErrnoException;
import android.system.Os;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;

final class SafeContentResolverApi21 extends SafeContentResolver {
    SafeContentResolverApi21(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public int getFileUidOrThrow(@NonNull FileDescriptor fileDescriptor) throws FileNotFoundException {
        try {
            return Os.fstat(fileDescriptor).st_uid;
        } catch (ErrnoException e) {
            throw new FileNotFoundException(e.getMessage());
        }
    }
}
