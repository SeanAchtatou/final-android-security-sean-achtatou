package de.cketti.safecontentresolver;

public final class BuildConfig {
    public static final String APPLICATION_ID = "de.cketti.safecontentresolver";
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final String FLAVOR = "";
    public static final int VERSION_CODE = -1;
    public static final String VERSION_NAME = "";
}
