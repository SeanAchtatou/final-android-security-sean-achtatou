package de.cketti.safecontentresolver;

class ErrnoException extends Exception {
    public final int errno;
    private final String functionName;

    public ErrnoException(String functionName2, int errno2) {
        this.functionName = functionName2;
        this.errno = errno2;
    }

    public String getMessage() {
        return this.functionName + " failed: " + this.errno;
    }
}
