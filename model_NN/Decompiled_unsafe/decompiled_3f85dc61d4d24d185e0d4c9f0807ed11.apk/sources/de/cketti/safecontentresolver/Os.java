package de.cketti.safecontentresolver;

import android.content.Context;
import com.getkeepsafe.relinker.MissingLibraryException;
import com.getkeepsafe.relinker.ReLinker;

class Os {
    private static final String LIBRARY_NAME = "os-compat";
    private static Context context;
    private static boolean libraryNeedsLoading = true;
    private static UnsupportedOperationException loadFailedException;

    private static native int nativeFstat(int i) throws ErrnoException;

    Os() {
    }

    static synchronized void init(Context context2) {
        synchronized (Os.class) {
            if (context2 == null) {
                throw new NullPointerException("Argument 'context' must not be null");
            } else if (context == null) {
                context = context2.getApplicationContext();
            }
        }
    }

    static int fstat(int fileDescriptor) throws ErrnoException, UnsupportedOperationException {
        synchronized (Os.class) {
            if (context == null) {
                throw new IllegalStateException("Call Os.init(Context) before attempting to call Os.fstat()");
            } else if (libraryNeedsLoading) {
                loadLibrary();
            } else if (loadFailedException != null) {
                throw loadFailedException;
            }
        }
        return nativeFstat(fileDescriptor);
    }

    private static void loadLibrary() {
        libraryNeedsLoading = false;
        try {
            ReLinker.loadLibrary(context, LIBRARY_NAME);
        } catch (MissingLibraryException | UnsatisfiedLinkError e) {
            loadFailedException = new UnsupportedOperationException("Failed to load native library os-compat", e);
            throw loadFailedException;
        }
    }
}
