package android.support.design.widget;

import android.content.Context;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ScrollerCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;

abstract class HeaderBehavior<V extends View> extends ViewOffsetBehavior<V> {
    private static final int INVALID_POINTER = -1;
    private int mActivePointerId = -1;
    private Runnable mFlingRunnable;
    private boolean mIsBeingDragged;
    private int mLastMotionY;
    ScrollerCompat mScroller;
    private int mTouchSlop = -1;
    private VelocityTracker mVelocityTracker;

    public HeaderBehavior() {
    }

    public HeaderBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public boolean onInterceptTouchEvent(CoordinatorLayout parent, V child, MotionEvent ev) {
        int pointerIndex;
        if (this.mTouchSlop < 0) {
            this.mTouchSlop = ViewConfiguration.get(parent.getContext()).getScaledTouchSlop();
        }
        if (ev.getAction() == 2 && this.mIsBeingDragged) {
            return true;
        }
        switch (MotionEventCompat.getActionMasked(ev)) {
            case 0:
                this.mIsBeingDragged = false;
                int x = (int) ev.getX();
                int y = (int) ev.getY();
                if (canDragView(child) && parent.isPointInChildBounds(child, x, y)) {
                    this.mLastMotionY = y;
                    this.mActivePointerId = ev.getPointerId(0);
                    ensureVelocityTracker();
                    break;
                }
            case 1:
            case 3:
                this.mIsBeingDragged = false;
                this.mActivePointerId = -1;
                if (this.mVelocityTracker != null) {
                    this.mVelocityTracker.recycle();
                    this.mVelocityTracker = null;
                    break;
                }
                break;
            case 2:
                int activePointerId = this.mActivePointerId;
                if (!(activePointerId == -1 || (pointerIndex = ev.findPointerIndex(activePointerId)) == -1)) {
                    int y2 = (int) ev.getY(pointerIndex);
                    if (Math.abs(y2 - this.mLastMotionY) > this.mTouchSlop) {
                        this.mIsBeingDragged = true;
                        this.mLastMotionY = y2;
                        break;
                    }
                }
                break;
        }
        if (this.mVelocityTracker != null) {
            this.mVelocityTracker.addMovement(ev);
        }
        return this.mIsBeingDragged;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00c9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.support.design.widget.CoordinatorLayout r15, V r16, android.view.MotionEvent r17) {
        /*
            r14 = this;
            int r1 = r14.mTouchSlop
            if (r1 >= 0) goto L_0x0012
            android.content.Context r1 = r15.getContext()
            android.view.ViewConfiguration r1 = android.view.ViewConfiguration.get(r1)
            int r1 = r1.getScaledTouchSlop()
            r14.mTouchSlop = r1
        L_0x0012:
            int r1 = android.support.v4.view.MotionEventCompat.getActionMasked(r17)
            switch(r1) {
                case 0: goto L_0x0026;
                case 1: goto L_0x0096;
                case 2: goto L_0x0051;
                case 3: goto L_0x00bf;
                default: goto L_0x0019;
            }
        L_0x0019:
            android.view.VelocityTracker r1 = r14.mVelocityTracker
            if (r1 == 0) goto L_0x0024
            android.view.VelocityTracker r1 = r14.mVelocityTracker
            r0 = r17
            r1.addMovement(r0)
        L_0x0024:
            r1 = 1
        L_0x0025:
            return r1
        L_0x0026:
            float r1 = r17.getX()
            int r12 = (int) r1
            float r1 = r17.getY()
            int r13 = (int) r1
            r0 = r16
            boolean r1 = r15.isPointInChildBounds(r0, r12, r13)
            if (r1 == 0) goto L_0x004f
            r0 = r16
            boolean r1 = r14.canDragView(r0)
            if (r1 == 0) goto L_0x004f
            r14.mLastMotionY = r13
            r1 = 0
            r0 = r17
            int r1 = r0.getPointerId(r1)
            r14.mActivePointerId = r1
            r14.ensureVelocityTracker()
            goto L_0x0019
        L_0x004f:
            r1 = 0
            goto L_0x0025
        L_0x0051:
            int r1 = r14.mActivePointerId
            r0 = r17
            int r11 = r0.findPointerIndex(r1)
            r1 = -1
            if (r11 != r1) goto L_0x005e
            r1 = 0
            goto L_0x0025
        L_0x005e:
            r0 = r17
            float r1 = r0.getY(r11)
            int r13 = (int) r1
            int r1 = r14.mLastMotionY
            int r4 = r1 - r13
            boolean r1 = r14.mIsBeingDragged
            if (r1 != 0) goto L_0x007d
            int r1 = java.lang.Math.abs(r4)
            int r2 = r14.mTouchSlop
            if (r1 <= r2) goto L_0x007d
            r1 = 1
            r14.mIsBeingDragged = r1
            if (r4 <= 0) goto L_0x0092
            int r1 = r14.mTouchSlop
            int r4 = r4 - r1
        L_0x007d:
            boolean r1 = r14.mIsBeingDragged
            if (r1 == 0) goto L_0x0019
            r14.mLastMotionY = r13
            r0 = r16
            int r5 = r14.getMaxDragOffset(r0)
            r6 = 0
            r1 = r14
            r2 = r15
            r3 = r16
            r1.scroll(r2, r3, r4, r5, r6)
            goto L_0x0019
        L_0x0092:
            int r1 = r14.mTouchSlop
            int r4 = r4 + r1
            goto L_0x007d
        L_0x0096:
            android.view.VelocityTracker r1 = r14.mVelocityTracker
            if (r1 == 0) goto L_0x00bf
            android.view.VelocityTracker r1 = r14.mVelocityTracker
            r0 = r17
            r1.addMovement(r0)
            android.view.VelocityTracker r1 = r14.mVelocityTracker
            r2 = 1000(0x3e8, float:1.401E-42)
            r1.computeCurrentVelocity(r2)
            android.view.VelocityTracker r1 = r14.mVelocityTracker
            int r2 = r14.mActivePointerId
            float r10 = android.support.v4.view.VelocityTrackerCompat.getYVelocity(r1, r2)
            r0 = r16
            int r1 = r14.getScrollRangeForDragFling(r0)
            int r8 = -r1
            r9 = 0
            r5 = r14
            r6 = r15
            r7 = r16
            r5.fling(r6, r7, r8, r9, r10)
        L_0x00bf:
            r1 = 0
            r14.mIsBeingDragged = r1
            r1 = -1
            r14.mActivePointerId = r1
            android.view.VelocityTracker r1 = r14.mVelocityTracker
            if (r1 == 0) goto L_0x0019
            android.view.VelocityTracker r1 = r14.mVelocityTracker
            r1.recycle()
            r1 = 0
            r14.mVelocityTracker = r1
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.design.widget.HeaderBehavior.onTouchEvent(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.MotionEvent):boolean");
    }

    /* access modifiers changed from: package-private */
    public int setHeaderTopBottomOffset(CoordinatorLayout parent, V header, int newOffset) {
        return setHeaderTopBottomOffset(parent, header, newOffset, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    /* access modifiers changed from: package-private */
    public int setHeaderTopBottomOffset(CoordinatorLayout parent, View view, int newOffset, int minOffset, int maxOffset) {
        int newOffset2;
        int curOffset = getTopAndBottomOffset();
        if (minOffset == 0 || curOffset < minOffset || curOffset > maxOffset || curOffset == (newOffset2 = MathUtils.constrain(newOffset, minOffset, maxOffset))) {
            return 0;
        }
        setTopAndBottomOffset(newOffset2);
        return curOffset - newOffset2;
    }

    /* access modifiers changed from: package-private */
    public int getTopBottomOffsetForScrollingSibling() {
        return getTopAndBottomOffset();
    }

    /* access modifiers changed from: package-private */
    public final int scroll(CoordinatorLayout coordinatorLayout, V header, int dy, int minOffset, int maxOffset) {
        return setHeaderTopBottomOffset(coordinatorLayout, header, getTopBottomOffsetForScrollingSibling() - dy, minOffset, maxOffset);
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: V
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    final boolean fling(android.support.design.widget.CoordinatorLayout r10, V r11, int r12, int r13, float r14) {
        /*
            r9 = this;
            r1 = 0
            java.lang.Runnable r0 = r9.mFlingRunnable
            if (r0 == 0) goto L_0x000d
            java.lang.Runnable r0 = r9.mFlingRunnable
            r11.removeCallbacks(r0)
            r0 = 0
            r9.mFlingRunnable = r0
        L_0x000d:
            android.support.v4.widget.ScrollerCompat r0 = r9.mScroller
            if (r0 != 0) goto L_0x001b
            android.content.Context r0 = r11.getContext()
            android.support.v4.widget.ScrollerCompat r0 = android.support.v4.widget.ScrollerCompat.create(r0)
            r9.mScroller = r0
        L_0x001b:
            android.support.v4.widget.ScrollerCompat r0 = r9.mScroller
            int r2 = r9.getTopAndBottomOffset()
            int r4 = java.lang.Math.round(r14)
            r3 = r1
            r5 = r1
            r6 = r1
            r7 = r12
            r8 = r13
            r0.fling(r1, r2, r3, r4, r5, r6, r7, r8)
            android.support.v4.widget.ScrollerCompat r0 = r9.mScroller
            boolean r0 = r0.computeScrollOffset()
            if (r0 == 0) goto L_0x0043
            android.support.design.widget.HeaderBehavior$FlingRunnable r0 = new android.support.design.widget.HeaderBehavior$FlingRunnable
            r0.<init>(r10, r11)
            r9.mFlingRunnable = r0
            java.lang.Runnable r0 = r9.mFlingRunnable
            android.support.v4.view.ViewCompat.postOnAnimation(r11, r0)
            r1 = 1
        L_0x0042:
            return r1
        L_0x0043:
            r9.onFlingFinished(r10, r11)
            goto L_0x0042
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.design.widget.HeaderBehavior.fling(android.support.design.widget.CoordinatorLayout, android.view.View, int, int, float):boolean");
    }

    /* access modifiers changed from: package-private */
    public void onFlingFinished(CoordinatorLayout parent, View view) {
    }

    /* access modifiers changed from: package-private */
    public boolean canDragView(View view) {
        return false;
    }

    /* access modifiers changed from: package-private */
    public int getMaxDragOffset(View view) {
        return -view.getHeight();
    }

    /* access modifiers changed from: package-private */
    public int getScrollRangeForDragFling(View view) {
        return view.getHeight();
    }

    private void ensureVelocityTracker() {
        if (this.mVelocityTracker == null) {
            this.mVelocityTracker = VelocityTracker.obtain();
        }
    }

    private class FlingRunnable implements Runnable {
        private final V mLayout;
        private final CoordinatorLayout mParent;

        FlingRunnable(CoordinatorLayout parent, V layout) {
            this.mParent = parent;
            this.mLayout = layout;
        }

        public void run() {
            if (this.mLayout != null && HeaderBehavior.this.mScroller != null) {
                if (HeaderBehavior.this.mScroller.computeScrollOffset()) {
                    HeaderBehavior.this.setHeaderTopBottomOffset(this.mParent, this.mLayout, HeaderBehavior.this.mScroller.getCurrY());
                    ViewCompat.postOnAnimation(this.mLayout, this);
                    return;
                }
                HeaderBehavior.this.onFlingFinished(this.mParent, this.mLayout);
            }
        }
    }
}
