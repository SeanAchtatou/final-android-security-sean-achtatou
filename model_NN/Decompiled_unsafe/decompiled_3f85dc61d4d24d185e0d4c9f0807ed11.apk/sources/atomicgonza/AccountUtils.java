package atomicgonza;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.XmlResourceParser;
import android.util.Log;
import com.fsck.k9.Account;
import com.fsck.k9.AccountStats;
import com.fsck.k9.K9;
import com.fsck.k9.Preferences;
import com.fsck.k9.account.AccountCreator;
import com.fsck.k9.activity.setup.AccountSetupBasics;
import com.fsck.k9.controller.MessagingController;
import com.fsck.k9.controller.UidComparator;
import com.fsck.k9.controller.UidReverseComparator;
import com.fsck.k9.helper.UrlEncodingHelper;
import com.fsck.k9.mail.AuthType;
import com.fsck.k9.mail.ServerSettings;
import com.fsck.k9.mail.store.RemoteStore;
import com.fsck.k9.preferences.SettingsExporter;
import com.fsck.k9.provider.MessageProvider;
import com.fsck.k9.search.SearchAccount;
import exts.whats.R;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Comparator;
import java.util.Locale;
import me.leolin.shortcutbadger.ShortcutBadger;

public class AccountUtils {
    public static final int CODE_REQUEST_WIDGET = 25;
    public static final int RESULT_CLEARED_ACCOUNT = -11;
    static int unreadCount = 0;

    public static String[] splitEmail(String email) {
        String[] retParts = new String[2];
        String[] emailParts = email.split("@");
        retParts[0] = emailParts.length > 0 ? emailParts[0] : "";
        retParts[1] = emailParts.length > 1 ? emailParts[1] : "";
        return retParts;
    }

    public static Account initAccount(String emailAddress, Activity act) {
        URI outgoingUri;
        String email = emailAddress;
        String[] emailParts = splitEmail(email);
        String user = emailParts[0];
        String domain = emailParts[1];
        AccountSetupBasics.Provider mProvider = findProviderForDomain(domain, act);
        try {
            String userEnc = UrlEncodingHelper.encodeUtf8(user);
            String passwordEnc = UrlEncodingHelper.encodeUtf8("");
            String incomingUsername = mProvider.incomingUsernameTemplate.replaceAll("\\$email", email).replaceAll("\\$user", userEnc).replaceAll("\\$domain", domain);
            URI incomingUriTemplate = mProvider.incomingUriTemplate;
            URI incomingUri = new URI(incomingUriTemplate.getScheme(), AuthType.XOAUTH2 + ":" + (incomingUsername + ":" + passwordEnc), incomingUriTemplate.getHost(), incomingUriTemplate.getPort(), null, null, null);
            String outgoingUsername = mProvider.outgoingUsernameTemplate;
            URI outgoingUriTemplate = mProvider.outgoingUriTemplate;
            if (outgoingUsername != null) {
                outgoingUri = new URI(outgoingUriTemplate.getScheme(), (outgoingUsername.replaceAll("\\$email", email).replaceAll("\\$user", userEnc).replaceAll("\\$domain", domain) + ":" + passwordEnc) + ":" + AuthType.XOAUTH2, outgoingUriTemplate.getHost(), outgoingUriTemplate.getPort(), null, null, null);
            } else {
                outgoingUri = new URI(outgoingUriTemplate.getScheme(), null, outgoingUriTemplate.getHost(), outgoingUriTemplate.getPort(), null, null, null);
            }
            Account mAccount = Preferences.getPreferences(act).newAccount();
            mAccount.setName(getOwnerName(act));
            mAccount.setEmail(email);
            mAccount.setStoreUri(incomingUri.toString());
            mAccount.setTransportUri(outgoingUri.toString());
            setupFolderNames(incomingUriTemplate.getHost().toLowerCase(Locale.US), mAccount, act);
            ServerSettings decodeStoreUri = RemoteStore.decodeStoreUri(mAccount.getStoreUri());
            mAccount.setDeletePolicy(AccountCreator.getDefaultDeletePolicy(RemoteStore.decodeStoreUri(incomingUri.toString()).type));
            return mAccount;
        } catch (URISyntaxException e) {
            if (K9.DEBUG) {
                Log.e("AtomicGonza", "okAG Exception  " + new Exception().getStackTrace()[0].toString());
            }
            return null;
        }
    }

    private static AccountSetupBasics.Provider findProviderForDomain(String domain, Context ctx) {
        try {
            XmlResourceParser xml = ctx.getResources().getXml(R.id.showCustom);
            AccountSetupBasics.Provider provider = null;
            while (true) {
                try {
                    int xmlEventType = xml.next();
                    if (xmlEventType == 1) {
                        break;
                    } else if (xmlEventType != 2 || !"provider".equals(xml.getName()) || !domain.equalsIgnoreCase(getXmlAttribute(xml, "domain", ctx))) {
                        if (xmlEventType == 2) {
                            if ("incoming".equals(xml.getName()) && provider != null) {
                                provider.incomingUriTemplate = new URI(getXmlAttribute(xml, MessageProvider.MessageColumns.URI, ctx));
                                provider.incomingUsernameTemplate = getXmlAttribute(xml, SettingsExporter.USERNAME_ELEMENT, ctx);
                            }
                        }
                        if (xmlEventType == 2) {
                            if ("outgoing".equals(xml.getName()) && provider != null) {
                                provider.outgoingUriTemplate = new URI(getXmlAttribute(xml, MessageProvider.MessageColumns.URI, ctx));
                                provider.outgoingUsernameTemplate = getXmlAttribute(xml, SettingsExporter.USERNAME_ELEMENT, ctx);
                            }
                        }
                        if (xmlEventType == 3 && "provider".equals(xml.getName()) && provider != null) {
                            return provider;
                        }
                    } else {
                        AccountSetupBasics.Provider provider2 = new AccountSetupBasics.Provider();
                        provider2.id = getXmlAttribute(xml, "id", ctx);
                        provider2.label = getXmlAttribute(xml, "label", ctx);
                        provider2.domain = getXmlAttribute(xml, "domain", ctx);
                        provider2.note = getXmlAttribute(xml, "note", ctx);
                        provider = provider2;
                    }
                } catch (Exception e) {
                    e = e;
                    Log.e("k9", "Error while trying to load provider settings.", e);
                    return null;
                }
            }
        } catch (Exception e2) {
            e = e2;
            Log.e("k9", "Error while trying to load provider settings.", e);
            return null;
        }
        return null;
    }

    private static String getXmlAttribute(XmlResourceParser xml, String name, Context ctx) {
        int resId = xml.getAttributeResourceValue(null, name, 0);
        if (resId == 0) {
            return xml.getAttributeValue(null, name);
        }
        return ctx.getString(resId);
    }

    private static String getDefaultAccountName(Activity act) {
        Account account = Preferences.getPreferences(act).getDefaultAccount();
        if (account != null) {
            return account.getName();
        }
        return null;
    }

    private static String getOwnerName(Activity act) {
        String name = null;
        try {
            name = getDefaultAccountName(act);
        } catch (Exception e) {
            Log.e("k9", "Could not get default account name", e);
        }
        if (name == null) {
            return "";
        }
        return name;
    }

    private static void setupFolderNames(String domain, Account mAccount, Activity act) {
        mAccount.setDraftsFolderName(act.getString(com.fsck.k9.R.string.special_mailbox_name_drafts));
        mAccount.setTrashFolderName(act.getString(com.fsck.k9.R.string.special_mailbox_name_trash));
        mAccount.setSentFolderName(act.getString(com.fsck.k9.R.string.special_mailbox_name_sent));
        mAccount.setArchiveFolderName(act.getString(com.fsck.k9.R.string.special_mailbox_name_archive));
        if (domain.endsWith(".yahoo.com")) {
            mAccount.setSpamFolderName("Bulk Mail");
        } else {
            mAccount.setSpamFolderName(act.getString(com.fsck.k9.R.string.special_mailbox_name_spam));
        }
    }

    public static void updateUnreadWidget(Context context) {
        AccountStats stats = null;
        try {
            SearchAccount searchAccount = SearchAccount.createUnifiedInboxAccount(context);
            if (searchAccount != null) {
                stats = MessagingController.getInstance(context).getSearchAccountStatsSynchronous(searchAccount, null);
            }
            if (stats != null) {
                int unReadsUpdated = stats.unreadMessageCount;
                if (unReadsUpdated != unreadCount) {
                    unreadCount = unReadsUpdated;
                    ShortcutBadger.applyCount(context, unreadCount);
                    return;
                }
                return;
            }
            unreadCount = 0;
            ShortcutBadger.applyCount(context, unreadCount);
        } catch (Exception e) {
            Log.e("AtomicGonza", "okAG Exception " + e.getMessage() + " " + new Exception().getStackTrace()[0].toString());
        }
    }

    public static void removeAccountTask(Account mAccount, Context ctx) {
        try {
            mAccount.getLocalStore().delete();
        } catch (Exception e) {
        }
        MessagingController.getInstance(ctx).deleteAccount(mAccount);
        Preferences.getPreferences(ctx).deleteAccount(mAccount);
        K9.setServicesEnabled(ctx);
    }

    public static void guideUserToWidget(Activity act) {
        SharedPreferences pref = App.getPref(act);
        if (!pref.getBoolean(App.KEY_ASK_WIDGET_BEFORE, false)) {
            new AlertDialog.Builder(act).setTitle("Setup Widget").setMessage("If you want to see unread messages on your home screen just choose our widget on the widget section on your device and place it on your home screen.").setPositiveButton("Ok", (DialogInterface.OnClickListener) null).show();
            pref.edit().putBoolean(App.KEY_ASK_WIDGET_BEFORE, true).commit();
        }
    }

    public static Comparator getSearchComparator(String email) {
        if (GmailUtils.isGmailAccount(email)) {
            return new UidReverseComparator();
        }
        if (HotmailUtils.isHotmailccount(email)) {
            return new UidComparator();
        }
        return new UidReverseComparator();
    }

    public static Comparator getDownloadComparator(String email) {
        if (GmailUtils.isGmailAccount(email)) {
            return new UidReverseComparator();
        }
        if (HotmailUtils.isHotmailccount(email)) {
            return new UidReverseComparator();
        }
        return new UidReverseComparator();
    }
}
