package atomicgonza;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;
import com.fsck.k9.R;

public abstract class Dialogs {
    public static void displayguideDialog(Activity act, String link, boolean enableScript) {
        AlertDialog.Builder builder = new AlertDialog.Builder(act);
        WebView webView = new WebView(act);
        WebSettings websettings = webView.getSettings();
        websettings.setSupportZoom(true);
        websettings.setLightTouchEnabled(true);
        websettings.setBuiltInZoomControls(true);
        websettings.setLoadWithOverviewMode(true);
        if (enableScript) {
            websettings.setJavaScriptEnabled(true);
        }
        if (link != null) {
            webView.loadUrl(link);
            builder.setView(webView).create().show();
        }
    }

    public static void displayLicense(LayoutInflater infl, Context ctx) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        ViewGroup vg = (ViewGroup) infl.inflate((int) R.layout.activity_license, (ViewGroup) null);
        loadLic(ctx, (TextView) vg.findViewById(R.id.textviewLic));
        builder.setView(vg);
        builder.setNegativeButton((int) R.string.close, (DialogInterface.OnClickListener) null).create().setOnShowListener(new DialogInterface.OnShowListener() {
            public void onShow(DialogInterface dialog) {
            }
        });
        builder.show();
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x003f A[SYNTHETIC, Splitter:B:13:0x003f] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0044 A[Catch:{ Exception -> 0x008d }] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0049 A[Catch:{ Exception -> 0x008d }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void loadLic(android.content.Context r13, android.widget.TextView r14) {
        /*
            r12 = 0
            r6 = 0
            r4 = 0
            r0 = 0
            android.content.res.Resources r9 = r13.getResources()     // Catch:{ Exception -> 0x00bf }
            r10 = 2131165184(0x7f070000, float:1.7944578E38)
            java.io.InputStream r6 = r9.openRawResource(r10)     // Catch:{ Exception -> 0x00bf }
            java.io.InputStreamReader r5 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x00bf }
            r5.<init>(r6)     // Catch:{ Exception -> 0x00bf }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ Exception -> 0x00c2 }
            r1.<init>(r5)     // Catch:{ Exception -> 0x00c2 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x003a }
            r8.<init>()     // Catch:{ Exception -> 0x003a }
        L_0x001d:
            java.lang.String r7 = r1.readLine()     // Catch:{ Exception -> 0x003a }
            if (r7 == 0) goto L_0x0087
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x003a }
            r9.<init>()     // Catch:{ Exception -> 0x003a }
            java.lang.StringBuilder r9 = r9.append(r7)     // Catch:{ Exception -> 0x003a }
            java.lang.String r10 = "\n"
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x003a }
            java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x003a }
            r8.append(r9)     // Catch:{ Exception -> 0x003a }
            goto L_0x001d
        L_0x003a:
            r2 = move-exception
            r0 = r1
            r4 = r5
        L_0x003d:
            if (r6 == 0) goto L_0x0042
            r6.close()     // Catch:{ Exception -> 0x008d }
        L_0x0042:
            if (r4 == 0) goto L_0x0047
            r4.close()     // Catch:{ Exception -> 0x008d }
        L_0x0047:
            if (r0 == 0) goto L_0x004c
            r0.close()     // Catch:{ Exception -> 0x008d }
        L_0x004c:
            java.lang.String r9 = "AtomicGonza"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = "okAG "
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.String r11 = r2.getMessage()
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.String r11 = " "
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.StackTraceElement[] r11 = r2.getStackTrace()
            r11 = r11[r12]
            java.lang.String r11 = r11.toString()
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.String r10 = r10.toString()
            android.util.Log.e(r9, r10)
            r9 = 2131231728(0x7f0803f0, float:1.8079545E38)
            java.lang.String r9 = r13.getString(r9)
            r14.append(r9)
        L_0x0086:
            return
        L_0x0087:
            r14.setText(r8)     // Catch:{ Exception -> 0x003a }
            r0 = r1
            r4 = r5
            goto L_0x0086
        L_0x008d:
            r3 = move-exception
            java.lang.String r9 = "AtomicGonza"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = "okAG "
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.String r11 = r3.getMessage()
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.String r11 = " "
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.StackTraceElement[] r11 = r3.getStackTrace()
            r11 = r11[r12]
            java.lang.String r11 = r11.toString()
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.String r10 = r10.toString()
            android.util.Log.e(r9, r10)
            goto L_0x004c
        L_0x00bf:
            r2 = move-exception
            goto L_0x003d
        L_0x00c2:
            r2 = move-exception
            r4 = r5
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: atomicgonza.Dialogs.loadLic(android.content.Context, android.widget.TextView):void");
    }
}
