package atomicgonza;

import android.text.TextUtils;

public class Signature {
    public static String formatSignature(String signature) {
        if (TextUtils.isEmpty(signature) || signature.trim().startsWith("\r\n")) {
            return signature;
        }
        return "\r\n" + signature;
    }
}
