package atomicgonza;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.view.ViewGroup;
import com.fsck.k9.R;
import com.fsck.k9.activity.MessageList;
import com.fsck.k9.activity.Search;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

public class Ads {
    public static final String key_first_run = "key_day_purchase_base";
    public static final String key_first_run_after_purchase = "key_first_run_after_purchase";
    public static final String key_run_count = "key_count_until_purchase";
    public static final String key_run_count_after_purchase = "key_run_count_after_purchase";
    public static final String key_user_rejected = "key_user_reject_purchase";

    public static void checkToInitAds(MessageList act) {
        if (!(act instanceof Search)) {
            SharedPreferences prefs = App.getPref(act);
            if (!App.isAppPurchased(act, prefs)) {
                boolean adsStarted = false;
                if (App.isExpired(prefs, 0, 0)) {
                    AdView adView = (AdView) act.findViewById(R.id.adView);
                    Views.show((ViewGroup) adView);
                    adView.setAdListener(new AdListener() {
                        public void onAdClosed() {
                            super.onAdClosed();
                        }

                        public void onAdOpened() {
                            super.onAdOpened();
                        }

                        public void onAdFailedToLoad(int errorCode) {
                            super.onAdFailedToLoad(errorCode);
                        }

                        public void onAdLeftApplication() {
                            super.onAdLeftApplication();
                        }

                        public void onAdLoaded() {
                            super.onAdLoaded();
                        }
                    });
                    adView.loadAd(new AdRequest.Builder().build());
                    adsStarted = true;
                }
                if (App.isExpired(prefs, 0, 0)) {
                    final InterstitialAd mInterstitial = new InterstitialAd(act);
                    mInterstitial.setAdUnitId(act.getResources().getString(R.string.intertestial_publihser_id));
                    mInterstitial.loadAd(new AdRequest.Builder().build());
                    mInterstitial.setAdListener(new AdListener() {
                        public void onAdLoaded() {
                            super.onAdLoaded();
                            if (mInterstitial.isLoaded()) {
                                mInterstitial.show();
                            }
                        }
                    });
                    adsStarted = true;
                }
                if (adsStarted) {
                }
                act.setMenuPurchase(false);
                return;
            }
            act.setMenuPurchase(false);
        }
    }

    private static boolean checkIfTimePurchaseAndShowDialog(SharedPreferences prefs, MessageList act, int days, long launches) {
        long daysLimitInmillis = (long) (days * 24 * 60 * 60 * 1000);
        if (prefs.getBoolean(key_user_rejected, false)) {
            return false;
        }
        boolean shouldShowDialog = false;
        if (!prefs.contains(key_first_run) || !prefs.contains(key_run_count)) {
            shouldShowDialog = true;
            prefs.edit().putLong(key_run_count, 1).putLong(key_first_run, System.currentTimeMillis()).commit();
        } else {
            long timeFirstRunMillis = prefs.getLong(key_first_run, 0);
            long timeNow = System.currentTimeMillis();
            long runCounts = prefs.getLong(key_run_count, 0);
            if (timeNow < timeFirstRunMillis || timeNow > timeFirstRunMillis + daysLimitInmillis || runCounts >= launches) {
                prefs.edit().putLong(key_run_count, 1).putLong(key_first_run, System.currentTimeMillis()).commit();
                shouldShowDialog = true;
            } else {
                prefs.edit().putLong(key_run_count, runCounts + 1).commit();
            }
        }
        if (!shouldShowDialog) {
            return shouldShowDialog;
        }
        final MessageList messageList = act;
        final SharedPreferences sharedPreferences = prefs;
        final SharedPreferences sharedPreferences2 = prefs;
        new AlertDialog.Builder(act).setTitle((int) R.string.support_us).setMessage((int) R.string.support_us_msg).setNeutralButton((int) R.string.later, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        }).setNegativeButton((int) R.string.buy_now, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                messageList.purchaseItemAkMail(sharedPreferences);
            }
        }).setPositiveButton((int) R.string.no_txs, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                sharedPreferences2.edit().putBoolean(Ads.key_user_rejected, true).commit();
            }
        }).show();
        return shouldShowDialog;
    }

    private static void checkToValidatePurchase(SharedPreferences prefs, MessageList act, int days, long launches) {
        long daysLimitInmillis = (long) (days * 24 * 60 * 60 * 1000);
        if (!prefs.contains(key_first_run_after_purchase) || !prefs.contains(key_run_count_after_purchase)) {
            prefs.edit().putLong(key_run_count_after_purchase, 1).putLong(key_first_run_after_purchase, System.currentTimeMillis()).commit();
            return;
        }
        long timeFirstRunMillis = prefs.getLong(key_first_run_after_purchase, 0);
        long timeNow = System.currentTimeMillis();
        long runCounts = prefs.getLong(key_run_count, 0);
        if (timeNow < timeFirstRunMillis || timeNow > timeFirstRunMillis + daysLimitInmillis || runCounts >= launches) {
            prefs.edit().putLong(key_run_count_after_purchase, 1).putLong(key_first_run_after_purchase, System.currentTimeMillis()).commit();
            act.purchaseItemAkMail(prefs);
            return;
        }
        prefs.edit().putLong(key_run_count_after_purchase, runCounts + 1).commit();
    }
}
