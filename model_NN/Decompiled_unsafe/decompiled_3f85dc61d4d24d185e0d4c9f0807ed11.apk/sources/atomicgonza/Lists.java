package atomicgonza;

import android.util.Log;
import com.fsck.k9.mail.Message;
import java.util.List;

public class Lists {
    public static void printList(List<String> array) {
        if (array == null || array.isEmpty()) {
            Log.e("AtomicGonza", "okAG NULL ARRAY OR EMPTY " + new Exception().getStackTrace()[1].toString());
            return;
        }
        String temp = "";
        for (int i = 0; i < array.size(); i++) {
            temp = temp + "|" + array.get(i);
        }
        Log.e("AtomicGonza", "okAG array: " + (temp + "|") + " " + new Exception().getStackTrace()[1].toString());
    }

    public static void printArray(String[] array) {
        if (array == null || array.length == 0) {
            Log.e("AtomicGonza", "okAG NULL ARRAY OR EMPTY " + new Exception().getStackTrace()[1].toString());
            return;
        }
        String temp = "";
        for (int i = 0; i < array.length; i++) {
            temp = temp + "|" + array[i];
        }
        Log.e("AtomicGonza", "okAG array: " + (temp + "|") + " " + new Exception().getStackTrace()[1].toString());
    }

    public static <T extends Message> void printListMessages(List<T> array) {
        if (array == null || array.isEmpty()) {
            Log.e("AtomicGonza", "okAG NULL ARRAY OR EMPTY " + new Exception().getStackTrace()[1].toString());
            return;
        }
        String temp = "";
        for (int i = 0; i < array.size(); i++) {
            temp = temp + "|" + ((Message) array.get(i)).getUid() + "";
        }
        Log.e("AtomicGonza", "okAG array: " + (temp + "|") + " " + new Exception().getStackTrace()[1].toString());
    }
}
