package net.youmi.android;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.widget.Button;
import android.widget.RelativeLayout;

class ar extends RelativeLayout {
    Button a;
    ai b;
    ca c;

    public ar(Activity activity, ai aiVar, ca caVar) {
        super(activity);
        this.b = aiVar;
        this.c = caVar;
        this.a = new Button(activity);
        Drawable a2 = by.a(this.c, EMPTY_STATE_SET, PRESSED_ENABLED_STATE_SET);
        if (a2 != null) {
            this.a.setBackgroundDrawable(a2);
        } else {
            this.a.setText("返回");
        }
        this.a.setOnClickListener(new cp(this));
        RelativeLayout.LayoutParams a3 = h.a(this.c.b().a(), this.c.b().a());
        a3.addRule(15);
        a3.addRule(11);
        addView(this.a, a3);
        setBackgroundDrawable(by.a(this.c));
    }
}
