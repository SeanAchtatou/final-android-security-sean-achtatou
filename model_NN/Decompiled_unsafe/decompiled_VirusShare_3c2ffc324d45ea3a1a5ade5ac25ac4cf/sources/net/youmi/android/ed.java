package net.youmi.android;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import java.io.File;
import java.io.FileOutputStream;

class ed {
    private String a = "";
    /* access modifiers changed from: private */
    public String b = "";
    /* access modifiers changed from: private */
    public long c = -1;
    private long d = -1;

    ed(String str, long j, long j2) {
        this.a = str;
        this.b = Environment.getExternalStorageDirectory() + "/" + str + "/";
        this.c = j;
        this.d = j2;
        a();
        c();
    }

    /* access modifiers changed from: private */
    public boolean a(File file) {
        if (file == null) {
            return false;
        }
        if (this.d == -1) {
            return false;
        }
        return this.d > 0 && System.currentTimeMillis() - file.lastModified() > this.d;
    }

    private void c() {
        try {
            if (this.c != -1 || this.d != -1) {
                if (this.c > 0 || this.d > 0) {
                    new Thread(new an(this)).start();
                }
            }
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: package-private */
    public String a(String str) {
        return String.valueOf(this.b) + str;
    }

    /* access modifiers changed from: package-private */
    public void a() {
        try {
            File file = new File(this.b);
            if (!file.exists()) {
                file.mkdirs();
            }
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(Context context, String str, byte[] bArr) {
        try {
            if (az.a(context)) {
                FileOutputStream fileOutputStream = new FileOutputStream(c(str));
                fileOutputStream.write(bArr);
                fileOutputStream.flush();
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public String b(String str) {
        if (str == null) {
            return null;
        }
        String trim = str.trim();
        if (trim.length() == 0) {
            return null;
        }
        return cq.b(trim);
    }

    /* access modifiers changed from: package-private */
    public String[] b() {
        return new File(this.b).list();
    }

    /* access modifiers changed from: package-private */
    public String c(String str) {
        String b2 = b(str);
        if (b2 != null) {
            return a(b2);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public File d(String str) {
        return new File(c(str));
    }

    /* access modifiers changed from: package-private */
    public boolean e(String str) {
        String b2 = b(str);
        if (b2 == null) {
            return false;
        }
        return f(b2);
    }

    /* access modifiers changed from: package-private */
    public boolean f(String str) {
        File file = new File(a(str));
        if (file.exists()) {
            return file.canRead();
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public Bitmap g(String str) {
        try {
            return BitmapFactory.decodeFile(c(str));
        } catch (Exception e) {
            return null;
        }
    }
}
