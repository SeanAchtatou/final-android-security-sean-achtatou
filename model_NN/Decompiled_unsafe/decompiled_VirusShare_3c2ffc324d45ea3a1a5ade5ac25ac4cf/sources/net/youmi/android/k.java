package net.youmi.android;

import android.content.Context;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import java.util.Hashtable;

class k {
    static Hashtable a;
    static eo b;

    k() {
    }

    static bw a(Context context, String str, String str2, String str3, String str4, String str5) {
        try {
            PackageInfo packageArchiveInfo = context.getPackageManager().getPackageArchiveInfo(str, 1);
            if (packageArchiveInfo != null) {
                bw bwVar = new bw();
                bwVar.a = str5;
                bwVar.b = str2;
                bwVar.d = str4;
                bwVar.c = str3;
                ct ctVar = new ct();
                ctVar.a = packageArchiveInfo.packageName;
                ctVar.c = packageArchiveInfo.versionCode;
                ctVar.b = packageArchiveInfo.versionName;
                bwVar.f = ctVar;
                try {
                    PackageInfo packageInfo = context.getPackageManager().getPackageInfo(packageArchiveInfo.packageName, 0);
                    if (packageInfo != null) {
                        ct ctVar2 = new ct();
                        ctVar2.a = packageInfo.packageName;
                        ctVar2.c = packageInfo.versionCode;
                        ctVar2.b = packageInfo.versionName;
                        bwVar.e = ctVar2;
                    }
                } catch (Exception e) {
                }
                if (a == null) {
                    a = new Hashtable(20);
                }
                if (!a.containsKey(packageArchiveInfo.packageName)) {
                    a.put(packageArchiveInfo.packageName, bwVar);
                }
                return bwVar;
            }
        } catch (Exception e2) {
        }
        return null;
    }

    static void a(Context context) {
        try {
            IntentFilter intentFilter = new IntentFilter("android.intent.action.PACKAGE_ADDED");
            intentFilter.addAction("android.intent.action.PACKAGE_INSTALL");
            intentFilter.addDataScheme("package");
            if (b == null) {
                b = new eo();
            }
            context.registerReceiver(b, intentFilter);
        } catch (Exception e) {
        }
    }

    static synchronized void a(Context context, String str, String str2) {
        synchronized (k.class) {
            if (!(str == null || str2 == null)) {
                try {
                    if (a != null) {
                        if (a.containsKey(str)) {
                            bw bwVar = (bw) a.get(str);
                            if (bwVar != null) {
                                try {
                                    if (str2.equals("android.intent.action.PACKAGE_ADDED")) {
                                        try {
                                            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(str, 0);
                                            if (packageInfo != null) {
                                                ct ctVar = new ct();
                                                ctVar.a = packageInfo.packageName;
                                                ctVar.c = packageInfo.versionCode;
                                                ctVar.b = packageInfo.versionName;
                                                bwVar.g = ctVar;
                                                a(context, bwVar, 4);
                                            }
                                        } catch (Exception e) {
                                            f.a(e);
                                        }
                                        try {
                                            context.startActivity(context.getPackageManager().getLaunchIntentForPackage(str));
                                        } catch (Exception e2) {
                                        }
                                    }
                                } catch (Exception e3) {
                                }
                            }
                            try {
                                a.remove(str);
                            } catch (Exception e4) {
                            }
                        }
                    }
                } catch (Exception e5) {
                    try {
                        f.a(e5);
                    } catch (Exception e6) {
                        f.a(e6);
                    }
                }
            }
        }
        return;
    }

    static void a(Context context, bw bwVar, int i) {
        try {
            new Thread(new es(context, bwVar, i)).start();
        } catch (Exception e) {
            f.a(e);
        }
    }

    static void b(Context context, bw bwVar, int i) {
        String str;
        int i2;
        String str2;
        int i3;
        String str3;
        String str4;
        int i4;
        if (bwVar != null) {
            int i5 = bwVar.f != null ? bwVar.e != null ? 1 : 0 : 2;
            String str5 = null;
            if (bwVar.e != null) {
                int i6 = bwVar.e.c;
                str = bwVar.e.b;
                int i7 = i6;
                str5 = bwVar.e.a;
                i2 = i7;
            } else {
                str = null;
                i2 = 0;
            }
            if (bwVar.f != null) {
                int i8 = bwVar.f.c;
                str2 = bwVar.f.b;
                int i9 = i8;
                str5 = bwVar.f.a;
                i3 = i9;
            } else {
                str2 = null;
                i3 = 0;
            }
            if (bwVar.g != null) {
                int i10 = bwVar.g.c;
                str3 = bwVar.g.b;
                str4 = bwVar.g.a;
                i4 = i10;
            } else {
                str3 = null;
                str4 = str5;
                i4 = 0;
            }
            av.a(context, i, i5, i2, i3, i4, bwVar.a, str4, str, str2, str3, bwVar.c, bwVar.d, bwVar.b);
        }
    }
}
