package net.youmi.android;

import android.webkit.WebChromeClient;
import android.webkit.WebView;

class cg extends WebChromeClient {
    final /* synthetic */ aw a;

    cg(aw awVar) {
        this.a = awVar;
    }

    public void onProgressChanged(WebView webView, int i) {
        super.onProgressChanged(webView, i);
        this.a.a(i);
    }

    public void onReceivedTitle(WebView webView, String str) {
        super.onReceivedTitle(webView, str);
        try {
            if (this.a.b != null) {
                this.a.b.setTitle(str);
            }
        } catch (Exception e) {
            f.a(e);
        }
    }
}
