package net.youmi.android;

import java.math.BigInteger;
import java.net.URLEncoder;
import java.security.MessageDigest;

class cq {
    static final char[] a = "0123456789ABCDEF".toCharArray();
    static final char[] b = "0123456789abcdef".toCharArray();
    private static final char[] c = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-".toCharArray();
    private static final char d = ((char) Integer.parseInt("00000011", 2));
    private static final char e = ((char) Integer.parseInt("00001111", 2));
    private static final char f = ((char) Integer.parseInt("00111111", 2));
    private static final char g = ((char) Integer.parseInt("11111100", 2));
    private static final char h = ((char) Integer.parseInt("11110000", 2));
    private static final char i = ((char) Integer.parseInt("11000000", 2));
    private static final char[] j = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};
    private static int[] k = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31};

    cq() {
    }

    static final String a(int i2) {
        StringBuilder sb = new StringBuilder(i2);
        for (int i3 = 0; i3 < i2; i3++) {
            sb.append(c[bn.a(c.length)]);
        }
        return sb.toString();
    }

    static String a(String str) {
        try {
            String encode = URLEncoder.encode(str, "UTF-8");
            return encode.indexOf("+") > -1 ? encode.replace("+", "%20") : encode;
        } catch (Exception e2) {
            return "";
        }
    }

    static String a(byte[] bArr) {
        char[] cArr = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(bArr);
            byte[] digest = instance.digest();
            char[] cArr2 = new char[32];
            int i2 = 0;
            for (int i3 = 0; i3 < 16; i3++) {
                byte b2 = digest[i3];
                int i4 = i2 + 1;
                cArr2[i2] = cArr[(b2 >>> 4) & 15];
                i2 = i4 + 1;
                cArr2[i4] = cArr[b2 & 15];
            }
            return new String(cArr2);
        } catch (Exception e2) {
            return null;
        }
    }

    static final String a(byte[] bArr, String str) {
        StringBuilder sb = new StringBuilder();
        byte[] bytes = str.getBytes();
        byte[] bArr2 = new byte[(bytes.length + bArr.length)];
        System.arraycopy(bytes, 0, bArr2, 0, bytes.length);
        System.arraycopy(bArr, 0, bArr2, bytes.length, bArr.length);
        String substring = a(bArr2).substring(9, 18);
        int length = substring.length();
        for (int i2 = 0; i2 < 9; i2 += 3) {
            int i3 = i2 + 3;
            if (i3 > length) {
                i3 = length;
            }
            int parseInt = Integer.parseInt(substring.substring(i2, i3), 16);
            sb.append(c[parseInt < 64 ? 0 : parseInt >>> 6]);
            sb.append(c[parseInt & 63]);
        }
        String sb2 = sb.toString();
        String b2 = b(String.valueOf(sb2) + str);
        int i4 = 0;
        for (int i5 = 0; i5 < 26; i5++) {
            i4 <<= 1;
            if (b2.charAt(i5) > '7') {
                i4++;
            }
        }
        int i6 = i4 < 67 ? 67 : i4;
        int i7 = (i6 & 1) == 0 ? i6 - 1 : i6 - 2;
        while (i7 > 0) {
            boolean z = true;
            int i8 = 0;
            while (true) {
                if (i8 >= k.length) {
                    break;
                } else if (i7 % k[i8] == 0) {
                    z = false;
                    break;
                } else {
                    i8++;
                }
            }
            if (z) {
                break;
            }
            i7 -= 2;
        }
        sb.delete(0, sb.length());
        byte[] bytes2 = b2.getBytes();
        int length2 = bytes2.length;
        int length3 = bArr.length;
        int i9 = 0;
        int i10 = 0;
        while (i10 < length3) {
            int i11 = i9 + 1;
            byte b3 = (byte) (bytes2[i9] ^ bArr[i10]);
            int i12 = i11 >= length2 ? 0 : i11;
            sb.append(a[(b3 & 240) >>> 4]);
            sb.append(a[b3 & 15]);
            i10++;
            i9 = i12;
        }
        String sb3 = sb.toString();
        sb.delete(0, sb.length());
        int length4 = sb3.length();
        sb.append(c[length4 % 3]);
        int i13 = i6;
        for (int i14 = 0; i14 < length4; i14 += 3) {
            int parseInt2 = Integer.parseInt(sb3.substring(i14, i14 + 3 < length4 ? i14 + 3 : length4), 16);
            byte b4 = ((i13 * i7) + 13859) & 63;
            sb.append(c[(((byte) (parseInt2 < 64 ? 0 : parseInt2 >>> 6)) + b4) & 63]);
            i13 = ((b4 * i7) + 13859) & 63;
            sb.append(c[(parseInt2 + i13) & 63]);
        }
        return String.valueOf(sb2) + sb.toString();
    }

    static String b(String str) {
        if (str == null) {
            return null;
        }
        try {
            if (str.length() <= 0) {
                return null;
            }
            try {
                MessageDigest instance = MessageDigest.getInstance("MD5");
                instance.update(str.getBytes(), 0, str.length());
                return String.format("%032x", new BigInteger(1, instance.digest()));
            } catch (Exception e2) {
                return null;
            }
        } catch (Exception e3) {
            return "";
        }
    }

    static final String c(String str) {
        int length = str.length();
        StringBuilder sb = new StringBuilder((length << 1) / 3);
        int i2 = 0;
        for (int i3 = 0; i3 < length; i3 += 3) {
            int parseInt = Integer.parseInt(str.substring(i3, i3 + 3 < length ? i3 + 3 : length), 16);
            sb.append(c[(byte) (parseInt < 64 ? 0 : parseInt >>> 6)]);
            sb.append(c[parseInt & 63]);
            i2 = i2 + 1 + 1;
        }
        return sb.toString();
    }
}
