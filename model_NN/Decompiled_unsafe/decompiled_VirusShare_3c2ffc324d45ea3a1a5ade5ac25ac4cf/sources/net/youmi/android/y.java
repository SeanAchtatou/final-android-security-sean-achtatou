package net.youmi.android;

import com.madhouse.android.ads.AdView;

class y {
    final /* synthetic */ ca a;
    private ca b;
    private dv c;
    private af d;
    private ae e;

    public y(ca caVar, ca caVar2) {
        this.a = caVar;
        this.b = caVar2;
        if (caVar2.c()) {
            this.c = dv.b;
            return;
        }
        switch (caVar2.e()) {
            case 120:
                this.c = dv.a;
                return;
            case 160:
                this.c = dv.b;
                return;
            case AdView.PHONE_AD_MEASURE_240:
                this.c = dv.c;
                return;
            case AdView.PHONE_AD_MEASURE_320:
                this.c = dv.d;
                return;
            default:
                this.c = dv.b;
                return;
        }
    }

    public int a() {
        return this.c.b();
    }

    public dv b() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public af c() {
        if (this.d == null) {
            this.d = new af(this, this.b, this);
        }
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public ae d() {
        if (this.e == null) {
            this.e = new ae(this, this.b, this);
        }
        return this.e;
    }
}
