package net.youmi.android;

import android.content.Context;
import java.io.ByteArrayOutputStream;
import org.apache.http.HttpResponse;

class aq extends bj {
    protected String a;
    protected ByteArrayOutputStream b;

    public aq(Context context, String str) {
        super(context, str);
    }

    /* access modifiers changed from: protected */
    public boolean a() {
        this.b = new ByteArrayOutputStream(4096);
        this.k = this.b;
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean a(HttpResponse httpResponse) {
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean b() {
        try {
            if (this.b != null) {
                if (this.f == null) {
                    byte[] byteArray = this.b.toByteArray();
                    this.a = new String(byteArray, "utf-8");
                    if (this.a != null) {
                        this.f = cv.c(this.a);
                    }
                    if (this.f != null) {
                        this.f = this.f.trim().toLowerCase();
                        if (!this.f.equals("utf-8")) {
                            this.a = new String(byteArray, this.f);
                        }
                    }
                } else {
                    this.a = new String(this.b.toByteArray(), this.f);
                }
                return true;
            }
            this.m = 2;
            return false;
        } catch (Exception e) {
            this.m = 2;
        }
    }
}
