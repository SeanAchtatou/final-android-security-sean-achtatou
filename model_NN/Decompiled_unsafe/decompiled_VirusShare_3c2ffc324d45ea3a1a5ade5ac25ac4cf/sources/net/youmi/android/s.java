package net.youmi.android;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import org.apache.http.HttpHost;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

class s {
    private static String a;

    s() {
    }

    static String a() {
        if (a == null) {
            try {
                StringBuilder sb = new StringBuilder(256);
                sb.append("Mozilla/5.0 (Linux; U; Android ");
                sb.append(Build.VERSION.RELEASE);
                sb.append("; ");
                sb.append(eh.a().toLowerCase());
                sb.append("; ");
                sb.append(eh.b());
                sb.append(" Build/");
                sb.append(Build.ID);
                sb.append(") AppleWebkit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1");
                a = sb.toString();
            } catch (Exception e) {
                f.a(e);
                return "";
            }
        }
        return a;
    }

    static String a(Context context) {
        NetworkInfo activeNetworkInfo;
        try {
            if (dq.e(context) && (activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo()) != null && activeNetworkInfo.isAvailable()) {
                if (activeNetworkInfo.getType() != 0) {
                    return "wifi";
                }
                String extraInfo = activeNetworkInfo.getExtraInfo();
                if (extraInfo == null) {
                    return "";
                }
                String lowerCase = extraInfo.trim().toLowerCase();
                return lowerCase.length() > 10 ? lowerCase.substring(0, 10) : lowerCase;
            }
        } catch (Exception e) {
        }
        return "";
    }

    static DefaultHttpClient a(Context context, ee eeVar) {
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(c(context));
        defaultHttpClient.setRedirectHandler(new am(eeVar));
        return defaultHttpClient;
    }

    static boolean b(Context context) {
        if (!dq.e(context)) {
            return true;
        }
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo != null && activeNetworkInfo.isAvailable()) {
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    static HttpParams c(Context context) {
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpClientParams.setRedirecting(basicHttpParams, true);
        HttpProtocolParams.setUserAgent(basicHttpParams, a());
        HttpProtocolParams.setContentCharset(basicHttpParams, "utf-8");
        HttpProtocolParams.setHttpElementCharset(basicHttpParams, "utf-8");
        if (a(context).equals("cmwap")) {
            basicHttpParams.setParameter("http.route.default-proxy", new HttpHost("10.0.0.172", 80, (String) null));
        }
        return basicHttpParams;
    }
}
