package net.youmi.android;

import android.content.Context;
import android.os.Build;
import android.telephony.CellLocation;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import java.lang.reflect.Method;
import java.util.Locale;

class eh {
    private static int a = -1;
    private static int b = -1;
    private static int c = -1;
    private static int d = -1;
    private static int e = -1;
    private static String f;
    private static String g;
    private static int h = -1;
    private static String i;
    private static String j;
    private static String k = "";
    private static el l = null;
    private static int m = 0;
    /* access modifiers changed from: private */
    public static long n = 0;
    /* access modifiers changed from: private */
    public static long o = 0;
    /* access modifiers changed from: private */
    public static boolean p = false;

    eh() {
    }

    static String a() {
        if (g == null) {
            Locale locale = Locale.getDefault();
            g = String.format("%s-%s", locale.getLanguage(), locale.getCountry());
        }
        return g;
    }

    static el a(Context context) {
        if (l == null) {
            l = new el(context);
        }
        return l;
    }

    static boolean a(int i2) {
        return h >= i2;
    }

    static boolean a(long j2) {
        if (j2 - o <= g()) {
            return false;
        }
        if (!p) {
            return true;
        }
        if (j2 - n <= 35000) {
            return false;
        }
        p = false;
        return true;
    }

    static String b() {
        return Build.MODEL;
    }

    static void b(int i2) {
        h = i2;
        m = 0;
    }

    static void b(Context context) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            if (telephonyManager != null) {
                try {
                    String networkOperatorName = telephonyManager.getNetworkOperatorName();
                    if (networkOperatorName == null) {
                        i = "";
                    } else {
                        i = networkOperatorName;
                    }
                } catch (Exception e2) {
                }
                try {
                    String line1Number = telephonyManager.getLine1Number();
                    if (line1Number == null) {
                        j = "";
                        return;
                    }
                    String trim = line1Number.trim();
                    if (trim.length() > 11) {
                        int length = trim.length() - 11;
                        j = trim.substring(length, length + 7);
                    } else if (trim.length() == 11) {
                        j = trim.substring(0, 7);
                    } else {
                        j = "";
                    }
                } catch (Exception e3) {
                }
            }
        } catch (Exception e4) {
        }
    }

    static String c() {
        return "android " + Build.VERSION.RELEASE;
    }

    static String c(Context context) {
        if (j == null) {
            b(context);
        }
        return j == null ? "" : j;
    }

    static String d() {
        return Build.BRAND;
    }

    static String d(Context context) {
        if (i == null) {
            b(context);
        }
        return i == null ? "" : i;
    }

    static void e() {
        m++;
    }

    static boolean e(Context context) {
        if (er.a()) {
            return true;
        }
        return a(context).f();
    }

    static int f() {
        return m;
    }

    static synchronized String f(Context context) {
        String str;
        String str2;
        String str3;
        synchronized (eh.class) {
            if (f == null || f.length() <= 0) {
                TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
                if (telephonyManager != null) {
                    try {
                        String trim = telephonyManager.getNetworkOperator().trim();
                        if (trim.length() > 3) {
                            str2 = trim.substring(0, 3);
                            str3 = trim.substring(3);
                        } else {
                            str2 = "";
                            str3 = "";
                        }
                    } catch (Exception e2) {
                        str2 = "";
                        str3 = "";
                    }
                    try {
                        if (str2.length() == 0 || str3.length() == 0) {
                            try {
                                String subscriberId = telephonyManager.getSubscriberId();
                                if (subscriberId != null) {
                                    String trim2 = subscriberId.trim();
                                    if (trim2.length() > 5) {
                                        str2 = trim2.substring(0, 3);
                                        str3 = trim2.substring(3, 5);
                                    }
                                }
                            } catch (Exception e3) {
                                f.a(e3);
                                str2 = str2;
                            }
                        }
                        int phoneType = telephonyManager.getPhoneType();
                        if (phoneType == 1) {
                            if (a <= -1 || b <= -1) {
                                GsmCellLocation gsmCellLocation = (GsmCellLocation) telephonyManager.getCellLocation();
                                if (gsmCellLocation != null) {
                                    a = gsmCellLocation.getCid();
                                    b = gsmCellLocation.getLac();
                                }
                                if (a > -1 && b > -1) {
                                    f = "0|" + str2 + "|" + str3 + "|" + a + "|" + b;
                                    str = f;
                                }
                            } else {
                                f = "0|" + str2 + "|" + str3 + "|" + a + "|" + b;
                                str = f;
                            }
                        } else if (phoneType != 2) {
                            f = "";
                            str = f;
                        } else if (c <= -1 || e <= -1 || d <= -1) {
                            CellLocation cellLocation = telephonyManager.getCellLocation();
                            Method[] methods = cellLocation.getClass().getMethods();
                            if (methods != null) {
                                for (Method method : methods) {
                                    if (method != null) {
                                        if (method.getName().equals("getBaseStationId")) {
                                            c = ((Integer) method.invoke(cellLocation, new Object[0])).intValue();
                                        } else if (method.getName().equals("getNetworkId")) {
                                            e = ((Integer) method.invoke(cellLocation, new Object[0])).intValue();
                                        } else if (method.getName().equals("getSystemId")) {
                                            d = ((Integer) method.invoke(cellLocation, new Object[0])).intValue();
                                        }
                                    }
                                }
                            }
                            if (c > -1 && e > -1 && d > -1) {
                                f = "1|" + str2 + "|" + str3 + "|" + c + "|" + e + "|" + d;
                                str = f;
                            }
                        } else {
                            f = "1|" + str2 + "|" + str3 + "|" + c + "|" + e + "|" + d;
                            str = f;
                        }
                    } catch (Exception e4) {
                        f.a(e4);
                    }
                }
                str = "";
            } else {
                str = f;
            }
        }
        return str;
    }

    static long g() {
        long g2 = (long) bq.g();
        if (g2 < er.g()) {
            g2 = er.g();
        }
        return g2 < bk.a() ? bk.a() : g2;
    }
}
