package net.youmi.android;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;

class eu {
    static long a = 0;

    eu() {
    }

    static void a(Activity activity, AdView adView) {
        try {
            int taskId = activity.getTaskId();
            if (eh.a(taskId)) {
                ao.b(activity);
                return;
            }
            long currentTimeMillis = System.currentTimeMillis();
            if (currentTimeMillis - a < 60000) {
                ao.b(activity);
                return;
            }
            a = currentTimeMillis;
            eh.b(taskId);
            try {
                ao.a(activity);
            } catch (Exception e) {
            }
            a(activity);
            try {
                fd.a(activity, adView);
            } catch (Exception e2) {
            }
            try {
                k.a(activity.getApplicationContext());
            } catch (Exception e3) {
            }
            try {
                ei.a(activity, adView);
            } catch (Exception e4) {
            }
            try {
                de.a(activity);
            } catch (Exception e5) {
            }
        } catch (Exception e6) {
        }
    }

    static void a(Context context) {
        try {
            if (bq.h() == 0) {
                f.a("*****");
            } else if (bq.h() < 0) {
                f.a("*");
            } else {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < bq.h(); i++) {
                    sb.append("-");
                }
                f.a(sb.toString());
            }
            f.a("Current sdk version is youmi android sdk " + bq.f());
            f.a("App ID is set to " + er.c());
            f.a("App Sec is set to " + er.e());
            try {
                f.a("App PackageName is set to " + context.getPackageName());
                PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                if (packageInfo != null) {
                    f.a("App Version code is set to " + packageInfo.versionCode);
                    f.a("App Version name is set to " + packageInfo.versionName);
                }
            } catch (Exception e) {
            }
            try {
                f.a(er.a() ? String.valueOf("TestMode is set to ") + "TRUE" : eh.e(context) ? String.valueOf("TestMode is set to ") + "FALSE." : String.valueOf("TestMode is set to ") + "FALSE");
            } catch (Exception e2) {
            }
            f.a("Requesting interval is set to " + er.f() + " seconds");
        } catch (Exception e3) {
        }
    }
}
