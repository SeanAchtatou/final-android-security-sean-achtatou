package com.adwo.adsdk;

import android.graphics.Camera;
import android.graphics.Matrix;
import android.view.animation.Animation;
import android.view.animation.Transformation;

final class Y extends Animation {
    private static /* synthetic */ int[] k;
    private final float a;
    private final float b;
    private final float c;
    private final float d;
    private final float e = 0.0f;
    private final float f = 0.0f;
    private final float g;
    private final Q h;
    private final boolean i;
    private Camera j;

    private static /* synthetic */ int[] a() {
        int[] iArr = k;
        if (iArr == null) {
            iArr = new int[Q.values().length];
            try {
                iArr[Q.X.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[Q.Y.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[Q.Z.ordinal()] = 3;
            } catch (NoSuchFieldError e4) {
            }
            k = iArr;
        }
        return iArr;
    }

    public Y(float f2, float f3, float f4, float f5, float f6, float f7, float f8, boolean z, Q q) {
        this.a = f2;
        this.b = f3;
        this.c = f4;
        this.d = f5;
        this.g = f8;
        this.i = z;
        this.h = q;
    }

    public final void initialize(int i2, int i3, int i4, int i5) {
        super.initialize(i2, i3, i4, i5);
        this.j = new Camera();
    }

    /* access modifiers changed from: protected */
    public final void applyTransformation(float f2, Transformation transformation) {
        float f3 = this.a;
        float f4 = f3 + ((this.b - f3) * f2);
        float f5 = this.c;
        float f6 = this.d;
        float f7 = this.e;
        float f8 = this.f;
        Camera camera = this.j;
        Matrix matrix = transformation.getMatrix();
        camera.save();
        if (this.i) {
            camera.translate(0.0f, 0.0f, this.g * f2);
        } else {
            camera.translate(0.0f, 0.0f, this.g * (1.0f - f2));
        }
        switch (a()[this.h.ordinal()]) {
            case 1:
                camera.rotateX(f4);
                break;
            case 2:
                camera.rotateY(f4);
                break;
            case 3:
                camera.rotateZ(f4);
                break;
        }
        camera.getMatrix(matrix);
        camera.restore();
        matrix.preTranslate(-f5, -f6);
        matrix.postTranslate(f5 - f7, f6 - f8);
    }
}
