package com.adwo.adsdk;

import android.graphics.Bitmap;
import android.os.Message;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

final class D extends WebChromeClient {
    private /* synthetic */ C0013n a;

    D(C0013n nVar) {
        this.a = nVar;
    }

    public final void onCloseWindow(WebView webView) {
        super.onCloseWindow(webView);
    }

    public final boolean onCreateWindow(WebView webView, boolean z, boolean z2, Message message) {
        return super.onCreateWindow(webView, z, z2, message);
    }

    public final boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
        return super.onJsAlert(webView, str, str2, jsResult);
    }

    public final boolean onJsBeforeUnload(WebView webView, String str, String str2, JsResult jsResult) {
        return super.onJsBeforeUnload(webView, str, str2, jsResult);
    }

    public final boolean onJsConfirm(WebView webView, String str, String str2, JsResult jsResult) {
        return super.onJsConfirm(webView, str, str2, jsResult);
    }

    public final boolean onJsPrompt(WebView webView, String str, String str2, String str3, JsPromptResult jsPromptResult) {
        return super.onJsPrompt(webView, str, str2, str3, jsPromptResult);
    }

    public final void onReceivedIcon(WebView webView, Bitmap bitmap) {
        super.onReceivedIcon(webView, bitmap);
    }

    public final void onRequestFocus(WebView webView) {
        super.onRequestFocus(webView);
    }

    public final void onReceivedTitle(WebView webView, String str) {
        this.a.b().setTitle(str);
        super.onReceivedTitle(webView, str);
    }

    public final void onProgressChanged(WebView webView, int i) {
        this.a.b().setProgress(i * 100);
        super.onProgressChanged(webView, i);
    }
}
