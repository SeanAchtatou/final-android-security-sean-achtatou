package com.adwo.adsdk;

import android.view.animation.DecelerateInterpolator;

final class N implements Runnable {
    /* access modifiers changed from: private */
    public C0001b a;
    /* access modifiers changed from: private */
    public C0001b b;
    private Q c;
    /* access modifiers changed from: private */
    public /* synthetic */ AdwoAdView d;

    public N(AdwoAdView adwoAdView, C0001b bVar, Q q) {
        this.d = adwoAdView;
        this.a = bVar;
        this.c = q;
    }

    public final void run() {
        this.b = this.d.c;
        if (this.b != null) {
            this.b.setVisibility(8);
            this.b.c();
        }
        this.a.setVisibility(0);
        Y y = new Y(90.0f, 0.0f, ((float) this.d.getWidth()) / 2.0f, ((float) this.d.getHeight()) / 2.0f, 0.0f, 0.0f, -0.4f * ((float) this.d.getWidth()), false, this.c);
        y.setDuration(500);
        y.setFillAfter(true);
        y.setInterpolator(new DecelerateInterpolator());
        y.setAnimationListener(new O(this));
        this.d.startAnimation(y);
    }
}
