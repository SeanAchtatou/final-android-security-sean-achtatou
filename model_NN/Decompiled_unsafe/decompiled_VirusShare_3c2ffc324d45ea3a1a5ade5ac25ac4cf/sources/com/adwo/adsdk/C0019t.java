package com.adwo.adsdk;

import android.view.MotionEvent;
import android.view.View;

/* renamed from: com.adwo.adsdk.t  reason: case insensitive filesystem */
final class C0019t implements View.OnTouchListener {
    private /* synthetic */ C0013n a;

    C0019t(C0013n nVar) {
        this.a = nVar;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return this.a.p.onTouchEvent(motionEvent);
    }
}
