package com.adwo.adsdk;

import android.content.Context;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.Set;

public final class FSAdUtil {
    private static boolean B = true;
    private static final FSAdUtil C = new FSAdUtil();
    private static byte[] D = null;
    private static int F = 0;
    private static boolean b = false;
    private static byte[] c = null;
    private static byte[] d = null;
    private static int g = 1;
    private static int h = 2;
    private static int i = 3;
    private static int j = 4;
    private static volatile int k = 1;
    private static byte n = 0;
    private static byte o = 0;
    private static byte[] p = null;
    private static byte[] q = null;
    private static byte r = 0;
    private static short s = 0;
    private static byte[] t = null;
    private static byte u = 2;
    private static byte v = 3;
    private static byte w = 18;
    private static Set y = new HashSet();
    private byte A;
    private String E = null;
    private TelephonyManager a;
    private String e = null;
    private String f;
    private String l = null;
    private Context m = null;
    private String x = "";
    /* access modifiers changed from: private */
    public SplashAdListener z;

    public final void setSplashAdListener(SplashAdListener splashAdListener) {
        this.z = splashAdListener;
    }

    protected static boolean a() {
        return B;
    }

    public static FSAdUtil getInstance() {
        return C;
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00d6  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x003d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void loadAd(android.app.Activity r11, java.lang.String r12, boolean r13) {
        /*
            r10 = this;
            r9 = 8
            r8 = 6
            r7 = 9
            r6 = 0
            r5 = 1
            r10.m = r11
            android.content.Context r0 = r10.m
            java.lang.String r1 = "phone"
            java.lang.Object r0 = r0.getSystemService(r1)
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0
            r10.a = r0
            r10.a(r11)
            android.content.res.Resources r0 = r11.getResources()
            android.content.res.Configuration r0 = r0.getConfiguration()
            java.util.Locale r0 = r0.locale
            java.lang.String r0 = r0.getLanguage()
            java.lang.String r1 = "en"
            boolean r1 = r0.contains(r1)
            if (r1 != 0) goto L_0x0201
            java.lang.String r1 = "zh"
            boolean r1 = r0.contains(r1)
            if (r1 == 0) goto L_0x01a9
            r0 = r6
        L_0x0037:
            com.adwo.adsdk.FSAdUtil.u = r0
            java.lang.String r0 = r10.l
            if (r0 != 0) goto L_0x004d
            android.telephony.TelephonyManager r0 = r10.a
            if (r0 == 0) goto L_0x0204
            android.telephony.TelephonyManager r0 = r10.a
            java.lang.String r0 = r0.getSimSerialNumber()
            r10.f = r0
            java.lang.String r0 = r10.f
        L_0x004b:
            r10.l = r0
        L_0x004d:
            byte[] r0 = com.adwo.adsdk.FSAdUtil.D
            if (r0 != 0) goto L_0x0054
            r10.b()
        L_0x0054:
            java.lang.String r0 = r11.getPackageName()
            java.lang.String r1 = "UTF-8"
            byte[] r0 = r0.getBytes(r1)     // Catch:{ UnsupportedEncodingException -> 0x0207 }
            com.adwo.adsdk.FSAdUtil.t = r0     // Catch:{ UnsupportedEncodingException -> 0x0207 }
        L_0x0060:
            r10.b(r11)
            java.util.Properties r0 = java.lang.System.getProperties()
            java.lang.String r1 = "os.version"
            java.lang.Object r0 = r0.get(r1)
            java.lang.String r0 = (java.lang.String) r0
            if (r0 == 0) goto L_0x0089
            int r1 = r0.length()
            r2 = 3
            if (r1 < r2) goto L_0x0089
            r1 = 3
            java.lang.String r0 = r0.substring(r6, r1)
            r1 = 4621819117588971520(0x4024000000000000, double:10.0)
            double r3 = java.lang.Double.parseDouble(r0)     // Catch:{ Exception -> 0x0a39 }
            double r0 = r1 * r3
            int r0 = (int) r0     // Catch:{ Exception -> 0x0a39 }
            byte r0 = (byte) r0     // Catch:{ Exception -> 0x0a39 }
            com.adwo.adsdk.FSAdUtil.r = r0     // Catch:{ Exception -> 0x0a39 }
        L_0x0089:
            java.lang.Class<android.os.Build> r0 = android.os.Build.class
            java.lang.String r1 = android.os.Build.MODEL     // Catch:{ SecurityException -> 0x0a36, NoSuchFieldException -> 0x0a33, IllegalArgumentException -> 0x0a30, IllegalAccessException -> 0x0a2d, UnsupportedEncodingException -> 0x0a2a }
            java.lang.String r1 = r1.toLowerCase()     // Catch:{ SecurityException -> 0x0a36, NoSuchFieldException -> 0x0a33, IllegalArgumentException -> 0x0a30, IllegalAccessException -> 0x0a2d, UnsupportedEncodingException -> 0x0a2a }
            java.lang.String r2 = "UTF-8"
            byte[] r1 = r1.getBytes(r2)     // Catch:{ SecurityException -> 0x0a36, NoSuchFieldException -> 0x0a33, IllegalArgumentException -> 0x0a30, IllegalAccessException -> 0x0a2d, UnsupportedEncodingException -> 0x0a2a }
            com.adwo.adsdk.FSAdUtil.q = r1     // Catch:{ SecurityException -> 0x0a36, NoSuchFieldException -> 0x0a33, IllegalArgumentException -> 0x0a30, IllegalAccessException -> 0x0a2d, UnsupportedEncodingException -> 0x0a2a }
            java.lang.String r1 = "MANUFACTURER"
            java.lang.reflect.Field r0 = r0.getField(r1)     // Catch:{ SecurityException -> 0x0a36, NoSuchFieldException -> 0x0a33, IllegalArgumentException -> 0x0a30, IllegalAccessException -> 0x0a2d, UnsupportedEncodingException -> 0x0a2a }
            android.os.Build r1 = new android.os.Build     // Catch:{ SecurityException -> 0x0a36, NoSuchFieldException -> 0x0a33, IllegalArgumentException -> 0x0a30, IllegalAccessException -> 0x0a2d, UnsupportedEncodingException -> 0x0a2a }
            r1.<init>()     // Catch:{ SecurityException -> 0x0a36, NoSuchFieldException -> 0x0a33, IllegalArgumentException -> 0x0a30, IllegalAccessException -> 0x0a2d, UnsupportedEncodingException -> 0x0a2a }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ SecurityException -> 0x0a36, NoSuchFieldException -> 0x0a33, IllegalArgumentException -> 0x0a30, IllegalAccessException -> 0x0a2d, UnsupportedEncodingException -> 0x0a2a }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ SecurityException -> 0x0a36, NoSuchFieldException -> 0x0a33, IllegalArgumentException -> 0x0a30, IllegalAccessException -> 0x0a2d, UnsupportedEncodingException -> 0x0a2a }
            if (r0 == 0) goto L_0x00be
            int r1 = r0.length()     // Catch:{ SecurityException -> 0x0a36, NoSuchFieldException -> 0x0a33, IllegalArgumentException -> 0x0a30, IllegalAccessException -> 0x0a2d, UnsupportedEncodingException -> 0x0a2a }
            if (r1 <= 0) goto L_0x00be
            java.lang.String r0 = r0.toLowerCase()     // Catch:{ SecurityException -> 0x0a36, NoSuchFieldException -> 0x0a33, IllegalArgumentException -> 0x0a30, IllegalAccessException -> 0x0a2d, UnsupportedEncodingException -> 0x0a2a }
            java.lang.String r1 = "UTF-8"
            byte[] r0 = r0.getBytes(r1)     // Catch:{ SecurityException -> 0x0a36, NoSuchFieldException -> 0x0a33, IllegalArgumentException -> 0x0a30, IllegalAccessException -> 0x0a2d, UnsupportedEncodingException -> 0x0a2a }
            com.adwo.adsdk.FSAdUtil.p = r0     // Catch:{ SecurityException -> 0x0a36, NoSuchFieldException -> 0x0a33, IllegalArgumentException -> 0x0a30, IllegalAccessException -> 0x0a2d, UnsupportedEncodingException -> 0x0a2a }
        L_0x00be:
            r10.c()
            int r0 = com.adwo.adsdk.FSAdUtil.F
            r1 = 460(0x1cc, float:6.45E-43)
            if (r0 != r1) goto L_0x0121
            java.lang.String r0 = r10.l
            if (r0 == 0) goto L_0x0121
            java.lang.String r0 = r10.l
            r1 = 2
            byte[] r1 = new byte[r1]
            int r2 = r0.length()
            if (r2 < r8) goto L_0x0119
            r3 = 4
            java.lang.String r3 = r0.substring(r3, r8)
            java.lang.String r4 = "0"
            boolean r4 = r3.endsWith(r4)
            if (r4 != 0) goto L_0x00f3
            java.lang.String r4 = "2"
            boolean r4 = r3.endsWith(r4)
            if (r4 != 0) goto L_0x00f3
            java.lang.String r4 = "7"
            boolean r4 = r3.endsWith(r4)
            if (r4 == 0) goto L_0x0461
        L_0x00f3:
            r3 = 7
            if (r2 < r3) goto L_0x0119
            r3 = 7
            java.lang.String r3 = r0.substring(r8, r3)
            java.lang.String r4 = "0"
            boolean r4 = r3.equals(r4)
            if (r4 == 0) goto L_0x0211
            r1[r6] = r6
        L_0x0105:
            r3 = 10
            if (r2 < r3) goto L_0x0119
            r2 = 10
            java.lang.String r0 = r0.substring(r9, r2)
            java.lang.String r2 = "01"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x02c8
            r1[r5] = r5
        L_0x0119:
            byte r0 = r1[r6]
            com.adwo.adsdk.FSAdUtil.n = r0
            byte r0 = r1[r5]
            com.adwo.adsdk.FSAdUtil.o = r0
        L_0x0121:
            if (r12 == 0) goto L_0x012b
            int r0 = r12.length()
            r1 = 32
            if (r0 == r1) goto L_0x0141
        L_0x012b:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = "CONFIGURATION ERROR:  Incorrect Adwo publisher ID.  Should 32 [a-z,0-9] characters:  "
            r0.<init>(r1)
            byte[] r1 = com.adwo.adsdk.FSAdUtil.c
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            java.lang.String r1 = "Adwo SDK"
            android.util.Log.e(r1, r0)
        L_0x0141:
            java.lang.String r0 = "Adwo SDK"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "Your Adwo PID is "
            r1.<init>(r2)
            java.lang.StringBuilder r1 = r1.append(r12)
            java.lang.String r1 = r1.toString()
            android.util.Log.d(r0, r1)
            java.lang.String r0 = "UTF-8"
            byte[] r0 = r12.getBytes(r0)     // Catch:{ UnsupportedEncodingException -> 0x0a11 }
            com.adwo.adsdk.FSAdUtil.c = r0     // Catch:{ UnsupportedEncodingException -> 0x0a11 }
        L_0x015d:
            r10.A = r6
            android.util.DisplayMetrics r0 = new android.util.DisplayMetrics
            r0.<init>()
            android.content.res.Resources r0 = r11.getResources()
            android.util.DisplayMetrics r0 = r0.getDisplayMetrics()
            java.lang.String r1 = "Adwo SDK"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Version 2.3 width: "
            r2.<init>(r3)
            int r3 = r0.widthPixels
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = " height:"
            java.lang.StringBuilder r2 = r2.append(r3)
            int r3 = r0.heightPixels
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = " density:"
            java.lang.StringBuilder r2 = r2.append(r3)
            float r0 = r0.density
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            android.util.Log.i(r1, r0)
            com.adwo.adsdk.FSAdUtil.B = r13
            com.adwo.adsdk.U r0 = new com.adwo.adsdk.U
            r0.<init>(r10, r6, r13)
            android.content.Context[] r1 = new android.content.Context[r5]
            r1[r6] = r11
            r0.execute(r1)
            return
        L_0x01a9:
            java.lang.String r1 = "ko"
            boolean r1 = r0.contains(r1)
            if (r1 == 0) goto L_0x01b4
            r0 = 5
            goto L_0x0037
        L_0x01b4:
            java.lang.String r1 = "fr"
            boolean r1 = r0.contains(r1)
            if (r1 == 0) goto L_0x01bf
            r0 = 3
            goto L_0x0037
        L_0x01bf:
            java.lang.String r1 = "es"
            boolean r1 = r0.contains(r1)
            if (r1 == 0) goto L_0x01ca
            r0 = r9
            goto L_0x0037
        L_0x01ca:
            java.lang.String r1 = "de"
            boolean r1 = r0.contains(r1)
            if (r1 == 0) goto L_0x01d5
            r0 = r8
            goto L_0x0037
        L_0x01d5:
            java.lang.String r1 = "it"
            boolean r1 = r0.contains(r1)
            if (r1 == 0) goto L_0x01e0
            r0 = 7
            goto L_0x0037
        L_0x01e0:
            java.lang.String r1 = "ja"
            boolean r1 = r0.contains(r1)
            if (r1 == 0) goto L_0x01eb
            r0 = 4
            goto L_0x0037
        L_0x01eb:
            java.lang.String r1 = "ru"
            boolean r1 = r0.contains(r1)
            if (r1 == 0) goto L_0x01f6
            r0 = r7
            goto L_0x0037
        L_0x01f6:
            java.lang.String r1 = "pt"
            boolean r0 = r0.contains(r1)
            if (r0 == 0) goto L_0x0201
            r0 = r5
            goto L_0x0037
        L_0x0201:
            r0 = 2
            goto L_0x0037
        L_0x0204:
            r0 = 0
            goto L_0x004b
        L_0x0207:
            r0 = move-exception
            java.lang.String r0 = "Package Name ERROR:  Incorrect application pakage name.  "
            java.lang.String r1 = "Adwo SDK"
            android.util.Log.e(r1, r0)
            goto L_0x0060
        L_0x0211:
            java.lang.String r4 = "1"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x021d
            r1[r6] = r5
            goto L_0x0105
        L_0x021d:
            java.lang.String r4 = "2"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x022a
            r3 = 2
            r1[r6] = r3
            goto L_0x0105
        L_0x022a:
            java.lang.String r4 = "3"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x0237
            r3 = 3
            r1[r6] = r3
            goto L_0x0105
        L_0x0237:
            java.lang.String r4 = "4"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x0244
            r3 = 4
            r1[r6] = r3
            goto L_0x0105
        L_0x0244:
            java.lang.String r4 = "5"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x0251
            r3 = 5
            r1[r6] = r3
            goto L_0x0105
        L_0x0251:
            java.lang.String r4 = "6"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x025d
            r1[r6] = r8
            goto L_0x0105
        L_0x025d:
            java.lang.String r4 = "7"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x026a
            r3 = 7
            r1[r6] = r3
            goto L_0x0105
        L_0x026a:
            java.lang.String r4 = "8"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x0276
            r1[r6] = r9
            goto L_0x0105
        L_0x0276:
            java.lang.String r4 = "9"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x0282
            r1[r6] = r7
            goto L_0x0105
        L_0x0282:
            java.lang.String r4 = "A"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x0290
            r3 = 10
            r1[r6] = r3
            goto L_0x0105
        L_0x0290:
            java.lang.String r4 = "B"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x029e
            r3 = 11
            r1[r6] = r3
            goto L_0x0105
        L_0x029e:
            java.lang.String r4 = "C"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x02ac
            r3 = 12
            r1[r6] = r3
            goto L_0x0105
        L_0x02ac:
            java.lang.String r4 = "D"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x02ba
            r3 = 13
            r1[r6] = r3
            goto L_0x0105
        L_0x02ba:
            java.lang.String r4 = "E"
            boolean r3 = r4.equals(r3)
            if (r3 == 0) goto L_0x0105
            r3 = 14
            r1[r6] = r3
            goto L_0x0105
        L_0x02c8:
            java.lang.String r2 = "02"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x02d5
            r0 = 3
            r1[r5] = r0
            goto L_0x0119
        L_0x02d5:
            java.lang.String r2 = "03"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x02e1
            r1[r5] = r7
            goto L_0x0119
        L_0x02e1:
            java.lang.String r2 = "04"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x02ef
            r0 = 12
            r1[r5] = r0
            goto L_0x0119
        L_0x02ef:
            java.lang.String r2 = "05"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x02fb
            r1[r5] = r9
            goto L_0x0119
        L_0x02fb:
            java.lang.String r2 = "06"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0308
            r0 = 7
            r1[r5] = r0
            goto L_0x0119
        L_0x0308:
            java.lang.String r2 = "07"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0314
            r1[r5] = r8
            goto L_0x0119
        L_0x0314:
            java.lang.String r2 = "08"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0321
            r0 = 5
            r1[r5] = r0
            goto L_0x0119
        L_0x0321:
            java.lang.String r2 = "09"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x032e
            r0 = 2
            r1[r5] = r0
            goto L_0x0119
        L_0x032e:
            java.lang.String r2 = "10"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x033c
            r0 = 14
            r1[r5] = r0
            goto L_0x0119
        L_0x033c:
            java.lang.String r2 = "11"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x034a
            r0 = 18
            r1[r5] = r0
            goto L_0x0119
        L_0x034a:
            java.lang.String r2 = "12"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0358
            r0 = 13
            r1[r5] = r0
            goto L_0x0119
        L_0x0358:
            java.lang.String r2 = "13"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0366
            r0 = 19
            r1[r5] = r0
            goto L_0x0119
        L_0x0366:
            java.lang.String r2 = "14"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0374
            r0 = 15
            r1[r5] = r0
            goto L_0x0119
        L_0x0374:
            java.lang.String r2 = "15"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0382
            r0 = 11
            r1[r5] = r0
            goto L_0x0119
        L_0x0382:
            java.lang.String r2 = "16"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0390
            r0 = 10
            r1[r5] = r0
            goto L_0x0119
        L_0x0390:
            java.lang.String r2 = "17"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x039e
            r0 = 17
            r1[r5] = r0
            goto L_0x0119
        L_0x039e:
            java.lang.String r2 = "18"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x03ac
            r0 = 16
            r1[r5] = r0
            goto L_0x0119
        L_0x03ac:
            java.lang.String r2 = "19"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x03ba
            r0 = 20
            r1[r5] = r0
            goto L_0x0119
        L_0x03ba:
            java.lang.String r2 = "20"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x03c8
            r0 = 29
            r1[r5] = r0
            goto L_0x0119
        L_0x03c8:
            java.lang.String r2 = "21"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x03d6
            r0 = 27
            r1[r5] = r0
            goto L_0x0119
        L_0x03d6:
            java.lang.String r2 = "22"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x03e4
            r0 = 24
            r1[r5] = r0
            goto L_0x0119
        L_0x03e4:
            java.lang.String r2 = "23"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x03f2
            r0 = 25
            r1[r5] = r0
            goto L_0x0119
        L_0x03f2:
            java.lang.String r2 = "24"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0400
            r0 = 26
            r1[r5] = r0
            goto L_0x0119
        L_0x0400:
            java.lang.String r2 = "25"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x040e
            r0 = 30
            r1[r5] = r0
            goto L_0x0119
        L_0x040e:
            java.lang.String r2 = "26"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x041c
            r0 = 21
            r1[r5] = r0
            goto L_0x0119
        L_0x041c:
            java.lang.String r2 = "27"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x042a
            r0 = 22
            r1[r5] = r0
            goto L_0x0119
        L_0x042a:
            java.lang.String r2 = "28"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0438
            r0 = 23
            r1[r5] = r0
            goto L_0x0119
        L_0x0438:
            java.lang.String r2 = "29"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0446
            r0 = 28
            r1[r5] = r0
            goto L_0x0119
        L_0x0446:
            java.lang.String r2 = "30"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0454
            r0 = 31
            r1[r5] = r0
            goto L_0x0119
        L_0x0454:
            java.lang.String r2 = "31"
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x0119
            r0 = 4
            r1[r5] = r0
            goto L_0x0119
        L_0x0461:
            java.lang.String r4 = "1"
            boolean r4 = r3.endsWith(r4)
            if (r4 == 0) goto L_0x0732
            if (r2 < r7) goto L_0x0119
            java.lang.String r3 = r0.substring(r9, r7)
            java.lang.String r4 = "0"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x0493
            r3 = 24
            r1[r6] = r3
        L_0x047b:
            r3 = 13
            if (r2 < r3) goto L_0x0119
            r2 = 10
            r3 = 13
            java.lang.String r0 = r0.substring(r2, r3)
            java.lang.String r2 = "010"
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x04c7
            r1[r5] = r5
            goto L_0x0119
        L_0x0493:
            java.lang.String r4 = "1"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x04a0
            r3 = 15
            r1[r6] = r3
            goto L_0x047b
        L_0x04a0:
            java.lang.String r4 = "2"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x04ad
            r3 = 16
            r1[r6] = r3
            goto L_0x047b
        L_0x04ad:
            java.lang.String r4 = "5"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x04ba
            r3 = 19
            r1[r6] = r3
            goto L_0x047b
        L_0x04ba:
            java.lang.String r4 = "6"
            boolean r3 = r4.equals(r3)
            if (r3 == 0) goto L_0x047b
            r3 = 20
            r1[r6] = r3
            goto L_0x047b
        L_0x04c7:
            java.lang.String r2 = "022"
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x04d4
            r0 = 3
            r1[r5] = r0
            goto L_0x0119
        L_0x04d4:
            java.lang.String r2 = "31"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x04e4
            java.lang.String r2 = "33"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x04e8
        L_0x04e4:
            r1[r5] = r7
            goto L_0x0119
        L_0x04e8:
            java.lang.String r2 = "35"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x04f8
            java.lang.String r2 = "34"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x04fe
        L_0x04f8:
            r0 = 12
            r1[r5] = r0
            goto L_0x0119
        L_0x04fe:
            java.lang.String r2 = "47"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x050e
            java.lang.String r2 = "48"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0512
        L_0x050e:
            r1[r5] = r9
            goto L_0x0119
        L_0x0512:
            java.lang.String r2 = "024"
            boolean r2 = r0.equals(r2)
            if (r2 != 0) goto L_0x052a
            java.lang.String r2 = "41"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x052a
            java.lang.String r2 = "42"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x052f
        L_0x052a:
            r0 = 7
            r1[r5] = r0
            goto L_0x0119
        L_0x052f:
            java.lang.String r2 = "43"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x053b
            r1[r5] = r8
            goto L_0x0119
        L_0x053b:
            java.lang.String r2 = "45"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x054b
            java.lang.String r2 = "46"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0550
        L_0x054b:
            r0 = 5
            r1[r5] = r0
            goto L_0x0119
        L_0x0550:
            java.lang.String r2 = "021"
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x055d
            r0 = 2
            r1[r5] = r0
            goto L_0x0119
        L_0x055d:
            java.lang.String r2 = "025"
            boolean r2 = r0.equals(r2)
            if (r2 != 0) goto L_0x0575
            java.lang.String r2 = "51"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0575
            java.lang.String r2 = "52"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x057b
        L_0x0575:
            r0 = 14
            r1[r5] = r0
            goto L_0x0119
        L_0x057b:
            java.lang.String r2 = "57"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0589
            r0 = 18
            r1[r5] = r0
            goto L_0x0119
        L_0x0589:
            java.lang.String r2 = "55"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0599
            java.lang.String r2 = "56"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x059f
        L_0x0599:
            r0 = 13
            r1[r5] = r0
            goto L_0x0119
        L_0x059f:
            java.lang.String r2 = "59"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x05ad
            r0 = 19
            r1[r5] = r0
            goto L_0x0119
        L_0x05ad:
            java.lang.String r2 = "79"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x05bd
            java.lang.String r2 = "70"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x05c3
        L_0x05bd:
            r0 = 15
            r1[r5] = r0
            goto L_0x0119
        L_0x05c3:
            java.lang.String r2 = "53"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x05db
            java.lang.String r2 = "54"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x05db
            java.lang.String r2 = "63"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x05e1
        L_0x05db:
            r0 = 11
            r1[r5] = r0
            goto L_0x0119
        L_0x05e1:
            java.lang.String r2 = "37"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x05f1
            java.lang.String r2 = "39"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x05f7
        L_0x05f1:
            r0 = 10
            r1[r5] = r0
            goto L_0x0119
        L_0x05f7:
            java.lang.String r2 = "027"
            boolean r2 = r0.equals(r2)
            if (r2 != 0) goto L_0x060f
            java.lang.String r2 = "71"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x060f
            java.lang.String r2 = "72"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0615
        L_0x060f:
            r0 = 17
            r1[r5] = r0
            goto L_0x0119
        L_0x0615:
            java.lang.String r2 = "73"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0625
            java.lang.String r2 = "74"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x062b
        L_0x0625:
            r0 = 16
            r1[r5] = r0
            goto L_0x0119
        L_0x062b:
            java.lang.String r2 = "020"
            boolean r2 = r0.equals(r2)
            if (r2 != 0) goto L_0x064b
            java.lang.String r2 = "75"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x064b
            java.lang.String r2 = "76"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x064b
            java.lang.String r2 = "66"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0651
        L_0x064b:
            r0 = 20
            r1[r5] = r0
            goto L_0x0119
        L_0x0651:
            java.lang.String r2 = "77"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x065f
            r0 = 29
            r1[r5] = r0
            goto L_0x0119
        L_0x065f:
            java.lang.String r2 = "898"
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x066d
            r0 = 27
            r1[r5] = r0
            goto L_0x0119
        L_0x066d:
            java.lang.String r2 = "028"
            boolean r2 = r0.equals(r2)
            if (r2 != 0) goto L_0x068d
            java.lang.String r2 = "81"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x068d
            java.lang.String r2 = "82"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x068d
            java.lang.String r2 = "83"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0693
        L_0x068d:
            r0 = 24
            r1[r5] = r0
            goto L_0x0119
        L_0x0693:
            java.lang.String r2 = "85"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0119
            java.lang.String r2 = "87"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x06b3
            java.lang.String r2 = "88"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x06b3
            java.lang.String r2 = "69"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x06b9
        L_0x06b3:
            r0 = 26
            r1[r5] = r0
            goto L_0x0119
        L_0x06b9:
            java.lang.String r2 = "89"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x06c7
            r0 = 30
            r1[r5] = r0
            goto L_0x0119
        L_0x06c7:
            java.lang.String r2 = "029"
            boolean r2 = r0.equals(r2)
            if (r2 != 0) goto L_0x06d7
            java.lang.String r2 = "91"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x06dd
        L_0x06d7:
            r0 = 21
            r1[r5] = r0
            goto L_0x0119
        L_0x06dd:
            java.lang.String r2 = "93"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x06ed
            java.lang.String r2 = "94"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x06f3
        L_0x06ed:
            r0 = 22
            r1[r5] = r0
            goto L_0x0119
        L_0x06f3:
            java.lang.String r2 = "97"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0701
            r0 = 23
            r1[r5] = r0
            goto L_0x0119
        L_0x0701:
            java.lang.String r2 = "95"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x070f
            r0 = 28
            r1[r5] = r0
            goto L_0x0119
        L_0x070f:
            java.lang.String r2 = "90"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x071f
            java.lang.String r2 = "99"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0725
        L_0x071f:
            r0 = 31
            r1[r5] = r0
            goto L_0x0119
        L_0x0725:
            java.lang.String r2 = "023"
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x0119
            r0 = 4
            r1[r5] = r0
            goto L_0x0119
        L_0x0732:
            java.lang.String r4 = "3"
            boolean r3 = r3.endsWith(r4)
            if (r3 == 0) goto L_0x0119
            if (r2 < r7) goto L_0x0119
            java.lang.String r3 = r0.substring(r9, r7)
            java.lang.String r4 = "3"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x0764
            r3 = 17
            r1[r6] = r3
        L_0x074c:
            r3 = 13
            if (r2 < r3) goto L_0x0119
            r2 = 10
            r3 = 13
            java.lang.String r0 = r0.substring(r2, r3)
            java.lang.String r2 = "010"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0798
            r1[r5] = r5
            goto L_0x0119
        L_0x0764:
            java.lang.String r4 = "4"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x0771
            r3 = 18
            r1[r6] = r3
            goto L_0x074c
        L_0x0771:
            java.lang.String r4 = "7"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x077e
            r3 = 21
            r1[r6] = r3
            goto L_0x074c
        L_0x077e:
            java.lang.String r4 = "8"
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x078b
            r3 = 22
            r1[r6] = r3
            goto L_0x074c
        L_0x078b:
            java.lang.String r4 = "9"
            boolean r3 = r4.equals(r3)
            if (r3 == 0) goto L_0x074c
            r3 = 23
            r1[r6] = r3
            goto L_0x074c
        L_0x0798:
            java.lang.String r2 = "022"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x07a5
            r0 = 3
            r1[r5] = r0
            goto L_0x0119
        L_0x07a5:
            java.lang.String r2 = "31"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x07b5
            java.lang.String r2 = "33"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x07b9
        L_0x07b5:
            r1[r5] = r7
            goto L_0x0119
        L_0x07b9:
            java.lang.String r2 = "35"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x07c9
            java.lang.String r2 = "34"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x07cf
        L_0x07c9:
            r0 = 12
            r1[r5] = r0
            goto L_0x0119
        L_0x07cf:
            java.lang.String r2 = "47"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x07df
            java.lang.String r2 = "48"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x07e3
        L_0x07df:
            r1[r5] = r9
            goto L_0x0119
        L_0x07e3:
            java.lang.String r2 = "024"
            boolean r2 = r2.equals(r0)
            if (r2 != 0) goto L_0x07fb
            java.lang.String r2 = "41"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x07fb
            java.lang.String r2 = "42"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0800
        L_0x07fb:
            r0 = 7
            r1[r5] = r0
            goto L_0x0119
        L_0x0800:
            java.lang.String r2 = "43"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x080c
            r1[r5] = r8
            goto L_0x0119
        L_0x080c:
            java.lang.String r2 = "45"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x081c
            java.lang.String r2 = "46"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0821
        L_0x081c:
            r0 = 5
            r1[r5] = r0
            goto L_0x0119
        L_0x0821:
            java.lang.String r2 = "021"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x082e
            r0 = 2
            r1[r5] = r0
            goto L_0x0119
        L_0x082e:
            java.lang.String r2 = "025"
            boolean r2 = r0.equals(r2)
            if (r2 != 0) goto L_0x0846
            java.lang.String r2 = "51"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0846
            java.lang.String r2 = "52"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x084c
        L_0x0846:
            r0 = 14
            r1[r5] = r0
            goto L_0x0119
        L_0x084c:
            java.lang.String r2 = "57"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x085c
            java.lang.String r2 = "58"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0862
        L_0x085c:
            r0 = 18
            r1[r5] = r0
            goto L_0x0119
        L_0x0862:
            java.lang.String r2 = "55"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0872
            java.lang.String r2 = "56"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0878
        L_0x0872:
            r0 = 13
            r1[r5] = r0
            goto L_0x0119
        L_0x0878:
            java.lang.String r2 = "59"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0886
            r0 = 19
            r1[r5] = r0
            goto L_0x0119
        L_0x0886:
            java.lang.String r2 = "79"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0896
            java.lang.String r2 = "70"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x089c
        L_0x0896:
            r0 = 15
            r1[r5] = r0
            goto L_0x0119
        L_0x089c:
            java.lang.String r2 = "53"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x08b4
            java.lang.String r2 = "54"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x08b4
            java.lang.String r2 = "63"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x08ba
        L_0x08b4:
            r0 = 11
            r1[r5] = r0
            goto L_0x0119
        L_0x08ba:
            java.lang.String r2 = "37"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x08ca
            java.lang.String r2 = "39"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x08d0
        L_0x08ca:
            r0 = 10
            r1[r5] = r0
            goto L_0x0119
        L_0x08d0:
            java.lang.String r2 = "027"
            boolean r2 = r2.equals(r0)
            if (r2 != 0) goto L_0x08e8
            java.lang.String r2 = "71"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x08e8
            java.lang.String r2 = "72"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x08ee
        L_0x08e8:
            r0 = 17
            r1[r5] = r0
            goto L_0x0119
        L_0x08ee:
            java.lang.String r2 = "73"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x08fe
            java.lang.String r2 = "74"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0904
        L_0x08fe:
            r0 = 16
            r1[r5] = r0
            goto L_0x0119
        L_0x0904:
            java.lang.String r2 = "020"
            boolean r2 = r2.equals(r0)
            if (r2 != 0) goto L_0x0924
            java.lang.String r2 = "75"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0924
            java.lang.String r2 = "76"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0924
            java.lang.String r2 = "66"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x092a
        L_0x0924:
            r0 = 20
            r1[r5] = r0
            goto L_0x0119
        L_0x092a:
            java.lang.String r2 = "77"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0938
            r0 = 29
            r1[r5] = r0
            goto L_0x0119
        L_0x0938:
            java.lang.String r2 = "898"
            boolean r2 = r2.equals(r0)
            if (r2 == 0) goto L_0x0946
            r0 = 27
            r1[r5] = r0
            goto L_0x0119
        L_0x0946:
            java.lang.String r2 = "028"
            boolean r2 = r2.equals(r0)
            if (r2 != 0) goto L_0x0966
            java.lang.String r2 = "81"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0966
            java.lang.String r2 = "82"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0966
            java.lang.String r2 = "83"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x096c
        L_0x0966:
            r0 = 24
            r1[r5] = r0
            goto L_0x0119
        L_0x096c:
            java.lang.String r2 = "85"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x097a
            r0 = 25
            r1[r5] = r0
            goto L_0x0119
        L_0x097a:
            java.lang.String r2 = "87"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0992
            java.lang.String r2 = "88"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x0992
            java.lang.String r2 = "69"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0998
        L_0x0992:
            r0 = 26
            r1[r5] = r0
            goto L_0x0119
        L_0x0998:
            java.lang.String r2 = "89"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x09a6
            r0 = 30
            r1[r5] = r0
            goto L_0x0119
        L_0x09a6:
            java.lang.String r2 = "029"
            boolean r2 = r2.equals(r0)
            if (r2 != 0) goto L_0x09b6
            java.lang.String r2 = "91"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x09bc
        L_0x09b6:
            r0 = 21
            r1[r5] = r0
            goto L_0x0119
        L_0x09bc:
            java.lang.String r2 = "93"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x09cc
            java.lang.String r2 = "94"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x09d2
        L_0x09cc:
            r0 = 22
            r1[r5] = r0
            goto L_0x0119
        L_0x09d2:
            java.lang.String r2 = "97"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x09e0
            r0 = 23
            r1[r5] = r0
            goto L_0x0119
        L_0x09e0:
            java.lang.String r2 = "95"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x09ee
            r0 = 28
            r1[r5] = r0
            goto L_0x0119
        L_0x09ee:
            java.lang.String r2 = "90"
            boolean r2 = r0.startsWith(r2)
            if (r2 != 0) goto L_0x09fe
            java.lang.String r2 = "99"
            boolean r2 = r0.startsWith(r2)
            if (r2 == 0) goto L_0x0a04
        L_0x09fe:
            r0 = 31
            r1[r5] = r0
            goto L_0x0119
        L_0x0a04:
            java.lang.String r2 = "023"
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x0119
            r0 = 4
            r1[r5] = r0
            goto L_0x0119
        L_0x0a11:
            r0 = move-exception
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = "CONFIGURATION ERROR:  Incorrect Adwo PID.  Should 32 [a-z,0-9] characters:  "
            r0.<init>(r1)
            byte[] r1 = com.adwo.adsdk.FSAdUtil.c
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            java.lang.String r1 = "Adwo SDK"
            android.util.Log.e(r1, r0)
            goto L_0x015d
        L_0x0a2a:
            r0 = move-exception
            goto L_0x00be
        L_0x0a2d:
            r0 = move-exception
            goto L_0x00be
        L_0x0a30:
            r0 = move-exception
            goto L_0x00be
        L_0x0a33:
            r0 = move-exception
            goto L_0x00be
        L_0x0a36:
            r0 = move-exception
            goto L_0x00be
        L_0x0a39:
            r0 = move-exception
            goto L_0x0089
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.FSAdUtil.loadAd(android.app.Activity, java.lang.String, boolean):void");
    }

    private byte[] b() {
        String line1Number;
        if (!(this.a == null || (line1Number = this.a.getLine1Number()) == null)) {
            try {
                D = line1Number.getBytes("UTF-8");
                return line1Number.getBytes("UTF-8");
            } catch (UnsupportedEncodingException e2) {
            }
        }
        return null;
    }

    private byte[] a(Context context) {
        if (context.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == -1) {
            Log.e("Adwo SDK", "Cannot request an ad without READ_PHONE_STATE permissions!  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"android.permission.READ_PHONE_STATE\" />");
        }
        if (this.e == null && this.a != null) {
            this.e = this.a.getDeviceId();
        }
        if (this.e == null) {
            this.e = Settings.System.getString(context.getContentResolver(), "android_id");
        }
        if (this.e == null) {
            this.e = "00000000";
        }
        try {
            if (this.e != null) {
                d = this.e.getBytes("UTF-8");
                return this.e.getBytes("UTF-8");
            }
        } catch (UnsupportedEncodingException e2) {
        }
        return "00000000".getBytes();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:101:0x024d, code lost:
        r35 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x0257, code lost:
        r35 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x0258, code lost:
        r36 = null;
        r3 = null;
        r37 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x0262, code lost:
        r36 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x0263, code lost:
        r33 = r36;
        r36 = r35;
        r35 = r33;
        r3 = r37;
        r37 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x0271, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x0272, code lost:
        r33 = r3;
        r3 = r37;
        r37 = r36;
        r36 = r35;
        r35 = r33;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0040, code lost:
        r36 = com.adwo.adsdk.R.a(com.adwo.adsdk.FSAdUtil.v, com.adwo.adsdk.FSAdUtil.w, com.adwo.adsdk.FSAdUtil.r, r36, com.adwo.adsdk.FSAdUtil.u, false, com.adwo.adsdk.FSAdUtil.c, r37, 0, com.adwo.adsdk.FSAdUtil.d, com.adwo.adsdk.FSAdUtil.p, com.adwo.adsdk.FSAdUtil.q, 0, 0, (short) com.adwo.adsdk.FSAdUtil.F, com.adwo.adsdk.FSAdUtil.n, com.adwo.adsdk.FSAdUtil.o, com.adwo.adsdk.FSAdUtil.D, com.adwo.adsdk.FSAdUtil.t, false, 0.0d, 0.0d, (byte) 0, r28, r29, com.adwo.adsdk.FSAdUtil.s);
        com.adwo.adsdk.FSAdUtil.k = com.adwo.adsdk.FSAdUtil.g;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x027e, code lost:
        r35 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x0281, code lost:
        r35 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x0282, code lost:
        r36 = null;
        r4 = null;
        r37 = null;
        r3 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x028e, code lost:
        r36 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x028f, code lost:
        r4 = null;
        r33 = r35;
        r35 = r36;
        r36 = r33;
        r34 = r37;
        r37 = null;
        r3 = r34;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x029f, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x02a0, code lost:
        r33 = r4;
        r4 = r3;
        r3 = r37;
        r37 = r36;
        r36 = r35;
        r35 = r33;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x02ad, code lost:
        r35 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x02b1, code lost:
        r35 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0084, code lost:
        if (com.adwo.adsdk.FSAdUtil.k != com.adwo.adsdk.FSAdUtil.g) goto L_0x00ce;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0086, code lost:
        r35 = (java.net.HttpURLConnection) new java.net.URL(new java.lang.String(com.adwo.adsdk.S.d, "UTF-8")).openConnection();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        r35.setConnectTimeout(com.adwo.adsdk.S.a);
        r35.setReadTimeout(com.adwo.adsdk.S.a);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x00ad, code lost:
        if (r35 != null) goto L_0x01bd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x00af, code lost:
        if (r35 == null) goto L_0x00b4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        r35.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00d5, code lost:
        if (com.adwo.adsdk.FSAdUtil.k == com.adwo.adsdk.FSAdUtil.h) goto L_0x00e0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00de, code lost:
        if (com.adwo.adsdk.FSAdUtil.k != com.adwo.adsdk.FSAdUtil.i) goto L_0x014d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00e0, code lost:
        r35 = (java.net.HttpURLConnection) new java.net.URL(new java.lang.String(com.adwo.adsdk.S.h, "utf-8")).openConnection();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        r35.setRequestProperty("X-Online-Host", new java.lang.String(com.adwo.adsdk.S.f, "utf-8"));
        r35.setConnectTimeout(com.adwo.adsdk.S.a * 2);
        r35.setReadTimeout(com.adwo.adsdk.S.a * 2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x011e, code lost:
        r36 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x011f, code lost:
        r4 = null;
        r33 = r35;
        r35 = r36;
        r36 = r33;
        r37 = null;
        r3 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:?, code lost:
        r35.printStackTrace();
        android.util.Log.w("Adwo SDK", "Could not get ad from Adwo servers,Network Error!");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x013a, code lost:
        if (r3 != null) goto L_0x013c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x013f, code lost:
        if (r37 != null) goto L_0x0141;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0141, code lost:
        r37.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0144, code lost:
        if (r36 != null) goto L_0x0146;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0146, code lost:
        r36.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0149, code lost:
        r35 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0154, code lost:
        if (com.adwo.adsdk.FSAdUtil.k != com.adwo.adsdk.FSAdUtil.j) goto L_0x02b1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0156, code lost:
        r35 = (java.net.HttpURLConnection) new java.net.URL(new java.lang.String(com.adwo.adsdk.S.d, "UTF-8")).openConnection(new java.net.Proxy(java.net.Proxy.Type.HTTP, new java.net.InetSocketAddress(new java.lang.String(com.adwo.adsdk.S.i, "utf-8"), 80)));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:?, code lost:
        r35.setConnectTimeout(com.adwo.adsdk.S.a * 2);
        r35.setReadTimeout(com.adwo.adsdk.S.a * 2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x019d, code lost:
        r36 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x019e, code lost:
        r33 = r36;
        r36 = r35;
        r35 = r33;
        r3 = null;
        r37 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x01aa, code lost:
        if (r3 != null) goto L_0x01ac;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:?, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x01af, code lost:
        if (r37 != null) goto L_0x01b1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x01b1, code lost:
        r37.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x01b4, code lost:
        if (r36 != null) goto L_0x01b6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x01b6, code lost:
        r36.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:?, code lost:
        throw r35;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:?, code lost:
        r35.setRequestMethod("POST");
        r35.setDoOutput(true);
        r35.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        r35.setRequestProperty("Content-Length", java.lang.Integer.toString(r36.length));
        r37 = r35.getOutputStream();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:?, code lost:
        r37.write(r36);
        r36 = r35.getInputStream();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:?, code lost:
        r3 = new java.io.ByteArrayOutputStream();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x01fc, code lost:
        r4 = r36.read();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x0201, code lost:
        if (r4 != -1) goto L_0x022a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x0203, code lost:
        r3 = r3.toByteArray();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x0208, code lost:
        if (r3.length <= 0) goto L_0x023d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x020a, code lost:
        r3 = com.adwo.adsdk.FSAd.a(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x020e, code lost:
        if (r3 == null) goto L_0x0217;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:?, code lost:
        com.adwo.adsdk.FSAdUtil.s = (short) (com.adwo.adsdk.FSAdUtil.s + 1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x0217, code lost:
        if (r37 == null) goto L_0x021c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:?, code lost:
        r37.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x021c, code lost:
        if (r36 == null) goto L_0x0221;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x021e, code lost:
        r36.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:89:0x0221, code lost:
        if (r35 == null) goto L_0x024d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x0223, code lost:
        r35.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x0226, code lost:
        r35 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:?, code lost:
        r3.write(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x022e, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x022f, code lost:
        r4 = null;
        r33 = r35;
        r35 = r3;
        r3 = r37;
        r37 = r36;
        r36 = r33;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x023d, code lost:
        android.util.Log.w("Adwo SDK", "Could not get ad from Adwo servers.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x0244, code lost:
        r3 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x0248, code lost:
        r35 = r4;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:108:0x0271 A[ExcHandler: all (r3v14 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:74:0x01f7] */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x02ad  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x013c A[SYNTHETIC, Splitter:B:41:0x013c] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0141 A[Catch:{ Exception -> 0x0247 }] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0146 A[Catch:{ Exception -> 0x0247 }] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x01ac A[SYNTHETIC, Splitter:B:57:0x01ac] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x01b1 A[Catch:{ Exception -> 0x0254 }] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x01b6 A[Catch:{ Exception -> 0x0254 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static synchronized com.adwo.adsdk.FSAd a(android.content.Context r35, byte r36, byte r37) {
        /*
            java.lang.Class<com.adwo.adsdk.FSAdUtil> r31 = com.adwo.adsdk.FSAdUtil.class
            monitor-enter(r31)
            java.lang.String r3 = "android.permission.INTERNET"
            r0 = r35
            r1 = r3
            int r35 = r0.checkCallingOrSelfPermission(r1)     // Catch:{ all -> 0x01ba }
            r3 = -1
            r0 = r35
            r1 = r3
            if (r0 != r1) goto L_0x001c
            java.lang.String r35 = "Cannot request an ad without INTERNET permissions!  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"android.permission.INTERNET\" />"
            java.lang.String r3 = "Adwo SDK"
            r0 = r3
            r1 = r35
            android.util.Log.e(r0, r1)     // Catch:{ all -> 0x01ba }
        L_0x001c:
            r32 = 0
            r8 = 0
            java.util.Set r35 = com.adwo.adsdk.FSAdUtil.y     // Catch:{ all -> 0x01ba }
            int r35 = r35.size()     // Catch:{ all -> 0x01ba }
            r0 = r35
            short r0 = (short) r0     // Catch:{ all -> 0x01ba }
            r28 = r0
            r0 = r28
            java.lang.Integer[] r0 = new java.lang.Integer[r0]     // Catch:{ all -> 0x01ba }
            r29 = r0
            r35 = 0
            java.util.Set r3 = com.adwo.adsdk.FSAdUtil.y     // Catch:{ all -> 0x01ba }
            java.util.Iterator r3 = r3.iterator()     // Catch:{ all -> 0x01ba }
            r4 = r35
        L_0x003a:
            boolean r35 = r3.hasNext()     // Catch:{ all -> 0x01ba }
            if (r35 != 0) goto L_0x00b8
            byte r3 = com.adwo.adsdk.FSAdUtil.v     // Catch:{ all -> 0x01ba }
            byte r4 = com.adwo.adsdk.FSAdUtil.w     // Catch:{ all -> 0x01ba }
            byte r5 = com.adwo.adsdk.FSAdUtil.r     // Catch:{ all -> 0x01ba }
            byte r7 = com.adwo.adsdk.FSAdUtil.u     // Catch:{ all -> 0x01ba }
            byte[] r9 = com.adwo.adsdk.FSAdUtil.c     // Catch:{ all -> 0x01ba }
            r11 = 0
            byte[] r12 = com.adwo.adsdk.FSAdUtil.d     // Catch:{ all -> 0x01ba }
            byte[] r13 = com.adwo.adsdk.FSAdUtil.p     // Catch:{ all -> 0x01ba }
            byte[] r14 = com.adwo.adsdk.FSAdUtil.q     // Catch:{ all -> 0x01ba }
            r15 = 0
            r16 = 0
            int r35 = com.adwo.adsdk.FSAdUtil.F     // Catch:{ all -> 0x01ba }
            r0 = r35
            short r0 = (short) r0     // Catch:{ all -> 0x01ba }
            r17 = r0
            byte r18 = com.adwo.adsdk.FSAdUtil.n     // Catch:{ all -> 0x01ba }
            byte r19 = com.adwo.adsdk.FSAdUtil.o     // Catch:{ all -> 0x01ba }
            byte[] r20 = com.adwo.adsdk.FSAdUtil.D     // Catch:{ all -> 0x01ba }
            byte[] r21 = com.adwo.adsdk.FSAdUtil.t     // Catch:{ all -> 0x01ba }
            r22 = 0
            r23 = 0
            r25 = 0
            r27 = 0
            short r30 = com.adwo.adsdk.FSAdUtil.s     // Catch:{ all -> 0x01ba }
            r6 = r36
            r10 = r37
            byte[] r36 = com.adwo.adsdk.R.a(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r25, r27, r28, r29, r30)     // Catch:{ all -> 0x01ba }
            r37 = 0
            r3 = 0
            r4 = 0
            int r35 = com.adwo.adsdk.FSAdUtil.g     // Catch:{ all -> 0x01ba }
            com.adwo.adsdk.FSAdUtil.k = r35     // Catch:{ all -> 0x01ba }
            int r35 = com.adwo.adsdk.FSAdUtil.k     // Catch:{ Exception -> 0x0281, all -> 0x0257 }
            int r5 = com.adwo.adsdk.FSAdUtil.g     // Catch:{ Exception -> 0x0281, all -> 0x0257 }
            r0 = r35
            r1 = r5
            if (r0 != r1) goto L_0x00ce
            java.net.URL r35 = new java.net.URL     // Catch:{ Exception -> 0x0281, all -> 0x0257 }
            java.lang.String r5 = new java.lang.String     // Catch:{ Exception -> 0x0281, all -> 0x0257 }
            byte[] r6 = com.adwo.adsdk.S.d     // Catch:{ Exception -> 0x0281, all -> 0x0257 }
            java.lang.String r7 = "UTF-8"
            r5.<init>(r6, r7)     // Catch:{ Exception -> 0x0281, all -> 0x0257 }
            r0 = r35
            r1 = r5
            r0.<init>(r1)     // Catch:{ Exception -> 0x0281, all -> 0x0257 }
            java.net.URLConnection r35 = r35.openConnection()     // Catch:{ Exception -> 0x0281, all -> 0x0257 }
            java.net.HttpURLConnection r35 = (java.net.HttpURLConnection) r35     // Catch:{ Exception -> 0x0281, all -> 0x0257 }
            int r4 = com.adwo.adsdk.S.a     // Catch:{ Exception -> 0x011e, all -> 0x019d }
            r0 = r35
            r1 = r4
            r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x011e, all -> 0x019d }
            int r4 = com.adwo.adsdk.S.a     // Catch:{ Exception -> 0x011e, all -> 0x019d }
            r0 = r35
            r1 = r4
            r0.setReadTimeout(r1)     // Catch:{ Exception -> 0x011e, all -> 0x019d }
        L_0x00ad:
            if (r35 != 0) goto L_0x01bd
            if (r35 == 0) goto L_0x00b4
            r35.disconnect()     // Catch:{ Exception -> 0x0251 }
        L_0x00b4:
            r35 = 0
        L_0x00b6:
            monitor-exit(r31)
            return r35
        L_0x00b8:
            java.lang.Object r35 = r3.next()     // Catch:{ all -> 0x01ba }
            java.lang.Integer r35 = (java.lang.Integer) r35     // Catch:{ all -> 0x01ba }
            int r35 = r35.intValue()     // Catch:{ all -> 0x01ba }
            java.lang.Integer r35 = java.lang.Integer.valueOf(r35)     // Catch:{ all -> 0x01ba }
            r29[r4] = r35     // Catch:{ all -> 0x01ba }
            int r35 = r4 + 1
            r4 = r35
            goto L_0x003a
        L_0x00ce:
            int r35 = com.adwo.adsdk.FSAdUtil.k     // Catch:{ Exception -> 0x0281, all -> 0x0257 }
            int r5 = com.adwo.adsdk.FSAdUtil.h     // Catch:{ Exception -> 0x0281, all -> 0x0257 }
            r0 = r35
            r1 = r5
            if (r0 == r1) goto L_0x00e0
            int r35 = com.adwo.adsdk.FSAdUtil.k     // Catch:{ Exception -> 0x0281, all -> 0x0257 }
            int r5 = com.adwo.adsdk.FSAdUtil.i     // Catch:{ Exception -> 0x0281, all -> 0x0257 }
            r0 = r35
            r1 = r5
            if (r0 != r1) goto L_0x014d
        L_0x00e0:
            java.net.URL r35 = new java.net.URL     // Catch:{ Exception -> 0x0281, all -> 0x0257 }
            java.lang.String r5 = new java.lang.String     // Catch:{ Exception -> 0x0281, all -> 0x0257 }
            byte[] r6 = com.adwo.adsdk.S.h     // Catch:{ Exception -> 0x0281, all -> 0x0257 }
            java.lang.String r7 = "utf-8"
            r5.<init>(r6, r7)     // Catch:{ Exception -> 0x0281, all -> 0x0257 }
            r0 = r35
            r1 = r5
            r0.<init>(r1)     // Catch:{ Exception -> 0x0281, all -> 0x0257 }
            java.net.URLConnection r35 = r35.openConnection()     // Catch:{ Exception -> 0x0281, all -> 0x0257 }
            java.net.HttpURLConnection r35 = (java.net.HttpURLConnection) r35     // Catch:{ Exception -> 0x0281, all -> 0x0257 }
            java.lang.String r4 = "X-Online-Host"
            java.lang.String r5 = new java.lang.String     // Catch:{ Exception -> 0x011e, all -> 0x019d }
            byte[] r6 = com.adwo.adsdk.S.f     // Catch:{ Exception -> 0x011e, all -> 0x019d }
            java.lang.String r7 = "utf-8"
            r5.<init>(r6, r7)     // Catch:{ Exception -> 0x011e, all -> 0x019d }
            r0 = r35
            r1 = r4
            r2 = r5
            r0.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x011e, all -> 0x019d }
            int r4 = com.adwo.adsdk.S.a     // Catch:{ Exception -> 0x011e, all -> 0x019d }
            int r4 = r4 * 2
            r0 = r35
            r1 = r4
            r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x011e, all -> 0x019d }
            int r4 = com.adwo.adsdk.S.a     // Catch:{ Exception -> 0x011e, all -> 0x019d }
            int r4 = r4 * 2
            r0 = r35
            r1 = r4
            r0.setReadTimeout(r1)     // Catch:{ Exception -> 0x011e, all -> 0x019d }
            goto L_0x00ad
        L_0x011e:
            r36 = move-exception
            r4 = r32
            r33 = r35
            r35 = r36
            r36 = r33
            r34 = r37
            r37 = r3
            r3 = r34
        L_0x012d:
            r35.printStackTrace()     // Catch:{ all -> 0x027e }
            java.lang.String r35 = "Adwo SDK"
            java.lang.String r5 = "Could not get ad from Adwo servers,Network Error!"
            r0 = r35
            r1 = r5
            android.util.Log.w(r0, r1)     // Catch:{ all -> 0x027e }
            if (r3 == 0) goto L_0x013f
            r3.close()     // Catch:{ Exception -> 0x0247 }
        L_0x013f:
            if (r37 == 0) goto L_0x0144
            r37.close()     // Catch:{ Exception -> 0x0247 }
        L_0x0144:
            if (r36 == 0) goto L_0x02ad
            r36.disconnect()     // Catch:{ Exception -> 0x0247 }
            r35 = r4
            goto L_0x00b6
        L_0x014d:
            int r35 = com.adwo.adsdk.FSAdUtil.k     // Catch:{ Exception -> 0x0281, all -> 0x0257 }
            int r5 = com.adwo.adsdk.FSAdUtil.j     // Catch:{ Exception -> 0x0281, all -> 0x0257 }
            r0 = r35
            r1 = r5
            if (r0 != r1) goto L_0x02b1
            java.net.URL r35 = new java.net.URL     // Catch:{ Exception -> 0x0281, all -> 0x0257 }
            java.lang.String r5 = new java.lang.String     // Catch:{ Exception -> 0x0281, all -> 0x0257 }
            byte[] r6 = com.adwo.adsdk.S.d     // Catch:{ Exception -> 0x0281, all -> 0x0257 }
            java.lang.String r7 = "UTF-8"
            r5.<init>(r6, r7)     // Catch:{ Exception -> 0x0281, all -> 0x0257 }
            r0 = r35
            r1 = r5
            r0.<init>(r1)     // Catch:{ Exception -> 0x0281, all -> 0x0257 }
            java.net.Proxy r5 = new java.net.Proxy     // Catch:{ Exception -> 0x0281, all -> 0x0257 }
            java.net.Proxy$Type r6 = java.net.Proxy.Type.HTTP     // Catch:{ Exception -> 0x0281, all -> 0x0257 }
            java.net.InetSocketAddress r7 = new java.net.InetSocketAddress     // Catch:{ Exception -> 0x0281, all -> 0x0257 }
            java.lang.String r8 = new java.lang.String     // Catch:{ Exception -> 0x0281, all -> 0x0257 }
            byte[] r9 = com.adwo.adsdk.S.i     // Catch:{ Exception -> 0x0281, all -> 0x0257 }
            java.lang.String r10 = "utf-8"
            r8.<init>(r9, r10)     // Catch:{ Exception -> 0x0281, all -> 0x0257 }
            r9 = 80
            r7.<init>(r8, r9)     // Catch:{ Exception -> 0x0281, all -> 0x0257 }
            r5.<init>(r6, r7)     // Catch:{ Exception -> 0x0281, all -> 0x0257 }
            r0 = r35
            r1 = r5
            java.net.URLConnection r35 = r0.openConnection(r1)     // Catch:{ Exception -> 0x0281, all -> 0x0257 }
            java.net.HttpURLConnection r35 = (java.net.HttpURLConnection) r35     // Catch:{ Exception -> 0x0281, all -> 0x0257 }
            int r4 = com.adwo.adsdk.S.a     // Catch:{ Exception -> 0x011e, all -> 0x019d }
            int r4 = r4 * 2
            r0 = r35
            r1 = r4
            r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x011e, all -> 0x019d }
            int r4 = com.adwo.adsdk.S.a     // Catch:{ Exception -> 0x011e, all -> 0x019d }
            int r4 = r4 * 2
            r0 = r35
            r1 = r4
            r0.setReadTimeout(r1)     // Catch:{ Exception -> 0x011e, all -> 0x019d }
            goto L_0x00ad
        L_0x019d:
            r36 = move-exception
            r33 = r36
            r36 = r35
            r35 = r33
            r34 = r3
            r3 = r37
            r37 = r34
        L_0x01aa:
            if (r3 == 0) goto L_0x01af
            r3.close()     // Catch:{ Exception -> 0x0254 }
        L_0x01af:
            if (r37 == 0) goto L_0x01b4
            r37.close()     // Catch:{ Exception -> 0x0254 }
        L_0x01b4:
            if (r36 == 0) goto L_0x01b9
            r36.disconnect()     // Catch:{ Exception -> 0x0254 }
        L_0x01b9:
            throw r35     // Catch:{ all -> 0x01ba }
        L_0x01ba:
            r35 = move-exception
            monitor-exit(r31)
            throw r35
        L_0x01bd:
            java.lang.String r4 = "POST"
            r0 = r35
            r1 = r4
            r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x011e, all -> 0x019d }
            r4 = 1
            r0 = r35
            r1 = r4
            r0.setDoOutput(r1)     // Catch:{ Exception -> 0x011e, all -> 0x019d }
            java.lang.String r4 = "Content-Type"
            java.lang.String r5 = "application/x-www-form-urlencoded"
            r0 = r35
            r1 = r4
            r2 = r5
            r0.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x011e, all -> 0x019d }
            java.lang.String r4 = "Content-Length"
            r0 = r36
            int r0 = r0.length     // Catch:{ Exception -> 0x011e, all -> 0x019d }
            r5 = r0
            java.lang.String r5 = java.lang.Integer.toString(r5)     // Catch:{ Exception -> 0x011e, all -> 0x019d }
            r0 = r35
            r1 = r4
            r2 = r5
            r0.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x011e, all -> 0x019d }
            java.io.OutputStream r37 = r35.getOutputStream()     // Catch:{ Exception -> 0x011e, all -> 0x019d }
            r0 = r37
            r1 = r36
            r0.write(r1)     // Catch:{ Exception -> 0x028e, all -> 0x0262 }
            java.io.InputStream r36 = r35.getInputStream()     // Catch:{ Exception -> 0x028e, all -> 0x0262 }
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x022e, all -> 0x0271 }
            r3.<init>()     // Catch:{ Exception -> 0x022e, all -> 0x0271 }
        L_0x01fc:
            int r4 = r36.read()     // Catch:{ Exception -> 0x022e, all -> 0x0271 }
            r5 = -1
            if (r4 != r5) goto L_0x022a
            byte[] r3 = r3.toByteArray()     // Catch:{ Exception -> 0x022e, all -> 0x0271 }
            int r4 = r3.length     // Catch:{ Exception -> 0x022e, all -> 0x0271 }
            if (r4 <= 0) goto L_0x023d
            com.adwo.adsdk.FSAd r3 = com.adwo.adsdk.FSAd.a(r3)     // Catch:{ Exception -> 0x022e, all -> 0x0271 }
            if (r3 == 0) goto L_0x0217
            short r4 = com.adwo.adsdk.FSAdUtil.s     // Catch:{ Exception -> 0x029f, all -> 0x0271 }
            int r4 = r4 + 1
            short r4 = (short) r4     // Catch:{ Exception -> 0x029f, all -> 0x0271 }
            com.adwo.adsdk.FSAdUtil.s = r4     // Catch:{ Exception -> 0x029f, all -> 0x0271 }
        L_0x0217:
            if (r37 == 0) goto L_0x021c
            r37.close()     // Catch:{ Exception -> 0x024c }
        L_0x021c:
            if (r36 == 0) goto L_0x0221
            r36.close()     // Catch:{ Exception -> 0x024c }
        L_0x0221:
            if (r35 == 0) goto L_0x024d
            r35.disconnect()     // Catch:{ Exception -> 0x024c }
            r35 = r3
            goto L_0x00b6
        L_0x022a:
            r3.write(r4)     // Catch:{ Exception -> 0x022e, all -> 0x0271 }
            goto L_0x01fc
        L_0x022e:
            r3 = move-exception
            r4 = r32
            r33 = r35
            r35 = r3
            r3 = r37
            r37 = r36
            r36 = r33
            goto L_0x012d
        L_0x023d:
            java.lang.String r3 = "Adwo SDK"
            java.lang.String r4 = "Could not get ad from Adwo servers."
            android.util.Log.w(r3, r4)     // Catch:{ Exception -> 0x022e, all -> 0x0271 }
            r3 = r32
            goto L_0x0217
        L_0x0247:
            r35 = move-exception
            r35 = r4
            goto L_0x00b6
        L_0x024c:
            r35 = move-exception
        L_0x024d:
            r35 = r3
            goto L_0x00b6
        L_0x0251:
            r35 = move-exception
            goto L_0x00b4
        L_0x0254:
            r36 = move-exception
            goto L_0x01b9
        L_0x0257:
            r35 = move-exception
            r36 = r4
            r33 = r3
            r3 = r37
            r37 = r33
            goto L_0x01aa
        L_0x0262:
            r36 = move-exception
            r33 = r36
            r36 = r35
            r35 = r33
            r34 = r3
            r3 = r37
            r37 = r34
            goto L_0x01aa
        L_0x0271:
            r3 = move-exception
            r33 = r3
            r3 = r37
            r37 = r36
            r36 = r35
            r35 = r33
            goto L_0x01aa
        L_0x027e:
            r35 = move-exception
            goto L_0x01aa
        L_0x0281:
            r35 = move-exception
            r36 = r4
            r4 = r32
            r33 = r37
            r37 = r3
            r3 = r33
            goto L_0x012d
        L_0x028e:
            r36 = move-exception
            r4 = r32
            r33 = r35
            r35 = r36
            r36 = r33
            r34 = r37
            r37 = r3
            r3 = r34
            goto L_0x012d
        L_0x029f:
            r4 = move-exception
            r33 = r4
            r4 = r3
            r3 = r37
            r37 = r36
            r36 = r35
            r35 = r33
            goto L_0x012d
        L_0x02ad:
            r35 = r4
            goto L_0x00b6
        L_0x02b1:
            r35 = r4
            goto L_0x00ad
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.FSAdUtil.a(android.content.Context, byte, byte):com.adwo.adsdk.FSAd");
    }

    private void c() {
        try {
            String networkOperator = this.a.getNetworkOperator();
            if (networkOperator != null && networkOperator.length() >= 4) {
                F = Integer.parseInt(networkOperator.substring(0, 3));
                Integer.parseInt(networkOperator.substring(3));
            }
        } catch (Exception e2) {
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String b(android.content.Context r5) {
        /*
            r4 = this;
            java.lang.Class<android.webkit.WebSettings> r0 = android.webkit.WebSettings.class
            r1 = 2
            java.lang.Class[] r1 = new java.lang.Class[r1]     // Catch:{ Exception -> 0x003a }
            r2 = 0
            java.lang.Class<android.content.Context> r3 = android.content.Context.class
            r1[r2] = r3     // Catch:{ Exception -> 0x003a }
            r2 = 1
            java.lang.Class<android.webkit.WebView> r3 = android.webkit.WebView.class
            r1[r2] = r3     // Catch:{ Exception -> 0x003a }
            java.lang.reflect.Constructor r1 = r0.getDeclaredConstructor(r1)     // Catch:{ Exception -> 0x003a }
            r0 = 1
            r1.setAccessible(r0)     // Catch:{ Exception -> 0x003a }
            r0 = 2
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ all -> 0x0034 }
            r2 = 0
            r0[r2] = r5     // Catch:{ all -> 0x0034 }
            r2 = 1
            r3 = 0
            r0[r2] = r3     // Catch:{ all -> 0x0034 }
            java.lang.Object r0 = r1.newInstance(r0)     // Catch:{ all -> 0x0034 }
            android.webkit.WebSettings r0 = (android.webkit.WebSettings) r0     // Catch:{ all -> 0x0034 }
            java.lang.String r0 = r0.getUserAgentString()     // Catch:{ all -> 0x0034 }
            r4.x = r0     // Catch:{ all -> 0x0034 }
            java.lang.String r0 = r4.x     // Catch:{ all -> 0x0034 }
            r2 = 0
            r1.setAccessible(r2)     // Catch:{ Exception -> 0x003a }
        L_0x0033:
            return r0
        L_0x0034:
            r0 = move-exception
            r2 = 0
            r1.setAccessible(r2)     // Catch:{ Exception -> 0x003a }
            throw r0     // Catch:{ Exception -> 0x003a }
        L_0x003a:
            r0 = move-exception
            android.webkit.WebView r0 = new android.webkit.WebView
            r0.<init>(r5)
            android.webkit.WebSettings r0 = r0.getSettings()
            java.lang.String r0 = r0.getUserAgentString()
            r4.x = r0
            java.lang.String r0 = r4.x
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.FSAdUtil.b(android.content.Context):java.lang.String");
    }
}
