package com.adwo.adsdk;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;
import java.io.InputStream;

public final class X extends View implements Runnable {
    private Bitmap a;
    private V b = new V();
    private int c;
    private Thread d = null;
    private Paint e = new Paint();
    private volatile boolean f = true;

    public X(Context context) {
        super(context);
    }

    public final void a(InputStream inputStream) {
        this.b.a(inputStream);
        this.c = this.b.a();
        this.a = this.b.b(0);
    }

    public final void a() {
        b();
        if (this.a != null && this.c > 0) {
            this.d = new Thread(this);
            this.f = true;
            this.d.start();
        }
    }

    public final void b() {
        if (this.d != null) {
            this.f = false;
            this.d.interrupt();
            this.d = null;
        }
    }

    /* access modifiers changed from: protected */
    public final void onDraw(Canvas canvas) {
        if (this.a != null) {
            canvas.drawBitmap(this.a, 0.0f, 0.0f, this.e);
            this.a = this.b.b();
        }
    }

    public final void run() {
        while (this.f) {
            int i = 0;
            while (i < this.c) {
                try {
                    int a2 = this.b.a(i);
                    postInvalidate();
                    Thread.sleep((long) a2);
                    i++;
                } catch (Exception e2) {
                }
            }
        }
    }
}
