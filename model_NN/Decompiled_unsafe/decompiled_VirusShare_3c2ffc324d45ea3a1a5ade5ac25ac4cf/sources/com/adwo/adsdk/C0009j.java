package com.adwo.adsdk;

import android.content.Context;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;

/* renamed from: com.adwo.adsdk.j  reason: case insensitive filesystem */
final class C0009j {
    Context a;
    protected int b = -1;
    protected String c = null;
    protected String d = null;
    protected String e = null;
    protected byte f;
    protected List g = new ArrayList();
    protected List h = new ArrayList();
    protected String i = null;
    C0011l j;

    public static C0009j a(Context context, byte[] bArr) {
        C0009j b2 = R.b(bArr);
        if (b2 == null) {
            return null;
        }
        b2.a = context;
        if (b2.c == null && b2.e == null) {
            return null;
        }
        if (b2.c != null && b2.c.length() == 0) {
            return null;
        }
        if (b2.e != null && b2.e.length() == 0) {
            return null;
        }
        Log.d("Adwo SDK", "Get an ad from Adwo servers.");
        return b2;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (this.d != null) {
            new C0010k(this).start();
        }
    }

    public final String b() {
        return this.c;
    }

    public final String c() {
        return this.e;
    }

    public final String toString() {
        return this.c;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof C0009j)) {
            return false;
        }
        C0009j jVar = (C0009j) obj;
        if (this.c != null && jVar.c != null && this.c.equals(jVar.c)) {
            return true;
        }
        if (this.e != null && jVar.e != null && this.e.equals(jVar.e)) {
            return true;
        }
        if (this.i == null || jVar.i == null || !this.i.equals(jVar.i)) {
            return false;
        }
        return true;
    }

    public final int hashCode() {
        return toString().hashCode();
    }
}
