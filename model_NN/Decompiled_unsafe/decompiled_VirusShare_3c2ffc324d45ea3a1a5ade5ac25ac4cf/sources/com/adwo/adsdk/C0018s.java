package com.adwo.adsdk;

import android.app.Activity;
import android.os.Environment;
import android.widget.Toast;

/* renamed from: com.adwo.adsdk.s  reason: case insensitive filesystem */
final class C0018s implements Runnable {
    private final /* synthetic */ Activity a;
    private final /* synthetic */ String b;

    C0018s(C0013n nVar, Activity activity, String str) {
        this.a = activity;
        this.b = str;
    }

    public final void run() {
        Toast.makeText(this.a, "开始下载\n" + this.b + "\n至目录" + Environment.getExternalStorageDirectory() + "/adwo/", 1).show();
    }
}
