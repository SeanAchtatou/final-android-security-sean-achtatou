package com.adwo.adsdk;

import android.view.animation.Animation;

final class O implements Animation.AnimationListener {
    private /* synthetic */ N a;

    O(N n) {
        this.a = n;
    }

    public final void onAnimationStart(Animation animation) {
    }

    public final void onAnimationEnd(Animation animation) {
        if (this.a.b != null) {
            this.a.d.removeView(this.a.b);
            this.a.b.a();
            this.a.b = null;
        }
        this.a.d.c = this.a.a;
    }

    public final void onAnimationRepeat(Animation animation) {
    }
}
