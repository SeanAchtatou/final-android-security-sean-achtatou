package com.adwo.adsdk;

final class J implements Runnable {
    private /* synthetic */ I a;
    private final /* synthetic */ C0001b b;
    private final /* synthetic */ int c;
    private final /* synthetic */ boolean d;

    J(I i, C0001b bVar, int i2, boolean z) {
        this.a = i;
        this.b = bVar;
        this.c = i2;
        this.d = z;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [boolean] */
    /* JADX WARN: Type inference failed for: r2v1, types: [int] */
    /* JADX WARN: Type inference failed for: r2v2 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r3 = this;
            r2 = 0
            com.adwo.adsdk.I r0 = r3.a     // Catch:{ Exception -> 0x0057 }
            com.adwo.adsdk.AdwoAdView r0 = r0.a     // Catch:{ Exception -> 0x0057 }
            com.adwo.adsdk.b r1 = r3.b     // Catch:{ Exception -> 0x0057 }
            r0.addView(r1)     // Catch:{ Exception -> 0x0057 }
            int r0 = r3.c     // Catch:{ Exception -> 0x0057 }
            if (r0 != 0) goto L_0x006a
            com.adwo.adsdk.b r0 = r3.b     // Catch:{ Exception -> 0x0057 }
            r0.b()     // Catch:{ Exception -> 0x0057 }
            boolean r0 = r3.d     // Catch:{ Exception -> 0x0057 }
            if (r0 == 0) goto L_0x004b
            com.adwo.adsdk.I r0 = r3.a     // Catch:{ Exception -> 0x0057 }
            com.adwo.adsdk.AdwoAdView r0 = r0.a     // Catch:{ Exception -> 0x0057 }
            com.adwo.adsdk.b r1 = r3.b     // Catch:{ Exception -> 0x0057 }
            com.adwo.adsdk.AdwoAdView.b(r0, r1)     // Catch:{ Exception -> 0x0057 }
        L_0x0024:
            com.adwo.adsdk.I r0 = r3.a
            com.adwo.adsdk.AdwoAdView r0 = r0.a
            r0.a = r2
        L_0x002c:
            com.adwo.adsdk.b r0 = r3.b
            if (r0 == 0) goto L_0x004a
            com.adwo.adsdk.b r0 = r3.b
            com.adwo.adsdk.j r0 = r0.d()
            java.util.List r0 = r0.h
            if (r0 == 0) goto L_0x004a
            com.adwo.adsdk.b r0 = r3.b
            com.adwo.adsdk.j r0 = r0.d()
            java.util.List r0 = r0.h
            int r1 = r0.size()
            if (r1 == 0) goto L_0x004a
        L_0x0048:
            if (r2 < r1) goto L_0x0080
        L_0x004a:
            return
        L_0x004b:
            com.adwo.adsdk.I r0 = r3.a     // Catch:{ Exception -> 0x0057 }
            com.adwo.adsdk.AdwoAdView r0 = r0.a     // Catch:{ Exception -> 0x0057 }
            com.adwo.adsdk.b r1 = r3.b     // Catch:{ Exception -> 0x0057 }
            com.adwo.adsdk.AdwoAdView.c(r0, r1)     // Catch:{ Exception -> 0x0057 }
            goto L_0x0024
        L_0x0057:
            r0 = move-exception
            java.lang.String r1 = "Adwo SDK"
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0076 }
            android.util.Log.e(r1, r0)     // Catch:{ all -> 0x0076 }
            com.adwo.adsdk.I r0 = r3.a
            com.adwo.adsdk.AdwoAdView r0 = r0.a
            r0.a = r2
            goto L_0x002c
        L_0x006a:
            com.adwo.adsdk.I r0 = r3.a     // Catch:{ Exception -> 0x0057 }
            com.adwo.adsdk.AdwoAdView r0 = r0.a     // Catch:{ Exception -> 0x0057 }
            com.adwo.adsdk.b r1 = r3.b     // Catch:{ Exception -> 0x0057 }
            r0.c = r1     // Catch:{ Exception -> 0x0057 }
            goto L_0x0024
        L_0x0076:
            r0 = move-exception
            com.adwo.adsdk.I r1 = r3.a
            com.adwo.adsdk.AdwoAdView r1 = r1.a
            r1.a = r2
            throw r0
        L_0x0080:
            com.adwo.adsdk.b r0 = r3.b
            com.adwo.adsdk.j r0 = r0.d()
            java.util.List r0 = r0.h
            java.lang.Object r0 = r0.get(r2)
            java.lang.String r0 = (java.lang.String) r0
            com.adwo.adsdk.C0012m.c(r0)
            int r0 = r2 + 1
            r2 = r0
            goto L_0x0048
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.J.run():void");
    }
}
