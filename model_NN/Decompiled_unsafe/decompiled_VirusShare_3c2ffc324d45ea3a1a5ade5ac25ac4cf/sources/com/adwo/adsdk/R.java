package com.adwo.adsdk;

public final class R {
    static {
        byte[] bArr = {20, 10, 1, 20};
    }

    private static short a(byte b, byte b2) {
        return (short) ((b << 8) | (b2 & 255));
    }

    /* JADX WARNING: Removed duplicated region for block: B:137:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0165  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x017a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static com.adwo.adsdk.FSAd a(byte[] r8) {
        /*
            r3 = 0
            r2 = 7
            r6 = 1
            r7 = 0
            int r0 = r8.length
            if (r0 != r6) goto L_0x00a3
            byte r0 = r8[r7]
            r1 = -29
            if (r0 != r1) goto L_0x0016
            java.lang.String r0 = "Adwo SDK"
            java.lang.String r1 = "Adwo_Pid inexist"
            android.util.Log.e(r0, r1)
        L_0x0014:
            r0 = r3
        L_0x0015:
            return r0
        L_0x0016:
            byte r0 = r8[r7]
            r1 = -32
            if (r0 != r1) goto L_0x0024
            java.lang.String r0 = "Adwo SDK"
            java.lang.String r1 = "Server busy"
            android.util.Log.e(r0, r1)
            goto L_0x0014
        L_0x0024:
            byte r0 = r8[r7]
            r1 = -28
            if (r0 != r1) goto L_0x0032
            java.lang.String r0 = "Adwo SDK"
            java.lang.String r1 = "Adwo_Pid inactive"
            android.util.Log.e(r0, r1)
            goto L_0x0014
        L_0x0032:
            byte r0 = r8[r7]
            r1 = -31
            if (r0 == r1) goto L_0x004a
            byte r0 = r8[r7]
            r1 = -24
            if (r0 == r1) goto L_0x004a
            byte r0 = r8[r7]
            r1 = -23
            if (r0 == r1) goto L_0x004a
            byte r0 = r8[r7]
            r1 = -25
            if (r0 != r1) goto L_0x0061
        L_0x004a:
            java.lang.String r0 = "Adwo SDK"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "No ad available: "
            r1.<init>(r2)
            byte r2 = r8[r7]
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            android.util.Log.e(r0, r1)
            goto L_0x0014
        L_0x0061:
            byte r0 = r8[r7]
            r1 = -30
            if (r0 != r1) goto L_0x006f
            java.lang.String r0 = "Adwo SDK"
            java.lang.String r1 = "Unknown Error"
            android.util.Log.e(r0, r1)
            goto L_0x0014
        L_0x006f:
            byte r0 = r8[r7]
            r1 = -26
            if (r0 != r1) goto L_0x007d
            java.lang.String r0 = "Adwo SDK"
            java.lang.String r1 = "Error in receiving data"
            android.util.Log.e(r0, r1)
            goto L_0x0014
        L_0x007d:
            byte r0 = r8[r7]
            r1 = -27
            if (r0 != r1) goto L_0x008b
            java.lang.String r0 = "Adwo SDK"
            java.lang.String r1 = "Error in request data"
            android.util.Log.e(r0, r1)
            goto L_0x0014
        L_0x008b:
            java.lang.String r0 = "Adwo SDK"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "Unknown Error "
            r1.<init>(r2)
            byte r2 = r8[r7]
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            android.util.Log.e(r0, r1)
            goto L_0x0014
        L_0x00a3:
            int r0 = r8.length
            if (r0 >= r2) goto L_0x00b0
            java.lang.String r0 = "Adwo SDK"
            java.lang.String r1 = "Error in receiving data"
            android.util.Log.e(r0, r1)
            r0 = r3
            goto L_0x0015
        L_0x00b0:
            com.adwo.adsdk.FSAd r0 = new com.adwo.adsdk.FSAd
            r0.<init>()
            int r1 = a(r8, r7)
            r0.a = r1
            int r1 = r7 + 4
            int r1 = r1 + 1
            int r1 = r1 + 1
            int r1 = r1 + 1
            int r1 = r1 + 1
            byte r2 = r8[r2]
            r3 = 8
            int r1 = r1 + 1
            byte r3 = r8[r3]
            short r2 = a(r2, r3)
            java.lang.String r3 = new java.lang.String     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            r4 = 9
            java.lang.String r5 = "UTF-8"
            r3.<init>(r8, r4, r2, r5)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            r0.c = r3     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            int r1 = r2 + 9
        L_0x00de:
            int r2 = r1 + 1
            byte r1 = r8[r1]
            r0.e = r1
            int r1 = r2 + 1
            byte r2 = r8[r2]
            r3 = -95
            if (r2 != r3) goto L_0x00ff
            int r2 = r1 + 1
            byte r1 = r8[r1]
            r1 = r1 & 255(0xff, float:3.57E-43)
            short r1 = (short) r1
            if (r1 == 0) goto L_0x01c2
            java.lang.String r3 = new java.lang.String     // Catch:{ Exception -> 0x01be }
            java.lang.String r4 = "UTF-8"
            r3.<init>(r8, r2, r1, r4)     // Catch:{ Exception -> 0x01be }
            r0.b = r3     // Catch:{ Exception -> 0x01be }
            int r1 = r1 + r2
        L_0x00ff:
            int r2 = r1 + 1
            byte r1 = r8[r1]
            r3 = -94
            if (r1 != r3) goto L_0x022f
            int r1 = r2 + 1
            byte r2 = r8[r2]
            int r3 = r1 + 1
            byte r1 = r8[r1]
            short r1 = a(r2, r1)
            if (r1 == 0) goto L_0x01c9
            java.lang.String r2 = new java.lang.String     // Catch:{ Exception -> 0x01c5 }
            java.lang.String r4 = "UTF-8"
            r2.<init>(r8, r3, r1, r4)     // Catch:{ Exception -> 0x01c5 }
            r0.d = r2     // Catch:{ Exception -> 0x01c5 }
            int r1 = r1 + r3
        L_0x011f:
            int r2 = r1 + 1
            byte r1 = r8[r1]
            r3 = -93
            if (r1 != r3) goto L_0x022c
            int r1 = r2 + 1
            byte r2 = r8[r2]
            int r3 = r1 + 1
            byte r1 = r8[r1]
            short r1 = a(r2, r1)
            if (r1 == 0) goto L_0x01d0
            java.lang.String r2 = new java.lang.String     // Catch:{ Exception -> 0x01cc }
            java.lang.String r4 = "UTF-8"
            r2.<init>(r8, r3, r1, r4)     // Catch:{ Exception -> 0x01cc }
            r0.g = r2     // Catch:{ Exception -> 0x01cc }
            int r1 = r1 + r3
        L_0x013f:
            int r2 = r1 + 1
            byte r1 = r8[r1]
            r3 = -92
            if (r1 != r3) goto L_0x0229
            int r1 = r2 + 1
            byte r2 = r8[r2]
            int r3 = r1 + 1
            byte r1 = r8[r1]
            short r1 = a(r2, r1)
            if (r1 == 0) goto L_0x01d7
            java.lang.String r2 = new java.lang.String     // Catch:{ Exception -> 0x01d3 }
            java.lang.String r4 = "UTF-8"
            r2.<init>(r8, r3, r1, r4)     // Catch:{ Exception -> 0x01d3 }
            java.util.List r4 = r0.f     // Catch:{ Exception -> 0x01d3 }
            r4.add(r2)     // Catch:{ Exception -> 0x01d3 }
            int r2 = r3 + r1
        L_0x0163:
            if (r1 <= r6) goto L_0x0229
            int r1 = r2 + 1
            byte r2 = r8[r2]
            int r3 = r1 + 1
            byte r1 = r8[r1]
            short r1 = a(r2, r1)
            if (r1 == 0) goto L_0x0226
            r2 = r7
        L_0x0174:
            if (r2 < r1) goto L_0x01d9
            r1 = r3
        L_0x0177:
            int r2 = r8.length
            if (r1 >= r2) goto L_0x0015
            int r2 = r1 + 1
            byte r1 = r8[r1]
            r3 = -91
            if (r1 != r3) goto L_0x0223
            int r1 = r2 + 1
            byte r2 = r8[r2]
            int r3 = r1 + 1
            byte r1 = r8[r1]
            short r1 = a(r2, r1)
            if (r1 == 0) goto L_0x0220
            r2 = r7
        L_0x0191:
            if (r2 < r1) goto L_0x01f5
            r1 = r3
        L_0x0194:
            int r2 = r8.length
            if (r1 >= r2) goto L_0x0015
            int r2 = r1 + 1
            byte r1 = r8[r1]
            r3 = -86
            if (r1 != r3) goto L_0x0015
            int r1 = a(r8, r2)
            int r2 = r2 + 4
            if (r1 == 0) goto L_0x0015
            java.lang.String r3 = new java.lang.String     // Catch:{ Exception -> 0x01b2 }
            java.lang.String r4 = "UTF-8"
            r3.<init>(r8, r2, r1, r4)     // Catch:{ Exception -> 0x01b2 }
            r0.i = r3     // Catch:{ Exception -> 0x01b2 }
            goto L_0x0015
        L_0x01b2:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0015
        L_0x01b8:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x00de
        L_0x01be:
            r1 = move-exception
            r1.printStackTrace()
        L_0x01c2:
            r1 = r2
            goto L_0x00ff
        L_0x01c5:
            r1 = move-exception
            r1.printStackTrace()
        L_0x01c9:
            r1 = r3
            goto L_0x011f
        L_0x01cc:
            r1 = move-exception
            r1.printStackTrace()
        L_0x01d0:
            r1 = r3
            goto L_0x013f
        L_0x01d3:
            r2 = move-exception
            r2.printStackTrace()
        L_0x01d7:
            r2 = r3
            goto L_0x0163
        L_0x01d9:
            int r4 = r3 + 1
            byte r3 = r8[r3]     // Catch:{ Exception -> 0x0218 }
            int r5 = r4 + 1
            byte r4 = r8[r4]     // Catch:{ Exception -> 0x021c }
            short r3 = a(r3, r4)     // Catch:{ Exception -> 0x021c }
            java.lang.String r4 = new java.lang.String     // Catch:{ Exception -> 0x021c }
            java.lang.String r6 = "UTF-8"
            r4.<init>(r8, r5, r3, r6)     // Catch:{ Exception -> 0x021c }
            java.util.List r6 = r0.f     // Catch:{ Exception -> 0x021c }
            r6.add(r4)     // Catch:{ Exception -> 0x021c }
            int r3 = r3 + r5
            int r2 = r2 + 1
            goto L_0x0174
        L_0x01f5:
            int r4 = r3 + 1
            byte r3 = r8[r3]     // Catch:{ Exception -> 0x0214 }
            int r5 = r4 + 1
            byte r4 = r8[r4]     // Catch:{ Exception -> 0x0211 }
            short r3 = a(r3, r4)     // Catch:{ Exception -> 0x0211 }
            java.lang.String r4 = new java.lang.String     // Catch:{ Exception -> 0x0211 }
            java.lang.String r6 = "UTF-8"
            r4.<init>(r8, r5, r3, r6)     // Catch:{ Exception -> 0x0211 }
            java.util.List r6 = r0.h     // Catch:{ Exception -> 0x0211 }
            r6.add(r4)     // Catch:{ Exception -> 0x0211 }
            int r3 = r3 + r5
            int r2 = r2 + 1
            goto L_0x0191
        L_0x0211:
            r1 = move-exception
            r1 = r5
            goto L_0x0194
        L_0x0214:
            r1 = move-exception
            r1 = r4
            goto L_0x0194
        L_0x0218:
            r1 = move-exception
            r1 = r4
            goto L_0x0177
        L_0x021c:
            r1 = move-exception
            r1 = r5
            goto L_0x0177
        L_0x0220:
            r1 = r3
            goto L_0x0194
        L_0x0223:
            r1 = r2
            goto L_0x0194
        L_0x0226:
            r1 = r3
            goto L_0x0177
        L_0x0229:
            r1 = r2
            goto L_0x0177
        L_0x022c:
            r1 = r2
            goto L_0x013f
        L_0x022f:
            r1 = r2
            goto L_0x011f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.R.a(byte[]):com.adwo.adsdk.FSAd");
    }

    /* JADX WARNING: Removed duplicated region for block: B:121:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0168  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x017d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static com.adwo.adsdk.C0009j b(byte[] r8) {
        /*
            r3 = 0
            r2 = 7
            r6 = 1
            r7 = 0
            int r0 = r8.length
            if (r0 != r6) goto L_0x00a3
            byte r0 = r8[r7]
            r1 = -29
            if (r0 != r1) goto L_0x0016
            java.lang.String r0 = "Adwo SDK"
            java.lang.String r1 = "Adwo_Pid inexist"
            android.util.Log.e(r0, r1)
        L_0x0014:
            r0 = r3
        L_0x0015:
            return r0
        L_0x0016:
            byte r0 = r8[r7]
            r1 = -32
            if (r0 != r1) goto L_0x0024
            java.lang.String r0 = "Adwo SDK"
            java.lang.String r1 = "Server busy"
            android.util.Log.e(r0, r1)
            goto L_0x0014
        L_0x0024:
            byte r0 = r8[r7]
            r1 = -28
            if (r0 != r1) goto L_0x0032
            java.lang.String r0 = "Adwo SDK"
            java.lang.String r1 = "Adwo_Pid inactive"
            android.util.Log.e(r0, r1)
            goto L_0x0014
        L_0x0032:
            byte r0 = r8[r7]
            r1 = -31
            if (r0 == r1) goto L_0x004a
            byte r0 = r8[r7]
            r1 = -24
            if (r0 == r1) goto L_0x004a
            byte r0 = r8[r7]
            r1 = -23
            if (r0 == r1) goto L_0x004a
            byte r0 = r8[r7]
            r1 = -25
            if (r0 != r1) goto L_0x0061
        L_0x004a:
            java.lang.String r0 = "Adwo SDK"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "No ad available: "
            r1.<init>(r2)
            byte r2 = r8[r7]
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            android.util.Log.e(r0, r1)
            goto L_0x0014
        L_0x0061:
            byte r0 = r8[r7]
            r1 = -30
            if (r0 != r1) goto L_0x006f
            java.lang.String r0 = "Adwo SDK"
            java.lang.String r1 = "Unknown Error"
            android.util.Log.e(r0, r1)
            goto L_0x0014
        L_0x006f:
            byte r0 = r8[r7]
            r1 = -26
            if (r0 != r1) goto L_0x007d
            java.lang.String r0 = "Adwo SDK"
            java.lang.String r1 = "Error in receiving data"
            android.util.Log.e(r0, r1)
            goto L_0x0014
        L_0x007d:
            byte r0 = r8[r7]
            r1 = -27
            if (r0 != r1) goto L_0x008b
            java.lang.String r0 = "Adwo SDK"
            java.lang.String r1 = "Error in request data"
            android.util.Log.e(r0, r1)
            goto L_0x0014
        L_0x008b:
            java.lang.String r0 = "Adwo SDK"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "Unknown Error "
            r1.<init>(r2)
            byte r2 = r8[r7]
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            android.util.Log.e(r0, r1)
            goto L_0x0014
        L_0x00a3:
            int r0 = r8.length
            if (r0 >= r2) goto L_0x00b0
            java.lang.String r0 = "Adwo SDK"
            java.lang.String r1 = "Error in receiving data"
            android.util.Log.e(r0, r1)
            r0 = r3
            goto L_0x0015
        L_0x00b0:
            com.adwo.adsdk.j r0 = new com.adwo.adsdk.j
            r0.<init>()
            int r1 = a(r8, r7)
            r0.b = r1
            int r1 = r7 + 4
            int r1 = r1 + 1
            int r1 = r1 + 1
            int r1 = r1 + 1
            int r1 = r1 + 1
            byte r2 = r8[r2]
            r3 = 8
            int r1 = r1 + 1
            byte r3 = r8[r3]
            short r2 = a(r2, r3)
            java.lang.String r3 = new java.lang.String     // Catch:{ UnsupportedEncodingException -> 0x01b2 }
            r4 = 9
            java.lang.String r5 = "UTF-8"
            r3.<init>(r8, r4, r2, r5)     // Catch:{ UnsupportedEncodingException -> 0x01b2 }
            r0.d = r3     // Catch:{ UnsupportedEncodingException -> 0x01b2 }
            int r1 = r2 + 9
        L_0x00de:
            int r2 = r1 + 1
            byte r1 = r8[r1]
            r0.f = r1
            int r1 = r2 + 1
            byte r2 = r8[r2]
            r3 = -95
            if (r2 != r3) goto L_0x00ff
            int r2 = r1 + 1
            byte r1 = r8[r1]
            r1 = r1 & 255(0xff, float:3.57E-43)
            short r1 = (short) r1
            if (r1 == 0) goto L_0x01bc
            java.lang.String r3 = new java.lang.String     // Catch:{ Exception -> 0x01b8 }
            java.lang.String r4 = "UTF-8"
            r3.<init>(r8, r2, r1, r4)     // Catch:{ Exception -> 0x01b8 }
            r0.c = r3     // Catch:{ Exception -> 0x01b8 }
            int r1 = r1 + r2
        L_0x00ff:
            int r2 = r1 + 1
            byte r1 = r8[r1]
            r3 = -94
            if (r1 != r3) goto L_0x0200
            int r1 = r2 + 1
            byte r2 = r8[r2]
            int r3 = r1 + 1
            byte r1 = r8[r1]
            short r1 = a(r2, r1)
            if (r1 == 0) goto L_0x01c3
            java.lang.String r2 = new java.lang.String     // Catch:{ Exception -> 0x01bf }
            java.lang.String r4 = "UTF-8"
            r2.<init>(r8, r3, r1, r4)     // Catch:{ Exception -> 0x01bf }
            r0.e = r2     // Catch:{ Exception -> 0x01bf }
            int r1 = r1 + r3
        L_0x011f:
            int r2 = r1 + 1
            byte r1 = r8[r1]
            r3 = -93
            if (r1 != r3) goto L_0x01fd
            int r1 = r2 + 1
            byte r2 = r8[r2]
            int r3 = r1 + 1
            byte r1 = r8[r1]
            short r1 = a(r2, r1)
            if (r1 == 0) goto L_0x01ca
            java.lang.String r2 = new java.lang.String     // Catch:{ Exception -> 0x01c6 }
            java.lang.String r4 = "UTF-8"
            r2.<init>(r8, r3, r1, r4)     // Catch:{ Exception -> 0x01c6 }
            r0.i = r2     // Catch:{ Exception -> 0x01c6 }
            int r1 = r1 + r3
        L_0x013f:
            int r2 = r8.length
            if (r1 >= r2) goto L_0x0015
            int r2 = r1 + 1
            byte r1 = r8[r1]
            r3 = -92
            if (r1 != r3) goto L_0x01fa
            int r1 = r2 + 1
            byte r2 = r8[r2]
            int r3 = r1 + 1
            byte r1 = r8[r1]
            short r1 = a(r2, r1)
            if (r1 == 0) goto L_0x01d1
            java.lang.String r2 = new java.lang.String     // Catch:{ Exception -> 0x01cd }
            java.lang.String r4 = "UTF-8"
            r2.<init>(r8, r3, r1, r4)     // Catch:{ Exception -> 0x01cd }
            java.util.List r4 = r0.g     // Catch:{ Exception -> 0x01cd }
            r4.add(r2)     // Catch:{ Exception -> 0x01cd }
            int r2 = r3 + r1
        L_0x0166:
            if (r1 <= r6) goto L_0x01fa
            int r1 = r2 + 1
            byte r2 = r8[r2]
            int r3 = r1 + 1
            byte r1 = r8[r1]
            short r1 = a(r2, r1)
            if (r1 == 0) goto L_0x01f8
            r2 = r7
        L_0x0177:
            if (r2 < r1) goto L_0x01d3
            r1 = r3
        L_0x017a:
            int r2 = r8.length
            if (r1 >= r2) goto L_0x0015
            int r2 = r1 + 1
            byte r1 = r8[r1]
            r3 = -91
            if (r1 != r3) goto L_0x0015
            int r1 = r2 + 1
            byte r2 = r8[r2]
            int r3 = r1 + 1
            byte r1 = r8[r1]
            short r1 = a(r2, r1)
            if (r1 == 0) goto L_0x0015
            r2 = r7
        L_0x0194:
            if (r2 >= r1) goto L_0x0015
            int r4 = r3 + 1
            byte r3 = r8[r3]     // Catch:{ Exception -> 0x01ef }
            int r5 = r4 + 1
            byte r4 = r8[r4]     // Catch:{ Exception -> 0x01ef }
            short r3 = a(r3, r4)     // Catch:{ Exception -> 0x01ef }
            java.lang.String r4 = new java.lang.String     // Catch:{ Exception -> 0x01ef }
            java.lang.String r6 = "UTF-8"
            r4.<init>(r8, r5, r3, r6)     // Catch:{ Exception -> 0x01ef }
            java.util.List r6 = r0.h     // Catch:{ Exception -> 0x01ef }
            r6.add(r4)     // Catch:{ Exception -> 0x01ef }
            int r3 = r3 + r5
            int r2 = r2 + 1
            goto L_0x0194
        L_0x01b2:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x00de
        L_0x01b8:
            r1 = move-exception
            r1.printStackTrace()
        L_0x01bc:
            r1 = r2
            goto L_0x00ff
        L_0x01bf:
            r1 = move-exception
            r1.printStackTrace()
        L_0x01c3:
            r1 = r3
            goto L_0x011f
        L_0x01c6:
            r1 = move-exception
            r1.printStackTrace()
        L_0x01ca:
            r1 = r3
            goto L_0x013f
        L_0x01cd:
            r2 = move-exception
            r2.printStackTrace()
        L_0x01d1:
            r2 = r3
            goto L_0x0166
        L_0x01d3:
            int r4 = r3 + 1
            byte r3 = r8[r3]     // Catch:{ Exception -> 0x01f2 }
            int r5 = r4 + 1
            byte r4 = r8[r4]     // Catch:{ Exception -> 0x01f5 }
            short r3 = a(r3, r4)     // Catch:{ Exception -> 0x01f5 }
            java.lang.String r4 = new java.lang.String     // Catch:{ Exception -> 0x01f5 }
            java.lang.String r6 = "UTF-8"
            r4.<init>(r8, r5, r3, r6)     // Catch:{ Exception -> 0x01f5 }
            java.util.List r6 = r0.g     // Catch:{ Exception -> 0x01f5 }
            r6.add(r4)     // Catch:{ Exception -> 0x01f5 }
            int r3 = r3 + r5
            int r2 = r2 + 1
            goto L_0x0177
        L_0x01ef:
            r1 = move-exception
            goto L_0x0015
        L_0x01f2:
            r1 = move-exception
            r1 = r4
            goto L_0x017a
        L_0x01f5:
            r1 = move-exception
            r1 = r5
            goto L_0x017a
        L_0x01f8:
            r1 = r3
            goto L_0x017a
        L_0x01fa:
            r1 = r2
            goto L_0x017a
        L_0x01fd:
            r1 = r2
            goto L_0x013f
        L_0x0200:
            r1 = r2
            goto L_0x011f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.R.b(byte[]):com.adwo.adsdk.j");
    }

    private static int a(byte[] bArr, int i) {
        return ((bArr[i + 0] & 255) << 24) + 0 + ((bArr[i + 1] & 255) << 16) + ((bArr[i + 2] & 255) << 8) + ((bArr[i + 3] & 255) << 0);
    }

    private static byte[] a(short s) {
        return new byte[]{(byte) ((65280 & s) >> 8), (byte) (s & 255)};
    }

    private static byte[] a(double d) {
        long doubleToLongBits = Double.doubleToLongBits(d);
        return new byte[]{(byte) ((int) ((doubleToLongBits >> 56) & 255)), (byte) ((int) ((doubleToLongBits >> 48) & 255)), (byte) ((int) ((doubleToLongBits >> 40) & 255)), (byte) ((int) ((doubleToLongBits >> 32) & 255)), (byte) ((int) ((doubleToLongBits >> 24) & 255)), (byte) ((int) ((doubleToLongBits >> 16) & 255)), (byte) ((int) ((doubleToLongBits >> 8) & 255)), (byte) ((int) (doubleToLongBits & 255))};
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v23, resolved type: short} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v24, resolved type: short} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v45, resolved type: short} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] a(byte r2, byte r3, byte r4, byte r5, byte r6, boolean r7, byte[] r8, byte r9, short r10, byte[] r11, byte[] r12, byte[] r13, short r14, short r15, short r16, byte r17, byte r18, byte[] r19, byte[] r20, boolean r21, double r22, double r24, byte r26, short r27, java.lang.Integer[] r28, short r29) {
        /*
            java.io.ByteArrayOutputStream r14 = new java.io.ByteArrayOutputStream
            r14.<init>()
            r15 = 2
            byte[] r15 = new byte[r15]
            r14.write(r15)     // Catch:{ IOException -> 0x00fa }
            r0 = r14
            r1 = r26
            r0.write(r1)     // Catch:{ IOException -> 0x00fa }
            r14.write(r2)     // Catch:{ IOException -> 0x00fa }
            r14.write(r3)     // Catch:{ IOException -> 0x00fa }
            r14.write(r4)     // Catch:{ IOException -> 0x00fa }
            r14.write(r5)     // Catch:{ IOException -> 0x00fa }
            r14.write(r6)     // Catch:{ IOException -> 0x00fa }
            if (r7 == 0) goto L_0x00f4
            r2 = 0
            r14.write(r2)     // Catch:{ IOException -> 0x00fa }
        L_0x0026:
            r14.write(r8)     // Catch:{ IOException -> 0x00fa }
            r14.write(r9)     // Catch:{ IOException -> 0x00fa }
            byte[] r2 = a(r10)     // Catch:{ IOException -> 0x00fa }
            r14.write(r2)     // Catch:{ IOException -> 0x00fa }
            if (r11 == 0) goto L_0x00ff
            r2 = 73
            r14.write(r2)     // Catch:{ IOException -> 0x00fa }
            int r2 = r11.length     // Catch:{ IOException -> 0x00fa }
            byte r2 = (byte) r2     // Catch:{ IOException -> 0x00fa }
            r14.write(r2)     // Catch:{ IOException -> 0x00fa }
            r14.write(r11)     // Catch:{ IOException -> 0x00fa }
        L_0x0042:
            r2 = 0
            byte[] r2 = a(r2)     // Catch:{ IOException -> 0x00fa }
            r14.write(r2)     // Catch:{ IOException -> 0x00fa }
            r2 = 0
            byte[] r2 = a(r2)     // Catch:{ IOException -> 0x00fa }
            r14.write(r2)     // Catch:{ IOException -> 0x00fa }
            if (r12 == 0) goto L_0x010a
            r2 = 77
            r14.write(r2)     // Catch:{ IOException -> 0x00fa }
            int r2 = r12.length     // Catch:{ IOException -> 0x00fa }
            byte r2 = (byte) r2     // Catch:{ IOException -> 0x00fa }
            r14.write(r2)     // Catch:{ IOException -> 0x00fa }
            r14.write(r12)     // Catch:{ IOException -> 0x00fa }
        L_0x0061:
            if (r13 == 0) goto L_0x0115
            r2 = 66
            r14.write(r2)     // Catch:{ IOException -> 0x00fa }
            int r2 = r13.length     // Catch:{ IOException -> 0x00fa }
            byte r2 = (byte) r2     // Catch:{ IOException -> 0x00fa }
            r14.write(r2)     // Catch:{ IOException -> 0x00fa }
            r14.write(r13)     // Catch:{ IOException -> 0x00fa }
        L_0x0070:
            byte[] r2 = a(r16)     // Catch:{ IOException -> 0x00fa }
            r14.write(r2)     // Catch:{ IOException -> 0x00fa }
            r0 = r14
            r1 = r17
            r0.write(r1)     // Catch:{ IOException -> 0x00fa }
            r0 = r14
            r1 = r18
            r0.write(r1)     // Catch:{ IOException -> 0x00fa }
            if (r19 == 0) goto L_0x0120
            r2 = 78
            r14.write(r2)     // Catch:{ IOException -> 0x00fa }
            r0 = r19
            int r0 = r0.length     // Catch:{ IOException -> 0x00fa }
            r2 = r0
            byte r2 = (byte) r2     // Catch:{ IOException -> 0x00fa }
            r14.write(r2)     // Catch:{ IOException -> 0x00fa }
            r0 = r14
            r1 = r19
            r0.write(r1)     // Catch:{ IOException -> 0x00fa }
        L_0x0098:
            if (r20 == 0) goto L_0x012b
            r2 = 80
            r14.write(r2)     // Catch:{ IOException -> 0x00fa }
            r0 = r20
            int r0 = r0.length     // Catch:{ IOException -> 0x00fa }
            r2 = r0
            byte r2 = (byte) r2     // Catch:{ IOException -> 0x00fa }
            r14.write(r2)     // Catch:{ IOException -> 0x00fa }
            r0 = r14
            r1 = r20
            r0.write(r1)     // Catch:{ IOException -> 0x00fa }
        L_0x00ad:
            if (r21 == 0) goto L_0x0136
            r2 = 76
            r14.write(r2)     // Catch:{ IOException -> 0x00fa }
            byte[] r2 = a(r22)     // Catch:{ IOException -> 0x00fa }
            r3 = 16
            r14.write(r3)     // Catch:{ IOException -> 0x00fa }
            r14.write(r2)     // Catch:{ IOException -> 0x00fa }
            byte[] r2 = a(r24)     // Catch:{ IOException -> 0x00fa }
            r14.write(r2)     // Catch:{ IOException -> 0x00fa }
        L_0x00c7:
            byte[] r2 = a(r27)     // Catch:{ IOException -> 0x00fa }
            r14.write(r2)     // Catch:{ IOException -> 0x00fa }
            r2 = 0
        L_0x00cf:
            r0 = r2
            r1 = r27
            if (r0 < r1) goto L_0x0140
            byte[] r2 = a(r29)     // Catch:{ IOException -> 0x00fa }
            r14.write(r2)     // Catch:{ IOException -> 0x00fa }
        L_0x00db:
            byte[] r2 = r14.toByteArray()
            int r3 = r2.length
            r4 = 2
            int r3 = r3 - r4
            short r3 = (short) r3
            r4 = 0
            r5 = 65280(0xff00, float:9.1477E-41)
            r5 = r5 & r3
            int r5 = r5 >> 8
            byte r5 = (byte) r5
            r2[r4] = r5
            r4 = 1
            r3 = r3 & 255(0xff, float:3.57E-43)
            byte r3 = (byte) r3
            r2[r4] = r3
            return r2
        L_0x00f4:
            r2 = 1
            r14.write(r2)     // Catch:{ IOException -> 0x00fa }
            goto L_0x0026
        L_0x00fa:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x00db
        L_0x00ff:
            r2 = 73
            r14.write(r2)     // Catch:{ IOException -> 0x00fa }
            r2 = 0
            r14.write(r2)     // Catch:{ IOException -> 0x00fa }
            goto L_0x0042
        L_0x010a:
            r2 = 77
            r14.write(r2)     // Catch:{ IOException -> 0x00fa }
            r2 = 0
            r14.write(r2)     // Catch:{ IOException -> 0x00fa }
            goto L_0x0061
        L_0x0115:
            r2 = 66
            r14.write(r2)     // Catch:{ IOException -> 0x00fa }
            r2 = 0
            r14.write(r2)     // Catch:{ IOException -> 0x00fa }
            goto L_0x0070
        L_0x0120:
            r2 = 78
            r14.write(r2)     // Catch:{ IOException -> 0x00fa }
            r2 = 0
            r14.write(r2)     // Catch:{ IOException -> 0x00fa }
            goto L_0x0098
        L_0x012b:
            r2 = 80
            r14.write(r2)     // Catch:{ IOException -> 0x00fa }
            r2 = 0
            r14.write(r2)     // Catch:{ IOException -> 0x00fa }
            goto L_0x00ad
        L_0x0136:
            r2 = 76
            r14.write(r2)     // Catch:{ IOException -> 0x00fa }
            r2 = 0
            r14.write(r2)     // Catch:{ IOException -> 0x00fa }
            goto L_0x00c7
        L_0x0140:
            r3 = r28[r2]     // Catch:{ IOException -> 0x00fa }
            int r3 = r3.intValue()     // Catch:{ IOException -> 0x00fa }
            r4 = 4
            byte[] r4 = new byte[r4]     // Catch:{ IOException -> 0x00fa }
            r5 = 0
            int r6 = r3 >>> 24
            byte r6 = (byte) r6     // Catch:{ IOException -> 0x00fa }
            r4[r5] = r6     // Catch:{ IOException -> 0x00fa }
            r5 = 1
            int r6 = r3 >>> 16
            byte r6 = (byte) r6     // Catch:{ IOException -> 0x00fa }
            r4[r5] = r6     // Catch:{ IOException -> 0x00fa }
            r5 = 2
            int r6 = r3 >>> 8
            byte r6 = (byte) r6     // Catch:{ IOException -> 0x00fa }
            r4[r5] = r6     // Catch:{ IOException -> 0x00fa }
            r5 = 3
            byte r3 = (byte) r3     // Catch:{ IOException -> 0x00fa }
            r4[r5] = r3     // Catch:{ IOException -> 0x00fa }
            r14.write(r4)     // Catch:{ IOException -> 0x00fa }
            int r2 = r2 + 1
            goto L_0x00cf
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adwo.adsdk.R.a(byte, byte, byte, byte, byte, boolean, byte[], byte, short, byte[], byte[], byte[], short, short, short, byte, byte, byte[], byte[], boolean, double, double, byte, short, java.lang.Integer[], short):byte[]");
    }
}
