package com.adwo.adsdk;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;

public final class FSAd implements Parcelable {
    public static final Parcelable.Creator CREATOR = new T();
    protected int a = -1;
    protected String b = null;
    protected String c = null;
    protected String d = null;
    protected byte e;
    protected List f = new ArrayList();
    protected String g = null;
    protected List h = new ArrayList();
    protected String i = null;

    protected static FSAd a(byte[] bArr) {
        FSAd a2 = R.a(bArr);
        if (a2 == null) {
            return null;
        }
        if (a2.b == null && a2.d == null) {
            return null;
        }
        if (a2.b != null && a2.b.length() == 0) {
            return null;
        }
        if (a2.d != null && a2.d.length() == 0) {
            return null;
        }
        Log.d("Adwo SDK", "Get an ad from Adwo servers.");
        return a2;
    }

    protected FSAd() {
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof C0009j)) {
            return false;
        }
        C0009j jVar = (C0009j) obj;
        if (this.b != null && jVar.c != null && this.b.equals(jVar.c)) {
            return true;
        }
        if (this.d != null && jVar.e != null && this.d.equals(jVar.e)) {
            return true;
        }
        if (this.g == null || jVar.i == null || !this.g.equals(jVar.i)) {
            return false;
        }
        return true;
    }

    public final int hashCode() {
        return toString().hashCode();
    }

    public final int describeContents() {
        return 0;
    }

    public final void writeToParcel(Parcel parcel, int i2) {
        parcel.writeInt(this.a);
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        parcel.writeByte(this.e);
        parcel.writeString(this.g);
        parcel.writeString(this.i);
    }
}
