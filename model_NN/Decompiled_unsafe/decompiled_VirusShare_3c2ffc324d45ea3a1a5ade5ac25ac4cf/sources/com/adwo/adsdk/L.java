package com.adwo.adsdk;

import android.view.animation.Animation;

final class L implements Animation.AnimationListener {
    private /* synthetic */ AdwoAdView a;
    private final /* synthetic */ C0001b b;
    private final /* synthetic */ Q c;

    L(AdwoAdView adwoAdView, C0001b bVar, Q q) {
        this.a = adwoAdView;
        this.b = bVar;
        this.c = q;
    }

    public final void onAnimationStart(Animation animation) {
    }

    public final void onAnimationEnd(Animation animation) {
        this.a.post(new N(this.a, this.b, this.c));
    }

    public final void onAnimationRepeat(Animation animation) {
    }
}
