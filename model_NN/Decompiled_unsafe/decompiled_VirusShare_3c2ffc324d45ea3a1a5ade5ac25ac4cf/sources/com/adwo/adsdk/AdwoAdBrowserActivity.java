package com.adwo.adsdk;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.webkit.CookieManager;

public class AdwoAdBrowserActivity extends Activity {
    private static String[] b = {"explode", "toptobottom", "bottomtotop"};
    /* access modifiers changed from: private */
    public static boolean c;
    /* access modifiers changed from: private */
    public C0013n a;
    private Handler d = new H(this);

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        boolean z;
        boolean z2;
        boolean z3;
        long j;
        super.onCreate(bundle);
        requestWindowFeature(2);
        setProgressBarVisibility(true);
        int i = 0;
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            long j2 = extras.getLong("transitionTime", 600);
            extras.getString("overlayTransition");
            i = extras.getInt("shouldResizeOverlay", 0);
            boolean z4 = extras.getBoolean("shouldShowTitlebar", false);
            extras.getString("overlayTitle");
            extras.getBoolean("shouldShowBottomBar", true);
            boolean z5 = extras.getBoolean("shouldEnableBottomBar", true);
            z = extras.getBoolean("shouldMakeOverlayTransparent", false);
            z2 = z5;
            z3 = z4;
            j = j2;
        } else {
            z = false;
            z2 = true;
            z3 = true;
            j = 600;
        }
        this.a = new C0013n(this, i, j, b[(int) (Math.random() * 3.0d)], z3, z2, z);
        setContentView(this.a);
        CookieManager.getInstance().setAcceptCookie(true);
        String stringExtra = getIntent().getStringExtra("url");
        if (stringExtra != null) {
            this.a.c(stringExtra);
        } else {
            this.a.c("http://www.adwo.com");
        }
        setRequestedOrientation(1);
        c = false;
        this.d.sendEmptyMessageDelayed(1, 5000);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return super.onKeyDown(i, keyEvent);
        }
        if (c) {
            return super.onKeyDown(i, keyEvent);
        }
        return false;
    }
}
