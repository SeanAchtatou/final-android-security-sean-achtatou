package com.adwo.adsdk;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

final class M extends BroadcastReceiver {
    private /* synthetic */ AdwoAdView a;

    public M(AdwoAdView adwoAdView) {
        this.a = adwoAdView;
    }

    public final void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.SCREEN_OFF")) {
            this.a.b = false;
        } else if (intent.getAction().equals("android.intent.action.SCREEN_ON")) {
            this.a.b = true;
        }
    }
}
