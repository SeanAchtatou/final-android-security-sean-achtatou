package com.madhouse.android.ads;

import I.I;
import java.util.ListResourceBundle;

public class Resource_zh_TW extends ListResourceBundle {
    private Object[][] _ = {new Object[]{I.I(58), "關閉"}, new Object[]{I.I(64), "上一頁"}, new Object[]{I.I(73), "下一頁"}, new Object[]{I.I(78), "刷新"}, new Object[]{I.I(86), "打開默認瀏覽器"}, new Object[]{I.I(107), "取消"}};

    /* access modifiers changed from: protected */
    public final Object[][] getContents() {
        return this._;
    }
}
