package com.madhouse.android.ads;

import I.I;
import java.util.ListResourceBundle;

public class Resource_en_US extends ListResourceBundle {
    private Object[][] _ = {new Object[]{I.I(58), I.I(114)}, new Object[]{I.I(64), I.I(120)}, new Object[]{I.I(73), I.I(129)}, new Object[]{I.I(78), I.I(134)}, new Object[]{I.I(86), I.I(142)}, new Object[]{I.I(107), I.I(163)}};

    /* access modifiers changed from: protected */
    public final Object[][] getContents() {
        return this._;
    }
}
