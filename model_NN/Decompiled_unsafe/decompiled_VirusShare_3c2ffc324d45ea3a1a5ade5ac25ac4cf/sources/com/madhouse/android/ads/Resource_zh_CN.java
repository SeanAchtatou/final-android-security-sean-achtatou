package com.madhouse.android.ads;

import I.I;
import java.util.ListResourceBundle;

public class Resource_zh_CN extends ListResourceBundle {
    private Object[][] _ = {new Object[]{I.I(58), "关闭"}, new Object[]{I.I(64), "前一页"}, new Object[]{I.I(73), "后一页"}, new Object[]{I.I(78), "刷新"}, new Object[]{I.I(86), "打开默认浏览器"}, new Object[]{I.I(107), "取消"}};

    /* access modifiers changed from: protected */
    public final Object[][] getContents() {
        return this._;
    }
}
