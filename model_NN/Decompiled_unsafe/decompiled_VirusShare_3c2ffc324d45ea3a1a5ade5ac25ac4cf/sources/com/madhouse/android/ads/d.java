package com.madhouse.android.ads;

import android.graphics.Bitmap;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;

final class d extends aj {
    private /* synthetic */ ccccc _;

    d(ccccc ccccc) {
        this._ = ccccc;
    }

    public final void onPageFinished(WebView webView, String str) {
        if (this._.__ != null) {
            try {
                this._.__.onPageFinished(webView, str);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        super.onPageFinished(webView, str);
    }

    public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        if (this._.__ != null) {
            try {
                this._.__.onPageStarted(webView, str, bitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        super.onPageStarted(webView, str, bitmap);
    }

    public final void onReceivedError(WebView webView, int i, String str, String str2) {
        if (this._.__ != null) {
            try {
                this._.__.onReceivedError(webView, i, str, str2);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        super.onReceivedError(webView, i, str, str2);
    }

    public final void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, bz bzVar) {
        sslErrorHandler.proceed();
        super.onReceivedSslError(webView, sslErrorHandler, bzVar);
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (!fffff.__(str)) {
            webView.loadUrl(str);
            return true;
        } else if (this._.__ == null) {
            return true;
        } else {
            try {
                this._.__.shouldOverrideMarketUrlLoading(webView, str);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return true;
            }
        }
    }
}
