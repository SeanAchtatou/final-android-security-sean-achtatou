package com.madhouse.android.ads;

public interface AdListener {
    void onAdEvent(AdView adView, int i);

    void onAdFullScreenStatus(boolean z);
}
