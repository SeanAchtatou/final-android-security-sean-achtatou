package com.madhouse.android.ads;

import I.I;
import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.location.Location;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.RelativeLayout;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

public class AdView extends RelativeLayout {
    public static final int BANNER_ANIMATION_TYPE_CURLDOWN = 6;
    public static final int BANNER_ANIMATION_TYPE_CURLUP = 5;
    public static final int BANNER_ANIMATION_TYPE_FADEINOUT = 2;
    public static final int BANNER_ANIMATION_TYPE_FLIPFROMLEFT = 3;
    public static final int BANNER_ANIMATION_TYPE_FLIPFROMRIGHT = 4;
    public static final int BANNER_ANIMATION_TYPE_NONE = 0;
    public static final int BANNER_ANIMATION_TYPE_RANDOM = 1;
    public static final int BANNER_ANIMATION_TYPE_SLIDEFROMLEFT = 7;
    public static final int BANNER_ANIMATION_TYPE_SLIDEFROMRIGHT = 8;
    public static final int EVENT_INCOMPLETE_PERMISSION = 3;
    public static final int EVENT_INVALIDAD = 2;
    public static final int EVENT_NEWAD = 1;
    public static final int PHONE_AD_MEASURE_176 = 176;
    public static final int PHONE_AD_MEASURE_240 = 240;
    public static final int PHONE_AD_MEASURE_320 = 320;
    public static final int PHONE_AD_MEASURE_360 = 360;
    public static final int PHONE_AD_MEASURE_480 = 480;
    public static final int PHONE_AD_MEASURE_640 = 640;
    public static final int PHONE_AD_MEASURE_AUTO = 0;
    public static final int TABLET_AD_MEASURE_300 = 300;
    public static final int TABLET_AD_MEASURE_468 = 468;
    public static final int TABLET_AD_MEASURE_728 = 728;
    private static final float[][] _ = {new float[]{0.0f, 90.0f, -90.0f, 0.0f}, new float[]{0.0f, -90.0f, 90.0f, 0.0f}};
    private static final List aa = new ArrayList();
    /* access modifiers changed from: private */
    public static final String[] b = {I.I(1635), I.I(1670), I.I(1699)};
    /* access modifiers changed from: private */
    public static p ddd;
    /* access modifiers changed from: private */
    public static int dddd = 0;
    private static final Object ddddd = new Object();
    private String $;
    /* access modifiers changed from: private */
    public int $$;
    private int $$$;
    private int $$$$;
    /* access modifiers changed from: private */
    public AdListener $$$$$;
    /* access modifiers changed from: private */
    public Context __;
    /* access modifiers changed from: private */
    public Handler ___;
    private Timer ____;
    private String _____;
    /* access modifiers changed from: private */
    public boolean a;
    private boolean aaa;
    private int aaaa;
    /* access modifiers changed from: private */
    public int aaaaa;
    /* access modifiers changed from: private */
    public boolean bb;
    private boolean bbb;
    /* access modifiers changed from: private */
    public String bbbb;
    /* access modifiers changed from: private */
    public boolean bbbbb;
    /* access modifiers changed from: private */
    public boolean c;
    /* access modifiers changed from: private */
    public ccccc cc;
    private al ccc;
    /* access modifiers changed from: private */
    public ff cccc;
    /* access modifiers changed from: private */
    public bj ccccc;
    private bf d;
    private o dd;
    private String e;
    /* access modifiers changed from: private */
    public bx ee;
    private n eee;
    private boolean eeee;
    private boolean eeeee;
    /* access modifiers changed from: private */
    public _ f;

    private void $() {
        if (!this.c) {
            ah.__(this.__);
            this.c = true;
            this.bbbb = null;
            this.bbbbb = false;
            _(false);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.madhouse.android.ads.AdView._(java.lang.StringBuffer, boolean, java.lang.String, java.lang.String):void
     arg types: [java.lang.StringBuffer, int, java.lang.String, java.lang.String]
     candidates:
      com.madhouse.android.ads.AdView._(com.madhouse.android.ads.AdView, int, int, int):void
      com.madhouse.android.ads.AdView._(com.madhouse.android.ads.AdView, int, int, boolean):void
      com.madhouse.android.ads.AdView._(java.lang.StringBuffer, boolean, java.lang.String, java.lang.String):void */
    /* access modifiers changed from: private */
    public String $$() {
        String str = this._____;
        if (str == null) {
            return null;
        }
        String adPosition = getAdPosition();
        if (adPosition == null) {
            return null;
        }
        String $2 = this.ccc.$();
        if ($2 == null) {
            return null;
        }
        int bb2 = this.ccc.bb();
        String replaceAll = this.cc.getSettings().getUserAgentString().replaceAll(I.I(3353), "");
        StringBuffer stringBuffer = new StringBuffer(1024);
        stringBuffer.append(b[this.aaaaa]).append(str);
        _(stringBuffer, true, I.I(2203), adPosition);
        _(stringBuffer, false, I.I(2221), $2);
        _(stringBuffer, false, I.I(2320), "" + bb2);
        _(stringBuffer, false, I.I(2240), replaceAll);
        _(stringBuffer, false, I.I(3357), "" + (f._ ? 1 : 0));
        _(stringBuffer, false, I.I(2380), I.I(2383));
        _(stringBuffer, false, I.I(3360), _($2, str));
        _(stringBuffer, false, I.I(2233), I.I(497));
        String _2 = bf._._(I.I(1078));
        String _3 = bf._._(I.I(1084));
        if (_2 == null || _3 == null) {
            I.I(3363);
            _2 = I.I(3369);
        }
        _(stringBuffer, false, I.I(3375), _2);
        _(stringBuffer, false, I.I(3378), this.__.getPackageName());
        return stringBuffer.toString();
    }

    static final /* synthetic */ void $$(AdView adView) {
        adView._(false);
        adView.bbb = true;
        adView.bb = false;
    }

    /* access modifiers changed from: private */
    public void $$$() {
        if (!this.eeee) {
            ((Activity) this.__).getWindow().clearFlags(1024);
        }
    }

    /* access modifiers changed from: private */
    public static int $$$$() {
        int i;
        synchronized (ddddd) {
            i = dddd;
            if (dddd == 0) {
                dddd = 1;
            }
        }
        return i;
    }

    public AdView(Context context) {
        this(context, null, 0);
    }

    public AdView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.madhouse.android.ads.AdView._(android.content.Context, android.util.AttributeSet, java.lang.String, int, int, int, boolean):void
     arg types: [android.content.Context, android.util.AttributeSet, ?[OBJECT, ARRAY], int, int, int, int]
     candidates:
      com.madhouse.android.ads.AdView._(com.madhouse.android.ads.AdView, java.lang.String, int, int, int, boolean, java.lang.String):void
      com.madhouse.android.ads.AdView._(android.content.Context, android.util.AttributeSet, java.lang.String, int, int, int, boolean):void */
    public AdView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.$$ = -1;
        this.$$$ = -1;
        this.$$$$ = -1;
        this.a = false;
        this.aaa = false;
        this.aaaa = 0;
        this.aaaaa = 0;
        this.bb = false;
        this.bbb = false;
        this.bbbb = null;
        this.bbbbb = false;
        this.c = false;
        this.cccc = new ff();
        this.ee = new fff(this);
        this.eee = new n(this);
        this.eeee = false;
        this.eeeee = true;
        this.f = new _(this);
        _(context, attributeSet, (String) null, 600, 0, 1, false);
    }

    public AdView(Context context, AttributeSet attributeSet, int i, String str, int i2, int i3, int i4, boolean z) {
        super(context, attributeSet, i);
        this.$$ = -1;
        this.$$$ = -1;
        this.$$$$ = -1;
        this.a = false;
        this.aaa = false;
        this.aaaa = 0;
        this.aaaaa = 0;
        this.bb = false;
        this.bbb = false;
        this.bbbb = null;
        this.bbbbb = false;
        this.c = false;
        this.cccc = new ff();
        this.ee = new fff(this);
        this.eee = new n(this);
        this.eeee = false;
        this.eeeee = true;
        this.f = new _(this);
        _(context, attributeSet, str, i2, i3, i4, z);
    }

    public AdView(Context context, AttributeSet attributeSet, int i, String str, int i2, int i3, boolean z) {
        super(context, attributeSet, i);
        this.$$ = -1;
        this.$$$ = -1;
        this.$$$$ = -1;
        this.a = false;
        this.aaa = false;
        this.aaaa = 0;
        this.aaaaa = 0;
        this.bb = false;
        this.bbb = false;
        this.bbbb = null;
        this.bbbbb = false;
        this.c = false;
        this.cccc = new ff();
        this.ee = new fff(this);
        this.eee = new n(this);
        this.eeee = false;
        this.eeeee = true;
        this.f = new _(this);
        _(context, attributeSet, str, i2, i3, 1, z);
    }

    public AdView(Context context, AttributeSet attributeSet, int i, String str, int i2, boolean z) {
        super(context, attributeSet, i);
        this.$$ = -1;
        this.$$$ = -1;
        this.$$$$ = -1;
        this.a = false;
        this.aaa = false;
        this.aaaa = 0;
        this.aaaaa = 0;
        this.bb = false;
        this.bbb = false;
        this.bbbb = null;
        this.bbbbb = false;
        this.c = false;
        this.cccc = new ff();
        this.ee = new fff(this);
        this.eee = new n(this);
        this.eeee = false;
        this.eeeee = true;
        this.f = new _(this);
        _(context, attributeSet, str, i2, 0, 1, z);
    }

    static final /* synthetic */ String _(AdView adView, bg bgVar) {
        String str = null;
        if (bgVar == null || bgVar._ == null) {
            return null;
        }
        if (bgVar._.indexOf(I.I(2773)) < 0) {
            return bgVar._;
        }
        String[] split = bgVar._.split(I.I(2676));
        for (String trim : split) {
            String trim2 = trim.trim();
            if (trim2.startsWith(I.I(2773))) {
                str = trim2.substring(I.I(2773).length()).trim();
            } else if (trim2.startsWith(I.I(2778))) {
                long j = bgVar.___ - bgVar.__;
                I.I(2783) + j;
                if (j > 0) {
                    I.I(2804) + bgVar._.length();
                    adView.e = "" + (((long) (bgVar._.length() * 1000)) / j);
                    I.I(2824) + adView.e;
                }
            } else if (trim2.startsWith(I.I(2842))) {
                bf._(I.I(1084), trim2.substring(I.I(2842).length()).trim());
            } else if (trim2.startsWith(I.I(2846))) {
                bf._(I.I(1078), trim2.substring(I.I(2846).length()).trim());
            }
        }
        return str;
    }

    private static String _(String str, String str2) {
        int i;
        try {
            i = Integer.parseInt(str.substring(str.length() - 1));
        } catch (Exception e2) {
            i = 0;
        }
        return fffff._(str + str2.substring(0, str2.length() - i)).substring(0, 8);
    }

    private void _(int i) {
        int i2 = 20;
        if (i >= 20) {
            i2 = i > 600 ? 600 : i;
        }
        this.$$ = i2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void _(android.content.Context r6, android.util.AttributeSet r7, java.lang.String r8, int r9, int r10, int r11, boolean r12) {
        /*
            r5 = this;
            r5.__ = r6
            java.lang.String r0 = r5._____
            if (r0 != 0) goto L_0x001b
            android.content.Context r0 = r5.__
            r1 = 3910(0xf46, float:5.479E-42)
            java.lang.String r1 = I.I.I(r1)
            java.lang.String r0 = com.madhouse.android.ads.fffff._(r0, r1)
            if (r0 == 0) goto L_0x0022
            r5._____ = r0
            android.content.Context r1 = r5.__
            com.madhouse.android.ads.AdManager.setApplicationId(r1, r0)
        L_0x001b:
            r0 = 1
        L_0x001c:
            if (r0 != 0) goto L_0x0036
            r5._____()
        L_0x0021:
            return
        L_0x0022:
            java.lang.String r0 = com.madhouse.android.ads.AdManager._()
            if (r0 == 0) goto L_0x002b
            r5._____ = r0
            goto L_0x001b
        L_0x002b:
            r0 = 3917(0xf4d, float:5.489E-42)
            java.lang.String r0 = I.I.I(r0)
            com.madhouse.android.ads.f.____(r0)
            r0 = 0
            goto L_0x001c
        L_0x0036:
            r0 = 1
            r5.setFocusable(r0)
            r0 = 1
            r5.setClickable(r0)
            r0 = 262144(0x40000, float:3.67342E-40)
            r5.setDescendantFocusability(r0)
            com.madhouse.android.ads.f._ = r12
            if (r7 == 0) goto L_0x00a9
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r1 = 3722(0xe8a, float:5.216E-42)
            java.lang.String r1 = I.I.I(r1)
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = r6.getPackageName()
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r1 = 3758(0xeae, float:5.266E-42)
            java.lang.String r1 = I.I.I(r1)
            r2 = 0
            boolean r1 = r7.getAttributeBooleanValue(r0, r1, r2)
            com.madhouse.android.ads.f._ = r1
            r1 = 3764(0xeb4, float:5.274E-42)
            java.lang.String r1 = I.I.I(r1)
            java.lang.String r1 = r7.getAttributeValue(r0, r1)
            if (r1 == 0) goto L_0x00da
            r5.___(r1)
            r1 = 3803(0xedb, float:5.329E-42)
            java.lang.String r1 = I.I.I(r1)
            r2 = 60
            int r1 = r7.getAttributeIntValue(r0, r1, r2)
            r5._(r1)
            r1 = 3814(0xee6, float:5.345E-42)
            java.lang.String r1 = I.I.I(r1)
            r2 = 0
            int r1 = r7.getAttributeIntValue(r0, r1, r2)
            r5.___(r1)
            r1 = 3824(0xef0, float:5.359E-42)
            java.lang.String r1 = I.I.I(r1)
            r2 = 1
            int r0 = r7.getAttributeIntValue(r0, r1, r2)
            r5.setAnimationType(r0)
        L_0x00a9:
            java.lang.String r0 = r5.getAdPosition()
            if (r0 != 0) goto L_0x00b4
            if (r8 == 0) goto L_0x00e5
            r5.___(r8)
        L_0x00b4:
            java.lang.String r0 = r5.getAdPosition()
            if (r0 == 0) goto L_0x0021
            r1 = 0
            java.util.List r2 = com.madhouse.android.ads.AdView.aa
            java.util.Iterator r2 = r2.iterator()
        L_0x00c1:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x00f3
            java.lang.Object r6 = r2.next()
            com.madhouse.android.ads.AdView r6 = (com.madhouse.android.ads.AdView) r6
            java.lang.String r3 = r6.getAdPosition()
            boolean r3 = r0.equalsIgnoreCase(r3)
            if (r3 == 0) goto L_0x00c1
            int r1 = r1 + 1
            goto L_0x00c1
        L_0x00da:
            r0 = 3775(0xebf, float:5.29E-42)
            java.lang.String r0 = I.I.I(r0)
            com.madhouse.android.ads.f.____(r0)
            goto L_0x0021
        L_0x00e5:
            r0 = 3775(0xebf, float:5.29E-42)
            java.lang.String r0 = I.I.I(r0)
            com.madhouse.android.ads.f.____(r0)
            r5._____()
            goto L_0x0021
        L_0x00f3:
            r2 = 2
            if (r1 < r2) goto L_0x0149
            r5._____()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r2 = 3842(0xf02, float:5.384E-42)
            java.lang.String r2 = I.I.I(r2)
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r0)
            r2 = 3882(0xf2a, float:5.44E-42)
            java.lang.String r2 = I.I.I(r2)
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.madhouse.android.ads.f.____(r1)
            r1 = 1865(0x749, float:2.613E-42)
            java.lang.String r1 = I.I.I(r1)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r3 = 3842(0xf02, float:5.384E-42)
            java.lang.String r3 = I.I.I(r3)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            r2 = 3882(0xf2a, float:5.44E-42)
            java.lang.String r2 = I.I.I(r2)
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            android.util.Log.e(r1, r0)
            goto L_0x0021
        L_0x0149:
            java.util.List r0 = com.madhouse.android.ads.AdView.aa
            r0.add(r5)
            int r0 = r5.$$
            r1 = -1
            if (r0 != r1) goto L_0x0156
            r5._(r9)
        L_0x0156:
            int r0 = r5.$$$
            r1 = -1
            if (r0 != r1) goto L_0x015e
            r5.___(r10)
        L_0x015e:
            int r0 = r5.$$$$
            r1 = -1
            if (r0 != r1) goto L_0x0166
            r5.setAnimationType(r11)
        L_0x0166:
            android.os.Handler r0 = new android.os.Handler
            r0.<init>()
            r5.___ = r0
            com.madhouse.android.ads.ccccc r0 = new com.madhouse.android.ads.ccccc
            android.content.Context r1 = r5.__
            r2 = 0
            r3 = 0
            r4 = 1
            r0.<init>(r1, r2, r3, r4)
            r5.cc = r0
            com.madhouse.android.ads.o r0 = new com.madhouse.android.ads.o
            r0.<init>(r5)
            r5.dd = r0
            com.madhouse.android.ads.o r0 = r5.dd
            com.madhouse.android.ads.bx r1 = r5.ee
            r0._(r1)
            com.madhouse.android.ads.al r0 = new com.madhouse.android.ads.al
            android.content.Context r1 = r5.__
            r0.<init>(r1)
            r5.ccc = r0
            com.madhouse.android.ads.al r0 = r5.ccc
            com.madhouse.android.ads.bx r1 = r5.ee
            r0._(r1)
            com.madhouse.android.ads.bj r0 = new com.madhouse.android.ads.bj
            android.content.Context r1 = r5.__
            android.os.Handler r2 = r5.___
            r0.<init>(r1, r2)
            r5.ccccc = r0
            com.madhouse.android.ads.bj r0 = r5.ccccc
            com.madhouse.android.ads.bx r1 = r5.ee
            r0._(r1)
            com.madhouse.android.ads.bf r0 = new com.madhouse.android.ads.bf
            android.content.Context r1 = r5.__
            r0.<init>(r1)
            r5.d = r0
            com.madhouse.android.ads.bf r0 = r5.d
            com.madhouse.android.ads.bx r1 = r5.ee
            r0._(r1)
            com.madhouse.android.ads.ff r0 = new com.madhouse.android.ads.ff
            r0.<init>()
            r5.cccc = r0
            com.madhouse.android.ads.ccccc r0 = r5.cc
            com.madhouse.android.ads.o r1 = r5.dd
            r2 = 1865(0x749, float:2.613E-42)
            java.lang.String r2 = I.I.I(r2)
            r0.addJavascriptInterface(r1, r2)
            com.madhouse.android.ads.ccccc r0 = r5.cc
            com.madhouse.android.ads.al r1 = r5.ccc
            r2 = 1872(0x750, float:2.623E-42)
            java.lang.String r2 = I.I.I(r2)
            r0.addJavascriptInterface(r1, r2)
            com.madhouse.android.ads.ccccc r0 = r5.cc
            com.madhouse.android.ads.bj r1 = r5.ccccc
            r2 = 1879(0x757, float:2.633E-42)
            java.lang.String r2 = I.I.I(r2)
            r0.addJavascriptInterface(r1, r2)
            com.madhouse.android.ads.ccccc r0 = r5.cc
            com.madhouse.android.ads.ff r1 = r5.cccc
            r2 = 1885(0x75d, float:2.641E-42)
            java.lang.String r2 = I.I.I(r2)
            r0.addJavascriptInterface(r1, r2)
            com.madhouse.android.ads.ccccc r0 = r5.cc
            com.madhouse.android.ads.bf r1 = r5.d
            r2 = 1889(0x761, float:2.647E-42)
            java.lang.String r2 = I.I.I(r2)
            r0.addJavascriptInterface(r1, r2)
            com.madhouse.android.ads.ccccc r0 = r5.cc
            com.madhouse.android.ads.n r1 = r5.eee
            r0.__ = r1
            android.widget.RelativeLayout$LayoutParams r0 = new android.widget.RelativeLayout$LayoutParams
            r1 = -2
            r2 = -2
            r0.<init>(r1, r2)
            r1 = 14
            r0.addRule(r1)
            r1 = 10
            r0.addRule(r1)
            com.madhouse.android.ads.ccccc r1 = r5.cc
            r5.addView(r1, r0)
            java.lang.Thread$UncaughtExceptionHandler r0 = java.lang.Thread.getDefaultUncaughtExceptionHandler()
            boolean r0 = r0 instanceof com.madhouse.android.ads.ah
            if (r0 != 0) goto L_0x022f
            com.madhouse.android.ads.ah r0 = new com.madhouse.android.ads.ah
            android.content.Context r1 = r5.__
            r0.<init>(r1)
            java.lang.Thread.setDefaultUncaughtExceptionHandler(r0)
        L_0x022f:
            int r0 = super.getVisibility()
            if (r0 != 0) goto L_0x0021
            r5._()
            r0 = 1
            r5._(r0)
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: com.madhouse.android.ads.AdView._(android.content.Context, android.util.AttributeSet, java.lang.String, int, int, int, boolean):void");
    }

    static final /* synthetic */ void _(AdView adView, int i, int i2, int i3) {
        if (adView.cc != null) {
            int i4 = adView.$$$$ - 3;
            float f2 = _[i4][2];
            float f3 = _[i4][3];
            __ __2 = new __(f2, f3, ((float) i) / 2.0f, ((float) i2) / 2.0f, -0.4f * ((float) i), false, 1);
            __2.setDuration(500);
            __2.setFillAfter(true);
            __2.setInterpolator(new DecelerateInterpolator());
            adView.cc.startAnimation(__2);
        }
    }

    static final /* synthetic */ void _(AdView adView, int i, int i2, boolean z) {
        if (adView.cc != null) {
            adView.$();
            if (adView.eeeee) {
                adView.eeeee = false;
                adView.cc._(i, i2);
                if (adView.___ != null) {
                    adView.___.post(new j(adView));
                    return;
                }
                return;
            }
            adView.cc._(i, i2);
        }
    }

    static final /* synthetic */ void _(AdView adView, String str, int i, int i2, int i3, boolean z, String str2) {
        adView.$();
        if (ddd == null && adView.___ != null) {
            adView.___.post(new ggg(adView, str, i, i2, i3, z, str2));
        }
    }

    static final /* synthetic */ void _(AdView adView, String str, boolean z) {
        boolean ____2 = AdManager.____();
        AdManager._(z);
        if (adView.___ != null) {
            adView.___.post(new k(adView, str, ____2));
        }
    }

    static final void _(String str) {
    }

    private static void _(StringBuffer stringBuffer, boolean z, String str, String str2) {
        String str3;
        if (str2 != null && str2.length() != 0) {
            try {
                str3 = str + I.I(2761) + URLEncoder.encode(str2, I.I(2763));
            } catch (Exception e2) {
                str3 = "";
            }
            if (str3 != null && str3.length() != 0) {
                stringBuffer.append(z ? I.I(2769) : I.I(2771)).append(str3);
            }
        }
    }

    /* access modifiers changed from: private */
    public static boolean _(Context context) {
        try {
            context.enforceCallingOrSelfPermission(I.I(3382), I.I(3418));
            context.enforceCallingOrSelfPermission(I.I(3498), I.I(3538));
            context.enforceCallingOrSelfPermission(I.I(3622), I.I(3650));
            return true;
        } catch (SecurityException e2) {
            f.____(e2.toString());
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.madhouse.android.ads.AdView._(java.lang.StringBuffer, boolean, java.lang.String, java.lang.String):void
     arg types: [java.lang.StringBuffer, int, java.lang.String, java.lang.String]
     candidates:
      com.madhouse.android.ads.AdView._(com.madhouse.android.ads.AdView, int, int, int):void
      com.madhouse.android.ads.AdView._(com.madhouse.android.ads.AdView, int, int, boolean):void
      com.madhouse.android.ads.AdView._(java.lang.StringBuffer, boolean, java.lang.String, java.lang.String):void */
    /* access modifiers changed from: private */
    public String __(String str) {
        int i;
        StringBuffer stringBuffer = new StringBuffer(1024);
        stringBuffer.append(str);
        _(stringBuffer, true, I.I(2203), getAdPosition());
        _(stringBuffer, false, I.I(2207), "" + this.$$);
        String I2 = I.I(2210);
        StringBuilder append = new StringBuilder().append("");
        switch (this.$$$) {
            case PHONE_AD_MEASURE_176 /*176*/:
                i = 1;
                break;
            case PHONE_AD_MEASURE_240 /*240*/:
                i = 2;
                break;
            case TABLET_AD_MEASURE_300 /*300*/:
                i = 7;
                break;
            case PHONE_AD_MEASURE_320 /*320*/:
                i = 3;
                break;
            case PHONE_AD_MEASURE_360 /*360*/:
                i = 4;
                break;
            case TABLET_AD_MEASURE_468 /*468*/:
                i = 8;
                break;
            case PHONE_AD_MEASURE_480 /*480*/:
                i = 5;
                break;
            case PHONE_AD_MEASURE_640 /*640*/:
                i = 6;
                break;
            case TABLET_AD_MEASURE_728 /*728*/:
                i = 9;
                break;
            default:
                i = 0;
                break;
        }
        _(stringBuffer, false, I2, append.append(i).toString());
        _(stringBuffer, false, I.I(2213), "" + this.$$$$);
        _(stringBuffer, false, I.I(2217), "" + $$$$());
        _(stringBuffer, false, I.I(2221), this.ccc.$());
        _(stringBuffer, false, I.I(2225), this.ccc.___());
        _(stringBuffer, false, I.I(2229), al.____() + I.I(679) + al._____());
        _(stringBuffer, false, I.I(2233), I.I(497));
        _(stringBuffer, false, I.I(2236), al.$$());
        _(stringBuffer, false, I.I(2240), this.cc.getSettings().getUserAgentString());
        _(stringBuffer, false, I.I(2243), "" + this.ccc.$$$());
        _(stringBuffer, false, I.I(2246), "" + this.ccc.$$$$());
        _(stringBuffer, false, I.I(2249), "" + this.ccc.$$$$$());
        _(stringBuffer, false, I.I(2252), "" + this.ccc.b());
        _(stringBuffer, false, I.I(2256), "" + this.ccc.aaaaa());
        Location _2 = this.ccc._();
        if (_2 != null) {
            _(stringBuffer, false, I.I(2260), "" + _2.getLongitude());
            _(stringBuffer, false, I.I(2264), "" + _2.getLatitude());
        }
        _(stringBuffer, false, I.I(2268), "" + this.ccc.bbb());
        _(stringBuffer, false, I.I(2272), "" + this.ccc.bbbb());
        _(stringBuffer, false, I.I(2276), "" + this.ccc.bbbbb());
        _(stringBuffer, false, I.I(2280), "" + this.ccc.c());
        _(stringBuffer, false, I.I(2284), "" + this.ccc.cc());
        _(stringBuffer, false, I.I(2288), "" + this.ccc.ccc());
        _(stringBuffer, false, I.I(2292), "" + this.ccc.cccc());
        _(stringBuffer, false, I.I(2296), "" + this.ccc.ccccc());
        _(stringBuffer, false, I.I(2300), "" + this.ccc.d());
        _(stringBuffer, false, I.I(2304), "" + this.ccc.dd());
        _(stringBuffer, false, I.I(2308), "" + this.ccc.ddd());
        _(stringBuffer, false, I.I(2312), "" + this.ccc.dddd());
        _(stringBuffer, false, I.I(2316), "" + this.ccccc._());
        _(stringBuffer, false, I.I(2320), "" + this.ccc.bb());
        _(stringBuffer, false, I.I(2323), this.ccc.a());
        _(stringBuffer, false, I.I(2327), this.ccc.aa());
        _(stringBuffer, false, I.I(2331), this.ccc.aaa());
        _(stringBuffer, false, I.I(2335), this.ccc.aaaa());
        _(stringBuffer, false, I.I(2339), this.e);
        _(stringBuffer, false, I.I(2343), AdManager._____());
        _(stringBuffer, false, I.I(2352), "" + (I.I(2347).equalsIgnoreCase(AdManager.___()) ? 1 : 2));
        _(stringBuffer, false, I.I(2356), AdManager.__());
        _(stringBuffer, false, I.I(2360), AdManager.$());
        _(stringBuffer, false, I.I(2363), AdManager.$$());
        _(stringBuffer, false, I.I(2367), AdManager.$$$());
        _(stringBuffer, false, I.I(2370), AdManager.$$$$());
        _(stringBuffer, false, I.I(2373), AdManager.$$$$$());
        _(stringBuffer, false, I.I(2376), al.__());
        _(stringBuffer, false, I.I(2380), I.I(2383));
        _(stringBuffer, false, I.I(2394), ah._(this.__));
        return stringBuffer.toString();
    }

    /* access modifiers changed from: private */
    public void __(int i) {
        if (this.$$$$$ != null) {
            this.$$$$$.onAdEvent(this, i);
        }
    }

    static final /* synthetic */ void __(AdView adView, int i, int i2, boolean z) {
        if (ddd != null) {
            ddd._(i, i2, z);
        }
    }

    private void ___(int i) {
        switch (i) {
            case 0:
            case PHONE_AD_MEASURE_176 /*176*/:
            case PHONE_AD_MEASURE_240 /*240*/:
            case TABLET_AD_MEASURE_300 /*300*/:
            case PHONE_AD_MEASURE_320 /*320*/:
            case PHONE_AD_MEASURE_360 /*360*/:
            case TABLET_AD_MEASURE_468 /*468*/:
            case PHONE_AD_MEASURE_480 /*480*/:
            case PHONE_AD_MEASURE_640 /*640*/:
            case TABLET_AD_MEASURE_728 /*728*/:
                this.$$$ = i;
                return;
            default:
                this.$$$ = 0;
                return;
        }
    }

    static final /* synthetic */ void ___(AdView adView, int i) {
        adView.$();
        adView.__(i);
        if (i == 1) {
            adView.aaaa++;
            if (adView.aaaa > 2 && !adView.aaa) {
                adView.setVisibility(8);
            }
        }
        System.gc();
    }

    static final /* synthetic */ void ___(AdView adView, String str) {
        if (adView.cc != null) {
            int width = adView.cc.getWidth();
            int height = adView.cc.getHeight();
            float f2 = ((float) width) / 2.0f;
            float f3 = ((float) height) / 2.0f;
            float f4 = -0.4f * ((float) width);
            int i = adView.$$$$ - 3;
            if (i >= 0 && i <= _.length - 1) {
                __ __2 = new __(_[i][0], _[i][1], f2, f3, f4, true, 1);
                __2.setDuration(500);
                __2.setFillAfter(true);
                __2.setInterpolator(new AccelerateInterpolator());
                __2.setAnimationListener(new g(adView, str, width, height));
                adView.cc.startAnimation(__2);
            }
        }
    }

    private void ___(String str) {
        this.$ = str;
        if (this.$ == null) {
            f.____(I.I(1963));
        }
    }

    /* access modifiers changed from: private */
    public void ____() {
        if (this.cc != null && getVisibility() == 0 && !this.bbbbb) {
            I.I(1992);
            this.cc._(this.bbbb);
            this.bbbbb = true;
        }
    }

    static final /* synthetic */ void ____(AdView adView, String str) {
        if (adView.___ != null) {
            adView.___.post(new l(adView, str));
        }
    }

    /* access modifiers changed from: private */
    public void _____() {
        _(false);
        this.a = true;
        this.bbbbb = true;
        this.c = true;
        this.bbb = true;
        this.bb = true;
    }

    static final /* synthetic */ void _____(AdView adView, String str) {
        if (adView.___ != null) {
            adView.___.post(new m(adView, str));
        }
    }

    static final /* synthetic */ void a(AdView adView) {
        View childAt;
        if (ddd != null) {
            adView.$$$();
            try {
                adView.ccccc.__();
                p._(ddd);
                ViewParent parent = ddd.getParent();
                if (parent != null) {
                    ((ViewGroup) parent).removeView(ddd);
                    ViewGroup viewGroup = (ViewGroup) parent;
                    int childCount = viewGroup.getChildCount() - 1;
                    while (true) {
                        if (childCount < 0) {
                            break;
                        }
                        childAt = viewGroup.getChildAt(childCount);
                        if (childAt.getVisibility() != 0 || (!(childAt instanceof p) && !(childAt instanceof ___))) {
                            childCount--;
                        }
                    }
                    childAt.requestFocus();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            dddd = 0;
            ddd = null;
            System.gc();
        }
    }

    static final /* synthetic */ void aa(AdView adView) {
        if (adView.cc != null) {
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration(500);
            alphaAnimation.startNow();
            alphaAnimation.setFillAfter(true);
            alphaAnimation.setInterpolator(new LinearInterpolator());
            adView.cc.startAnimation(alphaAnimation);
        }
    }

    static final /* synthetic */ void aaaa(AdView adView) {
        if ((ddd == null || !ddd.$$$) && !___.__()) {
            for (AdView adView2 : aa) {
                if (adView2.dd != null) {
                    adView2.dd.c_(false);
                }
            }
            return;
        }
        for (AdView adView3 : aa) {
            if (adView3.dd != null) {
                adView3.dd.c_(true);
            }
        }
    }

    static final /* synthetic */ void bbb(AdView adView) {
        if (ddd != null) {
            adView.ccccc.__();
            if (adView.___ != null) {
                adView.___.post(new gggg(adView));
            }
        }
    }

    static final /* synthetic */ void ccc(AdView adView) {
        Window window = ((Activity) adView.__).getWindow();
        int i = window.getAttributes().flags;
        window.setFlags(1024, 1024);
        if (i == window.getAttributes().flags) {
            adView.eeee = true;
        }
    }

    /* access modifiers changed from: protected */
    public final void _() {
        if (super.getVisibility() != 0) {
            f.___(I.I(2014));
        } else if (this.a) {
        } else {
            if (this.bb) {
                f.__(I.I(2042));
            } else if (this.bbb) {
                if (!this.c && this.cc != null && this.bbbb != null) {
                    ____();
                }
            } else if (this.ccc.bb() == -1) {
                f.____(I.I(2054));
            } else {
                this.bb = true;
                new h(this).start();
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void _(boolean z) {
        synchronized (this) {
            if (z) {
                if (this.$$ > 0 && !this.c && !this.a) {
                    if (this.____ == null) {
                        this.____ = new Timer();
                        this.____.schedule(new ggggg(this), (long) (this.$$ * 1000), (long) (this.$$ * 1000));
                    }
                }
            }
            if ((!z || this.$$ == 0) && this.____ != null) {
                this.____.cancel();
                this.____ = null;
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        if (hasFocus()) {
            Rect rect = new Rect(getPaddingLeft() + 0, getPaddingTop() + 0, (getWidth() - getPaddingLeft()) - getPaddingRight(), (getHeight() - getPaddingTop()) - getPaddingBottom());
            Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setColor(-1147097);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(5.0f);
            paint.setPathEffect(new CornerPathEffect(5.0f));
            Path path = new Path();
            path.addRoundRect(new RectF(rect), 5.0f, 5.0f, Path.Direction.CW);
            canvas.drawPath(path, paint);
        }
    }

    public final String getAdPosition() {
        return this.$;
    }

    public final int getVisibility() {
        return super.getVisibility();
    }

    /* access modifiers changed from: protected */
    public final void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.aaa = true;
        if (!this.a && this.bbb && !this.c && !this.bbbbb) {
            ____();
        }
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        _(false);
        this.aaa = false;
        aa.remove(this);
        if (this.c && this.dd != null) {
            this.dd.a_(false);
        }
        if (this.ccccc != null) {
            bj bjVar = this.ccccc;
            if (!(bjVar.$$ == null || bjVar.$$.length == 0)) {
                for (String str : bjVar.$$) {
                    if (!(str == null || str.length() == 0)) {
                        az.__(bjVar._, str);
                    }
                }
            }
        }
        this.dd = null;
        this.ccc = null;
        this.d = null;
        this.cccc = null;
        this.cc = null;
        if (this.___ != null) {
            this.___.postDelayed(new i(this), 500);
        }
    }

    /* access modifiers changed from: protected */
    public final void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        if (this.cc != null) {
            if (i >= 0 && i2 >= 0 && i3 <= this.ccc.$$$() && i4 <= this.ccc.$$$$()) {
                int width = getWidth();
                int height = getHeight();
                if (width >= 0 && height >= 0) {
                    int width2 = this.cc.getWidth();
                    int height2 = this.cc.getHeight();
                    if (width >= width2 && height >= height2) {
                        return;
                    }
                    if (f._) {
                        f.___(this.$ + I.I(1733));
                        return;
                    }
                    Log.w(I.I(813), this.$ + I.I(1733));
                    setVisibility(8);
                }
            } else if (f._) {
                f.___(this.$ + I.I(1795));
            } else {
                Log.w(I.I(813), this.$ + I.I(1795));
                setVisibility(8);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (this.c && this.ccc != null) {
            this.ccc.b_();
        }
    }

    public final void onWindowFocusChanged(boolean z) {
        _(z);
    }

    /* access modifiers changed from: protected */
    public final void onWindowVisibilityChanged(int i) {
        super.onWindowVisibilityChanged(i);
        if (getVisibility() == 0) {
            if (this.c && this.dd != null) {
                this.dd.a_(i == 0);
            }
            _(i == 0);
        }
    }

    public final void setAnimationType(int i) {
        if (i < 0 || i > 8) {
            this.$$$$ = 1;
        } else {
            this.$$$$ = i;
        }
    }

    public final void setListener(AdListener adListener) {
        synchronized (this) {
            this.$$$$$ = adListener;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void setVisibility(int r4) {
        /*
            r3 = this;
            r1 = 0
            int r0 = super.getVisibility()
            if (r0 == r4) goto L_0x0022
            monitor-enter(r3)
            int r0 = r3.getChildCount()     // Catch:{ all -> 0x0033 }
        L_0x000c:
            if (r1 >= r0) goto L_0x0018
            android.view.View r2 = r3.getChildAt(r1)     // Catch:{ all -> 0x0033 }
            r2.setVisibility(r4)     // Catch:{ all -> 0x0033 }
            int r1 = r1 + 1
            goto L_0x000c
        L_0x0018:
            super.setVisibility(r4)     // Catch:{ all -> 0x0033 }
            if (r4 != 0) goto L_0x003e
            boolean r0 = r3.a     // Catch:{ all -> 0x0033 }
            if (r0 == 0) goto L_0x0023
            monitor-exit(r3)     // Catch:{ all -> 0x0033 }
        L_0x0022:
            return
        L_0x0023:
            boolean r0 = r3.c     // Catch:{ all -> 0x0033 }
            if (r0 == 0) goto L_0x0036
            com.madhouse.android.ads.o r0 = r3.dd     // Catch:{ all -> 0x0033 }
            if (r0 == 0) goto L_0x0031
            com.madhouse.android.ads.o r0 = r3.dd     // Catch:{ all -> 0x0033 }
            r1 = 1
            r0.a_(r1)     // Catch:{ all -> 0x0033 }
        L_0x0031:
            monitor-exit(r3)     // Catch:{ all -> 0x0033 }
            goto L_0x0022
        L_0x0033:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        L_0x0036:
            r3._()     // Catch:{ all -> 0x0033 }
            r0 = 1
            r3._(r0)     // Catch:{ all -> 0x0033 }
            goto L_0x0031
        L_0x003e:
            boolean r0 = r3.c     // Catch:{ all -> 0x0033 }
            if (r0 == 0) goto L_0x004d
            com.madhouse.android.ads.o r0 = r3.dd     // Catch:{ all -> 0x0033 }
            if (r0 == 0) goto L_0x0031
            com.madhouse.android.ads.o r0 = r3.dd     // Catch:{ all -> 0x0033 }
            r1 = 0
            r0.a_(r1)     // Catch:{ all -> 0x0033 }
            goto L_0x0031
        L_0x004d:
            r0 = 0
            r3._(r0)     // Catch:{ all -> 0x0033 }
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.madhouse.android.ads.AdView.setVisibility(int):void");
    }
}
