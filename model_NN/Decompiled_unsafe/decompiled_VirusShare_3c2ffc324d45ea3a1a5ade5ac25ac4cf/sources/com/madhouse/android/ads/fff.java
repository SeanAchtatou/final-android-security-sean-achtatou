package com.madhouse.android.ads;

import I.I;
import android.content.Context;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Timer;

class fff implements bx {
    final /* synthetic */ AdView _;

    private fff() {
    }

    fff(AdView adView) {
        this._ = adView;
    }

    protected static final bg _(Context context, String str) {
        byte[] _2 = n._(context, I.I(3338));
        if (I.I(2148).equals(_2 != null ? new String(_2) : null)) {
            f.____(I.I(3226));
            return new bg(I.I(2148));
        }
        ak _3 = _._(context);
        if (_3._ != 0) {
            bg _4 = _(str, _3.__, _3.___);
            String str2 = _4._;
            if (str2.equals(I.I(2148))) {
                f.___(I.I(3258));
                __(context, I.I(2148));
                return _4;
            } else if (str2.equals(I.I(2152))) {
                f.___(I.I(3289));
                __(context, I.I(2152));
                return _4;
            } else if (!str2.startsWith(I.I(3320))) {
                return _4;
            } else {
                f.____(I.I(3326) + str2);
                return _4;
            }
        } else {
            f.___(I.I(2851));
            return new bg(I.I(2156));
        }
    }

    private static bg _(InputStream inputStream, int i, int i2) {
        Timer timer;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(Math.max(1024, i));
        byte[] bArr = new byte[1024];
        by byVar = new by();
        if (i <= 0 || i2 <= 0) {
            timer = null;
        } else {
            Timer timer2 = new Timer();
            timer2.schedule(new ffff(byVar), (long) i2);
            timer = timer2;
        }
        long currentTimeMillis = System.currentTimeMillis();
        int i3 = 0;
        while (true) {
            try {
                int read = inputStream.read(bArr);
                if (read < 0) {
                    break;
                }
                byteArrayOutputStream.write(bArr, 0, read);
                i3 += read;
                if (i > 0 && i3 > i && byVar._) {
                    break;
                }
            } catch (Exception e) {
            }
        }
        long currentTimeMillis2 = System.currentTimeMillis();
        if (timer != null) {
            timer.cancel();
        }
        return new bg(new String(byteArrayOutputStream.toByteArray()), currentTimeMillis, currentTimeMillis2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:51:0x0108 A[Catch:{ Exception -> 0x0162 }] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x010e  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0151 A[SYNTHETIC, Splitter:B:59:0x0151] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0156  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x01b1 A[SYNTHETIC, Splitter:B:93:0x01b1] */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x01b6  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.madhouse.android.ads.bg _(java.lang.String r11, java.lang.String r12, java.lang.String r13) {
        /*
            r9 = 0
            r8 = 10000(0x2710, float:1.4013E-41)
            r7 = 2931(0xb73, float:4.107E-42)
            r5 = -1
            r6 = 3033(0xbd9, float:4.25E-42)
            if (r11 == 0) goto L_0x01d1
            java.net.URL r0 = new java.net.URL     // Catch:{ MalformedURLException -> 0x009a }
            r0.<init>(r11)     // Catch:{ MalformedURLException -> 0x009a }
        L_0x000f:
            if (r12 == 0) goto L_0x00b2
            int r1 = r12.length()
            r2 = 7
            if (r1 < r2) goto L_0x00b2
            if (r13 == 0) goto L_0x00b2
            java.util.Properties r1 = java.lang.System.getProperties()
            java.util.Properties r2 = java.lang.System.getProperties()
            r3 = 2887(0xb47, float:4.046E-42)
            java.lang.String r3 = I.I.I(r3)
            r4 = 2896(0xb50, float:4.058E-42)
            java.lang.String r4 = I.I.I(r4)
            r2.put(r3, r4)
            r2 = 2901(0xb55, float:4.065E-42)
            java.lang.String r2 = I.I.I(r2)
            r1.setProperty(r2, r12)
            r2 = 2916(0xb64, float:4.086E-42)
            java.lang.String r2 = I.I.I(r2)
            r1.setProperty(r2, r13)
            java.net.URLConnection r11 = r0.openConnection()     // Catch:{ IOException -> 0x00a7 }
            java.net.HttpURLConnection r11 = (java.net.HttpURLConnection) r11     // Catch:{ IOException -> 0x00a7 }
            r0 = r11
        L_0x004a:
            r1 = 2949(0xb85, float:4.132E-42)
            java.lang.String r1 = I.I.I(r1)     // Catch:{ ProtocolException -> 0x00d2 }
            r0.setRequestMethod(r1)     // Catch:{ ProtocolException -> 0x00d2 }
            r0.setConnectTimeout(r8)
            r0.setReadTimeout(r8)
            r1 = 1
            r0.setInstanceFollowRedirects(r1)
            r0.connect()     // Catch:{ IOException -> 0x00df }
            int r1 = r0.getResponseCode()     // Catch:{ IOException -> 0x0187, all -> 0x01ad }
            r2 = 200(0xc8, float:2.8E-43)
            if (r1 != r2) goto L_0x0174
            java.lang.String r1 = r0.getContentType()     // Catch:{ IOException -> 0x0187, all -> 0x01ad }
            r2 = 2980(0xba4, float:4.176E-42)
            java.lang.String r2 = I.I.I(r2)     // Catch:{ IOException -> 0x0187, all -> 0x01ad }
            boolean r1 = r1.startsWith(r2)     // Catch:{ IOException -> 0x0187, all -> 0x01ad }
            if (r1 != 0) goto L_0x0088
            java.lang.String r1 = r0.getContentType()     // Catch:{ IOException -> 0x0187, all -> 0x01ad }
            r2 = 3005(0xbbd, float:4.211E-42)
            java.lang.String r2 = I.I.I(r2)     // Catch:{ IOException -> 0x0187, all -> 0x01ad }
            boolean r1 = r1.startsWith(r2)     // Catch:{ IOException -> 0x0187, all -> 0x01ad }
            if (r1 == 0) goto L_0x00ec
        L_0x0088:
            com.madhouse.android.ads.bg r1 = new com.madhouse.android.ads.bg     // Catch:{ IOException -> 0x0187, all -> 0x01ad }
            r2 = 3022(0xbce, float:4.235E-42)
            java.lang.String r2 = I.I.I(r2)     // Catch:{ IOException -> 0x0187, all -> 0x01ad }
            r1.<init>(r2)     // Catch:{ IOException -> 0x0187, all -> 0x01ad }
            if (r0 == 0) goto L_0x0098
            r0.disconnect()
        L_0x0098:
            r0 = r1
        L_0x0099:
            return r0
        L_0x009a:
            r0 = move-exception
            com.madhouse.android.ads.bg r0 = new com.madhouse.android.ads.bg
            r1 = 2877(0xb3d, float:4.032E-42)
            java.lang.String r1 = I.I.I(r1)
            r0.<init>(r1)
            goto L_0x0099
        L_0x00a7:
            r0 = move-exception
            com.madhouse.android.ads.bg r0 = new com.madhouse.android.ads.bg
            java.lang.String r1 = I.I.I(r7)
            r0.<init>(r1)
            goto L_0x0099
        L_0x00b2:
            java.net.URLConnection r11 = r0.openConnection()     // Catch:{ IOException -> 0x00c7 }
            java.net.HttpURLConnection r11 = (java.net.HttpURLConnection) r11     // Catch:{ IOException -> 0x00c7 }
            r1 = 2944(0xb80, float:4.125E-42)
            java.lang.String r1 = I.I.I(r1)
            java.lang.String r0 = r0.getHost()
            r11.setRequestProperty(r1, r0)
            r0 = r11
            goto L_0x004a
        L_0x00c7:
            r0 = move-exception
            com.madhouse.android.ads.bg r0 = new com.madhouse.android.ads.bg
            java.lang.String r1 = I.I.I(r7)
            r0.<init>(r1)
            goto L_0x0099
        L_0x00d2:
            r0 = move-exception
            com.madhouse.android.ads.bg r0 = new com.madhouse.android.ads.bg
            r1 = 2953(0xb89, float:4.138E-42)
            java.lang.String r1 = I.I.I(r1)
            r0.<init>(r1)
            goto L_0x0099
        L_0x00df:
            r0 = move-exception
            com.madhouse.android.ads.bg r0 = new com.madhouse.android.ads.bg
            r1 = 2966(0xb96, float:4.156E-42)
            java.lang.String r1 = I.I.I(r1)
            r0.<init>(r1)
            goto L_0x0099
        L_0x00ec:
            r1 = 3045(0xbe5, float:4.267E-42)
            java.lang.String r1 = I.I.I(r1)     // Catch:{ Exception -> 0x015c }
            java.lang.String r1 = r0.getHeaderField(r1)     // Catch:{ Exception -> 0x015c }
            if (r1 == 0) goto L_0x0160
            int r1 = java.lang.Integer.parseInt(r1)     // Catch:{ Exception -> 0x015c }
        L_0x00fc:
            r2 = 3057(0xbf1, float:4.284E-42)
            java.lang.String r2 = I.I.I(r2)     // Catch:{ Exception -> 0x0162 }
            java.lang.String r2 = r0.getHeaderField(r2)     // Catch:{ Exception -> 0x0162 }
            if (r2 == 0) goto L_0x0166
            int r2 = java.lang.Integer.parseInt(r2)     // Catch:{ Exception -> 0x0162 }
        L_0x010c:
            if (r2 > 0) goto L_0x0110
            r2 = 500(0x1f4, float:7.0E-43)
        L_0x0110:
            r0.getURL()     // Catch:{ IOException -> 0x0187, all -> 0x01ad }
            java.io.InputStream r3 = r0.getInputStream()     // Catch:{ IOException -> 0x0187, all -> 0x01ad }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x01ce, all -> 0x01c6 }
            r4.<init>()     // Catch:{ IOException -> 0x01ce, all -> 0x01c6 }
            r5 = 3071(0xbff, float:4.303E-42)
            java.lang.String r5 = I.I.I(r5)     // Catch:{ IOException -> 0x01ce, all -> 0x01c6 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IOException -> 0x01ce, all -> 0x01c6 }
            java.lang.StringBuilder r4 = r4.append(r2)     // Catch:{ IOException -> 0x01ce, all -> 0x01c6 }
            java.lang.String r4 = r4.toString()     // Catch:{ IOException -> 0x01ce, all -> 0x01c6 }
            com.madhouse.android.ads.AdView._(r4)     // Catch:{ IOException -> 0x01ce, all -> 0x01c6 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x01ce, all -> 0x01c6 }
            r4.<init>()     // Catch:{ IOException -> 0x01ce, all -> 0x01c6 }
            r5 = 3086(0xc0e, float:4.324E-42)
            java.lang.String r5 = I.I.I(r5)     // Catch:{ IOException -> 0x01ce, all -> 0x01c6 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IOException -> 0x01ce, all -> 0x01c6 }
            java.lang.StringBuilder r4 = r4.append(r1)     // Catch:{ IOException -> 0x01ce, all -> 0x01c6 }
            java.lang.String r4 = r4.toString()     // Catch:{ IOException -> 0x01ce, all -> 0x01c6 }
            com.madhouse.android.ads.AdView._(r4)     // Catch:{ IOException -> 0x01ce, all -> 0x01c6 }
            com.madhouse.android.ads.bg r1 = _(r3, r1, r2)     // Catch:{ IOException -> 0x01ce, all -> 0x01c6 }
            if (r3 == 0) goto L_0x0154
            r3.close()     // Catch:{ IOException -> 0x0168 }
        L_0x0154:
            if (r0 == 0) goto L_0x0159
            r0.disconnect()
        L_0x0159:
            r0 = r1
            goto L_0x0099
        L_0x015c:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ IOException -> 0x0187, all -> 0x01ad }
        L_0x0160:
            r1 = r5
            goto L_0x00fc
        L_0x0162:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ IOException -> 0x0187, all -> 0x01ad }
        L_0x0166:
            r2 = r5
            goto L_0x010c
        L_0x0168:
            r0 = move-exception
            com.madhouse.android.ads.bg r0 = new com.madhouse.android.ads.bg
            java.lang.String r1 = I.I.I(r6)
            r0.<init>(r1)
            goto L_0x0099
        L_0x0174:
            com.madhouse.android.ads.bg r1 = new com.madhouse.android.ads.bg     // Catch:{ IOException -> 0x0187, all -> 0x01ad }
            r2 = 3099(0xc1b, float:4.343E-42)
            java.lang.String r2 = I.I.I(r2)     // Catch:{ IOException -> 0x0187, all -> 0x01ad }
            r1.<init>(r2)     // Catch:{ IOException -> 0x0187, all -> 0x01ad }
            if (r0 == 0) goto L_0x0184
            r0.disconnect()
        L_0x0184:
            r0 = r1
            goto L_0x0099
        L_0x0187:
            r1 = move-exception
            r1 = r9
        L_0x0189:
            com.madhouse.android.ads.bg r2 = new com.madhouse.android.ads.bg     // Catch:{ all -> 0x01c9 }
            r3 = 3110(0xc26, float:4.358E-42)
            java.lang.String r3 = I.I.I(r3)     // Catch:{ all -> 0x01c9 }
            r2.<init>(r3)     // Catch:{ all -> 0x01c9 }
            if (r1 == 0) goto L_0x0199
            r1.close()     // Catch:{ IOException -> 0x01a1 }
        L_0x0199:
            if (r0 == 0) goto L_0x019e
            r0.disconnect()
        L_0x019e:
            r0 = r2
            goto L_0x0099
        L_0x01a1:
            r0 = move-exception
            com.madhouse.android.ads.bg r0 = new com.madhouse.android.ads.bg
            java.lang.String r1 = I.I.I(r6)
            r0.<init>(r1)
            goto L_0x0099
        L_0x01ad:
            r1 = move-exception
            r2 = r9
        L_0x01af:
            if (r2 == 0) goto L_0x01b4
            r2.close()     // Catch:{ IOException -> 0x01ba }
        L_0x01b4:
            if (r0 == 0) goto L_0x01b9
            r0.disconnect()
        L_0x01b9:
            throw r1
        L_0x01ba:
            r0 = move-exception
            com.madhouse.android.ads.bg r0 = new com.madhouse.android.ads.bg
            java.lang.String r1 = I.I.I(r6)
            r0.<init>(r1)
            goto L_0x0099
        L_0x01c6:
            r1 = move-exception
            r2 = r3
            goto L_0x01af
        L_0x01c9:
            r2 = move-exception
            r10 = r2
            r2 = r1
            r1 = r10
            goto L_0x01af
        L_0x01ce:
            r1 = move-exception
            r1 = r3
            goto L_0x0189
        L_0x01d1:
            r0 = r9
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.madhouse.android.ads.fff._(java.lang.String, java.lang.String, java.lang.String):com.madhouse.android.ads.bg");
    }

    private static void __(Context context, String str) {
        n._(context, I.I(3338), str.getBytes());
    }

    public void onCallback(String str) {
        if (this._.cc != null) {
            this._.cc._(str);
        }
    }
}
