package com.madhouse.android.ads;

import I.I;
import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Vibrator;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.util.DisplayMetrics;
import java.io.File;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;

final class al {
    private static String[] $$$ = {I.I(1143), I.I(1151), I.I(1156), I.I(1161), I.I(1166), I.I(1171), I.I(1178), I.I(1185), I.I(1191), I.I(1197), I.I(1203), I.I(1208)};
    private static Vibrator bbbbb = null;
    private static Timer c = null;
    private bx $ = null;
    private int $$ = 0;
    private ay $$$$ = new am(this);
    private ay $$$$$ = new aq(this);
    private Context _;
    private TelephonyManager __ = null;
    private SensorManager ___ = null;
    private int[] ____ = null;
    private boolean _____ = false;
    private ay a = new ar(this);
    private ay aa = new as(this);
    private ay aaa = new at(this);
    private ay aaaa = new au(this);
    private ay aaaaa = new av(this);
    private ay b = new aw(this);
    private ay bb = new ax(this);
    private ay bbb = new an(this);
    private ay bbbb = new ao(this);

    static String $$() {
        return Build.VERSION.RELEASE;
    }

    al(Context context) {
        int i = 0;
        this._ = context;
        this.___ = (SensorManager) context.getSystemService(I.I(1930));
        this.__ = (TelephonyManager) context.getSystemService(I.I(1937));
        List<Sensor> sensorList = this.___.getSensorList(-1);
        this.____ = new int[sensorList.size()];
        for (Sensor type : sensorList) {
            this.____[i] = type.getType();
            i++;
        }
        this._____ = ddddd();
    }

    private int _(int i) {
        if (this.____ == null) {
            return 0;
        }
        for (int i2 : this.____) {
            if (i2 == i) {
                return 1;
            }
        }
        return 0;
    }

    /* access modifiers changed from: private */
    public void _(String str) {
        if (this.$ != null) {
            this.$.onCallback(str);
        }
    }

    static String __() {
        return Calendar.getInstance().getTimeZone().getDisplayName(true, 0);
    }

    static String ____() {
        return Build.BRAND;
    }

    static String _____() {
        return Build.MODEL;
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0046 A[ADDED_TO_REGION] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean ddddd() {
        /*
            r9 = this;
            r8 = 0
            r7 = 3
            java.lang.String r0 = android.os.Build.VERSION.SDK     // Catch:{ Exception -> 0x004a }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ Exception -> 0x004a }
            android.content.Context r1 = r9._     // Catch:{ Exception -> 0x0055 }
            android.content.pm.PackageManager r1 = r1.getPackageManager()     // Catch:{ Exception -> 0x0055 }
            android.content.Context r2 = r9._     // Catch:{ Exception -> 0x0055 }
            java.lang.String r2 = r2.getPackageName()     // Catch:{ Exception -> 0x0055 }
            r3 = 1024(0x400, float:1.435E-42)
            android.content.pm.ApplicationInfo r1 = r1.getApplicationInfo(r2, r3)     // Catch:{ Exception -> 0x0055 }
            java.lang.Class r2 = r1.getClass()     // Catch:{ Exception -> 0x0055 }
            java.lang.reflect.Field[] r2 = r2.getFields()     // Catch:{ Exception -> 0x0055 }
            r3 = 1943(0x797, float:2.723E-42)
            java.lang.String r3 = I.I.I(r3)     // Catch:{ Exception -> 0x0055 }
            r4 = r8
            r5 = r7
        L_0x002a:
            int r6 = r2.length     // Catch:{ Exception -> 0x005a }
            if (r4 >= r6) goto L_0x0042
            r6 = r2[r4]     // Catch:{ Exception -> 0x005a }
            java.lang.String r6 = r6.getName()     // Catch:{ Exception -> 0x005a }
            boolean r6 = r3.equals(r6)     // Catch:{ Exception -> 0x005a }
            if (r6 == 0) goto L_0x003f
            r6 = r2[r4]     // Catch:{ Exception -> 0x005a }
            int r5 = r6.getInt(r1)     // Catch:{ Exception -> 0x005a }
        L_0x003f:
            int r4 = r4 + 1
            goto L_0x002a
        L_0x0042:
            r1 = r0
            r0 = r5
        L_0x0044:
            if (r1 <= r7) goto L_0x0053
            if (r0 <= r7) goto L_0x0053
            r0 = 1
        L_0x0049:
            return r0
        L_0x004a:
            r0 = move-exception
            r1 = r7
            r2 = r7
        L_0x004d:
            r0.printStackTrace()
            r0 = r1
            r1 = r2
            goto L_0x0044
        L_0x0053:
            r0 = r8
            goto L_0x0049
        L_0x0055:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r7
            goto L_0x004d
        L_0x005a:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r5
            goto L_0x004d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.madhouse.android.ads.al.ddddd():boolean");
    }

    private final int e() {
        try {
            this._.enforceCallingOrSelfPermission(I.I(2538), I.I(2578));
            return 1;
        } catch (SecurityException e) {
            f.____(e.toString());
            return 0;
        }
    }

    private final int ee() {
        try {
            this._.enforceCallingOrSelfPermission(I.I(2397), I.I(2439));
            return 1;
        } catch (SecurityException e) {
            f.____(e.toString());
            return 0;
        }
    }

    /* access modifiers changed from: private */
    public void eee() {
        if (c != null) {
            c.cancel();
            c = null;
        }
        if (bbbbb != null) {
            bbbbb.cancel();
            bbbbb = null;
        }
    }

    /* access modifiers changed from: package-private */
    public final String $() {
        return this.__.getDeviceId();
    }

    /* access modifiers changed from: package-private */
    public final int $$$() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) this._).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }

    /* access modifiers changed from: package-private */
    public final int $$$$() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) this._).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    /* access modifiers changed from: package-private */
    public final float $$$$$() {
        if (!this._____) {
            return 1.0f;
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) this._).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.density;
    }

    /* access modifiers changed from: package-private */
    public final Location _() {
        if (e() == 0) {
            return null;
        }
        return ((LocationManager) this._.getSystemService(I.I(2525))).getLastKnownLocation(I.I(2534));
    }

    /* access modifiers changed from: package-private */
    public final void _(bx bxVar) {
        if (bxVar != null) {
            this.$ = bxVar;
        }
    }

    /* access modifiers changed from: package-private */
    public final String ___() {
        String string = Settings.Secure.getString(this._.getContentResolver(), I.I(2750));
        return (string == null || string.length() == 0) ? this.__.getDeviceId() : string;
    }

    public final String _a() {
        return this.__.getDeviceId();
    }

    public final void _b(int i, String str) {
        if (_(1) != 0) {
            this.$$$$._ = str;
            this.___.registerListener(this.$$$$, this.___.getDefaultSensor(1), i);
        }
    }

    public final void _c() {
        this.___.unregisterListener(this.$$$$);
    }

    public final void _d(int i, String str) {
        if (_(2) != 0) {
            this.$$$$$._ = str;
            this.___.registerListener(this.$$$$$, this.___.getDefaultSensor(2), i);
        }
    }

    public final void _e() {
        this.___.unregisterListener(this.$$$$$);
    }

    public final void _f(int i, String str) {
        if (_(3) != 0) {
            this.a._ = str;
            this.___.registerListener(this.a, this.___.getDefaultSensor(3), i);
        }
    }

    public final void _g() {
        this.___.unregisterListener(this.a);
    }

    public final void _h(int i, String str) {
        if (_(4) != 0) {
            this.aa._ = str;
            this.___.registerListener(this.aa, this.___.getDefaultSensor(4), i);
        }
    }

    public final void _i() {
        this.___.unregisterListener(this.aa);
    }

    public final void _j(int i, String str) {
        if (_(5) != 0) {
            this.aaa._ = str;
            this.___.registerListener(this.aaa, this.___.getDefaultSensor(5), i);
        }
    }

    public final void _k() {
        this.___.unregisterListener(this.aaa);
    }

    public final void _l(int i, String str) {
        if (_(6) != 0) {
            this.aaaa._ = str;
            this.___.registerListener(this.aaaa, this.___.getDefaultSensor(6), i);
        }
    }

    public final void _m() {
        this.___.unregisterListener(this.aaaa);
    }

    public final void _n(int i, String str) {
        if (_(7) != 0) {
            this.aaaaa._ = str;
            this.___.registerListener(this.aaaaa, this.___.getDefaultSensor(7), i);
        }
    }

    public final void _o() {
        this.___.unregisterListener(this.aaaaa);
    }

    public final void _p(int i, String str) {
        if (_(8) != 0) {
            this.b._ = str;
            this.___.registerListener(this.b, this.___.getDefaultSensor(8), i);
        }
    }

    public final void _q() {
        this.___.unregisterListener(this.b);
    }

    public final void _r(int i, String str) {
        if (_(9) != 0) {
            this.bb._ = str;
            this.___.registerListener(this.bb, this.___.getDefaultSensor(9), i);
        }
    }

    public final void _s() {
        this.___.unregisterListener(this.bb);
    }

    public final void _t(int i, String str) {
        if (_(10) != 0) {
            this.bbb._ = str;
            this.___.registerListener(this.bbb, this.___.getDefaultSensor(10), i);
        }
    }

    public final void _u() {
        this.___.unregisterListener(this.bbb);
    }

    public final void _v(int i, String str) {
        if (_(11) != 0) {
            this.bbbb._ = str;
            this.___.registerListener(this.bbbb, this.___.getDefaultSensor(11), i);
        }
    }

    public final void _w() {
        this.___.unregisterListener(this.bbbb);
    }

    public final void _x(long[] jArr, int i, int i2) {
        if (dddd() != 0) {
            eee();
            Vibrator vibrator = (Vibrator) this._.getSystemService(I.I(1213));
            bbbbb = vibrator;
            if (vibrator != null) {
                bbbbb.vibrate(jArr, i);
                if (i != -1 && i < jArr.length - 1 && i2 != 0) {
                    Timer timer = new Timer();
                    c = timer;
                    timer.schedule(new ap(this), (long) i2);
                }
            }
        }
    }

    public final void _y() {
        eee();
    }

    /* access modifiers changed from: package-private */
    public final String a() {
        return "" + this._.getResources().getConfiguration().mcc;
    }

    /* access modifiers changed from: package-private */
    public final String aa() {
        return "" + this._.getResources().getConfiguration().mnc;
    }

    /* access modifiers changed from: package-private */
    public final String aaa() {
        try {
            if (ee() == 0) {
                return null;
            }
            GsmCellLocation gsmCellLocation = (GsmCellLocation) this.__.getCellLocation();
            if (gsmCellLocation == null) {
                return null;
            }
            return "" + gsmCellLocation.getLac();
        } catch (Exception e) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public final String aaaa() {
        try {
            if (ee() == 0) {
                return null;
            }
            GsmCellLocation gsmCellLocation = (GsmCellLocation) this.__.getCellLocation();
            if (gsmCellLocation == null) {
                return null;
            }
            return "" + gsmCellLocation.getCid();
        } catch (Exception e) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public final long aaaaa() {
        String replaceAll;
        String[] split = new String(n._(new File(I.I(2662)))).split(I.I(2676));
        int i = 0;
        int i2 = 0;
        for (int i3 = 0; i3 < split.length; i3++) {
            if (split[i3].startsWith(I.I(2678))) {
                String replaceAll2 = split[i3].replaceAll(I.I(2686), "");
                if (!(replaceAll2 == null || replaceAll2.length() == 0)) {
                    try {
                        i2 = Integer.parseInt(replaceAll2);
                    } catch (Exception e) {
                        i2 = 0;
                    }
                }
            } else if (!(!split[i3].startsWith(I.I(2689)) || (replaceAll = split[i3].replaceAll(I.I(2686), "")) == null || replaceAll.length() == 0)) {
                try {
                    i = Integer.parseInt(replaceAll);
                } catch (Exception e2) {
                    i = 0;
                }
            }
        }
        return (long) (i2 + i);
    }

    /* access modifiers changed from: package-private */
    public final int b() {
        try {
            String str = new String(n._(new File(I.I(2696)))).split(I.I(2676))[0];
            if (str == null || str.length() == 0) {
                return 0;
            }
            return Integer.parseInt(str) / 1000;
        } catch (Exception e) {
            return 0;
        }
    }

    public final void b_() {
        int i = this._.getResources().getConfiguration().orientation == 2 ? 2 : 1;
        if (!(this.$$ == 0 || this.$$ == i)) {
            _(I.I(1320) + i + I.I(184));
        }
        this.$$ = i;
    }

    /* access modifiers changed from: package-private */
    public final int bb() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this._.getSystemService(I.I(3213))).getActiveNetworkInfo();
        String str = $$$[this.__.getNetworkType()];
        if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
            return -1;
        }
        if (activeNetworkInfo.getType() == 1) {
            return 3;
        }
        if (activeNetworkInfo.getType() == 0) {
            if (I.I(1151).equals(str) || I.I(1156).equals(str)) {
                return 0;
            }
            if (I.I(1161).equals(str) || I.I(1171).equals(str) || I.I(1178).equals(str) || I.I(1191).equals(str) || I.I(1197).equals(str) || I.I(1203).equals(str)) {
                return 2;
            }
        }
        return 3;
    }

    /* access modifiers changed from: package-private */
    public final int bbb() {
        return _(1);
    }

    /* access modifiers changed from: package-private */
    public final int bbbb() {
        return _(2);
    }

    /* access modifiers changed from: package-private */
    public final int bbbbb() {
        return _(3);
    }

    /* access modifiers changed from: package-private */
    public final int c() {
        return _(4);
    }

    /* access modifiers changed from: package-private */
    public final int cc() {
        return _(5);
    }

    /* access modifiers changed from: package-private */
    public final int ccc() {
        return _(6);
    }

    /* access modifiers changed from: package-private */
    public final int cccc() {
        return _(7);
    }

    /* access modifiers changed from: package-private */
    public final int ccccc() {
        return _(8);
    }

    /* access modifiers changed from: package-private */
    public final int d() {
        return _(9);
    }

    /* access modifiers changed from: package-private */
    public final int dd() {
        return _(10);
    }

    /* access modifiers changed from: package-private */
    public final int ddd() {
        return _(11);
    }

    /* access modifiers changed from: package-private */
    public final int dddd() {
        try {
            this._.enforceCallingOrSelfPermission(I.I(1222), I.I(1249));
            return 1;
        } catch (SecurityException e) {
            f.____(e.toString());
            return 0;
        }
    }
}
