package com.madhouse.android.ads;

import android.graphics.Camera;
import android.graphics.Matrix;
import android.view.animation.Animation;
import android.view.animation.Transformation;

final class __ extends Animation {
    private final boolean $;
    private int $$;
    private Camera $$$;
    private final float _;
    private final float __;
    private final float ___;
    private final float ____;
    private final float _____;

    protected __(float f, float f2, float f3, float f4, float f5, boolean z, int i) {
        this._ = f;
        this.__ = f2;
        this.___ = f3;
        this.____ = f4;
        this._____ = f5;
        this.$ = z;
        this.$$ = i;
    }

    /* access modifiers changed from: protected */
    public final void applyTransformation(float f, Transformation transformation) {
        float f2 = this.___;
        float f3 = this.____;
        Camera camera = this.$$$;
        Matrix matrix = transformation.getMatrix();
        camera.save();
        switch (this.$$) {
            case 1:
                float f4 = this._;
                float f5 = f4 + ((this.__ - f4) * f);
                if (this.$) {
                    camera.translate(0.0f, 0.0f, this._____ * f);
                } else {
                    camera.translate(0.0f, 0.0f, this._____ * (1.0f - f));
                }
                camera.rotateY(f5);
                break;
            case 2:
                camera.translate(0.0f, 0.0f, this._____ * (1.0f - f));
                camera.rotateX(this._ + ((this.__ - this._) * f));
                break;
            case 3:
                camera.translate(0.0f, 0.0f, this._____ * f);
                camera.rotateX(this._ + ((this.__ - this._) * f));
                break;
        }
        this.$$$.getMatrix(matrix);
        this.$$$.restore();
        matrix.preTranslate(-f2, -f3);
        matrix.postTranslate(f2, f3);
    }

    public final void initialize(int i, int i2, int i3, int i4) {
        super.initialize(i, i2, i3, i4);
        this.$$$ = new Camera();
    }
}
