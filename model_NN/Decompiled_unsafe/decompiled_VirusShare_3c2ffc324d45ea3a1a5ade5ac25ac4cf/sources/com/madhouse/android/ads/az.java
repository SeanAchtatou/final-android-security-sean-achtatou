package com.madhouse.android.ads;

import I.I;
import android.content.Context;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

final class az {
    /* access modifiers changed from: private */
    public static Map _ = new HashMap();

    private az() {
    }

    static final String _(Context context, String str) {
        return new File(context.getFilesDir() + I.I(186) + __(str)).getAbsolutePath();
    }

    static final void _(Context context, String str, int i, be beVar) {
        String _2 = _(context, str);
        if (!_.containsKey(_2)) {
            ba baVar = new ba(_2, str, i);
            if (!baVar._()) {
                bc bcVar = new bc(context, baVar);
                if (beVar != null) {
                    bcVar.__ = beVar;
                }
                _.put(_2, bcVar);
                bcVar.start();
            } else if (beVar != null) {
                beVar.onDownloadReachBuffer(str);
            }
        }
    }

    static final void _(String str, bd bdVar) {
        if (_.containsKey(str)) {
            bc bcVar = (bc) _.get(str);
            if (bdVar != null) {
                bcVar.___ = bdVar;
            }
        } else if (new ba(str, I.I(576), 20)._() && bdVar != null) {
            bdVar.onDownloadBufferUpdate(100);
        }
    }

    /* access modifiers changed from: private */
    public static String __(String str) {
        return fffff._(str).substring(0, 8);
    }

    static final void __(Context context, String str) {
        String _2 = _(context, str);
        if (_.containsKey(_2)) {
            ((bc) _.remove(_2))._ = false;
        }
    }
}
