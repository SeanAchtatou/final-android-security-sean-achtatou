package com.madhouse.android.ads;

import I.I;
import android.content.Context;
import android.util.Log;
import java.lang.Thread;

final class ah implements Thread.UncaughtExceptionHandler {
    private static Thread.UncaughtExceptionHandler __ = null;
    private Context _;

    ah(Context context) {
        this._ = context;
        __ = Thread.getDefaultUncaughtExceptionHandler();
    }

    static final String _(Context context) {
        byte[] _2 = n._(context, I.I(1615));
        if (_2 == null || _2.length <= 0) {
            return null;
        }
        return new String(_2);
    }

    static final void __(Context context) {
        context.deleteFile(I.I(1615));
    }

    public final void uncaughtException(Thread thread, Throwable th) {
        StackTraceElement[] stackTrace = th.getStackTrace();
        StringBuffer stringBuffer = new StringBuffer(1024);
        stringBuffer.append(th.getClass().getName()).append(I.I(1583)).append(th.getMessage());
        String name = getClass().getPackage().getName();
        boolean z = false;
        for (StackTraceElement stackTraceElement : stackTrace) {
            String stackTraceElement2 = stackTraceElement.toString();
            if (stackTraceElement2.startsWith(I.I(1586))) {
                break;
            }
            stringBuffer.append(I.I(1594)).append(stackTraceElement2);
            if (stackTraceElement2.startsWith(name)) {
                z = true;
            }
        }
        if (z) {
            __(this._);
            Log.e(I.I(1597), stringBuffer.toString());
            n._(this._, I.I(1615), stringBuffer.toString().getBytes());
        }
        __.uncaughtException(thread, th);
    }
}
