package com.madhouse.android.ads;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.animation.TranslateAnimation;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import java.net.URISyntaxException;

final class ___ extends LinearLayout implements eeeee {
    private static _ $$$;
    private static boolean _ = false;
    private C$$$ $;
    /* access modifiers changed from: private */
    public c $$;
    private int __;
    private int ___;
    private bbb ____;
    /* access modifiers changed from: private */
    public ccccc _____;

    protected ___(Context context) {
        super(context);
        new DisplayMetrics();
        DisplayMetrics displayMetrics = context.getApplicationContext().getResources().getDisplayMetrics();
        if (displayMetrics.heightPixels > displayMetrics.widthPixels) {
            this.__ = (int) (((double) displayMetrics.heightPixels) * 0.0475d);
            this.___ = (int) (((double) displayMetrics.heightPixels) * 0.08125d);
        } else {
            this.__ = (int) (((double) displayMetrics.widthPixels) * 0.0475d);
            this.___ = (int) (((double) displayMetrics.widthPixels) * 0.08125d);
        }
        setOrientation(1);
        setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        this.____ = new bbb(this, context, this.__);
        addView(this.____, new LinearLayout.LayoutParams(-1, this.__, 1.0f));
        RelativeLayout relativeLayout = new RelativeLayout(context);
        relativeLayout.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        this._____ = new ccccc(context, null, 0, 0);
        this._____.__ = this;
        relativeLayout.addView(this._____, new RelativeLayout.LayoutParams(-1, -1));
        this._____.setDownloadListener(new ____(this, context));
        this.$$ = new c(this, context);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(14);
        layoutParams.addRule(12);
        c cVar = this.$$;
        cVar._.setOnClickListener(new _____(this));
        c cVar2 = this.$$;
        cVar2.__.setOnClickListener(new C$(this));
        this.$$.setVisibility(4);
        relativeLayout.addView(this.$$, layoutParams);
        addView(relativeLayout, new LinearLayout.LayoutParams(-1, -1, 100.0f));
        this.$ = new C$$$(this, context, displayMetrics.widthPixels, this.___);
        addView(this.$, new LinearLayout.LayoutParams(-1, this.___, 1.0f));
    }

    static void _(_ _2) {
        $$$ = _2;
    }

    protected static final void _(boolean z) {
        _ = z;
    }

    static final /* synthetic */ void __(___ ___2) {
        ___2.removeAllViews();
        ((FrameLayout) ___2.getParent()).removeView(___2);
        _ = false;
    }

    protected static final boolean __() {
        return _;
    }

    /* access modifiers changed from: protected */
    public final void _() {
        TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, 1.0f);
        translateAnimation.setDuration(700);
        translateAnimation.setAnimationListener(new C$$(this));
        startAnimation(translateAnimation);
    }

    /* access modifiers changed from: protected */
    public final void _(String str) {
        _ = true;
        this._____._(str);
        this._____._();
        TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
        translateAnimation.setDuration(700);
        startAnimation(translateAnimation);
    }

    public final boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (keyEvent.getAction() != 0 || keyEvent.getKeyCode() != 4) {
            return super.dispatchKeyEvent(keyEvent);
        }
        if (this._____.canGoBack()) {
            this._____.goBack();
        } else if (_) {
            _();
            _ = false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public final void onAttachedToWindow() {
        _ = true;
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        _ = false;
        if (0 == 0 && $$$ != null) {
            $$$.onClose();
            $$$ = null;
        }
        super.onDetachedFromWindow();
        System.gc();
    }

    public final void onPageFinished(WebView webView, String str) {
        this.____._();
        this.$._();
    }

    public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        bbb bbb = this.____;
        bbb.__.setVisibility(8);
        bbb.___.setVisibility(0);
    }

    public final void onReceivedError(WebView webView, int i, String str, String str2) {
    }

    public final void onReceivedIcon(WebView webView, Bitmap bitmap) {
        bbb bbb = this.____;
        if (bbb.__ != null && bitmap != null) {
            bbb.__.setImageBitmap(bitmap);
            bbb.__.setVisibility(0);
        }
    }

    public final void onReceivedTitle(WebView webView, String str) {
        bbb bbb = this.____;
        if (bbb._ != null) {
            bbb._.setText(str);
        }
    }

    public final void shouldOverrideMarketUrlLoading(WebView webView, String str) {
        try {
            fffff.__(getContext(), str, $$$);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }
}
