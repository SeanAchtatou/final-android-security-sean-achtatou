package com.madhouse.android.ads;

import android.content.Context;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;

final class ccc extends ImageButton implements View.OnLongClickListener {
    private final Runnable $ = new cccc(this);
    final Handler _ = new Handler();
    View.OnClickListener __;
    long ___ = 1000;
    boolean ____;
    private c _____;

    ccc(c cVar, Context context, c cVar2) {
        super(context);
        setOnLongClickListener(this);
        setClickable(true);
        setFocusable(true);
        this._____ = cVar2;
    }

    /* access modifiers changed from: package-private */
    public void _() {
        this.__.onClick(this);
        c cVar = this._____;
        cVar.___.removeMessages(8533591);
        cVar.___.sendEmptyMessageDelayed(8533591, 5000);
    }

    public final boolean dispatchUnhandledMove(View view, int i) {
        clearFocus();
        return super.dispatchUnhandledMove(view, i);
    }

    public final boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 66 || i == 23) {
            setPressed(true);
        }
        return super.onKeyDown(i, keyEvent);
    }

    public final boolean onKeyUp(int i, KeyEvent keyEvent) {
        this.____ = false;
        if (i == 66 || i == 23) {
            if (isPressed()) {
                _();
            }
            setPressed(false);
        }
        return super.onKeyUp(i, keyEvent);
    }

    public final boolean onLongClick(View view) {
        this.____ = true;
        this._.post(this.$);
        return true;
    }

    public final boolean onTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action == 3 || action == 1) {
            this.____ = false;
            if (action == 1) {
                _();
            }
        }
        return super.onTouchEvent(motionEvent);
    }

    public final void setEnabled(boolean z) {
        if (!z) {
            setPressed(false);
        }
        super.setEnabled(z);
    }

    public final void setOnClickListener(View.OnClickListener onClickListener) {
        this.__ = onClickListener;
    }
}
