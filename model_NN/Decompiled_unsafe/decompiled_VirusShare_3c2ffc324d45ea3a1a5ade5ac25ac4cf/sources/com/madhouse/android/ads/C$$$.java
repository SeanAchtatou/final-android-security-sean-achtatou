package com.madhouse.android.ads;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

/* renamed from: com.madhouse.android.ads.$$$  reason: invalid class name */
final class C$$$ extends LinearLayout {
    private aaaa $;
    private aaaa $$;
    aaaa _;
    aaaa __;
    PopupWindow ___;
    final /* synthetic */ ___ ____;
    private aaaa _____;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    protected C$$$(___ ___2, Context context, int i, int i2) {
        super(context);
        this.____ = ___2;
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(i, i2);
        setOrientation(0);
        setBackgroundColor(-16777216);
        setLayoutParams(layoutParams);
        this._ = new aaaa(this, context, 17301540, 180, 1.0f, 128, false);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams((i2 * 9) / 10, -2, 1.0f);
        layoutParams2.gravity = 16;
        addView(this._, layoutParams2);
        this._.setOnClickListener(new C$$$$(this, ___2));
        this.__ = new aaaa(this, context, 17301540, 0, 1.0f, 128, false);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams((i2 * 9) / 10, -2, 1.0f);
        layoutParams3.gravity = 16;
        addView(this.__, layoutParams3);
        this.__.setOnClickListener(new C$$$$$(this, ___2));
        this.$ = new aaaa(this, context, 17301593, 0, 0.75f, 128, true);
        LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams((i2 * 9) / 10, -2, 1.0f);
        layoutParams4.gravity = 16;
        addView(this.$, layoutParams4);
        this.$.setOnClickListener(new a(this, ___2));
        this._____ = new aaaa(this, context, 17301585, 0, 0.75f, 128, true);
        LinearLayout.LayoutParams layoutParams5 = new LinearLayout.LayoutParams((i2 * 9) / 10, -2, 1.0f);
        layoutParams5.gravity = 16;
        addView(this._____, layoutParams5);
        this._____.setOnClickListener(new aa(this, ___2, context));
        this.$$ = new aaaa(this, context, 17301533, 0, 1.15f, 128, true);
        LinearLayout.LayoutParams layoutParams6 = new LinearLayout.LayoutParams((i2 * 9) / 10, -2, 1.0f);
        layoutParams6.gravity = 16;
        addView(this.$$, layoutParams6);
        this.$$.setOnClickListener(new aaa(this, ___2));
    }

    /* access modifiers changed from: package-private */
    public final void _() {
        if (this.____._____.canGoBack()) {
            this._._(true);
        } else {
            this._._(false);
        }
        if (this.____._____.canGoForward()) {
            this.__._(true);
        } else {
            this.__._(false);
        }
    }

    /* access modifiers changed from: package-private */
    public final void __() {
        if (this.___ != null) {
            this.___.dismiss();
            this.___ = null;
        }
    }
}
