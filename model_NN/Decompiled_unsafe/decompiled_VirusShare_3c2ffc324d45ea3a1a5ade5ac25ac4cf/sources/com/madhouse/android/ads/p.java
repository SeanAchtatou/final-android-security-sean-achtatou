package com.madhouse.android.ads;

import I.I;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.KeyEvent;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.webkit.URLUtil;
import android.webkit.WebView;
import android.widget.RelativeLayout;

final class p extends RelativeLayout implements eeeee {
    RelativeLayout $;
    al $$;
    boolean $$$ = false;
    int $$$$ = -1;
    final /* synthetic */ AdView $$$$$;
    int _ = 0;
    int __ = 0;
    int ___;
    int ____;
    ccccc _____;
    private boolean a;
    private String aa;
    private boolean aaa = true;
    private bf aaaa;
    private o aaaaa;
    private bx b = new q(this);
    private bv bb = new x(this);

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    p(AdView adView, Context context, String str, int i, int i2, int i3, boolean z, String str2) {
        super(context);
        this.$$$$$ = adView;
        this.___ = i;
        this.____ = i2;
        this._ = i3;
        this.a = z;
        this.aa = str2;
        this.$ = new RelativeLayout(context);
        this._____ = new ccccc(context, null, 0, 2);
        this._____.setFocusableInTouchMode(true);
        this.aaaaa = new ag(this);
        this.aaaaa._(this.b);
        this.$$ = new al(context);
        this.$$._(this.b);
        adView.ccccc._(this.b);
        adView.ccccc.cc = this.bb;
        this.aaaa = new bf(context);
        this.aaaa._(this.b);
        this._____.addJavascriptInterface(this.aaaaa, I.I(1865));
        this._____.addJavascriptInterface(this.$$, I.I(1872));
        this._____.addJavascriptInterface(adView.ccccc, I.I(1879));
        this._____.addJavascriptInterface(adView.cccc, I.I(1885));
        this._____.addJavascriptInterface(this.aaaa, I.I(1889));
        this._____.__ = this;
        if (adView.ccccc.aaa) {
            this.__ = 1;
            ca caVar = adView.ccccc.$;
            caVar._(1, 1);
            if (caVar != null) {
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams.setMargins(adView.ccccc.$$$$, adView.ccccc.$$$$$, 0, 0);
                caVar.setLayoutParams(layoutParams);
                this.$.addView(caVar, layoutParams);
            }
        }
        this.$.addView(this._____, new RelativeLayout.LayoutParams(-2, -2));
        if (URLUtil.isHttpUrl(str) || URLUtil.isHttpsUrl(str) || URLUtil.isAssetUrl(str)) {
            this._____._(str);
        } else {
            this._____.loadDataWithBaseURL(null, str, I.I(1897), I.I(1907), null);
        }
        addView(this.$, ___());
    }

    static final /* synthetic */ void _(p pVar) {
        ((Activity) pVar.$$$$$.__).setRequestedOrientation(pVar.$$$$);
        pVar.setBackgroundColor(0);
        pVar.$.setLayoutParams(pVar.___());
        ca caVar = pVar.$$$$$.ccccc.$;
        if (caVar != null) {
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) caVar.getLayoutParams();
            layoutParams.setMargins(pVar.$$$$$.ccccc.$$$$, pVar.$$$$$.ccccc.$$$$$, 0, 0);
            caVar.setLayoutParams(layoutParams);
            caVar._(pVar.$$$$$.ccccc.a, pVar.$$$$$.ccccc.aa);
            caVar.requestLayout();
        }
        if (pVar._____ != null) {
            pVar._____._(pVar.___, pVar.____);
            pVar._____.requestLayout();
            pVar._____.requestFocus();
        }
    }

    static final /* synthetic */ void __(p pVar) {
        switch (pVar._) {
            case 0:
                pVar.$$$ = true;
                return;
            case 1:
                AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
                alphaAnimation.setDuration(500);
                alphaAnimation.startNow();
                alphaAnimation.setFillAfter(true);
                alphaAnimation.setInterpolator(new LinearInterpolator());
                alphaAnimation.setAnimationListener(new ab(pVar));
                pVar._____.startAnimation(alphaAnimation);
                return;
            case 2:
                ScaleAnimation scaleAnimation = new ScaleAnimation(0.01f, 1.0f, 0.01f, 1.0f, 2, 0.5f, 2, 0.5f);
                scaleAnimation.setDuration(500);
                scaleAnimation.startNow();
                scaleAnimation.setFillAfter(true);
                scaleAnimation.setInterpolator(new LinearInterpolator());
                scaleAnimation.setAnimationListener(new ac(pVar));
                pVar._____.startAnimation(scaleAnimation);
                return;
            case 3:
                __ __2 = new __(0.0f, -1080.0f, (float) (pVar.___ / 2), (float) (pVar.____ / 2), 10000.0f, true, 2);
                __2.setDuration(500);
                __2.setFillAfter(true);
                __2.startNow();
                __2.setInterpolator(new DecelerateInterpolator());
                __2.setAnimationListener(new ad(pVar));
                pVar._____.startAnimation(__2);
                return;
            case 4:
                TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, -1.0f, 1, 0.0f);
                translateAnimation.setFillAfter(true);
                translateAnimation.setInterpolator(new AccelerateInterpolator());
                translateAnimation.setDuration(500);
                translateAnimation.setAnimationListener(new ae(pVar));
                pVar._____.startAnimation(translateAnimation);
                return;
            case 5:
                TranslateAnimation translateAnimation2 = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
                translateAnimation2.setFillAfter(true);
                translateAnimation2.setInterpolator(new AccelerateInterpolator());
                translateAnimation2.setDuration(500);
                translateAnimation2.setAnimationListener(new af(pVar));
                pVar._____.startAnimation(translateAnimation2);
                return;
            default:
                return;
        }
    }

    private RelativeLayout.LayoutParams ___() {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(14);
        if (this._ == 4) {
            layoutParams.addRule(10);
        } else if (this._ == 5) {
            layoutParams.addRule(12);
        } else if (this._ >= 0 && this._ <= 3) {
            layoutParams.addRule(15);
        }
        return layoutParams;
    }

    /* access modifiers changed from: package-private */
    public final void _() {
        if (this.$$$$$.___ != null) {
            this.$$$$$.___.post(new r(this));
        }
    }

    /* access modifiers changed from: package-private */
    public void _(int i) {
        if (!this.$$$) {
            if (this.a) {
                this.$$$$$.setVisibility(8);
            }
            AdView.ccc(this.$$$$$);
        }
        if (this.$$$$$.___ != null) {
            this.$$$$$.___.post(new y(this, i));
        }
    }

    /* access modifiers changed from: package-private */
    public final void _(int i, int i2, boolean z) {
        if (this._____ != null) {
            this._____._(i, i2);
        }
    }

    /* access modifiers changed from: package-private */
    public void __() {
        __ __2 = new __(0.0f, -1080.0f, (float) (this.___ / 2), (float) (this.____ / 2), 10000.0f, true, 3);
        __2.setDuration(500);
        __2.setFillAfter(true);
        __2.startNow();
        __2.setInterpolator(new AccelerateInterpolator());
        __2.setAnimationListener(new u(this));
        this.$.startAnimation(__2);
    }

    public final boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (keyEvent.getAction() != 0 || keyEvent.getKeyCode() != 4) {
            return super.dispatchKeyEvent(keyEvent);
        }
        AdView.bbb(this.$$$$$);
        return true;
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        if (this.aaaaa != null) {
            this.aaaaa.a_(false);
        }
        this.aaaaa = null;
        this.$$ = null;
        this.aaaa = null;
        bj c = this.$$$$$.ccccc;
        c.aaa = false;
        c.bb = null;
        c.aaaaa = null;
        c.$$ = null;
        c.$$$ = -1;
        if (c.$ != null) {
            c.$._();
            c.$ = null;
        }
        c._____ = null;
        c.cc = null;
        bj c2 = this.$$$$$.ccccc;
        bx bxVar = this.b;
        if (bxVar != null) {
            c2.____.remove(bxVar);
        }
        this.$$$ = false;
        AdView.aaaa(this.$$$$$);
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (this.$$ != null) {
            this.$$.b_();
        }
    }

    public final void onPageFinished(WebView webView, String str) {
        if (this.aaa && AdView.ddd != null) {
            this.aaa = false;
            if (this.__ == 0) {
                _(0);
            }
        }
    }

    public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
    }

    public final void onReceivedError(WebView webView, int i, String str, String str2) {
        if (!(this.aa == null || this.$$$$$.ee == null)) {
            this.$$$$$.ee.onCallback(I.I(170) + this.aa + I.I(182) + i + I.I(184));
        }
        if (this.__ == 0) {
            AdView.bbb(this.$$$$$);
        }
    }

    public final void onReceivedIcon(WebView webView, Bitmap bitmap) {
    }

    public final void onReceivedTitle(WebView webView, String str) {
    }

    /* access modifiers changed from: protected */
    public final void onWindowVisibilityChanged(int i) {
        super.onWindowVisibilityChanged(i);
        if (this.aaaaa != null) {
            this.aaaaa.a_(i == 0);
        }
    }

    public final void shouldOverrideMarketUrlLoading(WebView webView, String str) {
    }
}
