package com.madhouse.android.ads;

import I.I;
import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Handler;
import java.io.File;
import java.util.LinkedList;
import java.util.List;

final class bj {
    ca $;
    String[] $$;
    int $$$ = -1;
    int $$$$;
    int $$$$$;
    Context _;
    Handler __;
    MediaRecorder ___;
    List ____ = new LinkedList();
    bw _____;
    int a;
    int aa;
    boolean aaa = false;
    boolean aaaa = true;
    String aaaaa;
    be b = new be(this, (byte) 0);
    bp bb = null;
    int bbb = 0;
    MediaPlayer.OnPreparedListener bbbb = new bl(this);
    MediaPlayer.OnCompletionListener bbbbb = new bm(this);
    MediaPlayer.OnErrorListener c = new bn(this);
    bv cc;
    private String ccc;
    private bo cccc;

    bj(Context context, Handler handler) {
        this._ = context;
        this.__ = handler;
        this.ccc = context.getFilesDir() + I.I(3899);
    }

    static final /* synthetic */ void _(bj bjVar, String str) {
        for (bx onCallback : bjVar.____) {
            onCallback.onCallback(str);
        }
    }

    private void ___() {
        File file = new File(this.ccc);
        if (file.exists()) {
            file.delete();
        }
    }

    /* access modifiers changed from: package-private */
    public final int _() {
        try {
            this._.enforceCallingOrSelfPermission(I.I(705), I.I(737));
            return 1;
        } catch (SecurityException e) {
            f.____(e.toString());
            return 0;
        }
    }

    /* access modifiers changed from: package-private */
    public final void _(bx bxVar) {
        if (bxVar != null) {
            this.____.add(bxVar);
        }
    }

    /* access modifiers changed from: package-private */
    public final void __() {
        if (this.$ != null) {
            this.$._();
        }
    }

    public final void _a(String str) {
        if (_() != 0) {
            _b();
            try {
                ___();
                this.___ = new MediaRecorder();
                this.___.reset();
                this.___.setAudioSource(1);
                this.___.setOutputFormat(0);
                this.___.setAudioEncoder(0);
                this.___.setOutputFile(this.ccc);
                this.___.prepare();
                this.___.start();
                this.cccc = new bo(this, str);
                this.cccc.start();
            } catch (Exception e) {
                e.printStackTrace();
                _b();
            }
        }
    }

    public final void _b() {
        if (this.cccc != null) {
            this.cccc._ = false;
            this.cccc = null;
        }
        if (this.___ != null) {
            this.___.stop();
            this.___.release();
            this.___ = null;
        }
        ___();
    }

    public final void _l(int i, int i2) {
    }

    public final void _m(String str) {
    }

    public final void _p(int i, int i2, int i3, int i4, String[] strArr, int i5, String str) {
        if (strArr != null && strArr.length != 0 && this.__ != null) {
            this.__.post(new bk(this, i, i2, i3, i4, strArr, i5, str));
        }
    }

    public final void _q(int i) {
        if (this.$$ != null && i <= this.$$.length - 1 && this.$$[i] != null && this.bb != null) {
            bp bpVar = this.bb;
            if (bpVar._.$ != null && bpVar._.__ != null) {
                bpVar._.__.post(new bq(bpVar, i));
            }
        }
    }

    public final void _r(int i) {
        if (this.$$ != null && i <= this.$$.length - 1 && this.$$[i] != null && this.bb != null) {
            bp bpVar = this.bb;
            if (bpVar._.$ != null && i == bpVar._.$$$ && bpVar._.$.____() && bpVar._.__ != null) {
                bpVar._.__.post(new br(bpVar));
            }
        }
    }

    public final void _s(int i) {
        if (this.$$ != null && i <= this.$$.length - 1 && this.$$[i] != null && this.bb != null) {
            bp bpVar = this.bb;
            if (bpVar._.$ != null && i == bpVar._.$$$ && bpVar._.$.____() && bpVar._.__ != null) {
                bpVar._.__.post(new bs(bpVar));
            }
        }
    }

    public final void _t(int i) {
        if (this.$$ != null && i <= this.$$.length - 1 && this.$$[i] != null && this.bb != null) {
            bp bpVar = this.bb;
            if (bpVar._.$ != null && i == bpVar._.$$$ && bpVar._.__ != null) {
                bpVar._.__.post(new bt(bpVar));
            }
        }
    }

    public final void _u(int i, boolean z) {
        if (this.$$ != null && i <= this.$$.length - 1 && this.$$[i] != null && this.bb != null) {
            bp bpVar = this.bb;
            if (bpVar._.$ != null && bpVar._.__ != null) {
                bpVar._.__.post(new bu(bpVar, z));
            }
        }
    }
}
