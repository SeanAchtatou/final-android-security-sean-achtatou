package com.madhouse.android.ads;

import I.I;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

final class ca extends SurfaceView {
    MediaPlayer.OnCompletionListener $;
    MediaPlayer.OnPreparedListener $$;
    MediaPlayer.OnErrorListener $$$;
    int $$$$ = -1;
    private Context $$$$$;
    int _;
    int __ = 0;
    MediaPlayer ___ = null;
    int ____;
    int _____;
    private int a = -1;
    private int aa = -1;
    private String aaa;
    private boolean aaaa;
    private SurfaceHolder.Callback aaaaa = new cb(this);
    private MediaPlayer.OnVideoSizeChangedListener b = new cd(this);
    private MediaPlayer.OnBufferingUpdateListener bb = new ce(this);
    private MediaPlayer.OnPreparedListener bbb = new cf(this);
    private MediaPlayer.OnCompletionListener bbbb = new cg(this);
    private MediaPlayer.OnErrorListener bbbbb = new ch(this);
    private bd c = new bd(this);
    private ci cc = null;

    ca(Context context) {
        super(context);
        this.$$$$$ = context;
        this.____ = 0;
        this._____ = 0;
        getHolder().addCallback(this.aaaaa);
        getHolder().setType(3);
        this.__ = 0;
    }

    /* access modifiers changed from: package-private */
    public final void _() {
        if (this.___ != null) {
            this.___.stop();
            this.___.release();
            this.___ = null;
            this.__ = 0;
        }
        _____();
    }

    /* access modifiers changed from: package-private */
    public final void _(int i, int i2) {
        this.a = i;
        this.aa = i2;
    }

    /* access modifiers changed from: package-private */
    public final void _(String str, boolean z) {
        if (str != null) {
            this.aaa = str;
            this.aaaa = z;
            if (!this.aaaa) {
                az._(str, this.c);
            }
            if (this.aaa != null) {
                Intent intent = new Intent(I.I(499));
                intent.putExtra(I.I(537), I.I(545));
                this.$$$$$.sendBroadcast(intent);
                try {
                    if (this.___ == null) {
                        this.___ = new MediaPlayer();
                        this.___.setOnPreparedListener(this.bbb);
                        this.___.setOnCompletionListener(this.bbbb);
                        this.___.setOnErrorListener(this.bbbbb);
                        this.___.setOnBufferingUpdateListener(this.bb);
                        this.___.setOnVideoSizeChangedListener(this.b);
                        this.___.setAudioStreamType(3);
                        this.___.setScreenOnWhilePlaying(true);
                        this.___.setDisplay(getHolder());
                    }
                    this.___.reset();
                    this._ = -1;
                    this.___.setDataSource(this.aaa);
                    this.___.prepareAsync();
                    this.__ = 1;
                } catch (Exception e) {
                    Log.w(I.I(7), I.I(551) + this.aaa, e);
                    this.__ = -1;
                    this.bbbbb.onError(this.___, 1, 0);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void __() {
        if (____()) {
            this.___.start();
            this.__ = 3;
            if (!this.aaaa) {
                _____();
                this.cc = new ci(this);
                this.cc.start();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void ___() {
        if (____() && this.___.isPlaying()) {
            this.___.pause();
            this.__ = 4;
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean ____() {
        return (this.___ == null || this.__ == -1 || this.__ == 0 || this.__ == 1 || this.__ == 5) ? false : true;
    }

    /* access modifiers changed from: package-private */
    public final void _____() {
        if (this.cc != null) {
            this.cc._ = false;
            this.cc = null;
        }
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i, int i2) {
        int i3;
        int defaultSize;
        if (this.a <= 0 || this.aa <= 0) {
            int defaultSize2 = getDefaultSize(this.____, i);
            i3 = defaultSize2;
            defaultSize = getDefaultSize(this._____, i2);
        } else {
            int i4 = this.a;
            i3 = i4;
            defaultSize = this.aa;
        }
        if (this.____ > 1 && this._____ > 1 && i3 > 1 && defaultSize > 1) {
            if (this.____ * defaultSize > this._____ * i3) {
                defaultSize = (this._____ * i3) / this.____;
            }
            if (this.____ * defaultSize < this._____ * i3) {
                i3 = (this.____ * defaultSize) / this._____;
            }
        }
        setMeasuredDimension(i3, defaultSize);
    }
}
