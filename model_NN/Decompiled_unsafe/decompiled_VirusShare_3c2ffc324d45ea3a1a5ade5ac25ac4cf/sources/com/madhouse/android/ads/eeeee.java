package com.madhouse.android.ads;

import android.graphics.Bitmap;
import android.webkit.WebView;

interface eeeee {
    void onPageFinished(WebView webView, String str);

    void onPageStarted(WebView webView, String str, Bitmap bitmap);

    void onReceivedError(WebView webView, int i, String str, String str2);

    void onReceivedIcon(WebView webView, Bitmap bitmap);

    void onReceivedTitle(WebView webView, String str);

    void shouldOverrideMarketUrlLoading(WebView webView, String str);
}
