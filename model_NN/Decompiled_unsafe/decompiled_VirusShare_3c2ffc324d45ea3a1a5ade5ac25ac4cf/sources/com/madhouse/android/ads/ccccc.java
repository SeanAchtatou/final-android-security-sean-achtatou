package com.madhouse.android.ads;

import I.I;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.webkit.WebIconDatabase;
import android.webkit.WebSettings;
import android.webkit.WebView;
import java.lang.reflect.Method;

final class ccccc extends WebView {
    int _;
    eeeee __;
    int ___ = -1;
    int ____ = -1;
    private Handler _____ = new eeee(this);

    protected ccccc(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i);
        this._ = i2;
        setFocusable(true);
        setClickable(true);
        WebSettings settings = getSettings();
        settings.setJavaScriptEnabled(true);
        _(context);
        switch (i2) {
            case 0:
                setFocusableInTouchMode(true);
                settings.setCacheMode(0);
                settings.setPluginsEnabled(true);
                settings.setSupportZoom(true);
                _(true);
                __(true);
                break;
            case 1:
            case 2:
                this.___ = 0;
                this.____ = 0;
                setFocusableInTouchMode(false);
                settings.setCacheMode(2);
                setBackgroundColor(0);
                setVerticalScrollBarEnabled(false);
                setHorizontalScrollBarEnabled(false);
                settings.setSupportZoom(false);
                _(false);
                break;
        }
        setWebViewClient(new d(this));
        setWebChromeClient(new dd(this, context));
    }

    private void _(Context context) {
        try {
            Method[] methods = WebSettings.class.getMethods();
            String I2 = I.I(305);
            String I3 = I.I(326);
            String I4 = I.I(345);
            for (int i = 0; i < methods.length; i++) {
                String name = methods[i].getName();
                if (I2.equals(name) || I3.equals(name)) {
                    methods[i].invoke(getSettings(), new Boolean(true));
                } else if (I4.equals(name)) {
                    String absolutePath = context.getFilesDir().getAbsolutePath();
                    methods[i].invoke(getSettings(), absolutePath);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void _(boolean z) {
        try {
            Method[] methods = WebSettings.class.getMethods();
            for (int i = 0; i < methods.length; i++) {
                if (I.I(282).equals(methods[i].getName())) {
                    methods[i].invoke(getSettings(), new Boolean(z));
                    return;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void __(boolean z) {
        if (z) {
            WebIconDatabase.getInstance().open(getContext().getDir(I.I(255), 0).getPath());
            return;
        }
        WebIconDatabase.getInstance().removeAllIcons();
        WebIconDatabase.getInstance().close();
    }

    /* access modifiers changed from: protected */
    public final void _() {
        super.requestFocus();
    }

    /* access modifiers changed from: protected */
    public final void _(int i, int i2) {
        if (this.___ != i || this.____ != i2) {
            this.___ = i;
            this.____ = i2;
            this._____.sendMessage(new Message());
        }
    }

    /* access modifiers changed from: protected */
    public final void _(String str) {
        try {
            super.loadUrl(str);
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public final String __() {
        return super.getUrl();
    }

    /* access modifiers changed from: protected */
    public final void onAttachedToWindow() {
        if (this._ != 0) {
            super.clearHistory();
            super.clearCache(true);
        }
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public final void onDetachedFromWindow() {
        if (this._ != 0) {
            super.clearHistory();
            super.clearCache(true);
        } else {
            __(false);
        }
        destroy();
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i, int i2) {
        if (this.___ < 0 || this.____ < 0) {
            super.onMeasure(i, i2);
        } else {
            setMeasuredDimension(this.___, this.____);
        }
    }

    public final boolean onTouchEvent(MotionEvent motionEvent) {
        try {
            return super.onTouchEvent(motionEvent);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
