package com.madhouse.android.ads;

import I.I;
import android.content.Context;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;

class _ {
    final /* synthetic */ AdView _;

    private _() {
    }

    _(AdView adView) {
        this._ = adView;
    }

    protected static final ak _(Context context) {
        ak akVar = new ak();
        int __ = __(context);
        if (__ == 0 || __ == 2) {
            akVar._ = __;
            return akVar;
        }
        try {
            Cursor query = context.getContentResolver().query(Uri.parse(I.I(3121)), null, null, null, null);
            query.moveToFirst();
            if (query.getColumnIndex(I.I(3160)) != -1 || query.getColumnIndex(I.I(3166)) != -1 || query.getColumnIndex(I.I(3172)) != -1 || query.getColumnIndex(I.I(3178)) != -1) {
                if (query != null) {
                    query.close();
                }
                akVar._ = __;
                return akVar;
            } else if (query.getInt(query.getColumnIndex(I.I(3185))) > 0) {
                akVar._ = __;
                akVar.__ = query.getString(query.getColumnIndex(I.I(3185)));
                akVar.___ = query.getString(query.getColumnIndex(I.I(3191)));
                if (query != null) {
                    query.close();
                }
                return akVar;
            } else if (query.getInt(query.getColumnIndex(I.I(3196))) > 0) {
                akVar._ = __;
                akVar.__ = query.getString(query.getColumnIndex(I.I(3196)));
                akVar.___ = query.getString(query.getColumnIndex(I.I(3205)));
                if (query != null) {
                    query.close();
                }
                return akVar;
            } else {
                if (query != null) {
                    query.close();
                }
                akVar._ = __;
                return akVar;
            }
        } catch (Exception e) {
            akVar._ = __;
            return akVar;
        }
    }

    private static int __(Context context) {
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService(I.I(3213))).getActiveNetworkInfo();
            if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                if (activeNetworkInfo.getType() == 0 || activeNetworkInfo.getType() == 4 || activeNetworkInfo.getType() == 5 || activeNetworkInfo.getType() == 2 || activeNetworkInfo.getType() == 3) {
                    return 1;
                }
                if (activeNetworkInfo.getType() == 1 || activeNetworkInfo.getType() == 6) {
                    return 2;
                }
                if (activeNetworkInfo.getType() == 15) {
                    return 1;
                }
            }
            return 0;
        } catch (Exception e) {
            return 0;
        }
    }

    public void onClose() {
        AdView.aaaa(this._);
    }
}
