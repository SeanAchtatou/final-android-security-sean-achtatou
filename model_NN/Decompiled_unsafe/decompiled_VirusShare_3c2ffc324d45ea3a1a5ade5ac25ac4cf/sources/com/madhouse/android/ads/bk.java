package com.madhouse.android.ads;

final class bk implements Runnable {
    private /* synthetic */ int $;
    private /* synthetic */ String $$;
    private /* synthetic */ bj $$$;
    private /* synthetic */ int _;
    private /* synthetic */ int __;
    private /* synthetic */ int ___;
    private /* synthetic */ int ____;
    private /* synthetic */ String[] _____;

    bk(bj bjVar, int i, int i2, int i3, int i4, String[] strArr, int i5, String str) {
        this.$$$ = bjVar;
        this._ = i;
        this.__ = i2;
        this.___ = i3;
        this.____ = i4;
        this._____ = strArr;
        this.$ = i5;
        this.$$ = str;
    }

    public final void run() {
        bj bjVar = this.$$$;
        int i = this._;
        int i2 = this.__;
        int i3 = this.___;
        int i4 = this.____;
        String[] strArr = this._____;
        int i5 = this.$;
        String str = this.$$;
        bjVar.$$ = strArr;
        bjVar.$$$$ = i;
        bjVar.$$$$$ = i2;
        bjVar.a = i3;
        bjVar.aa = i4;
        bjVar.aaa = true;
        bjVar.aaaaa = str;
        bjVar.bb = new bp(bjVar);
        if (bjVar.$ == null) {
            bjVar.$ = new ca(bjVar._);
            bjVar.$._(bjVar.a, bjVar.aa);
            ca caVar = bjVar.$;
            int i6 = bjVar.a;
            int i7 = bjVar.aa;
            caVar.____ = i6;
            caVar._____ = i7;
            bjVar.$.$$ = bjVar.bbbb;
            bjVar.$.$ = bjVar.bbbbb;
            bjVar.$.$$$ = bjVar.c;
        }
        int i8 = i5 < 0 ? 0 : i5 > 100 ? 100 : i5;
        if (i8 > 0) {
            bjVar.aaaa = false;
            for (String str2 : strArr) {
                if (!(str2 == null || str2.length() == 0)) {
                    az._(bjVar._, str2, i8, bjVar.b);
                }
            }
            return;
        }
        bjVar.$$$ = 0;
        String str3 = strArr[bjVar.$$$];
        if (str3 != null && str3.length() != 0 && bjVar.b != null) {
            bjVar.b.onDownloadReachBuffer(str3);
        }
    }
}
