package com.madhouse.android.ads;

import I.I;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.webkit.DownloadListener;

final class ____ implements DownloadListener {
    private /* synthetic */ Context _;

    ____(___ ___, Context context) {
        this._ = context;
    }

    public final void onDownloadStart(String str, String str2, String str3, String str4, long j) {
        if (str3 != null) {
            if (str3.regionMatches(true, 0, I.I(361), 0, 10)) {
                return;
            }
        }
        Intent intent = new Intent(I.I(228));
        intent.addFlags(268435456);
        intent.setDataAndType(Uri.parse(str), str4);
        if (this._.getPackageManager().resolveActivity(intent, 65536) != null) {
            try {
                this._.startActivity(intent);
            } catch (ActivityNotFoundException e) {
                e.printStackTrace();
            }
        } else {
            intent.setClassName(I.I(372), I.I(392));
            try {
                this._.startActivity(intent);
            } catch (ActivityNotFoundException e2) {
                e2.printStackTrace();
            }
        }
    }
}
