package com.madhouse.android.ads;

import android.content.DialogInterface;
import android.webkit.JsPromptResult;
import android.widget.EditText;

final class e implements DialogInterface.OnClickListener {
    private /* synthetic */ JsPromptResult _;
    private /* synthetic */ EditText __;

    e(dd ddVar, JsPromptResult jsPromptResult, EditText editText) {
        this._ = jsPromptResult;
        this.__ = editText;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this._.confirm(this.__.getText().toString());
    }
}
