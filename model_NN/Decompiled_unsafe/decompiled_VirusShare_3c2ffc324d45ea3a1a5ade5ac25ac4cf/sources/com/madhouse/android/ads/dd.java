package com.madhouse.android.ads;

import I.I;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.WebView;
import android.widget.EditText;

final class dd extends ai {
    private /* synthetic */ Context _;
    private /* synthetic */ ccccc __;

    dd(ccccc ccccc, Context context) {
        this.__ = ccccc;
        this._ = context;
    }

    public final void addMessageToConsole(String str, int i, String str2) {
    }

    public final boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(this._);
            builder.setTitle(fffff.___(I.I(276)));
            builder.setMessage(str2);
            builder.setPositiveButton(17039370, new ddd(this, jsResult));
            builder.setCancelable(false);
            builder.create();
            builder.show();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public final boolean onJsConfirm(WebView webView, String str, String str2, JsResult jsResult) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(this._);
            builder.setTitle(fffff.___(I.I(268)));
            builder.setMessage(str2);
            builder.setPositiveButton(17039370, new dddd(this, jsResult));
            builder.setNeutralButton(17039360, new ddddd(this, jsResult));
            builder.setCancelable(false);
            builder.create();
            builder.show();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public final boolean onJsPrompt(WebView webView, String str, String str2, String str3, JsPromptResult jsPromptResult) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(this._);
            builder.setTitle(fffff.___(I.I(261)));
            EditText editText = new EditText(this._);
            editText.setSingleLine();
            editText.setText(str3);
            builder.setView(editText);
            builder.setPositiveButton(17039370, new e(this, jsPromptResult, editText));
            builder.setNeutralButton(17039360, new ee(this, jsPromptResult));
            builder.setOnCancelListener(new eee(this, jsPromptResult));
            builder.setCancelable(false);
            builder.create();
            builder.show();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public final void onReceivedIcon(WebView webView, Bitmap bitmap) {
        if (this.__.__ != null && this.__._ == 0) {
            try {
                this.__.__.onReceivedIcon(webView, bitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        super.onReceivedIcon(webView, bitmap);
    }

    public final void onReceivedTitle(WebView webView, String str) {
        if (this.__.__ != null && this.__._ == 0) {
            try {
                this.__.__.onReceivedTitle(webView, str);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        super.onReceivedTitle(webView, str);
    }

    public final void openFileChooser$509ba09e(bw bwVar) {
    }
}
