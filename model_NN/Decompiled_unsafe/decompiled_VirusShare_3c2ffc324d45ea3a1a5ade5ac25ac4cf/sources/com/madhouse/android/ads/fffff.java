package com.madhouse.android.ads;

import I.I;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.net.Uri;
import android.webkit.URLUtil;
import android.widget.RelativeLayout;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

final class fffff {
    static final String _ = null;

    private fffff() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    protected static final Bitmap _(Context context, int i, int i2, float f) {
        Bitmap decodeResource = BitmapFactory.decodeResource(context.getResources(), i);
        if (decodeResource != null) {
            int width = decodeResource.getWidth();
            int height = decodeResource.getHeight();
            int[] iArr = new int[(width * height)];
            int red = Color.red(-1);
            int green = Color.green(-1);
            int blue = Color.blue(-1);
            decodeResource.getPixels(iArr, 0, width, 0, 0, width, height);
            for (int i3 = 0; i3 < height; i3++) {
                for (int i4 = 0; i4 < width; i4++) {
                    int alpha = Color.alpha(iArr[(i3 * width) + i4]);
                    if (alpha > 0) {
                        iArr[(i3 * width) + i4] = Color.argb(alpha, red, green, blue);
                    }
                }
            }
            Bitmap createBitmap = Bitmap.createBitmap(iArr, width, height, Bitmap.Config.ARGB_8888);
            if (createBitmap != decodeResource) {
                decodeResource.recycle();
            }
            Matrix matrix = new Matrix();
            matrix.postRotate((float) i2);
            matrix.postScale(f, f);
            decodeResource = Bitmap.createBitmap(createBitmap, 0, 0, createBitmap.getWidth(), createBitmap.getHeight(), matrix, true);
            if (createBitmap != decodeResource) {
                createBitmap.recycle();
            }
        }
        return decodeResource;
    }

    protected static final String _(Context context, String str) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo != null) {
                return applicationInfo.metaData.getString(str);
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    protected static final String _(String str) {
        try {
            String bigInteger = new BigInteger(1, MessageDigest.getInstance(I.I(493)).digest(str.getBytes())).toString(16);
            while (bigInteger.length() < 32) {
                bigInteger = I.I(497) + bigInteger;
            }
            return bigInteger;
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

    protected static final void _(Context context, String str, _ _2) {
        if (str != null && URLUtil.isValidUrl(str)) {
            if (!AdManager.____()) {
                Intent intent = new Intent(I.I(228), Uri.parse(str));
                intent.addFlags(268435456);
                context.startActivity(intent);
            } else if (!___.__()) {
                ___ ___ = new ___(context);
                ___._(_2);
                ((Activity) context).addContentView(___, new RelativeLayout.LayoutParams(-1, -1));
                ___._(str);
            }
        }
    }

    protected static final void __(Context context, String str) {
        if (str != null && URLUtil.isValidUrl(str)) {
            Intent intent = new Intent(I.I(228), Uri.parse(str));
            intent.addFlags(268435456);
            context.startActivity(intent);
        }
    }

    protected static final void __(Context context, String str, _ _2) {
        if (str != null) {
            String str2 = null;
            if (str != null) {
                str2 = str.replaceFirst(I.I(428), I.I(455));
                if (str.equals(str2)) {
                    str2 = str.replaceFirst(I.I(465), I.I(455));
                }
            }
            Intent intent = new Intent(I.I(228), Uri.parse(str2));
            intent.addFlags(268435456);
            try {
                context.startActivity(intent);
            } catch (ActivityNotFoundException e) {
                _(context, str, _2);
            }
        }
    }

    protected static final boolean __(String str) {
        return str != null && (str.startsWith(I.I(455)) || str.startsWith(I.I(428)) || str.startsWith(I.I(465)));
    }

    protected static final String ___(String str) {
        try {
            return I.I(188).equals(Locale.getDefault().getLanguage()) ? I.I(191).equals(Locale.getDefault().getCountry()) ? ResourceBundle.getBundle(I.I(194), Locale.SIMPLIFIED_CHINESE).getString(str) : ResourceBundle.getBundle(I.I(194), Locale.TRADITIONAL_CHINESE).getString(str) : ResourceBundle.getBundle(I.I(194), Locale.US).getString(str);
        } catch (MissingResourceException e) {
            return null;
        }
    }
}
