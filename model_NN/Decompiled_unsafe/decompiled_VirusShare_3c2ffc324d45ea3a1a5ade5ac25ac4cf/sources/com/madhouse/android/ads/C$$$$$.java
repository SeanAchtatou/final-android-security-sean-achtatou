package com.madhouse.android.ads;

import android.view.View;

/* renamed from: com.madhouse.android.ads.$$$$$  reason: invalid class name */
final class C$$$$$ implements View.OnClickListener {
    private /* synthetic */ C$$$ _;

    C$$$$$(C$$$ $$$, ___ ___) {
        this._ = $$$;
    }

    public final void onClick(View view) {
        if (this._.____._____.canGoForward()) {
            this._.____._____.goForward();
            this._._();
            if (!this._.____._____.canGoForward() && this._.____._____.canGoBack()) {
                this._._.requestFocus();
            }
        }
    }
}
