package com.madhouse.android.ads;

import I.I;
import android.content.Context;

final class bf {
    static bh _;
    private bx __;

    bf(Context context) {
        if (_ == null) {
            _ = new bh(context, I.I(1913));
        }
    }

    static int _(String str, String str2) {
        return _._(str, str2);
    }

    private void _(String str) {
        if (this.__ != null) {
            this.__.onCallback(str);
        }
    }

    /* access modifiers changed from: package-private */
    public final void _(bx bxVar) {
        if (bxVar != null) {
            this.__ = bxVar;
        }
    }

    public final void _a(int i) {
        _._ = (long) (i << 10);
    }

    public final void _b(String str, String str2, String str3) {
        if (str3 != null && str3.length() != 0) {
            _(I.I(170) + str3 + I.I(182) + _._(str, str2) + I.I(184));
        }
    }

    public final void _c(String str, String str2) {
        if (str2 != null && str2.length() != 0) {
            _(I.I(170) + str2 + I.I(1116) + _._(str) + I.I(1119));
        }
    }

    public final void _d(String str) {
        if (!I.I(1078).equalsIgnoreCase(str) && !I.I(1084).equalsIgnoreCase(str)) {
            bh bhVar = _;
            if (str != null && str.length() != 0) {
                try {
                    bhVar._();
                    bhVar.__.delete(I.I(1108), I.I(1122) + str + I.I(1130), null);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    bhVar.__();
                }
            }
        }
    }

    public final void _e() {
        String str;
        bh bhVar = _;
        String[] strArr = {I.I(1078), I.I(1084)};
        if (strArr == null || strArr.length <= 0) {
            str = null;
        } else {
            StringBuffer stringBuffer = new StringBuffer(I.I(1089) + strArr[0] + I.I(1103));
            for (int i = 1; i < strArr.length; i++) {
                stringBuffer.append(I.I(1105)).append(strArr[i]).append(I.I(1103));
            }
            stringBuffer.append(I.I(184));
            str = stringBuffer.toString();
        }
        try {
            bhVar._();
            bhVar.__.delete(I.I(1108), str, null);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            bhVar.__();
        }
    }

    public final void _f(int i, String str) {
        if (str != null && str.length() != 0) {
            String[] _2 = _._(new String[]{I.I(1078), I.I(1084)});
            _(I.I(170) + str + I.I(1116) + ((_2 == null || i < 0 || i >= _2.length) ? null : _2[i]) + I.I(1119));
        }
    }

    public final void _g(String str) {
        String str2;
        if (str != null && str.length() != 0) {
            String[] _2 = _._(new String[]{I.I(1078), I.I(1084)});
            if (_2 == null || _2.length <= 0) {
                str2 = null;
            } else {
                StringBuffer stringBuffer = new StringBuffer(_2[0]);
                for (int i = 1; i < _2.length; i++) {
                    stringBuffer.append(I.I(25)).append(_2[i]);
                }
                str2 = stringBuffer.toString();
            }
            _(I.I(170) + str + I.I(1116) + str2 + I.I(1119));
        }
    }

    public final int _h() {
        String[] _2 = _._(new String[]{I.I(1078), I.I(1084)});
        if (_2 != null) {
            return _2.length;
        }
        return 0;
    }
}
