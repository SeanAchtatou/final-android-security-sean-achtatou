package com.madhouse.android.ads;

import I.I;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

final class bh {
    long _ = 1048576;
    SQLiteDatabase __;
    private Context ___;
    private String ____;
    private Cursor _____;

    bh(Context context, String str) {
        this.___ = context;
        this.____ = str;
    }

    /* JADX INFO: finally extract failed */
    private int __(String str, String str2) {
        try {
            _();
            ContentValues contentValues = new ContentValues();
            contentValues.put(I.I(1139), str);
            contentValues.put(I.I(1133), str2);
            int i = this.__.insert(I.I(1108), null, contentValues) == -1 ? 0 : 1;
            __();
            return i;
        } catch (Exception e) {
            e.printStackTrace();
            __();
            return 0;
        } catch (Throwable th) {
            __();
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    private int ___(String str, String str2) {
        try {
            _();
            ContentValues contentValues = new ContentValues();
            contentValues.put(I.I(1133), str2);
            int i = this.__.update(I.I(1108), contentValues, new StringBuilder().append(I.I(1122)).append(str).append(I.I(1130)).toString(), null) <= 0 ? 0 : 1;
            __();
            return i;
        } catch (Exception e) {
            e.printStackTrace();
            __();
            return 0;
        } catch (Throwable th) {
            __();
            throw th;
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized int _(String str, String str2) {
        int i;
        if (str != null) {
            if (str.length() != 0) {
                if (str2 != null) {
                    if (str2.length() != 0) {
                        i = _(str) == null ? (this.___.getDatabasePath(this.____).length() + ((long) str.length())) + ((long) str2.length()) > this._ ? 0 : __(str, str2) : ___(str, str2);
                    }
                }
                i = 0;
            }
        }
        i = 0;
        return i;
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: package-private */
    public final synchronized String _(String str) {
        String str2;
        if (str != null) {
            if (str.length() != 0) {
                try {
                    _();
                    this._____ = this.__.query(I.I(1108), null, I.I(1122) + str + I.I(1130), null, null, null, null);
                    this._____.moveToFirst();
                    if (this._____.getCount() == 0) {
                        __();
                        str2 = null;
                    } else {
                        str2 = (this._____.isAfterLast() || this._____.getString(1) == null) ? null : this._____.getString(2);
                        __();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    __();
                    str2 = null;
                } catch (Throwable th) {
                    __();
                    throw th;
                }
            }
        }
        str2 = null;
        return str2;
    }

    /* access modifiers changed from: package-private */
    public void _() {
        if (this.__ == null) {
            this.__ = new bi(this, this.___, this.____, null, 1).getWritableDatabase();
        }
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0083 A[SYNTHETIC, Splitter:B:15:0x0083] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0089 A[SYNTHETIC, Splitter:B:20:0x0089] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized java.lang.String[] _(java.lang.String[] r11) {
        /*
            r10 = this;
            r9 = 0
            r3 = 1
            r8 = 0
            monitor-enter(r10)
            if (r11 == 0) goto L_0x00c3
            int r0 = r11.length     // Catch:{ all -> 0x00b2 }
            if (r0 <= 0) goto L_0x00c3
            java.lang.StringBuffer r0 = new java.lang.StringBuffer     // Catch:{ all -> 0x00b2 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00b2 }
            r1.<init>()     // Catch:{ all -> 0x00b2 }
            r2 = 1089(0x441, float:1.526E-42)
            java.lang.String r2 = I.I.I(r2)     // Catch:{ all -> 0x00b2 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00b2 }
            r2 = 0
            r2 = r11[r2]     // Catch:{ all -> 0x00b2 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00b2 }
            r2 = 1103(0x44f, float:1.546E-42)
            java.lang.String r2 = I.I.I(r2)     // Catch:{ all -> 0x00b2 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00b2 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00b2 }
            r0.<init>(r1)     // Catch:{ all -> 0x00b2 }
            r1 = r3
        L_0x0033:
            int r2 = r11.length     // Catch:{ all -> 0x00b2 }
            if (r1 >= r2) goto L_0x0052
            r2 = 1105(0x451, float:1.548E-42)
            java.lang.String r2 = I.I.I(r2)     // Catch:{ all -> 0x00b2 }
            java.lang.StringBuffer r2 = r0.append(r2)     // Catch:{ all -> 0x00b2 }
            r3 = r11[r1]     // Catch:{ all -> 0x00b2 }
            java.lang.StringBuffer r2 = r2.append(r3)     // Catch:{ all -> 0x00b2 }
            r3 = 1103(0x44f, float:1.546E-42)
            java.lang.String r3 = I.I.I(r3)     // Catch:{ all -> 0x00b2 }
            r2.append(r3)     // Catch:{ all -> 0x00b2 }
            int r1 = r1 + 1
            goto L_0x0033
        L_0x0052:
            r1 = 184(0xb8, float:2.58E-43)
            java.lang.String r1 = I.I.I(r1)     // Catch:{ all -> 0x00b2 }
            r0.append(r1)     // Catch:{ all -> 0x00b2 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00b2 }
            r3 = r0
        L_0x0060:
            r10._()     // Catch:{ Exception -> 0x00b5 }
            android.database.sqlite.SQLiteDatabase r0 = r10.__     // Catch:{ Exception -> 0x00b5 }
            r1 = 1108(0x454, float:1.553E-42)
            java.lang.String r1 = I.I.I(r1)     // Catch:{ Exception -> 0x00b5 }
            r2 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x00b5 }
            r10._____ = r0     // Catch:{ Exception -> 0x00b5 }
            android.database.Cursor r0 = r10._____     // Catch:{ Exception -> 0x00b5 }
            r0.moveToFirst()     // Catch:{ Exception -> 0x00b5 }
            android.database.Cursor r0 = r10._____     // Catch:{ Exception -> 0x00b5 }
            int r0 = r0.getCount()     // Catch:{ Exception -> 0x00b5 }
            if (r0 != 0) goto L_0x0089
            r10.__()     // Catch:{ all -> 0x00b2 }
            r0 = r8
        L_0x0087:
            monitor-exit(r10)
            return r0
        L_0x0089:
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ Exception -> 0x00b5 }
            r1 = r9
        L_0x008c:
            android.database.Cursor r2 = r10._____     // Catch:{ Exception -> 0x00b5 }
            boolean r2 = r2.isAfterLast()     // Catch:{ Exception -> 0x00b5 }
            if (r2 != 0) goto L_0x00ae
            android.database.Cursor r2 = r10._____     // Catch:{ Exception -> 0x00b5 }
            r3 = 1
            java.lang.String r2 = r2.getString(r3)     // Catch:{ Exception -> 0x00b5 }
            if (r2 == 0) goto L_0x00ae
            android.database.Cursor r2 = r10._____     // Catch:{ Exception -> 0x00b5 }
            r3 = 1
            java.lang.String r2 = r2.getString(r3)     // Catch:{ Exception -> 0x00b5 }
            r0[r1] = r2     // Catch:{ Exception -> 0x00b5 }
            android.database.Cursor r2 = r10._____     // Catch:{ Exception -> 0x00b5 }
            r2.moveToNext()     // Catch:{ Exception -> 0x00b5 }
            int r1 = r1 + 1
            goto L_0x008c
        L_0x00ae:
            r10.__()     // Catch:{ all -> 0x00b2 }
            goto L_0x0087
        L_0x00b2:
            r0 = move-exception
            monitor-exit(r10)
            throw r0
        L_0x00b5:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x00be }
            r10.__()     // Catch:{ all -> 0x00b2 }
            r0 = r8
            goto L_0x0087
        L_0x00be:
            r0 = move-exception
            r10.__()     // Catch:{ all -> 0x00b2 }
            throw r0     // Catch:{ all -> 0x00b2 }
        L_0x00c3:
            r3 = r8
            goto L_0x0060
        */
        throw new UnsupportedOperationException("Method not decompiled: com.madhouse.android.ads.bh._(java.lang.String[]):java.lang.String[]");
    }

    /* access modifiers changed from: package-private */
    public void __() {
        if (this._____ != null) {
            this._____.close();
            this._____ = null;
        }
        if (this.__ != null) {
            this.__.close();
            this.__ = null;
        }
    }
}
