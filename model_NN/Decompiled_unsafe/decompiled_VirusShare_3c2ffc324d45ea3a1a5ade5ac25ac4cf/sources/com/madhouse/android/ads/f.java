package com.madhouse.android.ads;

import I.I;
import android.util.Log;

final class f {
    static boolean _;

    private f() {
    }

    protected static final void _(String str) {
        if (str != null && str.length() != 0 && _) {
            Log.d(I.I(813), str);
        }
    }

    protected static final void __(String str) {
        if (str != null && str.length() != 0 && _) {
            Log.i(I.I(813), str);
        }
    }

    protected static final void ___(String str) {
        if (str != null && str.length() != 0 && _) {
            Log.w(I.I(813), str);
        }
    }

    protected static final void ____(String str) {
        if (str != null && str.length() != 0 && _) {
            Log.e(I.I(813), str);
        }
    }
}
