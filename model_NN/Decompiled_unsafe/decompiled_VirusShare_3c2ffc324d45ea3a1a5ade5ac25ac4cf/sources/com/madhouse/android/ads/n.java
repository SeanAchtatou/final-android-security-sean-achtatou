package com.madhouse.android.ads;

import I.I;
import android.graphics.Bitmap;
import android.webkit.WebView;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

class n implements eeeee {
    private /* synthetic */ AdView _;

    private n() {
    }

    n(AdView adView) {
        this._ = adView;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0015, code lost:
        if (r0 != null) goto L_0x0017;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x001c, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x001d, code lost:
        r3 = r1;
        r1 = r0;
        r0 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0022 A[SYNTHETIC, Splitter:B:17:0x0022] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0014 A[ExcHandler: Exception (e java.lang.Exception), Splitter:B:1:0x0003] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static boolean _(android.content.Context r4, java.lang.String r5, byte[] r6) {
        /*
            r2 = 0
            r0 = 0
            r1 = 0
            java.io.FileOutputStream r0 = r4.openFileOutput(r5, r1)     // Catch:{ Exception -> 0x0014, all -> 0x001c }
            r0.write(r6)     // Catch:{ Exception -> 0x0014, all -> 0x002c }
            r0.flush()     // Catch:{ Exception -> 0x0014, all -> 0x002c }
            if (r0 == 0) goto L_0x0012
            r0.close()     // Catch:{ Exception -> 0x0026 }
        L_0x0012:
            r0 = 1
        L_0x0013:
            return r0
        L_0x0014:
            r1 = move-exception
            if (r0 == 0) goto L_0x001a
            r0.close()     // Catch:{ Exception -> 0x0028 }
        L_0x001a:
            r0 = r2
            goto L_0x0013
        L_0x001c:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
        L_0x0020:
            if (r1 == 0) goto L_0x0025
            r1.close()     // Catch:{ Exception -> 0x002a }
        L_0x0025:
            throw r0
        L_0x0026:
            r0 = move-exception
            goto L_0x0012
        L_0x0028:
            r0 = move-exception
            goto L_0x001a
        L_0x002a:
            r1 = move-exception
            goto L_0x0025
        L_0x002c:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
            goto L_0x0020
        */
        throw new UnsupportedOperationException("Method not decompiled: com.madhouse.android.ads.n._(android.content.Context, java.lang.String, byte[]):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        r1 = new byte[0];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0014, code lost:
        if (r0 != null) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x001b, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x001c, code lost:
        r2 = r1;
        r1 = r0;
        r0 = r2;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0021 A[SYNTHETIC, Splitter:B:20:0x0021] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0010 A[ExcHandler: Exception (e java.lang.Exception), Splitter:B:1:0x0001] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static byte[] _(android.content.Context r3, java.lang.String r4) {
        /*
            r0 = 0
            java.io.FileInputStream r0 = r3.openFileInput(r4)     // Catch:{ Exception -> 0x0010, all -> 0x001b }
            byte[] r1 = _(r0)     // Catch:{ Exception -> 0x0010 }
            if (r0 == 0) goto L_0x000e
            r0.close()     // Catch:{ Exception -> 0x0025 }
        L_0x000e:
            r0 = r1
        L_0x000f:
            return r0
        L_0x0010:
            r1 = move-exception
            r1 = 0
            byte[] r1 = new byte[r1]     // Catch:{ all -> 0x002b }
            if (r0 == 0) goto L_0x0019
            r0.close()     // Catch:{ Exception -> 0x0027 }
        L_0x0019:
            r0 = r1
            goto L_0x000f
        L_0x001b:
            r1 = move-exception
            r2 = r1
            r1 = r0
            r0 = r2
        L_0x001f:
            if (r1 == 0) goto L_0x0024
            r1.close()     // Catch:{ Exception -> 0x0029 }
        L_0x0024:
            throw r0
        L_0x0025:
            r0 = move-exception
            goto L_0x000e
        L_0x0027:
            r0 = move-exception
            goto L_0x0019
        L_0x0029:
            r1 = move-exception
            goto L_0x0024
        L_0x002b:
            r1 = move-exception
            r2 = r1
            r1 = r0
            r0 = r2
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.madhouse.android.ads.n._(android.content.Context, java.lang.String):byte[]");
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x001f A[SYNTHETIC, Splitter:B:18:0x001f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static byte[] _(java.io.File r3) {
        /*
            r0 = 0
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ Exception -> 0x000e, all -> 0x0019 }
            r1.<init>(r3)     // Catch:{ Exception -> 0x000e, all -> 0x0019 }
            byte[] r0 = _(r1)     // Catch:{ Exception -> 0x0030, all -> 0x0029 }
            r1.close()     // Catch:{ Exception -> 0x0023 }
        L_0x000d:
            return r0
        L_0x000e:
            r1 = move-exception
        L_0x000f:
            r1 = 0
            byte[] r1 = new byte[r1]     // Catch:{ all -> 0x002b }
            if (r0 == 0) goto L_0x0017
            r0.close()     // Catch:{ Exception -> 0x0025 }
        L_0x0017:
            r0 = r1
            goto L_0x000d
        L_0x0019:
            r1 = move-exception
            r2 = r1
            r1 = r0
            r0 = r2
        L_0x001d:
            if (r1 == 0) goto L_0x0022
            r1.close()     // Catch:{ Exception -> 0x0027 }
        L_0x0022:
            throw r0
        L_0x0023:
            r1 = move-exception
            goto L_0x000d
        L_0x0025:
            r0 = move-exception
            goto L_0x0017
        L_0x0027:
            r1 = move-exception
            goto L_0x0022
        L_0x0029:
            r0 = move-exception
            goto L_0x001d
        L_0x002b:
            r1 = move-exception
            r2 = r1
            r1 = r0
            r0 = r2
            goto L_0x001d
        L_0x0030:
            r0 = move-exception
            r0 = r1
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.madhouse.android.ads.n._(java.io.File):byte[]");
    }

    protected static byte[] _(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(1024);
        byte[] bArr = new byte[1024];
        while (true) {
            try {
                int read = inputStream.read(bArr);
                if (read < 0) {
                    break;
                }
                byteArrayOutputStream.write(bArr, 0, read);
            } catch (IOException e) {
            }
        }
        return byteArrayOutputStream.toByteArray();
    }

    public final void onPageFinished(WebView webView, String str) {
        AdView._(I.I(3954));
    }

    public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.madhouse.android.ads.AdView.__(com.madhouse.android.ads.AdView, boolean):boolean
     arg types: [com.madhouse.android.ads.AdView, int]
     candidates:
      com.madhouse.android.ads.AdView.__(com.madhouse.android.ads.AdView, int):int
      com.madhouse.android.ads.AdView.__(com.madhouse.android.ads.AdView, java.lang.String):java.lang.String
      com.madhouse.android.ads.AdView.__(com.madhouse.android.ads.AdView, boolean):boolean */
    public final void onReceivedError(WebView webView, int i, String str, String str2) {
        if (!this._.c) {
            f.____(I.I(3941) + i);
            this._.__(2);
            boolean unused = this._.bbbbb = false;
            this._._(true);
        }
    }

    public final void onReceivedIcon(WebView webView, Bitmap bitmap) {
    }

    public final void onReceivedTitle(WebView webView, String str) {
    }

    public final void shouldOverrideMarketUrlLoading(WebView webView, String str) {
    }
}
