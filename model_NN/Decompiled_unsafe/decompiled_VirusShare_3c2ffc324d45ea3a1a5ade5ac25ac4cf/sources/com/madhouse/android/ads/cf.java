package com.madhouse.android.ads;

import android.media.MediaPlayer;

final class cf implements MediaPlayer.OnPreparedListener {
    private /* synthetic */ ca _;

    cf(ca caVar) {
        this._ = caVar;
    }

    public final void onPrepared(MediaPlayer mediaPlayer) {
        this._.__ = 2;
        int videoWidth = mediaPlayer.getVideoWidth();
        int videoHeight = mediaPlayer.getVideoHeight();
        if (!(videoWidth == 0 || videoHeight == 0)) {
            this._.getHolder().setFixedSize(videoWidth, videoHeight);
        }
        if (this._.____ <= 0 && this._._____ <= 0) {
            this._.____ = videoWidth;
            this._._____ = videoHeight;
        }
        if (this._.$$ != null) {
            this._.$$.onPrepared(this._.___);
        }
    }
}
