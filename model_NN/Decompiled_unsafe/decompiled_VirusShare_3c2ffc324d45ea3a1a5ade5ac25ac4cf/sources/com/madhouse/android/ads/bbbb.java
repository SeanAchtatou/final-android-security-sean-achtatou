package com.madhouse.android.ads;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.widget.TextView;
import java.util.Timer;

final class bbbb extends TextView {
    private float $;
    private float $$;
    private String $$$;
    private Paint $$$$ = getPaint();
    boolean _;
    private float __;
    private float ___;
    private float ____;
    private float _____;

    protected bbbb(bbb bbb, Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, null, 0);
        setFocusable(true);
        setClickable(true);
        this.____ = (float) i2;
    }

    private void _() {
        if (isPressed() && !this._) {
            this._ = true;
        }
    }

    private void __() {
        if (isPressed()) {
            setBackgroundColor(-256);
        } else if (hasFocus()) {
            setBackgroundColor(-16776961);
        } else {
            setBackgroundColor(-16777216);
        }
    }

    public final boolean dispatchTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                setPressed(true);
                requestFocus();
                __();
                break;
            case 1:
                _();
                setPressed(false);
                __();
                break;
            case 2:
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                int left = getLeft();
                int top = getTop();
                int right = getRight();
                int bottom = getBottom();
                if (x < 0.0f || x > ((float) (right - left)) || y < 0.0f || y > ((float) (bottom - top))) {
                    setPressed(false);
                    __();
                    break;
                }
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    public final boolean dispatchTrackballEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            setPressed(true);
            __();
        } else if (motionEvent.getAction() == 1) {
            _();
            setPressed(false);
            __();
        }
        return super.dispatchTrackballEvent(motionEvent);
    }

    public final String getText() {
        return this.$$$;
    }

    /* access modifiers changed from: protected */
    public final void onDraw(Canvas canvas) {
        if (this.___ == 0.0f) {
            this.___ = (float) getWidth();
        }
        if (this.$$$ != null) {
            canvas.drawText(this.$$$, this._____ - this.$$, this.$, this.$$$$);
        }
        if (this._) {
            if (this.$$ > this.__ - this.___) {
                this._ = false;
                this.$$ = 0.0f;
            } else {
                this.$$ = (float) (((double) this.$$) + 0.5d);
            }
            invalidate();
        }
    }

    /* access modifiers changed from: protected */
    public final void onFocusChanged(boolean z, int i, Rect rect) {
        __();
        super.onFocusChanged(z, i, rect);
    }

    public final boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 66 || i == 23) {
            setPressed(true);
            __();
        }
        return super.onKeyDown(i, keyEvent);
    }

    public final boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (i == 66 || i == 23) {
            _();
            setPressed(false);
            __();
        }
        return super.onKeyUp(i, keyEvent);
    }

    public final void setText(String str) {
        this.$$$ = str;
        this.__ = this.$$$$.measureText(str);
        this.___ = (float) getWidth();
        this._____ = (float) getPaddingLeft();
        this.$ = getTextSize() + ((this.____ - getTextSize()) / 4.0f);
        this.$$ = 0.0f;
        new Timer().schedule(new bbbbb(this), 3000);
    }

    public final void setTextColor(int i) {
        if (this.$$$$ != null) {
            this.$$$$.setColor(i);
        }
    }
}
