package com.madhouse.android.ads;

import android.content.Context;

public class AdManager {
    private static String $ = null;
    private static String $$ = null;
    private static String $$$ = null;
    private static String $$$$ = null;
    private static String $$$$$ = null;
    public static final String USER_GENDER_FEMALE = "female";
    public static final String USER_GENDER_MALE = "male";
    private static String _;
    private static String __;
    private static String ___;
    private static boolean ____ = true;
    private static String _____;

    protected static final String $() {
        return $;
    }

    protected static final String $$() {
        return $$;
    }

    protected static final String $$$() {
        return $$$;
    }

    protected static final String $$$$() {
        return $$$$;
    }

    protected static final String $$$$$() {
        return $$$$$;
    }

    private AdManager() {
    }

    protected static final String _() {
        return _;
    }

    static final void _(boolean z) {
        ____ = z;
    }

    protected static final String __() {
        return __;
    }

    protected static final String ___() {
        return ___;
    }

    protected static final boolean ____() {
        return ____;
    }

    protected static final String _____() {
        return _____;
    }

    public static final void setApplicationId(Context context, String str) {
        if (str != null && str.length() == 16) {
            _ = str;
        }
    }

    public static final void setBirthday(String str) {
        $ = str;
    }

    public static final void setCity(String str) {
        $$$ = str;
    }

    public static final void setFavorite(String str) {
        $$ = str;
    }

    public static final void setKeyword(String str) {
        _____ = str;
    }

    public static final void setPostalcode(String str) {
        $$$$ = str;
    }

    public static final void setUserAge(String str) {
        __ = str;
    }

    public static final void setUserGender(String str) {
        ___ = str;
    }

    public static final void setWork(String str) {
        $$$$$ = str;
    }
}
