package com.waps;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class OffersWebView extends Activity {
    /* access modifiers changed from: private */
    public String A = "";
    private String B = "";
    /* access modifiers changed from: private */
    public p C;
    private SharedPreferences D;
    private SharedPreferences E;
    private SharedPreferences F;
    /* access modifiers changed from: private */
    public SharedPreferences.Editor G;
    /* access modifiers changed from: private */
    public SharedPreferences.Editor H;
    /* access modifiers changed from: private */

    /* renamed from: I  reason: collision with root package name */
    public SharedPreferences.Editor f3I;
    private String J = "";
    /* access modifiers changed from: private */
    public PackageManager K;
    private List L;
    /* access modifiers changed from: private */
    public List M;
    private ListView N;
    private LinearLayout O;
    /* access modifiers changed from: private */
    public boolean P = false;
    private String Q = "";
    String a = "";
    o b;
    String c = "";
    String d = "";
    q e;
    File f = null;
    /* access modifiers changed from: private */
    public WebView g = null;
    private String h = null;
    /* access modifiers changed from: private */
    public ProgressBar i;
    private String j = "";
    private String k = "";
    private String l = "";
    private String m = "";
    private String n = "";
    /* access modifiers changed from: private */
    public String o = "false";
    /* access modifiers changed from: private */
    public String p;
    /* access modifiers changed from: private */
    public boolean q = true;
    private String r = "";
    private String s = "";
    private String t = "";
    private String u = "";
    private String v = "";
    private String w = "";
    /* access modifiers changed from: private */
    public String x = "";
    private String y = "";
    private String z = "";

    private void bindNewApp(PackageManager packageManager) {
        Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
        intent.addCategory("android.intent.category.LAUNCHER");
        this.L = packageManager.queryIntentActivities(intent, 0);
        this.M = getNewAppInfo(this.L, this.J);
        Collections.sort(this.M, new ResolveInfo.DisplayNameComparator(packageManager));
    }

    private void getAlreadyInstalledPackages(Context context) {
        List<PackageInfo> installedPackages = context.getPackageManager().getInstalledPackages(0);
        String str = "";
        for (int i2 = 0; i2 < installedPackages.size(); i2++) {
            str = str + installedPackages.get(i2).packageName;
        }
        SharedPreferences.Editor edit = context.getSharedPreferences("Package_Name", 3).edit();
        edit.putString("Package_Names", str);
        edit.commit();
    }

    /* access modifiers changed from: private */
    public void getDownload() {
        try {
            this.c = this.b.a(this.p);
            this.d = "/sdcard/download/";
            int b2 = (int) this.b.b(this.p);
            if (Environment.getExternalStorageState().equals("mounted")) {
                this.f = new File(this.d, this.c);
                if (this.f != null && this.f.length() == ((long) b2)) {
                    new AlertDialog.Builder(this).setTitle("温馨提示").setIcon(17301618).setMessage("该安装文件已存在于/sdcard/download/目录下，您可以直接安装或重新下载！").setPositiveButton("安装", new z(this)).setNeutralButton("重新下载", new y(this)).setNegativeButton("取消", new x(this)).create().show();
                } else if (this.f.length() >= ((long) b2) || this.f.length() == 0) {
                    if (this.f.length() == 0) {
                        Toast.makeText(this, "正在准备下载,请稍候...", 0).show();
                        this.b.execute(this.p);
                        if (this.o != null && "true".equals(this.o)) {
                            finish();
                        }
                    }
                } else if (o.h) {
                    Toast.makeText(this, "正在下载,请稍候...", 0).show();
                } else {
                    Toast.makeText(this, "正在准备下载,请稍候...", 0).show();
                    this.f.delete();
                    this.b.execute(this.p);
                    if (this.o != null && "true".equals(this.o)) {
                        finish();
                    }
                }
            } else {
                this.f = getFileStreamPath(this.c);
                if (this.f != null && this.f.length() == ((long) b2)) {
                    this.f.delete();
                    Toast.makeText(this, "正在准备下载,请稍候...", 0).show();
                    this.b.execute(this.p);
                    if (this.o != null && "true".equals(this.o)) {
                        finish();
                    }
                } else if (this.f != null && this.f.length() != 0) {
                    Toast.makeText(this, "正在下载,请稍候...", 0).show();
                } else if (this.f.length() == 0) {
                    Toast.makeText(this, "正在准备下载,请稍候...", 0).show();
                    this.b.execute(this.p);
                    if (this.o != null && "true".equals(this.o)) {
                        finish();
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void initMetaData(Bundle bundle) {
        if (bundle != null) {
            if (bundle.getString("Offers_URL") != null) {
                this.l = bundle.getString("Offers_URL");
            }
            if (bundle.getString("URL") != null) {
                this.l = bundle.getString("URL");
            }
            if (bundle.getString("UrlPath") != null) {
                this.r = bundle.getString("Notify_Id");
                this.s = bundle.getString("UrlPath");
                this.t = bundle.getString("ACTIVITY_FLAG");
                this.u = bundle.getString("SHWO_FLAG");
                if (this.s.contains("down_type")) {
                    this.v = bundle.getString("Notify_Url_Params");
                }
            }
            this.m = bundle.getString("URL_PARAMS");
            this.k = bundle.getString("CLIENT_PACKAGE");
            this.n = bundle.getString("USER_ID");
            this.o = bundle.getString("isFinshClose");
            this.m += "&publisher_user_id=" + this.n;
            this.m += "&at=" + System.currentTimeMillis();
            this.j = bundle.getString("offers_webview_tag");
        }
    }

    private void initNotityData(SharedPreferences sharedPreferences) {
        if (sharedPreferences != null) {
            this.w = sharedPreferences.getString("Notity_Id", "");
            this.y = sharedPreferences.getString("Notity_Title", "");
            this.z = sharedPreferences.getString("Notity_Content", "");
            this.x = sharedPreferences.getString("Notity_UrlPath", "");
            this.B = sharedPreferences.getString("offers_webview_tag", "");
            this.Q = sharedPreferences.getString("NotifyAd_Tag", "");
            if (this.x.contains("down_type")) {
                this.A = sharedPreferences.getString("Notity_UrlParams", "");
                this.A += "&at=" + System.currentTimeMillis();
            }
        }
    }

    private void showNewApp() {
        try {
            this.E = getSharedPreferences("DownLoadSave", 3);
            this.F = getSharedPreferences("Package_Name", 3);
            this.H = this.E.edit();
            this.f3I = this.F.edit();
            this.J = this.F.getString("Package_Names", "");
            this.K = getPackageManager();
            bindNewApp(this.K);
            this.O = new LinearLayout(this);
            this.O.setOrientation(1);
            this.O.setGravity(17);
            this.O.setBackgroundColor(-16777216);
            RelativeLayout relativeLayout = new RelativeLayout(this);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
            TextView textView = new TextView(this);
            textView.setText("新安装应用列表");
            textView.setTextSize(18.0f);
            textView.setTextColor(-1);
            textView.setPadding(0, 10, 0, 10);
            ImageView imageView = new ImageView(this);
            imageView.setImageResource(17301545);
            imageView.setId(2);
            imageView.setPadding(10, 0, 10, 0);
            layoutParams.addRule(1, imageView.getId());
            relativeLayout.addView(imageView);
            relativeLayout.addView(textView, layoutParams);
            LinearLayout linearLayout = new LinearLayout(this);
            linearLayout.setOrientation(0);
            linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 1.0f));
            this.N = new ListView(this);
            Button button = new Button(this);
            button.setGravity(1);
            button.setText("确　定");
            button.setTextSize(15.0f);
            button.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 1.0f));
            Button button2 = new Button(this);
            button2.setGravity(1);
            button2.setText("关　闭");
            button2.setTextSize(15.0f);
            button2.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 1.0f));
            linearLayout.addView(button);
            linearLayout.addView(button2);
            this.N.setAdapter((ListAdapter) new af(this, this, this.M));
            this.N.setOnItemClickListener(new ac(this));
            this.N.setBackgroundColor(-1);
            this.O.addView(relativeLayout);
            this.O.addView(this.N);
            this.O.addView(linearLayout);
            setContentView(this.O);
            button.setOnClickListener(new ad(this));
            button2.setOnClickListener(new v(this));
        } catch (Exception e2) {
        }
    }

    private void showNotifyList(SharedPreferences sharedPreferences) {
        this.E = getSharedPreferences("DownLoadSave", 3);
        this.F = getSharedPreferences("Package_Name", 3);
        this.H = this.E.edit();
        this.f3I = this.F.edit();
        this.J = this.F.getString("Package_Names", "");
        this.K = getPackageManager();
        bindNewApp(this.K);
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.M.size()) {
                String str = ((ResolveInfo) this.M.get(i3)).activityInfo.packageName;
                String string = sharedPreferences.getString("package_tag" + i3, "");
                if (string != null && !"".equals(string.trim()) && str.equals(string)) {
                    try {
                        startActivity(getPackageManager().getLaunchIntentForPackage(str));
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    private void showPushDialog() {
        try {
            this.D = getSharedPreferences("Notify", 3);
            initNotityData(this.D);
            this.G = this.D.edit();
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(this.y);
            builder.setMessage(this.z);
            if (this.x != null && !"".equals(this.x.trim())) {
                builder.setPositiveButton("确定", new aa(this));
            }
            builder.setNegativeButton("关闭", new ab(this));
            builder.show();
            AppConnect.getInstanceNoConnect(this).notify_receiver(this.w, 1);
        } catch (Exception e2) {
        }
    }

    public List getNewAppInfo(List list, String str) {
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < list.size(); i2++) {
            ResolveInfo resolveInfo = (ResolveInfo) list.get(i2);
            String str2 = resolveInfo.activityInfo.packageName;
            if (str != null && !"".equals(str) && !str.contains(str2)) {
                arrayList.add(resolveInfo);
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        Intent intent;
        RelativeLayout.LayoutParams layoutParams;
        RelativeLayout.LayoutParams layoutParams2;
        initMetaData(getIntent().getExtras());
        initNotityData(getSharedPreferences("Notify", 3));
        if ("feedback".equals(this.t)) {
            this.h = this.s + "?" + this.m;
        } else if (this.j == null || "".equals(this.j)) {
            if (!this.x.contains("down_type")) {
                AppConnect.getInstanceNoConnect(this).notify_receiver(this.w, 1);
                this.j = this.B;
                this.h = this.x + "?" + "nyid" + "=" + this.r + this.A;
            } else if (this.Q.equals("true")) {
                this.j = this.B;
                this.h = this.x + "&publisher_user_id=" + this.n + "&" + "at" + "=" + System.currentTimeMillis();
                this.o = "true";
                AppConnect.getInstanceNoConnect(this).notify_receiver(this.w, 1);
            } else if (this.Q.equals("false")) {
                AppConnect.getInstanceNoConnect(this).notify_receiver(this.w, 1);
                this.j = this.B;
                this.h = this.x + "&" + this.A;
                this.o = "true";
            }
        } else if (this.l.indexOf("?") > -1) {
            this.h = this.l + this.m;
        } else {
            this.h = this.l + "?a=1" + this.m;
        }
        this.h = this.h.replaceAll(" ", "%20");
        if (this.j == null || "".equals(this.j.trim()) || !this.j.equals("OffersWebView")) {
            SharedPreferences sharedPreferences = getSharedPreferences("Start_Tag", 3);
            SharedPreferences.Editor edit = sharedPreferences.edit();
            String string = sharedPreferences.getString("notify_start_tag", "");
            if (string == null || "".equals(string)) {
                try {
                    Intent intent2 = new Intent();
                    intent2.setComponent(new ComponentName(AppConnect.A.getPackageName(), AppConnect.A.getShortClassName().substring(1)));
                    startActivity(intent2);
                } catch (Exception e2) {
                }
            } else {
                setTheme(16973835);
                super.onCreate(bundle);
                requestWindowFeature(1);
                this.C = new p(this);
                if (q.c && AppConnect.E) {
                    this.E = getSharedPreferences("DownLoadSave", 3);
                    this.F = getSharedPreferences("Package_Name", 3);
                    this.H = this.E.edit();
                    this.f3I = this.F.edit();
                    if (this.E.getAll().size() == 1) {
                        try {
                            intent = getPackageManager().getLaunchIntentForPackage(this.E.getString("0", ""));
                        } catch (Exception e3) {
                            e3.printStackTrace();
                            intent = null;
                        }
                        startActivity(intent);
                        this.C.a(2);
                        edit.clear();
                        edit.commit();
                        finish();
                        return;
                    }
                    showNewApp();
                    edit.clear();
                    edit.commit();
                }
            }
        } else {
            super.onCreate(bundle);
            requestWindowFeature(1);
            getWindowManager().getDefaultDisplay().getWidth();
            getWindowManager().getDefaultDisplay().getHeight();
            RelativeLayout relativeLayout = new RelativeLayout(this);
            this.g = new WebView(this);
            this.i = new ProgressBar(this);
            this.i.setEnabled(true);
            this.i.setVisibility(0);
            WebSettings settings = this.g.getSettings();
            if (this.t == null || "".equals(this.t.trim())) {
                RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, -1);
                LinearLayout linearLayout = new LinearLayout(this);
                linearLayout.setOrientation(1);
                linearLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
                relativeLayout.setGravity(17);
                RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams4.addRule(13);
                relativeLayout.addView(this.g, layoutParams3);
                relativeLayout.addView(this.i, layoutParams4);
                linearLayout.addView(relativeLayout, new ViewGroup.LayoutParams(-1, -1));
                setContentView(linearLayout);
            } else {
                if ("notify".equals(this.t)) {
                    RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(-1, -2);
                    RelativeLayout.LayoutParams layoutParams6 = new RelativeLayout.LayoutParams(-1, -1);
                    if ("show".equals(this.u)) {
                        AppConnect.getInstanceNoConnect(this).notify_receiver(this.r, 0);
                    }
                    RelativeLayout.LayoutParams layoutParams7 = layoutParams6;
                    layoutParams = layoutParams5;
                    layoutParams2 = layoutParams7;
                } else {
                    layoutParams = null;
                    layoutParams2 = null;
                }
                if ("feedback".equals(this.t)) {
                    RelativeLayout.LayoutParams layoutParams8 = new RelativeLayout.LayoutParams(-1, -2);
                    RelativeLayout.LayoutParams layoutParams9 = new RelativeLayout.LayoutParams(-1, -1);
                    relativeLayout.setBackgroundColor(-1);
                    setRequestedOrientation(1);
                    RelativeLayout.LayoutParams layoutParams10 = layoutParams9;
                    layoutParams = layoutParams8;
                    layoutParams2 = layoutParams10;
                }
                RelativeLayout.LayoutParams layoutParams11 = new RelativeLayout.LayoutParams(-2, -2);
                this.g.setId(2);
                this.g.setLayoutParams(layoutParams2);
                Button button = new Button(this);
                button.setLayoutParams(layoutParams);
                button.setId(1);
                button.setText("关　闭");
                button.getBackground().setAlpha(100);
                this.i.setLayoutParams(layoutParams11);
                layoutParams.addRule(12);
                layoutParams2.addRule(2, 1);
                layoutParams11.addRule(13);
                relativeLayout.addView(this.g, layoutParams2);
                relativeLayout.addView(this.i, layoutParams11);
                setContentView(relativeLayout);
                button.setOnClickListener(new u(this));
            }
            this.g.setWebViewClient(new ae(this, null));
            this.g.addJavascriptInterface(new SDKUtils(this), "SDKUtils");
            settings.setJavaScriptEnabled(true);
            WebView webView = this.g;
            WebView.enablePlatformNotifications();
            this.g.setScrollbarFadingEnabled(true);
            this.g.setScrollBarStyle(0);
            if (this.o != null && "true".equals(this.o)) {
                Toast.makeText(this, "加载中,请稍候...", 0).show();
            }
            this.g.loadUrl(this.h);
            this.g.setDownloadListener(new w(this));
            if (getSharedPreferences("Package_Name", 3).getString("Package_Names", "") == "") {
                getAlreadyInstalledPackages(this);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.g != null) {
            this.g.destroy();
        }
        finish();
        super.onDestroy();
    }

    public void onItemClick(AdapterView adapterView, View view, int i2, long j2) {
        ResolveInfo resolveInfo = (ResolveInfo) this.M.get(i2);
        ComponentName componentName = new ComponentName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.name);
        Intent intent = new Intent();
        intent.setComponent(componentName);
        startActivity(intent);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (this.g == null || i2 != 4 || !this.g.canGoBack()) {
            return super.onKeyDown(i2, keyEvent);
        }
        if (!this.q) {
            finish();
            this.q = true;
        }
        this.g.goBack();
        p.c = true;
        return true;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (!(this.h == null || this.g == null)) {
            this.g.loadUrl(this.h);
        }
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }
}
