package com.waps;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.view.View;
import android.widget.AdapterView;

class ac implements AdapterView.OnItemClickListener {
    final /* synthetic */ OffersWebView a;

    ac(OffersWebView offersWebView) {
        this.a = offersWebView;
    }

    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        ResolveInfo resolveInfo = (ResolveInfo) this.a.M.get(i);
        ComponentName componentName = new ComponentName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.name);
        Intent intent = new Intent();
        intent.setComponent(componentName);
        this.a.startActivity(intent);
    }
}
