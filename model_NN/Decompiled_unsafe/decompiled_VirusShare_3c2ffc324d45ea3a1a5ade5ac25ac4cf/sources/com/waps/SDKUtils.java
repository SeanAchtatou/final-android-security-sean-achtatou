package com.waps;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.widget.Toast;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class SDKUtils {
    /* access modifiers changed from: private */
    public Activity a;
    private PackageManager b;
    private ApplicationInfo c;
    private String d = "";
    private PackageInfo e;
    private final ScheduledExecutorService f = Executors.newScheduledThreadPool(1);

    public SDKUtils(Activity activity) {
        this.a = activity;
    }

    public void close() {
        this.a.finish();
    }

    public void closeOfDialog(String str) {
        submit("提示", str);
    }

    public void closeSubmit(String str) {
        Toast.makeText(this.a, str, 1).show();
        this.a.finish();
    }

    public String getCountryCode() {
        return Locale.getDefault().getCountry();
    }

    public String getDeviceName() {
        return Build.MODEL;
    }

    public String getDeviceOSVersion() {
        return Build.VERSION.RELEASE;
    }

    public String getImsi() {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) this.a.getSystemService("phone");
            return telephonyManager != null ? telephonyManager.getSubscriberId() : "";
        } catch (Exception e2) {
            e2.printStackTrace();
            return "";
        }
    }

    public String getInstalled() {
        String str;
        Exception e2;
        try {
            this.b = this.a.getPackageManager();
            List<PackageInfo> installedPackages = this.b.getInstalledPackages(0);
            String str2 = "";
            int i = 0;
            while (i < installedPackages.size()) {
                try {
                    PackageInfo packageInfo = installedPackages.get(i);
                    int i2 = packageInfo.applicationInfo.flags;
                    ApplicationInfo applicationInfo = packageInfo.applicationInfo;
                    if ((i2 & 1) <= 0) {
                        str2 = str2 + packageInfo.packageName + ";";
                    }
                    i++;
                } catch (Exception e3) {
                    e2 = e3;
                    str = str2;
                    e2.printStackTrace();
                    return str;
                }
            }
            return str2;
        } catch (Exception e4) {
            Exception exc = e4;
            str = "";
            e2 = exc;
            e2.printStackTrace();
            return str;
        }
    }

    public String getLanguageCode() {
        return Locale.getDefault().getLanguage();
    }

    public String getParams() {
        return AppConnect.getInstance(this.a).getParams(this.a);
    }

    public void getPushAd() {
        AppConnect.getInstance(this.a).getPushAd();
    }

    public String getSDKVersion() {
        try {
            this.e = this.b.getPackageInfo(this.a.getPackageName(), 0);
            return this.e.versionName;
        } catch (Exception e2) {
            e2.printStackTrace();
            return "";
        }
    }

    public String getUdid() {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) this.a.getSystemService("phone");
            return telephonyManager != null ? telephonyManager.getDeviceId() : "";
        } catch (Exception e2) {
            e2.printStackTrace();
            return "";
        }
    }

    public String getWAPS_ID() {
        try {
            return AppConnect.getInstance(this.a).getWapsId();
        } catch (Exception e2) {
            e2.printStackTrace();
            return "";
        }
    }

    public String getWAPS_PID() {
        this.b = this.a.getPackageManager();
        try {
            this.c = this.b.getApplicationInfo(this.a.getPackageName(), 128);
            Object obj = this.c.metaData.get("WAPS_PID");
            if (obj == null) {
                return "";
            }
            this.d = obj.toString();
            return (this.d == null || this.d.equals("")) ? "" : this.d;
        } catch (Exception e2) {
            e2.printStackTrace();
            return "";
        }
    }

    public String isInstalled(String str) {
        try {
            return this.a.getPackageManager().getLaunchIntentForPackage("com.waps") != null ? "true" : "false";
        } catch (Exception e2) {
            e2.printStackTrace();
            return "";
        }
    }

    public void load(String str) {
        if (str != null) {
            try {
                if (!"".equals(str)) {
                    this.b = this.a.getPackageManager();
                    this.a.startActivity(this.b.getLaunchIntentForPackage(str));
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public void submit(String str, String str2) {
        if (str2 == null || "".equals(str2)) {
            this.a.finish();
        } else {
            new AlertDialog.Builder(this.a).setTitle(str).setMessage(str2).setPositiveButton("确定", new ag(this)).create().show();
        }
    }
}
