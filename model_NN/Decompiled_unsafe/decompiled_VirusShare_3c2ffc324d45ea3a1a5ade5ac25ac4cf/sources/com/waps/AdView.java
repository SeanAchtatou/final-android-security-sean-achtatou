package com.waps;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

public class AdView implements DisplayAdNotifier {
    static long e = 0;
    static int f = 30;
    LinearLayout a;
    boolean b = false;
    View c;
    Context d;
    String g;
    private int[] h;
    private int i;
    private AnimationType j;
    private int k = -1;
    /* access modifiers changed from: private */
    public String l = "";
    final Handler mHandler = new Handler();
    final Runnable mUpdateResults = new b(this);

    public AdView(Context context, LinearLayout linearLayout) {
        this.d = context;
        this.a = linearLayout;
        this.h = new int[]{0};
        this.i = 0;
        this.k = 0;
    }

    /* access modifiers changed from: private */
    public void updateResultsInUi() {
        try {
            if (this.a.getChildCount() != 0) {
                this.a.removeView(this.a.getChildAt(0));
            }
            this.a.removeAllViews();
            if (this.c != null && this.b) {
                this.a.refreshDrawableState();
                this.a.setAlwaysDrawnWithCacheEnabled(true);
                this.a.clearFocus();
                this.a.clearDisappearingChildren();
                this.a.addView(this.c);
                this.l = Build.VERSION.SDK;
                Integer.parseInt(this.l);
                this.a.clearAnimation();
                if (this.k == 0) {
                    this.j = new AnimationType(this.h);
                } else if (this.k == 1) {
                    this.j = new AnimationType(this.i);
                } else if (this.k == 2) {
                    this.j = new AnimationType(this.h);
                }
                this.j.startAnimation(this.a);
                this.b = false;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void DisplayAd() {
        DisplayAd(f);
    }

    public void DisplayAd(int i2) {
        if (this.d.getSharedPreferences("ShowAdFlag", 3).getBoolean("show_ad_flag", true)) {
            showADS();
            e = System.currentTimeMillis();
            if (f < 20) {
                f = 20;
            }
            new a(this).start();
        }
    }

    public void getDisplayAdResponse(View view) {
        if (view != null) {
            ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            int width = ((Activity) this.d).getResources().getConfiguration().orientation == 1 ? ((Activity) this.d).getWindowManager().getDefaultDisplay().getWidth() : ((Activity) this.d).getResources().getConfiguration().orientation == 2 ? ((Activity) this.d).getWindowManager().getDefaultDisplay().getHeight() : 0;
            this.c = view;
            this.c.setLayoutParams(new ViewGroup.LayoutParams(width, (int) (((double) width) / (((double) layoutParams.width) / ((double) layoutParams.height)))));
            this.b = true;
            this.mHandler.post(this.mUpdateResults);
            return;
        }
        this.c = view;
        this.b = true;
        this.mHandler.post(this.mUpdateResults);
    }

    public void getDisplayAdResponseFailed(String str) {
        this.b = false;
        this.mHandler.post(this.mUpdateResults);
    }

    public AdView setAnimationType(int i2) {
        this.i = i2;
        this.k = 1;
        return this;
    }

    public AdView setAnimationType(int[] iArr) {
        this.h = iArr;
        this.k = 2;
        return this;
    }

    public void showADS() {
        AppConnect.getInstance(this.d).getDisplayAd(this);
    }
}
