package com.waps;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Looper;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public final class AppConnect {
    public static ComponentName A = null;
    protected static String B = "";
    protected static String C = "";
    public static boolean E = false;

    /* renamed from: I  reason: collision with root package name */
    private static AppConnect f2I = null;
    /* access modifiers changed from: private */
    public static r J = null;
    private static DisplayAd K = null;
    public static final String LIBRARY_VERSION_NUMBER = "1.5.2";
    private static boolean aA = true;
    private static String aD = "";
    /* access modifiers changed from: private */
    public static boolean aE = false;
    private static boolean aF = true;
    private static final byte[] aL = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, 62, -1, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, 63, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51};
    /* access modifiers changed from: private */
    public static boolean ai = true;
    /* access modifiers changed from: private */
    public static boolean aj = false;
    private static String ak = null;
    /* access modifiers changed from: private */
    public static UpdatePointsNotifier ao;
    /* access modifiers changed from: private */
    public static String au = "";
    /* access modifiers changed from: private */
    public static String av = "receiver/install?";
    /* access modifiers changed from: private */
    public static String aw = "install";
    n D;
    private k F = null;
    /* access modifiers changed from: private */
    public Context G = null;
    /* access modifiers changed from: private */
    public final ScheduledExecutorService H = Executors.newScheduledThreadPool(1);
    /* access modifiers changed from: private */
    public String L = "";
    private String M = "";
    private String N = "";
    private String O = "";
    private String P = "";
    private String Q = "";
    /* access modifiers changed from: private */
    public String R = "";
    /* access modifiers changed from: private */
    public String S = "";
    private String T = "";
    private String U = "";
    /* access modifiers changed from: private */
    public String V = "";
    /* access modifiers changed from: private */
    public String W = "";
    private String X = "http://app.wapx.cn/action/account/offerlist?";
    private String Y = "http://app.wapx.cn/action/account/ownslist?";
    /* access modifiers changed from: private */
    public String Z = "";
    final String a = "x";
    private int aB = 0;
    private boolean aC = true;
    private String aG = "";
    private String aH = "";
    private String aI = "";
    private String aJ = "";
    private Bitmap aK;
    private byte[] aM;
    private int aN;
    private int aO;
    private boolean aP;
    private int aQ;
    /* access modifiers changed from: private */
    public String aa = "";
    /* access modifiers changed from: private */
    public String ab = "";
    private String ac = "";
    private String ad = "";
    private String ae = "";
    private int af = 0;
    private int ag = 0;
    /* access modifiers changed from: private */
    public String ah = "";
    private String al = "";
    private String am = "";
    private String an = "";
    private g ap = null;
    private j aq = null;
    private f ar = null;
    private i as = null;
    /* access modifiers changed from: private */
    public h at = null;
    private boolean ax = true;
    private int ay;
    private String az = "";
    final String b = "y";
    final String c = "net";
    final String d = "imsi";
    final String e = "udid";
    final String f = "device_name";
    final String g = "device_type";
    final String h = "os_version";
    final String i = "country_code";
    final String j = "language";
    final String k = "app_version";
    final String l = "sdk_version";
    final String m = "act";
    final String n = "userid";
    final String o = "channel";
    final String p = "points";
    final String q = "points";
    final String r = "install";
    final String s = "uninstall";
    final String t = "load";
    final String u = "device_width";
    final String v = "device_height";
    protected String w = "";
    protected String x = "";
    protected String y = "";
    protected String z = "";

    public AppConnect() {
    }

    private AppConnect(Context context) {
        this.aa = getParams(context);
        if (Looper.myLooper() == null) {
            Looper.prepare();
        }
        this.F = new k(this, null);
        this.F.execute(new Void[0]);
    }

    public AppConnect(Context context, int i2) {
        this.aa = getParams(context);
    }

    private AppConnect(Context context, String str) {
        this.G = context;
        this.aa = getParams(context);
        this.aa += "&userid=" + str;
        if (Looper.myLooper() == null) {
            Looper.prepare();
        }
        this.F = new k(this, null);
        this.F.execute(new Void[0]);
    }

    private void FinishApplicationRunnable() {
        if (((Activity) this.G).isFinishing() && q.c) {
            new p(this.G).a();
            E = true;
        }
        if (((Activity) this.G).isFinishing() && !o.g) {
            SharedPreferences.Editor edit = this.G.getSharedPreferences("Package_Name", 3).edit();
            edit.clear();
            edit.commit();
            q.c = false;
        }
    }

    /* access modifiers changed from: private */
    public void UpdateDialog(String str) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(this.G);
            builder.setTitle("新版提示");
            builder.setMessage("有新版本(" + this.ah + "),是否下载?");
            builder.setPositiveButton("下载", new d(this, str));
            builder.setNegativeButton("下次再说", new e(this));
            builder.show();
        } catch (Exception e2) {
        }
    }

    private void awardPointsHelper() {
        this.ar = new f(this, null);
        this.ar.execute(new Void[0]);
    }

    private Document buildDocument(String str) {
        try {
            DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
            return newInstance.newDocumentBuilder().parse(new ByteArrayInputStream(str.getBytes("UTF-8")));
        } catch (Exception e2) {
            return null;
        }
    }

    /* access modifiers changed from: private */
    public boolean buildResponse(String str) {
        DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
        try {
            Document parse = newInstance.newDocumentBuilder().parse(new ByteArrayInputStream(str.getBytes("UTF-8")));
            this.aH = getNodeTrimValue(parse.getElementsByTagName("Title"));
            this.aI = getNodeTrimValue(parse.getElementsByTagName("Content"));
            this.aJ = getNodeTrimValue(parse.getElementsByTagName("ClickUrl"));
            String nodeTrimValue = getNodeTrimValue(parse.getElementsByTagName("Image"));
            decodeBase64(nodeTrimValue.getBytes(), 0, nodeTrimValue.getBytes().length);
            this.aK = BitmapFactory.decodeByteArray(this.aM, 0, this.aN);
            SharedPreferences sharedPreferences = this.G.getSharedPreferences("Push_Settings", 3);
            new p(this.G).a(sharedPreferences.getInt("push_icon", 0), sharedPreferences.getBoolean("push_sound", true), this.aK, this.aH, this.aI, this.aJ);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return false;
    }

    private void getAlreadyInstalledPackages(Context context) {
        List<PackageInfo> installedPackages = context.getPackageManager().getInstalledPackages(0);
        String str = "";
        for (int i2 = 0; i2 < installedPackages.size(); i2++) {
            str = str + installedPackages.get(i2).packageName;
        }
        SharedPreferences.Editor edit = context.getSharedPreferences("Package_Name", 3).edit();
        edit.putString("Package_Names", str);
        edit.commit();
    }

    public static AppConnect getInstance(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("PushFlag", 3);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        if (sharedPreferences.getString("push_flag", "").equals("")) {
            edit.putString("push_flag", "true");
        }
        edit.commit();
        if (J == null) {
            J = new r();
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (!(activeNetworkInfo == null || activeNetworkInfo.getExtraInfo() == null || !activeNetworkInfo.getExtraInfo().toLowerCase().contains("wap"))) {
                J.a(true);
            }
        }
        if (f2I == null) {
            f2I = new AppConnect(context);
        }
        if (K == null) {
            K = new DisplayAd(context);
        }
        return f2I;
    }

    public static AppConnect getInstance(Context context, int i2) {
        if (J == null) {
            J = new r();
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (!(activeNetworkInfo == null || activeNetworkInfo.getExtraInfo() == null || !activeNetworkInfo.getExtraInfo().toLowerCase().contains("wap"))) {
                J.a(true);
            }
        }
        if (f2I == null) {
            f2I = new AppConnect(context, i2);
        }
        if (K == null) {
            K = new DisplayAd(context);
        }
        return f2I;
    }

    public static AppConnect getInstance(Context context, String str) {
        if (J == null) {
            J = new r();
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (!(activeNetworkInfo == null || activeNetworkInfo.getExtraInfo() == null || !activeNetworkInfo.getExtraInfo().toLowerCase().contains("wap"))) {
                J.a(true);
            }
        }
        if (f2I == null) {
            f2I = new AppConnect(context, str);
        }
        if (K == null) {
            K = new DisplayAd(context);
        }
        return f2I;
    }

    public static AppConnect getInstance(String str, Context context) {
        aD = str;
        if (aA) {
            List<ActivityManager.RunningTaskInfo> runningTasks = ((ActivityManager) context.getSystemService("activity")).getRunningTasks(2);
            if (runningTasks != null && !runningTasks.isEmpty()) {
                A = runningTasks.get(0).baseActivity;
            }
            aA = false;
        }
        if (J == null) {
            J = new r();
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (!(activeNetworkInfo == null || activeNetworkInfo.getExtraInfo() == null || !activeNetworkInfo.getExtraInfo().toLowerCase().contains("wap"))) {
                J.a(true);
            }
        }
        if (f2I == null) {
            f2I = new AppConnect(context);
        }
        if (K == null) {
            K = new DisplayAd(context);
        }
        return f2I;
    }

    public static AppConnect getInstanceNoConnect(Context context) {
        if (J == null) {
            J = new r();
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (!(activeNetworkInfo == null || activeNetworkInfo.getExtraInfo() == null || !activeNetworkInfo.getExtraInfo().toLowerCase().contains("wap"))) {
                J.a(true);
            }
        }
        if (f2I == null) {
            f2I = new AppConnect(context, 0);
        }
        if (K == null) {
            K = new DisplayAd(context);
        }
        return f2I;
    }

    private String getNodeTrimValue(NodeList nodeList) {
        Element element = (Element) nodeList.item(0);
        if (element == null) {
            return null;
        }
        NodeList childNodes = element.getChildNodes();
        int length = childNodes.getLength();
        String str = "";
        for (int i2 = 0; i2 < length; i2++) {
            Node item = childNodes.item(i2);
            if (item != null) {
                str = str + item.getNodeValue();
            }
        }
        if (str == null || str.equals("")) {
            return null;
        }
        return str.trim();
    }

    private void getPointsHelper() {
        this.ap = new g(this, null);
        this.ap.execute(new Void[0]);
    }

    /* access modifiers changed from: private */
    public void getPushAd(String str, String str2, String str3) {
        getPushAdDateFromServer("http://app.wapx.cn/action/", str2, str3);
    }

    private void getPushAdDateFromServer(String str, String str2, String str3) {
        this.D = new n(this, str + str2, str3);
        this.D.execute(new Void[0]);
    }

    /* access modifiers changed from: private */
    public boolean handleAwardPointsResponse(String str) {
        Document buildDocument = buildDocument(str);
        if (buildDocument != null) {
            String nodeTrimValue = getNodeTrimValue(buildDocument.getElementsByTagName("Success"));
            if (nodeTrimValue != null && nodeTrimValue.equals("true")) {
                String nodeTrimValue2 = getNodeTrimValue(buildDocument.getElementsByTagName("Points"));
                String nodeTrimValue3 = getNodeTrimValue(buildDocument.getElementsByTagName("CurrencyName"));
                if (!(nodeTrimValue2 == null || nodeTrimValue3 == null)) {
                    ao.getUpdatePoints(nodeTrimValue3, Integer.parseInt(nodeTrimValue2));
                    return true;
                }
            } else if (nodeTrimValue != null && nodeTrimValue.endsWith("false")) {
                ao.getUpdatePointsFailed(getNodeTrimValue(buildDocument.getElementsByTagName("Message")));
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public boolean handleConnectResponse(String str) {
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        String str7;
        try {
            Document buildDocument = buildDocument(str);
            if (!(buildDocument == null || buildDocument.getElementsByTagName("Version") == null)) {
                String nodeTrimValue = getNodeTrimValue(buildDocument.getElementsByTagName("Success"));
                String nodeTrimValue2 = getNodeTrimValue(buildDocument.getElementsByTagName("Version"));
                String nodeTrimValue3 = getNodeTrimValue(buildDocument.getElementsByTagName("Clear"));
                if (buildDocument.getElementsByTagName("Notify") == null || buildDocument.getElementsByTagName("Notify").getLength() <= 0) {
                    str2 = "";
                    str3 = "";
                    str4 = "";
                    str5 = "";
                } else {
                    String nodeTrimValue4 = getNodeTrimValue(buildDocument.getElementsByTagName("Id"));
                    String nodeTrimValue5 = getNodeTrimValue(buildDocument.getElementsByTagName("title"));
                    String nodeTrimValue6 = getNodeTrimValue(buildDocument.getElementsByTagName("content"));
                    str2 = nodeTrimValue4;
                    str3 = getNodeTrimValue(buildDocument.getElementsByTagName("url"));
                    String str8 = nodeTrimValue5;
                    str4 = nodeTrimValue6;
                    str5 = str8;
                }
                String nodeTrimValue7 = getNodeTrimValue(buildDocument.getElementsByTagName("AppList"));
                if (buildDocument.getElementsByTagName("Push") == null || buildDocument.getElementsByTagName("Push").getLength() <= 0) {
                    aE = false;
                    str6 = "";
                    str7 = "";
                } else {
                    String nodeTrimValue8 = getNodeTrimValue(buildDocument.getElementsByTagName("Wait"));
                    String nodeTrimValue9 = getNodeTrimValue(buildDocument.getElementsByTagName("Loop"));
                    aE = true;
                    String str9 = nodeTrimValue9;
                    str6 = nodeTrimValue8;
                    str7 = str9;
                }
                this.aG = getNodeTrimValue(buildDocument.getElementsByTagName("AD"));
                if (this.aG == null || "".equals(this.aG) || !this.aG.equals("0")) {
                    aF = true;
                } else {
                    aF = false;
                }
                SharedPreferences.Editor edit = this.G.getSharedPreferences("ShowAdFlag", 3).edit();
                edit.putBoolean("show_ad_flag", aF);
                edit.commit();
                if (nodeTrimValue != null && nodeTrimValue.equals("true")) {
                    if (nodeTrimValue2 != null && !"".equals(nodeTrimValue2)) {
                        this.ah = nodeTrimValue2;
                    }
                    if (nodeTrimValue3 != null && !"".equals(nodeTrimValue3.trim())) {
                        aj = true;
                    }
                    if (str2 != null && !"".equals(str2.trim())) {
                        this.w = str2;
                    }
                    if (str5 != null && !"".equals(str5.trim())) {
                        this.x = str5;
                    }
                    if (str4 != null && !"".equals(str4.trim())) {
                        this.y = str4;
                    }
                    if (str3 != null && !"".equals(str3.trim())) {
                        this.z = str3;
                    }
                    if (nodeTrimValue7 != null && !"".equals(nodeTrimValue7.trim())) {
                        SharedPreferences.Editor edit2 = this.G.getSharedPreferences("Finalize_Flag", 3).edit();
                        edit2.putString("appList", nodeTrimValue7);
                        edit2.commit();
                    }
                    if (str6 != null && !"".equals(str6.trim())) {
                        B = str6;
                    }
                    if (str7 != null && !"".equals(str7.trim())) {
                        C = str7;
                    }
                    return true;
                }
            }
        } catch (Exception e2) {
        }
        return false;
    }

    /* access modifiers changed from: private */
    public boolean handleGetPointsResponse(String str) {
        String nodeTrimValue;
        Document buildDocument = buildDocument(str);
        if (!(buildDocument == null || (nodeTrimValue = getNodeTrimValue(buildDocument.getElementsByTagName("Success"))) == null || !nodeTrimValue.equals("true"))) {
            this.G.getSharedPreferences("Points", 0);
            String nodeTrimValue2 = getNodeTrimValue(buildDocument.getElementsByTagName("Points"));
            String nodeTrimValue3 = getNodeTrimValue(buildDocument.getElementsByTagName("CurrencyName"));
            if (!(nodeTrimValue2 == null || nodeTrimValue3 == null)) {
                ao.getUpdatePoints(nodeTrimValue3, Integer.parseInt(nodeTrimValue2));
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public boolean handleSpendPointsResponse(String str) {
        Document buildDocument = buildDocument(str);
        if (buildDocument != null) {
            String nodeTrimValue = getNodeTrimValue(buildDocument.getElementsByTagName("Success"));
            if (nodeTrimValue != null && nodeTrimValue.equals("true")) {
                String nodeTrimValue2 = getNodeTrimValue(buildDocument.getElementsByTagName("Points"));
                String nodeTrimValue3 = getNodeTrimValue(buildDocument.getElementsByTagName("CurrencyName"));
                if (!(nodeTrimValue2 == null || nodeTrimValue3 == null)) {
                    ao.getUpdatePoints(nodeTrimValue3, Integer.parseInt(nodeTrimValue2));
                    return true;
                }
            } else if (nodeTrimValue != null && nodeTrimValue.endsWith("false")) {
                ao.getUpdatePointsFailed(getNodeTrimValue(buildDocument.getElementsByTagName("Message")));
                return true;
            }
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    /* access modifiers changed from: private */
    public void loadApps() {
        FileInputStream fileInputStream;
        FileOutputStream fileOutputStream;
        BufferedReader bufferedReader;
        FileInputStream fileInputStream2;
        File file;
        FileOutputStream fileOutputStream2;
        int i2 = 0;
        new Intent("android.intent.action.MAIN", (Uri) null).addCategory("android.intent.category.LAUNCHER");
        String str = "";
        try {
            if (Environment.getExternalStorageState().equals("mounted")) {
                File file2 = new File(Environment.getExternalStorageDirectory().toString() + "/Android");
                File file3 = new File(Environment.getExternalStorageDirectory().toString() + "/Android/Package.dat");
                if (!file2.exists()) {
                    file2.mkdir();
                }
                if (!file3.exists()) {
                    file3.createNewFile();
                }
                FileInputStream fileInputStream3 = new FileInputStream(file3);
                try {
                    BufferedReader bufferedReader2 = new BufferedReader(new InputStreamReader(fileInputStream3));
                    if (bufferedReader2 != null) {
                        while (true) {
                            try {
                                String readLine = bufferedReader2.readLine();
                                if (readLine == null) {
                                    break;
                                }
                                str = str + readLine;
                            } catch (Exception e2) {
                                e = e2;
                                fileOutputStream = null;
                                BufferedReader bufferedReader3 = bufferedReader2;
                                fileInputStream = fileInputStream3;
                                bufferedReader = bufferedReader3;
                                try {
                                    e.printStackTrace();
                                    try {
                                        fileOutputStream.close();
                                        fileInputStream.close();
                                        bufferedReader.close();
                                    } catch (Exception e3) {
                                        e3.printStackTrace();
                                        return;
                                    }
                                } catch (Throwable th) {
                                    th = th;
                                    try {
                                        fileOutputStream.close();
                                        fileInputStream.close();
                                        bufferedReader.close();
                                    } catch (Exception e4) {
                                        e4.printStackTrace();
                                    }
                                    throw th;
                                }
                            } catch (Throwable th2) {
                                th = th2;
                                fileOutputStream = null;
                                BufferedReader bufferedReader4 = bufferedReader2;
                                fileInputStream = fileInputStream3;
                                bufferedReader = bufferedReader4;
                                fileOutputStream.close();
                                fileInputStream.close();
                                bufferedReader.close();
                                throw th;
                            }
                        }
                    }
                    BufferedReader bufferedReader5 = bufferedReader2;
                    file = file3;
                    fileInputStream2 = fileInputStream3;
                    bufferedReader = bufferedReader5;
                } catch (Exception e5) {
                    e = e5;
                    fileOutputStream = null;
                    fileInputStream = fileInputStream3;
                    bufferedReader = null;
                    e.printStackTrace();
                    fileOutputStream.close();
                    fileInputStream.close();
                    bufferedReader.close();
                } catch (Throwable th3) {
                    th = th3;
                    fileOutputStream = null;
                    fileInputStream = fileInputStream3;
                    bufferedReader = null;
                    fileOutputStream.close();
                    fileInputStream.close();
                    bufferedReader.close();
                    throw th;
                }
            } else {
                bufferedReader = null;
                fileInputStream2 = null;
                file = null;
            }
            try {
                List<PackageInfo> installedPackages = this.G.getPackageManager().getInstalledPackages(0);
                for (int i3 = 0; i3 < installedPackages.size(); i3++) {
                    PackageInfo packageInfo = installedPackages.get(i3);
                    int i4 = packageInfo.applicationInfo.flags;
                    ApplicationInfo applicationInfo = packageInfo.applicationInfo;
                    if ((i4 & 1) <= 0) {
                        i2++;
                        String str2 = packageInfo.packageName;
                        if (str2.startsWith("com.")) {
                            String substring = str2.substring(3, str2.length());
                            if (!str.contains(substring)) {
                                au += substring + ";";
                            }
                        }
                    }
                }
                byte[] bytes = au.getBytes("UTF-8");
                if (file != null) {
                    FileOutputStream fileOutputStream3 = new FileOutputStream(file, true);
                    try {
                        fileOutputStream3.write(bytes);
                        fileOutputStream2 = fileOutputStream3;
                    } catch (Exception e6) {
                        e = e6;
                        fileInputStream = fileInputStream2;
                        fileOutputStream = fileOutputStream3;
                        e.printStackTrace();
                        fileOutputStream.close();
                        fileInputStream.close();
                        bufferedReader.close();
                    } catch (Throwable th4) {
                        th = th4;
                        fileInputStream = fileInputStream2;
                        fileOutputStream = fileOutputStream3;
                        fileOutputStream.close();
                        fileInputStream.close();
                        bufferedReader.close();
                        throw th;
                    }
                } else {
                    fileOutputStream2 = null;
                }
                try {
                    fileOutputStream2.close();
                    fileInputStream2.close();
                    bufferedReader.close();
                } catch (Exception e7) {
                    e7.printStackTrace();
                }
            } catch (Exception e8) {
                e = e8;
                fileInputStream = fileInputStream2;
                fileOutputStream = null;
                e.printStackTrace();
                fileOutputStream.close();
                fileInputStream.close();
                bufferedReader.close();
            } catch (Throwable th5) {
                th = th5;
                fileInputStream = fileInputStream2;
                fileOutputStream = null;
                fileOutputStream.close();
                fileInputStream.close();
                bufferedReader.close();
                throw th;
            }
        } catch (Exception e9) {
            e = e9;
            bufferedReader = null;
            fileOutputStream = null;
            fileInputStream = null;
            e.printStackTrace();
            fileOutputStream.close();
            fileInputStream.close();
            bufferedReader.close();
        } catch (Throwable th6) {
            th = th6;
            bufferedReader = null;
            fileOutputStream = null;
            fileInputStream = null;
            fileOutputStream.close();
            fileInputStream.close();
            bufferedReader.close();
            throw th;
        }
    }

    private void notifyReceiverHelper() {
        this.as = new i(this, this.ad);
        this.as.execute(new Void[0]);
    }

    private void packageReceiverHelper() {
        this.as = new i(this, this.ac);
        this.as.execute(new Void[0]);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.waps.p.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, boolean):void
     arg types: [java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, int]
     candidates:
      com.waps.p.a(int, boolean, android.graphics.Bitmap, java.lang.String, java.lang.String, java.lang.String):void
      com.waps.p.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, boolean):void */
    /* access modifiers changed from: private */
    public void sendNotify(String str, String str2, String str3, String str4, String str5) {
        new p(this.G).a(str, str2, str3, str4, str5, true);
    }

    private void spendPointsHelper() {
        this.aq = new j(this, null);
        this.aq.execute(new Void[0]);
    }

    private String toHexString(byte[] bArr, String str) {
        StringBuilder sb = new StringBuilder();
        for (byte b2 : bArr) {
            String hexString = Integer.toHexString(b2 & 255);
            if (hexString.length() == 1) {
                sb.append("0");
            }
            sb.append(hexString).append(str);
        }
        return sb.toString();
    }

    /* access modifiers changed from: private */
    public String toMD5(byte[] bArr) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.reset();
            instance.update(bArr);
            return toHexString(instance.digest(), "");
        } catch (Exception e2) {
            e2.printStackTrace();
            return "";
        }
    }

    public void awardPoints(int i2, UpdatePointsNotifier updatePointsNotifier) {
        if (i2 >= 0) {
            this.W = "" + i2;
            if (f2I != null) {
                ao = updatePointsNotifier;
                f2I.awardPointsHelper();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void decodeBase64(byte[] bArr, int i2, int i3) {
        byte b2;
        this.aM = new byte[bArr.length];
        this.aN = 0;
        this.aP = false;
        this.aO = 0;
        if (i3 < 0) {
            this.aP = true;
        }
        int i4 = 0;
        int i5 = i2;
        while (true) {
            if (i4 >= i3) {
                break;
            }
            int i6 = i5 + 1;
            byte b3 = bArr[i5];
            if (b3 == 61) {
                this.aP = true;
                break;
            }
            if (b3 >= 0 && b3 < aL.length && (b2 = aL[b3]) >= 0) {
                int i7 = this.aO + 1;
                this.aO = i7;
                this.aO = i7 % 4;
                this.aQ = b2 + (this.aQ << 6);
                if (this.aO == 0) {
                    byte[] bArr2 = this.aM;
                    int i8 = this.aN;
                    this.aN = i8 + 1;
                    bArr2[i8] = (byte) ((this.aQ >> 16) & 255);
                    byte[] bArr3 = this.aM;
                    int i9 = this.aN;
                    this.aN = i9 + 1;
                    bArr3[i9] = (byte) ((this.aQ >> 8) & 255);
                    byte[] bArr4 = this.aM;
                    int i10 = this.aN;
                    this.aN = i10 + 1;
                    bArr4[i10] = (byte) (this.aQ & 255);
                }
            }
            i4++;
            i5 = i6;
        }
        if (this.aP && this.aO != 0) {
            this.aQ <<= 6;
            switch (this.aO) {
                case 2:
                    this.aQ <<= 6;
                    byte[] bArr5 = this.aM;
                    int i11 = this.aN;
                    this.aN = i11 + 1;
                    bArr5[i11] = (byte) ((this.aQ >> 16) & 255);
                    return;
                case 3:
                    byte[] bArr6 = this.aM;
                    int i12 = this.aN;
                    this.aN = i12 + 1;
                    bArr6[i12] = (byte) ((this.aQ >> 16) & 255);
                    byte[] bArr7 = this.aM;
                    int i13 = this.aN;
                    this.aN = i13 + 1;
                    bArr7[i13] = (byte) ((this.aQ >> 8) & 255);
                    return;
                default:
                    return;
            }
        }
    }

    public void finalize() {
        try {
            f2I = null;
            this.H.schedule(new l(this, null), 0, TimeUnit.SECONDS);
            if (((Activity) this.G).isFinishing() && this.ax) {
                SharedPreferences.Editor edit = this.G.getSharedPreferences("PushFlag", 3).edit();
                edit.clear();
                edit.commit();
                this.ax = false;
                SharedPreferences.Editor edit2 = this.G.getSharedPreferences("Start_Tag", 3).edit();
                edit2.clear();
                edit2.commit();
            }
        } catch (Exception e2) {
        }
    }

    public void getDisplayAd(DisplayAdNotifier displayAdNotifier) {
        K.getDisplayAdDataFromServer("http://ads.wapx.cn/action/", this.aa, displayAdNotifier);
    }

    public String getParams(Context context) {
        this.G = context;
        initMetaData();
        this.aa += "app_id=" + this.R + "&";
        this.aa += "udid=" + this.L + "&";
        this.aa += "imsi=" + this.al + "&";
        this.aa += "net=" + this.am + "&";
        this.aa += "app_version=" + this.S + "&";
        this.aa += "sdk_version=" + this.T + "&";
        this.aa += "device_name=" + this.M + "&";
        this.aa += "y=" + this.an + "&";
        this.aa += "device_type=" + this.N + "&";
        this.aa += "os_version=" + this.O + "&";
        this.aa += "country_code=" + this.P + "&";
        this.aa += "language=" + this.Q + "&";
        this.aa += "act=" + context.getPackageName() + "." + context.getClass().getSimpleName();
        if (this.U != null && !"".equals(this.U)) {
            this.aa += "&";
            this.aa += "channel=" + this.U;
        }
        if (this.af > 0 && this.ag > 0) {
            this.aa += "&";
            this.aa += "device_width=" + this.af + "&";
            this.aa += "device_height=" + this.ag;
        }
        return this.aa.replaceAll(" ", "%20");
    }

    public void getPoints(UpdatePointsNotifier updatePointsNotifier) {
        if (f2I != null) {
            ao = updatePointsNotifier;
            f2I.getPointsHelper();
        }
    }

    public void getPushAd() {
        aE = true;
        getPushAd("", "push/api_ad?", this.aa);
    }

    /* access modifiers changed from: protected */
    public String getWapsId() {
        return aD;
    }

    public void initMetaData() {
        String obj;
        PackageManager packageManager = this.G.getPackageManager();
        try {
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(this.G.getPackageName(), 128);
            if (applicationInfo != null && applicationInfo.metaData != null) {
                if (getWapsId() == null || "".equals(getWapsId())) {
                    String string = applicationInfo.metaData.getString("WAPS_ID");
                    if (string == null || "".equals(string)) {
                        string = applicationInfo.metaData.getString("APP_ID");
                    }
                    if (string == null || string.equals("")) {
                        Log.w("WAPS_SDK", "WapsId is not setted！ Please check it!");
                        return;
                    } else {
                        this.R = string.trim();
                        Log.d("WAPS_SDK", "WapsId is setted by manifest, the value is: " + this.R);
                    }
                } else {
                    this.R = getWapsId();
                    Log.d("WAPS_SDK", "WapsId is setted by code,the value is: " + this.R);
                    String string2 = applicationInfo.metaData.getString("WAPS_ID");
                    if (string2 == null || "".equals(string2)) {
                        string2 = applicationInfo.metaData.getString("APP_ID");
                    }
                    if (string2 != null && !string2.equals("")) {
                        if (!this.R.equals(string2.trim())) {
                            Log.w("WAPS_SDK", "WapsId is setted by code is not equals the value be setted by manifest! Please chick it!");
                        } else {
                            Log.d("WAPS_SDK", "The WapsId in manifest is: " + string2.trim());
                        }
                    }
                }
                this.Z = this.G.getPackageName();
                String string3 = applicationInfo.metaData.getString("CLIENT_PACKAGE");
                if (string3 != null && !string3.equals("")) {
                    this.Z = string3;
                }
                Object obj2 = applicationInfo.metaData.get("WAPS_PID");
                if (!(obj2 == null || (obj = obj2.toString()) == null || obj.equals(""))) {
                    this.U = obj;
                }
                this.S = packageManager.getPackageInfo(this.G.getPackageName(), 0).versionName;
                this.N = "android";
                this.M = Build.MODEL;
                this.O = Build.VERSION.RELEASE;
                this.P = Locale.getDefault().getCountry();
                this.Q = Locale.getDefault().getLanguage();
                this.al = ((TelephonyManager) this.G.getSystemService("phone")).getSubscriberId();
                try {
                    Context context = this.G;
                    Context context2 = this.G;
                    NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
                    if (activeNetworkInfo == null || activeNetworkInfo.getTypeName().toLowerCase().equals("mobile")) {
                        this.am = activeNetworkInfo.getExtraInfo().toLowerCase();
                    } else {
                        this.am = activeNetworkInfo.getTypeName().toLowerCase();
                    }
                    Log.d("WAPS_SDK", "The net is: " + this.am);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                this.T = LIBRARY_VERSION_NUMBER;
                SharedPreferences sharedPreferences = this.G.getSharedPreferences("appPrefrences", 0);
                String string4 = applicationInfo.metaData.getString("DEVICE_ID");
                if (string4 == null || string4.equals("")) {
                    TelephonyManager telephonyManager = (TelephonyManager) this.G.getSystemService("phone");
                    if (telephonyManager != null) {
                        this.L = telephonyManager.getDeviceId();
                        if (this.L == null || this.L.length() == 0) {
                            this.L = "0";
                        }
                        try {
                            this.L = this.L.toLowerCase();
                            if (Integer.valueOf(Integer.parseInt(this.L)).intValue() == 0) {
                                StringBuffer stringBuffer = new StringBuffer();
                                stringBuffer.append("EMULATOR");
                                String string5 = sharedPreferences.getString("emulatorDeviceId", null);
                                if (string5 == null || string5.equals("")) {
                                    for (int i2 = 0; i2 < 32; i2++) {
                                        stringBuffer.append("1234567890abcdefghijklmnopqrstuvw".charAt(((int) (Math.random() * 100.0d)) % 30));
                                    }
                                    this.L = stringBuffer.toString().toLowerCase();
                                    SharedPreferences.Editor edit = sharedPreferences.edit();
                                    edit.putString("emulatorDeviceId", this.L);
                                    edit.commit();
                                } else {
                                    this.L = string5;
                                }
                            }
                        } catch (NumberFormatException e3) {
                        }
                    } else {
                        this.L = null;
                    }
                } else {
                    this.L = string4;
                }
                this.an = toMD5(("kingxiaoguang@gmail.com" + this.L + this.R).toLowerCase().getBytes()).toLowerCase();
                try {
                    DisplayMetrics displayMetrics = new DisplayMetrics();
                    ((WindowManager) this.G.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
                    this.af = displayMetrics.widthPixels;
                    this.ag = displayMetrics.heightPixels;
                } catch (Exception e4) {
                }
                this.ay = sharedPreferences.getInt("PrimaryColor", 0);
                String string6 = sharedPreferences.getString("InstallReferral", null);
                if (string6 != null && !string6.equals("")) {
                    this.ab = string6;
                }
            }
        } catch (Exception e5) {
        }
    }

    public void notify_receiver(String str, int i2) {
        switch (i2) {
            case 0:
                av = "notify/show?";
                aw = "nyid";
                break;
            case 1:
                av = "notify/click?";
                aw = "nyid";
                break;
        }
        this.ad = str;
        if (f2I != null) {
            f2I.notifyReceiverHelper();
        }
    }

    public void package_receiver(String str, int i2) {
        switch (i2) {
            case 0:
                av = "receiver/install?";
                aw = "install";
                break;
            case 1:
                av = "receiver/load_offer?";
                aw = "load";
                break;
            case 2:
                av = "receiver/load_ad?";
                aw = "load";
                break;
            case 3:
                av = "receiver/uninstall?";
                av = "uninstall";
                break;
            default:
                av = "receiver/install?";
                aw = "install";
                break;
        }
        this.ac = str;
        if (f2I != null) {
            f2I.packageReceiverHelper();
        }
    }

    public void setPushAudio(boolean z2) {
        this.aC = z2;
        SharedPreferences.Editor edit = this.G.getSharedPreferences("Push_Settings", 3).edit();
        edit.putBoolean("push_sound", this.aC);
        edit.commit();
    }

    public void setPushIcon(int i2) {
        this.aB = i2;
        SharedPreferences.Editor edit = this.G.getSharedPreferences("Push_Settings", 3).edit();
        edit.putInt("push_icon", this.aB);
        edit.commit();
    }

    public void showFeedback() {
        this.G.startActivity(showFeedback_forTab());
    }

    public Intent showFeedback_forTab() {
        Intent intent = new Intent();
        intent.setClass(this.G, OffersWebView.class);
        intent.putExtra("UrlPath", "http://app.wapx.cn/action/feedback/form");
        intent.putExtra("ACTIVITY_FLAG", "feedback");
        intent.putExtra("URL_PARAMS", this.aa);
        intent.putExtra("offers_webview_tag", "OffersWebView");
        return intent;
    }

    public void showMore(Context context) {
        showMore(context, this.L);
    }

    public void showMore(Context context, String str) {
        Intent intent = new Intent(context, OffersWebView.class);
        intent.putExtra("Offers_URL", this.Y);
        intent.putExtra("USER_ID", str);
        intent.putExtra("URL_PARAMS", this.aa);
        intent.putExtra("CLIENT_PACKAGE", this.Z);
        intent.putExtra("offers_webview_tag", "OffersWebView");
        context.startActivity(intent);
    }

    public Intent showMore_forTab(Context context) {
        return showMore_forTab(context, this.L);
    }

    public Intent showMore_forTab(Context context, String str) {
        Intent intent = new Intent(context, OffersWebView.class);
        intent.putExtra("Offers_URL", this.Y);
        intent.putExtra("USER_ID", str);
        intent.putExtra("URL_PARAMS", this.aa);
        intent.putExtra("CLIENT_PACKAGE", this.Z);
        intent.putExtra("offers_webview_tag", "OffersWebView");
        return intent;
    }

    public void showOffers(Context context) {
        showOffers(context, this.L);
    }

    public void showOffers(Context context, String str) {
        Intent intent = new Intent(context, OffersWebView.class);
        intent.putExtra("Offers_URL", this.X);
        intent.putExtra("USER_ID", str);
        intent.putExtra("URL_PARAMS", this.aa);
        intent.putExtra("CLIENT_PACKAGE", this.Z);
        intent.putExtra("offers_webview_tag", "OffersWebView");
        context.startActivity(intent);
    }

    public Intent showOffers_forTab(Context context) {
        return showOffers_forTab(context, this.L);
    }

    public Intent showOffers_forTab(Context context, String str) {
        Intent intent = new Intent(context, OffersWebView.class);
        intent.putExtra("Offers_URL", this.X);
        intent.putExtra("USER_ID", str);
        intent.putExtra("URL_PARAMS", this.aa);
        intent.putExtra("CLIENT_PACKAGE", this.Z);
        intent.putExtra("offers_webview_tag", "OffersWebView");
        return intent;
    }

    public void spendPoints(int i2, UpdatePointsNotifier updatePointsNotifier) {
        if (i2 >= 0) {
            this.V = "" + i2;
            if (f2I != null) {
                ao = updatePointsNotifier;
                f2I.spendPointsHelper();
            }
        }
    }
}
