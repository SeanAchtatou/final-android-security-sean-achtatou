package com.waps;

import android.content.Context;
import android.content.pm.ResolveInfo;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.List;

class af extends BaseAdapter {
    final /* synthetic */ OffersWebView a;
    private Context b;
    private List c;

    public af(OffersWebView offersWebView, Context context, List list) {
        this.a = offersWebView;
        this.b = context;
        this.c = list;
    }

    public int getCount() {
        return this.c.size();
    }

    public Object getItem(int i) {
        return null;
    }

    public long getItemId(int i) {
        return 0;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        ResolveInfo resolveInfo = (ResolveInfo) this.c.get(i);
        RelativeLayout relativeLayout = new RelativeLayout(this.b);
        ImageView imageView = new ImageView(this.b);
        imageView.setId(1);
        imageView.setLayoutParams(new ViewGroup.LayoutParams(50, 50));
        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        imageView.setPadding(10, 0, 0, 0);
        TextView textView = new TextView(this.b);
        textView.setTextSize(18.0f);
        textView.setTextColor(-16777216);
        textView.setPadding(5, 0, 0, 0);
        layoutParams.addRule(1, imageView.getId());
        layoutParams.addRule(15);
        imageView.setImageDrawable(resolveInfo.loadIcon(this.a.K));
        textView.setText(resolveInfo.loadLabel(this.a.K).toString().trim());
        relativeLayout.addView(imageView);
        relativeLayout.addView(textView, layoutParams);
        return relativeLayout;
    }
}
