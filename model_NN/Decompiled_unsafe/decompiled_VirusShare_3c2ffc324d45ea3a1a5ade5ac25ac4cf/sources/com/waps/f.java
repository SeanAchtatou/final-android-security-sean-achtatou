package com.waps;

import android.os.AsyncTask;

class f extends AsyncTask {
    final /* synthetic */ AppConnect a;

    private f(AppConnect appConnect) {
        this.a = appConnect;
    }

    /* synthetic */ f(AppConnect appConnect, d dVar) {
        this(appConnect);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        boolean z = false;
        String a2 = AppConnect.J.a("http://app.wapx.cn/action/account/award?", this.a.aa + "&points=" + this.a.W);
        if (a2 != null) {
            z = this.a.handleAwardPointsResponse(a2);
        }
        if (!z) {
            AppConnect.ao.getUpdatePointsFailed("赠送积分失败.");
        }
        return Boolean.valueOf(z);
    }
}
