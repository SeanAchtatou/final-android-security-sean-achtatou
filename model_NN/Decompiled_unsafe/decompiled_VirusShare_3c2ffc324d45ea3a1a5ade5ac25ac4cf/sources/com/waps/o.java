package com.waps;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.view.View;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;

public class o extends AsyncTask {
    public static boolean g = false;
    protected static boolean h = false;
    private static Context s;
    p a;
    q b;
    int c;
    r d;
    String e = "";
    String f = "";
    float i = 0.0f;
    float j = 0.0f;
    NumberFormat k = new DecimalFormat("#0");
    float l;
    InputStream m = null;
    FileOutputStream n = null;
    String o = "";
    String p = "";
    private String q;
    private View r;

    public o(Context context, View view, String str) {
        s = context;
        this.r = view;
        this.q = str;
        this.e = str.substring(str.indexOf("http://") + 7, str.indexOf("/", str.indexOf("http://") + 8));
        this.f = str.substring(0, str.indexOf("/", str.indexOf("http://") + 8));
        this.a = new p(s);
        this.d = new r();
        Context context2 = s;
        Context context3 = s;
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context2.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.getExtraInfo() != null && activeNetworkInfo.getExtraInfo().toLowerCase().contains("wap")) {
            this.d.a(true);
        }
    }

    /* access modifiers changed from: protected */
    public String a(String str) {
        return str.substring(str.lastIndexOf("/") + 1);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public String doInBackground(String... strArr) {
        HttpResponse execute;
        this.c = (int) System.currentTimeMillis();
        this.a.a(this.r, "正在获取文件名...", this.c, "0 %");
        try {
            this.o = a(this.q);
            this.p = "/sdcard/download/";
            this.i = (float) b(this.q);
            if (!this.d.a()) {
                execute = new DefaultHttpClient().execute(new HttpGet(strArr[0].replaceAll(" ", "%20")));
            } else {
                String str = strArr[0];
                HttpHost httpHost = new HttpHost("10.0.0.172", 80, "http");
                HttpHost httpHost2 = new HttpHost(this.e, 80, "http");
                HttpGet httpGet = new HttpGet(str.replaceAll(" ", "%20").replaceFirst(this.f, ""));
                BasicHttpParams basicHttpParams = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(basicHttpParams, 15000);
                HttpConnectionParams.setSoTimeout(basicHttpParams, 30000);
                DefaultHttpClient defaultHttpClient = new DefaultHttpClient(basicHttpParams);
                defaultHttpClient.getParams().setParameter("http.route.default-proxy", httpHost);
                execute = defaultHttpClient.execute(httpHost2, httpGet);
            }
            this.m = execute.getEntity().getContent();
            if (Environment.getExternalStorageState().equals("mounted")) {
                File file = new File(this.p);
                File file2 = new File(this.p, this.o);
                if (!file.exists()) {
                    file.mkdir();
                }
                if (!file2.exists()) {
                    file2.createNewFile();
                }
                this.n = new FileOutputStream(file2);
            } else {
                this.n = s.openFileOutput(this.o, 3);
            }
            if (this.m != null) {
                byte[] bArr = new byte[51200];
                while (true) {
                    int read = this.m.read(bArr);
                    if (read == -1) {
                        break;
                    } else if (Integer.parseInt(this.k.format((double) this.l)) > 100) {
                        this.a.a(this.r, this.o, this.c, this.p + this.o, "下载失败，请重新下载");
                        break;
                    } else {
                        this.n.write(bArr, 0, read);
                        this.j = ((float) read) + this.j;
                        this.l = (this.j / this.i) * 100.0f;
                        publishProgress(Integer.valueOf(((int) (this.j / this.i)) * 100));
                        this.k.format((double) this.l);
                        this.a.a(this.r, this.o, this.c, this.k.format((double) this.l) + " %");
                        h = true;
                    }
                }
                int read2 = this.m.read(bArr);
                Thread.sleep(1000);
                if (read2 == -1) {
                    h = false;
                    String str2 = this.p + this.o;
                    File file3 = Environment.getExternalStorageState().equals("mounted") ? new File(str2) : s.getFileStreamPath(this.o);
                    if (this.o.endsWith(".apk")) {
                        this.a.a(this.r, this.o, this.c, str2, "下载完成,点击安装");
                    } else {
                        this.a.a(this.r, this.o, this.c, str2, "下载完成！");
                    }
                    g = true;
                    Intent intent = new Intent();
                    intent.setAction("android.intent.action.VIEW");
                    intent.setDataAndType(Uri.fromFile(file3), "application/vnd.android.package-archive");
                    s.startActivity(intent);
                    this.b = new q(this.a, this.c, this.o);
                    a(this.b);
                }
            }
            try {
                if (this.m != null) {
                    this.m.close();
                }
                if (this.n == null) {
                    return "";
                }
                this.n.close();
                return "";
            } catch (Exception e2) {
                return "";
            }
        } catch (Exception e3) {
            e3.printStackTrace();
            try {
                if (this.m != null) {
                    this.m.close();
                }
                if (this.n == null) {
                    return "";
                }
                this.n.close();
                return "";
            } catch (Exception e4) {
                return "";
            }
        } catch (Throwable th) {
            try {
                if (this.m != null) {
                    this.m.close();
                }
                if (this.n != null) {
                    this.n.close();
                }
            } catch (Exception e5) {
            }
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public void a(q qVar) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.PACKAGE_ADDED");
        intentFilter.addDataScheme("package");
        s.registerReceiver(qVar, intentFilter);
    }

    /* access modifiers changed from: protected */
    public long b(String str) {
        if (!this.d.a()) {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            httpURLConnection.setConnectTimeout(5000);
            httpURLConnection.setRequestMethod("GET");
            return (long) httpURLConnection.getContentLength();
        }
        HttpHost httpHost = new HttpHost("10.0.0.172", 80, "http");
        HttpHost httpHost2 = new HttpHost(this.e, 80, "http");
        HttpGet httpGet = new HttpGet(str.replaceAll(" ", "%20").replaceFirst(this.f, ""));
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 15000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 30000);
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(basicHttpParams);
        defaultHttpClient.getParams().setParameter("http.route.default-proxy", httpHost);
        return defaultHttpClient.execute(httpHost2, httpGet).getEntity().getContentLength();
    }
}
