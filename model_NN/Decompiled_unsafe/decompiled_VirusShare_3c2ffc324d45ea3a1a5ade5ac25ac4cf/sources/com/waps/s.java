package com.waps;

import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.View;

class s implements View.OnClickListener {
    final /* synthetic */ DisplayAd a;

    s(DisplayAd displayAd) {
        this.a = displayAd;
    }

    public void onClick(View view) {
        String str;
        boolean z;
        Intent intent;
        try {
            ApplicationInfo applicationInfo = this.a.g.getPackageManager().getApplicationInfo(this.a.g.getPackageName(), 128);
            str = this.a.g.getPackageName();
            String string = applicationInfo.metaData.getString("CLIENT_PACKAGE");
            if (string != null && !string.equals("")) {
                str = string;
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            str = "";
        }
        if (this.a.l == null || "".equals(this.a.l)) {
            z = true;
        } else {
            try {
                intent = this.a.g.getPackageManager().getLaunchIntentForPackage(this.a.l);
            } catch (Exception e2) {
                e2.printStackTrace();
                intent = null;
            }
            if (intent != null) {
                this.a.g.startActivity(intent);
                AppConnect.getInstanceNoConnect(this.a.g).package_receiver(this.a.l, 2);
                z = false;
            } else {
                z = true;
            }
        }
        if (!z) {
            return;
        }
        if (this.a.m == null || "".equals(this.a.m)) {
            Intent intent2 = new Intent(this.a.g, OffersWebView.class);
            intent2.putExtra("URL", this.a.k);
            intent2.putExtra("isFinshClose", "true");
            intent2.putExtra("CLIENT_PACKAGE", str);
            intent2.putExtra("offers_webview_tag", "OffersWebView");
            this.a.g.startActivity(intent2);
            return;
        }
        Intent intent3 = new Intent("android.intent.action.VIEW", Uri.parse(this.a.k));
        intent3.setFlags(268435456);
        this.a.g.startActivity(intent3);
    }
}
