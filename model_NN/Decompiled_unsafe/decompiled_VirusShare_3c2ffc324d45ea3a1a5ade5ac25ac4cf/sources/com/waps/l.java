package com.waps;

import android.app.Activity;
import android.content.SharedPreferences;

class l implements Runnable {
    final /* synthetic */ AppConnect a;

    private l(AppConnect appConnect) {
        this.a = appConnect;
    }

    /* synthetic */ l(AppConnect appConnect, d dVar) {
        this(appConnect);
    }

    public void run() {
        SharedPreferences sharedPreferences = this.a.G.getSharedPreferences("Finalize_Flag", 3);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        String string = sharedPreferences.getString("appList", "");
        if (string != null && !"".equals(string.trim()) && string.equals("2")) {
            if (((Activity) this.a.G).isFinishing() && q.c) {
                new p(this.a.G).a();
                AppConnect.E = true;
                p.c = false;
            }
            if (((Activity) this.a.G).isFinishing() && !o.g) {
                SharedPreferences.Editor edit2 = this.a.G.getSharedPreferences("Package_Name", 3).edit();
                edit2.clear();
                edit2.commit();
                q.c = false;
                edit.clear();
                edit.commit();
            }
        }
    }
}
