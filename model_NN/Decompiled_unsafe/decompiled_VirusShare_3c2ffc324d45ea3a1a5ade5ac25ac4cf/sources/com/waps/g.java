package com.waps;

import android.os.AsyncTask;

class g extends AsyncTask {
    final /* synthetic */ AppConnect a;

    private g(AppConnect appConnect) {
        this.a = appConnect;
    }

    /* synthetic */ g(AppConnect appConnect, d dVar) {
        this(appConnect);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        boolean z = false;
        String a2 = AppConnect.J.a("http://app.wapx.cn/action/account/getinfo?", this.a.aa);
        if (a2 != null) {
            z = this.a.handleGetPointsResponse(a2);
        }
        if (!z) {
            AppConnect.ao.getUpdatePointsFailed("无法更新积分");
        }
        return Boolean.valueOf(z);
    }
}
