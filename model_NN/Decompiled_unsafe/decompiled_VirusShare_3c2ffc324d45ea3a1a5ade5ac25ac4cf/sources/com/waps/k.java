package com.waps;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Environment;
import java.io.File;
import java.util.concurrent.TimeUnit;

class k extends AsyncTask {
    final /* synthetic */ AppConnect a;

    private k(AppConnect appConnect) {
        this.a = appConnect;
    }

    /* synthetic */ k(AppConnect appConnect, d dVar) {
        this(appConnect);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        boolean z = false;
        String access$100 = this.a.aa;
        if (!this.a.ab.equals("")) {
            access$100 = access$100 + "&" + this.a.ab;
        }
        String a2 = AppConnect.J.a("http://app.wapx.cn/action/connect/active?", access$100);
        if (a2 != null) {
            z = this.a.handleConnectResponse(a2);
        }
        return Boolean.valueOf(z);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Boolean bool) {
        try {
            if (AppConnect.ai && this.a.ah != null && !"".equals(this.a.ah) && this.a.ah.compareTo(this.a.S) > 0) {
                this.a.UpdateDialog("http://app.wapx.cn/action/app/update?" + this.a.aa);
            }
            if (AppConnect.aj && Environment.getExternalStorageState().equals("mounted")) {
                File file = new File(Environment.getExternalStorageDirectory().toString() + "/Android/Package.dat");
                if (file.exists()) {
                    file.delete();
                }
            }
            if (AppConnect.au == null || "".equals(AppConnect.au)) {
                this.a.loadApps();
                h unused = this.a.at = new h(this.a, null);
                this.a.at.execute(new Void[0]);
            }
            SharedPreferences sharedPreferences = this.a.G.getSharedPreferences("PushFlag", 3);
            if (sharedPreferences.getString("push_flag", "").equals("true")) {
                if (this.a.w == null || "".equals(this.a.w.trim())) {
                    this.a.H.schedule(new m(this.a, AppConnect.C, this.a.aa), (long) Integer.parseInt(AppConnect.B), TimeUnit.SECONDS);
                } else {
                    this.a.H.schedule(new m(this.a, this.a.w, this.a.x, this.a.y, this.a.z, this.a.aa), 0, TimeUnit.SECONDS);
                }
                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putString("push_flag", "false");
                edit.commit();
            }
        } catch (Exception e) {
        } finally {
            boolean unused2 = AppConnect.aj = false;
        }
    }
}
