package com.waps;

import android.os.AsyncTask;

class j extends AsyncTask {
    final /* synthetic */ AppConnect a;

    private j(AppConnect appConnect) {
        this.a = appConnect;
    }

    /* synthetic */ j(AppConnect appConnect, d dVar) {
        this(appConnect);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        boolean z = false;
        String a2 = AppConnect.J.a("http://app.wapx.cn/action/account/spend?", this.a.aa + "&points=" + this.a.V);
        if (a2 != null) {
            z = this.a.handleSpendPointsResponse(a2);
        }
        if (!z) {
            AppConnect.ao.getUpdatePointsFailed("消费积分失败.");
        }
        return Boolean.valueOf(z);
    }
}
