package com.waps;

import android.app.ActivityManager;
import android.os.Build;
import android.os.PowerManager;
import java.util.List;

class a extends Thread {
    final /* synthetic */ AdView a;

    a(AdView adView) {
        this.a = adView;
    }

    public void run() {
        while (true) {
            try {
                List<ActivityManager.RunningTaskInfo> runningTasks = ((ActivityManager) this.a.d.getSystemService("activity")).getRunningTasks(2);
                if (runningTasks != null && !runningTasks.isEmpty()) {
                    long currentTimeMillis = System.currentTimeMillis() - AdView.e;
                    this.a.g = runningTasks.get(0).topActivity.getShortClassName();
                    String unused = this.a.l = Build.VERSION.SDK;
                    int parseInt = Integer.parseInt(this.a.l);
                    PowerManager powerManager = (PowerManager) this.a.d.getSystemService("power");
                    if (parseInt >= 7) {
                        if (((Boolean) powerManager.getClass().getDeclaredMethod("isScreenOn", new Class[0]).invoke(powerManager, new Object[0])).booleanValue() && this.a.d.getClass().toString().contains(this.a.g) && currentTimeMillis > ((long) ((AdView.f * 1000) - 1000))) {
                            this.a.showADS();
                            AdView.e = System.currentTimeMillis();
                        }
                    } else if (this.a.d.getClass().toString().contains(this.a.g) && currentTimeMillis > ((long) ((AdView.f * 1000) - 1000))) {
                        this.a.showADS();
                        AdView.e = System.currentTimeMillis();
                    }
                }
                sleep((long) (AdView.f * 1000));
            } catch (Exception e) {
                return;
            }
        }
    }
}
