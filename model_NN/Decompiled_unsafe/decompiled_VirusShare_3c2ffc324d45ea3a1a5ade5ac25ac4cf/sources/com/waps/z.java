package com.waps;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import java.io.File;

class z implements DialogInterface.OnClickListener {
    final /* synthetic */ OffersWebView a;

    z(OffersWebView offersWebView) {
        this.a = offersWebView;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        o.g = true;
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        intent.setDataAndType(Uri.fromFile(new File(this.a.d + this.a.c)), "application/vnd.android.package-archive");
        this.a.startActivity(intent);
        this.a.e = new q(this.a.c);
        this.a.b.a(this.a.e);
        if (this.a.o != null && "true".equals(this.a.o)) {
            this.a.finish();
        }
    }
}
