package com.waps;

import android.os.AsyncTask;

class t extends AsyncTask {
    final /* synthetic */ DisplayAd a;

    private t(DisplayAd displayAd) {
        this.a = displayAd;
    }

    /* synthetic */ t(DisplayAd displayAd, s sVar) {
        this(displayAd);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        boolean z = false;
        String a2 = this.a.f.a(this.a.h, this.a.j);
        if (a2 == null || a2.length() == 0) {
            this.a.e.getDisplayAdResponseFailed("Network Error.");
        } else {
            z = this.a.buildResponse(a2);
            if (!z) {
                this.a.e.getDisplayAdResponseFailed("Ad content has error.");
            }
        }
        return Boolean.valueOf(z);
    }
}
