package com.waps;

import android.content.DialogInterface;
import android.content.Intent;

class aa implements DialogInterface.OnClickListener {
    final /* synthetic */ OffersWebView a;

    aa(OffersWebView offersWebView) {
        this.a = offersWebView;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        Intent intent = new Intent();
        intent.setClass(this.a, OffersWebView.class);
        intent.putExtra("UrlPath", this.a.x);
        intent.putExtra("isFinshClose", "true");
        intent.putExtra("ACTIVITY_FLAG", "notify");
        intent.putExtra("offers_webview_tag", "OffersWebView");
        if (this.a.x.contains("down_type")) {
            intent.putExtra("Notify_Url_Params", this.a.A);
        }
        this.a.startActivity(intent);
        this.a.C.a(2);
        this.a.G.clear();
        this.a.G.commit();
        dialogInterface.cancel();
        this.a.finish();
    }
}
