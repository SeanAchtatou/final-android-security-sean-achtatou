package com.waps;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.io.ByteArrayInputStream;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DisplayAd {
    private static final byte[] n = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, 62, -1, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, 63, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51};
    Bitmap a;
    View b;
    final String c = "Display Ad";
    private t d = null;
    /* access modifiers changed from: private */
    public DisplayAdNotifier e;
    /* access modifiers changed from: private */
    public r f = null;
    /* access modifiers changed from: private */
    public Context g;
    /* access modifiers changed from: private */
    public String h = "";
    private String i = "";
    /* access modifiers changed from: private */
    public String j = "";
    /* access modifiers changed from: private */
    public String k;
    /* access modifiers changed from: private */
    public String l;
    /* access modifiers changed from: private */
    public String m;
    private byte[] o;
    private int p;
    private int q;
    private boolean r;
    private int s;

    public DisplayAd(Context context) {
        this.g = context;
        this.f = new r();
        Context context2 = this.g;
        Context context3 = this.g;
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context2.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.getExtraInfo() != null && activeNetworkInfo.getExtraInfo().toLowerCase().contains("wap")) {
            this.f.a(true);
        }
    }

    /* access modifiers changed from: private */
    public boolean buildResponse(String str) {
        DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
        try {
            Document parse = newInstance.newDocumentBuilder().parse(new ByteArrayInputStream(str.getBytes("UTF-8")));
            this.k = getNodeTrimValue(parse.getElementsByTagName("ClickUrl"));
            this.l = getNodeTrimValue(parse.getElementsByTagName("AdPackage"));
            this.m = getNodeTrimValue(parse.getElementsByTagName("NewBrowser"));
            String nodeTrimValue = getNodeTrimValue(parse.getElementsByTagName("Image"));
            if (nodeTrimValue == null || "".equals(nodeTrimValue)) {
                this.e.getDisplayAdResponse(this.b);
                return true;
            }
            decodeBase64(nodeTrimValue.getBytes(), 0, nodeTrimValue.getBytes().length);
            this.a = BitmapFactory.decodeByteArray(this.o, 0, this.p);
            String nodeTrimValue2 = getNodeTrimValue(parse.getElementsByTagName("ShowType"));
            if (nodeTrimValue2 == null || !nodeTrimValue2.equals("1")) {
                this.b = new ImageView(this.g);
                this.b.setLayoutParams(new ViewGroup.LayoutParams(this.a.getWidth(), this.a.getHeight()));
                ((ImageView) this.b).setImageBitmap(this.a);
            } else {
                String nodeTrimValue3 = getNodeTrimValue(parse.getElementsByTagName("Fcolor"));
                String nodeTrimValue4 = getNodeTrimValue(parse.getElementsByTagName("Bcolor"));
                String nodeTrimValue5 = getNodeTrimValue(parse.getElementsByTagName("Mcolor"));
                String nodeTrimValue6 = getNodeTrimValue(parse.getElementsByTagName("AppName"));
                String nodeTrimValue7 = getNodeTrimValue(parse.getElementsByTagName("AppWords"));
                String nodeTrimValue8 = getNodeTrimValue(parse.getElementsByTagName("AppMore"));
                String nodeTrimValue9 = getNodeTrimValue(parse.getElementsByTagName("W"));
                String nodeTrimValue10 = getNodeTrimValue(parse.getElementsByTagName("H"));
                this.b = getView(this.g, nodeTrimValue3, nodeTrimValue4, nodeTrimValue5, nodeTrimValue6, nodeTrimValue7, nodeTrimValue8, this.a, getNodeTrimValue(parse.getElementsByTagName("Mark")));
                this.b.setLayoutParams(new ViewGroup.LayoutParams(Integer.parseInt(nodeTrimValue9), Integer.parseInt(nodeTrimValue10)));
            }
            this.b.setOnClickListener(new s(this));
            this.e.getDisplayAdResponse(this.b);
            this.b = null;
            this.o = null;
            return true;
        } catch (Exception e2) {
            e2.printStackTrace();
            return true;
        }
    }

    private int[] getARGB(String str) {
        String[] split = str.split(",");
        int[] iArr = new int[split.length];
        for (int i2 = 0; i2 < split.length; i2++) {
            iArr[i2] = Integer.parseInt(split[i2]);
        }
        return iArr;
    }

    private String getNodeTrimValue(NodeList nodeList) {
        Element element = (Element) nodeList.item(0);
        if (element == null) {
            return null;
        }
        NodeList childNodes = element.getChildNodes();
        int length = childNodes.getLength();
        String str = "";
        for (int i2 = 0; i2 < length; i2++) {
            Node item = childNodes.item(i2);
            if (item != null) {
                str = str + item.getNodeValue();
            }
        }
        if (str == null || str.equals("")) {
            return null;
        }
        return str.trim();
    }

    private View getView(Context context, String str, String str2, String str3, String str4, String str5, String str6, Bitmap bitmap, String str7) {
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(0);
        linearLayout.setGravity(16);
        RelativeLayout relativeLayout = new RelativeLayout(context);
        relativeLayout.setGravity(16);
        relativeLayout.setBackgroundColor(Color.argb(getARGB(str2)[0], getARGB(str2)[1], getARGB(str2)[2], getARGB(str2)[3]));
        relativeLayout.setPadding(7, 5, 5, 5);
        RelativeLayout relativeLayout2 = new RelativeLayout(context);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        ImageView imageView = new ImageView(context);
        imageView.setId(1);
        imageView.setImageBitmap(bitmap);
        LinearLayout linearLayout2 = new LinearLayout(context);
        linearLayout2.setOrientation(1);
        linearLayout2.setPadding(5, 0, 7, 0);
        linearLayout2.setGravity(16);
        RelativeLayout relativeLayout3 = new RelativeLayout(context);
        TextView textView = new TextView(context);
        textView.setText(str4);
        textView.setTextSize(16.0f);
        textView.setTextColor(Color.argb(getARGB(str)[0], getARGB(str)[1], getARGB(str)[2], getARGB(str)[3]));
        TextView textView2 = new TextView(context);
        textView2.setText(str6);
        textView2.setTextSize(16.0f);
        textView2.setTextColor(Color.argb(getARGB(str3)[0], getARGB(str3)[1], getARGB(str3)[2], getARGB(str3)[3]));
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams3.addRule(9);
        layoutParams4.addRule(11);
        relativeLayout3.addView(textView, layoutParams3);
        relativeLayout3.addView(textView2, layoutParams4);
        TextView textView3 = new TextView(context);
        textView3.setText(str5);
        textView3.setTextSize(15.0f);
        textView3.setTextColor(Color.argb(getARGB(str)[0], getARGB(str)[1], getARGB(str)[2], getARGB(str)[3]));
        linearLayout2.addView(relativeLayout3);
        linearLayout2.addView(textView3);
        layoutParams.addRule(1, imageView.getId());
        layoutParams2.addRule(9);
        relativeLayout2.addView(imageView, layoutParams2);
        relativeLayout2.addView(linearLayout2, layoutParams);
        TextView textView4 = new TextView(context);
        textView4.setText(str7);
        textView4.setGravity(16);
        textView4.setTextSize(9.0f);
        textView4.setId(2);
        RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(15, -1);
        RelativeLayout.LayoutParams layoutParams6 = new RelativeLayout.LayoutParams(-2, -2);
        new RelativeLayout.LayoutParams(-2, -2);
        layoutParams5.addRule(11);
        layoutParams6.addRule(0, textView4.getId());
        textView4.setLayoutParams(layoutParams5);
        relativeLayout.addView(textView4);
        relativeLayout.addView(relativeLayout2, layoutParams6);
        linearLayout.addView(relativeLayout);
        return linearLayout;
    }

    /* access modifiers changed from: package-private */
    public void decodeBase64(byte[] bArr, int i2, int i3) {
        byte b2;
        this.o = new byte[bArr.length];
        this.p = 0;
        this.r = false;
        this.q = 0;
        if (i3 < 0) {
            this.r = true;
        }
        int i4 = 0;
        int i5 = i2;
        while (true) {
            if (i4 >= i3) {
                break;
            }
            int i6 = i5 + 1;
            byte b3 = bArr[i5];
            if (b3 == 61) {
                this.r = true;
                break;
            }
            if (b3 >= 0 && b3 < n.length && (b2 = n[b3]) >= 0) {
                int i7 = this.q + 1;
                this.q = i7;
                this.q = i7 % 4;
                this.s = b2 + (this.s << 6);
                if (this.q == 0) {
                    byte[] bArr2 = this.o;
                    int i8 = this.p;
                    this.p = i8 + 1;
                    bArr2[i8] = (byte) ((this.s >> 16) & 255);
                    byte[] bArr3 = this.o;
                    int i9 = this.p;
                    this.p = i9 + 1;
                    bArr3[i9] = (byte) ((this.s >> 8) & 255);
                    byte[] bArr4 = this.o;
                    int i10 = this.p;
                    this.p = i10 + 1;
                    bArr4[i10] = (byte) (this.s & 255);
                }
            }
            i4++;
            i5 = i6;
        }
        if (this.r && this.q != 0) {
            this.s <<= 6;
            switch (this.q) {
                case 2:
                    this.s <<= 6;
                    byte[] bArr5 = this.o;
                    int i11 = this.p;
                    this.p = i11 + 1;
                    bArr5[i11] = (byte) ((this.s >> 16) & 255);
                    return;
                case 3:
                    byte[] bArr6 = this.o;
                    int i12 = this.p;
                    this.p = i12 + 1;
                    bArr6[i12] = (byte) ((this.s >> 16) & 255);
                    byte[] bArr7 = this.o;
                    int i13 = this.p;
                    this.p = i13 + 1;
                    bArr7[i13] = (byte) ((this.s >> 8) & 255);
                    return;
                default:
                    return;
            }
        }
    }

    public void getDisplayAdDataFromServer(String str, String str2, DisplayAdNotifier displayAdNotifier) {
        this.i = str;
        this.h = this.i + "display/ad?";
        this.j = str2;
        this.e = displayAdNotifier;
        this.d = new t(this, null);
        this.d.execute(new Void[0]);
    }
}
