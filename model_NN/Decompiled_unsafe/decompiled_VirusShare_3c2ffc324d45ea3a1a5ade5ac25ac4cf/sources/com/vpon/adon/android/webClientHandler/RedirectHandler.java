package com.vpon.adon.android.webClientHandler;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import com.vpon.adon.android.WebInApp;
import com.vpon.adon.android.entity.Ad;
import com.vpon.adon.android.utils.IOUtils;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.spec.InvalidKeySpecException;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class RedirectHandler extends WebClientHandler {
    public RedirectHandler(WebClientHandler next) {
        super(next);
    }

    public boolean handle(Context context, Ad ad, String url) {
        int webViewState;
        if (!url.startsWith("redir://")) {
            return doNext(context, ad, url);
        }
        try {
            if (url.contains("redir?t=o")) {
                webViewState = 0;
            } else {
                webViewState = 1;
            }
            String redirUrl = url.split("redir://")[1];
            switch (webViewState) {
                case 0:
                    redirUrl = redirUrl.split("t=o")[0];
                    break;
                case 1:
                    redirUrl = redirUrl.split("t=i")[0];
                    break;
            }
            String redirUrl2 = String.valueOf("http://" + redirUrl) + "data=" + IOUtils.instance().getRedirectPackCryptoString(ad.getAdRedirectPack().toString());
            switch (webViewState) {
                case 0:
                    context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(redirUrl2)));
                    break;
                case 1:
                    Intent intent = new Intent(context, WebInApp.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("url", redirUrl2);
                    bundle.putInt("adWidth", ad.getAdWidth());
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                    break;
            }
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e2) {
            e2.printStackTrace();
        } catch (InvalidKeySpecException e3) {
            e3.printStackTrace();
        } catch (NoSuchPaddingException e4) {
            e4.printStackTrace();
        } catch (IllegalBlockSizeException e5) {
            e5.printStackTrace();
        } catch (BadPaddingException e6) {
            e6.printStackTrace();
        } catch (IOException e7) {
            e7.printStackTrace();
        } catch (NoSuchProviderException e8) {
            e8.printStackTrace();
        } catch (ActivityNotFoundException e9) {
            e9.printStackTrace();
        } catch (Exception e10) {
            e10.printStackTrace();
        }
        return true;
    }
}
