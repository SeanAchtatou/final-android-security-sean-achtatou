package com.vpon.adon.android.webClientHandler;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.vpon.adon.android.entity.Ad;

public class AndroidMarketHandler extends WebClientHandler {
    public AndroidMarketHandler(WebClientHandler next) {
        super(next);
    }

    public boolean handle(Context context, Ad ad, String url) {
        if (!url.startsWith("market:")) {
            return doNext(context, ad, url);
        }
        context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
        return true;
    }
}
