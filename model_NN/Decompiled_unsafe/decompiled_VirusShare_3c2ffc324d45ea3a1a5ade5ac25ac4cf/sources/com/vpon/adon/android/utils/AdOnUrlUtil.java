package com.vpon.adon.android.utils;

import android.util.Log;
import com.vpon.adon.android.AdOnPlatform;

public class AdOnUrlUtil {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$vpon$adon$android$AdOnPlatform;

    static /* synthetic */ int[] $SWITCH_TABLE$com$vpon$adon$android$AdOnPlatform() {
        int[] iArr = $SWITCH_TABLE$com$vpon$adon$android$AdOnPlatform;
        if (iArr == null) {
            iArr = new int[AdOnPlatform.values().length];
            try {
                iArr[AdOnPlatform.CN.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[AdOnPlatform.TW.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            $SWITCH_TABLE$com$vpon$adon$android$AdOnPlatform = iArr;
        }
        return iArr;
    }

    public static String getAdReqUrl(AdOnPlatform platform) {
        switch ($SWITCH_TABLE$com$vpon$adon$android$AdOnPlatform()[platform.ordinal()]) {
            case 1:
                return "http://tw.ad.adon.vpon.com/api/webviewAdReq";
            case 2:
                return "http://cn.ad.adon.vpon.com/api/webviewAdReq";
            default:
                Log.v("SDK", "AdOnUrlUtil Config: unknown platform!!");
                Log.v("SDK", "Please check with engineer.");
                return "http://tw.ad.adon.vpon.com/api/webviewAdReq";
        }
    }

    public static String getAdErrorUrl(AdOnPlatform platform) {
        switch ($SWITCH_TABLE$com$vpon$adon$android$AdOnPlatform()[platform.ordinal()]) {
            case 1:
                return "http://tw.ad.adon.vpon.com/api/webviewSdkError";
            case 2:
                return "http://cn.ad.adon.vpon.com/api/swebviewSdkError";
            default:
                Log.v("SDK", "AdOnUrlUtil Config: unknown platform!!");
                Log.v("SDK", "Please check with engineer.");
                return "http://tw.ad.adon.vpon.com/api/webviewSdkError";
        }
    }

    public static String getAdClickUrl(AdOnPlatform platform) {
        switch ($SWITCH_TABLE$com$vpon$adon$android$AdOnPlatform()[platform.ordinal()]) {
            case 1:
                return "http://tw.ad.adon.vpon.com/api/webviewAdClick";
            case 2:
                return "http://cn.ad.adon.vpon.com/api/webviewAdClick";
            default:
                Log.v("SDK", "AdOnUrlUtil Config: unknown platform!!");
                Log.v("SDK", "Please check with engineer.");
                return "http://cn.ad.adon.vpon.com/api/webviewAdClick";
        }
    }
}
