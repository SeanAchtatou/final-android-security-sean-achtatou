package com.admogo.obj;

import android.graphics.drawable.Drawable;
import java.util.List;

public class Custom {
    public String adText;
    public int animationType;
    public String appDes;
    public Drawable appIcon;
    public String appName;
    public String clickLink;
    public String downloadLink;
    public String iconLink;
    public Drawable image;
    public String imageLink;
    public List<String> imageUrlList;
    public String link;
    public int linkType;
    public String subText;
    public int type;
}
