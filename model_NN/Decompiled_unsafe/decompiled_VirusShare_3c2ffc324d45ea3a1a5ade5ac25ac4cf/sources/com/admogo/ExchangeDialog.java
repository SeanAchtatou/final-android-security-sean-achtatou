package com.admogo;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Browser;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.admogo.AsyncImageLoader;
import com.shirley.morytest.R;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class ExchangeDialog extends ProgressDialog {
    private AdMogoLayout adMogoLayout;
    private Drawable appIcon;
    private AsyncImageLoader asyncImageLoader = new AsyncImageLoader();
    private Context context;
    private String description;
    private String downloadLink;
    private String id;
    private List<String> imageList;
    private boolean isHorizontal;
    /* access modifiers changed from: private */
    public Drawable mogoBtnBg;
    /* access modifiers changed from: private */
    public Drawable mogoBtnPrsBg;
    private Drawable mogoCloseBtn;
    private int px10;
    private int px104;
    private int px132;
    private int px135;
    private int px156;
    private int px160;
    private int px17;
    private int px186;
    private int px200;
    private int px25;
    private int px255;
    private int px28;
    private int px320;
    private int px34;
    private int px36;
    private int px48;
    private int px5;
    private int px53;
    private int px55;
    private int px60;
    private int px8;
    private int px90;
    private String title;
    private int type;

    public ExchangeDialog(int type2, String id2, AdMogoLayout adMogoLayout2, Context context2, Drawable appicon, String title2, String desc, List<String> imgList, String link, boolean isHorizontal2) {
        super(context2);
        this.type = type2;
        this.id = id2;
        this.adMogoLayout = adMogoLayout2;
        this.context = context2;
        this.appIcon = appicon;
        this.title = title2;
        this.description = desc;
        this.imageList = imgList;
        this.downloadLink = link;
        this.isHorizontal = isHorizontal2;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        float density = this.context.getResources().getDisplayMetrics().density;
        this.px8 = (int) (8.0f * density);
        this.px320 = (int) (320.0f * density);
        this.px55 = (int) (55.0f * density);
        this.px36 = (int) (36.0f * density);
        this.px10 = (int) (10.0f * density);
        this.px5 = (int) (5.0f * density);
        this.px160 = (int) (160.0f * density);
        this.px60 = (int) (60.0f * density);
        this.px132 = (int) (132.0f * density);
        this.px34 = (int) (34.0f * density);
        this.px28 = (int) (28.0f * density);
        this.px53 = (int) (53.0f * density);
        this.px17 = (int) (17.0f * density);
        this.px186 = (int) (186.0f * density);
        this.px104 = (int) (104.0f * density);
        this.px156 = (int) (156.0f * density);
        this.px255 = (int) (255.0f * density);
        this.px48 = (int) (48.0f * density);
        this.px200 = (int) (200.0f * density);
        this.px25 = (int) (25.0f * density);
        this.px90 = (int) (90.0f * density);
        this.px135 = (int) (135.0f * density);
        InputStream dialogBgStream = getClass().getResourceAsStream("/com/admogo/assets/adsmogo_dialog_bg.png");
        getWindow().setBackgroundDrawable(new BitmapDrawable(dialogBgStream));
        InputStream btnBgStream = getClass().getResourceAsStream("/com/admogo/assets/adsmogo_btn_bg.png");
        this.mogoBtnBg = new BitmapDrawable(btnBgStream);
        InputStream btnBgPrsStream = getClass().getResourceAsStream("/com/admogo/assets/adsmogo_btn_bg_prs.png");
        this.mogoBtnPrsBg = new BitmapDrawable(btnBgPrsStream);
        InputStream closeStream = getClass().getResourceAsStream("/com/admogo/assets/adsmogo_close.png");
        this.mogoCloseBtn = new BitmapDrawable(closeStream);
        try {
            dialogBgStream.close();
            btnBgStream.close();
            btnBgPrsStream.close();
            closeStream.close();
        } catch (IOException e) {
            Log.w("image stream exception", e);
        }
        if (this.imageList.size() <= 0) {
            initWindow();
        } else if (this.isHorizontal) {
            initLWindow();
        } else {
            initPWindow();
        }
    }

    private void initWindow() {
        WindowManager.LayoutParams p = getWindow().getAttributes();
        p.height = this.px320;
        p.width = this.px320;
        getWindow().setAttributes(p);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(this.px320, this.px320);
        layoutParams.addRule(13);
        RelativeLayout.LayoutParams windowParams = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams.addRule(13);
        windowParams.setMargins(this.px8, this.px8, this.px8, this.px8);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, this.px55);
        layoutParams2.addRule(10);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(this.px36, this.px36);
        layoutParams3.leftMargin = this.px10;
        layoutParams3.rightMargin = this.px10;
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-1, this.px160);
        layoutParams4.addRule(13);
        RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(-1, this.px60);
        layoutParams5.addRule(12);
        layoutParams5.bottomMargin = this.px10;
        RelativeLayout.LayoutParams layoutParams6 = new RelativeLayout.LayoutParams(this.px53, this.px17);
        layoutParams6.addRule(11);
        layoutParams6.addRule(12);
        layoutParams6.setMargins(0, 0, this.px10, 0);
        RelativeLayout relativeLayout = new RelativeLayout(this.context);
        RelativeLayout relativeLayout2 = new RelativeLayout(this.context);
        relativeLayout2.setBackgroundResource(R.drawable.bg);
        relativeLayout2.setLayoutParams(windowParams);
        LinearLayout linearLayout = new LinearLayout(this.context);
        linearLayout.setBackgroundResource(R.drawable.about);
        linearLayout.setOrientation(0);
        linearLayout.setGravity(16);
        linearLayout.setLayoutParams(layoutParams2);
        ImageView imageView = new ImageView(this.context);
        imageView.setBackgroundDrawable(this.appIcon);
        imageView.setLayoutParams(layoutParams3);
        TextView textView = new TextView(this.context);
        textView.setText(this.title);
        textView.setTextColor(Color.rgb(51, 51, 51));
        textView.setTextSize(16.0f);
        linearLayout.addView(imageView);
        linearLayout.addView(textView);
        relativeLayout2.addView(linearLayout);
        LinearLayout linearLayout2 = new LinearLayout(this.context);
        TextView textView2 = new TextView(this.context);
        textView2.setText(this.description);
        textView2.setTextColor(-7829368);
        textView2.setTextSize(16.0f);
        textView2.setLineSpacing(1.0f, 1.5f);
        textView2.setPadding(this.px10, 0, this.px10, 0);
        linearLayout2.addView(textView2);
        linearLayout2.setOrientation(1);
        linearLayout2.setLayoutParams(layoutParams4);
        relativeLayout2.addView(linearLayout2);
        LinearLayout linearLayout3 = new LinearLayout(this.context);
        linearLayout3.setOrientation(0);
        linearLayout3.setGravity(17);
        LinearLayout.LayoutParams layoutParams7 = new LinearLayout.LayoutParams(this.px132, this.px34);
        layoutParams7.setMargins(this.px5, 0, this.px5, 0);
        Button button = new Button(this.context);
        button.setBackgroundDrawable(this.mogoBtnBg);
        button.setText("下载");
        button.setTextSize(18.0f);
        button.setPadding(0, 0, 0, 0);
        button.setGravity(17);
        button.setTextColor(-1);
        button.setLayoutParams(layoutParams7);
        button.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                if (event.getAction() == 0) {
                    view.setBackgroundDrawable(ExchangeDialog.this.mogoBtnPrsBg);
                    return false;
                } else if (event.getAction() != 1) {
                    return false;
                } else {
                    view.setBackgroundDrawable(ExchangeDialog.this.mogoBtnBg);
                    ExchangeDialog.this.downloadAPK();
                    return false;
                }
            }
        });
        Button button2 = new Button(this.context);
        button2.setBackgroundDrawable(this.mogoBtnBg);
        button2.setText("收藏");
        button2.setTextSize(18.0f);
        button2.setTextColor(-1);
        button2.setPadding(0, 0, 0, 0);
        button2.setGravity(17);
        button2.setLayoutParams(layoutParams7);
        button2.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                if (event.getAction() == 0) {
                    view.setBackgroundDrawable(ExchangeDialog.this.mogoBtnPrsBg);
                    return false;
                } else if (event.getAction() != 1) {
                    return false;
                } else {
                    view.setBackgroundDrawable(ExchangeDialog.this.mogoBtnBg);
                    ExchangeDialog.this.addMark();
                    return false;
                }
            }
        });
        linearLayout3.addView(button);
        linearLayout3.addView(button2);
        linearLayout3.setLayoutParams(layoutParams5);
        relativeLayout2.addView(linearLayout3);
        Button button3 = new Button(this.context);
        button3.setBackgroundDrawable(this.mogoCloseBtn);
        RelativeLayout.LayoutParams layoutParams8 = new RelativeLayout.LayoutParams(this.px28, this.px28);
        layoutParams8.addRule(11);
        button3.setLayoutParams(layoutParams8);
        button3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                ExchangeDialog.this.closeDialog();
            }
        });
        relativeLayout.addView(relativeLayout2);
        relativeLayout.addView(button3);
        addContentView(relativeLayout, windowParams);
    }

    private void initPWindow() {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams.addRule(13);
        RelativeLayout.LayoutParams windowParams = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams.addRule(13);
        windowParams.setMargins(this.px8, this.px8, this.px8, this.px8);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, this.px55);
        layoutParams2.addRule(10);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(this.px36, this.px36);
        layoutParams3.leftMargin = this.px10;
        layoutParams3.rightMargin = this.px10;
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-1, this.px320);
        layoutParams4.addRule(13);
        RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(-1, this.px60);
        layoutParams5.addRule(12);
        layoutParams5.bottomMargin = this.px10;
        RelativeLayout.LayoutParams layoutParams6 = new RelativeLayout.LayoutParams(this.px53, this.px17);
        layoutParams6.addRule(11);
        layoutParams6.addRule(12);
        layoutParams6.setMargins(0, 0, this.px10, 0);
        RelativeLayout relativeLayout = new RelativeLayout(this.context);
        RelativeLayout relativeLayout2 = new RelativeLayout(this.context);
        relativeLayout2.setBackgroundResource(R.drawable.bg);
        relativeLayout2.setLayoutParams(windowParams);
        LinearLayout linearLayout = new LinearLayout(this.context);
        linearLayout.setBackgroundResource(R.drawable.about);
        linearLayout.setOrientation(0);
        linearLayout.setGravity(16);
        linearLayout.setLayoutParams(layoutParams2);
        ImageView imageView = new ImageView(this.context);
        imageView.setBackgroundDrawable(this.appIcon);
        imageView.setLayoutParams(layoutParams3);
        TextView textView = new TextView(this.context);
        textView.setText(this.title);
        textView.setTextColor(Color.rgb(51, 51, 51));
        textView.setTextSize(16.0f);
        linearLayout.addView(imageView);
        linearLayout.addView(textView);
        relativeLayout2.addView(linearLayout);
        LinearLayout linearLayout2 = new LinearLayout(this.context);
        TextView textView2 = new TextView(this.context);
        textView2.setText(this.description);
        textView2.setTextColor(-7829368);
        textView2.setTextSize(16.0f);
        textView2.setMaxLines(4);
        textView2.setLineSpacing(1.0f, 1.3f);
        textView2.setPadding(this.px10, 0, this.px10, 0);
        linearLayout2.addView(textView2);
        linearLayout2.setOrientation(1);
        linearLayout2.setGravity(17);
        linearLayout2.setLayoutParams(layoutParams4);
        LinearLayout.LayoutParams layoutParams7 = new LinearLayout.LayoutParams(-1, this.px186);
        layoutParams7.topMargin = this.px10;
        LinearLayout.LayoutParams layoutParams8 = new LinearLayout.LayoutParams(-2, -1);
        LinearLayout.LayoutParams layoutParams9 = new LinearLayout.LayoutParams(this.px104, this.px156);
        layoutParams9.setMargins(this.px10, 0, this.px10, 0);
        HorizontalScrollView horizontalScrollView = new HorizontalScrollView(this.context);
        horizontalScrollView.setBackgroundResource(R.drawable.bottom);
        horizontalScrollView.setLayoutParams(layoutParams7);
        horizontalScrollView.setHorizontalScrollBarEnabled(false);
        LinearLayout linearLayout3 = new LinearLayout(this.context);
        linearLayout3.setOrientation(0);
        linearLayout3.setGravity(16);
        linearLayout3.setLayoutParams(layoutParams8);
        for (int i = 0; i < this.imageList.size(); i++) {
            ImageView imageView2 = new ImageView(this.context);
            imageView2.setLayoutParams(layoutParams9);
            imageView2.setId(i + 1000);
            imageView2.setBackgroundColor(-7829368);
            loadImage(this.imageList.get(i), i + 1000);
            linearLayout3.addView(imageView2);
        }
        horizontalScrollView.addView(linearLayout3);
        linearLayout2.addView(horizontalScrollView);
        relativeLayout2.addView(linearLayout2);
        LinearLayout linearLayout4 = new LinearLayout(this.context);
        linearLayout4.setOrientation(0);
        linearLayout4.setGravity(17);
        LinearLayout.LayoutParams layoutParams10 = new LinearLayout.LayoutParams(this.px132, this.px34);
        layoutParams10.setMargins(this.px5, 0, this.px5, 0);
        Button button = new Button(this.context);
        button.setBackgroundDrawable(this.mogoBtnBg);
        button.setText("下载");
        button.setTextSize(18.0f);
        button.setPadding(0, 0, 0, 0);
        button.setGravity(17);
        button.setTextColor(-1);
        button.setLayoutParams(layoutParams10);
        button.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                if (event.getAction() == 0) {
                    view.setBackgroundDrawable(ExchangeDialog.this.mogoBtnPrsBg);
                    return false;
                } else if (event.getAction() != 1) {
                    return false;
                } else {
                    view.setBackgroundDrawable(ExchangeDialog.this.mogoBtnBg);
                    ExchangeDialog.this.downloadAPK();
                    return false;
                }
            }
        });
        Button button2 = new Button(this.context);
        button2.setBackgroundDrawable(this.mogoBtnBg);
        button2.setText("收藏");
        button2.setTextSize(18.0f);
        button2.setPadding(0, 0, 0, 0);
        button2.setGravity(17);
        button2.setTextColor(-1);
        button2.setLayoutParams(layoutParams10);
        button2.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                if (event.getAction() == 0) {
                    view.setBackgroundDrawable(ExchangeDialog.this.mogoBtnPrsBg);
                    return false;
                } else if (event.getAction() != 1) {
                    return false;
                } else {
                    view.setBackgroundDrawable(ExchangeDialog.this.mogoBtnBg);
                    ExchangeDialog.this.addMark();
                    return false;
                }
            }
        });
        linearLayout4.addView(button);
        linearLayout4.addView(button2);
        linearLayout4.setLayoutParams(layoutParams5);
        relativeLayout2.addView(linearLayout4);
        Button button3 = new Button(this.context);
        button3.setBackgroundDrawable(this.mogoCloseBtn);
        RelativeLayout.LayoutParams layoutParams11 = new RelativeLayout.LayoutParams(this.px28, this.px28);
        layoutParams11.addRule(11);
        button3.setLayoutParams(layoutParams11);
        button3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ExchangeDialog.this.closeDialog();
            }
        });
        relativeLayout.addView(relativeLayout2);
        relativeLayout.addView(button3);
        addContentView(relativeLayout, windowParams);
    }

    private void initLWindow() {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams.addRule(13);
        RelativeLayout.LayoutParams windowParams = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams.addRule(13);
        windowParams.setMargins(this.px8, this.px8, this.px8, this.px8);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, this.px55);
        layoutParams2.addRule(10);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(this.px36, this.px36);
        layoutParams3.leftMargin = this.px10;
        layoutParams3.rightMargin = this.px10;
        LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(-1, this.px48);
        LinearLayout.LayoutParams layoutParams5 = new LinearLayout.LayoutParams(-1, this.px160);
        RelativeLayout.LayoutParams layoutParams6 = new RelativeLayout.LayoutParams(this.px53, this.px17);
        layoutParams6.addRule(11);
        layoutParams6.addRule(12);
        layoutParams6.setMargins(0, 0, this.px10, this.px55);
        RelativeLayout relativeLayout = new RelativeLayout(this.context);
        LinearLayout linearLayout = new LinearLayout(this.context);
        linearLayout.setOrientation(1);
        linearLayout.setBackgroundResource(R.drawable.bg);
        linearLayout.setLayoutParams(windowParams);
        LinearLayout linearLayout2 = new LinearLayout(this.context);
        linearLayout2.setBackgroundResource(R.drawable.about);
        linearLayout2.setOrientation(0);
        linearLayout2.setGravity(16);
        linearLayout2.setLayoutParams(layoutParams2);
        ImageView imageView = new ImageView(this.context);
        imageView.setBackgroundDrawable(this.appIcon);
        imageView.setLayoutParams(layoutParams3);
        TextView textView = new TextView(this.context);
        textView.setText(this.title);
        textView.setTextColor(Color.rgb(51, 51, 51));
        textView.setTextSize(16.0f);
        linearLayout2.addView(imageView);
        linearLayout2.addView(textView);
        linearLayout.addView(linearLayout2);
        LinearLayout linearLayout3 = new LinearLayout(this.context);
        TextView textView2 = new TextView(this.context);
        textView2.setText(this.description);
        textView2.setTextColor(-7829368);
        textView2.setTextSize(16.0f);
        textView2.setMaxLines(4);
        textView2.setLineSpacing(1.0f, 1.2f);
        textView2.setPadding(this.px10, 0, this.px10, 0);
        linearLayout3.addView(textView2);
        linearLayout3.setOrientation(1);
        linearLayout3.setLayoutParams(layoutParams4);
        linearLayout.addView(linearLayout3);
        LinearLayout linearLayout4 = new LinearLayout(this.context);
        linearLayout4.setOrientation(0);
        linearLayout4.setLayoutParams(layoutParams5);
        linearLayout4.setGravity(17);
        linearLayout4.setBackgroundColor(R.drawable.bottom);
        LinearLayout.LayoutParams layoutParams7 = new LinearLayout.LayoutParams(this.px255, -1);
        LinearLayout.LayoutParams layoutParams8 = new LinearLayout.LayoutParams(-2, -1);
        LinearLayout.LayoutParams layoutParams9 = new LinearLayout.LayoutParams(this.px90, this.px135);
        layoutParams9.setMargins(this.px10, 0, this.px10, 0);
        HorizontalScrollView horizontalScrollView = new HorizontalScrollView(this.context);
        horizontalScrollView.setLayoutParams(layoutParams7);
        horizontalScrollView.setHorizontalScrollBarEnabled(false);
        LinearLayout linearLayout5 = new LinearLayout(this.context);
        linearLayout5.setOrientation(0);
        linearLayout5.setGravity(16);
        linearLayout5.setLayoutParams(layoutParams8);
        for (int i = 0; i < this.imageList.size(); i++) {
            ImageView imageView2 = new ImageView(this.context);
            imageView2.setLayoutParams(layoutParams9);
            imageView2.setId(i + 1000);
            imageView2.setBackgroundColor(-7829368);
            loadImage(this.imageList.get(i), i + 1000);
            linearLayout5.addView(imageView2);
        }
        horizontalScrollView.addView(linearLayout5);
        LinearLayout.LayoutParams layoutParams10 = new LinearLayout.LayoutParams(this.px200, -1);
        LinearLayout linearLayout6 = new LinearLayout(this.context);
        linearLayout6.setOrientation(1);
        linearLayout6.setLayoutParams(layoutParams10);
        linearLayout6.setGravity(17);
        LinearLayout.LayoutParams layoutParams11 = new LinearLayout.LayoutParams(this.px132, this.px34);
        layoutParams11.setMargins(0, 0, 0, this.px25);
        Button button = new Button(this.context);
        button.setBackgroundDrawable(this.mogoBtnBg);
        button.setText("下载");
        button.setTextSize(18.0f);
        button.setPadding(0, 0, 0, 0);
        button.setGravity(17);
        button.setTextColor(-1);
        button.setLayoutParams(layoutParams11);
        button.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                if (event.getAction() == 0) {
                    view.setBackgroundDrawable(ExchangeDialog.this.mogoBtnPrsBg);
                    return false;
                } else if (event.getAction() != 1) {
                    return false;
                } else {
                    view.setBackgroundDrawable(ExchangeDialog.this.mogoBtnBg);
                    ExchangeDialog.this.downloadAPK();
                    return false;
                }
            }
        });
        Button button2 = new Button(this.context);
        button2.setBackgroundDrawable(this.mogoBtnBg);
        button2.setText("收藏");
        button2.setTextSize(18.0f);
        button2.setPadding(0, 0, 0, 0);
        button2.setGravity(17);
        button2.setTextColor(-1);
        button2.setLayoutParams(layoutParams11);
        button2.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                if (event.getAction() == 0) {
                    view.setBackgroundDrawable(ExchangeDialog.this.mogoBtnPrsBg);
                    return false;
                } else if (event.getAction() != 1) {
                    return false;
                } else {
                    view.setBackgroundDrawable(ExchangeDialog.this.mogoBtnBg);
                    ExchangeDialog.this.addMark();
                    return false;
                }
            }
        });
        linearLayout6.addView(button);
        linearLayout6.addView(button2);
        linearLayout4.setLayoutParams(layoutParams5);
        linearLayout4.addView(horizontalScrollView);
        linearLayout4.addView(linearLayout6);
        linearLayout.addView(linearLayout4);
        Button button3 = new Button(this.context);
        button3.setBackgroundDrawable(this.mogoCloseBtn);
        RelativeLayout.LayoutParams layoutParams12 = new RelativeLayout.LayoutParams(this.px28, this.px28);
        layoutParams12.addRule(11);
        button3.setLayoutParams(layoutParams12);
        button3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ExchangeDialog.this.closeDialog();
            }
        });
        relativeLayout.addView(linearLayout);
        relativeLayout.addView(button3);
        addContentView(relativeLayout, windowParams);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* access modifiers changed from: private */
    public void addMark() {
        dismiss();
        this.adMogoLayout.countExClick(this.type, this.id, 0, 0, 1);
        ContentValues inputValue = new ContentValues();
        inputValue.put("bookmark", (Integer) 1);
        inputValue.put("title", this.title);
        inputValue.put("url", this.downloadLink);
        this.context.getContentResolver().insert(Browser.BOOKMARKS_URI, inputValue);
        Toast.makeText(this.context, "添加至浏览器书签", 0).show();
    }

    /* access modifiers changed from: private */
    public void closeDialog() {
        dismiss();
        this.adMogoLayout.countExClick(this.type, this.id, 1, 0, 0);
    }

    /* access modifiers changed from: private */
    public void downloadAPK() {
        dismiss();
        this.adMogoLayout.countExClick(this.type, this.id, 0, 1, 0);
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(this.downloadLink));
        intent.addFlags(268435456);
        this.context.startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        this.mogoBtnBg = null;
        this.mogoBtnPrsBg = null;
        this.mogoCloseBtn = null;
        cancel();
        this.adMogoLayout.closeDialog();
    }

    private void loadImage(String url, final int id2) {
        Drawable cacheImage = this.asyncImageLoader.loadDrawable(url, new AsyncImageLoader.ImageCallback() {
            public void imageLoaded(Drawable imageDrawable) {
                ((ImageView) ExchangeDialog.this.findViewById(id2)).setImageDrawable(imageDrawable);
            }
        });
        if (cacheImage != null) {
            ((ImageView) findViewById(id2)).setImageDrawable(cacheImage);
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || event.getRepeatCount() != 0) {
            return false;
        }
        closeDialog();
        return false;
    }
}
