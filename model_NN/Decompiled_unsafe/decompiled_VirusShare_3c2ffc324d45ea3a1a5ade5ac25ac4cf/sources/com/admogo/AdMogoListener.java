package com.admogo;

public interface AdMogoListener {
    void onClickAd();

    void onCloseMogoDialog();

    void onFailedReceiveAd();

    void onReceiveAd();
}
