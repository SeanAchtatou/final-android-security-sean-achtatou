package com.admogo;

import android.graphics.drawable.Drawable;
import android.os.Handler;
import java.lang.ref.SoftReference;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AsyncImageLoader {
    private ExecutorService executorService = Executors.newFixedThreadPool(5);
    /* access modifiers changed from: private */
    public final Handler handler = new Handler();
    public Map<String, SoftReference<Drawable>> imageCache = new HashMap();

    public interface ImageCallback {
        void imageLoaded(Drawable drawable);
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public Drawable loadDrawable(final String imageUrl, final ImageCallback callback) {
        if (this.imageCache.containsKey(imageUrl)) {
            SoftReference<Drawable> softReference = this.imageCache.get(imageUrl);
            if (softReference.get() != null) {
                return (Drawable) softReference.get();
            }
        }
        this.executorService.submit(new Runnable() {
            public void run() {
                try {
                    final Drawable drawable = AsyncImageLoader.this.loadImageFromUrl(imageUrl);
                    AsyncImageLoader.this.imageCache.put(imageUrl, new SoftReference(drawable));
                    Handler access$0 = AsyncImageLoader.this.handler;
                    final ImageCallback imageCallback = callback;
                    access$0.post(new Runnable() {
                        public void run() {
                            imageCallback.imageLoaded(drawable);
                        }
                    });
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        });
        return null;
    }

    /* access modifiers changed from: protected */
    public Drawable loadImageFromUrl(String imageUrl) {
        try {
            return Drawable.createFromStream(new URL(imageUrl).openStream(), "image.png");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
