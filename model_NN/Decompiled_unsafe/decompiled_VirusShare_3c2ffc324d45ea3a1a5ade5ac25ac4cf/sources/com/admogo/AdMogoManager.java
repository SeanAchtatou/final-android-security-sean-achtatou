package com.admogo;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.view.WindowManager;
import com.admogo.obj.Custom;
import com.admogo.obj.Exchange;
import com.admogo.obj.Extra;
import com.admogo.obj.Mogo;
import com.admogo.obj.Ration;
import com.admogo.util.AdMogoUtil;
import com.madhouse.android.ads.AdView;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AdMogoManager {
    private static final String PREFS_STRING_CONFIG = "config";
    private static final String PREFS_STRING_TIMESTAMP = "timestamp";
    private static final int TIMEOUT_TIME = 8000;
    /* access modifiers changed from: private */
    public static int URL_INDEX = 0;
    private static long configExpireTimeout = 1800000;
    public static Ration lastRation;
    public static boolean refetchConfig = false;
    private static Hashtable<String, String> tempConfig = new Hashtable<>();
    private int adType;
    public int ad_type;
    public String birthday;
    /* access modifiers changed from: private */
    public WeakReference<Context> contextReference;
    private String cpuInfo;
    public String deviceIDHash;
    public String deviceName;
    private Extra extra;
    public String gender;
    private String getCountryCode;
    public boolean isSendData = false;
    public String keyAdMogo;
    public String keywords;
    public Location location;
    private String memoryInfo;
    public String networkType;
    private String operInfo;
    public String os;
    private String packageName;
    private int pngSize = 0;
    private Ration randomRation;
    private List<Ration> rationsList;
    Iterator<Ration> rollovers;
    public final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    private String screenSize;
    private Runnable searchURL = new Runnable() {
        public void run() {
            AdMogoManager.URL_INDEX = AdMogoManager.URL_INDEX + 1;
            if (AdMogoManager.URL_INDEX < 4) {
                AdMogoUtil.changeURL(AdMogoManager.URL_INDEX);
                AdMogoManager.refetchConfig = true;
                AdMogoManager.this.fetchConfig();
            }
        }
    };
    private List<Ration> tempRationsList = new ArrayList();
    private double totalWeight = 0.0d;
    private double versionCode;

    public AdMogoManager(WeakReference<Context> contextReference2, String keyAdMogo2, int adType2) {
        Log.i(AdMogoUtil.ADMOGO, "Creating adMogoManager...");
        this.contextReference = contextReference2;
        this.keyAdMogo = keyAdMogo2;
        this.adType = adType2;
        WindowManager WM = (WindowManager) contextReference2.get().getSystemService("window");
        int width = WM.getDefaultDisplay().getWidth();
        int height = WM.getDefaultDisplay().getHeight();
        if (width <= height) {
            this.screenSize = String.valueOf(width) + "*" + height;
            switch (width) {
                case AdView.PHONE_AD_MEASURE_240 /*240*/:
                    this.pngSize = 1;
                    break;
                case AdView.PHONE_AD_MEASURE_320 /*320*/:
                    this.pngSize = 2;
                    break;
                case AdView.PHONE_AD_MEASURE_480 /*480*/:
                    this.pngSize = 3;
                    break;
                default:
                    this.pngSize = 0;
                    break;
            }
        } else {
            this.screenSize = String.valueOf(height) + "*" + width;
            switch (height) {
                case AdView.PHONE_AD_MEASURE_240 /*240*/:
                    this.pngSize = 1;
                    break;
                case AdView.PHONE_AD_MEASURE_320 /*320*/:
                    this.pngSize = 2;
                    break;
                case AdView.PHONE_AD_MEASURE_480 /*480*/:
                    this.pngSize = 3;
                    break;
                default:
                    this.pngSize = 0;
                    break;
            }
        }
        try {
            this.packageName = contextReference2.get().getPackageName();
            this.versionCode = (double) contextReference2.get().getPackageManager().getPackageInfo(this.packageName, 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            this.versionCode = 1.0d;
        }
        try {
            this.networkType = GetUserInfo.getNetworkType(contextReference2.get());
            this.operInfo = GetUserInfo.getOperators(contextReference2.get());
            this.cpuInfo = GetUserInfo.GetCPUInfo();
            this.memoryInfo = GetUserInfo.getMemoryInfo(contextReference2.get());
        } catch (Exception e2) {
            Log.w("getPhoneInfo", e2.toString());
        }
        this.deviceIDHash = GetUserInfo.getDeviceID(contextReference2.get());
        this.deviceIDHash = AdMogoUtil.convertToHex(this.deviceIDHash);
        this.os = URLEncoder.encode(Build.VERSION.RELEASE);
        this.deviceName = URLEncoder.encode(Build.MODEL);
        Log.d(AdMogoUtil.ADMOGO, "Hashed device ID is: " + this.deviceIDHash);
        Log.d(AdMogoUtil.ADMOGO, "AdsMOGO SDK Version : 1.0.8");
        Log.i(AdMogoUtil.ADMOGO, "Finished creating adMogoManager");
    }

    public static void setConfigExpireTimeout(long configExpireTimeout2) {
        configExpireTimeout = configExpireTimeout2;
    }

    public Extra getExtra() {
        if (this.totalWeight > 0.0d) {
            return this.extra;
        }
        Log.e(AdMogoUtil.ADMOGO, "Sum of ration weights is 0 - no ads to be shown");
        return null;
    }

    public Ration getRation(Ration activeRation) {
        Random random = new Random();
        this.totalWeight = 100.0d;
        if (activeRation == null || this.extra.improveClick == 0) {
            double r = random.nextDouble() * this.totalWeight;
            double s = 0.0d;
            Log.d(AdMogoUtil.ADMOGO, "Dart is <" + r + "> of <" + this.totalWeight + ">");
            Iterator<Ration> it = this.rationsList.iterator();
            Ration ration = null;
            while (it.hasNext()) {
                ration = it.next();
                s += ration.weight;
                if (s >= r) {
                    break;
                }
            }
            this.randomRation = ration;
            return ration;
        }
        this.tempRationsList.clear();
        for (int i = 0; i < this.rationsList.size(); i++) {
            if (!this.rationsList.get(i).nid.equals(activeRation.nid)) {
                this.tempRationsList.add(this.rationsList.get(i));
            }
        }
        double tempWeight = activeRation.weight;
        this.tempRationsList.add(activeRation);
        double r2 = random.nextDouble() * (this.totalWeight - tempWeight);
        double s2 = 0.0d;
        this.rollovers = this.tempRationsList.iterator();
        Log.d(AdMogoUtil.ADMOGO, "Dart is <" + r2 + "> of <" + this.totalWeight + ">");
        Iterator<Ration> it2 = this.tempRationsList.iterator();
        Ration ration2 = null;
        while (it2.hasNext()) {
            ration2 = it2.next();
            s2 += ration2.weight;
            if (s2 >= r2) {
                break;
            }
        }
        this.randomRation = ration2;
        return ration2;
    }

    public Ration getRollover() {
        if (this.rollovers == null) {
            return null;
        }
        Ration ration = null;
        if (this.rollovers.hasNext()) {
            ration = this.rollovers.next();
            lastRation = ration;
            if (this.randomRation != null && this.randomRation.type == ration.type) {
                if (!this.rollovers.hasNext()) {
                    return null;
                }
                ration = this.rollovers.next();
            }
        }
        return ration;
    }

    public void resetRollover() {
        this.rollovers = this.rationsList.iterator();
    }

    public Custom getCustom(String nid) {
        try {
            HttpResponse httpResponse = new DefaultHttpClient().execute(new HttpGet(String.format(AdMogoUtil.urlCustom, this.keyAdMogo, nid, this.deviceIDHash, this.getCountryCode, Integer.valueOf(this.adType))));
            Log.d(AdMogoUtil.ADMOGO, httpResponse.getStatusLine().toString());
            HttpEntity entity = httpResponse.getEntity();
            if (entity != null) {
                return parseCustomJsonString(convertStreamToString(entity.getContent()));
            }
        } catch (ClientProtocolException e) {
            Log.e(AdMogoUtil.ADMOGO, "Caught ClientProtocolException in getCustom()", e);
        } catch (IOException e2) {
            Log.e(AdMogoUtil.ADMOGO, "Caught IOException in getCustom()", e2);
        }
        return null;
    }

    public Mogo getMogo(String nid) {
        HttpClient httpClient = new DefaultHttpClient();
        this.gender = AdMogoTargeting.getGender().toString();
        this.keywords = AdMogoTargeting.getKeywords();
        GregorianCalendar gc = AdMogoTargeting.getBirthDate();
        int year = gc.get(1);
        int month = gc.get(2);
        int date = gc.get(5);
        this.birthday = new StringBuilder().append(year).toString();
        if (month < 10) {
            this.birthday = String.valueOf(this.birthday) + "0" + month;
        } else {
            this.birthday = String.valueOf(this.birthday) + month;
        }
        if (date < 10) {
            this.birthday = String.valueOf(this.birthday) + "0" + date;
        } else {
            this.birthday = String.valueOf(this.birthday) + date;
        }
        try {
            HttpResponse httpResponse = httpClient.execute(new HttpGet(String.format(AdMogoUtil.urlMogo, this.keyAdMogo, nid, this.deviceIDHash, this.getCountryCode, Integer.valueOf((int) AdMogoUtil.VERSION), this.networkType, this.os, this.deviceName)));
            Log.d(AdMogoUtil.ADMOGO, httpResponse.getStatusLine().toString());
            HttpEntity entity = httpResponse.getEntity();
            if (entity != null) {
                return parseMogoJsonString(convertStreamToString(entity.getContent()));
            }
        } catch (ClientProtocolException e) {
            Log.e(AdMogoUtil.ADMOGO, "Caught ClientProtocolException in getMogo()", e);
        } catch (IOException e2) {
            Log.e(AdMogoUtil.ADMOGO, "Caught IOException in getMogo()", e2);
        }
        return null;
    }

    public Exchange getExchange(String nid) {
        try {
            HttpResponse httpResponse = new DefaultHttpClient().execute(new HttpGet(String.format(AdMogoUtil.urlExchange, this.keyAdMogo, Integer.valueOf(this.adType), this.getCountryCode)));
            Log.d(AdMogoUtil.ADMOGO, httpResponse.getStatusLine().toString());
            HttpEntity entity = httpResponse.getEntity();
            if (entity != null) {
                return parseExchangeJsonString(convertStreamToString(entity.getContent()));
            }
        } catch (ClientProtocolException e) {
            Log.e(AdMogoUtil.ADMOGO, "Caught ClientProtocolException in getExchange()", e);
        } catch (IOException e2) {
            Log.e(AdMogoUtil.ADMOGO, "Caught IOException in getExchange()", e2);
        }
        return null;
    }

    public void fetchConfig() {
        Context context = this.contextReference.get();
        if (context != null) {
            SharedPreferences adMogoPrefs = context.getSharedPreferences(this.keyAdMogo, 0);
            String jsonString = adMogoPrefs.getString(String.valueOf(this.keyAdMogo) + this.adType + PREFS_STRING_CONFIG, null);
            long timestamp = adMogoPrefs.getLong(String.valueOf(this.keyAdMogo) + this.adType + PREFS_STRING_TIMESTAMP, -1);
            if (System.currentTimeMillis() >= configExpireTimeout + timestamp || TextUtils.isEmpty(tempConfig.get(String.valueOf(this.keyAdMogo) + this.adType)) || refetchConfig) {
                refetchConfig = false;
                Log.i(AdMogoUtil.ADMOGO, "Stored config info not present or expired, fetching fresh data");
                int ad_Type_info = 2;
                if (this.adType == 1) {
                    ad_Type_info = 2;
                } else if (this.adType == 2 || this.adType == 3 || this.adType == 4 || this.adType == 5) {
                    ad_Type_info = 8;
                } else if (this.adType == 7) {
                    ad_Type_info = 32;
                } else if (this.adType == 6) {
                    ad_Type_info = 128;
                }
                HttpGet httpGet = new HttpGet(String.format(AdMogoUtil.urlConfig, this.keyAdMogo, Integer.valueOf((int) AdMogoUtil.VERSION), AdMogoUtil.VER, this.packageName, Double.valueOf(this.versionCode), Integer.valueOf(ad_Type_info), this.getCountryCode, this.networkType, this.operInfo, this.deviceIDHash, this.os, this.deviceName, this.screenSize, this.cpuInfo, this.memoryInfo));
                BasicHttpParams basicHttpParams = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(basicHttpParams, TIMEOUT_TIME);
                HttpConnectionParams.setSoTimeout(basicHttpParams, TIMEOUT_TIME);
                try {
                    HttpResponse httpResponse = new DefaultHttpClient(basicHttpParams).execute(httpGet);
                    Log.d(AdMogoUtil.ADMOGO, String.format("Showing Config:\n Mogo_ID: %s\n CountryCode: %s", this.keyAdMogo, this.getCountryCode));
                    Log.d(AdMogoUtil.ADMOGO, httpResponse.getStatusLine().toString());
                    HttpEntity entity = httpResponse.getEntity();
                    if (entity != null) {
                        if (httpResponse.getStatusLine().getStatusCode() == 200) {
                            jsonString = convertStreamToString(entity.getContent());
                            this.scheduler.schedule(new SendDataRunnable(), 0, TimeUnit.SECONDS);
                            Log.d(AdMogoUtil.ADMOGO, "Prefs{" + this.keyAdMogo + "}: {\"" + PREFS_STRING_CONFIG + "\": \"" + jsonString + "\", \"" + PREFS_STRING_TIMESTAMP + "\": " + timestamp + "}");
                            SharedPreferences.Editor editor = adMogoPrefs.edit();
                            editor.putString(String.valueOf(this.keyAdMogo) + this.adType + PREFS_STRING_CONFIG, jsonString);
                            editor.putLong(String.valueOf(this.keyAdMogo) + this.adType + PREFS_STRING_TIMESTAMP, System.currentTimeMillis());
                            editor.commit();
                        } else {
                            this.scheduler.schedule(this.searchURL, 0, TimeUnit.SECONDS);
                        }
                    }
                } catch (ClientProtocolException e) {
                    Log.e(AdMogoUtil.ADMOGO, "Caught ClientProtocolException in fetchConfig()", e);
                    if (this.adType == 1) {
                        if (jsonString == null) {
                            searchURL();
                            return;
                        }
                        this.scheduler.schedule(this.searchURL, 0, TimeUnit.SECONDS);
                    } else if (jsonString == null) {
                        searchURL();
                        return;
                    } else {
                        this.scheduler.schedule(this.searchURL, 0, TimeUnit.SECONDS);
                    }
                } catch (IOException e2) {
                    Log.e(AdMogoUtil.ADMOGO, "Caught IOException in fetchConfig()", e2);
                    if (this.adType == 1) {
                        if (jsonString == null) {
                            searchURL();
                            return;
                        }
                        this.scheduler.schedule(this.searchURL, 0, TimeUnit.SECONDS);
                    } else if (jsonString == null) {
                        searchURL();
                        return;
                    } else {
                        this.scheduler.schedule(this.searchURL, 0, TimeUnit.SECONDS);
                    }
                }
            } else {
                Log.i(AdMogoUtil.ADMOGO, "Using stored config data");
            }
            tempConfig.put(String.valueOf(this.keyAdMogo) + this.adType, jsonString);
            parseConfigurationString(jsonString);
        }
    }

    private class SendDataRunnable implements Runnable {
        public SendDataRunnable() {
        }

        public void run() {
            try {
                DataBackup backup = new DataBackup();
                backup.open((Context) AdMogoManager.this.contextReference.get());
                String data = backup.getDataList();
                backup.close();
                if (!TextUtils.isEmpty(data) && !AdMogoManager.this.isSendData) {
                    String nid = AdMogoUtil.convertToHex(String.valueOf(AdMogoManager.this.keyAdMogo) + data + "Q8tFVImbNuvsmBwWwdqsPE6jsRQsSPkQ");
                    AdMogoManager.this.isSendData = true;
                    HttpPost httpRequest = new HttpPost(AdMogoUtil.urlRecordData);
                    HttpParams httpParameters = new BasicHttpParams();
                    HttpConnectionParams.setConnectionTimeout(httpParameters, 15000);
                    HttpConnectionParams.setSoTimeout(httpParameters, 15000);
                    HttpClient httpClient = new DefaultHttpClient(httpParameters);
                    List<NameValuePair> params = new ArrayList<>();
                    params.add(new BasicNameValuePair("appid", AdMogoManager.this.keyAdMogo));
                    params.add(new BasicNameValuePair("data", data));
                    params.add(new BasicNameValuePair("nid", nid));
                    httpRequest.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
                    HttpResponse httpResponse = httpClient.execute(httpRequest);
                    Log.d(AdMogoUtil.ADMOGO, httpResponse.getStatusLine().toString());
                    if (httpResponse.getStatusLine().getStatusCode() == 200) {
                        Log.d(AdMogoUtil.ADMOGO, "Data Backup Success");
                        backup.open((Context) AdMogoManager.this.contextReference.get());
                        if (backup.clearData()) {
                            AdMogoManager.this.isSendData = true;
                        }
                        backup.close();
                    }
                }
            } catch (IOException e) {
                Log.e(AdMogoUtil.ADMOGO, "Caught IOException in Send Data", e);
            }
        }
    }

    private void searchURL() {
        URL_INDEX++;
        if (URL_INDEX < 4) {
            AdMogoUtil.changeURL(URL_INDEX);
            refetchConfig = true;
            fetchConfig();
        }
    }

    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is), 8192);
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String line = reader.readLine();
                if (line == null) {
                    try {
                        is.close();
                        return sb.toString();
                    } catch (IOException e) {
                        Log.e(AdMogoUtil.ADMOGO, "Caught IOException in convertStreamToString()", e);
                        return null;
                    }
                } else {
                    sb.append(String.valueOf(line) + "\n");
                }
            } catch (IOException e2) {
                Log.e(AdMogoUtil.ADMOGO, "Caught IOException in convertStreamToString()", e2);
                try {
                    is.close();
                    return null;
                } catch (IOException e3) {
                    Log.e(AdMogoUtil.ADMOGO, "Caught IOException in convertStreamToString()", e3);
                    return null;
                }
            } catch (Throwable th) {
                try {
                    is.close();
                    throw th;
                } catch (IOException e4) {
                    Log.e(AdMogoUtil.ADMOGO, "Caught IOException in convertStreamToString()", e4);
                    return null;
                }
            }
        }
    }

    private void parseConfigurationString(String jsonString) {
        try {
            JSONObject json = new JSONObject(jsonString);
            parseExtraJson(json.getJSONObject("extra"));
            parseRationsJson(json.getJSONArray("rations"));
        } catch (JSONException e) {
            Log.e(AdMogoUtil.ADMOGO, "Unable to parse response from JSON. This may or may not be fatal.", e);
            this.extra = new Extra();
        } catch (NullPointerException e2) {
            Log.e(AdMogoUtil.ADMOGO, "Unable to parse response from JSON. This may or may not be fatal.", e2);
            this.extra = new Extra();
        }
    }

    private void parseExtraJson(JSONObject json) {
        Extra extra2 = new Extra();
        try {
            extra2.cycleTime = json.getInt("cycle_time");
            extra2.locationOn = json.getInt("location_on");
            extra2.transition = json.getInt("transition");
            extra2.improveClick = json.getInt("improve_click");
            JSONObject backgroundColor = json.getJSONObject("background_color_rgb");
            extra2.bgRed = backgroundColor.getInt("red");
            extra2.bgGreen = backgroundColor.getInt("green");
            extra2.bgBlue = backgroundColor.getInt("blue");
            extra2.bgAlpha = backgroundColor.getInt("alpha") * 255;
            JSONObject textColor = json.getJSONObject("text_color_rgb");
            extra2.fgRed = textColor.getInt("red");
            extra2.fgGreen = textColor.getInt("green");
            extra2.fgBlue = textColor.getInt("blue");
            extra2.fgAlpha = textColor.getInt("alpha") * 255;
        } catch (JSONException e) {
            Log.e(AdMogoUtil.ADMOGO, "Exception in parsing config.extra JSON. This may or may not be fatal.", e);
        }
        this.extra = extra2;
    }

    private void parseRationsJson(JSONArray json) {
        List<Ration> rationsList2 = new ArrayList<>();
        this.totalWeight = 0.0d;
        int i = 0;
        while (i < json.length()) {
            try {
                JSONObject jsonRation = json.getJSONObject(i);
                if (jsonRation != null) {
                    Ration ration = new Ration();
                    ration.nid = jsonRation.getString("nid");
                    ration.type = jsonRation.getInt("type");
                    ration.name = jsonRation.getString("nname");
                    ration.weight = (double) jsonRation.getInt("weight");
                    ration.priority = jsonRation.getInt("priority");
                    ration.testmodel = jsonRation.getInt("testmodel") != 0;
                    switch (ration.type) {
                        case 8:
                            JSONObject keyObj = jsonRation.getJSONObject("key");
                            ration.key = keyObj.getString("siteID");
                            ration.key2 = keyObj.getString("publisherID");
                            break;
                        default:
                            ration.key = jsonRation.getString("key");
                            break;
                    }
                    this.totalWeight += ration.weight;
                    rationsList2.add(ration);
                }
                i++;
            } catch (JSONException e) {
                Log.e(AdMogoUtil.ADMOGO, "JSONException in parsing config.rations JSON. This may or may not be fatal.", e);
            }
        }
        Collections.sort(rationsList2);
        this.rationsList = rationsList2;
        this.rollovers = this.rationsList.iterator();
    }

    private List<String> parseImageJson(JSONArray json) {
        List<String> imageList = new ArrayList<>();
        int i = 0;
        while (i < json.length()) {
            try {
                String imageURL = json.getString(i);
                if (imageURL != null) {
                    imageList.add(imageURL);
                }
                i++;
            } catch (JSONException e) {
                Log.e(AdMogoUtil.ADMOGO, "JSONException in parsing image JSON. This may or may not be fatal.", e);
                return null;
            }
        }
        return imageList;
    }

    private Custom parseCustomJsonString(String jsonString) {
        Custom custom = new Custom();
        try {
            JSONObject json = new JSONObject(jsonString);
            custom.type = json.getInt("ad_type");
            custom.imageLink = json.getString("img_url");
            custom.iconLink = json.getString("AppIcon");
            custom.appName = json.getString("AppName");
            custom.link = json.getString("redirect_url");
            custom.appDes = json.getString("AppDes");
            custom.adText = json.getString("ad_text");
            custom.linkType = json.getInt("link_type");
            custom.imageUrlList = parseImageJson(json.getJSONArray("AppImage"));
            switch (this.pngSize) {
                case 1:
                    custom.iconLink = custom.iconLink.replace("76x76", "38x38");
                    break;
                case 2:
                    custom.iconLink = custom.iconLink.replace("76x76", "38x38");
                    break;
                case 3:
                    custom.imageLink = custom.imageLink.replace("38x38", "60x60");
                    custom.iconLink = custom.iconLink.replace("76x76", "60x60");
                    for (int i = 0; i < custom.imageUrlList.size(); i++) {
                        custom.imageUrlList.set(i, custom.imageUrlList.get(i).replace("104x156", "208x312"));
                    }
                    break;
            }
            custom.image = fetchImage(custom.imageLink);
            if (custom.type == 2) {
                custom.appIcon = fetchImage(custom.iconLink);
            }
            return custom;
        } catch (JSONException e) {
            Log.e(AdMogoUtil.ADMOGO, "Caught JSONException in parseCustomJsonString()", e);
            return null;
        }
    }

    private Mogo parseMogoJsonString(String jsonString) {
        Mogo mogo = new Mogo();
        try {
            JSONObject json = new JSONObject(jsonString);
            mogo.type = json.getInt("banner_type");
            mogo.imageLink = json.getString("img_url");
            mogo.link = json.getString("redirect_url");
            mogo.description = json.getString("ad_text");
            mogo.image = fetchImage(mogo.imageLink);
            return mogo;
        } catch (JSONException e) {
            Log.e(AdMogoUtil.ADMOGO, "Caught JSONException in parseMogoJsonString()", e);
            return null;
        }
    }

    private Exchange parseExchangeJsonString(String jsonString) {
        Exchange exchange = new Exchange();
        try {
            JSONObject json = new JSONObject(jsonString);
            exchange.type = json.getInt("ad_type");
            exchange.cid = json.getString("cid");
            exchange.imageLink = json.getString("img_url");
            exchange.iconLink = json.getString("AppIcon");
            exchange.appName = json.getString("AppName");
            exchange.link = json.getString("redirect_url");
            exchange.appDes = json.getString("AppDes");
            exchange.adText = json.getString("ad_text");
            exchange.linkType = json.getInt("link_type");
            exchange.imageUrlList = parseImageJson(json.getJSONArray("AppImage"));
            switch (this.pngSize) {
                case 1:
                    exchange.iconLink = exchange.iconLink.replace("76x76", "38x38");
                    break;
                case 2:
                    exchange.iconLink = exchange.iconLink.replace("76x76", "38x38");
                    break;
                case 3:
                    exchange.imageLink = exchange.imageLink.replace("38x38", "60x60");
                    exchange.iconLink = exchange.iconLink.replace("76x76", "60x60");
                    for (int i = 0; i < exchange.imageUrlList.size(); i++) {
                        exchange.imageUrlList.set(i, exchange.imageUrlList.get(i).replace("104x156", "208x312"));
                    }
                    break;
            }
            exchange.image = fetchImage(exchange.imageLink);
            if (exchange.type == 2) {
                exchange.appIcon = fetchImage(exchange.iconLink);
            }
            return exchange;
        } catch (JSONException e) {
            Log.w(AdMogoUtil.ADMOGO, "No Exchange Ad Show");
            return null;
        }
    }

    private Drawable fetchImage(String urlString) {
        try {
            InputStream is = (InputStream) new URL(urlString).getContent();
            Drawable d = Drawable.createFromStream(is, "src");
            is.close();
            return d;
        } catch (Exception e) {
            Log.w(AdMogoUtil.ADMOGO, "Unable to fetchImage(): ", e);
            return null;
        }
    }

    public void setLocation(String counCode) {
        this.getCountryCode = counCode;
    }

    public Location getLocation() {
        if (this.contextReference == null) {
            return null;
        }
        Context context = this.contextReference.get();
        if (context == null) {
            return null;
        }
        try {
            LocationManager locationManager = (LocationManager) context.getSystemService("location");
            Criteria criteria = new Criteria();
            criteria.setAccuracy(1);
            criteria.setAltitudeRequired(false);
            criteria.setBearingRequired(false);
            criteria.setCostAllowed(true);
            criteria.setPowerRequirement(1);
            return locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, true));
        } catch (Exception e) {
            Log.w(AdMogoUtil.ADMOGO, "Get Location", e);
            return null;
        }
    }

    public static void clear() {
        Log.d(AdMogoUtil.ADMOGO, "Is Cleaning");
        AdMogoTargeting.countryCode = "";
        URL_INDEX = 0;
        AdMogoUtil.changeURL(URL_INDEX);
        tempConfig.clear();
    }
}
