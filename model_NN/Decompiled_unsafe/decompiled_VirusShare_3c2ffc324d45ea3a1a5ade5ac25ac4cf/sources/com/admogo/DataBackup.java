package com.admogo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.UUID;

public class DataBackup {
    private static final String CREATE_MOGO_BACKUP = "CREATE TABLE IF NOT EXISTS mogo_backup (_id INTEGER PRIMARY KEY AUTOINCREMENT, uuid TEXT NOT NULL, nid TEXT NOT NULL, aid TEXT NOT NULL, country TEXT NOT NULL, type INTEGER NOT NULL, request INTEGER NOT NULL, impression INTEGER NOT NULL, click INTEGER NOT NULL, blank INTEGER NOT NULL, datetime DATETIME NOT NULL, adtype INTEGER NOT NULL)";
    private static final String DATABASE_NAME = "AdsMOGO";
    private static final int DATABASE_VERSION = 1;
    private String ADTYPE = "adtype";
    private String AID = "aid";
    private String BLANK = "blank";
    private String CLICK = "click";
    private String COUNTRY = "country";
    private String DATETIME = "datetime";
    private String IMPRESSION = "impression";
    private String NID = "nid";
    private String REQUEST = "request";
    private String TABLE_MOGO_BACKUP = "mogo_backup";
    private String TYPE = "type";
    private String UID = "uuid";
    private SQLiteDatabase mDb;
    private DatabaseHelper mDbHelper;

    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, DataBackup.DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, 1);
        }

        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DataBackup.CREATE_MOGO_BACKUP);
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }

    public void open(Context context) {
        this.mDbHelper = new DatabaseHelper(context);
        this.mDb = this.mDbHelper.getWritableDatabase();
    }

    public void close() {
        this.mDb.close();
        this.mDbHelper.close();
    }

    public long insertData(Object[] data) {
        String date = getStrTime(Calendar.getInstance().getTimeInMillis() - ((long) TimeZone.getDefault().getRawOffset()));
        UUID uuid = UUID.randomUUID();
        ContentValues insert = new ContentValues();
        insert.put(this.UID, uuid.toString());
        insert.put(this.AID, data[0].toString());
        insert.put(this.NID, data[1].toString());
        insert.put(this.TYPE, data[2].toString());
        insert.put(this.ADTYPE, data[3].toString());
        insert.put(this.COUNTRY, data[4].toString());
        insert.put(this.REQUEST, data[5].toString());
        insert.put(this.IMPRESSION, data[6].toString());
        insert.put(this.CLICK, data[7].toString());
        insert.put(this.BLANK, data[8].toString());
        insert.put(this.DATETIME, date);
        return this.mDb.insert(this.TABLE_MOGO_BACKUP, null, insert);
    }

    public String getDataList() {
        Cursor cursor = this.mDb.query(this.TABLE_MOGO_BACKUP, new String[]{this.UID, this.NID, this.AID, this.COUNTRY, this.TYPE, "SUM (" + this.REQUEST + ")", "SUM (" + this.IMPRESSION + ")", "SUM (" + this.CLICK + ")", "SUM (" + this.BLANK + ")", this.DATETIME, this.ADTYPE}, null, null, "strftime('%Y-%m-%d %H'," + this.DATETIME + ")," + this.TYPE + "," + this.AID, null, null);
        String totalData = "";
        if (cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                do {
                    totalData = String.valueOf(totalData) + ("<HitTable><ID>" + cursor.getString(0) + "</ID>") + ("<Nid>" + cursor.getString(1) + "</Nid>") + ("<Aid>" + cursor.getString(2) + "</Aid>") + ("<Country>" + cursor.getString(3) + "</Country>") + ("<Type>" + cursor.getString(4) + "</Type>") + ("<Requests>" + cursor.getString(5) + "</Requests>") + ("<Impressions>" + cursor.getString(6) + "</Impressions>") + ("<Clicks>" + cursor.getString(7) + "</Clicks>") + ("<Blanks>" + cursor.getString(8) + "</Blanks>") + ("<DateTime>" + cursor.getString(9).replace(" ", "T") + "</DateTime>") + ("<ADType>" + cursor.getString(10) + "</ADType>") + "</HitTable>";
                } while (cursor.moveToNext());
            }
            totalData = String.valueOf("<?xml version=\"1.0\" standalone=\"yes\"?><DocumentElement>") + totalData + "</DocumentElement>";
        }
        cursor.close();
        return totalData;
    }

    public boolean clearData() {
        return this.mDb.delete(this.TABLE_MOGO_BACKUP, null, null) > 0;
    }

    public static String getStrTime(long cc_time) {
        return new SimpleDateFormat("yyyy-MM-dd HH:00:00").format(new Date(cc_time));
    }
}
