package com.admogo.util;

import android.app.Activity;
import android.util.DisplayMetrics;

public class AdMogoUtil extends Activity {
    public static final String ADMOGO = "AdsMOGO SDK";
    public static final int CUSTOM_TYPE_BANNER = 1;
    public static final int CUSTOM_TYPE_FULL = 3;
    public static final int CUSTOM_TYPE_ICON = 2;
    public static final int EXCHANGE_TYPE_BANNER = 1;
    public static final int EXCHANGE_TYPE_FULL = 3;
    public static final int EXCHANGE_TYPE_ICON = 2;
    public static final int MOGO_TYPE_BANNER = 1;
    public static final int MOGO_TYPE_ICON = 2;
    public static final int NETWORK_TYPE_4THSCREEN = 13;
    public static final int NETWORK_TYPE_ADCHINA = 21;
    public static final int NETWORK_TYPE_ADMOB = 1;
    public static final int NETWORK_TYPE_ADMOGO = 10;
    public static final int NETWORK_TYPE_ADSENSE = 14;
    public static final int NETWORK_TYPE_ADTOUCH = 28;
    public static final int NETWORK_TYPE_ADWO = 33;
    public static final int NETWORK_TYPE_AIRAD = 32;
    public static final int NETWORK_TYPE_APPMEDIA = 36;
    public static final int NETWORK_TYPE_BAIDU = 44;
    public static final int NETWORK_TYPE_CASEE = 25;
    public static final int NETWORK_TYPE_CUSTOM = 9;
    public static final int NETWORK_TYPE_DOMOB = 29;
    public static final int NETWORK_TYPE_DOUBLECLICK = 15;
    public static final int NETWORK_TYPE_EVENT = 17;
    public static final int NETWORK_TYPE_EXCHANGE = 45;
    public static final int NETWORK_TYPE_GENERIC = 16;
    public static final int NETWORK_TYPE_GREYSTRIP = 7;
    public static final int NETWORK_TYPE_INMOBI = 18;
    public static final int NETWORK_TYPE_IZP = 40;
    public static final int NETWORK_TYPE_JUMPTAP = 2;
    public static final int NETWORK_TYPE_LIVERAIL = 5;
    public static final int NETWORK_TYPE_LSENSE = 34;
    public static final int NETWORK_TYPE_MDOTM = 12;
    public static final int NETWORK_TYPE_MEDIALETS = 4;
    public static final int NETWORK_TYPE_MILLENNIAL = 6;
    public static final int NETWORK_TYPE_MOBCLIX = 11;
    public static final int NETWORK_TYPE_MOGO = 27;
    public static final int NETWORK_TYPE_QUATTRO = 8;
    public static final int NETWORK_TYPE_SMAATO = 35;
    public static final int NETWORK_TYPE_SMART = 26;
    public static final int NETWORK_TYPE_VIDEOEGG = 3;
    public static final int NETWORK_TYPE_VPON = 30;
    public static final int NETWORK_TYPE_WINAD = 37;
    public static final int NETWORK_TYPE_WIYUN = 22;
    public static final int NETWORK_TYPE_WOOBOO = 23;
    public static final int NETWORK_TYPE_YOUMI = 24;
    public static final int NETWORK_TYPE_ZESTADZ = 20;
    public static final String VER = "1.0.8";
    public static final int VERSION = 281;
    private static double density = -1.0d;
    public static String urlClick = "http://clk.adsmogo.com/exclick.ashx?appid=%s&appver=%d&nid=%s&type=%d&uuid=%s&client=2&adtype=%s&country=%s%s";
    public static String urlConfig = "http://cfg.adsmogo.com/getinfo.ashx?appid=%s&appver=%d&v=%s&client=2&pn=%s&userver=%s&adtype=%s&country=%s&nt=%s&mno=%s&uuid=%s&os=%s&dn=%s&size=%s%s&ram=%s";
    public static String urlCustom = "http://cus.adsmogo.com/custom.ashx?appid=%s&nid=%s&uuid=%s&client=2&country=%s&adtype=%s";
    public static String urlExchange = "http://cus.adsmogo.com/Exchange.ashx?appid=%s&adtype=%s&country=%s&client=2";
    public static String urlImpression = "http://imp.adsmogo.com/exmet.ashx?appid=%s&nid=%s&type=%d&uuid=%s&client=2&adtype=%s&country=%s";
    public static String urlMogo = "http://www.adsmogo.com/adserv.php?appid=%s&nid=%s&type=%d&uuid=%s&client=2&adtype=%s&country=%s";
    public static String urlNull = "http://blk.adsmogo.com/blank.ashx?appid=%s&nid=%s&type=%d&uuid=%s&client=2&adtype=%s&country=%s";
    public static String urlRecordData = "http://cfg.adsmogo.com/batch.ashx";
    public static String urlRequest = "http://req.adsmogo.com/exrequest.ashx?appid=%s&nid=%s&type=%d&uuid=%s&client=2&adtype=%s&country=%s";

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v1, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v3, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v4, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String convertToHex(java.lang.String r8) {
        /*
            boolean r6 = android.text.TextUtils.isEmpty(r8)     // Catch:{ NoSuchAlgorithmException -> 0x005b }
            if (r6 != 0) goto L_0x0060
            java.lang.String r6 = "000000000000000"
            boolean r6 = r8.equals(r6)     // Catch:{ NoSuchAlgorithmException -> 0x005b }
            if (r6 == 0) goto L_0x0026
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ NoSuchAlgorithmException -> 0x005b }
            java.lang.String r7 = java.lang.String.valueOf(r8)     // Catch:{ NoSuchAlgorithmException -> 0x005b }
            r6.<init>(r7)     // Catch:{ NoSuchAlgorithmException -> 0x005b }
            java.lang.StringBuilder r6 = r6.append(r8)     // Catch:{ NoSuchAlgorithmException -> 0x005b }
            java.lang.String r7 = "00"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ NoSuchAlgorithmException -> 0x005b }
            java.lang.String r6 = r6.toString()     // Catch:{ NoSuchAlgorithmException -> 0x005b }
        L_0x0025:
            return r6
        L_0x0026:
            byte[] r5 = r8.getBytes()     // Catch:{ NoSuchAlgorithmException -> 0x005b }
            java.lang.String r6 = "MD5"
            java.security.MessageDigest r3 = java.security.MessageDigest.getInstance(r6)     // Catch:{ NoSuchAlgorithmException -> 0x005b }
            byte[] r5 = r3.digest(r5)     // Catch:{ NoSuchAlgorithmException -> 0x005b }
            java.lang.StringBuffer r0 = new java.lang.StringBuffer     // Catch:{ NoSuchAlgorithmException -> 0x005b }
            r0.<init>()     // Catch:{ NoSuchAlgorithmException -> 0x005b }
            r4 = 0
        L_0x003a:
            int r6 = r5.length     // Catch:{ NoSuchAlgorithmException -> 0x005b }
            if (r4 < r6) goto L_0x0042
            java.lang.String r6 = r0.toString()     // Catch:{ NoSuchAlgorithmException -> 0x005b }
            goto L_0x0025
        L_0x0042:
            byte r1 = r5[r4]     // Catch:{ NoSuchAlgorithmException -> 0x005b }
            if (r1 >= 0) goto L_0x0048
            int r1 = r1 + 256
        L_0x0048:
            r6 = 16
            if (r1 >= r6) goto L_0x0051
            java.lang.String r6 = "0"
            r0.append(r6)     // Catch:{ NoSuchAlgorithmException -> 0x005b }
        L_0x0051:
            java.lang.String r6 = java.lang.Integer.toHexString(r1)     // Catch:{ NoSuchAlgorithmException -> 0x005b }
            r0.append(r6)     // Catch:{ NoSuchAlgorithmException -> 0x005b }
            int r4 = r4 + 1
            goto L_0x003a
        L_0x005b:
            r6 = move-exception
            r2 = r6
            r2.printStackTrace()
        L_0x0060:
            java.lang.String r6 = ""
            goto L_0x0025
        */
        throw new UnsupportedOperationException("Method not decompiled: com.admogo.util.AdMogoUtil.convertToHex(java.lang.String):java.lang.String");
    }

    public static void changeURL(int index) {
        switch (index) {
            case 0:
                urlConfig = "http://cfg.adsmogo.com/getinfo.ashx?appid=%s&appver=%d&v=%s&client=2&pn=%s&userver=%s&adtype=%s&country=%s&nt=%s&mno=%s&uuid=%s&os=%s&dn=%s&size=%s%s&ram=%s";
                urlCustom = "http://cus.adsmogo.com/custom.ashx?appid=%s&nid=%s&uuid=%s&client=2&country=%s&adtype=%s";
                urlMogo = "http://www.adsmogo.com/adserv.php?appid=%s&nid=%s&type=%d&uuid=%s&client=2&adtype=%s&country=%s";
                urlExchange = "http://cus.adsmogo.com/Exchange.ashx?appid=%s&adtype=%s&country=%s&client=2";
                urlRecordData = "http://cfg.adsmogo.com/batch.ashx";
                urlImpression = "http://imp.adsmogo.com/exmet.ashx?appid=%s&nid=%s&type=%d&uuid=%s&client=2&adtype=%s&country=%s";
                urlClick = "http://clk.adsmogo.com/exclick.ashx?appid=%s&appver=%d&nid=%s&type=%d&uuid=%s&client=2&adtype=%s&country=%s%s";
                urlRequest = "http://req.adsmogo.com/exrequest.ashx?appid=%s&nid=%s&type=%d&uuid=%s&client=2&adtype=%s&country=%s";
                urlNull = "http://blk.adsmogo.com/blank.ashx?appid=%s&nid=%s&type=%d&uuid=%s&client=2&adtype=%s&country=%s";
                return;
            case 1:
                urlConfig = "http://cfg.adsmogo.org/getinfo.ashx?appid=%s&appver=%d&v=%s&client=2&pn=%s&userver=%s&adtype=%s&country=%s&nt=%s&mno=%s&uuid=%s&os=%s&dn=%s&size=%s%s&ram=%s";
                urlCustom = "http://cus.adsmogo.org/custom.ashx?appid=%s&nid=%s&uuid=%s&client=2&country=%s&adtype=%s";
                urlMogo = "http://www.adsmogo.org/adserv.php?appid=%s&nid=%s&type=%d&uuid=%s&client=2&adtype=%s&country=%s";
                urlExchange = "http://cus.adsmogo.org/Exchange.ashx?appid=%s&adtype=%s&country=%s&client=2";
                urlRecordData = "http://cfg.adsmogo.org/batch.ashx";
                urlImpression = "http://imp.adsmogo.org/exmet.ashx?appid=%s&nid=%s&type=%d&uuid=%s&client=2&adtype=%s&country=%s";
                urlClick = "http://clk.adsmogo.org/exclick.ashx?appid=%s&appver=%d&nid=%s&type=%d&uuid=%s&client=2&adtype=%s&country=%s%s";
                urlRequest = "http://req.adsmogo.org/exrequest.ashx?appid=%s&nid=%s&type=%d&uuid=%s&client=2&adtype=%s&country=%s";
                urlNull = "http://blk.adsmogo.org/blank.ashx?appid=%s&nid=%s&type=%d&uuid=%s&client=2&adtype=%s&country=%s";
                return;
            case 2:
                urlConfig = "http://cfg.adsmogo.net/getinfo.ashx?appid=%s&appver=%d&v=%s&client=2&pn=%s&userver=%s&adtype=%s&country=%s&nt=%s&mno=%s&uuid=%s&os=%s&dn=%s&size=%s%s&ram=%s";
                urlCustom = "http://cus.adsmogo.net/custom.ashx?appid=%s&nid=%s&uuid=%s&client=2&country=%s&adtype=%s";
                urlMogo = "http://www.adsmogo.net/adserv.php?appid=%s&nid=%s&type=%d&uuid=%s&client=2&adtype=%s&country=%s";
                urlExchange = "http://cus.adsmogo.net/Exchange.ashx?appid=%s&adtype=%s&country=%s&client=2";
                urlRecordData = "http://cfg.adsmogo.net/batch.ashx";
                urlImpression = "http://imp.adsmogo.net/exmet.ashx?appid=%s&nid=%s&type=%d&uuid=%s&client=2&adtype=%s&country=%s";
                urlClick = "http://clk.adsmogo.net/exclick.ashx?appid=%s&appver=%d&nid=%s&type=%d&uuid=%s&client=2&adtype=%s&country=%s%s";
                urlRequest = "http://req.adsmogo.net/exrequest.ashx?appid=%s&nid=%s&type=%d&uuid=%s&client=2&adtype=%s&country=%s";
                urlNull = "http://blk.adsmogo.net/blank.ashx?appid=%s&nid=%s&type=%d&uuid=%s&client=2&adtype=%s&country=%s";
                return;
            case 3:
                urlConfig = "http://cfg.adsmogo.mobi/getinfo.ashx?appid=%s&appver=%d&v=%s&client=2&pn=%s&userver=%s&adtype=%s&country=%s&nt=%s&mno=%s&uuid=%s&os=%s&dn=%s&size=%s%s&ram=%s";
                urlCustom = "http://cus.adsmogo.mobi/custom.ashx?appid=%s&nid=%s&uuid=%s&client=2&country=%s&adtype=%s";
                urlMogo = "http://www.adsmogo.mobi/adserv.php?appid=%s&nid=%s&type=%d&uuid=%s&client=2&adtype=%s&country=%s";
                urlExchange = "http://cus.adsmogo.mobi/Exchange.ashx?appid=%s&adtype=%s&country=%s&client=2";
                urlRecordData = "http://cfg.adsmogo.mobi/batch.ashx";
                urlImpression = "http://imp.adsmogo.mobi/exmet.ashx?appid=%s&nid=%s&type=%d&uuid=%s&client=2&adtype=%s&country=%s";
                urlClick = "http://clk.adsmogo.mobi/exclick.ashx?appid=%s&appver=%d&nid=%s&type=%d&uuid=%s&client=2&adtype=%s&country=%s%s";
                urlRequest = "http://req.adsmogo.mobi/exrequest.ashx?appid=%s&nid=%s&type=%d&uuid=%s&client=2&adtype=%s&country=%s";
                urlNull = "http://blk.adsmogo.mobi/blank.ashx?appid=%s&nid=%s&type=%d&uuid=%s&client=2&adtype=%s&country=%s";
                return;
            default:
                return;
        }
    }

    public static double getDensity(Activity activity) {
        if (density == -1.0d) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            density = (double) displayMetrics.density;
        }
        return density;
    }

    public static int convertToScreenPixels(int dipPixels, double density2) {
        return (int) convertToScreenPixels((double) dipPixels, density2);
    }

    public static double convertToScreenPixels(double dipPixels, double density2) {
        return density2 > 0.0d ? dipPixels * density2 : dipPixels;
    }
}
