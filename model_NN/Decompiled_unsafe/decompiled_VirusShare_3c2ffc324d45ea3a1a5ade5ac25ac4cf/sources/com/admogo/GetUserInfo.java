package com.admogo;

import android.app.ActivityManager;
import android.content.Context;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import com.admogo.util.AdMogoUtil;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Locale;

public class GetUserInfo {
    public static String getDeviceID(Context context) {
        TelephonyManager tm = (TelephonyManager) context.getSystemService("phone");
        String tmDevice = "";
        try {
            if (isPermission(context, "android.permission.READ_PHONE_STATE")) {
                tmDevice = tm.getDeviceId();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (TextUtils.isEmpty(tmDevice)) {
            Log.w(AdMogoUtil.ADMOGO, "No IMEI");
            tmDevice = getIDByMAC(context);
            if (tmDevice == null) {
                Log.w(AdMogoUtil.ADMOGO, "Failed to take mac as IMEI");
                return "";
            }
        }
        return tmDevice;
    }

    public static boolean isPermission(Context context, String paramString) {
        return context.getPackageManager().checkPermission(paramString, context.getPackageName()) == 0;
    }

    private static String getIDByMAC(Context context) {
        try {
            return ((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getMacAddress();
        } catch (Exception e) {
            Log.i(AdMogoUtil.ADMOGO, "Could not read MAC, forget to include ACCESS_WIFI_STATE permission?", e);
            return null;
        }
    }

    public static String getNetworkType(Context context) {
        NetworkInfo networkinfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (networkinfo == null) {
            return "0";
        }
        String networkType = networkinfo.getTypeName();
        if (networkType.equalsIgnoreCase("mobile")) {
            return "1";
        }
        if (networkType.equalsIgnoreCase("wifi")) {
            return "2";
        }
        return networkType;
    }

    public static String getOperators(Context cx) {
        String mno = ((TelephonyManager) cx.getSystemService("phone")).getSimOperator();
        if (mno.equals("") || mno == null) {
            mno = "00000";
        }
        return mno;
    }

    public static String GetCPUInfo() {
        try {
            String[] cpuTotalInfo = run(new String[]{"/system/bin/cat", "/proc/cpuinfo"}, "/system/bin/").split("\n");
            int cpuProcessorCount = 0;
            String cpuMIPS = "";
            for (String split : cpuTotalInfo) {
                String[] cpuInfo = split.split("\t: ");
                if (cpuInfo[0].equals("processor")) {
                    cpuProcessorCount = Integer.parseInt(cpuInfo[1]);
                }
                if (cpuInfo[0].equals("BogoMIPS")) {
                    cpuMIPS = cpuInfo[1];
                }
            }
            return "&cc=" + (cpuProcessorCount + 1) + "&cm=" + cpuMIPS;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getMemoryInfo(Context context) {
        ((ActivityManager) context.getSystemService("activity")).getMemoryInfo(new ActivityManager.MemoryInfo());
        try {
            return run(new String[]{"/system/bin/cat", "/proc/meminfo"}, "/system/bin/").split("\n")[0].split(":")[1].replace("\t", "").replace(" ", "").toLowerCase();
        } catch (IOException e) {
            Log.i("fetch_process_info", "ex=" + e.toString());
            return "";
        }
    }

    public static synchronized String run(String[] cmd, String workdirectory) throws IOException {
        String result;
        synchronized (GetUserInfo.class) {
            result = "";
            try {
                ProcessBuilder builder = new ProcessBuilder(cmd);
                if (workdirectory != null) {
                    builder.directory(new File(workdirectory));
                }
                builder.redirectErrorStream(true);
                InputStream in = builder.start().getInputStream();
                byte[] re = new byte[1024];
                while (in.read(re) != -1) {
                    result = String.valueOf(result) + new String(re);
                }
                in.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    /* JADX INFO: Multiple debug info for r0v2 android.location.LocationManager: [D('contextString' java.lang.String), D('locationManager' android.location.LocationManager)] */
    /* JADX INFO: Multiple debug info for r1v1 java.lang.String: [D('provider' java.lang.String), D('criteria' android.location.Criteria)] */
    /* JADX INFO: Multiple debug info for r8v1 java.util.List<android.location.Address>: [D('context' android.content.Context), D('addresses' java.util.List<android.location.Address>)] */
    /* JADX INFO: Multiple debug info for r0v5 java.lang.String: [D('sb' java.lang.StringBuilder), D('locationManager' android.location.LocationManager)] */
    /* JADX INFO: Multiple debug info for r8v5 android.location.Address: [D('address' android.location.Address), D('addresses' java.util.List<android.location.Address>)] */
    public static String getCityName(Context context) throws IOException {
        String cityName;
        LocationManager locationManager = (LocationManager) context.getSystemService("location");
        Criteria criteria = new Criteria();
        criteria.setAccuracy(1);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setCostAllowed(false);
        criteria.setPowerRequirement(1);
        String provider = locationManager.getBestProvider(criteria, true);
        if (provider == null) {
            return null;
        }
        Location location = locationManager.getLastKnownLocation(provider);
        while (location == null) {
            locationManager.requestLocationUpdates("gps", 2000, 1.0f, new LocationListener() {
                public void onLocationChanged(Location location) {
                }

                public void onProviderDisabled(String provider) {
                }

                public void onProviderEnabled(String provider) {
                }

                public void onStatusChanged(String provider, int status, Bundle extras) {
                }
            });
        }
        List<Address> addresses = new Geocoder(context, Locale.CHINA).getFromLocation(location.getLatitude(), location.getLongitude(), 1);
        StringBuilder sb = new StringBuilder();
        if (addresses.size() > 0) {
            sb.append(addresses.get(0).getLocality());
            cityName = sb.toString();
        } else {
            cityName = "";
        }
        return cityName;
    }
}
