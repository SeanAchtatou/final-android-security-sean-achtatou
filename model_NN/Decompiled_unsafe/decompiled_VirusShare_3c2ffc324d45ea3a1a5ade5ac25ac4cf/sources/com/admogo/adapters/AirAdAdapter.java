package com.admogo.adapters;

import android.app.Activity;
import android.util.Log;
import com.admogo.AdMogoLayout;
import com.admogo.obj.Ration;
import com.admogo.util.AdMogoUtil;
import com.mt.airad.AirAD;

public class AirAdAdapter extends AdMogoAdapter implements AirAD.AdListener {
    private Activity activity;
    private AirAD adView;

    public AirAdAdapter(AdMogoLayout adMogoLayout, Ration ration) {
        super(adMogoLayout, ration);
    }

    public void handle() {
        AdMogoLayout adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
        if (adMogoLayout != null) {
            this.activity = adMogoLayout.activityReference.get();
            if (this.activity != null) {
                try {
                    AirAD.setGlobalParameter(this.ration.key, this.ration.testmodel);
                    if (adMogoLayout.getAdType() == 1) {
                        this.adView = new AirAD(this.activity);
                        this.adView.setAdListener(this);
                        adMogoLayout.addView(this.adView);
                        return;
                    }
                    adMogoLayout.getAdType();
                } catch (Exception e) {
                    adMogoLayout.rollover();
                }
            }
        }
    }

    public void onAdBannerClicked() {
        Log.d(AdMogoUtil.ADMOGO, "AirAD onClick");
    }

    public void onAdBannerReceive() {
        AdMogoLayout adMogoLayout;
        Log.d(AdMogoUtil.ADMOGO, "AirAD success");
        this.adView.setAdListener(null);
        if (!this.activity.isFinishing() && (adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout.adMogoManager.resetRollover();
            adMogoLayout.handler.post(new AdMogoLayout.ViewAdRunnable(adMogoLayout, this.adView, 32));
            adMogoLayout.rotateThreadedDelayed();
        }
    }

    public void onAdBannerReceiveFailed() {
        AdMogoLayout adMogoLayout;
        Log.d(AdMogoUtil.ADMOGO, "AirAD failed");
        this.adView.setAdListener(null);
        if (!this.activity.isFinishing() && (adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout.rollover();
        }
    }

    public void onMultiAdDismiss() {
        Log.d(AdMogoUtil.ADMOGO, "AirAD dismiss");
    }

    public void finish() {
        Log.d(AdMogoUtil.ADMOGO, "AirAD finish");
    }
}
