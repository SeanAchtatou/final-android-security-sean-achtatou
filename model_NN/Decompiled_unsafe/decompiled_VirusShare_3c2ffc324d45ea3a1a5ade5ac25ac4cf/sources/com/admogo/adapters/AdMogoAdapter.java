package com.admogo.adapters;

import android.util.Log;
import com.admogo.AdMogoLayout;
import com.admogo.obj.Ration;
import com.admogo.util.AdMogoUtil;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import org.json.JSONException;

public abstract class AdMogoAdapter {
    static AdMogoAdapter adapter;
    protected final WeakReference<AdMogoLayout> adMogoLayoutReference;
    protected Ration ration;

    public abstract void finish();

    public abstract void handle();

    public AdMogoAdapter(AdMogoLayout adMogoLayout, Ration ration2) {
        this.adMogoLayoutReference = new WeakReference<>(adMogoLayout);
        this.ration = ration2;
    }

    private static AdMogoAdapter getAdapter(AdMogoLayout adMogoLayout, Ration ration2) {
        try {
            switch (ration2.type) {
                case 1:
                    if (Class.forName("com.google.ads.AdView") != null) {
                        return getNetworkAdapter("com.admogo.adapters.GoogleAdMobAdsAdapter", adMogoLayout, ration2);
                    }
                    return unknownAdNetwork(adMogoLayout, ration2);
                case 2:
                case 3:
                case 4:
                case 5:
                case 8:
                case AdMogoUtil.NETWORK_TYPE_ADMOGO /*10*/:
                case 11:
                case AdMogoUtil.NETWORK_TYPE_4THSCREEN /*13*/:
                case AdMogoUtil.NETWORK_TYPE_ADSENSE /*14*/:
                case AdMogoUtil.NETWORK_TYPE_DOUBLECLICK /*15*/:
                case AdMogoUtil.NETWORK_TYPE_GENERIC /*16*/:
                case AdMogoUtil.NETWORK_TYPE_EVENT /*17*/:
                case 19:
                case 31:
                case 38:
                case 39:
                case 41:
                case 42:
                case 43:
                default:
                    return unknownAdNetwork(adMogoLayout, ration2);
                case 6:
                    if (Class.forName("com.millennialmedia.android.MMAdView") != null) {
                        return getNetworkAdapter("com.admogo.adapters.MillennialAdapter", adMogoLayout, ration2);
                    }
                    return unknownAdNetwork(adMogoLayout, ration2);
                case 7:
                    if (Class.forName("com.greystripe.android.sdk.BannerView") != null) {
                        return getNetworkAdapter("com.admogo.adapters.GreystripeAdapter", adMogoLayout, ration2);
                    }
                    return unknownAdNetwork(adMogoLayout, ration2);
                case AdMogoUtil.NETWORK_TYPE_CUSTOM /*9*/:
                    return new CustomAdapter(adMogoLayout, ration2);
                case AdMogoUtil.NETWORK_TYPE_MDOTM /*12*/:
                    return getNetworkAdapter("com.admogo.adapters.MdotMAdapter", adMogoLayout, ration2);
                case AdMogoUtil.NETWORK_TYPE_INMOBI /*18*/:
                    if (Class.forName("com.inmobi.androidsdk.impl.InMobiAdView") != null) {
                        return getNetworkAdapter("com.admogo.adapters.InMobiAdapter", adMogoLayout, ration2);
                    }
                    return unknownAdNetwork(adMogoLayout, ration2);
                case AdMogoUtil.NETWORK_TYPE_ZESTADZ /*20*/:
                    if (Class.forName("com.zestadz.android.ZestADZAdView") != null) {
                        return getNetworkAdapter("com.admogo.adapters.ZestAdzAdapter", adMogoLayout, ration2);
                    }
                    return unknownAdNetwork(adMogoLayout, ration2);
                case AdMogoUtil.NETWORK_TYPE_ADCHINA /*21*/:
                    if (Class.forName("com.adchina.android.ads.views.AdView") != null) {
                        return getNetworkAdapter("com.admogo.adapters.AdChinaAdapter", adMogoLayout, ration2);
                    }
                    return unknownAdNetwork(adMogoLayout, ration2);
                case AdMogoUtil.NETWORK_TYPE_WIYUN /*22*/:
                    if (Class.forName("com.wiyun.ad.AdView") != null) {
                        return getNetworkAdapter("com.admogo.adapters.WiyunAdapter", adMogoLayout, ration2);
                    }
                    return unknownAdNetwork(adMogoLayout, ration2);
                case AdMogoUtil.NETWORK_TYPE_WOOBOO /*23*/:
                    if (Class.forName("com.wooboo.adlib_android.WoobooAdView") != null) {
                        return getNetworkAdapter("com.admogo.adapters.WoobooAdapter", adMogoLayout, ration2);
                    }
                    return unknownAdNetwork(adMogoLayout, ration2);
                case AdMogoUtil.NETWORK_TYPE_YOUMI /*24*/:
                    if (Class.forName("net.youmi.android.AdView") != null) {
                        return getNetworkAdapter("com.admogo.adapters.YoumiAdapter", adMogoLayout, ration2);
                    }
                    return unknownAdNetwork(adMogoLayout, ration2);
                case AdMogoUtil.NETWORK_TYPE_CASEE /*25*/:
                    if (Class.forName("com.casee.adsdk.CaseeAdView") != null) {
                        return getNetworkAdapter("com.admogo.adapters.CaseeAdapter", adMogoLayout, ration2);
                    }
                    return unknownAdNetwork(adMogoLayout, ration2);
                case AdMogoUtil.NETWORK_TYPE_SMART /*26*/:
                    if (Class.forName("com.madhouse.android.ads.AdView") != null) {
                        return getNetworkAdapter("com.admogo.adapters.SmartMADAdapter", adMogoLayout, ration2);
                    }
                    return unknownAdNetwork(adMogoLayout, ration2);
                case AdMogoUtil.NETWORK_TYPE_MOGO /*27*/:
                    return new MogoAdapter(adMogoLayout, ration2);
                case AdMogoUtil.NETWORK_TYPE_ADTOUCH /*28*/:
                    if (Class.forName("com.energysource.szj.embeded.AdView") != null) {
                        return getNetworkAdapter("com.admogo.adapters.AdTouchAdapter", adMogoLayout, ration2);
                    }
                    return unknownAdNetwork(adMogoLayout, ration2);
                case AdMogoUtil.NETWORK_TYPE_DOMOB /*29*/:
                    if (Class.forName("cn.domob.android.ads.DomobAdView") != null) {
                        return getNetworkAdapter("com.admogo.adapters.DomobAdapter", adMogoLayout, ration2);
                    }
                    return unknownAdNetwork(adMogoLayout, ration2);
                case AdMogoUtil.NETWORK_TYPE_VPON /*30*/:
                    if (Class.forName("com.vpon.adon.android.AdView") != null) {
                        return getNetworkAdapter("com.admogo.adapters.VponCNAdapter", adMogoLayout, ration2);
                    }
                    return unknownAdNetwork(adMogoLayout, ration2);
                case AdMogoUtil.NETWORK_TYPE_AIRAD /*32*/:
                    if (Class.forName("com.mt.airad.AirAD") != null) {
                        return getNetworkAdapter("com.admogo.adapters.AirAdAdapter", adMogoLayout, ration2);
                    }
                    return unknownAdNetwork(adMogoLayout, ration2);
                case AdMogoUtil.NETWORK_TYPE_ADWO /*33*/:
                    if (Class.forName("com.adwo.adsdk.AdwoAdView") != null) {
                        return getNetworkAdapter("com.admogo.adapters.AdwoAdapter", adMogoLayout, ration2);
                    }
                    return unknownAdNetwork(adMogoLayout, ration2);
                case AdMogoUtil.NETWORK_TYPE_LSENSE /*34*/:
                    if (Class.forName("com.l.adlib_android.AdView") != null) {
                        return getNetworkAdapter("com.admogo.adapters.LSenseAdapter", adMogoLayout, ration2);
                    }
                    return unknownAdNetwork(adMogoLayout, ration2);
                case AdMogoUtil.NETWORK_TYPE_SMAATO /*35*/:
                    if (Class.forName("com.smaato.SOMA.SOMABanner") != null) {
                        return getNetworkAdapter("com.admogo.adapters.SmaatoAdapter", adMogoLayout, ration2);
                    }
                    return unknownAdNetwork(adMogoLayout, ration2);
                case AdMogoUtil.NETWORK_TYPE_APPMEDIA /*36*/:
                    if (Class.forName("cn.appmedia.ad.BannerAdView") != null) {
                        return getNetworkAdapter("com.admogo.adapters.AppMediaAdapter", adMogoLayout, ration2);
                    }
                    return unknownAdNetwork(adMogoLayout, ration2);
                case AdMogoUtil.NETWORK_TYPE_WINAD /*37*/:
                    if (Class.forName("com.winad.android.ads.AdView") != null) {
                        return getNetworkAdapter("com.admogo.adapters.WinAdAdapter", adMogoLayout, ration2);
                    }
                    return unknownAdNetwork(adMogoLayout, ration2);
                case AdMogoUtil.NETWORK_TYPE_IZP /*40*/:
                    if (Class.forName("com.izp.views.IZPView") != null) {
                        return getNetworkAdapter("com.admogo.adapters.IZPAdapter", adMogoLayout, ration2);
                    }
                    return unknownAdNetwork(adMogoLayout, ration2);
                case AdMogoUtil.NETWORK_TYPE_BAIDU /*44*/:
                    try {
                        return new BaiduJsonAdapter(adMogoLayout, ration2);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        break;
                    }
                case AdMogoUtil.NETWORK_TYPE_EXCHANGE /*45*/:
                    return new ExchangeAdapter(adMogoLayout, ration2);
            }
        } catch (ClassNotFoundException e2) {
            return unknownAdNetwork(adMogoLayout, ration2);
        } catch (VerifyError e3) {
            Log.e("AdMogo", "Caught VerifyError", e3);
            return unknownAdNetwork(adMogoLayout, ration2);
        }
    }

    private static AdMogoAdapter getNetworkAdapter(String networkAdapter, AdMogoLayout adMogoLayout, Ration ration2) {
        try {
            return (AdMogoAdapter) Class.forName(networkAdapter).getConstructor(AdMogoLayout.class, Ration.class).newInstance(adMogoLayout, ration2);
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | SecurityException | InvocationTargetException e) {
            return null;
        }
    }

    private static AdMogoAdapter unknownAdNetwork(AdMogoLayout adMogoLayout, Ration ration2) {
        Log.w(AdMogoUtil.ADMOGO, "Unsupported ration type: " + ration2.type);
        return null;
    }

    public static void handle(AdMogoLayout adMogoLayout, Ration ration2) throws Throwable {
        if (adapter != null) {
            adapter.finish();
        }
        adapter = getAdapter(adMogoLayout, ration2);
        if (adapter != null) {
            if (9 == ration2.type || 27 == ration2.type || 45 == ration2.type) {
                Log.d(AdMogoUtil.ADMOGO, "HTTP/1.1 200 OK");
            } else {
                adMogoLayout.countRequest();
            }
            Log.d(AdMogoUtil.ADMOGO, "Valid adapter, calling handle()");
            adapter.handle();
            return;
        }
        throw new Exception("Invalid adapter");
    }
}
