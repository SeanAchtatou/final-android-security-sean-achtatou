package com.admogo.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.admogo.AdMogoLayout;
import com.admogo.obj.Ration;
import com.admogo.util.AdMogoUtil;
import com.baidu.Ad;
import com.baidu.AdRequest;
import com.baidu.AdType;
import com.madhouse.android.ads.AdView;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class BaiduJsonAdapter extends AdMogoAdapter {
    static int AD_TYPE = 1;
    static String APP_ID = "debug";
    static String APP_SEC = "debug";
    public static Ad baiduAd = null;
    static List<Ad> imageAdList = null;
    static int imageIndex = 0;
    static int imageSize;
    static List<Ad> textAdList = null;
    static int textIndex = 0;
    static int textSize;
    Activity activity;
    private Drawable tempDrawable;

    public BaiduJsonAdapter(AdMogoLayout adMogoLayout, Ration ration) throws JSONException {
        super(adMogoLayout, ration);
        JSONObject jsonObject = new JSONObject(this.ration.key);
        APP_ID = jsonObject.getString("AppID");
        APP_SEC = jsonObject.getString("AppSEC");
    }

    public void handle() {
        AdMogoLayout adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
        if (adMogoLayout != null) {
            AD_TYPE = ((int) (Math.random() * 2.0d)) + 1;
            if (AD_TYPE == 1 && imageAdList == null) {
                adMogoLayout.handler.post(new FetchBaiduRunnable(this));
            } else if (AD_TYPE == 2 && textAdList == null) {
                adMogoLayout.handler.post(new FetchBaiduRunnable(this));
            } else {
                adMogoLayout.handler.post(new DisplayBaiduRunnable(this));
            }
        }
    }

    public void displayBaidu() {
        AdMogoLayout adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
        if (adMogoLayout != null) {
            this.activity = adMogoLayout.activityReference.get();
            if (this.activity != null) {
                double density = AdMogoUtil.getDensity(this.activity);
                double px320 = (double) AdMogoUtil.convertToScreenPixels((int) AdView.PHONE_AD_MEASURE_320, density);
                double px50 = (double) AdMogoUtil.convertToScreenPixels(50, density);
                switch (AD_TYPE) {
                    case 1:
                        RelativeLayout relativeLayout = new RelativeLayout(this.activity);
                        imageSize = imageAdList.size() - 1;
                        if (imageSize >= 0) {
                            baiduAd = imageAdList.get(imageIndex);
                            if (imageIndex < imageSize) {
                                imageIndex++;
                            } else {
                                adMogoLayout.handler.post(new UpdateBaiduRunnable(this));
                                imageIndex = 0;
                            }
                            ImageView imageView = new ImageView(this.activity);
                            imageView.setImageDrawable(fetchImage(baiduAd.getPicUrl()));
                            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) px320, (int) px50);
                            layoutParams.addRule(13);
                            relativeLayout.addView(imageView, layoutParams);
                            adMogoLayout.pushSubView(relativeLayout, 44);
                            break;
                        } else {
                            adMogoLayout.rollover();
                            return;
                        }
                    case 2:
                        RelativeLayout relativeLayout2 = new RelativeLayout(this.activity);
                        textSize = textAdList.size() - 1;
                        if (textSize >= 0) {
                            baiduAd = textAdList.get(textIndex);
                            if (textIndex < textSize) {
                                textIndex++;
                            } else {
                                adMogoLayout.handler.post(new UpdateBaiduRunnable(this));
                                textIndex = 0;
                            }
                            relativeLayout2.setLayoutParams(new FrameLayout.LayoutParams((int) px320, (int) px50));
                            ImageView imageView2 = new ImageView(this.activity);
                            int backgroundColor = Color.rgb(adMogoLayout.extra.bgRed, adMogoLayout.extra.bgGreen, adMogoLayout.extra.bgBlue);
                            imageView2.setBackgroundDrawable(new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{Color.argb(180, 255, 255, 255), backgroundColor, backgroundColor, backgroundColor}));
                            relativeLayout2.addView(imageView2, new RelativeLayout.LayoutParams(-1, -1));
                            ImageView imageView3 = new ImageView(this.activity);
                            imageView3.setImageDrawable(fetchImage(baiduAd.getPicUrl()));
                            imageView3.setId(10);
                            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(38, 38);
                            layoutParams2.addRule(15);
                            layoutParams2.setMargins(4, 0, 4, 0);
                            relativeLayout2.addView(imageView3, layoutParams2);
                            TextView textView = new TextView(this.activity);
                            textView.setTextSize(16.0f);
                            textView.setText(String.valueOf(baiduAd.getTitle()) + "-" + baiduAd.getDescription());
                            textView.setTypeface(Typeface.DEFAULT_BOLD, 1);
                            textView.setTextColor(Color.rgb(adMogoLayout.extra.fgRed, adMogoLayout.extra.fgGreen, adMogoLayout.extra.fgBlue));
                            RelativeLayout.LayoutParams textViewParams = new RelativeLayout.LayoutParams(-1, -1);
                            textViewParams.addRule(1, imageView3.getId());
                            textViewParams.addRule(10);
                            textViewParams.addRule(12);
                            textViewParams.addRule(15);
                            textViewParams.addRule(13);
                            textView.setGravity(16);
                            relativeLayout2.addView(textView, textViewParams);
                            adMogoLayout.pushSubView(relativeLayout2, 44);
                            break;
                        } else {
                            adMogoLayout.rollover();
                            return;
                        }
                    default:
                        Log.w(AdMogoUtil.ADMOGO, "Unknown baidu type!");
                        adMogoLayout.rotateThreadedNow();
                        return;
                }
                this.tempDrawable = null;
                adMogoLayout.adMogoManager.resetRollover();
                adMogoLayout.rotateThreadedDelayed();
            }
        }
    }

    private static class FetchBaiduRunnable implements Runnable {
        private BaiduJsonAdapter baiduAdapter;

        public FetchBaiduRunnable(BaiduJsonAdapter baiduAdapter2) {
            this.baiduAdapter = baiduAdapter2;
        }

        public void run() {
            AdMogoLayout adMogoLayout = (AdMogoLayout) this.baiduAdapter.adMogoLayoutReference.get();
            Activity activity = adMogoLayout.activityReference.get();
            switch (BaiduJsonAdapter.AD_TYPE) {
                case 1:
                    Log.d(AdMogoUtil.ADMOGO, "Serving baidu type: image");
                    BaiduJsonAdapter.imageAdList = AdRequest.getAds(activity.getApplicationContext(), AdType.IMAGE, BaiduJsonAdapter.APP_ID, BaiduJsonAdapter.APP_SEC);
                    break;
                case 2:
                    Log.d(AdMogoUtil.ADMOGO, "Serving baidu type: text");
                    BaiduJsonAdapter.textAdList = AdRequest.getAds(activity.getApplicationContext(), AdType.TEXT, BaiduJsonAdapter.APP_ID, BaiduJsonAdapter.APP_SEC);
                    break;
                default:
                    Log.w(AdMogoUtil.ADMOGO, "Unknown baidu type!");
                    adMogoLayout.rotateThreadedNow();
                    return;
            }
            adMogoLayout.handler.postDelayed(new DisplayBaiduRunnable(this.baiduAdapter), 3000);
        }
    }

    private static class UpdateBaiduRunnable implements Runnable {
        private BaiduJsonAdapter baiduAdapter;

        public UpdateBaiduRunnable(BaiduJsonAdapter baiduAdapter2) {
            this.baiduAdapter = baiduAdapter2;
        }

        public void run() {
            AdMogoLayout adMogoLayout = (AdMogoLayout) this.baiduAdapter.adMogoLayoutReference.get();
            Activity activity = adMogoLayout.activityReference.get();
            switch (BaiduJsonAdapter.AD_TYPE) {
                case 1:
                    Log.d(AdMogoUtil.ADMOGO, "Serving baidu type: image");
                    BaiduJsonAdapter.imageAdList = AdRequest.getAds(activity.getApplicationContext(), AdType.IMAGE, BaiduJsonAdapter.APP_ID, BaiduJsonAdapter.APP_SEC);
                    return;
                case 2:
                    Log.d(AdMogoUtil.ADMOGO, "Serving baidu type: text");
                    BaiduJsonAdapter.textAdList = AdRequest.getAds(activity.getApplicationContext(), AdType.TEXT, BaiduJsonAdapter.APP_ID, BaiduJsonAdapter.APP_SEC);
                    return;
                default:
                    Log.w(AdMogoUtil.ADMOGO, "Unknown baidu type!");
                    adMogoLayout.rotateThreadedNow();
                    return;
            }
        }
    }

    private static class DisplayBaiduRunnable implements Runnable {
        private BaiduJsonAdapter baiduAdapter;

        public DisplayBaiduRunnable(BaiduJsonAdapter baiduAdapter2) {
            this.baiduAdapter = baiduAdapter2;
        }

        public void run() {
            this.baiduAdapter.displayBaidu();
        }
    }

    private Drawable fetchImage(String urlString) {
        try {
            InputStream is = (InputStream) new URL(urlString).getContent();
            Drawable d = Drawable.createFromStream(is, "src");
            is.close();
            return d;
        } catch (Exception e) {
            Log.w("Baidu", "Unable to fetchImage()");
            this.tempDrawable = new BitmapDrawable(getClass().getResourceAsStream("/com/admogo/assets/baidu_icon.png"));
            return this.tempDrawable;
        }
    }

    public void finish() {
        Log.d(AdMogoUtil.ADMOGO, "Baidu Finished");
    }
}
