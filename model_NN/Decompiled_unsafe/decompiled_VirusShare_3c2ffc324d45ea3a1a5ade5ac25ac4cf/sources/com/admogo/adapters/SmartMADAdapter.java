package com.admogo.adapters;

import android.app.Activity;
import android.util.Log;
import android.view.ViewGroup;
import com.admogo.AdMogoLayout;
import com.admogo.AdMogoTargeting;
import com.admogo.obj.Ration;
import com.admogo.util.AdMogoUtil;
import com.madhouse.android.ads.AdListener;
import com.madhouse.android.ads.AdManager;
import com.madhouse.android.ads.AdView;
import org.json.JSONException;
import org.json.JSONObject;

public class SmartMADAdapter extends AdMogoAdapter implements AdListener {
    private String AdSpaceID = null;
    private String AppID = null;
    private Activity activity;
    private AdView adView;

    public SmartMADAdapter(AdMogoLayout adMogoLayout, Ration ration) throws JSONException {
        super(adMogoLayout, ration);
        JSONObject jsonObject = new JSONObject(this.ration.key);
        this.AppID = jsonObject.getString("AppID");
        this.AdSpaceID = jsonObject.getString("AdSpaceID");
    }

    public void handle() {
        AdMogoLayout adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
        if (adMogoLayout != null) {
            if (adMogoLayout.getChildAt(0) != null && (adMogoLayout.getChildAt(0) instanceof AdView)) {
                adMogoLayout.removeAllViews();
            }
            this.activity = adMogoLayout.activityReference.get();
            if (this.activity != null) {
                AdMogoTargeting.Gender gender = AdMogoTargeting.getGender();
                if (gender != null) {
                    AdManager.setUserGender(gender.toString());
                }
                if (AdMogoTargeting.getBirthDate() != null) {
                    AdManager.setUserAge(String.valueOf(AdMogoTargeting.getAge()));
                }
                try {
                    AdManager.setApplicationId(this.activity, this.AppID);
                    this.adView = new AdView(this.activity, null, 0, this.AdSpaceID, 600, 0, 2, this.ration.testmodel);
                    this.adView.setListener(this);
                    adMogoLayout.addView(this.adView, new ViewGroup.LayoutParams(-2, -2));
                } catch (IllegalArgumentException e) {
                    adMogoLayout.rollover();
                }
            }
        }
    }

    public void onAdEvent(AdView adView2, int event) {
        Log.d(AdMogoUtil.ADMOGO, "SmartMAD on Ad Status");
        adView2.setListener(null);
        AdMogoLayout adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
        if (adMogoLayout != null) {
            if (this.activity.isFinishing()) {
                adView2.setVisibility(8);
            } else if (event == 1) {
                Log.d(AdMogoUtil.ADMOGO, "Smart success");
                if (adMogoLayout.getAdType() == 6) {
                    adMogoLayout.adMogoManager.resetRollover();
                    adMogoLayout.handler.post(new AdMogoLayout.ViewAdRunnable(adMogoLayout, adView2, 26));
                } else if (adMogoLayout.getAdType() != 7) {
                    adMogoLayout.adMogoManager.resetRollover();
                    adMogoLayout.handler.post(new AdMogoLayout.ViewAdRunnable(adMogoLayout, adView2, 26));
                    adMogoLayout.rotateThreadedDelayed();
                }
            } else {
                Log.d(AdMogoUtil.ADMOGO, "Smart failure");
                adMogoLayout.rollover();
            }
        }
    }

    public void onAdFullScreenStatus(boolean status) {
        Log.d(AdMogoUtil.ADMOGO, "SmartMAD on Ad FullScreenStatus");
    }

    public void finish() {
        this.adView = null;
        Log.d(AdMogoUtil.ADMOGO, "SmartMAD Finished");
    }
}
