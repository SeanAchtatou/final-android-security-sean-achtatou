package com.admogo.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.admogo.AdMogoLayout;
import com.admogo.obj.Ration;
import com.admogo.util.AdMogoUtil;
import com.madhouse.android.ads.AdView;
import java.util.concurrent.TimeUnit;

public class ExchangeAdapter extends AdMogoAdapter {
    public ExchangeAdapter(AdMogoLayout adMogoLayout, Ration ration) {
        super(adMogoLayout, ration);
    }

    public void handle() {
        AdMogoLayout adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
        if (adMogoLayout != null) {
            adMogoLayout.scheduler.schedule(new FetchExchangeRunnable(this), 0, TimeUnit.SECONDS);
        }
    }

    public void displayExchange() {
        Activity activity;
        AdMogoLayout adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
        if (adMogoLayout != null && (activity = adMogoLayout.activityReference.get()) != null) {
            double density = AdMogoUtil.getDensity(activity);
            double px320 = (double) AdMogoUtil.convertToScreenPixels((int) AdView.PHONE_AD_MEASURE_320, density);
            double px50 = (double) AdMogoUtil.convertToScreenPixels(50, density);
            double px42 = (double) AdMogoUtil.convertToScreenPixels(42, density);
            double px4 = (double) AdMogoUtil.convertToScreenPixels(4, density);
            switch (adMogoLayout.exchange.type) {
                case 1:
                    Log.d(AdMogoUtil.ADMOGO, "Serving exchange type: banner");
                    RelativeLayout bannerView = new RelativeLayout(activity);
                    if (adMogoLayout.exchange.image != null) {
                        ImageView bannerImageView = new ImageView(activity);
                        bannerImageView.setImageDrawable(adMogoLayout.exchange.image);
                        bannerImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) px320, (int) px50);
                        layoutParams.addRule(13);
                        bannerView.addView(bannerImageView, layoutParams);
                        adMogoLayout.pushSubView(bannerView, 45);
                        break;
                    } else {
                        adMogoLayout.rotateThreadedNow();
                        return;
                    }
                case 2:
                    Log.d(AdMogoUtil.ADMOGO, "Serving exchange type: icon");
                    RelativeLayout relativeLayout = new RelativeLayout(activity);
                    if (adMogoLayout.exchange.image != null) {
                        relativeLayout.setLayoutParams(new FrameLayout.LayoutParams((int) px320, (int) px50));
                        ImageView blendView = new ImageView(activity);
                        int backgroundColor = Color.rgb(adMogoLayout.extra.bgRed, adMogoLayout.extra.bgGreen, adMogoLayout.extra.bgBlue);
                        blendView.setBackgroundDrawable(new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-1, backgroundColor, backgroundColor, backgroundColor}));
                        relativeLayout.addView(blendView, new RelativeLayout.LayoutParams((int) px320, (int) px50));
                        ImageView imageView = new ImageView(activity);
                        imageView.setImageDrawable(adMogoLayout.exchange.image);
                        imageView.setId(10);
                        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams((int) px42, (int) px42);
                        layoutParams2.setMargins((int) px4, (int) px4, (int) px4, (int) px4);
                        relativeLayout.addView(imageView, layoutParams2);
                        TextView textView = new TextView(activity);
                        try {
                            if (adMogoLayout.exchange.adText == null) {
                            }
                        } catch (Exception e) {
                            adMogoLayout.exchange.adText = "Haven't description!";
                        }
                        Log.v("adMogoLayout.exchange.adText", "aaa:" + adMogoLayout.exchange.adText);
                        textView.setText(adMogoLayout.exchange.adText);
                        textView.setTypeface(Typeface.DEFAULT_BOLD, 1);
                        textView.setTextColor(Color.rgb(adMogoLayout.extra.fgRed, adMogoLayout.extra.fgGreen, adMogoLayout.extra.fgBlue));
                        RelativeLayout.LayoutParams textViewParams = new RelativeLayout.LayoutParams(-1, -1);
                        textViewParams.addRule(1, imageView.getId());
                        textViewParams.addRule(10);
                        textViewParams.addRule(12);
                        textViewParams.addRule(15);
                        textViewParams.addRule(13);
                        textView.setGravity(16);
                        relativeLayout.addView(textView, textViewParams);
                        adMogoLayout.pushSubView(relativeLayout, 45);
                        break;
                    } else {
                        adMogoLayout.rotateThreadedNow();
                        return;
                    }
                case 3:
                    Log.d(AdMogoUtil.ADMOGO, "Serving exchange type: full");
                    RelativeLayout relativeLayout2 = new RelativeLayout(activity);
                    relativeLayout2.setBackgroundColor(-16777216);
                    if (adMogoLayout.exchange.image != null) {
                        ImageView imageView2 = new ImageView(activity);
                        imageView2.setImageDrawable(adMogoLayout.exchange.image);
                        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, -2);
                        layoutParams3.addRule(13);
                        ImageButton closeBtn = new ImageButton(activity);
                        closeBtn.setBackgroundResource(17301533);
                        relativeLayout2.addView(imageView2, layoutParams3);
                        relativeLayout2.addView(closeBtn);
                        final RelativeLayout relativeLayout3 = relativeLayout2;
                        closeBtn.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View view) {
                                relativeLayout3.setVisibility(8);
                            }
                        });
                        adMogoLayout.pushSubView(relativeLayout2, 45);
                        break;
                    } else {
                        adMogoLayout.rotateThreadedNow();
                        return;
                    }
                default:
                    Log.w(AdMogoUtil.ADMOGO, "Unknown exchange type!");
                    adMogoLayout.rotateThreadedNow();
                    return;
            }
            adMogoLayout.exchange.image = null;
            adMogoLayout.adMogoManager.resetRollover();
            adMogoLayout.rotateThreadedDelayed();
        }
    }

    private static class FetchExchangeRunnable implements Runnable {
        private ExchangeAdapter exchangeAdapter;

        public FetchExchangeRunnable(ExchangeAdapter exchangeAdapter2) {
            this.exchangeAdapter = exchangeAdapter2;
        }

        public void run() {
            AdMogoLayout adMogoLayout = (AdMogoLayout) this.exchangeAdapter.adMogoLayoutReference.get();
            if (adMogoLayout != null) {
                adMogoLayout.exchange = adMogoLayout.adMogoManager.getExchange(this.exchangeAdapter.ration.nid);
                if (adMogoLayout.exchange == null) {
                    adMogoLayout.rollover();
                } else {
                    adMogoLayout.handler.post(new DisplayExchangeRunnable(this.exchangeAdapter));
                }
            }
        }
    }

    private static class DisplayExchangeRunnable implements Runnable {
        private ExchangeAdapter exchangeAdapter;

        public DisplayExchangeRunnable(ExchangeAdapter exchangeAdapter2) {
            this.exchangeAdapter = exchangeAdapter2;
        }

        public void run() {
            this.exchangeAdapter.displayExchange();
        }
    }

    public void finish() {
        Log.d(AdMogoUtil.ADMOGO, "Exchange Finished");
    }
}
