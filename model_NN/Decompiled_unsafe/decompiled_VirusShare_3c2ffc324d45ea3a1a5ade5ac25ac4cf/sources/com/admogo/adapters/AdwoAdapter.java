package com.admogo.adapters;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import com.admogo.AdMogoLayout;
import com.admogo.obj.Extra;
import com.admogo.obj.Ration;
import com.admogo.util.AdMogoUtil;
import com.adwo.adsdk.AdListener;
import com.adwo.adsdk.AdwoAdView;
import com.adwo.adsdk.AdwoSplashAdActivity;
import com.adwo.adsdk.FSAd;
import com.adwo.adsdk.FSAdUtil;
import com.adwo.adsdk.SplashAdListener;

public class AdwoAdapter extends AdMogoAdapter implements AdListener, SplashAdListener {
    static final int SPLASH_AD_REQUEST = 10;
    private Activity activity;
    private AdMogoLayout adMogoLayout;
    private FSAdUtil adUtil;
    private AdwoAdView adView;

    public AdwoAdapter(AdMogoLayout adMogoLayout2, Ration ration) {
        super(adMogoLayout2, ration);
    }

    public void handle() {
        this.adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
        if (this.adMogoLayout != null) {
            this.activity = this.adMogoLayout.activityReference.get();
            if (this.activity != null) {
                try {
                    if (this.adMogoLayout.getAdType() == 6) {
                        this.adUtil = FSAdUtil.getInstance();
                        this.adUtil.setSplashAdListener(this);
                        this.adUtil.loadAd(this.activity, this.ration.key, true);
                    } else if (this.adMogoLayout.getAdType() != 7) {
                        Extra extra = new Extra();
                        this.adView = new AdwoAdView(this.activity, this.ration.key, 4194432, Color.rgb(extra.bgRed, extra.bgGreen, extra.bgBlue), this.ration.testmodel, 0);
                        this.adView.setListener(this);
                        this.adMogoLayout.addView(this.adView);
                    }
                } catch (Exception e) {
                    this.adMogoLayout.rollover();
                }
            }
        }
    }

    public void onFailedToReceiveAd(AdwoAdView adView2) {
        AdMogoLayout adMogoLayout2;
        Log.d(AdMogoUtil.ADMOGO, "AdWo failure");
        adView2.setListener(null);
        if (!this.activity.isFinishing() && (adMogoLayout2 = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout2.rollover();
        }
    }

    public void onFailedToReceiveRefreshedAd(AdwoAdView adView2) {
        AdMogoLayout adMogoLayout2;
        Log.v(AdMogoUtil.ADMOGO, "AdWo onFailedToReceiveRefreshedAd");
        adView2.setListener(null);
        if (!this.activity.isFinishing() && (adMogoLayout2 = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout2.adMogoManager.resetRollover();
            adView2.finalize();
            adMogoLayout2.handler.post(new AdMogoLayout.ViewAdRunnable(adMogoLayout2, adView2, 33));
            adMogoLayout2.rotateThreadedDelayed();
        }
    }

    public void onReceiveAd(AdwoAdView adView2) {
        AdMogoLayout adMogoLayout2;
        Log.v(AdMogoUtil.ADMOGO, "AdWo onReceiveAd");
        adView2.setListener(null);
        if (!this.activity.isFinishing() && (adMogoLayout2 = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout2.adMogoManager.resetRollover();
            adView2.finalize();
            adMogoLayout2.handler.post(new AdMogoLayout.ViewAdRunnable(adMogoLayout2, adView2, 33));
            adMogoLayout2.rotateThreadedDelayed();
        }
    }

    public void onFailedToReceiveAd() {
        AdMogoLayout adMogoLayout2;
        Log.d(AdMogoUtil.ADMOGO, "AdWo FULLSCREEN failure");
        this.adUtil.setSplashAdListener(null);
        if (!this.activity.isFinishing() && (adMogoLayout2 = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout2.rollover();
        }
    }

    public void onReceiveAd(FSAd fsAd) {
        if (fsAd != null) {
            Intent splashAdsIntent = new Intent(this.activity, AdwoSplashAdActivity.class);
            splashAdsIntent.putExtra("FSAd", fsAd);
            this.activity.startActivityForResult(splashAdsIntent, 10);
        }
    }

    public void finish() {
        Log.d(AdMogoUtil.ADMOGO, "AdWo finish");
    }
}
