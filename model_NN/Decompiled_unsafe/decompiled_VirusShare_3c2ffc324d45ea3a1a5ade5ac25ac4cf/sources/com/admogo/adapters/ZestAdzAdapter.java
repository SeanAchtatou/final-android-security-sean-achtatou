package com.admogo.adapters;

import android.app.Activity;
import android.util.Log;
import com.admogo.AdMogoLayout;
import com.admogo.obj.Ration;
import com.admogo.util.AdMogoUtil;
import com.zestadz.android.AdManager;
import com.zestadz.android.ZestADZAdView;

public class ZestAdzAdapter extends AdMogoAdapter implements ZestADZAdView.ZestADZListener {
    private Activity activity;

    public ZestAdzAdapter(AdMogoLayout adMogoLayout, Ration ration) {
        super(adMogoLayout, ration);
    }

    public void handle() {
        AdMogoLayout adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
        if (adMogoLayout != null) {
            try {
                AdManager.setadclientId(this.ration.key);
                try {
                    this.activity = adMogoLayout.activityReference.get();
                    if (this.activity != null) {
                        ZestADZAdView adView = new ZestADZAdView(this.activity);
                        adView.setListener(this);
                        adView.displayAd();
                    }
                } catch (Exception e) {
                    adMogoLayout.rollover();
                }
            } catch (IllegalArgumentException e2) {
                adMogoLayout.rollover();
            }
        }
    }

    public void AdReturned(ZestADZAdView adView) {
        AdMogoLayout adMogoLayout;
        Log.d(AdMogoUtil.ADMOGO, "ZestADZ success");
        adView.setListener((ZestADZAdView.ZestADZListener) null);
        if (!this.activity.isFinishing() && (adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout.adMogoManager.resetRollover();
            adMogoLayout.handler.post(new AdMogoLayout.ViewAdRunnable(adMogoLayout, adView, 20));
            adMogoLayout.rotateThreadedDelayed();
        }
    }

    public void AdFailed(ZestADZAdView adView) {
        AdMogoLayout adMogoLayout;
        Log.d(AdMogoUtil.ADMOGO, "ZestADZ failure");
        adView.setListener((ZestADZAdView.ZestADZListener) null);
        if (!this.activity.isFinishing() && (adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout.rollover();
        }
    }

    public void finish() {
        Log.d(AdMogoUtil.ADMOGO, "ZestAdz Finished");
    }
}
