package com.admogo.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import cn.domob.android.ads.DomobAdListener;
import cn.domob.android.ads.DomobAdManager;
import cn.domob.android.ads.DomobAdView;
import com.admogo.AdMogoLayout;
import com.admogo.AdMogoTargeting;
import com.admogo.obj.Extra;
import com.admogo.obj.Ration;
import com.admogo.util.AdMogoUtil;
import com.madhouse.android.ads.AdManager;
import java.util.GregorianCalendar;

public class DomobAdapter extends AdMogoAdapter implements DomobAdListener {
    private Activity activity;
    private DomobAdView adView;

    public DomobAdapter(AdMogoLayout adMogoLayout, Ration ration) {
        super(adMogoLayout, ration);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: SimpleMethodDetails{com.admogo.AdMogoLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void}
     arg types: [cn.domob.android.ads.DomobAdView, android.view.ViewGroup$LayoutParams]
     candidates:
      ClspMth{android.view.ViewGroup.addView(android.view.View, int):void}
      SimpleMethodDetails{com.admogo.AdMogoLayout.addView(android.view.View, android.view.ViewGroup$LayoutParams):void} */
    public void handle() {
        boolean z;
        AdMogoLayout adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
        if (adMogoLayout != null) {
            this.activity = adMogoLayout.activityReference.get();
            if (this.activity != null) {
                this.adView = new DomobAdView(this.activity);
                Extra extra = adMogoLayout.extra;
                int bgColor = Color.rgb(extra.bgRed, extra.bgGreen, extra.bgBlue);
                int fgColor = Color.rgb(extra.fgRed, extra.fgGreen, extra.fgBlue);
                try {
                    DomobAdManager.setPublisherId(this.ration.key);
                    this.adView.setAdListener(this);
                    AdMogoTargeting.Gender gender = AdMogoTargeting.getGender();
                    if (gender == AdMogoTargeting.Gender.FEMALE) {
                        DomobAdManager.setGender(AdManager.USER_GENDER_FEMALE);
                    } else if (gender == AdMogoTargeting.Gender.MALE) {
                        DomobAdManager.setGender(AdManager.USER_GENDER_MALE);
                    }
                    GregorianCalendar birthDate = AdMogoTargeting.getBirthDate();
                    if (birthDate != null) {
                        DomobAdManager.setBirthday(birthDate);
                    }
                    String postalCode = AdMogoTargeting.getPostalCode();
                    if (!TextUtils.isEmpty(postalCode)) {
                        DomobAdManager.setPostalCode(postalCode);
                    }
                    if (extra.locationOn == 1) {
                        z = true;
                    } else {
                        z = false;
                    }
                    DomobAdManager.setAllowUseOfLocation(z);
                    this.adView.setBackgroundColor(bgColor);
                    this.adView.setPrimaryTextColor(fgColor);
                    this.adView.setKeywords(this.ration.key2);
                    DomobAdManager.setIsTestMode(this.ration.testmodel);
                    adMogoLayout.addView((View) this.adView, new ViewGroup.LayoutParams(-2, -2));
                    this.adView.requestFreshAd();
                    this.adView.setRequestInterval(0);
                } catch (IllegalArgumentException e) {
                    adMogoLayout.rollover();
                }
            }
        }
    }

    public void onFailedToReceiveFreshAd(DomobAdView adView2) {
        AdMogoLayout adMogoLayout;
        Log.d(AdMogoUtil.ADMOGO, "Domob Failed");
        adView2.setAdListener((DomobAdListener) null);
        if (!this.activity.isFinishing() && (adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout.rollover();
        }
    }

    public void onReceivedFreshAd(DomobAdView adView2) {
        AdMogoLayout adMogoLayout;
        Log.d(AdMogoUtil.ADMOGO, "Domob Success");
        adView2.setAdListener((DomobAdListener) null);
        if (!this.activity.isFinishing() && (adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout.adMogoManager.resetRollover();
            adMogoLayout.handler.post(new AdMogoLayout.ViewAdRunnable(adMogoLayout, adView2, 29));
            adMogoLayout.rotateThreadedDelayed();
        }
    }

    public void finish() {
        if (this.adView != null) {
            this.adView = null;
        }
        Log.d(AdMogoUtil.ADMOGO, "Domob Finished");
    }

    public void onLandingPageClose() {
        Log.d(AdMogoUtil.ADMOGO, "Domob LandingPageClose");
    }

    public void onLandingPageOpening() {
        Log.d(AdMogoUtil.ADMOGO, "Domob LandingPageOpening");
    }
}
