package com.admogo.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.admogo.AdMogoLayout;
import com.admogo.obj.Ration;
import com.admogo.util.AdMogoUtil;
import com.madhouse.android.ads.AdView;
import java.util.concurrent.TimeUnit;

public class CustomAdapter extends AdMogoAdapter {
    public CustomAdapter(AdMogoLayout adMogoLayout, Ration ration) {
        super(adMogoLayout, ration);
    }

    public void handle() {
        AdMogoLayout adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
        if (adMogoLayout != null) {
            adMogoLayout.scheduler.schedule(new FetchCustomRunnable(this), 0, TimeUnit.SECONDS);
        }
    }

    public void displayCustom() {
        Activity activity;
        AdMogoLayout adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
        if (adMogoLayout != null && (activity = adMogoLayout.activityReference.get()) != null) {
            double density = AdMogoUtil.getDensity(activity);
            double px320 = (double) AdMogoUtil.convertToScreenPixels((int) AdView.PHONE_AD_MEASURE_320, density);
            double px50 = (double) AdMogoUtil.convertToScreenPixels(50, density);
            double px42 = (double) AdMogoUtil.convertToScreenPixels(42, density);
            double px4 = (double) AdMogoUtil.convertToScreenPixels(4, density);
            switch (adMogoLayout.custom.type) {
                case 1:
                    Log.d(AdMogoUtil.ADMOGO, "Serving custom type: banner");
                    RelativeLayout bannerView = new RelativeLayout(activity);
                    if (adMogoLayout.custom.image != null) {
                        ImageView bannerImageView = new ImageView(activity);
                        bannerImageView.setImageDrawable(adMogoLayout.custom.image);
                        bannerImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) px320, (int) px50);
                        layoutParams.addRule(13);
                        bannerView.addView(bannerImageView, layoutParams);
                        adMogoLayout.pushSubView(bannerView, 9);
                        break;
                    } else {
                        adMogoLayout.rotateThreadedNow();
                        return;
                    }
                case 2:
                    Log.d(AdMogoUtil.ADMOGO, "Serving custom type: icon");
                    RelativeLayout relativeLayout = new RelativeLayout(activity);
                    relativeLayout.setGravity(17);
                    if (adMogoLayout.custom.image != null) {
                        relativeLayout.setLayoutParams(new FrameLayout.LayoutParams((int) px320, (int) px50));
                        ImageView blendView = new ImageView(activity);
                        int backgroundColor = Color.rgb(adMogoLayout.extra.bgRed, adMogoLayout.extra.bgGreen, adMogoLayout.extra.bgBlue);
                        blendView.setBackgroundDrawable(new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-1, backgroundColor, backgroundColor, backgroundColor}));
                        relativeLayout.addView(blendView, new RelativeLayout.LayoutParams(-1, -1));
                        ImageView imageView = new ImageView(activity);
                        imageView.setImageDrawable(adMogoLayout.custom.image);
                        imageView.setId(10);
                        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams((int) px42, (int) px42);
                        layoutParams2.setMargins((int) px4, (int) px4, (int) px4, (int) px4);
                        relativeLayout.addView(imageView, layoutParams2);
                        TextView textView = new TextView(activity);
                        try {
                            if (adMogoLayout.custom.adText == null) {
                            }
                        } catch (Exception e) {
                            adMogoLayout.custom.adText = "Haven't description!";
                        }
                        textView.setText(adMogoLayout.custom.adText);
                        textView.setTypeface(Typeface.DEFAULT_BOLD, 1);
                        textView.setTextColor(Color.rgb(adMogoLayout.extra.fgRed, adMogoLayout.extra.fgGreen, adMogoLayout.extra.fgBlue));
                        RelativeLayout.LayoutParams textViewParams = new RelativeLayout.LayoutParams(-1, -1);
                        textViewParams.addRule(1, imageView.getId());
                        textViewParams.addRule(10);
                        textViewParams.addRule(12);
                        textViewParams.addRule(15);
                        textViewParams.addRule(13);
                        textView.setGravity(16);
                        relativeLayout.addView(textView, textViewParams);
                        adMogoLayout.pushSubView(relativeLayout, 9);
                        break;
                    } else {
                        adMogoLayout.rollover();
                        return;
                    }
                case 3:
                    Log.d(AdMogoUtil.ADMOGO, "Serving custom type: full");
                    RelativeLayout relativeLayout2 = new RelativeLayout(activity);
                    relativeLayout2.setBackgroundColor(-16777216);
                    if (adMogoLayout.custom.image != null) {
                        ImageView imageView2 = new ImageView(activity);
                        imageView2.setImageDrawable(adMogoLayout.custom.image);
                        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, -2);
                        layoutParams3.addRule(13);
                        relativeLayout2.addView(imageView2, layoutParams3);
                        adMogoLayout.pushSubView(relativeLayout2, 9);
                        break;
                    } else {
                        adMogoLayout.rotateThreadedNow();
                        return;
                    }
                default:
                    Log.w(AdMogoUtil.ADMOGO, "Unknown custom type!");
                    adMogoLayout.rotateThreadedNow();
                    return;
            }
            adMogoLayout.custom.image = null;
            adMogoLayout.adMogoManager.resetRollover();
            adMogoLayout.rotateThreadedDelayed();
        }
    }

    private static class FetchCustomRunnable implements Runnable {
        private CustomAdapter customAdapter;

        public FetchCustomRunnable(CustomAdapter customAdapter2) {
            this.customAdapter = customAdapter2;
        }

        public void run() {
            AdMogoLayout adMogoLayout = (AdMogoLayout) this.customAdapter.adMogoLayoutReference.get();
            if (adMogoLayout != null) {
                adMogoLayout.custom = adMogoLayout.adMogoManager.getCustom(this.customAdapter.ration.nid);
                if (adMogoLayout.custom == null) {
                    adMogoLayout.rotateThreadedNow();
                } else {
                    adMogoLayout.handler.post(new DisplayCustomRunnable(this.customAdapter));
                }
            }
        }
    }

    private static class DisplayCustomRunnable implements Runnable {
        private CustomAdapter customAdapter;

        public DisplayCustomRunnable(CustomAdapter customAdapter2) {
            this.customAdapter = customAdapter2;
        }

        public void run() {
            this.customAdapter.displayCustom();
        }
    }

    public void finish() {
        Log.d(AdMogoUtil.ADMOGO, "Custom Finished");
    }
}
