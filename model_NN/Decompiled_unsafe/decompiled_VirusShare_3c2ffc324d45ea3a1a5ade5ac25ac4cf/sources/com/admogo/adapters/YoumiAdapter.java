package com.admogo.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.util.Log;
import android.view.ViewGroup;
import com.admogo.AdMogoLayout;
import com.admogo.obj.Extra;
import com.admogo.obj.Ration;
import com.admogo.util.AdMogoUtil;
import net.youmi.android.AdManager;
import net.youmi.android.AdView;
import net.youmi.android.AdViewListener;
import org.json.JSONException;
import org.json.JSONObject;

public class YoumiAdapter extends AdMogoAdapter implements AdViewListener {
    private String AppID = null;
    private String AppSecret = null;
    private Activity activity;
    private AdView adView;

    public YoumiAdapter(AdMogoLayout adMogoLayout, Ration ration) throws JSONException {
        super(adMogoLayout, ration);
        JSONObject jsonObject = new JSONObject(this.ration.key);
        this.AppID = jsonObject.getString("AppID");
        this.AppSecret = jsonObject.getString("AppSecret");
    }

    public void handle() {
        AdMogoLayout adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
        if (adMogoLayout != null) {
            this.activity = adMogoLayout.activityReference.get();
            if (this.activity != null) {
                Extra extra = adMogoLayout.extra;
                int refreshTime = extra.cycleTime;
                if (refreshTime < 30) {
                    refreshTime = 30;
                } else if (refreshTime > 200) {
                    refreshTime = 200;
                }
                try {
                    AdManager.init(this.AppID, this.AppSecret, refreshTime, this.ration.testmodel);
                    this.adView = new AdView(this.activity, Color.rgb(extra.bgRed, extra.bgGreen, extra.bgBlue), Color.rgb(extra.fgRed, extra.fgGreen, extra.fgBlue), 255);
                    this.adView.setAdViewListener(this);
                    adMogoLayout.addView(this.adView, new ViewGroup.LayoutParams(-2, -2));
                } catch (IllegalArgumentException e) {
                    adMogoLayout.rollover();
                }
            }
        }
    }

    public void finish() {
        Log.d(AdMogoUtil.ADMOGO, "Youmi Finished");
    }

    public void onConnectFailed(AdView adView2) {
        AdMogoLayout adMogoLayout;
        Log.d(AdMogoUtil.ADMOGO, "youMi failure");
        adView2.setAdViewListener(null);
        if (!this.activity.isFinishing() && (adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout.rollover();
        }
    }

    public void onAdViewSwitchedAd(AdView adView2) {
        AdMogoLayout adMogoLayout;
        Log.d(AdMogoUtil.ADMOGO, "youMi success");
        adView2.setAdViewListener(null);
        if (!this.activity.isFinishing() && (adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout.adMogoManager.resetRollover();
            adMogoLayout.handler.post(new AdMogoLayout.ViewAdRunnable(adMogoLayout, adView2, 24));
            adMogoLayout.rotateThreadedDelayed();
        }
    }
}
