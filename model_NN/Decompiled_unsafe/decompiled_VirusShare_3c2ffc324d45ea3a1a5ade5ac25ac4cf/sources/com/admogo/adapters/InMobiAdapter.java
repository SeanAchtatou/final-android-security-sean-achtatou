package com.admogo.adapters;

import android.app.Activity;
import android.location.Location;
import android.util.Log;
import com.admogo.AdMogoLayout;
import com.admogo.AdMogoTargeting;
import com.admogo.obj.Extra;
import com.admogo.obj.Ration;
import com.admogo.util.AdMogoUtil;
import com.inmobi.androidsdk.EducationType;
import com.inmobi.androidsdk.EthnicityType;
import com.inmobi.androidsdk.GenderType;
import com.inmobi.androidsdk.InMobiAdDelegate;
import com.inmobi.androidsdk.impl.InMobiAdView;
import java.util.Date;

public final class InMobiAdapter extends AdMogoAdapter implements InMobiAdDelegate {
    private Activity activity;
    public int adUnit = 9;
    private Extra extra = null;

    public InMobiAdapter(AdMogoLayout adMogoLayout, Ration ration) {
        super(adMogoLayout, ration);
        this.extra = adMogoLayout.extra;
    }

    public void handle() {
        AdMogoLayout adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
        if (adMogoLayout != null) {
            this.activity = adMogoLayout.activityReference.get();
            if (this.activity != null) {
                InMobiAdView.requestAdUnitWithDelegate(this.activity.getApplicationContext(), this, this.activity, this.adUnit).loadNewAd();
            }
        }
    }

    public void adRequestCompleted(InMobiAdView adView) {
        AdMogoLayout adMogoLayout;
        Log.d(AdMogoUtil.ADMOGO, "InMobi success");
        if (!this.activity.isFinishing() && (adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout.adMogoManager.resetRollover();
            adMogoLayout.handler.post(new AdMogoLayout.ViewAdRunnable(adMogoLayout, adView, 18));
            adMogoLayout.rotateThreadedDelayed();
            adView.stopReceivingNotifications();
        }
    }

    public void adRequestFailed(InMobiAdView adView) {
        AdMogoLayout adMogoLayout;
        Log.d(AdMogoUtil.ADMOGO, "InMobi failure");
        adView.stopReceivingNotifications();
        if (!this.activity.isFinishing() && (adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get()) != null) {
            adMogoLayout.rollover();
        }
    }

    public int age() {
        return AdMogoTargeting.getAge();
    }

    public String areaCode() {
        return null;
    }

    public Location currentLocation() {
        return null;
    }

    public Date dateOfBirth() {
        return null;
    }

    public EducationType education() {
        return null;
    }

    public EthnicityType ethnicity() {
        return null;
    }

    public GenderType gender() {
        AdMogoTargeting.Gender gender = AdMogoTargeting.getGender();
        if (AdMogoTargeting.Gender.MALE == gender) {
            return GenderType.G_M;
        }
        if (AdMogoTargeting.Gender.FEMALE == gender) {
            return GenderType.G_F;
        }
        return GenderType.G_None;
    }

    public int income() {
        return 0;
    }

    public String interests() {
        return null;
    }

    public boolean isLocationInquiryAllowed() {
        if (this.extra.locationOn == 1) {
            return true;
        }
        return false;
    }

    public boolean isPublisherProvidingLocation() {
        return false;
    }

    public String keywords() {
        return AdMogoTargeting.getKeywords();
    }

    public String postalCode() {
        return AdMogoTargeting.getPostalCode();
    }

    public String searchString() {
        return null;
    }

    public String siteId() {
        return this.ration.key;
    }

    public boolean testMode() {
        return this.ration.testmodel;
    }

    public void finish() {
        Log.d(AdMogoUtil.ADMOGO, "InMobi Finished");
    }
}
