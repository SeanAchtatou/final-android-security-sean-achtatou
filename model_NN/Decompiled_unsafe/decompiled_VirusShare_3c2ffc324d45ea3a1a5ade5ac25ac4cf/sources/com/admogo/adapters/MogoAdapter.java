package com.admogo.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.admogo.AdMogoLayout;
import com.admogo.obj.Ration;
import com.admogo.util.AdMogoUtil;
import com.madhouse.android.ads.AdView;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;

public class MogoAdapter extends AdMogoAdapter {
    public MogoAdapter(AdMogoLayout adMogoLayout, Ration ration) {
        super(adMogoLayout, ration);
    }

    public void handle() {
        AdMogoLayout adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
        if (adMogoLayout != null) {
            adMogoLayout.scheduler.schedule(new FetchMogoRunnable(this), 0, TimeUnit.SECONDS);
        }
    }

    public void displayMogo() {
        Activity activity;
        AdMogoLayout adMogoLayout = (AdMogoLayout) this.adMogoLayoutReference.get();
        if (adMogoLayout != null && (activity = adMogoLayout.activityReference.get()) != null) {
            switch (adMogoLayout.mogo.type) {
                case 1:
                    Log.d(AdMogoUtil.ADMOGO, "Serving Mogo type: banner");
                    RelativeLayout bannerView = new RelativeLayout(activity);
                    if (adMogoLayout.mogo.image != null) {
                        ImageView bannerImageView = new ImageView(activity);
                        bannerImageView.setImageDrawable(adMogoLayout.mogo.image);
                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
                        layoutParams.addRule(13);
                        bannerView.addView(bannerImageView, layoutParams);
                        adMogoLayout.pushSubView(bannerView, 27);
                        break;
                    } else {
                        adMogoLayout.rotateThreadedNow();
                        return;
                    }
                case 2:
                    Log.d(AdMogoUtil.ADMOGO, "Serving Mogo type: icon");
                    RelativeLayout relativeLayout = new RelativeLayout(activity);
                    if (adMogoLayout.mogo.image != null) {
                        double density = AdMogoUtil.getDensity(activity);
                        double px320 = (double) AdMogoUtil.convertToScreenPixels((int) AdView.PHONE_AD_MEASURE_320, density);
                        double px50 = (double) AdMogoUtil.convertToScreenPixels(50, density);
                        double px4 = (double) AdMogoUtil.convertToScreenPixels(4, density);
                        double px6 = (double) AdMogoUtil.convertToScreenPixels(6, density);
                        relativeLayout.setLayoutParams(new FrameLayout.LayoutParams((int) px320, (int) px50));
                        ImageView blendView = new ImageView(activity);
                        int backgroundColor = Color.rgb(adMogoLayout.extra.bgRed, adMogoLayout.extra.bgGreen, adMogoLayout.extra.bgBlue);
                        blendView.setBackgroundDrawable(new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-1, backgroundColor, backgroundColor, backgroundColor}));
                        relativeLayout.addView(blendView, new RelativeLayout.LayoutParams(-1, -1));
                        ImageView imageView = new ImageView(activity);
                        imageView.setImageDrawable(adMogoLayout.mogo.image);
                        imageView.setId(10);
                        imageView.setPadding((int) px4, 0, (int) px6, 0);
                        imageView.setScaleType(ImageView.ScaleType.CENTER);
                        relativeLayout.addView(imageView, new RelativeLayout.LayoutParams(-2, -1));
                        ImageView imageView2 = new ImageView(activity);
                        InputStream drawableStream = getClass().getResourceAsStream("/com/admogo/assets/ad_frame.gif");
                        new BitmapDrawable(drawableStream);
                        imageView2.setImageDrawable(null);
                        imageView2.setPadding((int) px4, 0, (int) px6, 0);
                        imageView2.setScaleType(ImageView.ScaleType.CENTER);
                        relativeLayout.addView(imageView2, new RelativeLayout.LayoutParams(-2, -1));
                        TextView textView = new TextView(activity);
                        try {
                            if (adMogoLayout.mogo.description == null) {
                            }
                        } catch (Exception e) {
                            adMogoLayout.mogo.description = "Haven't description!";
                        }
                        textView.setText(adMogoLayout.mogo.description);
                        textView.setTypeface(Typeface.DEFAULT_BOLD, 1);
                        textView.setTextColor(Color.rgb(adMogoLayout.extra.fgRed, adMogoLayout.extra.fgGreen, adMogoLayout.extra.fgBlue));
                        RelativeLayout.LayoutParams textViewParams = new RelativeLayout.LayoutParams(-1, -1);
                        textViewParams.addRule(1, imageView.getId());
                        textViewParams.addRule(10);
                        textViewParams.addRule(12);
                        textViewParams.addRule(15);
                        textViewParams.addRule(13);
                        textView.setGravity(16);
                        relativeLayout.addView(textView, textViewParams);
                        adMogoLayout.pushSubView(relativeLayout, 27);
                        try {
                            drawableStream.close();
                            break;
                        } catch (IOException e2) {
                            Log.e(AdMogoUtil.ADMOGO, e2.toString());
                            break;
                        }
                    } else {
                        adMogoLayout.rotateThreadedNow();
                        return;
                    }
                default:
                    Log.w(AdMogoUtil.ADMOGO, "Unknown mogo type!");
                    adMogoLayout.rotateThreadedNow();
                    return;
            }
            adMogoLayout.mogo.image = null;
            adMogoLayout.adMogoManager.resetRollover();
            adMogoLayout.rotateThreadedDelayed();
        }
    }

    private static class FetchMogoRunnable implements Runnable {
        private MogoAdapter mogoAdapter;

        public FetchMogoRunnable(MogoAdapter mogoAdapter2) {
            this.mogoAdapter = mogoAdapter2;
        }

        public void run() {
            AdMogoLayout adMogoLayout = (AdMogoLayout) this.mogoAdapter.adMogoLayoutReference.get();
            if (adMogoLayout != null) {
                adMogoLayout.mogo = adMogoLayout.adMogoManager.getMogo(this.mogoAdapter.ration.nid);
                if (adMogoLayout.mogo == null) {
                    adMogoLayout.rollover();
                } else {
                    adMogoLayout.handler.post(new DisplayMogoRunnable(this.mogoAdapter));
                }
            }
        }
    }

    private static class DisplayMogoRunnable implements Runnable {
        private MogoAdapter mogoAdapter;

        public DisplayMogoRunnable(MogoAdapter mogoAdapter2) {
            this.mogoAdapter = mogoAdapter2;
        }

        public void run() {
            this.mogoAdapter.displayMogo();
        }
    }

    public void finish() {
        Log.d(AdMogoUtil.ADMOGO, "Mogo Finished");
    }
}
