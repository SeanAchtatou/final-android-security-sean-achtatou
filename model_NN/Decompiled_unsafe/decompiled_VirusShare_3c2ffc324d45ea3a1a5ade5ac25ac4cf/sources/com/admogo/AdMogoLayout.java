package com.admogo;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.admogo.adapters.AdMogoAdapter;
import com.admogo.adapters.BaiduJsonAdapter;
import com.admogo.obj.Custom;
import com.admogo.obj.Exchange;
import com.admogo.obj.Extra;
import com.admogo.obj.Mogo;
import com.admogo.obj.Ration;
import com.admogo.util.AdMogoUtil;
import com.adwo.adsdk.AdwoAdView;
import com.baidu.AdRequest;
import com.baidu.ClickType;
import com.madhouse.android.ads.AdView;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

public class AdMogoLayout extends RelativeLayout {
    public static final String ADMOGO_ADTYPE = "ADMOGO_ADTYPE";
    public static final String ADMOGO_KEY = "ADMOGO_KEY";
    private static final int TIMEOUT_TIME = 8000;
    /* access modifiers changed from: private */
    public static int baiduClick1 = 0;
    /* access modifiers changed from: private */
    public static int baiduClick2 = 0;
    /* access modifiers changed from: private */
    public static int baiduClick3 = 0;
    /* access modifiers changed from: private */
    public static int baiduImp = 0;
    /* access modifiers changed from: private */
    public static String countryCode;
    static Context getContext;
    /* access modifiers changed from: private */
    public static boolean isEmulator = false;
    /* access modifiers changed from: private */
    public static int totalAdType;
    public Ration activeRation;
    public WeakReference<Activity> activityReference;
    /* access modifiers changed from: private */
    public AdMogoLayout adMogoLayout;
    private AdMogoListener adMogoListener;
    public AdMogoManager adMogoManager;
    private int ad_type;
    public String clickflag = "";
    public String clickview = "";
    public Custom custom;
    private float density;
    public Exchange exchange;
    public Extra extra;
    public final Handler handler = new Handler();
    /* access modifiers changed from: private */
    public boolean hasWindow;
    private int height = 50;
    /* access modifiers changed from: private */
    public boolean isScheduled;
    private String keyAdMogo;
    private int maxHeight;
    private int maxWidth;
    public Mogo mogo;
    public Ration nextRation;
    public final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    public WeakReference<RelativeLayout> superViewReference;
    private int width = AdView.PHONE_AD_MEASURE_320;

    public void setMaxWidth(int width2) {
        this.maxWidth = width2;
    }

    public void setMaxHeight(int height2) {
        this.maxHeight = height2;
    }

    public AdMogoLayout(Activity context, String keyAdMogo2, int ad_type2) {
        super(context);
        init(context, keyAdMogo2, ad_type2);
    }

    public AdMogoLayout(Activity context, String keyAdMogo2) {
        super(context);
        init(context, keyAdMogo2, 1);
    }

    public AdMogoLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init((Activity) context, getAdMogoKey(context), 1);
    }

    /* access modifiers changed from: protected */
    public String getAdMogoKey(Context context) {
        String packageName = context.getPackageName();
        String activityName = context.getClass().getName();
        PackageManager pm = context.getPackageManager();
        try {
            Bundle bundle = pm.getActivityInfo(new ComponentName(packageName, activityName), 128).metaData;
            if (bundle != null) {
                return bundle.getString(ADMOGO_KEY);
            }
            try {
                Bundle bundle2 = pm.getApplicationInfo(packageName, 128).metaData;
                if (bundle2 != null) {
                    return bundle2.getString(ADMOGO_KEY);
                }
                return null;
            } catch (PackageManager.NameNotFoundException e) {
                return null;
            }
        } catch (PackageManager.NameNotFoundException e2) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void init(Activity context, String keyAdMogo2, int ad_type2) {
        this.density = getResources().getDisplayMetrics().density;
        this.width = (int) (((float) this.width) * this.density);
        this.height = (int) (((float) this.height) * this.density);
        getContext = context;
        this.activityReference = new WeakReference<>(context);
        this.superViewReference = new WeakReference<>(this);
        this.keyAdMogo = keyAdMogo2;
        this.hasWindow = true;
        this.isScheduled = true;
        this.ad_type = ad_type2;
        this.scheduler.schedule(new InitRunnable(this, keyAdMogo2, ad_type2), 0, TimeUnit.SECONDS);
        setHorizontalScrollBarEnabled(false);
        setVerticalScrollBarEnabled(false);
        this.maxWidth = 0;
        this.maxHeight = 0;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthSize = View.MeasureSpec.getSize(widthMeasureSpec);
        int heightSize = View.MeasureSpec.getSize(heightMeasureSpec);
        if (this.maxWidth > 0 && widthSize > this.maxWidth) {
            widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(this.maxWidth, Integer.MIN_VALUE);
        }
        if (this.maxHeight > 0 && heightSize > this.maxHeight) {
            heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(this.maxHeight, Integer.MIN_VALUE);
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int visibility) {
        if (visibility == 0) {
            this.hasWindow = true;
            if (!this.isScheduled) {
                this.isScheduled = true;
                if (this.extra != null) {
                    rotateThreadedNow();
                } else {
                    this.scheduler.schedule(new InitRunnable(this, this.keyAdMogo, this.ad_type), 0, TimeUnit.SECONDS);
                }
            }
        } else {
            this.hasWindow = false;
        }
    }

    /* access modifiers changed from: private */
    public void rotateAd() {
        if (!this.hasWindow) {
            this.isScheduled = false;
            return;
        }
        Log.i(AdMogoUtil.ADMOGO, "Rotating Ad");
        this.nextRation = this.adMogoManager.getRation(this.activeRation);
        this.handler.post(new HandleAdRunnable(this));
    }

    /* access modifiers changed from: private */
    public void handleAd() {
        if (this.nextRation == null) {
            Log.e(AdMogoUtil.ADMOGO, "nextRation is null!");
            countFailed();
            this.adMogoManager.resetRollover();
            if (this.ad_type == 1) {
                rotateThreadedDelayed();
                return;
            }
            return;
        }
        Log.d(AdMogoUtil.ADMOGO, String.format("Showing ad:\n\tnid: %s\n\tname: %s\n\ttype: %d\n\tkey: %s\n\tkey2: %s", this.nextRation.nid, this.nextRation.name, Integer.valueOf(this.nextRation.type), this.nextRation.key, this.nextRation.key2));
        try {
            AdMogoAdapter.handle(this, this.nextRation);
        } catch (Throwable th) {
            Log.w(AdMogoUtil.ADMOGO, "Caught an exception in adapter:", th);
            rollover();
        }
    }

    public void rotateThreadedNow() {
        this.scheduler.schedule(new RotateAdRunnable(this), 0, TimeUnit.SECONDS);
    }

    public void rotateThreadedDelayed() {
        if (totalAdType != 24) {
            Log.d(AdMogoUtil.ADMOGO, "Will call rotateAd() in " + this.extra.cycleTime + " seconds");
            this.scheduler.schedule(new RotateAdRunnable(this), (long) this.extra.cycleTime, TimeUnit.SECONDS);
        } else if (this.extra.cycleTime <= 30) {
            Log.d(AdMogoUtil.ADMOGO, "YouMiAd need call rotateAd() in 30 seconds");
            this.scheduler.schedule(new RotateAdRunnable(this), 28, TimeUnit.SECONDS);
        } else {
            Log.d(AdMogoUtil.ADMOGO, "Will call rotateAd() in " + this.extra.cycleTime + " seconds");
            this.scheduler.schedule(new RotateAdRunnable(this), (long) (this.extra.cycleTime - 2), TimeUnit.SECONDS);
        }
    }

    public void stopScheduler() {
    }

    public void pushSubView(ViewGroup subView, int adType) {
        if (baiduImp != 0) {
            this.scheduler.schedule(new PingBaiduUrlRunnable(null), 0, TimeUnit.SECONDS);
        }
        RelativeLayout superView = this.superViewReference.get();
        this.clickview = subView.toString();
        if (superView != null) {
            if (24 == adType) {
                if (superView.getChildCount() > 2) {
                    superView.removeViewAt(0);
                }
            } else if (29 == adType || 28 == adType || 26 == adType || 37 == adType) {
                int count = superView.getChildCount();
                if (count > 1) {
                    for (int i = 0; i < count - 1; i++) {
                        superView.removeViewAt(0);
                    }
                }
            } else if (33 == adType) {
                int count2 = superView.getChildCount();
                int i2 = 0;
                while (true) {
                    if (i2 >= count2) {
                        break;
                    } else if (superView.getChildAt(i2) instanceof AdwoAdView) {
                        ((AdwoAdView) superView.getChildAt(i2)).finalize();
                        break;
                    } else {
                        i2++;
                    }
                }
                if (count2 > 1) {
                    for (int i3 = 0; i3 < count2 - 1; i3++) {
                        superView.removeViewAt(0);
                    }
                }
            } else if (9 == adType || 27 == adType || 45 == adType || 21 == adType || 35 == adType || 30 == adType || 44 == adType) {
                superView.removeAllViews();
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(this.width, this.height);
                layoutParams.addRule(13);
                superView.addView(subView, layoutParams);
            } else if (this.ad_type == 1) {
                superView.removeAllViews();
                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -2);
                layoutParams2.addRule(13);
                superView.addView(subView, layoutParams2);
            } else if (this.ad_type == 6) {
                RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, -1);
                layoutParams3.addRule(13);
                superView.addView(subView, layoutParams3);
            }
            this.activeRation = this.nextRation;
            if (9 == adType || 27 == adType || 45 == adType) {
                Log.d(AdMogoUtil.ADMOGO, "Added subview");
            } else if (44 == adType) {
                Log.d(AdMogoUtil.ADMOGO, "Added subview");
                baiduImp = 1;
                countImpression();
            } else {
                Log.d(AdMogoUtil.ADMOGO, "Added subview");
                countImpression();
            }
        }
    }

    public void rollover() {
        this.nextRation = this.adMogoManager.getRollover();
        this.handler.post(new HandleAdRunnable(this));
    }

    public void countRequest() {
        if (this.nextRation != null) {
            String url = String.format(AdMogoUtil.urlRequest, this.keyAdMogo, this.nextRation.nid, Integer.valueOf(this.nextRation.type), this.adMogoManager.deviceIDHash, Integer.valueOf(this.ad_type), countryCode);
            Log.d(AdMogoUtil.ADMOGO, String.format("Showing Request:\n nid: %s\n Type: %s", this.nextRation.nid, Integer.valueOf(this.nextRation.type)));
            this.scheduler.schedule(new PingUrlRunnable(url, new Object[]{this.keyAdMogo, this.nextRation.nid, Integer.valueOf(this.nextRation.type), Integer.valueOf(this.ad_type), countryCode, 1, 0, 0, 0}), 0, TimeUnit.SECONDS);
        }
    }

    private void countImpression() {
        if (this.adMogoListener != null) {
            this.adMogoListener.onReceiveAd();
        }
        if (getAdType() == 6) {
            this.activeRation = this.nextRation;
        }
        if (this.activeRation != null) {
            String url = String.format(AdMogoUtil.urlImpression, this.keyAdMogo, this.activeRation.nid, Integer.valueOf(this.activeRation.type), this.adMogoManager.deviceIDHash, Integer.valueOf(this.ad_type), countryCode);
            Log.d(AdMogoUtil.ADMOGO, String.format("Showing Impression:\n nid: %s\n Type: %s", this.activeRation.nid, Integer.valueOf(this.activeRation.type)));
            this.scheduler.schedule(new PingUrlRunnable(url, new Object[]{this.keyAdMogo, this.activeRation.nid, Integer.valueOf(this.activeRation.type), Integer.valueOf(this.ad_type), countryCode, 0, 1, 0, 0}), 0, TimeUnit.SECONDS);
        }
    }

    private void countClick() {
        if (this.adMogoListener != null) {
            this.adMogoListener.onClickAd();
        }
        if (getAdType() == 6) {
            this.activeRation = this.nextRation;
        }
        if (this.activeRation != null && !this.clickview.equals(this.clickflag)) {
            this.clickflag = this.clickview;
            String url = String.format(AdMogoUtil.urlClick, this.keyAdMogo, Integer.valueOf((int) AdMogoUtil.VERSION), this.activeRation.nid, Integer.valueOf(this.activeRation.type), this.adMogoManager.deviceIDHash, Integer.valueOf(this.ad_type), countryCode, "");
            Log.d(AdMogoUtil.ADMOGO, String.format("Showing Click:\n nid: %s\n Type: %s", this.activeRation.nid, Integer.valueOf(this.activeRation.type)));
            this.scheduler.schedule(new PingUrlRunnable(url, new Object[]{this.keyAdMogo, this.activeRation.nid, Integer.valueOf(this.activeRation.type), Integer.valueOf(this.ad_type), countryCode, 0, 0, 1, 0}), 0, TimeUnit.SECONDS);
        }
    }

    private void countFailed() {
        if (this.adMogoListener != null) {
            this.adMogoListener.onFailedReceiveAd();
        }
        String url = String.format(AdMogoUtil.urlNull, this.keyAdMogo, AdMogoManager.lastRation.nid, Integer.valueOf(AdMogoManager.lastRation.type), this.adMogoManager.deviceIDHash, Integer.valueOf(this.ad_type), countryCode);
        Log.d(AdMogoUtil.ADMOGO, String.format("Showing impFailed:\n nid: %s\n Type: %s", AdMogoManager.lastRation.nid, Integer.valueOf(AdMogoManager.lastRation.type)));
        this.scheduler.schedule(new PingUrlRunnable(url, new Object[]{this.keyAdMogo, AdMogoManager.lastRation.nid, Integer.valueOf(AdMogoManager.lastRation.type), Integer.valueOf(this.ad_type), countryCode, 0, 0, 0, 1}), 0, TimeUnit.SECONDS);
    }

    public void countExClick(int type, String id, int clickNum, int downloadNum, int markNum) {
        if (this.activeRation != null && !this.clickview.equals(this.clickflag)) {
            String actionTag = "";
            if (clickNum == 1) {
                actionTag = "";
            } else if (downloadNum == 1) {
                actionTag = "&download=1";
            } else if (markNum == 1) {
                actionTag = "&favorite=1";
            }
            this.clickflag = this.clickview;
            String url = String.format(AdMogoUtil.urlClick, this.keyAdMogo, Integer.valueOf((int) AdMogoUtil.VERSION), id, Integer.valueOf(type), this.adMogoManager.deviceIDHash, Integer.valueOf(this.ad_type), countryCode, actionTag);
            Log.d(AdMogoUtil.ADMOGO, String.format("Showing Click:\n nid: %s\n Type: %s", this.activeRation.nid, Integer.valueOf(this.activeRation.type)));
            this.scheduler.schedule(new PingUrlRunnable(url, new Object[]{this.keyAdMogo, this.activeRation.nid, Integer.valueOf(this.activeRation.type), Integer.valueOf(this.ad_type), countryCode, 0, 0, 1, 0}), 0, TimeUnit.SECONDS);
        }
    }

    public void closeDialog() {
        if (this.adMogoListener != null) {
            this.adMogoListener.onCloseMogoDialog();
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case 0:
                if (this.ad_type != 6 || this.activeRation != null) {
                    Log.d(AdMogoUtil.ADMOGO, "Intercepted ACTION_DOWN event");
                    if (this.activeRation != null) {
                        if (this.activeRation.type != 9) {
                            if (this.activeRation.type != 27) {
                                if (this.activeRation.type != 45) {
                                    if (this.activeRation.type == 44) {
                                        if (BaiduJsonAdapter.baiduAd != null && BaiduJsonAdapter.baiduAd.getClickURL() != null) {
                                            if (BaiduJsonAdapter.baiduAd.getClickType() == ClickType.BROWSE) {
                                                baiduClick1 = 1;
                                            } else if (BaiduJsonAdapter.baiduAd.getClickType() == ClickType.PHONE) {
                                                baiduClick2 = 1;
                                            } else if (BaiduJsonAdapter.baiduAd.getClickType() == ClickType.DOWNLOAD) {
                                                baiduClick3 = 1;
                                            }
                                            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(BaiduJsonAdapter.baiduAd.getClickURL()));
                                            intent.addFlags(268435456);
                                            try {
                                                if (this.activityReference != null) {
                                                    Activity activity = this.activityReference.get();
                                                    if (activity != null) {
                                                        activity.startActivity(intent);
                                                        break;
                                                    } else {
                                                        return false;
                                                    }
                                                } else {
                                                    return false;
                                                }
                                            } catch (Exception e) {
                                                Log.w(AdMogoUtil.ADMOGO, "Could not handle click to Baidu", e);
                                                break;
                                            }
                                        } else {
                                            Log.w(AdMogoUtil.ADMOGO, "In onInterceptTouchEvent(), but baiduAd or baiduAd.link is null");
                                            break;
                                        }
                                    } else {
                                        countClick();
                                        break;
                                    }
                                } else if (this.exchange != null && this.exchange.link != null) {
                                    if (this.exchange.linkType == 2) {
                                        Context context = this.activityReference.get();
                                        boolean isH = false;
                                        int ori = context.getResources().getConfiguration().orientation;
                                        if (ori == 2) {
                                            isH = true;
                                        } else if (ori == 1) {
                                            isH = false;
                                        }
                                        if (this.adMogoListener != null) {
                                            this.adMogoListener.onClickAd();
                                        }
                                        new ExchangeDialog(this.activeRation.type, this.exchange.cid, this.adMogoLayout, context, this.exchange.appIcon, this.exchange.appName, this.exchange.appDes, this.exchange.imageUrlList, this.exchange.link, isH).show();
                                        break;
                                    } else {
                                        if (this.adMogoListener != null) {
                                            this.adMogoListener.onClickAd();
                                        }
                                        countExClick(this.activeRation.type, this.exchange.cid, 1, 0, 0);
                                        Intent intent2 = new Intent("android.intent.action.VIEW", Uri.parse(this.exchange.link));
                                        intent2.addFlags(268435456);
                                        try {
                                            if (this.activityReference != null) {
                                                Activity activity2 = this.activityReference.get();
                                                if (activity2 != null) {
                                                    activity2.startActivity(intent2);
                                                    break;
                                                } else {
                                                    return false;
                                                }
                                            } else {
                                                return false;
                                            }
                                        } catch (Exception e2) {
                                            Log.w(AdMogoUtil.ADMOGO, "Could not handle click to " + this.exchange.link, e2);
                                            break;
                                        }
                                    }
                                } else {
                                    Log.w(AdMogoUtil.ADMOGO, "In onInterceptTouchEvent(), but exchange or custom.link is null");
                                    break;
                                }
                            } else if (this.mogo != null && this.mogo.link != null) {
                                countClick();
                                Intent intent3 = new Intent("android.intent.action.VIEW", Uri.parse(this.mogo.link));
                                intent3.addFlags(268435456);
                                try {
                                    if (this.activityReference != null) {
                                        Activity activity3 = this.activityReference.get();
                                        if (activity3 != null) {
                                            activity3.startActivity(intent3);
                                            break;
                                        } else {
                                            return false;
                                        }
                                    } else {
                                        return false;
                                    }
                                } catch (Exception e3) {
                                    Log.w(AdMogoUtil.ADMOGO, "Could not handle click to " + this.mogo.link, e3);
                                    break;
                                }
                            } else {
                                Log.w(AdMogoUtil.ADMOGO, "In onInterceptTouchEvent(), but mogo or mogo.link is null");
                                break;
                            }
                        } else if (this.custom != null && this.custom.link != null) {
                            if (this.custom.linkType == 2) {
                                Context context2 = this.activityReference.get();
                                boolean isH2 = false;
                                int ori2 = context2.getResources().getConfiguration().orientation;
                                if (ori2 == 2) {
                                    isH2 = true;
                                } else if (ori2 == 1) {
                                    isH2 = false;
                                }
                                if (this.adMogoListener != null) {
                                    this.adMogoListener.onClickAd();
                                }
                                new ExchangeDialog(this.activeRation.type, this.activeRation.nid, this.adMogoLayout, context2, this.custom.appIcon, this.custom.appName, this.custom.appDes, this.custom.imageUrlList, this.custom.link, isH2).show();
                                break;
                            } else {
                                countClick();
                                Intent intent4 = new Intent("android.intent.action.VIEW", Uri.parse(this.custom.link));
                                intent4.addFlags(268435456);
                                try {
                                    if (this.activityReference != null) {
                                        Activity activity4 = this.activityReference.get();
                                        if (activity4 != null) {
                                            activity4.startActivity(intent4);
                                            break;
                                        } else {
                                            return false;
                                        }
                                    } else {
                                        return false;
                                    }
                                } catch (Exception e4) {
                                    Log.w(AdMogoUtil.ADMOGO, "Could not handle click to " + this.custom.link, e4);
                                    break;
                                }
                            }
                        } else {
                            Log.w(AdMogoUtil.ADMOGO, "In onInterceptTouchEvent(), but custom or custom.link is null");
                            break;
                        }
                    }
                } else {
                    countClick();
                    return false;
                }
                break;
        }
        return false;
    }

    public void setAdMogoListener(AdMogoListener adListener) {
        this.adMogoListener = adListener;
    }

    private class InitRunnable implements Runnable {
        private WeakReference<AdMogoLayout> adMogoLayoutReference;
        private int ad_Type;
        private String keyAdMogo;

        public InitRunnable(AdMogoLayout adMogoLayout, String keyAdMogo2, int ad_Type2) {
            this.adMogoLayoutReference = new WeakReference<>(adMogoLayout);
            this.keyAdMogo = keyAdMogo2;
            this.ad_Type = ad_Type2;
        }

        public void run() {
            Activity activity;
            AdMogoLayout.this.adMogoLayout = this.adMogoLayoutReference.get();
            if (AdMogoLayout.this.adMogoLayout != null && (activity = AdMogoLayout.this.adMogoLayout.activityReference.get()) != null) {
                if (AdMogoLayout.this.adMogoLayout.adMogoManager == null) {
                    AdMogoLayout.this.adMogoLayout.adMogoManager = new AdMogoManager(new WeakReference(activity.getApplicationContext()), this.keyAdMogo, this.ad_Type);
                }
                if (!AdMogoLayout.this.adMogoLayout.hasWindow) {
                    AdMogoLayout.this.adMogoLayout.isScheduled = false;
                    return;
                }
                if (TextUtils.isEmpty(AdMogoTargeting.countryCode)) {
                    TelephonyManager telephony = (TelephonyManager) activity.getSystemService("phone");
                    String imei = telephony.getDeviceId();
                    if (imei == null || imei.equals("000000000000000")) {
                        AdMogoLayout.isEmulator = true;
                    }
                    if (AdMogoLayout.isEmulator) {
                        AdMogoLayout.countryCode = Locale.getDefault().getCountry().toLowerCase();
                    } else {
                        AdMogoLayout.countryCode = telephony.getNetworkCountryIso().toLowerCase();
                        if (TextUtils.isEmpty(AdMogoLayout.countryCode)) {
                            AdMogoLayout.countryCode = Locale.getDefault().getCountry().toLowerCase();
                        }
                    }
                } else {
                    AdMogoLayout.countryCode = AdMogoTargeting.countryCode;
                }
                AdMogoLayout.this.adMogoLayout.adMogoManager.setLocation(AdMogoLayout.countryCode);
                AdMogoLayout.this.adMogoLayout.adMogoManager.fetchConfig();
                AdMogoLayout.this.adMogoLayout.extra = AdMogoLayout.this.adMogoLayout.adMogoManager.getExtra();
                if (AdMogoLayout.this.adMogoLayout.extra == null) {
                    Log.i(AdMogoUtil.ADMOGO, "Stop Show Ads");
                    return;
                }
                if (AdMogoLayout.this.adMogoLayout.extra.locationOn == 1) {
                    Log.d(AdMogoUtil.ADMOGO, "location is ON");
                    if (!AdMogoLayout.isEmulator && TextUtils.isEmpty(AdMogoTargeting.countryCode)) {
                        AdMogoLayout.this.scheduler.schedule(new getAdByCountryCode(AdMogoLayout.this.adMogoLayout, activity, AdMogoLayout.countryCode), 0, TimeUnit.SECONDS);
                    }
                }
                AdMogoLayout.this.adMogoLayout.rotateAd();
            }
        }
    }

    private static class getAdByCountryCode implements Runnable {
        Context activity;
        AdMogoLayout adMogoLayout;
        String simCode;

        public getAdByCountryCode(AdMogoLayout adMogoLayout2, Context context, String simCountryCode) {
            this.adMogoLayout = adMogoLayout2;
            this.activity = context;
            this.simCode = simCountryCode;
        }

        public void run() {
            Location location = this.adMogoLayout.adMogoManager.getLocation();
            if (location != null) {
                try {
                    List<Address> addressList = new Geocoder(this.activity).getFromLocation(location.getLatitude(), location.getLongitude(), 5);
                    if (addressList == null || addressList.size() <= 0) {
                        Log.e(AdMogoUtil.ADMOGO, "addressList is null or addressList.size() is 0");
                        return;
                    }
                    AdMogoLayout.countryCode = addressList.get(0).getCountryCode().toLowerCase();
                    AdMogoManager.refetchConfig = !AdMogoLayout.countryCode.equals(this.simCode);
                    if (AdMogoManager.refetchConfig) {
                        this.adMogoLayout.adMogoManager.setLocation(AdMogoLayout.countryCode);
                        AdMogoTargeting.countryCode = AdMogoLayout.countryCode;
                        this.adMogoLayout.adMogoManager.fetchConfig();
                        this.adMogoLayout.extra = this.adMogoLayout.adMogoManager.getExtra();
                    }
                } catch (Exception e) {
                    Log.w(AdMogoUtil.ADMOGO, "get countryCode failed", e);
                }
            } else {
                Log.w(AdMogoUtil.ADMOGO, "location is null");
            }
        }
    }

    private static class HandleAdRunnable implements Runnable {
        private WeakReference<AdMogoLayout> adMogoLayoutReference;

        public HandleAdRunnable(AdMogoLayout adMogoLayout) {
            this.adMogoLayoutReference = new WeakReference<>(adMogoLayout);
        }

        public void run() {
            AdMogoLayout adMogoLayout = this.adMogoLayoutReference.get();
            if (adMogoLayout != null) {
                adMogoLayout.handleAd();
            }
        }
    }

    public static class ViewAdRunnable implements Runnable {
        private WeakReference<AdMogoLayout> adMogoLayoutReference;
        private ViewGroup nextView;

        public ViewAdRunnable(AdMogoLayout adMogoLayout, ViewGroup nextView2, int adType) {
            this.adMogoLayoutReference = new WeakReference<>(adMogoLayout);
            this.nextView = nextView2;
            AdMogoLayout.totalAdType = adType;
        }

        public void run() {
            AdMogoLayout adMogoLayout = this.adMogoLayoutReference.get();
            if (adMogoLayout != null) {
                adMogoLayout.pushSubView(this.nextView, AdMogoLayout.totalAdType);
            }
        }
    }

    public void CountImpAd() {
        countImpression();
    }

    private static class RotateAdRunnable implements Runnable {
        private WeakReference<AdMogoLayout> adMogoLayoutReference;

        public RotateAdRunnable(AdMogoLayout adMogoLayout) {
            this.adMogoLayoutReference = new WeakReference<>(adMogoLayout);
        }

        public void run() {
            AdMogoLayout adMogoLayout = this.adMogoLayoutReference.get();
            if (adMogoLayout != null) {
                adMogoLayout.rotateAd();
            }
        }
    }

    private static class PingUrlRunnable implements Runnable {
        private Object[] data = new Object[9];
        private String url;

        public PingUrlRunnable(String url2, Object[] data2) {
            this.url = url2;
            this.data = data2;
        }

        public void run() {
            HttpParams httpParameters = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParameters, AdMogoLayout.TIMEOUT_TIME);
            HttpConnectionParams.setSoTimeout(httpParameters, AdMogoLayout.TIMEOUT_TIME);
            try {
                HttpResponse httpResponse = new DefaultHttpClient(httpParameters).execute(new HttpGet(this.url));
                Log.d(AdMogoUtil.ADMOGO, httpResponse.getStatusLine().toString());
                if (httpResponse.getStatusLine().getStatusCode() != 200) {
                    AdMogoLayout.insertData(this.data);
                }
            } catch (ClientProtocolException e) {
                AdMogoLayout.insertData(this.data);
                Log.e(AdMogoUtil.ADMOGO, "Caught ClientProtocolException in PingUrlRunnable", e);
            } catch (IOException e2) {
                AdMogoLayout.insertData(this.data);
                Log.e(AdMogoUtil.ADMOGO, "Caught IOException in PingUrlRunnable", e2);
            }
        }
    }

    private static class PingBaiduUrlRunnable implements Runnable {
        private PingBaiduUrlRunnable() {
        }

        /* synthetic */ PingBaiduUrlRunnable(PingBaiduUrlRunnable pingBaiduUrlRunnable) {
            this();
        }

        public void run() {
            String baiduURL = AdRequest.getClicklogUrl(BaiduJsonAdapter.baiduAd, AdMogoLayout.baiduImp, AdMogoLayout.baiduClick1, AdMogoLayout.baiduClick2, AdMogoLayout.baiduClick3);
            HttpParams httpParameters = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParameters, AdMogoLayout.TIMEOUT_TIME);
            HttpConnectionParams.setSoTimeout(httpParameters, AdMogoLayout.TIMEOUT_TIME);
            try {
                Log.d("Baidu", new DefaultHttpClient(httpParameters).execute(new HttpGet(baiduURL)).getStatusLine().toString());
            } catch (ClientProtocolException e) {
                Log.e(AdMogoUtil.ADMOGO, "Caught ClientProtocolException in PingBaiduUrlRunnable", e);
            } catch (IOException e2) {
                Log.e(AdMogoUtil.ADMOGO, "Caught IOException in PingBaiduUrlRunnable", e2);
            }
            AdMogoLayout.baiduImp = 0;
            AdMogoLayout.baiduClick1 = 0;
            AdMogoLayout.baiduClick2 = 0;
            AdMogoLayout.baiduClick3 = 0;
        }
    }

    /* access modifiers changed from: private */
    public static synchronized void insertData(Object[] data) {
        synchronized (AdMogoLayout.class) {
            Log.d(AdMogoUtil.ADMOGO, "Data Backup");
            DataBackup backup = new DataBackup();
            backup.open(getContext);
            backup.insertData(data);
            backup.close();
        }
    }

    public int getAdType() {
        return this.ad_type;
    }
}
