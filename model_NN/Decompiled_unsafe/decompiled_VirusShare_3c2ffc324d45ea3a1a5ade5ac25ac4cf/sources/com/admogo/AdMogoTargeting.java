package com.admogo;

import android.util.Log;
import com.admogo.util.AdMogoUtil;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Set;

public class AdMogoTargeting {
    public static final int APad_BIG_BANNER_AD = 2;
    public static final int APad_RECTANGLE_AD = 4;
    public static final int APad_SKYSCRAPER_AD = 5;
    public static final int APad_SMALL_BANNER_AD = 3;
    public static final int FULLSCREEN_AD = 6;
    public static final int PHONE_BANNER_AD = 1;
    public static final int VIDEO_AD = 7;
    private static int adchinaDefaultImage;
    private static String appName;
    private static GregorianCalendar birthDate;
    private static String birthday;
    private static String companyName;
    public static String countryCode = "";
    private static Gender gender;
    private static Set<String> keywordSet;
    private static String keywords;
    private static String postalCode;
    private static String telephoneNumber;

    public enum Gender {
        UNKNOWN,
        MALE,
        FEMALE
    }

    static {
        resetData();
    }

    public static void resetData() {
        appName = null;
        companyName = null;
        gender = Gender.UNKNOWN;
        birthDate = null;
        birthday = null;
        postalCode = null;
        keywords = null;
        keywordSet = null;
        telephoneNumber = null;
    }

    public static String getAppName() {
        return appName;
    }

    public static void setAppName(String appName2) {
        appName = appName2;
    }

    public static String getCompanyName() {
        return companyName;
    }

    public static void setCompanyName(String companyName2) {
        companyName = companyName2;
    }

    @Deprecated
    public static void setTestMode(boolean testMode) {
        Log.w(AdMogoUtil.ADMOGO, "Please set testMode in Website");
    }

    public static Gender getGender() {
        return gender;
    }

    public static void setGender(Gender gender2) {
        if (gender2 == null) {
            gender2 = Gender.UNKNOWN;
        }
        gender = gender2;
    }

    public static int getAge() {
        if (birthDate != null) {
            return Calendar.getInstance().get(1) - birthDate.get(1);
        }
        return -1;
    }

    public static String getBirthday() {
        return birthday;
    }

    public static void setBirthday(String birthday2) {
        birthday = birthday2;
        int year = Integer.parseInt(birthday2.substring(0, 4));
        int month = Integer.parseInt(birthday2.substring(4, 6));
        int date = Integer.parseInt(birthday2.substring(6, 8));
        GregorianCalendar birthDate2 = new GregorianCalendar();
        birthDate2.set(1, year);
        birthDate2.set(2, month);
        birthDate2.set(5, date);
        setBirthDate(birthDate2);
    }

    public static GregorianCalendar getBirthDate() {
        return birthDate;
    }

    public static void setBirthDate(GregorianCalendar birthDate2) {
        birthDate = birthDate2;
    }

    public static void setAge(int age) {
        birthDate = new GregorianCalendar(Calendar.getInstance().get(1) - age, 0, 1);
    }

    public static String getPostalCode() {
        return postalCode;
    }

    public static void setPostalCode(String postalCode2) {
        postalCode = postalCode2;
    }

    public static Set<String> getKeywordSet() {
        return keywordSet;
    }

    public static String getKeywords() {
        return keywords;
    }

    public static void setKeywordSet(Set<String> keywords2) {
        keywordSet = keywords2;
    }

    public static void setKeywords(String keywords2) {
        keywords = keywords2;
    }

    public static void addKeyword(String keyword) {
        if (keywordSet == null) {
            keywordSet = new HashSet();
        }
        keywordSet.add(keyword);
    }

    public static String getTelephoneNumber() {
        return telephoneNumber;
    }

    public static void setTelephoneNumber(String telephoneNumber2) {
        telephoneNumber = telephoneNumber2;
    }

    public static int getAdchinaDefaultImage() {
        return adchinaDefaultImage;
    }

    public static void setAdchinaDefaultImage(int adchinaDefaultImage2) {
        adchinaDefaultImage = adchinaDefaultImage2;
    }
}
