package com.shirley.morytest;

import android.content.Context;
import android.content.SharedPreferences;

public class Store {
    public static final String MYPREFS = "windrey";
    private Context context;

    public Store(Context c) {
        this.context = c;
    }

    /* access modifiers changed from: protected */
    public void save(String key, int val) {
        SharedPreferences.Editor editor = this.context.getSharedPreferences(MYPREFS, 1).edit();
        editor.putInt(key, val);
        editor.commit();
    }

    public int load(String key) {
        return this.context.getSharedPreferences(MYPREFS, 1).getInt(key, 0);
    }
}
