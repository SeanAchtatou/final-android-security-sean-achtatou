package com.shirley.morytest;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.ImageView;
import com.waps.AppConnect;
import java.util.Random;

public class LoadingActivity extends Activity {
    public AnimationDrawable LoadingAnimation;
    public ImageView loadImgView;
    Handler myHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    LoadingActivity.this.LoadingAnimation.start();
                    return;
                case 2:
                    Intent intent = new Intent(LoadingActivity.this, GameActivity.class);
                    intent.setFlags(67108864);
                    LoadingActivity.this.startActivity(intent);
                    LoadingActivity.this.finish();
                    return;
                default:
                    return;
            }
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.main);
        AppConnect.getInstance(this);
        AppConnect.getInstance(this).setPushAudio(false);
        AppConnect.getInstance(this).setPushIcon(R.drawable.push_icon);
        this.loadImgView = (ImageView) findViewById(R.id.loadimgview);
        this.loadImgView.setBackgroundResource(R.drawable.loading);
        this.LoadingAnimation = (AnimationDrawable) this.loadImgView.getBackground();
        this.LoadingAnimation.setOneShot(false);
        this.myHandler.sendMessageDelayed(Message.obtain(this.myHandler, 1, 0, 0), 500);
        this.myHandler.sendMessageDelayed(Message.obtain(this.myHandler, 2, 0, 0), 3000);
    }

    public static int generateRandom(int a, int b) {
        if (a <= b) {
            return new Random().nextInt(b - a) + a;
        }
        try {
            return new Random().nextInt(a - b) + b;
        } catch (Exception e) {
            e.printStackTrace();
            return 0 + a;
        }
    }
}
