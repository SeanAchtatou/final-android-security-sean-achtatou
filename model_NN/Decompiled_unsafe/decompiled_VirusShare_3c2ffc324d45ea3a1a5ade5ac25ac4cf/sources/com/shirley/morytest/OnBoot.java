package com.shirley.morytest;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class OnBoot extends BroadcastReceiver {
    public void onReceive(Context arg0, Intent intent) {
        ((AlarmManager) arg0.getSystemService("alarm")).setRepeating(2, 0, 120000, PendingIntent.getBroadcast(arg0, 0, new Intent(arg0, OnTimeup.class), 0));
    }
}
