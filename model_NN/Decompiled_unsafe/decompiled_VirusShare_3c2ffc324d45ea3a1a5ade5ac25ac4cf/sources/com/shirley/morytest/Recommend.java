package com.shirley.morytest;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;

public class Recommend {
    Context context;
    String[] packName = {"dangerb.game.llk", "com.dangerb.chongqiwawa", "com.dangerb.cubemic", "com.dangerb.wordroid", "com.dangerb.getfood", "com.dangerb.process", "com.dangerb.theft", "com.dangerb.SignalTools", "com.dangerb.packagemanager", "com.dangerb.widget.bear", "com.dangerb.mixedcolor", "com.dangerb.pingtu.yangyanmeinv", "com.dangerb.bct", "com.dangerb.mackupcolortest", "com.dangerb.ninetest2", "com.dangerb.swimtrainer", "com.dangerb.psychologicaltest", "com.dangerb.movieranking", "com.dangerb.sextest"};
    String[] softName = {"水果连连看", "充气娃娃连连看", "方块解密", "快乐背单词", "美女收集者", "任务管理器", "手机防盗锁", "手机信号分析指示器", "小蜜帮您卸", "小熊桌面宠物", "颜色大作战", "养眼美女拼图", "中医体质测试", "乐嘉性格色彩测试", "九型人格测试", "游泳教练", "8大最潮心理测试", "每周电影票房榜", "那方面质量测试"};

    public Recommend(Context c) {
        this.context = c;
    }

    public void RecommendOption() {
        new AlertDialog.Builder(this.context).setTitle((int) R.string.app_name).setIcon((int) R.drawable.icon).setItems(this.softName, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Recommend.this.OpenDownLoad(Recommend.this.packName[which]);
            }
        }).setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
            }
        }).show();
    }

    public void OpenDownLoad(String packageName) {
        try {
            PackageManager pm = this.context.getPackageManager();
            pm.getApplicationInfo(packageName, 8192);
            this.context.startActivity(new Intent(pm.getLaunchIntentForPackage(packageName)));
        } catch (Exception e) {
            try {
                this.context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + packageName)));
            } catch (ActivityNotFoundException e2) {
                try {
                    this.context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=" + packageName)));
                } catch (ActivityNotFoundException e3) {
                }
            }
        }
    }
}
