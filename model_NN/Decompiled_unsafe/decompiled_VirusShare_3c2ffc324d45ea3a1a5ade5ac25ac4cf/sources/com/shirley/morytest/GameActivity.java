package com.shirley.morytest;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import com.admogo.AdMogoLayout;
import com.admogo.AdMogoManager;
import com.mobclick.android.MobclickAgent;

public class GameActivity extends Activity {
    Sound bgMusic;
    GameView gv;
    private Store store;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        MobclickAgent.onError(this);
        MobclickAgent.update(this);
        MobclickAgent.setUpdateOnlyWifi(false);
        WindowManager manager = getWindowManager();
        int width = manager.getDefaultDisplay().getWidth();
        int height = manager.getDefaultDisplay().getHeight();
        this.store = new Store(this);
        int isMusic = this.store.load("music");
        int isEffect = this.store.load("effect");
        if (isMusic == 1) {
            AppData.bPlayBg = true;
        } else {
            AppData.bPlayBg = false;
        }
        if (isEffect == 1) {
            AppData.bPlayEffect = true;
        } else {
            AppData.bPlayEffect = false;
        }
        int iLevel = this.store.load("level");
        if (iLevel < 1) {
            iLevel = 1;
        }
        if (iLevel > 20) {
            iLevel = 20;
        }
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(1);
        linearLayout.setBackgroundColor(Color.rgb(202, 136, 54));
        this.gv = new GameView(this, width, height - 15, iLevel);
        int iViewHeight = this.gv.getViewHeight();
        LinearLayout linearLayout2 = new LinearLayout(this);
        this.gv.pb = new ProgressBar(this, null, 16842872);
        this.gv.pb.setLayoutParams(new LinearLayout.LayoutParams(-1, 15));
        linearLayout2.addView(this.gv.pb);
        linearLayout.addView(linearLayout2);
        Log.e("viewHeight", new StringBuilder(String.valueOf(iViewHeight)).toString());
        LinearLayout linearLayout3 = new LinearLayout(this);
        linearLayout3.setLayoutParams(new LinearLayout.LayoutParams(width, iViewHeight));
        linearLayout3.addView(this.gv);
        linearLayout.addView(linearLayout3);
        AdMogoLayout adMogoLayout = new AdMogoLayout(this, "f72429c0a326464d92a8386370d97acd", 1);
        adMogoLayout.setMaxWidth(width);
        adMogoLayout.setMaxHeight(height);
        LinearLayout linearLayout4 = new LinearLayout(this);
        linearLayout4.setBackgroundResource(R.drawable.bottom);
        linearLayout4.addView(adMogoLayout, new RelativeLayout.LayoutParams(-2, -2));
        linearLayout4.invalidate();
        linearLayout.addView(linearLayout4, new RelativeLayout.LayoutParams(-2, (width - iViewHeight) - 15));
        setContentView(linearLayout);
        this.gv.start();
    }

    public void start() {
        this.gv.start();
    }

    public void newGame() {
        this.gv.newGame(1);
    }

    public void pauseGame() {
        this.gv.pause();
    }

    public boolean onMenuOpened(int featureId, Menu menu) {
        GridDialog dialog = new GridDialog(this);
        dialog.bindEvent(this);
        dialog.show();
        return false;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, (int) R.string.newgame);
        return super.onCreateOptionsMenu(menu);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        this.gv.removeCallbacks(this.gv);
        this.bgMusic.destroyBg();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        this.gv.run();
        this.bgMusic = new Sound(this);
        if (this.store.load("music") == 1) {
            this.bgMusic.playBg();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        AdMogoManager.clear();
    }
}
