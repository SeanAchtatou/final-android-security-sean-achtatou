package com.shirley.morytest;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int about = 2130837504;
        public static final int bg = 2130837505;
        public static final int bottom = 2130837506;
        public static final int button_bg = 2130837507;
        public static final int button_bg_sel = 2130837508;
        public static final int exit = 2130837509;
        public static final int fa = 2130837510;
        public static final int fb = 2130837511;
        public static final int fc = 2130837512;
        public static final int fd = 2130837513;
        public static final int fe = 2130837514;
        public static final int ff = 2130837515;
        public static final int fg = 2130837516;
        public static final int fh = 2130837517;
        public static final int fi = 2130837518;
        public static final int fj = 2130837519;
        public static final int fk = 2130837520;
        public static final int fl = 2130837521;
        public static final int fm = 2130837522;
        public static final int fn = 2130837523;
        public static final int fo = 2130837524;
        public static final int fp = 2130837525;
        public static final int g = 2130837526;
        public static final int grid_bg = 2130837527;
        public static final int help = 2130837528;
        public static final int ic_spinner1 = 2130837529;
        public static final int ic_spinner2 = 2130837530;
        public static final int ic_spinner3 = 2130837531;
        public static final int ic_spinner4 = 2130837532;
        public static final int ic_spinner5 = 2130837533;
        public static final int ic_spinner6 = 2130837534;
        public static final int ic_spinner7 = 2130837535;
        public static final int ic_spinner8 = 2130837536;
        public static final int icon = 2130837537;
        public static final int imgbg = 2130837538;
        public static final int loadbg = 2130837539;
        public static final int loading = 2130837540;
        public static final int music = 2130837541;
        public static final int newgame = 2130837542;
        public static final int push_icon = 2130837543;
        public static final int rearrage = 2130837544;
        public static final int setbg = 2130837545;
        public static final int titleicon = 2130837546;
        public static final int top = 2130837547;
        public static final int tytle = 2130837548;
        public static final int unit = 2130837549;
    }

    public static final class id {
        public static final int RelativeLayout01 = 2131099652;
        public static final int RelativeLayout02 = 2131099648;
        public static final int about_text = 2131099649;
        public static final int app = 2131099662;
        public static final int appIcon = 2131099663;
        public static final int content_text = 2131099661;
        public static final int description = 2131099667;
        public static final int help_text = 2131099655;
        public static final int iconloading = 2131099669;
        public static final int item_image = 2131099653;
        public static final int item_text = 2131099654;
        public static final int layout_root = 2131099650;
        public static final int loadimgview = 2131099656;
        public static final int mygridview = 2131099651;
        public static final int notification = 2131099665;
        public static final int notify_image = 2131099659;
        public static final int notify_layout = 2131099657;
        public static final int notify_text = 2131099660;
        public static final int progress_bar = 2131099668;
        public static final int progress_text = 2131099664;
        public static final int relative = 2131099658;
        public static final int title = 2131099666;
    }

    public static final class layout {
        public static final int aboutdialog = 2130903040;
        public static final int button = 2130903041;
        public static final int grid_dialog = 2130903042;
        public static final int grid_item = 2130903043;
        public static final int helpdialog = 2130903044;
        public static final int main = 2130903045;
        public static final int push_layout = 2130903046;
        public static final int umeng_download_notification = 2130903047;
    }

    public static final class raw {
        public static final int effect = 2130968576;
        public static final int fail = 2130968577;
        public static final int music = 2130968578;
        public static final int pass = 2130968579;
    }

    public static final class string {
        public static final int about = 2131034119;
        public static final int aboutstr = 2131034124;
        public static final int app_name = 2131034113;
        public static final int ci = 2131034133;
        public static final int close = 2131034120;
        public static final int di = 2131034129;
        public static final int fail = 2131034132;
        public static final int gameover = 2131034125;
        public static final int guan = 2131034130;
        public static final int hello = 2131034112;
        public static final int help = 2131034117;
        public static final int helpstr = 2131034123;
        public static final int music = 2131034118;
        public static final int newgame = 2131034121;
        public static final int next = 2131034115;
        public static final int ok = 2131034116;
        public static final int pass1 = 2131034127;
        public static final int pass2 = 2131034128;
        public static final int remembermsg = 2131034126;
        public static final int restart = 2131034114;
        public static final int todo = 2131034131;
        public static final int tytle = 2131034122;
    }
}
