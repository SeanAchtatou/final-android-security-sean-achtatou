package com.shirley.morytest;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GridDialog extends Dialog {
    Context context;
    private List<int[]> griditem = new ArrayList();
    private GridView gridview;

    public GridDialog(Context cxt, boolean cancelable, DialogInterface.OnCancelListener cancelListener) {
        super(cxt, cancelable, cancelListener);
        this.griditem.add(new int[]{R.drawable.tytle, R.string.tytle});
        this.griditem.add(new int[]{R.drawable.help, R.string.help});
        this.griditem.add(new int[]{R.drawable.music, R.string.music});
        this.griditem.add(new int[]{R.drawable.newgame, R.string.newgame});
        this.griditem.add(new int[]{R.drawable.about, R.string.about});
        this.griditem.add(new int[]{R.drawable.exit, R.string.close});
        this.context = cxt;
    }

    public GridDialog(Context cxt, int theme) {
        super(cxt, theme);
        this.griditem.add(new int[]{R.drawable.tytle, R.string.tytle});
        this.griditem.add(new int[]{R.drawable.help, R.string.help});
        this.griditem.add(new int[]{R.drawable.music, R.string.music});
        this.griditem.add(new int[]{R.drawable.newgame, R.string.newgame});
        this.griditem.add(new int[]{R.drawable.about, R.string.about});
        this.griditem.add(new int[]{R.drawable.exit, R.string.close});
        this.context = cxt;
    }

    private void initGrid() {
        List<Map<String, Object>> items = new ArrayList<>();
        for (int[] item : this.griditem) {
            Map<String, Object> map = new HashMap<>();
            map.put("image", Integer.valueOf(item[0]));
            map.put("title", getContext().getString(item[1]));
            items.add(map);
        }
        SimpleAdapter adapter = new SimpleAdapter(getContext(), items, R.layout.grid_item, new String[]{"title", "image"}, new int[]{R.id.item_text, R.id.item_image});
        this.gridview = (GridView) findViewById(R.id.mygridview);
        this.gridview.setAdapter((ListAdapter) adapter);
    }

    public GridDialog(Context cxt) {
        super(cxt);
        this.griditem.add(new int[]{R.drawable.tytle, R.string.tytle});
        this.griditem.add(new int[]{R.drawable.help, R.string.help});
        this.griditem.add(new int[]{R.drawable.music, R.string.music});
        this.griditem.add(new int[]{R.drawable.newgame, R.string.newgame});
        this.griditem.add(new int[]{R.drawable.about, R.string.about});
        this.griditem.add(new int[]{R.drawable.exit, R.string.close});
        this.context = cxt;
        requestWindowFeature(1);
        setContentView((int) R.layout.grid_dialog);
        setCanceledOnTouchOutside(true);
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        getWindow().setAttributes(lp);
        getWindow().addFlags(128);
        lp.alpha = 1.0f;
        lp.dimAmount = 0.3f;
        new DisplayMetrics();
        lp.y = this.context.getResources().getDisplayMetrics().heightPixels - 200;
        initGrid();
    }

    public void bindEvent(Activity activity) {
        setOwnerActivity(activity);
        this.gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
                switch (position) {
                    case 0:
                        ((GameActivity) GridDialog.this.context).pauseGame();
                        GridDialog.this.dismiss();
                        return;
                    case 1:
                        GridDialog.this.help();
                        GridDialog.this.dismiss();
                        return;
                    case 2:
                        Log.e("ggg", "music");
                        GridDialog.this.MusicOption();
                        GridDialog.this.dismiss();
                        return;
                    case 3:
                        ((GameActivity) GridDialog.this.context).newGame();
                        GridDialog.this.dismiss();
                        return;
                    case 4:
                        new Recommend(GridDialog.this.context).RecommendOption();
                        GridDialog.this.dismiss();
                        return;
                    case 5:
                        ((GameActivity) GridDialog.this.context).finish();
                        GridDialog.this.dismiss();
                        return;
                    default:
                        return;
                }
            }
        });
    }

    private void redirect(Class<?> cls) {
        if (getOwnerActivity().getClass() != cls) {
            dismiss();
            Intent intent = new Intent();
            intent.setClass(getContext(), cls);
            getContext().startActivity(intent);
        }
    }

    /* access modifiers changed from: private */
    public void MusicOption() {
        final Store store = new Store(this.context);
        int isMusic = store.load("music");
        int isEffect = store.load("effect");
        boolean musicOn = false;
        boolean effectOn = false;
        if (isMusic == 1) {
            musicOn = true;
        }
        if (isEffect == 1) {
            effectOn = true;
        }
        new AlertDialog.Builder(this.context).setTitle((int) R.string.app_name).setIcon((int) R.drawable.titleicon).setMultiChoiceItems(new String[]{"背景音乐", "游戏音效"}, new boolean[]{musicOn, effectOn}, new DialogInterface.OnMultiChoiceClickListener() {
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if (which == 0) {
                    ((GameActivity) GridDialog.this.context).bgMusic.switchBg();
                    if (isChecked) {
                        store.save("music", 1);
                        AppData.bPlayBg = true;
                        return;
                    }
                    store.save("music", 0);
                    AppData.bPlayBg = false;
                } else if (isChecked) {
                    store.save("effect", 1);
                    AppData.bPlayEffect = true;
                } else {
                    store.save("effect", 0);
                    AppData.bPlayEffect = false;
                }
            }
        }).setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
            }
        }).show();
    }

    /* access modifiers changed from: private */
    public void help() {
        new AlertDialog.Builder(this.context).setTitle((int) R.string.help).setIcon((int) R.drawable.titleicon).setView(LayoutInflater.from(this.context).inflate((int) R.layout.helpdialog, (ViewGroup) null)).setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).create().show();
    }

    private void about() {
        new AlertDialog.Builder(this.context).setTitle((int) R.string.about).setIcon((int) R.drawable.titleicon).setView(LayoutInflater.from(this.context).inflate((int) R.layout.aboutdialog, (ViewGroup) null)).setPositiveButton((int) R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).create().show();
    }
}
