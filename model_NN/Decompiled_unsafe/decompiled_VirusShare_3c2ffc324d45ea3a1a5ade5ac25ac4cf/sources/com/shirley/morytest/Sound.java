package com.shirley.morytest;

import android.content.Context;
import android.media.MediaPlayer;
import android.util.Log;

public class Sound {
    private Context context;
    public int isMusic;
    private MediaPlayer mp_bg;

    public Sound(Context c) {
        this.context = c;
        initMp();
    }

    public void initMp() {
        this.mp_bg = MediaPlayer.create(this.context, (int) R.raw.music);
    }

    public void playBg() {
        this.mp_bg.setLooping(true);
        this.mp_bg.start();
    }

    public void switchBg() {
        if (this.mp_bg.isPlaying()) {
            this.mp_bg.pause();
            Log.v("pause", "pause");
            return;
        }
        playBg();
    }

    public void destroyBg() {
        this.mp_bg.stop();
    }

    public void StopBg() {
        this.mp_bg.stop();
    }
}
