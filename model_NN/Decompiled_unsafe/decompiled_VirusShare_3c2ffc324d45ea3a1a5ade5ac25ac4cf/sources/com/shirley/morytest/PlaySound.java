package com.shirley.morytest;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import java.util.HashMap;

public class PlaySound {
    public static final int SOUND_FAIL = 2;
    public static final int SOUND_HIT = 1;
    public static final int SOUND_PASS = 3;
    private Context ctx;
    private SoundPool soundPool;
    private HashMap<Integer, Integer> soundPoolMap;

    public PlaySound(Context context) {
        this.ctx = context;
        initSounds();
    }

    private void initSounds() {
        this.soundPool = new SoundPool(4, 3, 100);
        this.soundPoolMap = new HashMap<>();
        this.soundPoolMap.put(1, Integer.valueOf(this.soundPool.load(this.ctx, R.raw.effect, 1)));
        this.soundPoolMap.put(2, Integer.valueOf(this.soundPool.load(this.ctx, R.raw.fail, 1)));
        this.soundPoolMap.put(3, Integer.valueOf(this.soundPool.load(this.ctx, R.raw.pass, 1)));
    }

    public void playSound(int sound) {
        AudioManager mgr = (AudioManager) this.ctx.getSystemService("audio");
        float volume = ((float) mgr.getStreamVolume(3)) / ((float) mgr.getStreamMaxVolume(3));
        this.soundPool.play(this.soundPoolMap.get(Integer.valueOf(sound)).intValue(), volume, volume, 1, 0, 1.0f);
    }

    public void PlayHit() {
        playSound(1);
    }

    public void PlayFail() {
        playSound(2);
    }

    public void PlayPass() {
        playSound(3);
    }
}
