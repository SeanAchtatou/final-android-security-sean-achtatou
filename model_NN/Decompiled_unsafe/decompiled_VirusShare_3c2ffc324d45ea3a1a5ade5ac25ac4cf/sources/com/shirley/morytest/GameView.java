package com.shirley.morytest;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.madhouse.android.ads.AdView;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class GameView extends View implements Runnable {
    final int ST_OK = 3;
    final int ST_PAUSE = 0;
    final int ST_PLAYING = 2;
    final int ST_STOP = 1;
    boolean bCanClick = false;
    boolean bCheckOk = false;
    boolean bNeedCheck = false;
    public Bitmap bmBg;
    public Bitmap bmDefault;
    public Bitmap bmImg;
    public Bitmap bmTop;
    int curProgress = this.iMaxTime;
    private Context cxt;
    private int delay = 20;
    PlaySound efMusic;
    int gameState = 1;
    public int iBoxW = 20;
    int iClickTimes = 0;
    public int iCol = 4;
    private int iCurrentX = -1;
    private int iCurrentY = -1;
    public int iDis = 10;
    public int iLevel = 1;
    int iMaxTime = 100000;
    private int iNewX = -1;
    private int iNewY = -1;
    private int iOldX = -1;
    private int iOldY = -1;
    public int iRow = 6;
    private int iScreen_H = 800;
    private int iScreen_W = AdView.PHONE_AD_MEASURE_480;
    int iTime = 0;
    int iTop = 99;
    int iYesTimes = 0;
    public Bitmap[] image;
    public int[] imageType = {R.drawable.fa, R.drawable.fb, R.drawable.fc, R.drawable.fd, R.drawable.fe, R.drawable.ff, R.drawable.fg, R.drawable.fh, R.drawable.fi, R.drawable.fj, R.drawable.fk, R.drawable.fl, R.drawable.fm, R.drawable.fn, R.drawable.fo, R.drawable.fp};
    public ProgressBar pb;
    public MyPoint[][] point = ((MyPoint[][]) Array.newInstance(MyPoint.class, this.iRow, this.iCol));
    int progressTime = this.iMaxTime;
    public List<Integer> type = new ArrayList();

    public class MyPoint {
        public boolean bOk;
        public boolean bShow;
        public int iType;
        public int x;
        public int y;

        public MyPoint(int newx, int newy, int iType2, boolean bOk2, boolean bShow2) {
            this.bOk = false;
            this.iType = -1;
            this.bShow = false;
            this.x = newx;
            this.y = newy;
            this.bOk = bOk2;
            this.bShow = bShow2;
            this.iType = iType2;
        }

        public MyPoint() {
            this.bOk = false;
            this.iType = -1;
            this.bShow = false;
            this.x = -1;
            this.y = -1;
            this.bOk = false;
            this.iType = -1;
        }

        public boolean equals(MyPoint p) {
            if (p.x == this.x && p.y == this.y) {
                return true;
            }
            return false;
        }

        public boolean isOk() {
            return this.bOk;
        }

        public void setIsOk(boolean bOk2) {
            this.bOk = bOk2;
        }

        public int getType() {
            return this.iType;
        }

        public void setType(int iType2) {
            this.iType = iType2;
        }

        public void setXy(int ix, int iy) {
            this.x = ix;
            this.y = iy;
        }
    }

    public GameView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFocusable(true);
        setFocusableInTouchMode(true);
        this.cxt = context;
        new Thread(this).start();
    }

    public GameView(Context context, int w, int h, int iLevel2) {
        super(context);
        this.cxt = context;
        setSize(w, h);
    }

    public GameView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setFocusable(true);
        setFocusableInTouchMode(true);
        this.cxt = context;
        new Thread(this).start();
    }

    public void initType() {
        Log.e("test", "initType");
        int size = this.iRow * this.iCol;
        int len = this.imageType.length;
        this.type.clear();
        int p = 0;
        for (int k = 0; k < size / 2; k++) {
            this.type.add(Integer.valueOf(this.imageType[p]));
            this.type.add(Integer.valueOf(this.imageType[p]));
            p++;
            if (p == len + 1) {
                p = 0;
            }
        }
    }

    public void initPoint() {
        Log.e("test", "initPoint");
        new Random();
        for (int i = 0; i < this.iRow; i++) {
            for (int j = 0; j < this.iCol; j++) {
                this.point[i][j] = new MyPoint();
                this.point[i][j].setXy((this.iDis * (j + 1)) + (this.iBoxW * j), (this.iDis * (i + 1)) + (this.iBoxW * i) + this.iTop);
            }
        }
    }

    public void reset() {
        Log.e("test", "reset");
        Random ad = new Random();
        for (int i = 0; i < this.iRow; i++) {
            for (int j = 0; j < this.iCol; j++) {
                if (this.type == null || this.type.size() <= 0) {
                    this.point[i][j].iType = -1;
                } else {
                    int index = ad.nextInt(this.type.size());
                    this.point[i][j].iType = this.type.get(index).intValue();
                    this.type.remove(index);
                }
            }
        }
        Log.e("tttt", "setover");
    }

    public void fillImage() {
        int lth = this.imageType.length;
        this.image = new Bitmap[lth];
        for (int i = 0; i < lth; i++) {
            Bitmap bitmap = Bitmap.createBitmap(this.iBoxW - 4, this.iBoxW - 4, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            Drawable drw = this.cxt.getResources().getDrawable(this.imageType[i]);
            drw.setBounds(2, 2, this.iBoxW - 6, this.iBoxW - 6);
            drw.draw(canvas);
            this.image[i] = bitmap;
        }
        Bitmap bitmap2 = Bitmap.createBitmap(this.iBoxW, this.iBoxW, Bitmap.Config.ARGB_8888);
        Canvas canvas2 = new Canvas(bitmap2);
        Drawable drw2 = this.cxt.getResources().getDrawable(R.drawable.unit);
        drw2.setBounds(0, 0, this.iBoxW, this.iBoxW);
        drw2.draw(canvas2);
        this.bmDefault = bitmap2;
        Bitmap bitmap3 = Bitmap.createBitmap(this.iScreen_W, this.iTop, Bitmap.Config.ARGB_8888);
        Canvas canvas3 = new Canvas(bitmap3);
        Drawable drw3 = this.cxt.getResources().getDrawable(R.drawable.top);
        drw3.setBounds(0, 0, this.iScreen_W, this.iTop);
        drw3.draw(canvas3);
        this.bmTop = bitmap3;
        Bitmap bitmap4 = Bitmap.createBitmap(this.iBoxW, this.iBoxW, Bitmap.Config.ARGB_8888);
        Canvas canvas4 = new Canvas(bitmap4);
        Drawable drw4 = this.cxt.getResources().getDrawable(R.drawable.imgbg);
        drw4.setBounds(0, 0, this.iBoxW, this.iBoxW);
        drw4.draw(canvas4);
        this.bmImg = bitmap4;
        Bitmap bitmap5 = Bitmap.createBitmap(this.iScreen_W, this.iScreen_H - this.iTop, Bitmap.Config.ARGB_8888);
        Canvas canvas5 = new Canvas(bitmap5);
        Drawable drw5 = this.cxt.getResources().getDrawable(R.drawable.bg);
        drw5.setBounds(0, 0, this.iScreen_W, this.iScreen_H - this.iTop);
        drw5.draw(canvas5);
        this.bmBg = bitmap5;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        new Paint();
        canvas.clipRect(0, 0, this.iScreen_W, this.iScreen_H);
        canvas.drawBitmap(this.bmTop, 0.0f, 0.0f, (Paint) null);
        canvas.drawBitmap(this.bmBg, 0.0f, (float) this.iTop, (Paint) null);
        for (int i = 0; i < this.iRow; i++) {
            for (int j = 0; j < this.iCol; j++) {
                if (!this.point[i][j].bShow) {
                    canvas.drawBitmap(this.bmDefault, (float) this.point[i][j].x, (float) this.point[i][j].y, (Paint) null);
                } else if (this.point[i][j].iType > 0) {
                    canvas.drawBitmap(this.bmImg, (float) this.point[i][j].x, (float) this.point[i][j].y, (Paint) null);
                    canvas.drawBitmap(this.image[Arrays.binarySearch(this.imageType, this.point[i][j].iType)], (float) (this.point[i][j].x + 1), (float) (this.point[i][j].y + 1), (Paint) null);
                } else {
                    canvas.drawBitmap(this.bmDefault, (float) this.point[i][j].x, (float) this.point[i][j].y, (Paint) null);
                }
            }
        }
        Paint hilite = new Paint();
        hilite.setTextSize(30.0f);
        hilite.setColor(-1);
        hilite.setFlags(1);
        hilite.setARGB(255, 255, 255, 255);
        canvas.drawText(String.valueOf(getResources().getString(R.string.di)) + this.iLevel + getResources().getString(R.string.guan), (float) ((this.iScreen_W / 2) - 50), 28.0f, hilite);
        hilite.setARGB(255, 249, 11, 14);
        hilite.setTextSize(20.0f);
        canvas.drawText(String.valueOf(getResources().getString(R.string.todo)) + this.iClickTimes + getResources().getString(R.string.ci), 22.0f, 28.0f, hilite);
        canvas.drawText(String.valueOf(getResources().getString(R.string.fail)) + (this.iClickTimes - this.iYesTimes) + getResources().getString(R.string.ci), 22.0f, 48.0f, hilite);
        super.onDraw(canvas);
    }

    /* access modifiers changed from: protected */
    public void setSize(int w, int h) {
        this.iScreen_W = w;
        this.iScreen_H = h;
        this.iBoxW = (w - (this.iDis * (this.iCol + 1))) / this.iCol;
        this.iRow = ((h - this.iTop) - 60) / (this.iBoxW + this.iDis);
        this.iScreen_H = this.iTop + (this.iRow * this.iBoxW) + ((this.iRow + 1) * this.iDis);
        fillImage();
    }

    public int getViewHeight() {
        return this.iScreen_H;
    }

    public void showToast() {
        Toast.makeText(this.cxt, getResources().getString(R.string.remembermsg), 1).show();
    }

    public void start() {
        this.iClickTimes = 0;
        this.iYesTimes = 0;
        initType();
        initPoint();
        reset();
        changeAllPointState(false, true);
        this.progressTime = this.iMaxTime - ((this.iLevel - 1) * 1000);
        if (this.progressTime < 50000) {
            this.progressTime = 50000;
        }
        this.curProgress = this.progressTime;
        this.pb.setMax(this.progressTime);
        this.pb.setProgress(this.curProgress);
        this.pb.setSecondaryProgress(0);
        this.pb.setBackgroundColor(Color.rgb(202, 136, 54));
        this.bCanClick = false;
        this.gameState = 2;
        this.efMusic = new PlaySound(this.cxt);
        new Thread(this).start();
        showToast();
    }

    public void newGame(int iLv) {
        this.iClickTimes = 0;
        this.iYesTimes = 0;
        this.iLevel = iLv;
        initType();
        reset();
        changeAllPointState(false, true);
        this.progressTime = 100000 - ((this.iLevel - 1) * 10);
        this.curProgress = this.progressTime;
        this.pb.setMax(this.progressTime);
        this.pb.setProgress(this.curProgress);
        this.pb.setSecondaryProgress(0);
        this.bCanClick = false;
        this.gameState = 2;
        run();
        showToast();
    }

    public void pause() {
        if (this.gameState == 0) {
            this.gameState = 2;
        } else if (this.gameState == 2) {
            this.gameState = 0;
        }
    }

    public void changeAllPointState(boolean bOk, boolean bShow) {
        for (int i = 0; i < this.iRow; i++) {
            for (int j = 0; j < this.iCol; j++) {
                this.point[i][j].bOk = bOk;
                this.point[i][j].bShow = bShow;
            }
        }
    }

    public void setContext(Context context) {
        Log.e("test", "setcontext");
        this.cxt = context;
    }

    /* access modifiers changed from: protected */
    public boolean checkOk() {
        for (int i = 0; i < this.iRow; i++) {
            for (int j = 0; j < this.iCol; j++) {
                if (!this.point[i][j].bOk) {
                    return false;
                }
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void select(int ix, int iy) {
        for (int i = 0; i < this.iRow; i++) {
            int j = 0;
            while (j < this.iCol) {
                if (ix < this.point[i][j].x || ix > this.point[i][j].x + this.iBoxW || iy < this.point[i][j].y || iy > this.point[i][j].y + this.iBoxW) {
                    j++;
                } else {
                    this.iCurrentX = i;
                    this.iCurrentY = j;
                    return;
                }
            }
        }
        this.iCurrentX = -1;
        this.iCurrentY = -1;
    }

    public boolean onTouchEvent(MotionEvent event) {
        boolean z;
        if (!this.bCanClick) {
            return super.onTouchEvent(event);
        }
        switch (event.getAction()) {
            case 0:
                if (this.bCheckOk) {
                    this.iTime = -1;
                    check();
                    this.bCheckOk = false;
                }
                select((int) event.getX(), (int) event.getY());
                if (this.iCurrentX == -1 || this.iCurrentY == -1 || !this.point[this.iCurrentX][this.iCurrentY].bOk) {
                    boolean bYes = false;
                    if (!this.bNeedCheck && this.iCurrentX != -1 && this.iCurrentY != -1 && !this.point[this.iCurrentX][this.iCurrentY].bShow) {
                        this.point[this.iCurrentX][this.iCurrentY].bShow = true;
                        bYes = true;
                    }
                    if (this.bNeedCheck && !((this.iCurrentX == this.iOldX && this.iCurrentY == this.iOldY) || this.iCurrentX == -1 || this.iCurrentY == -1)) {
                        if (!this.point[this.iCurrentX][this.iCurrentY].bShow) {
                            this.point[this.iCurrentX][this.iCurrentY].bShow = true;
                        }
                        this.bCheckOk = true;
                        this.iTime = 15;
                    }
                    if (bYes && !this.bNeedCheck && this.iCurrentX != -1 && this.iCurrentY != -1) {
                        this.iOldX = this.iCurrentX;
                        this.iOldY = this.iCurrentY;
                        if (this.bNeedCheck) {
                            z = false;
                        } else {
                            z = true;
                        }
                        this.bNeedCheck = z;
                    }
                    if (!(this.iCurrentX == -1 || this.iCurrentY == -1 || !AppData.bPlayEffect)) {
                        this.efMusic.PlayHit();
                        break;
                    }
                } else {
                    return false;
                }
                break;
        }
        return super.onTouchEvent(event);
    }

    public void check() {
        boolean z;
        this.iClickTimes++;
        Log.e("check", "check");
        if (this.point[this.iOldX][this.iOldY].iType == this.point[this.iCurrentX][this.iCurrentY].iType) {
            this.point[this.iOldX][this.iOldY].bOk = true;
            this.point[this.iCurrentX][this.iCurrentY].bOk = true;
            this.point[this.iOldX][this.iOldY].bShow = true;
            this.point[this.iCurrentX][this.iCurrentY].bShow = true;
            this.iYesTimes++;
        } else {
            this.point[this.iOldX][this.iOldY].bOk = false;
            this.point[this.iCurrentX][this.iCurrentY].bOk = false;
            this.point[this.iOldX][this.iOldY].bShow = false;
            this.point[this.iCurrentX][this.iCurrentY].bShow = false;
        }
        if (!(this.iCurrentX == -1 || this.iCurrentY == -1)) {
            this.iOldX = this.iCurrentX;
            this.iOldY = this.iCurrentY;
            if (this.bNeedCheck) {
                z = false;
            } else {
                z = true;
            }
            this.bNeedCheck = z;
        }
        if (checkOk()) {
            this.gameState = 3;
        }
        this.bCheckOk = false;
    }

    public void nextLevel() {
        if (AppData.bPlayEffect) {
            this.efMusic.PlayPass();
        }
        new AlertDialog.Builder(this.cxt).setTitle((int) R.string.app_name).setMessage(String.valueOf(getResources().getString(R.string.pass1)) + this.iLevel + getResources().getString(R.string.pass2)).setPositiveButton((int) R.string.next, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
                GameView.this.iLevel++;
                GameView.this.newGame(GameView.this.iLevel);
            }
        }).show();
    }

    public void gameOver() {
        Log.e("over", "over");
        if (AppData.bPlayEffect) {
            this.efMusic.PlayFail();
        }
        this.bCheckOk = false;
        this.bCanClick = false;
        this.gameState = 1;
        new AlertDialog.Builder(this.cxt).setTitle((int) R.string.app_name).setMessage((int) R.string.gameover).setPositiveButton((int) R.string.restart, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
                GameView.this.newGame(GameView.this.iLevel);
            }
        }).setNeutralButton((int) R.string.close, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
                System.exit(0);
            }
        }).show();
    }

    public void MyThread() {
        this.curProgress -= this.delay;
        if (this.curProgress <= 0) {
            gameOver();
        } else if (this.curProgress == this.progressTime - (this.delay * 350)) {
            changeAllPointState(false, false);
            this.bCanClick = true;
            postDelayed(this, (long) this.delay);
            this.pb.setProgress(this.curProgress);
        } else if (this.gameState == 3) {
            nextLevel();
            this.gameState = 1;
        } else {
            postInvalidate();
            postDelayed(this, (long) this.delay);
            this.pb.setProgress(this.curProgress);
        }
    }

    public void run() {
        if (this.gameState == 2 || this.gameState == 3) {
            Log.e("ttt", "play");
            if (this.bCheckOk) {
                this.iTime--;
                if (this.iTime < 0) {
                    check();
                    this.bCheckOk = false;
                }
            }
            MyThread();
            return;
        }
        Log.e("ttt", "pause");
        postInvalidate();
        postDelayed(this, (long) this.delay);
    }
}
