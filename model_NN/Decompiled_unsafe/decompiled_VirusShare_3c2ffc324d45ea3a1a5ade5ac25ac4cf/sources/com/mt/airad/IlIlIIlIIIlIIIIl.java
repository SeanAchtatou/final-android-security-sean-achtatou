package com.mt.airad;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ParseException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

class IlIlIIlIIIlIIIIl {
    private static IlIlIIlIIIlIIIIl _$1 = null;
    private HttpClient _$2;
    private IIlllIllllIIllll _$3;
    private List<NameValuePair> _$4;
    private HttpResponse _$5;

    private IlIlIIlIIIlIIIIl() {
        this._$5 = null;
        this._$4 = null;
    }

    private IlIlIIlIIIlIIIIl(AirAD airAD) {
        this._$5 = null;
        this._$4 = null;
        this._$3 = IIlllIllllIIllll._$1();
    }

    protected static IlIlIIlIIIlIIIIl _$1(AirAD airAD) {
        if (_$1 == null) {
            _$1 = new IlIlIIlIIIlIIIIl(airAD);
        }
        return _$1;
    }

    private String _$1(String str) {
        return IllllIlIlIllIIIl._$1(str);
    }

    private void _$1() {
        if (this._$2 != null && this._$2.getConnectionManager() != null) {
            this._$2.getConnectionManager().shutdown();
        }
    }

    private String _$2(String str) {
        return IllllIlIlIllIIIl._$2(str);
    }

    private HttpClient _$2() {
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpProtocolParams.setVersion(basicHttpParams, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setContentCharset(basicHttpParams, "ISO-8859-1");
        HttpProtocolParams.setUserAgent(basicHttpParams, "airad-1.2/java/android");
        HttpProtocolParams.setUseExpectContinue(basicHttpParams, true);
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        schemeRegistry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
        return new DefaultHttpClient(new ThreadSafeClientConnManager(basicHttpParams, schemeRegistry), basicHttpParams);
    }

    protected static IlIlIIlIIIlIIIIl _$3() {
        if (_$1 == null) {
            _$1 = new IlIlIIlIIIlIIIIl();
        }
        return _$1;
    }

    /* access modifiers changed from: protected */
    public HashMap<String, Bitmap> _$1(HashMap<String, String> hashMap) {
        HashMap<String, Bitmap> hashMap2 = new HashMap<>();
        try {
            String[] strArr = this._$3._$20;
            for (int i = 0; i < strArr.length; i++) {
                if (hashMap.containsKey(strArr[i])) {
                    HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(hashMap.get(strArr[i])).openConnection();
                    httpURLConnection.setRequestMethod("GET");
                    httpURLConnection.setConnectTimeout(10000);
                    httpURLConnection.setDoInput(true);
                    httpURLConnection.connect();
                    if (httpURLConnection.getResponseCode() == 200) {
                        InputStream inputStream = httpURLConnection.getInputStream();
                        hashMap2.put(strArr[i], BitmapFactory.decodeStream(inputStream));
                        inputStream.close();
                    }
                }
            }
        } catch (MalformedURLException e) {
            hashMap2.clear();
        } catch (IOException e2) {
            hashMap2.clear();
        } catch (NullPointerException e3) {
            hashMap2.clear();
        }
        return hashMap2;
    }

    /* access modifiers changed from: protected */
    public JSONObject _$1(boolean z, String str, int i, String str2, String str3) {
        String entityUtils;
        if (z) {
            try {
                _$1();
                this._$2 = null;
                this._$2 = _$2();
            } catch (ClientProtocolException e) {
                lllllIllllIllIIl._$1(".");
                this._$5 = null;
                return null;
            } catch (IOException e2) {
                lllllIllllIllIIl._$1("..");
                this._$5 = null;
                return null;
            } catch (JSONException e3) {
                lllllIllllIllIIl._$1("...");
                this._$5 = null;
                return null;
            } catch (NullPointerException e4) {
                lllllIllllIllIIl._$1("....");
                this._$5 = null;
                return null;
            } catch (ParseException e5) {
                lllllIllllIllIIl._$1(".....");
                this._$5 = null;
                return null;
            } catch (IllegalStateException e6) {
                lllllIllllIllIIl._$1("......");
                this._$5 = null;
                return null;
            } catch (Throwable th) {
                this._$5 = null;
                throw th;
            }
        }
        if (this._$2 == null) {
            this._$2 = _$2();
        }
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, i);
        if (str3 == null) {
            this._$5 = null;
            return null;
        }
        HttpPost httpPost = new HttpPost(_$2(str2));
        this._$4 = new ArrayList();
        this._$4.add(new BasicNameValuePair("data", _$1(str3)));
        httpPost.setEntity(new UrlEncodedFormEntity(this._$4, "UTF-8"));
        httpPost.setParams(basicHttpParams);
        this._$5 = this._$2.execute(httpPost);
        JSONObject jSONObject = (this._$5.getStatusLine().getStatusCode() != 200 || (entityUtils = EntityUtils.toString(this._$5.getEntity())) == null) ? null : new JSONObject(_$2(entityUtils));
        this._$5 = null;
        return jSONObject;
    }
}
