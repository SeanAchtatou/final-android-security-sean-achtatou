package com.mt.airad;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

class IlllIllIIIlIIIII {
    private IlllIllIIIlIIIII() {
    }

    protected static Field _$1(Class cls, String str) {
        Class cls2 = cls;
        while (cls2 != Object.class) {
            try {
                return cls2.getDeclaredField(str);
            } catch (NoSuchFieldException e) {
                cls2 = cls2.getSuperclass();
            }
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.mt.airad.IlllIllIIIlIIIII._$1(java.lang.Class, java.lang.String):java.lang.reflect.Field
     arg types: [java.lang.Class<?>, java.lang.String]
     candidates:
      com.mt.airad.IlllIllIIIlIIIII._$1(java.lang.Object, java.lang.String):java.lang.reflect.Field
      com.mt.airad.IlllIllIIIlIIIII._$1(java.lang.Class, java.lang.String):java.lang.reflect.Field */
    protected static Field _$1(Object obj, String str) {
        return _$1((Class) obj.getClass(), str);
    }

    protected static void _$1(Field field) {
        if (!Modifier.isPublic(field.getModifiers()) || !Modifier.isPublic(field.getDeclaringClass().getModifiers())) {
            field.setAccessible(true);
        }
    }

    protected static boolean _$1(Object obj, String str, Object obj2) {
        Field _$1 = _$1(obj, str);
        if (_$1 == null) {
            return false;
        }
        _$1(_$1);
        try {
            _$1.set(obj, obj2);
            return true;
        } catch (IllegalAccessException e) {
            return false;
        }
    }
}
