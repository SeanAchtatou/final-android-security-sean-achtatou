package com.baidu;

import android.content.Context;

public class Ad {
    private String a;
    private String b;
    private String c;
    private b d;
    private String e;
    private String f;
    private String g;
    private String h;
    private ClickType i = ClickType.BROWSE;
    private String j;

    private Ad() {
    }

    static Ad a(Context context, String str, String str2, String str3, long j2, String str4, String str5, String str6, String str7) {
        Ad ad = new Ad();
        ad.c = str;
        ad.b = str2.replace("$$", " ");
        ad.a = j2 + "";
        ad.d = b.a(str4);
        ad.h = str5;
        ad.g = str6;
        ad.e = str3;
        if (str3.toLowerCase().endsWith(".apk")) {
            ad.i = ClickType.DOWNLOAD;
        }
        if (!ad.h.equals("")) {
            ad.i = ClickType.PHONE;
        }
        ad.j = str7;
        return ad;
    }

    /* access modifiers changed from: package-private */
    public b a() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public String b() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public String c() {
        return this.a;
    }

    public boolean equals(Object obj) {
        if (obj instanceof Ad) {
            return c().equals(((Ad) obj).c());
        }
        return false;
    }

    public ClickType getClickType() {
        return this.i;
    }

    public String getClickURL() {
        return this.e;
    }

    public String getDescription() {
        return this.b;
    }

    public String getPhone() {
        return this.h;
    }

    public String getPicUrl() {
        return this.j;
    }

    public String getSURL() {
        return this.f;
    }

    public String getTitle() {
        return this.c;
    }

    public AdType getType() {
        return this.d.c();
    }

    public String toString() {
        return String.format("%s; %s; %s; %s; %s", c(), a(), getTitle(), getDescription(), getPhone());
    }
}
