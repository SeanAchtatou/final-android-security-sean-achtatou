package com.baidu;

import android.util.Log;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;

class g {
    private static final SimpleDateFormat a = new SimpleDateFormat("MM-dd HH:mm:ss.SSS");

    static {
        a();
    }

    g() {
    }

    public static int a(String str) {
        if (!a(5)) {
            return -1;
        }
        c("Mobads SDK", str);
        return Log.w("Mobads SDK", str);
    }

    public static int a(String str, String str2) {
        return a(String.format("%s:: %s", str, str2));
    }

    public static int a(String str, String str2, Throwable th) {
        return b(String.format("%s:: %s", str, str2), th);
    }

    public static int a(String str, Throwable th) {
        if (!a(5)) {
            return -1;
        }
        b("Mobads SDK", str, th);
        return Log.w("Mobads SDK", str, th);
    }

    public static void a() {
        e.a("_b_sdk.log");
    }

    public static boolean a(int i) {
        return a("Mobads SDK", i);
    }

    public static boolean a(String str, int i) {
        return i >= 6;
    }

    public static int b(String str) {
        if (!a(6)) {
            return -1;
        }
        c("Mobads SDK", str);
        return Log.e("Mobads SDK", str);
    }

    public static int b(String str, String str2) {
        return b(String.format("%s:: %s", str, str2));
    }

    public static int b(String str, Throwable th) {
        if (!a(6)) {
            return -1;
        }
        b("Mobads SDK", str, th);
        return Log.e("Mobads SDK", str, th);
    }

    private static void b(String str, String str2, Throwable th) {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        th.printStackTrace(printWriter);
        c(str, str2 + "\n" + stringWriter.toString());
        printWriter.close();
        try {
            stringWriter.close();
        } catch (IOException e) {
            Log.w("Log.debug", "", e);
        }
    }

    private static synchronized void c(String str, String str2) {
        synchronized (g.class) {
        }
    }
}
