package com.baidu;

enum b {
    TEXT(1),
    IMAGE(2),
    APP(3);
    
    private int d;

    private b(int i) {
        this.d = i;
    }

    public static b a(String str) {
        return TEXT.toString().equals(str.toLowerCase()) ? TEXT : IMAGE.toString().equals(str.toLowerCase()) ? IMAGE : APP.toString().equals(str.toLowerCase()) ? APP : TEXT;
    }

    public static b[] a() {
        return (b[]) e.clone();
    }

    public int b() {
        return this.d;
    }

    public AdType c() {
        AdType adType = AdType.TEXT;
        switch (c.a[ordinal()]) {
            case 1:
            case 2:
                return AdType.TEXT;
            case 3:
                return AdType.IMAGE;
            default:
                return adType;
        }
    }

    public String toString() {
        return super.toString().toLowerCase();
    }
}
