package com.baidu;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

final class e {
    private static final Proxy a = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.0.0.172", 80));
    private static final Proxy b = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.0.0.200", 80));

    e() {
    }

    public static String a(Context context, String str) {
        return a(context, str, 40000, 60000);
    }

    public static String a(Context context, String str, int i, int i2) {
        if (str.startsWith("file:///")) {
            return d(context, str);
        }
        StringBuilder sb = new StringBuilder();
        HttpURLConnection b2 = b(context, str, i, i2);
        b2.connect();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(b2.getInputStream()));
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine == null) {
                break;
            }
            sb.append(readLine);
        }
        g.a("AdUtil.get", String.format("Header: %d/%d %d %s \t%s", Integer.valueOf(sb.length()), Integer.valueOf(b2.getContentLength()), Integer.valueOf(b2.getResponseCode()), b2.getResponseMessage(), sb));
        if (bufferedReader != null) {
            bufferedReader.close();
        }
        if (b2.getContentLength() < 0) {
            sb = new StringBuilder("{error}");
        }
        return sb.toString();
    }

    public static boolean a(String str) {
        g.a("AdUtil.deleteExt", str);
        if (!"mounted".equals(Environment.getExternalStorageState())) {
            return false;
        }
        File file = new File(Environment.getExternalStorageDirectory() + File.separator + str);
        if (file.exists()) {
            return file.delete();
        }
        return false;
    }

    private static HttpURLConnection b(Context context, String str, int i, int i2) {
        HttpURLConnection httpURLConnection;
        URL url = new URL(str);
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        NetworkInfo networkInfo = connectivityManager.getNetworkInfo(0);
        NetworkInfo networkInfo2 = connectivityManager.getNetworkInfo(1);
        if (networkInfo2 != null && networkInfo2.isAvailable()) {
            g.a("", "WIFI is available");
            httpURLConnection = (HttpURLConnection) url.openConnection();
        } else if (networkInfo == null || !networkInfo.isAvailable()) {
            httpURLConnection = null;
        } else {
            String extraInfo = networkInfo.getExtraInfo();
            String lowerCase = extraInfo != null ? extraInfo.toLowerCase() : "";
            g.a("current APN", lowerCase);
            httpURLConnection = (lowerCase.startsWith("cmwap") || lowerCase.startsWith("uniwap") || lowerCase.startsWith("3gwap")) ? (HttpURLConnection) url.openConnection(a) : lowerCase.startsWith("ctwap") ? (HttpURLConnection) url.openConnection(b) : (HttpURLConnection) url.openConnection();
        }
        httpURLConnection.setConnectTimeout(i);
        httpURLConnection.setReadTimeout(i2);
        return httpURLConnection;
    }

    public static void b(Context context, String str) {
        if (!c(context, str)) {
            b("Cannot request an ad without Internet permissions!  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"" + str + "\" />");
        }
    }

    public static void b(String str) {
        g.b("BaiduMobAds SDK", str);
        throw new SecurityException(str);
    }

    public static String c(String str) {
        byte[] bytes = str.getBytes();
        char[] cArr = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(bytes);
            byte[] digest = instance.digest();
            char[] cArr2 = new char[32];
            int i = 0;
            for (int i2 = 0; i2 < 16; i2++) {
                byte b2 = digest[i2];
                int i3 = i + 1;
                cArr2[i] = cArr[(b2 >>> 4) & 15];
                i = i3 + 1;
                cArr2[i3] = cArr[b2 & 15];
            }
            return new String(cArr2);
        } catch (NoSuchAlgorithmException e) {
            g.a("AdUtil.getMD5", "", e);
            return null;
        }
    }

    public static boolean c(Context context, String str) {
        boolean z = context.checkCallingOrSelfPermission(str) != -1;
        g.a("hasPermission ", z + " | " + str);
        return z;
    }

    private static String d(Context context, String str) {
        StringBuilder sb = new StringBuilder();
        int indexOf = str.indexOf(63);
        URLConnection openConnection = new URL(indexOf >= 0 ? str.substring(0, indexOf) : str).openConnection();
        openConnection.connect();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(openConnection.getInputStream()));
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine == null) {
                break;
            }
            sb.append(readLine);
        }
        if (bufferedReader != null) {
            bufferedReader.close();
        }
        return sb.toString();
    }
}
