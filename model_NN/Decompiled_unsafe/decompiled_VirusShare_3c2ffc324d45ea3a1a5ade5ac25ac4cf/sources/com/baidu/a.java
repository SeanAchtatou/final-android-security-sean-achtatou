package com.baidu;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.location.Location;
import android.location.LocationManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.CellLocation;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.madhouse.android.ads.AdView;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

class a {
    private static String a;
    private static String b;
    private static String c;
    private static final SimpleDateFormat d = new SimpleDateFormat("yyyyMMddHHmmss");

    a() {
    }

    static String a() {
        return Build.MODEL;
    }

    public static String a(Context context) {
        if (a == null) {
            a = a(context, "BaiduMobAd_APP_ID");
        }
        return a;
    }

    public static String a(Context context, b bVar, String str, String str2) {
        int i;
        int i2;
        int i3;
        StringBuilder sb = new StringBuilder();
        sb.append("u=default&ie=1");
        try {
            a(sb, "q", b(context));
            a(sb, "appid", a(context));
            a(sb, "at", bVar.b());
            a(sb, "n", 5);
            DisplayMetrics displayMetrics = new DisplayMetrics();
            try {
                displayMetrics = h(context);
            } catch (Exception e) {
                g.a("createAdReqURL", e);
            }
            int ceil = (int) Math.ceil((double) (((float) displayMetrics.widthPixels) * displayMetrics.density));
            int ceil2 = (int) Math.ceil((double) (displayMetrics.density * ((float) displayMetrics.heightPixels)));
            int min = Math.min(ceil, ceil2);
            switch (min) {
                case AdView.PHONE_AD_MEASURE_320 /*320*/:
                    i = 48;
                    break;
                case 400:
                    i = 85;
                    break;
                case AdView.PHONE_AD_MEASURE_480 /*480*/:
                    i = 80;
                    break;
                case AdView.PHONE_AD_MEASURE_640 /*640*/:
                    i = 96;
                    break;
                default:
                    i = (int) (((double) min) * 0.15d);
                    break;
            }
            a(sb, "w", i);
            a(sb, "h", min);
            a(sb, "tm", "512");
            a(sb, "cm", "512");
            a(sb, "md", c(context) ? 1 : 0);
            try {
                a(sb, "tp", a());
                a(sb, "brd", b());
                a(sb, "os", "android");
                a(sb, "bdr", c());
            } catch (Exception e2) {
                g.a("createAdReqURL", e2);
            }
            a(sb, "sw", ceil);
            a(sb, "sh", ceil2);
            try {
                a(sb, "sn", f(context));
            } catch (Exception e3) {
                g.a("createAdReqURL", e3);
            }
            try {
                a(sb, "nop", g(context));
            } catch (Exception e4) {
                g.a("createAdReqURL", e4);
            }
            try {
                a(sb, "cs", i(context));
            } catch (Exception e5) {
                g.a("createAdReqURL", e5);
            }
            try {
                a(sb, "v", str2);
            } catch (Exception e6) {
                g.a("createAdReqURL", e6);
            }
            try {
                a(sb, "cid", e(context));
            } catch (Exception e7) {
                g.a("createAdReqURL", e7);
            }
            try {
                a(sb, "im", d(context));
            } catch (Exception e8) {
                g.a("createAdReqURL", e8);
            }
            try {
                if (e.c(context, "android.permission.ACCESS_FINE_LOCATION")) {
                    Location lastKnownLocation = ((LocationManager) context.getSystemService("location")).getLastKnownLocation("gps");
                    g.a("createAdReqURL", "location: " + lastKnownLocation);
                    if (lastKnownLocation != null) {
                        long currentTimeMillis = System.currentTimeMillis();
                        g.a("createAdReqURL", String.format("%s - %s = %s", Long.valueOf(currentTimeMillis), Long.valueOf(lastKnownLocation.getTime()), Long.valueOf(currentTimeMillis - lastKnownLocation.getTime())));
                        a(sb, "g", String.format("%s_%s_%s", Long.valueOf(lastKnownLocation.getTime()), Double.valueOf(lastKnownLocation.getLongitude()), Double.valueOf(lastKnownLocation.getLatitude())));
                    }
                }
            } catch (Exception e9) {
                g.a("createAdReqURL", e9);
            }
            try {
                if (e.c(context, "android.permission.ACCESS_WIFI_STATE")) {
                    WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
                    if (wifiManager.isWifiEnabled()) {
                        g.a("[d]", wifiManager.getScanResults() + "");
                        int i4 = -1;
                        int i5 = Integer.MAX_VALUE;
                        int i6 = 0;
                        while (i6 < wifiManager.getScanResults().size()) {
                            ScanResult scanResult = wifiManager.getScanResults().get(i6);
                            int abs = Math.abs(scanResult.level);
                            g.a(String.format("%s %s_%s", scanResult.SSID, scanResult.BSSID, Integer.valueOf(abs)));
                            if (i5 > abs) {
                                i2 = i6;
                                i3 = abs;
                            } else {
                                i2 = i4;
                                i3 = i5;
                            }
                            i6++;
                            i5 = i3;
                            i4 = i2;
                        }
                        if (i4 >= 0) {
                            ScanResult scanResult2 = wifiManager.getScanResults().get(i4);
                            a(sb, "wi", String.format("%s_%s", scanResult2.BSSID.replace(":", "").toLowerCase(), Integer.valueOf(Math.abs(scanResult2.level))));
                            g.a(String.format("[best]%s %s_%s", scanResult2.SSID, scanResult2.BSSID, Integer.valueOf(Math.abs(scanResult2.level))));
                        }
                        WifiInfo connectionInfo = wifiManager.getConnectionInfo();
                        g.a(String.format("[active]%s %s_%s", connectionInfo.getSSID(), connectionInfo.getMacAddress(), Integer.valueOf(Math.abs(connectionInfo.getRssi()))));
                    }
                }
            } catch (Exception e10) {
                g.a("createAdReqURL", e10);
            }
        } catch (Exception e11) {
            g.a("createAdReqURL", e11);
        }
        g.a("createAdReqURL", sb.toString());
        String str3 = str + "?code=" + f.a(sb.toString());
        g.a("createAdReqURL", str3);
        return str3;
    }

    private static String a(Context context, String str) {
        String str2 = "error";
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo != null) {
                str2 = applicationInfo.metaData.get(str) + "";
                if (str2.trim().equals("")) {
                    throw new Exception();
                }
            }
        } catch (Exception e) {
            g.b(String.format("Could not read %s meta-data from AndroidManifest.xml", str), e);
            str2 = str2;
        }
        g.a(String.format("getMetaData{%s: %s}", str, str2));
        return str2;
    }

    public static void a(String str) {
        a = str;
    }

    private static void a(StringBuilder sb, String str, int i) {
        a(sb, str, "" + i);
    }

    private static void a(StringBuilder sb, String str, String str2) {
        try {
            sb.append("&").append(str).append("=").append(str2);
        } catch (Exception e) {
            g.a("addParamter", e);
        }
    }

    public static String b() {
        return Build.BRAND;
    }

    public static String b(Context context) {
        if (b == null) {
            b(a(context, "BaiduMobAd_APP_SEC"));
        }
        return b;
    }

    public static void b(String str) {
        b = str + "_cpr";
    }

    public static String c() {
        return Build.VERSION.SDK;
    }

    public static boolean c(Context context) {
        return "debug_cpr".equals(b(context));
    }

    public static String d() {
        return d.format(new Date(System.currentTimeMillis()));
    }

    static String d(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getSubscriberId();
    }

    public static String e(Context context) {
        String format = String.format("%s_%s_%s", 0, 0, 0);
        try {
            CellLocation cellLocation = ((TelephonyManager) context.getSystemService("phone")).getCellLocation();
            g.a("getLocation cell:", cellLocation + "");
            if (cellLocation == null) {
                return format;
            }
            if (cellLocation instanceof GsmCellLocation) {
                GsmCellLocation gsmCellLocation = (GsmCellLocation) cellLocation;
                return String.format("%s_%s_%s", String.format("%d", Integer.valueOf(gsmCellLocation.getCid())), String.format("%d", Integer.valueOf(gsmCellLocation.getLac())), 0);
            }
            String[] split = cellLocation.toString().replace("[", "").replace("]", "").split(",");
            return String.format("%s_%s_%s", split[0], split[3], split[4]);
        } catch (Exception e) {
            g.a("getLocation", e);
            return format;
        }
    }

    public static String f(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
    }

    public static String g(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getNetworkOperator();
    }

    public static DisplayMetrics h(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics;
    }

    public static String i(Context context) {
        if (c == null) {
            StringBuilder sb = new StringBuilder();
            try {
                Enumeration<JarEntry> entries = new JarFile(context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).sourceDir).entries();
                while (entries.hasMoreElements()) {
                    JarEntry nextElement = entries.nextElement();
                    if (!"AndroidManifest.xml".equals(nextElement.getName())) {
                        sb.append(String.format("%s_%d__", nextElement.getName(), Long.valueOf(nextElement.getSize())));
                    }
                }
            } catch (Exception e) {
                g.a("AdManager.getCS", e);
            }
            c = e.c(sb.toString());
        }
        return c;
    }

    public static void j(Context context) {
        e.b(context, "android.permission.INTERNET");
        e.b(context, "android.permission.ACCESS_NETWORK_STATE");
        e.b(context, "android.permission.READ_PHONE_STATE");
        e.b(context, "android.permission.ACCESS_COARSE_LOCATION");
    }
}
