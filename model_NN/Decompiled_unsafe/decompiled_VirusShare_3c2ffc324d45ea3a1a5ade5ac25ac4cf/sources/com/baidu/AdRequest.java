package com.baidu;

import android.content.Context;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class AdRequest {
    public static List<Ad> getAds(Context context, AdType adType, String str, String str2) {
        a.j(context);
        a.a(str);
        a.b(str2);
        ArrayList arrayList = new ArrayList();
        try {
            JSONArray jSONArray = new JSONObject(e.a(context, a.a(context, adType.a(), "http://mobads.baidu.com:80/cpro/ui/mads.php", "lite"))).getJSONArray("ad");
            for (int i = 0; i < jSONArray.length(); i++) {
                try {
                    JSONObject jSONObject = jSONArray.getJSONObject(i);
                    long currentTimeMillis = System.currentTimeMillis();
                    arrayList.add(Ad.a(context, jSONObject.getString("tit"), jSONObject.getString("desc"), jSONObject.getString("curl"), currentTimeMillis, jSONObject.getString("type"), jSONObject.getString("phone"), jSONObject.getString("clklogurl"), jSONObject.getString("w_picurl")));
                } catch (Exception e) {
                    g.a("getAds", e);
                }
            }
        } catch (Exception e2) {
            g.a("getAds", e2);
        }
        return arrayList;
    }

    public static String getClicklogUrl(Ad ad, int i, int i2, int i3, int i4) {
        if (i <= 0) {
            throw new IllegalArgumentException("showCount应该大于0 (showCount should be more than 0)");
        }
        String str = ad.b() + "&extra=" + f.a(String.format("%05x%05x%05x%x,%s", Integer.valueOf(i), Integer.valueOf(i2 + i4), Integer.valueOf(i3), Integer.valueOf(ad.a().b()), a.d()));
        g.a("AdRequest.getClicklog", str);
        return str;
    }
}
