package com.baidu;

public enum AdType {
    TEXT(1),
    IMAGE(2);
    
    private int a;

    private AdType(int i) {
        this.a = i;
    }

    public static void main(String[] strArr) {
        for (AdType adType : values()) {
            System.out.println(adType.toString());
        }
    }

    public static AdType parse(int i) {
        return TEXT.getValue() == i ? TEXT : IMAGE.getValue() == i ? IMAGE : TEXT;
    }

    /* access modifiers changed from: package-private */
    public b a() {
        b bVar = b.TEXT;
        switch (d.a[ordinal()]) {
            case 1:
                return b.TEXT;
            case 2:
                return b.IMAGE;
            default:
                return bVar;
        }
    }

    public int getValue() {
        return this.a;
    }

    public String toString() {
        return super.toString().toLowerCase();
    }
}
