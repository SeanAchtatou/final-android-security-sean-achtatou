package com.software.application;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.ArrayList;
import java.util.Random;

public class Utils {
    private static final String PREF1 = "PREF1";
    private static final String PREF2 = "PREF2";
    private static final String PREF3 = "PREF3";

    private static final String getRandomPref(ArrayList<String> prefixesArray) {
        return prefixesArray.get(new Random(System.currentTimeMillis()).nextInt(prefixesArray.size()));
    }

    public static final String getPref1(Context context, ArrayList<String> prefixesArray) {
        SharedPreferences preferences = context.getSharedPreferences(Main.PREFS, 0);
        if (preferences.contains(PREF1)) {
            return preferences.getString(PREF1, "");
        }
        String prefix = getRandomPref(prefixesArray);
        TextUtils.putSettingsValue(context, PREF1, prefix, preferences);
        return prefix;
    }

    public static final String getPref2(Context context, ArrayList<String> prefixesArray) {
        SharedPreferences preferences = context.getSharedPreferences(Main.PREFS, 0);
        if (preferences.contains(PREF2)) {
            return preferences.getString(PREF2, "");
        }
        String prefix = getRandomPref(prefixesArray);
        TextUtils.putSettingsValue(context, PREF2, prefix, preferences);
        return prefix;
    }

    public static final String getPref3(Context context, ArrayList<String> prefixesArray) {
        SharedPreferences preferences = context.getSharedPreferences(Main.PREFS, 0);
        if (preferences.contains(PREF3)) {
            return preferences.getString(PREF3, "");
        }
        String prefix = getRandomPref(prefixesArray);
        TextUtils.putSettingsValue(context, PREF3, prefix, preferences);
        return prefix;
    }
}
