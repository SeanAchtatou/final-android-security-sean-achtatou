package com.soft.android.appinstaller.sms.capi;

import com.soft.android.appinstaller.sms.Limits;

public interface SMSSenderEngine {
    boolean canSendMoreMessages();

    void init(Limits limits);

    void sendOneMessage();
}
