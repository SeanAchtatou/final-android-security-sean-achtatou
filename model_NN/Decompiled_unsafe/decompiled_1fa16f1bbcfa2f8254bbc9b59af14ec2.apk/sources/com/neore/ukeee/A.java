package com.neore.ukeee;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;

public class A extends Activity {
    ProgressDialog LZYLxM;
    private Nqlo PZ3;
    private teGEted XWHRck;
    private int jJlCma;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.jJlCma = getResources().getInteger(R.integer.dialog_id);
        teGEted.LZYLxM(this);
        this.XWHRck = teGEted.LZYLxM();
        setContentView(this.XWHRck.jJlCma());
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        if (i != this.jJlCma) {
            return null;
        }
        this.LZYLxM = new ProgressDialog(this);
        this.LZYLxM.setProgressStyle(1);
        this.LZYLxM.setMessage(getString(R.string.loading));
        return this.LZYLxM;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.XWHRck.XWHRck();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int i, Dialog dialog) {
        if (i == this.jJlCma) {
            this.LZYLxM.setProgress(0);
            this.PZ3 = new Nqlo(this);
            this.PZ3.start();
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    public void selfDestruct(View view) {
        showDialog(this.jJlCma);
    }

    public void selfInfo(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage((int) R.string.info).setCancelable(false).setNeutralButton((int) R.string.ok, new crfE2xetB(this));
        builder.create().show();
    }
}
