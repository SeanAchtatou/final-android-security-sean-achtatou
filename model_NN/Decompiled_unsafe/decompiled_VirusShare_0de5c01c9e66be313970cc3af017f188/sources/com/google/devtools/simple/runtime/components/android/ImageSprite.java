package com.google.devtools.simple.runtime.components.android;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import com.google.devtools.simple.common.ComponentCategory;
import com.google.devtools.simple.common.PropertyCategory;
import com.google.devtools.simple.runtime.annotations.DesignerComponent;
import com.google.devtools.simple.runtime.annotations.DesignerProperty;
import com.google.devtools.simple.runtime.annotations.SimpleObject;
import com.google.devtools.simple.runtime.annotations.SimpleProperty;
import com.google.devtools.simple.runtime.annotations.UsesPermissions;
import com.google.devtools.simple.runtime.components.android.util.MediaUtil;
import gnu.kawa.xml.ElementType;
import java.io.IOException;

@SimpleObject
@UsesPermissions(permissionNames = "android.permission.INTERNET")
@DesignerComponent(category = ComponentCategory.ANIMATION, description = "<p>A 'sprite' that can be placed on a <code>Canvas</code>, where it can react to touches and drags, interact with other sprites (<code>Ball</code>s and other <code>ImageSprite</code>s) and the edge of the Canvas, and move according to its property values.  Its appearance is that of the image specified in its <code>Picture</code> property (unless its <code>Visible</code> property is <code>False</code>.</p> <p>To have an <code>ImageSprite</code> move 10 pixels to the left every 1000 milliseconds (one second), for example, you would set the <code>Speed</code> property to 10 [pixels], the <code>Interval</code> property to 1000 [milliseconds], the <code>Heading</code> property to 180 [degrees], and the <code>Enabled</code> property to <code>True</code>.  A sprite whose <code>Rotates</code> property is <code>True</code> will rotate its image as the sprite's <code>Heading</code> changes.  Checking for collisions with a rotated sprite currently checks the sprite's unrotated position so that collision checking will be inaccurate for tall narrow or short wide sprites that are rotated.  Any of the sprite properties can be changed at any time under program control.</p> ", version = 4)
public class ImageSprite extends Sprite {
    private double cachedRotationHeading;
    private BitmapDrawable drawable;
    private final Form form;
    private int heightHint = -1;
    private Matrix mat;
    private String picturePath = ElementType.MATCH_ANY_LOCALNAME;
    private Bitmap rotatedBitmap;
    private BitmapDrawable rotatedDrawable;
    private boolean rotates;
    private boolean rotationCached;
    private Bitmap unrotatedBitmap;
    private int widthHint = -1;

    public ImageSprite(ComponentContainer container) {
        super(container);
        this.form = container.$form();
        this.mat = new Matrix();
        this.rotates = true;
        this.rotationCached = false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public void onDraw(Canvas canvas) {
        if (this.unrotatedBitmap != null && this.visible) {
            int xinit = (int) Math.round(this.xLeft);
            int yinit = (int) Math.round(this.yTop);
            int w = Width();
            int h = Height();
            if (!this.rotates) {
                this.drawable.setBounds(xinit, yinit, xinit + w, yinit + h);
                this.drawable.draw(canvas);
                return;
            }
            if (!this.rotationCached || this.cachedRotationHeading != Heading()) {
                this.mat.setRotate((float) (-Heading()), (float) (w / 2), (float) (h / 2));
                this.rotatedBitmap = Bitmap.createBitmap(this.unrotatedBitmap, 0, 0, this.unrotatedBitmap.getWidth(), this.unrotatedBitmap.getHeight(), this.mat, true);
                this.rotatedDrawable = new BitmapDrawable(this.rotatedBitmap);
                this.cachedRotationHeading = Heading();
            }
            this.rotatedDrawable.setBounds(((w / 2) + xinit) - (this.rotatedBitmap.getWidth() / 2), ((h / 2) + yinit) - (this.rotatedBitmap.getHeight() / 2), (w / 2) + xinit + (this.rotatedBitmap.getWidth() / 2), (h / 2) + yinit + (this.rotatedBitmap.getHeight() / 2));
            this.rotatedDrawable.draw(canvas);
        }
    }

    @SimpleProperty(category = PropertyCategory.APPEARANCE, description = "The picture that determines the sprite's appearence")
    public String Picture() {
        return this.picturePath;
    }

    @SimpleProperty
    @DesignerProperty(defaultValue = ElementType.MATCH_ANY_LOCALNAME, editorType = DesignerProperty.PROPERTY_TYPE_ASSET)
    public void Picture(String path) {
        if (path == null) {
            path = ElementType.MATCH_ANY_LOCALNAME;
        }
        this.picturePath = path;
        try {
            this.drawable = MediaUtil.getBitmapDrawable(this.form, this.picturePath);
        } catch (IOException e) {
            Log.e("ImageSprite", "Unable to load " + this.picturePath);
            this.drawable = null;
        }
        if (this.drawable != null) {
            this.unrotatedBitmap = this.drawable.getBitmap();
        } else {
            this.unrotatedBitmap = null;
        }
        registerChange();
    }

    @SimpleProperty
    public int Height() {
        if (this.heightHint != -1 && this.heightHint != -2) {
            return this.heightHint;
        }
        if (this.drawable == null) {
            return 0;
        }
        return this.drawable.getBitmap().getHeight();
    }

    @SimpleProperty
    public void Height(int height) {
        this.heightHint = height;
        registerChange();
    }

    @SimpleProperty
    public int Width() {
        if (this.widthHint != -1 && this.widthHint != -2) {
            return this.widthHint;
        }
        if (this.drawable == null) {
            return 0;
        }
        return this.drawable.getBitmap().getWidth();
    }

    @SimpleProperty
    public void Width(int width) {
        this.widthHint = width;
        registerChange();
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR, description = "If true, the sprite image rotates to match the sprite's heading.If false, the sprite image does not rotate when the sprite changes heading.The sprite rotates around its centerpoint.")
    public boolean Rotates() {
        return this.rotates;
    }

    @SimpleProperty
    @DesignerProperty(defaultValue = "True", editorType = DesignerProperty.PROPERTY_TYPE_BOOLEAN)
    public void Rotates(boolean rotates2) {
        this.rotates = rotates2;
        registerChange();
    }
}
