package com.google.devtools.simple.runtime.components.android;

import android.content.Intent;
import android.provider.MediaStore;
import android.util.Log;
import com.google.devtools.simple.common.ComponentCategory;
import com.google.devtools.simple.common.PropertyCategory;
import com.google.devtools.simple.runtime.annotations.DesignerComponent;
import com.google.devtools.simple.runtime.annotations.SimpleObject;
import com.google.devtools.simple.runtime.annotations.SimpleProperty;

@SimpleObject
@DesignerComponent(category = ComponentCategory.MEDIA, description = "<p>A special-purpose button. When the user taps an image picker, the device's image gallery appears, and the user can choose an image. After the user picks an image, the property <code>ImagePath</code> is set to a name that designates the image.</p>", version = 3)
public class ImagePicker extends Picker implements ActivityResultListener {
    private String imagePath;

    public ImagePicker(ComponentContainer container) {
        super(container);
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR)
    public String ImagePath() {
        return this.imagePath;
    }

    /* access modifiers changed from: protected */
    public Intent getIntent() {
        return new Intent("android.intent.action.PICK", MediaStore.Images.Media.INTERNAL_CONTENT_URI);
    }

    public void resultReturned(int requestCode, int resultCode, Intent data) {
        if (requestCode == this.requestCode && resultCode == -1) {
            this.imagePath = data.getData().toString();
            Log.i("ImagePicker", "Image imagePath = " + this.imagePath);
            AfterPicking();
        }
    }
}
