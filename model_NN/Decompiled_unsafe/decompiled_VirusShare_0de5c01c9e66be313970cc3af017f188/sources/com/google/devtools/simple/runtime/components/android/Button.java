package com.google.devtools.simple.runtime.components.android;

import com.google.devtools.simple.common.ComponentCategory;
import com.google.devtools.simple.runtime.annotations.DesignerComponent;
import com.google.devtools.simple.runtime.annotations.SimpleEvent;
import com.google.devtools.simple.runtime.annotations.SimpleObject;
import com.google.devtools.simple.runtime.events.EventDispatcher;

@SimpleObject
@DesignerComponent(category = ComponentCategory.BASIC, description = "Button with the ability to detect clicks.  Many aspects of its appearance can be changed, as well as whether it is clickable (<code>Enabled</code>), can be changed in the Designer or in the Blocks Editor.", version = 3)
public final class Button extends ButtonBase {
    public Button(ComponentContainer container) {
        super(container);
    }

    public void click() {
        Click();
    }

    @SimpleEvent
    public void Click() {
        EventDispatcher.dispatchEvent(this, "Click", new Object[0]);
    }

    public boolean longClick() {
        return LongClick();
    }

    @SimpleEvent
    public boolean LongClick() {
        return EventDispatcher.dispatchEvent(this, "LongClick", new Object[0]);
    }
}
