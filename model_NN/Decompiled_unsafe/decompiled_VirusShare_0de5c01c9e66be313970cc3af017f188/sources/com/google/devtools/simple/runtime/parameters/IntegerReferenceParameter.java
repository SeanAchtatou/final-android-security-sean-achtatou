package com.google.devtools.simple.runtime.parameters;

public final class IntegerReferenceParameter extends ReferenceParameter {
    private int value;

    public IntegerReferenceParameter(int value2) {
        set(value2);
    }

    public int get() {
        return this.value;
    }

    public void set(int value2) {
        this.value = value2;
    }
}
