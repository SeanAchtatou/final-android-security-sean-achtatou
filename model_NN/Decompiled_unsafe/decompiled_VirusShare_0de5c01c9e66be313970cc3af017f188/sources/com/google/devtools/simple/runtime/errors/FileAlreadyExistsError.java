package com.google.devtools.simple.runtime.errors;

import com.google.devtools.simple.runtime.annotations.SimpleObject;

@SimpleObject
public class FileAlreadyExistsError extends RuntimeError {
    public FileAlreadyExistsError(String message) {
        super(message);
    }
}
