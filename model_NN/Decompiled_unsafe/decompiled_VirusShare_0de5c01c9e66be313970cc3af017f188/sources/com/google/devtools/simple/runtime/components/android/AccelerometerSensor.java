package com.google.devtools.simple.runtime.components.android;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import com.google.devtools.simple.common.ComponentCategory;
import com.google.devtools.simple.common.PropertyCategory;
import com.google.devtools.simple.runtime.annotations.DesignerComponent;
import com.google.devtools.simple.runtime.annotations.DesignerProperty;
import com.google.devtools.simple.runtime.annotations.SimpleEvent;
import com.google.devtools.simple.runtime.annotations.SimpleObject;
import com.google.devtools.simple.runtime.annotations.SimpleProperty;
import com.google.devtools.simple.runtime.components.SensorComponent;
import com.google.devtools.simple.runtime.events.EventDispatcher;
import java.util.LinkedList;
import java.util.Queue;

@SimpleObject
@DesignerComponent(category = ComponentCategory.SENSORS, description = "<p>Non-visible component that can detect shaking and measure acceleration approximately in three dimensions using SI units (m/s<sup>2</sup>).  The components are: <ul><li> <strong>xAccel</strong>: 0 when the phone is at rest on a flat      surface, positive when the phone is tilted to the right (i.e.,      its left side is raised), and negative when the phone is tilted      to the left (i.e., its right size is raised).</li> <li> <strong>yAccel</strong>: 0 when the phone is at rest on a flat      surface, positive when its bottom is raised, and negative when      its top is raised. </li> <li> <strong>zAccel</strong>: Equal to -9.8 (earth's gravity in meters per      second per second when the device is at rest parallel to the ground      with the display facing up,      0 when perpindicular to the ground, and +9.8 when facing down.       The value can also be affected by accelerating it with or against      gravity. </li></ul></p> ", iconName = "images/accelerometersensor.png", nonVisible = true, version = 1)
public class AccelerometerSensor extends AndroidNonvisibleComponent implements OnStopListener, OnResumeListener, SensorComponent, SensorEventListener, Deleteable {
    private static final int SENSOR_CACHE_SIZE = 10;
    private static final double SHAKE_THRESHOLD = 8.0d;
    private final Queue<Float> X_CACHE = new LinkedList();
    private final Queue<Float> Y_CACHE = new LinkedList();
    private final Queue<Float> Z_CACHE = new LinkedList();
    private Sensor accelerometerSensor;
    private int accuracy;
    private boolean enabled;
    private final SensorManager sensorManager;
    private float xAccel;
    private float yAccel;
    private float zAccel;

    public AccelerometerSensor(ComponentContainer container) {
        super(container.$form());
        this.form.registerForOnResume(this);
        this.form.registerForOnStop(this);
        this.enabled = true;
        this.sensorManager = (SensorManager) container.$context().getSystemService("sensor");
        this.accelerometerSensor = this.sensorManager.getDefaultSensor(1);
        startListening();
    }

    @SimpleEvent
    public void AccelerationChanged(float xAccel2, float yAccel2, float zAccel2) {
        this.xAccel = xAccel2;
        this.yAccel = yAccel2;
        this.zAccel = zAccel2;
        addToSensorCache(this.X_CACHE, xAccel2);
        addToSensorCache(this.Y_CACHE, yAccel2);
        addToSensorCache(this.Z_CACHE, zAccel2);
        if (isShaking(this.X_CACHE, xAccel2) || isShaking(this.Y_CACHE, yAccel2) || isShaking(this.Z_CACHE, zAccel2)) {
            Shaking();
        }
        EventDispatcher.dispatchEvent(this, "AccelerationChanged", Float.valueOf(xAccel2), Float.valueOf(yAccel2), Float.valueOf(zAccel2));
    }

    @SimpleEvent
    public void Shaking() {
        EventDispatcher.dispatchEvent(this, "Shaking", new Object[0]);
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR)
    public boolean Available() {
        if (this.sensorManager.getSensorList(1).size() > 0) {
            return true;
        }
        return false;
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR)
    public boolean Enabled() {
        return this.enabled;
    }

    private void startListening() {
        this.sensorManager.registerListener(this, this.accelerometerSensor, 1);
    }

    private void stopListening() {
        this.sensorManager.unregisterListener(this);
    }

    @SimpleProperty
    @DesignerProperty(defaultValue = "True", editorType = DesignerProperty.PROPERTY_TYPE_BOOLEAN)
    public void Enabled(boolean enabled2) {
        if (this.enabled != enabled2) {
            this.enabled = enabled2;
            if (enabled2) {
                startListening();
            } else {
                stopListening();
            }
        }
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR)
    public float XAccel() {
        return this.xAccel;
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR)
    public float YAccel() {
        return this.yAccel;
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR)
    public float ZAccel() {
        return this.zAccel;
    }

    private void addToSensorCache(Queue<Float> cache, float value) {
        if (cache.size() >= 10) {
            cache.remove();
        }
        cache.add(Float.valueOf(value));
    }

    private boolean isShaking(Queue<Float> cache, float currentValue) {
        float average = 0.0f;
        for (Float floatValue : cache) {
            average += floatValue.floatValue();
        }
        return ((double) Math.abs((average / ((float) cache.size())) - currentValue)) > SHAKE_THRESHOLD;
    }

    public void onSensorChanged(SensorEvent sensorEvent) {
        if (this.enabled) {
            float[] values = sensorEvent.values;
            this.xAccel = values[0];
            this.yAccel = values[1];
            this.zAccel = values[2];
            this.accuracy = sensorEvent.accuracy;
            AccelerationChanged(this.xAccel, this.yAccel, this.zAccel);
        }
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy2) {
    }

    public void onResume() {
        if (this.enabled) {
            startListening();
        }
    }

    public void onStop() {
        if (this.enabled) {
            stopListening();
        }
    }

    public void onDelete() {
        if (this.enabled) {
            stopListening();
        }
    }
}
