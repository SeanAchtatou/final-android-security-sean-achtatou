package com.google.devtools.simple.runtime.components.android;

import com.google.devtools.simple.runtime.annotations.SimpleObject;
import com.google.devtools.simple.runtime.components.Component;
import com.google.devtools.simple.runtime.components.HandlesEventDispatching;

@SimpleObject
public abstract class AndroidNonvisibleComponent implements Component {
    protected final Form form;

    protected AndroidNonvisibleComponent(Form form2) {
        this.form = form2;
    }

    public HandlesEventDispatching getDispatchDelegate() {
        return this.form;
    }
}
