package com.google.devtools.simple.runtime.errors;

import com.google.devtools.simple.runtime.annotations.SimpleObject;

@SimpleObject
public class NoSuchFileError extends RuntimeError {
    public NoSuchFileError(String message) {
        super(message);
    }
}
