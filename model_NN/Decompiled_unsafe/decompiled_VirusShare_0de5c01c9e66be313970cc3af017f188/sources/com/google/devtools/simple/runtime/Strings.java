package com.google.devtools.simple.runtime;

import com.google.devtools.simple.runtime.annotations.SimpleFunction;
import com.google.devtools.simple.runtime.annotations.SimpleObject;
import com.google.devtools.simple.runtime.parameters.ObjectReferenceParameter;

@SimpleObject
public final class Strings {
    private Strings() {
    }

    private static void checkIndex(String function, int index, int max) {
        if (index < 0 || index > max) {
            throw new IllegalArgumentException("Index for " + function + "() out of range. " + "Should be between 0 and " + max + ", but is " + index);
        }
    }

    private static void checkLength(String function, int len) {
        if (len < 0) {
            throw new IllegalArgumentException("Length for " + function + "() out of range. " + "Should be greater than 0, but is " + len);
        }
    }

    private static String mid(String function, String str, int start, int len) {
        checkIndex(function, start, str.length());
        checkLength(function, len);
        int end = start + len;
        if (end > str.length()) {
            end = str.length();
        }
        return str.substring(start, end);
    }

    @SimpleFunction
    public static int InStr(String str1, String str2, int start) {
        checkIndex("InStr", start, str1.length());
        return str1.indexOf(str2, start);
    }

    @SimpleFunction
    public static int InStrRev(String str1, String str2, int start) {
        checkIndex("InStrRev", start, str1.length());
        return str1.lastIndexOf(str2, start);
    }

    @SimpleFunction
    public static void LCase(ObjectReferenceParameter<String> str) {
        str.set(str.get().toLowerCase());
    }

    @SimpleFunction
    public static void UCase(ObjectReferenceParameter<String> str) {
        str.set(str.get().toUpperCase());
    }

    @SimpleFunction
    public static String Left(String str, int len) {
        return mid("Left", str, 0, len);
    }

    @SimpleFunction
    public static String Right(String str, int len) {
        int start = str.length() - len;
        if (start < 0) {
            start = 0;
        }
        return mid("Right", str, start, len);
    }

    @SimpleFunction
    public static String Mid(String str, int start, int len) {
        return mid("Mid", str, start, len);
    }

    @SimpleFunction
    public static int Len(String str) {
        return str.length();
    }

    @SimpleFunction
    public static void Trim(ObjectReferenceParameter<String> str) {
        LTrim(str);
        RTrim(str);
    }

    @SimpleFunction
    public static void LTrim(ObjectReferenceParameter<String> str) {
        char[] chars = str.get().toCharArray();
        int count = 0;
        int i = 0;
        while (i < chars.length && chars[i] == ' ') {
            i++;
            count++;
        }
        if (count > 0) {
            str.set(new String(chars, count, chars.length - count));
        }
    }

    @SimpleFunction
    public static void RTrim(ObjectReferenceParameter<String> str) {
        char[] chars = str.get().toCharArray();
        int count = 0;
        int i = chars.length - 1;
        while (i > 0 && chars[i] == ' ') {
            i--;
            count++;
        }
        if (count > 0) {
            str.set(new String(chars, 0, chars.length - count));
        }
    }

    @SimpleFunction
    public static void Replace(ObjectReferenceParameter<String> str, String find, String replace, int start, int count) {
        String s = str.get();
        checkIndex("Replace", start, s.length());
        if (find == null || replace == null) {
            throw new NullPointerException();
        }
        String find2 = "\\Q" + find + "\\E";
        String s1 = s.substring(0, start);
        String s2 = s.substring(start);
        if (count != -1) {
            while (true) {
                count--;
                if (count < 0) {
                    break;
                }
                s2 = s2.replaceFirst(find2, replace);
            }
        } else {
            s2 = s2.replaceAll(find2, replace);
        }
        str.set(s1 + s2);
    }

    @SimpleFunction
    public static int StrComp(String str1, String str2) {
        return str1.compareTo(str2);
    }

    @SimpleFunction
    public static void StrReverse(ObjectReferenceParameter<String> str) {
        str.set(new StringBuffer(str.get()).reverse().toString());
    }
}
