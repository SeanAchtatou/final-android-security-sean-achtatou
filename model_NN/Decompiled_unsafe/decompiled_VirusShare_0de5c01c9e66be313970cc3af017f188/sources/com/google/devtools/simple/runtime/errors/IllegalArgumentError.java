package com.google.devtools.simple.runtime.errors;

import com.google.devtools.simple.runtime.annotations.SimpleObject;

@SimpleObject
public class IllegalArgumentError extends RuntimeError {
    public IllegalArgumentError() {
    }

    public IllegalArgumentError(String msg) {
        super(msg);
    }
}
