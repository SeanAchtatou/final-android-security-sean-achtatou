package com.google.devtools.simple.runtime;

import com.google.devtools.simple.runtime.annotations.SimpleFunction;
import com.google.devtools.simple.runtime.annotations.SimpleObject;
import com.google.devtools.simple.runtime.variants.Variant;
import java.util.Locale;

@SimpleObject
public final class Conversions {
    private Conversions() {
    }

    @SimpleFunction
    public static int Asc(String str) {
        try {
            return str.charAt(0);
        } catch (IndexOutOfBoundsException e) {
            throw new IllegalArgumentException("String length for Asc() must be 1 or greater");
        }
    }

    @SimpleFunction
    public static String Chr(int value) {
        return Character.toString((char) value);
    }

    @SimpleFunction
    public static String Hex(Variant v) {
        return Long.toHexString(v.getLong()).toUpperCase(Locale.ENGLISH);
    }
}
