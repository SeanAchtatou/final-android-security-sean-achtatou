package com.google.devtools.simple.runtime.variants;

import com.google.devtools.simple.runtime.helpers.ConvHelpers;

public final class BooleanVariant extends Variant {
    private boolean value;

    public static final BooleanVariant getBooleanVariant(boolean value2) {
        return new BooleanVariant(value2);
    }

    private BooleanVariant(boolean value2) {
        super((byte) 1);
        this.value = value2;
    }

    public boolean getBoolean() {
        return this.value;
    }

    public byte getByte() {
        return (byte) ConvHelpers.boolean2integer(this.value);
    }

    public short getShort() {
        return (short) ConvHelpers.boolean2integer(this.value);
    }

    public int getInteger() {
        return ConvHelpers.boolean2integer(this.value);
    }

    public long getLong() {
        return ConvHelpers.boolean2long(this.value);
    }

    public float getSingle() {
        return ConvHelpers.boolean2single(this.value);
    }

    public double getDouble() {
        return ConvHelpers.boolean2double(this.value);
    }

    public String getString() {
        return ConvHelpers.boolean2string(this.value);
    }

    public Variant add(Variant rightOp) {
        switch (rightOp.getKind()) {
            case 1:
            case 2:
            case 3:
            case 4:
                return IntegerVariant.getIntegerVariant(getInteger() + rightOp.getInteger());
            default:
                return rightOp.add(this);
        }
    }

    public Variant sub(Variant rightOp) {
        switch (rightOp.getKind()) {
            case 1:
            case 2:
            case 3:
            case 4:
                return IntegerVariant.getIntegerVariant(getInteger() - rightOp.getInteger());
            case 5:
                return LongVariant.getLongVariant(getLong() - rightOp.getLong());
            case 6:
                return SingleVariant.getSingleVariant(getSingle() - rightOp.getSingle());
            case 7:
            case 8:
                return DoubleVariant.getDoubleVariant(getDouble() - rightOp.getDouble());
            default:
                return super.sub(rightOp);
        }
    }

    public Variant mul(Variant rightOp) {
        switch (rightOp.getKind()) {
            case 1:
            case 2:
            case 3:
            case 4:
                return IntegerVariant.getIntegerVariant(getInteger() * rightOp.getInteger());
            default:
                return rightOp.mul(this);
        }
    }

    public Variant div(Variant rightOp) {
        return DoubleVariant.getDoubleVariant(getDouble()).div(rightOp);
    }

    public Variant idiv(Variant rightOp) {
        switch (rightOp.getKind()) {
            case 1:
            case 2:
            case 3:
            case 4:
                return IntegerVariant.getIntegerVariant(getInteger() / rightOp.getInteger());
            case 5:
                return LongVariant.getLongVariant(getLong() / rightOp.getLong());
            case 6:
                return SingleVariant.getSingleVariant(getSingle()).idiv(rightOp);
            case 7:
            case 8:
                return DoubleVariant.getDoubleVariant(getDouble()).idiv(rightOp);
            default:
                return super.idiv(rightOp);
        }
    }

    public Variant mod(Variant rightOp) {
        switch (rightOp.getKind()) {
            case 1:
            case 2:
            case 3:
            case 4:
                return IntegerVariant.getIntegerVariant(getInteger() % rightOp.getInteger());
            case 5:
                return LongVariant.getLongVariant(getLong() % rightOp.getLong());
            case 6:
                return SingleVariant.getSingleVariant(getSingle() % rightOp.getSingle());
            case 7:
            case 8:
                return DoubleVariant.getDoubleVariant(getDouble() % rightOp.getDouble());
            default:
                return super.sub(rightOp);
        }
    }

    public Variant pow(Variant rightOp) {
        return DoubleVariant.getDoubleVariant(getDouble()).pow(rightOp);
    }

    public Variant neg() {
        return IntegerVariant.getIntegerVariant(-getInteger());
    }

    public Variant shl(Variant rightOp) {
        return this;
    }

    public Variant shr(Variant rightOp) {
        return this;
    }

    public int cmp(Variant rightOp) {
        switch (rightOp.getKind()) {
            case 1:
            case 2:
            case 3:
            case 4:
                return getInteger() - rightOp.getInteger();
            default:
                return -rightOp.cmp(this);
        }
    }

    public Variant not() {
        return getBooleanVariant(!this.value);
    }

    public Variant and(Variant rightOp) {
        switch (rightOp.getKind()) {
            case 1:
                return getBooleanVariant(this.value & rightOp.getBoolean());
            case 2:
            case 3:
            case 4:
                return IntegerVariant.getIntegerVariant(getInteger() & rightOp.getInteger());
            case 5:
            case 8:
                return LongVariant.getLongVariant(((long) getInteger()) & rightOp.getLong());
            case 6:
            case 7:
            default:
                return super.and(rightOp);
        }
    }

    public Variant or(Variant rightOp) {
        switch (rightOp.getKind()) {
            case 1:
                return getBooleanVariant(this.value | rightOp.getBoolean());
            case 2:
            case 3:
            case 4:
                return IntegerVariant.getIntegerVariant(getInteger() | rightOp.getInteger());
            case 5:
            case 8:
                return LongVariant.getLongVariant(((long) getInteger()) | rightOp.getLong());
            case 6:
            case 7:
            default:
                return super.or(rightOp);
        }
    }

    public Variant xor(Variant rightOp) {
        switch (rightOp.getKind()) {
            case 1:
                return getBooleanVariant(this.value ^ rightOp.getBoolean());
            case 2:
            case 3:
            case 4:
                return IntegerVariant.getIntegerVariant(getInteger() ^ rightOp.getInteger());
            case 5:
            case 8:
                return LongVariant.getLongVariant(((long) getInteger()) ^ rightOp.getLong());
            case 6:
            case 7:
            default:
                return super.xor(rightOp);
        }
    }
}
