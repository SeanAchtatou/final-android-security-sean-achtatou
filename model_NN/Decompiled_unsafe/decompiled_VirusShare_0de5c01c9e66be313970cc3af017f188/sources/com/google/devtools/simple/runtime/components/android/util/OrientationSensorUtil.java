package com.google.devtools.simple.runtime.components.android.util;

public class OrientationSensorUtil {
    private OrientationSensorUtil() {
    }

    static float mod(float dividend, float quotient) {
        float result = dividend % quotient;
        return (result == 0.0f || Math.signum(dividend) == Math.signum(quotient)) ? result : result + quotient;
    }

    public static float normalizeAzimuth(float azimuth) {
        return mod(azimuth, 360.0f);
    }

    public static float normalizePitch(float pitch) {
        return mod(pitch + 180.0f, 360.0f) - 180.0f;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public static float normalizeRoll(float roll) {
        float roll2 = Math.max(Math.min(roll, 180.0f), -180.0f);
        if (roll2 >= -90.0f && roll2 <= 90.0f) {
            return roll2;
        }
        float roll3 = 180.0f - roll2;
        if (roll3 >= 270.0f) {
            return roll3 - 360.0f;
        }
        return roll3;
    }
}
