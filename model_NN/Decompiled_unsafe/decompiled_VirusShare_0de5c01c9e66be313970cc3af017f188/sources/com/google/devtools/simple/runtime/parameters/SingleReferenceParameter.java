package com.google.devtools.simple.runtime.parameters;

public final class SingleReferenceParameter extends ReferenceParameter {
    private float value;

    public SingleReferenceParameter(float value2) {
        set(value2);
    }

    public float get() {
        return this.value;
    }

    public void set(float value2) {
        this.value = value2;
    }
}
