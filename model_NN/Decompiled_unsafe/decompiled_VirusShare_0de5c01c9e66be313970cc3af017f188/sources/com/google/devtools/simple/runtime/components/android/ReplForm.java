package com.google.devtools.simple.runtime.components.android;

import android.content.ComponentName;
import android.os.Bundle;
import android.widget.Toast;
import com.google.devtools.simple.runtime.components.android.util.ReplCommController;

public class ReplForm extends Form {
    private ReplCommController formReplCommController;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        $context().getPackageManager().setComponentEnabledSetting(new ComponentName(getPackageName(), getClass().getName()), 2, 1);
        this.formReplCommController = new ReplCommController(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.formReplCommController.startListening(true);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        this.formReplCommController.stopListening(false);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.formReplCommController.destroy();
    }

    /* access modifiers changed from: protected */
    public void startNewForm(String nextFormName, String startupValue) {
        runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(ReplForm.this, "Switching forms is not currently supported during development.", 1).show();
            }
        });
    }
}
