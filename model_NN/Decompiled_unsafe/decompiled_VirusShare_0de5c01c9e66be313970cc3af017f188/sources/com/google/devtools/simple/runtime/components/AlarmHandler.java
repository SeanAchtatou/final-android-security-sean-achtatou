package com.google.devtools.simple.runtime.components;

public interface AlarmHandler {
    void alarm();
}
