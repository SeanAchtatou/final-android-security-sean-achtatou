package com.google.devtools.simple.runtime.components;

import com.google.devtools.simple.runtime.annotations.SimpleDataElement;
import com.google.devtools.simple.runtime.annotations.SimpleObject;

@SimpleObject
public interface Component {
    @SimpleDataElement
    public static final int ALIGNMENT_CENTER = 1;
    @SimpleDataElement
    public static final int ALIGNMENT_NORMAL = 0;
    @SimpleDataElement
    public static final int ALIGNMENT_OPPOSITE = 2;
    @SimpleDataElement
    public static final int COLOR_BLACK = -16777216;
    @SimpleDataElement
    public static final int COLOR_BLUE = -16776961;
    @SimpleDataElement
    public static final int COLOR_CYAN = -16711681;
    @SimpleDataElement
    public static final int COLOR_DEFAULT = 0;
    @SimpleDataElement
    public static final int COLOR_DKGRAY = -12303292;
    @SimpleDataElement
    public static final int COLOR_GRAY = -7829368;
    @SimpleDataElement
    public static final int COLOR_GREEN = -16711936;
    @SimpleDataElement
    public static final int COLOR_LTGRAY = -3355444;
    @SimpleDataElement
    public static final int COLOR_MAGENTA = -65281;
    @SimpleDataElement
    public static final int COLOR_NONE = 16777215;
    @SimpleDataElement
    public static final int COLOR_ORANGE = -14336;
    @SimpleDataElement
    public static final int COLOR_PINK = -20561;
    @SimpleDataElement
    public static final int COLOR_RED = -65536;
    @SimpleDataElement
    public static final int COLOR_WHITE = -1;
    @SimpleDataElement
    public static final int COLOR_YELLOW = -256;
    public static final String DEFAULT_VALUE_COLOR_BLACK = "&HFF000000";
    public static final String DEFAULT_VALUE_COLOR_BLUE = "&HFF0000FF";
    public static final String DEFAULT_VALUE_COLOR_CYAN = "&HFF00FFFF";
    public static final String DEFAULT_VALUE_COLOR_DEFAULT = "&H00000000";
    public static final String DEFAULT_VALUE_COLOR_DKGRAY = "&HFF444444";
    public static final String DEFAULT_VALUE_COLOR_GRAY = "&HFF888888";
    public static final String DEFAULT_VALUE_COLOR_GREEN = "&HFF00FF00";
    public static final String DEFAULT_VALUE_COLOR_LTGRAY = "&HFFCCCCCC";
    public static final String DEFAULT_VALUE_COLOR_MAGENTA = "&HFFFF00FF";
    public static final String DEFAULT_VALUE_COLOR_NONE = "&H00FFFFFF";
    public static final String DEFAULT_VALUE_COLOR_ORANGE = "&HFFFFC800";
    public static final String DEFAULT_VALUE_COLOR_PINK = "&HFFFFAFAF";
    public static final String DEFAULT_VALUE_COLOR_RED = "&HFFFF0000";
    public static final String DEFAULT_VALUE_COLOR_WHITE = "&HFFFFFFFF";
    public static final String DEFAULT_VALUE_COLOR_YELLOW = "&HFFFFFF00";
    @SimpleDataElement
    public static final int DIRECTION_EAST = 3;
    public static final int DIRECTION_MAX = 4;
    public static final int DIRECTION_MIN = -4;
    public static final int DIRECTION_NONE = 0;
    @SimpleDataElement
    public static final int DIRECTION_NORTH = 1;
    @SimpleDataElement
    public static final int DIRECTION_NORTHEAST = 2;
    @SimpleDataElement
    public static final int DIRECTION_NORTHWEST = -4;
    @SimpleDataElement
    public static final int DIRECTION_SOUTH = -1;
    @SimpleDataElement
    public static final int DIRECTION_SOUTHEAST = 4;
    @SimpleDataElement
    public static final int DIRECTION_SOUTHWEST = -2;
    @SimpleDataElement
    public static final int DIRECTION_WEST = -3;
    @SimpleDataElement
    public static final float FONT_DEFAULT_SIZE = 14.0f;
    @SimpleDataElement
    public static final int LAYOUT_ORIENTATION_HORIZONTAL = 0;
    @SimpleDataElement
    public static final int LAYOUT_ORIENTATION_VERTICAL = 1;
    @SimpleDataElement
    public static final int LENGTH_FILL_PARENT = -2;
    @SimpleDataElement
    public static final int LENGTH_PREFERRED = -1;
    @SimpleDataElement
    public static final int LENGTH_UNKNOWN = -3;
    @SimpleDataElement
    public static final int TYPEFACE_DEFAULT = 0;
    @SimpleDataElement
    public static final int TYPEFACE_MONOSPACE = 3;
    @SimpleDataElement
    public static final int TYPEFACE_SANSSERIF = 1;
    @SimpleDataElement
    public static final int TYPEFACE_SERIF = 2;

    HandlesEventDispatching getDispatchDelegate();
}
