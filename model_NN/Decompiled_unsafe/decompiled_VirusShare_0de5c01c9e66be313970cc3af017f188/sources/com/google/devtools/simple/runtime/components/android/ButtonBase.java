package com.google.devtools.simple.runtime.components.android;

import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import com.google.android.googlelogin.GoogleLoginServiceConstants;
import com.google.devtools.simple.common.PropertyCategory;
import com.google.devtools.simple.runtime.annotations.DesignerProperty;
import com.google.devtools.simple.runtime.annotations.SimpleEvent;
import com.google.devtools.simple.runtime.annotations.SimpleObject;
import com.google.devtools.simple.runtime.annotations.SimpleProperty;
import com.google.devtools.simple.runtime.annotations.UsesPermissions;
import com.google.devtools.simple.runtime.components.Component;
import com.google.devtools.simple.runtime.components.android.util.MediaUtil;
import com.google.devtools.simple.runtime.components.android.util.TextViewUtil;
import com.google.devtools.simple.runtime.components.android.util.ViewUtil;
import com.google.devtools.simple.runtime.events.EventDispatcher;
import gnu.kawa.xml.ElementType;
import java.io.IOException;

@SimpleObject
@UsesPermissions(permissionNames = "android.permission.INTERNET")
public abstract class ButtonBase extends AndroidViewComponent implements View.OnClickListener, View.OnFocusChangeListener, View.OnLongClickListener {
    private static final String LOG_TAG = "ButtonBase";
    private int backgroundColor;
    private Drawable backgroundImageDrawable;
    private boolean bold;
    private Drawable defaultButtonDrawable;
    private ColorStateList defaultColorStateList;
    private int fontTypeface;
    private String imagePath = ElementType.MATCH_ANY_LOCALNAME;
    private boolean italic;
    private int textAlignment;
    private int textColor;
    private final Button view;

    public abstract void click();

    public ButtonBase(ComponentContainer container) {
        super(container);
        this.view = new Button(container.$context());
        this.defaultButtonDrawable = this.view.getBackground();
        this.defaultColorStateList = this.view.getTextColors();
        container.$add(this);
        this.view.setOnClickListener(this);
        this.view.setOnFocusChangeListener(this);
        this.view.setOnLongClickListener(this);
        TextAlignment(1);
        BackgroundColor(0);
        Image(ElementType.MATCH_ANY_LOCALNAME);
        Enabled(true);
        this.fontTypeface = 0;
        TextViewUtil.setFontTypeface(this.view, this.fontTypeface, this.bold, this.italic);
        FontSize(14.0f);
        Text(ElementType.MATCH_ANY_LOCALNAME);
        TextColor(0);
    }

    public View getView() {
        return this.view;
    }

    @SimpleEvent
    public void GotFocus() {
        EventDispatcher.dispatchEvent(this, "GotFocus", new Object[0]);
    }

    @SimpleEvent
    public void LostFocus() {
        EventDispatcher.dispatchEvent(this, "LostFocus", new Object[0]);
    }

    @SimpleProperty(category = PropertyCategory.APPEARANCE, userVisible = GoogleLoginServiceConstants.PREFER_HOSTED)
    public int TextAlignment() {
        return this.textAlignment;
    }

    @SimpleProperty(userVisible = GoogleLoginServiceConstants.PREFER_HOSTED)
    @DesignerProperty(defaultValue = "1", editorType = DesignerProperty.PROPERTY_TYPE_TEXTALIGNMENT)
    public void TextAlignment(int alignment) {
        this.textAlignment = alignment;
        TextViewUtil.setAlignment(this.view, alignment, true);
    }

    @SimpleProperty(category = PropertyCategory.APPEARANCE)
    public String Image() {
        return this.imagePath;
    }

    @SimpleProperty(description = "Specifies the path of the button's image.  If there is both an Image and a BackgroundColor, only the Image will be visible.")
    @DesignerProperty(defaultValue = ElementType.MATCH_ANY_LOCALNAME, editorType = DesignerProperty.PROPERTY_TYPE_ASSET)
    public void Image(String path) {
        if (!path.equals(this.imagePath) || this.backgroundImageDrawable == null) {
            if (path == null) {
                path = ElementType.MATCH_ANY_LOCALNAME;
            }
            this.imagePath = path;
            this.backgroundImageDrawable = null;
            if (this.imagePath.length() > 0) {
                try {
                    this.backgroundImageDrawable = MediaUtil.getBitmapDrawable(this.container.$form(), this.imagePath);
                } catch (IOException e) {
                    Log.e(LOG_TAG, "Unable to load " + this.imagePath);
                }
            }
            updateAppearance();
        }
    }

    @SimpleProperty(category = PropertyCategory.APPEARANCE, description = "Returns the button's background color")
    public int BackgroundColor() {
        return this.backgroundColor;
    }

    @SimpleProperty(description = "Specifies the button's background color. The background color will not be visible if an Image is being displayed.")
    @DesignerProperty(defaultValue = Component.DEFAULT_VALUE_COLOR_DEFAULT, editorType = DesignerProperty.PROPERTY_TYPE_COLOR)
    public void BackgroundColor(int argb) {
        this.backgroundColor = argb;
        updateAppearance();
    }

    private void updateAppearance() {
        if (this.backgroundImageDrawable != null) {
            ViewUtil.setBackgroundImage(this.view, this.backgroundImageDrawable);
        } else if (this.backgroundColor == 0) {
            ViewUtil.setBackgroundDrawable(this.view, this.defaultButtonDrawable);
        } else {
            ViewUtil.setBackgroundDrawable(this.view, null);
            TextViewUtil.setBackgroundColor(this.view, this.backgroundColor);
        }
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR)
    public boolean Enabled() {
        return TextViewUtil.isEnabled(this.view);
    }

    @SimpleProperty
    @DesignerProperty(defaultValue = "True", editorType = DesignerProperty.PROPERTY_TYPE_BOOLEAN)
    public void Enabled(boolean enabled) {
        TextViewUtil.setEnabled(this.view, enabled);
    }

    @SimpleProperty(category = PropertyCategory.APPEARANCE, userVisible = GoogleLoginServiceConstants.PREFER_HOSTED)
    public boolean FontBold() {
        return this.bold;
    }

    @SimpleProperty(userVisible = GoogleLoginServiceConstants.PREFER_HOSTED)
    @DesignerProperty(defaultValue = "False", editorType = DesignerProperty.PROPERTY_TYPE_BOOLEAN)
    public void FontBold(boolean bold2) {
        this.bold = bold2;
        TextViewUtil.setFontTypeface(this.view, this.fontTypeface, bold2, this.italic);
    }

    @SimpleProperty(category = PropertyCategory.APPEARANCE, userVisible = GoogleLoginServiceConstants.PREFER_HOSTED)
    public boolean FontItalic() {
        return this.italic;
    }

    @SimpleProperty(userVisible = GoogleLoginServiceConstants.PREFER_HOSTED)
    @DesignerProperty(defaultValue = "False", editorType = DesignerProperty.PROPERTY_TYPE_BOOLEAN)
    public void FontItalic(boolean italic2) {
        this.italic = italic2;
        TextViewUtil.setFontTypeface(this.view, this.fontTypeface, this.bold, italic2);
    }

    @SimpleProperty(category = PropertyCategory.APPEARANCE, userVisible = GoogleLoginServiceConstants.PREFER_HOSTED)
    public float FontSize() {
        return TextViewUtil.getFontSize(this.view);
    }

    @SimpleProperty(userVisible = GoogleLoginServiceConstants.PREFER_HOSTED)
    @DesignerProperty(defaultValue = "14.0", editorType = DesignerProperty.PROPERTY_TYPE_NON_NEGATIVE_FLOAT)
    public void FontSize(float size) {
        TextViewUtil.setFontSize(this.view, size);
    }

    @SimpleProperty(category = PropertyCategory.APPEARANCE, userVisible = GoogleLoginServiceConstants.PREFER_HOSTED)
    public int FontTypeface() {
        return this.fontTypeface;
    }

    @SimpleProperty(userVisible = GoogleLoginServiceConstants.PREFER_HOSTED)
    @DesignerProperty(defaultValue = "0", editorType = DesignerProperty.PROPERTY_TYPE_TYPEFACE)
    public void FontTypeface(int typeface) {
        this.fontTypeface = typeface;
        TextViewUtil.setFontTypeface(this.view, this.fontTypeface, this.bold, this.italic);
    }

    @SimpleProperty(category = PropertyCategory.APPEARANCE)
    public String Text() {
        return TextViewUtil.getText(this.view);
    }

    @SimpleProperty
    @DesignerProperty(defaultValue = ElementType.MATCH_ANY_LOCALNAME, editorType = DesignerProperty.PROPERTY_TYPE_STRING)
    public void Text(String text) {
        TextViewUtil.setText(this.view, text);
    }

    @SimpleProperty(category = PropertyCategory.APPEARANCE)
    public int TextColor() {
        return this.textColor;
    }

    @SimpleProperty
    @DesignerProperty(defaultValue = Component.DEFAULT_VALUE_COLOR_DEFAULT, editorType = DesignerProperty.PROPERTY_TYPE_COLOR)
    public void TextColor(int argb) {
        this.textColor = argb;
        if (argb != 0) {
            TextViewUtil.setTextColor(this.view, argb);
        } else {
            TextViewUtil.setTextColors(this.view, this.defaultColorStateList);
        }
    }

    public boolean longClick() {
        return false;
    }

    public void onClick(View view2) {
        click();
    }

    public void onFocusChange(View previouslyFocused, boolean gainFocus) {
        if (gainFocus) {
            GotFocus();
        } else {
            LostFocus();
        }
    }

    public boolean onLongClick(View view2) {
        return longClick();
    }
}
