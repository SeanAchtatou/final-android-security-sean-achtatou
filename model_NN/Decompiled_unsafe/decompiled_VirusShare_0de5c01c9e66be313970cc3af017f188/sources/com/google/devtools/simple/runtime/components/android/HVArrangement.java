package com.google.devtools.simple.runtime.components.android;

import android.app.Activity;
import android.view.View;
import com.google.devtools.simple.runtime.annotations.SimpleObject;
import com.google.devtools.simple.runtime.components.Component;
import com.google.devtools.simple.runtime.components.android.util.ViewUtil;

@SimpleObject
public class HVArrangement extends AndroidViewComponent implements Component, ComponentContainer {
    private final Activity context;
    private final int orientation;
    private final LinearLayout viewLayout;

    public HVArrangement(ComponentContainer container, int orientation2) {
        super(container);
        this.context = container.$context();
        this.orientation = orientation2;
        this.viewLayout = new LinearLayout(this.context, orientation2, 100, 100);
        container.$add(this);
    }

    public Activity $context() {
        return this.context;
    }

    public Form $form() {
        return this.container.$form();
    }

    public void $add(AndroidViewComponent component) {
        this.viewLayout.add(component);
    }

    public void setChildWidth(AndroidViewComponent component, int width) {
        if (this.orientation == 0) {
            ViewUtil.setChildWidthForHorizontalLayout(component.getView(), width);
        } else {
            ViewUtil.setChildWidthForVerticalLayout(component.getView(), width);
        }
    }

    public void setChildHeight(AndroidViewComponent component, int height) {
        if (this.orientation == 0) {
            ViewUtil.setChildHeightForHorizontalLayout(component.getView(), height);
        } else {
            ViewUtil.setChildHeightForVerticalLayout(component.getView(), height);
        }
    }

    public View getView() {
        return this.viewLayout.getLayoutManager();
    }
}
