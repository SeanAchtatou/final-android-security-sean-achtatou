package com.google.devtools.simple.runtime.components.android;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import com.google.android.googlelogin.GoogleLoginServiceConstants;
import com.google.devtools.simple.common.ComponentCategory;
import com.google.devtools.simple.common.PropertyCategory;
import com.google.devtools.simple.runtime.annotations.DesignerComponent;
import com.google.devtools.simple.runtime.annotations.DesignerProperty;
import com.google.devtools.simple.runtime.annotations.SimpleEvent;
import com.google.devtools.simple.runtime.annotations.SimpleFunction;
import com.google.devtools.simple.runtime.annotations.SimpleObject;
import com.google.devtools.simple.runtime.annotations.SimpleProperty;
import com.google.devtools.simple.runtime.annotations.UsesPermissions;
import com.google.devtools.simple.runtime.components.Component;
import com.google.devtools.simple.runtime.components.android.util.AsynchUtil;
import com.google.devtools.simple.runtime.components.util.ErrorMessages;
import com.google.devtools.simple.runtime.events.EventDispatcher;
import gnu.kawa.xml.ElementType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import twitter4j.DirectMessage;
import twitter4j.Paging;
import twitter4j.Query;
import twitter4j.Status;
import twitter4j.Tweet;
import twitter4j.TwitterException;
import twitter4j.User;
import twitter4j.http.AccessToken;
import twitter4j.http.RequestToken;

@SimpleObject
@UsesPermissions(permissionNames = "android.permission.INTERNET")
@DesignerComponent(category = ComponentCategory.SOCIAL, description = "<p>A non-visible component that enables communication with <a href=\"http://www.twitter.com\" target=\"_blank\">Twitter</a>. Methods are included to enabling searching (<code>SearchTwitter</code>) or logging into (<code>Authorize</code>) Twitter.  Once a user has authorized their Twitter account (and the authorization has been confirmed successful by the <code>IsAuthorized</code> event), many more operations are available:<ul><li> Setting the status of the logged-in user (<code>SetStatus</code>)     </li><li> Directing a message to a specific user      (<code>DirectMessage</code>)</li> <li> Receiving the most recent messages directed to the logged-in user      (<code>RequestDirectMessages</code>)</li> <li> Following a specific user (<code>Follow</code>)</li><li> Ceasing to follow a specific user (<code>StopFollowing</code>)</li><li> Getting a list of users following the logged-in user      (<code>RequestFollowers</code>)</li> <li> Getting the most recent messages of users followed by the      logged-in user (<code>RequestFriendTimeline</code>)</li> <li> Getting the most recent mentions of the logged-in user      (<code>RequestMentions</code>)</li></ul></p> <p>You must obtain a Comsumer Key and Consumer Secret for Twitter authorization  specific to your app from http://twitter.com/oauth_clients/new </p>", iconName = "images/twitter.png", nonVisible = true, version = 2)
public final class Twitter extends AndroidNonvisibleComponent implements ActivityResultListener, Component {
    private static final String ACCESS_SECRET_TAG = "TwitterOauthAccessSecret";
    private static final String ACCESS_TOKEN_TAG = "TwitterOauthAccessToken";
    private static final String CALLBACK_URL = "appinventor://twitter";
    private static final String MAX_CHARACTERS = "160";
    private static final String MAX_MENTIONS_RETURNED = "20";
    private static final String URL_HOST = "twitter";
    /* access modifiers changed from: private */
    public static final String WEBVIEW_ACTIVITY_CLASS = WebViewActivity.class.getName();
    /* access modifiers changed from: private */
    public volatile AccessToken accessToken;
    private volatile String consumerKey = ElementType.MATCH_ANY_LOCALNAME;
    private volatile String consumerSecret = ElementType.MATCH_ANY_LOCALNAME;
    /* access modifiers changed from: private */
    public final ComponentContainer container;
    /* access modifiers changed from: private */
    public final List<String> directMessages;
    /* access modifiers changed from: private */
    public final List<String> followers;
    /* access modifiers changed from: private */
    public final Handler handler;
    /* access modifiers changed from: private */
    public final Object lock = new Object();
    /* access modifiers changed from: private */
    public final List<String> mentions;
    /* access modifiers changed from: private */
    public final int requestCode;
    /* access modifiers changed from: private */
    public volatile RequestToken requestToken;
    /* access modifiers changed from: private */
    public final List<String> searchResults;
    private final SharedPreferences sharedPreferences;
    /* access modifiers changed from: private */
    public final List<List<String>> timeline;
    private volatile twitter4j.Twitter twitter;
    /* access modifiers changed from: private */
    public final Object twitterLock = new Object();
    /* access modifiers changed from: private */
    public volatile String userName = ElementType.MATCH_ANY_LOCALNAME;

    public Twitter(ComponentContainer container2) {
        super(container2.$form());
        this.container = container2;
        this.handler = new Handler();
        this.mentions = new ArrayList();
        this.followers = new ArrayList();
        this.timeline = new ArrayList();
        this.directMessages = new ArrayList();
        this.searchResults = new ArrayList();
        this.sharedPreferences = container2.$context().getSharedPreferences("Twitter", 0);
        this.accessToken = retrieveAccessToken();
        this.requestCode = this.form.registerForActivityResult(this);
    }

    @SimpleFunction(description = "Twitter's API no longer supports login via username and password. Use the Authorize call instead.", userVisible = GoogleLoginServiceConstants.PREFER_HOSTED)
    public void Login(String username, String password) {
        this.form.dispatchErrorOccurredEvent(this, "Login", ErrorMessages.ERROR_TWITTER_UNSUPPORTED_LOGIN_FUNCTION, new Object[0]);
    }

    @SimpleEvent(description = "This event is raised after the program calls <code>Authorize</code> if the authorization was successful.  It is also called after a call to <code>CheckAuthorized</code> if we already have a valid access token. After this event has been raised, any other method for this component can be called.")
    public void IsAuthorized() {
        EventDispatcher.dispatchEvent(this, "IsAuthorized", new Object[0]);
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR, description = "The user name of the authorized user. Empty if there is no authorized user.")
    public String Username() {
        String str;
        synchronized (this.lock) {
            str = this.userName;
        }
        return str;
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR)
    public String ConsumerKey() {
        return this.consumerKey;
    }

    @SimpleProperty
    @DesignerProperty(defaultValue = ElementType.MATCH_ANY_LOCALNAME, editorType = DesignerProperty.PROPERTY_TYPE_STRING)
    public void ConsumerKey(String consumerKey2) {
        this.consumerKey = consumerKey2;
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR)
    public String ConsumerSecret() {
        return this.consumerSecret;
    }

    @SimpleProperty
    @DesignerProperty(defaultValue = ElementType.MATCH_ANY_LOCALNAME, editorType = DesignerProperty.PROPERTY_TYPE_STRING)
    public void ConsumerSecret(String consumerSecret2) {
        this.consumerSecret = consumerSecret2;
    }

    @SimpleFunction(description = "Redirects user to login to Twitter via the Web browser using the OAuth protocol if we don't already have authorization.")
    public void Authorize() {
        final twitter4j.Twitter myTwitter;
        if (this.consumerKey.length() == 0 || this.consumerSecret.length() == 0) {
            this.form.dispatchErrorOccurredEvent(this, "Authorize", ErrorMessages.ERROR_TWITTER_BLANK_CONSUMER_KEY_OR_SECRET, new Object[0]);
            return;
        }
        final String myConsumerKey = this.consumerKey;
        final String myConsumerSecret = this.consumerSecret;
        synchronized (this.lock) {
            if (this.twitter == null) {
                this.twitter = new twitter4j.Twitter();
            }
            myTwitter = this.twitter;
        }
        AsynchUtil.runAsynchronously(new Runnable() {
            public void run() {
                RequestToken newRequestToken;
                if (Twitter.this.checkAccessToken(myConsumerKey, myConsumerSecret)) {
                    Twitter.this.handler.post(new Runnable() {
                        public void run() {
                            Twitter.this.IsAuthorized();
                        }
                    });
                    return;
                }
                try {
                    synchronized (Twitter.this.twitterLock) {
                        myTwitter.setOAuthConsumer(myConsumerKey, myConsumerSecret);
                        newRequestToken = myTwitter.getOAuthRequestToken(Twitter.CALLBACK_URL);
                    }
                    String authURL = newRequestToken.getAuthorizationURL();
                    synchronized (Twitter.this.lock) {
                        RequestToken unused = Twitter.this.requestToken = newRequestToken;
                    }
                    Intent browserIntent = new Intent("android.intent.action.MAIN", Uri.parse(authURL));
                    browserIntent.setClassName(Twitter.this.container.$context(), Twitter.WEBVIEW_ACTIVITY_CLASS);
                    Twitter.this.container.$context().startActivityForResult(browserIntent, Twitter.this.requestCode);
                } catch (TwitterException e) {
                    Log.i("Twitter", "Got exception: " + e.getMessage());
                    e.printStackTrace();
                    Twitter.this.form.dispatchErrorOccurredEvent(Twitter.this, "Authorize", ErrorMessages.ERROR_TWITTER_EXCEPTION, e.getMessage());
                    Twitter.this.DeAuthorize();
                }
            }
        });
    }

    @SimpleFunction(description = "Checks whether we already have access, and if so, causes IsAuthorized event handler to be called.")
    public void CheckAuthorized() {
        final String myConsumerKey = this.consumerKey;
        final String myConsumerSecret = this.consumerSecret;
        AsynchUtil.runAsynchronously(new Runnable() {
            public void run() {
                if (Twitter.this.checkAccessToken(myConsumerKey, myConsumerSecret)) {
                    Twitter.this.handler.post(new Runnable() {
                        public void run() {
                            Twitter.this.IsAuthorized();
                        }
                    });
                }
            }
        });
    }

    public void resultReturned(int requestCode2, int resultCode, Intent data) {
        final RequestToken myRequestToken;
        final twitter4j.Twitter myTwitter;
        Log.i("Twitter", "Got result " + resultCode);
        if (data != null) {
            Uri uri = data.getData();
            if (uri != null) {
                Log.i("Twitter", "Intent URI: " + uri.toString());
                final String oauthVerifier = uri.getQueryParameter("oauth_verifier");
                synchronized (this.lock) {
                    myRequestToken = this.requestToken;
                    myTwitter = this.twitter;
                }
                if (myTwitter == null) {
                    Log.e("Twitter", "twitter field is unexpectedly null");
                    new RuntimeException().printStackTrace();
                    this.form.dispatchErrorOccurredEvent(this, "Authorize", ErrorMessages.ERROR_TWITTER_UNABLE_TO_GET_ACCESS_TOKEN, "internal error: can't access Twitter library");
                }
                if (myRequestToken == null || oauthVerifier == null || oauthVerifier.length() == 0) {
                    this.form.dispatchErrorOccurredEvent(this, "Authorize", ErrorMessages.ERROR_TWITTER_AUTHORIZATION_FAILED, new Object[0]);
                    DeAuthorize();
                    return;
                }
                AsynchUtil.runAsynchronously(new Runnable() {
                    public void run() {
                        AccessToken resultAccessToken;
                        try {
                            synchronized (Twitter.this.twitterLock) {
                                resultAccessToken = myTwitter.getOAuthAccessToken(myRequestToken, oauthVerifier);
                            }
                            synchronized (Twitter.this.lock) {
                                AccessToken unused = Twitter.this.accessToken = resultAccessToken;
                                String unused2 = Twitter.this.userName = Twitter.this.accessToken.getScreenName();
                                Twitter.this.saveAccessToken(resultAccessToken);
                            }
                            Twitter.this.handler.post(new Runnable() {
                                public void run() {
                                    Twitter.this.IsAuthorized();
                                }
                            });
                        } catch (TwitterException e) {
                            Log.e("Twitter", "Got exception: " + e.getMessage());
                            e.printStackTrace();
                            Twitter.this.form.dispatchErrorOccurredEvent(Twitter.this, "Authorize", ErrorMessages.ERROR_TWITTER_UNABLE_TO_GET_ACCESS_TOKEN, e.getMessage());
                            Twitter.this.DeAuthorize();
                        }
                    }
                });
                return;
            }
            Log.e("Twitter", "uri retured from WebView activity was unexpectedly null");
            return;
        }
        Log.e("Twitter", "intent retured from WebView activity was unexpectedly null");
    }

    /* access modifiers changed from: private */
    public void saveAccessToken(AccessToken accessToken2) {
        SharedPreferences.Editor sharedPrefsEditor = this.sharedPreferences.edit();
        if (accessToken2 == null) {
            sharedPrefsEditor.remove(ACCESS_TOKEN_TAG);
            sharedPrefsEditor.remove(ACCESS_SECRET_TAG);
        } else {
            sharedPrefsEditor.putString(ACCESS_TOKEN_TAG, accessToken2.getToken());
            sharedPrefsEditor.putString(ACCESS_SECRET_TAG, accessToken2.getTokenSecret());
        }
        sharedPrefsEditor.commit();
    }

    private AccessToken retrieveAccessToken() {
        String token = this.sharedPreferences.getString(ACCESS_TOKEN_TAG, ElementType.MATCH_ANY_LOCALNAME);
        String secret = this.sharedPreferences.getString(ACCESS_SECRET_TAG, ElementType.MATCH_ANY_LOCALNAME);
        if (token.length() == 0 || secret.length() == 0) {
            return null;
        }
        return new AccessToken(token, secret);
    }

    @SimpleFunction(description = "Removes Twitter authorization from this running app instance")
    public void DeAuthorize() {
        twitter4j.Twitter oldTwitter;
        synchronized (this.lock) {
            this.requestToken = null;
            this.accessToken = null;
            this.userName = ElementType.MATCH_ANY_LOCALNAME;
            oldTwitter = this.twitter;
            this.twitter = null;
            saveAccessToken(this.accessToken);
        }
        if (oldTwitter != null) {
            synchronized (this.twitterLock) {
                oldTwitter.setOAuthAccessToken(null);
            }
        }
    }

    @SimpleFunction(description = "This updates the logged-in user's status to the specified Text, which will be trimmed if it exceeds 160 characters. <p><u>Requirements</u>: This should only be called after the <code>IsAuthorized</code> event has been raised, indicating that the user has successfully logged in to Twitter.</p>")
    public void SetStatus(final String status) {
        final twitter4j.Twitter myTwitter;
        synchronized (this.lock) {
            myTwitter = this.twitter;
        }
        if (myTwitter == null) {
            this.form.dispatchErrorOccurredEvent(this, "SetStatus", ErrorMessages.ERROR_TWITTER_SET_STATUS_FAILED, "Need to login?");
            return;
        }
        AsynchUtil.runAsynchronously(new Runnable() {
            public void run() {
                try {
                    synchronized (Twitter.this.twitterLock) {
                        myTwitter.updateStatus(status);
                    }
                } catch (TwitterException e) {
                    Twitter.this.form.dispatchErrorOccurredEvent(Twitter.this, "SetStatus", ErrorMessages.ERROR_TWITTER_SET_STATUS_FAILED, e.getMessage());
                }
            }
        });
    }

    @SimpleFunction(description = "Requests the 20 most recent mentions of the logged-in user.  When the mentions have been retrieved, the system will raise the <code>MentionsReceived</code> event and set the <code>Mentions</code> property to the list of mentions.<p><u>Requirements</u>: This should only be called after the <code>IsAuthorized</code> event has been raised, indicating that the user has successfully logged in to Twitter.</p>")
    public void RequestMentions() {
        final twitter4j.Twitter myTwitter;
        synchronized (this.lock) {
            myTwitter = this.twitter;
        }
        if (myTwitter == null) {
            this.form.dispatchErrorOccurredEvent(this, "RequestMentions", ErrorMessages.ERROR_TWITTER_REQUEST_MENTIONS_FAILED, "Need to login?");
            return;
        }
        AsynchUtil.runAsynchronously(new Runnable() {
            List<Status> replies = Collections.emptyList();

            public void run() {
                try {
                    synchronized (Twitter.this.twitterLock) {
                        this.replies = myTwitter.getMentions();
                    }
                    Twitter.this.handler.post(new Runnable() {
                        public void run() {
                            Twitter.this.mentions.clear();
                            for (Status status : AnonymousClass5.this.replies) {
                                Twitter.this.mentions.add(status.getUser().getScreenName() + " " + status.getText());
                            }
                            Twitter.this.MentionsReceived(Twitter.this.mentions);
                        }
                    });
                } catch (TwitterException e) {
                    try {
                        Twitter.this.form.dispatchErrorOccurredEvent(Twitter.this, "RequestMentions", ErrorMessages.ERROR_TWITTER_REQUEST_MENTIONS_FAILED, e.getMessage());
                    } finally {
                        Twitter.this.handler.post(new Runnable() {
                            public void run() {
                                Twitter.this.mentions.clear();
                                for (Status status : AnonymousClass5.this.replies) {
                                    Twitter.this.mentions.add(status.getUser().getScreenName() + " " + status.getText());
                                }
                                Twitter.this.MentionsReceived(Twitter.this.mentions);
                            }
                        });
                    }
                }
            }
        });
    }

    @SimpleEvent(description = "This event is raised when the mentions of the logged-in user requested through <code>RequestMentions</code> have been retrieved.  A list of the mentions can then be found in the <code>mentions</code> parameter or the <code>Mentions</code> property.")
    public void MentionsReceived(List<String> mentions2) {
        EventDispatcher.dispatchEvent(this, "MentionsReceived", mentions2);
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR, description = "This property contains a list of mentions of the logged-in user.  Initially, the list is empty.  To set it, the program must: <ol> <li> Call the <code>Authorize</code> method.</li> <li> Wait for the <code>IsAuthorized</code> event.</li> <li> Call the <code>RequestMentions</code> method.</li> <li> Wait for the <code>MentionsReceived</code> event.</li></ol>\nThe value of this property will then be set to the list of mentions (and will maintain its value until any subsequent calls to <code>RequestMentions</code>).")
    public List<String> Mentions() {
        return this.mentions;
    }

    @SimpleFunction
    public void RequestFollowers() {
        final String myUserId;
        final twitter4j.Twitter myTwitter;
        synchronized (this.lock) {
            myUserId = this.userName;
            myTwitter = this.twitter;
        }
        if (myTwitter == null || myUserId.length() == 0) {
            this.form.dispatchErrorOccurredEvent(this, "RequestFollowers", ErrorMessages.ERROR_TWITTER_REQUEST_FOLLOWERS_FAILED, "Need to login?");
            return;
        }
        AsynchUtil.runAsynchronously(new Runnable() {
            List<User> friends = Collections.emptyList();

            public void run() {
                try {
                    synchronized (Twitter.this.twitterLock) {
                        this.friends = myTwitter.getFollowersStatuses(myUserId, new Paging());
                    }
                    Twitter.this.handler.post(new Runnable() {
                        public void run() {
                            Twitter.this.followers.clear();
                            for (User user : AnonymousClass6.this.friends) {
                                Twitter.this.followers.add(user.getName());
                            }
                            Twitter.this.FollowersReceived(Twitter.this.followers);
                        }
                    });
                } catch (TwitterException e) {
                    try {
                        Twitter.this.form.dispatchErrorOccurredEvent(Twitter.this, "RequestFollowers", ErrorMessages.ERROR_TWITTER_REQUEST_FOLLOWERS_FAILED, e.getMessage());
                    } finally {
                        Twitter.this.handler.post(new Runnable() {
                            public void run() {
                                Twitter.this.followers.clear();
                                for (User user : AnonymousClass6.this.friends) {
                                    Twitter.this.followers.add(user.getName());
                                }
                                Twitter.this.FollowersReceived(Twitter.this.followers);
                            }
                        });
                    }
                }
            }
        });
    }

    @SimpleEvent(description = "This event is raised when all of the followers of the logged-in user requested through <code>RequestFollowers</code> have been retrieved. A list of the followers can then be found in the <code>followers</code> parameter or the <code>Followers</code> property.")
    public void FollowersReceived(List<String> followers2) {
        EventDispatcher.dispatchEvent(this, "FollowersReceived", followers2);
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR, description = "This property contains a list of the followers of the logged-in user.  Initially, the list is empty.  To set it, the program must: <ol> <li> Call the <code>Authorize</code> method.</li> <li> Wait for the <code>IsAuthorized</code> event.</li> <li> Call the <code>RequestFollowers</code> method.</li> <li> Wait for the <code>FollowersReceived</code> event.</li></ol>\nThe value of this property will then be set to the list of followers (and maintain its value until any subsequent call to <code>RequestFollowers</code>).")
    public List<String> Followers() {
        return this.followers;
    }

    @SimpleFunction(description = "Requests the 20 most recent direct messages sent to the logged-in user.  When the messages have been retrieved, the system will raise the <code>DirectMessagesReceived</code> event and set the <code>DirectMessages</code> property to the list of messages.<p><u>Requirements</u>: This should only be called after the <code>IsAuthorized</code> event has been raised, indicating that the user has successfully logged in to Twitter.</p>")
    public void RequestDirectMessages() {
        final twitter4j.Twitter myTwitter;
        synchronized (this.lock) {
            myTwitter = this.twitter;
        }
        if (myTwitter == null) {
            this.form.dispatchErrorOccurredEvent(this, "RequestDirectMessages", ErrorMessages.ERROR_TWITTER_REQUEST_DIRECT_MESSAGES_FAILED, "Need to login?");
            return;
        }
        AsynchUtil.runAsynchronously(new Runnable() {
            List<DirectMessage> messages = Collections.emptyList();

            public void run() {
                try {
                    synchronized (Twitter.this.twitterLock) {
                        this.messages = myTwitter.getDirectMessages();
                    }
                    Twitter.this.handler.post(new Runnable() {
                        public void run() {
                            Twitter.this.directMessages.clear();
                            for (DirectMessage message : AnonymousClass7.this.messages) {
                                Twitter.this.directMessages.add(message.getSenderScreenName() + " " + message.getText());
                            }
                            Twitter.this.DirectMessagesReceived(Twitter.this.directMessages);
                        }
                    });
                } catch (TwitterException e) {
                    try {
                        Twitter.this.form.dispatchErrorOccurredEvent(Twitter.this, "RequestDirectMessages", ErrorMessages.ERROR_TWITTER_REQUEST_DIRECT_MESSAGES_FAILED, e.getMessage());
                    } finally {
                        Twitter.this.handler.post(new Runnable() {
                            public void run() {
                                Twitter.this.directMessages.clear();
                                for (DirectMessage message : AnonymousClass7.this.messages) {
                                    Twitter.this.directMessages.add(message.getSenderScreenName() + " " + message.getText());
                                }
                                Twitter.this.DirectMessagesReceived(Twitter.this.directMessages);
                            }
                        });
                    }
                }
            }
        });
    }

    @SimpleEvent(description = "This event is raised when the recent messages requested through <code>RequestDirectMessages</code> have been retrieved. A list of the messages can then be found in the <code>messages</code> parameter or the <code>Messages</code> property.")
    public void DirectMessagesReceived(List<String> messages) {
        EventDispatcher.dispatchEvent(this, "DirectMessagesReceived", messages);
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR, description = "This property contains a list of the most recent messages mentioning the logged-in user.  Initially, the list is empty.  To set it, the program must: <ol> <li> Call the <code>Authorize</code> method.</li> <li> Wait for the <code>Authorized</code> event.</li> <li> Call the <code>RequestDirectMessages</code> method.</li> <li> Wait for the <code>DirectMessagesReceived</code> event.</li></ol>\nThe value of this property will then be set to the list of direct messages retrieved (and maintain that value until any subsequent call to <code>RequestDirectMessages</code>).")
    public List<String> DirectMessages() {
        return this.directMessages;
    }

    @SimpleFunction(description = "This sends a direct (private) message to the specified user.  The message will be trimmed if it exceeds 160characters. <p><u>Requirements</u>: This should only be called after the <code>IsAuthorized</code> event has been raised, indicating that the user has successfully logged in to Twitter.</p>")
    public void DirectMessage(final String user, final String message) {
        synchronized (this.lock) {
            if (this.twitter == null) {
                this.form.dispatchErrorOccurredEvent(this, "DirectMessage", ErrorMessages.ERROR_TWITTER_DIRECT_MESSAGE_FAILED, "Need to login?");
                return;
            }
            final twitter4j.Twitter myTwitter = this.twitter;
            AsynchUtil.runAsynchronously(new Runnable() {
                public void run() {
                    try {
                        synchronized (Twitter.this.twitterLock) {
                            myTwitter.sendDirectMessage(user, message);
                        }
                    } catch (TwitterException e) {
                        Twitter.this.form.dispatchErrorOccurredEvent(Twitter.this, "DirectMessage", ErrorMessages.ERROR_TWITTER_DIRECT_MESSAGE_FAILED, e.getMessage());
                    }
                }
            });
        }
    }

    @SimpleFunction
    public void Follow(final String user) {
        synchronized (this.lock) {
            if (this.twitter == null) {
                this.form.dispatchErrorOccurredEvent(this, "Follow", ErrorMessages.ERROR_TWITTER_FOLLOW_FAILED, "Need to login?");
                return;
            }
            final twitter4j.Twitter myTwitter = this.twitter;
            AsynchUtil.runAsynchronously(new Runnable() {
                public void run() {
                    try {
                        synchronized (Twitter.this.twitterLock) {
                            myTwitter.enableNotification(user);
                        }
                    } catch (TwitterException e) {
                        Twitter.this.form.dispatchErrorOccurredEvent(Twitter.this, "Follow", ErrorMessages.ERROR_TWITTER_FOLLOW_FAILED, e.getMessage());
                    }
                }
            });
        }
    }

    @SimpleFunction
    public void StopFollowing(final String user) {
        final twitter4j.Twitter myTwitter;
        synchronized (this.lock) {
            myTwitter = this.twitter;
        }
        if (myTwitter == null) {
            this.form.dispatchErrorOccurredEvent(this, "StopFollowing", ErrorMessages.ERROR_TWITTER_STOP_FOLLOWING_FAILED, "Need to login?");
            return;
        }
        AsynchUtil.runAsynchronously(new Runnable() {
            public void run() {
                try {
                    synchronized (Twitter.this.twitterLock) {
                        myTwitter.disableNotification(user);
                    }
                } catch (TwitterException e) {
                    Twitter.this.form.dispatchErrorOccurredEvent(Twitter.this, "StopFollowing", ErrorMessages.ERROR_TWITTER_STOP_FOLLOWING_FAILED, e.getMessage());
                }
            }
        });
    }

    @SimpleFunction
    public void RequestFriendTimeline() {
        final twitter4j.Twitter myTwitter;
        synchronized (this.lock) {
            myTwitter = this.twitter;
        }
        if (myTwitter == null) {
            this.form.dispatchErrorOccurredEvent(this, "RequestFriendTimeline", ErrorMessages.ERROR_TWITTER_REQUEST_FRIEND_TIMELINE_FAILED, "Need to login?");
            return;
        }
        AsynchUtil.runAsynchronously(new Runnable() {
            List<Status> messages = Collections.emptyList();

            public void run() {
                try {
                    this.messages = myTwitter.getFriendsTimeline();
                } catch (TwitterException e) {
                    Twitter.this.form.dispatchErrorOccurredEvent(Twitter.this, "RequestFriendTimeline", ErrorMessages.ERROR_TWITTER_REQUEST_FRIEND_TIMELINE_FAILED, e.getMessage());
                } finally {
                    Twitter.this.handler.post(new Runnable() {
                        public void run() {
                            Twitter.this.timeline.clear();
                            for (Status message : AnonymousClass11.this.messages) {
                                List<String> status = new ArrayList<>();
                                status.add(message.getUser().getName());
                                status.add(message.getText());
                                Twitter.this.timeline.add(status);
                            }
                            Twitter.this.FriendTimelineReceived(Twitter.this.timeline);
                        }
                    });
                }
            }
        });
    }

    @SimpleEvent(description = "This event is raised when the messages requested through <code>RequestFriendTimeline</code> have been retrieved. The <code>timeline</code> parameter and the <code>Timeline</code> property will contain a list of lists, where each sub-list contains a status update of the form (username message)")
    public void FriendTimelineReceived(List<List<String>> timeline2) {
        EventDispatcher.dispatchEvent(this, "FriendTimelineReceived", timeline2);
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR, description = "This property contains the 20 most recent messages of users being followed.  Initially, the list is empty.  To set it, the program must: <ol> <li> Call the <code>Authorize</code> method.</li> <li> Wait for the <code>IsAuthorized</code> event.</li> <li> Specify users to follow with one or more calls to the <code>Follow</code> method.</li> <li> Call the <code>RequestFriendTimeline</code> method.</li> <li> Wait for the <code>FriendTimelineReceived</code> event.</li> </ol>\nThe value of this property will then be set to the list of messages (and maintain its value until any subsequent call to <code>RequestFriendTimeline</code>.")
    public List<List<String>> FriendTimeline() {
        return this.timeline;
    }

    @SimpleFunction
    public void SearchTwitter(final String query) {
        final twitter4j.Twitter myTwitter;
        synchronized (this.lock) {
            if (this.twitter == null) {
                myTwitter = new twitter4j.Twitter();
            } else {
                myTwitter = this.twitter;
            }
        }
        AsynchUtil.runAsynchronously(new Runnable() {
            List<Tweet> tweets = Collections.emptyList();

            public void run() {
                try {
                    synchronized (Twitter.this.twitterLock) {
                        this.tweets = myTwitter.search(new Query(query)).getTweets();
                    }
                    Twitter.this.handler.post(new Runnable() {
                        public void run() {
                            Twitter.this.searchResults.clear();
                            for (Tweet tweet : AnonymousClass12.this.tweets) {
                                Twitter.this.searchResults.add(tweet.getFromUser() + " " + tweet.getText());
                            }
                            Twitter.this.SearchSuccessful(Twitter.this.searchResults);
                        }
                    });
                } catch (TwitterException e) {
                    try {
                        Twitter.this.form.dispatchErrorOccurredEvent(Twitter.this, "SearchTwitter", ErrorMessages.ERROR_TWITTER_SEARCH_FAILED, e.getMessage());
                    } finally {
                        Twitter.this.handler.post(new Runnable() {
                            public void run() {
                                Twitter.this.searchResults.clear();
                                for (Tweet tweet : AnonymousClass12.this.tweets) {
                                    Twitter.this.searchResults.add(tweet.getFromUser() + " " + tweet.getText());
                                }
                                Twitter.this.SearchSuccessful(Twitter.this.searchResults);
                            }
                        });
                    }
                }
            }
        });
    }

    @SimpleEvent(description = "This event is raised when the results of the search requested through <code>SearchSuccessful</code> have been retrieved. A list of the results can then be found in the <code>results</code> parameter or the <code>Results</code> property.")
    public void SearchSuccessful(List<String> searchResults2) {
        EventDispatcher.dispatchEvent(this, "SearchSuccessful", searchResults2);
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR, description = "This property, which is initially empty, is set to a list of search results after the program: <ol><li>Calls the <code>SearchTwitter</code> method.</li> <li>Waits for the <code>SearchSuccessful</code> event.</li></ol>\nThe value of the property will then be the same as the parameter to <code>SearchSuccessful</code>.  Note that it is not necessary to call the <code>Authorize</code> method before calling <code>SearchTwitter</code>.")
    public List<String> SearchResults() {
        return this.searchResults;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean checkAccessToken(java.lang.String r9, java.lang.String r10) {
        /*
            r8 = this;
            r4 = 0
            java.lang.Object r5 = r8.lock
            monitor-enter(r5)
            twitter4j.http.AccessToken r6 = r8.accessToken     // Catch:{ all -> 0x004e }
            if (r6 != 0) goto L_0x000a
            monitor-exit(r5)     // Catch:{ all -> 0x004e }
        L_0x0009:
            return r4
        L_0x000a:
            twitter4j.http.AccessToken r1 = r8.accessToken     // Catch:{ all -> 0x004e }
            twitter4j.Twitter r6 = r8.twitter     // Catch:{ all -> 0x004e }
            if (r6 != 0) goto L_0x0017
            twitter4j.Twitter r6 = new twitter4j.Twitter     // Catch:{ all -> 0x004e }
            r6.<init>()     // Catch:{ all -> 0x004e }
            r8.twitter = r6     // Catch:{ all -> 0x004e }
        L_0x0017:
            twitter4j.Twitter r2 = r8.twitter     // Catch:{ all -> 0x004e }
            monitor-exit(r5)     // Catch:{ all -> 0x004e }
            java.lang.Object r6 = r8.twitterLock     // Catch:{ TwitterException -> 0x0054 }
            monitor-enter(r6)     // Catch:{ TwitterException -> 0x0054 }
            r2.setOAuthConsumer(r9, r10)     // Catch:{ all -> 0x0051 }
            r2.setOAuthAccessToken(r1)     // Catch:{ all -> 0x0051 }
            twitter4j.User r3 = r2.verifyCredentials()     // Catch:{ all -> 0x0051 }
            monitor-exit(r6)     // Catch:{ all -> 0x0051 }
            java.lang.Object r6 = r8.lock     // Catch:{ TwitterException -> 0x0054 }
            monitor-enter(r6)     // Catch:{ TwitterException -> 0x0054 }
            java.lang.String r5 = r3.getScreenName()     // Catch:{ all -> 0x006d }
            r8.userName = r5     // Catch:{ all -> 0x006d }
            monitor-exit(r6)     // Catch:{ all -> 0x006d }
            java.lang.String r5 = "Twitter"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ TwitterException -> 0x0054 }
            r6.<init>()     // Catch:{ TwitterException -> 0x0054 }
            java.lang.String r7 = "Saved accessToken is valid. UserId is "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ TwitterException -> 0x0054 }
            java.lang.String r7 = r8.userName     // Catch:{ TwitterException -> 0x0054 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ TwitterException -> 0x0054 }
            java.lang.String r6 = r6.toString()     // Catch:{ TwitterException -> 0x0054 }
            android.util.Log.i(r5, r6)     // Catch:{ TwitterException -> 0x0054 }
            r4 = 1
            goto L_0x0009
        L_0x004e:
            r4 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x004e }
            throw r4
        L_0x0051:
            r5 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x0051 }
            throw r5     // Catch:{ TwitterException -> 0x0054 }
        L_0x0054:
            r0 = move-exception
            java.lang.Object r5 = r8.lock
            monitor-enter(r5)
            r6 = 0
            r8.accessToken = r6     // Catch:{ all -> 0x0070 }
            java.lang.String r6 = ""
            r8.userName = r6     // Catch:{ all -> 0x0070 }
            twitter4j.http.AccessToken r6 = r8.accessToken     // Catch:{ all -> 0x0070 }
            r8.saveAccessToken(r6)     // Catch:{ all -> 0x0070 }
            monitor-exit(r5)     // Catch:{ all -> 0x0070 }
            java.lang.String r5 = "Twitter"
            java.lang.String r6 = "Saved access token is not valid---clearing it in shared prefs"
            android.util.Log.i(r5, r6)
            goto L_0x0009
        L_0x006d:
            r5 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x006d }
            throw r5     // Catch:{ TwitterException -> 0x0054 }
        L_0x0070:
            r4 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x0070 }
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.devtools.simple.runtime.components.android.Twitter.checkAccessToken(java.lang.String, java.lang.String):boolean");
    }
}
