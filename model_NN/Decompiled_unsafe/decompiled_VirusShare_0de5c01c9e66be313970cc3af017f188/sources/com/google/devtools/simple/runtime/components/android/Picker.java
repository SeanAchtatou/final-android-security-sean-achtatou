package com.google.devtools.simple.runtime.components.android;

import android.content.Intent;
import com.google.devtools.simple.runtime.annotations.SimpleEvent;
import com.google.devtools.simple.runtime.annotations.SimpleFunction;
import com.google.devtools.simple.runtime.annotations.SimpleObject;
import com.google.devtools.simple.runtime.events.EventDispatcher;

@SimpleObject
public abstract class Picker extends ButtonBase implements ActivityResultListener {
    protected final ComponentContainer container;
    protected int requestCode;

    /* access modifiers changed from: protected */
    public abstract Intent getIntent();

    public Picker(ComponentContainer container2) {
        super(container2);
        this.container = container2;
    }

    public void click() {
        BeforePicking();
        if (this.requestCode == 0) {
            this.requestCode = this.container.$form().registerForActivityResult(this);
        }
        this.container.$context().startActivityForResult(getIntent(), this.requestCode);
    }

    @SimpleFunction
    public void Open() {
        click();
    }

    @SimpleEvent
    public void BeforePicking() {
        EventDispatcher.dispatchEvent(this, "BeforePicking", new Object[0]);
    }

    @SimpleEvent
    public void AfterPicking() {
        EventDispatcher.dispatchEvent(this, "AfterPicking", new Object[0]);
    }
}
