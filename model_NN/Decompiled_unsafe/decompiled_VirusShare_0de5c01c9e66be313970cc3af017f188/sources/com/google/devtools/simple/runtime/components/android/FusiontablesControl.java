package com.google.devtools.simple.runtime.components.android;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import com.google.devtools.simple.common.ComponentCategory;
import com.google.devtools.simple.common.PropertyCategory;
import com.google.devtools.simple.runtime.annotations.DesignerComponent;
import com.google.devtools.simple.runtime.annotations.DesignerProperty;
import com.google.devtools.simple.runtime.annotations.SimpleEvent;
import com.google.devtools.simple.runtime.annotations.SimpleFunction;
import com.google.devtools.simple.runtime.annotations.SimpleObject;
import com.google.devtools.simple.runtime.annotations.SimpleProperty;
import com.google.devtools.simple.runtime.annotations.UsesPermissions;
import com.google.devtools.simple.runtime.components.Component;
import com.google.devtools.simple.runtime.components.android.util.ClientLoginHelper;
import com.google.devtools.simple.runtime.components.android.util.IClientLoginHelper;
import com.google.devtools.simple.runtime.components.android.util.SdkLevel;
import com.google.devtools.simple.runtime.events.EventDispatcher;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;

@SimpleObject
@DesignerComponent(category = ComponentCategory.EXPERIMENTAL, description = "A non-visible component that communicates with Google Fusion Tables. Fusion Tables lets you store, share, query and visualize data tables; this component lets you query, create, and modify these tables, using the <a href=\"http://code.google.com/apis/fusiontables/docs/developers_reference.html\" target=\"_blank\">Fusion Tables SQL API</a>.</p>To use the component, define a query, call DoQuery to execute the query, and GotResult will hand you the result when it is ready.<p>Note that you do not need to worry about encoding the query, but you do have to make sure it follows the syntax described in the reference manual, which means that things like capitalization for names of columns matters, and that single quotes need to be used around column names if there are spaces in them. <p>The results of the query will generally be returned in CSV format, and can be converted to list format using the \"list from csv table\" or \"list from csv row\" blocks.", iconName = "images/fusiontables.png", nonVisible = true, version = 1)
@UsesPermissions(permissionNames = "android.permission.INTERNET,android.permission.ACCOUNT_MANAGER,android.permission.MANAGE_ACCOUNTS,android.permission.GET_ACCOUNTS,android.permission.USE_CREDENTIALS")
public class FusiontablesControl extends AndroidNonvisibleComponent implements Component {
    private static final String DEFAULT_QUERY = "show tables";
    private static final String DIALOG_TEXT = "Choose an account to access FusionTables";
    private static final String FUSIONTABLES_SERVICE = "fusiontables";
    private static final String FUSION_QUERY_URL = "http://www.google.com/fusiontables/api/query";
    private static final String LOG_TAG = "fusion";
    private static final int SERVER_TIMEOUT_MS = 30000;
    /* access modifiers changed from: private */
    public final Activity activity;
    private String query = DEFAULT_QUERY;
    /* access modifiers changed from: private */
    public final IClientLoginHelper requestHelper = createClientLoginHelper(DIALOG_TEXT, FUSIONTABLES_SERVICE);

    public FusiontablesControl(ComponentContainer componentContainer) {
        super(componentContainer.$form());
        this.activity = componentContainer.$context();
    }

    @SimpleProperty
    @DesignerProperty(defaultValue = DEFAULT_QUERY, editorType = DesignerProperty.PROPERTY_TYPE_STRING)
    public void Query(String query2) {
        this.query = query2;
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR, description = "The query to send to the Fusion Tables API. <p>For legal query formats and examples, see the Fusion Tables SQL API reference manual. <p>Note that you do not need to worry about encoding the query, but you do have to make sure it follows the syntax described in the reference manual, which means that things like capitalization for names of columns matters, and that single quotes need to be used around column names if there are spaces in them. ")
    public String Query() {
        return this.query;
    }

    @SimpleFunction(description = "Send the query to the Fusion Tables server.")
    public void DoQuery() {
        if (this.requestHelper != null) {
            new QueryProcessor().execute(this.query);
            return;
        }
        this.form.dispatchErrorOccurredEvent(this, "DoQuery", 3, new Object[0]);
    }

    @SimpleEvent(description = "Indicates that the Fusion Tables query has finished processing, with a result.  The result of the query will generally be returned in CSV format, and can be converted to list format using the \"list from csv table\" or \"list from csv row\" blocks.")
    public void GotResult(String result) {
        EventDispatcher.dispatchEvent(this, "GotResult", result);
    }

    private IClientLoginHelper createClientLoginHelper(String accountPrompt, String service) {
        if (SdkLevel.getLevel() < 5) {
            return null;
        }
        HttpClient httpClient = new DefaultHttpClient();
        HttpConnectionParams.setSoTimeout(httpClient.getParams(), SERVER_TIMEOUT_MS);
        HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), SERVER_TIMEOUT_MS);
        return new ClientLoginHelper(this.activity, service, accountPrompt, httpClient);
    }

    /* access modifiers changed from: private */
    public HttpUriRequest genFusiontablesQuery(String query2) throws IOException {
        HttpPost request = new HttpPost(FUSION_QUERY_URL);
        ArrayList<BasicNameValuePair> pair = new ArrayList<>(1);
        pair.add(new BasicNameValuePair("sql", query2));
        UrlEncodedFormEntity entity = new UrlEncodedFormEntity(pair, "UTF-8");
        entity.setContentType("application/x-www-form-urlencoded");
        request.setEntity(entity);
        return request;
    }

    private class QueryProcessor extends AsyncTask<String, Void, String> {
        private ProgressDialog progress;

        private QueryProcessor() {
            this.progress = null;
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            this.progress = ProgressDialog.show(FusiontablesControl.this.activity, "Fusiontables", "processing query...", true);
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            try {
                HttpUriRequest request = FusiontablesControl.this.genFusiontablesQuery(params[0]);
                Log.d(FusiontablesControl.LOG_TAG, "Fetching: " + params[0]);
                HttpResponse response = FusiontablesControl.this.requestHelper.execute(request);
                ByteArrayOutputStream outstream = new ByteArrayOutputStream();
                response.getEntity().writeTo(outstream);
                Log.d(FusiontablesControl.LOG_TAG, "Response: " + response.getStatusLine().toString());
                return outstream.toString();
            } catch (IOException e) {
                e.printStackTrace();
                return e.getMessage();
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            this.progress.dismiss();
            FusiontablesControl.this.GotResult(result);
        }
    }
}
