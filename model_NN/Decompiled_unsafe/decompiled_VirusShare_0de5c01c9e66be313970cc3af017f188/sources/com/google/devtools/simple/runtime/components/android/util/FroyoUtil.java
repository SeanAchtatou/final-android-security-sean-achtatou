package com.google.devtools.simple.runtime.components.android.util;

import android.view.Display;

public class FroyoUtil {
    private FroyoUtil() {
    }

    public static int getRotation(Display display) {
        return display.getRotation();
    }
}
