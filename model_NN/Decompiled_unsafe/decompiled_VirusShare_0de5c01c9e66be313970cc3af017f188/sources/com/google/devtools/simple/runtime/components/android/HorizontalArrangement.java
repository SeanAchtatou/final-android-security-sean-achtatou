package com.google.devtools.simple.runtime.components.android;

import com.google.devtools.simple.common.ComponentCategory;
import com.google.devtools.simple.runtime.annotations.DesignerComponent;
import com.google.devtools.simple.runtime.annotations.SimpleObject;

@SimpleObject
@DesignerComponent(category = ComponentCategory.ARRANGEMENTS, description = "<p>A formatting element in which to place components that should be displayed from left to right.  If you wish to have components displayed one over another, use <code>VerticalArrangement</code> instead.</p>", version = 1)
public class HorizontalArrangement extends HVArrangement {
    public HorizontalArrangement(ComponentContainer container) {
        super(container, 0);
    }
}
