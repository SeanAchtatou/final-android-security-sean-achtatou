package com.google.devtools.simple.runtime.components.android.util;

import android.app.Activity;
import android.util.Log;
import com.google.android.googlelogin.GoogleLoginServiceBlockingHelper;
import gnu.kawa.xml.ElementType;

public class LoginServiceUtil {
    private static final String LOG_TAG = "LoginServiceUtil";

    private LoginServiceUtil() {
    }

    public static String getPhoneEmailAddress(Activity activityContext) {
        String userEmail = ElementType.MATCH_ANY_LOCALNAME;
        try {
            GoogleLoginServiceBlockingHelper googleLoginService = new GoogleLoginServiceBlockingHelper(activityContext);
            userEmail = googleLoginService.getAccount(false);
            googleLoginService.close();
            return userEmail;
        } catch (Exception e) {
            Log.w(LOG_TAG, e);
            return userEmail;
        }
    }
}
