package com.google.devtools.simple.runtime.helpers;

public final class ExprHelpers {
    private ExprHelpers() {
    }

    public static float idiv(float leftOp, float rightOp) {
        return (float) Math.round(leftOp / rightOp);
    }

    public static double idiv(double leftOp, double rightOp) {
        return (double) Math.round(leftOp / rightOp);
    }

    public static boolean like(String string, String pattern) {
        throw new UnsupportedOperationException();
    }

    public static int pow(int leftOp, int rightOp) {
        return (int) Math.pow((double) leftOp, (double) rightOp);
    }

    public static float pow(float leftOp, float rightOp) {
        return (float) Math.pow((double) leftOp, (double) rightOp);
    }
}
