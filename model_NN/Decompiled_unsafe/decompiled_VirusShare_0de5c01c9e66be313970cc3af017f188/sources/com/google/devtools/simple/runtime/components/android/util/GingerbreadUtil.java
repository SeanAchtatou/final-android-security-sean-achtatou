package com.google.devtools.simple.runtime.components.android.util;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookieStore;

public class GingerbreadUtil {
    private GingerbreadUtil() {
    }

    public static CookieHandler newCookieManager() {
        return new CookieManager();
    }

    public static boolean clearCookies(CookieHandler cookieHandler) {
        CookieStore cookieStore;
        if (!(cookieHandler instanceof CookieManager) || (cookieStore = ((CookieManager) cookieHandler).getCookieStore()) == null) {
            return false;
        }
        cookieStore.removeAll();
        return true;
    }
}
