package com.google.devtools.simple.runtime.components.android.util;

import android.os.Build;

public class SdkLevel {
    public static final int LEVEL_CUPCAKE = 3;
    public static final int LEVEL_DONUT = 4;
    public static final int LEVEL_ECLAIR = 5;
    public static final int LEVEL_ECLAIR_0_1 = 6;
    public static final int LEVEL_ECLAIR_MR1 = 7;
    public static final int LEVEL_FROYO = 8;
    public static final int LEVEL_GINGERBREAD = 9;
    public static final int LEVEL_GINGERBREAD_MR1 = 10;
    public static final int LEVEL_HONEYCOMB = 11;

    private SdkLevel() {
    }

    public static int getLevel() {
        return Integer.parseInt(Build.VERSION.SDK);
    }
}
