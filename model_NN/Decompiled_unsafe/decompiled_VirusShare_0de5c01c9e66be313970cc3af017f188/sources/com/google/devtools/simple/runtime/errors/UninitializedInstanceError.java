package com.google.devtools.simple.runtime.errors;

import com.google.devtools.simple.runtime.annotations.SimpleObject;

@SimpleObject
public class UninitializedInstanceError extends RuntimeError {
}
