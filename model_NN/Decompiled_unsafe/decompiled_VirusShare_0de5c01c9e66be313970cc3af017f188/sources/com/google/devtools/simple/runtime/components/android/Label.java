package com.google.devtools.simple.runtime.components.android;

import android.view.View;
import android.widget.TextView;
import com.google.android.googlelogin.GoogleLoginServiceConstants;
import com.google.devtools.simple.common.ComponentCategory;
import com.google.devtools.simple.common.PropertyCategory;
import com.google.devtools.simple.runtime.annotations.DesignerComponent;
import com.google.devtools.simple.runtime.annotations.DesignerProperty;
import com.google.devtools.simple.runtime.annotations.SimpleObject;
import com.google.devtools.simple.runtime.annotations.SimpleProperty;
import com.google.devtools.simple.runtime.components.Component;
import com.google.devtools.simple.runtime.components.android.util.TextViewUtil;
import gnu.kawa.xml.ElementType;

@SimpleObject
@DesignerComponent(category = ComponentCategory.BASIC, description = "A Label displays a piece of text, which is specified through the <code>Text</code> property.  Other properties, all of which can be set in the Designer or Blocks Editor, control the appearance and placement of the text.", version = 2)
public final class Label extends AndroidViewComponent {
    private int backgroundColor;
    private boolean bold;
    private int fontTypeface = 0;
    private boolean italic;
    private int textAlignment;
    private int textColor;
    private final TextView view;

    public Label(ComponentContainer container) {
        super(container);
        this.view = new TextView(container.$context());
        container.$add(this);
        TextAlignment(0);
        BackgroundColor(Component.COLOR_NONE);
        TextViewUtil.setFontTypeface(this.view, this.fontTypeface, this.bold, this.italic);
        FontSize(14.0f);
        Text(ElementType.MATCH_ANY_LOCALNAME);
        TextColor(Component.COLOR_BLACK);
    }

    public View getView() {
        return this.view;
    }

    @SimpleProperty(category = PropertyCategory.APPEARANCE, userVisible = GoogleLoginServiceConstants.PREFER_HOSTED)
    public int TextAlignment() {
        return this.textAlignment;
    }

    @SimpleProperty(userVisible = GoogleLoginServiceConstants.PREFER_HOSTED)
    @DesignerProperty(defaultValue = "0", editorType = DesignerProperty.PROPERTY_TYPE_TEXTALIGNMENT)
    public void TextAlignment(int alignment) {
        this.textAlignment = alignment;
        TextViewUtil.setAlignment(this.view, alignment, false);
    }

    @SimpleProperty(category = PropertyCategory.APPEARANCE)
    public int BackgroundColor() {
        return this.backgroundColor;
    }

    @SimpleProperty
    @DesignerProperty(defaultValue = Component.DEFAULT_VALUE_COLOR_NONE, editorType = DesignerProperty.PROPERTY_TYPE_COLOR)
    public void BackgroundColor(int argb) {
        this.backgroundColor = argb;
        if (argb != 0) {
            TextViewUtil.setBackgroundColor(this.view, argb);
        } else {
            TextViewUtil.setBackgroundColor(this.view, Component.COLOR_NONE);
        }
    }

    @SimpleProperty(category = PropertyCategory.APPEARANCE, userVisible = GoogleLoginServiceConstants.PREFER_HOSTED)
    public boolean FontBold() {
        return this.bold;
    }

    @SimpleProperty(userVisible = GoogleLoginServiceConstants.PREFER_HOSTED)
    @DesignerProperty(defaultValue = "False", editorType = DesignerProperty.PROPERTY_TYPE_BOOLEAN)
    public void FontBold(boolean bold2) {
        this.bold = bold2;
        TextViewUtil.setFontTypeface(this.view, this.fontTypeface, bold2, this.italic);
    }

    @SimpleProperty(category = PropertyCategory.APPEARANCE, userVisible = GoogleLoginServiceConstants.PREFER_HOSTED)
    public boolean FontItalic() {
        return this.italic;
    }

    @SimpleProperty(userVisible = GoogleLoginServiceConstants.PREFER_HOSTED)
    @DesignerProperty(defaultValue = "False", editorType = DesignerProperty.PROPERTY_TYPE_BOOLEAN)
    public void FontItalic(boolean italic2) {
        this.italic = italic2;
        TextViewUtil.setFontTypeface(this.view, this.fontTypeface, this.bold, italic2);
    }

    @SimpleProperty(category = PropertyCategory.APPEARANCE, userVisible = GoogleLoginServiceConstants.PREFER_HOSTED)
    public float FontSize() {
        return TextViewUtil.getFontSize(this.view);
    }

    @SimpleProperty(userVisible = GoogleLoginServiceConstants.PREFER_HOSTED)
    @DesignerProperty(defaultValue = "14.0", editorType = DesignerProperty.PROPERTY_TYPE_NON_NEGATIVE_FLOAT)
    public void FontSize(float size) {
        TextViewUtil.setFontSize(this.view, size);
    }

    @SimpleProperty(category = PropertyCategory.APPEARANCE, userVisible = GoogleLoginServiceConstants.PREFER_HOSTED)
    public int FontTypeface() {
        return this.fontTypeface;
    }

    @SimpleProperty(userVisible = GoogleLoginServiceConstants.PREFER_HOSTED)
    @DesignerProperty(defaultValue = "0", editorType = DesignerProperty.PROPERTY_TYPE_TYPEFACE)
    public void FontTypeface(int typeface) {
        this.fontTypeface = typeface;
        TextViewUtil.setFontTypeface(this.view, this.fontTypeface, this.bold, this.italic);
    }

    @SimpleProperty(category = PropertyCategory.APPEARANCE)
    public String Text() {
        return TextViewUtil.getText(this.view);
    }

    @SimpleProperty
    @DesignerProperty(defaultValue = ElementType.MATCH_ANY_LOCALNAME, editorType = DesignerProperty.PROPERTY_TYPE_STRING)
    public void Text(String text) {
        TextViewUtil.setText(this.view, text);
    }

    @SimpleProperty(category = PropertyCategory.APPEARANCE)
    public int TextColor() {
        return this.textColor;
    }

    @SimpleProperty
    @DesignerProperty(defaultValue = Component.DEFAULT_VALUE_COLOR_BLACK, editorType = DesignerProperty.PROPERTY_TYPE_COLOR)
    public void TextColor(int argb) {
        this.textColor = argb;
        if (argb != 0) {
            TextViewUtil.setTextColor(this.view, argb);
        } else {
            TextViewUtil.setTextColor(this.view, Component.COLOR_BLACK);
        }
    }
}
