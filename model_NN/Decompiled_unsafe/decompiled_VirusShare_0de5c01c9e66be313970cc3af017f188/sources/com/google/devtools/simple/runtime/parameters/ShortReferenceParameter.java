package com.google.devtools.simple.runtime.parameters;

public final class ShortReferenceParameter extends ReferenceParameter {
    private short value;

    public ShortReferenceParameter(short value2) {
        set(value2);
    }

    public short get() {
        return this.value;
    }

    public void set(short value2) {
        this.value = value2;
    }
}
