package com.google.devtools.simple.runtime.components.android.util;

import java.text.DecimalFormat;

public final class YailNumberToString {
    private static final double BIGBOUND = 1000000.0d;
    private static final double SMALLBOUND = 1.0E-6d;
    private static final String decPattern = "#####0.0####";
    private static final DecimalFormat formatterDec = new DecimalFormat(decPattern);
    private static final DecimalFormat formatterSci = new DecimalFormat(sciPattern);
    private static final String sciPattern = "0.####E0";

    public static String format(double number) {
        double mag = Math.abs(number);
        if (mag >= BIGBOUND || mag <= SMALLBOUND) {
            return formatterSci.format(number);
        }
        return formatterDec.format(number);
    }
}
