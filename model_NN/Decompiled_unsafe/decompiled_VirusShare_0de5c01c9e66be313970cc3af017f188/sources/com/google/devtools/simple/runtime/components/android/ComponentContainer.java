package com.google.devtools.simple.runtime.components.android;

import android.app.Activity;

public interface ComponentContainer {
    void $add(AndroidViewComponent androidViewComponent);

    Activity $context();

    Form $form();

    void setChildHeight(AndroidViewComponent androidViewComponent, int i);

    void setChildWidth(AndroidViewComponent androidViewComponent, int i);
}
