package com.google.devtools.simple.runtime.helpers;

import com.google.devtools.simple.runtime.Arrays;
import com.google.devtools.simple.runtime.collections.Collection;
import com.google.devtools.simple.runtime.variants.ArrayVariant;
import com.google.devtools.simple.runtime.variants.IntegerVariant;
import com.google.devtools.simple.runtime.variants.ObjectVariant;
import com.google.devtools.simple.runtime.variants.Variant;

public final class StmtHelpers {
    private StmtHelpers() {
    }

    public static int forEachCount(Variant v) {
        if (v instanceof ArrayVariant) {
            return Arrays.UBound(v, 1);
        }
        return ((Collection) ((ObjectVariant) v).getObject()).Count();
    }

    public static Variant forEachItem(Variant v, int index) {
        if (!(v instanceof ArrayVariant)) {
            return ((Collection) ((ObjectVariant) v).getObject()).Item(index);
        }
        return ((ArrayVariant) v).array(new Variant[]{IntegerVariant.getIntegerVariant(index)});
    }
}
