package com.google.devtools.simple.runtime.components.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ScrollView;
import com.google.devtools.simple.common.ComponentCategory;
import com.google.devtools.simple.common.PropertyCategory;
import com.google.devtools.simple.runtime.annotations.DesignerComponent;
import com.google.devtools.simple.runtime.annotations.DesignerProperty;
import com.google.devtools.simple.runtime.annotations.SimpleEvent;
import com.google.devtools.simple.runtime.annotations.SimpleObject;
import com.google.devtools.simple.runtime.annotations.SimpleProperty;
import com.google.devtools.simple.runtime.annotations.UsesPermissions;
import com.google.devtools.simple.runtime.components.Component;
import com.google.devtools.simple.runtime.components.HandlesEventDispatching;
import com.google.devtools.simple.runtime.components.android.collect.Lists;
import com.google.devtools.simple.runtime.components.android.collect.Maps;
import com.google.devtools.simple.runtime.components.android.collect.Sets;
import com.google.devtools.simple.runtime.components.android.util.MediaUtil;
import com.google.devtools.simple.runtime.components.android.util.SdkLevel;
import com.google.devtools.simple.runtime.components.android.util.ViewUtil;
import com.google.devtools.simple.runtime.components.util.ErrorMessages;
import com.google.devtools.simple.runtime.events.EventDispatcher;
import gnu.kawa.xml.ElementType;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@SimpleObject
@UsesPermissions(permissionNames = "android.permission.INTERNET")
@DesignerComponent(category = ComponentCategory.ARRANGEMENTS, description = "Top-level component containing all other components in the program", showOnPalette = false, version = 6)
public class Form extends Activity implements Component, ComponentContainer, HandlesEventDispatching {
    private static final boolean APPEND_TITLES = false;
    public static final String APPINVENTOR_URL_SCHEME = "appinventor";
    private static final String ARGUMENT_NAME = "APP_INVENTOR_START";
    private static final String LOG_TAG = "Form";
    private static final String RESULT_NAME = "APP_INVENTOR_RESULT";
    private static final int SWITCH_FORM_REQUEST_CODE = 1;
    private static Form activeForm;
    private static long minimumToastWait = 10000000000L;
    private static int nextRequestCode = 2;
    private final HashMap<Integer, ActivityResultListener> activityResultMap = Maps.newHashMap();
    /* access modifiers changed from: private */
    public final Handler androidUIHandler = new Handler();
    private int backgroundColor;
    private Drawable backgroundDrawable;
    private String backgroundImagePath = ElementType.MATCH_ANY_LOCALNAME;
    private String formName;
    /* access modifiers changed from: private */
    public FrameLayout frameLayout;
    private long lastToastTime = (System.nanoTime() - minimumToastWait);
    private String nextFormName;
    private final Set<OnDestroyListener> onDestroyListeners = Sets.newHashSet();
    private final Set<OnPauseListener> onPauseListeners = Sets.newHashSet();
    private final Set<OnResumeListener> onResumeListeners = Sets.newHashSet();
    private final Set<OnStopListener> onStopListeners = Sets.newHashSet();
    /* access modifiers changed from: private */
    public boolean screenInitialized;
    private boolean scrollable;
    private String startupValue = ElementType.MATCH_ANY_LOCALNAME;
    private Layout viewLayout;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        String className = getClass().getName();
        this.formName = className.substring(className.lastIndexOf(46) + 1);
        Log.d(LOG_TAG, "Form " + this.formName + " got onCreate");
        activeForm = this;
        Log.i(LOG_TAG, "activeForm is now " + activeForm.formName);
        this.viewLayout = new LinearLayout(this, 1);
        Scrollable(true);
        BackgroundColor(-1);
        Intent startIntent = getIntent();
        if (startIntent != null && startIntent.hasExtra(ARGUMENT_NAME)) {
            this.startupValue = startIntent.getStringExtra(ARGUMENT_NAME);
        }
        $define();
        Initialize();
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        final int newOrientation = newConfig.orientation;
        if (newOrientation == 2 || newOrientation == 1) {
            this.androidUIHandler.post(new Runnable() {
                public void run() {
                    boolean dispatchEventNow = false;
                    if (Form.this.frameLayout != null) {
                        if (newOrientation == 2) {
                            if (Form.this.frameLayout.getWidth() >= Form.this.frameLayout.getHeight()) {
                                dispatchEventNow = true;
                            }
                        } else if (Form.this.frameLayout.getHeight() >= Form.this.frameLayout.getWidth()) {
                            dispatchEventNow = true;
                        }
                    }
                    if (dispatchEventNow) {
                        Form.this.ScreenOrientationChanged();
                    } else {
                        Form.this.androidUIHandler.post(this);
                    }
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        String result;
        Log.d(LOG_TAG, "Form " + this.formName + " got onActivityResult, requestCode = " + requestCode + ", resultCode = " + resultCode);
        if (requestCode == 1) {
            if (data == null || !data.hasExtra(RESULT_NAME)) {
                result = ElementType.MATCH_ANY_LOCALNAME;
            } else {
                result = data.getStringExtra(RESULT_NAME);
            }
            OtherScreenClosed(this.nextFormName, result);
            return;
        }
        ActivityResultListener component = this.activityResultMap.get(Integer.valueOf(requestCode));
        if (component != null) {
            component.resultReturned(requestCode, resultCode, data);
        }
    }

    public int registerForActivityResult(ActivityResultListener listener) {
        int requestCode = generateNewRequestCode();
        this.activityResultMap.put(Integer.valueOf(requestCode), listener);
        return requestCode;
    }

    public void unregisterForActivityResult(ActivityResultListener listener) {
        List<Integer> keysToDelete = Lists.newArrayList();
        for (Map.Entry<Integer, ActivityResultListener> mapEntry : this.activityResultMap.entrySet()) {
            if (listener.equals(mapEntry.getValue())) {
                keysToDelete.add(mapEntry.getKey());
            }
        }
        for (Integer key : keysToDelete) {
            this.activityResultMap.remove(key);
        }
    }

    private static int generateNewRequestCode() {
        int i = nextRequestCode;
        nextRequestCode = i + 1;
        return i;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        Log.d(LOG_TAG, "Form " + this.formName + " got onResume");
        activeForm = this;
        for (OnResumeListener onResumeListener : this.onResumeListeners) {
            onResumeListener.onResume();
        }
    }

    public void registerForOnResume(OnResumeListener component) {
        this.onResumeListeners.add(component);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        Log.d(LOG_TAG, "Form " + this.formName + " got onPause");
        for (OnPauseListener onPauseListener : this.onPauseListeners) {
            onPauseListener.onPause();
        }
    }

    public void registerForOnPause(OnPauseListener component) {
        this.onPauseListeners.add(component);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        Log.d(LOG_TAG, "Form " + this.formName + " got onStop");
        for (OnStopListener onStopListener : this.onStopListeners) {
            onStopListener.onStop();
        }
    }

    public void registerForOnStop(OnStopListener component) {
        this.onStopListeners.add(component);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG, "Form " + this.formName + " got onDestroy");
        EventDispatcher.removeDispatchDelegate(this);
        for (OnDestroyListener onDestroyListener : this.onDestroyListeners) {
            onDestroyListener.onDestroy();
        }
    }

    public void registerForOnDestroy(OnDestroyListener component) {
        this.onDestroyListeners.add(component);
    }

    /* access modifiers changed from: package-private */
    public void $define() {
        throw new UnsupportedOperationException();
    }

    public boolean canDispatchEvent(Component component, String eventName) {
        boolean canDispatch = this.screenInitialized || (component == this && eventName.equals("Initialize"));
        if (canDispatch) {
            activeForm = this;
        }
        return canDispatch;
    }

    public boolean dispatchEvent(Component component, String componentName, String eventName, Object[] args) {
        throw new UnsupportedOperationException();
    }

    @SimpleEvent(description = "Screen starting")
    public void Initialize() {
        this.androidUIHandler.post(new Runnable() {
            public void run() {
                if (Form.this.frameLayout == null || Form.this.frameLayout.getWidth() == 0 || Form.this.frameLayout.getHeight() == 0) {
                    Form.this.androidUIHandler.post(this);
                    return;
                }
                EventDispatcher.dispatchEvent(Form.this, "Initialize", new Object[0]);
                boolean unused = Form.this.screenInitialized = true;
            }
        });
    }

    @SimpleEvent(description = "Screen orientation changed")
    public void ScreenOrientationChanged() {
        EventDispatcher.dispatchEvent(this, "ScreenOrientationChanged", new Object[0]);
    }

    @SimpleEvent(description = "Event raised when an error occurs. Only some errors will raise this condition.  For those errors, the system will show a notification by default.  You can use this event handler to prescribe an error behavior different than the default.")
    public void ErrorOccurred(Component component, String functionName, int errorNumber, String message) {
        String componentType = component.getClass().getName();
        Log.e(LOG_TAG, "Form " + this.formName + " ErrorOccurred, errorNumber = " + errorNumber + ", componentType = " + componentType.substring(componentType.lastIndexOf(".") + 1) + ", functionName = " + functionName + ", messages = " + message);
        if (!EventDispatcher.dispatchEvent(this, "ErrorOccurred", component, functionName, Integer.valueOf(errorNumber), message) && this.screenInitialized) {
            new Notifier(this).ShowAlert("Error " + errorNumber + ": " + message);
        }
    }

    public void dispatchErrorOccurredEvent(Component component, String functionName, int errorNumber, Object... messageArgs) {
        final int i = errorNumber;
        final Object[] objArr = messageArgs;
        final Component component2 = component;
        final String str = functionName;
        runOnUiThread(new Runnable() {
            public void run() {
                Form.this.ErrorOccurred(component2, str, i, ErrorMessages.formatMessage(i, objArr));
            }
        });
    }

    @SimpleProperty(category = PropertyCategory.APPEARANCE)
    public boolean Scrollable() {
        return this.scrollable;
    }

    @SimpleProperty
    @DesignerProperty(defaultValue = "True", editorType = DesignerProperty.PROPERTY_TYPE_BOOLEAN)
    public void Scrollable(boolean scrollable2) {
        if (this.scrollable != scrollable2 || this.frameLayout == null) {
            if (this.frameLayout != null) {
                this.frameLayout.removeAllViews();
            }
            this.scrollable = scrollable2;
            this.frameLayout = scrollable2 ? new ScrollView(this) : new FrameLayout(this);
            this.frameLayout.addView(this.viewLayout.getLayoutManager(), new ViewGroup.LayoutParams(-1, -1));
            this.frameLayout.setBackgroundColor(this.backgroundColor);
            if (this.backgroundDrawable != null) {
                ViewUtil.setBackgroundImage(this.frameLayout, this.backgroundDrawable);
            }
            setContentView(this.frameLayout);
            this.frameLayout.requestLayout();
        }
    }

    @SimpleProperty(category = PropertyCategory.APPEARANCE)
    public int BackgroundColor() {
        return this.backgroundColor;
    }

    @SimpleProperty
    @DesignerProperty(defaultValue = Component.DEFAULT_VALUE_COLOR_WHITE, editorType = DesignerProperty.PROPERTY_TYPE_COLOR)
    public void BackgroundColor(int argb) {
        this.backgroundColor = argb;
        if (argb != 0) {
            this.viewLayout.getLayoutManager().setBackgroundColor(argb);
            this.frameLayout.setBackgroundColor(argb);
            return;
        }
        this.viewLayout.getLayoutManager().setBackgroundColor(-1);
        this.frameLayout.setBackgroundColor(-1);
    }

    @SimpleProperty(category = PropertyCategory.APPEARANCE, description = "The screen background image.")
    public String BackgroundImage() {
        return this.backgroundImagePath;
    }

    @SimpleProperty(category = PropertyCategory.APPEARANCE, description = "The screen background image.")
    @DesignerProperty(defaultValue = ElementType.MATCH_ANY_LOCALNAME, editorType = DesignerProperty.PROPERTY_TYPE_ASSET)
    public void BackgroundImage(String path) {
        if (path == null) {
            path = ElementType.MATCH_ANY_LOCALNAME;
        }
        this.backgroundImagePath = path;
        try {
            this.backgroundDrawable = MediaUtil.getBitmapDrawable(this, this.backgroundImagePath);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Unable to load " + this.backgroundImagePath);
            this.backgroundDrawable = null;
        }
        ViewUtil.setBackgroundImage(this.frameLayout, this.backgroundDrawable);
        this.frameLayout.invalidate();
    }

    @SimpleProperty(category = PropertyCategory.APPEARANCE)
    public String Title() {
        return getTitle().toString();
    }

    @SimpleProperty
    @DesignerProperty(defaultValue = ElementType.MATCH_ANY_LOCALNAME, editorType = DesignerProperty.PROPERTY_TYPE_STRING)
    public void Title(String title) {
        setTitle(title);
    }

    @SimpleProperty(category = PropertyCategory.APPEARANCE, description = "The requested screen orientation. Commonly used values are \"unspecified\", \"landscape\", \"portrait\", \"sensor\", and \"behind\".")
    public String ScreenOrientation() {
        switch (getRequestedOrientation()) {
            case -1:
                return "unspecified";
            case 0:
                return "landscape";
            case 1:
                return "portrait";
            case 2:
                return "user";
            case 3:
                return "behind";
            case 4:
                return "sensor";
            case 5:
                return "nosensor";
            case 6:
                return "sensorLandscape";
            case 7:
                return "sensorPortrait";
            case 8:
                return "reverseLandscape";
            case 9:
                return "reversePortrait";
            case 10:
                return "fullSensor";
            default:
                return "unspecified";
        }
    }

    @SimpleProperty(category = PropertyCategory.APPEARANCE, description = "The requested screen orientation. Commonly used values are \"unspecified\", \"landscape\", \"portrait\", \"sensor\", and \"behind\".")
    @DesignerProperty(defaultValue = "unspecified", editorType = DesignerProperty.PROPERTY_TYPE_SCREEN_ORIENTATION)
    public void ScreenOrientation(String screenOrientation) {
        if (screenOrientation.equalsIgnoreCase("behind")) {
            setRequestedOrientation(3);
        } else if (screenOrientation.equalsIgnoreCase("landscape")) {
            setRequestedOrientation(0);
        } else if (screenOrientation.equalsIgnoreCase("nosensor")) {
            setRequestedOrientation(5);
        } else if (screenOrientation.equalsIgnoreCase("portrait")) {
            setRequestedOrientation(1);
        } else if (screenOrientation.equalsIgnoreCase("sensor")) {
            setRequestedOrientation(4);
        } else if (screenOrientation.equalsIgnoreCase("unspecified")) {
            setRequestedOrientation(-1);
        } else if (screenOrientation.equalsIgnoreCase("user")) {
            setRequestedOrientation(2);
        } else if (SdkLevel.getLevel() < 9) {
            dispatchErrorOccurredEvent(this, "ScreenOrientation", ErrorMessages.ERROR_INVALID_SCREEN_ORIENTATION, screenOrientation);
        } else if (screenOrientation.equalsIgnoreCase("fullSensor")) {
            setRequestedOrientation(10);
        } else if (screenOrientation.equalsIgnoreCase("reverseLandscape")) {
            setRequestedOrientation(8);
        } else if (screenOrientation.equalsIgnoreCase("reversePortrait")) {
            setRequestedOrientation(9);
        } else if (screenOrientation.equalsIgnoreCase("sensorLandscape")) {
            setRequestedOrientation(6);
        } else if (screenOrientation.equalsIgnoreCase("sensorPortrait")) {
            setRequestedOrientation(7);
        } else {
            dispatchErrorOccurredEvent(this, "ScreenOrientation", ErrorMessages.ERROR_INVALID_SCREEN_ORIENTATION, screenOrientation);
        }
    }

    @SimpleProperty(userVisible = false)
    @DesignerProperty(defaultValue = ElementType.MATCH_ANY_LOCALNAME, editorType = DesignerProperty.PROPERTY_TYPE_ASSET)
    public void Icon(String name) {
    }

    @SimpleProperty(category = PropertyCategory.APPEARANCE)
    public int Width() {
        return this.frameLayout.getWidth();
    }

    @SimpleProperty(category = PropertyCategory.APPEARANCE)
    public int Height() {
        return this.frameLayout.getHeight();
    }

    public static void switchForm(String nextFormName2) {
        if (activeForm != null) {
            activeForm.startNewForm(nextFormName2, null);
            return;
        }
        throw new IllegalStateException("activeForm is null");
    }

    public static void switchFormWithStartupValue(String nextFormName2, String startupValue2) {
        if (activeForm != null) {
            activeForm.startNewForm(nextFormName2, startupValue2);
            return;
        }
        throw new IllegalStateException("activeForm is null");
    }

    /* access modifiers changed from: protected */
    public void startNewForm(String nextFormName2, String startupValue2) {
        Intent activityIntent = new Intent();
        activityIntent.setClassName(this, getPackageName() + "." + nextFormName2);
        if (startupValue2 != null) {
            activityIntent.putExtra(ARGUMENT_NAME, startupValue2);
        }
        this.nextFormName = nextFormName2;
        try {
            startActivityForResult(activityIntent, 1);
        } catch (ActivityNotFoundException e) {
            dispatchErrorOccurredEvent(this, startupValue2 == null ? "open another screen" : "open another screen with start text", ErrorMessages.ERROR_SCREEN_NOT_FOUND, nextFormName2);
        }
    }

    @SimpleEvent(description = "Event raised when another screen has closed and control has returned to this screen.")
    public void OtherScreenClosed(String otherScreenName, String result) {
        Log.i(LOG_TAG, "Form " + this.formName + " OtherScreenClosed, otherScreenName = " + otherScreenName + ", result = " + result);
        EventDispatcher.dispatchEvent(this, "OtherScreenClosed", otherScreenName, result);
    }

    public HandlesEventDispatching getDispatchDelegate() {
        return this;
    }

    public Activity $context() {
        return this;
    }

    public Form $form() {
        return this;
    }

    public void $add(AndroidViewComponent component) {
        this.viewLayout.add(component);
    }

    public void setChildWidth(AndroidViewComponent component, int width) {
        ViewUtil.setChildWidthForVerticalLayout(component.getView(), width);
    }

    public void setChildHeight(AndroidViewComponent component, int height) {
        ViewUtil.setChildHeightForVerticalLayout(component.getView(), height);
    }

    public void setTitle(CharSequence newTitle) {
        super.setTitle(newTitle);
    }

    public static Form getActiveForm() {
        return activeForm;
    }

    public static String getStartupValue() {
        if (activeForm != null) {
            return activeForm.startupValue;
        }
        throw new IllegalStateException("activeForm is null");
    }

    public static void finishActivity() {
        if (activeForm != null) {
            activeForm.finish();
            return;
        }
        throw new IllegalStateException("activeForm is null");
    }

    public static void finishActivityWithResult(String result) {
        if (activeForm != null) {
            Intent resultIntent = new Intent();
            resultIntent.putExtra(RESULT_NAME, result);
            activeForm.setResult(-1, resultIntent);
            activeForm.finish();
            return;
        }
        throw new IllegalStateException("activeForm is null");
    }

    public static void finishApplication() {
        if (activeForm != null) {
            activeForm.finish();
            System.exit(0);
            return;
        }
        throw new IllegalStateException("activeForm is null");
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        addExitButtonToMenu(menu);
        return true;
    }

    public void addExitButtonToMenu(Menu menu) {
        menu.add(0, 0, 1, "Stop this application").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                Form.this.showExitApplicationNotification();
                return true;
            }
        }).setIcon(17301560);
    }

    /* access modifiers changed from: private */
    public void showExitApplicationNotification() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Stop application?");
        alertDialog.setCancelable(false);
        alertDialog.setMessage("Stop this application and exit?  You'll need to relaunch the application to use it again.");
        alertDialog.setButton(-1, "Stop and exit", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Form.finishApplication();
            }
        });
        alertDialog.setButton(-2, "Don't stop", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();
    }

    public void clear() {
        this.viewLayout.getLayoutManager().removeAllViews();
        this.screenInitialized = false;
    }

    public void deleteComponent(Object component) {
        if (component instanceof OnStopListener) {
            OnStopListener onStopListener = (OnStopListener) component;
            if (this.onStopListeners.contains(onStopListener)) {
                this.onStopListeners.remove(onStopListener);
            }
        }
        if (component instanceof OnResumeListener) {
            OnResumeListener onResumeListener = (OnResumeListener) component;
            if (this.onResumeListeners.contains(onResumeListener)) {
                this.onResumeListeners.remove(onResumeListener);
            }
        }
        if (component instanceof OnPauseListener) {
            OnPauseListener onPauseListener = (OnPauseListener) component;
            if (this.onPauseListeners.contains(onPauseListener)) {
                this.onPauseListeners.remove(onPauseListener);
            }
        }
        if (component instanceof OnDestroyListener) {
            OnDestroyListener onDestroyListener = (OnDestroyListener) component;
            if (this.onDestroyListeners.contains(onDestroyListener)) {
                this.onDestroyListeners.remove(onDestroyListener);
            }
        }
        if (component instanceof Deleteable) {
            ((Deleteable) component).onDelete();
        }
    }

    public void dontGrabTouchEventsForComponent() {
        this.frameLayout.requestDisallowInterceptTouchEvent(true);
    }

    /* access modifiers changed from: protected */
    public boolean toastAllowed() {
        long now = System.nanoTime();
        if (now <= this.lastToastTime + minimumToastWait) {
            return false;
        }
        this.lastToastTime = now;
        return true;
    }

    public void callInitialize(Object component) throws Throwable {
        try {
            Method method = component.getClass().getMethod("Initialize", null);
            try {
                Log.d(LOG_TAG, "calling Initialize method for Object " + component.toString());
                method.invoke(component, null);
            } catch (InvocationTargetException e) {
                Log.d(LOG_TAG, "invoke exception: " + e.getMessage());
                throw e.getTargetException();
            }
        } catch (SecurityException e2) {
            Log.d(LOG_TAG, "Security exception " + e2.getMessage());
        } catch (NoSuchMethodException e3) {
        }
    }
}
