package com.google.devtools.simple.runtime.variants;

import gnu.bytecode.ConstantPool;
import java.util.Calendar;

public final class DateVariant extends Variant {
    private Calendar value;

    public static final DateVariant getDateVariant(Calendar value2) {
        return new DateVariant(value2);
    }

    private DateVariant(Calendar value2) {
        super(ConstantPool.INTERFACE_METHODREF);
        this.value = value2;
    }

    public Calendar getDate() {
        return this.value;
    }

    public boolean identical(Variant rightOp) {
        return cmp(rightOp) == 0;
    }

    public int cmp(Variant rightOp) {
        if (rightOp.getKind() != 11) {
            return super.cmp(rightOp);
        }
        return this.value.compareTo(rightOp.getDate());
    }

    public boolean typeof(Class<?> type) {
        return type.isInstance(this.value);
    }
}
