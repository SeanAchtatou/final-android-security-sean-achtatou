package com.google.devtools.simple.runtime.components.android;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ListPickerActivity extends ListActivity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent().hasExtra(ListPicker.LIST_ACTIVITY_ARG_NAME)) {
            setListAdapter(new ArrayAdapter(this, 17367043, getIntent().getStringArrayExtra(ListPicker.LIST_ACTIVITY_ARG_NAME)));
            getListView().setTextFilterEnabled(true);
            return;
        }
        setResult(0);
        finish();
    }

    public void onListItemClick(ListView lv, View v, int position, long id) {
        Intent resultIntent = new Intent();
        resultIntent.putExtra(ListPicker.LIST_ACTIVITY_RESULT_NAME, (String) getListView().getItemAtPosition(position));
        resultIntent.putExtra(ListPicker.LIST_ACTIVITY_RESULT_INDEX, position + 1);
        setResult(-1, resultIntent);
        finish();
    }
}
