package com.google.devtools.simple.runtime.components.android;

import com.google.devtools.simple.common.ComponentCategory;
import com.google.devtools.simple.runtime.annotations.DesignerComponent;
import com.google.devtools.simple.runtime.annotations.SimpleObject;

@SimpleObject
@DesignerComponent(category = ComponentCategory.ARRANGEMENTS, description = "<p>A formatting element in which to place components that should be displayed one below another.  (The first child component is stored on top, the second beneath it, etc.)  If you wish to have components displayed next to one another, use <code>HorizontalArrangement</code> instead.</p>", version = 1)
public class VerticalArrangement extends HVArrangement {
    public VerticalArrangement(ComponentContainer container) {
        super(container, 1);
    }
}
