package com.google.devtools.simple.runtime.components.android.util;

import com.google.devtools.simple.runtime.components.android.collect.Lists;
import com.google.devtools.simple.runtime.components.util.YailList;
import gnu.kawa.xml.ElementType;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;
import twitter4j.org.json.HTTP;

public final class CsvUtil {
    private CsvUtil() {
    }

    public static YailList fromCsvTable(String csvString) throws Exception {
        CsvParser csvParser = new CsvParser(new StringReader(csvString));
        ArrayList<YailList> csvList = new ArrayList<>();
        while (csvParser.hasNext()) {
            csvList.add(YailList.makeList((List) csvParser.next()));
        }
        csvParser.throwAnyProblem();
        return YailList.makeList((List) csvList);
    }

    public static YailList fromCsvRow(String csvString) throws Exception {
        CsvParser csvParser = new CsvParser(new StringReader(csvString));
        if (csvParser.hasNext()) {
            YailList row = YailList.makeList((List) csvParser.next());
            if (csvParser.hasNext()) {
                throw new IllegalArgumentException("CSV text has multiple rows. Expected just one row.");
            }
            csvParser.throwAnyProblem();
            return row;
        }
        throw new IllegalArgumentException("CSV text cannot be parsed as a row.");
    }

    public static String toCsvRow(YailList csvRow) {
        StringBuilder csvStringBuilder = new StringBuilder();
        makeCsvRow(csvRow, csvStringBuilder);
        return csvStringBuilder.toString();
    }

    public static String toCsvTable(YailList csvList) {
        StringBuilder csvStringBuilder = new StringBuilder();
        for (Object rowObj : csvList.toArray()) {
            makeCsvRow((YailList) rowObj, csvStringBuilder);
            csvStringBuilder.append(HTTP.CRLF);
        }
        return csvStringBuilder.toString();
    }

    private static void makeCsvRow(YailList row, StringBuilder csvStringBuilder) {
        String fieldDelim = ElementType.MATCH_ANY_LOCALNAME;
        for (Object fieldObj : row.toArray()) {
            csvStringBuilder.append(fieldDelim).append("\"").append(fieldObj.toString().replaceAll("\"", "\"\"")).append("\"");
            fieldDelim = ",";
        }
    }

    private static class CsvParser implements Iterator<List<String>> {
        private final Pattern ESCAPED_QUOTE_PATTERN = Pattern.compile("\"\"");
        private final char[] buf = new char[10240];
        private int cellLength = -1;
        private int delimitedCellLength = -1;
        private final Reader in;
        private Exception lastException;
        private int limit;
        private boolean opened = true;
        private int pos;
        private long previouslyRead;

        public CsvParser(Reader in2) {
            this.in = in2;
        }

        public void skip(long charPosition) throws IOException {
            int n;
            while (charPosition > 0 && (n = this.in.read(this.buf, 0, Math.min((int) charPosition, this.buf.length))) >= 0) {
                this.previouslyRead += (long) n;
                charPosition -= (long) n;
            }
        }

        public boolean hasNext() {
            if (this.limit == 0) {
                fill();
            }
            return (this.pos < this.limit || indexAfterCompactionAndFilling(this.pos) < this.limit) && lookingAtCell();
        }

        public ArrayList<String> next() {
            boolean trailingComma;
            boolean haveMoreData;
            ArrayList<String> result = Lists.newArrayList();
            do {
                if (this.buf[this.pos] != '\"') {
                    result.add(new String(this.buf, this.pos, this.cellLength));
                } else {
                    result.add(this.ESCAPED_QUOTE_PATTERN.matcher(new String(this.buf, this.pos + 1, this.cellLength - 2)).replaceAll("\""));
                }
                if (this.delimitedCellLength <= 0 || this.buf[(this.pos + this.delimitedCellLength) - 1] != ',') {
                    trailingComma = false;
                } else {
                    trailingComma = true;
                }
                this.pos += this.delimitedCellLength;
                this.cellLength = -1;
                this.delimitedCellLength = -1;
                if (this.pos < this.limit || indexAfterCompactionAndFilling(this.pos) < this.limit) {
                    haveMoreData = true;
                } else {
                    haveMoreData = false;
                }
                if (!trailingComma || !haveMoreData) {
                    return result;
                }
            } while (lookingAtCell());
            return result;
        }

        public long getCharPosition() {
            return this.previouslyRead + ((long) this.pos);
        }

        private int indexAfterCompactionAndFilling(int i) {
            if (this.pos > 0) {
                i = compact(i);
            }
            fill();
            return i;
        }

        private int compact(int i) {
            int oldPos = this.pos;
            this.pos = 0;
            int toMove = this.limit - oldPos;
            if (toMove > 0) {
                System.arraycopy(this.buf, oldPos, this.buf, 0, toMove);
            }
            this.limit -= oldPos;
            this.previouslyRead += (long) oldPos;
            return i - oldPos;
        }

        private void fill() {
            int toFill = this.buf.length - this.limit;
            while (this.opened && toFill > 0) {
                try {
                    int n = this.in.read(this.buf, this.limit, toFill);
                    if (n == -1) {
                        this.opened = false;
                    } else {
                        this.limit += n;
                        toFill -= n;
                    }
                } catch (IOException e) {
                    this.lastException = e;
                    this.opened = false;
                }
            }
        }

        private boolean lookingAtCell() {
            return this.buf[this.pos] == '\"' ? findUnescapedEndQuote(this.pos + 1) : findUnquotedCellEnd(this.pos);
        }

        private boolean findUnescapedEndQuote(int i) {
            while (true) {
                if (i >= this.limit && (i = indexAfterCompactionAndFilling(i)) >= this.limit) {
                    this.lastException = new IllegalArgumentException("Syntax Error. unclosed quoted cell");
                    return false;
                } else if (this.buf[i] != '\"' || ((i = checkedIndex(i + 1)) != this.limit && this.buf[i] == '\"')) {
                    i++;
                }
            }
            this.cellLength = i - this.pos;
            return findDelimOrEnd(i);
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
            jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
            	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
            	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
            */
        /* JADX WARNING: Removed duplicated region for block: B:13:0x0046 A[LOOP:0: B:1:0x0001->B:13:0x0046, LOOP_END] */
        /* JADX WARNING: Removed duplicated region for block: B:15:0x0014 A[SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:16:0x001f A[SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x003a A[SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:3:0x0005  */
        private boolean findDelimOrEnd(int r5) {
            /*
                r4 = this;
                r1 = 1
            L_0x0001:
                int r2 = r4.limit
                if (r5 < r2) goto L_0x000d
                int r5 = r4.indexAfterCompactionAndFilling(r5)
                int r2 = r4.limit
                if (r5 >= r2) goto L_0x0049
            L_0x000d:
                char[] r2 = r4.buf
                char r2 = r2[r5]
                switch(r2) {
                    case 9: goto L_0x0046;
                    case 10: goto L_0x003a;
                    case 13: goto L_0x001f;
                    case 32: goto L_0x0046;
                    case 44: goto L_0x003a;
                    default: goto L_0x0014;
                }
            L_0x0014:
                java.io.IOException r1 = new java.io.IOException
                java.lang.String r2 = "Syntax Error: non-whitespace between closing quote and delimiter or end"
                r1.<init>(r2)
                r4.lastException = r1
                r1 = 0
            L_0x001e:
                return r1
            L_0x001f:
                int r2 = r5 + 1
                int r0 = r4.checkedIndex(r2)
                char[] r2 = r4.buf
                char r2 = r2[r0]
                r3 = 10
                if (r2 != r3) goto L_0x0033
                int r2 = r0 + 1
                int r0 = r4.checkedIndex(r2)
            L_0x0033:
                int r2 = r4.pos
                int r2 = r0 - r2
                r4.delimitedCellLength = r2
                goto L_0x001e
            L_0x003a:
                int r2 = r5 + 1
                int r2 = r4.checkedIndex(r2)
                int r3 = r4.pos
                int r2 = r2 - r3
                r4.delimitedCellLength = r2
                goto L_0x001e
            L_0x0046:
                int r5 = r5 + 1
                goto L_0x0001
            L_0x0049:
                int r2 = r4.limit
                int r3 = r4.pos
                int r2 = r2 - r3
                r4.delimitedCellLength = r2
                goto L_0x001e
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.devtools.simple.runtime.components.android.util.CsvUtil.CsvParser.findDelimOrEnd(int):boolean");
        }

        private int checkedIndex(int i) {
            return i < this.limit ? i : indexAfterCompactionAndFilling(i);
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
            jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
            	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
            	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
            */
        /* JADX WARNING: Removed duplicated region for block: B:15:0x0017 A[SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:16:0x0024 A[SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0045 A[SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:3:0x0005  */
        /* JADX WARNING: Removed duplicated region for block: B:7:0x0014 A[LOOP:0: B:1:0x0001->B:7:0x0014, LOOP_END] */
        private boolean findUnquotedCellEnd(int r5) {
            /*
                r4 = this;
                r1 = 1
            L_0x0001:
                int r2 = r4.limit
                if (r5 < r2) goto L_0x000d
                int r5 = r4.indexAfterCompactionAndFilling(r5)
                int r2 = r4.limit
                if (r5 >= r2) goto L_0x0050
            L_0x000d:
                char[] r2 = r4.buf
                char r2 = r2[r5]
                switch(r2) {
                    case 10: goto L_0x0017;
                    case 13: goto L_0x0024;
                    case 34: goto L_0x0045;
                    case 44: goto L_0x0017;
                    default: goto L_0x0014;
                }
            L_0x0014:
                int r5 = r5 + 1
                goto L_0x0001
            L_0x0017:
                int r2 = r4.pos
                int r2 = r5 - r2
                r4.cellLength = r2
                int r2 = r4.cellLength
                int r2 = r2 + 1
                r4.delimitedCellLength = r2
            L_0x0023:
                return r1
            L_0x0024:
                int r2 = r4.pos
                int r2 = r5 - r2
                r4.cellLength = r2
                int r2 = r5 + 1
                int r0 = r4.checkedIndex(r2)
                char[] r2 = r4.buf
                char r2 = r2[r0]
                r3 = 10
                if (r2 != r3) goto L_0x003e
                int r2 = r0 + 1
                int r0 = r4.checkedIndex(r2)
            L_0x003e:
                int r2 = r4.pos
                int r2 = r0 - r2
                r4.delimitedCellLength = r2
                goto L_0x0023
            L_0x0045:
                java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
                java.lang.String r2 = "Syntax Error: quote in unquoted cell"
                r1.<init>(r2)
                r4.lastException = r1
                r1 = 0
                goto L_0x0023
            L_0x0050:
                int r2 = r4.limit
                int r3 = r4.pos
                int r2 = r2 - r3
                r4.cellLength = r2
                r4.delimitedCellLength = r2
                goto L_0x0023
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.devtools.simple.runtime.components.android.util.CsvUtil.CsvParser.findUnquotedCellEnd(int):boolean");
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }

        public void throwAnyProblem() throws Exception {
            if (this.lastException != null) {
                throw this.lastException;
            }
        }
    }
}
