package com.google.devtools.simple.runtime.components.android.util;

import java.util.Locale;

public interface ITextToSpeech {

    public interface TextToSpeechCallback {
        void onFailure();

        void onSuccess();
    }

    void onResume();

    void onStop();

    void speak(String str, Locale locale);
}
