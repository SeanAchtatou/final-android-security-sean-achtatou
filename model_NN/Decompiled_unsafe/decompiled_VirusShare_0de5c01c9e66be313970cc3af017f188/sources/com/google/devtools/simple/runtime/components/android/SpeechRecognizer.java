package com.google.devtools.simple.runtime.components.android;

import android.content.Intent;
import android.util.Log;
import com.google.devtools.simple.common.ComponentCategory;
import com.google.devtools.simple.common.PropertyCategory;
import com.google.devtools.simple.runtime.annotations.DesignerComponent;
import com.google.devtools.simple.runtime.annotations.SimpleEvent;
import com.google.devtools.simple.runtime.annotations.SimpleFunction;
import com.google.devtools.simple.runtime.annotations.SimpleObject;
import com.google.devtools.simple.runtime.annotations.SimpleProperty;
import com.google.devtools.simple.runtime.components.Component;
import com.google.devtools.simple.runtime.events.EventDispatcher;
import gnu.kawa.xml.ElementType;

@SimpleObject
@DesignerComponent(category = ComponentCategory.MISC, description = "Component for using Voice Recognition to convert from speech to text", iconName = "images/speechRecognizer.png", nonVisible = true, version = 1)
public class SpeechRecognizer extends AndroidNonvisibleComponent implements Component, ActivityResultListener {
    private final ComponentContainer container;
    private int requestCode;
    private String result = ElementType.MATCH_ANY_LOCALNAME;

    public SpeechRecognizer(ComponentContainer container2) {
        super(container2.$form());
        this.container = container2;
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR)
    public String Result() {
        return this.result;
    }

    @SimpleFunction
    public void GetText() {
        BeforeGettingText();
        Intent intent = new Intent("android.speech.action.RECOGNIZE_SPEECH");
        intent.putExtra("android.speech.extra.LANGUAGE_MODEL", "free_form");
        if (this.requestCode == 0) {
            this.requestCode = this.form.registerForActivityResult(this);
        }
        this.container.$context().startActivityForResult(intent, this.requestCode);
    }

    public void resultReturned(int requestCode2, int resultCode, Intent data) {
        Log.i("VoiceRecognizer", "Returning result. Request code = " + requestCode2 + ", result code = " + resultCode);
        if (requestCode2 == this.requestCode && resultCode == -1) {
            if (data.hasExtra("android.speech.extra.RESULTS")) {
                this.result = data.getExtras().getStringArrayList("android.speech.extra.RESULTS").get(0);
            } else {
                this.result = ElementType.MATCH_ANY_LOCALNAME;
            }
            AfterGettingText(this.result);
        }
    }

    @SimpleEvent
    public void BeforeGettingText() {
        EventDispatcher.dispatchEvent(this, "BeforeGettingText", new Object[0]);
    }

    @SimpleEvent
    public void AfterGettingText(String result2) {
        EventDispatcher.dispatchEvent(this, "AfterGettingText", result2);
    }
}
