package com.google.devtools.simple.runtime.parameters;

public final class DoubleReferenceParameter extends ReferenceParameter {
    private double value;

    public DoubleReferenceParameter(double value2) {
        set(value2);
    }

    public double get() {
        return this.value;
    }

    public void set(double value2) {
        this.value = value2;
    }
}
