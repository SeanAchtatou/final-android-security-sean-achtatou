package com.google.devtools.simple.runtime.errors;

import com.google.devtools.simple.runtime.annotations.SimpleObject;

@SimpleObject
public class FileIOError extends RuntimeError {
    public FileIOError(String message) {
        super(message);
    }
}
