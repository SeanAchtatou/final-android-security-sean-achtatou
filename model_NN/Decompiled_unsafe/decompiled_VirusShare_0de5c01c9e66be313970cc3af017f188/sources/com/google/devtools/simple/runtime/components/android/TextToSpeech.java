package com.google.devtools.simple.runtime.components.android;

import android.util.Log;
import com.google.devtools.simple.common.ComponentCategory;
import com.google.devtools.simple.common.PropertyCategory;
import com.google.devtools.simple.runtime.annotations.DesignerComponent;
import com.google.devtools.simple.runtime.annotations.DesignerProperty;
import com.google.devtools.simple.runtime.annotations.SimpleEvent;
import com.google.devtools.simple.runtime.annotations.SimpleFunction;
import com.google.devtools.simple.runtime.annotations.SimpleObject;
import com.google.devtools.simple.runtime.annotations.SimpleProperty;
import com.google.devtools.simple.runtime.components.Component;
import com.google.devtools.simple.runtime.components.android.collect.Maps;
import com.google.devtools.simple.runtime.components.android.util.ExternalTextToSpeech;
import com.google.devtools.simple.runtime.components.android.util.ITextToSpeech;
import com.google.devtools.simple.runtime.components.android.util.InternalTextToSpeech;
import com.google.devtools.simple.runtime.components.android.util.SdkLevel;
import com.google.devtools.simple.runtime.events.EventDispatcher;
import gnu.kawa.xml.ElementType;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;

@SimpleObject
@DesignerComponent(category = ComponentCategory.MISC, description = "Component for using TextToSpeech to speak a message", iconName = "images/textToSpeech.png", nonVisible = true, version = 1)
public class TextToSpeech extends AndroidNonvisibleComponent implements Component, OnStopListener, OnResumeListener {
    private static final String LOG_TAG = "TextToSpeech";
    private static final Map<String, Locale> iso3CountryToLocaleMap = Maps.newHashMap();
    private static final Map<String, Locale> iso3LanguageToLocaleMap = Maps.newHashMap();
    private String country;
    private String iso2Country;
    private String iso2Language;
    private String language;
    /* access modifiers changed from: private */
    public boolean result = false;
    private final ITextToSpeech tts;

    static {
        initLocaleMaps();
    }

    private static void initLocaleMaps() {
        for (Locale locale : Locale.getAvailableLocales()) {
            try {
                String iso3Country = locale.getISO3Country();
                if (iso3Country.length() > 0) {
                    iso3CountryToLocaleMap.put(iso3Country, locale);
                }
            } catch (MissingResourceException e) {
            }
            try {
                String iso3Language = locale.getISO3Language();
                if (iso3Language.length() > 0) {
                    iso3LanguageToLocaleMap.put(iso3Language, locale);
                }
            } catch (MissingResourceException e2) {
            }
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TextToSpeech(ComponentContainer container) {
        super(container.$form());
        boolean useExternalLibrary = false;
        Language(ElementType.MATCH_ANY_LOCALNAME);
        Country(ElementType.MATCH_ANY_LOCALNAME);
        useExternalLibrary = SdkLevel.getLevel() < 4 ? true : useExternalLibrary;
        Log.v(LOG_TAG, "Using " + (useExternalLibrary ? "external" : "internal") + " TTS library.");
        ITextToSpeech.TextToSpeechCallback callback = new ITextToSpeech.TextToSpeechCallback() {
            public void onSuccess() {
                boolean unused = TextToSpeech.this.result = true;
                TextToSpeech.this.AfterSpeaking(true);
            }

            public void onFailure() {
                boolean unused = TextToSpeech.this.result = false;
                TextToSpeech.this.AfterSpeaking(false);
            }
        };
        this.tts = useExternalLibrary ? new ExternalTextToSpeech(container, callback) : new InternalTextToSpeech(container.$context(), callback);
        this.form.registerForOnStop(this);
        this.form.registerForOnResume(this);
        this.form.setVolumeControlStream(3);
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR)
    public boolean Result() {
        return this.result;
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR)
    @DesignerProperty(defaultValue = ElementType.MATCH_ANY_LOCALNAME, editorType = DesignerProperty.PROPERTY_TYPE_STRING)
    public void Language(String language2) {
        Locale locale;
        switch (language2.length()) {
            case 2:
                locale = new Locale(language2);
                this.language = locale.getLanguage();
                break;
            case 3:
                locale = iso3LanguageToLocale(language2);
                this.language = locale.getISO3Language();
                break;
            default:
                locale = Locale.getDefault();
                this.language = locale.getLanguage();
                break;
        }
        this.iso2Language = locale.getLanguage();
    }

    private static Locale iso3LanguageToLocale(String iso3Language) {
        Locale mappedLocale = iso3LanguageToLocaleMap.get(iso3Language);
        if (mappedLocale == null) {
            mappedLocale = iso3LanguageToLocaleMap.get(iso3Language.toLowerCase(Locale.ENGLISH));
        }
        return mappedLocale == null ? Locale.getDefault() : mappedLocale;
    }

    @SimpleProperty
    public String Language() {
        return this.language;
    }

    @SimpleProperty(category = PropertyCategory.BEHAVIOR)
    @DesignerProperty(defaultValue = ElementType.MATCH_ANY_LOCALNAME, editorType = DesignerProperty.PROPERTY_TYPE_STRING)
    public void Country(String country2) {
        Locale locale;
        switch (country2.length()) {
            case 2:
                locale = new Locale(country2);
                this.country = locale.getCountry();
                break;
            case 3:
                locale = iso3CountryToLocale(country2);
                this.country = locale.getISO3Country();
                break;
            default:
                locale = Locale.getDefault();
                this.country = locale.getCountry();
                break;
        }
        this.iso2Country = locale.getCountry();
    }

    private static Locale iso3CountryToLocale(String iso3Country) {
        Locale mappedLocale = iso3CountryToLocaleMap.get(iso3Country);
        if (mappedLocale == null) {
            mappedLocale = iso3CountryToLocaleMap.get(iso3Country.toUpperCase(Locale.ENGLISH));
        }
        return mappedLocale == null ? Locale.getDefault() : mappedLocale;
    }

    @SimpleProperty
    public String Country() {
        return this.country;
    }

    @SimpleFunction
    public void Speak(String message) {
        BeforeSpeaking();
        this.tts.speak(message, new Locale(this.iso2Language, this.iso2Country));
    }

    @SimpleEvent
    public void BeforeSpeaking() {
        EventDispatcher.dispatchEvent(this, "BeforeSpeaking", new Object[0]);
    }

    @SimpleEvent
    public void AfterSpeaking(boolean result2) {
        EventDispatcher.dispatchEvent(this, "AfterSpeaking", Boolean.valueOf(result2));
    }

    public void onStop() {
        this.tts.onStop();
    }

    public void onResume() {
        this.tts.onResume();
    }
}
