package com.google.devtools.simple.runtime;

import com.google.devtools.simple.runtime.annotations.SimpleFunction;
import com.google.devtools.simple.runtime.annotations.SimpleObject;
import com.google.devtools.simple.runtime.variants.Variant;
import gnu.kawa.xml.ElementType;
import java.lang.reflect.Array;
import java.util.HashSet;
import java.util.Set;

@SimpleObject
public final class Arrays {
    private Arrays() {
    }

    @SimpleFunction
    public static String[] Filter(String[] array, String str, boolean include) {
        if (str == null) {
            throw new NullPointerException();
        }
        Set<String> result = new HashSet<>();
        for (String a : array) {
            if (a.contains(str) == include) {
                result.add(a);
            }
        }
        return (String[]) result.toArray(new String[result.size()]);
    }

    @SimpleFunction
    public static String Join(String[] array, String separator) {
        if (separator == null) {
            throw new NullPointerException();
        }
        StringBuilder sb = new StringBuilder();
        String sep = ElementType.MATCH_ANY_LOCALNAME;
        for (String a : array) {
            sb.append(sep).append(a);
            sep = separator;
        }
        return sb.toString();
    }

    @SimpleFunction
    public static String[] Split(String str, String separator, int count) {
        if (separator == null) {
            throw new NullPointerException();
        } else if (count > 0) {
            return str.split("\\Q" + separator + "\\E", count);
        } else {
            throw new IllegalArgumentException("Count for Split() out of range. Should greater than 0.");
        }
    }

    @SimpleFunction
    public static int UBound(Variant array, int dim) {
        if (dim <= 0) {
            throw new IllegalArgumentException("Dimension for UBound() out of range. Should be greater than 0.");
        }
        Object arr = array.getArray();
        while (true) {
            dim--;
            if (dim <= 0) {
                return Array.getLength(arr);
            }
            arr = Array.get(arr, 0);
        }
    }
}
