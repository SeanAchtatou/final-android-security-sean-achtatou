package com.google.android.googlelogin;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class GoogleLoginServiceHelper {
    private static final boolean DEBUG = false;
    private static final boolean LOCAL_LOGV = false;
    private static final String TAG = "GoogleLoginServiceHelper";

    private GoogleLoginServiceHelper() {
    }

    public static void getAccount(final Activity activity, final int requestCode, final boolean requireGoogle) {
        final Handler handler = new Handler();
        new Thread() {
            /* JADX WARNING: Removed duplicated region for block: B:15:0x0040  */
            /* JADX WARNING: Removed duplicated region for block: B:18:0x0047  */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r9 = this;
                    android.os.Bundle r1 = new android.os.Bundle
                    r1.<init>()
                    r5 = -1
                    r3 = 0
                    com.google.android.googlelogin.GoogleLoginServiceBlockingHelper r4 = new com.google.android.googlelogin.GoogleLoginServiceBlockingHelper     // Catch:{ GoogleLoginServiceNotFoundException -> 0x0033 }
                    android.app.Activity r6 = r2     // Catch:{ GoogleLoginServiceNotFoundException -> 0x0033 }
                    android.app.Application r6 = r6.getApplication()     // Catch:{ GoogleLoginServiceNotFoundException -> 0x0033 }
                    r4.<init>(r6)     // Catch:{ GoogleLoginServiceNotFoundException -> 0x0033 }
                    r6 = 1
                    java.lang.String[] r0 = new java.lang.String[r6]     // Catch:{ GoogleLoginServiceNotFoundException -> 0x004e, all -> 0x004b }
                    r6 = 0
                    boolean r7 = r4     // Catch:{ GoogleLoginServiceNotFoundException -> 0x004e, all -> 0x004b }
                    java.lang.String r7 = r4.getAccount(r7)     // Catch:{ GoogleLoginServiceNotFoundException -> 0x004e, all -> 0x004b }
                    r0[r6] = r7     // Catch:{ GoogleLoginServiceNotFoundException -> 0x004e, all -> 0x004b }
                    java.lang.String r6 = "accounts"
                    r1.putStringArray(r6, r0)     // Catch:{ GoogleLoginServiceNotFoundException -> 0x004e, all -> 0x004b }
                    if (r4 == 0) goto L_0x0051
                    r4.close()
                    r3 = r4
                L_0x0029:
                    android.os.Handler r6 = r0
                    android.app.Activity r7 = r2
                    int r8 = r3
                    com.google.android.googlelogin.GoogleLoginServiceHelper.postActivityResult(r6, r7, r5, r8, r1)
                    return
                L_0x0033:
                    r2 = move-exception
                L_0x0034:
                    r5 = 0
                    java.lang.String r6 = "errorCode"
                    int r7 = r2.getErrorCode()     // Catch:{ all -> 0x0044 }
                    r1.putInt(r6, r7)     // Catch:{ all -> 0x0044 }
                    if (r3 == 0) goto L_0x0029
                    r3.close()
                    goto L_0x0029
                L_0x0044:
                    r6 = move-exception
                L_0x0045:
                    if (r3 == 0) goto L_0x004a
                    r3.close()
                L_0x004a:
                    throw r6
                L_0x004b:
                    r6 = move-exception
                    r3 = r4
                    goto L_0x0045
                L_0x004e:
                    r2 = move-exception
                    r3 = r4
                    goto L_0x0034
                L_0x0051:
                    r3 = r4
                    goto L_0x0029
                */
                throw new UnsupportedOperationException("Method not decompiled: com.google.android.googlelogin.GoogleLoginServiceHelper.AnonymousClass1.run():void");
            }
        }.start();
    }

    public static void getCredentials(Activity activity, int requestCode, Bundle requestExtras, boolean requireGoogle, String service, boolean promptUser) {
        final Handler handler = new Handler();
        final Activity activity2 = activity;
        final boolean z = requireGoogle;
        final String str = service;
        final boolean z2 = promptUser;
        final Bundle bundle = requestExtras;
        final int i = requestCode;
        new Thread() {
            /* JADX WARNING: Removed duplicated region for block: B:39:0x00b7  */
            /* JADX WARNING: Removed duplicated region for block: B:42:0x00be  */
            /* JADX WARNING: Removed duplicated region for block: B:50:? A[RETURN, SYNTHETIC] */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r10 = this;
                    r6 = 0
                    r4 = 0
                    android.os.Bundle r1 = new android.os.Bundle
                    r1.<init>()
                    com.google.android.googlelogin.GoogleLoginServiceBlockingHelper r5 = new com.google.android.googlelogin.GoogleLoginServiceBlockingHelper     // Catch:{ GoogleLoginServiceNotFoundException -> 0x00c5 }
                    android.app.Activity r7 = r1     // Catch:{ GoogleLoginServiceNotFoundException -> 0x00c5 }
                    android.app.Application r7 = r7.getApplication()     // Catch:{ GoogleLoginServiceNotFoundException -> 0x00c5 }
                    r5.<init>(r7)     // Catch:{ GoogleLoginServiceNotFoundException -> 0x00c5 }
                    boolean r7 = r2     // Catch:{ GoogleLoginServiceNotFoundException -> 0x00a0, all -> 0x00c2 }
                    java.lang.String r0 = r5.getAccount(r7)     // Catch:{ GoogleLoginServiceNotFoundException -> 0x00a0, all -> 0x00c2 }
                    java.lang.String r7 = r3     // Catch:{ GoogleLoginServiceNotFoundException -> 0x00a0, all -> 0x00c2 }
                    boolean r8 = r4     // Catch:{ GoogleLoginServiceNotFoundException -> 0x00a0, all -> 0x00c2 }
                    if (r8 != 0) goto L_0x001f
                    r6 = 1
                L_0x001f:
                    com.google.android.googleapps.GoogleLoginCredentialsResult r2 = r5.getCredentials(r0, r7, r6)     // Catch:{ GoogleLoginServiceNotFoundException -> 0x00a0, all -> 0x00c2 }
                    java.lang.String r6 = "callerExtras"
                    android.os.Bundle r7 = r5     // Catch:{ GoogleLoginServiceNotFoundException -> 0x00a0, all -> 0x00c2 }
                    r1.putBundle(r6, r7)     // Catch:{ GoogleLoginServiceNotFoundException -> 0x00a0, all -> 0x00c2 }
                    java.lang.String r6 = r2.getCredentialsString()     // Catch:{ GoogleLoginServiceNotFoundException -> 0x00a0, all -> 0x00c2 }
                    if (r6 == 0) goto L_0x006c
                    java.lang.String r6 = "authtoken"
                    java.lang.String r7 = r2.getCredentialsString()     // Catch:{ GoogleLoginServiceNotFoundException -> 0x00a0, all -> 0x00c2 }
                    r1.putString(r6, r7)     // Catch:{ GoogleLoginServiceNotFoundException -> 0x00a0, all -> 0x00c2 }
                    java.lang.String r6 = "authAccount"
                    java.lang.String r7 = r2.getAccount()     // Catch:{ GoogleLoginServiceNotFoundException -> 0x00a0, all -> 0x00c2 }
                    r1.putString(r6, r7)     // Catch:{ GoogleLoginServiceNotFoundException -> 0x00a0, all -> 0x00c2 }
                    java.lang.String r6 = r3     // Catch:{ GoogleLoginServiceNotFoundException -> 0x00a0, all -> 0x00c2 }
                    java.lang.String r7 = "youtube"
                    boolean r6 = r6.equals(r7)     // Catch:{ GoogleLoginServiceNotFoundException -> 0x00a0, all -> 0x00c2 }
                    if (r6 == 0) goto L_0x005b
                    java.lang.String r6 = "YouTubeUser"
                    java.lang.String r7 = r2.getAccount()     // Catch:{ GoogleLoginServiceNotFoundException -> 0x00a0, all -> 0x00c2 }
                    java.lang.String r8 = "YouTubeUser"
                    java.lang.String r7 = r5.peekCredentials(r7, r8)     // Catch:{ GoogleLoginServiceNotFoundException -> 0x00a0, all -> 0x00c2 }
                    r1.putString(r6, r7)     // Catch:{ GoogleLoginServiceNotFoundException -> 0x00a0, all -> 0x00c2 }
                L_0x005b:
                    android.os.Handler r6 = r6     // Catch:{ GoogleLoginServiceNotFoundException -> 0x00a0, all -> 0x00c2 }
                    android.app.Activity r7 = r1     // Catch:{ GoogleLoginServiceNotFoundException -> 0x00a0, all -> 0x00c2 }
                    r8 = -1
                    int r9 = r7     // Catch:{ GoogleLoginServiceNotFoundException -> 0x00a0, all -> 0x00c2 }
                    com.google.android.googlelogin.GoogleLoginServiceHelper.postActivityResult(r6, r7, r8, r9, r1)     // Catch:{ GoogleLoginServiceNotFoundException -> 0x00a0, all -> 0x00c2 }
                    if (r5 == 0) goto L_0x006a
                    r5.close()
                L_0x006a:
                    r4 = r5
                L_0x006b:
                    return
                L_0x006c:
                    android.content.Intent r6 = r2.getCredentialsIntent()     // Catch:{ GoogleLoginServiceNotFoundException -> 0x00a0, all -> 0x00c2 }
                    if (r6 == 0) goto L_0x0098
                    boolean r6 = r4     // Catch:{ GoogleLoginServiceNotFoundException -> 0x00a0, all -> 0x00c2 }
                    if (r6 == 0) goto L_0x0087
                    android.os.Handler r6 = r6     // Catch:{ GoogleLoginServiceNotFoundException -> 0x00a0, all -> 0x00c2 }
                    com.google.android.googlelogin.GoogleLoginServiceHelper$2$1 r7 = new com.google.android.googlelogin.GoogleLoginServiceHelper$2$1     // Catch:{ GoogleLoginServiceNotFoundException -> 0x00a0, all -> 0x00c2 }
                    r7.<init>(r2)     // Catch:{ GoogleLoginServiceNotFoundException -> 0x00a0, all -> 0x00c2 }
                    r6.post(r7)     // Catch:{ GoogleLoginServiceNotFoundException -> 0x00a0, all -> 0x00c2 }
                    if (r5 == 0) goto L_0x0085
                    r5.close()
                L_0x0085:
                    r4 = r5
                    goto L_0x006b
                L_0x0087:
                    android.os.Handler r6 = r6     // Catch:{ GoogleLoginServiceNotFoundException -> 0x00a0, all -> 0x00c2 }
                    android.app.Activity r7 = r1     // Catch:{ GoogleLoginServiceNotFoundException -> 0x00a0, all -> 0x00c2 }
                    r8 = -1
                    int r9 = r7     // Catch:{ GoogleLoginServiceNotFoundException -> 0x00a0, all -> 0x00c2 }
                    com.google.android.googlelogin.GoogleLoginServiceHelper.postActivityResult(r6, r7, r8, r9, r1)     // Catch:{ GoogleLoginServiceNotFoundException -> 0x00a0, all -> 0x00c2 }
                    if (r5 == 0) goto L_0x0096
                    r5.close()
                L_0x0096:
                    r4 = r5
                    goto L_0x006b
                L_0x0098:
                    java.lang.RuntimeException r6 = new java.lang.RuntimeException     // Catch:{ GoogleLoginServiceNotFoundException -> 0x00a0, all -> 0x00c2 }
                    java.lang.String r7 = "Malformed credentialsResult from helper.getCredentials()"
                    r6.<init>(r7)     // Catch:{ GoogleLoginServiceNotFoundException -> 0x00a0, all -> 0x00c2 }
                    throw r6     // Catch:{ GoogleLoginServiceNotFoundException -> 0x00a0, all -> 0x00c2 }
                L_0x00a0:
                    r3 = move-exception
                    r4 = r5
                L_0x00a2:
                    java.lang.String r6 = "errorCode"
                    int r7 = r3.getErrorCode()     // Catch:{ all -> 0x00bb }
                    r1.putInt(r6, r7)     // Catch:{ all -> 0x00bb }
                    android.os.Handler r6 = r6     // Catch:{ all -> 0x00bb }
                    android.app.Activity r7 = r1     // Catch:{ all -> 0x00bb }
                    r8 = 0
                    int r9 = r7     // Catch:{ all -> 0x00bb }
                    com.google.android.googlelogin.GoogleLoginServiceHelper.postActivityResult(r6, r7, r8, r9, r1)     // Catch:{ all -> 0x00bb }
                    if (r4 == 0) goto L_0x006b
                    r4.close()
                    goto L_0x006b
                L_0x00bb:
                    r6 = move-exception
                L_0x00bc:
                    if (r4 == 0) goto L_0x00c1
                    r4.close()
                L_0x00c1:
                    throw r6
                L_0x00c2:
                    r6 = move-exception
                    r4 = r5
                    goto L_0x00bc
                L_0x00c5:
                    r3 = move-exception
                    goto L_0x00a2
                */
                throw new UnsupportedOperationException("Method not decompiled: com.google.android.googlelogin.GoogleLoginServiceHelper.AnonymousClass2.run():void");
            }
        }.start();
    }

    public static void invalidateAuthToken(final Activity activity, final int requestCode, final String authToken) {
        final Handler handler = new Handler();
        new Thread() {
            /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
            /* JADX WARNING: Removed duplicated region for block: B:15:0x0045  */
            /* JADX WARNING: Removed duplicated region for block: B:22:? A[RETURN, SYNTHETIC] */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r9 = this;
                    r2 = 0
                    com.google.android.googlelogin.GoogleLoginServiceBlockingHelper r3 = new com.google.android.googlelogin.GoogleLoginServiceBlockingHelper     // Catch:{ GoogleLoginServiceNotFoundException -> 0x0023 }
                    android.app.Activity r4 = r2     // Catch:{ GoogleLoginServiceNotFoundException -> 0x0023 }
                    android.app.Application r4 = r4.getApplication()     // Catch:{ GoogleLoginServiceNotFoundException -> 0x0023 }
                    r3.<init>(r4)     // Catch:{ GoogleLoginServiceNotFoundException -> 0x0023 }
                    java.lang.String r4 = r4     // Catch:{ GoogleLoginServiceNotFoundException -> 0x004c, all -> 0x0049 }
                    r3.invalidateAuthToken(r4)     // Catch:{ GoogleLoginServiceNotFoundException -> 0x004c, all -> 0x0049 }
                    if (r3 == 0) goto L_0x0016
                    r3.close()
                L_0x0016:
                    android.os.Handler r4 = r0
                    android.app.Activity r5 = r2
                    r6 = -1
                    int r7 = r3
                    r8 = 0
                    com.google.android.googlelogin.GoogleLoginServiceHelper.postActivityResult(r4, r5, r6, r7, r8)
                    r2 = r3
                L_0x0022:
                    return
                L_0x0023:
                    r1 = move-exception
                L_0x0024:
                    android.os.Bundle r0 = new android.os.Bundle     // Catch:{ all -> 0x0042 }
                    r0.<init>()     // Catch:{ all -> 0x0042 }
                    java.lang.String r4 = "errorCode"
                    int r5 = r1.getErrorCode()     // Catch:{ all -> 0x0042 }
                    r0.putInt(r4, r5)     // Catch:{ all -> 0x0042 }
                    android.os.Handler r4 = r0     // Catch:{ all -> 0x0042 }
                    android.app.Activity r5 = r2     // Catch:{ all -> 0x0042 }
                    r6 = 0
                    int r7 = r3     // Catch:{ all -> 0x0042 }
                    com.google.android.googlelogin.GoogleLoginServiceHelper.postActivityResult(r4, r5, r6, r7, r0)     // Catch:{ all -> 0x0042 }
                    if (r2 == 0) goto L_0x0022
                    r2.close()
                    goto L_0x0022
                L_0x0042:
                    r4 = move-exception
                L_0x0043:
                    if (r2 == 0) goto L_0x0048
                    r2.close()
                L_0x0048:
                    throw r4
                L_0x0049:
                    r4 = move-exception
                    r2 = r3
                    goto L_0x0043
                L_0x004c:
                    r1 = move-exception
                    r2 = r3
                    goto L_0x0024
                */
                throw new UnsupportedOperationException("Method not decompiled: com.google.android.googlelogin.GoogleLoginServiceHelper.AnonymousClass3.run():void");
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public static void sendActivityResult(Activity activity, int resultCode, int requestCode, Bundle bundle) {
        PendingIntent apr = activity.createPendingResult(requestCode, null, 0);
        if (apr != null) {
            try {
                Intent intent = new Intent();
                if (bundle != null) {
                    intent.putExtras(bundle);
                }
                apr.send(activity, resultCode, intent);
            } catch (PendingIntent.CanceledException e) {
            }
        }
    }

    /* access modifiers changed from: private */
    public static void postActivityResult(Handler handler, final Activity activity, final int resultCode, final int requestCode, final Bundle bundle) {
        handler.post(new Runnable() {
            public void run() {
                GoogleLoginServiceHelper.sendActivityResult(activity, resultCode, requestCode, bundle);
            }
        });
    }
}
