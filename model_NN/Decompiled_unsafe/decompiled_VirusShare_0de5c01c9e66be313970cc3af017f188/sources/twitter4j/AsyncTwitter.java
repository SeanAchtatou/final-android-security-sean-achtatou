package twitter4j;

import java.util.Date;
import twitter4j.Twitter;

public class AsyncTwitter extends Twitter {
    public static final int BLOCK = 22;
    public static final int CREATE = 12;
    public static final int CREATED_BLOCK = 43;
    public static final int CREATE_FAVORITE = 18;
    public static final int CREATE_FRIENDSHIP = 32;
    public static final int CURRENT_TRENDS = 45;
    public static final int DAILY_TRENDS = 46;
    public static final int DESTORY = 13;
    public static final int DESTROY = 13;
    public static final int DESTROYED_BLOCK = 42;
    public static final int DESTROY_DIRECT_MESSAGES = 40;
    public static final int DESTROY_FAVORITE = 19;
    public static final int DESTROY_FRIENDSHIP = 33;
    public static final int DESTROY_STATUS = 26;
    public static final int DIRECT_MESSAGES = 10;
    public static final int DISABLE_NOTIFICATION = 36;
    public static final int ENABLE_NOTIFICATION = 35;
    public static final int EXISTS = 28;
    private static final int EXISTS_BLOCK = 48;
    public static final int EXISTS_FRIENDSHIP = 34;
    public static final int FAVORITES = 17;
    public static final int FEATURED = 8;
    public static final int FOLLOW = 14;
    public static final int FOLLOWERS = 7;
    public static final int FOLLOWERS_IDS = 30;
    public static final int FRIENDS = 6;
    public static final int FRIENDS_IDS = 29;
    public static final int FRIENDS_TIMELINE = 1;
    private static final int GET_BLOCKING_USERS = 49;
    private static final int GET_BLOCKING_USERS_IDS = 50;
    public static final int GET_DOWNTIME_SCHEDULE = 25;
    public static final int HOME_TIMELINE = 51;
    public static final int LEAVE = 15;
    public static final int MENTIONS = 37;
    public static final int PUBLIC_TIMELINE = 0;
    public static final int RATE_LIMIT_STATUS = 28;
    public static final int REPLIES = 5;
    public static final int RETWEETED_BY_ME = 53;
    public static final int RETWEETED_TO_ME = 54;
    public static final int RETWEETS_OF_ME = 55;
    public static final int RETWEET_STATUS = 52;
    public static final int SEARCH = 27;
    public static final int SEND_DIRECT_MESSAGE = 11;
    public static final int SHOW = 3;
    public static final int SHOW_STATUS = 38;
    public static final int TEST = 24;
    public static final int TRENDS = 44;
    public static final int UNBLOCK = 23;
    public static final int UPDATE = 4;
    public static final int UPDATE_DELIVERLY_DEVICE = 21;
    public static final int UPDATE_LOCATION = 20;
    public static final int UPDATE_PROFILE = 41;
    public static final int UPDATE_PROFILE_COLORS = 31;
    public static final int UPDATE_STATUS = 39;
    public static final int USER_DETAIL = 9;
    public static final int USER_TIMELINE = 2;
    public static final int WEEKLY_TRENDS = 47;
    static Class class$twitter4j$AsyncTwitter = null;
    private static transient Dispatcher dispatcher = null;
    private static final long serialVersionUID = -2008667933225051907L;
    private boolean shutdown = false;

    public AsyncTwitter(String id, String password) {
        super(id, password);
    }

    public AsyncTwitter(String id, String password, String baseURL) {
        super(id, password, baseURL);
    }

    public void searchAcync(Query query, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 27, listener, new Object[]{query}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.searched(this.this$0.search((Query) args[0]));
            }
        });
    }

    public void getTrendsAsync(TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 44, listener, null) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotTrends(this.this$0.getTrends());
            }
        });
    }

    public void getCurrentTrendsAsync(TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 45, listener, null) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotCurrentTrends(this.this$0.getCurrentTrends());
            }
        });
    }

    public void getCurrentTrendsAsync(boolean excludeHashTags, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 45, listener, new Object[]{new Boolean(excludeHashTags)}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotCurrentTrends(this.this$0.getCurrentTrends(((Boolean) args[0]).booleanValue()));
            }
        });
    }

    public void getDailyTrendsAsync(TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 46, listener, null) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotDailyTrends(this.this$0.getDailyTrends());
            }
        });
    }

    public void getDailyTrendsAsync(Date date, boolean excludeHashTags, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 46, listener, new Object[]{date, new Boolean(excludeHashTags)}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotDailyTrends(this.this$0.getDailyTrends((Date) args[0], ((Boolean) args[1]).booleanValue()));
            }
        });
    }

    public void getWeeklyTrendsAsync(TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 47, listener, null) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotWeeklyTrends(this.this$0.getWeeklyTrends());
            }
        });
    }

    public void getWeeklyTrendsAsync(Date date, boolean excludeHashTags, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 47, listener, new Object[]{date, new Boolean(excludeHashTags)}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotWeeklyTrends(this.this$0.getWeeklyTrends((Date) args[0], ((Boolean) args[1]).booleanValue()));
            }
        });
    }

    public void getPublicTimelineAsync(TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 0, listener, null) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotPublicTimeline(this.this$0.getPublicTimeline());
            }
        });
    }

    public void getPublicTimelineAsync(int sinceID, TwitterListener listener) {
        getPublicTimelineAsync((long) sinceID, listener);
    }

    public void getPublicTimelineAsync(long sinceID, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 0, listener, new Long[]{new Long(sinceID)}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotPublicTimeline(this.this$0.getPublicTimeline(((Long) args[0]).longValue()));
            }
        });
    }

    public void getHomeTimelineAsync(TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 51, listener, null) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotHomeTimeline(this.this$0.getHomeTimeline());
            }
        });
    }

    public void getHomeTimelineAsync(Paging paging, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 51, listener, new Object[]{paging}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotHomeTimeline(this.this$0.getHomeTimeline((Paging) args[0]));
            }
        });
    }

    public void getFriendsTimelineAsync(TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 1, listener, null) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotFriendsTimeline(this.this$0.getFriendsTimeline());
            }
        });
    }

    public void getFriendsTimelineAsync(Paging paging, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 1, listener, new Object[]{paging}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotFriendsTimeline(this.this$0.getFriendsTimeline((Paging) args[0]));
            }
        });
    }

    public void getFriendsTimelineByPageAsync(int page, TwitterListener listener) {
        getFriendsTimelineAsync(new Paging(page), listener);
    }

    public void getFriendsTimelineAsync(int page, TwitterListener listener) {
        getFriendsTimelineAsync(new Paging(page), listener);
    }

    public void getFriendsTimelineAsync(long sinceId, int page, TwitterListener listener) {
        getFriendsTimelineAsync(new Paging(page, sinceId), listener);
    }

    public void getFriendsTimelineAsync(String id, TwitterListener listener) {
        throw new IllegalStateException("The Twitter API is not supporting this method anymore");
    }

    public void getFriendsTimelineAsync(String id, Paging paging, TwitterListener listener) {
        throw new IllegalStateException("The Twitter API is not supporting this method anymore");
    }

    public void getFriendsTimelineByPageAsync(String id, int page, TwitterListener listener) {
        throw new IllegalStateException("The Twitter API is not supporting this method anymore");
    }

    public void getFriendsTimelineAsync(String id, int page, TwitterListener listener) {
        throw new IllegalStateException("The Twitter API is not supporting this method anymore");
    }

    public void getFriendsTimelineAsync(long sinceId, String id, int page, TwitterListener listener) {
        throw new IllegalStateException("The Twitter API is not supporting this method anymore");
    }

    public void getFriendsTimelineAsync(Date since, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 1, listener, new Object[]{since}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotFriendsTimeline(this.this$0.getFriendsTimeline((Date) args[0]));
            }
        });
    }

    public void getFriendsTimelineAsync(long sinceId, TwitterListener listener) {
        getFriendsTimelineAsync(new Paging(sinceId), listener);
    }

    public void getFriendsTimelineAsync(String id, Date since, TwitterListener listener) {
        throw new IllegalStateException("The Twitter API is not supporting this method anymore");
    }

    public void getFriendsTimelineAsync(String id, long sinceId, TwitterListener listener) {
        throw new IllegalStateException("The Twitter API is not supporting this method anymore");
    }

    public void getUserTimelineAsync(String id, int count, Date since, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 2, listener, new Object[]{id, new Integer(count), since}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotUserTimeline(this.this$0.getUserTimeline((String) args[0], ((Integer) args[1]).intValue(), (Date) args[2]));
            }
        });
    }

    public void getUserTimelineAsync(String id, Paging paging, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 2, listener, new Object[]{id, paging}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotUserTimeline(this.this$0.getUserTimeline((String) args[0], (Paging) args[1]));
            }
        });
    }

    public void getUserTimelineAsync(String id, int page, long sinceId, TwitterListener listener) {
        getUserTimelineAsync(id, new Paging(page, sinceId), listener);
    }

    public void getUserTimelineAsync(String id, Date since, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 2, listener, new Object[]{id, since}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotUserTimeline(this.this$0.getUserTimeline((String) args[0], (Date) args[1]));
            }
        });
    }

    public void getUserTimelineAsync(String id, int count, TwitterListener listener) {
        getUserTimelineAsync(id, new Paging().count(count), listener);
    }

    public void getUserTimelineAsync(int count, Date since, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 2, listener, new Object[]{new Integer(count), since}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotUserTimeline(this.this$0.getUserTimeline(((Integer) args[0]).intValue(), (Date) args[1]));
            }
        });
    }

    public void getUserTimelineAsync(Paging paging, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 2, listener, new Object[]{paging}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotUserTimeline(this.this$0.getUserTimeline((Paging) args[0]));
            }
        });
    }

    public void getUserTimelineAsync(int count, long sinceId, TwitterListener listener) {
        getUserTimelineAsync(new Paging(sinceId).count(count), listener);
    }

    public void getUserTimelineAsync(String id, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 2, listener, new Object[]{id}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotUserTimeline(this.this$0.getUserTimeline((String) args[0]));
            }
        });
    }

    public void getUserTimelineAsync(String id, long sinceId, TwitterListener listener) {
        getUserTimelineAsync(id, new Paging(sinceId), listener);
    }

    public void getUserTimelineAsync(TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 2, listener, null) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotUserTimeline(this.this$0.getUserTimeline());
            }
        });
    }

    public void getUserTimelineAsync(long sinceId, TwitterListener listener) {
        getUserTimelineAsync(new Paging(sinceId), listener);
    }

    public void getRepliesAsync(TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 5, listener, null) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotReplies(this.this$0.getReplies());
            }
        });
    }

    public void getMentionsAsync(TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 37, listener, null) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotMentions(this.this$0.getMentions());
            }
        });
    }

    public void getMentionsAsync(Paging paging, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 37, listener, new Object[]{paging}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotMentions(this.this$0.getMentions((Paging) args[0]));
            }
        });
    }

    public void getRetweetedByMeAsync(TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 53, listener, null) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotRetweetedByMe(this.this$0.getRetweetedByMe());
            }
        });
    }

    public void getRetweetedByMeAsync(Paging paging, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 53, listener, new Object[]{paging}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotRetweetedByMe(this.this$0.getRetweetedByMe((Paging) args[0]));
            }
        });
    }

    public void getRetweetedToMeAsync(TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 54, listener, null) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotRetweetedToMe(this.this$0.getRetweetedToMe());
            }
        });
    }

    public void getRetweetedToMeAsync(Paging paging, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 54, listener, new Object[]{paging}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotRetweetedToMe(this.this$0.getRetweetedToMe((Paging) args[0]));
            }
        });
    }

    public void getRetweetsOfMeAsync(TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 55, listener, null) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotRetweetsOfMe(this.this$0.getRetweetsOfMe());
            }
        });
    }

    public void getRetweetsOfMeAsync(Paging paging, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 55, listener, new Object[]{paging}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotRetweetsOfMe(this.this$0.getRetweetsOfMe((Paging) args[0]));
            }
        });
    }

    public void getRepliesAsync(long sinceId, TwitterListener listener) {
        getMentionsAsync(new Paging(sinceId), listener);
    }

    public void getRepliesByPageAsync(int page, TwitterListener listener) {
        getMentionsAsync(new Paging(page), listener);
    }

    public void getRepliesAsync(int page, TwitterListener listener) {
        getMentionsAsync(new Paging(page), listener);
    }

    public void getRepliesAsync(long sinceId, int page, TwitterListener listener) {
        getMentionsAsync(new Paging(page, sinceId), listener);
    }

    public void showAsync(int id, TwitterListener listener) {
        showAsync((long) id, listener);
    }

    public void showAsync(long id, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 3, listener, new Object[]{new Long(id)}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotShow(this.this$0.show(((Long) args[0]).longValue()));
            }
        });
    }

    public void showStatusAsync(long id, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 38, listener, new Object[]{new Long(id)}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotShowStatus(this.this$0.showStatus(((Long) args[0]).longValue()));
            }
        });
    }

    public void updateAsync(String status, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 4, listener, new String[]{status}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.updated(this.this$0.update((String) args[0]));
            }
        });
    }

    public void updateAsync(String status) {
        getDispatcher().invokeLater(new AsyncTask(this, 4, new TwitterAdapter(), new String[]{status}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.updated(this.this$0.update((String) args[0]));
            }
        });
    }

    public void updateStatusAsync(String status, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 39, listener, new String[]{status}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.updatedStatus(this.this$0.updateStatus((String) args[0]));
            }
        });
    }

    public void updateStatusAsync(String status) {
        getDispatcher().invokeLater(new AsyncTask(this, 39, new TwitterAdapter(), new String[]{status}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.updatedStatus(this.this$0.updateStatus((String) args[0]));
            }
        });
    }

    public void updateAsync(String status, long inReplyToStatusId, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 4, listener, new Object[]{status, new Long(inReplyToStatusId)}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.updated(this.this$0.update((String) args[0], ((Long) args[1]).longValue()));
            }
        });
    }

    public void updateAsync(String status, long inReplyToStatusId) {
        getDispatcher().invokeLater(new AsyncTask(this, 4, new TwitterAdapter(), new Object[]{status, new Long(inReplyToStatusId)}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.updated(this.this$0.update((String) args[0], ((Long) args[1]).longValue()));
            }
        });
    }

    public void updateStatusAsync(String status, long inReplyToStatusId, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 39, listener, new Object[]{status, new Long(inReplyToStatusId)}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.updatedStatus(this.this$0.updateStatus((String) args[0], ((Long) args[1]).longValue()));
            }
        });
    }

    public void updateStatusAsync(String status, long inReplyToStatusId) {
        getDispatcher().invokeLater(new AsyncTask(this, 39, new TwitterAdapter(), new Object[]{status, new Long(inReplyToStatusId)}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.updatedStatus(this.this$0.updateStatus((String) args[0], ((Long) args[1]).longValue()));
            }
        });
    }

    public void destoryStatusAsync(int statusId) {
        destroyStatusAsync((long) statusId);
    }

    public void destroyStatusAsync(int statusId) {
        destroyStatusAsync((long) statusId);
    }

    public void destroyStatusAsync(long statusId) {
        getDispatcher().invokeLater(new AsyncTask(this, 26, new TwitterAdapter(), new Long[]{new Long(statusId)}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.destroyedStatus(this.this$0.destroyStatus(((Long) args[0]).longValue()));
            }
        });
    }

    public void destoryStatusAsync(int statusId, TwitterListener listener) {
        destroyStatusAsync((long) statusId, listener);
    }

    public void destroyStatusAsync(int statusId, TwitterListener listener) {
        destroyStatusAsync((long) statusId, listener);
    }

    public void destroyStatusAsync(long statusId, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 26, listener, new Long[]{new Long(statusId)}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.destroyedStatus(this.this$0.destroyStatus(((Long) args[0]).longValue()));
            }
        });
    }

    public void retweetStatusAsync(long statusId, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 52, listener, new Long[]{new Long(statusId)}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.retweetedStatus(this.this$0.retweetStatus(((Long) args[0]).longValue()));
            }
        });
    }

    public void retweetStatusAsync(long statusId) {
        retweetStatusAsync(statusId, new TwitterAdapter());
    }

    public void getUserDetailAsync(String id, TwitterListener listener) {
        showUserAsync(id, listener);
    }

    public void showUserAsync(String id, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 9, listener, new Object[]{id}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotUserDetail(this.this$0.showUser((String) args[0]));
            }
        });
    }

    public void getFriendsAsync(TwitterListener listener) {
        getFriendsStatusesAsync(listener);
    }

    public void getFriendsStatusesAsync(TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 6, listener, null) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotFriends(this.this$0.getFriendsStatuses());
            }
        });
    }

    public void getFriendsAsync(Paging paging, TwitterListener listener) {
        getFriendsStatusesAsync(paging, listener);
    }

    public void getFriendsStatusesAsync(Paging paging, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 6, listener, new Object[]{paging}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotFriends(this.this$0.getFriendsStatuses((Paging) args[0]));
            }
        });
    }

    public void getFriendsAsync(int page, TwitterListener listener) {
        getFriendsStatusesAsync(new Paging(page), listener);
    }

    public void getFriendsAsync(String id, TwitterListener listener) {
        getFriendsStatusesAsync(id, listener);
    }

    public void getFriendsStatusesAsync(String id, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 6, listener, new Object[]{id}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotFriends(this.this$0.getFriendsStatuses((String) args[0]));
            }
        });
    }

    public void getFriendsAsync(String id, Paging paging, TwitterListener listener) {
        getFriendsStatusesAsync(id, paging, listener);
    }

    public void getFriendsStatusesAsync(String id, Paging paging, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 6, listener, new Object[]{id, paging}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotFriends(this.this$0.getFriendsStatuses((String) args[0], (Paging) args[1]));
            }
        });
    }

    public void getFriendsAsync(String id, int page, TwitterListener listener) {
        getFriendsStatusesAsync(id, new Paging(page), listener);
    }

    public void getFollowersAsync(TwitterListener listener) {
        getFollowersStatusesAsync(listener);
    }

    public void getFollowersStatusesAsync(TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 7, listener, null) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotFollowers(this.this$0.getFollowers());
            }
        });
    }

    public void getFollowersAsync(Paging paging, TwitterListener listener) {
        getFollowersStatusesAsync(paging, listener);
    }

    public void getFollowersStatusesAsync(Paging paging, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 7, listener, new Object[]{paging}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotFollowers(this.this$0.getFollowersStatuses((Paging) args[0]));
            }
        });
    }

    public void getFollowersAsync(int page, TwitterListener listener) {
        getFollowersStatusesAsync(new Paging(page), listener);
    }

    public void getFollowersAsync(String id, TwitterListener listener) {
        getFollowersStatusesAsync(id, listener);
    }

    public void getFollowersStatusesAsync(String id, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 7, listener, new Object[]{id}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotFollowers(this.this$0.getFollowersStatuses((String) args[0]));
            }
        });
    }

    public void getFollowersAsync(String id, Paging paging, TwitterListener listener) {
        getFollowersStatusesAsync(id, paging, listener);
    }

    public void getFollowersStatusesAsync(String id, Paging paging, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 7, listener, new Object[]{id, paging}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotFollowers(this.this$0.getFollowersStatuses((String) args[0], (Paging) args[1]));
            }
        });
    }

    public void getFollowersAsync(String id, int page, TwitterListener listener) {
        getFollowersStatusesAsync(id, new Paging(page), listener);
    }

    public void getFeaturedAsync(TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 8, listener, null) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotFeatured(this.this$0.getFeatured());
            }
        });
    }

    public void getDirectMessagesAsync(TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 10, listener, null) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotDirectMessages(this.this$0.getDirectMessages());
            }
        });
    }

    public void getDirectMessagesAsync(Paging paging, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 10, listener, new Object[]{paging}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotDirectMessages(this.this$0.getDirectMessages((Paging) args[0]));
            }
        });
    }

    public void getDirectMessagesByPageAsync(int page, TwitterListener listener) {
        getDirectMessagesAsync(new Paging(page), listener);
    }

    public void getDirectMessagesByPageAsync(int page, int sinceId, TwitterListener listener) {
        getDirectMessagesAsync(new Paging(page, (long) sinceId), listener);
    }

    public void getDirectMessagesAsync(int sinceId, TwitterListener listener) {
        getDirectMessagesAsync(new Paging((long) sinceId), listener);
    }

    public void getDirectMessagesAsync(Date since, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 10, listener, new Object[]{since}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotDirectMessages(this.this$0.getDirectMessages((Date) args[0]));
            }
        });
    }

    public void getSentDirectMessagesAsync(TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 10, listener, null) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotSentDirectMessages(this.this$0.getSentDirectMessages());
            }
        });
    }

    public void getSentDirectMessagesAsync(Paging paging, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 10, listener, new Object[]{paging}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotSentDirectMessages(this.this$0.getSentDirectMessages((Paging) args[0]));
            }
        });
    }

    public void getSentDirectMessagesAsync(Date since, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 10, listener, new Object[]{since}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotSentDirectMessages(this.this$0.getSentDirectMessages((Date) args[0]));
            }
        });
    }

    public void getSentDirectMessagesAsync(int sinceId, TwitterListener listener) {
        getSentDirectMessagesAsync(new Paging((long) sinceId), listener);
    }

    public void getSentDirectMessagesAsync(int page, int sinceId, TwitterListener listener) {
        getSentDirectMessagesAsync(new Paging(page, (long) sinceId), listener);
    }

    public void sendDirectMessageAsync(String id, String text, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 11, listener, new String[]{id, text}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.sentDirectMessage(this.this$0.sendDirectMessage((String) args[0], (String) args[1]));
            }
        });
    }

    public void sendDirectMessageAsync(String id, String text) {
        getDispatcher().invokeLater(new AsyncTask(this, 11, new TwitterAdapter(), new String[]{id, text}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.sentDirectMessage(this.this$0.sendDirectMessage((String) args[0], (String) args[1]));
            }
        });
    }

    public void deleteDirectMessageAsync(int id, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 40, listener, new Object[]{new Integer(id)}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.deletedDirectMessage(this.this$0.deleteDirectMessage(((Integer) args[0]).intValue()));
            }
        });
    }

    public void destroyDirectMessageAsync(int id, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 40, listener, new Object[]{new Integer(id)}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.destroyedDirectMessage(this.this$0.destroyDirectMessage(((Integer) args[0]).intValue()));
            }
        });
    }

    public void destroyDirectMessageAsync(int id) {
        getDispatcher().invokeLater(new AsyncTask(this, 40, new TwitterAdapter(), new Object[]{new Integer(id)}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.destroyedDirectMessage(this.this$0.destroyDirectMessage(((Integer) args[0]).intValue()));
            }
        });
    }

    public void createAsync(String id, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 12, listener, new String[]{id}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.created(this.this$0.create((String) args[0]));
            }
        });
    }

    public void createFriendshipAsync(String id, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 32, listener, new String[]{id}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.createdFriendship(this.this$0.createFriendship((String) args[0]));
            }
        });
    }

    public void createFriendshipAsync(String id, boolean follow, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 32, listener, new Object[]{id, new Boolean(follow)}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.createdFriendship(this.this$0.createFriendship((String) args[0], ((Boolean) args[1]).booleanValue()));
            }
        });
    }

    public void createAsync(String id) {
        getDispatcher().invokeLater(new AsyncTask(this, 12, new TwitterAdapter(), new String[]{id}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.created(this.this$0.create((String) args[0]));
            }
        });
    }

    public void createFriendshipAsync(String id) {
        createFriendshipAsync(id, new TwitterAdapter());
    }

    public void destroyAsync(String id, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 13, listener, new String[]{id}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.destroyed(this.this$0.destroy((String) args[0]));
            }
        });
    }

    public void destroyAsync(String id) {
        getDispatcher().invokeLater(new AsyncTask(this, 13, new TwitterAdapter(), new String[]{id}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.destroyed(this.this$0.destroy((String) args[0]));
            }
        });
    }

    public void destroyFriendshipAsync(String id, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 33, listener, new String[]{id}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.destroyedFriendship(this.this$0.destroyFriendship((String) args[0]));
            }
        });
    }

    public void destroyFriendshipAsync(String id) {
        destroyFriendshipAsync(id, new TwitterAdapter());
    }

    public void existsAsync(String userA, String userB, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 28, listener, new String[]{userA, userB}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotExists(this.this$0.exists((String) args[0], (String) args[1]));
            }
        });
    }

    public void existsFriendshipAsync(String userA, String userB, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 34, listener, new String[]{userA, userB}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotExistsFriendship(this.this$0.existsFriendship((String) args[0], (String) args[1]));
            }
        });
    }

    public void getFriendsIDsAsync(TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 29, listener, null) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotFriendsIDs(this.this$0.getFriendsIDs());
            }
        });
    }

    public void getFriendsIDsAsync(Paging paging, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 29, listener, new Object[]{paging}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotFriendsIDs(this.this$0.getFriendsIDs((Paging) args[0]));
            }
        });
    }

    public void getFriendsIDsAsync(long cursor, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 29, listener, new Object[]{new Long(cursor)}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotFriendsIDs(this.this$0.getFriendsIDs(((Long) args[0]).longValue()));
            }
        });
    }

    public void getFriendsIDsAsync(int userId, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 29, listener, new Integer[]{new Integer(userId)}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotFriendsIDs(this.this$0.getFriendsIDs(((Integer) args[0]).intValue()));
            }
        });
    }

    public void getFriendsIDsAsync(int userId, Paging paging, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 29, listener, new Object[]{new Integer(userId), paging}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotFriendsIDs(this.this$0.getFriendsIDs(((Integer) args[0]).intValue(), (Paging) args[1]));
            }
        });
    }

    public void getFriendsIDsAsync(int userId, long cursor, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 29, listener, new Object[]{new Integer(userId), new Long(cursor)}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotFriendsIDs(this.this$0.getFriendsIDs(((Integer) args[0]).intValue(), ((Long) args[1]).longValue()));
            }
        });
    }

    public void getFriendsIDsAsync(String screenName, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 29, listener, new String[]{screenName}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotFriendsIDs(this.this$0.getFriendsIDs((String) args[0]));
            }
        });
    }

    public void getFriendsIDsAsync(String screenName, Paging paging, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 29, listener, new Object[]{screenName, paging}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotFriendsIDs(this.this$0.getFriendsIDs((String) args[0], (Paging) args[1]));
            }
        });
    }

    public void getFriendsIDsAsync(String screenName, long cursor, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 29, listener, new Object[]{screenName, new Long(cursor)}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotFriendsIDs(this.this$0.getFriendsIDs((String) args[0], ((Long) args[1]).longValue()));
            }
        });
    }

    public void getFollowersIDsAsync(TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 30, listener, null) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotFollowersIDs(this.this$0.getFollowersIDs());
            }
        });
    }

    public void getFollowersIDsAsync(Paging paging, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 30, listener, new Object[]{paging}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotFollowersIDs(this.this$0.getFollowersIDs((Paging) args[0]));
            }
        });
    }

    public void getFollowersIDsAsync(long cursor, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 30, listener, new Object[]{new Long(cursor)}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotFollowersIDs(this.this$0.getFollowersIDs(((Long) args[0]).longValue()));
            }
        });
    }

    public void getFollowersIDsAsync(int userId, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 30, listener, new Integer[]{new Integer(userId)}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotFollowersIDs(this.this$0.getFollowersIDs(((Integer) args[0]).intValue()));
            }
        });
    }

    public void getFollowersIDsAsync(int userId, Paging paging, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 30, listener, new Object[]{new Integer(userId), paging}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotFollowersIDs(this.this$0.getFollowersIDs(((Integer) args[0]).intValue(), (Paging) args[1]));
            }
        });
    }

    public void getFollowersIDsAsync(int userId, long cursor, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 30, listener, new Object[]{new Integer(userId), new Long(cursor)}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotFollowersIDs(this.this$0.getFollowersIDs(((Integer) args[0]).intValue(), ((Long) args[1]).longValue()));
            }
        });
    }

    public void getFollowersIDsAsync(String screenName, TwitterListener listener) throws TwitterException {
        getDispatcher().invokeLater(new AsyncTask(this, 30, listener, new String[]{screenName}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotFollowersIDs(this.this$0.getFollowersIDs((String) args[0]));
            }
        });
    }

    public void getFollowersIDsAsync(String screenName, Paging paging, TwitterListener listener) throws TwitterException {
        getDispatcher().invokeLater(new AsyncTask(this, 30, listener, new Object[]{screenName, paging}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotFollowersIDs(this.this$0.getFollowersIDs((String) args[0], (Paging) args[1]));
            }
        });
    }

    public void getFollowersIDsAsync(String screenName, long cursor, TwitterListener listener) throws TwitterException {
        getDispatcher().invokeLater(new AsyncTask(this, 30, listener, new Object[]{screenName, new Long(cursor)}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotFollowersIDs(this.this$0.getFollowersIDs((String) args[0], ((Long) args[1]).longValue()));
            }
        });
    }

    public void updateLocationAsync(String location, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 20, listener, new Object[]{location}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.updatedLocation(this.this$0.updateLocation((String) args[0]));
            }
        });
    }

    public void updateProfileAsync(String name, String email, String url, String location, String description, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 41, listener, new String[]{name, email, url, location, description}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.updatedProfile(this.this$0.updateProfile((String) args[0], (String) args[1], (String) args[2], (String) args[3], (String) args[4]));
            }
        });
    }

    public void updateProfileAsync(String name, String email, String url, String location, String description) {
        updateProfileAsync(name, email, url, location, description, new TwitterAdapter());
    }

    public void rateLimitStatusAsync(TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 28, listener, new Object[0]) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotRateLimitStatus(this.this$0.rateLimitStatus());
            }
        });
    }

    public void updateDeliverlyDeviceAsync(Twitter.Device device, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 20, listener, new Object[]{device}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.updatedDeliverlyDevice(this.this$0.updateDeliverlyDevice((Twitter.Device) args[0]));
            }
        });
    }

    public void updateProfileColorsAsync(String profileBackgroundColor, String profileTextColor, String profileLinkColor, String profileSidebarFillColor, String profileSidebarBorderColor, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 31, listener, new Object[]{profileBackgroundColor, profileTextColor, profileLinkColor, profileSidebarFillColor, profileSidebarBorderColor}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.updatedProfileColors(this.this$0.updateProfileColors((String) args[0], (String) args[1], (String) args[2], (String) args[3], (String) args[4]));
            }
        });
    }

    public void updateProfileColorsAsync(String profileBackgroundColor, String profileTextColor, String profileLinkColor, String profileSidebarFillColor, String profileSidebarBorderColor) {
        updateProfileColorsAsync(profileBackgroundColor, profileTextColor, profileLinkColor, profileSidebarFillColor, profileSidebarBorderColor, new TwitterAdapter());
    }

    public void favoritesAsync(TwitterListener listener) {
        getFavoritesAsync(listener);
    }

    public void getFavoritesAsync(TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 17, listener, null) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotFavorites(this.this$0.getFavorites());
            }
        });
    }

    public void favoritesAsync(int page, TwitterListener listener) {
        getFavoritesAsync(page, listener);
    }

    public void getFavoritesAsync(int page, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 17, listener, new Object[]{new Integer(page)}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotFavorites(this.this$0.getFavorites(((Integer) args[0]).intValue()));
            }
        });
    }

    public void favoritesAsync(String id, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 17, listener, new Object[]{id}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotFavorites(this.this$0.favorites((String) args[0]));
            }
        });
    }

    public void getFavoritesAsync(String id, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 17, listener, new Object[]{id}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotFavorites(this.this$0.getFavorites((String) args[0]));
            }
        });
    }

    public void favoritesAsync(String id, int page, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 17, listener, new Object[]{id, new Integer(page)}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotFavorites(this.this$0.favorites((String) args[0], ((Integer) args[1]).intValue()));
            }
        });
    }

    public void getFavoritesAsync(String id, int page, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 17, listener, new Object[]{id, new Integer(page)}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotFavorites(this.this$0.getFavorites((String) args[0], ((Integer) args[1]).intValue()));
            }
        });
    }

    public void createFavoriteAsync(int id, TwitterListener listener) {
        createFavoriteAsync((long) id, listener);
    }

    public void createFavoriteAsync(long id, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 17, listener, new Object[]{new Long(id)}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.createdFavorite(this.this$0.createFavorite(((Long) args[0]).longValue()));
            }
        });
    }

    public void createFavoriteAsync(int id) {
        createFavoriteAsync((long) id);
    }

    public void createFavoriteAsync(long id) {
        getDispatcher().invokeLater(new AsyncTask(this, 17, new TwitterAdapter(), new Object[]{new Long(id)}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.createdFavorite(this.this$0.createFavorite(((Long) args[0]).longValue()));
            }
        });
    }

    public void destroyFavoriteAsync(int id, TwitterListener listener) {
        destroyFavoriteAsync((long) id, listener);
    }

    public void destroyFavoriteAsync(long id, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 17, listener, new Object[]{new Long(id)}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.destroyedFavorite(this.this$0.destroyFavorite(((Long) args[0]).longValue()));
            }
        });
    }

    public void destroyFavoriteAsync(int id) {
        destroyFavoriteAsync((long) id);
    }

    public void destroyFavoriteAsync(long id) {
        getDispatcher().invokeLater(new AsyncTask(this, 17, new TwitterAdapter(), new Object[]{new Long(id)}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.destroyedFavorite(this.this$0.destroyFavorite(((Long) args[0]).longValue()));
            }
        });
    }

    public void followAsync(String id, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 14, listener, new String[]{id}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.followed(this.this$0.follow((String) args[0]));
            }
        });
    }

    public void enableNotificationAsync(String id, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 35, listener, new String[]{id}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.enabledNotification(this.this$0.enableNotification((String) args[0]));
            }
        });
    }

    public void followAsync(String id) {
        getDispatcher().invokeLater(new AsyncTask(this, 14, new TwitterAdapter(), new String[]{id}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.followed(this.this$0.follow((String) args[0]));
            }
        });
    }

    public void enableNotificationAsync(String id) {
        enableNotificationAsync(id, new TwitterAdapter());
    }

    public void leaveAsync(String id, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 15, listener, new String[]{id}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.left(this.this$0.leave((String) args[0]));
            }
        });
    }

    public void disableNotificationAsync(String id, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 36, listener, new String[]{id}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.disabledNotification(this.this$0.disableNotification((String) args[0]));
            }
        });
    }

    public void leaveAsync(String id) {
        getDispatcher().invokeLater(new AsyncTask(this, 15, new TwitterAdapter(), new String[]{id}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.left(this.this$0.leave((String) args[0]));
            }
        });
    }

    public void disableNotificationAsync(String id) {
        disableNotificationAsync(id, new TwitterAdapter());
    }

    public void blockAsync(String id) {
        getDispatcher().invokeLater(new AsyncTask(this, 22, new TwitterAdapter(), new String[]{id}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.blocked(this.this$0.block((String) args[0]));
            }
        });
    }

    public void createBlockAsync(String id, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 43, listener, new String[]{id}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.createdBlock(this.this$0.createBlock((String) args[0]));
            }
        });
    }

    public void createBlockAsync(String id) {
        createBlockAsync(id, new TwitterAdapter());
    }

    public void unblockAsync(String id) {
        getDispatcher().invokeLater(new AsyncTask(this, 23, new TwitterAdapter(), new String[]{id}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.unblocked(this.this$0.unblock((String) args[0]));
            }
        });
    }

    public void destroyBlockAsync(String id, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 42, listener, new String[]{id}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.destroyedBlock(this.this$0.destroyBlock((String) args[0]));
            }
        });
    }

    public void destroyBlockAsync(String id) {
        destroyBlockAsync(id, new TwitterAdapter());
    }

    public void existsBlockAsync(String id, TwitterListener listener) {
        getDispatcher().invokeLater(new AsyncTask(this, 48, listener, new String[]{id}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotExistsBlock(this.this$0.existsBlock((String) args[0]));
            }
        });
    }

    public void getBlockingUsersAsync(TwitterListener listener) throws TwitterException {
        getDispatcher().invokeLater(new AsyncTask(this, GET_BLOCKING_USERS, listener, null) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotBlockingUsers(this.this$0.getBlockingUsers());
            }
        });
    }

    public void getBlockingUsersAsync(int page, TwitterListener listener) throws TwitterException {
        getDispatcher().invokeLater(new AsyncTask(this, GET_BLOCKING_USERS, listener, new Integer[]{new Integer(page)}) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotBlockingUsers(this.this$0.getBlockingUsers(((Integer) args[0]).intValue()));
            }
        });
    }

    public void getBlockingUsersIDsAsync(TwitterListener listener) throws TwitterException {
        getDispatcher().invokeLater(new AsyncTask(this, GET_BLOCKING_USERS_IDS, listener, null) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.gotBlockingUsersIDs(this.this$0.getBlockingUsersIDs());
            }
        });
    }

    public void testAsync() {
        getDispatcher().invokeLater(new AsyncTask(this, 24, new TwitterAdapter(), new Object[0]) {
            private final AsyncTwitter this$0;

            {
                this.this$0 = r1;
            }

            public void invoke(TwitterListener listener, Object[] args) throws TwitterException {
                listener.tested(this.this$0.test());
            }
        });
    }

    static Class class$(String x0) {
        try {
            return Class.forName(x0);
        } catch (ClassNotFoundException x1) {
            throw new NoClassDefFoundError().initCause(x1);
        }
    }

    public void shutdown() {
        Class cls;
        if (class$twitter4j$AsyncTwitter == null) {
            cls = class$("twitter4j.AsyncTwitter");
            class$twitter4j$AsyncTwitter = cls;
        } else {
            cls = class$twitter4j$AsyncTwitter;
        }
        synchronized (cls) {
            this.shutdown = true;
            throw new IllegalStateException("Already shut down");
        }
    }

    private Dispatcher getDispatcher() {
        if (true == this.shutdown) {
            throw new IllegalStateException("Already shut down");
        }
        if (dispatcher == null) {
            dispatcher = new Dispatcher("Twitter4J Async Dispatcher", Configuration.getNumberOfAsyncThreads());
        }
        return dispatcher;
    }

    public void getDowntimeScheduleAsync() {
        throw new RuntimeException("this method is not supported by the Twitter API anymore", new NoSuchMethodException("this method is not supported by the Twitter API anymore"));
    }

    public void getAuthenticatedUserAsync(TwitterListener listener) {
        if (getUserId() == null) {
            throw new IllegalStateException("User Id not specified.");
        }
        getUserDetailAsync(getUserId(), listener);
    }

    abstract class AsyncTask implements Runnable {
        Object[] args;
        TwitterListener listener;
        int method;
        private final AsyncTwitter this$0;

        /* access modifiers changed from: package-private */
        public abstract void invoke(TwitterListener twitterListener, Object[] objArr) throws TwitterException;

        AsyncTask(AsyncTwitter asyncTwitter, int method2, TwitterListener listener2, Object[] args2) {
            this.this$0 = asyncTwitter;
            this.method = method2;
            this.listener = listener2;
            this.args = args2;
        }

        public void run() {
            try {
                invoke(this.listener, this.args);
            } catch (TwitterException te) {
                if (this.listener != null) {
                    this.listener.onException(te, this.method);
                }
            }
        }
    }
}
