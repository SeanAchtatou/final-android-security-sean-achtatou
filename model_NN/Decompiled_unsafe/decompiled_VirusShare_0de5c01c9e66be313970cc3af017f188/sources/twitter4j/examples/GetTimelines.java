package twitter4j.examples;

import java.util.List;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;

public class GetTimelines {
    public static void main(String[] args) {
        Twitter unauthenticatedTwitter = new Twitter();
        System.out.println("Showing public timeline.");
        try {
            for (Status status : unauthenticatedTwitter.getPublicTimeline()) {
                System.out.println(new StringBuffer().append(status.getUser().getName()).append(":").append(status.getText()).toString());
            }
            if (args.length < 2) {
                System.out.println("You need to specify TwitterID/Password combination to show UserTimelines.");
                System.out.println("Usage: java twitter4j.examples.GetTimelines ID Password");
                System.exit(0);
            }
            Twitter twitter = new Twitter(args[0], args[1]);
            List<Status> statuses = twitter.getFriendsTimeline();
            System.out.println("------------------------------");
            System.out.println(new StringBuffer().append("Showing ").append(args[0]).append("'s friends timeline.").toString());
            for (Status status2 : statuses) {
                System.out.println(new StringBuffer().append(status2.getUser().getName()).append(":").append(status2.getText()).toString());
            }
            List<Status> statuses2 = twitter.getUserTimeline();
            System.out.println("------------------------------");
            System.out.println(new StringBuffer().append("Showing ").append(args[0]).append("'s timeline.").toString());
            for (Status status3 : statuses2) {
                System.out.println(new StringBuffer().append(status3.getUser().getName()).append(":").append(status3.getText()).toString());
            }
            Status status4 = twitter.show(81642112L);
            System.out.println("------------------------------");
            System.out.println(new StringBuffer().append("Showing ").append(status4.getUser().getName()).append("'s status updated at ").append(status4.getCreatedAt()).toString());
            System.out.println(status4.getText());
            System.exit(0);
        } catch (TwitterException te) {
            System.out.println(new StringBuffer().append("Failed to get timeline: ").append(te.getMessage()).toString());
            System.exit(-1);
        }
    }
}
