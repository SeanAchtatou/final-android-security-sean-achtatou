package twitter4j.examples;

public class OAuthUpdate {
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0088, code lost:
        r5 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0089, code lost:
        java.lang.System.out.println(new java.lang.StringBuffer().append("Failed to get timeline: ").append(r5.getMessage()).toString());
        java.lang.System.exit(-1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x00ae, code lost:
        java.lang.System.out.println("Failed to read the system input.");
        java.lang.System.exit(-1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00ad A[ExcHandler: IOException (e java.io.IOException), Splitter:B:1:0x0001] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void main(java.lang.String[] r11) {
        /*
            r10 = -1
            twitter4j.Twitter r6 = new twitter4j.Twitter     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            r6.<init>()     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            twitter4j.http.RequestToken r3 = r6.getOAuthRequestToken()     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.io.PrintStream r7 = java.lang.System.out     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.lang.String r8 = "Got request token."
            r7.println(r8)     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.io.PrintStream r7 = java.lang.System.out     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.lang.StringBuffer r8 = new java.lang.StringBuffer     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            r8.<init>()     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.lang.String r9 = "Request token: "
            java.lang.StringBuffer r8 = r8.append(r9)     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.lang.String r9 = r3.getToken()     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.lang.StringBuffer r8 = r8.append(r9)     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.lang.String r8 = r8.toString()     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            r7.println(r8)     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.io.PrintStream r7 = java.lang.System.out     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.lang.StringBuffer r8 = new java.lang.StringBuffer     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            r8.<init>()     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.lang.String r9 = "Request token secret: "
            java.lang.StringBuffer r8 = r8.append(r9)     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.lang.String r9 = r3.getTokenSecret()     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.lang.StringBuffer r8 = r8.append(r9)     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.lang.String r8 = r8.toString()     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            r7.println(r8)     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            r0 = 0
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.io.InputStreamReader r7 = new java.io.InputStreamReader     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.io.InputStream r8 = java.lang.System.in     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            r7.<init>(r8)     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            r1.<init>(r7)     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
        L_0x0056:
            if (r0 != 0) goto L_0x00b9
            java.io.PrintStream r7 = java.lang.System.out     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.lang.String r8 = "Open the following URL and grant access to your account:"
            r7.println(r8)     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.io.PrintStream r7 = java.lang.System.out     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.lang.String r8 = r3.getAuthorizationURL()     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            r7.println(r8)     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.io.PrintStream r7 = java.lang.System.out     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.lang.String r8 = "Hit enter when it's done.[Enter]:"
            r7.print(r8)     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            r1.readLine()     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            twitter4j.http.AccessToken r0 = r3.getAccessToken()     // Catch:{ TwitterException -> 0x0077, IOException -> 0x00ad }
            goto L_0x0056
        L_0x0077:
            r5 = move-exception
            r7 = 401(0x191, float:5.62E-43)
            int r8 = r5.getStatusCode()     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            if (r7 != r8) goto L_0x00a9
            java.io.PrintStream r7 = java.lang.System.out     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.lang.String r8 = "Unable to get the access token."
            r7.println(r8)     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            goto L_0x0056
        L_0x0088:
            r5 = move-exception
            java.io.PrintStream r7 = java.lang.System.out
            java.lang.StringBuffer r8 = new java.lang.StringBuffer
            r8.<init>()
            java.lang.String r9 = "Failed to get timeline: "
            java.lang.StringBuffer r8 = r8.append(r9)
            java.lang.String r9 = r5.getMessage()
            java.lang.StringBuffer r8 = r8.append(r9)
            java.lang.String r8 = r8.toString()
            r7.println(r8)
            java.lang.System.exit(r10)
        L_0x00a8:
            return
        L_0x00a9:
            r5.printStackTrace()     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            goto L_0x0056
        L_0x00ad:
            r2 = move-exception
            java.io.PrintStream r7 = java.lang.System.out
            java.lang.String r8 = "Failed to read the system input."
            r7.println(r8)
            java.lang.System.exit(r10)
            goto L_0x00a8
        L_0x00b9:
            java.io.PrintStream r7 = java.lang.System.out     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.lang.String r8 = "Got access token."
            r7.println(r8)     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.io.PrintStream r7 = java.lang.System.out     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.lang.StringBuffer r8 = new java.lang.StringBuffer     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            r8.<init>()     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.lang.String r9 = "Access token: "
            java.lang.StringBuffer r8 = r8.append(r9)     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.lang.String r9 = r0.getToken()     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.lang.StringBuffer r8 = r8.append(r9)     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.lang.String r8 = r8.toString()     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            r7.println(r8)     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.io.PrintStream r7 = java.lang.System.out     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.lang.StringBuffer r8 = new java.lang.StringBuffer     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            r8.<init>()     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.lang.String r9 = "Access token secret: "
            java.lang.StringBuffer r8 = r8.append(r9)     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.lang.String r9 = r0.getTokenSecret()     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.lang.StringBuffer r8 = r8.append(r9)     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.lang.String r8 = r8.toString()     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            r7.println(r8)     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            r7 = 0
            r7 = r11[r7]     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            twitter4j.Status r4 = r6.updateStatus(r7)     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.io.PrintStream r7 = java.lang.System.out     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.lang.StringBuffer r8 = new java.lang.StringBuffer     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            r8.<init>()     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.lang.String r9 = "Successfully updated the status to ["
            java.lang.StringBuffer r8 = r8.append(r9)     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.lang.String r9 = r4.getText()     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.lang.StringBuffer r8 = r8.append(r9)     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.lang.String r9 = "]."
            java.lang.StringBuffer r8 = r8.append(r9)     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            java.lang.String r8 = r8.toString()     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            r7.println(r8)     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            r7 = 0
            java.lang.System.exit(r7)     // Catch:{ TwitterException -> 0x0088, IOException -> 0x00ad }
            goto L_0x00a8
        */
        throw new UnsupportedOperationException("Method not decompiled: twitter4j.examples.OAuthUpdate.main(java.lang.String[]):void");
    }
}
