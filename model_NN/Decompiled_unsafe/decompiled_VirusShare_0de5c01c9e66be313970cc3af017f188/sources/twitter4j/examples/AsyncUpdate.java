package twitter4j.examples;

import twitter4j.AsyncTwitter;
import twitter4j.Status;
import twitter4j.TwitterAdapter;
import twitter4j.TwitterException;

public class AsyncUpdate {
    static Object lock = new Object();

    public static void main(String[] args) throws InterruptedException {
        if (args.length < 3) {
            System.out.println("Usage: java twitter4j.examples.AsyncUpdate ID Password text");
            System.exit(-1);
        }
        new AsyncTwitter(args[0], args[1]).updateStatusAsync(args[2], new TwitterAdapter() {
            public void updated(Status status) {
                System.out.println(new StringBuffer().append("Successfully updated the status to [").append(status.getText()).append("].").toString());
                synchronized (AsyncUpdate.lock) {
                    AsyncUpdate.lock.notify();
                }
            }

            public void onException(TwitterException e, int method) {
                if (method == 39) {
                    e.printStackTrace();
                    synchronized (AsyncUpdate.lock) {
                        AsyncUpdate.lock.notify();
                    }
                    return;
                }
                synchronized (AsyncUpdate.lock) {
                    AsyncUpdate.lock.notify();
                }
                throw new AssertionError("Should not happen");
            }
        });
        synchronized (lock) {
            lock.wait();
        }
    }
}
