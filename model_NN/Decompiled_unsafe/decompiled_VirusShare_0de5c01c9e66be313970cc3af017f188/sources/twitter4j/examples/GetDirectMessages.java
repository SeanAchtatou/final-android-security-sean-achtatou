package twitter4j.examples;

import twitter4j.DirectMessage;
import twitter4j.Twitter;
import twitter4j.TwitterException;

public class GetDirectMessages {
    public static void main(String[] args) {
        if (args.length < 2) {
            System.out.println("No TwitterID/Password specified.");
            System.out.println("Usage: java twitter4j.examples.GetDirectMessages ID Password");
            System.exit(-1);
        }
        try {
            for (DirectMessage message : new Twitter(args[0], args[1]).getDirectMessages()) {
                System.out.println(new StringBuffer().append("Sender:").append(message.getSenderScreenName()).toString());
                System.out.println(new StringBuffer().append("Text:").append(message.getText()).append("\n").toString());
            }
            System.exit(0);
        } catch (TwitterException te) {
            System.out.println(new StringBuffer().append("Failed to get messages: ").append(te.getMessage()).toString());
            System.exit(-1);
        }
    }
}
