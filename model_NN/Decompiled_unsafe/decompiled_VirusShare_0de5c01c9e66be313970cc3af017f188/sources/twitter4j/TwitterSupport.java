package twitter4j;

import twitter4j.http.HttpClient;

class TwitterSupport {
    protected final boolean USE_SSL;
    protected HttpClient http;
    protected String source;

    TwitterSupport() {
        this(null, null);
    }

    TwitterSupport(String userId, String password) {
        this.http = new HttpClient();
        this.source = Configuration.getSource();
        this.USE_SSL = Configuration.useSSL();
        setClientVersion(null);
        setClientURL(null);
        setUserId(userId);
        setPassword(password);
    }

    public void setUserAgent(String userAgent) {
        this.http.setUserAgent(userAgent);
    }

    public String getUserAgent() {
        return this.http.getUserAgent();
    }

    public void setClientVersion(String version) {
        setRequestHeader("X-Twitter-Client-Version", Configuration.getCilentVersion(version));
    }

    public String getClientVersion() {
        return this.http.getRequestHeader("X-Twitter-Client-Version");
    }

    public void setClientURL(String clientURL) {
        setRequestHeader("X-Twitter-Client-URL", Configuration.getClientURL(clientURL));
    }

    public String getClientURL() {
        return this.http.getRequestHeader("X-Twitter-Client-URL");
    }

    public synchronized void setUserId(String userId) {
        this.http.setUserId(Configuration.getUser(userId));
    }

    public String getUserId() {
        return this.http.getUserId();
    }

    public synchronized void setPassword(String password) {
        this.http.setPassword(Configuration.getPassword(password));
    }

    public String getPassword() {
        return this.http.getPassword();
    }

    public void setHttpProxy(String proxyHost, int proxyPort) {
        this.http.setProxyHost(proxyHost);
        this.http.setProxyPort(proxyPort);
    }

    public void setHttpProxyAuth(String proxyUser, String proxyPass) {
        this.http.setProxyAuthUser(proxyUser);
        this.http.setProxyAuthPassword(proxyPass);
    }

    public void setHttpConnectionTimeout(int connectionTimeout) {
        this.http.setConnectionTimeout(connectionTimeout);
    }

    public void setHttpReadTimeout(int readTimeoutMilliSecs) {
        this.http.setReadTimeout(readTimeoutMilliSecs);
    }

    public void setSource(String source2) {
        this.source = Configuration.getSource(source2);
        setRequestHeader("X-Twitter-Client", this.source);
    }

    public String getSource() {
        return this.source;
    }

    public void setRequestHeader(String name, String value) {
        this.http.setRequestHeader(name, value);
    }

    public void forceUsePost(boolean forceUsePost) {
    }

    public boolean isUsePostForced() {
        return false;
    }

    public void setRetryCount(int retryCount) {
        this.http.setRetryCount(retryCount);
    }

    public void setRetryIntervalSecs(int retryIntervalSecs) {
        this.http.setRetryIntervalSecs(retryIntervalSecs);
    }
}
