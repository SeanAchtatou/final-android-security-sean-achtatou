package twitter4j;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import twitter4j.http.Response;

public class RetweetDetails extends TwitterResponse implements Serializable {
    static final long serialVersionUID = 1957982268696560598L;
    private long retweetId;
    private Date retweetedAt;
    private User retweetingUser;

    RetweetDetails(Response res, Twitter twitter) throws TwitterException {
        super(res);
        init(res, res.asDocument().getDocumentElement(), twitter);
    }

    RetweetDetails(Response res, Element elem, Twitter twitter) throws TwitterException {
        super(res);
        init(res, elem, twitter);
    }

    private void init(Response res, Element elem, Twitter twitter) throws TwitterException {
        ensureRootNodeNameIs("retweet_details", elem);
        this.retweetId = getChildLong("retweet_id", elem);
        this.retweetedAt = getChildDate("retweeted_at", elem);
        this.retweetingUser = new User(res, (Element) elem.getElementsByTagName("retweeting_user").item(0), twitter);
    }

    public long getRetweetId() {
        return this.retweetId;
    }

    public Date getRetweetedAt() {
        return this.retweetedAt;
    }

    public User getRetweetingUser() {
        return this.retweetingUser;
    }

    static List<RetweetDetails> createRetweetDetails(Response res, Twitter twitter) throws TwitterException {
        Document doc = res.asDocument();
        if (isRootNodeNilClasses(doc)) {
            return new ArrayList(0);
        }
        try {
            ensureRootNodeNameIs("retweets", doc);
            NodeList list = doc.getDocumentElement().getElementsByTagName("retweet_details");
            int size = list.getLength();
            List<RetweetDetails> statuses = new ArrayList<>(size);
            for (int i = 0; i < size; i++) {
                statuses.add(new RetweetDetails(res, (Element) list.item(i), twitter));
            }
            return statuses;
        } catch (TwitterException e) {
            ensureRootNodeNameIs("nil-classes", doc);
            return new ArrayList(0);
        }
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RetweetDetails)) {
            return false;
        }
        if (this.retweetId != ((RetweetDetails) o).retweetId) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (((((int) (this.retweetId ^ (this.retweetId >>> 32))) * 31) + this.retweetedAt.hashCode()) * 31) + this.retweetingUser.hashCode();
    }

    public String toString() {
        return new StringBuffer().append("RetweetDetails{retweetId=").append(this.retweetId).append(", retweetedAt=").append(this.retweetedAt).append(", retweetingUser=").append(this.retweetingUser).append('}').toString();
    }
}
