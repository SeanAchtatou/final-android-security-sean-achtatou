package twitter4j;

import com.google.devtools.simple.runtime.annotations.DesignerProperty;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import twitter4j.http.Response;
import twitter4j.org.json.JSONException;
import twitter4j.org.json.JSONObject;

public class Status extends TwitterResponse implements Serializable {
    private static final long serialVersionUID = 1608000492860584608L;
    private Date createdAt;
    private long id;
    private String inReplyToScreenName;
    private long inReplyToStatusId;
    private int inReplyToUserId;
    private boolean isFavorited;
    private boolean isTruncated;
    private double latitude = -1.0d;
    private double longitude = -1.0d;
    private RetweetDetails retweetDetails;
    private String source;
    private String text;
    private User user = null;

    Status(Response res, Twitter twitter) throws TwitterException {
        super(res);
        init(res, res.asDocument().getDocumentElement(), twitter);
    }

    Status(Response res, Element elem, Twitter twitter) throws TwitterException {
        super(res);
        init(res, elem, twitter);
    }

    public Status(String str) throws TwitterException, JSONException {
        JSONObject json = new JSONObject(str);
        this.id = json.getLong("id");
        this.text = json.getString(DesignerProperty.PROPERTY_TYPE_TEXT);
        this.source = json.getString("source");
        this.createdAt = parseDate(json.getString("created_at"), "EEE MMM dd HH:mm:ss z yyyy");
        this.inReplyToStatusId = getLong("in_reply_to_status_id", json);
        this.inReplyToUserId = getInt("in_reply_to_user_id", json);
        this.isFavorited = getBoolean("favorited", json);
        this.user = new User(json.getJSONObject("user"));
    }

    private void init(Response res, Element elem, Twitter twitter) throws TwitterException {
        ensureRootNodeNameIs("status", elem);
        this.user = new User(res, (Element) elem.getElementsByTagName("user").item(0), twitter);
        this.id = getChildLong("id", elem);
        this.text = getChildText(DesignerProperty.PROPERTY_TYPE_TEXT, elem);
        this.source = getChildText("source", elem);
        this.createdAt = getChildDate("created_at", elem);
        this.isTruncated = getChildBoolean("truncated", elem);
        this.inReplyToStatusId = getChildLong("in_reply_to_status_id", elem);
        this.inReplyToUserId = getChildInt("in_reply_to_user_id", elem);
        this.isFavorited = getChildBoolean("favorited", elem);
        this.inReplyToScreenName = getChildText("in_reply_to_screen_name", elem);
        NodeList georssPoint = elem.getElementsByTagName("georss:point");
        if (1 == georssPoint.getLength()) {
            String[] point = georssPoint.item(0).getFirstChild().getNodeValue().split(" ");
            this.latitude = Double.parseDouble(point[0]);
            this.longitude = Double.parseDouble(point[1]);
        }
        NodeList retweetDetailsNode = elem.getElementsByTagName("retweet_details");
        if (1 == retweetDetailsNode.getLength()) {
            this.retweetDetails = new RetweetDetails(res, (Element) retweetDetailsNode.item(0), twitter);
        }
    }

    public Date getCreatedAt() {
        return this.createdAt;
    }

    public long getId() {
        return this.id;
    }

    public String getText() {
        return this.text;
    }

    public String getSource() {
        return this.source;
    }

    public boolean isTruncated() {
        return this.isTruncated;
    }

    public long getInReplyToStatusId() {
        return this.inReplyToStatusId;
    }

    public int getInReplyToUserId() {
        return this.inReplyToUserId;
    }

    public String getInReplyToScreenName() {
        return this.inReplyToScreenName;
    }

    public double getLatitude() {
        return this.latitude;
    }

    public double getLongitude() {
        return this.longitude;
    }

    public boolean isFavorited() {
        return this.isFavorited;
    }

    public User getUser() {
        return this.user;
    }

    public boolean isRetweet() {
        return this.retweetDetails != null;
    }

    public RetweetDetails getRetweetDetails() {
        return this.retweetDetails;
    }

    static List<Status> constructStatuses(Response res, Twitter twitter) throws TwitterException {
        Document doc = res.asDocument();
        if (isRootNodeNilClasses(doc)) {
            return new ArrayList(0);
        }
        try {
            ensureRootNodeNameIs("statuses", doc);
            NodeList list = doc.getDocumentElement().getElementsByTagName("status");
            int size = list.getLength();
            List<Status> statuses = new ArrayList<>(size);
            for (int i = 0; i < size; i++) {
                statuses.add(new Status(res, (Element) list.item(i), twitter));
            }
            return statuses;
        } catch (TwitterException e) {
            ensureRootNodeNameIs("nil-classes", doc);
            return new ArrayList(0);
        }
    }

    public int hashCode() {
        return (int) this.id;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Status) || ((Status) obj).id != this.id) {
            return false;
        }
        return true;
    }

    public String toString() {
        return new StringBuffer().append("Status{createdAt=").append(this.createdAt).append(", id=").append(this.id).append(", text='").append(this.text).append('\'').append(", source='").append(this.source).append('\'').append(", isTruncated=").append(this.isTruncated).append(", inReplyToStatusId=").append(this.inReplyToStatusId).append(", inReplyToUserId=").append(this.inReplyToUserId).append(", isFavorited=").append(this.isFavorited).append(", inReplyToScreenName='").append(this.inReplyToScreenName).append('\'').append(", latitude=").append(this.latitude).append(", longitude=").append(this.longitude).append(", retweetDetails=").append(this.retweetDetails).append(", user=").append(this.user).append('}').toString();
    }
}
