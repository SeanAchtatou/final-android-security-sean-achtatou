package twitter4j.http;

import twitter4j.TwitterException;

public class RequestToken extends OAuthToken {
    private static final long serialVersionUID = -8214365845469757952L;
    private HttpClient httpClient;

    public String getParameter(String x0) {
        return super.getParameter(x0);
    }

    public String getToken() {
        return super.getToken();
    }

    public String getTokenSecret() {
        return super.getTokenSecret();
    }

    public String toString() {
        return super.toString();
    }

    RequestToken(Response res, HttpClient httpClient2) throws TwitterException {
        super(res);
        this.httpClient = httpClient2;
    }

    RequestToken(String token, String tokenSecret) {
        super(token, tokenSecret);
    }

    public String getAuthorizationURL() {
        return new StringBuffer().append(this.httpClient.getAuthorizationURL()).append("?oauth_token=").append(getToken()).toString();
    }

    public String getAuthenticationURL() {
        return new StringBuffer().append(this.httpClient.getAuthenticationRL()).append("?oauth_token=").append(getToken()).toString();
    }

    public AccessToken getAccessToken() throws TwitterException {
        return this.httpClient.getOAuthAccessToken(this);
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        RequestToken that = (RequestToken) o;
        if (this.httpClient != null) {
            if (this.httpClient.equals(that.httpClient)) {
                return true;
            }
        } else if (that.httpClient == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return (super.hashCode() * 31) + (this.httpClient != null ? this.httpClient.hashCode() : 0);
    }
}
