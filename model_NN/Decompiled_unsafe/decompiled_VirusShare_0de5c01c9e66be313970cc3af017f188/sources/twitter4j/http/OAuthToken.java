package twitter4j.http;

import java.io.Serializable;
import javax.crypto.spec.SecretKeySpec;
import twitter4j.TwitterException;

abstract class OAuthToken implements Serializable {
    String[] responseStr;
    private transient SecretKeySpec secretKeySpec;
    private String token;
    private String tokenSecret;

    public OAuthToken(String token2, String tokenSecret2) {
        this.responseStr = null;
        this.token = token2;
        this.tokenSecret = tokenSecret2;
    }

    OAuthToken(Response response) throws TwitterException {
        this(response.asString());
    }

    OAuthToken(String string) {
        this.responseStr = null;
        this.responseStr = string.split("&");
        this.tokenSecret = getParameter("oauth_token_secret");
        this.token = getParameter("oauth_token");
    }

    public String getToken() {
        return this.token;
    }

    public String getTokenSecret() {
        return this.tokenSecret;
    }

    /* access modifiers changed from: package-private */
    public void setSecretKeySpec(SecretKeySpec secretKeySpec2) {
        this.secretKeySpec = secretKeySpec2;
    }

    /* access modifiers changed from: package-private */
    public SecretKeySpec getSecretKeySpec() {
        return this.secretKeySpec;
    }

    public String getParameter(String parameter) {
        for (String str : this.responseStr) {
            if (str.startsWith(new StringBuffer().append(parameter).append('=').toString())) {
                return str.split("=")[1].trim();
            }
        }
        return null;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OAuthToken)) {
            return false;
        }
        OAuthToken that = (OAuthToken) o;
        if (this.secretKeySpec == null ? that.secretKeySpec != null : !this.secretKeySpec.equals(that.secretKeySpec)) {
            return false;
        }
        if (!this.token.equals(that.token)) {
            return false;
        }
        if (!this.tokenSecret.equals(that.tokenSecret)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (((this.token.hashCode() * 31) + this.tokenSecret.hashCode()) * 31) + (this.secretKeySpec != null ? this.secretKeySpec.hashCode() : 0);
    }

    public String toString() {
        return new StringBuffer().append("OAuthToken{token='").append(this.token).append('\'').append(", tokenSecret='").append(this.tokenSecret).append('\'').append(", secretKeySpec=").append(this.secretKeySpec).append('}').toString();
    }
}
