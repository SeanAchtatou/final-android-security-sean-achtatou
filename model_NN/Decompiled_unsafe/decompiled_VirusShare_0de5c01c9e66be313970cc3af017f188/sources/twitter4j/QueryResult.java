package twitter4j;

import java.util.ArrayList;
import java.util.List;
import twitter4j.http.Response;
import twitter4j.org.json.JSONArray;
import twitter4j.org.json.JSONException;
import twitter4j.org.json.JSONObject;

public class QueryResult extends TwitterResponse {
    private static final long serialVersionUID = -9059136565234613286L;
    private double completedIn;
    private long maxId;
    private int page;
    private String query;
    private String refreshUrl;
    private int resultsPerPage;
    private long sinceId;
    private int total = -1;
    private List<Tweet> tweets;
    private String warning;

    QueryResult(Response res, TwitterSupport twitterSupport) throws TwitterException {
        super(res);
        JSONObject json = res.asJSONObject();
        try {
            this.sinceId = json.getLong("since_id");
            this.maxId = json.getLong("max_id");
            this.refreshUrl = getString("refresh_url", json, true);
            this.resultsPerPage = json.getInt("results_per_page");
            this.warning = getString("warning", json, false);
            this.completedIn = json.getDouble("completed_in");
            this.page = json.getInt("page");
            this.query = getString("query", json, true);
            JSONArray array = json.getJSONArray("results");
            this.tweets = new ArrayList(array.length());
            for (int i = 0; i < array.length(); i++) {
                this.tweets.add(new Tweet(array.getJSONObject(i), twitterSupport));
            }
        } catch (JSONException jsone) {
            throw new TwitterException(new StringBuffer().append(jsone.getMessage()).append(":").append(json.toString()).toString(), jsone);
        }
    }

    QueryResult(Query query2) throws TwitterException {
        this.sinceId = query2.getSinceId();
        this.resultsPerPage = query2.getRpp();
        this.page = query2.getPage();
        this.tweets = new ArrayList(0);
    }

    public long getSinceId() {
        return this.sinceId;
    }

    public long getMaxId() {
        return this.maxId;
    }

    public String getRefreshUrl() {
        return this.refreshUrl;
    }

    public int getResultsPerPage() {
        return this.resultsPerPage;
    }

    public int getTotal() {
        return this.total;
    }

    public String getWarning() {
        return this.warning;
    }

    public double getCompletedIn() {
        return this.completedIn;
    }

    public int getPage() {
        return this.page;
    }

    public String getQuery() {
        return this.query;
    }

    public List<Tweet> getTweets() {
        return this.tweets;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        QueryResult that = (QueryResult) o;
        if (Double.compare(that.completedIn, this.completedIn) != 0) {
            return false;
        }
        if (this.maxId != that.maxId) {
            return false;
        }
        if (this.page != that.page) {
            return false;
        }
        if (this.resultsPerPage != that.resultsPerPage) {
            return false;
        }
        if (this.sinceId != that.sinceId) {
            return false;
        }
        if (this.total != that.total) {
            return false;
        }
        if (!this.query.equals(that.query)) {
            return false;
        }
        if (this.refreshUrl == null ? that.refreshUrl != null : !this.refreshUrl.equals(that.refreshUrl)) {
            return false;
        }
        if (this.tweets == null ? that.tweets != null : !this.tweets.equals(that.tweets)) {
            return false;
        }
        if (this.warning != null) {
            if (this.warning.equals(that.warning)) {
                return true;
            }
        } else if (that.warning == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i;
        int i2;
        int i3 = 0;
        int i4 = ((((int) (this.sinceId ^ (this.sinceId >>> 32))) * 31) + ((int) (this.maxId ^ (this.maxId >>> 32)))) * 31;
        if (this.refreshUrl != null) {
            i = this.refreshUrl.hashCode();
        } else {
            i = 0;
        }
        int i5 = (((((i4 + i) * 31) + this.resultsPerPage) * 31) + this.total) * 31;
        if (this.warning != null) {
            i2 = this.warning.hashCode();
        } else {
            i2 = 0;
        }
        int result = i5 + i2;
        long temp = this.completedIn != 0.0d ? Double.doubleToLongBits(this.completedIn) : 0;
        int hashCode = ((((((result * 31) + ((int) ((temp >>> 32) ^ temp))) * 31) + this.page) * 31) + this.query.hashCode()) * 31;
        if (this.tweets != null) {
            i3 = this.tweets.hashCode();
        }
        return hashCode + i3;
    }

    public String toString() {
        return new StringBuffer().append("QueryResult{sinceId=").append(this.sinceId).append(", maxId=").append(this.maxId).append(", refreshUrl='").append(this.refreshUrl).append('\'').append(", resultsPerPage=").append(this.resultsPerPage).append(", total=").append(this.total).append(", warning='").append(this.warning).append('\'').append(", completedIn=").append(this.completedIn).append(", page=").append(this.page).append(", query='").append(this.query).append('\'').append(", tweets=").append(this.tweets).append('}').toString();
    }
}
