package twitter4j;

import gnu.kawa.xml.ElementType;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import twitter4j.http.HTMLEntity;
import twitter4j.http.Response;
import twitter4j.org.json.JSONException;
import twitter4j.org.json.JSONObject;

public class TwitterResponse implements Serializable {
    private static final boolean IS_DALVIK = Configuration.isDalvik();
    private static Map<String, SimpleDateFormat> formatMap = new HashMap();
    private static final long serialVersionUID = 3519962197957449562L;
    private transient int rateLimitLimit = -1;
    private transient int rateLimitRemaining = -1;
    private transient long rateLimitReset = -1;

    public TwitterResponse() {
    }

    public TwitterResponse(Response res) {
        String limit = res.getResponseHeader("X-RateLimit-Limit");
        if (limit != null) {
            this.rateLimitLimit = Integer.parseInt(limit);
        }
        String remaining = res.getResponseHeader("X-RateLimit-Remaining");
        if (remaining != null) {
            this.rateLimitRemaining = Integer.parseInt(remaining);
        }
        String reset = res.getResponseHeader("X-RateLimit-Reset");
        if (reset != null) {
            this.rateLimitReset = Long.parseLong(reset);
        }
    }

    protected static void ensureRootNodeNameIs(String rootName, Element elem) throws TwitterException {
        if (!rootName.equals(elem.getNodeName())) {
            throw new TwitterException(new StringBuffer().append("Unexpected root node name:").append(elem.getNodeName()).append(". Expected:").append(rootName).append(". Check the availability of the Twitter API at http://status.twitter.com/.").toString());
        }
    }

    protected static void ensureRootNodeNameIs(String[] rootNames, Element elem) throws TwitterException {
        String actualRootName = elem.getNodeName();
        String[] arr$ = rootNames;
        int len$ = arr$.length;
        int i$ = 0;
        while (i$ < len$) {
            if (!arr$[i$].equals(actualRootName)) {
                i$++;
            } else {
                return;
            }
        }
        String expected = ElementType.MATCH_ANY_LOCALNAME;
        for (int i = 0; i < rootNames.length; i++) {
            if (i != 0) {
                expected = new StringBuffer().append(expected).append(" or ").toString();
            }
            expected = new StringBuffer().append(expected).append(rootNames[i]).toString();
        }
        throw new TwitterException(new StringBuffer().append("Unexpected root node name:").append(elem.getNodeName()).append(". Expected:").append(expected).append(". Check the availability of the Twitter API at http://status.twitter.com/.").toString());
    }

    protected static void ensureRootNodeNameIs(String rootName, Document doc) throws TwitterException {
        Element elem = doc.getDocumentElement();
        if (!rootName.equals(elem.getNodeName())) {
            throw new TwitterException(new StringBuffer().append("Unexpected root node name:").append(elem.getNodeName()).append(". Expected:").append(rootName).append(". Check the availability of the Twitter API at http://status.twitter.com/").toString());
        }
    }

    protected static boolean isRootNodeNilClasses(Document doc) {
        String root = doc.getDocumentElement().getNodeName();
        return "nil-classes".equals(root) || "nilclasses".equals(root);
    }

    protected static String getChildText(String str, Element elem) {
        return HTMLEntity.unescape(getTextContent(str, elem));
    }

    protected static String getTextContent(String str, Element elem) {
        Node node;
        String nodeValue;
        NodeList nodelist = elem.getElementsByTagName(str);
        if (nodelist.getLength() <= 0 || (node = nodelist.item(0).getFirstChild()) == null || (nodeValue = node.getNodeValue()) == null) {
            return ElementType.MATCH_ANY_LOCALNAME;
        }
        return nodeValue;
    }

    protected static int getChildInt(String str, Element elem) {
        String str2 = getTextContent(str, elem);
        if (str2 == null || ElementType.MATCH_ANY_LOCALNAME.equals(str2)) {
            return -1;
        }
        return Integer.valueOf(str2).intValue();
    }

    protected static long getChildLong(String str, Element elem) {
        String str2 = getTextContent(str, elem);
        if (str2 == null || ElementType.MATCH_ANY_LOCALNAME.equals(str2)) {
            return -1;
        }
        return Long.valueOf(str2).longValue();
    }

    protected static String getString(String name, JSONObject json, boolean decode) {
        String returnValue;
        try {
            String returnValue2 = json.getString(name);
            if (!decode) {
                return returnValue2;
            }
            try {
                return URLDecoder.decode(returnValue2, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                return returnValue;
            }
        } catch (JSONException e2) {
            return null;
        }
    }

    protected static boolean getChildBoolean(String str, Element elem) {
        return Boolean.valueOf(getTextContent(str, elem)).booleanValue();
    }

    protected static Date getChildDate(String str, Element elem) throws TwitterException {
        return getChildDate(str, elem, "EEE MMM d HH:mm:ss z yyyy");
    }

    protected static Date getChildDate(String str, Element elem, String format) throws TwitterException {
        return parseDate(getChildText(str, elem), format);
    }

    protected static Date parseDate(String str, String format) throws TwitterException {
        Date parse;
        SimpleDateFormat sdf = formatMap.get(format);
        if (sdf == null) {
            sdf = new SimpleDateFormat(format, Locale.ENGLISH);
            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
            formatMap.put(format, sdf);
        }
        try {
            synchronized (sdf) {
                parse = sdf.parse(str);
            }
            return parse;
        } catch (ParseException e) {
            throw new TwitterException(new StringBuffer().append("Unexpected format(").append(str).append(") returned from twitter.com").toString());
        }
    }

    protected static int getInt(String key, JSONObject json) throws JSONException {
        String str = json.getString(key);
        if (str == null || "null".equals(str)) {
            return -1;
        }
        return Integer.parseInt(str);
    }

    protected static long getLong(String key, JSONObject json) throws JSONException {
        String str = json.getString(key);
        if (str == null || "null".equals(str)) {
            return -1;
        }
        return Long.parseLong(str);
    }

    protected static boolean getBoolean(String key, JSONObject json) throws JSONException {
        String str = json.getString(key);
        if (str == null || "null".equals(str)) {
            return false;
        }
        return Boolean.valueOf(str).booleanValue();
    }

    public int getRateLimitLimit() {
        return this.rateLimitLimit;
    }

    public int getRateLimitRemaining() {
        return this.rateLimitRemaining;
    }

    public long getRateLimitReset() {
        return this.rateLimitReset;
    }
}
