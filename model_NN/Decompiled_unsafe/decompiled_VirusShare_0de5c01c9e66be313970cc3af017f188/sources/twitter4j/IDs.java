package twitter4j;

import java.util.Arrays;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import twitter4j.http.Response;

public class IDs extends TwitterResponse {
    private static String[] ROOT_NODE_NAMES = {"id_list", "ids"};
    private static final long serialVersionUID = -6585026560164704953L;
    private int[] ids;
    private long nextCursor;
    private long previousCursor;

    IDs(Response res) throws TwitterException {
        super(res);
        Element elem = res.asDocument().getDocumentElement();
        ensureRootNodeNameIs(ROOT_NODE_NAMES, elem);
        NodeList idlist = elem.getElementsByTagName("id");
        this.ids = new int[idlist.getLength()];
        int i = 0;
        while (i < idlist.getLength()) {
            try {
                this.ids[i] = Integer.parseInt(idlist.item(i).getFirstChild().getNodeValue());
                i++;
            } catch (NumberFormatException nfe) {
                throw new TwitterException(new StringBuffer().append("Twitter API returned malformed response: ").append(elem).toString(), nfe);
            }
        }
        this.previousCursor = getChildLong("previous_cursor", elem);
        this.nextCursor = getChildLong("next_cursor", elem);
    }

    public int[] getIDs() {
        return this.ids;
    }

    public boolean hasPrevious() {
        return 0 != this.previousCursor;
    }

    public long getPreviousCursor() {
        return this.previousCursor;
    }

    public boolean hasNext() {
        return 0 != this.nextCursor;
    }

    public long getNextCursor() {
        return this.nextCursor;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof IDs)) {
            return false;
        }
        if (!Arrays.equals(this.ids, ((IDs) o).ids)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        if (this.ids != null) {
            return Arrays.hashCode(this.ids);
        }
        return 0;
    }

    public String toString() {
        return new StringBuffer().append("IDs{ids=").append(this.ids).append(", previousCursor=").append(this.previousCursor).append(", nextCursor=").append(this.nextCursor).append('}').toString();
    }
}
