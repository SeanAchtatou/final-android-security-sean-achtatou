package gnu.mapping;

import gnu.lists.Consumer;
import java.io.Writer;

public class CharArrayOutPort extends OutPort {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: gnu.mapping.OutPort.<init>(java.io.Writer, boolean, gnu.text.Path):void
     arg types: [?[OBJECT, ARRAY], int, gnu.text.Path]
     candidates:
      gnu.mapping.OutPort.<init>(java.io.Writer, gnu.text.PrettyWriter, boolean):void
      gnu.mapping.OutPort.<init>(java.io.Writer, boolean, boolean):void
      gnu.mapping.OutPort.<init>(java.io.Writer, boolean, gnu.text.Path):void */
    public CharArrayOutPort() {
        super((Writer) null, false, CharArrayInPort.stringPath);
    }

    public int length() {
        return this.bout.bufferFillPointer;
    }

    public void setLength(int length) {
        this.bout.bufferFillPointer = length;
    }

    public void reset() {
        this.bout.bufferFillPointer = 0;
    }

    public char[] toCharArray() {
        int length = this.bout.bufferFillPointer;
        char[] result = new char[length];
        System.arraycopy(this.bout.buffer, 0, result, 0, length);
        return result;
    }

    public void close() {
    }

    /* access modifiers changed from: protected */
    public boolean closeOnExit() {
        return false;
    }

    public String toString() {
        return new String(this.bout.buffer, 0, this.bout.bufferFillPointer);
    }

    public String toSubString(int beginIndex, int endIndex) {
        if (endIndex <= this.bout.bufferFillPointer) {
            return new String(this.bout.buffer, beginIndex, endIndex - beginIndex);
        }
        throw new IndexOutOfBoundsException();
    }

    public String toSubString(int beginIndex) {
        return new String(this.bout.buffer, beginIndex, this.bout.bufferFillPointer - beginIndex);
    }

    public void writeTo(Consumer out) {
        out.write(this.bout.buffer, 0, this.bout.bufferFillPointer);
    }

    public void writeTo(int start, int count, Consumer out) {
        out.write(this.bout.buffer, start, count);
    }
}
