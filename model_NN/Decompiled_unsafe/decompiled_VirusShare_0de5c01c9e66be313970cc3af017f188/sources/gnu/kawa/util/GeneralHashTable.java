package gnu.kawa.util;

import java.util.Map;

public class GeneralHashTable<K, V> extends AbstractHashTable<HashNode<K, V>, K, V> {
    /* access modifiers changed from: protected */
    public /* bridge */ /* synthetic */ int getEntryHashCode(Map.Entry x0) {
        return getEntryHashCode((HashNode) ((HashNode) x0));
    }

    /* access modifiers changed from: protected */
    public /* bridge */ /* synthetic */ Map.Entry getEntryNext(Map.Entry x0) {
        return getEntryNext((HashNode) ((HashNode) x0));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: gnu.kawa.util.GeneralHashTable.setEntryNext(gnu.kawa.util.HashNode, gnu.kawa.util.HashNode):void
     arg types: [java.util.Map$Entry, java.util.Map$Entry]
     candidates:
      gnu.kawa.util.GeneralHashTable.setEntryNext(java.util.Map$Entry, java.util.Map$Entry):void
      gnu.kawa.util.AbstractHashTable.setEntryNext(java.util.Map$Entry, java.util.Map$Entry):void
      gnu.kawa.util.GeneralHashTable.setEntryNext(gnu.kawa.util.HashNode, gnu.kawa.util.HashNode):void */
    /* access modifiers changed from: protected */
    public /* bridge */ /* synthetic */ void setEntryNext(Map.Entry x0, Map.Entry x1) {
        setEntryNext((HashNode) ((HashNode) x0), (HashNode) ((HashNode) x1));
    }

    public GeneralHashTable() {
    }

    public GeneralHashTable(int capacity) {
        super(capacity);
    }

    /* access modifiers changed from: protected */
    public int getEntryHashCode(HashNode<K, V> entry) {
        return entry.hash;
    }

    /* access modifiers changed from: protected */
    public HashNode<K, V> getEntryNext(HashNode<K, V> entry) {
        return entry.next;
    }

    /* access modifiers changed from: protected */
    public void setEntryNext(HashNode<K, V> entry, HashNode<K, V> next) {
        entry.next = next;
    }

    /* access modifiers changed from: protected */
    public HashNode<K, V>[] allocEntries(int n) {
        return (HashNode[]) new HashNode[n];
    }

    /* access modifiers changed from: protected */
    public HashNode<K, V> makeEntry(Object obj, int hash, Object obj2) {
        HashNode<K, V> node = new HashNode<>(obj, obj2);
        node.hash = hash;
        return node;
    }

    public HashNode<K, V> getNode(Object key) {
        return (HashNode) super.getNode(key);
    }
}
