package gnu.kawa.lispexpr;

import gnu.bytecode.Access;
import gnu.expr.Keyword;
import gnu.expr.QuoteExp;
import gnu.expr.Special;
import gnu.kawa.util.GeneralHashTable;
import gnu.lists.FString;
import gnu.lists.LList;
import gnu.lists.Pair;
import gnu.lists.PairWithPosition;
import gnu.lists.Sequence;
import gnu.mapping.Environment;
import gnu.mapping.InPort;
import gnu.mapping.SimpleSymbol;
import gnu.mapping.Symbol;
import gnu.mapping.Values;
import gnu.math.IntNum;
import gnu.text.Char;
import gnu.text.Lexer;
import gnu.text.LineBufferedReader;
import gnu.text.SourceMessages;
import gnu.text.SyntaxException;
import java.io.IOException;

public class LispReader extends Lexer {
    static final int SCM_COMPLEX = 1;
    public static final int SCM_NUMBERS = 1;
    public static final char TOKEN_ESCAPE_CHAR = '￿';
    protected boolean seenEscapes;
    GeneralHashTable<Integer, Object> sharedStructureTable;

    public LispReader(LineBufferedReader port) {
        super(port);
    }

    public LispReader(LineBufferedReader port, SourceMessages messages) {
        super(port, messages);
    }

    public final void readNestedComment(char c1, char c2) throws IOException, SyntaxException {
        int commentNesting = 1;
        int startLine = this.port.getLineNumber();
        int startColumn = this.port.getColumnNumber();
        do {
            int c = read();
            if (c == 124) {
                c = read();
                if (c == c1) {
                    commentNesting--;
                }
            } else if (c == c1 && (c = read()) == c2) {
                commentNesting++;
            }
            if (c < 0) {
                eofError("unexpected end-of-file in " + c1 + c2 + " comment starting here", startLine + 1, startColumn - 1);
                return;
            }
        } while (commentNesting > 0);
    }

    static char getReadCase() {
        try {
            char read_case = Environment.getCurrent().get("symbol-read-case", "P").toString().charAt(0);
            if (read_case == 'P') {
                return read_case;
            }
            if (read_case == 'u') {
                return 'U';
            }
            if (read_case == 'd' || read_case == 'l' || read_case == 'L') {
                return 'D';
            }
            if (read_case == 'i') {
                return Access.INNERCLASS_CONTEXT;
            }
            return read_case;
        } catch (Exception e) {
            return 'P';
        }
    }

    public Object readValues(int ch, ReadTable rtable) throws IOException, SyntaxException {
        return readValues(ch, rtable.lookup(ch), rtable);
    }

    public Object readValues(int ch, ReadTableEntry entry, ReadTable rtable) throws IOException, SyntaxException {
        int startPos = this.tokenBufferLength;
        this.seenEscapes = false;
        switch (entry.getKind()) {
            case 0:
                String err = "invalid character #\\" + ((char) ch);
                if (this.interactive) {
                    fatal(err);
                } else {
                    error(err);
                }
                return Values.empty;
            case 1:
                return Values.empty;
            case 2:
            case 3:
            case 4:
            default:
                return readAndHandleToken(ch, startPos, rtable);
            case 5:
            case 6:
                return entry.read(this, ch, -1);
        }
    }

    /* access modifiers changed from: protected */
    public Object readAndHandleToken(int ch, int startPos, ReadTable rtable) throws IOException, SyntaxException {
        int j;
        Object value;
        readToken(ch, getReadCase(), rtable);
        int endPos = this.tokenBufferLength;
        if (!this.seenEscapes && (value = parseNumber(this.tokenBuffer, startPos, endPos - startPos, 0, 0, 1)) != null && !(value instanceof String)) {
            return value;
        }
        char readCase = getReadCase();
        if (readCase == 'I') {
            int upperCount = 0;
            int lowerCount = 0;
            int i = startPos;
            while (i < endPos) {
                char ci = this.tokenBuffer[i];
                if (ci == 65535) {
                    i++;
                } else if (Character.isLowerCase(ci)) {
                    lowerCount++;
                } else if (Character.isUpperCase(ci)) {
                    upperCount++;
                }
                i++;
            }
            if (lowerCount == 0) {
                readCase = 'D';
            } else if (upperCount == 0) {
                readCase = 'U';
            } else {
                readCase = 'P';
            }
        }
        boolean handleUri = endPos >= startPos + 2 && this.tokenBuffer[endPos + -1] == '}' && this.tokenBuffer[endPos + -2] != 65535 && peek() == 58;
        int packageMarker = -1;
        int lbrace = -1;
        int rbrace = -1;
        int braceNesting = 0;
        int i2 = startPos;
        int j2 = startPos;
        while (i2 < endPos) {
            char ci2 = this.tokenBuffer[i2];
            if (ci2 == 65535) {
                i2++;
                if (i2 < endPos) {
                    j = j2 + 1;
                    this.tokenBuffer[j2] = this.tokenBuffer[i2];
                } else {
                    j = j2;
                }
            } else {
                if (handleUri) {
                    if (ci2 == '{') {
                        if (lbrace < 0) {
                            lbrace = j2;
                        } else if (braceNesting == 0) {
                        }
                        braceNesting++;
                    } else if (ci2 == '}') {
                        braceNesting--;
                        if (braceNesting >= 0) {
                            if (braceNesting == 0) {
                                if (rbrace < 0) {
                                    rbrace = j2;
                                }
                            }
                        }
                    }
                }
                if (braceNesting <= 0) {
                    if (ci2 == ':') {
                        packageMarker = packageMarker >= 0 ? -1 : j2;
                    } else if (readCase == 'U') {
                        ci2 = Character.toUpperCase(ci2);
                    } else if (readCase == 'D') {
                        ci2 = Character.toLowerCase(ci2);
                    }
                }
                j = j2 + 1;
                this.tokenBuffer[j2] = ci2;
            }
            i2++;
            j2 = j;
        }
        int endPos2 = j2;
        int len = endPos2 - startPos;
        if (lbrace >= 0 && rbrace > lbrace) {
            String prefix = lbrace > 0 ? new String(this.tokenBuffer, startPos, lbrace - startPos) : null;
            int lbrace2 = lbrace + 1;
            String str = new String(this.tokenBuffer, lbrace2, rbrace - lbrace2);
            int ch2 = read();
            int ch3 = read();
            Object rightOperand = readValues(ch3, rtable.lookup(ch3), rtable);
            if (!(rightOperand instanceof SimpleSymbol)) {
                error("expected identifier in symbol after '{URI}:'");
            }
            return Symbol.valueOf(rightOperand.toString(), str, prefix);
        } else if (rtable.initialColonIsKeyword && packageMarker == startPos && len > 1) {
            int startPos2 = startPos + 1;
            return Keyword.make(new String(this.tokenBuffer, startPos2, endPos2 - startPos2).intern());
        } else if (!rtable.finalColonIsKeyword || packageMarker != endPos2 - 1 || (len <= 1 && !this.seenEscapes)) {
            return rtable.makeSymbol(new String(this.tokenBuffer, startPos, len));
        } else {
            return Keyword.make(new String(this.tokenBuffer, startPos, len - 1).intern());
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: package-private */
    public void readToken(int ch, char readCase, ReadTable rtable) throws IOException, SyntaxException {
        boolean inEscapes = false;
        int braceNesting = 0;
        while (true) {
            if (ch < 0) {
                if (inEscapes) {
                    eofError("unexpected EOF between escapes");
                } else {
                    return;
                }
            }
            ReadTableEntry entry = rtable.lookup(ch);
            int kind = entry.getKind();
            if (kind != 0) {
                if (ch == rtable.postfixLookupOperator && !inEscapes) {
                    int next = this.port.peek();
                    if (next == rtable.postfixLookupOperator) {
                        unread(ch);
                        return;
                    } else if (validPostfixLookupStart(next, rtable)) {
                        kind = 5;
                    }
                }
                if (kind != 3) {
                    if (kind != 4) {
                        if (!inEscapes) {
                            switch (kind) {
                                case 1:
                                    unread(ch);
                                    return;
                                case 2:
                                    if (ch == 123 && entry == ReadTableEntry.brace) {
                                        braceNesting++;
                                    }
                                    tokenBufferAppend(ch);
                                    break;
                                case 4:
                                    inEscapes = true;
                                    this.seenEscapes = true;
                                    break;
                                case 5:
                                    unread(ch);
                                    return;
                                case 6:
                                    tokenBufferAppend(ch);
                                    break;
                            }
                        } else {
                            tokenBufferAppend(65535);
                            tokenBufferAppend(ch);
                        }
                    } else {
                        inEscapes = !inEscapes;
                        this.seenEscapes = true;
                    }
                } else {
                    int ch2 = read();
                    if (ch2 < 0) {
                        eofError("unexpected EOF after single escape");
                    }
                    if (rtable.hexEscapeAfterBackslash && (ch2 == 120 || ch2 == 88)) {
                        ch2 = readHexEscape();
                    }
                    tokenBufferAppend(65535);
                    tokenBufferAppend(ch2);
                    this.seenEscapes = true;
                }
            } else if (inEscapes) {
                tokenBufferAppend(65535);
                tokenBufferAppend(ch);
            } else if (ch != 125 || braceNesting - 1 < 0) {
                unread(ch);
            } else {
                tokenBufferAppend(ch);
            }
            ch = read();
        }
        unread(ch);
    }

    public Object readObject() throws IOException, SyntaxException {
        int line;
        int column;
        Object value;
        char saveReadState = ((InPort) this.port).readState;
        int startPos = this.tokenBufferLength;
        ((InPort) this.port).readState = ' ';
        try {
            ReadTable rtable = ReadTable.getCurrent();
            do {
                line = this.port.getLineNumber();
                column = this.port.getColumnNumber();
                int ch = this.port.read();
                if (ch < 0) {
                    Object obj = Sequence.eofValue;
                    this.tokenBufferLength = startPos;
                    ((InPort) this.port).readState = saveReadState;
                    return obj;
                }
                value = readValues(ch, rtable);
            } while (value == Values.empty);
            Object handlePostfix = handlePostfix(value, rtable, line, column);
            this.tokenBufferLength = startPos;
            ((InPort) this.port).readState = saveReadState;
            return handlePostfix;
        } catch (Throwable th) {
            Throwable th2 = th;
            this.tokenBufferLength = startPos;
            ((InPort) this.port).readState = saveReadState;
            throw th2;
        }
    }

    /* access modifiers changed from: protected */
    public boolean validPostfixLookupStart(int ch, ReadTable rtable) throws IOException {
        if (ch < 0 || ch == 58 || ch == rtable.postfixLookupOperator) {
            return false;
        }
        if (ch == 44) {
            return true;
        }
        int kind = rtable.lookup(ch).getKind();
        if (kind == 2 || kind == 6 || kind == 4 || kind == 3) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public Object handlePostfix(Object value, ReadTable rtable, int line, int column) throws IOException, SyntaxException {
        if (value == QuoteExp.voidExp) {
            value = Values.empty;
        }
        while (true) {
            int ch = this.port.peek();
            if (ch < 0 || ch != rtable.postfixLookupOperator) {
                break;
            }
            this.port.read();
            if (!validPostfixLookupStart(this.port.peek(), rtable)) {
                unread();
                break;
            }
            int ch2 = this.port.read();
            value = PairWithPosition.make(LispLanguage.lookup_sym, LList.list2(value, LList.list2(rtable.makeSymbol(LispLanguage.quasiquote_sym), readValues(ch2, rtable.lookup(ch2), rtable))), this.port.getName(), line + 1, column + 1);
        }
        return value;
    }

    private boolean isPotentialNumber(char[] buffer, int start, int end) {
        boolean z = true;
        int sawDigits = 0;
        for (int i = start; i < end; i++) {
            char ch = buffer[i];
            if (Character.isDigit(ch)) {
                sawDigits++;
            } else if (ch == '-' || ch == '+') {
                if (i + 1 == end) {
                    return false;
                }
            } else if (ch == '#') {
                return true;
            } else {
                if (Character.isLetter(ch) || ch == '/' || ch == '_' || ch == '^') {
                    if (i == start) {
                        return false;
                    }
                } else if (ch != '.') {
                    return false;
                }
            }
        }
        if (sawDigits <= 0) {
            z = false;
        }
        return z;
    }

    public static Object parseNumber(CharSequence str, int radix) {
        char[] buf;
        if (str instanceof FString) {
            buf = ((FString) str).data;
        } else {
            buf = str.toString().toCharArray();
        }
        return parseNumber(buf, 0, str.length(), 0, radix, 1);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r37v2, types: [gnu.math.RatNum] */
    /* JADX WARN: Type inference failed for: r37v3, types: [gnu.math.RatNum] */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x01a9, code lost:
        r33 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x01ab, code lost:
        if (r4 >= 0) goto L_0x0516;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x01ad, code lost:
        if (r49 == false) goto L_0x01dd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x01b3, code lost:
        if ((r11 + 4) >= r25) goto L_0x01dd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x01bb, code lost:
        if (r53[r11 + 3] != '.') goto L_0x01dd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:125:0x01c3, code lost:
        if (r53[r11 + 4] != '0') goto L_0x01dd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:127:0x01c9, code lost:
        if (r53[r11] != 'i') goto L_0x0295;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:129:0x01d1, code lost:
        if (r53[r11 + 1] != 'n') goto L_0x0295;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:0x01d9, code lost:
        if (r53[r11 + 2] != 'f') goto L_0x0295;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:132:0x01db, code lost:
        r33 = 'i';
     */
    /* JADX WARNING: Code restructure failed: missing block: B:133:0x01dd, code lost:
        if (r33 != 0) goto L_0x02af;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:134:0x01df, code lost:
        return "no digits";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:182:0x0299, code lost:
        if (r53[r11] != 'n') goto L_0x01dd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:184:0x02a1, code lost:
        if (r53[r11 + 1] != 'a') goto L_0x01dd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:186:0x02a9, code lost:
        if (r53[r11 + 2] != 'n') goto L_0x01dd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:187:0x02ab, code lost:
        r33 = 'n';
     */
    /* JADX WARNING: Code restructure failed: missing block: B:188:0x02af, code lost:
        r42 = r11 + 5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:189:0x02b3, code lost:
        if (r29 != false) goto L_0x02b7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:190:0x02b5, code lost:
        if (r51 == false) goto L_0x02b7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:192:0x02bb, code lost:
        if (r56 == 'i') goto L_0x02cb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:194:0x02c1, code lost:
        if (r56 == 'I') goto L_0x02cb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:196:0x02c7, code lost:
        if (r56 != ' ') goto L_0x032b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:197:0x02c9, code lost:
        if (r29 == false) goto L_0x032b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:198:0x02cb, code lost:
        r32 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:199:0x02cd, code lost:
        r26 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:200:0x02d1, code lost:
        if (r33 == 0) goto L_0x0331;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:202:0x02d9, code lost:
        if (r33 != 'i') goto L_0x032e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:203:0x02db, code lost:
        r20 = Double.POSITIVE_INFINITY;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:205:0x02df, code lost:
        if (r7 == false) goto L_0x02e6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:206:0x02e1, code lost:
        r20 = -r20;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:207:0x02e6, code lost:
        r0 = new gnu.math.DFloNum(r20);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:209:0x02f1, code lost:
        if (r56 == 'e') goto L_0x02f9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:210:0x02f3, code lost:
        r37 = r37;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:211:0x02f7, code lost:
        if (r56 != 'E') goto L_0x02fd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:212:0x02f9, code lost:
        r37 = r37.toExact();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:214:0x0301, code lost:
        if (r42 >= r25) goto L_0x04dc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:215:0x0303, code lost:
        r11 = r42 + 1;
        r17 = r53[r42];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:216:0x030b, code lost:
        if (r17 != '@') goto L_0x0431;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:217:0x030d, code lost:
        r16 = parseNumber(r53, r11, r25 - r11, r56, 10, r58);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:218:0x031f, code lost:
        if ((r16 instanceof java.lang.String) != false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:220:0x0325, code lost:
        if ((r16 instanceof gnu.math.RealNum) != false) goto L_0x040c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:221:0x0327, code lost:
        return "invalid complex polar constant";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:222:0x032b, code lost:
        r32 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:223:0x032e, code lost:
        r20 = Double.NaN;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:224:0x0331, code lost:
        if (r28 >= 0) goto L_0x0335;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:225:0x0333, code lost:
        if (r22 < 0) goto L_0x039e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:227:0x0337, code lost:
        if (r4 <= r22) goto L_0x033d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:228:0x0339, code lost:
        if (r22 < 0) goto L_0x033d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:229:0x033b, code lost:
        r4 = r22;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:230:0x033d, code lost:
        if (r39 == null) goto L_0x0345;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:232:0x0345, code lost:
        r0 = new java.lang.String(r53, r4, r42 - r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:233:0x0350, code lost:
        if (r28 < 0) goto L_0x0388;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:234:0x0352, code lost:
        r26 = java.lang.Character.toLowerCase(r53[r28]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:235:0x035c, code lost:
        if (r26 == 'e') goto L_0x0388;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:236:0x035e, code lost:
        r43 = r28 - r4;
        r50 = r0.substring(0, r43) + 'e' + r0.substring(r43 + 1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:237:0x0388, code lost:
        r20 = gnu.lists.Convert.parseDouble(r50);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:238:0x038e, code lost:
        if (r7 == false) goto L_0x0395;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:239:0x0390, code lost:
        r20 = -r20;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:240:0x0395, code lost:
        r0 = new gnu.math.DFloNum(r20);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:241:0x039e, code lost:
        r34 = valueOf(r53, r4, r42 - r4, r57, r7, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:242:0x03a8, code lost:
        if (r39 != null) goto L_0x03c9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:243:0x03aa, code lost:
        r38 = r34;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:244:0x03ae, code lost:
        if (r32 == false) goto L_0x0512;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:246:0x03b4, code lost:
        if (r38.isExact() == false) goto L_0x0512;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:248:0x03b8, code lost:
        if (r40 == false) goto L_0x0407;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:250:0x03be, code lost:
        if (r38.isZero() == false) goto L_0x0407;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:251:0x03c0, code lost:
        r5 = -0.0d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:252:0x03c2, code lost:
        r0 = new gnu.math.DFloNum(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:254:0x03cd, code lost:
        if (r34.isZero() == false) goto L_0x03fc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:255:0x03cf, code lost:
        r41 = r39.isZero();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:256:0x03d3, code lost:
        if (r32 == false) goto L_0x03eb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:258:0x03d7, code lost:
        if (r41 == false) goto L_0x03e3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:259:0x03d9, code lost:
        r5 = Double.NaN;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:260:0x03db, code lost:
        r0 = new gnu.math.DFloNum(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:261:0x03e0, code lost:
        r38 = r37;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:262:0x03e3, code lost:
        if (r40 == false) goto L_0x03e8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:263:0x03e5, code lost:
        r5 = Double.NEGATIVE_INFINITY;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:264:0x03e8, code lost:
        r5 = Double.POSITIVE_INFINITY;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:265:0x03eb, code lost:
        if (r41 == false) goto L_0x03f3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:267:0x03f3, code lost:
        r37 = gnu.math.RatNum.make(r39, r34);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:268:0x03fc, code lost:
        r38 = gnu.math.RatNum.make(r39, r34);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:269:0x0407, code lost:
        r5 = r38.doubleValue();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:270:0x040c, code lost:
        r45 = (gnu.math.RealNum) r16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:271:0x0414, code lost:
        if (r37.isZero() == false) goto L_0x0427;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:273:0x041a, code lost:
        if (r45.isExact() != false) goto L_0x0427;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:277:0x0435, code lost:
        if (r17 == '-') goto L_0x043d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:279:0x043b, code lost:
        if (r17 != '+') goto L_0x0498;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:280:0x043d, code lost:
        r11 = r11 - 1;
        r31 = parseNumber(r53, r11, r25 - r11, r56, 10, r58);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:281:0x0451, code lost:
        if ((r31 instanceof java.lang.String) == false) goto L_0x0457;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:284:0x045b, code lost:
        if ((r31 instanceof gnu.math.Complex) != false) goto L_0x047a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:286:0x047a, code lost:
        r18 = (gnu.math.Complex) r31;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:287:0x0486, code lost:
        if (r18.re().isZero() != false) goto L_0x048c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:288:0x0488, code lost:
        return "invalid numeric constant";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:290:0x0498, code lost:
        r35 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:292:0x049e, code lost:
        if (java.lang.Character.isLetter(r17) != false) goto L_0x04bf;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:293:0x04a0, code lost:
        r11 = r11 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:295:0x04a5, code lost:
        if (r35 != 1) goto L_0x04d8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:296:0x04a7, code lost:
        r44 = r53[r11 - 1];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:297:0x04af, code lost:
        if (r44 == 'i') goto L_0x04b7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:299:0x04b5, code lost:
        if (r44 != 'I') goto L_0x04d8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:301:0x04b9, code lost:
        if (r11 >= r25) goto L_0x04cc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:302:0x04bb, code lost:
        return "junk after imaginary suffix 'i'";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:303:0x04bf, code lost:
        r35 = r35 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:304:0x04c3, code lost:
        if (r11 == r25) goto L_0x04a2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:305:0x04c5, code lost:
        r17 = r53[r11];
        r11 = r11 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:307:0x04d8, code lost:
        return "excess junk after number";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:309:0x04e0, code lost:
        if ((r37 instanceof gnu.math.DFloNum) == false) goto L_0x04f1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:310:0x04e2, code lost:
        if (r26 <= 0) goto L_0x04f1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:312:0x04e8, code lost:
        if (r26 == 'e') goto L_0x04f1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:313:0x04ea, code lost:
        r20 = r37.doubleValue();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:314:0x04ee, code lost:
        switch(r26) {
            case 100: goto L_0x0502;
            case com.google.devtools.simple.runtime.components.util.ErrorMessages.ERROR_LOCATION_SENSOR_LONGITUDE_NOT_FOUND :int: goto L_0x04f7;
            case 108: goto L_0x050a;
            case 115: goto L_0x04f7;
            default: goto L_0x04f1;
        };
     */
    /* JADX WARNING: Code restructure failed: missing block: B:319:0x0512, code lost:
        r37 = r38;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:320:0x0516, code lost:
        r42 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:358:?, code lost:
        return r16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:359:?, code lost:
        return "floating-point number after fraction symbol '/'";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:360:?, code lost:
        return "0/0 is undefined";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:361:?, code lost:
        return new gnu.math.DFloNum(0.0d);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:362:?, code lost:
        return gnu.math.Complex.polar(r37, r45);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:363:?, code lost:
        return r31;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:364:?, code lost:
        return "invalid numeric constant (" + r31 + ")";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:365:?, code lost:
        return gnu.math.Complex.make(r37, r18.im());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:366:?, code lost:
        return gnu.math.Complex.make(gnu.math.IntNum.zero(), r37);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:367:?, code lost:
        return r37;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:368:?, code lost:
        return java.lang.Float.valueOf((float) r20);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:369:?, code lost:
        return java.lang.Double.valueOf(r20);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:370:?, code lost:
        return java.math.BigDecimal.valueOf(r20);
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.Object parseNumber(char[] r53, int r54, int r55, char r56, int r57, int r58) {
        /*
            int r25 = r54 + r55
            r11 = r54
            r0 = r25
            if (r11 < r0) goto L_0x000b
            java.lang.String r16 = "no digits"
        L_0x000a:
            return r16
        L_0x000b:
            int r42 = r11 + 1
            char r17 = r53[r11]
        L_0x000f:
            r3 = 35
            r0 = r17
            if (r0 != r3) goto L_0x00d5
            r0 = r42
            r1 = r25
            if (r0 < r1) goto L_0x0020
            java.lang.String r16 = "no digits"
            r11 = r42
            goto L_0x000a
        L_0x0020:
            int r11 = r42 + 1
            char r17 = r53[r42]
            switch(r17) {
                case 66: goto L_0x0044;
                case 68: goto L_0x0062;
                case 69: goto L_0x0076;
                case 73: goto L_0x0076;
                case 79: goto L_0x0058;
                case 88: goto L_0x006c;
                case 98: goto L_0x0044;
                case 100: goto L_0x0062;
                case 101: goto L_0x0076;
                case 105: goto L_0x0076;
                case 111: goto L_0x0058;
                case 120: goto L_0x006c;
                default: goto L_0x0027;
            }
        L_0x0027:
            r52 = 0
        L_0x0029:
            r3 = 10
            r0 = r17
            int r23 = java.lang.Character.digit(r0, r3)
            if (r23 >= 0) goto L_0x0089
            r3 = 82
            r0 = r17
            if (r0 == r3) goto L_0x003f
            r3 = 114(0x72, float:1.6E-43)
            r0 = r17
            if (r0 != r3) goto L_0x00b0
        L_0x003f:
            if (r57 == 0) goto L_0x009c
            java.lang.String r16 = "duplicate radix specifier"
            goto L_0x000a
        L_0x0044:
            if (r57 == 0) goto L_0x0049
            java.lang.String r16 = "duplicate radix specifier"
            goto L_0x000a
        L_0x0049:
            r57 = 2
            r42 = r11
        L_0x004d:
            r0 = r42
            r1 = r25
            if (r0 < r1) goto L_0x00cd
            java.lang.String r16 = "no digits"
            r11 = r42
            goto L_0x000a
        L_0x0058:
            if (r57 == 0) goto L_0x005d
            java.lang.String r16 = "duplicate radix specifier"
            goto L_0x000a
        L_0x005d:
            r57 = 8
            r42 = r11
            goto L_0x004d
        L_0x0062:
            if (r57 == 0) goto L_0x0067
            java.lang.String r16 = "duplicate radix specifier"
            goto L_0x000a
        L_0x0067:
            r57 = 10
            r42 = r11
            goto L_0x004d
        L_0x006c:
            if (r57 == 0) goto L_0x0071
            java.lang.String r16 = "duplicate radix specifier"
            goto L_0x000a
        L_0x0071:
            r57 = 16
            r42 = r11
            goto L_0x004d
        L_0x0076:
            if (r56 == 0) goto L_0x0084
            r3 = 32
            r0 = r56
            if (r0 != r3) goto L_0x0081
            java.lang.String r16 = "non-prefix exactness specifier"
            goto L_0x000a
        L_0x0081:
            java.lang.String r16 = "duplicate exactness specifier"
            goto L_0x000a
        L_0x0084:
            r56 = r17
            r42 = r11
            goto L_0x004d
        L_0x0089:
            int r3 = r52 * 10
            int r52 = r3 + r23
            r0 = r25
            if (r11 < r0) goto L_0x0095
            java.lang.String r16 = "missing letter after '#'"
            goto L_0x000a
        L_0x0095:
            int r42 = r11 + 1
            char r17 = r53[r11]
            r11 = r42
            goto L_0x0029
        L_0x009c:
            r3 = 2
            r0 = r52
            if (r0 < r3) goto L_0x00a7
            r3 = 35
            r0 = r52
            if (r0 <= r3) goto L_0x00ab
        L_0x00a7:
            java.lang.String r16 = "invalid radix specifier"
            goto L_0x000a
        L_0x00ab:
            r57 = r52
            r42 = r11
            goto L_0x004d
        L_0x00b0:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r5 = "unknown modifier '#"
            java.lang.StringBuilder r3 = r3.append(r5)
            r0 = r17
            java.lang.StringBuilder r3 = r3.append(r0)
            r5 = 39
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r16 = r3.toString()
            goto L_0x000a
        L_0x00cd:
            int r11 = r42 + 1
            char r17 = r53[r42]
            r42 = r11
            goto L_0x000f
        L_0x00d5:
            if (r56 != 0) goto L_0x00d9
            r56 = 32
        L_0x00d9:
            if (r57 != 0) goto L_0x00e3
            r30 = r55
        L_0x00dd:
            int r30 = r30 + -1
            if (r30 >= 0) goto L_0x0108
            r57 = 10
        L_0x00e3:
            r3 = 45
            r0 = r17
            if (r0 != r3) goto L_0x0113
            r7 = 1
        L_0x00ea:
            r40 = r7
            r3 = 45
            r0 = r17
            if (r0 == r3) goto L_0x00f8
            r3 = 43
            r0 = r17
            if (r0 != r3) goto L_0x0115
        L_0x00f8:
            r49 = 1
        L_0x00fa:
            if (r49 == 0) goto L_0x051a
            r0 = r42
            r1 = r25
            if (r0 < r1) goto L_0x0118
            java.lang.String r16 = "no digits following sign"
            r11 = r42
            goto L_0x000a
        L_0x0108:
            int r3 = r54 + r30
            char r3 = r53[r3]
            r5 = 46
            if (r3 != r5) goto L_0x00dd
            r57 = 10
            goto L_0x00e3
        L_0x0113:
            r7 = 0
            goto L_0x00ea
        L_0x0115:
            r49 = 0
            goto L_0x00fa
        L_0x0118:
            int r11 = r42 + 1
            char r17 = r53[r42]
        L_0x011c:
            r3 = 105(0x69, float:1.47E-43)
            r0 = r17
            if (r0 == r3) goto L_0x0128
            r3 = 73
            r0 = r17
            if (r0 != r3) goto L_0x0175
        L_0x0128:
            r0 = r25
            if (r11 != r0) goto L_0x0175
            int r3 = r11 + -2
            r0 = r54
            if (r0 != r3) goto L_0x0175
            r3 = r58 & 1
            if (r3 == 0) goto L_0x0175
            char r48 = r53[r54]
            r3 = 43
            r0 = r48
            if (r0 == r3) goto L_0x0148
            r3 = 45
            r0 = r48
            if (r0 == r3) goto L_0x0148
            java.lang.String r16 = "no digits"
            goto L_0x000a
        L_0x0148:
            r3 = 105(0x69, float:1.47E-43)
            r0 = r56
            if (r0 == r3) goto L_0x0154
            r3 = 73
            r0 = r56
            if (r0 != r3) goto L_0x0166
        L_0x0154:
            gnu.math.DComplex r16 = new gnu.math.DComplex
            r12 = 0
            if (r7 == 0) goto L_0x0163
            r5 = -4616189618054758400(0xbff0000000000000, double:-1.0)
        L_0x015c:
            r0 = r16
            r0.<init>(r12, r5)
            goto L_0x000a
        L_0x0163:
            r5 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            goto L_0x015c
        L_0x0166:
            if (r7 == 0) goto L_0x0170
            gnu.math.CComplex r3 = gnu.math.Complex.imMinusOne()
        L_0x016c:
            r16 = r3
            goto L_0x000a
        L_0x0170:
            gnu.math.CComplex r3 = gnu.math.Complex.imOne()
            goto L_0x016c
        L_0x0175:
            int r47 = r11 + -1
            r29 = 0
            r28 = -1
            r4 = -1
            r22 = -1
            r19 = 0
            r51 = 0
            r39 = 0
            r8 = 0
        L_0x0186:
            r0 = r17
            r1 = r57
            int r24 = java.lang.Character.digit(r0, r1)
            if (r24 < 0) goto L_0x01e3
            if (r29 == 0) goto L_0x0198
            if (r22 >= 0) goto L_0x0198
            java.lang.String r16 = "digit after '#' in number"
            goto L_0x000a
        L_0x0198:
            if (r4 >= 0) goto L_0x019c
            int r4 = r11 + -1
        L_0x019c:
            r0 = r57
            long r5 = (long) r0
            long r5 = r5 * r8
            r0 = r24
            long r12 = (long) r0
            long r8 = r5 + r12
        L_0x01a5:
            r0 = r25
            if (r11 != r0) goto L_0x028d
        L_0x01a9:
            r33 = 0
            if (r4 >= 0) goto L_0x0516
            if (r49 == 0) goto L_0x01dd
            int r3 = r11 + 4
            r0 = r25
            if (r3 >= r0) goto L_0x01dd
            int r3 = r11 + 3
            char r3 = r53[r3]
            r5 = 46
            if (r3 != r5) goto L_0x01dd
            int r3 = r11 + 4
            char r3 = r53[r3]
            r5 = 48
            if (r3 != r5) goto L_0x01dd
            char r3 = r53[r11]
            r5 = 105(0x69, float:1.47E-43)
            if (r3 != r5) goto L_0x0295
            int r3 = r11 + 1
            char r3 = r53[r3]
            r5 = 110(0x6e, float:1.54E-43)
            if (r3 != r5) goto L_0x0295
            int r3 = r11 + 2
            char r3 = r53[r3]
            r5 = 102(0x66, float:1.43E-43)
            if (r3 != r5) goto L_0x0295
            r33 = 105(0x69, float:1.47E-43)
        L_0x01dd:
            if (r33 != 0) goto L_0x02af
            java.lang.String r16 = "no digits"
            goto L_0x000a
        L_0x01e3:
            switch(r17) {
                case 46: goto L_0x01e9;
                case 47: goto L_0x0265;
                case 68: goto L_0x01fc;
                case 69: goto L_0x01fc;
                case 70: goto L_0x01fc;
                case 76: goto L_0x01fc;
                case 83: goto L_0x01fc;
                case 100: goto L_0x01fc;
                case 101: goto L_0x01fc;
                case 102: goto L_0x01fc;
                case 108: goto L_0x01fc;
                case 115: goto L_0x01fc;
                default: goto L_0x01e6;
            }
        L_0x01e6:
            int r11 = r11 + -1
            goto L_0x01a9
        L_0x01e9:
            if (r22 < 0) goto L_0x01ef
            java.lang.String r16 = "duplicate '.' in number"
            goto L_0x000a
        L_0x01ef:
            r3 = 10
            r0 = r57
            if (r0 == r3) goto L_0x01f9
            java.lang.String r16 = "'.' in non-decimal number"
            goto L_0x000a
        L_0x01f9:
            int r22 = r11 + -1
            goto L_0x01a5
        L_0x01fc:
            r0 = r25
            if (r11 == r0) goto L_0x0206
            r3 = 10
            r0 = r57
            if (r0 == r3) goto L_0x0209
        L_0x0206:
            int r11 = r11 + -1
            goto L_0x01a9
        L_0x0209:
            char r36 = r53[r11]
            int r27 = r11 + -1
            r3 = 43
            r0 = r36
            if (r0 == r3) goto L_0x0219
            r3 = 45
            r0 = r36
            if (r0 != r3) goto L_0x022d
        L_0x0219:
            int r11 = r11 + 1
            r0 = r25
            if (r11 >= r0) goto L_0x0229
            char r3 = r53[r11]
            r5 = 10
            int r3 = java.lang.Character.digit(r3, r5)
            if (r3 >= 0) goto L_0x023b
        L_0x0229:
            java.lang.String r16 = "missing exponent digits"
            goto L_0x000a
        L_0x022d:
            r3 = 10
            r0 = r36
            int r3 = java.lang.Character.digit(r0, r3)
            if (r3 >= 0) goto L_0x023b
            int r11 = r11 + -1
            goto L_0x01a9
        L_0x023b:
            if (r28 < 0) goto L_0x0241
            java.lang.String r16 = "duplicate exponent"
            goto L_0x000a
        L_0x0241:
            r3 = 10
            r0 = r57
            if (r0 == r3) goto L_0x024b
            java.lang.String r16 = "exponent in non-decimal number"
            goto L_0x000a
        L_0x024b:
            if (r4 >= 0) goto L_0x0251
            java.lang.String r16 = "mantissa with no digits"
            goto L_0x000a
        L_0x0251:
            r28 = r27
        L_0x0253:
            int r11 = r11 + 1
            r0 = r25
            if (r11 >= r0) goto L_0x01a9
            char r3 = r53[r11]
            r5 = 10
            int r3 = java.lang.Character.digit(r3, r5)
            if (r3 >= 0) goto L_0x0253
            goto L_0x01a9
        L_0x0265:
            if (r39 == 0) goto L_0x026b
            java.lang.String r16 = "multiple fraction symbol '/'"
            goto L_0x000a
        L_0x026b:
            if (r4 >= 0) goto L_0x0271
            java.lang.String r16 = "no digits before fraction symbol '/'"
            goto L_0x000a
        L_0x0271:
            if (r28 >= 0) goto L_0x0275
            if (r22 < 0) goto L_0x0279
        L_0x0275:
            java.lang.String r16 = "fraction symbol '/' following exponent or '.'"
            goto L_0x000a
        L_0x0279:
            int r5 = r11 - r4
            r3 = r53
            r6 = r57
            gnu.math.IntNum r39 = valueOf(r3, r4, r5, r6, r7, r8)
            r4 = -1
            r8 = 0
            r7 = 0
            r29 = 0
            r51 = 0
            goto L_0x01a5
        L_0x028d:
            int r42 = r11 + 1
            char r17 = r53[r11]
            r11 = r42
            goto L_0x0186
        L_0x0295:
            char r3 = r53[r11]
            r5 = 110(0x6e, float:1.54E-43)
            if (r3 != r5) goto L_0x01dd
            int r3 = r11 + 1
            char r3 = r53[r3]
            r5 = 97
            if (r3 != r5) goto L_0x01dd
            int r3 = r11 + 2
            char r3 = r53[r3]
            r5 = 110(0x6e, float:1.54E-43)
            if (r3 != r5) goto L_0x01dd
            r33 = 110(0x6e, float:1.54E-43)
            goto L_0x01dd
        L_0x02af:
            int r11 = r11 + 5
            r42 = r11
        L_0x02b3:
            if (r29 != 0) goto L_0x02b7
            if (r51 == 0) goto L_0x02b7
        L_0x02b7:
            r3 = 105(0x69, float:1.47E-43)
            r0 = r56
            if (r0 == r3) goto L_0x02cb
            r3 = 73
            r0 = r56
            if (r0 == r3) goto L_0x02cb
            r3 = 32
            r0 = r56
            if (r0 != r3) goto L_0x032b
            if (r29 == 0) goto L_0x032b
        L_0x02cb:
            r32 = 1
        L_0x02cd:
            r37 = 0
            r26 = 0
            if (r33 == 0) goto L_0x0331
            r32 = 1
            r3 = 105(0x69, float:1.47E-43)
            r0 = r33
            if (r0 != r3) goto L_0x032e
            r20 = 9218868437227405312(0x7ff0000000000000, double:Infinity)
        L_0x02dd:
            gnu.math.DFloNum r37 = new gnu.math.DFloNum
            if (r7 == 0) goto L_0x02e6
            r0 = r20
            double r0 = -r0
            r20 = r0
        L_0x02e6:
            r0 = r37
            r1 = r20
            r0.<init>(r1)
        L_0x02ed:
            r3 = 101(0x65, float:1.42E-43)
            r0 = r56
            if (r0 == r3) goto L_0x02f9
            r3 = 69
            r0 = r56
            if (r0 != r3) goto L_0x02fd
        L_0x02f9:
            gnu.math.RatNum r37 = r37.toExact()
        L_0x02fd:
            r0 = r42
            r1 = r25
            if (r0 >= r1) goto L_0x04dc
            int r11 = r42 + 1
            char r17 = r53[r42]
            r3 = 64
            r0 = r17
            if (r0 != r3) goto L_0x0431
            int r12 = r25 - r11
            r14 = 10
            r10 = r53
            r13 = r56
            r15 = r58
            java.lang.Object r16 = parseNumber(r10, r11, r12, r13, r14, r15)
            r0 = r16
            boolean r3 = r0 instanceof java.lang.String
            if (r3 != 0) goto L_0x000a
            r0 = r16
            boolean r3 = r0 instanceof gnu.math.RealNum
            if (r3 != 0) goto L_0x040c
            java.lang.String r16 = "invalid complex polar constant"
            goto L_0x000a
        L_0x032b:
            r32 = 0
            goto L_0x02cd
        L_0x032e:
            r20 = 9221120237041090560(0x7ff8000000000000, double:NaN)
            goto L_0x02dd
        L_0x0331:
            if (r28 >= 0) goto L_0x0335
            if (r22 < 0) goto L_0x039e
        L_0x0335:
            r0 = r22
            if (r4 <= r0) goto L_0x033d
            if (r22 < 0) goto L_0x033d
            r4 = r22
        L_0x033d:
            if (r39 == 0) goto L_0x0345
            java.lang.String r16 = "floating-point number after fraction symbol '/'"
            r11 = r42
            goto L_0x000a
        L_0x0345:
            java.lang.String r50 = new java.lang.String
            int r3 = r42 - r4
            r0 = r50
            r1 = r53
            r0.<init>(r1, r4, r3)
            if (r28 < 0) goto L_0x0388
            char r3 = r53[r28]
            char r26 = java.lang.Character.toLowerCase(r3)
            r3 = 101(0x65, float:1.42E-43)
            r0 = r26
            if (r0 == r3) goto L_0x0388
            int r43 = r28 - r4
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r5 = 0
            r0 = r50
            r1 = r43
            java.lang.String r5 = r0.substring(r5, r1)
            java.lang.StringBuilder r3 = r3.append(r5)
            r5 = 101(0x65, float:1.42E-43)
            java.lang.StringBuilder r3 = r3.append(r5)
            int r5 = r43 + 1
            r0 = r50
            java.lang.String r5 = r0.substring(r5)
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r50 = r3.toString()
        L_0x0388:
            double r20 = gnu.lists.Convert.parseDouble(r50)
            gnu.math.DFloNum r37 = new gnu.math.DFloNum
            if (r7 == 0) goto L_0x0395
            r0 = r20
            double r0 = -r0
            r20 = r0
        L_0x0395:
            r0 = r37
            r1 = r20
            r0.<init>(r1)
            goto L_0x02ed
        L_0x039e:
            int r5 = r42 - r4
            r3 = r53
            r6 = r57
            gnu.math.IntNum r34 = valueOf(r3, r4, r5, r6, r7, r8)
            if (r39 != 0) goto L_0x03c9
            r37 = r34
            r38 = r37
        L_0x03ae:
            if (r32 == 0) goto L_0x0512
            boolean r3 = r38.isExact()
            if (r3 == 0) goto L_0x0512
            gnu.math.DFloNum r37 = new gnu.math.DFloNum
            if (r40 == 0) goto L_0x0407
            boolean r3 = r38.isZero()
            if (r3 == 0) goto L_0x0407
            r5 = -9223372036854775808
        L_0x03c2:
            r0 = r37
            r0.<init>(r5)
            goto L_0x02ed
        L_0x03c9:
            boolean r3 = r34.isZero()
            if (r3 == 0) goto L_0x03fc
            boolean r41 = r39.isZero()
            if (r32 == 0) goto L_0x03eb
            gnu.math.DFloNum r37 = new gnu.math.DFloNum
            if (r41 == 0) goto L_0x03e3
            r5 = 9221120237041090560(0x7ff8000000000000, double:NaN)
        L_0x03db:
            r0 = r37
            r0.<init>(r5)
        L_0x03e0:
            r38 = r37
            goto L_0x03ae
        L_0x03e3:
            if (r40 == 0) goto L_0x03e8
            r5 = -4503599627370496(0xfff0000000000000, double:-Infinity)
            goto L_0x03db
        L_0x03e8:
            r5 = 9218868437227405312(0x7ff0000000000000, double:Infinity)
            goto L_0x03db
        L_0x03eb:
            if (r41 == 0) goto L_0x03f3
            java.lang.String r16 = "0/0 is undefined"
            r11 = r42
            goto L_0x000a
        L_0x03f3:
            r0 = r39
            r1 = r34
            gnu.math.RatNum r37 = gnu.math.RatNum.make(r0, r1)
            goto L_0x03e0
        L_0x03fc:
            r0 = r39
            r1 = r34
            gnu.math.RatNum r37 = gnu.math.RatNum.make(r0, r1)
            r38 = r37
            goto L_0x03ae
        L_0x0407:
            double r5 = r38.doubleValue()
            goto L_0x03c2
        L_0x040c:
            r45 = r16
            gnu.math.RealNum r45 = (gnu.math.RealNum) r45
            boolean r3 = r37.isZero()
            if (r3 == 0) goto L_0x0427
            boolean r3 = r45.isExact()
            if (r3 != 0) goto L_0x0427
            gnu.math.DFloNum r16 = new gnu.math.DFloNum
            r5 = 0
            r0 = r16
            r0.<init>(r5)
            goto L_0x000a
        L_0x0427:
            r0 = r37
            r1 = r45
            gnu.math.DComplex r16 = gnu.math.Complex.polar(r0, r1)
            goto L_0x000a
        L_0x0431:
            r3 = 45
            r0 = r17
            if (r0 == r3) goto L_0x043d
            r3 = 43
            r0 = r17
            if (r0 != r3) goto L_0x0498
        L_0x043d:
            int r11 = r11 + -1
            int r12 = r25 - r11
            r14 = 10
            r10 = r53
            r13 = r56
            r15 = r58
            java.lang.Object r31 = parseNumber(r10, r11, r12, r13, r14, r15)
            r0 = r31
            boolean r3 = r0 instanceof java.lang.String
            if (r3 == 0) goto L_0x0457
            r16 = r31
            goto L_0x000a
        L_0x0457:
            r0 = r31
            boolean r3 = r0 instanceof gnu.math.Complex
            if (r3 != 0) goto L_0x047a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r5 = "invalid numeric constant ("
            java.lang.StringBuilder r3 = r3.append(r5)
            r0 = r31
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r5 = ")"
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r16 = r3.toString()
            goto L_0x000a
        L_0x047a:
            r18 = r31
            gnu.math.Complex r18 = (gnu.math.Complex) r18
            gnu.math.RealNum r46 = r18.re()
            boolean r3 = r46.isZero()
            if (r3 != 0) goto L_0x048c
            java.lang.String r16 = "invalid numeric constant"
            goto L_0x000a
        L_0x048c:
            gnu.math.RealNum r3 = r18.im()
            r0 = r37
            gnu.math.Complex r16 = gnu.math.Complex.make(r0, r3)
            goto L_0x000a
        L_0x0498:
            r35 = 0
        L_0x049a:
            boolean r3 = java.lang.Character.isLetter(r17)
            if (r3 != 0) goto L_0x04bf
            int r11 = r11 + -1
        L_0x04a2:
            r3 = 1
            r0 = r35
            if (r0 != r3) goto L_0x04d8
            int r3 = r11 + -1
            char r44 = r53[r3]
            r3 = 105(0x69, float:1.47E-43)
            r0 = r44
            if (r0 == r3) goto L_0x04b7
            r3 = 73
            r0 = r44
            if (r0 != r3) goto L_0x04d8
        L_0x04b7:
            r0 = r25
            if (r11 >= r0) goto L_0x04cc
            java.lang.String r16 = "junk after imaginary suffix 'i'"
            goto L_0x000a
        L_0x04bf:
            int r35 = r35 + 1
            r0 = r25
            if (r11 == r0) goto L_0x04a2
            int r42 = r11 + 1
            char r17 = r53[r11]
            r11 = r42
            goto L_0x049a
        L_0x04cc:
            gnu.math.IntNum r3 = gnu.math.IntNum.zero()
            r0 = r37
            gnu.math.Complex r16 = gnu.math.Complex.make(r3, r0)
            goto L_0x000a
        L_0x04d8:
            java.lang.String r16 = "excess junk after number"
            goto L_0x000a
        L_0x04dc:
            r0 = r37
            boolean r3 = r0 instanceof gnu.math.DFloNum
            if (r3 == 0) goto L_0x04f1
            if (r26 <= 0) goto L_0x04f1
            r3 = 101(0x65, float:1.42E-43)
            r0 = r26
            if (r0 == r3) goto L_0x04f1
            double r20 = r37.doubleValue()
            switch(r26) {
                case 100: goto L_0x0502;
                case 102: goto L_0x04f7;
                case 108: goto L_0x050a;
                case 115: goto L_0x04f7;
                default: goto L_0x04f1;
            }
        L_0x04f1:
            r11 = r42
            r16 = r37
            goto L_0x000a
        L_0x04f7:
            r0 = r20
            float r3 = (float) r0
            java.lang.Float r16 = java.lang.Float.valueOf(r3)
            r11 = r42
            goto L_0x000a
        L_0x0502:
            java.lang.Double r16 = java.lang.Double.valueOf(r20)
            r11 = r42
            goto L_0x000a
        L_0x050a:
            java.math.BigDecimal r16 = java.math.BigDecimal.valueOf(r20)
            r11 = r42
            goto L_0x000a
        L_0x0512:
            r37 = r38
            goto L_0x02ed
        L_0x0516:
            r42 = r11
            goto L_0x02b3
        L_0x051a:
            r11 = r42
            goto L_0x011c
        */
        throw new UnsupportedOperationException("Method not decompiled: gnu.kawa.lispexpr.LispReader.parseNumber(char[], int, int, char, int, int):java.lang.Object");
    }

    private static IntNum valueOf(char[] buffer, int digits_start, int number_of_digits, int radix, boolean negative, long lvalue) {
        if (number_of_digits + radix > 28) {
            return IntNum.valueOf(buffer, digits_start, number_of_digits, radix, negative);
        }
        if (negative) {
            lvalue = -lvalue;
        }
        return IntNum.make(lvalue);
    }

    public int readEscape() throws IOException, SyntaxException {
        int c = read();
        if (c >= 0) {
            return readEscape(c);
        }
        eofError("unexpected EOF in character literal");
        return -1;
    }

    /* JADX WARNING: Removed duplicated region for block: B:45:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00a5 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00a9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int readEscape(int r11) throws java.io.IOException, gnu.text.SyntaxException {
        /*
            r10 = this;
            r9 = 32
            r8 = 9
            r5 = -1
            r4 = 63
            r7 = 10
            char r6 = (char) r11
            switch(r6) {
                case 9: goto L_0x0030;
                case 10: goto L_0x0030;
                case 13: goto L_0x0030;
                case 32: goto L_0x0030;
                case 34: goto L_0x0026;
                case 48: goto L_0x00ad;
                case 49: goto L_0x00ad;
                case 50: goto L_0x00ad;
                case 51: goto L_0x00ad;
                case 52: goto L_0x00ad;
                case 53: goto L_0x00ad;
                case 54: goto L_0x00ad;
                case 55: goto L_0x00ad;
                case 67: goto L_0x0088;
                case 77: goto L_0x006b;
                case 88: goto L_0x00f2;
                case 92: goto L_0x0029;
                case 94: goto L_0x0097;
                case 97: goto L_0x000f;
                case 98: goto L_0x0011;
                case 101: goto L_0x0023;
                case 102: goto L_0x001d;
                case 110: goto L_0x0017;
                case 114: goto L_0x0020;
                case 116: goto L_0x0014;
                case 117: goto L_0x00ce;
                case 118: goto L_0x001a;
                case 120: goto L_0x00f2;
                default: goto L_0x000d;
            }
        L_0x000d:
            r4 = r11
        L_0x000e:
            return r4
        L_0x000f:
            r11 = 7
            goto L_0x000d
        L_0x0011:
            r11 = 8
            goto L_0x000d
        L_0x0014:
            r11 = 9
            goto L_0x000d
        L_0x0017:
            r11 = 10
            goto L_0x000d
        L_0x001a:
            r11 = 11
            goto L_0x000d
        L_0x001d:
            r11 = 12
            goto L_0x000d
        L_0x0020:
            r11 = 13
            goto L_0x000d
        L_0x0023:
            r11 = 27
            goto L_0x000d
        L_0x0026:
            r11 = 34
            goto L_0x000d
        L_0x0029:
            r11 = 92
            goto L_0x000d
        L_0x002c:
            int r11 = r10.read()
        L_0x0030:
            if (r11 >= 0) goto L_0x0039
            java.lang.String r4 = "unexpected EOF in literal"
            r10.eofError(r4)
            r4 = r5
            goto L_0x000e
        L_0x0039:
            if (r11 != r7) goto L_0x004a
        L_0x003b:
            if (r11 != r7) goto L_0x000d
        L_0x003d:
            int r11 = r10.read()
            if (r11 >= 0) goto L_0x0062
            java.lang.String r4 = "unexpected EOF in literal"
            r10.eofError(r4)
            r4 = r5
            goto L_0x000e
        L_0x004a:
            r4 = 13
            if (r11 != r4) goto L_0x005a
            int r4 = r10.peek()
            if (r4 != r7) goto L_0x0057
            r10.skip()
        L_0x0057:
            r11 = 10
            goto L_0x003b
        L_0x005a:
            if (r11 == r9) goto L_0x002c
            if (r11 == r8) goto L_0x002c
            r10.unread(r11)
            goto L_0x003b
        L_0x0062:
            if (r11 == r9) goto L_0x003d
            if (r11 == r8) goto L_0x003d
            r10.unread(r11)
            r4 = -2
            goto L_0x000e
        L_0x006b:
            int r11 = r10.read()
            r5 = 45
            if (r11 == r5) goto L_0x0079
            java.lang.String r5 = "Invalid escape character syntax"
            r10.error(r5)
            goto L_0x000e
        L_0x0079:
            int r11 = r10.read()
            r4 = 92
            if (r11 != r4) goto L_0x0085
            int r11 = r10.readEscape()
        L_0x0085:
            r4 = r11 | 128(0x80, float:1.794E-43)
            goto L_0x000e
        L_0x0088:
            int r11 = r10.read()
            r5 = 45
            if (r11 == r5) goto L_0x0097
            java.lang.String r5 = "Invalid escape character syntax"
            r10.error(r5)
            goto L_0x000e
        L_0x0097:
            int r11 = r10.read()
            r5 = 92
            if (r11 != r5) goto L_0x00a3
            int r11 = r10.readEscape()
        L_0x00a3:
            if (r11 != r4) goto L_0x00a9
            r4 = 127(0x7f, float:1.78E-43)
            goto L_0x000e
        L_0x00a9:
            r4 = r11 & 159(0x9f, float:2.23E-43)
            goto L_0x000e
        L_0x00ad:
            int r11 = r11 + -48
            r0 = 0
        L_0x00b0:
            int r0 = r0 + 1
            r4 = 3
            if (r0 >= r4) goto L_0x000d
            int r1 = r10.read()
            char r4 = (char) r1
            r5 = 8
            int r3 = java.lang.Character.digit(r4, r5)
            if (r3 < 0) goto L_0x00c7
            int r4 = r11 << 3
            int r11 = r4 + r3
            goto L_0x00b0
        L_0x00c7:
            if (r1 < 0) goto L_0x000d
            r10.unread(r1)
            goto L_0x000d
        L_0x00ce:
            r11 = 0
            r2 = 4
        L_0x00d0:
            int r2 = r2 + -1
            if (r2 < 0) goto L_0x000d
            int r1 = r10.read()
            if (r1 >= 0) goto L_0x00df
            java.lang.String r4 = "premature EOF in \\u escape"
            r10.eofError(r4)
        L_0x00df:
            char r4 = (char) r1
            r5 = 16
            int r3 = java.lang.Character.digit(r4, r5)
            if (r3 >= 0) goto L_0x00ed
            java.lang.String r4 = "non-hex character following \\u"
            r10.error(r4)
        L_0x00ed:
            int r4 = r11 * 16
            int r11 = r4 + r3
            goto L_0x00d0
        L_0x00f2:
            int r4 = r10.readHexEscape()
            goto L_0x000e
        */
        throw new UnsupportedOperationException("Method not decompiled: gnu.kawa.lispexpr.LispReader.readEscape(int):int");
    }

    public int readHexEscape() throws IOException, SyntaxException {
        int d;
        int c = 0;
        while (true) {
            d = read();
            int v = Character.digit((char) d, 16);
            if (v < 0) {
                break;
            }
            c = (c << 4) + v;
        }
        if (d != 59 && d >= 0) {
            unread(d);
        }
        return c;
    }

    public final Object readObject(int c) throws IOException, SyntaxException {
        unread(c);
        return readObject();
    }

    public Object readCommand() throws IOException, SyntaxException {
        return readObject();
    }

    /* access modifiers changed from: protected */
    public Object makeNil() {
        return LList.Empty;
    }

    /* access modifiers changed from: protected */
    public Pair makePair(Object car, int line, int column) {
        return makePair(car, LList.Empty, line, column);
    }

    /* access modifiers changed from: protected */
    public Pair makePair(Object car, Object cdr, int line, int column) {
        String pname = this.port.getName();
        if (pname == null || line < 0) {
            return Pair.make(car, cdr);
        }
        return PairWithPosition.make(car, cdr, pname, line + 1, column + 1);
    }

    /* access modifiers changed from: protected */
    public void setCdr(Object pair, Object cdr) {
        ((Pair) pair).setCdrBackdoor(cdr);
    }

    public static Object readNumberWithRadix(int previous, LispReader reader, int radix) throws IOException, SyntaxException {
        int startPos = reader.tokenBufferLength - previous;
        reader.readToken(reader.read(), 'P', ReadTable.getCurrent());
        int endPos = reader.tokenBufferLength;
        if (startPos == endPos) {
            reader.error("missing numeric token");
            return IntNum.zero();
        }
        Object result = parseNumber(reader.tokenBuffer, startPos, endPos - startPos, 0, radix, 0);
        if (result instanceof String) {
            reader.error((String) result);
            return IntNum.zero();
        } else if (result != null) {
            return result;
        } else {
            reader.error("invalid numeric constant");
            return IntNum.zero();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Character.digit(int, int):int}
     arg types: [char, int]
     candidates:
      ClspMth{java.lang.Character.digit(char, int):int}
      ClspMth{java.lang.Character.digit(int, int):int} */
    public static Object readCharacter(LispReader reader) throws IOException, SyntaxException {
        int ch = reader.read();
        if (ch < 0) {
            reader.eofError("unexpected EOF in character literal");
        }
        int startPos = reader.tokenBufferLength;
        reader.tokenBufferAppend(ch);
        reader.readToken(reader.read(), 'D', ReadTable.getCurrent());
        char[] tokenBuffer = reader.tokenBuffer;
        int length = reader.tokenBufferLength - startPos;
        if (length == 1) {
            return Char.make(tokenBuffer[startPos]);
        }
        String name = new String(tokenBuffer, startPos, length);
        int ch2 = Char.nameToChar(name);
        if (ch2 >= 0) {
            return Char.make(ch2);
        }
        char c = tokenBuffer[startPos];
        if (c == 'x' || c == 'X') {
            int value = 0;
            int i = 1;
            while (i != length) {
                int v = Character.digit(tokenBuffer[startPos + i], 16);
                if (v >= 0 && (value = (value * 16) + v) <= 1114111) {
                    i++;
                }
            }
            return Char.make(value);
        }
        int ch3 = Character.digit((int) c, 8);
        if (ch3 >= 0) {
            int value2 = ch3;
            int i2 = 1;
            while (i2 != length) {
                int ch4 = Character.digit(tokenBuffer[startPos + i2], 8);
                if (ch4 >= 0) {
                    value2 = (value2 * 8) + ch4;
                    i2++;
                }
            }
            return Char.make(value2);
        }
        reader.error("unknown character name: " + name);
        return Char.make(63);
    }

    public static Object readSpecial(LispReader reader) throws IOException, SyntaxException {
        int ch = reader.read();
        if (ch < 0) {
            reader.eofError("unexpected EOF in #! special form");
        }
        if (ch == 47 && reader.getLineNumber() == 0 && reader.getColumnNumber() == 3) {
            ReaderIgnoreRestOfLine.getInstance().read(reader, 35, 1);
            return Values.empty;
        }
        int startPos = reader.tokenBufferLength;
        reader.tokenBufferAppend(ch);
        reader.readToken(reader.read(), 'D', ReadTable.getCurrent());
        String name = new String(reader.tokenBuffer, startPos, reader.tokenBufferLength - startPos);
        if (name.equals("optional")) {
            return Special.optional;
        }
        if (name.equals("rest")) {
            return Special.rest;
        }
        if (name.equals("key")) {
            return Special.key;
        }
        if (name.equals("eof")) {
            return Special.eof;
        }
        if (name.equals("void")) {
            return QuoteExp.voidExp;
        }
        if (name.equals("default")) {
            return Special.dfault;
        }
        if (name.equals("undefined")) {
            return Special.undefined;
        }
        if (name.equals("abstract")) {
            return Special.abstractSpecial;
        }
        if (name.equals("null")) {
            return null;
        }
        reader.error("unknown named constant #!" + name);
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x0060  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0078  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x007e  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0084  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x008a  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0096  */
    /* JADX WARNING: Removed duplicated region for block: B:51:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static gnu.lists.SimpleVector readSimpleVector(gnu.kawa.lispexpr.LispReader r11, char r12) throws java.io.IOException, gnu.text.SyntaxException {
        /*
            r10 = 40
            r9 = 32
            r6 = 0
            r5 = 0
        L_0x0006:
            int r0 = r11.read()
            if (r0 >= 0) goto L_0x0011
            java.lang.String r7 = "unexpected EOF reading uniform vector"
            r11.eofError(r7)
        L_0x0011:
            char r7 = (char) r0
            r8 = 10
            int r1 = java.lang.Character.digit(r7, r8)
            if (r1 >= 0) goto L_0x0036
            r7 = 8
            if (r5 == r7) goto L_0x0028
            r7 = 16
            if (r5 == r7) goto L_0x0028
            if (r5 == r9) goto L_0x0028
            r7 = 64
            if (r5 != r7) goto L_0x0030
        L_0x0028:
            r7 = 70
            if (r12 != r7) goto L_0x002e
            if (r5 < r9) goto L_0x0030
        L_0x002e:
            if (r0 == r10) goto L_0x003b
        L_0x0030:
            java.lang.String r7 = "invalid uniform vector syntax"
            r11.error(r7)
        L_0x0035:
            return r6
        L_0x0036:
            int r7 = r5 * 10
            int r5 = r7 + r1
            goto L_0x0006
        L_0x003b:
            r7 = -1
            r8 = 41
            java.lang.Object r3 = gnu.kawa.lispexpr.ReaderParens.readList(r11, r10, r7, r8)
            r7 = 0
            int r2 = gnu.lists.LList.listLength(r3, r7)
            if (r2 >= 0) goto L_0x004f
            java.lang.String r7 = "invalid elements in uniform vector syntax"
            r11.error(r7)
            goto L_0x0035
        L_0x004f:
            r4 = r3
            gnu.lists.Sequence r4 = (gnu.lists.Sequence) r4
            switch(r12) {
                case 70: goto L_0x0056;
                case 83: goto L_0x0059;
                case 85: goto L_0x005c;
                default: goto L_0x0055;
            }
        L_0x0055:
            goto L_0x0035
        L_0x0056:
            switch(r5) {
                case 32: goto L_0x0066;
                case 64: goto L_0x006c;
                default: goto L_0x0059;
            }
        L_0x0059:
            switch(r5) {
                case 8: goto L_0x0072;
                case 16: goto L_0x0078;
                case 32: goto L_0x007e;
                case 64: goto L_0x0084;
                default: goto L_0x005c;
            }
        L_0x005c:
            switch(r5) {
                case 8: goto L_0x0060;
                case 16: goto L_0x008a;
                case 32: goto L_0x0090;
                case 64: goto L_0x0096;
                default: goto L_0x005f;
            }
        L_0x005f:
            goto L_0x0035
        L_0x0060:
            gnu.lists.U8Vector r6 = new gnu.lists.U8Vector
            r6.<init>(r4)
            goto L_0x0035
        L_0x0066:
            gnu.lists.F32Vector r6 = new gnu.lists.F32Vector
            r6.<init>(r4)
            goto L_0x0035
        L_0x006c:
            gnu.lists.F64Vector r6 = new gnu.lists.F64Vector
            r6.<init>(r4)
            goto L_0x0035
        L_0x0072:
            gnu.lists.S8Vector r6 = new gnu.lists.S8Vector
            r6.<init>(r4)
            goto L_0x0035
        L_0x0078:
            gnu.lists.S16Vector r6 = new gnu.lists.S16Vector
            r6.<init>(r4)
            goto L_0x0035
        L_0x007e:
            gnu.lists.S32Vector r6 = new gnu.lists.S32Vector
            r6.<init>(r4)
            goto L_0x0035
        L_0x0084:
            gnu.lists.S64Vector r6 = new gnu.lists.S64Vector
            r6.<init>(r4)
            goto L_0x0035
        L_0x008a:
            gnu.lists.U16Vector r6 = new gnu.lists.U16Vector
            r6.<init>(r4)
            goto L_0x0035
        L_0x0090:
            gnu.lists.U32Vector r6 = new gnu.lists.U32Vector
            r6.<init>(r4)
            goto L_0x0035
        L_0x0096:
            gnu.lists.U64Vector r6 = new gnu.lists.U64Vector
            r6.<init>(r4)
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: gnu.kawa.lispexpr.LispReader.readSimpleVector(gnu.kawa.lispexpr.LispReader, char):gnu.lists.SimpleVector");
    }
}
