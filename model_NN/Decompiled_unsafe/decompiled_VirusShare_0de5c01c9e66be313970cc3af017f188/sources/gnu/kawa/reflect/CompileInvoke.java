package gnu.kawa.reflect;

import gnu.bytecode.ClassType;
import gnu.bytecode.ObjectType;
import gnu.bytecode.Type;
import gnu.expr.ClassExp;
import gnu.expr.Expression;
import gnu.expr.InlineCalls;
import gnu.expr.Keyword;
import gnu.expr.PrimProcedure;
import gnu.mapping.MethodProc;

public class CompileInvoke {
    /* JADX WARN: Failed to insert an additional move for type inference into block B:312:0x04ca */
    /* JADX WARN: Type inference failed for: r21v3 */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: gnu.expr.InlineCalls.visit(gnu.expr.Expression, gnu.bytecode.Type):gnu.expr.Expression
     arg types: [gnu.expr.Expression, gnu.bytecode.PrimType]
     candidates:
      gnu.expr.InlineCalls.visit(gnu.expr.Expression, java.lang.Object):java.lang.Object
      gnu.expr.ExpVisitor.visit(gnu.expr.Expression, java.lang.Object):R
      gnu.expr.InlineCalls.visit(gnu.expr.Expression, gnu.bytecode.Type):gnu.expr.Expression */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: gnu.expr.InlineCalls.visit(gnu.expr.Expression, gnu.bytecode.Type):gnu.expr.Expression
     arg types: [gnu.expr.ApplyExp, gnu.bytecode.ObjectType]
     candidates:
      gnu.expr.InlineCalls.visit(gnu.expr.Expression, java.lang.Object):java.lang.Object
      gnu.expr.ExpVisitor.visit(gnu.expr.Expression, java.lang.Object):R
      gnu.expr.InlineCalls.visit(gnu.expr.Expression, gnu.bytecode.Type):gnu.expr.Expression */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: gnu.expr.InlineCalls.visit(gnu.expr.Expression, gnu.bytecode.Type):gnu.expr.Expression
     arg types: [gnu.expr.ApplyExp, ?[OBJECT, ARRAY]]
     candidates:
      gnu.expr.InlineCalls.visit(gnu.expr.Expression, java.lang.Object):java.lang.Object
      gnu.expr.ExpVisitor.visit(gnu.expr.Expression, java.lang.Object):R
      gnu.expr.InlineCalls.visit(gnu.expr.Expression, gnu.bytecode.Type):gnu.expr.Expression */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: gnu.expr.InlineCalls.visit(gnu.expr.Expression, gnu.bytecode.Type):gnu.expr.Expression
     arg types: [gnu.expr.Expression, gnu.bytecode.ObjectType]
     candidates:
      gnu.expr.InlineCalls.visit(gnu.expr.Expression, java.lang.Object):java.lang.Object
      gnu.expr.ExpVisitor.visit(gnu.expr.Expression, java.lang.Object):R
      gnu.expr.InlineCalls.visit(gnu.expr.Expression, gnu.bytecode.Type):gnu.expr.Expression */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: gnu.expr.InlineCalls.visit(gnu.expr.Expression, gnu.bytecode.Type):gnu.expr.Expression
     arg types: [gnu.expr.ApplyExp, gnu.bytecode.Type]
     candidates:
      gnu.expr.InlineCalls.visit(gnu.expr.Expression, java.lang.Object):java.lang.Object
      gnu.expr.ExpVisitor.visit(gnu.expr.Expression, java.lang.Object):R
      gnu.expr.InlineCalls.visit(gnu.expr.Expression, gnu.bytecode.Type):gnu.expr.Expression */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x0299, code lost:
        if ((gnu.kawa.reflect.ClassMethods.selectApplicable(r3, new gnu.bytecode.Type[]{gnu.expr.Compilation.typeClassType}) >> 32) == 1) goto L_0x029b;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static gnu.expr.Expression validateApplyInvoke(gnu.expr.ApplyExp r69, gnu.expr.InlineCalls r70, gnu.bytecode.Type r71, gnu.mapping.Procedure r72) {
        /*
            r35 = r72
            gnu.kawa.reflect.Invoke r35 = (gnu.kawa.reflect.Invoke) r35
            r0 = r35
            char r0 = r0.kind
            r39 = r0
            gnu.expr.Compilation r24 = r70.getCompilation()
            gnu.expr.Expression[] r5 = r69.getArgs()
            int r0 = r5.length
            r48 = r0
            r0 = r24
            boolean r9 = r0.mustCompile
            if (r9 == 0) goto L_0x002e
            if (r48 == 0) goto L_0x002e
            r9 = 86
            r0 = r39
            if (r0 == r9) goto L_0x0029
            r9 = 42
            r0 = r39
            if (r0 != r9) goto L_0x0032
        L_0x0029:
            r9 = 1
            r0 = r48
            if (r0 != r9) goto L_0x0032
        L_0x002e:
            r69.visitArgs(r70)
        L_0x0031:
            return r69
        L_0x0032:
            r9 = 0
            r9 = r5[r9]
            r10 = 0
            r0 = r70
            gnu.expr.Expression r19 = r0.visit(r9, r10)
            r9 = 0
            r5[r9] = r19
            r9 = 86
            r0 = r39
            if (r0 == r9) goto L_0x004b
            r9 = 42
            r0 = r39
            if (r0 != r9) goto L_0x0107
        L_0x004b:
            gnu.bytecode.Type r66 = r19.getType()
        L_0x004f:
            r0 = r66
            boolean r9 = r0 instanceof gnu.expr.PairClassType
            if (r9 == 0) goto L_0x0113
            r9 = 78
            r0 = r39
            if (r0 != r9) goto L_0x0113
            gnu.expr.PairClassType r66 = (gnu.expr.PairClassType) r66
            r0 = r66
            gnu.bytecode.ClassType r0 = r0.instanceType
            r65 = r0
        L_0x0063:
            r0 = r39
            java.lang.String r47 = getMethodName(r5, r0)
            r9 = 86
            r0 = r39
            if (r0 == r9) goto L_0x0075
            r9 = 42
            r0 = r39
            if (r0 != r9) goto L_0x0123
        L_0x0075:
            int r6 = r48 + -1
            r7 = 2
            r8 = 0
        L_0x0079:
            r9 = 78
            r0 = r39
            if (r0 != r9) goto L_0x0222
            r0 = r65
            boolean r9 = r0 instanceof gnu.bytecode.ArrayType
            if (r9 == 0) goto L_0x0222
            r21 = r65
            gnu.bytecode.ArrayType r21 = (gnu.bytecode.ArrayType) r21
            gnu.bytecode.Type r29 = r21.getComponentType()
            r60 = 0
            r42 = 0
            int r9 = r5.length
            r10 = 3
            if (r9 < r10) goto L_0x00ca
            r9 = 1
            r9 = r5[r9]
            boolean r9 = r9 instanceof gnu.expr.QuoteExp
            if (r9 == 0) goto L_0x00ca
            r9 = 1
            r9 = r5[r9]
            gnu.expr.QuoteExp r9 = (gnu.expr.QuoteExp) r9
            java.lang.Object r20 = r9.getValue()
            r0 = r20
            boolean r9 = r0 instanceof gnu.expr.Keyword
            if (r9 == 0) goto L_0x00ca
            java.lang.String r9 = "length"
            gnu.expr.Keyword r20 = (gnu.expr.Keyword) r20
            java.lang.String r47 = r20.getName()
            r0 = r47
            boolean r9 = r9.equals(r0)
            if (r9 != 0) goto L_0x00c5
            java.lang.String r9 = "size"
            r0 = r47
            boolean r9 = r9.equals(r0)
            if (r9 == 0) goto L_0x00ca
        L_0x00c5:
            r9 = 2
            r60 = r5[r9]
            r42 = 1
        L_0x00ca:
            if (r60 != 0) goto L_0x00d8
            java.lang.Integer r9 = new java.lang.Integer
            int r10 = r5.length
            int r10 = r10 + -1
            r9.<init>(r10)
            gnu.expr.QuoteExp r60 = gnu.expr.QuoteExp.getInstance(r9)
        L_0x00d8:
            gnu.bytecode.PrimType r9 = gnu.bytecode.Type.intType
            r0 = r70
            r1 = r60
            gnu.expr.Expression r60 = r0.visit(r1, r9)
            gnu.expr.ApplyExp r17 = new gnu.expr.ApplyExp
            gnu.kawa.reflect.ArrayNew r9 = new gnu.kawa.reflect.ArrayNew
            r0 = r29
            r9.<init>(r0)
            r10 = 1
            gnu.expr.Expression[] r10 = new gnu.expr.Expression[r10]
            r11 = 0
            r10[r11] = r60
            r0 = r17
            r0.<init>(r9, r10)
            r0 = r17
            r1 = r21
            r0.setType(r1)
            if (r42 == 0) goto L_0x0152
            int r9 = r5.length
            r10 = 3
            if (r9 != r10) goto L_0x0152
            r69 = r17
            goto L_0x0031
        L_0x0107:
            r0 = r35
            gnu.expr.Language r9 = r0.language
            r0 = r19
            gnu.bytecode.Type r66 = r9.getTypeFor(r0)
            goto L_0x004f
        L_0x0113:
            r0 = r66
            boolean r9 = r0 instanceof gnu.bytecode.ObjectType
            if (r9 == 0) goto L_0x011f
            r65 = r66
            gnu.bytecode.ObjectType r65 = (gnu.bytecode.ObjectType) r65
            goto L_0x0063
        L_0x011f:
            r65 = 0
            goto L_0x0063
        L_0x0123:
            r9 = 78
            r0 = r39
            if (r0 != r9) goto L_0x012f
            r6 = r48
            r7 = 0
            r8 = -1
            goto L_0x0079
        L_0x012f:
            r9 = 83
            r0 = r39
            if (r0 == r9) goto L_0x013b
            r9 = 115(0x73, float:1.61E-43)
            r0 = r39
            if (r0 != r9) goto L_0x0141
        L_0x013b:
            int r6 = r48 + -2
            r7 = 2
            r8 = -1
            goto L_0x0079
        L_0x0141:
            r9 = 80
            r0 = r39
            if (r0 != r9) goto L_0x014d
            int r6 = r48 + -2
            r7 = 3
            r8 = 1
            goto L_0x0079
        L_0x014d:
            r69.visitArgs(r70)
            goto L_0x0031
        L_0x0152:
            gnu.expr.LetExp r43 = new gnu.expr.LetExp
            r9 = 1
            gnu.expr.Expression[] r9 = new gnu.expr.Expression[r9]
            r10 = 0
            r9[r10] = r17
            r0 = r43
            r0.<init>(r9)
            r9 = 0
            java.lang.String r9 = (java.lang.String) r9
            r0 = r43
            r1 = r21
            gnu.expr.Declaration r15 = r0.addDeclaration(r9, r1)
            r0 = r17
            r15.noteValue(r0)
            gnu.expr.BeginExp r22 = new gnu.expr.BeginExp
            r22.<init>()
            r34 = 0
            if (r42 == 0) goto L_0x01e6
            r32 = 3
        L_0x017a:
            int r9 = r5.length
            r0 = r32
            if (r0 >= r9) goto L_0x020e
            r18 = r5[r32]
            if (r42 == 0) goto L_0x01aa
            int r9 = r32 + 1
            int r10 = r5.length
            if (r9 >= r10) goto L_0x01aa
            r0 = r18
            boolean r9 = r0 instanceof gnu.expr.QuoteExp
            if (r9 == 0) goto L_0x01aa
            r9 = r18
            gnu.expr.QuoteExp r9 = (gnu.expr.QuoteExp) r9
            java.lang.Object r37 = r9.getValue()
            r0 = r37
            boolean r9 = r0 instanceof gnu.expr.Keyword
            if (r9 == 0) goto L_0x01aa
            gnu.expr.Keyword r37 = (gnu.expr.Keyword) r37
            java.lang.String r40 = r37.getName()
            int r34 = java.lang.Integer.parseInt(r40)     // Catch:{ Throwable -> 0x01e9 }
            int r32 = r32 + 1
            r18 = r5[r32]     // Catch:{ Throwable -> 0x01e9 }
        L_0x01aa:
            r0 = r70
            r1 = r18
            r2 = r29
            gnu.expr.Expression r18 = r0.visit(r1, r2)
            gnu.expr.ApplyExp r9 = new gnu.expr.ApplyExp
            gnu.kawa.reflect.ArraySet r10 = new gnu.kawa.reflect.ArraySet
            r0 = r29
            r10.<init>(r0)
            r11 = 3
            gnu.expr.Expression[] r11 = new gnu.expr.Expression[r11]
            r12 = 0
            gnu.expr.ReferenceExp r13 = new gnu.expr.ReferenceExp
            r13.<init>(r15)
            r11[r12] = r13
            r12 = 1
            java.lang.Integer r13 = new java.lang.Integer
            r0 = r34
            r13.<init>(r0)
            gnu.expr.QuoteExp r13 = gnu.expr.QuoteExp.getInstance(r13)
            r11[r12] = r13
            r12 = 2
            r11[r12] = r18
            r9.<init>(r10, r11)
            r0 = r22
            r0.add(r9)
            int r34 = r34 + 1
            int r32 = r32 + 1
            goto L_0x017a
        L_0x01e6:
            r32 = 1
            goto L_0x017a
        L_0x01e9:
            r31 = move-exception
            r9 = 101(0x65, float:1.42E-43)
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = "non-integer keyword '"
            java.lang.StringBuilder r10 = r10.append(r11)
            r0 = r40
            java.lang.StringBuilder r10 = r10.append(r0)
            java.lang.String r11 = "' in array constructor"
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.String r10 = r10.toString()
            r0 = r24
            r0.error(r9, r10)
            goto L_0x0031
        L_0x020e:
            gnu.expr.ReferenceExp r9 = new gnu.expr.ReferenceExp
            r9.<init>(r15)
            r0 = r22
            r0.add(r9)
            r0 = r22
            r1 = r43
            r1.body = r0
            r69 = r43
            goto L_0x0031
        L_0x0222:
            if (r65 == 0) goto L_0x073c
            if (r47 == 0) goto L_0x073c
            r0 = r65
            boolean r9 = r0 instanceof gnu.expr.TypeValue
            if (r9 == 0) goto L_0x025e
            r9 = 78
            r0 = r39
            if (r0 != r9) goto L_0x025e
            r9 = r65
            gnu.expr.TypeValue r9 = (gnu.expr.TypeValue) r9
            gnu.mapping.Procedure r25 = r9.getConstructor()
            if (r25 == 0) goto L_0x025e
            int r9 = r48 + -1
            gnu.expr.Expression[] r0 = new gnu.expr.Expression[r9]
            r68 = r0
            r9 = 1
            r10 = 0
            int r11 = r48 + -1
            r0 = r68
            java.lang.System.arraycopy(r5, r9, r0, r10, r11)
            gnu.expr.ApplyExp r9 = new gnu.expr.ApplyExp
            r0 = r25
            r1 = r68
            r9.<init>(r0, r1)
            r0 = r70
            r1 = r71
            gnu.expr.Expression r69 = r0.visit(r9, r1)
            goto L_0x0031
        L_0x025e:
            if (r24 != 0) goto L_0x02f8
            r23 = 0
        L_0x0262:
            r4 = r65
            r0 = r47
            r1 = r23
            r2 = r35
            gnu.expr.PrimProcedure[] r3 = getMethods(r4, r0, r1, r2)     // Catch:{ Exception -> 0x030e }
            int r52 = gnu.kawa.reflect.ClassMethods.selectApplicable(r3, r6)     // Catch:{ Exception -> 0x030e }
            r34 = -1
            r9 = 78
            r0 = r39
            if (r0 != r9) goto L_0x049e
            r9 = 1
            int r38 = hasKeywordArgument(r9, r5)
            int r9 = r5.length
            r0 = r38
            if (r0 < r9) goto L_0x029b
            if (r52 > 0) goto L_0x049e
            r9 = 1
            gnu.bytecode.Type[] r9 = new gnu.bytecode.Type[r9]
            r10 = 0
            gnu.bytecode.ClassType r11 = gnu.expr.Compilation.typeClassType
            r9[r10] = r11
            long r9 = gnu.kawa.reflect.ClassMethods.selectApplicable(r3, r9)
            r11 = 32
            long r9 = r9 >> r11
            r11 = 1
            int r9 = (r9 > r11 ? 1 : (r9 == r11 ? 0 : -1))
            if (r9 != 0) goto L_0x049e
        L_0x029b:
            r0 = r38
            r1 = r23
            java.lang.Object[] r62 = checkKeywords(r4, r5, r0, r1)
            r0 = r62
            int r9 = r0.length
            int r9 = r9 * 2
            int r10 = r5.length
            int r10 = r10 - r38
            if (r9 == r10) goto L_0x02c1
            java.lang.String r9 = "add"
            r10 = 86
            r11 = 0
            r0 = r35
            gnu.expr.Language r12 = r0.language
            gnu.expr.PrimProcedure[] r9 = gnu.kawa.reflect.ClassMethods.getMethods(r4, r9, r10, r11, r12)
            r10 = 2
            int r9 = gnu.kawa.reflect.ClassMethods.selectApplicable(r9, r10)
            if (r9 <= 0) goto L_0x049e
        L_0x02c1:
            r30 = 0
            r32 = 0
        L_0x02c5:
            r0 = r62
            int r9 = r0.length
            r0 = r32
            if (r0 >= r9) goto L_0x0337
            r9 = r62[r32]
            boolean r9 = r9 instanceof java.lang.String
            if (r9 == 0) goto L_0x02f5
            if (r30 != 0) goto L_0x032f
            java.lang.StringBuffer r30 = new java.lang.StringBuffer
            r30.<init>()
            java.lang.String r9 = "no field or setter "
            r0 = r30
            r0.append(r9)
        L_0x02e0:
            r9 = 96
            r0 = r30
            r0.append(r9)
            r9 = r62[r32]
            r0 = r30
            r0.append(r9)
            r9 = 39
            r0 = r30
            r0.append(r9)
        L_0x02f5:
            int r32 = r32 + 1
            goto L_0x02c5
        L_0x02f8:
            r0 = r24
            gnu.bytecode.ClassType r9 = r0.curClass
            if (r9 == 0) goto L_0x0306
            r0 = r24
            gnu.bytecode.ClassType r0 = r0.curClass
            r23 = r0
            goto L_0x0262
        L_0x0306:
            r0 = r24
            gnu.bytecode.ClassType r0 = r0.mainClass
            r23 = r0
            goto L_0x0262
        L_0x030e:
            r31 = move-exception
            r9 = 119(0x77, float:1.67E-43)
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = "unknown class: "
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.String r11 = r65.getName()
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.String r10 = r10.toString()
            r0 = r24
            r0.error(r9, r10)
            goto L_0x0031
        L_0x032f:
            java.lang.String r9 = ", "
            r0 = r30
            r0.append(r9)
            goto L_0x02e0
        L_0x0337:
            if (r30 == 0) goto L_0x0356
            java.lang.String r9 = " in class "
            r0 = r30
            r0.append(r9)
            java.lang.String r9 = r65.getName()
            r0 = r30
            r0.append(r9)
            r9 = 119(0x77, float:1.67E-43)
            java.lang.String r10 = r30.toString()
            r0 = r24
            r0.error(r9, r10)
            goto L_0x0031
        L_0x0356:
            int r9 = r5.length
            r0 = r38
            if (r0 >= r9) goto L_0x03e7
            r0 = r38
            gnu.expr.Expression[] r0 = new gnu.expr.Expression[r0]
            r68 = r0
            r9 = 0
            r10 = 0
            r0 = r68
            r1 = r38
            java.lang.System.arraycopy(r5, r9, r0, r10, r1)
            gnu.expr.ApplyExp r9 = new gnu.expr.ApplyExp
            gnu.expr.Expression r10 = r69.getFunction()
            r0 = r68
            r9.<init>(r10, r0)
            r0 = r70
            gnu.expr.Expression r16 = r0.visit(r9, r4)
            gnu.expr.ApplyExp r16 = (gnu.expr.ApplyExp) r16
        L_0x037d:
            r0 = r16
            r0.setType(r4)
            r28 = r16
            int r9 = r5.length
            if (r9 <= 0) goto L_0x048c
            r32 = 0
        L_0x0389:
            r0 = r62
            int r9 = r0.length
            r0 = r32
            if (r0 >= r9) goto L_0x040a
            r61 = r62[r32]
            r0 = r61
            boolean r9 = r0 instanceof gnu.bytecode.Method
            if (r9 == 0) goto L_0x03f8
            r9 = r61
            gnu.bytecode.Method r9 = (gnu.bytecode.Method) r9
            gnu.bytecode.Type[] r9 = r9.getParameterTypes()
            r10 = 0
            r64 = r9[r10]
        L_0x03a3:
            if (r64 == 0) goto L_0x03af
            r0 = r35
            gnu.expr.Language r9 = r0.language
            r0 = r64
            gnu.bytecode.Type r64 = r9.getLangTypeFor(r0)
        L_0x03af:
            int r9 = r32 * 2
            int r9 = r9 + r38
            int r9 = r9 + 1
            r9 = r5[r9]
            r0 = r70
            r1 = r64
            gnu.expr.Expression r18 = r0.visit(r9, r1)
            r9 = 3
            gnu.expr.Expression[] r0 = new gnu.expr.Expression[r9]
            r58 = r0
            r9 = 0
            r58[r9] = r16
            r9 = 1
            gnu.expr.QuoteExp r10 = new gnu.expr.QuoteExp
            r0 = r61
            r10.<init>(r0)
            r58[r9] = r10
            r9 = 2
            r58[r9] = r18
            gnu.expr.ApplyExp r16 = new gnu.expr.ApplyExp
            gnu.kawa.reflect.SlotSet r9 = gnu.kawa.reflect.SlotSet.setFieldReturnObject
            r0 = r16
            r1 = r58
            r0.<init>(r9, r1)
            r0 = r16
            r0.setType(r4)
            int r32 = r32 + 1
            goto L_0x0389
        L_0x03e7:
            gnu.expr.ApplyExp r16 = new gnu.expr.ApplyExp
            r9 = 0
            r9 = r3[r9]
            r10 = 1
            gnu.expr.Expression[] r10 = new gnu.expr.Expression[r10]
            r11 = 0
            r10[r11] = r19
            r0 = r16
            r0.<init>(r9, r10)
            goto L_0x037d
        L_0x03f8:
            r0 = r61
            boolean r9 = r0 instanceof gnu.bytecode.Field
            if (r9 == 0) goto L_0x0407
            r9 = r61
            gnu.bytecode.Field r9 = (gnu.bytecode.Field) r9
            gnu.bytecode.Type r64 = r9.getType()
            goto L_0x03a3
        L_0x0407:
            r64 = 0
            goto L_0x03a3
        L_0x040a:
            int r9 = r5.length
            r0 = r38
            if (r0 != r9) goto L_0x0472
            r58 = 1
        L_0x0411:
            r28 = r16
            int r9 = r5.length
            r0 = r58
            if (r0 >= r9) goto L_0x048c
            gnu.expr.LetExp r43 = new gnu.expr.LetExp
            r9 = 1
            gnu.expr.Expression[] r9 = new gnu.expr.Expression[r9]
            r10 = 0
            r9[r10] = r28
            r0 = r43
            r0.<init>(r9)
            r9 = 0
            java.lang.String r9 = (java.lang.String) r9
            r0 = r43
            gnu.expr.Declaration r15 = r0.addDeclaration(r9, r4)
            r0 = r28
            r15.noteValue(r0)
            gnu.expr.BeginExp r22 = new gnu.expr.BeginExp
            r22.<init>()
            r32 = r58
        L_0x043a:
            int r9 = r5.length
            r0 = r32
            if (r0 >= r9) goto L_0x047a
            r9 = 3
            gnu.expr.Expression[] r0 = new gnu.expr.Expression[r9]
            r33 = r0
            r9 = 0
            gnu.expr.ReferenceExp r10 = new gnu.expr.ReferenceExp
            r10.<init>(r15)
            r33[r9] = r10
            r9 = 1
            java.lang.String r10 = "add"
            gnu.expr.QuoteExp r10 = gnu.expr.QuoteExp.getInstance(r10)
            r33[r9] = r10
            r9 = 2
            r10 = r5[r32]
            r33[r9] = r10
            gnu.expr.ApplyExp r9 = new gnu.expr.ApplyExp
            gnu.kawa.reflect.Invoke r10 = gnu.kawa.reflect.Invoke.invoke
            r0 = r33
            r9.<init>(r10, r0)
            r10 = 0
            r0 = r70
            gnu.expr.Expression r9 = r0.visit(r9, r10)
            r0 = r22
            r0.add(r9)
            int r32 = r32 + 1
            goto L_0x043a
        L_0x0472:
            r0 = r62
            int r9 = r0.length
            int r9 = r9 * 2
            int r58 = r9 + r38
            goto L_0x0411
        L_0x047a:
            gnu.expr.ReferenceExp r9 = new gnu.expr.ReferenceExp
            r9.<init>(r15)
            r0 = r22
            r0.add(r9)
            r0 = r22
            r1 = r43
            r1.body = r0
            r28 = r43
        L_0x048c:
            r0 = r28
            r1 = r69
            gnu.expr.Expression r9 = r0.setLine(r1)
            r0 = r70
            r1 = r71
            gnu.expr.Expression r69 = r0.checkType(r9, r1)
            goto L_0x0031
        L_0x049e:
            if (r52 < 0) goto L_0x061f
            r32 = 1
        L_0x04a2:
            r0 = r32
            r1 = r48
            if (r0 >= r1) goto L_0x054c
            r21 = 0
            int r9 = r48 + -1
            r0 = r32
            if (r0 != r9) goto L_0x04d9
            r41 = 1
        L_0x04b2:
            r9 = 80
            r0 = r39
            if (r0 != r9) goto L_0x04bd
            r9 = 2
            r0 = r32
            if (r0 == r9) goto L_0x04c8
        L_0x04bd:
            r9 = 78
            r0 = r39
            if (r0 == r9) goto L_0x04dc
            r9 = 1
            r0 = r32
            if (r0 != r9) goto L_0x04dc
        L_0x04c8:
            r21 = 0
        L_0x04ca:
            r9 = r5[r32]
            r0 = r70
            r1 = r21
            gnu.expr.Expression r9 = r0.visit(r9, r1)
            r5[r32] = r9
            int r32 = r32 + 1
            goto L_0x04a2
        L_0x04d9:
            r41 = 0
            goto L_0x04b2
        L_0x04dc:
            r9 = 80
            r0 = r39
            if (r0 != r9) goto L_0x04ea
            r9 = 1
            r0 = r32
            if (r0 != r9) goto L_0x04ea
            r21 = r4
            goto L_0x04ca
        L_0x04ea:
            if (r52 <= 0) goto L_0x04ca
            r9 = 78
            r0 = r39
            if (r0 != r9) goto L_0x0525
            r9 = 1
        L_0x04f3:
            int r54 = r32 - r9
            r36 = 0
        L_0x04f7:
            r0 = r36
            r1 = r52
            if (r0 >= r1) goto L_0x04ca
            r56 = r3[r36]
            r9 = 83
            r0 = r39
            if (r0 == r9) goto L_0x0527
            boolean r9 = r56.takesTarget()
            if (r9 == 0) goto L_0x0527
            r9 = 1
        L_0x050c:
            int r55 = r54 + r9
            if (r41 == 0) goto L_0x0529
            boolean r9 = r56.takesVarArgs()
            if (r9 == 0) goto L_0x0529
            int r9 = r56.minArgs()
            r0 = r55
            if (r0 != r9) goto L_0x0529
            r21 = 0
        L_0x0520:
            if (r21 == 0) goto L_0x04ca
            int r36 = r36 + 1
            goto L_0x04f7
        L_0x0525:
            r9 = r7
            goto L_0x04f3
        L_0x0527:
            r9 = 0
            goto L_0x050c
        L_0x0529:
            r0 = r56
            r1 = r55
            gnu.bytecode.Type r57 = r0.getParameterType(r1)
            if (r36 != 0) goto L_0x0536
            r21 = r57
            goto L_0x0520
        L_0x0536:
            r0 = r57
            boolean r9 = r0 instanceof gnu.bytecode.PrimType
            r0 = r21
            boolean r10 = r0 instanceof gnu.bytecode.PrimType
            if (r9 == r10) goto L_0x0543
            r21 = 0
            goto L_0x0520
        L_0x0543:
            r0 = r21
            r1 = r57
            gnu.bytecode.Type r21 = gnu.bytecode.Type.lowestCommonSuperType(r0, r1)
            goto L_0x0520
        L_0x054c:
            long r50 = selectApplicable(r3, r4, r5, r6, r7, r8)
            r9 = 32
            long r9 = r50 >> r9
            int r0 = (int) r9
            r53 = r0
            r0 = r50
            int r0 = (int) r0
            r45 = r0
        L_0x055c:
            int r0 = r3.length
            r49 = r0
            int r9 = r53 + r45
            if (r9 != 0) goto L_0x058c
            r9 = 78
            r0 = r39
            if (r0 != r9) goto L_0x058c
            java.lang.String r9 = "valueOf"
            gnu.kawa.reflect.Invoke r10 = gnu.kawa.reflect.Invoke.invokeStatic
            r0 = r23
            gnu.expr.PrimProcedure[] r3 = getMethods(r4, r9, r0, r10)
            r7 = 1
            int r6 = r48 + -1
            r14 = -1
            r9 = r3
            r10 = r4
            r11 = r5
            r12 = r6
            r13 = r7
            long r50 = selectApplicable(r9, r10, r11, r12, r13, r14)
            r9 = 32
            long r9 = r50 >> r9
            int r0 = (int) r9
            r53 = r0
            r0 = r50
            int r0 = (int) r0
            r45 = r0
        L_0x058c:
            int r9 = r53 + r45
            if (r9 != 0) goto L_0x064c
            r9 = 80
            r0 = r39
            if (r0 == r9) goto L_0x059c
            boolean r9 = r24.warnInvokeUnknownMethod()
            if (r9 == 0) goto L_0x05f0
        L_0x059c:
            r9 = 78
            r0 = r39
            if (r0 != r9) goto L_0x05b7
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            r0 = r47
            java.lang.StringBuilder r9 = r9.append(r0)
            java.lang.String r10 = "/valueOf"
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r47 = r9.toString()
        L_0x05b7:
            java.lang.StringBuilder r59 = new java.lang.StringBuilder
            r59.<init>()
            int r9 = r3.length
            int r9 = r9 + r49
            if (r9 != 0) goto L_0x0625
            java.lang.String r9 = "no accessible method '"
            r0 = r59
            r0.append(r9)
        L_0x05c8:
            r0 = r59
            r1 = r47
            r0.append(r1)
            java.lang.String r9 = "' in "
            r0 = r59
            r0.append(r9)
            java.lang.String r9 = r65.getName()
            r0 = r59
            r0.append(r9)
            r9 = 80
            r0 = r39
            if (r0 != r9) goto L_0x0649
            r9 = 101(0x65, float:1.42E-43)
        L_0x05e7:
            java.lang.String r10 = r59.toString()
            r0 = r24
            r0.error(r9, r10)
        L_0x05f0:
            if (r34 < 0) goto L_0x073c
            gnu.expr.Expression[] r0 = new gnu.expr.Expression[r6]
            r44 = r0
            r46 = r3[r34]
            boolean r67 = r46.takesVarArgs()
            r26 = 0
            if (r8 < 0) goto L_0x0608
            int r27 = r26 + 1
            r9 = r5[r8]
            r44[r26] = r9
            r26 = r27
        L_0x0608:
            r63 = r7
        L_0x060a:
            int r9 = r5.length
            r0 = r63
            if (r0 >= r9) goto L_0x071e
            r0 = r44
            int r9 = r0.length
            r0 = r26
            if (r0 >= r9) goto L_0x071e
            r9 = r5[r63]
            r44[r26] = r9
            int r63 = r63 + 1
            int r26 = r26 + 1
            goto L_0x060a
        L_0x061f:
            r53 = 0
            r45 = 0
            goto L_0x055c
        L_0x0625:
            r9 = -983040(0xfffffffffff10000, float:NaN)
            r0 = r52
            if (r0 != r9) goto L_0x0633
            java.lang.String r9 = "too few arguments for method '"
            r0 = r59
            r0.append(r9)
            goto L_0x05c8
        L_0x0633:
            r9 = -917504(0xfffffffffff20000, float:NaN)
            r0 = r52
            if (r0 != r9) goto L_0x0641
            java.lang.String r9 = "too many arguments for method '"
            r0 = r59
            r0.append(r9)
            goto L_0x05c8
        L_0x0641:
            java.lang.String r9 = "no possibly applicable method '"
            r0 = r59
            r0.append(r9)
            goto L_0x05c8
        L_0x0649:
            r9 = 119(0x77, float:1.67E-43)
            goto L_0x05e7
        L_0x064c:
            r9 = 1
            r0 = r53
            if (r0 == r9) goto L_0x0658
            if (r53 != 0) goto L_0x065b
            r9 = 1
            r0 = r45
            if (r0 != r9) goto L_0x065b
        L_0x0658:
            r34 = 0
            goto L_0x05f0
        L_0x065b:
            if (r53 <= 0) goto L_0x06d2
            r0 = r53
            int r34 = gnu.mapping.MethodProc.mostSpecific(r3, r0)
            if (r34 >= 0) goto L_0x067f
            r9 = 83
            r0 = r39
            if (r0 != r9) goto L_0x067f
            r32 = 0
        L_0x066d:
            r0 = r32
            r1 = r53
            if (r0 >= r1) goto L_0x067f
            r9 = r3[r32]
            boolean r9 = r9.getStaticFlag()
            if (r9 == 0) goto L_0x06cc
            if (r34 < 0) goto L_0x06ca
            r34 = -1
        L_0x067f:
            if (r34 >= 0) goto L_0x05f0
            r9 = 80
            r0 = r39
            if (r0 == r9) goto L_0x068d
            boolean r9 = r24.warnInvokeUnknownMethod()
            if (r9 == 0) goto L_0x05f0
        L_0x068d:
            java.lang.StringBuffer r59 = new java.lang.StringBuffer
            r59.<init>()
            java.lang.String r9 = "more than one definitely applicable method `"
            r0 = r59
            r0.append(r9)
            r0 = r59
            r1 = r47
            r0.append(r1)
            java.lang.String r9 = "' in "
            r0 = r59
            r0.append(r9)
            java.lang.String r9 = r65.getName()
            r0 = r59
            r0.append(r9)
            r0 = r53
            r1 = r59
            append(r3, r0, r1)
            r9 = 80
            r0 = r39
            if (r0 != r9) goto L_0x06cf
            r9 = 101(0x65, float:1.42E-43)
        L_0x06bf:
            java.lang.String r10 = r59.toString()
            r0 = r24
            r0.error(r9, r10)
            goto L_0x05f0
        L_0x06ca:
            r34 = r32
        L_0x06cc:
            int r32 = r32 + 1
            goto L_0x066d
        L_0x06cf:
            r9 = 119(0x77, float:1.67E-43)
            goto L_0x06bf
        L_0x06d2:
            r9 = 80
            r0 = r39
            if (r0 == r9) goto L_0x06de
            boolean r9 = r24.warnInvokeUnknownMethod()
            if (r9 == 0) goto L_0x05f0
        L_0x06de:
            java.lang.StringBuffer r59 = new java.lang.StringBuffer
            r59.<init>()
            java.lang.String r9 = "more than one possibly applicable method '"
            r0 = r59
            r0.append(r9)
            r0 = r59
            r1 = r47
            r0.append(r1)
            java.lang.String r9 = "' in "
            r0 = r59
            r0.append(r9)
            java.lang.String r9 = r65.getName()
            r0 = r59
            r0.append(r9)
            r0 = r45
            r1 = r59
            append(r3, r0, r1)
            r9 = 80
            r0 = r39
            if (r0 != r9) goto L_0x071b
            r9 = 101(0x65, float:1.42E-43)
        L_0x0710:
            java.lang.String r10 = r59.toString()
            r0 = r24
            r0.error(r9, r10)
            goto L_0x05f0
        L_0x071b:
            r9 = 119(0x77, float:1.67E-43)
            goto L_0x0710
        L_0x071e:
            gnu.expr.ApplyExp r28 = new gnu.expr.ApplyExp
            r0 = r28
            r1 = r46
            r2 = r44
            r0.<init>(r1, r2)
            r0 = r28
            r1 = r69
            r0.setLine(r1)
            r0 = r70
            r1 = r28
            r2 = r71
            gnu.expr.Expression r69 = r0.visitApplyOnly(r1, r2)
            goto L_0x0031
        L_0x073c:
            r69.visitArgs(r70)
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: gnu.kawa.reflect.CompileInvoke.validateApplyInvoke(gnu.expr.ApplyExp, gnu.expr.InlineCalls, gnu.bytecode.Type, gnu.mapping.Procedure):gnu.expr.Expression");
    }

    static Object[] checkKeywords(ObjectType type, Expression[] args, int start, ClassType caller) {
        int len = args.length;
        int npairs = 0;
        while ((npairs * 2) + start + 1 < len && (args[(npairs * 2) + start].valueIfConstant() instanceof Keyword)) {
            npairs++;
        }
        Object[] fields = new Object[npairs];
        for (int i = 0; i < npairs; i++) {
            String name = ((Keyword) args[(i * 2) + start].valueIfConstant()).getName();
            String lookupMember = SlotSet.lookupMember(type, name, caller);
            if (lookupMember == null) {
                lookupMember = type.getMethod(ClassExp.slotToMethodName("add", name), SlotSet.type1Array);
            }
            if (lookupMember == null) {
                lookupMember = name;
            }
            fields[i] = lookupMember;
        }
        return fields;
    }

    private static String getMethodName(Expression[] args, char kind) {
        if (kind == 'N') {
            return "<init>";
        }
        int nameIndex = kind == 'P' ? 2 : 1;
        if (args.length >= nameIndex + 1) {
            return ClassMethods.checkName(args[nameIndex], false);
        }
        return null;
    }

    private static void append(PrimProcedure[] methods, int mcount, StringBuffer sbuf) {
        for (int i = 0; i < mcount; i++) {
            sbuf.append("\n  candidate: ");
            sbuf.append(methods[i]);
        }
    }

    protected static PrimProcedure[] getMethods(ObjectType ctype, String mname, ClassType caller, Invoke iproc) {
        char c = 'P';
        int kind = iproc.kind;
        if (kind != 80) {
            c = (kind == 42 || kind == 86) ? 'V' : 0;
        }
        return ClassMethods.getMethods(ctype, mname, c, caller, iproc.language);
    }

    static int hasKeywordArgument(int argsStartIndex, Expression[] args) {
        for (int i = argsStartIndex; i < args.length; i++) {
            if (args[i].valueIfConstant() instanceof Keyword) {
                return i;
            }
        }
        return args.length;
    }

    private static long selectApplicable(PrimProcedure[] methods, ObjectType ctype, Expression[] args, int margsLength, int argsStartIndex, int objIndex) {
        Type[] atypes = new Type[margsLength];
        int dst = 0;
        if (objIndex >= 0) {
            atypes[0] = ctype;
            dst = 0 + 1;
        }
        int src = argsStartIndex;
        while (src < args.length && dst < atypes.length) {
            Expression arg = args[src];
            Type atype = null;
            if (InlineCalls.checkIntValue(arg) != null) {
                atype = Type.intType;
            } else if (InlineCalls.checkLongValue(arg) != null) {
                atype = Type.longType;
            } else if (0 == 0) {
                atype = arg.getType();
            }
            atypes[dst] = atype;
            src++;
            dst++;
        }
        return ClassMethods.selectApplicable(methods, atypes);
    }

    public static synchronized PrimProcedure getStaticMethod(ClassType type, String name, Expression[] args) {
        int index;
        PrimProcedure primProcedure;
        synchronized (CompileInvoke.class) {
            PrimProcedure[] methods = getMethods(type, name, null, Invoke.invokeStatic);
            long num = selectApplicable(methods, type, args, args.length, 0, -1);
            int okCount = (int) (num >> 32);
            int maybeCount = (int) num;
            if (methods == null) {
                index = -1;
            } else if (okCount > 0) {
                index = MethodProc.mostSpecific(methods, okCount);
            } else if (maybeCount == 1) {
                index = 0;
            } else {
                index = -1;
            }
            if (index < 0) {
                primProcedure = null;
            } else {
                primProcedure = methods[index];
            }
        }
        return primProcedure;
    }
}
