package gnu.kawa.reflect;

import gnu.bytecode.Type;
import gnu.mapping.Procedure;
import gnu.mapping.Procedure1;
import gnu.mapping.PropertySet;
import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.lang.reflect.Array;

public class ArrayNew extends Procedure1 implements Externalizable {
    Type element_type;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: gnu.mapping.LazyPropertyKey.set(gnu.mapping.PropertySet, java.lang.String):void
     arg types: [gnu.kawa.reflect.ArrayNew, java.lang.String]
     candidates:
      gnu.mapping.PropertyKey.set(gnu.mapping.PropertySet, ?):void
      gnu.mapping.LazyPropertyKey.set(gnu.mapping.PropertySet, java.lang.String):void */
    public ArrayNew(Type element_type2) {
        this.element_type = element_type2;
        setProperty(Procedure.validateApplyKey, "gnu.kawa.reflect.CompileArrays:validateArrayNew");
        Procedure.compilerKey.set((PropertySet) this, "*gnu.kawa.reflect.CompileArrays:getForArrayNew");
    }

    public boolean isSideEffectFree() {
        return true;
    }

    public Object apply1(Object count) {
        return Array.newInstance(this.element_type.getImplementationType().getReflectClass(), ((Number) count).intValue());
    }

    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeObject(this.element_type);
    }

    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        this.element_type = (Type) in.readObject();
    }
}
