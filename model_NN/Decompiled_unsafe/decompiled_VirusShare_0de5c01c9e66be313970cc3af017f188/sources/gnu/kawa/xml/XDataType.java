package gnu.kawa.xml;

import com.google.devtools.simple.runtime.annotations.DesignerProperty;
import gnu.bytecode.ClassType;
import gnu.bytecode.CodeAttr;
import gnu.bytecode.Method;
import gnu.bytecode.Type;
import gnu.bytecode.Variable;
import gnu.expr.Compilation;
import gnu.expr.Declaration;
import gnu.expr.Expression;
import gnu.expr.Target;
import gnu.expr.TypeValue;
import gnu.kawa.reflect.InstanceOf;
import gnu.lists.Consumer;
import gnu.lists.SeqPosition;
import gnu.mapping.Procedure;
import gnu.mapping.Values;
import gnu.math.Duration;
import gnu.math.IntNum;
import gnu.math.Unit;
import gnu.text.Path;
import gnu.text.Printable;
import gnu.text.URIPath;
import gnu.xml.TextUtils;
import java.math.BigDecimal;

public class XDataType extends Type implements TypeValue {
    public static final int ANY_ATOMIC_TYPE_CODE = 3;
    public static final int ANY_SIMPLE_TYPE_CODE = 2;
    public static final int ANY_URI_TYPE_CODE = 33;
    public static final int BASE64_BINARY_TYPE_CODE = 34;
    public static final int BOOLEAN_TYPE_CODE = 31;
    public static final int BYTE_TYPE_CODE = 11;
    public static final int DATE_TIME_TYPE_CODE = 20;
    public static final int DATE_TYPE_CODE = 21;
    public static final int DAY_TIME_DURATION_TYPE_CODE = 30;
    public static final BigDecimal DECIMAL_ONE = BigDecimal.valueOf(1L);
    public static final int DECIMAL_TYPE_CODE = 4;
    public static final Double DOUBLE_ONE = makeDouble(1.0d);
    public static final int DOUBLE_TYPE_CODE = 19;
    public static final Double DOUBLE_ZERO = makeDouble(0.0d);
    public static final int DURATION_TYPE_CODE = 28;
    public static final int ENTITY_TYPE_CODE = 47;
    public static final Float FLOAT_ONE = makeFloat(1.0f);
    public static final int FLOAT_TYPE_CODE = 18;
    public static final Float FLOAT_ZERO = makeFloat(0.0f);
    public static final int G_DAY_TYPE_CODE = 26;
    public static final int G_MONTH_DAY_TYPE_CODE = 25;
    public static final int G_MONTH_TYPE_CODE = 27;
    public static final int G_YEAR_MONTH_TYPE_CODE = 23;
    public static final int G_YEAR_TYPE_CODE = 24;
    public static final int HEX_BINARY_TYPE_CODE = 35;
    public static final int IDREF_TYPE_CODE = 46;
    public static final int ID_TYPE_CODE = 45;
    public static final int INTEGER_TYPE_CODE = 5;
    public static final int INT_TYPE_CODE = 9;
    public static final int LANGUAGE_TYPE_CODE = 41;
    public static final int LONG_TYPE_CODE = 8;
    public static final int NAME_TYPE_CODE = 43;
    public static final int NCNAME_TYPE_CODE = 44;
    public static final int NEGATIVE_INTEGER_TYPE_CODE = 7;
    public static final int NMTOKEN_TYPE_CODE = 42;
    public static final int NONNEGATIVE_INTEGER_TYPE_CODE = 12;
    public static final int NON_POSITIVE_INTEGER_TYPE_CODE = 6;
    public static final int NORMALIZED_STRING_TYPE_CODE = 39;
    public static final int NOTATION_TYPE_CODE = 36;
    public static final XDataType NotationType = new XDataType("NOTATION", ClassType.make("gnu.kawa.xml.Notation"), 36);
    public static final int POSITIVE_INTEGER_TYPE_CODE = 17;
    public static final int QNAME_TYPE_CODE = 32;
    public static final int SHORT_TYPE_CODE = 10;
    public static final int STRING_TYPE_CODE = 38;
    public static final int TIME_TYPE_CODE = 22;
    public static final int TOKEN_TYPE_CODE = 40;
    public static final int UNSIGNED_BYTE_TYPE_CODE = 16;
    public static final int UNSIGNED_INT_TYPE_CODE = 14;
    public static final int UNSIGNED_LONG_TYPE_CODE = 13;
    public static final int UNSIGNED_SHORT_TYPE_CODE = 15;
    public static final int UNTYPED_ATOMIC_TYPE_CODE = 37;
    public static final int UNTYPED_TYPE_CODE = 48;
    public static final int YEAR_MONTH_DURATION_TYPE_CODE = 29;
    public static final XDataType anyAtomicType = new XDataType("anyAtomicType", Type.objectType, 3);
    public static final XDataType anySimpleType = new XDataType("anySimpleType", Type.objectType, 2);
    public static final XDataType anyURIType = new XDataType("anyURI", ClassType.make("gnu.text.Path"), 33);
    public static final XDataType base64BinaryType = new XDataType("base64Binary", ClassType.make("gnu.kawa.xml.Base64Binary"), 34);
    public static final XDataType booleanType = new XDataType(DesignerProperty.PROPERTY_TYPE_BOOLEAN, Type.booleanType, 31);
    public static final XDataType dayTimeDurationType = new XDataType("dayTimeDuration", ClassType.make("gnu.math.Duration"), 30);
    public static final XDataType decimalType = new XDataType("decimal", ClassType.make("java.lang.Number"), 4);
    public static final XDataType doubleType = new XDataType("double", ClassType.make("java.lang.Double"), 19);
    public static final XDataType durationType = new XDataType("duration", ClassType.make("gnu.math.Duration"), 28);
    public static final XDataType floatType = new XDataType(DesignerProperty.PROPERTY_TYPE_FLOAT, ClassType.make("java.lang.Float"), 18);
    public static final XDataType hexBinaryType = new XDataType("hexBinary", ClassType.make("gnu.kawa.xml.HexBinary"), 35);
    public static final XDataType stringStringType = new XDataType("String", ClassType.make("java.lang.String"), 38);
    public static final XDataType stringType = new XDataType(DesignerProperty.PROPERTY_TYPE_STRING, ClassType.make("java.lang.CharSequence"), 38);
    public static final XDataType untypedAtomicType = new XDataType(DesignerProperty.PROPERTY_TYPE_STRING, ClassType.make("gnu.kawa.xml.UntypedAtomic"), 37);
    public static final XDataType untypedType = new XDataType("untyped", Type.objectType, 48);
    public static final XDataType yearMonthDurationType = new XDataType("yearMonthDuration", ClassType.make("gnu.math.Duration"), 29);
    XDataType baseType;
    Type implementationType;
    Object name;
    int typeCode;

    public XDataType(Object name2, Type implementationType2, int typeCode2) {
        super(implementationType2);
        this.name = name2;
        if (name2 != null) {
            setName(name2.toString());
        }
        this.implementationType = implementationType2;
        this.typeCode = typeCode2;
    }

    public Class getReflectClass() {
        return this.implementationType.getReflectClass();
    }

    public Type getImplementationType() {
        return this.implementationType;
    }

    public void emitCoerceFromObject(CodeAttr code) {
        Compilation.getCurrent().compileConstant(this, Target.pushObject);
        Method meth = ClassType.make("gnu.kawa.xml.XDataType").getDeclaredMethod("coerceFromObject", 1);
        code.emitSwap();
        code.emitInvokeVirtual(meth);
        this.implementationType.emitCoerceFromObject(code);
    }

    public void emitCoerceToObject(CodeAttr code) {
        if (this.typeCode == 31) {
            this.implementationType.emitCoerceToObject(code);
        } else {
            super.emitCoerceToObject(code);
        }
    }

    public void emitTestIf(Variable incoming, Declaration decl, Compilation comp) {
        CodeAttr code = comp.getCode();
        if (this.typeCode == 31) {
            if (incoming != null) {
                code.emitLoad(incoming);
            }
            Type.javalangBooleanType.emitIsInstance(code);
            code.emitIfIntNotZero();
            if (decl != null) {
                code.emitLoad(incoming);
                Type.booleanType.emitCoerceFromObject(code);
                decl.compileStore(comp);
                return;
            }
            return;
        }
        comp.compileConstant(this, Target.pushObject);
        if (incoming == null) {
            code.emitSwap();
        } else {
            code.emitLoad(incoming);
        }
        code.emitInvokeVirtual(Compilation.typeType.getDeclaredMethod("isInstance", 1));
        code.emitIfIntNotZero();
        if (decl != null) {
            code.emitLoad(incoming);
            emitCoerceFromObject(code);
            decl.compileStore(comp);
        }
    }

    public Expression convertValue(Expression value) {
        return null;
    }

    public boolean isInstance(Object obj) {
        boolean z = false;
        switch (this.typeCode) {
            case 2:
                if ((obj instanceof SeqPosition) || (obj instanceof Nodes)) {
                    return false;
                }
                return true;
            case 3:
                if ((obj instanceof Values) || (obj instanceof SeqPosition)) {
                    return false;
                }
                return true;
            case 4:
                if ((obj instanceof BigDecimal) || (obj instanceof IntNum)) {
                    z = true;
                }
                return z;
            case 18:
                return obj instanceof Float;
            case 19:
                return obj instanceof Double;
            case 28:
                return obj instanceof Duration;
            case 29:
                if (!(obj instanceof Duration) || ((Duration) obj).unit() != Unit.month) {
                    return false;
                }
                return true;
            case 30:
                return (obj instanceof Duration) && ((Duration) obj).unit() == Unit.second;
            case 31:
                return obj instanceof Boolean;
            case 33:
                return obj instanceof Path;
            case 37:
                return obj instanceof UntypedAtomic;
            case 38:
                return obj instanceof CharSequence;
            case 48:
                return true;
            default:
                return super.isInstance(obj);
        }
    }

    public void emitIsInstance(Variable incoming, Compilation comp, Target target) {
        InstanceOf.emitIsInstance(this, incoming, comp, target);
    }

    public String toString(Object value) {
        return value.toString();
    }

    public void print(Object value, Consumer out) {
        if (value instanceof Printable) {
            ((Printable) value).print(out);
        } else {
            out.write(toString(value));
        }
    }

    public boolean castable(Object value) {
        try {
            cast(value);
            return true;
        } catch (Throwable th) {
            return false;
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 8 */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x016b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object cast(java.lang.Object r9) {
        /*
            r8 = this;
            java.lang.Object r9 = gnu.kawa.xml.KNode.atomicValue(r9)
            boolean r6 = r9 instanceof gnu.kawa.xml.UntypedAtomic
            if (r6 == 0) goto L_0x0018
            int r6 = r8.typeCode
            r7 = 37
            if (r6 != r7) goto L_0x000f
        L_0x000e:
            return r9
        L_0x000f:
            java.lang.String r6 = r9.toString()
            java.lang.Object r9 = r8.valueOf(r6)
            goto L_0x000e
        L_0x0018:
            boolean r6 = r9 instanceof java.lang.String
            if (r6 == 0) goto L_0x0025
            java.lang.String r6 = r9.toString()
            java.lang.Object r9 = r8.valueOf(r6)
            goto L_0x000e
        L_0x0025:
            int r6 = r8.typeCode
            switch(r6) {
                case 4: goto L_0x0074;
                case 5: goto L_0x002a;
                case 6: goto L_0x002a;
                case 7: goto L_0x002a;
                case 8: goto L_0x002a;
                case 9: goto L_0x002a;
                case 10: goto L_0x002a;
                case 11: goto L_0x002a;
                case 12: goto L_0x002a;
                case 13: goto L_0x002a;
                case 14: goto L_0x002a;
                case 15: goto L_0x002a;
                case 16: goto L_0x002a;
                case 17: goto L_0x002a;
                case 18: goto L_0x00b2;
                case 19: goto L_0x00da;
                case 20: goto L_0x0129;
                case 21: goto L_0x0129;
                case 22: goto L_0x0129;
                case 23: goto L_0x0102;
                case 24: goto L_0x0102;
                case 25: goto L_0x0102;
                case 26: goto L_0x0102;
                case 27: goto L_0x0102;
                case 28: goto L_0x013d;
                case 29: goto L_0x0145;
                case 30: goto L_0x014d;
                case 31: goto L_0x0044;
                case 32: goto L_0x002a;
                case 33: goto L_0x003f;
                case 34: goto L_0x0155;
                case 35: goto L_0x0167;
                case 36: goto L_0x002a;
                case 37: goto L_0x0034;
                case 38: goto L_0x002f;
                default: goto L_0x002a;
            }
        L_0x002a:
            java.lang.Object r9 = r8.coerceFromObject(r9)
            goto L_0x000e
        L_0x002f:
            java.lang.String r9 = gnu.xml.TextUtils.asString(r9)
            goto L_0x000e
        L_0x0034:
            gnu.kawa.xml.UntypedAtomic r6 = new gnu.kawa.xml.UntypedAtomic
            java.lang.String r7 = gnu.xml.TextUtils.stringValue(r9)
            r6.<init>(r7)
            r9 = r6
            goto L_0x000e
        L_0x003f:
            gnu.text.URIPath r9 = gnu.text.URIPath.makeURI(r9)
            goto L_0x000e
        L_0x0044:
            boolean r6 = r9 instanceof java.lang.Boolean
            if (r6 == 0) goto L_0x0057
            java.lang.Boolean r9 = (java.lang.Boolean) r9
            boolean r6 = r9.booleanValue()
            if (r6 == 0) goto L_0x0054
            java.lang.Boolean r6 = java.lang.Boolean.TRUE
        L_0x0052:
            r9 = r6
            goto L_0x000e
        L_0x0054:
            java.lang.Boolean r6 = java.lang.Boolean.FALSE
            goto L_0x0052
        L_0x0057:
            boolean r6 = r9 instanceof java.lang.Number
            if (r6 == 0) goto L_0x002a
            java.lang.Number r9 = (java.lang.Number) r9
            double r0 = r9.doubleValue()
            r6 = 0
            int r6 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r6 == 0) goto L_0x006d
            boolean r6 = java.lang.Double.isNaN(r0)
            if (r6 == 0) goto L_0x0071
        L_0x006d:
            java.lang.Boolean r6 = java.lang.Boolean.FALSE
        L_0x006f:
            r9 = r6
            goto L_0x000e
        L_0x0071:
            java.lang.Boolean r6 = java.lang.Boolean.TRUE
            goto L_0x006f
        L_0x0074:
            boolean r6 = r9 instanceof java.math.BigDecimal
            if (r6 != 0) goto L_0x000e
            boolean r6 = r9 instanceof gnu.math.RealNum
            if (r6 == 0) goto L_0x0083
            gnu.math.RealNum r9 = (gnu.math.RealNum) r9
            java.math.BigDecimal r9 = r9.asBigDecimal()
            goto L_0x000e
        L_0x0083:
            boolean r6 = r9 instanceof java.lang.Float
            if (r6 != 0) goto L_0x008b
            boolean r6 = r9 instanceof java.lang.Double
            if (r6 == 0) goto L_0x0097
        L_0x008b:
            java.lang.Number r9 = (java.lang.Number) r9
            double r0 = r9.doubleValue()
            java.math.BigDecimal r9 = java.math.BigDecimal.valueOf(r0)
            goto L_0x000e
        L_0x0097:
            boolean r6 = r9 instanceof java.lang.Boolean
            if (r6 == 0) goto L_0x002a
            java.lang.Boolean r9 = (java.lang.Boolean) r9
            boolean r6 = r9.booleanValue()
            if (r6 == 0) goto L_0x00ad
            gnu.math.IntNum r6 = gnu.math.IntNum.one()
        L_0x00a7:
            java.lang.Object r9 = r8.cast(r6)
            goto L_0x000e
        L_0x00ad:
            gnu.math.IntNum r6 = gnu.math.IntNum.zero()
            goto L_0x00a7
        L_0x00b2:
            boolean r6 = r9 instanceof java.lang.Float
            if (r6 != 0) goto L_0x000e
            boolean r6 = r9 instanceof java.lang.Number
            if (r6 == 0) goto L_0x00c6
            java.lang.Number r9 = (java.lang.Number) r9
            float r6 = r9.floatValue()
            java.lang.Float r9 = makeFloat(r6)
            goto L_0x000e
        L_0x00c6:
            boolean r6 = r9 instanceof java.lang.Boolean
            if (r6 == 0) goto L_0x002a
            java.lang.Boolean r9 = (java.lang.Boolean) r9
            boolean r6 = r9.booleanValue()
            if (r6 == 0) goto L_0x00d7
            java.lang.Float r6 = gnu.kawa.xml.XDataType.FLOAT_ONE
        L_0x00d4:
            r9 = r6
            goto L_0x000e
        L_0x00d7:
            java.lang.Float r6 = gnu.kawa.xml.XDataType.FLOAT_ZERO
            goto L_0x00d4
        L_0x00da:
            boolean r6 = r9 instanceof java.lang.Double
            if (r6 != 0) goto L_0x000e
            boolean r6 = r9 instanceof java.lang.Number
            if (r6 == 0) goto L_0x00ee
            java.lang.Number r9 = (java.lang.Number) r9
            double r6 = r9.doubleValue()
            java.lang.Double r9 = makeDouble(r6)
            goto L_0x000e
        L_0x00ee:
            boolean r6 = r9 instanceof java.lang.Boolean
            if (r6 == 0) goto L_0x002a
            java.lang.Boolean r9 = (java.lang.Boolean) r9
            boolean r6 = r9.booleanValue()
            if (r6 == 0) goto L_0x00ff
            java.lang.Double r6 = gnu.kawa.xml.XDataType.DOUBLE_ONE
        L_0x00fc:
            r9 = r6
            goto L_0x000e
        L_0x00ff:
            java.lang.Double r6 = gnu.kawa.xml.XDataType.DOUBLE_ZERO
            goto L_0x00fc
        L_0x0102:
            boolean r6 = r9 instanceof gnu.math.DateTime
            if (r6 == 0) goto L_0x002a
            gnu.kawa.xml.XTimeType r8 = (gnu.kawa.xml.XTimeType) r8
            int r6 = r8.typeCode
            int r2 = gnu.kawa.xml.XTimeType.components(r6)
            r3 = r9
            gnu.math.DateTime r3 = (gnu.math.DateTime) r3
            int r5 = r3.components()
            if (r2 == r5) goto L_0x011d
            r6 = r5 & 14
            r7 = 14
            if (r6 != r7) goto L_0x0123
        L_0x011d:
            gnu.math.DateTime r9 = r3.cast(r2)
            goto L_0x000e
        L_0x0123:
            java.lang.ClassCastException r6 = new java.lang.ClassCastException
            r6.<init>()
            throw r6
        L_0x0129:
            boolean r6 = r9 instanceof gnu.math.DateTime
            if (r6 == 0) goto L_0x002a
            gnu.kawa.xml.XTimeType r8 = (gnu.kawa.xml.XTimeType) r8
            int r6 = r8.typeCode
            int r4 = gnu.kawa.xml.XTimeType.components(r6)
            gnu.math.DateTime r9 = (gnu.math.DateTime) r9
            gnu.math.DateTime r9 = r9.cast(r4)
            goto L_0x000e
        L_0x013d:
            gnu.math.BaseUnit r6 = gnu.math.Unit.duration
            gnu.math.Duration r9 = r8.castToDuration(r9, r6)
            goto L_0x000e
        L_0x0145:
            gnu.math.NamedUnit r6 = gnu.math.Unit.month
            gnu.math.Duration r9 = r8.castToDuration(r9, r6)
            goto L_0x000e
        L_0x014d:
            gnu.math.NamedUnit r6 = gnu.math.Unit.second
            gnu.math.Duration r9 = r8.castToDuration(r9, r6)
            goto L_0x000e
        L_0x0155:
            boolean r6 = r9 instanceof gnu.kawa.xml.BinaryObject
            if (r6 == 0) goto L_0x0167
            gnu.kawa.xml.Base64Binary r6 = new gnu.kawa.xml.Base64Binary
            gnu.kawa.xml.BinaryObject r9 = (gnu.kawa.xml.BinaryObject) r9
            byte[] r7 = r9.getBytes()
            r6.<init>(r7)
            r9 = r6
            goto L_0x000e
        L_0x0167:
            boolean r6 = r9 instanceof gnu.kawa.xml.BinaryObject
            if (r6 == 0) goto L_0x002a
            gnu.kawa.xml.HexBinary r6 = new gnu.kawa.xml.HexBinary
            gnu.kawa.xml.BinaryObject r9 = (gnu.kawa.xml.BinaryObject) r9
            byte[] r7 = r9.getBytes()
            r6.<init>(r7)
            r9 = r6
            goto L_0x000e
        */
        throw new UnsupportedOperationException("Method not decompiled: gnu.kawa.xml.XDataType.cast(java.lang.Object):java.lang.Object");
    }

    /* access modifiers changed from: package-private */
    public Duration castToDuration(Object value, Unit unit) {
        if (!(value instanceof Duration)) {
            return (Duration) coerceFromObject(value);
        }
        Duration dur = (Duration) value;
        if (dur.unit() == unit) {
            return dur;
        }
        int months = dur.getTotalMonths();
        long seconds = dur.getTotalSeconds();
        int nanos = dur.getNanoSecondsOnly();
        if (unit == Unit.second) {
            months = 0;
        }
        if (unit == Unit.month) {
            seconds = 0;
            nanos = 0;
        }
        return Duration.make(months, seconds, nanos, unit);
    }

    public Object coerceFromObject(Object obj) {
        if (isInstance(obj)) {
            return obj;
        }
        throw new ClassCastException("cannot cast " + obj + " to " + this.name);
    }

    public int compare(Type other) {
        if (this == other || ((this == stringStringType && other == stringType) || (this == stringType && other == stringStringType))) {
            return 0;
        }
        return this.implementationType.compare(other);
    }

    public Object valueOf(String value) {
        char ch;
        switch (this.typeCode) {
            case 4:
                String value2 = value.trim();
                int i = value2.length();
                do {
                    i--;
                    if (i < 0) {
                        return new BigDecimal(value2);
                    }
                    ch = value2.charAt(i);
                    if (ch != 'e') {
                    }
                    throw new IllegalArgumentException("not a valid decimal: '" + value2 + "'");
                } while (ch != 'E');
                throw new IllegalArgumentException("not a valid decimal: '" + value2 + "'");
            case 18:
            case 19:
                String value3 = value.trim();
                if ("INF".equals(value3)) {
                    value3 = "Infinity";
                } else if ("-INF".equals(value3)) {
                    value3 = "-Infinity";
                }
                return this.typeCode == 18 ? Float.valueOf(value3) : Double.valueOf(value3);
            case 28:
                return Duration.parseDuration(value);
            case 29:
                return Duration.parseYearMonthDuration(value);
            case 30:
                return Duration.parseDayTimeDuration(value);
            case 31:
                String value4 = value.trim();
                if (value4.equals("true") || value4.equals("1")) {
                    return Boolean.TRUE;
                }
                if (value4.equals("false") || value4.equals("0")) {
                    return Boolean.FALSE;
                }
                throw new IllegalArgumentException("not a valid boolean: '" + value4 + "'");
            case 33:
                return URIPath.makeURI(TextUtils.replaceWhitespace(value, true));
            case 34:
                return Base64Binary.valueOf(value);
            case 35:
                return HexBinary.valueOf(value);
            case 37:
                return new UntypedAtomic(value);
            case 38:
                return value;
            default:
                throw new RuntimeException("valueOf not implemented for " + this.name);
        }
    }

    public static Float makeFloat(float value) {
        return Float.valueOf(value);
    }

    public static Double makeDouble(double value) {
        return Double.valueOf(value);
    }

    public Procedure getConstructor() {
        return null;
    }
}
