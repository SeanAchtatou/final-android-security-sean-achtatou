package gnu.math;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class IntFraction extends RatNum implements Externalizable {
    IntNum den;
    IntNum num;

    IntFraction() {
    }

    public IntFraction(IntNum num2, IntNum den2) {
        this.num = num2;
        this.den = den2;
    }

    public final IntNum numerator() {
        return this.num;
    }

    public final IntNum denominator() {
        return this.den;
    }

    public final boolean isNegative() {
        return this.num.isNegative();
    }

    public final int sign() {
        return this.num.sign();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: gnu.math.RatNum.compare(gnu.math.RatNum, gnu.math.RatNum):int
     arg types: [gnu.math.IntFraction, gnu.math.RatNum]
     candidates:
      gnu.math.Complex.compare(gnu.math.Complex, gnu.math.Complex):int
      gnu.math.Quantity.compare(gnu.math.Quantity, gnu.math.Quantity):int
      gnu.math.RatNum.compare(gnu.math.RatNum, gnu.math.RatNum):int */
    public final int compare(Object obj) {
        if (obj instanceof RatNum) {
            return RatNum.compare((RatNum) this, (RatNum) obj);
        }
        return ((RealNum) obj).compareReversed(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: gnu.math.RatNum.compare(gnu.math.RatNum, gnu.math.RatNum):int
     arg types: [gnu.math.RatNum, gnu.math.IntFraction]
     candidates:
      gnu.math.Complex.compare(gnu.math.Complex, gnu.math.Complex):int
      gnu.math.Quantity.compare(gnu.math.Quantity, gnu.math.Quantity):int
      gnu.math.RatNum.compare(gnu.math.RatNum, gnu.math.RatNum):int */
    public int compareReversed(Numeric x) {
        return RatNum.compare((RatNum) x, (RatNum) this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: gnu.math.RatNum.add(gnu.math.RatNum, gnu.math.RatNum, int):gnu.math.RatNum
     arg types: [gnu.math.IntFraction, gnu.math.RatNum, int]
     candidates:
      gnu.math.RealNum.add(gnu.math.RealNum, gnu.math.RealNum, int):gnu.math.RealNum
      gnu.math.Complex.add(gnu.math.Complex, gnu.math.Complex, int):gnu.math.Complex
      gnu.math.Quantity.add(gnu.math.Quantity, gnu.math.Quantity, int):gnu.math.Quantity
      gnu.math.RatNum.add(gnu.math.RatNum, gnu.math.RatNum, int):gnu.math.RatNum */
    public Numeric add(Object y, int k) {
        if (y instanceof RatNum) {
            return RatNum.add((RatNum) this, (RatNum) y, k);
        }
        if (y instanceof Numeric) {
            return ((Numeric) y).addReversed(this, k);
        }
        throw new IllegalArgumentException();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: gnu.math.RatNum.add(gnu.math.RatNum, gnu.math.RatNum, int):gnu.math.RatNum
     arg types: [gnu.math.RatNum, gnu.math.IntFraction, int]
     candidates:
      gnu.math.RealNum.add(gnu.math.RealNum, gnu.math.RealNum, int):gnu.math.RealNum
      gnu.math.Complex.add(gnu.math.Complex, gnu.math.Complex, int):gnu.math.Complex
      gnu.math.Quantity.add(gnu.math.Quantity, gnu.math.Quantity, int):gnu.math.Quantity
      gnu.math.RatNum.add(gnu.math.RatNum, gnu.math.RatNum, int):gnu.math.RatNum */
    public Numeric addReversed(Numeric x, int k) {
        if (x instanceof RatNum) {
            return RatNum.add((RatNum) x, (RatNum) this, k);
        }
        throw new IllegalArgumentException();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: gnu.math.RatNum.times(gnu.math.RatNum, gnu.math.RatNum):gnu.math.RatNum
     arg types: [gnu.math.IntFraction, gnu.math.RatNum]
     candidates:
      gnu.math.RealNum.times(gnu.math.RealNum, gnu.math.RealNum):gnu.math.RealNum
      gnu.math.Complex.times(gnu.math.Complex, gnu.math.Complex):gnu.math.Complex
      gnu.math.Quantity.times(gnu.math.Quantity, gnu.math.Quantity):gnu.math.Quantity
      gnu.math.RatNum.times(gnu.math.RatNum, gnu.math.RatNum):gnu.math.RatNum */
    public Numeric mul(Object y) {
        if (y instanceof RatNum) {
            return RatNum.times((RatNum) this, (RatNum) y);
        }
        if (y instanceof Numeric) {
            return ((Numeric) y).mulReversed(this);
        }
        throw new IllegalArgumentException();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: gnu.math.RatNum.times(gnu.math.RatNum, gnu.math.RatNum):gnu.math.RatNum
     arg types: [gnu.math.RatNum, gnu.math.IntFraction]
     candidates:
      gnu.math.RealNum.times(gnu.math.RealNum, gnu.math.RealNum):gnu.math.RealNum
      gnu.math.Complex.times(gnu.math.Complex, gnu.math.Complex):gnu.math.Complex
      gnu.math.Quantity.times(gnu.math.Quantity, gnu.math.Quantity):gnu.math.Quantity
      gnu.math.RatNum.times(gnu.math.RatNum, gnu.math.RatNum):gnu.math.RatNum */
    public Numeric mulReversed(Numeric x) {
        if (x instanceof RatNum) {
            return RatNum.times((RatNum) x, (RatNum) this);
        }
        throw new IllegalArgumentException();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: gnu.math.RatNum.divide(gnu.math.RatNum, gnu.math.RatNum):gnu.math.RatNum
     arg types: [gnu.math.IntFraction, gnu.math.RatNum]
     candidates:
      gnu.math.RealNum.divide(gnu.math.RealNum, gnu.math.RealNum):gnu.math.RealNum
      gnu.math.Complex.divide(gnu.math.Complex, gnu.math.Complex):gnu.math.Complex
      gnu.math.Quantity.divide(gnu.math.Quantity, gnu.math.Quantity):gnu.math.Quantity
      gnu.math.RatNum.divide(gnu.math.RatNum, gnu.math.RatNum):gnu.math.RatNum */
    public Numeric div(Object y) {
        if (y instanceof RatNum) {
            return RatNum.divide((RatNum) this, (RatNum) y);
        }
        if (y instanceof Numeric) {
            return ((Numeric) y).divReversed(this);
        }
        throw new IllegalArgumentException();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: gnu.math.RatNum.divide(gnu.math.RatNum, gnu.math.RatNum):gnu.math.RatNum
     arg types: [gnu.math.RatNum, gnu.math.IntFraction]
     candidates:
      gnu.math.RealNum.divide(gnu.math.RealNum, gnu.math.RealNum):gnu.math.RealNum
      gnu.math.Complex.divide(gnu.math.Complex, gnu.math.Complex):gnu.math.Complex
      gnu.math.Quantity.divide(gnu.math.Quantity, gnu.math.Quantity):gnu.math.Quantity
      gnu.math.RatNum.divide(gnu.math.RatNum, gnu.math.RatNum):gnu.math.RatNum */
    public Numeric divReversed(Numeric x) {
        if (x instanceof RatNum) {
            return RatNum.divide((RatNum) x, (RatNum) this);
        }
        throw new IllegalArgumentException();
    }

    public static IntFraction neg(IntFraction x) {
        return new IntFraction(IntNum.neg(x.numerator()), x.denominator());
    }

    public Numeric neg() {
        return neg(this);
    }

    public long longValue() {
        return toExactInt(4).longValue();
    }

    public double doubleValue() {
        boolean neg = this.num.isNegative();
        if (!this.den.isZero()) {
            IntNum n = this.num;
            if (neg) {
                n = IntNum.neg(n);
            }
            int num_len = n.intLength();
            int den_len = this.den.intLength();
            int exp = 0;
            if (num_len < den_len + 54) {
                int exp2 = (den_len + 54) - num_len;
                n = IntNum.shift(n, exp2);
                exp = -exp2;
            }
            IntNum quot = new IntNum();
            IntNum remainder = new IntNum();
            IntNum.divide(n, this.den, quot, remainder, 3);
            return quot.canonicalize().roundToDouble(exp, neg, !remainder.canonicalize().isZero());
        } else if (neg) {
            return Double.NEGATIVE_INFINITY;
        } else {
            return this.num.isZero() ? Double.NaN : Double.POSITIVE_INFINITY;
        }
    }

    public String toString(int radix) {
        return this.num.toString(radix) + '/' + this.den.toString(radix);
    }

    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeObject(this.num);
        out.writeObject(this.den);
    }

    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        this.num = (IntNum) in.readObject();
        this.den = (IntNum) in.readObject();
    }
}
