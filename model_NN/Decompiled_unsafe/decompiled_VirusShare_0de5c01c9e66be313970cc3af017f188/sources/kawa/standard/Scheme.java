package kawa.standard;

import com.google.devtools.simple.runtime.annotations.DesignerProperty;
import gnu.bytecode.ArrayType;
import gnu.bytecode.ClassType;
import gnu.bytecode.Type;
import gnu.expr.ApplyExp;
import gnu.expr.Declaration;
import gnu.expr.Expression;
import gnu.expr.Language;
import gnu.expr.ReferenceExp;
import gnu.kawa.functions.Apply;
import gnu.kawa.functions.ApplyToArgs;
import gnu.kawa.functions.DisplayFormat;
import gnu.kawa.functions.GetNamedPart;
import gnu.kawa.functions.IsEq;
import gnu.kawa.functions.IsEqual;
import gnu.kawa.functions.IsEqv;
import gnu.kawa.functions.Map;
import gnu.kawa.functions.Not;
import gnu.kawa.functions.NumberCompare;
import gnu.kawa.functions.NumberPredicate;
import gnu.kawa.lispexpr.LangObjType;
import gnu.kawa.lispexpr.LangPrimType;
import gnu.kawa.lispexpr.LispLanguage;
import gnu.kawa.lispexpr.LispReader;
import gnu.kawa.lispexpr.ReadTable;
import gnu.kawa.lispexpr.ReaderDispatch;
import gnu.kawa.lispexpr.ReaderDispatchMisc;
import gnu.kawa.lispexpr.ReaderParens;
import gnu.kawa.lispexpr.ReaderQuote;
import gnu.kawa.reflect.InstanceOf;
import gnu.kawa.servlet.HttpRequestContext;
import gnu.lists.AbstractFormat;
import gnu.mapping.CharArrayInPort;
import gnu.mapping.Environment;
import gnu.mapping.InPort;
import gnu.mapping.Namespace;
import gnu.mapping.SimpleEnvironment;
import gnu.mapping.Symbol;
import gnu.mapping.WrappedException;
import gnu.text.SourceMessages;
import gnu.text.SyntaxException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import kawa.lang.Eval;

public class Scheme extends LispLanguage {
    public static final Apply apply = new Apply("apply", applyToArgs);
    static final Declaration applyFieldDecl = Declaration.getDeclarationFromStatic("kawa.standard.Scheme", "applyToArgs");
    public static final ApplyToArgs applyToArgs = new ApplyToArgs("apply-to-args", instance);
    public static LangPrimType booleanType;
    public static final AbstractFormat displayFormat = new DisplayFormat(false, 'S');
    public static final Map forEach = new Map(false, applyToArgs, applyFieldDecl, isEq);
    public static final Scheme instance = new Scheme(kawaEnvironment);
    public static final InstanceOf instanceOf = new InstanceOf(instance, GetNamedPart.INSTANCEOF_METHOD_NAME);
    public static final IsEq isEq = new IsEq(instance, "eq?");
    public static final IsEqual isEqual = new IsEqual(instance, "equal?");
    public static final IsEqv isEqv = new IsEqv(instance, "eqv?", isEq);
    public static final NumberPredicate isEven = new NumberPredicate(instance, "even?", 2);
    public static final NumberPredicate isOdd = new NumberPredicate(instance, "odd?", 1);
    protected static final SimpleEnvironment kawaEnvironment = Environment.make("kawa-environment", r5Environment);
    public static final Map map = new Map(true, applyToArgs, applyFieldDecl, isEq);
    public static final Not not = new Not(instance, "not");
    public static final Environment nullEnvironment = Environment.make("null-environment");
    public static final NumberCompare numEqu = NumberCompare.make(instance, "=", 8);
    public static final NumberCompare numGEq = NumberCompare.make(instance, ">=", 24);
    public static final NumberCompare numGrt = NumberCompare.make(instance, ">", 16);
    public static final NumberCompare numLEq = NumberCompare.make(instance, "<=", 12);
    public static final NumberCompare numLss = NumberCompare.make(instance, "<", 4);
    public static final Environment r4Environment = Environment.make("r4rs-environment", nullEnvironment);
    public static final Environment r5Environment = Environment.make("r5rs-environment", r4Environment);
    static HashMap<Type, String> typeToStringMap;
    static HashMap<String, Type> types;
    public static final Namespace unitNamespace = Namespace.valueOf("http://kawa.gnu.org/unit", "unit");
    public static final AbstractFormat writeFormat = new DisplayFormat(true, 'S');

    static {
        String str;
        instance.initScheme();
        int withServlets = HttpRequestContext.importServletDefinitions;
        if (withServlets > 0) {
            try {
                Scheme scheme = instance;
                if (withServlets > 1) {
                    str = "gnu.kawa.servlet.servlets";
                } else {
                    str = "gnu.kawa.servlet.HTTP";
                }
                scheme.loadClass(str);
            } catch (Throwable th) {
            }
        }
    }

    public static Scheme getInstance() {
        return instance;
    }

    public static Environment builtin() {
        return kawaEnvironment;
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [kawa.standard.Scheme] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private void initScheme() {
        /*
            r4 = this;
            gnu.mapping.Environment r0 = kawa.standard.Scheme.nullEnvironment
            r4.environ = r0
            gnu.mapping.Environment r0 = r4.environ
            gnu.mapping.Symbol r1 = gnu.kawa.lispexpr.LispLanguage.lookup_sym
            r2 = 0
            gnu.kawa.reflect.StaticFieldLocation r3 = kawa.standard.Scheme.getNamedPartLocation
            r0.addLocation(r1, r2, r3)
            java.lang.String r0 = "lambda"
            java.lang.String r1 = "kawa.standard.SchemeCompilation"
            java.lang.String r2 = "lambda"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "quote"
            java.lang.String r1 = "kawa.lang.Quote"
            java.lang.String r2 = "plainQuote"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "%define"
            java.lang.String r1 = "kawa.standard.define"
            java.lang.String r2 = "defineRaw"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "define"
            java.lang.String r1 = "kawa.lib.prim_syntax"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "if"
            java.lang.String r1 = "kawa.lib.prim_syntax"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "set!"
            java.lang.String r1 = "kawa.standard.set_b"
            java.lang.String r2 = "set"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "cond"
            java.lang.String r1 = "kawa.lib.std_syntax"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "case"
            java.lang.String r1 = "kawa.lib.std_syntax"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "and"
            java.lang.String r1 = "kawa.lib.std_syntax"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "or"
            java.lang.String r1 = "kawa.lib.std_syntax"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "%let"
            java.lang.String r1 = "kawa.standard.let"
            java.lang.String r2 = "let"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "let"
            java.lang.String r1 = "kawa.lib.std_syntax"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "let*"
            java.lang.String r1 = "kawa.lib.std_syntax"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "letrec"
            java.lang.String r1 = "kawa.lib.prim_syntax"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "begin"
            java.lang.String r1 = "kawa.standard.begin"
            java.lang.String r2 = "begin"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "do"
            java.lang.String r1 = "kawa.lib.std_syntax"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "delay"
            java.lang.String r1 = "kawa.lib.std_syntax"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "%make-promise"
            java.lang.String r1 = "kawa.lib.std_syntax"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "quasiquote"
            java.lang.String r1 = "kawa.lang.Quote"
            java.lang.String r2 = "quasiQuote"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "define-syntax"
            java.lang.String r1 = "kawa.lib.prim_syntax"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "let-syntax"
            java.lang.String r1 = "kawa.standard.let_syntax"
            java.lang.String r2 = "let_syntax"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "letrec-syntax"
            java.lang.String r1 = "kawa.standard.let_syntax"
            java.lang.String r2 = "letrec_syntax"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "syntax-rules"
            java.lang.String r1 = "kawa.standard.syntax_rules"
            java.lang.String r2 = "syntax_rules"
            r4.defSntxStFld(r0, r1, r2)
            gnu.mapping.Environment r0 = kawa.standard.Scheme.nullEnvironment
            r0.setLocked()
            gnu.mapping.Environment r0 = kawa.standard.Scheme.r4Environment
            r4.environ = r0
            java.lang.String r0 = "not"
            java.lang.String r1 = "kawa.standard.Scheme"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "boolean?"
            java.lang.String r1 = "kawa.lib.misc"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "eq?"
            java.lang.String r1 = "kawa.standard.Scheme"
            java.lang.String r2 = "isEq"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "eqv?"
            java.lang.String r1 = "kawa.standard.Scheme"
            java.lang.String r2 = "isEqv"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "equal?"
            java.lang.String r1 = "kawa.standard.Scheme"
            java.lang.String r2 = "isEqual"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "pair?"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "cons"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "car"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "cdr"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "set-car!"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "set-cdr!"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "caar"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "cadr"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "cdar"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "cddr"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "caaar"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "caadr"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "cadar"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "caddr"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "cdaar"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "cdadr"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "cddar"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "cdddr"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "caaaar"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "caaadr"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "caadar"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "caaddr"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "cadaar"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "cadadr"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "caddar"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "cadddr"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "cdaaar"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "cdaadr"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "cdadar"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "cdaddr"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "cddaar"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "cddadr"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "cdddar"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "cddddr"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "null?"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "list?"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "length"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "append"
            java.lang.String r1 = "kawa.standard.append"
            java.lang.String r2 = "append"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "reverse"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "reverse!"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "list-tail"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "list-ref"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "memq"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "memv"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "member"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "assq"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "assv"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "assoc"
            java.lang.String r1 = "kawa.lib.lists"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "symbol?"
            java.lang.String r1 = "kawa.lib.misc"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "symbol->string"
            java.lang.String r1 = "kawa.lib.misc"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "string->symbol"
            java.lang.String r1 = "kawa.lib.misc"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "symbol=?"
            java.lang.String r1 = "kawa.lib.misc"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "symbol-local-name"
            java.lang.String r1 = "kawa.lib.misc"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "symbol-namespace"
            java.lang.String r1 = "kawa.lib.misc"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "symbol-namespace-uri"
            java.lang.String r1 = "kawa.lib.misc"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "symbol-prefix"
            java.lang.String r1 = "kawa.lib.misc"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "namespace-uri"
            java.lang.String r1 = "kawa.lib.misc"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "namespace-prefix"
            java.lang.String r1 = "kawa.lib.misc"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "number?"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "quantity?"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "complex?"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "real?"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "rational?"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "integer?"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "exact?"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "inexact?"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "="
            java.lang.String r1 = "kawa.standard.Scheme"
            java.lang.String r2 = "numEqu"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "<"
            java.lang.String r1 = "kawa.standard.Scheme"
            java.lang.String r2 = "numLss"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = ">"
            java.lang.String r1 = "kawa.standard.Scheme"
            java.lang.String r2 = "numGrt"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "<="
            java.lang.String r1 = "kawa.standard.Scheme"
            java.lang.String r2 = "numLEq"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = ">="
            java.lang.String r1 = "kawa.standard.Scheme"
            java.lang.String r2 = "numGEq"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "zero?"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "positive?"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "negative?"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "odd?"
            java.lang.String r1 = "kawa.standard.Scheme"
            java.lang.String r2 = "isOdd"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "even?"
            java.lang.String r1 = "kawa.standard.Scheme"
            java.lang.String r2 = "isEven"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "max"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "min"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "+"
            java.lang.String r1 = "gnu.kawa.functions.AddOp"
            java.lang.String r2 = "$Pl"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "-"
            java.lang.String r1 = "gnu.kawa.functions.AddOp"
            java.lang.String r2 = "$Mn"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "*"
            java.lang.String r1 = "gnu.kawa.functions.MultiplyOp"
            java.lang.String r2 = "$St"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "/"
            java.lang.String r1 = "gnu.kawa.functions.DivideOp"
            java.lang.String r2 = "$Sl"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "abs"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "quotient"
            java.lang.String r1 = "gnu.kawa.functions.DivideOp"
            java.lang.String r2 = "quotient"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "remainder"
            java.lang.String r1 = "gnu.kawa.functions.DivideOp"
            java.lang.String r2 = "remainder"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "modulo"
            java.lang.String r1 = "gnu.kawa.functions.DivideOp"
            java.lang.String r2 = "modulo"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "div"
            java.lang.String r1 = "gnu.kawa.functions.DivideOp"
            java.lang.String r2 = "div"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "mod"
            java.lang.String r1 = "gnu.kawa.functions.DivideOp"
            java.lang.String r2 = "mod"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "div0"
            java.lang.String r1 = "gnu.kawa.functions.DivideOp"
            java.lang.String r2 = "div0"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "mod0"
            java.lang.String r1 = "gnu.kawa.functions.DivideOp"
            java.lang.String r2 = "mod0"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "div-and-mod"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "div0-and-mod0"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "gcd"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "lcm"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "numerator"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "denominator"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "floor"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "ceiling"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "truncate"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "round"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "rationalize"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "exp"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "log"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "sin"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "cos"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "tan"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "asin"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "acos"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "atan"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "sqrt"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "expt"
            java.lang.String r1 = "kawa.standard.expt"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "make-rectangular"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "make-polar"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "real-part"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "imag-part"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "magnitude"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "angle"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "inexact"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "exact"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "exact->inexact"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "inexact->exact"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "number->string"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "string->number"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "char?"
            java.lang.String r1 = "kawa.lib.characters"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "char=?"
            java.lang.String r1 = "kawa.lib.characters"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "char<?"
            java.lang.String r1 = "kawa.lib.characters"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "char>?"
            java.lang.String r1 = "kawa.lib.characters"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "char<=?"
            java.lang.String r1 = "kawa.lib.characters"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "char>=?"
            java.lang.String r1 = "kawa.lib.characters"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "char-ci=?"
            java.lang.String r1 = "kawa.lib.rnrs.unicode"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "char-ci<?"
            java.lang.String r1 = "kawa.lib.rnrs.unicode"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "char-ci>?"
            java.lang.String r1 = "kawa.lib.rnrs.unicode"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "char-ci<=?"
            java.lang.String r1 = "kawa.lib.rnrs.unicode"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "char-ci>=?"
            java.lang.String r1 = "kawa.lib.rnrs.unicode"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "char-alphabetic?"
            java.lang.String r1 = "kawa.lib.rnrs.unicode"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "char-numeric?"
            java.lang.String r1 = "kawa.lib.rnrs.unicode"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "char-whitespace?"
            java.lang.String r1 = "kawa.lib.rnrs.unicode"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "char-upper-case?"
            java.lang.String r1 = "kawa.lib.rnrs.unicode"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "char-lower-case?"
            java.lang.String r1 = "kawa.lib.rnrs.unicode"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "char-title-case?"
            java.lang.String r1 = "kawa.lib.rnrs.unicode"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "char->integer"
            java.lang.String r1 = "kawa.lib.characters"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "integer->char"
            java.lang.String r1 = "kawa.lib.characters"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "char-upcase"
            java.lang.String r1 = "kawa.lib.rnrs.unicode"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "char-downcase"
            java.lang.String r1 = "kawa.lib.rnrs.unicode"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "char-titlecase"
            java.lang.String r1 = "kawa.lib.rnrs.unicode"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "char-foldcase"
            java.lang.String r1 = "kawa.lib.rnrs.unicode"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "char-general-category"
            java.lang.String r1 = "kawa.lib.rnrs.unicode"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "string?"
            java.lang.String r1 = "kawa.lib.strings"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "make-string"
            java.lang.String r1 = "kawa.lib.strings"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "string-length"
            java.lang.String r1 = "kawa.lib.strings"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "string-ref"
            java.lang.String r1 = "kawa.lib.strings"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "string-set!"
            java.lang.String r1 = "kawa.lib.strings"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "string=?"
            java.lang.String r1 = "kawa.lib.strings"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "string<?"
            java.lang.String r1 = "kawa.lib.strings"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "string>?"
            java.lang.String r1 = "kawa.lib.strings"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "string<=?"
            java.lang.String r1 = "kawa.lib.strings"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "string>=?"
            java.lang.String r1 = "kawa.lib.strings"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "string-ci=?"
            java.lang.String r1 = "kawa.lib.rnrs.unicode"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "string-ci<?"
            java.lang.String r1 = "kawa.lib.rnrs.unicode"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "string-ci>?"
            java.lang.String r1 = "kawa.lib.rnrs.unicode"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "string-ci<=?"
            java.lang.String r1 = "kawa.lib.rnrs.unicode"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "string-ci>=?"
            java.lang.String r1 = "kawa.lib.rnrs.unicode"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "string-normalize-nfd"
            java.lang.String r1 = "kawa.lib.rnrs.unicode"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "string-normalize-nfkd"
            java.lang.String r1 = "kawa.lib.rnrs.unicode"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "string-normalize-nfc"
            java.lang.String r1 = "kawa.lib.rnrs.unicode"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "string-normalize-nfkc"
            java.lang.String r1 = "kawa.lib.rnrs.unicode"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "substring"
            java.lang.String r1 = "kawa.lib.strings"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "string-append"
            java.lang.String r1 = "kawa.lib.strings"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "string-append/shared"
            java.lang.String r1 = "kawa.lib.strings"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "string->list"
            java.lang.String r1 = "kawa.lib.strings"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "list->string"
            java.lang.String r1 = "kawa.lib.strings"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "string-copy"
            java.lang.String r1 = "kawa.lib.strings"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "string-fill!"
            java.lang.String r1 = "kawa.lib.strings"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "vector?"
            java.lang.String r1 = "kawa.lib.vectors"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "make-vector"
            java.lang.String r1 = "kawa.lib.vectors"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "vector-length"
            java.lang.String r1 = "kawa.lib.vectors"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "vector-ref"
            java.lang.String r1 = "kawa.lib.vectors"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "vector-set!"
            java.lang.String r1 = "kawa.lib.vectors"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "list->vector"
            java.lang.String r1 = "kawa.lib.vectors"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "vector->list"
            java.lang.String r1 = "kawa.lib.vectors"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "vector-fill!"
            java.lang.String r1 = "kawa.lib.vectors"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "vector-append"
            java.lang.String r1 = "kawa.standard.vector_append"
            java.lang.String r2 = "vectorAppend"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "values-append"
            java.lang.String r1 = "gnu.kawa.functions.AppendValues"
            java.lang.String r2 = "appendValues"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "procedure?"
            java.lang.String r1 = "kawa.lib.misc"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "apply"
            java.lang.String r1 = "kawa.standard.Scheme"
            java.lang.String r2 = "apply"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "map"
            java.lang.String r1 = "kawa.standard.Scheme"
            java.lang.String r2 = "map"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "for-each"
            java.lang.String r1 = "kawa.standard.Scheme"
            java.lang.String r2 = "forEach"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "call-with-current-continuation"
            java.lang.String r1 = "gnu.kawa.functions.CallCC"
            java.lang.String r2 = "callcc"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "call/cc"
            java.lang.String r1 = "kawa.standard.callcc"
            java.lang.String r2 = "callcc"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "force"
            java.lang.String r1 = "kawa.lib.misc"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "call-with-input-file"
            java.lang.String r1 = "kawa.lib.ports"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "call-with-output-file"
            java.lang.String r1 = "kawa.lib.ports"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "input-port?"
            java.lang.String r1 = "kawa.lib.ports"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "output-port?"
            java.lang.String r1 = "kawa.lib.ports"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "current-input-port"
            java.lang.String r1 = "kawa.lib.ports"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "current-output-port"
            java.lang.String r1 = "kawa.lib.ports"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "with-input-from-file"
            java.lang.String r1 = "kawa.lib.ports"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "with-output-to-file"
            java.lang.String r1 = "kawa.lib.ports"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "open-input-file"
            java.lang.String r1 = "kawa.lib.ports"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "open-output-file"
            java.lang.String r1 = "kawa.lib.ports"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "close-input-port"
            java.lang.String r1 = "kawa.lib.ports"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "close-output-port"
            java.lang.String r1 = "kawa.lib.ports"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "read"
            java.lang.String r1 = "kawa.lib.ports"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "read-line"
            java.lang.String r1 = "kawa.lib.ports"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "read-char"
            java.lang.String r1 = "kawa.standard.readchar"
            java.lang.String r2 = "readChar"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "peek-char"
            java.lang.String r1 = "kawa.standard.readchar"
            java.lang.String r2 = "peekChar"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "eof-object?"
            java.lang.String r1 = "kawa.lib.ports"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "char-ready?"
            java.lang.String r1 = "kawa.lib.ports"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "write"
            java.lang.String r1 = "kawa.lib.ports"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "display"
            java.lang.String r1 = "kawa.lib.ports"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "print-as-xml"
            java.lang.String r1 = "gnu.xquery.lang.XQuery"
            java.lang.String r2 = "writeFormat"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "write-char"
            java.lang.String r1 = "kawa.lib.ports"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "newline"
            java.lang.String r1 = "kawa.lib.ports"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "load"
            java.lang.String r1 = "kawa.standard.load"
            java.lang.String r2 = "load"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "load-relative"
            java.lang.String r1 = "kawa.standard.load"
            java.lang.String r2 = "loadRelative"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "transcript-off"
            java.lang.String r1 = "kawa.lib.ports"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "transcript-on"
            java.lang.String r1 = "kawa.lib.ports"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "call-with-input-string"
            java.lang.String r1 = "kawa.lib.ports"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "open-input-string"
            java.lang.String r1 = "kawa.lib.ports"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "open-output-string"
            java.lang.String r1 = "kawa.lib.ports"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "get-output-string"
            java.lang.String r1 = "kawa.lib.ports"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "call-with-output-string"
            java.lang.String r1 = "kawa.lib.ports"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "force-output"
            java.lang.String r1 = "kawa.lib.ports"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "port-line"
            java.lang.String r1 = "kawa.lib.ports"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "set-port-line!"
            java.lang.String r1 = "kawa.lib.ports"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "port-column"
            java.lang.String r1 = "kawa.lib.ports"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "current-error-port"
            java.lang.String r1 = "kawa.lib.ports"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "input-port-line-number"
            java.lang.String r1 = "kawa.lib.ports"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "set-input-port-line-number!"
            java.lang.String r1 = "kawa.lib.ports"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "input-port-column-number"
            java.lang.String r1 = "kawa.lib.ports"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "input-port-read-state"
            java.lang.String r1 = "kawa.lib.ports"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "default-prompter"
            java.lang.String r1 = "kawa.lib.ports"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "input-port-prompter"
            java.lang.String r1 = "kawa.lib.ports"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "set-input-port-prompter!"
            java.lang.String r1 = "kawa.lib.ports"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "base-uri"
            java.lang.String r1 = "kawa.lib.misc"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "%syntax-error"
            java.lang.String r1 = "kawa.standard.syntax_error"
            java.lang.String r2 = "syntax_error"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "syntax-error"
            java.lang.String r1 = "kawa.lib.prim_syntax"
            r4.defProcStFld(r0, r1)
            gnu.mapping.Environment r0 = kawa.standard.Scheme.r4Environment
            r0.setLocked()
            gnu.mapping.Environment r0 = kawa.standard.Scheme.r5Environment
            r4.environ = r0
            java.lang.String r0 = "values"
            java.lang.String r1 = "kawa.lib.misc"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "call-with-values"
            java.lang.String r1 = "kawa.standard.call_with_values"
            java.lang.String r2 = "callWithValues"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "let-values"
            java.lang.String r1 = "kawa.lib.syntax"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "let*-values"
            java.lang.String r1 = "kawa.lib.syntax"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "case-lambda"
            java.lang.String r1 = "kawa.lib.syntax"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "receive"
            java.lang.String r1 = "kawa.lib.syntax"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "eval"
            java.lang.String r1 = "kawa.lang.Eval"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "repl"
            java.lang.String r1 = "kawa.standard.SchemeCompilation"
            java.lang.String r2 = "repl"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "scheme-report-environment"
            java.lang.String r1 = "kawa.lib.misc"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "null-environment"
            java.lang.String r1 = "kawa.lib.misc"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "interaction-environment"
            java.lang.String r1 = "kawa.lib.misc"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "dynamic-wind"
            java.lang.String r1 = "kawa.lib.misc"
            r4.defProcStFld(r0, r1)
            gnu.mapping.Environment r0 = kawa.standard.Scheme.r5Environment
            r0.setLocked()
            gnu.mapping.SimpleEnvironment r0 = kawa.standard.Scheme.kawaEnvironment
            r4.environ = r0
            java.lang.String r0 = "define-private"
            java.lang.String r1 = "kawa.lib.prim_syntax"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "define-constant"
            java.lang.String r1 = "kawa.lib.prim_syntax"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "define-autoload"
            java.lang.String r1 = "kawa.standard.define_autoload"
            java.lang.String r2 = "define_autoload"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "define-autoloads-from-file"
            java.lang.String r1 = "kawa.standard.define_autoload"
            java.lang.String r2 = "define_autoloads_from_file"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "exit"
            java.lang.String r1 = "kawa.lib.rnrs.programs"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "command-line"
            java.lang.String r1 = "kawa.lib.rnrs.programs"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "bitwise-arithmetic-shift"
            java.lang.String r1 = "gnu.kawa.functions.BitwiseOp"
            java.lang.String r2 = "ashift"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "arithmetic-shift"
            java.lang.String r1 = "gnu.kawa.functions.BitwiseOp"
            java.lang.String r2 = "ashift"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "ash"
            java.lang.String r1 = "gnu.kawa.functions.BitwiseOp"
            java.lang.String r2 = "ashift"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "bitwise-arithmetic-shift-left"
            java.lang.String r1 = "gnu.kawa.functions.BitwiseOp"
            java.lang.String r2 = "ashiftl"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "bitwise-arithmetic-shift-right"
            java.lang.String r1 = "gnu.kawa.functions.BitwiseOp"
            java.lang.String r2 = "ashiftr"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "bitwise-and"
            java.lang.String r1 = "gnu.kawa.functions.BitwiseOp"
            java.lang.String r2 = "and"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "logand"
            java.lang.String r1 = "gnu.kawa.functions.BitwiseOp"
            java.lang.String r2 = "and"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "bitwise-ior"
            java.lang.String r1 = "gnu.kawa.functions.BitwiseOp"
            java.lang.String r2 = "ior"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "logior"
            java.lang.String r1 = "gnu.kawa.functions.BitwiseOp"
            java.lang.String r2 = "ior"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "bitwise-xor"
            java.lang.String r1 = "gnu.kawa.functions.BitwiseOp"
            java.lang.String r2 = "xor"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "logxor"
            java.lang.String r1 = "gnu.kawa.functions.BitwiseOp"
            java.lang.String r2 = "xor"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "bitwise-if"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "bitwise-not"
            java.lang.String r1 = "gnu.kawa.functions.BitwiseOp"
            java.lang.String r2 = "not"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "lognot"
            java.lang.String r1 = "gnu.kawa.functions.BitwiseOp"
            java.lang.String r2 = "not"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "logop"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "bitwise-bit-set?"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "logbit?"
            java.lang.String r1 = "kawa.lib.numbers"
            java.lang.String r2 = "bitwise-bit-set?"
            java.lang.String r2 = gnu.expr.Compilation.mangleNameIfNeeded(r2)
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "logtest"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "bitwise-bit-count"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "logcount"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "bitwise-copy-bit"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "bitwise-copy-bit-field"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "bitwise-bit-field"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "bit-extract"
            java.lang.String r1 = "kawa.lib.numbers"
            java.lang.String r2 = "bitwise-bit-field"
            java.lang.String r2 = gnu.expr.Compilation.mangleNameIfNeeded(r2)
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "bitwise-length"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "integer-length"
            java.lang.String r1 = "kawa.lib.numbers"
            java.lang.String r2 = "bitwise$Mnlength"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "bitwise-first-bit-set"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "bitwise-rotate-bit-field"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "bitwise-reverse-bit-field"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "string-upcase!"
            java.lang.String r1 = "kawa.lib.strings"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "string-downcase!"
            java.lang.String r1 = "kawa.lib.strings"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "string-capitalize!"
            java.lang.String r1 = "kawa.lib.strings"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "string-upcase"
            java.lang.String r1 = "kawa.lib.rnrs.unicode"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "string-downcase"
            java.lang.String r1 = "kawa.lib.rnrs.unicode"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "string-titlecase"
            java.lang.String r1 = "kawa.lib.rnrs.unicode"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "string-foldcase"
            java.lang.String r1 = "kawa.lib.rnrs.unicode"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "string-capitalize"
            java.lang.String r1 = "kawa.lib.strings"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "primitive-virtual-method"
            java.lang.String r1 = "kawa.standard.prim_method"
            java.lang.String r2 = "virtual_method"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "primitive-static-method"
            java.lang.String r1 = "kawa.standard.prim_method"
            java.lang.String r2 = "static_method"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "primitive-interface-method"
            java.lang.String r1 = "kawa.standard.prim_method"
            java.lang.String r2 = "interface_method"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "primitive-constructor"
            java.lang.String r1 = "kawa.lib.reflection"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "primitive-op1"
            java.lang.String r1 = "kawa.standard.prim_method"
            java.lang.String r2 = "op1"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "primitive-get-field"
            java.lang.String r1 = "kawa.lib.reflection"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "primitive-set-field"
            java.lang.String r1 = "kawa.lib.reflection"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "primitive-get-static"
            java.lang.String r1 = "kawa.lib.reflection"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "primitive-set-static"
            java.lang.String r1 = "kawa.lib.reflection"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "primitive-array-new"
            java.lang.String r1 = "kawa.lib.reflection"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "primitive-array-get"
            java.lang.String r1 = "kawa.lib.reflection"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "primitive-array-set"
            java.lang.String r1 = "kawa.lib.reflection"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "primitive-array-length"
            java.lang.String r1 = "kawa.lib.reflection"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "subtype?"
            java.lang.String r1 = "kawa.lib.reflection"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "primitive-throw"
            java.lang.String r1 = "kawa.standard.prim_throw"
            java.lang.String r2 = "primitiveThrow"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "try-finally"
            java.lang.String r1 = "kawa.lib.syntax"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "try-catch"
            java.lang.String r1 = "kawa.lib.prim_syntax"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "throw"
            java.lang.String r1 = "kawa.standard.throw_name"
            java.lang.String r2 = "throwName"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "catch"
            java.lang.String r1 = "kawa.lib.system"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "error"
            java.lang.String r1 = "kawa.lib.misc"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "as"
            java.lang.String r1 = "gnu.kawa.functions.Convert"
            java.lang.String r2 = "as"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "instance?"
            java.lang.String r1 = "kawa.standard.Scheme"
            java.lang.String r2 = "instanceOf"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "synchronized"
            java.lang.String r1 = "kawa.lib.syntax"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "object"
            java.lang.String r1 = "kawa.standard.object"
            java.lang.String r2 = "objectSyntax"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "define-class"
            java.lang.String r1 = "kawa.standard.define_class"
            java.lang.String r2 = "define_class"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "define-simple-class"
            java.lang.String r1 = "kawa.standard.define_class"
            java.lang.String r2 = "define_simple_class"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "this"
            java.lang.String r1 = "kawa.standard.thisRef"
            java.lang.String r2 = "thisSyntax"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "make"
            java.lang.String r1 = "gnu.kawa.reflect.Invoke"
            java.lang.String r2 = "make"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "slot-ref"
            java.lang.String r1 = "gnu.kawa.reflect.SlotGet"
            java.lang.String r2 = "slotRef"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "slot-set!"
            java.lang.String r1 = "gnu.kawa.reflect.SlotSet"
            java.lang.String r2 = "set$Mnfield$Ex"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "field"
            java.lang.String r1 = "gnu.kawa.reflect.SlotGet"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "class-methods"
            java.lang.String r1 = "gnu.kawa.reflect.ClassMethods"
            java.lang.String r2 = "classMethods"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "static-field"
            java.lang.String r1 = "gnu.kawa.reflect.SlotGet"
            java.lang.String r2 = "staticField"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "invoke"
            java.lang.String r1 = "gnu.kawa.reflect.Invoke"
            java.lang.String r2 = "invoke"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "invoke-static"
            java.lang.String r1 = "gnu.kawa.reflect.Invoke"
            java.lang.String r2 = "invokeStatic"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "invoke-special"
            java.lang.String r1 = "gnu.kawa.reflect.Invoke"
            java.lang.String r2 = "invokeSpecial"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "define-macro"
            java.lang.String r1 = "kawa.lib.syntax"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "%define-macro"
            java.lang.String r1 = "kawa.standard.define_syntax"
            java.lang.String r2 = "define_macro"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "define-syntax-case"
            java.lang.String r1 = "kawa.lib.syntax"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "syntax-case"
            java.lang.String r1 = "kawa.standard.syntax_case"
            java.lang.String r2 = "syntax_case"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "%define-syntax"
            java.lang.String r1 = "kawa.standard.define_syntax"
            java.lang.String r2 = "define_syntax"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "syntax"
            java.lang.String r1 = "kawa.standard.syntax"
            java.lang.String r2 = "syntax"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "quasisyntax"
            java.lang.String r1 = "kawa.standard.syntax"
            java.lang.String r2 = "quasiSyntax"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "syntax-object->datum"
            java.lang.String r1 = "kawa.lib.std_syntax"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "datum->syntax-object"
            java.lang.String r1 = "kawa.lib.std_syntax"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "syntax->expression"
            java.lang.String r1 = "kawa.lib.prim_syntax"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "syntax-body->expression"
            java.lang.String r1 = "kawa.lib.prim_syntax"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "generate-temporaries"
            java.lang.String r1 = "kawa.lib.std_syntax"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "with-syntax"
            java.lang.String r1 = "kawa.lib.std_syntax"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "identifier?"
            java.lang.String r1 = "kawa.lib.std_syntax"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "free-identifier=?"
            java.lang.String r1 = "kawa.lib.std_syntax"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "syntax-source"
            java.lang.String r1 = "kawa.lib.std_syntax"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "syntax-line"
            java.lang.String r1 = "kawa.lib.std_syntax"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "syntax-column"
            java.lang.String r1 = "kawa.lib.std_syntax"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "begin-for-syntax"
            java.lang.String r1 = "kawa.lib.std_syntax"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "define-for-syntax"
            java.lang.String r1 = "kawa.lib.std_syntax"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "include"
            java.lang.String r1 = "kawa.lib.misc_syntax"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "include-relative"
            java.lang.String r1 = "kawa.lib.misc_syntax"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "file-exists?"
            java.lang.String r1 = "kawa.lib.files"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "file-directory?"
            java.lang.String r1 = "kawa.lib.files"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "file-readable?"
            java.lang.String r1 = "kawa.lib.files"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "file-writable?"
            java.lang.String r1 = "kawa.lib.files"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "delete-file"
            java.lang.String r1 = "kawa.lib.files"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "system-tmpdir"
            java.lang.String r1 = "kawa.lib.files"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "make-temporary-file"
            java.lang.String r1 = "kawa.lib.files"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "rename-file"
            java.lang.String r1 = "kawa.lib.files"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "copy-file"
            java.lang.String r1 = "kawa.lib.files"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "create-directory"
            java.lang.String r1 = "kawa.lib.files"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "->pathname"
            java.lang.String r1 = "kawa.lib.files"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "port-char-encoding"
            java.lang.Boolean r1 = java.lang.Boolean.TRUE
            r4.define(r0, r1)
            java.lang.String r0 = "symbol-read-case"
            java.lang.String r1 = "P"
            r4.define(r0, r1)
            java.lang.String r0 = "system"
            java.lang.String r1 = "kawa.lib.system"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "make-process"
            java.lang.String r1 = "kawa.lib.system"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "tokenize-string-to-string-array"
            java.lang.String r1 = "kawa.lib.system"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "tokenize-string-using-shell"
            java.lang.String r1 = "kawa.lib.system"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "command-parse"
            java.lang.String r1 = "kawa.lib.system"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "process-command-line-assignments"
            java.lang.String r1 = "kawa.lib.system"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "record-accessor"
            java.lang.String r1 = "kawa.lib.reflection"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "record-modifier"
            java.lang.String r1 = "kawa.lib.reflection"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "record-predicate"
            java.lang.String r1 = "kawa.lib.reflection"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "record-constructor"
            java.lang.String r1 = "kawa.lib.reflection"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "make-record-type"
            java.lang.String r1 = "kawa.lib.reflection"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "record-type-descriptor"
            java.lang.String r1 = "kawa.lib.reflection"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "record-type-name"
            java.lang.String r1 = "kawa.lib.reflection"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "record-type-field-names"
            java.lang.String r1 = "kawa.lib.reflection"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "record?"
            java.lang.String r1 = "kawa.lib.reflection"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "define-record-type"
            java.lang.String r1 = "gnu.kawa.slib.DefineRecordType"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "when"
            java.lang.String r1 = "kawa.lib.syntax"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "unless"
            java.lang.String r1 = "kawa.lib.syntax"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "fluid-let"
            java.lang.String r1 = "kawa.standard.fluid_let"
            java.lang.String r2 = "fluid_let"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "constant-fold"
            java.lang.String r1 = "kawa.standard.constant_fold"
            java.lang.String r2 = "constant_fold"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "make-parameter"
            java.lang.String r1 = "kawa.lib.parameters"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "parameterize"
            java.lang.String r1 = "kawa.lib.parameters"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "compile-file"
            java.lang.String r1 = "kawa.lib.system"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "environment-bound?"
            java.lang.String r1 = "kawa.lib.misc"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "scheme-implementation-version"
            java.lang.String r1 = "kawa.lib.misc"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "scheme-window"
            java.lang.String r1 = "kawa.lib.windows"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "define-procedure"
            java.lang.String r1 = "kawa.lib.std_syntax"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "add-procedure-properties"
            java.lang.String r1 = "kawa.lib.misc"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "make-procedure"
            java.lang.String r1 = "gnu.kawa.functions.MakeProcedure"
            java.lang.String r2 = "makeProcedure"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "procedure-property"
            java.lang.String r1 = "kawa.lib.misc"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "set-procedure-property!"
            java.lang.String r1 = "kawa.lib.misc"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "provide"
            java.lang.String r1 = "kawa.lib.misc_syntax"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "test-begin"
            java.lang.String r1 = "kawa.lib.misc_syntax"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "quantity->number"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "quantity->unit"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "make-quantity"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "define-namespace"
            java.lang.String r1 = "gnu.kawa.lispexpr.DefineNamespace"
            java.lang.String r2 = "define_namespace"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "define-xml-namespace"
            java.lang.String r1 = "gnu.kawa.lispexpr.DefineNamespace"
            java.lang.String r2 = "define_xml_namespace"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "define-private-namespace"
            java.lang.String r1 = "gnu.kawa.lispexpr.DefineNamespace"
            java.lang.String r2 = "define_private_namespace"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "define-unit"
            java.lang.String r1 = "kawa.standard.define_unit"
            java.lang.String r2 = "define_unit"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "define-base-unit"
            java.lang.String r1 = "kawa.standard.define_unit"
            java.lang.String r2 = "define_base_unit"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "duration"
            java.lang.String r1 = "kawa.lib.numbers"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "gentemp"
            java.lang.String r1 = "kawa.lib.misc"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "defmacro"
            java.lang.String r1 = "kawa.lib.syntax"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "setter"
            java.lang.String r1 = "gnu.kawa.functions.Setter"
            java.lang.String r2 = "setter"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "resource-url"
            java.lang.String r1 = "kawa.lib.misc_syntax"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "module-uri"
            java.lang.String r1 = "kawa.lib.misc_syntax"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "future"
            java.lang.String r1 = "kawa.lib.thread"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "sleep"
            java.lang.String r1 = "kawa.lib.thread"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "runnable"
            java.lang.String r1 = "kawa.lib.thread"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "trace"
            java.lang.String r1 = "kawa.lib.trace"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "untrace"
            java.lang.String r1 = "kawa.lib.trace"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "disassemble"
            java.lang.String r1 = "kawa.lib.trace"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "format"
            java.lang.String r1 = "gnu.kawa.functions.Format"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "parse-format"
            java.lang.String r1 = "gnu.kawa.functions.ParseFormat"
            java.lang.String r2 = "parseFormat"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "make-element"
            java.lang.String r1 = "gnu.kawa.xml.MakeElement"
            java.lang.String r2 = "makeElement"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "make-attribute"
            java.lang.String r1 = "gnu.kawa.xml.MakeAttribute"
            java.lang.String r2 = "makeAttribute"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "map-values"
            java.lang.String r1 = "gnu.kawa.functions.ValuesMap"
            java.lang.String r2 = "valuesMap"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "children"
            java.lang.String r1 = "gnu.kawa.xml.Children"
            java.lang.String r2 = "children"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "attributes"
            java.lang.String r1 = "gnu.kawa.xml.Attributes"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "unescaped-data"
            java.lang.String r1 = "gnu.kawa.xml.MakeUnescapedData"
            java.lang.String r2 = "unescapedData"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "keyword?"
            java.lang.String r1 = "kawa.lib.keywords"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "keyword->string"
            java.lang.String r1 = "kawa.lib.keywords"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "string->keyword"
            java.lang.String r1 = "kawa.lib.keywords"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "location"
            java.lang.String r1 = "kawa.standard.location"
            java.lang.String r2 = "location"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "define-alias"
            java.lang.String r1 = "kawa.standard.define_alias"
            java.lang.String r2 = "define_alias"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "define-variable"
            java.lang.String r1 = "kawa.standard.define_variable"
            java.lang.String r2 = "define_variable"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "define-member-alias"
            java.lang.String r1 = "kawa.standard.define_member_alias"
            java.lang.String r2 = "define_member_alias"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "define-enum"
            java.lang.String r1 = "gnu.kawa.slib.enums"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "import"
            java.lang.String r1 = "kawa.lib.syntax"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "require"
            java.lang.String r1 = "kawa.standard.require"
            java.lang.String r2 = "require"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "module-name"
            java.lang.String r1 = "kawa.standard.module_name"
            java.lang.String r2 = "module_name"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "module-extends"
            java.lang.String r1 = "kawa.standard.module_extends"
            java.lang.String r2 = "module_extends"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "module-implements"
            java.lang.String r1 = "kawa.standard.module_implements"
            java.lang.String r2 = "module_implements"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "module-static"
            java.lang.String r1 = "kawa.standard.module_static"
            java.lang.String r2 = "module_static"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "module-export"
            java.lang.String r1 = "kawa.standard.export"
            java.lang.String r2 = "module_export"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "export"
            java.lang.String r1 = "kawa.standard.export"
            java.lang.String r2 = "export"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "module-compile-options"
            java.lang.String r1 = "kawa.standard.module_compile_options"
            java.lang.String r2 = "module_compile_options"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "with-compile-options"
            java.lang.String r1 = "kawa.standard.with_compile_options"
            java.lang.String r2 = "with_compile_options"
            r4.defSntxStFld(r0, r1, r2)
            java.lang.String r0 = "array?"
            java.lang.String r1 = "kawa.lib.arrays"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "array-rank"
            java.lang.String r1 = "kawa.lib.arrays"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "make-array"
            java.lang.String r1 = "kawa.lib.arrays"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "array"
            java.lang.String r1 = "kawa.lib.arrays"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "array-start"
            java.lang.String r1 = "kawa.lib.arrays"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "array-end"
            java.lang.String r1 = "kawa.lib.arrays"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "shape"
            java.lang.String r1 = "kawa.lib.arrays"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "array-ref"
            java.lang.String r1 = "gnu.kawa.functions.ArrayRef"
            java.lang.String r2 = "arrayRef"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "array-set!"
            java.lang.String r1 = "gnu.kawa.functions.ArraySet"
            java.lang.String r2 = "arraySet"
            r4.defProcStFld(r0, r1, r2)
            java.lang.String r0 = "share-array"
            java.lang.String r1 = "kawa.lib.arrays"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "s8vector?"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "make-s8vector"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "s8vector"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "s8vector-length"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "s8vector-ref"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "s8vector-set!"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "s8vector->list"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "list->s8vector"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "u8vector?"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "make-u8vector"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "u8vector"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "u8vector-length"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "u8vector-ref"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "u8vector-set!"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "u8vector->list"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "list->u8vector"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "s16vector?"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "make-s16vector"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "s16vector"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "s16vector-length"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "s16vector-ref"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "s16vector-set!"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "s16vector->list"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "list->s16vector"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "u16vector?"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "make-u16vector"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "u16vector"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "u16vector-length"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "u16vector-ref"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "u16vector-set!"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "u16vector->list"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "list->u16vector"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "s32vector?"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "make-s32vector"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "s32vector"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "s32vector-length"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "s32vector-ref"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "s32vector-set!"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "s32vector->list"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "list->s32vector"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "u32vector?"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "make-u32vector"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "u32vector"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "u32vector-length"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "u32vector-ref"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "u32vector-set!"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "u32vector->list"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "list->u32vector"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "s64vector?"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "make-s64vector"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "s64vector"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "s64vector-length"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "s64vector-ref"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "s64vector-set!"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "s64vector->list"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "list->s64vector"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "u64vector?"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "make-u64vector"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "u64vector"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "u64vector-length"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "u64vector-ref"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "u64vector-set!"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "u64vector->list"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "list->u64vector"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "f32vector?"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "make-f32vector"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "f32vector"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "f32vector-length"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "f32vector-ref"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "f32vector-set!"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "f32vector->list"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "list->f32vector"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "f64vector?"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "make-f64vector"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "f64vector"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "f64vector-length"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "f64vector-ref"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "f64vector-set!"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "f64vector->list"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "list->f64vector"
            java.lang.String r1 = "kawa.lib.uniform"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "cut"
            java.lang.String r1 = "gnu.kawa.slib.cut"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "cute"
            java.lang.String r1 = "gnu.kawa.slib.cut"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "cond-expand"
            java.lang.String r1 = "kawa.lib.syntax"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "%cond-expand"
            java.lang.String r1 = "kawa.lib.syntax"
            r4.defSntxStFld(r0, r1)
            java.lang.String r0 = "*print-base*"
            java.lang.String r1 = "gnu.kawa.functions.DisplayFormat"
            java.lang.String r2 = "outBase"
            r4.defAliasStFld(r0, r1, r2)
            java.lang.String r0 = "*print-radix*"
            java.lang.String r1 = "gnu.kawa.functions.DisplayFormat"
            java.lang.String r2 = "outRadix"
            r4.defAliasStFld(r0, r1, r2)
            java.lang.String r0 = "*print-right-margin*"
            java.lang.String r1 = "gnu.text.PrettyWriter"
            java.lang.String r2 = "lineLengthLoc"
            r4.defAliasStFld(r0, r1, r2)
            java.lang.String r0 = "*print-miser-width*"
            java.lang.String r1 = "gnu.text.PrettyWriter"
            java.lang.String r2 = "miserWidthLoc"
            r4.defAliasStFld(r0, r1, r2)
            java.lang.String r0 = "html"
            java.lang.String r1 = "gnu.kawa.xml.XmlNamespace"
            java.lang.String r2 = "HTML"
            r4.defAliasStFld(r0, r1, r2)
            java.lang.String r0 = "unit"
            java.lang.String r1 = "kawa.standard.Scheme"
            java.lang.String r2 = "unitNamespace"
            r4.defAliasStFld(r0, r1, r2)
            java.lang.String r0 = "path"
            java.lang.String r1 = "gnu.kawa.lispexpr.LangObjType"
            java.lang.String r2 = "pathType"
            r4.defAliasStFld(r0, r1, r2)
            java.lang.String r0 = "filepath"
            java.lang.String r1 = "gnu.kawa.lispexpr.LangObjType"
            java.lang.String r2 = "filepathType"
            r4.defAliasStFld(r0, r1, r2)
            java.lang.String r0 = "URI"
            java.lang.String r1 = "gnu.kawa.lispexpr.LangObjType"
            java.lang.String r2 = "URIType"
            r4.defAliasStFld(r0, r1, r2)
            java.lang.String r0 = "resolve-uri"
            java.lang.String r1 = "kawa.lib.files"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "vector"
            java.lang.String r1 = "gnu.kawa.lispexpr.LangObjType"
            java.lang.String r2 = "vectorType"
            r4.defAliasStFld(r0, r1, r2)
            java.lang.String r0 = "string"
            java.lang.String r1 = "gnu.kawa.lispexpr.LangObjType"
            java.lang.String r2 = "stringType"
            r4.defAliasStFld(r0, r1, r2)
            java.lang.String r0 = "list"
            java.lang.String r1 = "gnu.kawa.lispexpr.LangObjType"
            java.lang.String r2 = "listType"
            r4.defAliasStFld(r0, r1, r2)
            java.lang.String r0 = "regex"
            java.lang.String r1 = "gnu.kawa.lispexpr.LangObjType"
            java.lang.String r2 = "regexType"
            r4.defAliasStFld(r0, r1, r2)
            java.lang.String r0 = "path?"
            java.lang.String r1 = "kawa.lib.files"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "filepath?"
            java.lang.String r1 = "kawa.lib.files"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "URI?"
            java.lang.String r1 = "kawa.lib.files"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "absolute-path?"
            java.lang.String r1 = "kawa.lib.files"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "path-scheme"
            java.lang.String r1 = "kawa.lib.files"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "path-authority"
            java.lang.String r1 = "kawa.lib.files"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "path-user-info"
            java.lang.String r1 = "kawa.lib.files"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "path-host"
            java.lang.String r1 = "kawa.lib.files"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "path-port"
            java.lang.String r1 = "kawa.lib.files"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "path-file"
            java.lang.String r1 = "kawa.lib.files"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "path-parent"
            java.lang.String r1 = "kawa.lib.files"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "path-directory"
            java.lang.String r1 = "kawa.lib.files"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "path-last"
            java.lang.String r1 = "kawa.lib.files"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "path-extension"
            java.lang.String r1 = "kawa.lib.files"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "path-fragment"
            java.lang.String r1 = "kawa.lib.files"
            r4.defProcStFld(r0, r1)
            java.lang.String r0 = "path-query"
            java.lang.String r1 = "kawa.lib.files"
            r4.defProcStFld(r0, r1)
            gnu.mapping.SimpleEnvironment r0 = kawa.standard.Scheme.kawaEnvironment
            r0.setLocked()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: kawa.standard.Scheme.initScheme():void");
    }

    public Scheme() {
        this.environ = kawaEnvironment;
        this.userEnv = getNewEnvironment();
    }

    protected Scheme(Environment env) {
        this.environ = env;
    }

    public String getName() {
        return "Scheme";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: kawa.standard.Scheme.eval(gnu.mapping.InPort, gnu.mapping.Environment):java.lang.Object
     arg types: [gnu.mapping.CharArrayInPort, gnu.mapping.Environment]
     candidates:
      kawa.standard.Scheme.eval(java.lang.Object, gnu.mapping.Environment):java.lang.Object
      kawa.standard.Scheme.eval(java.lang.String, gnu.mapping.Environment):java.lang.Object
      gnu.expr.Language.eval(gnu.mapping.InPort, gnu.mapping.CallContext):void
      gnu.expr.Language.eval(java.io.Reader, gnu.lists.Consumer):void
      gnu.expr.Language.eval(java.io.Reader, java.io.Writer):void
      gnu.expr.Language.eval(java.lang.String, gnu.lists.Consumer):void
      gnu.expr.Language.eval(java.lang.String, gnu.lists.PrintConsumer):void
      gnu.expr.Language.eval(java.lang.String, java.io.Writer):void
      kawa.standard.Scheme.eval(gnu.mapping.InPort, gnu.mapping.Environment):java.lang.Object */
    public static Object eval(String string, Environment env) {
        return eval((InPort) new CharArrayInPort(string), env);
    }

    public static Object eval(InPort port, Environment env) {
        SourceMessages messages = new SourceMessages();
        try {
            Object body = ReaderParens.readList((LispReader) Language.getDefaultLanguage().getLexer(port, messages), 0, 1, -1);
            if (!messages.seenErrors()) {
                return Eval.evalBody(body, env, messages);
            }
            throw new SyntaxException(messages);
        } catch (SyntaxException e) {
            throw new RuntimeException("eval: errors while compiling:\n" + e.getMessages().toString(20));
        } catch (IOException e2) {
            throw new RuntimeException("eval: I/O exception: " + e2.toString());
        } catch (RuntimeException ex) {
            throw ex;
        } catch (Error ex2) {
            throw ex2;
        } catch (Throwable ex3) {
            throw new WrappedException(ex3);
        }
    }

    public static Object eval(Object sexpr, Environment env) {
        try {
            return Eval.eval(sexpr, env);
        } catch (RuntimeException ex) {
            throw ex;
        } catch (Error ex2) {
            throw ex2;
        } catch (Throwable ex3) {
            throw new WrappedException(ex3);
        }
    }

    public AbstractFormat getFormat(boolean readable) {
        return readable ? writeFormat : displayFormat;
    }

    public int getNamespaceOf(Declaration decl) {
        return 3;
    }

    public static Type getTypeValue(Expression exp) {
        return getInstance().getTypeFor(exp);
    }

    static synchronized HashMap<String, Type> getTypeMap() {
        HashMap<String, Type> hashMap;
        synchronized (Scheme.class) {
            if (types == null) {
                booleanType = new LangPrimType(Type.booleanType, getInstance());
                types = new HashMap<>();
                types.put("void", LangPrimType.voidType);
                types.put("int", LangPrimType.intType);
                types.put("char", LangPrimType.charType);
                types.put(DesignerProperty.PROPERTY_TYPE_BOOLEAN, booleanType);
                types.put("byte", LangPrimType.byteType);
                types.put("short", LangPrimType.shortType);
                types.put("long", LangPrimType.longType);
                types.put(DesignerProperty.PROPERTY_TYPE_FLOAT, LangPrimType.floatType);
                types.put("double", LangPrimType.doubleType);
                types.put("never-returns", Type.neverReturnsType);
                types.put("Object", Type.objectType);
                types.put("String", Type.toStringType);
                types.put("object", Type.objectType);
                types.put("number", LangObjType.numericType);
                types.put("quantity", ClassType.make("gnu.math.Quantity"));
                types.put("complex", ClassType.make("gnu.math.Complex"));
                types.put("real", LangObjType.realType);
                types.put("rational", LangObjType.rationalType);
                types.put(DesignerProperty.PROPERTY_TYPE_INTEGER, LangObjType.integerType);
                types.put("symbol", ClassType.make("gnu.mapping.Symbol"));
                types.put("namespace", ClassType.make("gnu.mapping.Namespace"));
                types.put("keyword", ClassType.make("gnu.expr.Keyword"));
                types.put("pair", ClassType.make("gnu.lists.Pair"));
                types.put("pair-with-position", ClassType.make("gnu.lists.PairWithPosition"));
                types.put("constant-string", ClassType.make("java.lang.String"));
                types.put("abstract-string", ClassType.make("gnu.lists.CharSeq"));
                types.put("character", ClassType.make("gnu.text.Char"));
                types.put("vector", LangObjType.vectorType);
                types.put(DesignerProperty.PROPERTY_TYPE_STRING, LangObjType.stringType);
                types.put("list", LangObjType.listType);
                types.put("function", ClassType.make("gnu.mapping.Procedure"));
                types.put("procedure", ClassType.make("gnu.mapping.Procedure"));
                types.put("input-port", ClassType.make("gnu.mapping.InPort"));
                types.put("output-port", ClassType.make("gnu.mapping.OutPort"));
                types.put("string-output-port", ClassType.make("gnu.mapping.CharArrayOutPort"));
                types.put("record", ClassType.make("kawa.lang.Record"));
                types.put("type", LangObjType.typeType);
                types.put("class-type", LangObjType.typeClassType);
                types.put("class", LangObjType.typeClass);
                types.put("s8vector", ClassType.make("gnu.lists.S8Vector"));
                types.put("u8vector", ClassType.make("gnu.lists.U8Vector"));
                types.put("s16vector", ClassType.make("gnu.lists.S16Vector"));
                types.put("u16vector", ClassType.make("gnu.lists.U16Vector"));
                types.put("s32vector", ClassType.make("gnu.lists.S32Vector"));
                types.put("u32vector", ClassType.make("gnu.lists.U32Vector"));
                types.put("s64vector", ClassType.make("gnu.lists.S64Vector"));
                types.put("u64vector", ClassType.make("gnu.lists.U64Vector"));
                types.put("f32vector", ClassType.make("gnu.lists.F32Vector"));
                types.put("f64vector", ClassType.make("gnu.lists.F64Vector"));
                types.put("document", ClassType.make("gnu.kawa.xml.KDocument"));
                types.put("readtable", ClassType.make("gnu.kawa.lispexpr.ReadTable"));
            }
            hashMap = types;
        }
        return hashMap;
    }

    public static Type getNamedType(String name) {
        getTypeMap();
        Type type = types.get(name);
        if (type == null && (name.startsWith("elisp:") || name.startsWith("clisp:"))) {
            int colon = name.indexOf(58);
            Class clas = getNamedType(name.substring(colon + 1)).getReflectClass();
            String lang = name.substring(0, colon);
            Language interp = Language.getInstance(lang);
            if (interp == null) {
                throw new RuntimeException("unknown type '" + name + "' - unknown language '" + lang + '\'');
            }
            type = interp.getTypeFor(clas);
            if (type != null) {
                types.put(name, type);
            }
        }
        return type;
    }

    public Type getTypeFor(Class clas) {
        String name = clas.getName();
        if (clas.isPrimitive()) {
            return getNamedType(name);
        }
        if ("java.lang.String".equals(name)) {
            return Type.toStringType;
        }
        if ("gnu.math.IntNum".equals(name)) {
            return LangObjType.integerType;
        }
        if ("gnu.math.DFloNum".equals(name)) {
            return LangObjType.dflonumType;
        }
        if ("gnu.math.RatNum".equals(name)) {
            return LangObjType.rationalType;
        }
        if ("gnu.math.RealNum".equals(name)) {
            return LangObjType.realType;
        }
        if ("gnu.math.Numeric".equals(name)) {
            return LangObjType.numericType;
        }
        if ("gnu.lists.FVector".equals(name)) {
            return LangObjType.vectorType;
        }
        if ("gnu.lists.LList".equals(name)) {
            return LangObjType.listType;
        }
        if ("gnu.text.Path".equals(name)) {
            return LangObjType.pathType;
        }
        if ("gnu.text.URIPath".equals(name)) {
            return LangObjType.URIType;
        }
        if ("gnu.text.FilePath".equals(name)) {
            return LangObjType.filepathType;
        }
        if ("java.lang.Class".equals(name)) {
            return LangObjType.typeClass;
        }
        if ("gnu.bytecode.Type".equals(name)) {
            return LangObjType.typeType;
        }
        if ("gnu.bytecode.ClassType".equals(name)) {
            return LangObjType.typeClassType;
        }
        return Type.make(clas);
    }

    public String formatType(Type type) {
        if (typeToStringMap == null) {
            typeToStringMap = new HashMap<>();
            for (Map.Entry<String, Type> e : getTypeMap().entrySet()) {
                String s = (String) e.getKey();
                Type t = (Type) e.getValue();
                typeToStringMap.put(t, s);
                Type it = t.getImplementationType();
                if (it != t) {
                    typeToStringMap.put(it, s);
                }
            }
        }
        String str = typeToStringMap.get(type);
        return str != null ? str : super.formatType(type);
    }

    public static Type string2Type(String name) {
        Type t;
        if (name.endsWith("[]")) {
            t = string2Type(name.substring(0, name.length() - 2));
            if (t != null) {
                t = ArrayType.make(t);
            }
        } else {
            t = getNamedType(name);
        }
        if (t != null) {
            return t;
        }
        Type t2 = Language.string2Type(name);
        if (t2 != null) {
            types.put(name, t2);
        }
        return t2;
    }

    public Type getTypeFor(String name) {
        return string2Type(name);
    }

    public static Type exp2Type(Expression exp) {
        return getInstance().getTypeFor(exp);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r38v1 */
    /* JADX WARN: Type inference failed for: r38v3 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public gnu.expr.Expression checkDefaultBinding(gnu.mapping.Symbol r53, kawa.lang.Translator r54) {
        /*
            r52 = this;
            gnu.mapping.Namespace r21 = r53.getNamespace()
            java.lang.String r19 = r53.getLocalPart()
            r0 = r21
            boolean r0 = r0 instanceof gnu.kawa.xml.XmlNamespace
            r47 = r0
            if (r47 == 0) goto L_0x001f
            gnu.kawa.xml.XmlNamespace r21 = (gnu.kawa.xml.XmlNamespace) r21
            r0 = r21
            r1 = r19
            java.lang.Object r47 = r0.get(r1)
            gnu.expr.QuoteExp r17 = gnu.expr.QuoteExp.getInstance(r47)
        L_0x001e:
            return r17
        L_0x001f:
            java.lang.String r47 = r21.getName()
            gnu.mapping.Namespace r48 = kawa.standard.Scheme.unitNamespace
            java.lang.String r48 = r48.getName()
            r0 = r47
            r1 = r48
            if (r0 != r1) goto L_0x003a
            gnu.math.NamedUnit r45 = gnu.math.Unit.lookup(r19)
            if (r45 == 0) goto L_0x003a
            gnu.expr.QuoteExp r17 = gnu.expr.QuoteExp.getInstance(r45)
            goto L_0x001e
        L_0x003a:
            java.lang.String r20 = r53.toString()
            int r16 = r20.length()
            if (r16 != 0) goto L_0x0047
            r17 = 0
            goto L_0x001e
        L_0x0047:
            r47 = 1
            r0 = r16
            r1 = r47
            if (r0 <= r1) goto L_0x0105
            int r47 = r16 + -1
            r0 = r20
            r1 = r47
            char r47 = r0.charAt(r1)
            r48 = 63
            r0 = r47
            r1 = r48
            if (r0 != r1) goto L_0x0105
            int r18 = r19.length()
            r47 = 1
            r0 = r18
            r1 = r47
            if (r0 <= r1) goto L_0x0105
            r47 = 0
            int r48 = r18 + -1
            r0 = r19
            r1 = r47
            r2 = r48
            java.lang.String r47 = r0.substring(r1, r2)
            java.lang.String r33 = r47.intern()
            r0 = r21
            r1 = r33
            gnu.mapping.Symbol r34 = r0.getSymbol(r1)
            r47 = 0
            r0 = r54
            r1 = r34
            r2 = r47
            gnu.expr.Expression r32 = r0.rewrite(r1, r2)
            r0 = r32
            boolean r0 = r0 instanceof gnu.expr.ReferenceExp
            r47 = r0
            if (r47 == 0) goto L_0x00fa
            r47 = r32
            gnu.expr.ReferenceExp r47 = (gnu.expr.ReferenceExp) r47
            gnu.expr.Declaration r7 = r47.getBinding()
            if (r7 == 0) goto L_0x00b0
            r47 = 65536(0x10000, double:3.2379E-319)
            r0 = r47
            boolean r47 = r7.getFlag(r0)
            if (r47 == 0) goto L_0x00b2
        L_0x00b0:
            r32 = 0
        L_0x00b2:
            if (r32 == 0) goto L_0x0105
            gnu.expr.LambdaExp r17 = new gnu.expr.LambdaExp
            r47 = 1
            r0 = r17
            r1 = r47
            r0.<init>(r1)
            r0 = r17
            r1 = r53
            r0.setSymbol(r1)
            r47 = 0
            java.lang.Object r47 = (java.lang.Object) r47
            r0 = r17
            r1 = r47
            gnu.expr.Declaration r28 = r0.addDeclaration(r1)
            gnu.expr.ApplyExp r47 = new gnu.expr.ApplyExp
            gnu.kawa.reflect.InstanceOf r48 = kawa.standard.Scheme.instanceOf
            r49 = 2
            r0 = r49
            gnu.expr.Expression[] r0 = new gnu.expr.Expression[r0]
            r49 = r0
            r50 = 0
            gnu.expr.ReferenceExp r51 = new gnu.expr.ReferenceExp
            r0 = r51
            r1 = r28
            r0.<init>(r1)
            r49[r50] = r51
            r50 = 1
            r49[r50] = r32
            r47.<init>(r48, r49)
            r0 = r47
            r1 = r17
            r1.body = r0
            goto L_0x001e
        L_0x00fa:
            r0 = r32
            boolean r0 = r0 instanceof gnu.expr.QuoteExp
            r47 = r0
            if (r47 != 0) goto L_0x00b2
            r32 = 0
            goto L_0x00b2
        L_0x0105:
            r47 = 0
            r0 = r20
            r1 = r47
            char r4 = r0.charAt(r1)
            r47 = 45
            r0 = r47
            if (r4 == r0) goto L_0x0125
            r47 = 43
            r0 = r47
            if (r4 == r0) goto L_0x0125
            r47 = 10
            r0 = r47
            int r47 = java.lang.Character.digit(r4, r0)
            if (r47 < 0) goto L_0x0209
        L_0x0125:
            r31 = 0
            r13 = 0
        L_0x0128:
            r0 = r16
            if (r13 >= r0) goto L_0x01cd
            r0 = r20
            char r3 = r0.charAt(r13)
            r47 = 10
            r0 = r47
            int r47 = java.lang.Character.digit(r3, r0)
            if (r47 < 0) goto L_0x0157
            r47 = 3
            r0 = r31
            r1 = r47
            if (r0 >= r1) goto L_0x0149
            r31 = 2
        L_0x0146:
            int r13 = r13 + 1
            goto L_0x0128
        L_0x0149:
            r47 = 5
            r0 = r31
            r1 = r47
            if (r0 >= r1) goto L_0x0154
            r31 = 4
            goto L_0x0146
        L_0x0154:
            r31 = 5
            goto L_0x0146
        L_0x0157:
            r47 = 43
            r0 = r47
            if (r3 == r0) goto L_0x0163
            r47 = 45
            r0 = r47
            if (r3 != r0) goto L_0x0168
        L_0x0163:
            if (r31 != 0) goto L_0x0168
            r31 = 1
            goto L_0x0146
        L_0x0168:
            r47 = 46
            r0 = r47
            if (r3 != r0) goto L_0x0179
            r47 = 3
            r0 = r31
            r1 = r47
            if (r0 >= r1) goto L_0x0179
            r31 = 3
            goto L_0x0146
        L_0x0179:
            r47 = 101(0x65, float:1.42E-43)
            r0 = r47
            if (r3 == r0) goto L_0x0185
            r47 = 69
            r0 = r47
            if (r3 != r0) goto L_0x01cd
        L_0x0185:
            r47 = 2
            r0 = r31
            r1 = r47
            if (r0 == r1) goto L_0x0195
            r47 = 4
            r0 = r31
            r1 = r47
            if (r0 != r1) goto L_0x01cd
        L_0x0195:
            int r47 = r13 + 1
            r0 = r47
            r1 = r16
            if (r0 >= r1) goto L_0x01cd
            int r15 = r13 + 1
            r0 = r20
            char r24 = r0.charAt(r15)
            r47 = 45
            r0 = r24
            r1 = r47
            if (r0 == r1) goto L_0x01b5
            r47 = 43
            r0 = r24
            r1 = r47
            if (r0 != r1) goto L_0x01c1
        L_0x01b5:
            int r15 = r15 + 1
            r0 = r16
            if (r15 >= r0) goto L_0x01c1
            r0 = r20
            char r24 = r0.charAt(r15)
        L_0x01c1:
            r47 = 10
            r0 = r24
            r1 = r47
            int r47 = java.lang.Character.digit(r0, r1)
            if (r47 >= 0) goto L_0x026e
        L_0x01cd:
            r0 = r16
            if (r13 >= r0) goto L_0x0209
            r47 = 1
            r0 = r31
            r1 = r47
            if (r0 <= r1) goto L_0x0209
            gnu.math.DFloNum r25 = new gnu.math.DFloNum
            r47 = 0
            r0 = r20
            r1 = r47
            java.lang.String r47 = r0.substring(r1, r13)
            r0 = r25
            r1 = r47
            r0.<init>(r1)
            r9 = 0
            java.util.Vector r46 = new java.util.Vector
            r46.<init>()
            r14 = r13
        L_0x01f3:
            r0 = r16
            if (r14 >= r0) goto L_0x033e
            int r13 = r14 + 1
            r0 = r20
            char r3 = r0.charAt(r14)
            r47 = 42
            r0 = r47
            if (r3 != r0) goto L_0x02e4
            r0 = r16
            if (r13 != r0) goto L_0x0274
        L_0x0209:
            r47 = 2
            r0 = r16
            r1 = r47
            if (r0 <= r1) goto L_0x03e6
            r47 = 60
            r0 = r47
            if (r4 != r0) goto L_0x03e6
            int r47 = r16 + -1
            r0 = r20
            r1 = r47
            char r47 = r0.charAt(r1)
            r48 = 62
            r0 = r47
            r1 = r48
            if (r0 != r1) goto L_0x03e6
            r47 = 1
            int r48 = r16 + -1
            r0 = r20
            r1 = r47
            r2 = r48
            java.lang.String r20 = r0.substring(r1, r2)
            int r16 = r16 + -2
            r30 = 1
        L_0x023b:
            r29 = 0
        L_0x023d:
            r47 = 2
            r0 = r16
            r1 = r47
            if (r0 <= r1) goto L_0x03ea
            int r47 = r16 + -2
            r0 = r20
            r1 = r47
            char r47 = r0.charAt(r1)
            r48 = 91
            r0 = r47
            r1 = r48
            if (r0 != r1) goto L_0x03ea
            int r47 = r16 + -1
            r0 = r20
            r1 = r47
            char r47 = r0.charAt(r1)
            r48 = 93
            r0 = r47
            r1 = r48
            if (r0 != r1) goto L_0x03ea
            int r16 = r16 + -2
            int r29 = r29 + 1
            goto L_0x023d
        L_0x026e:
            r31 = 5
            int r13 = r15 + 1
            goto L_0x0146
        L_0x0274:
            int r14 = r13 + 1
            r0 = r20
            char r3 = r0.charAt(r13)
            r13 = r14
        L_0x027d:
            int r40 = r13 + -1
        L_0x027f:
            boolean r47 = java.lang.Character.isLetter(r3)
            if (r47 != 0) goto L_0x02fb
            int r39 = r13 + -1
            r0 = r39
            r1 = r40
            if (r0 == r1) goto L_0x0209
        L_0x028d:
            r0 = r20
            r1 = r40
            r2 = r39
            java.lang.String r47 = r0.substring(r1, r2)
            r46.addElement(r47)
            r12 = 0
            r47 = 94
            r0 = r47
            if (r3 != r0) goto L_0x04b3
            r12 = 1
            r0 = r16
            if (r13 == r0) goto L_0x0209
            int r14 = r13 + 1
            r0 = r20
            char r3 = r0.charAt(r13)
        L_0x02ae:
            r22 = r9
            r47 = 43
            r0 = r47
            if (r3 != r0) goto L_0x030f
            r12 = 1
            r0 = r16
            if (r14 == r0) goto L_0x0209
            int r13 = r14 + 1
            r0 = r20
            char r3 = r0.charAt(r14)
        L_0x02c3:
            r23 = 0
            r11 = 0
        L_0x02c6:
            r47 = 10
            r0 = r47
            int r8 = java.lang.Character.digit(r3, r0)
            if (r8 > 0) goto L_0x032a
            int r13 = r13 + -1
        L_0x02d2:
            if (r23 != 0) goto L_0x02d7
            r11 = 1
            if (r12 != 0) goto L_0x0209
        L_0x02d7:
            if (r22 == 0) goto L_0x02da
            int r11 = -r11
        L_0x02da:
            gnu.math.IntNum r47 = gnu.math.IntNum.make(r11)
            r46.addElement(r47)
            r14 = r13
            goto L_0x01f3
        L_0x02e4:
            r47 = 47
            r0 = r47
            if (r3 != r0) goto L_0x027d
            r0 = r16
            if (r13 == r0) goto L_0x0209
            if (r9 != 0) goto L_0x0209
            r9 = 1
            int r14 = r13 + 1
            r0 = r20
            char r3 = r0.charAt(r13)
            r13 = r14
            goto L_0x027d
        L_0x02fb:
            r0 = r16
            if (r13 != r0) goto L_0x0304
            r39 = r13
            r3 = 49
            goto L_0x028d
        L_0x0304:
            int r14 = r13 + 1
            r0 = r20
            char r3 = r0.charAt(r13)
            r13 = r14
            goto L_0x027f
        L_0x030f:
            r47 = 45
            r0 = r47
            if (r3 != r0) goto L_0x04b0
            r12 = 1
            r0 = r16
            if (r14 == r0) goto L_0x0209
            int r13 = r14 + 1
            r0 = r20
            char r3 = r0.charAt(r14)
            if (r22 != 0) goto L_0x0327
            r22 = 1
        L_0x0326:
            goto L_0x02c3
        L_0x0327:
            r22 = 0
            goto L_0x0326
        L_0x032a:
            int r47 = r11 * 10
            int r11 = r47 + r8
            int r23 = r23 + 1
            r0 = r16
            if (r13 == r0) goto L_0x02d2
            int r14 = r13 + 1
            r0 = r20
            char r3 = r0.charAt(r13)
            r13 = r14
            goto L_0x02c6
        L_0x033e:
            r0 = r16
            if (r14 != r0) goto L_0x0209
            int r47 = r46.size()
            int r26 = r47 >> 1
            r0 = r26
            gnu.expr.Expression[] r0 = new gnu.expr.Expression[r0]
            r41 = r0
            r13 = 0
        L_0x034f:
            r0 = r26
            if (r13 >= r0) goto L_0x03a9
            int r47 = r13 * 2
            java.lang.Object r37 = r46.elementAt(r47)
            java.lang.String r37 = (java.lang.String) r37
            gnu.mapping.Namespace r47 = kawa.standard.Scheme.unitNamespace
            java.lang.String r48 = r37.intern()
            gnu.mapping.Symbol r44 = r47.getSymbol(r48)
            r0 = r54
            r1 = r44
            gnu.expr.Expression r42 = r0.rewrite(r1)
            int r47 = r13 * 2
            int r47 = r47 + 1
            java.lang.Object r36 = r46.elementAt(r47)
            gnu.math.IntNum r36 = (gnu.math.IntNum) r36
            long r47 = r36.longValue()
            r49 = 1
            int r47 = (r47 > r49 ? 1 : (r47 == r49 ? 0 : -1))
            if (r47 == 0) goto L_0x03a4
            gnu.expr.ApplyExp r43 = new gnu.expr.ApplyExp
            kawa.standard.expt r47 = kawa.standard.expt.expt
            r48 = 2
            r0 = r48
            gnu.expr.Expression[] r0 = new gnu.expr.Expression[r0]
            r48 = r0
            r49 = 0
            r48[r49] = r42
            r49 = 1
            gnu.expr.QuoteExp r50 = gnu.expr.QuoteExp.getInstance(r36)
            r48[r49] = r50
            r0 = r43
            r1 = r47
            r2 = r48
            r0.<init>(r1, r2)
            r42 = r43
        L_0x03a4:
            r41[r13] = r42
            int r13 = r13 + 1
            goto L_0x034f
        L_0x03a9:
            r47 = 1
            r0 = r26
            r1 = r47
            if (r0 != r1) goto L_0x03d8
            r47 = 0
            r38 = r41[r47]
        L_0x03b5:
            gnu.expr.ApplyExp r17 = new gnu.expr.ApplyExp
            gnu.kawa.functions.MultiplyOp r47 = gnu.kawa.functions.MultiplyOp.$St
            r48 = 2
            r0 = r48
            gnu.expr.Expression[] r0 = new gnu.expr.Expression[r0]
            r48 = r0
            r49 = 0
            gnu.expr.QuoteExp r50 = gnu.expr.QuoteExp.getInstance(r25)
            r48[r49] = r50
            r49 = 1
            r48[r49] = r38
            r0 = r17
            r1 = r47
            r2 = r48
            r0.<init>(r1, r2)
            goto L_0x001e
        L_0x03d8:
            gnu.expr.ApplyExp r38 = new gnu.expr.ApplyExp
            gnu.kawa.functions.MultiplyOp r47 = gnu.kawa.functions.MultiplyOp.$St
            r0 = r38
            r1 = r47
            r2 = r41
            r0.<init>(r1, r2)
            goto L_0x03b5
        L_0x03e6:
            r30 = 0
            goto L_0x023b
        L_0x03ea:
            r6 = r20
            if (r29 == 0) goto L_0x03fa
            r47 = 0
            r0 = r20
            r1 = r47
            r2 = r16
            java.lang.String r6 = r0.substring(r1, r2)
        L_0x03fa:
            gnu.bytecode.Type r35 = getNamedType(r6)     // Catch:{ ClassNotFoundException -> 0x049e, Throwable -> 0x04ab }
            if (r29 <= 0) goto L_0x0438
            if (r30 == 0) goto L_0x0404
            if (r35 != 0) goto L_0x0438
        L_0x0404:
            java.lang.String r47 = r6.intern()     // Catch:{ ClassNotFoundException -> 0x049e, Throwable -> 0x04ab }
            r0 = r21
            r1 = r47
            gnu.mapping.Symbol r34 = r0.getSymbol(r1)     // Catch:{ ClassNotFoundException -> 0x049e, Throwable -> 0x04ab }
            r47 = 0
            r0 = r54
            r1 = r34
            r2 = r47
            gnu.expr.Expression r32 = r0.rewrite(r1, r2)     // Catch:{ ClassNotFoundException -> 0x049e, Throwable -> 0x04ab }
            r0 = r32
            r1 = r54
            gnu.expr.Expression r32 = gnu.expr.InlineCalls.inlineCalls(r0, r1)     // Catch:{ ClassNotFoundException -> 0x049e, Throwable -> 0x04ab }
            r0 = r32
            boolean r0 = r0 instanceof gnu.expr.ErrorExp     // Catch:{ ClassNotFoundException -> 0x049e, Throwable -> 0x04ab }
            r47 = r0
            if (r47 != 0) goto L_0x0438
            gnu.expr.Language r47 = r54.getLanguage()     // Catch:{ ClassNotFoundException -> 0x049e, Throwable -> 0x04ab }
            r0 = r47
            r1 = r32
            gnu.bytecode.Type r35 = r0.getTypeFor(r1)     // Catch:{ ClassNotFoundException -> 0x049e, Throwable -> 0x04ab }
        L_0x0438:
            if (r35 == 0) goto L_0x0449
        L_0x043a:
            int r29 = r29 + -1
            if (r29 < 0) goto L_0x0443
            gnu.bytecode.ArrayType r35 = gnu.bytecode.ArrayType.make(r35)     // Catch:{ ClassNotFoundException -> 0x049e, Throwable -> 0x04ab }
            goto L_0x043a
        L_0x0443:
            gnu.expr.QuoteExp r17 = gnu.expr.QuoteExp.getInstance(r35)     // Catch:{ ClassNotFoundException -> 0x049e, Throwable -> 0x04ab }
            goto L_0x001e
        L_0x0449:
            gnu.bytecode.Type r35 = gnu.bytecode.Type.lookupType(r6)     // Catch:{ ClassNotFoundException -> 0x049e, Throwable -> 0x04ab }
            r0 = r35
            boolean r0 = r0 instanceof gnu.bytecode.PrimType     // Catch:{ ClassNotFoundException -> 0x049e, Throwable -> 0x04ab }
            r47 = r0
            if (r47 == 0) goto L_0x046a
            java.lang.Class r5 = r35.getReflectClass()     // Catch:{ ClassNotFoundException -> 0x049e, Throwable -> 0x04ab }
        L_0x0459:
            if (r5 == 0) goto L_0x04ac
            if (r29 <= 0) goto L_0x0498
            gnu.bytecode.Type r35 = gnu.bytecode.Type.make(r5)     // Catch:{ ClassNotFoundException -> 0x049e, Throwable -> 0x04ab }
        L_0x0461:
            int r29 = r29 + -1
            if (r29 < 0) goto L_0x0494
            gnu.bytecode.ArrayType r35 = gnu.bytecode.ArrayType.make(r35)     // Catch:{ ClassNotFoundException -> 0x049e, Throwable -> 0x04ab }
            goto L_0x0461
        L_0x046a:
            r47 = 46
            r0 = r47
            int r47 = r6.indexOf(r0)     // Catch:{ ClassNotFoundException -> 0x049e, Throwable -> 0x04ab }
            if (r47 >= 0) goto L_0x048f
            java.lang.StringBuilder r47 = new java.lang.StringBuilder     // Catch:{ ClassNotFoundException -> 0x049e, Throwable -> 0x04ab }
            r47.<init>()     // Catch:{ ClassNotFoundException -> 0x049e, Throwable -> 0x04ab }
            r0 = r54
            java.lang.String r0 = r0.classPrefix     // Catch:{ ClassNotFoundException -> 0x049e, Throwable -> 0x04ab }
            r48 = r0
            java.lang.StringBuilder r47 = r47.append(r48)     // Catch:{ ClassNotFoundException -> 0x049e, Throwable -> 0x04ab }
            java.lang.String r48 = gnu.expr.Compilation.mangleNameIfNeeded(r6)     // Catch:{ ClassNotFoundException -> 0x049e, Throwable -> 0x04ab }
            java.lang.StringBuilder r47 = r47.append(r48)     // Catch:{ ClassNotFoundException -> 0x049e, Throwable -> 0x04ab }
            java.lang.String r6 = r47.toString()     // Catch:{ ClassNotFoundException -> 0x049e, Throwable -> 0x04ab }
        L_0x048f:
            java.lang.Class r5 = gnu.bytecode.ClassType.getContextClass(r6)     // Catch:{ ClassNotFoundException -> 0x049e, Throwable -> 0x04ab }
            goto L_0x0459
        L_0x0494:
            java.lang.Class r5 = r35.getReflectClass()     // Catch:{ ClassNotFoundException -> 0x049e, Throwable -> 0x04ab }
        L_0x0498:
            gnu.expr.QuoteExp r17 = gnu.expr.QuoteExp.getInstance(r5)     // Catch:{ ClassNotFoundException -> 0x049e, Throwable -> 0x04ab }
            goto L_0x001e
        L_0x049e:
            r10 = move-exception
            java.lang.Package r27 = gnu.bytecode.ArrayClassLoader.getContextPackage(r20)
            if (r27 == 0) goto L_0x04ac
            gnu.expr.QuoteExp r17 = gnu.expr.QuoteExp.getInstance(r27)
            goto L_0x001e
        L_0x04ab:
            r47 = move-exception
        L_0x04ac:
            r17 = 0
            goto L_0x001e
        L_0x04b0:
            r13 = r14
            goto L_0x02c3
        L_0x04b3:
            r14 = r13
            goto L_0x02ae
        */
        throw new UnsupportedOperationException("Method not decompiled: kawa.standard.Scheme.checkDefaultBinding(gnu.mapping.Symbol, kawa.lang.Translator):gnu.expr.Expression");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: gnu.expr.ApplyExp.<init>(gnu.expr.Expression, gnu.expr.Expression[]):void
     arg types: [gnu.expr.ReferenceExp, gnu.expr.Expression[]]
     candidates:
      gnu.expr.ApplyExp.<init>(gnu.bytecode.Method, gnu.expr.Expression[]):void
      gnu.expr.ApplyExp.<init>(gnu.mapping.Procedure, gnu.expr.Expression[]):void
      gnu.expr.ApplyExp.<init>(gnu.expr.Expression, gnu.expr.Expression[]):void */
    public Expression makeApply(Expression func, Expression[] args) {
        Expression[] exps = new Expression[(args.length + 1)];
        exps[0] = func;
        System.arraycopy(args, 0, exps, 1, args.length);
        return new ApplyExp((Expression) new ReferenceExp(applyFieldDecl), exps);
    }

    public Symbol asSymbol(String ident) {
        return Namespace.EmptyNamespace.getSymbol(ident);
    }

    public ReadTable createReadTable() {
        ReadTable tab = ReadTable.createInitial();
        tab.postfixLookupOperator = ':';
        ReaderDispatch dispatchTable = (ReaderDispatch) tab.lookup(35);
        dispatchTable.set(39, new ReaderQuote(asSymbol("syntax")));
        dispatchTable.set(96, new ReaderQuote(asSymbol("quasisyntax")));
        dispatchTable.set(44, ReaderDispatchMisc.getInstance());
        tab.putReaderCtorFld("path", "gnu.kawa.lispexpr.LangObjType", "pathType");
        tab.putReaderCtorFld("filepath", "gnu.kawa.lispexpr.LangObjType", "filepathType");
        tab.putReaderCtorFld("URI", "gnu.kawa.lispexpr.LangObjType", "URIType");
        tab.putReaderCtor("symbol", ClassType.make("gnu.mapping.Symbol"));
        tab.putReaderCtor("namespace", ClassType.make("gnu.mapping.Namespace"));
        tab.putReaderCtorFld("duration", "kawa.lib.numbers", "duration");
        tab.setFinalColonIsKeyword(true);
        return tab;
    }

    public static void registerEnvironment() {
        Language.setDefaults(getInstance());
    }
}
