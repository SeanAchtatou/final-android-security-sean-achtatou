package org.MobileDb;

import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;

public class Table {
    private long _offset;
    private Vector fields;
    private InputStream inputStream;
    private int last_find_index;
    private Row last_find_row;
    private boolean loadAllDataInMemory;
    public String name;
    private Field[] opt_fields;
    private Row[] opt_rows;
    private boolean optimized;
    private String pathToDb;
    private Vector rows;
    private int rowsCount;
    private boolean transaction;

    public Table() {
        this.optimized = false;
        this.opt_fields = null;
        this.opt_rows = null;
        this.last_find_index = -1;
        this.last_find_row = null;
        this.loadAllDataInMemory = true;
        this.pathToDb = null;
        this.transaction = false;
        this.inputStream = null;
        this.rowsCount = 0;
        this._offset = -1;
        this.name = "";
        this.fields = new Vector();
        this.rows = new Vector();
    }

    public Table(String tableName) {
        this.optimized = false;
        this.opt_fields = null;
        this.opt_rows = null;
        this.last_find_index = -1;
        this.last_find_row = null;
        this.loadAllDataInMemory = true;
        this.pathToDb = null;
        this.transaction = false;
        this.inputStream = null;
        this.rowsCount = 0;
        this._offset = -1;
        this.name = tableName;
        this.fields = new Vector();
        this.rows = new Vector();
    }

    public Table(String tableName, boolean loadAllDataInMemory2, String path) {
        this.optimized = false;
        this.opt_fields = null;
        this.opt_rows = null;
        this.last_find_index = -1;
        this.last_find_row = null;
        this.loadAllDataInMemory = true;
        this.pathToDb = null;
        this.transaction = false;
        this.inputStream = null;
        this.rowsCount = 0;
        this._offset = -1;
        this.name = tableName;
        this.fields = new Vector();
        this.loadAllDataInMemory = loadAllDataInMemory2;
        if (loadAllDataInMemory2) {
            this.rows = new Vector();
            this.pathToDb = null;
            return;
        }
        this.pathToDb = path;
    }

    public void addField(Field field) {
        if (!this.optimized) {
            Object[] objArr = {field};
            Vector.class.getMethod("addElement", Object.class).invoke(this.fields, objArr);
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 5 */
    public Field getField(int index) {
        if (this.optimized) {
            if (index >= 0 && index < this.opt_fields.length) {
                return this.opt_fields[index];
            }
        } else if (index >= 0) {
            if (index < ((Integer) Vector.class.getMethod("size", new Class[0]).invoke(this.fields, new Object[0])).intValue()) {
                Vector vector = this.fields;
                Class[] clsArr = {Integer.TYPE};
                return (Field) Vector.class.getMethod("elementAt", clsArr).invoke(vector, new Integer(index));
            }
        }
        return null;
    }

    public void removeField(Field field) {
        if (!this.optimized) {
            Object[] objArr = {field};
            ((Boolean) Vector.class.getMethod("removeElement", Object.class).invoke(this.fields, objArr)).booleanValue();
        }
    }

    public int fieldsCount() {
        if (this.optimized) {
            return this.opt_fields.length;
        }
        return ((Integer) Vector.class.getMethod("size", new Class[0]).invoke(this.fields, new Object[0])).intValue();
    }

    public void removeAllFields() {
        if (!this.optimized) {
            Vector.class.getMethod("removeAllElements", new Class[0]).invoke(this.fields, new Object[0]);
        }
    }

    public Row createRow() {
        if (this.optimized) {
            return null;
        }
        int[] types = new int[((Integer) Vector.class.getMethod("size", new Class[0]).invoke(this.fields, new Object[0])).intValue()];
        int i = 0;
        while (true) {
            if (i >= ((Integer) Vector.class.getMethod("size", new Class[0]).invoke(this.fields, new Object[0])).intValue()) {
                return new Row(types);
            }
            Vector vector = this.fields;
            Class[] clsArr = {Integer.TYPE};
            types[i] = ((Field) Vector.class.getMethod("elementAt", clsArr).invoke(vector, new Integer(i))).type;
            i++;
        }
    }

    public void addRow(Row row) {
        if (!this.optimized) {
            Object[] objArr = {row};
            Vector.class.getMethod("addElement", Object.class).invoke(this.rows, objArr);
        }
    }

    public void removeRow(Row row) {
        if (!this.optimized) {
            Object[] objArr = {row};
            ((Boolean) Vector.class.getMethod("removeElement", Object.class).invoke(this.rows, objArr)).booleanValue();
        }
    }

    public int rowsCount() {
        if (!this.loadAllDataInMemory) {
            return this.rowsCount;
        }
        if (this.optimized) {
            return this.opt_rows.length;
        }
        return ((Integer) Vector.class.getMethod("size", new Class[0]).invoke(this.rows, new Object[0])).intValue();
    }

    public Row getRow(int index) {
        if (this.loadAllDataInMemory) {
            if (this.optimized) {
                if (index >= 0 && index < this.opt_rows.length) {
                    this.last_find_row = this.opt_rows[index];
                    this.last_find_index = index;
                    return this.last_find_row;
                }
            } else if (index >= 0) {
                if (index < ((Integer) Vector.class.getMethod("size", new Class[0]).invoke(this.rows, new Object[0])).intValue()) {
                    Vector vector = this.rows;
                    Class[] clsArr = {Integer.TYPE};
                    this.last_find_row = (Row) Vector.class.getMethod("elementAt", clsArr).invoke(vector, new Integer(index));
                    this.last_find_index = index;
                    return this.last_find_row;
                }
            }
        } else if (index >= 0 && index < this.rowsCount) {
            this.last_find_index = index;
            if (this.transaction) {
                InputStream stream = this.inputStream;
                try {
                    if (((Integer) InputStream.class.getMethod("read", new Class[0]).invoke(stream, new Object[0])).intValue() == 11) {
                        Row row = createRow();
                        for (int i = 0; i < row.fieldsCount(); i++) {
                            int type = row.getFieldType(i);
                            if (type == Field.SMALL_INT) {
                                row.setValue(i, new Integer(((Integer) InputStream.class.getMethod("read", new Class[0]).invoke(stream, new Object[0])).intValue()));
                            } else if (type == Field.SHORT_INT) {
                                byte[] tmp = new byte[2];
                                MobileDatabase.readDataFromStream(stream, tmp);
                                row.setValue(i, new Integer(MobileDatabase.shortIntFromBytes(tmp)));
                            } else if (type == Field.INT) {
                                byte[] tmp2 = new byte[4];
                                MobileDatabase.readDataFromStream(stream, tmp2);
                                row.setValue(i, new Integer(MobileDatabase.intFromBytes(tmp2)));
                            } else if (type == Field.TIME) {
                                byte[] tmp3 = new byte[4];
                                MobileDatabase.readDataFromStream(stream, tmp3);
                                row.setValue(i, new Integer(MobileDatabase.intFromBytes(tmp3)));
                            } else if (type == Field.NAME) {
                                byte[] tmp4 = new byte[((Integer) InputStream.class.getMethod("read", new Class[0]).invoke(stream, new Object[0])).intValue()];
                                MobileDatabase.readDataFromStream(stream, tmp4);
                                row.setValue(i, MobileDatabase.getUtf8String(tmp4));
                            } else if (type == Field.TEXT) {
                                byte[] tmp5 = new byte[2];
                                MobileDatabase.readDataFromStream(stream, tmp5);
                                byte[] tmp6 = new byte[MobileDatabase.shortIntFromBytes(tmp5)];
                                MobileDatabase.readDataFromStream(stream, tmp6);
                                row.setValue(i, MobileDatabase.getUtf8String(tmp6));
                            } else if (type == Field.BINARY) {
                                byte[] tmp7 = new byte[4];
                                MobileDatabase.readDataFromStream(stream, tmp7);
                                byte[] tmp8 = new byte[MobileDatabase.intFromBytes(tmp7)];
                                MobileDatabase.readDataFromStream(stream, tmp8);
                                row.setValue(i, tmp8);
                            }
                        }
                        this.last_find_row = row;
                        return this.last_find_row;
                    }
                } catch (Exception e) {
                    Exception.class.getMethod("printStackTrace", new Class[0]).invoke(e, new Object[0]);
                }
            }
        }
        return null;
    }

    public void removeAllRows() {
        if (this.loadAllDataInMemory && !this.optimized) {
            Vector.class.getMethod("removeAllElements", new Class[0]).invoke(this.rows, new Object[0]);
        }
    }

    public boolean isOptimized() {
        return this.optimized;
    }

    public void optimize() {
        if (this.loadAllDataInMemory && !this.optimized) {
            this.opt_fields = new Field[((Integer) Vector.class.getMethod("size", new Class[0]).invoke(this.fields, new Object[0])).intValue()];
            Vector vector = this.fields;
            Object[] objArr = {this.opt_fields};
            Vector.class.getMethod("copyInto", Object[].class).invoke(vector, objArr);
            this.opt_rows = new Row[((Integer) Vector.class.getMethod("size", new Class[0]).invoke(this.rows, new Object[0])).intValue()];
            Vector vector2 = this.rows;
            Object[] objArr2 = {this.opt_rows};
            Vector.class.getMethod("copyInto", Object[].class).invoke(vector2, objArr2);
            this.optimized = true;
            this.fields = null;
            this.rows = null;
            System.class.getMethod("gc", new Class[0]).invoke(null, new Object[0]);
        }
    }

    public Object getFieldValueByName(String name2, int index) {
        if (this.loadAllDataInMemory) {
            if (this.optimized) {
                if (this.last_find_index != index && getRow(index) == null) {
                    return null;
                }
                for (int i = 0; i < this.opt_fields.length; i++) {
                    Class[] clsArr = {Object.class};
                    if (((Boolean) String.class.getMethod("equals", clsArr).invoke(name2, this.opt_fields[i].name)).booleanValue()) {
                        return this.last_find_row.getValue(i);
                    }
                }
            } else if (this.last_find_index == index || getRow(index) != null) {
                int i2 = 0;
                while (true) {
                    if (i2 >= ((Integer) Vector.class.getMethod("size", new Class[0]).invoke(this.fields, new Object[0])).intValue()) {
                        break;
                    }
                    Vector vector = this.fields;
                    Class[] clsArr2 = {Integer.TYPE};
                    String str = ((Field) Vector.class.getMethod("elementAt", clsArr2).invoke(vector, new Integer(i2))).name;
                    if (((Boolean) String.class.getMethod("equals", Object.class).invoke(name2, str)).booleanValue()) {
                        return this.last_find_row.getValue(i2);
                    }
                    i2++;
                }
            } else {
                return null;
            }
        } else if (this.last_find_index == index || getRow(index) != null) {
            int i3 = 0;
            while (true) {
                if (i3 >= ((Integer) Vector.class.getMethod("size", new Class[0]).invoke(this.fields, new Object[0])).intValue()) {
                    break;
                }
                Vector vector2 = this.fields;
                Class[] clsArr3 = {Integer.TYPE};
                String str2 = ((Field) Vector.class.getMethod("elementAt", clsArr3).invoke(vector2, new Integer(i3))).name;
                if (((Boolean) String.class.getMethod("equals", Object.class).invoke(name2, str2)).booleanValue()) {
                    return this.last_find_row.getValue(i3);
                }
                i3++;
            }
        } else {
            return null;
        }
        return null;
    }

    public void addRow() {
        this.rowsCount++;
    }

    public void setOffset(long offset) {
        this._offset = offset - 1;
    }

    public long getOffset() {
        return this._offset;
    }

    public void startTransaction() {
        if (!this.loadAllDataInMemory && this.rowsCount != 0) {
            try {
                this.inputStream = ((Class) Object.class.getMethod("getClass", new Class[0]).invoke(this, new Object[0])).getResourceAsStream(this.pathToDb);
                InputStream inputStream2 = this.inputStream;
                long j = this._offset;
                Class[] clsArr = {Long.TYPE};
                ((Long) InputStream.class.getMethod("skip", clsArr).invoke(inputStream2, new Long(j))).longValue();
                this.transaction = true;
            } catch (Exception e) {
                Exception.class.getMethod("printStackTrace", new Class[0]).invoke(e, new Object[0]);
            }
        }
    }

    public void stopTransaction() {
        if (!this.loadAllDataInMemory) {
            try {
                InputStream.class.getMethod("close", new Class[0]).invoke(this.inputStream, new Object[0]);
            } catch (IOException e) {
            }
            this.transaction = true;
        }
    }
}
