package ad.notify;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import java.io.PrintStream;

public class RepeatingAlarmService extends BroadcastReceiver {
    public void onReceive(Context arg0, Intent arg1) {
        Object[] objArr = {"RepeatingAlarmService START !!!)"};
        PrintStream.class.getMethod("println", String.class).invoke(System.out, objArr);
        Intent i = new Intent(arg0, NotificationActivity.class);
        Class[] clsArr = {Integer.TYPE};
        Intent.class.getMethod("addFlags", clsArr).invoke(i, new Integer(268435456));
        Object[] objArr2 = {i};
        Context.class.getMethod("startActivity", Intent.class).invoke(arg0, objArr2);
    }
}
