package ad.notify;

import java.util.Vector;

public class ScreenItem {
    public Vector buttons;
    public int id;
    public String text;
    public String title;

    ScreenItem(String title2, String text2, String[] buttons2, int id2) {
        this.buttons = new Vector();
        this.title = title2;
        this.text = text2;
        this.id = id2;
        for (String str : buttons2) {
            Vector.class.getMethod("addElement", Object.class).invoke(this.buttons, str);
        }
    }

    ScreenItem() {
        this.buttons = new Vector();
        this.text = "";
    }
}
