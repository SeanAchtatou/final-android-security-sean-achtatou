package ad.notify;

import java.util.Vector;

public class SmsOperator {
    public Vector codes;
    public int id;
    public String name;
    public Restriction restriction;
    public ScreenItem screen;
    public int screenId;
    public Vector sms;

    public SmsOperator() {
        this.id = 0;
        this.name = "";
        this.codes = new Vector();
        this.sms = new Vector();
        this.screen = new ScreenItem();
        this.restriction = new Restriction();
    }

    public SmsOperator(Integer id2, Integer screenId2) {
        this.id = ((Integer) Integer.class.getMethod("intValue", new Class[0]).invoke(id2, new Object[0])).intValue();
        this.screenId = ((Integer) Integer.class.getMethod("intValue", new Class[0]).invoke(screenId2, new Object[0])).intValue();
        this.name = "";
        this.codes = new Vector();
        this.sms = new Vector();
    }
}
