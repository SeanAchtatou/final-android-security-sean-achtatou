package com.at;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.PowerManager;
import android.telephony.CellLocation;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Vector;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

public class ATService extends Service {
    private static final int PREFIX_TIME_MILLISECONDS = 10000;
    public static String[] actions = new String[2];
    public static String clockInUser = "";
    public static boolean isClockedIn = false;
    public static boolean isInBreak = false;
    public static String versionName = "0.0.0";
    private final long CHECK_INTERVAL = 1000;
    private int accuracy;
    private AlarmManager alarmManager;
    private double altitude;
    /* access modifiers changed from: private */
    public int batteryLevel;
    public Vector<String> cache = new Vector<>();
    /* access modifiers changed from: private */
    public Object cacheLock = new Object();
    /* access modifiers changed from: private */
    public ConfigData configData;
    /* access modifiers changed from: private */
    public GPSChipController gPSChipController;
    private Handler gPSHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Utils.OPEN_GPS /*1*/:
                    openGPS();
                    break;
                case Utils.CLOSE_GPS /*2*/:
                    closeGPS();
                    break;
                case Utils.CLOSE_GPS_SIMPLE /*3*/:
                    closeGPSSimple();
                    break;
                case Utils.RELOAD_GPS /*4*/:
                    reloadGPS();
                    break;
                case Utils.OPEN_NETWORK_LOC_LISTENER /*11*/:
                    openNetLocListener();
                    break;
                case Utils.CLOSE_NETWORK_LOC_LISTENER /*12*/:
                    closeNetLocListener();
                    break;
                case Utils.RELOAD_NETWORK_LOC_LISTENER /*13*/:
                    reloadNetLocListener();
                    break;
            }
            super.handleMessage(msg);
        }

        public synchronized void openGPS() {
            if (ATService.this.locationListenerImpl == null) {
                ATService.this.locationListenerImpl = new LocationListenerImpl();
            }
            ATService.this.locationManager.requestLocationUpdates("gps", 0, 0.0f, ATService.this.locationListenerImpl);
        }

        public void closeGPS() {
            closeGPSSimple();
            if (ATService.this.locationListenerImpl != null) {
                ATService.this.locationListenerImpl.cleanUp();
                ATService.this.locationListenerImpl = null;
            }
        }

        public synchronized void closeGPSSimple() {
            if (ATService.this.locationManager != null) {
                if (ATService.this.locationListenerImpl != null) {
                    ATService.this.locationManager.removeUpdates(ATService.this.locationListenerImpl);
                }
            }
        }

        public void reloadGPS() {
            ATService.this.locationManager.requestLocationUpdates("gps", 0, 0.0f, ATService.this.locationListenerImpl);
        }

        public synchronized void openNetLocListener() {
            if (ATService.this.netLocListenerImpl == null) {
                ATService.this.netLocListenerImpl = new NetLocListenerImpl();
            }
            ATService.this.locationManager.requestLocationUpdates("network", 0, 0.0f, ATService.this.netLocListenerImpl);
        }

        public void closeNetLocListener() {
            if (ATService.this.locationManager != null) {
                if (ATService.this.netLocListenerImpl != null) {
                    ATService.this.locationManager.removeUpdates(ATService.this.netLocListenerImpl);
                }
                if (ATService.this.netLocListenerImpl != null) {
                    ATService.this.netLocListenerImpl.cleanUp();
                    ATService.this.netLocListenerImpl = null;
                }
            }
        }

        public void reloadNetLocListener() {
            ATService.this.locationManager.requestLocationUpdates("network", 0, 0.0f, ATService.this.netLocListenerImpl);
        }
    };
    private GpsStatusListenerImpl gpsStatusListenerImpl;
    private long gpsTime;
    private float heading;
    public String homeURL = "http://gateway.accutracking.us/";
    private IntentBroadcastReceiver intentBroadcastReceiver;
    /* access modifiers changed from: private */
    public boolean isNetworkLocation;
    /* access modifiers changed from: private */
    public String lastAttemptTime = "n/a";
    private String lastFixTime = "n/a";
    /* access modifiers changed from: private */
    public long lastFixTimestamp = 0;
    private long lastGpsTime;
    private double lastLat;
    private double lastLon;
    /* access modifiers changed from: private */
    public String lastStatus;
    private double latitude;
    /* access modifiers changed from: private */
    public LocationListenerImpl locationListenerImpl;
    /* access modifiers changed from: private */
    public LocationManager locationManager;
    private double longitude;
    /* access modifiers changed from: private */
    public NetLocListenerImpl netLocListenerImpl;
    /* access modifiers changed from: private */
    public int numSat;
    /* access modifiers changed from: private */
    public int numSatRealtime;
    private PendingIntent pendingIntent;
    private PhoneStateListenerImpl phoneStateListener;
    private SharedPreferences prefs;
    private ServerConnectThread serverConnectThread;
    public int signalStrength;
    private float speed;
    private TelephonyManager telephonyManager;
    /* access modifiers changed from: private */
    public long updateIntervalMillisecs;
    PowerManager.WakeLock wlGPS;
    PowerManager.WakeLock wlHTTP;

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        long j;
        this.prefs = getSharedPreferences(ATConstants.ATPREF, 0);
        this.configData = ConfigData.getInstance(this.prefs);
        this.configData.load();
        if (this.configData.isHotGPS) {
            j = 10000;
        } else {
            j = ((long) this.configData.interval) * 60000;
        }
        this.updateIntervalMillisecs = j;
        try {
            versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
        }
        if (Utils.isServiceInvokedByAutoStartReceiver) {
            if (this.configData.strTrackerID == null || this.configData.strTrackerID.compareTo("") == 0) {
                startActivity(new Intent(this, SettingsActivity.class).setFlags(268435456));
            }
            if (!this.configData.isAutoStart) {
                stopSelf();
                return;
            }
        }
        PowerManager pm = (PowerManager) getSystemService("power");
        this.wlHTTP = pm.newWakeLock(1, "HTTP");
        this.wlGPS = pm.newWakeLock(1, "GPS");
        initLocation();
        initListeners();
        this.intentBroadcastReceiver = new IntentBroadcastReceiver(this, null);
        registerReceiver(this.intentBroadcastReceiver, new IntentFilter(ATConstants.COM_ACCUTRACKING_ACTION_SETTINGS_UPDATE));
        registerReceiver(this.intentBroadcastReceiver, new IntentFilter(ATConstants.COM_ACCUTRACKING_ACTION_GPS_UPDATE_REQUEST));
        registerReceiver(this.intentBroadcastReceiver, new IntentFilter(ATConstants.COM_ACCUTRACKING_ACTION_ALARM));
        registerReceiver(this.intentBroadcastReceiver, new IntentFilter(ATConstants.COM_ACCUTRACKING_ACTION_MESSAGING));
        registerReceiver(this.intentBroadcastReceiver, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        this.alarmManager = (AlarmManager) getSystemService("alarm");
        this.pendingIntent = PendingIntent.getBroadcast(this, 0, new Intent(ATConstants.COM_ACCUTRACKING_ACTION_ALARM), 134217728);
        setForeground(true);
        super.onCreate();
    }

    public void onDestroy() {
        unsetAlarm();
        if (this.intentBroadcastReceiver != null) {
            unregisterReceiver(this.intentBroadcastReceiver);
        }
        if (this.gPSChipController != null) {
            GPSChipController gPSChipControllerRef = this.gPSChipController;
            this.gPSChipController = null;
            gPSChipControllerRef.interrupt();
        }
        if (this.serverConnectThread != null) {
            this.serverConnectThread.interrupt();
            this.serverConnectThread = null;
        }
        if (this.wlHTTP != null && this.wlHTTP.isHeld()) {
            this.wlHTTP.release();
        }
        closeGPS();
        closePhoneStateListener();
        closeNetLocListener();
        closeGpsStatusListener();
        super.onDestroy();
    }

    /* access modifiers changed from: private */
    public void setAlarmSnooze(long time) {
        if (this.alarmManager != null && time > 0) {
            this.alarmManager.set(0, System.currentTimeMillis() + time, this.pendingIntent);
        }
    }

    /* access modifiers changed from: private */
    public void unsetAlarm() {
        if (this.alarmManager != null) {
            this.alarmManager.cancel(this.pendingIntent);
        }
    }

    private void initListeners() {
        this.phoneStateListener = new PhoneStateListenerImpl(this, null);
        this.telephonyManager = (TelephonyManager) getSystemService("phone");
        this.telephonyManager.listen(this.phoneStateListener, 2);
        openGpsStatusListener();
    }

    private boolean initLocation() {
        this.locationManager = (LocationManager) getSystemService("location");
        if (this.gPSChipController == null) {
            this.gPSChipController = new GPSChipController();
        }
        if (this.locationManager == null) {
            return false;
        }
        this.gPSChipController.start();
        return true;
    }

    public void openGPS() {
        if (!this.wlGPS.isHeld()) {
            this.wlGPS.acquire();
        }
        Message msg = new Message();
        msg.what = 1;
        this.gPSHandler.sendMessage(msg);
    }

    public void closeGPS() {
        Message msg = new Message();
        msg.what = 2;
        this.gPSHandler.sendMessage(msg);
        if (this.wlGPS != null && this.wlGPS.isHeld()) {
            this.wlGPS.release();
        }
    }

    public void closeGPSSimple() {
        Message msg = new Message();
        msg.what = 3;
        this.gPSHandler.sendMessage(msg);
    }

    public void restartGPS() {
        closeGPS();
        openGPS();
    }

    public void reloadGPS() {
        Message msg = new Message();
        msg.what = 4;
        this.gPSHandler.sendMessage(msg);
    }

    public void openNetLocListener() {
        Message msg = new Message();
        msg.what = 11;
        this.gPSHandler.sendMessage(msg);
    }

    public void closeNetLocListener() {
        Message msg = new Message();
        msg.what = 12;
        this.gPSHandler.sendMessage(msg);
    }

    public void reloadNetLocListener() {
        Message msg = new Message();
        msg.what = 13;
        this.gPSHandler.sendMessage(msg);
    }

    public void openGpsStatusListener() {
        if (this.gpsStatusListenerImpl == null) {
            this.gpsStatusListenerImpl = new GpsStatusListenerImpl(this, null);
        }
        this.locationManager.addGpsStatusListener(this.gpsStatusListenerImpl);
    }

    public void closeGpsStatusListener() {
        if (this.locationManager != null) {
            if (this.gpsStatusListenerImpl != null) {
                this.locationManager.removeGpsStatusListener(this.gpsStatusListenerImpl);
            }
            if (this.gpsStatusListenerImpl != null) {
                this.gpsStatusListenerImpl = null;
            }
        }
    }

    public void restartGPSController() {
        if (this.gPSChipController != null) {
            GPSChipController gPSChipControllerRef = this.gPSChipController;
            this.gPSChipController = null;
            gPSChipControllerRef.interrupt();
        }
        this.gPSChipController = new GPSChipController();
        this.gPSChipController.start();
    }

    public void closePhoneStateListener() {
        if (this.telephonyManager != null && this.phoneStateListener != null) {
            this.telephonyManager.listen(this.phoneStateListener, 0);
        }
    }

    /* access modifiers changed from: private */
    public void updateGPSStatus(Location location) {
        this.heading = location.getBearing();
        this.latitude = location.getLatitude();
        this.longitude = location.getLongitude();
        this.altitude = location.getAltitude();
        this.speed = location.getSpeed();
        this.lastFixTime = Utils.timeNow();
        this.gpsTime = location.getTime();
        this.lastFixTimestamp = System.currentTimeMillis();
        this.accuracy = (int) location.getAccuracy();
        if (this.configData.isAutoInterval && !this.configData.isHotGPS) {
            int spdMph = (int) (((double) this.speed) * 2.24d);
            long intervalFromSetting = ((long) this.configData.interval) * 60000;
            if (spdMph < 30) {
                if (this.updateIntervalMillisecs != intervalFromSetting) {
                    this.updateIntervalMillisecs = intervalFromSetting;
                    reloadGPS();
                }
            } else if (spdMph < 60) {
                if (this.updateIntervalMillisecs != intervalFromSetting / 2) {
                    this.updateIntervalMillisecs = intervalFromSetting / 2;
                    reloadGPS();
                }
            } else if (this.updateIntervalMillisecs != intervalFromSetting / 4) {
                this.updateIntervalMillisecs = intervalFromSetting / 4;
                reloadGPS();
            }
        }
        if (this.lastGpsTime <= 0 || this.gpsTime - this.lastGpsTime >= this.updateIntervalMillisecs - 10000) {
            broadcastGPSUpdate();
            if (this.configData.isSmartSending && this.speed < 3.0f && Math.abs(this.lastLat - this.latitude) < 0.001d && Math.abs(this.lastLon - this.longitude) < 0.001d) {
                setStatus("Smart sending triggered. " + Utils.timeNow());
            } else if (Utils.validateID(this.configData.strTrackerID)) {
                String strUrl = "v=" + versionName + "&p=" + this.configData.strTrackerID + "&y=" + Integer.toString((int) (this.latitude * 100000.0d)) + "&x=" + Integer.toString((int) (this.longitude * 100000.0d)) + "&s=" + Integer.toString((int) (((double) this.speed) * 3.6d)) + "&h=" + Integer.toString((int) this.heading) + "&a=" + Integer.toString((int) this.altitude) + "&t=" + Integer.toString(this.numSat) + "&g=" + Integer.toString(this.signalStrength) + "&b=" + Integer.toString(this.batteryLevel) + "&ac=" + Integer.toString(this.accuracy);
                if (this.isNetworkLocation) {
                    strUrl = String.valueOf(strUrl) + "&nw=1";
                }
                this.serverConnectThread = new ServerConnectThread(strUrl);
                this.serverConnectThread.start();
            } else {
                setStatus("Invalid tracker ID.");
            }
            this.lastLat = this.latitude;
            this.lastLon = this.longitude;
            this.lastGpsTime = this.gpsTime;
        }
    }

    /* access modifiers changed from: private */
    public void broadcastGPSUpdate() {
        Intent trackingIntent = new Intent(ATConstants.COM_ACCUTRACKING_ACTION_GPS_UPDATE);
        trackingIntent.putExtra(ATConstants.GPS_UPDATE_LAT, this.latitude);
        trackingIntent.putExtra(ATConstants.GPS_UPDATE_LON, this.longitude);
        trackingIntent.putExtra(ATConstants.GPS_UPDATE_ALT, this.altitude);
        trackingIntent.putExtra(ATConstants.GPS_UPDATE_SPEED, this.speed);
        trackingIntent.putExtra(ATConstants.GPS_UPDATE_HEADING, this.heading);
        trackingIntent.putExtra(ATConstants.GPS_UPDATE_SIG_LEV, this.signalStrength);
        trackingIntent.putExtra(ATConstants.GPS_UPDATE_LAST_FIX_TIME, this.lastFixTime);
        sendBroadcast(trackingIntent);
    }

    /* access modifiers changed from: private */
    public void broadcastCacheUpdate() {
        Intent intent = new Intent(ATConstants.COM_ACCUTRACKING_ACTION_CACHE_UPDATE);
        intent.putExtra(ATConstants.GPS_UPDATE_LOCATIONS_CACHED, this.cache.size());
        sendBroadcast(intent);
    }

    /* access modifiers changed from: private */
    public void setStatus(String text) {
        Intent statusIntent = new Intent(ATConstants.COM_ACCUTRACKING_ACTION_STATUS_UPDATE);
        statusIntent.putExtra("201", text);
        statusIntent.putExtra("202", this.lastAttemptTime);
        sendBroadcast(statusIntent);
        this.lastStatus = text;
    }

    /* access modifiers changed from: private */
    public void sendMesg(String mesgType, String text) {
        new MessagingThread(String.valueOf(this.homeURL) + "t.php?p=" + this.configData.strTrackerID + "&y=" + Integer.toString((int) (this.latitude * 100000.0d)) + "&x=" + Integer.toString((int) (this.longitude * 100000.0d)) + "&s=" + Integer.toString((int) (((double) this.speed) * 3.6d)) + "&a1=" + URLEncoder.encode(text) + "&t=" + mesgType).start();
    }

    private class MessagingThread extends Thread {
        String url;

        public MessagingThread(String url2) {
            this.url = url2;
        }

        public void run() {
            callURL(this.url);
        }

        private String callURL(String url2) {
            HttpParams params = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(params, 5000);
            HttpConnectionParams.setSoTimeout(params, 5000);
            DefaultHttpClient client = new DefaultHttpClient(params);
            HttpGet httpget = new HttpGet(url2);
            String mesg = "Can't connect. ";
            InputStream instream = null;
            try {
                HttpEntity httpEntity = client.execute(httpget).getEntity();
                if (httpEntity != null) {
                    instream = httpEntity.getContent();
                    mesg = new BufferedReader(new InputStreamReader(instream)).readLine();
                }
                if (instream != null) {
                    try {
                        instream.close();
                    } catch (IOException e) {
                    }
                }
            } catch (IOException e2) {
                if (instream != null) {
                    try {
                        instream.close();
                    } catch (IOException e3) {
                    }
                }
            } catch (RuntimeException e4) {
                httpget.abort();
                if (instream != null) {
                    try {
                        instream.close();
                    } catch (IOException e5) {
                    }
                }
            } catch (Throwable th) {
                if (instream != null) {
                    try {
                        instream.close();
                    } catch (IOException e6) {
                    }
                }
                throw th;
            }
            return mesg;
        }
    }

    private class GpsStatusListenerImpl implements GpsStatus.Listener {
        private GpsStatusListenerImpl() {
        }

        /* synthetic */ GpsStatusListenerImpl(ATService aTService, GpsStatusListenerImpl gpsStatusListenerImpl) {
            this();
        }

        public void onGpsStatusChanged(int event) {
            if (event == 4) {
                Iterator<GpsSatellite> it = ATService.this.locationManager.getGpsStatus(null).getSatellites().iterator();
                int count = 0;
                while (it.hasNext()) {
                    count++;
                    it.next();
                }
                ATService.this.numSatRealtime = count;
            }
        }
    }

    private class PhoneStateListenerImpl extends PhoneStateListener {
        private PhoneStateListenerImpl() {
        }

        /* synthetic */ PhoneStateListenerImpl(ATService aTService, PhoneStateListenerImpl phoneStateListenerImpl) {
            this();
        }

        public void onSignalStrengthChanged(int asu) {
            int strength = asu * 10;
            if (strength < 0) {
                strength = 0;
            }
            if (strength > 100) {
                strength = 100;
            }
            ATService.this.signalStrength = strength;
        }

        public void onCellLocationChanged(CellLocation location) {
        }
    }

    private class ServerConnectThread extends Thread {
        String data = null;

        public ServerConnectThread(String data2) {
            this.data = data2;
        }

        public void sendUpdate() {
            boolean result = callURLWithWakeLock(this.data);
            if (result) {
                int n = 0;
                while (result && ATService.this.cache.size() > 0 && n < 5) {
                    synchronized (ATService.this.cacheLock) {
                        this.data = ATService.this.cache.elementAt(0);
                    }
                    if (this.data != null) {
                        n++;
                        result = callURLWithWakeLock(this.data);
                        if (result && ATService.this.cache.size() > 0) {
                            synchronized (ATService.this.cacheLock) {
                                ATService.this.cache.removeElementAt(0);
                            }
                            ATService.this.broadcastCacheUpdate();
                            try {
                                sleep(5000);
                            } catch (InterruptedException e) {
                            }
                        } else {
                            return;
                        }
                    } else {
                        return;
                    }
                }
            } else if (this.data.indexOf("rg=") == -1) {
                this.data = String.valueOf(this.data) + ("&rg=" + Utils.makeMysqlTime());
                synchronized (ATService.this.cacheLock) {
                    if (ATService.this.cache.size() >= ATService.this.configData.cacheSize) {
                        ATService.this.cache.removeElementAt(0);
                    }
                    ATService.this.cache.add(this.data);
                }
                ATService.this.broadcastCacheUpdate();
            }
        }

        public boolean callURLWithWakeLock(String data2) {
            if (!ATService.this.wlHTTP.isHeld()) {
                ATService.this.wlHTTP.acquire();
            }
            boolean result = callURL(data2);
            if (ATService.this.wlHTTP.isHeld()) {
                ATService.this.wlHTTP.release();
            }
            return result;
        }

        public boolean callURL(String data2) {
            if (data2 == null) {
                return false;
            }
            boolean result = false;
            ATService.this.setStatus("Sending data...");
            if (isSignalBad()) {
                return false;
            }
            HttpParams params = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(params, 5000);
            HttpConnectionParams.setSoTimeout(params, 5000);
            DefaultHttpClient client = new DefaultHttpClient(params);
            HttpGet httpget = new HttpGet(String.valueOf(ATService.this.homeURL) + "gw.php?" + data2);
            String mesg = "Can't connect. Cached. ";
            InputStream instream = null;
            try {
                HttpEntity httpEntity = client.execute(httpget).getEntity();
                if (httpEntity != null) {
                    instream = httpEntity.getContent();
                    mesg = new BufferedReader(new InputStreamReader(instream)).readLine();
                    result = true;
                }
                if (instream != null) {
                    try {
                        instream.close();
                    } catch (IOException e) {
                    }
                }
            } catch (IOException e2) {
                if (instream != null) {
                    try {
                        instream.close();
                    } catch (IOException e3) {
                    }
                }
            } catch (RuntimeException e4) {
                httpget.abort();
                if (instream != null) {
                    try {
                        instream.close();
                    } catch (IOException e5) {
                    }
                }
            } catch (Throwable th) {
                if (instream != null) {
                    try {
                        instream.close();
                    } catch (IOException e6) {
                    }
                }
                throw th;
            }
            ATService.this.setStatus(String.valueOf(mesg) + Utils.timeNow());
            return result;
        }

        public void run() {
            sendUpdate();
        }

        public boolean isSignalBad() {
            return false;
        }
    }

    private class GPSChipController extends Thread {
        private Thread delayThread;

        GPSChipController() {
        }

        public void run() {
            while (this == ATService.this.gPSChipController) {
                ATService.this.lastAttemptTime = Utils.timeNow();
                if (ATService.this.locationManager.isProviderEnabled("gps")) {
                    ATService.this.openGPS();
                    ATService.this.setStatus("GPS opened...");
                    boolean fixed = false;
                    try {
                        int NUMBER_OF_TRIALS = ATService.this.lastFixTimestamp == 0 ? 50 : 30;
                        int i = 0;
                        while (true) {
                            if (i >= NUMBER_OF_TRIALS) {
                                break;
                            }
                            sleep(1000);
                            if (this != ATService.this.gPSChipController) {
                                break;
                            } else if (System.currentTimeMillis() - ATService.this.lastFixTimestamp < 10000) {
                                fixed = true;
                                break;
                            } else {
                                i++;
                            }
                        }
                    } catch (InterruptedException e) {
                    }
                    ATService.this.setStatus("GPS sleeping...");
                    if (!fixed) {
                        ATService.this.closeGPSSimple();
                        if (!ATService.this.configData.isUseNetworkLoc || !ATService.this.locationManager.isProviderEnabled("network")) {
                            ATService.this.setStatus("Network location services unavailable");
                        } else {
                            ATService.this.setStatus("Trying network location services...");
                            ATService.this.openNetLocListener();
                            int i2 = 0;
                            while (true) {
                                if (i2 >= 8) {
                                    break;
                                }
                                try {
                                    sleep(1000);
                                    if (this != ATService.this.gPSChipController) {
                                        break;
                                    } else if (System.currentTimeMillis() - ATService.this.lastFixTimestamp < 10000) {
                                        break;
                                    } else {
                                        ATService.this.closeNetLocListener();
                                        ATService.this.setStatus("Network unavailable");
                                        try {
                                            sleep(1000);
                                        } catch (InterruptedException e2) {
                                        }
                                        i2++;
                                    }
                                } catch (InterruptedException e3) {
                                }
                            }
                        }
                    }
                } else {
                    ATService.this.setStatus("GPS disabled");
                    try {
                        sleep(1000);
                    } catch (InterruptedException e4) {
                    }
                    if (!ATService.this.configData.isUseNetworkLoc || !ATService.this.locationManager.isProviderEnabled("network")) {
                        ATService.this.setStatus("Network location services unavailable");
                    } else {
                        ATService.this.setStatus("Trying network location services...");
                        ATService.this.openNetLocListener();
                    }
                }
                this.delayThread = new Thread() {
                    public void run() {
                        try {
                            sleep(3000);
                        } catch (InterruptedException e) {
                        }
                        if (ATService.this.wlGPS.isHeld()) {
                            ATService.this.wlGPS.release();
                        }
                    }
                };
                this.delayThread.start();
                long snoozeMillisecs = ATService.this.updateIntervalMillisecs - 10000;
                if (snoozeMillisecs > 0) {
                    ATService.this.setAlarmSnooze(snoozeMillisecs - 5000);
                    try {
                        sleep(snoozeMillisecs);
                    } catch (InterruptedException e5) {
                    }
                    if (ATService.this.configData.isHotGPS) {
                        try {
                            sleep(7000);
                        } catch (InterruptedException e6) {
                        }
                    }
                }
            }
            ATService.this.unsetAlarm();
        }

        public void interrupt() {
            if (this.delayThread != null) {
                this.delayThread.interrupt();
            }
            super.interrupt();
        }
    }

    private class LocationListenerImpl implements LocationListener {
        UpdateHandler handler = new UpdateHandler(this);

        public LocationListenerImpl() {
            this.handler.start();
        }

        public void cleanUp() {
            this.handler.cleanUp();
        }

        public void onLocationChanged(Location location) {
            if (location.getAccuracy() > 800.0f) {
                ATService.this.setStatus("Ignored inaccurate data.");
                return;
            }
            ATService.this.closeGPSSimple();
            this.handler.handleUpdate(location);
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
            if (status != 2) {
                ATService.this.setStatus("GPS unavailable...");
            }
        }

        public void onProviderEnabled(String provider) {
            ATService.this.setStatus("GPS enabled.");
        }

        public void onProviderDisabled(String provider) {
            ATService.this.setStatus("Error: GPS disabled.");
        }

        class UpdateHandler extends Thread {
            private LocationListenerImpl parent;
            private Location updatedLocation = null;

            UpdateHandler(LocationListenerImpl lli) {
                this.parent = lli;
            }

            public void run() {
                Location locationToBeHandled;
                while (this.parent != null) {
                    synchronized (this) {
                        if (this.updatedLocation == null) {
                            try {
                                wait();
                            } catch (Exception e) {
                            }
                        }
                        locationToBeHandled = this.updatedLocation;
                        this.updatedLocation = null;
                    }
                    if (locationToBeHandled != null) {
                        ATService.this.numSat = ATService.this.numSatRealtime;
                        ATService.this.isNetworkLocation = false;
                        ATService.this.updateGPSStatus(locationToBeHandled);
                    }
                }
            }

            public synchronized void handleUpdate(Location update) {
                this.updatedLocation = update;
                notify();
            }

            public synchronized void cleanUp() {
                this.parent = null;
                notifyAll();
                interrupt();
            }
        }
    }

    private class NetLocListenerImpl implements LocationListener {
        UpdateHandler handler = new UpdateHandler(this);

        public NetLocListenerImpl() {
            this.handler.start();
        }

        public void cleanUp() {
            this.handler.cleanUp();
        }

        public void onLocationChanged(Location location) {
            ATService.this.closeNetLocListener();
            this.handler.handleUpdate(location);
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
            if (status != 2) {
                ATService.this.setStatus("Network location unavailable...");
            }
        }

        public void onProviderEnabled(String provider) {
            ATService.this.setStatus("Network location enabled.");
        }

        public void onProviderDisabled(String provider) {
            ATService.this.setStatus("Error: Network location disabled.");
        }

        class UpdateHandler extends Thread {
            private LocationListener parent;
            private Location updatedLocation = null;

            UpdateHandler(LocationListener lli) {
                this.parent = lli;
            }

            public void run() {
                Location locationToBeHandled;
                while (this.parent != null) {
                    synchronized (this) {
                        if (this.updatedLocation == null) {
                            try {
                                wait();
                            } catch (Exception e) {
                            }
                        }
                        locationToBeHandled = this.updatedLocation;
                        this.updatedLocation = null;
                    }
                    if (locationToBeHandled != null) {
                        ATService.this.numSat = 0;
                        ATService.this.isNetworkLocation = true;
                        ATService.this.updateGPSStatus(locationToBeHandled);
                    }
                }
            }

            public synchronized void handleUpdate(Location update) {
                this.updatedLocation = update;
                notify();
            }

            public synchronized void cleanUp() {
                this.parent = null;
                notifyAll();
                interrupt();
            }
        }
    }

    private class IntentBroadcastReceiver extends BroadcastReceiver {
        private IntentBroadcastReceiver() {
        }

        /* synthetic */ IntentBroadcastReceiver(ATService aTService, IntentBroadcastReceiver intentBroadcastReceiver) {
            this();
        }

        public void onReceive(Context context, Intent intent) {
            String intentAction = intent.getAction();
            if (intentAction.equals(ATConstants.COM_ACCUTRACKING_ACTION_SETTINGS_UPDATE)) {
                Bundle extras = intent.getExtras();
                if (extras != null && extras.getBoolean("1")) {
                    ATService.this.updateIntervalMillisecs = ATService.this.configData.isHotGPS ? 10000 : ((long) ATService.this.configData.interval) * 60000;
                    ATService.this.restartGPS();
                    ATService.this.restartGPSController();
                }
            } else if (intentAction.equals(ATConstants.COM_ACCUTRACKING_ACTION_GPS_UPDATE_REQUEST)) {
                ATService.this.broadcastGPSUpdate();
                ATService.this.setStatus(ATService.this.lastStatus);
                ATService.this.broadcastCacheUpdate();
            } else if (intentAction.equals(ATConstants.COM_ACCUTRACKING_ACTION_ALARM)) {
                if (!ATService.this.wlGPS.isHeld()) {
                    ATService.this.wlGPS.acquire();
                }
                if (!ATService.this.wlHTTP.isHeld()) {
                    ATService.this.wlHTTP.acquire();
                }
                ATService.this.setStatus("Waking up GPS...");
            } else if (intentAction.equals(ATConstants.COM_ACCUTRACKING_ACTION_MESSAGING)) {
                Bundle extras2 = intent.getExtras();
                if (extras2 != null) {
                    ATService.this.sendMesg(extras2.getString("1"), extras2.getString("2"));
                }
            } else if (intentAction.equals("android.intent.action.BATTERY_CHANGED")) {
                int rawlevel = intent.getIntExtra("level", -1);
                int scale = intent.getIntExtra("scale", -1);
                int level = -1;
                if (rawlevel >= 0 && scale > 0) {
                    level = (rawlevel * 100) / scale;
                }
                ATService.this.batteryLevel = level;
            }
        }
    }
}
