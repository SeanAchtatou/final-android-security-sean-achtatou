package com.at;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int about = 2130837504;
        public static final int exit = 2130837505;
        public static final int icon = 2130837506;
        public static final int iconsmall = 2130837507;
    }

    public static final class id {
        public static final int about = 2131099672;
        public static final int btnBackground = 2131099652;
        public static final int btnSettings = 2131099653;
        public static final int btnTimeTrack = 2131099654;
        public static final int checkboxAutoInterval = 2131099663;
        public static final int checkboxAutoStart = 2131099664;
        public static final int checkboxHotGPS = 2131099662;
        public static final int checkboxNetworkLoc = 2131099665;
        public static final int checkboxPwdProtect = 2131099667;
        public static final int checkboxSmartSending = 2131099661;
        public static final int editCache = 2131099666;
        public static final int editInterval = 2131099658;
        public static final int editPwd = 2131099669;
        public static final int editPwd2 = 2131099671;
        public static final int editTrackerID = 2131099660;
        public static final int exit = 2131099673;
        public static final int labelCache = 2131099650;
        public static final int labelGPS = 2131099649;
        public static final int labelInterval = 2131099657;
        public static final int labelPwd = 2131099668;
        public static final int labelPwd2 = 2131099670;
        public static final int labelSigLev = 2131099651;
        public static final int labelStatus = 2131099648;
        public static final int labelTrackerID = 2131099659;
        public static final int password_edit = 2131099656;
        public static final int password_view = 2131099655;
    }

    public static final class layout {
        public static final int main = 2130903040;
        public static final int password = 2130903041;
        public static final int settings = 2130903042;
        public static final int timetrack = 2130903043;
    }

    public static final class menu {
        public static final int mainmenu = 2131034112;
    }

    public static final class string {
        public static final int app_contacts = 2130968577;
        public static final int app_name = 2130968576;
    }
}
