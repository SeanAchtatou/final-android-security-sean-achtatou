package com.at;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class Utils {
    public static final int CLOSE_GPS = 2;
    public static final int CLOSE_GPS_SIMPLE = 3;
    public static final int CLOSE_NETWORK_LOC_LISTENER = 12;
    public static final int OPEN_GPS = 1;
    public static final int OPEN_NETWORK_LOC_LISTENER = 11;
    public static final int RELOAD_GPS = 4;
    public static final int RELOAD_NETWORK_LOC_LISTENER = 13;
    public static boolean isServiceInvokedByAutoStartReceiver = false;

    public static String timeNow() {
        String ampmString;
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        int h = c.get(10);
        int m = c.get(12);
        int s = c.get(13);
        int ampm = c.get(9);
        String strMin = Integer.toString(m);
        String strSec = Integer.toString(s);
        if (m < 10) {
            strMin = "0" + strMin;
        }
        if (s < 10) {
            strSec = "0" + strSec;
        }
        if (ampm == 0) {
            ampmString = " AM";
        } else {
            if (h == 0) {
                h = 12;
            }
            ampmString = " PM";
        }
        return String.valueOf(h) + ":" + strMin + ":" + strSec + ampmString;
    }

    public static boolean validateID(String id) {
        if (id == null || id.length() < 10) {
            return false;
        }
        if (Integer.parseInt(id.substring(0, 6)) == 0 && Integer.parseInt(id.substring(6)) == 0) {
            return false;
        }
        int sum1 = ((id.charAt(1) - '0') * 1000) + ((id.charAt(3) - '0') * 100) + ((id.charAt(5) - '0') * 10) + (id.charAt(8) - '0');
        int sum2 = ((id.charAt(0) - '0') * 100000) + ((id.charAt(2) - '0') * 10000) + ((id.charAt(4) - '0') * 1000) + ((id.charAt(6) - '0') * 100) + ((id.charAt(7) - '0') * 10) + (id.charAt(9) - '0');
        if (sum1 % 3 == 0 && sum2 % 17 == 0) {
            return true;
        }
        return false;
    }

    public static double round(double d, int decimal) {
        double powerOfTen = 1.0d;
        while (true) {
            int decimal2 = decimal;
            decimal = decimal2 - 1;
            if (decimal2 <= 0) {
                break;
            }
            powerOfTen *= 10.0d;
        }
        double d1 = d * powerOfTen;
        int d1asint = (int) d1;
        if (d1 - ((double) d1asint) >= 0.5d) {
            return ((double) (d1asint + 1)) / powerOfTen;
        }
        return ((double) d1asint) / powerOfTen;
    }

    public static String makeMysqlTime() {
        Calendar c = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        c.setTime(new Date());
        int y = c.get(1);
        int mon = c.get(2) + 1;
        int d = c.get(5);
        int h = c.get(11);
        int min = c.get(12);
        int s = c.get(13);
        String strYear = Integer.toString(y);
        String strMonth = Integer.toString(mon);
        if (mon < 10) {
            strMonth = "0" + strMonth;
        }
        String strDay = Integer.toString(d);
        if (d < 10) {
            strDay = "0" + strDay;
        }
        String strHour = Integer.toString(h);
        if (h < 10) {
            strHour = "0" + strHour;
        }
        String strMin = Integer.toString(min);
        if (min < 10) {
            strMin = "0" + strMin;
        }
        String strSecond = Integer.toString(s);
        if (s < 10) {
            strSecond = "0" + strSecond;
        }
        return String.valueOf(strYear) + strMonth + strDay + strHour + strMin + strSecond;
    }
}
