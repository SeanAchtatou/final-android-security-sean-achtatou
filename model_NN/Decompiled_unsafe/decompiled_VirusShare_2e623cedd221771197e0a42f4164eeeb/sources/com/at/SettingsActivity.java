package com.at;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

public class SettingsActivity extends Activity {
    private CheckBox checkboxAutoInterval;
    private CheckBox checkboxAutoStart;
    private CheckBox checkboxHotGPS;
    private CheckBox checkboxNetworkLoc;
    private CheckBox checkboxPwdProtect;
    private CheckBox checkboxSmartSending;
    private ConfigData configData;
    private EditText editPwd;
    private EditText editPwd2;
    private EditText editTextCache;
    private EditText editTextInterval;
    private EditText editTrackerID;
    private TextView labelPwd;
    private TextView labelPwd2;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initScreen();
        loadSettings();
    }

    private void initScreen() {
        setContentView((int) R.layout.settings);
        this.editTextInterval = (EditText) findViewById(R.id.editInterval);
        this.editTrackerID = (EditText) findViewById(R.id.editTrackerID);
        this.checkboxSmartSending = (CheckBox) findViewById(R.id.checkboxSmartSending);
        this.checkboxHotGPS = (CheckBox) findViewById(R.id.checkboxHotGPS);
        this.checkboxAutoInterval = (CheckBox) findViewById(R.id.checkboxAutoInterval);
        this.checkboxAutoStart = (CheckBox) findViewById(R.id.checkboxAutoStart);
        this.checkboxNetworkLoc = (CheckBox) findViewById(R.id.checkboxNetworkLoc);
        this.editTextCache = (EditText) findViewById(R.id.editCache);
        this.checkboxPwdProtect = (CheckBox) findViewById(R.id.checkboxPwdProtect);
        this.labelPwd = (TextView) findViewById(R.id.labelPwd);
        this.labelPwd2 = (TextView) findViewById(R.id.labelPwd2);
        this.editPwd = (EditText) findViewById(R.id.editPwd);
        this.editPwd2 = (EditText) findViewById(R.id.editPwd2);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    public void loadSettings() {
        this.configData = ConfigData.getInstance(null);
        this.editTextInterval.setText(Integer.toString(this.configData.interval));
        this.editTrackerID.setText(this.configData.strTrackerID);
        this.checkboxSmartSending.setChecked(this.configData.isSmartSending);
        this.checkboxHotGPS.setChecked(this.configData.isHotGPS);
        this.checkboxAutoInterval.setChecked(this.configData.isAutoInterval);
        this.checkboxAutoStart.setChecked(this.configData.isAutoStart);
        this.checkboxNetworkLoc.setChecked(this.configData.isUseNetworkLoc);
        this.editTextCache.setText(Integer.toString(this.configData.cacheSize));
        this.checkboxPwdProtect.setChecked(this.configData.isPwdProtected);
        updateUILogic(this.configData.isPwdProtected);
        this.editPwd.setText(this.configData.strPwd);
        this.editPwd2.setText(this.configData.strPwd);
        this.checkboxPwdProtect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SettingsActivity.this.updateUILogic(isChecked);
            }
        });
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public boolean saveSettings() {
        boolean isAllValid = true;
        int newInterval = this.configData.interval;
        String newIntervalText = this.editTextInterval.getText().toString();
        if (newIntervalText != null) {
            if (newIntervalText.compareTo("") == 0) {
                isAllValid = false;
                AccuTracking.invalidSettingMesg = "Interval value cannot be empty.";
            } else {
                newInterval = Integer.parseInt(newIntervalText);
                if (newInterval < 1 || newInterval > 9999) {
                    isAllValid = false;
                    AccuTracking.invalidSettingMesg = "Invalid interval value. Please enter a number between 1 and 9999.";
                }
            }
        }
        String strNewTrackerID = this.editTrackerID.getText().toString();
        if (!Utils.validateID(strNewTrackerID)) {
            isAllValid = false;
            AccuTracking.invalidSettingMesg = "Invalid tracker ID. Please see the installation guide.";
        }
        boolean isNewSmartSending = this.checkboxSmartSending.isChecked();
        boolean isNewHotGPS = this.checkboxHotGPS.isChecked();
        boolean isNewAutoInterval = this.checkboxAutoInterval.isChecked();
        boolean isNewAutoStart = this.checkboxAutoStart.isChecked();
        boolean isNewNetworkLoc = this.checkboxNetworkLoc.isChecked();
        int newCacheSize = Integer.parseInt(this.editTextCache.getText().toString());
        boolean isNewPwdProtected = this.checkboxPwdProtect.isChecked();
        String strNewPwd = this.editPwd.getText().toString();
        String pwd2 = this.editPwd2.getText().toString();
        if (isNewPwdProtected && strNewPwd.compareTo("") == 0) {
            isAllValid = false;
            AccuTracking.invalidSettingMesg = "Please enter a non-empty password.";
        }
        if (isNewPwdProtected && strNewPwd.compareTo(pwd2) != 0) {
            isAllValid = false;
            AccuTracking.invalidSettingMesg = "Password mismatch.";
        }
        if (isAllValid) {
            int oldInterval = this.configData.interval;
            boolean isOldAutoInterval = this.configData.isAutoInterval;
            boolean isOldSmartSending = this.configData.isSmartSending;
            boolean isOldHotGPS = this.configData.isHotGPS;
            boolean isOldNetworkLoc = this.configData.isUseNetworkLoc;
            String strOldTrackerID = this.configData.strTrackerID;
            this.configData.interval = newInterval;
            this.configData.isAutoInterval = isNewAutoInterval;
            this.configData.isAutoStart = isNewAutoStart;
            this.configData.isSmartSending = isNewSmartSending;
            this.configData.isHotGPS = isNewHotGPS;
            this.configData.isUseNetworkLoc = isNewNetworkLoc;
            this.configData.strTrackerID = strNewTrackerID;
            this.configData.cacheSize = newCacheSize;
            this.configData.isPwdProtected = isNewPwdProtected;
            this.configData.strPwd = strNewPwd;
            this.configData.save();
            Intent intent = new Intent(ATConstants.COM_ACCUTRACKING_ACTION_SETTINGS_UPDATE);
            if (!(this.configData.interval == oldInterval && this.configData.isHotGPS == isOldHotGPS)) {
                intent.putExtra("1", true);
                intent.putExtra(ATConstants.SETTINGS_NEW_INTERVAL, this.configData.interval);
            }
            if (this.configData.isAutoInterval != isOldAutoInterval) {
                intent.putExtra("2", true);
            }
            if (this.configData.isSmartSending != isOldSmartSending) {
                intent.putExtra(ATConstants.SETTINGS_UPDATE_SMART_SENDING, true);
            }
            if (this.configData.isUseNetworkLoc != isOldNetworkLoc) {
                intent.putExtra(ATConstants.SETTINGS_UPDATE_NETWORK_LOC, true);
            }
            if (!(this.configData.strTrackerID == null || this.configData.strTrackerID.compareTo(strOldTrackerID) == 0)) {
                intent.putExtra(ATConstants.SETTINGS_UPDATE_TRACKER_ID, true);
                intent.putExtra(ATConstants.SETTINGS_NEW_TRACKER_ID, this.configData.strTrackerID);
            }
            if (intent.getExtras() != null) {
                sendBroadcast(intent);
            }
        }
        return isAllValid;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        AccuTracking.bSettingsValid = saveSettings();
        super.onPause();
    }

    /* access modifiers changed from: private */
    public void updateUILogic(boolean isChecked) {
        this.labelPwd.setEnabled(isChecked);
        this.labelPwd2.setEnabled(isChecked);
        this.editPwd.setEnabled(isChecked);
        this.editPwd2.setEnabled(isChecked);
    }
}
