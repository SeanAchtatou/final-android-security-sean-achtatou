package com.at;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.util.regex.Pattern;

public class AccuTracking extends Activity {
    public static boolean bSettingsValid = true;
    public static String invalidSettingMesg = "";
    /* access modifiers changed from: private */
    public ConfigData configData;
    private IntentBroadcastReceiver intentBroadcastReceiver;
    private SharedPreferences prefs;
    private TextView textViewCache;
    private TextView textViewGPS;
    private TextView textViewSigLev;
    private TextView textViewStatus;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setPersistent(true);
        this.prefs = getSharedPreferences(ATConstants.ATPREF, 0);
        loadSettings();
        requestWindowFeature(3);
        if (this.configData.isPwdProtected) {
            showPasswordScreen();
        } else {
            initMainScreen();
        }
        if (this.configData.strTrackerID == null || this.configData.strTrackerID.compareTo("") == 0) {
            checkLocationServiceSettings();
        }
        Utils.isServiceInvokedByAutoStartReceiver = false;
        startService(new Intent(this, ATService.class));
        broadcastGPSUpdateRequest();
    }

    /* access modifiers changed from: private */
    public void initMainScreen() {
        setContentView((int) R.layout.main);
        setFeatureDrawableResource(3, R.drawable.iconsmall);
        this.textViewStatus = (TextView) findViewById(R.id.labelStatus);
        this.textViewGPS = (TextView) findViewById(R.id.labelGPS);
        this.textViewCache = (TextView) findViewById(R.id.labelCache);
        this.textViewSigLev = (TextView) findViewById(R.id.labelSigLev);
        ((Button) findViewById(R.id.btnTimeTrack)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AccuTracking.this.showTimeTrackScreen();
            }
        });
        ((Button) findViewById(R.id.btnSettings)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AccuTracking.this.showSettingsScreen();
            }
        });
        ((Button) findViewById(R.id.btnBackground)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AccuTracking.this.finish();
            }
        });
        broadcastGPSUpdateRequest();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        this.intentBroadcastReceiver = new IntentBroadcastReceiver(this, null);
        registerReceiver(this.intentBroadcastReceiver, new IntentFilter(ATConstants.COM_ACCUTRACKING_ACTION_GPS_UPDATE));
        registerReceiver(this.intentBroadcastReceiver, new IntentFilter(ATConstants.COM_ACCUTRACKING_ACTION_STATUS_UPDATE));
        registerReceiver(this.intentBroadcastReceiver, new IntentFilter(ATConstants.COM_ACCUTRACKING_ACTION_CACHE_UPDATE));
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        unregisterReceiver(this.intentBroadcastReceiver);
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!bSettingsValid) {
            showAlertScreen(invalidSettingMesg);
        }
    }

    private void checkLocationServiceSettings() {
        LocationManager locationManager = (LocationManager) getSystemService("location");
        boolean gpsEnabled = locationManager.isProviderEnabled("gps");
        boolean netLocEnabled = locationManager.isProviderEnabled("network");
        if (!gpsEnabled || !netLocEnabled) {
            String mesg = "";
            if (!gpsEnabled && !netLocEnabled) {
                mesg = String.valueOf(mesg) + "GPS Services & Google Location Services are disabled on this phone. Tracking won't work.";
            } else if (!gpsEnabled) {
                mesg = String.valueOf(mesg) + "GPS Services is disabled on this phone. You may see inaccurate or sparse tracking data.";
            } else if (!netLocEnabled) {
                mesg = String.valueOf(mesg) + "Google Location Services is disabled on this phone. You may have sparse tracking when indoors or under poor sky view conditions.";
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(String.valueOf(mesg) + " Would you like to open the Location Settings screen (Settings > Location)?").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    AccuTracking.this.showSettingsScreen();
                    AccuTracking.this.startActivityForResult(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"), 0);
                }
            }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                    AccuTracking.this.showSettingsScreen();
                }
            });
            builder.create().show();
            return;
        }
        showSettingsScreen();
    }

    /* access modifiers changed from: private */
    public void showSettingsScreen() {
        startActivity(new Intent(this, SettingsActivity.class));
    }

    /* access modifiers changed from: private */
    public void showTimeTrackScreen() {
        startActivity(new Intent(this, TimeTrackActivity.class));
    }

    private void showPasswordScreen() {
        final View textEntryView = LayoutInflater.from(this).inflate((int) R.layout.password, (ViewGroup) null);
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Password Required");
        dialog.setView(textEntryView);
        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                if (AccuTracking.this.configData.strPwd.compareTo(((EditText) textEntryView.findViewById(R.id.password_edit)).getText().toString()) == 0) {
                    AccuTracking.this.initMainScreen();
                    return;
                }
                Toast.makeText(AccuTracking.this, "Wrong Password!", 1).show();
                AccuTracking.this.finish();
            }
        });
        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                AccuTracking.this.finish();
            }
        });
        dialog.setCancelable(false);
        dialog.show();
    }

    private void showAboutcreen() {
        TextView textView = new TextView(this);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        String appName = getString(R.string.app_name);
        SpannableString linkifiedMesg = new SpannableString(String.valueOf(appName) + " v" + ATService.versionName + getString(R.string.app_contacts));
        Linkify.addLinks(linkifiedMesg, 1);
        textView.setText(linkifiedMesg);
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("About " + appName);
        dialog.setView(textView);
        dialog.create().show();
    }

    private void showAlertScreen(String mesg) {
        AlertDialog.Builder d = new AlertDialog.Builder(this);
        d.setTitle("Invalid Input");
        d.setMessage(mesg);
        d.setCancelable(false);
        d.setPositiveButton("Try Again", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
                AccuTracking.this.showSettingsScreen();
            }
        });
        d.setNegativeButton("Quit", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
                AccuTracking.this.cleanQuit();
            }
        });
        d.show();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        saveSettings();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        boolean result = super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.mainmenu, menu);
        return result;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        boolean result = super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.about /*2131099672*/:
                showAboutcreen();
                break;
            case R.id.exit /*2131099673*/:
                cleanQuit();
                break;
        }
        return result;
    }

    private void broadcastGPSUpdateRequest() {
        sendBroadcast(new Intent(ATConstants.COM_ACCUTRACKING_ACTION_GPS_UPDATE_REQUEST));
    }

    /* access modifiers changed from: private */
    public void cleanQuit() {
        stopService(new Intent(this, ATService.class));
        finish();
    }

    public void loadSettings() {
        this.configData = ConfigData.getInstance(this.prefs);
        this.configData.load();
    }

    public void saveSettings() {
        this.configData.save();
    }

    public String calcHeading(int hd) {
        if (hd < 0) {
            return "-";
        }
        if (hd < 23) {
            return "N";
        }
        if (hd < 68) {
            return "NE";
        }
        if (hd < 113) {
            return "E";
        }
        if (hd < 158) {
            return "SE";
        }
        if (hd < 203) {
            return "S";
        }
        if (hd < 248) {
            return "SW";
        }
        if (hd < 293) {
            return "W";
        }
        if (hd < 338) {
            return "NW";
        }
        if (hd < 360) {
            return "N";
        }
        return "-";
    }

    /* access modifiers changed from: private */
    public void updateLocationScreen(String string) {
        if (this.textViewGPS != null) {
            this.textViewGPS.setText(string);
            Linkify.addLinks(this.textViewGPS, Pattern.compile("[-.0-9]+,[-.0-9]+"), "http://maps.google.com/maps?q=");
        }
    }

    /* access modifiers changed from: private */
    public void setStatus(String string) {
        if (this.textViewStatus != null) {
            this.textViewStatus.setText(string);
        }
    }

    /* access modifiers changed from: private */
    public void setCacheSize(int size) {
        if (this.textViewCache != null) {
            this.textViewCache.setText("Locations cached: " + Integer.toString(size));
        }
    }

    /* access modifiers changed from: private */
    public void setSigLev(int signalStrength) {
        if (this.textViewSigLev != null) {
            this.textViewSigLev.setText("Signal level: " + Integer.toString(signalStrength) + "%");
        }
    }

    private class IntentBroadcastReceiver extends BroadcastReceiver {
        private IntentBroadcastReceiver() {
        }

        /* synthetic */ IntentBroadcastReceiver(AccuTracking accuTracking, IntentBroadcastReceiver intentBroadcastReceiver) {
            this();
        }

        public void onReceive(Context context, Intent intent) {
            String intentAction = intent.getAction();
            if (intentAction.equals(ATConstants.COM_ACCUTRACKING_ACTION_GPS_UPDATE)) {
                Bundle extras = intent.getExtras();
                StringBuffer sb = new StringBuffer();
                sb.append("Lat/Lon: ");
                sb.append(Utils.round(extras.getDouble(ATConstants.GPS_UPDATE_LAT), 5));
                sb.append(",");
                sb.append(Utils.round(extras.getDouble(ATConstants.GPS_UPDATE_LON), 5));
                sb.append("\nAltitude: ");
                sb.append(Integer.toString((int) (extras.getDouble(ATConstants.GPS_UPDATE_ALT) * 3.28d)));
                sb.append(" feet\nHeading: ");
                sb.append(AccuTracking.this.calcHeading((int) extras.getFloat(ATConstants.GPS_UPDATE_HEADING)));
                sb.append("\nSpeed: ");
                sb.append(Integer.toString((int) (((double) extras.getFloat(ATConstants.GPS_UPDATE_SPEED)) * 2.2369363d)));
                sb.append(" mph\nLast fix time: ");
                sb.append(extras.getString(ATConstants.GPS_UPDATE_LAST_FIX_TIME));
                AccuTracking.this.updateLocationScreen(sb.toString());
                AccuTracking.this.setSigLev(extras.getInt(ATConstants.GPS_UPDATE_SIG_LEV));
            } else if (intentAction.equals(ATConstants.COM_ACCUTRACKING_ACTION_STATUS_UPDATE)) {
                Bundle extras2 = intent.getExtras();
                StringBuffer sb2 = new StringBuffer();
                sb2.append(extras2.getString("201"));
                sb2.append("\nLast attempt time: ");
                sb2.append(extras2.getString("202"));
                AccuTracking.this.setStatus(sb2.toString());
            } else if (intentAction.equals(ATConstants.COM_ACCUTRACKING_ACTION_CACHE_UPDATE)) {
                AccuTracking.this.setCacheSize(intent.getExtras().getInt(ATConstants.GPS_UPDATE_LOCATIONS_CACHED));
            }
        }
    }
}
