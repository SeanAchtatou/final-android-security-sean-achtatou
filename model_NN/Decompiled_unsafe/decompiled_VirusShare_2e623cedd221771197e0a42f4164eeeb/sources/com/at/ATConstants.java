package com.at;

public interface ATConstants {
    public static final String ATPREF = "atpref";
    public static final String COM_ACCUTRACKING_ACTION_ALARM = "com.accutracking.action.ALARM";
    public static final String COM_ACCUTRACKING_ACTION_CACHE_UPDATE = "com.accutracking.action.CACHE.UPDATE";
    public static final String COM_ACCUTRACKING_ACTION_GPS_UPDATE = "com.accutracking.action.GPS.UPDATE";
    public static final String COM_ACCUTRACKING_ACTION_GPS_UPDATE_REQUEST = "com.accutracking.action.GPS.UPDATE.REQUEST";
    public static final String COM_ACCUTRACKING_ACTION_MESSAGING = "com.accutracking.action.MESSAGING";
    public static final String COM_ACCUTRACKING_ACTION_SETTINGS_UPDATE = "com.accutracking.action.SETTING.UPDATE";
    public static final String COM_ACCUTRACKING_ACTION_STATUS_UPDATE = "com.accutracking.action.STATUS.UPDATE";
    public static final String GPS_UPDATE_ALT = "103";
    public static final String GPS_UPDATE_HEADING = "105";
    public static final String GPS_UPDATE_LAST_FIX_TIME = "107";
    public static final String GPS_UPDATE_LAT = "101";
    public static final String GPS_UPDATE_LOCATIONS_CACHED = "108";
    public static final String GPS_UPDATE_LON = "102";
    public static final String GPS_UPDATE_SIG_LEV = "106";
    public static final String GPS_UPDATE_SPEED = "104";
    public static final String SETTINGS_NEW_INTERVAL = "6";
    public static final String SETTINGS_NEW_TRACKER_ID = "7";
    public static final String SETTINGS_UPDATE_AUTO_INTERVAL = "2";
    public static final String SETTINGS_UPDATE_INTERVAL = "1";
    public static final String SETTINGS_UPDATE_NETWORK_LOC = "5";
    public static final String SETTINGS_UPDATE_SMART_SENDING = "4";
    public static final String SETTINGS_UPDATE_TRACKER_ID = "3";
    public static final String STATUS_LAST_ATTEMPT_TIME = "202";
    public static final String STATUS_UPDATE = "201";
    public static final String TTRACK_ACTION_CLOCK_IN = "201";
    public static final String TTRACK_ACTION_CLOCK_OUT = "202";
    public static final String TTRACK_ACTION_END_BREAK = "204";
    public static final String TTRACK_ACTION_START_BREAK = "203";
    public static final String TTRACK_MESSAGE_TEXT = "2";
    public static final String TTRACK_MESSAGE_TYPE = "1";
}
