package com.at;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class TimeTrackActivity extends ListActivity {
    private static final String TTRACK_CLOCK_IN = "Clock In";
    private static final String TTRACK_CLOCK_OUT = "Clock Out ";
    private static final String TTRACK_END_BREAK = "End Break";
    private static final String TTRACK_START_BREAK = "Start Break";
    /* access modifiers changed from: private */
    public ArrayAdapter<String> arrayAdapter;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        updateScreenTexts();
        this.arrayAdapter = new ArrayAdapter<>(this, 17367043, ATService.actions);
        setListAdapter(this.arrayAdapter);
    }

    /* access modifiers changed from: private */
    public void updateScreenTexts() {
        String str;
        String[] strArr = ATService.actions;
        if (ATService.isClockedIn) {
            str = TTRACK_CLOCK_OUT + ATService.clockInUser;
        } else {
            str = TTRACK_CLOCK_IN;
        }
        strArr[0] = str;
        ATService.actions[1] = ATService.isInBreak ? TTRACK_END_BREAK : TTRACK_START_BREAK;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView listView, View view, int position, long id) {
        boolean z = false;
        super.onListItemClick(listView, view, position, id);
        if (position == 0) {
            if (!ATService.isClockedIn) {
                getClockedInUser();
                return;
            }
            if (ATService.isInBreak) {
                ATService.isInBreak = false;
                sendMesg(ATConstants.TTRACK_ACTION_END_BREAK, ATService.clockInUser);
            }
            sendMesg("202", ATService.clockInUser);
            if (!ATService.isClockedIn) {
                z = true;
            }
            ATService.isClockedIn = z;
            updateScreenTexts();
            this.arrayAdapter.notifyDataSetChanged();
        } else if (position != 1) {
        } else {
            if (!ATService.isClockedIn) {
                Toast.makeText(getApplicationContext(), "Please clock in first", 0).show();
                return;
            }
            if (ATService.isInBreak) {
                sendMesg(ATConstants.TTRACK_ACTION_END_BREAK, ATService.clockInUser);
            } else {
                sendMesg(ATConstants.TTRACK_ACTION_START_BREAK, ATService.clockInUser);
            }
            if (!ATService.isInBreak) {
                z = true;
            }
            ATService.isInBreak = z;
            updateScreenTexts();
            this.arrayAdapter.notifyDataSetChanged();
        }
    }

    /* access modifiers changed from: private */
    public void sendMesg(String mesgType, String text) {
        Intent timeTrackIntent = new Intent(ATConstants.COM_ACCUTRACKING_ACTION_MESSAGING);
        timeTrackIntent.putExtra("1", mesgType);
        timeTrackIntent.putExtra("2", text);
        sendBroadcast(timeTrackIntent);
    }

    private void getClockedInUser() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(TTRACK_CLOCK_IN);
        alert.setMessage("Your Name/ID:");
        final EditText input = new EditText(this);
        input.setSingleLine(true);
        alert.setView(input);
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                ATService.clockInUser = input.getText().toString();
                ATService.isClockedIn = !ATService.isClockedIn;
                TimeTrackActivity.this.sendMesg("201", ATService.clockInUser);
                TimeTrackActivity.this.updateScreenTexts();
                TimeTrackActivity.this.arrayAdapter.notifyDataSetChanged();
            }
        });
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        alert.show();
    }
}
