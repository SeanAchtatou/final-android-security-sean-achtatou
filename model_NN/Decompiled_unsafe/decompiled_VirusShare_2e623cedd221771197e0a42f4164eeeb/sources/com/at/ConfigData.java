package com.at;

import android.content.SharedPreferences;

public class ConfigData {
    private static ConfigData INSTANCE;
    public int cacheSize;
    public int interval;
    public boolean isAutoInterval;
    public boolean isAutoStart;
    public boolean isHotGPS;
    public boolean isPwdProtected;
    public boolean isSmartSending;
    public boolean isUseNetworkLoc;
    private SharedPreferences prefs;
    public String strPwd = "";
    public String strTrackerID = "";

    private ConfigData(SharedPreferences prefs2) {
        if (prefs2 != null) {
            this.prefs = prefs2;
        }
    }

    public static ConfigData getInstance(SharedPreferences prefs2) {
        if (INSTANCE == null) {
            INSTANCE = new ConfigData(prefs2);
        }
        return INSTANCE;
    }

    public void load() {
        this.interval = this.prefs.getInt("interval", 1);
        this.strTrackerID = this.prefs.getString("strTrackerID", this.strTrackerID);
        this.isSmartSending = this.prefs.getBoolean("bSmartSending", false);
        this.isHotGPS = this.prefs.getBoolean("bHotGPS", false);
        this.isAutoInterval = this.prefs.getBoolean("bAutoInterval", false);
        this.isUseNetworkLoc = this.prefs.getBoolean("bNetworkLoc", true);
        this.isPwdProtected = this.prefs.getBoolean("bPwdProtected", false);
        this.strPwd = this.prefs.getString("strPwd", "");
        this.cacheSize = this.prefs.getInt("cacheSize", 1000);
        this.isAutoStart = this.prefs.getBoolean("isAutoStart", true);
    }

    public void save() {
        SharedPreferences.Editor editor = this.prefs.edit();
        editor.putInt("interval", this.interval);
        editor.putString("strTrackerID", this.strTrackerID);
        editor.putBoolean("bSmartSending", this.isSmartSending);
        editor.putBoolean("bHotGPS", this.isHotGPS);
        editor.putBoolean("bAutoInterval", this.isAutoInterval);
        editor.putBoolean("bNetworkLoc", this.isUseNetworkLoc);
        editor.putBoolean("bPwdProtected", this.isPwdProtected);
        editor.putString("strPwd", this.strPwd);
        editor.putInt("cacheSize", this.cacheSize);
        editor.putBoolean("isAutoStart", this.isAutoStart);
        editor.commit();
    }
}
