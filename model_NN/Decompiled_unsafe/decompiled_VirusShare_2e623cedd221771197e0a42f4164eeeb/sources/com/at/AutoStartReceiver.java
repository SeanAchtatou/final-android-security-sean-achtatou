package com.at;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class AutoStartReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            Utils.isServiceInvokedByAutoStartReceiver = true;
            context.startService(new Intent(context, ATService.class));
        }
    }
}
