package org.codehaus.jackson.map.ser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.map.AnnotationIntrospector;
import org.codehaus.jackson.map.BeanDescription;
import org.codehaus.jackson.map.BeanProperty;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.SerializerFactory;
import org.codehaus.jackson.map.Serializers;
import org.codehaus.jackson.map.TypeSerializer;
import org.codehaus.jackson.map.introspect.AnnotatedClass;
import org.codehaus.jackson.map.introspect.AnnotatedField;
import org.codehaus.jackson.map.introspect.AnnotatedMember;
import org.codehaus.jackson.map.introspect.AnnotatedMethod;
import org.codehaus.jackson.map.introspect.BasicBeanDescription;
import org.codehaus.jackson.map.introspect.VisibilityChecker;
import org.codehaus.jackson.map.jsontype.TypeResolverBuilder;
import org.codehaus.jackson.map.type.TypeBindings;
import org.codehaus.jackson.map.util.ArrayBuilders;
import org.codehaus.jackson.map.util.ClassUtil;
import org.codehaus.jackson.type.JavaType;

public class BeanSerializerFactory extends BasicSerializerFactory {
    public static final BeanSerializerFactory instance = new BeanSerializerFactory(null);
    protected final SerializerFactory.Config _factoryConfig;

    public static class ConfigImpl extends SerializerFactory.Config {
        protected static final BeanSerializerModifier[] NO_MODIFIERS = new BeanSerializerModifier[0];
        protected static final Serializers[] NO_SERIALIZERS = new Serializers[0];
        protected final Serializers[] _additionalSerializers;
        protected final BeanSerializerModifier[] _modifiers;

        public ConfigImpl() {
            this(null, null);
        }

        protected ConfigImpl(Serializers[] allAdditionalSerializers, BeanSerializerModifier[] modifiers) {
            this._additionalSerializers = allAdditionalSerializers == null ? NO_SERIALIZERS : allAdditionalSerializers;
            this._modifiers = modifiers == null ? NO_MODIFIERS : modifiers;
        }

        public SerializerFactory.Config withAdditionalSerializers(Serializers additional) {
            if (additional != null) {
                return new ConfigImpl((Serializers[]) ArrayBuilders.insertInList(this._additionalSerializers, additional), this._modifiers);
            }
            throw new IllegalArgumentException("Can not pass null Serializers");
        }

        public SerializerFactory.Config withSerializerModifier(BeanSerializerModifier modifier) {
            if (modifier == null) {
                throw new IllegalArgumentException("Can not pass null modifier");
            }
            return new ConfigImpl(this._additionalSerializers, (BeanSerializerModifier[]) ArrayBuilders.insertInList(this._modifiers, modifier));
        }

        public boolean hasSerializers() {
            return this._additionalSerializers.length > 0;
        }

        public boolean hasSerializerModifiers() {
            return this._modifiers.length > 0;
        }

        public Iterable<Serializers> serializers() {
            return ArrayBuilders.arrayAsIterable(this._additionalSerializers);
        }

        public Iterable<BeanSerializerModifier> serializerModifiers() {
            return ArrayBuilders.arrayAsIterable(this._modifiers);
        }
    }

    @Deprecated
    protected BeanSerializerFactory() {
        this(null);
    }

    protected BeanSerializerFactory(SerializerFactory.Config config) {
        this._factoryConfig = config == null ? new ConfigImpl() : config;
    }

    public SerializerFactory.Config getConfig() {
        return this._factoryConfig;
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public SerializerFactory withConfig(SerializerFactory.Config config) {
        if (this._factoryConfig == config) {
            return this;
        }
        if (getClass() == BeanSerializerFactory.class) {
            return new BeanSerializerFactory(config);
        }
        throw new IllegalStateException("Subtype of BeanSerializerFactory (" + getClass().getName() + ") has not properly overridden method 'withAdditionalSerializers': can not instantiate subtype with " + "additional serializer definitions");
    }

    public JsonSerializer<Object> createSerializer(SerializationConfig config, JavaType type, BeanProperty property) {
        BasicBeanDescription beanDesc = (BasicBeanDescription) config.introspect(type);
        JsonSerializer<?> ser = findSerializerFromAnnotation(config, beanDesc.getClassInfo(), property);
        if (ser != null) {
            return ser;
        }
        JsonSerializer<?> ser2 = _findFirstSerializer(this._factoryConfig.serializers(), config, type, beanDesc, property);
        if (ser2 != null) {
            return ser2;
        }
        JsonSerializer<?> ser3 = super.findSerializerByLookup(type, config, beanDesc, property);
        if (ser3 != null) {
            return ser3;
        }
        JsonSerializer<?> ser4 = super.findSerializerByPrimaryType(type, config, beanDesc, property);
        if (ser4 != null) {
            return ser4;
        }
        JsonSerializer<?> ser5 = findBeanSerializer(config, type, beanDesc, property);
        if (ser5 == null) {
            return super.findSerializerByAddonType(config, type, beanDesc, property);
        }
        return ser5;
    }

    private static JsonSerializer<?> _findFirstSerializer(Iterable<Serializers> sers, SerializationConfig config, JavaType type, BeanDescription beanDesc, BeanProperty property) {
        for (Serializers ser : sers) {
            JsonSerializer<?> js = ser.findSerializer(config, type, beanDesc, property);
            if (js != null) {
                return js;
            }
        }
        return null;
    }

    public JsonSerializer<Object> findBeanSerializer(SerializationConfig config, JavaType type, BasicBeanDescription beanDesc, BeanProperty property) {
        if (!isPotentialBeanType(type.getRawClass())) {
            return null;
        }
        JsonSerializer<Object> serializer = constructBeanSerializer(config, beanDesc, property);
        if (!this._factoryConfig.hasSerializerModifiers()) {
            return serializer;
        }
        for (BeanSerializerModifier mod : this._factoryConfig.serializerModifiers()) {
            serializer = mod.modifySerializer(config, beanDesc, serializer);
        }
        return serializer;
    }

    public TypeSerializer findPropertyTypeSerializer(JavaType baseType, SerializationConfig config, AnnotatedMember accessor, BeanProperty property) {
        AnnotationIntrospector ai = config.getAnnotationIntrospector();
        TypeResolverBuilder<?> b = ai.findPropertyTypeResolver(accessor, baseType);
        if (b == null) {
            return createTypeSerializer(config, baseType, property);
        }
        return b.buildTypeSerializer(baseType, config.getSubtypeResolver().collectAndResolveSubtypes(accessor, config, ai), property);
    }

    public TypeSerializer findPropertyContentTypeSerializer(JavaType containerType, SerializationConfig config, AnnotatedMember accessor, BeanProperty property) {
        JavaType contentType = containerType.getContentType();
        AnnotationIntrospector ai = config.getAnnotationIntrospector();
        TypeResolverBuilder<?> b = ai.findPropertyContentTypeResolver(accessor, containerType);
        if (b == null) {
            return createTypeSerializer(config, contentType, property);
        }
        return b.buildTypeSerializer(contentType, config.getSubtypeResolver().collectAndResolveSubtypes(accessor, config, ai), property);
    }

    /* access modifiers changed from: protected */
    public JsonSerializer<Object> constructBeanSerializer(SerializationConfig config, BasicBeanDescription beanDesc, BeanProperty property) {
        List<BeanPropertyWriter> props;
        if (beanDesc.getBeanClass() == Object.class) {
            throw new IllegalArgumentException("Can not create bean serializer for Object.class");
        }
        BeanSerializerBuilder builder = constructBeanSerializerBuilder(beanDesc);
        List<BeanPropertyWriter> props2 = findBeanProperties(config, beanDesc);
        AnnotatedMethod anyGetter = beanDesc.findAnyGetter();
        if (this._factoryConfig.hasSerializerModifiers()) {
            if (props2 == null) {
                props2 = new ArrayList<>();
            }
            for (BeanSerializerModifier mod : this._factoryConfig.serializerModifiers()) {
                props2 = mod.changeProperties(config, beanDesc, props2);
            }
        }
        if (props2 != null && props2.size() != 0) {
            props = sortBeanProperties(config, beanDesc, filterBeanProperties(config, beanDesc, props2));
        } else if (anyGetter != null) {
            props = Collections.emptyList();
        } else if (beanDesc.hasKnownClassAnnotations()) {
            return builder.createDummy();
        } else {
            return null;
        }
        if (this._factoryConfig.hasSerializerModifiers()) {
            for (BeanSerializerModifier mod2 : this._factoryConfig.serializerModifiers()) {
                props = mod2.orderProperties(config, beanDesc, props);
            }
        }
        builder.setProperties(props);
        builder.setFilterId(findFilterId(config, beanDesc));
        if (anyGetter != null) {
            JavaType type = anyGetter.getType(beanDesc.bindingsForBeanType());
            builder.setAnyGetter(new AnyGetterWriter(anyGetter, MapSerializer.construct(null, type, config.isEnabled(SerializationConfig.Feature.USE_STATIC_TYPING), createTypeSerializer(config, type.getContentType(), property), property)));
        }
        processViews(config, builder);
        if (this._factoryConfig.hasSerializerModifiers()) {
            for (BeanSerializerModifier mod3 : this._factoryConfig.serializerModifiers()) {
                builder = mod3.updateBuilder(config, beanDesc, builder);
            }
        }
        return builder.build();
    }

    /* access modifiers changed from: protected */
    public BeanPropertyWriter constructFilteredBeanWriter(BeanPropertyWriter writer, Class<?>[] inViews) {
        return FilteredBeanPropertyWriter.constructViewBased(writer, inViews);
    }

    /* access modifiers changed from: protected */
    public PropertyBuilder constructPropertyBuilder(SerializationConfig config, BasicBeanDescription beanDesc) {
        return new PropertyBuilder(config, beanDesc);
    }

    /* access modifiers changed from: protected */
    public BeanSerializerBuilder constructBeanSerializerBuilder(BasicBeanDescription beanDesc) {
        return new BeanSerializerBuilder(beanDesc);
    }

    /* access modifiers changed from: protected */
    public Object findFilterId(SerializationConfig config, BasicBeanDescription beanDesc) {
        return config.getAnnotationIntrospector().findFilterId(beanDesc.getClassInfo());
    }

    /* access modifiers changed from: protected */
    public boolean isPotentialBeanType(Class<?> type) {
        return ClassUtil.canBeABeanType(type) == null && !ClassUtil.isProxyType(type);
    }

    /* access modifiers changed from: protected */
    public List<BeanPropertyWriter> findBeanProperties(SerializationConfig config, BasicBeanDescription beanDesc) {
        VisibilityChecker defaultVisibilityChecker = config.getDefaultVisibilityChecker();
        if (!config.isEnabled(SerializationConfig.Feature.AUTO_DETECT_GETTERS)) {
            defaultVisibilityChecker = defaultVisibilityChecker.withGetterVisibility(JsonAutoDetect.Visibility.NONE);
        }
        if (!config.isEnabled(SerializationConfig.Feature.AUTO_DETECT_IS_GETTERS)) {
            defaultVisibilityChecker = defaultVisibilityChecker.withIsGetterVisibility(JsonAutoDetect.Visibility.NONE);
        }
        if (!config.isEnabled(SerializationConfig.Feature.AUTO_DETECT_FIELDS)) {
            defaultVisibilityChecker = defaultVisibilityChecker.withFieldVisibility(JsonAutoDetect.Visibility.NONE);
        }
        AnnotationIntrospector intr = config.getAnnotationIntrospector();
        VisibilityChecker<?> vchecker = intr.findAutoDetectVisibility(beanDesc.getClassInfo(), defaultVisibilityChecker);
        LinkedHashMap<String, AnnotatedMethod> methodsByProp = beanDesc.findGetters(vchecker, null);
        LinkedHashMap<String, AnnotatedField> fieldsByProp = beanDesc.findSerializableFields(vchecker, methodsByProp.keySet());
        removeIgnorableTypes(config, beanDesc, methodsByProp);
        removeIgnorableTypes(config, beanDesc, fieldsByProp);
        if (methodsByProp.isEmpty() && fieldsByProp.isEmpty()) {
            return null;
        }
        boolean staticTyping = usesStaticTyping(config, beanDesc, null);
        PropertyBuilder pb = constructPropertyBuilder(config, beanDesc);
        ArrayList arrayList = new ArrayList(methodsByProp.size());
        TypeBindings typeBind = beanDesc.bindingsForBeanType();
        for (Map.Entry<String, AnnotatedField> en : fieldsByProp.entrySet()) {
            AnnotationIntrospector.ReferenceProperty prop = intr.findReferenceType((AnnotatedMember) en.getValue());
            if (prop == null || !prop.isBackReference()) {
                ArrayList arrayList2 = arrayList;
                arrayList2.add(_constructWriter(config, typeBind, pb, staticTyping, (String) en.getKey(), (AnnotatedMember) en.getValue()));
            }
        }
        for (Map.Entry<String, AnnotatedMethod> en2 : methodsByProp.entrySet()) {
            AnnotationIntrospector.ReferenceProperty prop2 = intr.findReferenceType((AnnotatedMember) en2.getValue());
            if (prop2 == null || !prop2.isBackReference()) {
                ArrayList arrayList3 = arrayList;
                arrayList3.add(_constructWriter(config, typeBind, pb, staticTyping, (String) en2.getKey(), (AnnotatedMember) en2.getValue()));
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public List<BeanPropertyWriter> filterBeanProperties(SerializationConfig config, BasicBeanDescription beanDesc, List<BeanPropertyWriter> props) {
        String[] ignored = config.getAnnotationIntrospector().findPropertiesToIgnore(beanDesc.getClassInfo());
        if (ignored != null && ignored.length > 0) {
            HashSet<String> ignoredSet = ArrayBuilders.arrayToSet(ignored);
            Iterator<BeanPropertyWriter> it = props.iterator();
            while (it.hasNext()) {
                if (ignoredSet.contains(it.next().getName())) {
                    it.remove();
                }
            }
        }
        return props;
    }

    /* access modifiers changed from: protected */
    public List<BeanPropertyWriter> sortBeanProperties(SerializationConfig config, BasicBeanDescription beanDesc, List<BeanPropertyWriter> props) {
        List<String> creatorProps = beanDesc.findCreatorPropertyNames();
        AnnotationIntrospector intr = config.getAnnotationIntrospector();
        AnnotatedClass ac = beanDesc.getClassInfo();
        String[] propOrder = intr.findSerializationPropertyOrder(ac);
        Boolean alpha = intr.findSerializationSortAlphabetically(ac);
        boolean sort = alpha != null && alpha.booleanValue();
        if (sort || !creatorProps.isEmpty() || propOrder != null) {
            return _sortBeanProperties(props, creatorProps, propOrder, sort);
        }
        return props;
    }

    /* access modifiers changed from: protected */
    public void processViews(SerializationConfig config, BeanSerializerBuilder builder) {
        List<BeanPropertyWriter> props = builder.getProperties();
        if (config.isEnabled(SerializationConfig.Feature.DEFAULT_VIEW_INCLUSION)) {
            int propCount = props.size();
            BeanPropertyWriter[] filtered = null;
            for (int i = 0; i < propCount; i++) {
                BeanPropertyWriter bpw = props.get(i);
                Class<?>[] views = bpw.getViews();
                if (views != null) {
                    if (filtered == null) {
                        filtered = new BeanPropertyWriter[props.size()];
                    }
                    filtered[i] = constructFilteredBeanWriter(bpw, views);
                }
            }
            if (filtered != null) {
                for (int i2 = 0; i2 < propCount; i2++) {
                    if (filtered[i2] == null) {
                        filtered[i2] = props.get(i2);
                    }
                }
                builder.setFilteredProperties(filtered);
                return;
            }
            return;
        }
        ArrayList<BeanPropertyWriter> explicit = new ArrayList<>(props.size());
        for (BeanPropertyWriter bpw2 : props) {
            Class<?>[] views2 = bpw2.getViews();
            if (views2 != null) {
                explicit.add(constructFilteredBeanWriter(bpw2, views2));
            }
        }
        builder.setFilteredProperties((BeanPropertyWriter[]) explicit.toArray(new BeanPropertyWriter[explicit.size()]));
    }

    /* access modifiers changed from: protected */
    public <T extends AnnotatedMember> void removeIgnorableTypes(SerializationConfig config, BasicBeanDescription beanDesc, Map<String, T> props) {
        if (!props.isEmpty()) {
            AnnotationIntrospector intr = config.getAnnotationIntrospector();
            Iterator<Map.Entry<String, T>> it = props.entrySet().iterator();
            HashMap<Class<?>, Boolean> ignores = new HashMap<>();
            while (it.hasNext()) {
                Class<?> type = ((AnnotatedMember) it.next().getValue()).getRawType();
                Boolean result = (Boolean) ignores.get(type);
                if (result == null) {
                    result = intr.isIgnorableType(((BasicBeanDescription) config.introspectClassAnnotations(type)).getClassInfo());
                    if (result == null) {
                        result = Boolean.FALSE;
                    }
                    ignores.put(type, result);
                }
                if (result.booleanValue()) {
                    it.remove();
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public BeanPropertyWriter _constructWriter(SerializationConfig config, TypeBindings typeContext, PropertyBuilder pb, boolean staticTyping, String name, AnnotatedMember accessor) {
        if (config.isEnabled(SerializationConfig.Feature.CAN_OVERRIDE_ACCESS_MODIFIERS)) {
            accessor.fixAccess();
        }
        JavaType type = accessor.getType(typeContext);
        BeanProperty.Std property = new BeanProperty.Std(name, type, pb.getClassAnnotations(), accessor);
        JsonSerializer<Object> annotatedSerializer = findSerializerFromAnnotation(config, accessor, property);
        TypeSerializer contentTypeSer = null;
        if (ClassUtil.isCollectionMapOrArray(type.getRawClass())) {
            contentTypeSer = findPropertyContentTypeSerializer(type, config, accessor, property);
        }
        BeanPropertyWriter pbw = pb.buildWriter(name, type, annotatedSerializer, findPropertyTypeSerializer(type, config, accessor, property), contentTypeSer, accessor, staticTyping);
        pbw.setViews(config.getAnnotationIntrospector().findSerializationViews(accessor));
        return pbw;
    }

    /* JADX INFO: Multiple debug info for r2v1 java.util.Iterator<java.lang.String>: [D('i$' int), D('i$' java.util.Iterator)] */
    /* access modifiers changed from: protected */
    public List<BeanPropertyWriter> _sortBeanProperties(List<BeanPropertyWriter> props, List<String> creatorProps, String[] propertyOrder, boolean sort) {
        Map<String, BeanPropertyWriter> all;
        int size = props.size();
        if (sort) {
            all = new TreeMap<>();
        } else {
            all = new LinkedHashMap<>(size * 2);
        }
        for (BeanPropertyWriter w : props) {
            all.put(w.getName(), w);
        }
        Map<String, BeanPropertyWriter> ordered = new LinkedHashMap<>(size * 2);
        if (propertyOrder != null) {
            for (String name : propertyOrder) {
                BeanPropertyWriter w2 = (BeanPropertyWriter) all.get(name);
                if (w2 != null) {
                    ordered.put(name, w2);
                }
            }
        }
        for (String name2 : creatorProps) {
            BeanPropertyWriter w3 = (BeanPropertyWriter) all.get(name2);
            if (w3 != null) {
                ordered.put(name2, w3);
            }
        }
        ordered.putAll(all);
        return new ArrayList(ordered.values());
    }
}
