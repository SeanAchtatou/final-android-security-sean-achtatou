package org.codehaus.jackson.map.introspect;

import com.senddroid.AdLog;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.codehaus.jackson.map.AnnotationIntrospector;
import org.codehaus.jackson.map.ClassIntrospector;
import org.codehaus.jackson.map.util.Annotations;
import org.codehaus.jackson.map.util.ArrayBuilders;
import org.codehaus.jackson.map.util.ClassUtil;

public final class AnnotatedClass extends Annotated {
    protected final AnnotationIntrospector _annotationIntrospector;
    protected final Class<?> _class;
    protected AnnotationMap _classAnnotations;
    protected List<AnnotatedConstructor> _constructors;
    protected List<AnnotatedMethod> _creatorMethods;
    protected AnnotatedConstructor _defaultConstructor;
    protected List<AnnotatedField> _fields;
    protected List<AnnotatedField> _ignoredFields;
    protected List<AnnotatedMethod> _ignoredMethods;
    protected AnnotatedMethodMap _memberMethods;
    protected final ClassIntrospector.MixInResolver _mixInResolver;
    protected final Class<?> _primaryMixIn;
    protected final Collection<Class<?>> _superTypes;

    private AnnotatedClass(Class<?> cls, List<Class<?>> superTypes, AnnotationIntrospector aintr, ClassIntrospector.MixInResolver mir) {
        this._class = cls;
        this._superTypes = superTypes;
        this._annotationIntrospector = aintr;
        this._mixInResolver = mir;
        this._primaryMixIn = this._mixInResolver == null ? null : this._mixInResolver.findMixInClassFor(this._class);
    }

    public static AnnotatedClass construct(Class<?> cls, AnnotationIntrospector aintr, ClassIntrospector.MixInResolver mir) {
        AnnotatedClass ac = new AnnotatedClass(cls, ClassUtil.findSuperTypes(cls, null), aintr, mir);
        ac.resolveClassAnnotations();
        return ac;
    }

    public static AnnotatedClass constructWithoutSuperTypes(Class<?> cls, AnnotationIntrospector aintr, ClassIntrospector.MixInResolver mir) {
        AnnotatedClass ac = new AnnotatedClass(cls, Collections.emptyList(), aintr, mir);
        ac.resolveClassAnnotations();
        return ac;
    }

    public Class<?> getAnnotated() {
        return this._class;
    }

    public int getModifiers() {
        return this._class.getModifiers();
    }

    public String getName() {
        return this._class.getName();
    }

    public <A extends Annotation> A getAnnotation(Class<A> acls) {
        if (this._classAnnotations == null) {
            return null;
        }
        return this._classAnnotations.get(acls);
    }

    public Type getGenericType() {
        return this._class;
    }

    public Class<?> getRawType() {
        return this._class;
    }

    public Annotations getAnnotations() {
        return this._classAnnotations;
    }

    public boolean hasAnnotations() {
        return this._classAnnotations.size() > 0;
    }

    public AnnotatedConstructor getDefaultConstructor() {
        return this._defaultConstructor;
    }

    public List<AnnotatedConstructor> getConstructors() {
        if (this._constructors == null) {
            return Collections.emptyList();
        }
        return this._constructors;
    }

    public List<AnnotatedMethod> getStaticMethods() {
        if (this._creatorMethods == null) {
            return Collections.emptyList();
        }
        return this._creatorMethods;
    }

    public Iterable<AnnotatedMethod> memberMethods() {
        return this._memberMethods;
    }

    public Iterable<AnnotatedMethod> ignoredMemberMethods() {
        if (this._ignoredMethods == null) {
            return Collections.emptyList();
        }
        return this._ignoredMethods;
    }

    public int getMemberMethodCount() {
        return this._memberMethods.size();
    }

    public AnnotatedMethod findMethod(String name, Class<?>[] paramTypes) {
        return this._memberMethods.find(name, paramTypes);
    }

    public int getFieldCount() {
        if (this._fields == null) {
            return 0;
        }
        return this._fields.size();
    }

    public Iterable<AnnotatedField> fields() {
        if (this._fields == null) {
            return Collections.emptyList();
        }
        return this._fields;
    }

    public Iterable<AnnotatedField> ignoredFields() {
        if (this._ignoredFields == null) {
            return Collections.emptyList();
        }
        return this._ignoredFields;
    }

    /* access modifiers changed from: protected */
    public void resolveClassAnnotations() {
        this._classAnnotations = new AnnotationMap();
        if (this._primaryMixIn != null) {
            _addClassMixIns(this._classAnnotations, this._class, this._primaryMixIn);
        }
        for (Annotation a : this._class.getDeclaredAnnotations()) {
            if (this._annotationIntrospector.isHandled(a)) {
                this._classAnnotations.addIfNotPresent(a);
            }
        }
        for (Class<?> cls : this._superTypes) {
            _addClassMixIns(this._classAnnotations, cls);
            for (Annotation a2 : cls.getDeclaredAnnotations()) {
                if (this._annotationIntrospector.isHandled(a2)) {
                    this._classAnnotations.addIfNotPresent(a2);
                }
            }
        }
        _addClassMixIns(this._classAnnotations, Object.class);
    }

    /* access modifiers changed from: protected */
    public void _addClassMixIns(AnnotationMap annotations, Class<?> toMask) {
        if (this._mixInResolver != null) {
            _addClassMixIns(annotations, toMask, this._mixInResolver.findMixInClassFor(toMask));
        }
    }

    /* access modifiers changed from: protected */
    public void _addClassMixIns(AnnotationMap annotations, Class<?> toMask, Class<?> mixin) {
        if (mixin != null) {
            for (Annotation a : mixin.getDeclaredAnnotations()) {
                if (this._annotationIntrospector.isHandled(a)) {
                    annotations.addIfNotPresent(a);
                }
            }
            for (Class<?> parent : ClassUtil.findSuperTypes(mixin, toMask)) {
                for (Annotation a2 : parent.getDeclaredAnnotations()) {
                    if (this._annotationIntrospector.isHandled(a2)) {
                        annotations.addIfNotPresent(a2);
                    }
                }
            }
        }
    }

    /* JADX INFO: Multiple debug info for r1v1 java.lang.reflect.Method[]: [D('arr$' java.lang.reflect.Method[]), D('arr$' java.lang.reflect.Constructor[])] */
    public void resolveCreators(boolean includeAll) {
        this._constructors = null;
        for (Constructor<?> ctor : this._class.getDeclaredConstructors()) {
            switch (ctor.getParameterTypes().length) {
                case AdLog.LOG_LEVEL_NONE:
                    this._defaultConstructor = _constructConstructor(ctor, true);
                    break;
                default:
                    if (includeAll) {
                        if (this._constructors == null) {
                            this._constructors = new ArrayList();
                        }
                        this._constructors.add(_constructConstructor(ctor, false));
                        break;
                    } else {
                        break;
                    }
            }
        }
        if (!(this._primaryMixIn == null || (this._defaultConstructor == null && this._constructors == null))) {
            _addConstructorMixIns(this._primaryMixIn);
        }
        if (this._defaultConstructor != null && this._annotationIntrospector.isIgnorableConstructor(this._defaultConstructor)) {
            this._defaultConstructor = null;
        }
        if (this._constructors != null) {
            int i = this._constructors.size();
            while (true) {
                i--;
                if (i >= 0) {
                    if (this._annotationIntrospector.isIgnorableConstructor(this._constructors.get(i))) {
                        this._constructors.remove(i);
                    }
                }
            }
        }
        this._creatorMethods = null;
        if (includeAll) {
            for (Method m : this._class.getDeclaredMethods()) {
                if (Modifier.isStatic(m.getModifiers()) && m.getParameterTypes().length >= 1) {
                    if (this._creatorMethods == null) {
                        this._creatorMethods = new ArrayList();
                    }
                    this._creatorMethods.add(_constructCreatorMethod(m));
                }
            }
            if (!(this._primaryMixIn == null || this._creatorMethods == null)) {
                _addFactoryMixIns(this._primaryMixIn);
            }
            if (this._creatorMethods != null) {
                int i2 = this._creatorMethods.size();
                while (true) {
                    i2--;
                    if (i2 < 0) {
                        return;
                    }
                    if (this._annotationIntrospector.isIgnorableMethod(this._creatorMethods.get(i2))) {
                        this._creatorMethods.remove(i2);
                    }
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.codehaus.jackson.map.introspect.AnnotatedClass._addMixOvers(java.lang.reflect.Constructor<?>, org.codehaus.jackson.map.introspect.AnnotatedConstructor, boolean):void
     arg types: [java.lang.reflect.Constructor<?>, org.codehaus.jackson.map.introspect.AnnotatedConstructor, int]
     candidates:
      org.codehaus.jackson.map.introspect.AnnotatedClass._addMixOvers(java.lang.reflect.Method, org.codehaus.jackson.map.introspect.AnnotatedMethod, boolean):void
      org.codehaus.jackson.map.introspect.AnnotatedClass._addMixOvers(java.lang.reflect.Constructor<?>, org.codehaus.jackson.map.introspect.AnnotatedConstructor, boolean):void */
    /* access modifiers changed from: protected */
    public void _addConstructorMixIns(Class<?> mixin) {
        MemberKey[] ctorKeys = null;
        int ctorCount = this._constructors == null ? 0 : this._constructors.size();
        for (Constructor<?> ctor : mixin.getDeclaredConstructors()) {
            switch (ctor.getParameterTypes().length) {
                case AdLog.LOG_LEVEL_NONE:
                    if (this._defaultConstructor == null) {
                        break;
                    } else {
                        _addMixOvers(ctor, this._defaultConstructor, false);
                        break;
                    }
                default:
                    if (ctorKeys == null) {
                        ctorKeys = new MemberKey[ctorCount];
                        for (int i = 0; i < ctorCount; i++) {
                            ctorKeys[i] = new MemberKey(this._constructors.get(i).getAnnotated());
                        }
                    }
                    MemberKey key = new MemberKey(ctor);
                    int i2 = 0;
                    while (true) {
                        if (i2 < ctorCount) {
                            if (key.equals(ctorKeys[i2])) {
                                _addMixOvers(ctor, this._constructors.get(i2), true);
                                break;
                            } else {
                                i2++;
                            }
                        } else {
                            break;
                        }
                    }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.codehaus.jackson.map.introspect.AnnotatedClass._addMixOvers(java.lang.reflect.Method, org.codehaus.jackson.map.introspect.AnnotatedMethod, boolean):void
     arg types: [java.lang.reflect.Method, org.codehaus.jackson.map.introspect.AnnotatedMethod, int]
     candidates:
      org.codehaus.jackson.map.introspect.AnnotatedClass._addMixOvers(java.lang.reflect.Constructor<?>, org.codehaus.jackson.map.introspect.AnnotatedConstructor, boolean):void
      org.codehaus.jackson.map.introspect.AnnotatedClass._addMixOvers(java.lang.reflect.Method, org.codehaus.jackson.map.introspect.AnnotatedMethod, boolean):void */
    /* access modifiers changed from: protected */
    public void _addFactoryMixIns(Class<?> mixin) {
        MemberKey[] methodKeys = null;
        int methodCount = this._creatorMethods.size();
        for (Method m : mixin.getDeclaredMethods()) {
            if (Modifier.isStatic(m.getModifiers()) && m.getParameterTypes().length != 0) {
                if (methodKeys == null) {
                    methodKeys = new MemberKey[methodCount];
                    for (int i = 0; i < methodCount; i++) {
                        methodKeys[i] = new MemberKey(this._creatorMethods.get(i).getAnnotated());
                    }
                }
                MemberKey key = new MemberKey(m);
                int i2 = 0;
                while (true) {
                    if (i2 < methodCount) {
                        if (key.equals(methodKeys[i2])) {
                            _addMixOvers(m, this._creatorMethods.get(i2), true);
                            break;
                        }
                        i2++;
                    } else {
                        break;
                    }
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.codehaus.jackson.map.introspect.AnnotatedClass._addMixOvers(java.lang.reflect.Method, org.codehaus.jackson.map.introspect.AnnotatedMethod, boolean):void
     arg types: [java.lang.reflect.Method, org.codehaus.jackson.map.introspect.AnnotatedMethod, int]
     candidates:
      org.codehaus.jackson.map.introspect.AnnotatedClass._addMixOvers(java.lang.reflect.Constructor<?>, org.codehaus.jackson.map.introspect.AnnotatedConstructor, boolean):void
      org.codehaus.jackson.map.introspect.AnnotatedClass._addMixOvers(java.lang.reflect.Method, org.codehaus.jackson.map.introspect.AnnotatedMethod, boolean):void */
    public void resolveMemberMethods(MethodFilter methodFilter, boolean collectIgnored) {
        Class<?> mixin;
        this._memberMethods = new AnnotatedMethodMap();
        AnnotatedMethodMap mixins = new AnnotatedMethodMap();
        _addMemberMethods(this._class, methodFilter, this._memberMethods, this._primaryMixIn, mixins);
        for (Class<?> cls : this._superTypes) {
            _addMemberMethods(cls, methodFilter, this._memberMethods, this._mixInResolver == null ? null : this._mixInResolver.findMixInClassFor(cls), mixins);
        }
        if (!(this._mixInResolver == null || (mixin = this._mixInResolver.findMixInClassFor(Object.class)) == null)) {
            _addMethodMixIns(methodFilter, this._memberMethods, mixin, mixins);
        }
        if (!mixins.isEmpty()) {
            Iterator<AnnotatedMethod> it = mixins.iterator();
            while (it.hasNext()) {
                AnnotatedMethod mixIn = it.next();
                try {
                    Method m = Object.class.getDeclaredMethod(mixIn.getName(), mixIn.getParameterClasses());
                    if (m != null) {
                        AnnotatedMethod am = _constructMethod(m);
                        _addMixOvers(mixIn.getAnnotated(), am, false);
                        this._memberMethods.add(am);
                    }
                } catch (Exception e) {
                }
            }
        }
        Iterator<AnnotatedMethod> it2 = this._memberMethods.iterator();
        while (it2.hasNext()) {
            AnnotatedMethod am2 = it2.next();
            if (this._annotationIntrospector.isIgnorableMethod(am2)) {
                it2.remove();
                if (collectIgnored) {
                    this._ignoredMethods = ArrayBuilders.addToList(this._ignoredMethods, am2);
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.codehaus.jackson.map.introspect.AnnotatedClass._addMixOvers(java.lang.reflect.Method, org.codehaus.jackson.map.introspect.AnnotatedMethod, boolean):void
     arg types: [java.lang.reflect.Method, org.codehaus.jackson.map.introspect.AnnotatedMethod, int]
     candidates:
      org.codehaus.jackson.map.introspect.AnnotatedClass._addMixOvers(java.lang.reflect.Constructor<?>, org.codehaus.jackson.map.introspect.AnnotatedConstructor, boolean):void
      org.codehaus.jackson.map.introspect.AnnotatedClass._addMixOvers(java.lang.reflect.Method, org.codehaus.jackson.map.introspect.AnnotatedMethod, boolean):void */
    /* access modifiers changed from: protected */
    public void _addMemberMethods(Class<?> cls, MethodFilter methodFilter, AnnotatedMethodMap methods, Class<?> mixInCls, AnnotatedMethodMap mixIns) {
        if (mixInCls != null) {
            _addMethodMixIns(methodFilter, methods, mixInCls, mixIns);
        }
        if (cls != null) {
            for (Method m : cls.getDeclaredMethods()) {
                if (_isIncludableMethod(m, methodFilter)) {
                    AnnotatedMethod old = methods.find(m);
                    if (old == null) {
                        AnnotatedMethod newM = _constructMethod(m);
                        methods.add(newM);
                        AnnotatedMethod old2 = mixIns.remove(m);
                        if (old2 != null) {
                            _addMixOvers(old2.getAnnotated(), newM, false);
                        }
                    } else {
                        _addMixUnders(m, old);
                        if (old.getDeclaringClass().isInterface() && !m.getDeclaringClass().isInterface()) {
                            methods.add(old.withMethod(m));
                        }
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void _addMethodMixIns(MethodFilter methodFilter, AnnotatedMethodMap methods, Class<?> mixInCls, AnnotatedMethodMap mixIns) {
        for (Method m : mixInCls.getDeclaredMethods()) {
            if (_isIncludableMethod(m, methodFilter)) {
                AnnotatedMethod am = methods.find(m);
                if (am != null) {
                    _addMixUnders(m, am);
                } else {
                    mixIns.add(_constructMethod(m));
                }
            }
        }
    }

    public void resolveFields(boolean collectIgnored) {
        LinkedHashMap<String, AnnotatedField> foundFields = new LinkedHashMap<>();
        _addFields(foundFields, this._class);
        Iterator<Map.Entry<String, AnnotatedField>> it = foundFields.entrySet().iterator();
        while (it.hasNext()) {
            AnnotatedField f = (AnnotatedField) ((Map.Entry) it.next()).getValue();
            if (this._annotationIntrospector.isIgnorableField(f)) {
                it.remove();
                if (collectIgnored) {
                    this._ignoredFields = ArrayBuilders.addToList(this._ignoredFields, f);
                }
            }
        }
        if (foundFields.isEmpty()) {
            this._fields = Collections.emptyList();
            return;
        }
        this._fields = new ArrayList(foundFields.size());
        this._fields.addAll(foundFields.values());
    }

    /* access modifiers changed from: protected */
    public void _addFields(Map<String, AnnotatedField> fields, Class<?> c) {
        Class<?> mixin;
        Class<?> parent = c.getSuperclass();
        if (parent != null) {
            _addFields(fields, parent);
            for (Field f : c.getDeclaredFields()) {
                if (_isIncludableField(f)) {
                    fields.put(f.getName(), _constructField(f));
                }
            }
            if (this._mixInResolver != null && (mixin = this._mixInResolver.findMixInClassFor(c)) != null) {
                _addFieldMixIns(mixin, fields);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void _addFieldMixIns(Class<?> mixin, Map<String, AnnotatedField> fields) {
        AnnotatedField maskedField;
        for (Field mixinField : mixin.getDeclaredFields()) {
            if (_isIncludableField(mixinField) && (maskedField = fields.get(mixinField.getName())) != null) {
                for (Annotation a : mixinField.getDeclaredAnnotations()) {
                    if (this._annotationIntrospector.isHandled(a)) {
                        maskedField.addOrOverride(a);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public AnnotatedMethod _constructMethod(Method m) {
        return new AnnotatedMethod(m, _collectRelevantAnnotations(m.getDeclaredAnnotations()), null);
    }

    /* access modifiers changed from: protected */
    public AnnotatedConstructor _constructConstructor(Constructor<?> ctor, boolean defaultCtor) {
        return new AnnotatedConstructor(ctor, _collectRelevantAnnotations(ctor.getDeclaredAnnotations()), defaultCtor ? null : _collectRelevantAnnotations(ctor.getParameterAnnotations()));
    }

    /* access modifiers changed from: protected */
    public AnnotatedMethod _constructCreatorMethod(Method m) {
        return new AnnotatedMethod(m, _collectRelevantAnnotations(m.getDeclaredAnnotations()), _collectRelevantAnnotations(m.getParameterAnnotations()));
    }

    /* access modifiers changed from: protected */
    public AnnotatedField _constructField(Field f) {
        return new AnnotatedField(f, _collectRelevantAnnotations(f.getDeclaredAnnotations()));
    }

    /* access modifiers changed from: protected */
    public AnnotationMap[] _collectRelevantAnnotations(Annotation[][] anns) {
        int len = anns.length;
        AnnotationMap[] result = new AnnotationMap[len];
        for (int i = 0; i < len; i++) {
            result[i] = _collectRelevantAnnotations(anns[i]);
        }
        return result;
    }

    /* access modifiers changed from: protected */
    public AnnotationMap _collectRelevantAnnotations(Annotation[] anns) {
        AnnotationMap annMap = new AnnotationMap();
        if (anns != null) {
            for (Annotation a : anns) {
                if (this._annotationIntrospector.isHandled(a)) {
                    annMap.add(a);
                }
            }
        }
        return annMap;
    }

    /* access modifiers changed from: protected */
    public boolean _isIncludableMethod(Method m, MethodFilter filter) {
        if ((filter == null || filter.includeMethod(m)) && !m.isSynthetic() && !m.isBridge()) {
            return true;
        }
        return false;
    }

    private boolean _isIncludableField(Field f) {
        if (f.isSynthetic()) {
            return false;
        }
        int mods = f.getModifiers();
        if (Modifier.isStatic(mods) || Modifier.isTransient(mods)) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void _addMixOvers(Constructor<?> mixin, AnnotatedConstructor target, boolean addParamAnnotations) {
        for (Annotation a : mixin.getDeclaredAnnotations()) {
            if (this._annotationIntrospector.isHandled(a)) {
                target.addOrOverride(a);
            }
        }
        if (addParamAnnotations) {
            Annotation[][] pa = mixin.getParameterAnnotations();
            int len = pa.length;
            for (int i = 0; i < len; i++) {
                for (Annotation a2 : pa[i]) {
                    target.addOrOverrideParam(i, a2);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void _addMixOvers(Method mixin, AnnotatedMethod target, boolean addParamAnnotations) {
        for (Annotation a : mixin.getDeclaredAnnotations()) {
            if (this._annotationIntrospector.isHandled(a)) {
                target.addOrOverride(a);
            }
        }
        if (addParamAnnotations) {
            Annotation[][] pa = mixin.getParameterAnnotations();
            int len = pa.length;
            for (int i = 0; i < len; i++) {
                for (Annotation a2 : pa[i]) {
                    target.addOrOverrideParam(i, a2);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void _addMixUnders(Method src, AnnotatedMethod target) {
        for (Annotation a : src.getDeclaredAnnotations()) {
            if (this._annotationIntrospector.isHandled(a)) {
                target.addIfNotPresent(a);
            }
        }
    }

    public String toString() {
        return "[AnnotedClass " + this._class.getName() + "]";
    }
}
