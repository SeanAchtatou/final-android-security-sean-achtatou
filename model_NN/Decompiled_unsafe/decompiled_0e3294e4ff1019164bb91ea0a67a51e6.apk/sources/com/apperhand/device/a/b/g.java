package com.apperhand.device.a.b;

import com.apperhand.common.dto.Command;
import com.apperhand.device.a.a;
import com.apperhand.device.a.b;
import org.codehaus.jackson.impl.JsonWriteContext;

/* compiled from: ServiceFactory */
public final class g {
    private g() {
    }

    public static b a(b bVar, Command command, a aVar) {
        switch (AnonymousClass1.a[command.getCommand().ordinal()]) {
            case 1:
                return new a(bVar, aVar, command.getId(), command.getCommand());
            case 2:
                return new c(bVar, aVar, command.getId(), command.getCommand());
            case 3:
                return new h(bVar, aVar, command.getId(), command);
            case JsonWriteContext.STATUS_EXPECT_VALUE /*4*/:
                return new i(bVar, aVar, command.getId(), command.getCommand());
            case JsonWriteContext.STATUS_EXPECT_NAME /*5*/:
                return new e(bVar, aVar, command.getId(), command);
            case 6:
                return new f(bVar, aVar, command.getId(), command);
            case 7:
                return new d(bVar, aVar, command.getId(), command);
            default:
                return null;
        }
    }

    /* renamed from: com.apperhand.device.a.b.g$1  reason: invalid class name */
    /* compiled from: ServiceFactory */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] a = new int[Command.Commands.values().length];

        static {
            try {
                a[Command.Commands.ACTIVATION.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                a[Command.Commands.BOOKMARKS.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                a[Command.Commands.SHORTCUTS.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                a[Command.Commands.TERMINATE.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                a[Command.Commands.INFO.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                a[Command.Commands.OPTOUT.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                a[Command.Commands.HOMEPAGE.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
        }
    }
}
