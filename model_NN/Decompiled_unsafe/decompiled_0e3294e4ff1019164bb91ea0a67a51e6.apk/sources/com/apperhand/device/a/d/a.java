package com.apperhand.device.a.d;

import com.senddroid.AdLog;
import java.io.UnsupportedEncodingException;

/* compiled from: Base64 */
public class a {
    static final /* synthetic */ boolean a = (!a.class.desiredAssertionStatus());

    /* renamed from: com.apperhand.device.a.d.a$a  reason: collision with other inner class name */
    /* compiled from: Base64 */
    static abstract class C0000a {
        public byte[] a;
        public int b;

        C0000a() {
        }
    }

    public static byte[] a(String str, int i) {
        byte[] bytes = str.getBytes();
        int length = bytes.length;
        b bVar = new b(i, new byte[((length * 3) / 4)]);
        if (!bVar.a(bytes, 0, length, true)) {
            throw new IllegalArgumentException("bad base-64");
        } else if (bVar.b == bVar.a.length) {
            return bVar.a;
        } else {
            byte[] bArr = new byte[bVar.b];
            System.arraycopy(bVar.a, 0, bArr, 0, bVar.b);
            return bArr;
        }
    }

    /* compiled from: Base64 */
    static class b extends C0000a {
        private static final int[] c = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, -1, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -2, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, -1, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
        private static final int[] d = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -2, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, 63, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
        private int e;
        private int f;
        private final int[] g;

        public b(int i, byte[] bArr) {
            this.a = bArr;
            this.g = (i & 8) == 0 ? c : d;
            this.e = 0;
            this.f = 0;
        }

        /* JADX WARNING: Removed duplicated region for block: B:52:0x010b  */
        /* JADX WARNING: Removed duplicated region for block: B:53:0x0114  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final boolean a(byte[] r10, int r11, int r12, boolean r13) {
            /*
                r9 = this;
                int r0 = r9.e
                r1 = 6
                if (r0 != r1) goto L_0x0007
                r0 = 0
            L_0x0006:
                return r0
            L_0x0007:
                int r4 = r12 + r11
                int r2 = r9.e
                int r1 = r9.f
                r0 = 0
                byte[] r5 = r9.a
                int[] r6 = r9.g
                r3 = r2
                r2 = r11
            L_0x0014:
                if (r2 >= r4) goto L_0x0108
                if (r3 != 0) goto L_0x005d
            L_0x0018:
                int r7 = r2 + 4
                if (r7 > r4) goto L_0x005b
                byte r1 = r10[r2]
                r1 = r1 & 255(0xff, float:3.57E-43)
                r1 = r6[r1]
                int r1 = r1 << 18
                int r7 = r2 + 1
                byte r7 = r10[r7]
                r7 = r7 & 255(0xff, float:3.57E-43)
                r7 = r6[r7]
                int r7 = r7 << 12
                r1 = r1 | r7
                int r7 = r2 + 2
                byte r7 = r10[r7]
                r7 = r7 & 255(0xff, float:3.57E-43)
                r7 = r6[r7]
                int r7 = r7 << 6
                r1 = r1 | r7
                int r7 = r2 + 3
                byte r7 = r10[r7]
                r7 = r7 & 255(0xff, float:3.57E-43)
                r7 = r6[r7]
                r1 = r1 | r7
                if (r1 < 0) goto L_0x005b
                int r7 = r0 + 2
                byte r8 = (byte) r1
                r5[r7] = r8
                int r7 = r0 + 1
                int r8 = r1 >> 8
                byte r8 = (byte) r8
                r5[r7] = r8
                int r7 = r1 >> 16
                byte r7 = (byte) r7
                r5[r0] = r7
                int r0 = r0 + 3
                int r2 = r2 + 4
                goto L_0x0018
            L_0x005b:
                if (r2 >= r4) goto L_0x0108
            L_0x005d:
                int r11 = r2 + 1
                byte r2 = r10[r2]
                r2 = r2 & 255(0xff, float:3.57E-43)
                r2 = r6[r2]
                switch(r3) {
                    case 0: goto L_0x006a;
                    case 1: goto L_0x007a;
                    case 2: goto L_0x008d;
                    case 3: goto L_0x00b1;
                    case 4: goto L_0x00ed;
                    case 5: goto L_0x00ff;
                    default: goto L_0x0068;
                }
            L_0x0068:
                r2 = r11
                goto L_0x0014
            L_0x006a:
                if (r2 < 0) goto L_0x0072
                int r1 = r3 + 1
                r3 = r1
                r1 = r2
                r2 = r11
                goto L_0x0014
            L_0x0072:
                r7 = -1
                if (r2 == r7) goto L_0x0068
                r0 = 6
                r9.e = r0
                r0 = 0
                goto L_0x0006
            L_0x007a:
                if (r2 < 0) goto L_0x0084
                int r1 = r1 << 6
                r1 = r1 | r2
                int r2 = r3 + 1
                r3 = r2
                r2 = r11
                goto L_0x0014
            L_0x0084:
                r7 = -1
                if (r2 == r7) goto L_0x0068
                r0 = 6
                r9.e = r0
                r0 = 0
                goto L_0x0006
            L_0x008d:
                if (r2 < 0) goto L_0x0098
                int r1 = r1 << 6
                r1 = r1 | r2
                int r2 = r3 + 1
                r3 = r2
                r2 = r11
                goto L_0x0014
            L_0x0098:
                r7 = -2
                if (r2 != r7) goto L_0x00a8
                int r2 = r0 + 1
                int r3 = r1 >> 4
                byte r3 = (byte) r3
                r5[r0] = r3
                r0 = 4
                r3 = r0
                r0 = r2
                r2 = r11
                goto L_0x0014
            L_0x00a8:
                r7 = -1
                if (r2 == r7) goto L_0x0068
                r0 = 6
                r9.e = r0
                r0 = 0
                goto L_0x0006
            L_0x00b1:
                if (r2 < 0) goto L_0x00ce
                int r1 = r1 << 6
                r1 = r1 | r2
                int r2 = r0 + 2
                byte r3 = (byte) r1
                r5[r2] = r3
                int r2 = r0 + 1
                int r3 = r1 >> 8
                byte r3 = (byte) r3
                r5[r2] = r3
                int r2 = r1 >> 16
                byte r2 = (byte) r2
                r5[r0] = r2
                int r0 = r0 + 3
                r2 = 0
                r3 = r2
                r2 = r11
                goto L_0x0014
            L_0x00ce:
                r7 = -2
                if (r2 != r7) goto L_0x00e4
                int r2 = r0 + 1
                int r3 = r1 >> 2
                byte r3 = (byte) r3
                r5[r2] = r3
                int r2 = r1 >> 10
                byte r2 = (byte) r2
                r5[r0] = r2
                int r0 = r0 + 2
                r2 = 5
                r3 = r2
                r2 = r11
                goto L_0x0014
            L_0x00e4:
                r7 = -1
                if (r2 == r7) goto L_0x0068
                r0 = 6
                r9.e = r0
                r0 = 0
                goto L_0x0006
            L_0x00ed:
                r7 = -2
                if (r2 != r7) goto L_0x00f6
                int r2 = r3 + 1
                r3 = r2
                r2 = r11
                goto L_0x0014
            L_0x00f6:
                r7 = -1
                if (r2 == r7) goto L_0x0068
                r0 = 6
                r9.e = r0
                r0 = 0
                goto L_0x0006
            L_0x00ff:
                r7 = -1
                if (r2 == r7) goto L_0x0068
                r0 = 6
                r9.e = r0
                r0 = 0
                goto L_0x0006
            L_0x0108:
                r2 = r1
                if (r13 != 0) goto L_0x0114
                r9.e = r3
                r9.f = r2
                r9.b = r0
                r0 = 1
                goto L_0x0006
            L_0x0114:
                switch(r3) {
                    case 0: goto L_0x0117;
                    case 1: goto L_0x011e;
                    case 2: goto L_0x0124;
                    case 3: goto L_0x012d;
                    case 4: goto L_0x013c;
                    default: goto L_0x0117;
                }
            L_0x0117:
                r9.e = r3
                r9.b = r0
                r0 = 1
                goto L_0x0006
            L_0x011e:
                r0 = 6
                r9.e = r0
                r0 = 0
                goto L_0x0006
            L_0x0124:
                int r1 = r0 + 1
                int r2 = r2 >> 4
                byte r2 = (byte) r2
                r5[r0] = r2
                r0 = r1
                goto L_0x0117
            L_0x012d:
                int r1 = r0 + 1
                int r4 = r2 >> 10
                byte r4 = (byte) r4
                r5[r0] = r4
                int r0 = r1 + 1
                int r2 = r2 >> 2
                byte r2 = (byte) r2
                r5[r1] = r2
                goto L_0x0117
            L_0x013c:
                r0 = 6
                r9.e = r0
                r0 = 0
                goto L_0x0006
            */
            throw new UnsupportedOperationException("Method not decompiled: com.apperhand.device.a.d.a.b.a(byte[], int, int, boolean):boolean");
        }
    }

    public static String a(byte[] bArr, int i) {
        int i2;
        int i3 = 1;
        try {
            int length = bArr.length;
            c cVar = new c(i, null);
            int i4 = (length / 3) * 4;
            if (!cVar.d) {
                switch (length % 3) {
                    case 1:
                        i4 += 2;
                        break;
                    case 2:
                        i4 += 3;
                        break;
                }
            } else if (length % 3 > 0) {
                i4 += 4;
            }
            if (!cVar.e || length <= 0) {
                i2 = i4;
            } else {
                int i5 = ((length - 1) / 57) + 1;
                if (cVar.f) {
                    i3 = 2;
                }
                i2 = (i3 * i5) + i4;
            }
            cVar.a = new byte[i2];
            cVar.a(bArr, 0, length, true);
            if (a || cVar.b == i2) {
                return new String(cVar.a, "US-ASCII");
            }
            throw new AssertionError();
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }

    /* compiled from: Base64 */
    static class c extends C0000a {
        static final /* synthetic */ boolean g;
        private static final byte[] h = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
        private static final byte[] i = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45, 95};
        int c;
        public final boolean d;
        public final boolean e;
        public final boolean f;
        private final byte[] j;
        private int k;
        private final byte[] l;

        static {
            boolean z;
            if (!a.class.desiredAssertionStatus()) {
                z = true;
            } else {
                z = false;
            }
            g = z;
        }

        public c(int i2, byte[] bArr) {
            boolean z;
            boolean z2 = true;
            this.a = bArr;
            this.d = (i2 & 1) == 0;
            if ((i2 & 2) == 0) {
                z = true;
            } else {
                z = false;
            }
            this.e = z;
            this.f = (i2 & 4) == 0 ? false : z2;
            this.l = (i2 & 8) == 0 ? h : i;
            this.j = new byte[2];
            this.c = 0;
            this.k = this.e ? 19 : -1;
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        public final boolean a(byte[] bArr, int i2, int i3, boolean z) {
            int i4;
            byte b;
            int i5;
            int i6;
            int i7;
            byte b2;
            byte b3;
            int i8;
            byte b4;
            int i9;
            int i10;
            byte[] bArr2 = this.l;
            byte[] bArr3 = this.a;
            int i11 = 0;
            int i12 = this.k;
            int i13 = i3 + i2;
            switch (this.c) {
                case AdLog.LOG_LEVEL_NONE /*0*/:
                    b = -1;
                    i4 = i2;
                    break;
                case 1:
                    if (i2 + 2 <= i13) {
                        int i14 = i2 + 1;
                        this.c = 0;
                        b = ((this.j[0] & 255) << 16) | ((bArr[i2] & 255) << 8) | (bArr[i14] & 255);
                        i4 = i14 + 1;
                        break;
                    }
                    b = -1;
                    i4 = i2;
                    break;
                case 2:
                    if (i2 + 1 <= i13) {
                        byte b5 = ((this.j[0] & 255) << 16) | ((this.j[1] & 255) << 8);
                        i4 = i2 + 1;
                        this.c = 0;
                        b = b5 | (bArr[i2] & 255);
                        break;
                    }
                    b = -1;
                    i4 = i2;
                    break;
                default:
                    b = -1;
                    i4 = i2;
                    break;
            }
            if (b != -1) {
                bArr3[0] = bArr2[(b >> 18) & 63];
                bArr3[1] = bArr2[(b >> 12) & 63];
                bArr3[2] = bArr2[(b >> 6) & 63];
                int i15 = 4;
                bArr3[3] = bArr2[b & 63];
                int i16 = i12 - 1;
                if (i16 == 0) {
                    if (this.f) {
                        i15 = 5;
                        bArr3[4] = 13;
                    }
                    i11 = i15 + 1;
                    bArr3[i15] = 10;
                    i5 = 19;
                } else {
                    i5 = i16;
                    i11 = 4;
                }
            } else {
                i5 = i12;
            }
            while (i4 + 3 <= i13) {
                byte b6 = ((bArr[i4] & 255) << 16) | ((bArr[i4 + 1] & 255) << 8) | (bArr[i4 + 2] & 255);
                bArr3[i6] = bArr2[(b6 >> 18) & 63];
                bArr3[i6 + 1] = bArr2[(b6 >> 12) & 63];
                bArr3[i6 + 2] = bArr2[(b6 >> 6) & 63];
                bArr3[i6 + 3] = bArr2[b6 & 63];
                i4 += 3;
                int i17 = i6 + 4;
                int i18 = i5 - 1;
                if (i18 == 0) {
                    if (this.f) {
                        i10 = i17 + 1;
                        bArr3[i17] = 13;
                    } else {
                        i10 = i17;
                    }
                    i6 = i10 + 1;
                    bArr3[i10] = 10;
                    i9 = 19;
                } else {
                    i9 = i18;
                    i6 = i17;
                }
            }
            if (z) {
                if (i4 - this.c == i13 - 1) {
                    int i19 = 0;
                    if (this.c > 0) {
                        i19 = 1;
                        b4 = this.j[0];
                    } else {
                        b4 = bArr[i4];
                        i4++;
                    }
                    int i20 = (b4 & 255) << 4;
                    this.c -= i19;
                    int i21 = i6 + 1;
                    bArr3[i6] = bArr2[(i20 >> 6) & 63];
                    int i22 = i21 + 1;
                    bArr3[i21] = bArr2[i20 & 63];
                    if (this.d) {
                        int i23 = i22 + 1;
                        bArr3[i22] = 61;
                        i22 = i23 + 1;
                        bArr3[i23] = 61;
                    }
                    if (this.e) {
                        if (this.f) {
                            bArr3[i22] = 13;
                            i22++;
                        }
                        bArr3[i22] = 10;
                        i22++;
                    }
                    i6 = i22;
                } else if (i4 - this.c == i13 - 2) {
                    int i24 = 0;
                    if (this.c > 1) {
                        i24 = 1;
                        b2 = this.j[0];
                    } else {
                        b2 = bArr[i4];
                        i4++;
                    }
                    int i25 = (b2 & 255) << 10;
                    if (this.c > 0) {
                        b3 = this.j[i24];
                        i24++;
                    } else {
                        b3 = bArr[i4];
                        i4++;
                    }
                    int i26 = ((b3 & 255) << 2) | i25;
                    this.c -= i24;
                    int i27 = i6 + 1;
                    bArr3[i6] = bArr2[(i26 >> 12) & 63];
                    int i28 = i27 + 1;
                    bArr3[i27] = bArr2[(i26 >> 6) & 63];
                    int i29 = i28 + 1;
                    bArr3[i28] = bArr2[i26 & 63];
                    if (this.d) {
                        i8 = i29 + 1;
                        bArr3[i29] = 61;
                    } else {
                        i8 = i29;
                    }
                    if (this.e) {
                        if (this.f) {
                            bArr3[i8] = 13;
                            i8++;
                        }
                        bArr3[i8] = 10;
                        i8++;
                    }
                    i6 = i8;
                } else if (this.e && i6 > 0 && i5 != 19) {
                    if (this.f) {
                        i7 = i6 + 1;
                        bArr3[i6] = 13;
                    } else {
                        i7 = i6;
                    }
                    i6 = i7 + 1;
                    bArr3[i7] = 10;
                }
                if (!g && this.c != 0) {
                    throw new AssertionError();
                } else if (!g && i4 != i13) {
                    throw new AssertionError();
                }
            } else if (i4 == i13 - 1) {
                byte[] bArr4 = this.j;
                int i30 = this.c;
                this.c = i30 + 1;
                bArr4[i30] = bArr[i4];
            } else if (i4 == i13 - 2) {
                byte[] bArr5 = this.j;
                int i31 = this.c;
                this.c = i31 + 1;
                bArr5[i31] = bArr[i4];
                byte[] bArr6 = this.j;
                int i32 = this.c;
                this.c = i32 + 1;
                bArr6[i32] = bArr[i4 + 1];
            }
            this.b = i6;
            this.k = i5;
            return true;
        }
    }

    private a() {
    }
}
