package com.apperhand.device.a.b;

import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.CommandStatus;
import com.apperhand.common.dto.protocol.BaseResponse;
import com.apperhand.common.dto.protocol.CommandStatusRequest;
import com.apperhand.device.a.a;
import com.apperhand.device.a.b;
import com.apperhand.device.a.d.f;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;

/* compiled from: UnexpectedExceptionService */
public final class j extends b {
    private Throwable g;

    public j(b bVar, a aVar, String str, Command.Commands commands, Throwable th) {
        super(bVar, aVar, str, commands);
        this.g = th;
    }

    /* access modifiers changed from: protected */
    public final Map<String, Object> a(BaseResponse baseResponse) throws f {
        return null;
    }

    /* access modifiers changed from: protected */
    public final BaseResponse a() throws f {
        return null;
    }

    public final void a(Map<String, Object> map) throws f {
        a(b());
    }

    /* access modifiers changed from: protected */
    public final CommandStatusRequest b() throws f {
        CommandStatusRequest b = super.b();
        StringWriter stringWriter = new StringWriter();
        this.g.printStackTrace(new PrintWriter(stringWriter));
        b.setStatuses(a(Command.Commands.UNEXPECTED_EXCEPTION, CommandStatus.Status.EXCEPTION, stringWriter.toString(), null));
        return b;
    }
}
