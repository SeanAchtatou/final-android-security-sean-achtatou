package com.apperhand.device.a.a;

import com.apperhand.common.dto.Bookmark;
import com.apperhand.common.dto.CommandInformation;
import com.apperhand.device.a.d.f;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* compiled from: BookmarksDMA */
public interface b {
    long a(Bookmark bookmark) throws f;

    CommandInformation a(List<String> list);

    Map<String, List<Bookmark>> a(Set<String> set) throws f;

    void a() throws f;

    void b(Bookmark bookmark) throws f;
}
