package com.apperhand.common.dto;

public class Bookmark extends BaseBrowserItem {
    private static final long serialVersionUID = -5295312317729930242L;

    public Bookmark() {
    }

    public Bookmark(long id, String title, String url, byte[] favicon, Status status) {
        super(id, title, url, favicon, status);
    }

    public Bookmark clone() {
        BaseBrowserItem baseClone = super.clone();
        return new Bookmark(baseClone.getId(), baseClone.getTitle(), baseClone.getUrl(), baseClone.getFavicon(), baseClone.getStatus());
    }

    public String toString() {
        return super.toString();
    }
}
