package com.apperhand.common.dto;

public class Shortcut extends BaseDTO {
    private static final long serialVersionUID = -7779209250474040249L;
    private byte[] icon;
    private long id;
    private String link;
    private String name;
    private int screen;
    private Status status;

    public Shortcut() {
        this(-1, null, null, null, null, -1);
    }

    public Shortcut(long id2, String name2, String link2, byte[] icon2, Status status2, int screen2) {
        this.id = id2;
        this.name = name2;
        this.link = link2;
        this.icon = icon2;
        this.status = status2;
        this.screen = screen2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getLink() {
        return this.link;
    }

    public void setLink(String link2) {
        this.link = link2;
    }

    public byte[] getIcon() {
        return this.icon;
    }

    public void setIcon(byte[] icon2) {
        this.icon = icon2;
    }

    public Status getStatus() {
        return this.status;
    }

    public void setStatus(Status status2) {
        this.status = status2;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id2) {
        this.id = id2;
    }

    public int getScreen() {
        return this.screen;
    }

    public void setScreen(int screen2) {
        this.screen = screen2;
    }

    public String toString() {
        return "Shortcut [id=" + this.id + ", name=" + this.name + ", link=" + this.link + ", status=" + this.status + ", screen=" + this.screen + "]";
    }

    public Shortcut clone() {
        Shortcut clone = new Shortcut();
        clone.setId(getId());
        clone.setName(getName());
        clone.setLink(getLink());
        clone.setStatus(getStatus());
        clone.setScreen(getScreen());
        System.arraycopy(getIcon(), 0, clone.getIcon(), 0, getIcon().length);
        return clone;
    }
}
