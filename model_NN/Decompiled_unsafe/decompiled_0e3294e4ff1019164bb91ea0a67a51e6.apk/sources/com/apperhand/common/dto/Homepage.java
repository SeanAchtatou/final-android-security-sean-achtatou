package com.apperhand.common.dto;

public class Homepage extends BaseDTO {
    private static final long serialVersionUID = 7900798598608490759L;
    private String pageURL;

    public Homepage() {
    }

    public Homepage(String pageURL2) {
        this.pageURL = pageURL2;
    }

    public String getPageURL() {
        return this.pageURL;
    }

    public void setPageURL(String pageURL2) {
        this.pageURL = pageURL2;
    }

    public String toString() {
        return "Homepage [pageURL=" + this.pageURL + "]";
    }
}
