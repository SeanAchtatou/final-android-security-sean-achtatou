package com.apperhand.common.dto;

import java.util.Map;

public class Activation extends BaseDTO {
    private static final long serialVersionUID = 3085832803649412152L;
    private String eula;
    private Map<String, String> parameters;

    public Activation() {
        this(null, null);
    }

    public Activation(String eula2, Map<String, String> parameters2) {
        this.eula = eula2;
        this.parameters = parameters2;
    }

    public String getEula() {
        return this.eula;
    }

    public void setEula(String eulaLink) {
        this.eula = eulaLink;
    }

    public Map<String, String> getParameters() {
        return this.parameters;
    }

    public void setParameters(Map<String, String> parameters2) {
        this.parameters = parameters2;
    }

    public String toString() {
        return "Activation [eula=" + this.eula + ", parameters=" + this.parameters + "]";
    }
}
