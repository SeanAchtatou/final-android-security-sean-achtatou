package com.Leadbolt;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.List;
import java.util.TimerTask;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

public class AdController {
    public static final String LB_LOG = "LBAdController";
    private static final String SDK_LEVEL = "00";
    private static final String SDK_VERSION = "3";
    private final int LB_MAX_POLL;
    private final int LB_SET_MANUAL_AFTER;
    private final int MAX_APP_ICONS;
    /* access modifiers changed from: private */
    public Activity activity;
    /* access modifiers changed from: private */
    public boolean adDestroyed;
    private String adDisplayInterval;
    /* access modifiers changed from: private */
    public boolean adLoaded;
    /* access modifiers changed from: private */
    public OfferPolling adPolling;
    private int additionalDockingMargin;
    private AlarmManager am;
    private String appId;
    private boolean asynchTask;
    private Button backBtn;
    private Button closeBtn;
    /* access modifiers changed from: private */
    public boolean completed;
    private Context context;
    private boolean dataretrieve;
    private String domain;
    private View footer;
    private TextView footerTitle;
    private Button fwdBtn;
    private Button homeBtn;
    /* access modifiers changed from: private */
    public boolean homeLoaded;
    private boolean initialized;
    private RelativeLayout layout;
    private boolean linkClicked;
    /* access modifiers changed from: private */
    public AdListener listener;
    /* access modifiers changed from: private */
    public boolean loadAd;
    /* access modifiers changed from: private */
    public boolean loadIcon;
    /* access modifiers changed from: private */
    public Runnable loadProgress;
    /* access modifiers changed from: private */
    public String loadUrl;
    /* access modifiers changed from: private */
    public boolean loading;
    /* access modifiers changed from: private */
    public ProgressDialog loadingDialog;
    private RelativeLayout.LayoutParams lpC;
    private WebView mainView;
    private ViewGroup mainViewParent;
    private View mask;
    private ViewGroup.MarginLayoutParams mlpC;
    /* access modifiers changed from: private */
    public boolean nativeOpen;
    private String notificationLaunchType;
    private boolean onRequest;
    private boolean onTimer;
    private PendingIntent pendingAlarmIntent;
    private Button pollBtn;
    /* access modifiers changed from: private */
    public int pollCount;
    /* access modifiers changed from: private */
    public int pollManual;
    /* access modifiers changed from: private */
    public int pollMax;
    /* access modifiers changed from: private */
    public Handler pollingHandler;
    private boolean pollingInitialized;
    /* access modifiers changed from: private */
    public Handler progressHandler;
    /* access modifiers changed from: private */
    public int progressInterval;
    private Button refreshBtn;
    private LBRequest req;
    private boolean requestInProgress;
    /* access modifiers changed from: private */
    public JSONObject results;
    private int sHeight;
    private int sWidth;
    /* access modifiers changed from: private */
    public String sectionid;
    private String subid;
    private TextView title;
    /* access modifiers changed from: private */
    public TelephonyManager tm;
    private List<NameValuePair> tokens;
    private View toolbar;
    private boolean useLocation;
    /* access modifiers changed from: private */
    public boolean useNotification;
    private boolean useSecure;
    /* access modifiers changed from: private */
    public WebView webview;

    public AdController(Activity act, String sid) {
        this(act, sid, new RelativeLayout(act));
    }

    public AdController(Activity act, String sid, WebView w) {
        this.LB_MAX_POLL = 500;
        this.LB_SET_MANUAL_AFTER = 10;
        this.MAX_APP_ICONS = 5;
        this.requestInProgress = false;
        this.completed = false;
        this.homeLoaded = false;
        this.linkClicked = false;
        this.pollingInitialized = false;
        this.useLocation = false;
        this.dataretrieve = true;
        this.nativeOpen = false;
        this.initialized = false;
        this.useNotification = false;
        this.additionalDockingMargin = 0;
        this.asynchTask = false;
        this.loadAd = true;
        this.onRequest = false;
        this.onTimer = false;
        this.listener = null;
        this.progressInterval = 0;
        this.adLoaded = false;
        this.adDestroyed = false;
        this.pollCount = 0;
        this.pollMax = 0;
        this.pollManual = 0;
        this.loadIcon = false;
        this.useSecure = false;
        this.activity = act;
        this.sectionid = sid;
        this.mainView = w;
        this.layout = new RelativeLayout(this.activity);
    }

    public AdController(Context ctx, String sid) {
        this.LB_MAX_POLL = 500;
        this.LB_SET_MANUAL_AFTER = 10;
        this.MAX_APP_ICONS = 5;
        this.requestInProgress = false;
        this.completed = false;
        this.homeLoaded = false;
        this.linkClicked = false;
        this.pollingInitialized = false;
        this.useLocation = false;
        this.dataretrieve = true;
        this.nativeOpen = false;
        this.initialized = false;
        this.useNotification = false;
        this.additionalDockingMargin = 0;
        this.asynchTask = false;
        this.loadAd = true;
        this.onRequest = false;
        this.onTimer = false;
        this.listener = null;
        this.progressInterval = 0;
        this.adLoaded = false;
        this.adDestroyed = false;
        this.pollCount = 0;
        this.pollMax = 0;
        this.pollManual = 0;
        this.loadIcon = false;
        this.useSecure = false;
        this.context = ctx;
        this.sectionid = sid;
    }

    public AdController(Activity act, String sid, AdListener lt) {
        this(act, sid, new RelativeLayout(act));
        this.listener = lt;
    }

    public AdController(Activity act, String sid, RelativeLayout ly) {
        this.LB_MAX_POLL = 500;
        this.LB_SET_MANUAL_AFTER = 10;
        this.MAX_APP_ICONS = 5;
        this.requestInProgress = false;
        this.completed = false;
        this.homeLoaded = false;
        this.linkClicked = false;
        this.pollingInitialized = false;
        this.useLocation = false;
        this.dataretrieve = true;
        this.nativeOpen = false;
        this.initialized = false;
        this.useNotification = false;
        this.additionalDockingMargin = 0;
        this.asynchTask = false;
        this.loadAd = true;
        this.onRequest = false;
        this.onTimer = false;
        this.listener = null;
        this.progressInterval = 0;
        this.adLoaded = false;
        this.adDestroyed = false;
        this.pollCount = 0;
        this.pollMax = 0;
        this.pollManual = 0;
        this.loadIcon = false;
        this.useSecure = false;
        this.activity = act;
        this.sectionid = sid;
        this.layout = ly == null ? new RelativeLayout(act) : ly;
        this.mainView = null;
    }

    public void setUseLocation(boolean uL) {
        this.useLocation = uL;
        AdLog.i(LB_LOG, "setUseLocation: " + uL);
    }

    public void setLayout(RelativeLayout ly) {
        this.layout = ly;
    }

    public void setAdditionalDockingMargin(int newOffset) {
        this.additionalDockingMargin = newOffset;
        AdLog.i(LB_LOG, "setAdditionalDockingMargin: " + newOffset);
    }

    public void setAsynchTask(boolean asynch) {
        this.asynchTask = asynch;
    }

    public void setSubId(String sbid) {
        this.subid = sbid;
    }

    public void setTokens(List<NameValuePair> tks) {
        this.tokens = tks;
    }

    public void setOnProgressInterval(int pI) {
        this.progressInterval = pI;
    }

    public void setSecureMode(boolean secureConnection) {
        this.useSecure = secureConnection;
    }

    public void destroyAd() {
        AdLog.i(LB_LOG, "destroyAd called");
        this.adDestroyed = true;
        closeUnlocker();
    }

    private void initialize() {
        AdLog.i(LB_LOG, "initializing...");
        if (this.activity != null) {
            if (this.webview == null) {
                this.webview = new WebView(this.activity);
                this.webview.getSettings().setJavaScriptEnabled(true);
                this.webview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
                this.webview.addJavascriptInterface(new LBJSInterface(this, null), "LBOUT");
                if (Build.VERSION.SDK_INT >= 8) {
                    this.webview.getSettings().setPluginState(WebSettings.PluginState.ON);
                }
            }
            this.webview.setWebChromeClient(new WebChromeClient() {
                public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {
                    new AlertDialog.Builder(AdController.this.activity).setTitle("Alert").setMessage(message).setPositiveButton(17039370, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            result.confirm();
                        }
                    }).setCancelable(false).create().show();
                    return true;
                }
            });
            this.webview.setWebViewClient(new WebViewClient() {
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    view.setPadding(0, 0, 0, 0);
                    view.setInitialScale(100);
                    view.setVerticalScrollBarEnabled(false);
                    view.setHorizontalScrollBarEnabled(false);
                    if (url.equals(AdController.this.loadUrl)) {
                        AdLog.i(AdController.LB_LOG, "Home loaded - loading = " + AdController.this.loading);
                        if (!AdController.this.loading) {
                            try {
                                if (AdController.this.results.get("useclickwindow").equals("1")) {
                                    AdLog.i(AdController.LB_LOG, "Going to use ClickWindow details");
                                    AdController.this.homeLoaded = true;
                                    AdController.this.loading = false;
                                    AdController.this.linkClicked();
                                    return;
                                }
                                AdLog.i(AdController.LB_LOG, "Normal window to be used");
                                AdController.this.loadAd();
                            } catch (Exception e) {
                                AdLog.e(AdController.LB_LOG, "Exception - " + e.getMessage());
                                AdController.this.loadAd();
                            }
                        }
                    } else {
                        AdLog.d(AdController.LB_LOG, "Link clicked!!");
                        if (AdController.this.loading) {
                            return;
                        }
                        if (AdController.this.nativeOpen || url.startsWith("market://") || url.contains("&usenative=1") || url.startsWith("http://market.android.com") || url.startsWith("https://market.android.com")) {
                            try {
                                AdLog.i(AdController.LB_LOG, "Opening URL natively");
                                view.stopLoading();
                                try {
                                    view.loadUrl(AdController.this.results.getString("clickhelpurl"));
                                } catch (Exception e2) {
                                }
                                AdController.this.activity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
                            } catch (Exception e3) {
                                AdController.this.closeUnlocker();
                            }
                        } else {
                            if (AdController.this.loadingDialog == null || !AdController.this.loadingDialog.isShowing()) {
                                AdController.this.loadingDialog = ProgressDialog.show(AdController.this.activity, "", "Loading....Please wait!", true);
                            }
                            AdController.this.linkClicked();
                        }
                    }
                }

                public void onPageFinished(WebView view, String url) {
                    if (AdController.this.loadingDialog != null && AdController.this.loadingDialog.isShowing()) {
                        AdController.this.loadingDialog.dismiss();
                    }
                    if (url.equals(AdController.this.loadUrl)) {
                        view.loadUrl("javascript:window.LBOUT.processHTML(document.getElementsByTagName('body')[0].getAttribute('ad_count'))");
                    }
                    AdController.this.loading = false;
                    AdController.this.webview.requestFocus(130);
                    try {
                        view.loadUrl("javascript:(function() { sdkNetworkCountry = '" + AdController.this.tm.getNetworkCountryIso() + "';" + "sdkNetworkOperator = '" + AdController.this.tm.getNetworkOperator() + "';" + "sdkNetworkOperatorName = '" + AdController.this.tm.getNetworkOperatorName() + "';" + "sdkPhoneNumber = '" + AdController.this.tm.getLine1Number() + "';" + "})()");
                    } catch (Exception e) {
                    }
                    if (url.contains("#app_close")) {
                        try {
                            Thread.sleep(1000);
                            AdController.this.closeUnlocker();
                        } catch (Exception e2) {
                        }
                    }
                }
            });
            try {
                NetworkInfo netInfo = ((ConnectivityManager) this.activity.getSystemService("connectivity")).getActiveNetworkInfo();
                if (netInfo != null) {
                    Boolean con = new Boolean(netInfo.isConnected());
                    boolean makeRequest = true;
                    try {
                        String displayinterval = this.activity.getSharedPreferences("Preference", 2).getString("SD_" + this.sectionid, "0");
                        if (!displayinterval.equals("0")) {
                            int curTimestamp = (int) (Calendar.getInstance().getTimeInMillis() / 1000);
                            if (displayinterval.equals("-1") || curTimestamp < new Integer(displayinterval).intValue()) {
                                makeRequest = false;
                            }
                        }
                    } catch (Exception e) {
                        AdLog.printStackTrace(LB_LOG, e);
                    }
                    if (con.booleanValue() && makeRequest) {
                        if (Build.VERSION.SDK_INT > 8 || this.asynchTask) {
                            AdLog.d(LB_LOG, "Request will not be made on main thread - SDK Level = " + Build.VERSION.SDK_INT);
                            if (this.asynchTask) {
                                AdLog.d(LB_LOG, "AsynchTask variable set");
                            }
                            this.req = new LBRequest(this, null);
                            this.req.execute("");
                            return;
                        }
                        AdLog.d(LB_LOG, "Request to be made on main thread - SDK Level = " + Build.VERSION.SDK_INT);
                        makeLBRequest();
                        if (this.loadAd) {
                            displayAd();
                        } else if (this.useNotification) {
                            setNotification();
                        } else if (this.loadIcon) {
                            displayIcon();
                        }
                    }
                } else {
                    AdLog.e(LB_LOG, "No Internet connection detected. No Ads loaded");
                    if (this.listener != null) {
                        try {
                            AdLog.i(LB_LOG, "onAdFailed triggered");
                            this.listener.onAdFailed();
                            this.adLoaded = true;
                        } catch (Exception e2) {
                            AdLog.i(LB_LOG, "Error while calling onAdFailed");
                            AdLog.printStackTrace(LB_LOG, e2);
                        }
                    }
                }
            } catch (Exception e3) {
                AdLog.printStackTrace(LB_LOG, e3);
                AdLog.e(LB_LOG, "Error Message No wifi - " + e3.getMessage());
                AdLog.e(LB_LOG, "No WIFI, 3G or Edge connection detected");
                if (this.listener != null) {
                    try {
                        AdLog.i(LB_LOG, "onAdFailed triggered");
                        this.listener.onAdFailed();
                        this.adLoaded = true;
                    } catch (Exception ex) {
                        AdLog.i(LB_LOG, "Error while calling onAdFailed");
                        AdLog.printStackTrace(LB_LOG, ex);
                    }
                }
            }
        } else if (((ConnectivityManager) this.context.getSystemService("connectivity")).getActiveNetworkInfo() != null) {
            if (Build.VERSION.SDK_INT > 8 || this.asynchTask) {
                if (this.loadIcon) {
                    AdLog.d(LB_LOG, "loadIcon Request will not be made on main thread - SDK Level = " + Build.VERSION.SDK_INT);
                } else {
                    AdLog.d(LB_LOG, "Notification Request will not be made on main thread - SDK Level = " + Build.VERSION.SDK_INT);
                }
                if (this.asynchTask) {
                    AdLog.d(LB_LOG, "AsynchTask variable set");
                }
                new LBRequest(this, null).execute("");
                return;
            }
            if (this.loadIcon) {
                AdLog.d(LB_LOG, "loadIcon Request will be made on main thread - SDK Level = " + Build.VERSION.SDK_INT);
            } else {
                AdLog.d(LB_LOG, "Notification Request will be made on main thread - SDK Level = " + Build.VERSION.SDK_INT);
            }
            makeLBRequest();
            if (this.loadIcon) {
                displayIcon();
            } else {
                setNotification();
            }
        } else if (!this.loadIcon) {
            setAlarmFromCookie();
        }
    }

    public void loadNotification() {
        AdLog.i(LB_LOG, "loadNotification called");
        if (!this.initialized) {
            this.useNotification = true;
            this.loadAd = false;
            this.notificationLaunchType = "App";
            this.onTimer = false;
            this.onRequest = false;
            initialize();
        }
    }

    /* access modifiers changed from: private */
    public void setNotification() {
        AdLog.d(LB_LOG, "setNotification called");
        if (this.results == null) {
            AdLog.e(LB_LOG, "Results are null - no notification will be loaded");
            return;
        }
        try {
            if (this.onRequest) {
                loadNotificationDetails();
            } else if (this.onTimer) {
                loadNotificationTimerDetails();
            } else if (this.results.getString("show").equals("1")) {
                String notificationtype = this.results.getString("notificationtype");
                if (notificationtype.equals("Immediate")) {
                    AdLog.i(LB_LOG, "Immediate notification to be fired");
                    loadNotificationOnRequest("App");
                } else if (notificationtype.equals("Recurring")) {
                    AdLog.i(LB_LOG, "Recurring notification to be created");
                    loadNotificationOnTimer();
                }
            } else {
                AdLog.i(LB_LOG, "Notification not be set for this user - DeviceId = " + this.tm.getDeviceId());
            }
        } catch (Exception e) {
            AdLog.printStackTrace(LB_LOG, e);
        }
    }

    private void loadNotificationOnTimer() {
        AdLog.i(LB_LOG, "loadNotificationOnTimer called");
        if (!this.initialized) {
            this.useNotification = true;
            this.loadAd = false;
            this.onRequest = true;
            this.onTimer = false;
            initialize();
            return;
        }
        loadNotificationTimerDetails();
    }

    private void loadNotificationTimerDetails() {
        if (this.results == null) {
            AdLog.e(LB_LOG, "Notification will not be loaded - no internet connection");
            return;
        }
        try {
            if (this.results.getString("show").equals("1")) {
                setAlarm();
            }
        } catch (Exception e) {
            AdLog.printStackTrace(LB_LOG, e);
        }
    }

    private void setAlarmFromCookie() {
        Context ctx;
        if (this.activity != null) {
            ctx = this.activity;
        } else {
            ctx = this.context;
        }
        SharedPreferences pref = ctx.getSharedPreferences("Preference", 2);
        SharedPreferences.Editor editor = pref.edit();
        long timesAttempted = (long) Integer.valueOf(pref.getString("SD_ALARM_ATTEMPTED_" + this.sectionid, "0")).intValue();
        long now = System.currentTimeMillis();
        long newStartTimeTime = now + 10000;
        if (timesAttempted > 25) {
            long alarminterval = pref.getLong("SD_ALARM_INTERVAL_" + this.sectionid, 0);
            AdLog.i(LB_LOG, "No internet, already tried 5 times, set it to timer " + alarminterval + "s");
            AdLog.i(LB_LOG, "Times attempted = " + timesAttempted);
            newStartTimeTime = now + (1000 * alarminterval);
            editor.putLong("SD_ALARM_TIME_" + this.sectionid, newStartTimeTime);
            editor.putString("SD_ALARM_ATTEMPTED_" + this.sectionid, "0");
            editor.commit();
        } else if (timesAttempted % 5 != 0 || timesAttempted <= 0) {
            AdLog.i(LB_LOG, "No internet, retry alarm in 10s");
            editor.putString("SD_ALARM_ATTEMPTED_" + this.sectionid, new StringBuilder().append(1 + timesAttempted).toString());
            editor.commit();
        } else {
            newStartTimeTime = now + 600000;
            AdLog.i(LB_LOG, "No internet, retry alarm in 10 mins");
            editor.putString("SD_ALARM_ATTEMPTED_" + this.sectionid, new StringBuilder().append(1 + timesAttempted).toString());
            editor.commit();
        }
        this.am = (AlarmManager) ctx.getSystemService("alarm");
        Intent intent = new Intent(ctx, AdNotification.class);
        intent.putExtra("sectionid", this.sectionid);
        this.pendingAlarmIntent = PendingIntent.getBroadcast(ctx, 0, intent, 134217728);
        try {
            this.am.set(0, newStartTimeTime, this.pendingAlarmIntent);
        } catch (Exception ex) {
            AdLog.printStackTrace(LB_LOG, ex);
        }
    }

    private void setAlarm() {
        Context ctx;
        int resultcookie;
        AdLog.i(LB_LOG, "setAlarm called");
        if (this.activity != null) {
            ctx = this.activity;
        } else {
            ctx = this.context;
        }
        SharedPreferences pref = ctx.getSharedPreferences("Preference", 2);
        SharedPreferences.Editor editor = pref.edit();
        if (this.results.length() > 0) {
            AdLog.i(LB_LOG, "internet connection found....initialize alarm from settings - Result Length=" + this.results.length());
            try {
                int iterationcounter = Integer.valueOf(pref.getString("SD_ITERATION_COUNTER_" + this.sectionid, "0")).intValue();
                try {
                    resultcookie = Integer.valueOf(this.results.getString("notificationcookie")).intValue();
                } catch (Exception e) {
                    resultcookie = 0;
                }
                AdLog.d(LB_LOG, "Values: ck=" + resultcookie + ", ic=" + iterationcounter + ", nLT=" + this.notificationLaunchType);
                this.am = (AlarmManager) ctx.getSystemService("alarm");
                Intent intent = new Intent(ctx.getApplicationContext(), AdNotification.class);
                intent.putExtra("sectionid", this.sectionid);
                this.pendingAlarmIntent = PendingIntent.getBroadcast(ctx.getApplicationContext(), 0, intent, 134217728);
                if (resultcookie == 1 || ((resultcookie == 0 && iterationcounter == 0) || this.notificationLaunchType.equals("Alarm"))) {
                    AdLog.i(LB_LOG, "alarm will be initialized - ck is " + resultcookie + ", ic is " + iterationcounter + ", nLT is " + this.notificationLaunchType);
                    Calendar cal = Calendar.getInstance();
                    long now = System.currentTimeMillis();
                    try {
                        int startat = Integer.parseInt(this.results.getString("notificationstart"));
                        cal.add(13, startat);
                        AdLog.d(LB_LOG, "Alarm initialized - Scheduled at " + startat + ", current time = " + now);
                        AdLog.d(LB_LOG, "----------------------------------------");
                        this.am.set(0, cal.getTimeInMillis(), this.pendingAlarmIntent);
                        editor.putLong("SD_ALARM_TIME_" + this.sectionid, now);
                        editor.putLong("SD_WAKE_TIME_" + this.sectionid, cal.getTimeInMillis());
                        editor.putLong("SD_ALARM_INTERVAL_" + this.sectionid, (long) startat);
                        editor.commit();
                    } catch (Exception e2) {
                        AdLog.e(LB_LOG, "Error caused while setting Alarm (if case): " + e2.getMessage());
                        AdLog.printStackTrace(LB_LOG, e2);
                    }
                } else {
                    long newstartat = pref.getLong("SD_WAKE_TIME_" + this.sectionid, 0);
                    Calendar cal2 = Calendar.getInstance();
                    long now2 = System.currentTimeMillis();
                    try {
                        cal2.setTimeInMillis(newstartat);
                        AdLog.d(LB_LOG, "Alarm reset - Scheduled at " + newstartat + ", current time = " + now2);
                        AdLog.d(LB_LOG, "----------------------------------------");
                        this.am.set(0, cal2.getTimeInMillis(), this.pendingAlarmIntent);
                        editor.putLong("SD_ALARM_TIME_" + this.sectionid, now2);
                        editor.putLong("SD_WAKE_TIME_" + this.sectionid, newstartat);
                        editor.commit();
                    } catch (Exception e3) {
                        AdLog.e(LB_LOG, "Error caused while setting Alarm (else case): " + e3.getMessage());
                        AdLog.printStackTrace(LB_LOG, e3);
                    }
                }
            } catch (Exception e4) {
            }
        } else {
            setAlarmFromCookie();
        }
    }

    public void loadNotificationOnRequest(String launchtype) {
        if (launchtype.equals("App") || launchtype.equals("Alarm")) {
            AdLog.i(LB_LOG, "loadNotificationOnRequest called");
            this.notificationLaunchType = launchtype;
            if (!this.initialized) {
                this.useNotification = true;
                this.loadAd = false;
                this.onRequest = true;
                this.onTimer = false;
                initialize();
                return;
            }
            loadNotificationDetails();
            return;
        }
        AdLog.e(LB_LOG, "Illegal use of loadNotificationOnRequest. LaunchType used = " + launchtype);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0276, code lost:
        if (r0.notificationLaunchType.equals(r23) != false) goto L_0x0278;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void loadNotificationDetails() {
        /*
            r25 = this;
            java.lang.String r21 = "LBAdController"
            java.lang.String r22 = "loadNotificationDetails called"
            com.Leadbolt.AdLog.i(r21, r22)
            r0 = r25
            org.json.JSONObject r0 = r0.results
            r21 = r0
            if (r21 != 0) goto L_0x0017
            java.lang.String r21 = "LBAdController"
            java.lang.String r22 = "Notification will not be loaded - no internet connection"
            com.Leadbolt.AdLog.e(r21, r22)
        L_0x0016:
            return
        L_0x0017:
            r0 = r25
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0210 }
            r21 = r0
            java.lang.String r22 = "show"
            java.lang.String r21 = r21.getString(r22)     // Catch:{ Exception -> 0x0210 }
            java.lang.String r22 = "1"
            boolean r21 = r21.equals(r22)     // Catch:{ Exception -> 0x0210 }
            if (r21 == 0) goto L_0x01c0
            java.lang.String r21 = "LBAdController"
            java.lang.String r22 = "notification to be fired"
            com.Leadbolt.AdLog.i(r21, r22)     // Catch:{ Exception -> 0x0210 }
            r0 = r25
            android.app.Activity r0 = r0.activity     // Catch:{ Exception -> 0x0210 }
            r21 = r0
            if (r21 == 0) goto L_0x0200
            r0 = r25
            android.app.Activity r6 = r0.activity     // Catch:{ Exception -> 0x0210 }
        L_0x003e:
            java.lang.String r21 = "notification"
            r0 = r21
            java.lang.Object r15 = r6.getSystemService(r0)     // Catch:{ Exception -> 0x0210 }
            android.app.NotificationManager r15 = (android.app.NotificationManager) r15     // Catch:{ Exception -> 0x0210 }
            r21 = 5
            r0 = r21
            int[] r10 = new int[r0]     // Catch:{ Exception -> 0x0210 }
            r10 = {17301620, 17301547, 17301516, 17301514, 17301618} // fill-array
            r9 = 0
            r0 = r25
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0206, all -> 0x020c }
            r21 = r0
            java.lang.String r22 = "notificationicon"
            int r9 = r21.getInt(r22)     // Catch:{ Exception -> 0x0206, all -> 0x020c }
            r9 = r10[r9]     // Catch:{ Exception -> 0x0210 }
        L_0x0060:
            r0 = r25
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0210 }
            r21 = r0
            java.lang.String r22 = "notificationtext"
            java.lang.String r17 = r21.getString(r22)     // Catch:{ Exception -> 0x0210 }
            java.lang.String r11 = ""
            r0 = r25
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x02a6 }
            r21 = r0
            java.lang.String r22 = "notificationinstruction"
            java.lang.String r11 = r21.getString(r22)     // Catch:{ Exception -> 0x02a6 }
        L_0x007a:
            r0 = r25
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0210 }
            r21 = r0
            java.lang.String r22 = "notificationtext"
            java.lang.String r5 = r21.getString(r22)     // Catch:{ Exception -> 0x0210 }
            r0 = r25
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0210 }
            r21 = r0
            java.lang.String r22 = "notificationdescription"
            java.lang.String r4 = r21.getString(r22)     // Catch:{ Exception -> 0x0210 }
            r0 = r25
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x02a3 }
            r21 = r0
            java.lang.String r22 = "notificationdisplay"
            java.lang.String r18 = r21.getString(r22)     // Catch:{ Exception -> 0x02a3 }
            java.lang.String r21 = ""
            r0 = r18
            r1 = r21
            boolean r21 = r0.equals(r1)     // Catch:{ Exception -> 0x02a3 }
            if (r21 != 0) goto L_0x0258
            java.lang.String r21 = "notificationtext"
            r0 = r18
            r1 = r21
            boolean r21 = r0.equals(r1)     // Catch:{ Exception -> 0x02a3 }
            if (r21 == 0) goto L_0x00c5
            r0 = r25
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x02a3 }
            r21 = r0
            r0 = r21
            r1 = r18
            java.lang.String r5 = r0.getString(r1)     // Catch:{ Exception -> 0x02a3 }
            r4 = r11
        L_0x00c5:
            long r19 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0210 }
            android.content.Intent r12 = new android.content.Intent     // Catch:{ Exception -> 0x0210 }
            java.lang.String r21 = "android.intent.action.VIEW"
            r0 = r25
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0210 }
            r22 = r0
            java.lang.String r23 = "notificationurl"
            java.lang.String r22 = r22.getString(r23)     // Catch:{ Exception -> 0x0210 }
            android.net.Uri r22 = android.net.Uri.parse(r22)     // Catch:{ Exception -> 0x0210 }
            r0 = r21
            r1 = r22
            r12.<init>(r0, r1)     // Catch:{ Exception -> 0x0210 }
            r21 = 0
            r22 = 0
            r0 = r21
            r1 = r22
            android.app.PendingIntent r3 = android.app.PendingIntent.getActivity(r6, r0, r12, r1)     // Catch:{ Exception -> 0x0210 }
            android.app.Notification r14 = new android.app.Notification     // Catch:{ Exception -> 0x0210 }
            r0 = r17
            r1 = r19
            r14.<init>(r9, r0, r1)     // Catch:{ Exception -> 0x0210 }
            int r0 = r14.flags     // Catch:{ Exception -> 0x0210 }
            r21 = r0
            r21 = r21 | 16
            r0 = r21
            r14.flags = r0     // Catch:{ Exception -> 0x0210 }
            r14.setLatestEventInfo(r6, r5, r4, r3)     // Catch:{ Exception -> 0x0210 }
            java.lang.String r21 = "Preference"
            r22 = 2
            r0 = r21
            r1 = r22
            android.content.SharedPreferences r16 = r6.getSharedPreferences(r0, r1)     // Catch:{ Exception -> 0x0210 }
            android.content.SharedPreferences$Editor r8 = r16.edit()     // Catch:{ Exception -> 0x0210 }
            java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0210 }
            java.lang.String r22 = "SD_NOTIFICATION_ID_"
            r21.<init>(r22)     // Catch:{ Exception -> 0x0210 }
            r0 = r25
            java.lang.String r0 = r0.sectionid     // Catch:{ Exception -> 0x0210 }
            r22 = r0
            java.lang.StringBuilder r21 = r21.append(r22)     // Catch:{ Exception -> 0x0210 }
            java.lang.String r21 = r21.toString()     // Catch:{ Exception -> 0x0210 }
            r22 = 0
            r0 = r16
            r1 = r21
            r2 = r22
            int r13 = r0.getInt(r1, r2)     // Catch:{ Exception -> 0x0210 }
            java.lang.String r21 = "LBAdController"
            java.lang.StringBuilder r22 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0210 }
            java.lang.String r23 = "Stored Pref - "
            r22.<init>(r23)     // Catch:{ Exception -> 0x0210 }
            r0 = r22
            java.lang.StringBuilder r22 = r0.append(r13)     // Catch:{ Exception -> 0x0210 }
            java.lang.String r22 = r22.toString()     // Catch:{ Exception -> 0x0210 }
            com.Leadbolt.AdLog.d(r21, r22)     // Catch:{ Exception -> 0x0210 }
            if (r13 != 0) goto L_0x0151
            r13 = 10001(0x2711, float:1.4014E-41)
        L_0x0151:
            r21 = 1001(0x3e9, float:1.403E-42)
            r0 = r21
            r15.notify(r0, r14)     // Catch:{ Exception -> 0x0210 }
            java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02a0 }
            java.lang.String r22 = "SD_NOTIFICATION_FIRED_"
            r21.<init>(r22)     // Catch:{ Exception -> 0x02a0 }
            r0 = r25
            java.lang.String r0 = r0.sectionid     // Catch:{ Exception -> 0x02a0 }
            r22 = r0
            java.lang.StringBuilder r21 = r21.append(r22)     // Catch:{ Exception -> 0x02a0 }
            java.lang.String r21 = r21.toString()     // Catch:{ Exception -> 0x02a0 }
            r0 = r21
            r1 = r19
            r8.putLong(r0, r1)     // Catch:{ Exception -> 0x02a0 }
            r8.commit()     // Catch:{ Exception -> 0x02a0 }
        L_0x0177:
            java.lang.String r21 = "LBAdController"
            java.lang.StringBuilder r22 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0210 }
            java.lang.String r23 = "New Notification created with ID - "
            r22.<init>(r23)     // Catch:{ Exception -> 0x0210 }
            r0 = r22
            java.lang.StringBuilder r22 = r0.append(r13)     // Catch:{ Exception -> 0x0210 }
            java.lang.String r22 = r22.toString()     // Catch:{ Exception -> 0x0210 }
            com.Leadbolt.AdLog.d(r21, r22)     // Catch:{ Exception -> 0x0210 }
            r0 = r25
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x029d }
            r21 = r0
            java.lang.String r22 = "notificationmultiple"
            java.lang.String r21 = r21.getString(r22)     // Catch:{ Exception -> 0x029d }
            java.lang.String r22 = "1"
            boolean r21 = r21.equals(r22)     // Catch:{ Exception -> 0x029d }
            if (r21 == 0) goto L_0x01c0
            java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x029d }
            java.lang.String r22 = "SD_NOTIFICATION_ID_"
            r21.<init>(r22)     // Catch:{ Exception -> 0x029d }
            r0 = r25
            java.lang.String r0 = r0.sectionid     // Catch:{ Exception -> 0x029d }
            r22 = r0
            java.lang.StringBuilder r21 = r21.append(r22)     // Catch:{ Exception -> 0x029d }
            java.lang.String r21 = r21.toString()     // Catch:{ Exception -> 0x029d }
            int r13 = r13 + 1
            r0 = r21
            r8.putInt(r0, r13)     // Catch:{ Exception -> 0x029d }
            r8.commit()     // Catch:{ Exception -> 0x029d }
        L_0x01c0:
            r25.incrementIterationCounter()
            r0 = r25
            boolean r0 = r0.onTimer     // Catch:{ Exception -> 0x01de }
            r21 = r0
            if (r21 != 0) goto L_0x01d9
            r0 = r25
            java.lang.String r0 = r0.notificationLaunchType     // Catch:{ Exception -> 0x01de }
            r21 = r0
            java.lang.String r22 = "Alarm"
            boolean r21 = r21.equals(r22)     // Catch:{ Exception -> 0x01de }
            if (r21 == 0) goto L_0x0016
        L_0x01d9:
            r25.setAlarm()     // Catch:{ Exception -> 0x01de }
            goto L_0x0016
        L_0x01de:
            r7 = move-exception
            java.lang.String r21 = "LBAdController"
            java.lang.StringBuilder r22 = new java.lang.StringBuilder
            java.lang.String r23 = "Error while setting Alarm - "
            r22.<init>(r23)
            java.lang.String r23 = r7.getMessage()
            java.lang.StringBuilder r22 = r22.append(r23)
            java.lang.String r22 = r22.toString()
            com.Leadbolt.AdLog.e(r21, r22)
            java.lang.String r21 = "LBAdController"
            r0 = r21
            com.Leadbolt.AdLog.printStackTrace(r0, r7)
            goto L_0x0016
        L_0x0200:
            r0 = r25
            android.content.Context r6 = r0.context     // Catch:{ Exception -> 0x0210 }
            goto L_0x003e
        L_0x0206:
            r7 = move-exception
            r9 = 0
            r9 = r10[r9]     // Catch:{ Exception -> 0x0210 }
            goto L_0x0060
        L_0x020c:
            r21 = move-exception
            r9 = r10[r9]     // Catch:{ Exception -> 0x0210 }
            throw r21     // Catch:{ Exception -> 0x0210 }
        L_0x0210:
            r7 = move-exception
            java.lang.String r21 = "LBAdController"
            r0 = r21
            com.Leadbolt.AdLog.printStackTrace(r0, r7)     // Catch:{ all -> 0x025e }
            r25.incrementIterationCounter()
            r0 = r25
            boolean r0 = r0.onTimer     // Catch:{ Exception -> 0x0236 }
            r21 = r0
            if (r21 != 0) goto L_0x0231
            r0 = r25
            java.lang.String r0 = r0.notificationLaunchType     // Catch:{ Exception -> 0x0236 }
            r21 = r0
            java.lang.String r22 = "Alarm"
            boolean r21 = r21.equals(r22)     // Catch:{ Exception -> 0x0236 }
            if (r21 == 0) goto L_0x0016
        L_0x0231:
            r25.setAlarm()     // Catch:{ Exception -> 0x0236 }
            goto L_0x0016
        L_0x0236:
            r7 = move-exception
            java.lang.String r21 = "LBAdController"
            java.lang.StringBuilder r22 = new java.lang.StringBuilder
            java.lang.String r23 = "Error while setting Alarm - "
            r22.<init>(r23)
            java.lang.String r23 = r7.getMessage()
            java.lang.StringBuilder r22 = r22.append(r23)
            java.lang.String r22 = r22.toString()
            com.Leadbolt.AdLog.e(r21, r22)
            java.lang.String r21 = "LBAdController"
            r0 = r21
            com.Leadbolt.AdLog.printStackTrace(r0, r7)
            goto L_0x0016
        L_0x0258:
            java.lang.String r5 = "You have 1 new message"
            java.lang.String r4 = "Tap to View"
            goto L_0x00c5
        L_0x025e:
            r21 = move-exception
            r25.incrementIterationCounter()
            r0 = r25
            boolean r0 = r0.onTimer     // Catch:{ Exception -> 0x027c }
            r22 = r0
            if (r22 != 0) goto L_0x0278
            r0 = r25
            java.lang.String r0 = r0.notificationLaunchType     // Catch:{ Exception -> 0x027c }
            r22 = r0
            java.lang.String r23 = "Alarm"
            boolean r22 = r22.equals(r23)     // Catch:{ Exception -> 0x027c }
            if (r22 == 0) goto L_0x027b
        L_0x0278:
            r25.setAlarm()     // Catch:{ Exception -> 0x027c }
        L_0x027b:
            throw r21
        L_0x027c:
            r7 = move-exception
            java.lang.String r22 = "LBAdController"
            java.lang.StringBuilder r23 = new java.lang.StringBuilder
            java.lang.String r24 = "Error while setting Alarm - "
            r23.<init>(r24)
            java.lang.String r24 = r7.getMessage()
            java.lang.StringBuilder r23 = r23.append(r24)
            java.lang.String r23 = r23.toString()
            com.Leadbolt.AdLog.e(r22, r23)
            java.lang.String r22 = "LBAdController"
            r0 = r22
            com.Leadbolt.AdLog.printStackTrace(r0, r7)
            goto L_0x027b
        L_0x029d:
            r21 = move-exception
            goto L_0x01c0
        L_0x02a0:
            r21 = move-exception
            goto L_0x0177
        L_0x02a3:
            r21 = move-exception
            goto L_0x00c5
        L_0x02a6:
            r21 = move-exception
            goto L_0x007a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.Leadbolt.AdController.loadNotificationDetails():void");
    }

    private void incrementIterationCounter() {
        AdLog.i(LB_LOG, "increment counter called");
        SharedPreferences pref = (this.activity != null ? this.activity : this.context).getSharedPreferences("Preference", 2);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("SD_ITERATION_COUNTER_" + this.sectionid, new StringBuilder().append(Integer.valueOf(pref.getString("SD_ITERATION_COUNTER_" + this.sectionid, "0")).intValue() + 1).toString());
        editor.commit();
    }

    public void loadAd() {
        AdLog.i(LB_LOG, "loadAd called");
        if (!this.initialized) {
            initialize();
            this.loadAd = true;
        } else {
            displayAd();
        }
        if (this.listener != null && this.progressInterval > 0) {
            if (this.loadProgress == null) {
                this.loadProgress = new Runnable() {
                    public void run() {
                        try {
                            if (!AdController.this.adLoaded && !AdController.this.adDestroyed) {
                                AdLog.i(AdController.LB_LOG, "onAdProgress triggered");
                                AdController.this.listener.onAdProgress();
                                AdController.this.progressHandler.postDelayed(AdController.this.loadProgress, (long) (AdController.this.progressInterval * 1000));
                            }
                        } catch (Exception e) {
                            AdLog.e(AdController.LB_LOG, "error when onAdProgress triggered");
                            AdLog.printStackTrace(AdController.LB_LOG, e);
                        }
                    }
                };
            }
            if (this.progressHandler == null) {
                this.progressHandler = new Handler();
                this.progressHandler.postDelayed(this.loadProgress, (long) (this.progressInterval * 1000));
            }
        }
    }

    public void loadIcon() {
        AdLog.i(LB_LOG, "loadIcon called");
        if (!this.initialized) {
            this.loadAd = false;
            this.loadIcon = true;
            initialize();
            return;
        }
        displayIcon();
    }

    /* access modifiers changed from: private */
    public void displayIcon() {
        String adText;
        AdLog.i(LB_LOG, "displayIcon called");
        if (this.results == null) {
            AdLog.e(LB_LOG, "Results are null - no icon will be loaded");
            return;
        }
        try {
            if (this.results.get("show").equals("1") && this.context != null) {
                AdLog.i(LB_LOG, "going to display icon");
                try {
                    adText = this.results.getString("adname");
                } catch (Exception e) {
                    AdLog.printStackTrace(LB_LOG, e);
                }
                SharedPreferences pref = this.context.getSharedPreferences("Preference", 2);
                int displayCount = pref.getInt("SD_ICON_DISPLAY_" + this.sectionid, 0);
                if (displayCount < 5) {
                    AdLog.i(LB_LOG, "MAX count not passed so display icon");
                    Intent shortcutIntent = new Intent("android.intent.action.VIEW", Uri.parse(String.valueOf(this.results.getString("adurl")) + this.sectionid));
                    Intent addIntent = new Intent();
                    addIntent.putExtra("android.intent.extra.shortcut.INTENT", shortcutIntent);
                    addIntent.putExtra("android.intent.extra.shortcut.NAME", adText);
                    try {
                        Bitmap bmp = Bitmap.createScaledBitmap(BitmapFactory.decodeStream(((HttpURLConnection) new URL(this.results.getString("adiconurl")).openConnection()).getInputStream()), 128, 128, true);
                        if (bmp != null) {
                            AdLog.i(LB_LOG, "bitmap not null");
                            addIntent.putExtra("android.intent.extra.shortcut.ICON", bmp);
                        } else {
                            AdLog.i(LB_LOG, "bitmap null");
                            addIntent.putExtra("android.intent.extra.shortcut.ICON_RESOURCE", Intent.ShortcutIconResource.fromContext(this.context, 17301593));
                        }
                    } catch (Exception e2) {
                        AdLog.i(LB_LOG, "exception in getting icon");
                        AdLog.printStackTrace(LB_LOG, e2);
                        addIntent.putExtra("android.intent.extra.shortcut.ICON_RESOURCE", Intent.ShortcutIconResource.fromContext(this.context, 17301593));
                    }
                    addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
                    this.context.sendBroadcast(addIntent);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putInt("SD_ICON_DISPLAY_" + this.sectionid, displayCount + 1);
                    editor.commit();
                    return;
                }
                AdLog.d(LB_LOG, "DisplayCount = " + displayCount + ", MAX_APP_ICONS = " + 5);
            }
        } catch (Exception e3) {
            AdLog.printStackTrace(LB_LOG, e3);
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x0844, code lost:
        closeUnlocker();
        r1.loading = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x0851, code lost:
        r14 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x0852, code lost:
        closeUnlocker();
        android.util.Log.d("AdController", "JSONException - " + r14.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x08ff, code lost:
        r14 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:133:?, code lost:
        com.Leadbolt.AdLog.i(com.Leadbolt.AdController.LB_LOG, "Error while calling onAdFailed");
        com.Leadbolt.AdLog.printStackTrace(com.Leadbolt.AdController.LB_LOG, r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:140:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:148:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:151:?, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:25:0x015f, B:128:0x08e9] */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x0851 A[ExcHandler: JSONException (r14v1 'e' org.json.JSONException A[CUSTOM_DECLARE]), Splitter:B:25:0x015f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void displayAd() {
        /*
            r50 = this;
            r0 = r50
            org.json.JSONObject r0 = r0.results
            r45 = r0
            if (r45 != 0) goto L_0x0010
            java.lang.String r45 = "LBAdController"
            java.lang.String r46 = "Results are null - no ad will be loaded"
            com.Leadbolt.AdLog.e(r45, r46)
        L_0x000f:
            return
        L_0x0010:
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x004d }
            r45 = r0
            java.lang.String r46 = "useclickwindow"
            boolean r45 = r45.isNull(r46)     // Catch:{ Exception -> 0x004d }
            if (r45 != 0) goto L_0x006d
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x004d }
            r45 = r0
            java.lang.String r46 = "useclickwindow"
            java.lang.String r45 = r45.getString(r46)     // Catch:{ Exception -> 0x004d }
            java.lang.String r46 = "1"
            boolean r45 = r45.equals(r46)     // Catch:{ Exception -> 0x004d }
            if (r45 == 0) goto L_0x006d
            r45 = 0
            r0 = r45
            r1 = r50
            r1.loading = r0     // Catch:{ Exception -> 0x004d }
            r45 = 0
            r0 = r45
            r1 = r50
            r1.linkClicked = r0     // Catch:{ Exception -> 0x004d }
            java.lang.String r45 = "LBAdController"
            java.lang.String r46 = "Going to use click window - cancel out of here..."
            com.Leadbolt.AdLog.i(r45, r46)     // Catch:{ Exception -> 0x004d }
            r50.linkClicked()     // Catch:{ Exception -> 0x004d }
            goto L_0x000f
        L_0x004d:
            r14 = move-exception
            java.lang.String r45 = "LBAdController"
            r0 = r45
            com.Leadbolt.AdLog.printStackTrace(r0, r14)
            java.lang.String r45 = "LBAdController"
            java.lang.StringBuilder r46 = new java.lang.StringBuilder
            java.lang.String r47 = "Exception when using ClickWindow - "
            r46.<init>(r47)
            java.lang.String r47 = r14.getMessage()
            java.lang.StringBuilder r46 = r46.append(r47)
            java.lang.String r46 = r46.toString()
            com.Leadbolt.AdLog.e(r45, r46)
        L_0x006d:
            r0 = r50
            boolean r0 = r0.homeLoaded
            r45 = r0
            if (r45 != 0) goto L_0x000f
            r45 = 1
            r0 = r45
            r1 = r50
            r1.homeLoaded = r0
            r45 = 0
            r0 = r45
            r1 = r50
            r1.linkClicked = r0
            r45 = 1
            r0 = r45
            r1 = r50
            r1.loading = r0     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            java.lang.String r46 = "show"
            java.lang.Object r45 = r45.get(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            java.lang.String r46 = "1"
            boolean r45 = r45.equals(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            if (r45 == 0) goto L_0x08d2
            r0 = r50
            boolean r0 = r0.completed     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            if (r45 != 0) goto L_0x08c5
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ Exception -> 0x0919, JSONException -> 0x0851 }
            r45 = r0
            r0 = r50
            android.webkit.WebView r0 = r0.webview     // Catch:{ Exception -> 0x0919, JSONException -> 0x0851 }
            r46 = r0
            r45.removeView(r46)     // Catch:{ Exception -> 0x0919, JSONException -> 0x0851 }
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ Exception -> 0x0919, JSONException -> 0x0851 }
            r45 = r0
            r0 = r50
            android.view.View r0 = r0.toolbar     // Catch:{ Exception -> 0x0919, JSONException -> 0x0851 }
            r46 = r0
            r45.removeView(r46)     // Catch:{ Exception -> 0x0919, JSONException -> 0x0851 }
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ Exception -> 0x0919, JSONException -> 0x0851 }
            r45 = r0
            r0 = r50
            android.widget.TextView r0 = r0.title     // Catch:{ Exception -> 0x0919, JSONException -> 0x0851 }
            r46 = r0
            r45.removeView(r46)     // Catch:{ Exception -> 0x0919, JSONException -> 0x0851 }
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ Exception -> 0x0919, JSONException -> 0x0851 }
            r45 = r0
            r0 = r50
            android.view.View r0 = r0.footer     // Catch:{ Exception -> 0x0919, JSONException -> 0x0851 }
            r46 = r0
            r45.removeView(r46)     // Catch:{ Exception -> 0x0919, JSONException -> 0x0851 }
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ Exception -> 0x0919, JSONException -> 0x0851 }
            r45 = r0
            r0 = r50
            android.widget.TextView r0 = r0.footerTitle     // Catch:{ Exception -> 0x0919, JSONException -> 0x0851 }
            r46 = r0
            r45.removeView(r46)     // Catch:{ Exception -> 0x0919, JSONException -> 0x0851 }
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ Exception -> 0x0919, JSONException -> 0x0851 }
            r45 = r0
            r0 = r50
            android.widget.Button r0 = r0.backBtn     // Catch:{ Exception -> 0x0919, JSONException -> 0x0851 }
            r46 = r0
            r45.removeView(r46)     // Catch:{ Exception -> 0x0919, JSONException -> 0x0851 }
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ Exception -> 0x0919, JSONException -> 0x0851 }
            r45 = r0
            r0 = r50
            android.widget.Button r0 = r0.fwdBtn     // Catch:{ Exception -> 0x0919, JSONException -> 0x0851 }
            r46 = r0
            r45.removeView(r46)     // Catch:{ Exception -> 0x0919, JSONException -> 0x0851 }
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ Exception -> 0x0919, JSONException -> 0x0851 }
            r45 = r0
            r0 = r50
            android.widget.Button r0 = r0.closeBtn     // Catch:{ Exception -> 0x0919, JSONException -> 0x0851 }
            r46 = r0
            r45.removeView(r46)     // Catch:{ Exception -> 0x0919, JSONException -> 0x0851 }
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ Exception -> 0x0919, JSONException -> 0x0851 }
            r45 = r0
            r0 = r50
            android.widget.Button r0 = r0.homeBtn     // Catch:{ Exception -> 0x0919, JSONException -> 0x0851 }
            r46 = r0
            r45.removeView(r46)     // Catch:{ Exception -> 0x0919, JSONException -> 0x0851 }
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ Exception -> 0x0919, JSONException -> 0x0851 }
            r45 = r0
            r0 = r50
            android.widget.Button r0 = r0.refreshBtn     // Catch:{ Exception -> 0x0919, JSONException -> 0x0851 }
            r46 = r0
            r45.removeView(r46)     // Catch:{ Exception -> 0x0919, JSONException -> 0x0851 }
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ Exception -> 0x0919, JSONException -> 0x0851 }
            r45 = r0
            r0 = r50
            android.view.View r0 = r0.mask     // Catch:{ Exception -> 0x0919, JSONException -> 0x0851 }
            r46 = r0
            r45.removeView(r46)     // Catch:{ Exception -> 0x0919, JSONException -> 0x0851 }
            r0 = r50
            android.widget.Button r0 = r0.pollBtn     // Catch:{ Exception -> 0x0919, JSONException -> 0x0851 }
            r45 = r0
            if (r45 == 0) goto L_0x015f
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ Exception -> 0x0919, JSONException -> 0x0851 }
            r45 = r0
            r45.removeAllViews()     // Catch:{ Exception -> 0x0919, JSONException -> 0x0851 }
        L_0x015f:
            android.view.View r45 = new android.view.View     // Catch:{ Exception -> 0x0916, JSONException -> 0x0851 }
            r0 = r50
            android.app.Activity r0 = r0.activity     // Catch:{ Exception -> 0x0916, JSONException -> 0x0851 }
            r46 = r0
            r45.<init>(r46)     // Catch:{ Exception -> 0x0916, JSONException -> 0x0851 }
            r0 = r45
            r1 = r50
            r1.mask = r0     // Catch:{ Exception -> 0x0916, JSONException -> 0x0851 }
            r0 = r50
            android.view.View r0 = r0.mask     // Catch:{ Exception -> 0x0916, JSONException -> 0x0851 }
            r45 = r0
            r46 = -1
            r45.setMinimumHeight(r46)     // Catch:{ Exception -> 0x0916, JSONException -> 0x0851 }
            r0 = r50
            android.view.View r0 = r0.mask     // Catch:{ Exception -> 0x0916, JSONException -> 0x0851 }
            r45 = r0
            r46 = -1
            r45.setMinimumWidth(r46)     // Catch:{ Exception -> 0x0916, JSONException -> 0x0851 }
            r0 = r50
            android.view.View r0 = r0.mask     // Catch:{ Exception -> 0x0916, JSONException -> 0x0851 }
            r45 = r0
            r46 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r45.setBackgroundColor(r46)     // Catch:{ Exception -> 0x0916, JSONException -> 0x0851 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0916, JSONException -> 0x0851 }
            r45 = r0
            java.lang.String r46 = "maskalpha"
            double r45 = r45.getDouble(r46)     // Catch:{ Exception -> 0x0916, JSONException -> 0x0851 }
            r47 = 4643176031446892544(0x406fe00000000000, double:255.0)
            double r5 = r45 * r47
            r0 = r50
            android.view.View r0 = r0.mask     // Catch:{ Exception -> 0x0916, JSONException -> 0x0851 }
            r45 = r0
            android.graphics.drawable.Drawable r45 = r45.getBackground()     // Catch:{ Exception -> 0x0916, JSONException -> 0x0851 }
            int r0 = (int) r5     // Catch:{ Exception -> 0x0916, JSONException -> 0x0851 }
            r46 = r0
            r45.setAlpha(r46)     // Catch:{ Exception -> 0x0916, JSONException -> 0x0851 }
            r0 = r50
            android.view.View r0 = r0.mask     // Catch:{ Exception -> 0x0916, JSONException -> 0x0851 }
            r45 = r0
            com.Leadbolt.AdController$4 r46 = new com.Leadbolt.AdController$4     // Catch:{ Exception -> 0x0916, JSONException -> 0x0851 }
            r0 = r46
            r1 = r50
            r0.<init>()     // Catch:{ Exception -> 0x0916, JSONException -> 0x0851 }
            r45.setOnClickListener(r46)     // Catch:{ Exception -> 0x0916, JSONException -> 0x0851 }
        L_0x01c6:
            android.view.ViewGroup$MarginLayoutParams r30 = new android.view.ViewGroup$MarginLayoutParams     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = -1
            r46 = -1
            r0 = r30
            r1 = r45
            r2 = r46
            r0.<init>(r1, r2)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = 0
            r46 = 0
            r47 = 0
            r48 = 0
            r0 = r30
            r1 = r45
            r2 = r46
            r3 = r47
            r4 = r48
            r0.setMargins(r1, r2, r3, r4)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            android.widget.RelativeLayout$LayoutParams r29 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r29.<init>(r30)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            java.lang.String r46 = "windowy"
            int r44 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            java.lang.String r13 = "Middle"
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0913, JSONException -> 0x0851 }
            r45 = r0
            java.lang.String r46 = "windowdockingy"
            java.lang.String r13 = r45.getString(r46)     // Catch:{ Exception -> 0x0913, JSONException -> 0x0851 }
        L_0x0209:
            r0 = r50
            int r0 = r0.additionalDockingMargin     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            if (r45 <= 0) goto L_0x025b
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            java.lang.String r46 = "titlevisible"
            int r45 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            if (r45 != 0) goto L_0x025b
            java.lang.String r45 = "Middle"
            r0 = r45
            boolean r45 = r13.equals(r0)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            if (r45 != 0) goto L_0x025b
            java.lang.String r45 = "LBAdController"
            java.lang.StringBuilder r46 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            java.lang.String r47 = "Additional Docking is set, adjusting banner by "
            r46.<init>(r47)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            int r0 = r0.additionalDockingMargin     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r47 = r0
            java.lang.StringBuilder r46 = r46.append(r47)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            java.lang.String r47 = "px"
            java.lang.StringBuilder r46 = r46.append(r47)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            java.lang.String r46 = r46.toString()     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            com.Leadbolt.AdLog.i(r45, r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            java.lang.String r45 = "Top"
            r0 = r45
            boolean r45 = r13.equals(r0)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            if (r45 == 0) goto L_0x082b
            r0 = r50
            int r0 = r0.additionalDockingMargin     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            int r44 = r44 + r45
        L_0x025b:
            android.view.ViewGroup$MarginLayoutParams r43 = new android.view.ViewGroup$MarginLayoutParams     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            java.lang.String r46 = "windowwidth"
            int r45 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r46 = r0
            java.lang.String r47 = "windowheight"
            int r46 = r46.getInt(r47)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r43
            r1 = r45
            r2 = r46
            r0.<init>(r1, r2)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            java.lang.String r46 = "windowx"
            int r45 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r46 = 0
            r47 = 0
            r0 = r43
            r1 = r45
            r2 = r44
            r3 = r46
            r4 = r47
            r0.setMargins(r1, r2, r3, r4)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            android.widget.RelativeLayout$LayoutParams r42 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r42.<init>(r43)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            java.lang.String r46 = "titlex"
            int r38 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            java.lang.String r46 = "titley"
            int r39 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            java.lang.String r46 = "titlewidth"
            int r37 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            java.lang.String r46 = "titleheight"
            int r36 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            android.view.ViewGroup$MarginLayoutParams r32 = new android.view.ViewGroup$MarginLayoutParams     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r32
            r1 = r37
            r2 = r36
            r0.<init>(r1, r2)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = 0
            r46 = 0
            r0 = r32
            r1 = r38
            r2 = r39
            r3 = r45
            r4 = r46
            r0.setMargins(r1, r2, r3, r4)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            android.widget.RelativeLayout$LayoutParams r31 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r31.<init>(r32)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            android.view.View r45 = new android.view.View     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            android.app.Activity r0 = r0.activity     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r46 = r0
            r45.<init>(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r45
            r1 = r50
            r1.toolbar = r0     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            java.lang.String r46 = "titlecolor"
            java.lang.String r12 = r45.getString(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            java.lang.String r45 = ""
            r0 = r45
            boolean r45 = r12.equals(r0)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            if (r45 != 0) goto L_0x031a
            if (r12 != 0) goto L_0x031c
        L_0x031a:
            java.lang.String r12 = "#E6E6E6"
        L_0x031c:
            r0 = r50
            android.view.View r0 = r0.toolbar     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            int r46 = android.graphics.Color.parseColor(r12)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45.setBackgroundColor(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            android.widget.TextView r45 = new android.widget.TextView     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            android.app.Activity r0 = r0.activity     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r46 = r0
            r45.<init>(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r45
            r1 = r50
            r1.title = r0     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            android.widget.TextView r0 = r0.title     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r46 = r0
            java.lang.String r47 = "titletext"
            java.lang.String r46 = r46.getString(r47)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45.setText(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            java.lang.String r46 = "titletextcolor"
            java.lang.String r9 = r45.getString(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            java.lang.String r45 = ""
            r0 = r45
            boolean r45 = r9.equals(r0)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            if (r45 != 0) goto L_0x0367
            if (r9 != 0) goto L_0x0369
        L_0x0367:
            java.lang.String r9 = "#E6E6E6"
        L_0x0369:
            r0 = r50
            android.widget.TextView r0 = r0.title     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            int r46 = android.graphics.Color.parseColor(r9)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45.setTextColor(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            java.lang.String r46 = "titletextheight"
            int r33 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            android.view.ViewGroup$MarginLayoutParams r41 = new android.view.ViewGroup$MarginLayoutParams     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            java.lang.String r46 = "titletextwidth"
            int r45 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r41
            r1 = r45
            r2 = r33
            r0.<init>(r1, r2)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            int r34 = r38 + 20
            int r45 = r36 - r33
            int r45 = r45 / 2
            int r45 = r45 + r39
            int r35 = r45 + 4
            r45 = 0
            r46 = 0
            r0 = r41
            r1 = r34
            r2 = r35
            r3 = r45
            r4 = r46
            r0.setMargins(r1, r2, r3, r4)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            android.widget.RelativeLayout$LayoutParams r40 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r40.<init>(r41)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            java.lang.String r46 = "footerx"
            int r23 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            java.lang.String r46 = "footery"
            int r24 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            java.lang.String r46 = "footerheight"
            int r21 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            android.view.ViewGroup$MarginLayoutParams r17 = new android.view.ViewGroup$MarginLayoutParams     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            java.lang.String r46 = "footerwidth"
            int r45 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r17
            r1 = r45
            r2 = r21
            r0.<init>(r1, r2)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = 0
            r46 = 0
            r0 = r17
            r1 = r23
            r2 = r24
            r3 = r45
            r4 = r46
            r0.setMargins(r1, r2, r3, r4)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            android.widget.RelativeLayout$LayoutParams r16 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r16.<init>(r17)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            android.view.View r45 = new android.view.View     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            android.app.Activity r0 = r0.activity     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r46 = r0
            r45.<init>(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r45
            r1 = r50
            r1.footer = r0     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            java.lang.String r46 = "footercolor"
            java.lang.String r15 = r45.getString(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            java.lang.String r45 = ""
            r0 = r45
            boolean r45 = r15.equals(r0)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            if (r45 != 0) goto L_0x0433
            if (r15 != 0) goto L_0x0435
        L_0x0433:
            java.lang.String r15 = "#E6E6E6"
        L_0x0435:
            r0 = r50
            android.view.View r0 = r0.footer     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            int r46 = android.graphics.Color.parseColor(r15)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45.setBackgroundColor(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            android.widget.TextView r45 = new android.widget.TextView     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            android.app.Activity r0 = r0.activity     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r46 = r0
            r45.<init>(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r45
            r1 = r50
            r1.footerTitle = r0     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            android.widget.TextView r0 = r0.footerTitle     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r46 = r0
            java.lang.String r47 = "footertext"
            java.lang.String r46 = r46.getString(r47)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45.setText(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            android.widget.TextView r0 = r0.footerTitle     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            r46 = 1092616192(0x41200000, float:10.0)
            r45.setTextSize(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            java.lang.String r46 = "footertextheight"
            int r22 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            java.lang.String r46 = "footertextcolor"
            java.lang.String r20 = r45.getString(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            java.lang.String r45 = ""
            r0 = r20
            r1 = r45
            boolean r45 = r0.equals(r1)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            if (r45 != 0) goto L_0x0499
            if (r20 != 0) goto L_0x049b
        L_0x0499:
            java.lang.String r20 = "#E6E6E6"
        L_0x049b:
            r0 = r50
            android.widget.TextView r0 = r0.footerTitle     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            int r46 = android.graphics.Color.parseColor(r20)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45.setTextColor(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            android.view.ViewGroup$MarginLayoutParams r28 = new android.view.ViewGroup$MarginLayoutParams     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            java.lang.String r46 = "footertextwidth"
            int r45 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r28
            r1 = r45
            r2 = r22
            r0.<init>(r1, r2)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            int r18 = r23 + 20
            int r45 = r21 - r22
            int r45 = r45 / 2
            int r19 = r24 + r45
            r45 = 0
            r46 = 0
            r0 = r28
            r1 = r18
            r2 = r19
            r3 = r45
            r4 = r46
            r0.setMargins(r1, r2, r3, r4)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            android.widget.RelativeLayout$LayoutParams r27 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r27.<init>(r28)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            int r45 = r36 + -5
            r46 = 0
            int r8 = java.lang.Math.max(r45, r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            int r45 = r8 / 2
            r0 = r45
            float r7 = (float) r0     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            android.widget.Button r45 = new android.widget.Button     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            android.app.Activity r0 = r0.activity     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r46 = r0
            r45.<init>(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r45
            r1 = r50
            r1.closeBtn = r0     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            android.widget.Button r0 = r0.closeBtn     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            java.lang.String r46 = "X"
            r45.setText(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            android.widget.Button r0 = r0.closeBtn     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            r0 = r45
            r0.setTextSize(r7)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            android.widget.Button r0 = r0.closeBtn     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            int r46 = android.graphics.Color.parseColor(r9)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45.setTextColor(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            android.widget.Button r0 = r0.closeBtn     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            r46 = 0
            r47 = 0
            r48 = 0
            r49 = 0
            r45.setPadding(r46, r47, r48, r49)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            android.widget.Button r0 = r0.closeBtn     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            int r46 = android.graphics.Color.parseColor(r12)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45.setBackgroundColor(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            android.widget.Button r0 = r0.closeBtn     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            com.Leadbolt.AdController$5 r46 = new com.Leadbolt.AdController$5     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r46
            r1 = r50
            r0.<init>()     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45.setOnClickListener(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            android.view.ViewGroup$MarginLayoutParams r45 = new android.view.ViewGroup$MarginLayoutParams     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r46 = 55
            r0 = r45
            r1 = r46
            r0.<init>(r1, r8)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r45
            r1 = r50
            r1.mlpC = r0     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            int r45 = r37 + r38
            int r45 = r45 + -55
            int r10 = r45 + -5
            int r45 = r36 - r8
            int r45 = r45 / 2
            int r45 = r45 + r39
            int r11 = r45 + 2
            r0 = r50
            android.view.ViewGroup$MarginLayoutParams r0 = r0.mlpC     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            r46 = 0
            r47 = 0
            r0 = r45
            r1 = r46
            r2 = r47
            r0.setMargins(r10, r11, r1, r2)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            android.widget.RelativeLayout$LayoutParams r45 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            android.view.ViewGroup$MarginLayoutParams r0 = r0.mlpC     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r46 = r0
            r45.<init>(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r45
            r1 = r50
            r1.lpC = r0     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            android.webkit.WebView r0 = r0.mainView     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            if (r45 == 0) goto L_0x05c9
            r0 = r50
            android.webkit.WebView r0 = r0.mainView     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            android.view.ViewParent r45 = r45.getParent()     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            android.view.ViewGroup r45 = (android.view.ViewGroup) r45     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r45
            r1 = r50
            r1.mainViewParent = r0     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            android.view.ViewGroup r0 = r0.mainViewParent     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            r0 = r50
            android.webkit.WebView r0 = r0.mainView     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r46 = r0
            r45.removeView(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            r0 = r50
            android.webkit.WebView r0 = r0.mainView     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r46 = r0
            r45.addView(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
        L_0x05c9:
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            java.lang.String r46 = "maskvisible"
            int r45 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r46 = 1
            r0 = r45
            r1 = r46
            if (r0 != r1) goto L_0x060c
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            r0 = r50
            android.view.View r0 = r0.mask     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r46 = r0
            r0 = r45
            r1 = r46
            r2 = r29
            r0.addView(r1, r2)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            android.webkit.WebView r0 = r0.mainView     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            if (r45 == 0) goto L_0x060c
            r0 = r50
            android.webkit.WebView r0 = r0.mainView     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            com.Leadbolt.AdController$6 r46 = new com.Leadbolt.AdController$6     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r46
            r1 = r50
            r0.<init>()     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45.setOnTouchListener(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
        L_0x060c:
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            r0 = r50
            android.webkit.WebView r0 = r0.webview     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r46 = r0
            r0 = r45
            r1 = r46
            r2 = r42
            r0.addView(r1, r2)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            java.lang.String r46 = "titlevisible"
            int r45 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r46 = 1
            r0 = r45
            r1 = r46
            if (r0 != r1) goto L_0x069c
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            r0 = r50
            android.view.View r0 = r0.toolbar     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r46 = r0
            r0 = r45
            r1 = r46
            r2 = r31
            r0.addView(r1, r2)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            java.lang.String r46 = "titletext"
            java.lang.String r45 = r45.getString(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            java.lang.String r46 = ""
            boolean r45 = r45.equals(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            if (r45 != 0) goto L_0x0673
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            r0 = r50
            android.widget.TextView r0 = r0.title     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r46 = r0
            r0 = r45
            r1 = r46
            r2 = r40
            r0.addView(r1, r2)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
        L_0x0673:
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            java.lang.String r46 = "showclose"
            int r45 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r46 = 1
            r0 = r45
            r1 = r46
            if (r0 != r1) goto L_0x069c
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            r0 = r50
            android.widget.Button r0 = r0.closeBtn     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r46 = r0
            r0 = r50
            android.widget.RelativeLayout$LayoutParams r0 = r0.lpC     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r47 = r0
            r45.addView(r46, r47)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
        L_0x069c:
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            java.lang.String r46 = "footervisible"
            int r45 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r46 = 1
            r0 = r45
            r1 = r46
            if (r0 != r1) goto L_0x06ee
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            r0 = r50
            android.view.View r0 = r0.footer     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r46 = r0
            r0 = r45
            r1 = r46
            r2 = r16
            r0.addView(r1, r2)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            java.lang.String r46 = "footertext"
            java.lang.String r45 = r45.getString(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            java.lang.String r46 = ""
            boolean r45 = r45.equals(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            if (r45 != 0) goto L_0x06ee
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            r0 = r50
            android.widget.TextView r0 = r0.footerTitle     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r46 = r0
            r0 = r45
            r1 = r46
            r2 = r27
            r0.addView(r1, r2)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
        L_0x06ee:
            java.lang.StringBuilder r45 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0843, JSONException -> 0x0851 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0843, JSONException -> 0x0851 }
            r46 = r0
            java.lang.String r47 = "adurl"
            java.lang.String r46 = r46.getString(r47)     // Catch:{ Exception -> 0x0843, JSONException -> 0x0851 }
            java.lang.String r46 = java.lang.String.valueOf(r46)     // Catch:{ Exception -> 0x0843, JSONException -> 0x0851 }
            r45.<init>(r46)     // Catch:{ Exception -> 0x0843, JSONException -> 0x0851 }
            r0 = r50
            java.lang.String r0 = r0.sectionid     // Catch:{ Exception -> 0x0843, JSONException -> 0x0851 }
            r46 = r0
            java.lang.StringBuilder r45 = r45.append(r46)     // Catch:{ Exception -> 0x0843, JSONException -> 0x0851 }
            java.lang.String r45 = r45.toString()     // Catch:{ Exception -> 0x0843, JSONException -> 0x0851 }
            r0 = r45
            r1 = r50
            r1.loadUrl = r0     // Catch:{ Exception -> 0x0843, JSONException -> 0x0851 }
            r0 = r50
            android.webkit.WebView r0 = r0.webview     // Catch:{ Exception -> 0x0843, JSONException -> 0x0851 }
            r45 = r0
            r0 = r50
            java.lang.String r0 = r0.loadUrl     // Catch:{ Exception -> 0x0843, JSONException -> 0x0851 }
            r46 = r0
            r45.loadUrl(r46)     // Catch:{ Exception -> 0x0843, JSONException -> 0x0851 }
            r45 = 1
            r0 = r45
            r1 = r50
            r1.loading = r0     // Catch:{ Exception -> 0x0843, JSONException -> 0x0851 }
        L_0x072e:
            android.view.ViewGroup$MarginLayoutParams r25 = new android.view.ViewGroup$MarginLayoutParams     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            int r0 = r0.sWidth     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            r0 = r50
            int r0 = r0.sHeight     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r46 = r0
            r0 = r25
            r1 = r45
            r2 = r46
            r0.<init>(r1, r2)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = 0
            r46 = 0
            r47 = 0
            r48 = 0
            r0 = r25
            r1 = r45
            r2 = r46
            r3 = r47
            r4 = r48
            r0.setMargins(r1, r2, r3, r4)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            android.widget.RelativeLayout$LayoutParams r26 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r26
            r1 = r25
            r0.<init>(r1)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            android.app.Activity r0 = r0.activity     // Catch:{ Exception -> 0x0910, JSONException -> 0x0851 }
            r45 = r0
            r0 = r50
            android.widget.RelativeLayout r0 = r0.layout     // Catch:{ Exception -> 0x0910, JSONException -> 0x0851 }
            r46 = r0
            r0 = r45
            r1 = r46
            r2 = r26
            r0.addContentView(r1, r2)     // Catch:{ Exception -> 0x0910, JSONException -> 0x0851 }
        L_0x0778:
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            java.lang.String r46 = "pollenable"
            int r45 = r45.getInt(r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r46 = 1
            r0 = r45
            r1 = r46
            if (r0 != r1) goto L_0x086f
            r0 = r50
            boolean r0 = r0.pollingInitialized     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            if (r45 != 0) goto L_0x086f
            r0 = r50
            int r0 = r0.pollCount     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            r0 = r50
            int r0 = r0.pollMax     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r46 = r0
            r0 = r45
            r1 = r46
            if (r0 > r1) goto L_0x086f
            r0 = r50
            int r0 = r0.pollCount     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            r0 = r50
            int r0 = r0.pollManual     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r46 = r0
            r0 = r45
            r1 = r46
            if (r0 >= r1) goto L_0x086f
            r45 = 1
            r0 = r45
            r1 = r50
            r1.pollingInitialized = r0     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            com.Leadbolt.AdController$OfferPolling r45 = new com.Leadbolt.AdController$OfferPolling     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r45
            r1 = r50
            r0.<init>()     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r45
            r1 = r50
            r1.adPolling = r0     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            android.os.Handler r45 = new android.os.Handler     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45.<init>()     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r45
            r1 = r50
            r1.pollingHandler = r0     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            java.lang.String r45 = "LBAdController"
            java.lang.StringBuilder r46 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0828, JSONException -> 0x0851 }
            java.lang.String r47 = "Polling initialized every "
            r46.<init>(r47)     // Catch:{ Exception -> 0x0828, JSONException -> 0x0851 }
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0828, JSONException -> 0x0851 }
            r47 = r0
            java.lang.String r48 = "pollinterval"
            int r47 = r47.getInt(r48)     // Catch:{ Exception -> 0x0828, JSONException -> 0x0851 }
            java.lang.StringBuilder r46 = r46.append(r47)     // Catch:{ Exception -> 0x0828, JSONException -> 0x0851 }
            java.lang.String r47 = "s"
            java.lang.StringBuilder r46 = r46.append(r47)     // Catch:{ Exception -> 0x0828, JSONException -> 0x0851 }
            java.lang.String r46 = r46.toString()     // Catch:{ Exception -> 0x0828, JSONException -> 0x0851 }
            com.Leadbolt.AdLog.d(r45, r46)     // Catch:{ Exception -> 0x0828, JSONException -> 0x0851 }
            r0 = r50
            android.os.Handler r0 = r0.pollingHandler     // Catch:{ Exception -> 0x0828, JSONException -> 0x0851 }
            r45 = r0
            r0 = r50
            com.Leadbolt.AdController$OfferPolling r0 = r0.adPolling     // Catch:{ Exception -> 0x0828, JSONException -> 0x0851 }
            r46 = r0
            r0 = r50
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0828, JSONException -> 0x0851 }
            r47 = r0
            java.lang.String r48 = "pollinterval"
            int r47 = r47.getInt(r48)     // Catch:{ Exception -> 0x0828, JSONException -> 0x0851 }
            r0 = r47
            int r0 = r0 * 1000
            r47 = r0
            r0 = r47
            long r0 = (long) r0     // Catch:{ Exception -> 0x0828, JSONException -> 0x0851 }
            r47 = r0
            r45.postDelayed(r46, r47)     // Catch:{ Exception -> 0x0828, JSONException -> 0x0851 }
            goto L_0x000f
        L_0x0828:
            r45 = move-exception
            goto L_0x000f
        L_0x082b:
            java.lang.String r45 = "Bottom"
            r0 = r45
            boolean r45 = r13.equals(r0)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            if (r45 == 0) goto L_0x025b
            r0 = r50
            int r0 = r0.additionalDockingMargin     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            int r44 = r44 - r45
            if (r44 >= 0) goto L_0x025b
            r44 = 0
            goto L_0x025b
        L_0x0843:
            r14 = move-exception
            r50.closeUnlocker()     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = 0
            r0 = r45
            r1 = r50
            r1.loading = r0     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            goto L_0x072e
        L_0x0851:
            r14 = move-exception
            r50.closeUnlocker()
            java.lang.String r45 = "AdController"
            java.lang.StringBuilder r46 = new java.lang.StringBuilder
            java.lang.String r47 = "JSONException - "
            r46.<init>(r47)
            java.lang.String r47 = r14.getMessage()
            java.lang.StringBuilder r46 = r46.append(r47)
            java.lang.String r46 = r46.toString()
            android.util.Log.d(r45, r46)
            goto L_0x000f
        L_0x086f:
            r0 = r50
            int r0 = r0.pollCount     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            r0 = r50
            int r0 = r0.pollMax     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r46 = r0
            r0 = r45
            r1 = r46
            if (r0 <= r1) goto L_0x000f
            r0 = r50
            int r0 = r0.pollCount     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            r0 = r50
            int r0 = r0.pollManual     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r46 = r0
            r0 = r45
            r1 = r46
            if (r0 < r1) goto L_0x000f
            r45 = 0
            r0 = r45
            r1 = r50
            r1.pollingInitialized = r0     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r50.showManualPoll()     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            goto L_0x000f
        L_0x08a0:
            r14 = move-exception
            r50.closeUnlocker()
            java.lang.String r45 = "LBAdController"
            r0 = r45
            com.Leadbolt.AdLog.printStackTrace(r0, r14)
            java.lang.String r45 = "AdController"
            java.lang.StringBuilder r46 = new java.lang.StringBuilder
            java.lang.String r47 = "Exception - "
            r46.<init>(r47)
            java.lang.String r47 = r14.getMessage()
            java.lang.StringBuilder r46 = r46.append(r47)
            java.lang.String r46 = r46.toString()
            android.util.Log.d(r45, r46)
            goto L_0x000f
        L_0x08c5:
            r45 = 0
            r0 = r45
            r1 = r50
            r1.loading = r0     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r50.closeUnlocker()     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            goto L_0x000f
        L_0x08d2:
            r45 = 0
            r0 = r45
            r1 = r50
            r1.loading = r0     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r50.closeUnlocker()     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r0 = r50
            com.Leadbolt.AdListener r0 = r0.listener     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            r45 = r0
            if (r45 == 0) goto L_0x000f
            java.lang.String r45 = "LBAdController"
            java.lang.String r46 = "onAdFailed triggered"
            com.Leadbolt.AdLog.i(r45, r46)     // Catch:{ Exception -> 0x08ff, JSONException -> 0x0851 }
            r0 = r50
            com.Leadbolt.AdListener r0 = r0.listener     // Catch:{ Exception -> 0x08ff, JSONException -> 0x0851 }
            r45 = r0
            r45.onAdFailed()     // Catch:{ Exception -> 0x08ff, JSONException -> 0x0851 }
            r45 = 1
            r0 = r45
            r1 = r50
            r1.adLoaded = r0     // Catch:{ Exception -> 0x08ff, JSONException -> 0x0851 }
            goto L_0x000f
        L_0x08ff:
            r14 = move-exception
            java.lang.String r45 = "LBAdController"
            java.lang.String r46 = "Error while calling onAdFailed"
            com.Leadbolt.AdLog.i(r45, r46)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            java.lang.String r45 = "LBAdController"
            r0 = r45
            com.Leadbolt.AdLog.printStackTrace(r0, r14)     // Catch:{ JSONException -> 0x0851, Exception -> 0x08a0 }
            goto L_0x000f
        L_0x0910:
            r45 = move-exception
            goto L_0x0778
        L_0x0913:
            r45 = move-exception
            goto L_0x0209
        L_0x0916:
            r45 = move-exception
            goto L_0x01c6
        L_0x0919:
            r45 = move-exception
            goto L_0x015f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.Leadbolt.AdController.displayAd():void");
    }

    /* access modifiers changed from: private */
    public void linkClicked() {
        int fTitlex;
        AdLog.i(LB_LOG, "linkClicked called");
        if (!this.linkClicked) {
            AdLog.i(LB_LOG, "Loading window...");
            this.homeLoaded = false;
            this.linkClicked = true;
            if (!this.loading) {
                AdLog.i(LB_LOG, "remove the views if required first...");
                try {
                    this.layout.removeView(this.webview);
                    this.layout.removeView(this.toolbar);
                    this.layout.removeView(this.title);
                    this.layout.removeView(this.footer);
                    this.layout.removeView(this.footerTitle);
                    this.layout.removeView(this.backBtn);
                    this.layout.removeView(this.fwdBtn);
                    this.layout.removeView(this.closeBtn);
                    this.layout.removeView(this.homeBtn);
                    this.layout.removeView(this.refreshBtn);
                    if (this.pollBtn != null) {
                        this.layout.removeAllViews();
                    }
                } catch (Exception e) {
                }
                int windowx = this.results.getInt("clickwindowx");
                int windowy = this.results.getInt("clickwindowy");
                ViewGroup.MarginLayoutParams marginLayoutParams = new ViewGroup.MarginLayoutParams(this.results.getInt("clickwindowwidth"), this.results.getInt("clickwindowheight"));
                marginLayoutParams.setMargins(windowx, windowy, 0, 0);
                RelativeLayout.LayoutParams wL = new RelativeLayout.LayoutParams(marginLayoutParams);
                int toolbarwidth = this.results.getInt("clicktitlewidth");
                int toolbarheight = this.results.getInt("clicktitleheight");
                int toolbarx = this.results.getInt("clicktitlex");
                int toolbary = this.results.getInt("clicktitley");
                ViewGroup.MarginLayoutParams marginLayoutParams2 = new ViewGroup.MarginLayoutParams(toolbarwidth, toolbarheight);
                marginLayoutParams2.setMargins(toolbarx, toolbary, 0, 0);
                RelativeLayout.LayoutParams tL = new RelativeLayout.LayoutParams(marginLayoutParams2);
                try {
                    this.toolbar.invalidate();
                } catch (Exception e2) {
                }
                try {
                    this.toolbar = new View(this.activity);
                    String clr = this.results.getString("clicktitlecolor");
                    if (clr.equals("") || clr == null) {
                        clr = "#E6E6E6";
                    }
                    this.toolbar.setBackgroundColor(Color.parseColor(clr));
                    try {
                        this.title.invalidate();
                    } catch (Exception e3) {
                    }
                    this.title = new TextView(this.activity);
                    this.title.setText(this.results.getString("clicktitletext"));
                    String clk = this.results.getString("clicktitletextcolor");
                    if (clk.equals("") || clk == null) {
                        clk = "#000000";
                    }
                    this.title.setTextColor(Color.parseColor(clk));
                    ViewGroup.MarginLayoutParams marginLayoutParams3 = new ViewGroup.MarginLayoutParams(this.results.getInt("clicktitletextwidth"), toolbarheight - 2);
                    marginLayoutParams3.setMargins(toolbarx + 20, toolbary + 8, 0, 0);
                    RelativeLayout.LayoutParams tvl = new RelativeLayout.LayoutParams(marginLayoutParams3);
                    int footerx = this.results.getInt("clickfooterx");
                    int footery = this.results.getInt("clickfootery");
                    int footerheight = this.results.getInt("clickfooterheight");
                    ViewGroup.MarginLayoutParams marginLayoutParams4 = new ViewGroup.MarginLayoutParams(this.results.getInt("clickfooterwidth"), footerheight);
                    marginLayoutParams4.setMargins(footerx, footery, 0, 0);
                    RelativeLayout.LayoutParams fL = new RelativeLayout.LayoutParams(marginLayoutParams4);
                    this.footer = new View(this.activity);
                    String fClr = this.results.getString("clickfootercolor");
                    if (fClr.equals("") || fClr == null) {
                        fClr = "#E6E6E6";
                    }
                    this.footer.setBackgroundColor(Color.parseColor(fClr));
                    this.footerTitle = new TextView(this.activity);
                    this.footerTitle.setText(this.results.getString("clickfootertext"));
                    this.footerTitle.setTextSize(10.0f);
                    String fClk = this.results.getString("clickfootertextcolor");
                    if (fClk.equals("") || fClk == null) {
                        fClk = "#000000";
                    }
                    this.footerTitle.setTextColor(Color.parseColor(fClk));
                    ViewGroup.MarginLayoutParams marginLayoutParams5 = new ViewGroup.MarginLayoutParams(this.results.getInt("clickfootertextwidth"), this.results.getInt("clickfootertextheight"));
                    int fTitlex2 = footerx;
                    if (this.results.getInt("shownavigation") == 1) {
                        fTitlex = fTitlex2 + 70;
                    } else {
                        fTitlex = fTitlex2 + 20;
                    }
                    marginLayoutParams5.setMargins(fTitlex, footery + 5, 0, 0);
                    RelativeLayout.LayoutParams fvl = new RelativeLayout.LayoutParams(marginLayoutParams5);
                    int titleBtnHeight = Math.max(toolbarheight - 5, 0);
                    float titleBtnFont = (float) (titleBtnHeight / 2);
                    if (titleBtnFont > 10.0f) {
                        titleBtnFont = 10.0f;
                    }
                    int navBtnHeight = Math.max(footerheight - 5, 0);
                    float navBtnFont = (float) (navBtnHeight / 2);
                    if (navBtnFont > 10.0f) {
                        navBtnFont = 10.0f;
                    }
                    if (this.homeBtn != null) {
                        this.homeBtn.invalidate();
                    }
                    this.homeBtn = new Button(this.activity);
                    this.homeBtn.setText("Back");
                    this.homeBtn.setTextSize(navBtnFont);
                    this.homeBtn.setTextColor(Color.parseColor(clk));
                    this.homeBtn.setPadding(0, 0, 0, 0);
                    this.homeBtn.setBackgroundColor(Color.parseColor(fClr));
                    this.homeBtn.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            AdController.this.loadAd();
                        }
                    });
                    if (this.closeBtn != null) {
                        this.closeBtn.invalidate();
                    }
                    this.closeBtn = new Button(this.activity);
                    this.closeBtn.setText("X");
                    this.closeBtn.setTextSize(titleBtnFont);
                    this.closeBtn.setTextColor(Color.parseColor(clk));
                    this.closeBtn.setPadding(0, 0, 0, 0);
                    this.closeBtn.setBackgroundColor(Color.parseColor(clr));
                    this.closeBtn.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            AdController.this.closeUnlocker();
                            if (AdController.this.listener != null) {
                                try {
                                    AdLog.i(AdController.LB_LOG, "onAdClosed triggered");
                                    AdController.this.listener.onAdClosed();
                                } catch (Exception e) {
                                    AdLog.e(AdController.LB_LOG, "error when onAdClosed triggered");
                                    AdLog.printStackTrace(AdController.LB_LOG, e);
                                }
                            }
                        }
                    });
                    this.fwdBtn = new Button(this.activity);
                    this.fwdBtn.setText(">");
                    this.fwdBtn.setTextSize(navBtnFont);
                    this.fwdBtn.setTextColor(Color.parseColor(clk));
                    this.fwdBtn.setPadding(0, 0, 0, 0);
                    this.fwdBtn.setBackgroundColor(Color.parseColor(fClr));
                    this.fwdBtn.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            AdController.this.goForward();
                        }
                    });
                    if (this.backBtn != null) {
                        this.backBtn.invalidate();
                    }
                    this.backBtn = new Button(this.activity);
                    this.backBtn.setText("<");
                    this.backBtn.setTextSize(navBtnFont);
                    this.backBtn.setTextColor(Color.parseColor(clk));
                    this.backBtn.setPadding(0, 0, 0, 0);
                    this.backBtn.setBackgroundColor(Color.parseColor(fClr));
                    this.backBtn.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            AdController.this.goBack();
                        }
                    });
                    ViewGroup.MarginLayoutParams marginLayoutParams6 = new ViewGroup.MarginLayoutParams(30, navBtnHeight);
                    int btny = ((this.results.getInt("clickfooterheight") - navBtnHeight) / 2) + footery + 3;
                    marginLayoutParams6.setMargins(footerx + 5, btny, 0, 0);
                    RelativeLayout.LayoutParams bB = new RelativeLayout.LayoutParams(marginLayoutParams6);
                    marginLayoutParams6.setMargins(footerx + 5 + 30, btny, 0, 0);
                    RelativeLayout.LayoutParams fB = new RelativeLayout.LayoutParams(marginLayoutParams6);
                    this.layout.addView(this.webview, wL);
                    if (this.listener != null) {
                        try {
                            AdLog.i(LB_LOG, "onAdClicked triggered");
                            this.listener.onAdClicked();
                        } catch (Exception e4) {
                            AdLog.e(LB_LOG, "error when onAdClicked triggered");
                            AdLog.printStackTrace(LB_LOG, e4);
                        }
                    }
                    if (this.results.getInt("clicktitlevisible") == 1) {
                        this.layout.addView(this.toolbar, tL);
                        if (!this.results.getString("clicktitletext").equals("")) {
                            this.layout.addView(this.title, tvl);
                        }
                        if (this.results.getInt("showclose") == 1) {
                            this.mlpC = new ViewGroup.MarginLayoutParams(55, titleBtnHeight);
                            this.mlpC.setMargins(((toolbarwidth + toolbarx) - 55) - 5, ((toolbarheight - titleBtnHeight) / 2) + toolbary + 2, 0, 0);
                            this.lpC = new RelativeLayout.LayoutParams(this.mlpC);
                            this.layout.addView(this.closeBtn, this.lpC);
                        }
                    }
                    if (this.results.getInt("clickfootervisible") == 1) {
                        this.layout.addView(this.footer, fL);
                        if (!this.results.getString("clickfootertext").equals("")) {
                            this.layout.addView(this.footerTitle, fvl);
                        }
                        if (this.results.getInt("shownavigation") == 1) {
                            this.layout.addView(this.backBtn, bB);
                            this.layout.addView(this.fwdBtn, fB);
                        }
                        if (this.results.getInt("showback") == 1) {
                            ViewGroup.MarginLayoutParams marginLayoutParams7 = new ViewGroup.MarginLayoutParams(55, navBtnHeight);
                            marginLayoutParams7.setMargins(((this.results.getInt("clickfooterwidth") + footerx) - 55) - 5, ((footerheight - navBtnHeight) / 2) + footery + 2, 0, 0);
                            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(marginLayoutParams7);
                            this.layout.addView(this.homeBtn, layoutParams);
                        }
                    }
                    if (this.results.get("useclickwindow").equals("1")) {
                        try {
                            this.loadUrl = String.valueOf(this.results.getString("adurl")) + this.sectionid;
                            this.webview.loadUrl(this.loadUrl);
                            this.loading = true;
                        } catch (Exception e5) {
                            closeUnlocker();
                            this.loading = false;
                        }
                        try {
                            ViewGroup.MarginLayoutParams marginLayoutParams8 = new ViewGroup.MarginLayoutParams(this.sWidth, this.sHeight);
                            marginLayoutParams8.setMargins(0, 0, 0, 0);
                            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(marginLayoutParams8);
                            try {
                                this.activity.addContentView(this.layout, layoutParams2);
                            } catch (Exception e6) {
                            }
                        } catch (Exception e7) {
                        }
                    }
                    AdLog.i(LB_LOG, "pE = " + this.results.getInt("pollenable") + ", pI = " + this.pollingInitialized + ", pC = " + this.pollCount + ", pM = " + this.pollMax + ", pMl = " + this.pollManual);
                    boolean iP = false;
                    if (this.results.getInt("pollenable") != 1 || this.pollingInitialized || this.pollCount <= 0) {
                        if (this.pollCount > this.pollMax || this.pollCount >= this.pollManual) {
                            if (this.pollCount > this.pollMax && this.pollCount >= this.pollManual) {
                                iP = false;
                            }
                        } else {
                            iP = true;
                        }
                    } else {
                        iP = true;
                    }
                    if (iP) {
                        AdLog.i(LB_LOG, "Polling to be initialized in linkClicked");
                        this.pollingInitialized = true;
                        this.adPolling = new OfferPolling();
                        this.pollingHandler = new Handler();
                        try {
                            AdLog.d(LB_LOG, "Polling initialized every " + this.results.getInt("pollinterval") + "s");
                            this.pollingHandler.postDelayed(this.adPolling, (long) (this.results.getInt("pollinterval") * 1000));
                        } catch (Exception e8) {
                        }
                    } else {
                        AdLog.i(LB_LOG, "Manual Polling in linkClicked");
                        this.pollingInitialized = false;
                        showManualPoll();
                    }
                } catch (JSONException ex) {
                    AdLog.printStackTrace(LB_LOG, ex);
                    AdLog.e(LB_LOG, "JSON Exception - " + ex.getMessage());
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void showCloseButton() {
        try {
            this.layout.removeView(this.closeBtn);
        } catch (Exception e) {
        }
        int titleBtnHeight = 0;
        String clr = "#E6E6E6";
        String clk = "#000000";
        if (this.homeLoaded) {
            try {
                titleBtnHeight = Math.max(this.results.getInt("titlewidth") - 5, 0);
                clr = this.results.getString("titlecolor");
                clk = this.results.getString("titletextcolor");
            } catch (Exception e2) {
            }
        } else if (this.linkClicked) {
            try {
                titleBtnHeight = Math.max(this.results.getInt("clicktitlewidth") - 5, 0);
                clr = this.results.getString("clicktitlecolor");
                clk = this.results.getString("clicktitletextcolor");
            } catch (Exception e3) {
            }
        }
        float titleBtnFont = (float) (titleBtnHeight / 2);
        if (titleBtnFont > 10.0f) {
            titleBtnFont = 10.0f;
        }
        if (this.closeBtn != null) {
            this.closeBtn.invalidate();
        }
        this.closeBtn = new Button(this.activity);
        this.closeBtn.setText("X");
        this.closeBtn.setTextSize(titleBtnFont);
        this.closeBtn.setTextColor(Color.parseColor(clk));
        this.closeBtn.setPadding(0, 0, 0, 0);
        this.closeBtn.setBackgroundColor(Color.parseColor(clr));
        this.closeBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AdController.this.closeUnlocker();
                if (AdController.this.listener != null) {
                    try {
                        AdLog.i(AdController.LB_LOG, "onAdClosed triggered");
                        AdController.this.listener.onAdClosed();
                    } catch (Exception e) {
                        AdLog.e(AdController.LB_LOG, "error when onAdClosed triggered");
                        AdLog.printStackTrace(AdController.LB_LOG, e);
                    }
                }
            }
        });
        this.mlpC = new ViewGroup.MarginLayoutParams(55, titleBtnHeight);
        if (this.homeLoaded) {
            try {
                this.mlpC.setMargins(((this.results.getInt("titlewidth") + this.results.getInt("titlex")) - 55) - 5, this.results.getInt("titley") + (Math.round((float) (this.results.getInt("titleheight") - titleBtnHeight)) / 2) + 2, 0, 0);
                this.lpC = new RelativeLayout.LayoutParams(this.mlpC);
            } catch (JSONException e4) {
            }
        } else if (this.linkClicked) {
            try {
                this.mlpC.setMargins(((this.results.getInt("clicktitlewidth") + this.results.getInt("clicktitlex")) - 55) - 5, this.results.getInt("clicktitley") + Math.round((float) ((this.results.getInt("clicktitleheight") - titleBtnHeight) / 2)) + 2, 0, 0);
            } catch (JSONException e5) {
            }
        }
        try {
            this.lpC = new RelativeLayout.LayoutParams(this.mlpC);
            if ((this.homeLoaded && this.results.getInt("titlevisible") == 1) || (this.linkClicked && this.results.getInt("clicktitlevisible") == 1)) {
                this.layout.addView(this.closeBtn, this.lpC);
            }
        } catch (Exception e6) {
        }
    }

    /* access modifiers changed from: private */
    public void showManualPoll() {
        this.pollingInitialized = false;
        if (this.adPolling != null) {
            this.adPolling.cancel();
        }
        this.pollBtn = new Button(this.activity);
        this.pollBtn.setText("Refresh");
        int pollBtnHeight = 0;
        String clr = "#E6E6E6";
        String clk = "#000000";
        if (this.linkClicked) {
            try {
                pollBtnHeight = Math.max(this.results.getInt("clickfooterheight") - 5, 0);
                clr = this.results.getString("clickfootercolor");
                clk = this.results.getString("clicktitletextcolor");
            } catch (Exception e) {
            }
        }
        float pollBtnFont = (float) (pollBtnHeight / 2);
        if (pollBtnFont > 10.0f) {
            pollBtnFont = 10.0f;
        }
        this.pollBtn.setPadding(0, 0, 0, 0);
        this.pollBtn.setTextSize(pollBtnFont);
        this.pollBtn.setTextColor(Color.parseColor(clk));
        this.pollBtn.setBackgroundColor(Color.parseColor(clr));
        this.pollBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AdController.this.loadingDialog = ProgressDialog.show(AdController.this.activity, "", "Checking....Please Wait!", true);
                if (AdController.this.adPolling == null) {
                    AdController.this.adPolling = new OfferPolling();
                }
                AdController.this.pollingHandler = new Handler();
                try {
                    AdLog.i(AdController.LB_LOG, "Manually Polling");
                    AdController.this.pollingHandler.post(AdController.this.adPolling);
                } catch (Exception e) {
                    AdLog.e(AdController.LB_LOG, "Error in manual polling - " + e.getMessage());
                    AdLog.printStackTrace(AdController.LB_LOG, e);
                }
            }
        });
        if (this.linkClicked) {
            try {
                int footerx = this.results.getInt("clickfooterx");
                int footery = this.results.getInt("clickfootery");
                int footerheight = this.results.getInt("clickfooterheight");
                ViewGroup.MarginLayoutParams hB = new ViewGroup.MarginLayoutParams(55, pollBtnHeight);
                hB.setMargins((this.results.getInt("clickfooterwidth") + footerx) - 120, ((footerheight - pollBtnHeight) / 2) + footery + 2, 0, 0);
                this.layout.addView(this.pollBtn, new RelativeLayout.LayoutParams(hB));
            } catch (Exception e2) {
                AdLog.e(LB_LOG, "Error (add Manual Poll btn before click): " + e2.getMessage());
                AdLog.printStackTrace(LB_LOG, e2);
            }
        }
    }

    /* access modifiers changed from: private */
    public void closeUnlocker() {
        this.adDestroyed = true;
        if (this.req != null) {
            this.req.cancel(true);
        }
        AdLog.i(LB_LOG, "closeUnlocker called");
        this.homeLoaded = false;
        this.linkClicked = false;
        this.pollingInitialized = false;
        try {
            this.layout.removeAllViews();
            if (this.adPolling != null) {
                this.adPolling.cancel();
            }
            if (this.mainView != null) {
                this.mainView.setOnTouchListener(null);
            }
        } catch (Exception e) {
            AdLog.printStackTrace(LB_LOG, e);
            AdLog.e(LB_LOG, "CloseUnlocker error - " + e.getMessage());
        }
    }

    /* access modifiers changed from: private */
    public void goBack() {
        if (this.webview.canGoBack()) {
            this.webview.goBack();
        }
    }

    /* access modifiers changed from: private */
    public void goForward() {
        if (this.webview.canGoForward()) {
            this.webview.goForward();
        }
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: private */
    /* JADX WARNING: Can't wrap try/catch for region: R(9:153|154|155|156|(3:158|159|160)|161|(3:162|163|(1:167))|168|(3:169|170|(2:172|222)(1:221))) */
    /* JADX WARNING: Code restructure failed: missing block: B:153:0x08ab, code lost:
        r14 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:156:?, code lost:
        com.Leadbolt.AdLog.printStackTrace(com.Leadbolt.AdController.LB_LOG, r14);
        r17 = false;
        r1.results = new org.json.JSONObject();
        r1.initialized = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:157:0x08ce, code lost:
        if (r0.listener != null) goto L_0x08d0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:160:?, code lost:
        com.Leadbolt.AdLog.i(com.Leadbolt.AdController.LB_LOG, "onAdFailed triggered");
        r0.listener.onAdFailed();
        r1.adLoaded = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:163:?, code lost:
        com.Leadbolt.AdLog.d(com.Leadbolt.AdController.LB_LOG, "Results - " + r0.results.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:167:0x091c, code lost:
        r1.initialized = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:168:0x0924, code lost:
        r1.requestInProgress = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:171:0x0932, code lost:
        if (r0.useNotification != false) goto L_0x0934;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:172:0x0934, code lost:
        r12 = r35.edit();
        r12.putLong("SD_NOTIFICATION_REQUESTED_" + r0.sectionid, java.lang.System.currentTimeMillis());
        r12.commit();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:182:0x099b, code lost:
        r53 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:185:?, code lost:
        com.Leadbolt.AdLog.d(com.Leadbolt.AdController.LB_LOG, "Results - " + r0.results.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:189:0x09d0, code lost:
        r1.initialized = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:190:0x09d8, code lost:
        r1.requestInProgress = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:193:0x09e6, code lost:
        if (r0.useNotification != false) goto L_0x09e8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:194:0x09e8, code lost:
        r12 = r35.edit();
        r12.putLong("SD_NOTIFICATION_REQUESTED_" + r0.sectionid, java.lang.System.currentTimeMillis());
        r12.commit();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:195:0x0a0f, code lost:
        throw r53;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:198:0x0a30, code lost:
        r11 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:201:?, code lost:
        com.Leadbolt.AdLog.i(com.Leadbolt.AdController.LB_LOG, "Error while calling onAdFailed");
        com.Leadbolt.AdLog.printStackTrace(com.Leadbolt.AdController.LB_LOG, r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:203:0x0a42, code lost:
        r1.initialized = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:205:0x0a4d, code lost:
        r1.initialized = false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void makeLBRequest() {
        /*
            r60 = this;
            r0 = r60
            boolean r0 = r0.requestInProgress
            r53 = r0
            if (r53 == 0) goto L_0x0009
        L_0x0008:
            return
        L_0x0009:
            r53 = 1
            r0 = r53
            r1 = r60
            r1.requestInProgress = r0
            java.util.Random r36 = new java.util.Random
            r36.<init>()
            r53 = 151(0x97, float:2.12E-43)
            r0 = r36
            r1 = r53
            int r53 = r0.nextInt(r1)
            r0 = r53
            int r0 = r0 + 250
            r37 = r0
            r0 = r37
            long r0 = (long) r0
            r53 = r0
            java.lang.Thread.sleep(r53)     // Catch:{ Exception -> 0x0a61 }
        L_0x002e:
            r0 = r60
            android.app.Activity r0 = r0.activity
            r53 = r0
            if (r53 != 0) goto L_0x0659
            r0 = r60
            android.content.Context r0 = r0.context
            r48 = r0
        L_0x003c:
            java.lang.String r53 = "Preference"
            r54 = 2
            r0 = r48
            r1 = r53
            r2 = r54
            android.content.SharedPreferences r35 = r0.getSharedPreferences(r1, r2)
            r0 = r60
            android.app.Activity r0 = r0.activity
            r53 = r0
            if (r53 == 0) goto L_0x0103
            android.util.DisplayMetrics r10 = new android.util.DisplayMetrics
            r10.<init>()
            r0 = r60
            android.app.Activity r0 = r0.activity
            r53 = r0
            android.view.WindowManager r53 = r53.getWindowManager()
            android.view.Display r53 = r53.getDefaultDisplay()
            r0 = r53
            r0.getMetrics(r10)
            android.graphics.Rect r38 = new android.graphics.Rect
            r38.<init>()
            r0 = r60
            android.app.Activity r0 = r0.activity
            r53 = r0
            android.view.Window r52 = r53.getWindow()
            android.view.View r53 = r52.getDecorView()
            r0 = r53
            r1 = r38
            r0.getWindowVisibleDisplayFrame(r1)
            r0 = r38
            int r0 = r0.top
            r44 = r0
            r53 = 16908290(0x1020002, float:2.3877235E-38)
            android.view.View r53 = r52.findViewById(r53)
            int r7 = r53.getTop()
            r0 = r44
            if (r7 <= r0) goto L_0x0661
            int r46 = r7 - r44
        L_0x009b:
            int r0 = r10.widthPixels
            r53 = r0
            r0 = r53
            r1 = r60
            r1.sWidth = r0
            int r0 = r10.heightPixels
            r53 = r0
            int r53 = r53 - r44
            int r53 = r53 - r46
            r0 = r53
            r1 = r60
            r1.sHeight = r0
            java.lang.String r53 = "LBAdController"
            java.lang.StringBuilder r54 = new java.lang.StringBuilder
            java.lang.String r55 = "Width = "
            r54.<init>(r55)
            r0 = r60
            int r0 = r0.sWidth
            r55 = r0
            java.lang.StringBuilder r54 = r54.append(r55)
            java.lang.String r55 = ", Height = "
            java.lang.StringBuilder r54 = r54.append(r55)
            r0 = r60
            int r0 = r0.sHeight
            r55 = r0
            java.lang.StringBuilder r54 = r54.append(r55)
            java.lang.String r54 = r54.toString()
            com.Leadbolt.AdLog.d(r53, r54)
            java.lang.String r53 = "LBAdController"
            java.lang.StringBuilder r54 = new java.lang.StringBuilder
            java.lang.String r55 = "Status Bar Height = "
            r54.<init>(r55)
            r0 = r54
            r1 = r44
            java.lang.StringBuilder r54 = r0.append(r1)
            java.lang.String r55 = ", TitleBarHeight = "
            java.lang.StringBuilder r54 = r54.append(r55)
            r0 = r54
            r1 = r46
            java.lang.StringBuilder r54 = r0.append(r1)
            java.lang.String r54 = r54.toString()
            com.Leadbolt.AdLog.d(r53, r54)
        L_0x0103:
            org.apache.http.params.BasicHttpParams r33 = new org.apache.http.params.BasicHttpParams
            r33.<init>()
            java.lang.String r53 = "http.protocol.version"
            org.apache.http.HttpVersion r54 = org.apache.http.HttpVersion.HTTP_1_1
            r0 = r33
            r1 = r53
            r2 = r54
            r0.setParameter(r1, r2)
            org.apache.http.impl.client.DefaultHttpClient r18 = new org.apache.http.impl.client.DefaultHttpClient
            r0 = r18
            r1 = r33
            r0.<init>(r1)
            java.util.Calendar r4 = java.util.Calendar.getInstance()
            r0 = r60
            android.app.Activity r0 = r0.activity
            r53 = r0
            if (r53 == 0) goto L_0x0665
            r0 = r60
            android.app.Activity r0 = r0.activity
            r53 = r0
            android.content.Context r53 = r53.getBaseContext()
            java.lang.String r54 = "phone"
            java.lang.Object r53 = r53.getSystemService(r54)
            android.telephony.TelephonyManager r53 = (android.telephony.TelephonyManager) r53
            r0 = r53
            r1 = r60
            r1.tm = r0
        L_0x0142:
            android.content.ContentResolver r53 = r48.getContentResolver()
            java.lang.String r54 = "android_id"
            java.lang.String r8 = android.provider.Settings.Secure.getString(r53, r54)
            r53 = 2
            r0 = r53
            java.lang.String[] r0 = new java.lang.String[r0]
            r50 = r0
            r53 = 0
            java.lang.String r54 = "http://ad.leadboltapps.net"
            r50[r53] = r54
            r53 = 1
            java.lang.String r54 = "http://ad.leadbolt.net"
            r50[r53] = r54
            r17 = 0
            r20 = 0
        L_0x0164:
            r0 = r50
            int r0 = r0.length
            r53 = r0
            r0 = r20
            r1 = r53
            if (r0 >= r1) goto L_0x0008
            if (r17 != 0) goto L_0x0008
            r53 = r50[r20]
            r0 = r53
            r1 = r60
            r1.domain = r0
            java.lang.StringBuilder r53 = new java.lang.StringBuilder
            r0 = r60
            java.lang.String r0 = r0.domain
            r54 = r0
            java.lang.String r54 = java.lang.String.valueOf(r54)
            r53.<init>(r54)
            java.lang.String r54 = "/show_app.conf?"
            java.lang.StringBuilder r53 = r53.append(r54)
            java.lang.String r49 = r53.toString()
            r0 = r60
            boolean r0 = r0.useNotification
            r53 = r0
            if (r53 == 0) goto L_0x067b
            java.lang.StringBuilder r53 = new java.lang.StringBuilder
            r0 = r60
            java.lang.String r0 = r0.domain
            r54 = r0
            java.lang.String r54 = java.lang.String.valueOf(r54)
            r53.<init>(r54)
            java.lang.String r54 = "/show_notification?"
            java.lang.StringBuilder r53 = r53.append(r54)
            java.lang.String r49 = r53.toString()
        L_0x01b3:
            java.lang.StringBuilder r53 = new java.lang.StringBuilder
            java.lang.String r54 = java.lang.String.valueOf(r49)
            r53.<init>(r54)
            java.lang.String r54 = "&section_id="
            java.lang.StringBuilder r53 = r53.append(r54)
            r0 = r60
            java.lang.String r0 = r0.sectionid
            r54 = r0
            java.lang.StringBuilder r53 = r53.append(r54)
            java.lang.String r49 = r53.toString()
            java.lang.StringBuilder r53 = new java.lang.StringBuilder
            java.lang.String r54 = java.lang.String.valueOf(r49)
            r53.<init>(r54)
            java.lang.String r54 = "&app_id="
            java.lang.StringBuilder r53 = r53.append(r54)
            r0 = r60
            java.lang.String r0 = r0.appId
            r54 = r0
            java.lang.StringBuilder r53 = r53.append(r54)
            java.lang.String r49 = r53.toString()
            r0 = r60
            boolean r0 = r0.useNotification
            r53 = r0
            if (r53 == 0) goto L_0x02b2
            java.lang.StringBuilder r53 = new java.lang.StringBuilder
            java.lang.String r54 = "SD_ITERATION_COUNTER_"
            r53.<init>(r54)
            r0 = r60
            java.lang.String r0 = r0.sectionid
            r54 = r0
            java.lang.StringBuilder r53 = r53.append(r54)
            java.lang.String r53 = r53.toString()
            java.lang.String r54 = "0"
            r0 = r35
            r1 = r53
            r2 = r54
            java.lang.String r53 = r0.getString(r1, r2)
            java.lang.Integer r53 = java.lang.Integer.valueOf(r53)
            int r23 = r53.intValue()
            java.lang.StringBuilder r53 = new java.lang.StringBuilder
            java.lang.String r54 = java.lang.String.valueOf(r49)
            r53.<init>(r54)
            java.lang.String r54 = "&iteration_counter="
            java.lang.StringBuilder r53 = r53.append(r54)
            r0 = r53
            r1 = r23
            java.lang.StringBuilder r53 = r0.append(r1)
            java.lang.String r49 = r53.toString()
            java.lang.StringBuilder r53 = new java.lang.StringBuilder
            java.lang.String r54 = java.lang.String.valueOf(r49)
            r53.<init>(r54)
            java.lang.String r54 = "&launch_type="
            java.lang.StringBuilder r53 = r53.append(r54)
            r0 = r60
            java.lang.String r0 = r0.notificationLaunchType
            r54 = r0
            java.lang.StringBuilder r53 = r53.append(r54)
            java.lang.String r49 = r53.toString()
            java.lang.StringBuilder r53 = new java.lang.StringBuilder
            java.lang.String r54 = "SD_NOTIFICATION_FIRED_"
            r53.<init>(r54)
            r0 = r60
            java.lang.String r0 = r0.sectionid
            r54 = r0
            java.lang.StringBuilder r53 = r53.append(r54)
            java.lang.String r53 = r53.toString()
            r54 = -1
            r0 = r35
            r1 = r53
            r2 = r54
            long r15 = r0.getLong(r1, r2)
            r53 = -1
            int r53 = (r15 > r53 ? 1 : (r15 == r53 ? 0 : -1))
            if (r53 != 0) goto L_0x069e
            r22 = -1
        L_0x027f:
            java.lang.StringBuilder r53 = new java.lang.StringBuilder
            java.lang.String r54 = java.lang.String.valueOf(r49)
            r53.<init>(r54)
            java.lang.String r54 = "&notification_fired="
            java.lang.StringBuilder r53 = r53.append(r54)
            r0 = r53
            r1 = r22
            java.lang.StringBuilder r53 = r0.append(r1)
            java.lang.String r49 = r53.toString()
            java.lang.String r53 = "LBAdController"
            java.lang.StringBuilder r54 = new java.lang.StringBuilder
            java.lang.String r55 = "NotificationFired = "
            r54.<init>(r55)
            r0 = r54
            r1 = r22
            java.lang.StringBuilder r54 = r0.append(r1)
            java.lang.String r54 = r54.toString()
            com.Leadbolt.AdLog.d(r53, r54)
        L_0x02b2:
            r0 = r60
            boolean r0 = r0.loadIcon
            r53 = r0
            if (r53 == 0) goto L_0x02f4
            java.lang.StringBuilder r53 = new java.lang.StringBuilder
            java.lang.String r54 = "SD_ICON_DISPLAY_"
            r53.<init>(r54)
            r0 = r60
            java.lang.String r0 = r0.sectionid
            r54 = r0
            java.lang.StringBuilder r53 = r53.append(r54)
            java.lang.String r53 = r53.toString()
            r54 = 0
            r0 = r35
            r1 = r53
            r2 = r54
            int r9 = r0.getInt(r1, r2)
            java.lang.StringBuilder r53 = new java.lang.StringBuilder
            java.lang.String r54 = java.lang.String.valueOf(r49)
            r53.<init>(r54)
            java.lang.String r54 = "&icon_displayed_count="
            java.lang.StringBuilder r53 = r53.append(r54)
            r0 = r53
            java.lang.StringBuilder r53 = r0.append(r9)
            java.lang.String r49 = r53.toString()
        L_0x02f4:
            org.apache.http.client.methods.HttpPost r19 = new org.apache.http.client.methods.HttpPost
            r0 = r19
            r1 = r49
            r0.<init>(r1)
            java.util.ArrayList r30 = new java.util.ArrayList
            r53 = 2
            r0 = r30
            r1 = r53
            r0.<init>(r1)
            r0 = r60
            java.lang.String r0 = r0.subid
            r53 = r0
            if (r53 == 0) goto L_0x0324
            org.apache.http.message.BasicNameValuePair r53 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r54 = "subid"
            r0 = r60
            java.lang.String r0 = r0.subid
            r55 = r0
            r53.<init>(r54, r55)
            r0 = r30
            r1 = r53
            r0.add(r1)
        L_0x0324:
            r0 = r60
            java.util.List<org.apache.http.NameValuePair> r0 = r0.tokens
            r53 = r0
            if (r53 == 0) goto L_0x036c
            java.lang.String r45 = ""
            r24 = 0
        L_0x0330:
            r0 = r60
            java.util.List<org.apache.http.NameValuePair> r0 = r0.tokens     // Catch:{ Exception -> 0x0709 }
            r53 = r0
            int r53 = r53.size()     // Catch:{ Exception -> 0x0709 }
            r0 = r24
            r1 = r53
            if (r0 < r1) goto L_0x06b8
            org.apache.http.message.BasicNameValuePair r53 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0709 }
            java.lang.String r54 = "tokens"
            r0 = r53
            r1 = r54
            r2 = r45
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x0709 }
            r0 = r30
            r1 = r53
            r0.add(r1)     // Catch:{ Exception -> 0x0709 }
            java.lang.String r53 = "LBAdController"
            java.lang.StringBuilder r54 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0709 }
            java.lang.String r55 = "Token Str = "
            r54.<init>(r55)     // Catch:{ Exception -> 0x0709 }
            r0 = r54
            r1 = r45
            java.lang.StringBuilder r54 = r0.append(r1)     // Catch:{ Exception -> 0x0709 }
            java.lang.String r54 = r54.toString()     // Catch:{ Exception -> 0x0709 }
            com.Leadbolt.AdLog.i(r53, r54)     // Catch:{ Exception -> 0x0709 }
        L_0x036c:
            r0 = r60
            boolean r0 = r0.useSecure     // Catch:{ Exception -> 0x0752 }
            r53 = r0
            if (r53 == 0) goto L_0x071a
            r39 = 0
        L_0x0376:
            org.apache.http.message.BasicNameValuePair r53 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0752 }
            java.lang.String r54 = "ref1"
            r0 = r53
            r1 = r54
            r2 = r39
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x0752 }
            r0 = r30
            r1 = r53
            r0.add(r1)     // Catch:{ Exception -> 0x0752 }
            org.apache.http.message.BasicNameValuePair r53 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0752 }
            java.lang.String r54 = "ref2"
            java.lang.String r55 = android.os.Build.VERSION.RELEASE     // Catch:{ Exception -> 0x0752 }
            r53.<init>(r54, r55)     // Catch:{ Exception -> 0x0752 }
            r0 = r30
            r1 = r53
            r0.add(r1)     // Catch:{ Exception -> 0x0752 }
            org.apache.http.message.BasicNameValuePair r53 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0752 }
            java.lang.String r54 = "ref3"
            java.lang.String r55 = "Android"
            r53.<init>(r54, r55)     // Catch:{ Exception -> 0x0752 }
            r0 = r30
            r1 = r53
            r0.add(r1)     // Catch:{ Exception -> 0x0752 }
            org.apache.http.message.BasicNameValuePair r53 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0752 }
            java.lang.String r54 = "ref4"
            java.lang.String r55 = r60.getLocalIpAddress()     // Catch:{ Exception -> 0x0752 }
            r53.<init>(r54, r55)     // Catch:{ Exception -> 0x0752 }
            r0 = r30
            r1 = r53
            r0.add(r1)     // Catch:{ Exception -> 0x0752 }
            org.apache.http.message.BasicNameValuePair r53 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0752 }
            java.lang.String r54 = "ref5"
            java.lang.StringBuilder r55 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0752 }
            r55.<init>()     // Catch:{ Exception -> 0x0752 }
            r56 = 15
            r0 = r56
            int r56 = r4.get(r0)     // Catch:{ Exception -> 0x0752 }
            java.lang.StringBuilder r55 = r55.append(r56)     // Catch:{ Exception -> 0x0752 }
            java.lang.String r55 = r55.toString()     // Catch:{ Exception -> 0x0752 }
            r53.<init>(r54, r55)     // Catch:{ Exception -> 0x0752 }
            r0 = r30
            r1 = r53
            r0.add(r1)     // Catch:{ Exception -> 0x0752 }
            org.apache.http.message.BasicNameValuePair r53 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0752 }
            java.lang.String r54 = "ref6"
            java.lang.StringBuilder r55 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0752 }
            r55.<init>()     // Catch:{ Exception -> 0x0752 }
            long r56 = r4.getTimeInMillis()     // Catch:{ Exception -> 0x0752 }
            r58 = 1000(0x3e8, double:4.94E-321)
            long r56 = r56 / r58
            r0 = r56
            int r0 = (int) r0     // Catch:{ Exception -> 0x0752 }
            r56 = r0
            java.lang.StringBuilder r55 = r55.append(r56)     // Catch:{ Exception -> 0x0752 }
            java.lang.String r55 = r55.toString()     // Catch:{ Exception -> 0x0752 }
            r53.<init>(r54, r55)     // Catch:{ Exception -> 0x0752 }
            r0 = r30
            r1 = r53
            r0.add(r1)     // Catch:{ Exception -> 0x0752 }
            org.apache.http.message.BasicNameValuePair r53 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0752 }
            java.lang.String r54 = "ref7"
            java.lang.StringBuilder r55 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0752 }
            r55.<init>()     // Catch:{ Exception -> 0x0752 }
            r0 = r60
            int r0 = r0.sWidth     // Catch:{ Exception -> 0x0752 }
            r56 = r0
            java.lang.StringBuilder r55 = r55.append(r56)     // Catch:{ Exception -> 0x0752 }
            java.lang.String r55 = r55.toString()     // Catch:{ Exception -> 0x0752 }
            r53.<init>(r54, r55)     // Catch:{ Exception -> 0x0752 }
            r0 = r30
            r1 = r53
            r0.add(r1)     // Catch:{ Exception -> 0x0752 }
            org.apache.http.message.BasicNameValuePair r53 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0752 }
            java.lang.String r54 = "ref8"
            java.lang.StringBuilder r55 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0752 }
            r55.<init>()     // Catch:{ Exception -> 0x0752 }
            r0 = r60
            int r0 = r0.sHeight     // Catch:{ Exception -> 0x0752 }
            r56 = r0
            java.lang.StringBuilder r55 = r55.append(r56)     // Catch:{ Exception -> 0x0752 }
            java.lang.String r55 = r55.toString()     // Catch:{ Exception -> 0x0752 }
            r53.<init>(r54, r55)     // Catch:{ Exception -> 0x0752 }
            r0 = r30
            r1 = r53
            r0.add(r1)     // Catch:{ Exception -> 0x0752 }
            r0 = r60
            boolean r0 = r0.useLocation     // Catch:{ Exception -> 0x0752 }
            r53 = r0
            if (r53 == 0) goto L_0x049f
            java.lang.String r53 = "location"
            r0 = r48
            r1 = r53
            java.lang.Object r26 = r0.getSystemService(r1)     // Catch:{ Exception -> 0x0a6f }
            android.location.LocationManager r26 = (android.location.LocationManager) r26     // Catch:{ Exception -> 0x0a6f }
            java.lang.String r53 = "gps"
            r0 = r26
            r1 = r53
            android.location.Location r27 = r0.getLastKnownLocation(r1)     // Catch:{ Exception -> 0x0a6f }
            double r53 = r27.getLongitude()     // Catch:{ Exception -> 0x0a6f }
            java.lang.String r28 = java.lang.String.valueOf(r53)     // Catch:{ Exception -> 0x0a6f }
            double r53 = r27.getLatitude()     // Catch:{ Exception -> 0x0a6f }
            java.lang.String r25 = java.lang.String.valueOf(r53)     // Catch:{ Exception -> 0x0a6f }
            org.apache.http.message.BasicNameValuePair r53 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0a6f }
            java.lang.String r54 = "ref9"
            r0 = r53
            r1 = r54
            r2 = r25
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x0a6f }
            r0 = r30
            r1 = r53
            r0.add(r1)     // Catch:{ Exception -> 0x0a6f }
            org.apache.http.message.BasicNameValuePair r53 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0a6f }
            java.lang.String r54 = "ref10"
            r0 = r53
            r1 = r54
            r2 = r28
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x0a6f }
            r0 = r30
            r1 = r53
            r0.add(r1)     // Catch:{ Exception -> 0x0a6f }
        L_0x049f:
            r0 = r60
            boolean r0 = r0.dataretrieve     // Catch:{ Exception -> 0x0752 }
            r53 = r0
            if (r53 == 0) goto L_0x0507
            org.apache.http.message.BasicNameValuePair r53 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0a6c }
            java.lang.String r54 = "ref11"
            r0 = r60
            android.telephony.TelephonyManager r0 = r0.tm     // Catch:{ Exception -> 0x0a6c }
            r55 = r0
            java.lang.String r55 = r55.getNetworkCountryIso()     // Catch:{ Exception -> 0x0a6c }
            r53.<init>(r54, r55)     // Catch:{ Exception -> 0x0a6c }
            r0 = r30
            r1 = r53
            r0.add(r1)     // Catch:{ Exception -> 0x0a6c }
            org.apache.http.message.BasicNameValuePair r53 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0a6c }
            java.lang.String r54 = "ref12"
            r0 = r60
            android.telephony.TelephonyManager r0 = r0.tm     // Catch:{ Exception -> 0x0a6c }
            r55 = r0
            java.lang.String r55 = r55.getNetworkOperator()     // Catch:{ Exception -> 0x0a6c }
            r53.<init>(r54, r55)     // Catch:{ Exception -> 0x0a6c }
            r0 = r30
            r1 = r53
            r0.add(r1)     // Catch:{ Exception -> 0x0a6c }
            org.apache.http.message.BasicNameValuePair r53 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0a6c }
            java.lang.String r54 = "ref13"
            r0 = r60
            android.telephony.TelephonyManager r0 = r0.tm     // Catch:{ Exception -> 0x0a6c }
            r55 = r0
            java.lang.String r55 = r55.getNetworkOperatorName()     // Catch:{ Exception -> 0x0a6c }
            r53.<init>(r54, r55)     // Catch:{ Exception -> 0x0a6c }
            r0 = r30
            r1 = r53
            r0.add(r1)     // Catch:{ Exception -> 0x0a6c }
            org.apache.http.message.BasicNameValuePair r53 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0a6c }
            java.lang.String r54 = "ref14"
            r0 = r60
            android.telephony.TelephonyManager r0 = r0.tm     // Catch:{ Exception -> 0x0a6c }
            r55 = r0
            java.lang.String r55 = r55.getLine1Number()     // Catch:{ Exception -> 0x0a6c }
            r53.<init>(r54, r55)     // Catch:{ Exception -> 0x0a6c }
            r0 = r30
            r1 = r53
            r0.add(r1)     // Catch:{ Exception -> 0x0a6c }
        L_0x0507:
            org.apache.http.message.BasicNameValuePair r53 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0752 }
            java.lang.String r54 = "ref15"
            java.lang.String r55 = "3"
            r53.<init>(r54, r55)     // Catch:{ Exception -> 0x0752 }
            r0 = r30
            r1 = r53
            r0.add(r1)     // Catch:{ Exception -> 0x0752 }
            org.apache.http.message.BasicNameValuePair r53 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0752 }
            java.lang.String r54 = "ref16"
            java.lang.String r55 = "00"
            r53.<init>(r54, r55)     // Catch:{ Exception -> 0x0752 }
            r0 = r30
            r1 = r53
            r0.add(r1)     // Catch:{ Exception -> 0x0752 }
            org.apache.http.message.BasicNameValuePair r53 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0752 }
            java.lang.String r54 = "ref17"
            r0 = r60
            android.telephony.TelephonyManager r0 = r0.tm     // Catch:{ Exception -> 0x0752 }
            r55 = r0
            java.lang.String r55 = r55.getDeviceId()     // Catch:{ Exception -> 0x0752 }
            r53.<init>(r54, r55)     // Catch:{ Exception -> 0x0752 }
            r0 = r30
            r1 = r53
            r0.add(r1)     // Catch:{ Exception -> 0x0752 }
            org.apache.http.message.BasicNameValuePair r53 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0752 }
            java.lang.String r54 = "ref18"
            java.lang.String r55 = android.os.Build.MANUFACTURER     // Catch:{ Exception -> 0x0752 }
            r53.<init>(r54, r55)     // Catch:{ Exception -> 0x0752 }
            r0 = r30
            r1 = r53
            r0.add(r1)     // Catch:{ Exception -> 0x0752 }
            org.apache.http.message.BasicNameValuePair r53 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0752 }
            java.lang.String r54 = "ref19"
            java.lang.String r55 = android.os.Build.MODEL     // Catch:{ Exception -> 0x0752 }
            r53.<init>(r54, r55)     // Catch:{ Exception -> 0x0752 }
            r0 = r30
            r1 = r53
            r0.add(r1)     // Catch:{ Exception -> 0x0752 }
            java.lang.String r53 = "connectivity"
            r0 = r48
            r1 = r53
            java.lang.Object r5 = r0.getSystemService(r1)     // Catch:{ Exception -> 0x0752 }
            android.net.ConnectivityManager r5 = (android.net.ConnectivityManager) r5     // Catch:{ Exception -> 0x0752 }
            r53 = 0
            r0 = r53
            android.net.NetworkInfo r53 = r5.getNetworkInfo(r0)     // Catch:{ Exception -> 0x0752 }
            android.net.NetworkInfo$State r29 = r53.getState()     // Catch:{ Exception -> 0x0752 }
            r53 = 1
            r0 = r53
            android.net.NetworkInfo r53 = r5.getNetworkInfo(r0)     // Catch:{ Exception -> 0x0752 }
            android.net.NetworkInfo$State r51 = r53.getState()     // Catch:{ Exception -> 0x0752 }
            java.lang.String r31 = ""
            android.net.NetworkInfo$State r53 = android.net.NetworkInfo.State.CONNECTED     // Catch:{ Exception -> 0x0752 }
            r0 = r51
            r1 = r53
            if (r0 == r1) goto L_0x0595
            android.net.NetworkInfo$State r53 = android.net.NetworkInfo.State.CONNECTING     // Catch:{ Exception -> 0x0752 }
            r0 = r51
            r1 = r53
            if (r0 != r1) goto L_0x071e
        L_0x0595:
            java.lang.String r31 = "wifi"
        L_0x0597:
            org.apache.http.message.BasicNameValuePair r53 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0752 }
            java.lang.String r54 = "ref20"
            r0 = r53
            r1 = r54
            r2 = r31
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x0752 }
            r0 = r30
            r1 = r53
            r0.add(r1)     // Catch:{ Exception -> 0x0752 }
            r0 = r60
            android.telephony.TelephonyManager r0 = r0.tm     // Catch:{ Exception -> 0x0752 }
            r53 = r0
            int r42 = r53.getSimState()     // Catch:{ Exception -> 0x0752 }
            java.lang.String r43 = ""
            switch(r42) {
                case 0: goto L_0x0746;
                case 1: goto L_0x0732;
                case 2: goto L_0x073a;
                case 3: goto L_0x073e;
                case 4: goto L_0x0736;
                case 5: goto L_0x0742;
                default: goto L_0x05ba;
            }     // Catch:{ Exception -> 0x0752 }
        L_0x05ba:
            org.apache.http.message.BasicNameValuePair r53 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0752 }
            java.lang.String r54 = "ref21"
            r0 = r53
            r1 = r54
            r2 = r43
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x0752 }
            r0 = r30
            r1 = r53
            r0.add(r1)     // Catch:{ Exception -> 0x0752 }
            java.lang.String r54 = "LBAdController"
            java.lang.StringBuilder r55 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0752 }
            java.lang.String r53 = "r20 - "
            r0 = r55
            r1 = r53
            r0.<init>(r1)     // Catch:{ Exception -> 0x0752 }
            java.lang.String r53 = "wifi"
            r0 = r31
            r1 = r53
            boolean r53 = r0.equals(r1)     // Catch:{ Exception -> 0x0752 }
            if (r53 == 0) goto L_0x074a
            java.lang.String r53 = "w"
        L_0x05e9:
            r0 = r55
            r1 = r53
            java.lang.StringBuilder r53 = r0.append(r1)     // Catch:{ Exception -> 0x0752 }
            java.lang.String r55 = ", r21 - "
            r0 = r53
            r1 = r55
            java.lang.StringBuilder r53 = r0.append(r1)     // Catch:{ Exception -> 0x0752 }
            r0 = r53
            r1 = r43
            java.lang.StringBuilder r53 = r0.append(r1)     // Catch:{ Exception -> 0x0752 }
            java.lang.String r53 = r53.toString()     // Catch:{ Exception -> 0x0752 }
            r0 = r54
            r1 = r53
            com.Leadbolt.AdLog.d(r0, r1)     // Catch:{ Exception -> 0x0752 }
            r0 = r60
            boolean r0 = r0.useSecure     // Catch:{ Exception -> 0x0752 }
            r53 = r0
            if (r53 == 0) goto L_0x074e
            r53 = 0
            r54 = 64
            r0 = r53
            r1 = r54
            java.lang.String r53 = r8.substring(r0, r1)     // Catch:{ Exception -> 0x0752 }
            r0 = r60
            r1 = r53
            java.lang.String r40 = r0.md5(r1)     // Catch:{ Exception -> 0x0752 }
        L_0x062a:
            org.apache.http.message.BasicNameValuePair r53 = new org.apache.http.message.BasicNameValuePair     // Catch:{ Exception -> 0x0752 }
            java.lang.String r54 = "ref22"
            r0 = r53
            r1 = r54
            r2 = r40
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x0752 }
            r0 = r30
            r1 = r53
            r0.add(r1)     // Catch:{ Exception -> 0x0752 }
            org.apache.http.client.entity.UrlEncodedFormEntity r53 = new org.apache.http.client.entity.UrlEncodedFormEntity     // Catch:{ Exception -> 0x0752 }
            r0 = r53
            r1 = r30
            r0.<init>(r1)     // Catch:{ Exception -> 0x0752 }
            r0 = r19
            r1 = r53
            r0.setEntity(r1)     // Catch:{ Exception -> 0x0752 }
        L_0x064e:
            r6 = 0
        L_0x064f:
            r53 = 10
            r0 = r53
            if (r6 < r0) goto L_0x075c
        L_0x0655:
            int r20 = r20 + 1
            goto L_0x0164
        L_0x0659:
            r0 = r60
            android.app.Activity r0 = r0.activity
            r48 = r0
            goto L_0x003c
        L_0x0661:
            r46 = 0
            goto L_0x009b
        L_0x0665:
            r0 = r60
            android.content.Context r0 = r0.context
            r53 = r0
            java.lang.String r54 = "phone"
            java.lang.Object r53 = r53.getSystemService(r54)
            android.telephony.TelephonyManager r53 = (android.telephony.TelephonyManager) r53
            r0 = r53
            r1 = r60
            r1.tm = r0
            goto L_0x0142
        L_0x067b:
            r0 = r60
            boolean r0 = r0.loadIcon
            r53 = r0
            if (r53 == 0) goto L_0x01b3
            java.lang.StringBuilder r53 = new java.lang.StringBuilder
            r0 = r60
            java.lang.String r0 = r0.domain
            r54 = r0
            java.lang.String r54 = java.lang.String.valueOf(r54)
            r53.<init>(r54)
            java.lang.String r54 = "/show_app_icon.conf?"
            java.lang.StringBuilder r53 = r53.append(r54)
            java.lang.String r49 = r53.toString()
            goto L_0x01b3
        L_0x069e:
            long r53 = java.lang.System.currentTimeMillis()
            r55 = 1000(0x3e8, double:4.94E-321)
            long r53 = r53 / r55
            r0 = r53
            int r0 = (int) r0
            r53 = r0
            r54 = 1000(0x3e8, double:4.94E-321)
            long r54 = r15 / r54
            r0 = r54
            int r0 = (int) r0
            r54 = r0
            int r22 = r53 - r54
            goto L_0x027f
        L_0x06b8:
            r0 = r60
            java.util.List<org.apache.http.NameValuePair> r0 = r0.tokens     // Catch:{ Exception -> 0x0709 }
            r53 = r0
            r0 = r53
            r1 = r24
            java.lang.Object r47 = r0.get(r1)     // Catch:{ Exception -> 0x0709 }
            org.apache.http.message.BasicNameValuePair r47 = (org.apache.http.message.BasicNameValuePair) r47     // Catch:{ Exception -> 0x0709 }
            java.lang.StringBuilder r53 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0709 }
            java.lang.String r54 = java.lang.String.valueOf(r45)     // Catch:{ Exception -> 0x0709 }
            r53.<init>(r54)     // Catch:{ Exception -> 0x0709 }
            java.lang.String r54 = r47.getName()     // Catch:{ Exception -> 0x0709 }
            byte[] r54 = r54.getBytes()     // Catch:{ Exception -> 0x0709 }
            r55 = 0
            java.lang.String r54 = android.util.Base64.encodeToString(r54, r55)     // Catch:{ Exception -> 0x0709 }
            java.lang.StringBuilder r53 = r53.append(r54)     // Catch:{ Exception -> 0x0709 }
            java.lang.String r54 = ":"
            java.lang.StringBuilder r53 = r53.append(r54)     // Catch:{ Exception -> 0x0709 }
            java.lang.String r54 = r47.getValue()     // Catch:{ Exception -> 0x0709 }
            byte[] r54 = r54.getBytes()     // Catch:{ Exception -> 0x0709 }
            r55 = 0
            java.lang.String r54 = android.util.Base64.encodeToString(r54, r55)     // Catch:{ Exception -> 0x0709 }
            java.lang.StringBuilder r53 = r53.append(r54)     // Catch:{ Exception -> 0x0709 }
            java.lang.String r54 = ","
            java.lang.StringBuilder r53 = r53.append(r54)     // Catch:{ Exception -> 0x0709 }
            java.lang.String r45 = r53.toString()     // Catch:{ Exception -> 0x0709 }
            int r24 = r24 + 1
            goto L_0x0330
        L_0x0709:
            r11 = move-exception
            java.lang.String r53 = "LBAdController"
            java.lang.String r54 = "Error while adding tokens"
            com.Leadbolt.AdLog.e(r53, r54)
            java.lang.String r53 = "LBAdController"
            r0 = r53
            com.Leadbolt.AdLog.printStackTrace(r0, r11)
            goto L_0x036c
        L_0x071a:
            r39 = r8
            goto L_0x0376
        L_0x071e:
            android.net.NetworkInfo$State r53 = android.net.NetworkInfo.State.CONNECTED     // Catch:{ Exception -> 0x0752 }
            r0 = r29
            r1 = r53
            if (r0 == r1) goto L_0x072e
            android.net.NetworkInfo$State r53 = android.net.NetworkInfo.State.CONNECTING     // Catch:{ Exception -> 0x0752 }
            r0 = r29
            r1 = r53
            if (r0 != r1) goto L_0x0597
        L_0x072e:
            java.lang.String r31 = "carrier"
            goto L_0x0597
        L_0x0732:
            java.lang.String r43 = "no_sim"
            goto L_0x05ba
        L_0x0736:
            java.lang.String r43 = "sim_carrier_locked"
            goto L_0x05ba
        L_0x073a:
            java.lang.String r43 = "sim_user_locked"
            goto L_0x05ba
        L_0x073e:
            java.lang.String r43 = "sim_puk_locked"
            goto L_0x05ba
        L_0x0742:
            java.lang.String r43 = "sim_ok"
            goto L_0x05ba
        L_0x0746:
            java.lang.String r43 = "sim_unknown"
            goto L_0x05ba
        L_0x074a:
            java.lang.String r53 = "c"
            goto L_0x05e9
        L_0x074e:
            r40 = 0
            goto L_0x062a
        L_0x0752:
            r11 = move-exception
            java.lang.String r53 = "LBAdController"
            r0 = r53
            com.Leadbolt.AdLog.printStackTrace(r0, r11)
            goto L_0x064e
        L_0x075c:
            if (r17 != 0) goto L_0x0655
            r34 = 0
            org.apache.http.HttpResponse r41 = r18.execute(r19)     // Catch:{ Exception -> 0x08ab }
            org.apache.http.StatusLine r53 = r41.getStatusLine()     // Catch:{ Exception -> 0x08ab }
            int r53 = r53.getStatusCode()     // Catch:{ Exception -> 0x08ab }
            r54 = 200(0xc8, float:2.8E-43)
            r0 = r53
            r1 = r54
            if (r0 != r1) goto L_0x0817
            r17 = 1
            org.apache.http.HttpEntity r13 = r41.getEntity()     // Catch:{ Exception -> 0x08ab }
            if (r13 == 0) goto L_0x0817
            java.io.InputStream r21 = r13.getContent()     // Catch:{ Exception -> 0x08ab }
            java.lang.String r45 = convertStreamToString(r21)     // Catch:{ Exception -> 0x08ab }
            org.json.JSONObject r53 = new org.json.JSONObject     // Catch:{ Exception -> 0x08ab }
            r0 = r53
            r1 = r45
            r0.<init>(r1)     // Catch:{ Exception -> 0x08ab }
            r0 = r53
            r1 = r60
            r1.results = r0     // Catch:{ Exception -> 0x08ab }
            r0 = r60
            org.json.JSONObject r0 = r0.results     // Catch:{ JSONException -> 0x0a69 }
            r53 = r0
            java.lang.String r54 = "usenative"
            java.lang.Object r53 = r53.get(r54)     // Catch:{ JSONException -> 0x0a69 }
            java.lang.String r54 = "1"
            boolean r53 = r53.equals(r54)     // Catch:{ JSONException -> 0x0a69 }
            if (r53 == 0) goto L_0x07af
            r53 = 1
            r0 = r53
            r1 = r60
            r1.nativeOpen = r0     // Catch:{ JSONException -> 0x0a69 }
        L_0x07af:
            r0 = r60
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0898 }
            r53 = r0
            java.lang.String r54 = "pollmaxcount"
            int r53 = r53.getInt(r54)     // Catch:{ Exception -> 0x0898 }
            r0 = r53
            r1 = r60
            r1.pollMax = r0     // Catch:{ Exception -> 0x0898 }
            r0 = r60
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0898 }
            r53 = r0
            java.lang.String r54 = "pollmanualafter"
            int r32 = r53.getInt(r54)     // Catch:{ Exception -> 0x0898 }
            r0 = r60
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0898 }
            r53 = r0
            java.lang.String r54 = "pollinterval"
            int r34 = r53.getInt(r54)     // Catch:{ Exception -> 0x0898 }
            if (r34 <= 0) goto L_0x088e
            int r53 = r32 * 60
            int r53 = r53 / r34
            r0 = r53
            r1 = r60
            r1.pollManual = r0     // Catch:{ Exception -> 0x0898 }
        L_0x07e5:
            android.content.SharedPreferences$Editor r12 = r35.edit()     // Catch:{ Exception -> 0x08ab }
            java.lang.StringBuilder r53 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0960 }
            java.lang.String r54 = "SD_"
            r53.<init>(r54)     // Catch:{ Exception -> 0x0960 }
            r0 = r60
            java.lang.String r0 = r0.sectionid     // Catch:{ Exception -> 0x0960 }
            r54 = r0
            java.lang.StringBuilder r53 = r53.append(r54)     // Catch:{ Exception -> 0x0960 }
            java.lang.String r53 = r53.toString()     // Catch:{ Exception -> 0x0960 }
            r0 = r60
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0960 }
            r54 = r0
            java.lang.String r55 = "displayinterval"
            java.lang.String r54 = r54.getString(r55)     // Catch:{ Exception -> 0x0960 }
            r0 = r53
            r1 = r54
            r12.putString(r0, r1)     // Catch:{ Exception -> 0x0960 }
        L_0x0811:
            r12.commit()     // Catch:{ Exception -> 0x08ab }
            r21.close()     // Catch:{ Exception -> 0x08ab }
        L_0x0817:
            java.lang.String r53 = "LBAdController"
            java.lang.StringBuilder r54 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0a56 }
            java.lang.String r55 = "Results - "
            r54.<init>(r55)     // Catch:{ Exception -> 0x0a56 }
            r0 = r60
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0a56 }
            r55 = r0
            java.lang.String r55 = r55.toString()     // Catch:{ Exception -> 0x0a56 }
            java.lang.StringBuilder r54 = r54.append(r55)     // Catch:{ Exception -> 0x0a56 }
            java.lang.String r54 = r54.toString()     // Catch:{ Exception -> 0x0a56 }
            com.Leadbolt.AdLog.d(r53, r54)     // Catch:{ Exception -> 0x0a56 }
            r0 = r60
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0a56 }
            r53 = r0
            if (r53 == 0) goto L_0x0853
            r0 = r60
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0a56 }
            r53 = r0
            java.lang.String r54 = "show"
            java.lang.String r53 = r53.getString(r54)     // Catch:{ Exception -> 0x0a56 }
            if (r53 == 0) goto L_0x0853
            r53 = 1
            r0 = r53
            r1 = r60
            r1.initialized = r0     // Catch:{ Exception -> 0x0a56 }
        L_0x0853:
            r53 = 0
            r0 = r53
            r1 = r60
            r1.requestInProgress = r0
            r0 = r60
            boolean r0 = r0.useNotification     // Catch:{ Exception -> 0x0a64 }
            r53 = r0
            if (r53 == 0) goto L_0x088a
            android.content.SharedPreferences$Editor r12 = r35.edit()     // Catch:{ Exception -> 0x0a64 }
            java.lang.StringBuilder r53 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0a64 }
            java.lang.String r54 = "SD_NOTIFICATION_REQUESTED_"
            r53.<init>(r54)     // Catch:{ Exception -> 0x0a64 }
            r0 = r60
            java.lang.String r0 = r0.sectionid     // Catch:{ Exception -> 0x0a64 }
            r54 = r0
            java.lang.StringBuilder r53 = r53.append(r54)     // Catch:{ Exception -> 0x0a64 }
            java.lang.String r53 = r53.toString()     // Catch:{ Exception -> 0x0a64 }
            long r54 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0a64 }
            r0 = r53
            r1 = r54
            r12.putLong(r0, r1)     // Catch:{ Exception -> 0x0a64 }
            r12.commit()     // Catch:{ Exception -> 0x0a64 }
        L_0x088a:
            int r6 = r6 + 1
            goto L_0x064f
        L_0x088e:
            r53 = 10
            r0 = r53
            r1 = r60
            r1.pollManual = r0     // Catch:{ Exception -> 0x0898 }
            goto L_0x07e5
        L_0x0898:
            r11 = move-exception
            r53 = 500(0x1f4, float:7.0E-43)
            r0 = r53
            r1 = r60
            r1.pollMax = r0     // Catch:{ Exception -> 0x08ab }
            r53 = 10
            r0 = r53
            r1 = r60
            r1.pollManual = r0     // Catch:{ Exception -> 0x08ab }
            goto L_0x07e5
        L_0x08ab:
            r14 = move-exception
            java.lang.String r53 = "LBAdController"
            r0 = r53
            com.Leadbolt.AdLog.printStackTrace(r0, r14)     // Catch:{ all -> 0x099b }
            r17 = 0
            org.json.JSONObject r53 = new org.json.JSONObject     // Catch:{ all -> 0x099b }
            r53.<init>()     // Catch:{ all -> 0x099b }
            r0 = r53
            r1 = r60
            r1.results = r0     // Catch:{ all -> 0x099b }
            r53 = 1
            r0 = r53
            r1 = r60
            r1.initialized = r0     // Catch:{ all -> 0x099b }
            r0 = r60
            com.Leadbolt.AdListener r0 = r0.listener     // Catch:{ all -> 0x099b }
            r53 = r0
            if (r53 == 0) goto L_0x08e8
            java.lang.String r53 = "LBAdController"
            java.lang.String r54 = "onAdFailed triggered"
            com.Leadbolt.AdLog.i(r53, r54)     // Catch:{ Exception -> 0x0a30 }
            r0 = r60
            com.Leadbolt.AdListener r0 = r0.listener     // Catch:{ Exception -> 0x0a30 }
            r53 = r0
            r53.onAdFailed()     // Catch:{ Exception -> 0x0a30 }
            r53 = 1
            r0 = r53
            r1 = r60
            r1.adLoaded = r0     // Catch:{ Exception -> 0x0a30 }
        L_0x08e8:
            java.lang.String r53 = "LBAdController"
            java.lang.StringBuilder r54 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0a41 }
            java.lang.String r55 = "Results - "
            r54.<init>(r55)     // Catch:{ Exception -> 0x0a41 }
            r0 = r60
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0a41 }
            r55 = r0
            java.lang.String r55 = r55.toString()     // Catch:{ Exception -> 0x0a41 }
            java.lang.StringBuilder r54 = r54.append(r55)     // Catch:{ Exception -> 0x0a41 }
            java.lang.String r54 = r54.toString()     // Catch:{ Exception -> 0x0a41 }
            com.Leadbolt.AdLog.d(r53, r54)     // Catch:{ Exception -> 0x0a41 }
            r0 = r60
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0a41 }
            r53 = r0
            if (r53 == 0) goto L_0x0924
            r0 = r60
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0a41 }
            r53 = r0
            java.lang.String r54 = "show"
            java.lang.String r53 = r53.getString(r54)     // Catch:{ Exception -> 0x0a41 }
            if (r53 == 0) goto L_0x0924
            r53 = 1
            r0 = r53
            r1 = r60
            r1.initialized = r0     // Catch:{ Exception -> 0x0a41 }
        L_0x0924:
            r53 = 0
            r0 = r53
            r1 = r60
            r1.requestInProgress = r0
            r0 = r60
            boolean r0 = r0.useNotification     // Catch:{ Exception -> 0x095d }
            r53 = r0
            if (r53 == 0) goto L_0x088a
            android.content.SharedPreferences$Editor r12 = r35.edit()     // Catch:{ Exception -> 0x095d }
            java.lang.StringBuilder r53 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x095d }
            java.lang.String r54 = "SD_NOTIFICATION_REQUESTED_"
            r53.<init>(r54)     // Catch:{ Exception -> 0x095d }
            r0 = r60
            java.lang.String r0 = r0.sectionid     // Catch:{ Exception -> 0x095d }
            r54 = r0
            java.lang.StringBuilder r53 = r53.append(r54)     // Catch:{ Exception -> 0x095d }
            java.lang.String r53 = r53.toString()     // Catch:{ Exception -> 0x095d }
            long r54 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x095d }
            r0 = r53
            r1 = r54
            r12.putLong(r0, r1)     // Catch:{ Exception -> 0x095d }
            r12.commit()     // Catch:{ Exception -> 0x095d }
            goto L_0x088a
        L_0x095d:
            r53 = move-exception
            goto L_0x088a
        L_0x0960:
            r11 = move-exception
            r0 = r60
            java.lang.String r0 = r0.adDisplayInterval     // Catch:{ Exception -> 0x08ab }
            r53 = r0
            if (r53 == 0) goto L_0x0a10
            r0 = r60
            java.lang.String r0 = r0.adDisplayInterval     // Catch:{ Exception -> 0x08ab }
            r53 = r0
            java.lang.String r54 = "0"
            boolean r53 = r53.equals(r54)     // Catch:{ Exception -> 0x08ab }
            if (r53 != 0) goto L_0x0a10
            java.lang.StringBuilder r53 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x08ab }
            java.lang.String r54 = "SD_"
            r53.<init>(r54)     // Catch:{ Exception -> 0x08ab }
            r0 = r60
            java.lang.String r0 = r0.sectionid     // Catch:{ Exception -> 0x08ab }
            r54 = r0
            java.lang.StringBuilder r53 = r53.append(r54)     // Catch:{ Exception -> 0x08ab }
            java.lang.String r53 = r53.toString()     // Catch:{ Exception -> 0x08ab }
            r0 = r60
            java.lang.String r0 = r0.adDisplayInterval     // Catch:{ Exception -> 0x08ab }
            r54 = r0
            r0 = r53
            r1 = r54
            r12.putString(r0, r1)     // Catch:{ Exception -> 0x08ab }
            goto L_0x0811
        L_0x099b:
            r53 = move-exception
            java.lang.String r54 = "LBAdController"
            java.lang.StringBuilder r55 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0a4c }
            java.lang.String r56 = "Results - "
            r55.<init>(r56)     // Catch:{ Exception -> 0x0a4c }
            r0 = r60
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0a4c }
            r56 = r0
            java.lang.String r56 = r56.toString()     // Catch:{ Exception -> 0x0a4c }
            java.lang.StringBuilder r55 = r55.append(r56)     // Catch:{ Exception -> 0x0a4c }
            java.lang.String r55 = r55.toString()     // Catch:{ Exception -> 0x0a4c }
            com.Leadbolt.AdLog.d(r54, r55)     // Catch:{ Exception -> 0x0a4c }
            r0 = r60
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0a4c }
            r54 = r0
            if (r54 == 0) goto L_0x09d8
            r0 = r60
            org.json.JSONObject r0 = r0.results     // Catch:{ Exception -> 0x0a4c }
            r54 = r0
            java.lang.String r55 = "show"
            java.lang.String r54 = r54.getString(r55)     // Catch:{ Exception -> 0x0a4c }
            if (r54 == 0) goto L_0x09d8
            r54 = 1
            r0 = r54
            r1 = r60
            r1.initialized = r0     // Catch:{ Exception -> 0x0a4c }
        L_0x09d8:
            r54 = 0
            r0 = r54
            r1 = r60
            r1.requestInProgress = r0
            r0 = r60
            boolean r0 = r0.useNotification     // Catch:{ Exception -> 0x0a67 }
            r54 = r0
            if (r54 == 0) goto L_0x0a0f
            android.content.SharedPreferences$Editor r12 = r35.edit()     // Catch:{ Exception -> 0x0a67 }
            java.lang.StringBuilder r54 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0a67 }
            java.lang.String r55 = "SD_NOTIFICATION_REQUESTED_"
            r54.<init>(r55)     // Catch:{ Exception -> 0x0a67 }
            r0 = r60
            java.lang.String r0 = r0.sectionid     // Catch:{ Exception -> 0x0a67 }
            r55 = r0
            java.lang.StringBuilder r54 = r54.append(r55)     // Catch:{ Exception -> 0x0a67 }
            java.lang.String r54 = r54.toString()     // Catch:{ Exception -> 0x0a67 }
            long r55 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0a67 }
            r0 = r54
            r1 = r55
            r12.putLong(r0, r1)     // Catch:{ Exception -> 0x0a67 }
            r12.commit()     // Catch:{ Exception -> 0x0a67 }
        L_0x0a0f:
            throw r53
        L_0x0a10:
            java.lang.StringBuilder r53 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x08ab }
            java.lang.String r54 = "SD_"
            r53.<init>(r54)     // Catch:{ Exception -> 0x08ab }
            r0 = r60
            java.lang.String r0 = r0.sectionid     // Catch:{ Exception -> 0x08ab }
            r54 = r0
            java.lang.StringBuilder r53 = r53.append(r54)     // Catch:{ Exception -> 0x08ab }
            java.lang.String r53 = r53.toString()     // Catch:{ Exception -> 0x08ab }
            java.lang.String r54 = "0"
            r0 = r53
            r1 = r54
            r12.putString(r0, r1)     // Catch:{ Exception -> 0x08ab }
            goto L_0x0811
        L_0x0a30:
            r11 = move-exception
            java.lang.String r53 = "LBAdController"
            java.lang.String r54 = "Error while calling onAdFailed"
            com.Leadbolt.AdLog.i(r53, r54)     // Catch:{ all -> 0x099b }
            java.lang.String r53 = "LBAdController"
            r0 = r53
            com.Leadbolt.AdLog.printStackTrace(r0, r11)     // Catch:{ all -> 0x099b }
            goto L_0x08e8
        L_0x0a41:
            r11 = move-exception
            r53 = 0
            r0 = r53
            r1 = r60
            r1.initialized = r0
            goto L_0x0924
        L_0x0a4c:
            r11 = move-exception
            r54 = 0
            r0 = r54
            r1 = r60
            r1.initialized = r0
            goto L_0x09d8
        L_0x0a56:
            r11 = move-exception
            r53 = 0
            r0 = r53
            r1 = r60
            r1.initialized = r0
            goto L_0x0853
        L_0x0a61:
            r53 = move-exception
            goto L_0x002e
        L_0x0a64:
            r53 = move-exception
            goto L_0x088a
        L_0x0a67:
            r54 = move-exception
            goto L_0x0a0f
        L_0x0a69:
            r53 = move-exception
            goto L_0x07af
        L_0x0a6c:
            r53 = move-exception
            goto L_0x0507
        L_0x0a6f:
            r53 = move-exception
            goto L_0x049f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.Leadbolt.AdController.makeLBRequest():void");
    }

    private class LBRequest extends AsyncTask<String, Void, String> {
        private LBRequest() {
        }

        /* synthetic */ LBRequest(AdController adController, LBRequest lBRequest) {
            this();
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            AdController.this.makeLBRequest();
            return null;
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
            super.onCancelled();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String res) {
            super.onPostExecute((Object) res);
            if (AdController.this.loadAd) {
                try {
                    if (AdController.this.results.getInt("timeopen") > 0) {
                        int time = AdController.this.results.getInt("timeopen") * 1000;
                        AdLog.i(AdController.LB_LOG, "Tease Time used - ad will load after " + time + "ms");
                        new Handler().postDelayed(new Runnable() {
                            public void run() {
                                AdLog.i(AdController.LB_LOG, "Tease Time passed - loading Ad");
                                AdController.this.displayAd();
                            }
                        }, (long) time);
                        return;
                    }
                    AdController.this.displayAd();
                } catch (Exception e) {
                    AdController.this.displayAd();
                }
            } else if (AdController.this.useNotification) {
                AdController.this.setNotification();
            } else if (AdController.this.loadIcon) {
                AdController.this.displayIcon();
            }
        }
    }

    class OfferPolling extends TimerTask {
        OfferPolling() {
        }

        public void run() {
            HttpEntity entity;
            String success = "0";
            try {
                HttpResponse response = new DefaultHttpClient().execute(new HttpPost(String.valueOf(AdController.this.results.getString("pollurl")) + AdController.this.sectionid));
                if (response.getStatusLine().getStatusCode() == 200 && (entity = response.getEntity()) != null) {
                    InputStream instream = entity.getContent();
                    success = AdController.convertStreamToString(instream);
                    instream.close();
                }
                if (AdController.this.loadingDialog != null && AdController.this.loadingDialog.isShowing()) {
                    AdController.this.loadingDialog.dismiss();
                }
                AdController adController = AdController.this;
                adController.pollCount = adController.pollCount + 1;
                if (success.contains("0")) {
                    try {
                        if (AdController.this.pollCount > AdController.this.pollMax || AdController.this.pollCount >= AdController.this.pollManual) {
                            AdController.this.showManualPoll();
                        } else {
                            AdController.this.pollingHandler.postDelayed(AdController.this.adPolling, (long) (AdController.this.results.getInt("pollinterval") * 1000));
                        }
                    } catch (Exception e) {
                    }
                } else if (success.contains("1")) {
                    AdController.this.closeUnlocker();
                    AdController.this.completed = true;
                    if (AdController.this.listener != null) {
                        try {
                            AdLog.i(AdController.LB_LOG, "onAdCompleted triggered");
                            AdController.this.listener.onAdCompleted();
                        } catch (Exception e2) {
                            AdLog.e(AdController.LB_LOG, "error when onAdCompleted triggered");
                            AdLog.printStackTrace(AdController.LB_LOG, e2);
                        }
                    }
                } else if (success.contains("2")) {
                    if (AdController.this.adPolling != null) {
                        AdController.this.adPolling.cancel();
                    }
                    try {
                        AdController.this.results.put("showclose", "1");
                        AdController.this.results.put("showclickclose", "1");
                    } catch (JSONException e3) {
                    }
                    AdController.this.showCloseButton();
                }
            } catch (IOException e4) {
                if (AdController.this.loadingDialog != null && AdController.this.loadingDialog.isShowing()) {
                    AdController.this.loadingDialog.dismiss();
                }
                AdController adController2 = AdController.this;
                adController2.pollCount = adController2.pollCount + 1;
                if (success.contains("0")) {
                    try {
                        if (AdController.this.pollCount > AdController.this.pollMax || AdController.this.pollCount >= AdController.this.pollManual) {
                            AdController.this.showManualPoll();
                        } else {
                            AdController.this.pollingHandler.postDelayed(AdController.this.adPolling, (long) (AdController.this.results.getInt("pollinterval") * 1000));
                        }
                    } catch (Exception e5) {
                    }
                } else if (success.contains("1")) {
                    AdController.this.closeUnlocker();
                    AdController.this.completed = true;
                    if (AdController.this.listener != null) {
                        try {
                            AdLog.i(AdController.LB_LOG, "onAdCompleted triggered");
                            AdController.this.listener.onAdCompleted();
                        } catch (Exception e6) {
                            AdLog.e(AdController.LB_LOG, "error when onAdCompleted triggered");
                            AdLog.printStackTrace(AdController.LB_LOG, e6);
                        }
                    }
                } else if (success.contains("2")) {
                    if (AdController.this.adPolling != null) {
                        AdController.this.adPolling.cancel();
                    }
                    try {
                        AdController.this.results.put("showclose", "1");
                        AdController.this.results.put("showclickclose", "1");
                    } catch (JSONException e7) {
                    }
                    AdController.this.showCloseButton();
                }
            } catch (JSONException e8) {
                if (AdController.this.loadingDialog != null && AdController.this.loadingDialog.isShowing()) {
                    AdController.this.loadingDialog.dismiss();
                }
                AdController adController3 = AdController.this;
                adController3.pollCount = adController3.pollCount + 1;
                if (success.contains("0")) {
                    try {
                        if (AdController.this.pollCount > AdController.this.pollMax || AdController.this.pollCount >= AdController.this.pollManual) {
                            AdController.this.showManualPoll();
                        } else {
                            AdController.this.pollingHandler.postDelayed(AdController.this.adPolling, (long) (AdController.this.results.getInt("pollinterval") * 1000));
                        }
                    } catch (Exception e9) {
                    }
                } else if (success.contains("1")) {
                    AdController.this.closeUnlocker();
                    AdController.this.completed = true;
                    if (AdController.this.listener != null) {
                        try {
                            AdLog.i(AdController.LB_LOG, "onAdCompleted triggered");
                            AdController.this.listener.onAdCompleted();
                        } catch (Exception e10) {
                            AdLog.e(AdController.LB_LOG, "error when onAdCompleted triggered");
                            AdLog.printStackTrace(AdController.LB_LOG, e10);
                        }
                    }
                } else if (success.contains("2")) {
                    if (AdController.this.adPolling != null) {
                        AdController.this.adPolling.cancel();
                    }
                    try {
                        AdController.this.results.put("showclose", "1");
                        AdController.this.results.put("showclickclose", "1");
                    } catch (JSONException e11) {
                    }
                    AdController.this.showCloseButton();
                }
            } catch (Exception e12) {
                if (AdController.this.loadingDialog != null && AdController.this.loadingDialog.isShowing()) {
                    AdController.this.loadingDialog.dismiss();
                }
                AdController adController4 = AdController.this;
                adController4.pollCount = adController4.pollCount + 1;
                if (success.contains("0")) {
                    try {
                        if (AdController.this.pollCount > AdController.this.pollMax || AdController.this.pollCount >= AdController.this.pollManual) {
                            AdController.this.showManualPoll();
                        } else {
                            AdController.this.pollingHandler.postDelayed(AdController.this.adPolling, (long) (AdController.this.results.getInt("pollinterval") * 1000));
                        }
                    } catch (Exception e13) {
                    }
                } else if (success.contains("1")) {
                    AdController.this.closeUnlocker();
                    AdController.this.completed = true;
                    if (AdController.this.listener != null) {
                        try {
                            AdLog.i(AdController.LB_LOG, "onAdCompleted triggered");
                            AdController.this.listener.onAdCompleted();
                        } catch (Exception e14) {
                            AdLog.e(AdController.LB_LOG, "error when onAdCompleted triggered");
                            AdLog.printStackTrace(AdController.LB_LOG, e14);
                        }
                    }
                } else if (success.contains("2")) {
                    if (AdController.this.adPolling != null) {
                        AdController.this.adPolling.cancel();
                    }
                    try {
                        AdController.this.results.put("showclose", "1");
                        AdController.this.results.put("showclickclose", "1");
                    } catch (JSONException e15) {
                    }
                    AdController.this.showCloseButton();
                }
            } catch (Throwable th) {
                if (AdController.this.loadingDialog != null && AdController.this.loadingDialog.isShowing()) {
                    AdController.this.loadingDialog.dismiss();
                }
                AdController adController5 = AdController.this;
                adController5.pollCount = adController5.pollCount + 1;
                if (success.contains("0")) {
                    try {
                        if (AdController.this.pollCount > AdController.this.pollMax || AdController.this.pollCount >= AdController.this.pollManual) {
                            AdController.this.showManualPoll();
                        } else {
                            AdController.this.pollingHandler.postDelayed(AdController.this.adPolling, (long) (AdController.this.results.getInt("pollinterval") * 1000));
                        }
                    } catch (Exception e16) {
                    }
                } else if (success.contains("1")) {
                    AdController.this.closeUnlocker();
                    AdController.this.completed = true;
                    if (AdController.this.listener != null) {
                        try {
                            AdLog.i(AdController.LB_LOG, "onAdCompleted triggered");
                            AdController.this.listener.onAdCompleted();
                        } catch (Exception e17) {
                            AdLog.e(AdController.LB_LOG, "error when onAdCompleted triggered");
                            AdLog.printStackTrace(AdController.LB_LOG, e17);
                        }
                    }
                } else if (success.contains("2")) {
                    if (AdController.this.adPolling != null) {
                        AdController.this.adPolling.cancel();
                    }
                    try {
                        AdController.this.results.put("showclose", "1");
                        AdController.this.results.put("showclickclose", "1");
                    } catch (JSONException e18) {
                    }
                    AdController.this.showCloseButton();
                }
                throw th;
            }
        }
    }

    /* access modifiers changed from: private */
    public static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is), 8192);
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String line = reader.readLine();
                if (line == null) {
                    try {
                        break;
                    } catch (IOException e) {
                        AdLog.printStackTrace(LB_LOG, e);
                    }
                } else {
                    sb.append(String.valueOf(line) + "\n");
                }
            } catch (IOException e2) {
                AdLog.printStackTrace(LB_LOG, e2);
                try {
                    is.close();
                } catch (IOException e3) {
                    AdLog.printStackTrace(LB_LOG, e3);
                }
            } catch (Throwable th) {
                try {
                    is.close();
                } catch (IOException e4) {
                    AdLog.printStackTrace(LB_LOG, e4);
                }
                throw th;
            }
        }
        is.close();
        return sb.toString();
    }

    private String getLocalIpAddress() {
        try {
            Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
            while (en.hasMoreElements()) {
                Enumeration<InetAddress> enumIpAddr = en.nextElement().getInetAddresses();
                while (true) {
                    if (enumIpAddr.hasMoreElements()) {
                        InetAddress inetAddress = enumIpAddr.nextElement();
                        if (!inetAddress.isLoopbackAddress()) {
                            return inetAddress.getHostAddress().toString();
                        }
                    }
                }
            }
        } catch (SocketException ex) {
            AdLog.printStackTrace(LB_LOG, ex);
        }
        return null;
    }

    public boolean onBackPressed() {
        if (!this.linkClicked) {
            return false;
        }
        loadAd();
        return true;
    }

    private class LBJSInterface {
        private LBJSInterface() {
        }

        /* synthetic */ LBJSInterface(AdController adController, LBJSInterface lBJSInterface) {
            this();
        }

        public void processHTML(String content) {
            if (content != null && content.equals("0")) {
                AdController.this.adDestroyed = true;
                if (AdController.this.listener != null) {
                    try {
                        AdLog.i(AdController.LB_LOG, "onAdFailed triggered");
                        AdController.this.listener.onAdFailed();
                    } catch (Exception e) {
                        AdLog.i(AdController.LB_LOG, "Error while calling onAdFailed");
                        AdLog.printStackTrace(AdController.LB_LOG, e);
                    }
                }
            }
            if (AdController.this.listener != null && !AdController.this.adDestroyed) {
                try {
                    AdLog.i(AdController.LB_LOG, "onAdLoaded triggered");
                    AdController.this.listener.onAdLoaded();
                    AdController.this.adLoaded = true;
                } catch (Exception e2) {
                    AdLog.i(AdController.LB_LOG, "Error while calling onAdLoaded");
                    AdLog.printStackTrace(AdController.LB_LOG, e2);
                }
            }
        }
    }

    private String md5(String s) {
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte[] messageDigest = digest.digest();
            StringBuffer hexString = new StringBuffer();
            for (byte b : messageDigest) {
                hexString.append(Integer.toHexString(b & 255));
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            AdLog.printStackTrace(LB_LOG, e);
            return "";
        }
    }
}
