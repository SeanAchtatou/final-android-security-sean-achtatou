package com.Leadbolt;

import android.content.Context;
import android.os.PowerManager;

public abstract class AdWakeLock {
    private static PowerManager.WakeLock wakeLock;

    public static void acquire(Context ctx) {
        if (wakeLock != null) {
            wakeLock.release();
        }
        try {
            wakeLock = ((PowerManager) ctx.getSystemService("power")).newWakeLock(805306394, "AdControllerWakeLock");
            wakeLock.acquire();
        } catch (Exception e) {
            AdLog.e(AdController.LB_LOG, "Error acquiring Wake Lock - " + e.getMessage());
            AdLog.printStackTrace(AdController.LB_LOG, e);
        }
    }

    public static void release() {
        try {
            if (wakeLock != null) {
                wakeLock.release();
            }
            wakeLock = null;
        } catch (Exception e) {
            AdLog.e(AdController.LB_LOG, "Error releasing Wake Lock - " + e.getMessage());
            AdLog.printStackTrace(AdController.LB_LOG, e);
            wakeLock = null;
        }
    }
}
