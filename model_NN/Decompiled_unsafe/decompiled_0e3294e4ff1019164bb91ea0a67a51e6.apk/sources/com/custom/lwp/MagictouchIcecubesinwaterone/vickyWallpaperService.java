package com.custom.lwp.MagictouchIcecubesinwaterone;

import android.service.wallpaper.WallpaperService;
import com.apperhand.device.android.AndroidSDKProvider;
import com.custom.lwp.MagictouchIcecubesinwatertwo.IvickyWallpaperService;
import com.custom.lwp.MagictouchIcecubesinwatertwo.n;

public class vickyWallpaperService extends IvickyWallpaperService {
    public vickyWallpaperService() {
        b();
    }

    public WallpaperService.Engine onCreateEngine() {
        AndroidSDKProvider.initSDK(this);
        return new n(this, getApplicationContext(), "vicky_wallpaper_preferences");
    }
}
