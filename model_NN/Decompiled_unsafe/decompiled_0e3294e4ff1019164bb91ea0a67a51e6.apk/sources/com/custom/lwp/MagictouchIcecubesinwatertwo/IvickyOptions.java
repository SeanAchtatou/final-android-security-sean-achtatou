package com.custom.lwp.MagictouchIcecubesinwatertwo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import com.custom.corelib.a;
import com.custom.corelib.d;
import com.custom.corelib.e;
import com.custom.lwp.MagictouchIcecubesinwaterone.R;
import org.codehaus.jackson.util.MinimalPrettyPrinter;

public abstract class IvickyOptions extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    private ListPreference a;
    private a b;
    private CheckBoxPreference c;
    private CharSequence[] d;
    private SharedPreferences e = null;

    private void a(String str) {
        SharedPreferences.Editor edit = this.e.edit();
        edit.putString("custom_image", str);
        edit.commit();
    }

    /* access modifiers changed from: protected */
    public abstract String a();

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i2 == -1 && this.b != null) {
            switch (i) {
                case 123:
                    a(e.a(this, intent.getData()));
                    return;
                case 124:
                    a(intent.getStringExtra("RESULT_PATH"));
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getPreferenceManager().setSharedPreferencesName(a());
        this.e = getPreferenceManager().getSharedPreferences();
        this.e.registerOnSharedPreferenceChangeListener(this);
        boolean a2 = IvickyWallpaperService.a();
        if (a2) {
            addPreferencesFromResource(R.xml.options_lite);
        } else {
            addPreferencesFromResource(R.xml.options_full);
        }
        this.a = new ListPreference(this);
        this.a.setKey("texture_type");
        this.a.setTitle((int) R.string.texture_type_title);
        this.a.setSummary((int) R.string.texture_type_summary);
        if (a2) {
            this.a.setEntries((int) R.array.texture_type_labels_lite);
            this.a.setEntryValues((int) R.array.texture_type_values_lite);
        } else {
            this.a.setEntries((int) R.array.texture_type_labels_full);
            this.a.setEntryValues((int) R.array.texture_type_values_full);
            this.d = this.a.getEntryValues();
        }
        this.a.setDefaultValue(this.a.getEntryValues()[0]);
        Preference a3 = e.a(this);
        a3.setTitle((int) R.string.view_more_apps_title);
        a3.setSummary((int) R.string.view_more_apps_summary);
        Preference b2 = e.b(this);
        if (a2) {
            getPreferenceScreen().addPreference(this.a);
            a aVar = new a(this, "market://details?id=com.custom.lwp.MagictouchClownfishcoralsone");
            aVar.setPersistent(false);
            aVar.setTitle((int) R.string.upgrade_version_title);
            aVar.setSummary((int) R.string.upgrade_version_summary);
            getPreferenceScreen().addPreference(aVar);
            getPreferenceScreen().addPreference(a3);
            getPreferenceScreen().addPreference(b2);
        } else {
            PreferenceCategory preferenceCategory = (PreferenceCategory) getPreferenceScreen().getPreference(0);
            preferenceCategory.addPreference(this.a);
            this.b = new a(this);
            this.b.setKey("custom_image");
            this.b.setTitle((int) R.string.custom_image_title);
            this.b.setDefaultValue("");
            this.b.setPersistent(true);
            preferenceCategory.addPreference(this.b);
            this.c = new CheckBoxPreference(this);
            this.c.setKey("keep_image_ratio");
            this.c.setTitle((int) R.string.keep_image_ratio_title);
            this.c.setSummary((int) R.string.keep_image_ratio_summary);
            this.c.setPersistent(true);
            this.c.setChecked(this.e.getBoolean("keep_image_ratio", false));
            preferenceCategory.addPreference(this.c);
            getPreferenceScreen().addPreference(preferenceCategory);
            d dVar = new d(this, (byte) 0);
            dVar.setTitle((int) R.string.audio_volume_title);
            dVar.setSummary((int) R.string.audio_volume_summary);
            dVar.setKey("audio_volume");
            dVar.setDefaultValue(50);
            dVar.setPersistent(true);
            ((PreferenceCategory) getPreferenceScreen().findPreference("audio_category")).addPreference(dVar);
            PreferenceCategory preferenceCategory2 = (PreferenceCategory) getPreferenceScreen().findPreference("links_category");
            preferenceCategory2.addPreference(a3);
            preferenceCategory2.addPreference(b2);
        }
        onSharedPreferenceChanged(this.e, null);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.e != null) {
            this.e.unregisterOnSharedPreferenceChangeListener(this);
        }
        super.onDestroy();
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String str) {
        if (this.b != null) {
            boolean equals = this.d != null ? sharedPreferences.getString("texture_type", "").equals(this.d[this.d.length - 1]) : false;
            this.b.setEnabled(equals);
            this.c.setEnabled(equals);
            this.b.setSummary((int) R.string.custom_image_none);
            String string = sharedPreferences.getString("custom_image", "");
            if (string.length() > 0) {
                this.b.setSummary(string);
                if (str != null && str.equals("custom_image")) {
                    try {
                        int width = getWindowManager().getDefaultDisplay().getWidth();
                        int height = getWindowManager().getDefaultDisplay().getHeight();
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inJustDecodeBounds = true;
                        BitmapFactory.decodeFile(string, options);
                        if (options.outWidth > width || options.outHeight > height) {
                            e.a(this, String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(getResources().getString(R.string.image_resolution_warning)) + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR) + String.valueOf(width)) + "x") + String.valueOf(height)) + "px");
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
            }
        }
        if (str != null && str.equals("keep_image_ratio") && sharedPreferences.getBoolean("keep_image_ratio", false)) {
            e.b(this, R.string.keep_image_ratio_warning);
        }
    }
}
