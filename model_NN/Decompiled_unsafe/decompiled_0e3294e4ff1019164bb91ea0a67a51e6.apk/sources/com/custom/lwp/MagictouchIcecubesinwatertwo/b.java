package com.custom.lwp.MagictouchIcecubesinwatertwo;

import android.os.Handler;
import com.custom.corelib.c;
import java.util.ArrayList;

final class b implements Runnable {
    private com.custom.corelib.b a = null;
    private Handler b = null;
    private ArrayList c = new ArrayList();
    private /* synthetic */ d d;

    public b(d dVar, com.custom.corelib.b bVar, Handler handler) {
        this.d = dVar;
        this.a = bVar;
        this.b = handler;
    }

    private void a(int i) {
        while (this.c.size() > i) {
            int i2 = ((e) this.c.get(0)).a;
            this.a.a(i2);
            this.a.b(i2);
            this.c.remove(0);
        }
    }

    public final void a() {
        a(0);
    }

    public final void a(int i, float f) {
        a(this.a.a());
        e eVar = new e(this);
        eVar.a = i;
        eVar.b = f;
        eVar.c = c.a();
        this.c.add(eVar);
    }

    public final void run() {
        if (this.c.size() > 0) {
            long a2 = c.a();
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= this.c.size()) {
                    break;
                }
                e eVar = (e) this.c.get(i2);
                if (a2 - eVar.c >= 150) {
                    eVar.c = a2;
                    if (eVar.b > 0.0f) {
                        if (((double) eVar.b) > 0.1d) {
                            eVar.b = (float) (((double) eVar.b) - 0.1d);
                        } else {
                            eVar.b = 0.0f;
                        }
                        com.custom.corelib.b bVar = this.a;
                        int i3 = eVar.a;
                        float f = eVar.b;
                        bVar.a(i3, f, f);
                    } else {
                        this.c.remove(i2);
                    }
                }
                i = i2 + 1;
            }
        }
        if (this.c.size() > 0) {
            this.b.postDelayed(this, 50);
        }
    }
}
