package com.custom.lwp.MagictouchIcecubesinwatertwo;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import com.custom.opengl.glwallpaperservice.m;

public final class n extends m implements SharedPreferences.OnSharedPreferenceChangeListener {
    private SharedPreferences a;
    /* access modifiers changed from: private */
    public d b;
    private /* synthetic */ IvickyWallpaperService c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public n(IvickyWallpaperService ivickyWallpaperService, Context context, String str) {
        super(ivickyWallpaperService);
        this.c = ivickyWallpaperService;
        this.b = new d(this, context);
        a(this.b);
        this.a = ivickyWallpaperService.getSharedPreferences(str, 0);
        this.a.registerOnSharedPreferenceChangeListener(this);
        onSharedPreferenceChanged(this.a, null);
    }

    public final void onCreate(SurfaceHolder surfaceHolder) {
        super.onCreate(surfaceHolder);
        setTouchEventsEnabled(true);
    }

    public final void onDestroy() {
        super.onDestroy();
        if (this.b != null) {
            this.b.b();
            this.b = null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0087  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x008c  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0108  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x010d  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0110  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onSharedPreferenceChanged(android.content.SharedPreferences r12, java.lang.String r13) {
        /*
            r11 = this;
            r4 = 32
            r3 = 16
            r10 = 50
            r7 = 0
            boolean r0 = com.custom.lwp.MagictouchIcecubesinwatertwo.IvickyWallpaperService.a()
            r1 = 0
            java.lang.String r2 = "1"
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            int r2 = r2.intValue()
            if (r0 != 0) goto L_0x0115
            java.lang.String r1 = "ripples_grid_size"
            java.lang.String r2 = java.lang.Integer.toString(r4)
            java.lang.String r1 = r12.getString(r1, r2)
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            int r1 = r1.intValue()
            java.lang.String r2 = "ripples_count"
            java.lang.String r3 = java.lang.Integer.toString(r3)
            java.lang.String r2 = r12.getString(r2, r3)
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            int r2 = r2.intValue()
            java.lang.String r3 = "audio_enabled"
            boolean r3 = r12.getBoolean(r3, r7)
            java.lang.String r4 = "audio_volume"
            int r4 = r12.getInt(r4, r10)
            float r4 = (float) r4
            r5 = 1120403456(0x42c80000, float:100.0)
            float r4 = r4 / r5
            java.lang.String r5 = "ripples_speed"
            java.lang.String r6 = "1"
            java.lang.String r5 = r12.getString(r5, r6)
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            int r5 = r5.intValue()
            r8 = r4
            r9 = r3
            r3 = r2
            r2 = r1
            r1 = r5
        L_0x0061:
            java.lang.String r4 = "texture_type"
            java.lang.String r5 = ""
            java.lang.String r4 = r12.getString(r4, r5)
            if (r4 == 0) goto L_0x0103
            java.lang.String r5 = "ice"
            boolean r5 = r4.equals(r5)
            if (r5 == 0) goto L_0x00a0
            com.custom.lwp.MagictouchIcecubesinwatertwo.g r0 = com.custom.lwp.MagictouchIcecubesinwatertwo.g.ice
            r4 = r0
        L_0x0076:
            java.lang.String r0 = "custom_image"
            java.lang.String r5 = ""
            java.lang.String r5 = r12.getString(r0, r5)
            java.lang.String r0 = "keep_image_ratio"
            boolean r6 = r12.getBoolean(r0, r7)
            switch(r1) {
                case 0: goto L_0x0108;
                case 1: goto L_0x010d;
                case 2: goto L_0x0110;
                default: goto L_0x0087;
            }
        L_0x0087:
            r7 = r10
        L_0x0088:
            com.custom.lwp.MagictouchIcecubesinwatertwo.d r0 = r11.b
            if (r0 == 0) goto L_0x0096
            com.custom.lwp.MagictouchIcecubesinwatertwo.d r0 = r11.b
            r0.a(r9)
            com.custom.lwp.MagictouchIcecubesinwatertwo.d r0 = r11.b
            r0.a(r8)
        L_0x0096:
            com.custom.lwp.MagictouchIcecubesinwatertwo.k r0 = new com.custom.lwp.MagictouchIcecubesinwatertwo.k
            r1 = r11
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            r11.a(r0)
            return
        L_0x00a0:
            java.lang.String r5 = "sky"
            boolean r5 = r4.equals(r5)
            if (r5 == 0) goto L_0x00ac
            com.custom.lwp.MagictouchIcecubesinwatertwo.g r0 = com.custom.lwp.MagictouchIcecubesinwatertwo.g.sky
            r4 = r0
            goto L_0x0076
        L_0x00ac:
            if (r0 != 0) goto L_0x0103
            java.lang.String r0 = "galaxy"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x00ba
            com.custom.lwp.MagictouchIcecubesinwatertwo.g r0 = com.custom.lwp.MagictouchIcecubesinwatertwo.g.galaxy
            r4 = r0
            goto L_0x0076
        L_0x00ba:
            java.lang.String r0 = "fire"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x00c6
            com.custom.lwp.MagictouchIcecubesinwatertwo.g r0 = com.custom.lwp.MagictouchIcecubesinwatertwo.g.fire
            r4 = r0
            goto L_0x0076
        L_0x00c6:
            java.lang.String r0 = "paper"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x00d2
            com.custom.lwp.MagictouchIcecubesinwatertwo.g r0 = com.custom.lwp.MagictouchIcecubesinwatertwo.g.paper
            r4 = r0
            goto L_0x0076
        L_0x00d2:
            java.lang.String r0 = "metal"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x00de
            com.custom.lwp.MagictouchIcecubesinwatertwo.g r0 = com.custom.lwp.MagictouchIcecubesinwatertwo.g.metal
            r4 = r0
            goto L_0x0076
        L_0x00de:
            java.lang.String r0 = "wood"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x00ea
            com.custom.lwp.MagictouchIcecubesinwatertwo.g r0 = com.custom.lwp.MagictouchIcecubesinwatertwo.g.wood
            r4 = r0
            goto L_0x0076
        L_0x00ea:
            java.lang.String r0 = "sand"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x00f6
            com.custom.lwp.MagictouchIcecubesinwatertwo.g r0 = com.custom.lwp.MagictouchIcecubesinwatertwo.g.sand
            r4 = r0
            goto L_0x0076
        L_0x00f6:
            java.lang.String r0 = "custom"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x0103
            com.custom.lwp.MagictouchIcecubesinwatertwo.g r0 = com.custom.lwp.MagictouchIcecubesinwatertwo.g.custom
            r4 = r0
            goto L_0x0076
        L_0x0103:
            com.custom.lwp.MagictouchIcecubesinwatertwo.g r0 = com.custom.lwp.MagictouchIcecubesinwatertwo.g.rocks
            r4 = r0
            goto L_0x0076
        L_0x0108:
            r0 = 30
            r7 = r0
            goto L_0x0088
        L_0x010d:
            r7 = r10
            goto L_0x0088
        L_0x0110:
            r0 = 60
            r7 = r0
            goto L_0x0088
        L_0x0115:
            r8 = r1
            r9 = r7
            r1 = r2
            r2 = r4
            goto L_0x0061
        */
        throw new UnsupportedOperationException("Method not decompiled: com.custom.lwp.MagictouchIcecubesinwatertwo.n.onSharedPreferenceChanged(android.content.SharedPreferences, java.lang.String):void");
    }

    public final void onTouchEvent(MotionEvent motionEvent) {
        super.onTouchEvent(motionEvent);
        a(new i(this, motionEvent));
    }

    public final void onVisibilityChanged(boolean z) {
        if (this.b != null) {
            this.b.b(z);
        }
        super.onVisibilityChanged(z);
    }
}
