package com.custom.lwp.MagictouchIcecubesinwatertwo;

import android.content.Context;
import android.graphics.Bitmap;
import android.opengl.GLUtils;
import android.os.Handler;
import android.os.SystemClock;
import android.view.MotionEvent;
import com.custom.corelib.e;
import com.custom.lwp.MagictouchIcecubesinwaterone.R;
import com.custom.opengl.a.a;
import com.custom.opengl.a.b;
import com.custom.opengl.glwallpaperservice.m;
import com.senddroid.AdLog;
import javax.microedition.khronos.opengles.GL10;
import org.codehaus.jackson.impl.JsonWriteContext;

public final class d implements com.custom.opengl.glwallpaperservice.d {
    private static /* synthetic */ int[] B;
    public static int a = 32;
    private static float i = 0.0f;
    private static float j = 1000.0f;
    private b A = new b();
    private m b = null;
    private Context c;
    private c d = null;
    private int e = 0;
    private int f = 0;
    private int g = 0;
    private int h = 0;
    private boolean k = false;
    private long l = 0;
    private boolean m = true;
    private com.custom.corelib.b n = null;
    private int o = 0;
    private float p = 0.0f;
    private int q = 0;
    private float r = 0.0f;
    private Handler s = null;
    private b t = null;
    private g u;
    private boolean v;
    private boolean w;
    private int[] x;
    private String y = "";
    private boolean z = false;

    public d(m mVar, Context context) {
        this.b = mVar;
        this.c = context;
        this.d = null;
        this.x = null;
        this.u = g.rocks;
        this.v = true;
        this.w = false;
    }

    private void c() {
        if (this.t != null) {
            this.t.a();
            this.t = null;
        }
        if (this.n != null) {
            this.n.d();
            this.n = null;
        }
        this.o = 0;
        this.q = 0;
    }

    private void d() {
        if (this.n != null && this.o != 0 && this.q == 0) {
            float c2 = this.n.c();
            if (c2 != 0.0f && this.p != 0.0f) {
                this.r = c2 * this.p;
                this.q = this.n.a(this.o, this.r);
            }
        }
    }

    private static /* synthetic */ int[] e() {
        int[] iArr = B;
        if (iArr == null) {
            iArr = new int[g.values().length];
            try {
                iArr[g.custom.ordinal()] = 10;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[g.fire.ordinal()] = 5;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[g.galaxy.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[g.ice.ordinal()] = 2;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[g.metal.ordinal()] = 7;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[g.paper.ordinal()] = 6;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[g.rocks.ordinal()] = 1;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[g.sand.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[g.sky.ordinal()] = 3;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[g.wood.ordinal()] = 8;
            } catch (NoSuchFieldError e11) {
            }
            B = iArr;
        }
        return iArr;
    }

    public final void a() {
        this.d = null;
    }

    public final void a(float f2) {
        this.p = f2;
    }

    public final void a(long j2) {
        this.A.a(j2);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final void a(MotionEvent motionEvent) {
        boolean z2;
        if (this.m && this.g != 0 && this.h != 0) {
            int x2 = (int) motionEvent.getX();
            int y2 = this.h - ((int) motionEvent.getY());
            if (x2 < 0) {
                x2 = 0;
            }
            if (x2 > this.g) {
                x2 = this.g;
            }
            if (y2 < 0) {
                y2 = 0;
            }
            if (y2 > this.h) {
                y2 = this.h;
            }
            switch (motionEvent.getAction()) {
                case AdLog.LOG_LEVEL_NONE /*0*/:
                    d();
                    z2 = true;
                    break;
                case 1:
                    this.k = false;
                    if (!(this.t == null || this.q == 0)) {
                        this.t.a(this.q, this.r);
                        this.q = 0;
                        this.t.run();
                    }
                    z2 = false;
                    break;
                case 2:
                    long elapsedRealtime = SystemClock.elapsedRealtime();
                    if (elapsedRealtime - this.l <= 100 || (this.e == x2 && this.f == y2)) {
                        z2 = false;
                    } else {
                        this.l = elapsedRealtime;
                        z2 = true;
                    }
                    d();
                    break;
                default:
                    z2 = false;
                    break;
            }
            if (z2) {
                this.e = x2;
                this.f = y2;
                this.k = true;
                this.b.a(1);
            }
        }
    }

    public final void a(g gVar) {
        if (this.u != gVar) {
            this.u = gVar;
            this.v = true;
        }
    }

    public final void a(String str, boolean z2) {
        String str2 = str == null ? "" : str;
        if (!this.y.equals(str2) || this.z != z2) {
            this.y = str2;
            this.z = z2;
            this.v = true;
        }
    }

    public final void a(GL10 gl10) {
        gl10.glFrontFace(2304);
        gl10.glEnable(3553);
        gl10.glEnableClientState(32884);
        gl10.glEnableClientState(32888);
        gl10.glDisable(3008);
        gl10.glDisable(2960);
        gl10.glHint(3152, 4353);
    }

    public final void a(GL10 gl10, int i2, int i3) {
        if (!(this.g == i2 && this.h == i3)) {
            this.d = null;
            this.g = i2;
            this.h = i3;
        }
        gl10.glViewport(0, 0, i2, i3);
        gl10.glMatrixMode(5889);
        gl10.glLoadIdentity();
        gl10.glOrthof(0.0f, (float) i2, 0.0f, (float) i3, i, j);
        this.b.a(1);
    }

    public final void a(boolean z2) {
        if (z2 != (this.n != null)) {
            if (!z2) {
                c();
            } else if (this.n == null) {
                this.n = new com.custom.corelib.b();
                this.n.a(this.c);
                this.o = this.n.b();
                this.s = new Handler();
                this.t = new b(this, this.n, this.s);
            }
        }
    }

    public final void b() {
        this.d = null;
        c();
    }

    public final void b(GL10 gl10) {
        Bitmap bitmap;
        int i2;
        this.A.a();
        if (this.d == null) {
            this.d = new h(a, a);
            this.d.a(gl10, this.g, this.h);
        }
        if (this.k) {
            this.k = false;
            this.d.a(this.e, this.f);
        }
        boolean a2 = this.d.a();
        gl10.glClear(16640);
        gl10.glMatrixMode(5888);
        gl10.glLoadIdentity();
        boolean z2 = this.w;
        if (this.w && e.a()) {
            this.v = true;
        }
        if (this.v) {
            this.v = false;
            if (this.x != null) {
                gl10.glDeleteTextures(this.x.length, this.x, 0);
                this.x = null;
            }
            this.x = new int[1];
            gl10.glGenTextures(this.x.length, this.x, 0);
            Context context = this.c;
            this.w = false;
            if (this.u != g.custom) {
                switch (e()[this.u.ordinal()]) {
                    case 1:
                        i2 = R.drawable.rocks;
                        break;
                    case 2:
                        i2 = R.drawable.ice;
                        break;
                    case 3:
                        i2 = R.drawable.sky;
                        break;
                    case JsonWriteContext.STATUS_EXPECT_VALUE /*4*/:
                        i2 = R.drawable.galaxy;
                        break;
                    case JsonWriteContext.STATUS_EXPECT_NAME /*5*/:
                        i2 = R.drawable.fire;
                        break;
                    case 6:
                        i2 = R.drawable.paper;
                        break;
                    case 7:
                        i2 = R.drawable.metal;
                        break;
                    case 8:
                        i2 = R.drawable.wood;
                        break;
                    case 9:
                        i2 = R.drawable.sand;
                        break;
                    default:
                        i2 = R.drawable.rocks;
                        break;
                }
                bitmap = e.a(context, i2);
            } else {
                Bitmap a3 = this.z ? e.a(this.y, this.g, this.h) : e.a(this.y, a.a(this.g), a.a(this.h));
                if (a3 != null) {
                    bitmap = a3;
                } else if (!e.a(this.y) || e.a()) {
                    bitmap = e.a(context, (int) R.drawable.invalid);
                } else {
                    bitmap = e.a(context, (int) R.drawable.loading);
                    this.w = true;
                }
            }
            if (bitmap != null) {
                gl10.glBindTexture(3553, this.x[0]);
                gl10.glTexParameterf(3553, 10240, 9729.0f);
                gl10.glTexParameterf(3553, 10241, 9729.0f);
                gl10.glTexParameterx(3553, 10242, 10497);
                gl10.glTexParameterx(3553, 10243, 10497);
                GLUtils.texImage2D(3553, 0, bitmap, 0);
                bitmap.recycle();
            }
            z2 = true;
        }
        gl10.glBindTexture(3553, this.x[0]);
        if (z2) {
            a2 = true;
        }
        this.d.a(gl10);
        if (!a2) {
            this.b.a(0);
        }
        this.A.b();
    }

    public final void b(boolean z2) {
        if (this.m != z2) {
            this.m = z2;
            if (z2) {
                this.b.a(1);
            }
        }
    }
}
