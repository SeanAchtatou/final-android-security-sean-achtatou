package com.custom.lwp.MagictouchIcecubesinwatertwo;

import java.lang.reflect.Array;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import javax.microedition.khronos.opengles.GL10;

public final class h implements c {
    public static int a = 16;
    private static int b = 18;
    private static float c = 0.125f;
    private static int d = 10;
    private int e = 0;
    private int f = 0;
    private int g = 0;
    private ArrayList h;
    private l[][] i;
    private j[][] j;
    private float[] k = b();
    private FloatBuffer l;
    private FloatBuffer m;
    private boolean n = true;
    private ShortBuffer o;
    private int p = 0;
    private int q = 0;
    private int r = 0;

    public h(int i2, int i3) {
        this.q = i2;
        this.r = i3;
        this.h = new ArrayList();
        this.i = (l[][]) Array.newInstance(l.class, this.q, this.r);
        this.j = (j[][]) Array.newInstance(j.class, this.q, this.r);
        ByteBuffer allocateDirect = ByteBuffer.allocateDirect(this.q * 2 * this.r * 4);
        allocateDirect.order(ByteOrder.nativeOrder());
        this.l = allocateDirect.asFloatBuffer();
        ByteBuffer allocateDirect2 = ByteBuffer.allocateDirect(this.q * 2 * this.r * 4);
        allocateDirect2.order(ByteOrder.nativeOrder());
        this.m = allocateDirect2.asFloatBuffer();
        ByteBuffer allocateDirect3 = ByteBuffer.allocateDirect((this.q - 1) * 6 * (this.r - 1) * 2);
        allocateDirect3.order(ByteOrder.nativeOrder());
        this.o = allocateDirect3.asShortBuffer();
    }

    private static float a(int i2, int i3, int i4, int i5) {
        return (float) Math.sqrt((double) ((((float) (i2 - i4)) * 1.0f * ((float) (i2 - i4))) + (((float) (i3 - i5)) * 1.0f * ((float) (i3 - i5)))));
    }

    private static float[] b() {
        float[] fArr = new float[2048];
        for (int i2 = 0; i2 < 2048; i2++) {
            double d2 = 1.0d - (((double) i2) / 2047.0d);
            double d3 = d2 * (((-Math.cos(2.0d * d2 * 3.1428571d * ((double) b))) * 0.5d) + 0.5d) * ((double) c) * d2 * d2 * d2 * d2 * d2 * d2 * d2;
            if (i2 == 0) {
                d3 = 0.0d;
            }
            fArr[i2] = (float) d3;
        }
        return fArr;
    }

    public final void a(int i2, int i3) {
        m mVar = new m(this, this.e);
        mVar.a = (int) (((((float) i2) * 1.0f) / ((float) this.f)) * ((float) (this.q - 1)));
        mVar.b = (int) (((((float) i3) * 1.0f) / ((float) this.g)) * ((float) (this.r - 1)));
        mVar.c = d;
        int i4 = mVar.a;
        int i5 = mVar.b;
        float a2 = a(i4, i5, 0, 0);
        float a3 = a(i4, i5, this.q, 0);
        if (a3 > a2) {
            a2 = a3;
        }
        float a4 = a(i4, i5, this.q, this.r);
        if (a4 > a2) {
            a2 = a4;
        }
        float a5 = a(i4, i5, 0, this.r);
        if (a5 <= a2) {
            a5 = a2;
        }
        mVar.d = (int) (((a5 / ((float) this.q)) * ((float) this.f)) + 341.0f);
        this.h.add(this.h.size(), mVar);
        while (this.h.size() > a) {
            this.h.remove(0);
        }
    }

    public final void a(GL10 gl10) {
        this.l.position(0);
        this.m.position(0);
        gl10.glVertexPointer(2, 5126, 0, this.l);
        gl10.glTexCoordPointer(2, 5126, 0, this.m);
        gl10.glDrawElements(4, this.p, 5123, this.o);
    }

    public final void a(GL10 gl10, int i2, int i3) {
        float f2;
        float f3;
        for (int i4 = 0; i4 < this.q; i4++) {
            for (int i5 = 0; i5 < this.r; i5++) {
                this.i[i4][i5] = new l(this);
                j jVar = new j(this);
                float f4 = ((float) i4) / ((float) (this.q - 1));
                float f5 = ((float) i5) / ((float) (this.r - 1));
                float sqrt = (float) Math.sqrt((double) ((f4 * f4) + (f5 * f5)));
                if (((double) sqrt) == 0.0d) {
                    f2 = 0.0f;
                    f3 = 0.0f;
                } else {
                    f2 = f4 / sqrt;
                    f3 = f5 / sqrt;
                }
                jVar.a[0] = f2;
                jVar.a[1] = f3;
                jVar.b = (int) (((float) i2) * sqrt * 2.0f);
                this.j[i4][i5] = jVar;
            }
        }
        this.o.position(0);
        for (int i6 = 0; i6 < this.q - 1; i6++) {
            for (int i7 = 0; i7 < this.r - 1; i7++) {
                short[] sArr = {(short) ((this.q * i6) + i7), (short) ((this.q * i6) + i7 + 1), (short) ((this.q * i6) + this.r + i7), (short) ((this.q * i6) + this.r + i7), (short) ((this.q * i6) + i7 + 1), (short) ((this.q * i6) + this.r + i7 + 1)};
                this.o.put(sArr);
                this.p = sArr.length + this.p;
            }
        }
        this.o.position(0);
        this.f = i2;
        this.g = i3;
        gl10.glDisable(2929);
        this.e = (int) Math.sqrt((double) ((i3 * i3) + (i2 * i2)));
        for (int i8 = 0; i8 < this.q; i8++) {
            for (int i9 = 0; i9 < this.r; i9++) {
                this.l.put((((float) i8) / (((float) this.q) - 1.0f)) * ((float) i2));
                this.l.put((((float) i9) / (((float) this.r) - 1.0f)) * ((float) i3));
                this.i[i8][i9].a[0] = ((float) i8) / (((float) this.q) - 1.0f);
                this.i[i8][i9].a[1] = -(((float) i9) / (((float) this.r) - 1.0f));
            }
        }
    }

    public final boolean a() {
        int i2;
        float f2;
        int i3;
        float f3;
        boolean z = this.h.size() > 0;
        if (z) {
            this.n = true;
            int i4 = 0;
            while (i4 < this.h.size()) {
                m mVar = (m) this.h.get(i4);
                if (mVar.c > mVar.d * 2) {
                    this.h.remove(i4);
                } else {
                    mVar.c += d;
                    i4++;
                }
            }
        }
        this.m.position(0);
        if (this.n) {
            int i5 = 0;
            while (true) {
                int i6 = i5;
                if (i6 >= this.q) {
                    break;
                }
                int i7 = 0;
                while (true) {
                    int i8 = i7;
                    if (i8 >= this.r) {
                        break;
                    }
                    float f4 = this.i[i6][i8].a[0];
                    float f5 = this.i[i6][i8].a[1];
                    int i9 = 0;
                    float f6 = f4;
                    while (i9 < this.h.size()) {
                        m mVar2 = (m) this.h.get(i9);
                        int i10 = i6 - mVar2.a;
                        int i11 = i8 - mVar2.b;
                        if (i10 < 0) {
                            i2 = i10 * -1;
                            f2 = -1.0f;
                        } else {
                            i2 = i10;
                            f2 = 1.0f;
                        }
                        if (i11 < 0) {
                            i3 = i11 * -1;
                            f3 = -1.0f;
                        } else {
                            i3 = i11;
                            f3 = 1.0f;
                        }
                        if (i2 > this.q - 1) {
                            i2 = this.q - 1;
                        }
                        if (i3 > this.r - 1) {
                            i3 = this.r - 1;
                        }
                        int i12 = mVar2.c - this.j[i2][i3].b;
                        if (i12 < 0) {
                            i12 = 0;
                        }
                        if (i12 > 2047) {
                            i12 = 2047;
                        }
                        float f7 = 1.0f - ((((float) mVar2.c) * 1.0f) / 2048.0f);
                        float f8 = f7 * f7;
                        if (f8 < 0.0f) {
                            f8 = 0.0f;
                        }
                        f6 += f2 * this.j[i2][i3].a[0] * this.k[i12] * f8;
                        i9++;
                        f5 = (f8 * this.j[i2][i3].a[1] * f3 * this.k[i12]) + f5;
                    }
                    this.m.put(f6);
                    this.m.put(f5);
                    i7 = i8 + 1;
                }
                i5 = i6 + 1;
            }
            this.n = false;
        }
        return z;
    }
}
