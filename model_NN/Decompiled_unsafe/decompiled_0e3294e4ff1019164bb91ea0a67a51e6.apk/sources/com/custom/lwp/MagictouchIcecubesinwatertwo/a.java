package com.custom.lwp.MagictouchIcecubesinwatertwo;

import android.app.AlertDialog;
import android.content.Intent;
import android.preference.Preference;
import android.provider.MediaStore;
import com.custom.corelib.e;
import com.custom.lwp.MagictouchIcecubesinwaterone.R;
import com.custom.uilib.FilePicker;
import java.util.ArrayList;

public final class a extends Preference {
    private /* synthetic */ IvickyOptions a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public a(IvickyOptions ivickyOptions) {
        super(ivickyOptions);
        this.a = ivickyOptions;
    }

    static /* synthetic */ void a(a aVar, boolean z) {
        if (z) {
            try {
                aVar.a.startActivityForResult(new Intent("android.intent.action.PICK", MediaStore.Images.Media.INTERNAL_CONTENT_URI), 123);
            } catch (Exception e) {
                e.a(aVar.a, e.getMessage());
            }
        } else {
            Intent intent = new Intent(aVar.a, FilePicker.class);
            intent.putExtra("START_PATH", e.a() ? e.b() : "/");
            intent.putExtra("SELECTION_MODE", 0);
            aVar.a.startActivityForResult(intent, 124);
        }
    }

    /* access modifiers changed from: protected */
    public final void onClick() {
        super.onClick();
        ArrayList arrayList = new ArrayList();
        arrayList.add(this.a.getResources().getString(R.string.options_image_picker_open_gallery));
        arrayList.add(this.a.getResources().getString(R.string.options_image_picker_browse_files));
        AlertDialog.Builder builder = new AlertDialog.Builder(this.a);
        builder.setItems((CharSequence[]) arrayList.toArray(new String[arrayList.size()]), new f(this)).create();
        builder.show();
    }
}
