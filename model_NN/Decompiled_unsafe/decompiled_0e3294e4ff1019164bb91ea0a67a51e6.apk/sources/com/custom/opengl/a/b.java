package com.custom.opengl.a;

public final class b {
    private final long a = 1;
    private long b;
    private long c = 0;

    public b() {
        a(50);
    }

    public final void a() {
        this.c = System.currentTimeMillis();
    }

    public final void a(long j) {
        this.b = 1000 / j;
    }

    public final void b() {
        long currentTimeMillis = (System.currentTimeMillis() + 1) - this.c;
        if (currentTimeMillis < this.b) {
            try {
                Thread.sleep(this.b - currentTimeMillis);
            } catch (InterruptedException e) {
            }
        }
    }
}
