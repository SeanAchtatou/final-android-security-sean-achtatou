package com.custom.opengl.glwallpaperservice;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLDisplay;

public class c extends i {
    protected int a = 4;
    protected int b = 4;
    protected int c = 4;
    private int[] d = new int[1];
    private int e = 0;
    private int f = 16;
    private int g = 0;

    public c() {
        super(new int[]{12324, 4, 12323, 4, 12322, 4, 12321, 0, 12325, 16, 12326, 0, 12344});
    }

    private int a(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig eGLConfig, int i) {
        if (egl10.eglGetConfigAttrib(eGLDisplay, eGLConfig, i, this.d)) {
            return this.d[0];
        }
        return 0;
    }

    public /* bridge */ /* synthetic */ EGLConfig a(EGL10 egl10, EGLDisplay eGLDisplay) {
        return super.a(egl10, eGLDisplay);
    }

    public final EGLConfig a(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig[] eGLConfigArr) {
        int i = 1000;
        EGLConfig eGLConfig = null;
        for (EGLConfig eGLConfig2 : eGLConfigArr) {
            int a2 = a(egl10, eGLDisplay, eGLConfig2, 12325);
            int a3 = a(egl10, eGLDisplay, eGLConfig2, 12326);
            if (a2 >= this.f && a3 >= this.g) {
                int abs = Math.abs(a(egl10, eGLDisplay, eGLConfig2, 12324) - this.a) + Math.abs(a(egl10, eGLDisplay, eGLConfig2, 12323) - this.b) + Math.abs(a(egl10, eGLDisplay, eGLConfig2, 12322) - this.c) + Math.abs(a(egl10, eGLDisplay, eGLConfig2, 12321) - this.e);
                if (abs < i) {
                    i = abs;
                    eGLConfig = eGLConfig2;
                }
            }
        }
        return eGLConfig;
    }
}
