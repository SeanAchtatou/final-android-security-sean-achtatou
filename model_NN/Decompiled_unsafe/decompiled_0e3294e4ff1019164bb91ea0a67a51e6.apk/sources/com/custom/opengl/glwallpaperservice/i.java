package com.custom.opengl.glwallpaperservice;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLDisplay;

abstract class i implements k {
    private int[] a;

    public i(int[] iArr) {
        this.a = iArr;
    }

    public EGLConfig a(EGL10 egl10, EGLDisplay eGLDisplay) {
        int[] iArr = new int[1];
        egl10.eglChooseConfig(eGLDisplay, this.a, null, 0, iArr);
        int i = iArr[0];
        if (i <= 0) {
            throw new IllegalArgumentException("No configs match configSpec");
        }
        EGLConfig[] eGLConfigArr = new EGLConfig[i];
        egl10.eglChooseConfig(eGLDisplay, this.a, eGLConfigArr, i, iArr);
        EGLConfig a2 = a(egl10, eGLDisplay, eGLConfigArr);
        if (a2 != null) {
            return a2;
        }
        throw new IllegalArgumentException("No config chosen");
    }

    /* access modifiers changed from: package-private */
    public abstract EGLConfig a(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig[] eGLConfigArr);
}
