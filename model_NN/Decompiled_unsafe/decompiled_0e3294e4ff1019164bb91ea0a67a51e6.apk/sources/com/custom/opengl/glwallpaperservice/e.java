package com.custom.opengl.glwallpaperservice;

final class e {
    private /* synthetic */ l a;

    /* synthetic */ e(l lVar) {
        this(lVar, (byte) 0);
    }

    private e(l lVar, byte b) {
        this.a = lVar;
    }

    public final synchronized void a(l lVar) {
        lVar.a = true;
        if (this.a.c == lVar) {
            this.a.c = (l) null;
        }
        notifyAll();
    }

    public final synchronized boolean b(l lVar) {
        boolean z;
        if (this.a.c == lVar || this.a.c == null) {
            this.a.c = lVar;
            notifyAll();
            z = true;
        } else {
            z = false;
        }
        return z;
    }

    public final synchronized void c(l lVar) {
        if (this.a.c == lVar) {
            this.a.c = (l) null;
        }
        notifyAll();
    }
}
