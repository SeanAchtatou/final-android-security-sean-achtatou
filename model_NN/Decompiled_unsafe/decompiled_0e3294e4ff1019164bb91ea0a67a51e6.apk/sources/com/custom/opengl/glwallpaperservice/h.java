package com.custom.opengl.glwallpaperservice;

import android.util.Log;
import android.view.SurfaceHolder;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;
import javax.microedition.khronos.opengles.GL;

final class h {
    private EGL10 a;
    private EGLDisplay b;
    private EGLSurface c;
    private EGLContext d;
    private EGLConfig e;
    private k f;
    private f g;
    private j h;
    private a i;

    public h(k kVar, f fVar, j jVar, a aVar) {
        this.f = kVar;
        this.g = fVar;
        this.h = jVar;
        this.i = aVar;
    }

    public final GL a(SurfaceHolder surfaceHolder) {
        if (!(this.c == null || this.c == EGL10.EGL_NO_SURFACE)) {
            this.a.eglMakeCurrent(this.b, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
            this.h.a(this.a, this.b, this.c);
        }
        this.c = this.h.a(this.a, this.b, this.e, surfaceHolder);
        if (this.c == null || this.c == EGL10.EGL_NO_SURFACE) {
            throw new RuntimeException("createWindowSurface failed");
        } else if (!this.a.eglMakeCurrent(this.b, this.c, this.c, this.d)) {
            throw new RuntimeException("eglMakeCurrent failed.");
        } else {
            return this.i != null ? this.i.a() : this.d.getGL();
        }
    }

    public final void a() {
        Log.d("EglHelperGLWallpaper", "start()");
        if (this.a == null) {
            Log.d("EglHelperGLWallpaper", "getting new EGL");
            this.a = (EGL10) EGLContext.getEGL();
        } else {
            Log.d("EglHelperGLWallpaper", "reusing EGL");
        }
        if (this.b == null) {
            Log.d("EglHelperGLWallpaper", "getting new display");
            this.b = this.a.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
        } else {
            Log.d("EglHelperGLWallpaper", "reusing display");
        }
        if (this.e == null) {
            Log.d("EglHelperGLWallpaper", "getting new config");
            this.a.eglInitialize(this.b, new int[2]);
            this.e = this.f.a(this.a, this.b);
        } else {
            Log.d("EglHelperGLWallpaper", "reusing config");
        }
        if (this.d == null) {
            Log.d("EglHelperGLWallpaper", "creating new context");
            this.d = this.g.a(this.a, this.b, this.e);
            if (this.d == null || this.d == EGL10.EGL_NO_CONTEXT) {
                throw new RuntimeException("createContext failed");
            }
        } else {
            Log.d("EglHelperGLWallpaper", "reusing context");
        }
        this.c = null;
    }

    public final boolean b() {
        this.a.eglSwapBuffers(this.b, this.c);
        return this.a.eglGetError() != 12302;
    }

    public final void c() {
        if (this.c != null && this.c != EGL10.EGL_NO_SURFACE) {
            this.a.eglMakeCurrent(this.b, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
            this.h.a(this.a, this.b, this.c);
            this.c = null;
        }
    }

    public final void d() {
        if (this.d != null) {
            this.g.a(this.a, this.b, this.d);
            this.d = null;
        }
        if (this.b != null) {
            this.a.eglTerminate(this.b);
            this.b = null;
        }
    }
}
