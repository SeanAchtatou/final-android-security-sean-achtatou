package com.custom.opengl.glwallpaperservice;

import android.service.wallpaper.WallpaperService;
import android.view.SurfaceHolder;

public class m extends WallpaperService.Engine {
    private l a = null;
    private k b;
    private f c;
    private j d;
    private a e;
    private /* synthetic */ GLWallpaperService f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public m(GLWallpaperService gLWallpaperService) {
        super(gLWallpaperService);
        this.f = gLWallpaperService;
    }

    public final void a(int i) {
        if (this.a != null) {
            this.a.a(i);
        }
    }

    public final void a(d dVar) {
        if (this.a != null) {
            throw new IllegalStateException("setRenderer has already been called for this instance");
        }
        if (this.b == null) {
            this.b = new g();
        }
        if (this.c == null) {
            this.c = new n();
        }
        if (this.d == null) {
            this.d = new b();
        }
        this.a = new l(dVar, this.b, this.c, this.d, this.e);
        this.a.start();
    }

    public final void a(Runnable runnable) {
        this.a.a(runnable);
    }

    public void onCreate(SurfaceHolder surfaceHolder) {
        super.onCreate(surfaceHolder);
    }

    public void onDestroy() {
        super.onDestroy();
        this.a.d();
    }

    public void onSurfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        this.a.a(i2, i3);
        super.onSurfaceChanged(surfaceHolder, i, i2, i3);
    }

    public void onSurfaceCreated(SurfaceHolder surfaceHolder) {
        this.a.a(surfaceHolder);
        super.onSurfaceCreated(surfaceHolder);
    }

    public void onSurfaceDestroyed(SurfaceHolder surfaceHolder) {
        this.a.a();
        super.onSurfaceDestroyed(surfaceHolder);
    }

    public void onVisibilityChanged(boolean z) {
        if (z) {
            this.a.c();
        } else {
            this.a.b();
        }
        super.onVisibilityChanged(z);
    }
}
