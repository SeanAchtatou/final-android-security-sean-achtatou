package com.custom.opengl.glwallpaperservice;

import android.view.SurfaceHolder;
import java.util.ArrayList;

public final class l extends Thread {
    public boolean a = false;
    private final e b = new e(this);
    /* access modifiers changed from: private */
    public l c;
    private k d;
    private f e;
    private j f;
    private a g;
    private SurfaceHolder h;
    private boolean i = true;
    private boolean j;
    private boolean k;
    private boolean l;
    private boolean m;
    private int n = 0;
    private int o = 0;
    private int p = 1;
    private boolean q = true;
    private boolean r;
    private d s;
    private ArrayList t = new ArrayList();
    private h u;

    l(d dVar, k kVar, f fVar, j jVar, a aVar) {
        this.s = dVar;
        this.d = kVar;
        this.e = fVar;
        this.f = jVar;
        this.g = aVar;
    }

    private void e() {
        if (this.m) {
            this.m = false;
            this.u.c();
            this.b.c(this);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:0x0117, code lost:
        if (r8 <= 0) goto L_0x0125;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x0119, code lost:
        if (r7 <= 0) goto L_0x0125;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x011b, code lost:
        r12.s.b(r1);
        r12.u.b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x0125, code lost:
        r11 = r2;
        r2 = r1;
        r1 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x0130, code lost:
        r1 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x0132, code lost:
        r3 = r1;
        r1 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x013a, code lost:
        r2 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x0015, code lost:
        continue;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:139:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0080, code lost:
        if (r5 == false) goto L_0x00f5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:?, code lost:
        r3 = h();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0086, code lost:
        if (r3 == null) goto L_0x0015;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0088, code lost:
        r3.run();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x008f, code lost:
        if (g() == false) goto L_0x0082;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0091, code lost:
        r0 = r12.b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0093, code lost:
        monitor-enter(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:?, code lost:
        e();
        r12.u.d();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x009c, code lost:
        monitor-exit(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x00e7, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x00ea, code lost:
        monitor-enter(r12.b);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:?, code lost:
        e();
        r12.u.d();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x00f4, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x00f5, code lost:
        if (r4 == false) goto L_0x0132;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x00f7, code lost:
        r1 = true;
        r3 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x00f9, code lost:
        if (r1 == false) goto L_0x0130;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:?, code lost:
        r1 = (javax.microedition.khronos.opengles.GL10) r12.u.a(r12.h);
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x0107, code lost:
        if (r3 == false) goto L_0x013a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x0109, code lost:
        r12.s.a(r1);
        r2 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x010f, code lost:
        if (r0 == false) goto L_0x0117;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x0111, code lost:
        r12.s.a(r1, r8, r7);
        r0 = false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void f() {
        /*
            r12 = this;
            r10 = 0
            r9 = 1
            com.custom.opengl.glwallpaperservice.h r0 = new com.custom.opengl.glwallpaperservice.h
            com.custom.opengl.glwallpaperservice.k r1 = r12.d
            com.custom.opengl.glwallpaperservice.f r2 = r12.e
            com.custom.opengl.glwallpaperservice.j r3 = r12.f
            com.custom.opengl.glwallpaperservice.a r4 = r12.g
            r0.<init>(r1, r2, r3, r4)
            r12.u = r0
            r0 = 0
            r1 = r9
            r2 = r0
            r0 = r9
        L_0x0015:
            boolean r3 = r12.g()     // Catch:{ all -> 0x00e7 }
            if (r3 == 0) goto L_0x0028
            com.custom.opengl.glwallpaperservice.e r0 = r12.b
            monitor-enter(r0)
            r12.e()     // Catch:{ all -> 0x012d }
            com.custom.opengl.glwallpaperservice.h r1 = r12.u     // Catch:{ all -> 0x012d }
            r1.d()     // Catch:{ all -> 0x012d }
            monitor-exit(r0)     // Catch:{ all -> 0x012d }
        L_0x0027:
            return
        L_0x0028:
            com.custom.opengl.glwallpaperservice.e r3 = r12.b     // Catch:{ all -> 0x00e7 }
            monitor-enter(r3)     // Catch:{ all -> 0x00e7 }
            r4 = r10
        L_0x002c:
            boolean r5 = r12.j     // Catch:{ all -> 0x00e4 }
            if (r5 == 0) goto L_0x0033
            r12.e()     // Catch:{ all -> 0x00e4 }
        L_0x0033:
            boolean r5 = r12.k     // Catch:{ all -> 0x00e4 }
            if (r5 != 0) goto L_0x005b
            boolean r5 = r12.l     // Catch:{ all -> 0x00e4 }
            if (r5 != 0) goto L_0x0046
            r12.e()     // Catch:{ all -> 0x00e4 }
            r5 = 1
            r12.l = r5     // Catch:{ all -> 0x00e4 }
            com.custom.opengl.glwallpaperservice.e r5 = r12.b     // Catch:{ all -> 0x00e4 }
            r5.notifyAll()     // Catch:{ all -> 0x00e4 }
        L_0x0046:
            boolean r5 = r12.a     // Catch:{ all -> 0x00e4 }
            if (r5 == 0) goto L_0x0074
            monitor-exit(r3)     // Catch:{ all -> 0x00e4 }
            com.custom.opengl.glwallpaperservice.e r0 = r12.b
            monitor-enter(r0)
            r12.e()     // Catch:{ all -> 0x0058 }
            com.custom.opengl.glwallpaperservice.h r1 = r12.u     // Catch:{ all -> 0x0058 }
            r1.d()     // Catch:{ all -> 0x0058 }
            monitor-exit(r0)     // Catch:{ all -> 0x0058 }
            goto L_0x0027
        L_0x0058:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0058 }
            throw r1
        L_0x005b:
            boolean r5 = r12.m     // Catch:{ all -> 0x00e4 }
            if (r5 != 0) goto L_0x0046
            com.custom.opengl.glwallpaperservice.e r5 = r12.b     // Catch:{ all -> 0x00e4 }
            boolean r5 = r5.b(r12)     // Catch:{ all -> 0x00e4 }
            if (r5 == 0) goto L_0x0046
            r4 = 1
            r12.m = r4     // Catch:{ all -> 0x00e4 }
            com.custom.opengl.glwallpaperservice.h r4 = r12.u     // Catch:{ all -> 0x00e4 }
            r4.a()     // Catch:{ all -> 0x00e4 }
            r4 = 1
            r12.q = r4     // Catch:{ all -> 0x00e4 }
            r4 = r9
            goto L_0x0046
        L_0x0074:
            boolean r5 = r12.r     // Catch:{ all -> 0x00e4 }
            if (r5 == 0) goto L_0x00a1
            r5 = 0
            r12.r = r5     // Catch:{ all -> 0x00e4 }
            r5 = r9
            r6 = r10
            r7 = r10
            r8 = r10
        L_0x007f:
            monitor-exit(r3)     // Catch:{ all -> 0x00e4 }
            if (r5 == 0) goto L_0x00f5
        L_0x0082:
            java.lang.Runnable r3 = r12.h()     // Catch:{ all -> 0x00e7 }
            if (r3 == 0) goto L_0x0015
            r3.run()     // Catch:{ all -> 0x00e7 }
            boolean r3 = r12.g()     // Catch:{ all -> 0x00e7 }
            if (r3 == 0) goto L_0x0082
            com.custom.opengl.glwallpaperservice.e r0 = r12.b
            monitor-enter(r0)
            r12.e()     // Catch:{ all -> 0x009e }
            com.custom.opengl.glwallpaperservice.h r1 = r12.u     // Catch:{ all -> 0x009e }
            r1.d()     // Catch:{ all -> 0x009e }
            monitor-exit(r0)     // Catch:{ all -> 0x009e }
            goto L_0x0027
        L_0x009e:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x009e }
            throw r1
        L_0x00a1:
            boolean r5 = r12.j     // Catch:{ all -> 0x00e4 }
            if (r5 != 0) goto L_0x00dd
            boolean r5 = r12.k     // Catch:{ all -> 0x00e4 }
            if (r5 == 0) goto L_0x00dd
            boolean r5 = r12.m     // Catch:{ all -> 0x00e4 }
            if (r5 == 0) goto L_0x00dd
            int r5 = r12.n     // Catch:{ all -> 0x00e4 }
            if (r5 <= 0) goto L_0x00dd
            int r5 = r12.o     // Catch:{ all -> 0x00e4 }
            if (r5 <= 0) goto L_0x00dd
            boolean r5 = r12.q     // Catch:{ all -> 0x00e4 }
            if (r5 != 0) goto L_0x00bd
            int r5 = r12.p     // Catch:{ all -> 0x00e4 }
            if (r5 != r9) goto L_0x00dd
        L_0x00bd:
            boolean r5 = r12.i     // Catch:{ all -> 0x00e4 }
            int r6 = r12.n     // Catch:{ all -> 0x00e4 }
            int r7 = r12.o     // Catch:{ all -> 0x00e4 }
            r8 = 0
            r12.i = r8     // Catch:{ all -> 0x00e4 }
            r8 = 0
            r12.q = r8     // Catch:{ all -> 0x00e4 }
            boolean r8 = r12.k     // Catch:{ all -> 0x00e4 }
            if (r8 == 0) goto L_0x0135
            boolean r8 = r12.l     // Catch:{ all -> 0x00e4 }
            if (r8 == 0) goto L_0x0135
            r5 = 0
            r12.l = r5     // Catch:{ all -> 0x00e4 }
            com.custom.opengl.glwallpaperservice.e r5 = r12.b     // Catch:{ all -> 0x00e4 }
            r5.notifyAll()     // Catch:{ all -> 0x00e4 }
            r5 = r10
            r8 = r6
            r6 = r9
            goto L_0x007f
        L_0x00dd:
            com.custom.opengl.glwallpaperservice.e r5 = r12.b     // Catch:{ all -> 0x00e4 }
            r5.wait()     // Catch:{ all -> 0x00e4 }
            goto L_0x002c
        L_0x00e4:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x00e4 }
            throw r0     // Catch:{ all -> 0x00e7 }
        L_0x00e7:
            r0 = move-exception
            com.custom.opengl.glwallpaperservice.e r1 = r12.b
            monitor-enter(r1)
            r12.e()     // Catch:{ all -> 0x012a }
            com.custom.opengl.glwallpaperservice.h r2 = r12.u     // Catch:{ all -> 0x012a }
            r2.d()     // Catch:{ all -> 0x012a }
            monitor-exit(r1)     // Catch:{ all -> 0x012a }
            throw r0
        L_0x00f5:
            if (r4 == 0) goto L_0x0132
            r1 = r9
            r3 = r9
        L_0x00f9:
            if (r1 == 0) goto L_0x0130
            com.custom.opengl.glwallpaperservice.h r0 = r12.u     // Catch:{ all -> 0x00e7 }
            android.view.SurfaceHolder r1 = r12.h     // Catch:{ all -> 0x00e7 }
            javax.microedition.khronos.opengles.GL r0 = r0.a(r1)     // Catch:{ all -> 0x00e7 }
            javax.microedition.khronos.opengles.GL10 r0 = (javax.microedition.khronos.opengles.GL10) r0     // Catch:{ all -> 0x00e7 }
            r1 = r0
            r0 = r9
        L_0x0107:
            if (r3 == 0) goto L_0x013a
            com.custom.opengl.glwallpaperservice.d r2 = r12.s     // Catch:{ all -> 0x00e7 }
            r2.a(r1)     // Catch:{ all -> 0x00e7 }
            r2 = r10
        L_0x010f:
            if (r0 == 0) goto L_0x0117
            com.custom.opengl.glwallpaperservice.d r0 = r12.s     // Catch:{ all -> 0x00e7 }
            r0.a(r1, r8, r7)     // Catch:{ all -> 0x00e7 }
            r0 = r10
        L_0x0117:
            if (r8 <= 0) goto L_0x0125
            if (r7 <= 0) goto L_0x0125
            com.custom.opengl.glwallpaperservice.d r3 = r12.s     // Catch:{ all -> 0x00e7 }
            r3.b(r1)     // Catch:{ all -> 0x00e7 }
            com.custom.opengl.glwallpaperservice.h r3 = r12.u     // Catch:{ all -> 0x00e7 }
            r3.b()     // Catch:{ all -> 0x00e7 }
        L_0x0125:
            r11 = r2
            r2 = r1
            r1 = r11
            goto L_0x0015
        L_0x012a:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x012a }
            throw r0
        L_0x012d:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x012d }
            throw r1
        L_0x0130:
            r1 = r2
            goto L_0x0107
        L_0x0132:
            r3 = r1
            r1 = r6
            goto L_0x00f9
        L_0x0135:
            r8 = r6
            r6 = r5
            r5 = r10
            goto L_0x007f
        L_0x013a:
            r2 = r3
            goto L_0x010f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.custom.opengl.glwallpaperservice.l.f():void");
    }

    private boolean g() {
        boolean z;
        synchronized (this.b) {
            z = this.a;
        }
        return z;
    }

    private Runnable h() {
        synchronized (this) {
            if (this.t.size() <= 0) {
                return null;
            }
            Runnable runnable = (Runnable) this.t.remove(0);
            return runnable;
        }
    }

    public final void a() {
        synchronized (this.b) {
            this.k = false;
            this.b.notifyAll();
            while (!this.l && isAlive() && !this.a) {
                try {
                    this.b.wait();
                } catch (InterruptedException e2) {
                    Thread.currentThread().interrupt();
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(int r4) {
        /*
            r3 = this;
            r2 = 1
            if (r4 < 0) goto L_0x0005
            if (r4 <= r2) goto L_0x000d
        L_0x0005:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "renderMode"
            r0.<init>(r1)
            throw r0
        L_0x000d:
            com.custom.opengl.glwallpaperservice.e r0 = r3.b
            monitor-enter(r0)
            int r1 = r3.p     // Catch:{ all -> 0x0028 }
            if (r1 != r4) goto L_0x0016
            monitor-exit(r0)     // Catch:{ all -> 0x0028 }
        L_0x0015:
            return
        L_0x0016:
            r3.p = r4     // Catch:{ all -> 0x0028 }
            if (r4 != r2) goto L_0x002b
            java.lang.String r1 = "GLThread"
            java.lang.String r2 = "RENDERMODE_CONTINUOUSLY"
            android.util.Log.i(r1, r2)     // Catch:{ all -> 0x0028 }
            com.custom.opengl.glwallpaperservice.e r1 = r3.b     // Catch:{ all -> 0x0028 }
            r1.notifyAll()     // Catch:{ all -> 0x0028 }
        L_0x0026:
            monitor-exit(r0)     // Catch:{ all -> 0x0028 }
            goto L_0x0015
        L_0x0028:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0028 }
            throw r1
        L_0x002b:
            java.lang.String r1 = "GLThread"
            java.lang.String r2 = "RENDERMODE_WHEN_DIRTY"
            android.util.Log.i(r1, r2)     // Catch:{ all -> 0x0028 }
            goto L_0x0026
        */
        throw new UnsupportedOperationException("Method not decompiled: com.custom.opengl.glwallpaperservice.l.a(int):void");
    }

    public final void a(int i2, int i3) {
        synchronized (this.b) {
            this.n = i2;
            this.o = i3;
            this.i = true;
            this.b.notifyAll();
        }
    }

    public final void a(SurfaceHolder surfaceHolder) {
        this.h = surfaceHolder;
        synchronized (this.b) {
            this.k = true;
            this.b.notifyAll();
        }
    }

    public final void a(Runnable runnable) {
        synchronized (this) {
            this.t.add(runnable);
            synchronized (this.b) {
                this.r = true;
                this.b.notifyAll();
            }
        }
    }

    public final void b() {
        synchronized (this.b) {
            this.j = true;
            this.b.notifyAll();
        }
    }

    public final void c() {
        synchronized (this.b) {
            this.j = false;
            this.q = true;
            this.b.notifyAll();
        }
    }

    public final void d() {
        synchronized (this.b) {
            this.a = true;
            this.b.notifyAll();
        }
        try {
            join();
        } catch (InterruptedException e2) {
            Thread.currentThread().interrupt();
        }
    }

    public final void run() {
        setName("GLThread " + getId());
        try {
            f();
        } catch (InterruptedException e2) {
        } finally {
            this.b.a(this);
        }
    }
}
