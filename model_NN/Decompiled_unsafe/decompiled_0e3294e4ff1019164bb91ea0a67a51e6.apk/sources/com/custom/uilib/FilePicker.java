package com.custom.uilib;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.custom.corelib.e;
import com.custom.lwp.MagictouchIcecubesinwaterone.R;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

public class FilePicker extends ListActivity {
    private List a = null;
    private TextView b;
    private ArrayList c;
    private LinearLayout d;
    private String e;
    /* access modifiers changed from: private */
    public String f = "/";
    private int g = 0;
    private File h;
    private HashMap i = new HashMap();

    private void a() {
        if (this.h != null) {
            setResult(-1, getIntent());
            getIntent().putExtra("RESULT_PATH", this.h.getPath());
        } else {
            setResult(0, getIntent());
        }
        finish();
    }

    static /* synthetic */ void a(FilePicker filePicker, String str) {
        filePicker.h = new File(str);
        filePicker.a();
    }

    private void a(String str) {
        File file;
        File[] fileArr;
        boolean z = str.length() < this.f.length();
        Integer num = (Integer) this.i.get(this.e);
        this.f = str;
        ArrayList arrayList = new ArrayList();
        this.a = new ArrayList();
        this.c = new ArrayList();
        File file2 = new File(this.f);
        File[] listFiles = file2.listFiles();
        if (listFiles == null) {
            this.f = "/";
            File file3 = new File(this.f);
            file = file3;
            fileArr = file3.listFiles();
        } else {
            File[] fileArr2 = listFiles;
            file = file2;
            fileArr = fileArr2;
        }
        this.b.setText(((Object) getText(R.string.file_picker_location)) + ": " + this.f);
        if (!this.f.equals("/")) {
            arrayList.add("/");
            a("/", R.drawable.file_picker_folder);
            this.a.add("/");
            arrayList.add("../");
            a("../", R.drawable.file_picker_folder);
            this.a.add(file.getParent());
            this.e = file.getParent();
        }
        TreeMap treeMap = new TreeMap();
        TreeMap treeMap2 = new TreeMap();
        TreeMap treeMap3 = new TreeMap();
        TreeMap treeMap4 = new TreeMap();
        for (File file4 : fileArr) {
            if (file4.isDirectory()) {
                String name = file4.getName();
                treeMap.put(name, name);
                treeMap2.put(name, file4.getPath());
            } else {
                treeMap3.put(file4.getName(), file4.getName());
                treeMap4.put(file4.getName(), file4.getPath());
            }
        }
        arrayList.addAll(treeMap.tailMap("").values());
        arrayList.addAll(treeMap3.tailMap("").values());
        this.a.addAll(treeMap2.tailMap("").values());
        this.a.addAll(treeMap4.tailMap("").values());
        SimpleAdapter simpleAdapter = new SimpleAdapter(this, this.c, R.layout.file_picker_item, new String[]{"key", "image"}, new int[]{R.id.file_picker_item_text, R.id.file_picker_item_image});
        for (String a2 : treeMap.tailMap("").values()) {
            a(a2, R.drawable.file_picker_folder);
        }
        for (String a3 : treeMap3.tailMap("").values()) {
            a(a3, R.drawable.file_picker_file);
        }
        simpleAdapter.notifyDataSetChanged();
        setListAdapter(simpleAdapter);
        if (num != null && z) {
            getListView().setSelection(num.intValue());
        }
    }

    private void a(String str, int i2) {
        HashMap hashMap = new HashMap();
        hashMap.put("key", str);
        hashMap.put("image", Integer.valueOf(i2));
        this.c.add(hashMap);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setResult(0, getIntent());
        setContentView((int) R.layout.file_picker);
        this.b = (TextView) findViewById(R.id.file_picker_location);
        this.g = getIntent().getIntExtra("SELECTION_MODE", 0);
        this.d = (LinearLayout) findViewById(R.id.file_picker_save_layout);
        ((Button) findViewById(R.id.file_picker_button_save)).setOnClickListener(new a(this));
        String stringExtra = getIntent().getStringExtra("START_PATH");
        if (e.b(stringExtra)) {
            stringExtra = "/";
        }
        a(stringExtra);
        if (this.g == 0) {
            this.d.setVisibility(8);
        } else {
            this.d.setVisibility(0);
        }
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView listView, View view, int i2, long j) {
        File file = new File((String) this.a.get(i2));
        if (!file.isDirectory()) {
            this.h = file;
            a();
        } else if (file.canRead()) {
            this.i.put(this.f, Integer.valueOf(i2));
            a((String) this.a.get(i2));
        } else {
            e.b(this, R.string.file_picker_cant_read_folder);
        }
    }
}
