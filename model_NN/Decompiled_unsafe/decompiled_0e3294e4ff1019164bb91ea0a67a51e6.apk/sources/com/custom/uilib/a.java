package com.custom.uilib;

import android.text.Editable;
import android.view.View;
import android.widget.EditText;
import com.custom.corelib.c;
import com.custom.lwp.MagictouchIcecubesinwaterone.R;

final class a implements View.OnClickListener {
    private /* synthetic */ FilePicker a;

    a(FilePicker filePicker) {
        this.a = filePicker;
    }

    public final void onClick(View view) {
        Editable text = ((EditText) this.a.findViewById(R.id.file_picker_file_input)).getText();
        if (text.length() > 0) {
            FilePicker.a(this.a, c.a(this.a.f, text.toString()));
        }
    }
}
