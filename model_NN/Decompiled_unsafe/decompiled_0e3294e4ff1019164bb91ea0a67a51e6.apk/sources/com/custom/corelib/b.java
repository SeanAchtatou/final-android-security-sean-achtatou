package com.custom.corelib;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import com.custom.lwp.MagictouchIcecubesinwaterone.R;
import java.util.HashSet;

public final class b {
    private SoundPool a = null;
    private HashSet b = null;
    private AudioManager c = null;
    private Context d = null;
    private int e = 0;
    private int f = 3;

    public final int a() {
        return this.e;
    }

    public final int a(int i, float f2) {
        return this.a.play(i, f2, f2, 1, -1, 1.0f);
    }

    public final void a(int i) {
        this.a.setLoop(i, 0);
    }

    public final void a(int i, float f2, float f3) {
        this.a.setVolume(i, f2, f3);
    }

    public final void a(Context context) {
        this.d = context;
        this.e = 2;
        this.f = 5;
        this.a = new SoundPool(2, 5, 0);
        this.b = new HashSet();
        this.c = (AudioManager) this.d.getSystemService("audio");
    }

    public final int b() {
        int load = this.a.load(this.d, R.raw.water, 1);
        this.b.add(Integer.valueOf(load));
        return load;
    }

    public final void b(int i) {
        this.a.stop(i);
    }

    public final float c() {
        return ((float) this.c.getStreamVolume(5)) / ((float) this.c.getStreamMaxVolume(5));
    }

    public final void d() {
        if (this.a != null) {
            this.a.release();
            this.a = null;
        }
        if (this.b != null) {
            this.b.clear();
            this.b = null;
        }
        if (this.c != null) {
            this.c.unloadSoundEffects();
            this.c = null;
        }
    }
}
