package com.custom.corelib;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import java.lang.reflect.Method;

public abstract class API4Service extends Service {
    private static final Class[] a;
    private static final Class[] b;
    private Method c = null;
    private Method d = null;
    private NotificationManager e = null;

    static {
        Class[] clsArr = new Class[2];
        a = clsArr;
        clsArr[0] = Integer.TYPE;
        a[1] = Notification.class;
        Class[] clsArr2 = new Class[1];
        b = clsArr2;
        clsArr2[0] = Boolean.TYPE;
    }
}
