package com.custom.corelib;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;
import com.custom.lwp.MagictouchIcecubesinwaterone.R;

public class LiveWallpaperLauncher extends Activity {
    private int a;

    public LiveWallpaperLauncher() {
        this.a = 0;
        this.a = R.string.select_wallpaper;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        try {
            startActivity(new Intent("android.service.wallpaper.LIVE_WALLPAPER_CHOOSER"));
            Toast.makeText(this, this.a, 1).show();
        } catch (Exception e) {
        }
        finish();
    }
}
