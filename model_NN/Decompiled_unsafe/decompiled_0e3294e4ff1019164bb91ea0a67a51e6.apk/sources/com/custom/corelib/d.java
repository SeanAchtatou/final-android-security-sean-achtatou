package com.custom.corelib;

import android.app.AlertDialog;
import android.content.Context;
import android.preference.DialogPreference;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.SeekBar;

public final class d extends DialogPreference {
    private SeekBar a;
    private int b;
    private int c;

    private d(Context context) {
        super(context, null);
        this.b = 100;
        this.c = 100;
        this.b = 100;
        this.c = 100;
    }

    public d(Context context, byte b2) {
        this(context);
    }

    /* access modifiers changed from: protected */
    public final void onDialogClosed(boolean z) {
        if (z && shouldPersist()) {
            persistInt(this.a.getProgress());
        }
    }

    /* access modifiers changed from: protected */
    public final void onPrepareDialogBuilder(AlertDialog.Builder builder) {
        LinearLayout linearLayout = new LinearLayout(getContext());
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        linearLayout.setMinimumWidth(400);
        linearLayout.setPadding(20, 20, 20, 20);
        this.a = new SeekBar(getContext());
        this.a.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        this.a.setMax(this.b);
        this.a.setProgress(getSharedPreferences().getInt(getKey(), this.c));
        linearLayout.addView(this.a);
        builder.setView(linearLayout);
        super.onPrepareDialogBuilder(builder);
    }

    /* access modifiers changed from: protected */
    public final void onSetInitialValue(boolean z, Object obj) {
        super.onSetInitialValue(z, obj);
        if (obj != null) {
            this.c = Integer.parseInt(obj.toString());
        }
    }
}
