package com.custom.corelib;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ImageView;
import com.senddroid.AdLog;

public class GlowImageView extends ImageView {
    private boolean a = false;
    private Paint b = null;
    private int c = -256;
    private int d = 60;
    private int e = 0;

    public GlowImageView(Context context) {
        super(context);
    }

    public GlowImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public GlowImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.b == null) {
            this.b = new Paint();
            this.b.setAntiAlias(true);
        }
        if (this.a) {
            this.b.setColor(this.c);
            this.b.setAlpha(this.d);
            canvas.drawRoundRect(new RectF((float) this.e, (float) this.e, (float) (getWidth() - this.e), (float) (getHeight() - this.e)), 5.0f, 5.0f, this.b);
        }
        super.onDraw(canvas);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        super.onTouchEvent(motionEvent);
        boolean z = this.a;
        switch (motionEvent.getAction()) {
            case AdLog.LOG_LEVEL_NONE /*0*/:
                this.a = true;
                break;
            case 1:
            case 3:
                this.a = false;
                break;
        }
        if (this.a != z) {
            invalidate();
        }
        return true;
    }

    public void setGlowAlpha(int i) {
        this.d = i;
    }

    public void setGlowColor(int i) {
        this.c = i;
    }

    public void setGlowMargin(int i) {
        this.e = i;
    }
}
