package com.custom.corelib;

import android.content.Intent;
import android.net.Uri;
import android.preference.Preference;
import android.preference.PreferenceActivity;

public final class a extends Preference {
    private PreferenceActivity a;
    private String b;

    public a(PreferenceActivity preferenceActivity, String str) {
        super(preferenceActivity);
        this.a = preferenceActivity;
        this.b = str;
    }

    /* access modifiers changed from: protected */
    public final void onClick() {
        super.onClick();
        try {
            this.a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(this.b)));
        } catch (Exception e) {
        }
    }
}
