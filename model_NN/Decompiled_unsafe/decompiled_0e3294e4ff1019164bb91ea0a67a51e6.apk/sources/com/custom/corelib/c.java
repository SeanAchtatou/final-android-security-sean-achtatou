package com.custom.corelib;

public final class c {
    private static String a = System.getProperty("line.separator");

    public static long a() {
        return System.currentTimeMillis();
    }

    public static String a(String str, String str2) {
        return e.b(str2) ? str : e.b(str) ? str2 : str.endsWith("/") ? String.valueOf(str) + str2 : String.valueOf(str) + "/" + str2;
    }
}
