package com.custom.corelib;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Environment;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.widget.Toast;
import com.custom.lwp.MagictouchIcecubesinwaterone.R;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public final class e {
    private static byte[] a = {17, 10, 19, 80, 1, -47, 69, -21, 45, 89, 95, -15, -77, 123, -36, -113, 101, -85, -64, 90};

    public static Bitmap a(Context context, int i) {
        InputStream openRawResource = context.getResources().openRawResource(i);
        if (openRawResource == null) {
            return null;
        }
        try {
            return BitmapFactory.decodeStream(openRawResource);
        } finally {
            try {
                openRawResource.close();
            } catch (IOException e) {
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap a(String str, int i, int i2) {
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(str, options);
            options.inSampleSize = (int) Math.sqrt((((double) options.outHeight) * ((double) options.outWidth)) / (((double) i) * ((double) i2)));
            options.inJustDecodeBounds = false;
            Bitmap decodeFile = BitmapFactory.decodeFile(str, options);
            if (decodeFile == null) {
                return null;
            }
            int width = decodeFile.getWidth();
            int height = decodeFile.getHeight();
            Matrix matrix = new Matrix();
            matrix.postScale(((float) i) / ((float) width), ((float) i2) / ((float) height));
            return Bitmap.createBitmap(decodeFile, 0, 0, width, height, matrix, true);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } catch (OutOfMemoryError e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static Preference a(PreferenceActivity preferenceActivity) {
        a aVar = new a(preferenceActivity, "market://search?q=pub:Soft_Sexy_Girls");
        aVar.setPersistent(false);
        return aVar;
    }

    public static String a(Context context, Uri uri) {
        try {
            Cursor query = context.getContentResolver().query(uri, new String[]{"_data"}, null, null, null);
            if (query != null && query.moveToFirst() && !query.isNull(0)) {
                return query.getString(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void a(Context context, String str) {
        Toast.makeText(context, str, 1).show();
    }

    public static boolean a() {
        try {
            String externalStorageState = Environment.getExternalStorageState();
            return "mounted".equals(externalStorageState) || "mounted_ro".equals(externalStorageState);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean a(String str) {
        try {
            File externalStorageDirectory = Environment.getExternalStorageDirectory();
            if (externalStorageDirectory == null) {
                return false;
            }
            return new File(str).getCanonicalPath().startsWith(externalStorageDirectory.getCanonicalPath());
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static Preference b(PreferenceActivity preferenceActivity) {
        a aVar = new a(preferenceActivity, "https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=AN92VY2MQYEVU");
        aVar.setPersistent(false);
        aVar.setTitle((int) R.string.preference_donate_title);
        aVar.setSummary((int) R.string.preference_donate_summary);
        return aVar;
    }

    public static String b() {
        return Environment.getExternalStorageDirectory().getAbsolutePath();
    }

    public static void b(Context context, int i) {
        Toast.makeText(context, i, 1).show();
    }

    public static boolean b(String str) {
        return str == null || str.length() == 0;
    }
}
