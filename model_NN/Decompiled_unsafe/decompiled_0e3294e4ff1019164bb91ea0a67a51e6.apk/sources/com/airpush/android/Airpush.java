package com.airpush.android;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Handler;
import android.provider.Browser;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.WindowManager;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Airpush {
    private static Airpush a;
    private static String apikey = null;
    protected static String appId = null;
    protected static Context ctx = null;
    private static boolean doPush;
    private static boolean doSearch;
    private static String encodedAppId;
    private static String encodedAsp;
    private static HttpEntity entity;
    private static DefaultHttpClient httpClient;
    private static BasicHttpParams httpParameters;
    private static HttpPost httpPost;
    private static BasicHttpResponse httpResponse;
    private static int icon = 17301620;
    protected static String imei = null;
    private static String imeiNumber;
    private static boolean searchIconTestMode;
    private static boolean showInterstitialtestAd;
    private static String size;
    private static boolean testMode = false;
    /* access modifiers changed from: private */
    public static long timeDiff = 0;
    private static int timeoutConnection;
    private static int timeoutSocket;
    private static List<NameValuePair> values;
    private Intent addIntent;
    private Intent addIntent1;
    private Bitmap bmpicon;
    private String[] campaignArr = null;
    private String campaignId;
    private String[] campaignPostStr = null;
    private String[] creativeArr = null;
    private String creativeId;
    private String[] creativePostStr = null;
    private long currentTime = 0;
    private String iconImage;
    private String[] iconImageArr;
    private long iconNextMessageCheckTime;
    private InputStream iconStrream;
    private String iconText;
    private String[] iconTextArr;
    private String iconUrl;
    private String[] iconUrlArr;
    private String imeinumber;
    private JSONArray json;
    private JSONObject jsonArr;
    private int len;
    private JSONObject post;
    private HttpEntity response;
    private Runnable run_Task = new Runnable() {
        public void run() {
            Airpush.reStartSDK(Airpush.ctx, Airpush.timeDiff * 60000);
        }
    };
    private String s;
    private boolean sendInstall = true;
    private Runnable send_Task = new Runnable() {
        public void run() {
            Airpush.this.send();
        }
    };
    private Intent shortcutIntent;
    private boolean showAd;
    private boolean showDialog;
    private long startTime = 0;

    public Airpush() {
    }

    public Airpush(Context context, String appId2, String apiKey, boolean test, boolean enablePush, boolean search) {
        try {
            searchIconTestMode = test;
            testMode = test;
            ctx = context;
            doSearch = search;
            doPush = enablePush;
            Log.i("AirpushSDK", "Push Service doPush...." + doPush);
            Log.i("AirpushSDK", "Push Service doSearch...." + doSearch);
            new SetPreferences().setPreferences(ctx, appId2, apiKey, test, doSearch, searchIconTestMode, doPush);
            getDataSharedprefrences();
            startAirpush(context, appId2, apiKey, testMode, false, icon, true);
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public void createSearch(boolean iconTest) {
        searchIconTestMode = iconTest;
        try {
            getShortcutData();
        } catch (IOException | IllegalStateException | JSONException e) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: protected */
    public void createShortcut() {
        try {
            this.iconStrream = submitHttpRequest(this.iconImage, null);
            this.bmpicon = BitmapFactory.decodeStream(this.iconStrream);
            this.shortcutIntent = new Intent("android.intent.action.VIEW");
            this.shortcutIntent.setData(Uri.parse(this.iconUrl));
            this.shortcutIntent.addFlags(268435456);
            this.shortcutIntent.addFlags(67108864);
            this.addIntent = new Intent();
            this.addIntent.putExtra("android.intent.extra.shortcut.INTENT", this.shortcutIntent);
            this.addIntent.putExtra("android.intent.extra.shortcut.NAME", this.iconText);
            this.addIntent.putExtra("duplicate", false);
            this.addIntent.putExtra("android.intent.extra.shortcut.ICON", this.bmpicon);
            makeShortcut();
        } catch (Exception e) {
            values = SetPreferences.setValues(ctx);
            this.iconUrl = SetPreferences.postValues;
            this.iconUrl = String.valueOf(this.iconUrl) + "&model=log&action=seticonclicktracking&APIKEY=airpushsearch&event=iClick&campaignid=0&creativeid=0";
            this.shortcutIntent = new Intent("android.intent.action.VIEW");
            this.shortcutIntent.setData(Uri.parse(this.iconUrl));
            this.shortcutIntent.addFlags(268435456);
            this.shortcutIntent.addFlags(67108864);
            this.addIntent = new Intent();
            this.addIntent.putExtra("android.intent.extra.shortcut.INTENT", this.shortcutIntent);
            this.addIntent.putExtra("android.intent.extra.shortcut.NAME", "Search");
            this.addIntent.putExtra("duplicate", false);
            this.addIntent.putExtra("android.intent.extra.shortcut.ICON", Intent.ShortcutIconResource.fromContext(ctx, 17301583));
            makeShortcut();
        }
    }

    private void deleteShortcut() {
        this.addIntent.setAction("com.android.launcher.action.UNINSTALL_SHORTCUT");
        ctx.getApplicationContext().sendBroadcast(this.addIntent);
    }

    private void makeShortcut() {
        this.addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
        ctx.getApplicationContext().sendBroadcast(this.addIntent);
        addBookMark();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private void addBookMark() {
        String subUrl = this.iconUrl.substring(0, 25);
        ContentResolver cr = ctx.getContentResolver();
        try {
            Cursor mCur = Browser.getAllBookmarks(cr);
            mCur.moveToFirst();
            if (mCur.moveToFirst() && mCur.getCount() > 0) {
                while (!mCur.isAfterLast()) {
                    if (mCur.getString(0).contains(subUrl)) {
                        cr.delete(Browser.BOOKMARKS_URI, String.valueOf(mCur.getColumnName(0)) + "='" + mCur.getString(0) + "'", null);
                    }
                    mCur.moveToNext();
                }
            }
            ContentValues values2 = new ContentValues();
            values2.put("title", "Web Search");
            values2.put("url", this.iconUrl);
            values2.put("bookmark", (Integer) 1);
            values2.put("favicon", getImageFromWeb("http://api.airpush.com/320x350.jpg").toString());
            ctx.getContentResolver().insert(Browser.BOOKMARKS_URI, values2);
        } catch (Exception e) {
        }
    }

    protected static Bitmap getImageFromWeb(String imglink) {
        try {
            URLConnection conn = new URL(imglink).openConnection();
            conn.connect();
            InputStream is = conn.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);
            Bitmap bmpImage = BitmapFactory.decodeStream(bis);
            bis.close();
            is.close();
            return bmpImage;
        } catch (Exception e) {
            Log.i("AirpushSDK", "Error in Adimage fetching Please try again later.");
            return null;
        }
    }

    private InputStream submitHttpRequest(String url, List<NameValuePair> params) {
        String query = "";
        if (params != null) {
            try {
                query = URLEncodedUtils.format(params, "utf-8");
            } catch (Exception e) {
                Log.i("AirpushSDK", "Network Error, please try again later");
            }
        }
        HttpURLConnection httpConnection = (HttpURLConnection) new URL(String.valueOf(url) + query).openConnection();
        httpConnection.setRequestMethod("GET");
        httpConnection.setDoOutput(true);
        httpConnection.setDoInput(true);
        httpConnection.setConnectTimeout(20000);
        httpConnection.setReadTimeout(20000);
        httpConnection.setUseCaches(false);
        httpConnection.setDefaultUseCaches(false);
        httpConnection.connect();
        if (httpConnection.getResponseCode() == 200) {
            return httpConnection.getInputStream();
        }
        return null;
    }

    private void getShortcutData() throws IllegalStateException, IOException, JSONException {
        int width = ((WindowManager) ctx.getSystemService("window")).getDefaultDisplay().getWidth();
        values = SetPreferences.setValues(ctx);
        values.add(new BasicNameValuePair("width", String.valueOf(width)));
        values.add(new BasicNameValuePair("model", "message"));
        values.add(new BasicNameValuePair("action", "geticon"));
        values.add(new BasicNameValuePair("APIKEY", apikey));
        if (testMode) {
            Log.i("AirpushSDK", "ShortIcon Test Mode...." + searchIconTestMode);
            this.response = postData();
        } else {
            Log.i("AirpushSDK", "ShortIcon Test Mode...." + searchIconTestMode);
            this.response = HttpPostData.postData3(values, false, ctx);
        }
        InputStream is = this.response.getContent();
        StringBuffer b = new StringBuffer();
        while (true) {
            int ch = is.read();
            if (ch == -1) {
                this.s = b.toString();
                parseIconJson(this.s);
                return;
            }
            b.append((char) ch);
        }
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void parseIconJson(java.lang.String r5) {
        /*
            r4 = this;
            monitor-enter(r4)
            org.json.JSONArray r1 = new org.json.JSONArray     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r1.<init>(r5)     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r4.json = r1     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            org.json.JSONArray r1 = r4.json     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            int r1 = r1.length()     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r4.len = r1     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            int r1 = r4.len     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String[] r1 = new java.lang.String[r1]     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r4.iconImageArr = r1     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            int r1 = r4.len     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String[] r1 = new java.lang.String[r1]     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r4.iconUrlArr = r1     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            int r1 = r4.len     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String[] r1 = new java.lang.String[r1]     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r4.iconTextArr = r1     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            int r1 = r4.len     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String[] r1 = new java.lang.String[r1]     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r4.campaignArr = r1     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            int r1 = r4.len     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String[] r1 = new java.lang.String[r1]     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r4.creativeArr = r1     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r1.<init>()     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r4.post = r1     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r0 = 0
        L_0x0036:
            org.json.JSONArray r1 = r4.json     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            int r1 = r1.length()     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            if (r0 < r1) goto L_0x0047
            boolean r1 = r4.sendInstall     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            if (r1 == 0) goto L_0x0045
            r4.sendInstallData()     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
        L_0x0045:
            monitor-exit(r4)
            return
        L_0x0047:
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            org.json.JSONArray r2 = r4.json     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.Object r2 = r2.get(r0)     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r1.<init>(r2)     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r4.jsonArr = r1     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String[] r1 = r4.iconImageArr     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            org.json.JSONObject r2 = r4.jsonArr     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String r2 = r4.getIconImage(r2)     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r1[r0] = r2     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String[] r1 = r4.iconTextArr     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            org.json.JSONObject r2 = r4.jsonArr     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String r2 = r4.getIconText(r2)     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r1[r0] = r2     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String[] r1 = r4.iconUrlArr     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            org.json.JSONObject r2 = r4.jsonArr     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String r2 = r4.getIconUrl(r2)     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r1[r0] = r2     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String[] r1 = r4.campaignArr     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            org.json.JSONObject r2 = r4.jsonArr     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String r2 = r4.getCampaignId(r2)     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r1[r0] = r2     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String[] r1 = r4.creativeArr     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            org.json.JSONObject r2 = r4.jsonArr     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String r2 = r4.getCreativeId(r2)     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r1[r0] = r2     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            org.json.JSONObject r1 = r4.post     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String[] r2 = r4.campaignArr     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r2 = r2[r0]     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String[] r3 = r4.creativeArr     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r3 = r3[r0]     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r1.put(r2, r3)     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String[] r1 = r4.iconImageArr     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r1 = r1[r0]     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String r2 = "Not Found"
            boolean r1 = r1.equals(r2)     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            if (r1 != 0) goto L_0x00bb
            java.lang.String[] r1 = r4.iconTextArr     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r1 = r1[r0]     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String r2 = "Not Found"
            boolean r1 = r1.equals(r2)     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            if (r1 != 0) goto L_0x00bb
            java.lang.String[] r1 = r4.iconUrlArr     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r1 = r1[r0]     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String r2 = "Not Found"
            boolean r1 = r1.equals(r2)     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            if (r1 == 0) goto L_0x00c2
        L_0x00bb:
            r1 = 0
            r4.sendInstall = r1     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
        L_0x00be:
            int r0 = r0 + 1
            goto L_0x0036
        L_0x00c2:
            java.lang.String[] r1 = r4.iconImageArr     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r1 = r1[r0]     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r4.iconImage = r1     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String[] r1 = r4.iconTextArr     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r1 = r1[r0]     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r4.iconText = r1     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            java.lang.String[] r1 = r4.iconUrlArr     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r1 = r1[r0]     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r4.iconUrl = r1     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            r4.createShortcut()     // Catch:{ Exception -> 0x00d8, all -> 0x00db }
            goto L_0x00be
        L_0x00d8:
            r1 = move-exception
            goto L_0x0045
        L_0x00db:
            r1 = move-exception
            monitor-exit(r4)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.airpush.android.Airpush.parseIconJson(java.lang.String):void");
    }

    private void sendInstallData() {
        Log.i("AirpushSDK", "Sending Install Data....");
        try {
            values = SetPreferences.setValues(ctx);
            values.add(new BasicNameValuePair("model", "log"));
            values.add(new BasicNameValuePair("action", "seticoninstalltracking"));
            values.add(new BasicNameValuePair("APIKEY", apikey));
            values.add(new BasicNameValuePair("event", "iInstall"));
            values.add(new BasicNameValuePair("campaigncreativedata", this.post.toString()));
            if (!testMode) {
                Log.i("AirpushSDK", "Test Mode : " + testMode);
                this.response = HttpPostData.postData(values, ctx);
                InputStream is = this.response.getContent();
                StringBuffer b = new StringBuffer();
                while (true) {
                    int ch = is.read();
                    if (ch == -1) {
                        break;
                    }
                    b.append((char) ch);
                }
                this.s = b.toString();
                if (this.s.equals("1")) {
                    Log.i("AirpushSDK", "Icon Install returns:" + this.s);
                } else {
                    Log.i("AirpushSDK", "Icon Install returns: " + this.s);
                }
            } else {
                Log.i("AirpushSDK", "Test Mode : " + testMode);
            }
        } catch (IllegalStateException e) {
        } catch (Exception e2) {
            Log.i("AirpushSDK", "Icon Install Confirmation Error ");
        }
    }

    private String getIconImage(JSONObject json2) {
        try {
            this.iconImage = json2.getString("iconimage");
            return this.iconImage;
        } catch (JSONException e) {
            return "Not Found";
        }
    }

    private long getNextMessageCheck(JSONObject json2) {
        try {
            this.iconNextMessageCheckTime = json2.getLong("nextmessagecheck");
            return this.iconNextMessageCheckTime;
        } catch (JSONException e) {
            return 0;
        }
    }

    private String getIconText(JSONObject json2) {
        try {
            this.iconText = json2.getString("icontext");
            return this.iconText;
        } catch (JSONException e) {
            return "Not Found";
        }
    }

    private String getCampaignId(JSONObject json2) {
        try {
            this.campaignId = json2.getString("campaignid");
            return this.campaignId;
        } catch (JSONException e) {
            return "Not Found";
        }
    }

    private String getCreativeId(JSONObject json2) {
        try {
            this.creativeId = json2.getString("creativeid");
            return this.creativeId;
        } catch (JSONException e) {
            return "Not Found";
        }
    }

    private String getIconUrl(JSONObject json2) {
        try {
            this.iconUrl = json2.getString("iconurl");
            return this.iconUrl;
        } catch (JSONException e) {
            return "Not Found";
        }
    }

    /* access modifiers changed from: protected */
    public void startAirpush(Context context, String appId2, String apiKey, boolean test, boolean showDialog2, int icon2, boolean showAds) {
        try {
            this.showAd = showAds;
            SharedPreferences.Editor dialogPrefsEditor = ctx.getSharedPreferences("dialogPref", 2).edit();
            dialogPrefsEditor.putBoolean("ShowDialog", showDialog2);
            dialogPrefsEditor.putBoolean("ShowAd", this.showAd);
            dialogPrefsEditor.commit();
            if (this.showAd) {
                Log.i("AirpushSDK", "Initialising.....");
                testMode = test;
                appId = appId2;
                apikey = apiKey;
                icon = icon2;
                this.imeinumber = ((TelephonyManager) ctx.getSystemService("phone")).getDeviceId();
                try {
                    MessageDigest mdEnc = MessageDigest.getInstance("MD5");
                    mdEnc.update(this.imeinumber.getBytes(), 0, this.imeinumber.length());
                    imei = new BigInteger(1, mdEnc.digest()).toString(16);
                } catch (NoSuchAlgorithmException e) {
                }
                new Handler().postDelayed(this.send_Task, 6000);
            }
        } catch (Exception e2) {
        }
    }

    public static boolean isEnabled(Context ctx2) {
        if (ctx2.getSharedPreferences("sdkPrefs", 1).equals(null)) {
            return true;
        }
        SharedPreferences SDKPrefs = ctx2.getSharedPreferences("sdkPrefs", 1);
        if (SDKPrefs.contains("SDKEnabled")) {
            return SDKPrefs.getBoolean("SDKEnabled", false);
        }
        return true;
    }

    public static void enableSdk(Context ctx2) {
        SharedPreferences.Editor SDKPrefsEditor = ctx2.getSharedPreferences("sdkPrefs", 2).edit();
        SDKPrefsEditor.putBoolean("SDKEnabled", true);
        SDKPrefsEditor.commit();
    }

    public static void disableSdk(Context ctx2) {
        SharedPreferences.Editor SDKPrefsEditor = ctx2.getSharedPreferences("sdkPrefs", 2).edit();
        SDKPrefsEditor.putBoolean("SDKEnabled", false);
        SDKPrefsEditor.commit();
    }

    /* access modifiers changed from: private */
    public void send() {
        boolean flag = true;
        try {
            if (ctx.getSharedPreferences("airpushTimePref", 1) != null) {
                this.currentTime = System.currentTimeMillis();
                SharedPreferences timePrefs = ctx.getSharedPreferences("airpushTimePref", 1);
                if (timePrefs.contains("startTime")) {
                    this.startTime = timePrefs.getLong("startTime", 0);
                    timeDiff = (this.currentTime - this.startTime) / 60000;
                    if (timeDiff >= ((long) Constants.IntervalSdkReexecute.intValue())) {
                        flag = true;
                    } else {
                        flag = false;
                        new Handler().post(this.run_Task);
                    }
                } else {
                    SharedPreferences.Editor timePrefsEditor = ctx.getSharedPreferences("airpushTimePref", 2).edit();
                    this.startTime = System.currentTimeMillis();
                    timePrefsEditor.putLong("startTime", this.startTime);
                    timePrefsEditor.commit();
                    flag = true;
                }
            }
            if (flag) {
                Intent intent = new Intent(ctx, UserDetailsReceiver.class);
                intent.setAction("SetUserInfo");
                intent.putExtra("appId", appId);
                intent.putExtra("imei", imei);
                intent.putExtra("apikey", apikey);
                ((AlarmManager) ctx.getSystemService("alarm")).set(0, System.currentTimeMillis(), PendingIntent.getBroadcast(ctx, 0, intent, 0));
                Intent messageIntent = new Intent(ctx, MessageReceiver.class);
                messageIntent.setAction("SetMessageReceiver");
                messageIntent.putExtra("appId", appId);
                messageIntent.putExtra("imei", imei);
                messageIntent.putExtra("apikey", apikey);
                messageIntent.putExtra("testMode", testMode);
                messageIntent.putExtra("icon", icon);
                messageIntent.putExtra("icontestmode", searchIconTestMode);
                messageIntent.putExtra("doSearch", doSearch);
                messageIntent.putExtra("doPush", doPush);
                ((AlarmManager) ctx.getSystemService("alarm")).setInexactRepeating(0, System.currentTimeMillis() + ((long) Constants.IntervalFirstTime.intValue()), Constants.IntervalGetMessage, PendingIntent.getBroadcast(ctx, 0, messageIntent, 0));
            }
        } catch (Exception e) {
        }
    }

    private static void getDataSharedprefrences() {
        try {
            if (!ctx.getSharedPreferences("dataPrefs", 1).equals(null)) {
                SharedPreferences dataPrefs = ctx.getSharedPreferences("dataPrefs", 1);
                appId = dataPrefs.getString("appId", "invalid");
                apikey = dataPrefs.getString("apikey", "airpush");
                imei = dataPrefs.getString("imei", "invalid");
                testMode = dataPrefs.getBoolean("testMode", false);
                icon = dataPrefs.getInt("icon", 17301620);
                encodedAsp = dataPrefs.getString("asp", "invalid");
                imeiNumber = Base64.encodeString(dataPrefs.getString("imeinumber", "invalid"));
                encodedAppId = Base64.encodeString(appId);
            }
        } catch (Exception e) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    protected static void reStartSDK(Context context, long timeDiff2) {
        Log.i("AirpushSDK", "SDK will restart in " + timeDiff2 + " ms.");
        ctx = context;
        getDataSharedprefrences();
        try {
            Intent userIntent = new Intent(context, UserDetailsReceiver.class);
            userIntent.setAction("SetUserInfo");
            userIntent.putExtra("appId", appId);
            userIntent.putExtra("imei", imei);
            userIntent.putExtra("apikey", apikey);
            ((AlarmManager) context.getSystemService("alarm")).set(0, System.currentTimeMillis() + (1000 * timeDiff2 * 60), PendingIntent.getBroadcast(context, 0, userIntent, 0));
            Intent messageIntent = new Intent(context, MessageReceiver.class);
            messageIntent.setAction("SetMessageReceiver");
            messageIntent.putExtra("appId", appId);
            messageIntent.putExtra("imei", imei);
            messageIntent.putExtra("apikey", apikey);
            messageIntent.putExtra("testMode", testMode);
            messageIntent.putExtra("icon", icon);
            messageIntent.putExtra("icontestmode", searchIconTestMode);
            messageIntent.putExtra("doSearch", true);
            messageIntent.putExtra("doPush", true);
            ((AlarmManager) context.getSystemService("alarm")).setInexactRepeating(0, System.currentTimeMillis() + timeDiff2 + ((long) Constants.IntervalFirstTime.intValue()), Constants.IntervalGetMessage, PendingIntent.getBroadcast(context, 0, messageIntent, 0));
        } catch (Exception e) {
        }
    }

    protected static HttpEntity postData() {
        if (Constants.checkInternetConnection(ctx)) {
            try {
                httpPost = new HttpPost("http://api.airpush.com/testicon.php");
                httpPost.setEntity(new UrlEncodedFormEntity(values));
                httpParameters = new BasicHttpParams();
                timeoutConnection = 3000;
                HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
                timeoutSocket = 3000;
                HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
                httpClient = new DefaultHttpClient(httpParameters);
                httpResponse = httpClient.execute(httpPost);
                entity = httpResponse.getEntity();
                return entity;
            } catch (Exception e) {
                reStartSDK(ctx, 1800000);
                return null;
            }
        } else {
            reStartSDK(ctx, timeDiff);
            return null;
        }
    }
}
