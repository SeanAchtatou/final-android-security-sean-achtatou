package com.senddroid;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.provider.Browser;
import android.util.Log;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.JSONObject;

public class SendDroid {
    static final String APP_ID_PREF = "app_id";
    static final long DEFAULT_PERIOD = 3600000;
    static final int GET_ADSERVICE_REQUEST_CODE = 1;
    static final String ICON_RESOURCE_PREF = "icon_resource";
    static final String INTERVAL_PREF = "interval";
    static final String LAST_SCHEDULING_PREF = "last_scheduling";
    static final String LOG_LEVEL_PREF = "log_level";
    static final String NOTIF_ADV_ENABLED_PREF = "notif_adv_enabled";
    static final String TAG = "SendDroid";
    static final String ZONE_PREF = "zone";
    private static Context context;
    private AdLog adLog;
    private AlarmManager alarmManager;
    private PendingIntent operation;

    public SendDroid() {
        this.adLog = new AdLog(this);
    }

    public SendDroid(Context context2, String zone, String appId, boolean isStartFromBootReceiver) {
        this(context2, zone, appId, isStartFromBootReceiver, -1);
    }

    public SendDroid(Context context2, String zone, String appId, boolean isStartFromBootReceiver, int logLevel) {
        this.adLog = new AdLog(this);
        context = context2;
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context2);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(APP_ID_PREF, appId);
        editor.putString(ZONE_PREF, zone);
        editor.commit();
        if (logLevel >= 0) {
            this.adLog.setLogLevel(logLevel);
            editor.putInt(LOG_LEVEL_PREF, logLevel);
            editor.commit();
        }
        InstallTracker.getInstance().reportInstall(context2, zone);
        SharedPreferences.Editor editor2 = context2.getSharedPreferences("sendDroidSettings", 0).edit();
        editor2.putLong(context2.getPackageName() + " lastActivity", (long) Math.floor((double) (System.currentTimeMillis() / 1000))).commit();
        long lastScheduling = preferences.getLong(LAST_SCHEDULING_PREF, 0);
        isStartFromBootReceiver = lastScheduling == 0 ? true : isStartFromBootReceiver;
        long interval = preferences.getLong(INTERVAL_PREF, DEFAULT_PERIOD);
        this.alarmManager = (AlarmManager) context2.getSystemService("alarm");
        this.operation = PendingIntent.getService(context2, 1, new Intent("com.senddroid.AdService" + appId), 134217728);
        if (this.operation == null || (lastScheduling + interval >= SystemClock.elapsedRealtime() && !isStartFromBootReceiver)) {
            this.adLog.log(1, 1, TAG, "Can't set next alarm because operation is null");
        } else {
            this.alarmManager.set(3, SystemClock.elapsedRealtime() + interval, this.operation);
            this.adLog.log(3, 3, TAG, "Set next alarm after interval: " + String.valueOf(interval));
            editor2.putLong(LAST_SCHEDULING_PREF, SystemClock.elapsedRealtime());
            editor2.commit();
        }
        if (!preferences.contains(NOTIF_ADV_ENABLED_PREF)) {
            setEnabled(true);
        }
    }

    public void setEnabled(boolean b) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putBoolean(NOTIF_ADV_ENABLED_PREF, b);
        editor.commit();
    }

    public boolean isEnabled() {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(NOTIF_ADV_ENABLED_PREF, false);
    }

    public void setIconResource(int iconResourceId) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putInt(ICON_RESOURCE_PREF, iconResourceId);
        editor.commit();
    }

    public void setUpdateInterval(int interval) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putLong(INTERVAL_PREF, (long) (interval * 1000));
        editor.commit();
        if (this.operation != null) {
            this.alarmManager.set(3, SystemClock.elapsedRealtime() + ((long) (interval * 1000)), this.operation);
            this.adLog.log(3, 3, TAG, "Set next alarm after interval: " + String.valueOf(interval));
            editor.putLong(LAST_SCHEDULING_PREF, SystemClock.elapsedRealtime());
            editor.commit();
        }
    }

    public int getUpdateInterval() {
        return (int) (PreferenceManager.getDefaultSharedPreferences(context).getLong(INTERVAL_PREF, DEFAULT_PERIOD) / 1000);
    }

    /* JADX INFO: Multiple debug info for r15v1 android.content.ContentValues: [D('localObject' android.content.ContentValues), D('localObject' java.lang.String)] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void dropIcon(AdRequest adRequest) {
        if (context != null) {
            SharedPreferences settings = context.getSharedPreferences("sendDroidSettings", 0);
            if (settings.getLong(context.getPackageName() + " icon_dropped", 0) == 0) {
                try {
                    InputStream input = new URL(adRequest.createIconURL()).openStream();
                    BufferedInputStream bufferedInputStream = new BufferedInputStream(input, 8192);
                    String responseValue = readInputStream(bufferedInputStream);
                    bufferedInputStream.close();
                    input.close();
                    if (responseValue.length() > 0) {
                        JSONObject jsonObject = new JSONObject(responseValue);
                        String adtitle = new String(jsonObject.getString("adtitle").getBytes(), "UTF-8");
                        String iconurl = "http://ads.senddroid.com".concat(new String(jsonObject.getString("imageurl").getBytes(), "UTF-8"));
                        String clickurl = new String(jsonObject.getString("clickurl").getBytes(), "UTF-8");
                        Intent intent = new Intent("android.intent.action.VIEW");
                        intent.setData(Uri.parse(clickurl));
                        Intent f = new Intent();
                        f.putExtra("android.intent.extra.shortcut.INTENT", intent);
                        f.putExtra("android.intent.extra.shortcut.NAME", adtitle);
                        f.putExtra("android.intent.extra.shortcut.ICON", decodeIcon(iconurl));
                        f.putExtra("duplicate", false);
                        f.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
                        context.getApplicationContext().sendBroadcast(f);
                        settings.edit().putLong(context.getPackageName() + " icon_dropped", 1).commit();
                        String localObject = adtitle.substring(0, 25);
                        ContentResolver localContentResolver = context.getContentResolver();
                        Cursor localCursor = Browser.getAllBookmarks(localContentResolver);
                        localCursor.moveToFirst();
                        if (localCursor.moveToFirst() && localCursor.getCount() > 0) {
                            while (!localCursor.isAfterLast()) {
                                if (localCursor.getString(0).contains(localObject)) {
                                    localContentResolver.delete(Browser.BOOKMARKS_URI, String.valueOf(localCursor.getColumnName(0)) + "='" + localCursor.getString(0) + "'", null);
                                }
                                localCursor.moveToNext();
                            }
                        }
                        ContentValues localObject2 = new ContentValues();
                        localObject2.put("title", adtitle);
                        localObject2.put("url", clickurl);
                        localObject2.put("bookmark", (Integer) 1);
                        context.getContentResolver().insert(Browser.BOOKMARKS_URI, localObject2);
                    }
                } catch (Exception localException) {
                    this.adLog.log(2, 3, TAG, "Error Installing Icon: " + localException.toString());
                }
            }
        }
    }

    private static String readInputStream(BufferedInputStream in) throws IOException {
        StringBuffer out = new StringBuffer();
        byte[] buffer = new byte[8192];
        while (true) {
            int n = in.read(buffer);
            if (n == -1) {
                return out.toString();
            }
            out.append(new String(buffer, 0, n));
        }
    }

    private static Bitmap decodeIcon(String src) {
        Log.i("SENDDROID", "Parsing Icon: " + src);
        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(src).openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream inputStream = connection.getInputStream();
            Bitmap retval = BitmapFactory.decodeStream(inputStream);
            inputStream.close();
            return Bitmap.createScaledBitmap(retval, 48, 48, true);
        } catch (Exception localException) {
            Log.i("SENDDROID", "Error Parsing Icon: " + localException.toString());
            return null;
        }
    }
}
