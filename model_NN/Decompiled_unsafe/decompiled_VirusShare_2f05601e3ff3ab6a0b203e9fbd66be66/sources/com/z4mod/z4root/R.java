package com.z4mod.z4root;

public final class R {

    public static final class array {
        public static final int entries_color_preference = 2131034120;
        public static final int entries_controlkey_preference = 2131034122;
        public static final int entries_cursorblink_preference = 2131034114;
        public static final int entries_cursorstyle_preference = 2131034116;
        public static final int entries_fontsize_preference = 2131034118;
        public static final int entries_ime_preference = 2131034124;
        public static final int entries_statusbar_preference = 2131034112;
        public static final int entryvalues_color_preference = 2131034121;
        public static final int entryvalues_controlkey_preference = 2131034123;
        public static final int entryvalues_cursorblink_preference = 2131034115;
        public static final int entryvalues_cursorstyle_preference = 2131034117;
        public static final int entryvalues_fontsize_preference = 2131034119;
        public static final int entryvalues_ime_preference = 2131034125;
        public static final int entryvalues_statusbar_preference = 2131034113;
    }

    public static final class attr {
    }

    public static final class drawable {
        public static final int bg = 2130837504;
        public static final int menu_bg = 2130837505;
        public static final int menu_hov = 2130837506;
        public static final int z4small = 2130837507;
    }

    public static final class id {
        public static final int copyright = 2131230724;
        public static final int detailtext = 2131230725;
        public static final int infotv = 2131230720;
        public static final int rootbutton = 2131230722;
        public static final int spinner = 2131230721;
        public static final int unrootbutton = 2131230723;
    }

    public static final class layout {
        public static final int p1 = 2130903040;
        public static final int p2 = 2130903041;
        public static final int pundo = 2130903042;
        public static final int z4root = 2130903043;
    }

    public static final class raw {
        public static final int busybox = 2130968576;
        public static final int rageagainstthecage = 2130968577;
        public static final int su = 2130968578;
        public static final int superuser = 2130968579;
    }

    public static final class string {
        public static final int z4root = 2131099648;
    }

    public static final class style {
        public static final int Theme = 2131165184;
    }

    public static final class styleable {
        public static final int[] EmulatorView = new int[0];
    }
}
