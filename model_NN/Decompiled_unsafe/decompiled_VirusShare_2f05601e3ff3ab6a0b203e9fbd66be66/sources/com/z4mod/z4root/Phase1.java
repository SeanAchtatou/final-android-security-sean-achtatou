package com.z4mod.z4root;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.widget.TextView;
import jackpal.androidterm.Exec;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Calendar;

public class Phase1 extends Activity {
    static final int SHOW_SETTINGS_DIALOG = 1;
    static final int SHOW_SETTINGS_ERROR_DIALOG = 2;
    TextView t;
    PowerManager.WakeLock wl;

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: Method info already added: android.content.Context.openFileOutput(java.lang.String, int):java.io.FileOutputStream in method: com.z4mod.z4root.Phase1.SaveIncludedFileIntoFilesFolder(int, java.lang.String, android.content.Context):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Method info already added: android.content.Context.openFileOutput(java.lang.String, int):java.io.FileOutputStream
        	at jadx.core.dex.info.InfoStorage.putMethod(InfoStorage.java:42)
        	at jadx.core.dex.info.MethodInfo.fromDex(MethodInfo.java:50)
        	at jadx.core.dex.instructions.InsnDecoder.invoke(InsnDecoder.java:678)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:540)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public static void SaveIncludedFileIntoFilesFolder(int r1, java.lang.String r2, android.content.Context r3) {
        /*
            r5 = 1
            android.content.res.Resources r4 = r8.getResources()
            java.io.InputStream r2 = r4.openRawResource(r6)
            java.io.FileOutputStream r1 = r8.openFileOutput(r7, r5)
            r4 = 1024(0x400, float:1.435E-42)
            byte[] r0 = new byte[r4]
        L_0x0011:
            int r3 = r2.read(r0)
            if (r3 >= 0) goto L_0x0028
            r2.close()
            java.nio.channels.FileChannel r4 = r1.getChannel()
            r4.force(r5)
            r1.flush()
            r1.close()
            return
        L_0x0028:
            r4 = 0
            r1.write(r0, r4, r3)
            goto L_0x0011
        */
        throw new UnsupportedOperationException("Method not decompiled: com.z4mod.z4root.Phase1.SaveIncludedFileIntoFilesFolder(int, java.lang.String, android.content.Context):void");
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.p1);
        this.t = (TextView) findViewById(R.id.infotv);
        new Thread() {
            public void run() {
                Phase1.this.dostuff();
            }
        }.start();
    }

    public void saystuff(final String stuff) {
        runOnUiThread(new Runnable() {
            public void run() {
                Phase1.this.t.setText(stuff);
            }
        });
    }

    public void dostuff() {
        this.wl = ((PowerManager) getSystemService("power")).newWakeLock(268435482, "z4root");
        this.wl.acquire();
        saystuff("Saving required file...");
        try {
            SaveIncludedFileIntoFilesFolder(R.raw.rageagainstthecage, "rageagainstthecage", getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        int[] processId = new int[SHOW_SETTINGS_DIALOG];
        FileDescriptor fd = Exec.createSubprocess("/system/bin/sh", "-", null, processId);
        Log.i("AAA", "Got processid: " + processId[0]);
        FileOutputStream out = new FileOutputStream(fd);
        final FileInputStream in = new FileInputStream(fd);
        new Thread() {
            public void run() {
                byte[] mBuffer = new byte[4096];
                int read = 0;
                while (read >= 0) {
                    try {
                        read = in.read(mBuffer);
                        String str = new String(mBuffer, 0, read);
                        Log.i("AAA", str);
                        if (str.contains("Forked")) {
                            Log.i("BBB", "FORKED FOUND!");
                            Phase1.this.saystuff("Forking completed");
                            PendingIntent sender = PendingIntent.getBroadcast(Phase1.this.getApplicationContext(), 0, new Intent(Phase1.this.getApplicationContext(), AlarmReceiver.class), 0);
                            Calendar cal = Calendar.getInstance();
                            cal.add(13, 5);
                            ((AlarmManager) Phase1.this.getSystemService("alarm")).set(0, cal.getTimeInMillis(), sender);
                            Phase1.this.saystuff("Attempting to apply root...");
                            return;
                        } else if (str.contains("Cannot find adb")) {
                            Phase1.this.runOnUiThread(new Runnable() {
                                public void run() {
                                    Phase1.this.showDialog(Phase1.SHOW_SETTINGS_DIALOG);
                                }
                            });
                        }
                    } catch (Exception e) {
                        read = -1;
                        e.printStackTrace();
                    }
                }
            }
        }.start();
        try {
            out.write(("chmod 777 " + getFilesDir() + "/rageagainstthecage\n").getBytes());
            out.flush();
            out.write((getFilesDir() + "/rageagainstthecage\n").getBytes());
            out.flush();
            saystuff("Running exploit in order to obtain root access...");
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        if (id == SHOW_SETTINGS_DIALOG) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("USB Debugging must be enabled!");
            builder.setMessage("In order for this to work, USB debugging must be enabled. The settings page will open when you press OK. Please enable USB debugging, and then retry.").setCancelable(true).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    try {
                        Phase1.this.startActivity(new Intent("android.settings.APPLICATION_DEVELOPMENT_SETTINGS"));
                    } catch (Exception e) {
                        try {
                            Phase1.this.startActivity(new Intent("android.settings.APPLICATION_SETTINGS"));
                        } catch (Exception e2) {
                            Phase1.this.showDialog(Phase1.SHOW_SETTINGS_ERROR_DIALOG);
                            return;
                        }
                    }
                    Phase1.this.finish();
                }
            });
            return builder.create();
        } else if (id != SHOW_SETTINGS_ERROR_DIALOG) {
            return super.onCreateDialog(id);
        } else {
            AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
            builder2.setTitle("USB Debugging must be enabled!");
            builder2.setMessage("Could not automatically open the correct settings page. Please turn USB debugging on and retry!").setCancelable(true).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Phase1.this.finish();
                }
            });
            return builder2.create();
        }
    }
}
