package com.z4mod.z4root;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.widget.TextView;
import jackpal.androidterm.Exec;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

public class Phase2 extends Activity {
    TextView t;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.p2);
        new Thread() {
            public void run() {
                Phase2.this.dostuff();
            }
        }.start();
    }

    public void saystuff(String stuff) {
        runOnUiThread(new Runnable() {
            public void run() {
            }
        });
    }

    public void dostuff() {
        final PowerManager.WakeLock wl = ((PowerManager) getSystemService("power")).newWakeLock(268435482, "z4root");
        wl.acquire();
        Log.i("AAA", "Starting");
        int[] processId = new int[1];
        FileDescriptor fd = Exec.createSubprocess("/system/bin/sh", "-", null, processId);
        Log.i("AAA", "Got processid: " + processId[0]);
        FileOutputStream out = new FileOutputStream(fd);
        final FileInputStream in = new FileInputStream(fd);
        new Thread() {
            public void run() {
                byte[] mBuffer = new byte[4096];
                int read = 0;
                while (read >= 0) {
                    try {
                        read = in.read(mBuffer);
                        String str = new String(mBuffer, 0, read);
                        Phase2.this.saystuff(str);
                        Log.i("AAA", str);
                    } catch (Exception e) {
                    }
                }
                wl.release();
            }
        }.start();
        try {
            out.write("id\n".getBytes());
            out.flush();
            try {
                SaveIncludedZippedFileIntoFilesFolder(R.raw.busybox, "busybox", getApplicationContext());
                SaveIncludedZippedFileIntoFilesFolder(R.raw.su, "su", getApplicationContext());
                SaveIncludedZippedFileIntoFilesFolder(R.raw.superuser, "SuperUser.apk", getApplicationContext());
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.write(("chmod 777 " + getFilesDir() + "/busybox\n").getBytes());
            out.flush();
            out.write((getFilesDir() + "/busybox mount -o remount,rw /system\n").getBytes());
            out.flush();
            out.write((getFilesDir() + "/busybox cp " + getFilesDir() + "/su /system/bin/\n").getBytes());
            out.flush();
            out.write((getFilesDir() + "/busybox cp " + getFilesDir() + "/SuperUser.apk /system/app\n").getBytes());
            out.flush();
            out.write((getFilesDir() + "/busybox cp " + getFilesDir() + "/busybox /system/bin/\n").getBytes());
            out.flush();
            out.write("chown root.root /system/bin/busybox\nchmod 755 /system/bin/busybox\n".getBytes());
            out.flush();
            out.write("chown root.root /system/bin/su\nchmod 6755 /system/bin/su\n".getBytes());
            out.flush();
            out.write("chown root.root /system/app/SuperUser.apk\nchmod 755 /system/app/SuperUser.apk\n".getBytes());
            out.flush();
            out.write(("rm " + getFilesDir() + "/busybox\n").getBytes());
            out.flush();
            out.write(("rm " + getFilesDir() + "/su\n").getBytes());
            out.flush();
            out.write(("rm " + getFilesDir() + "/SuperUser.apk\n").getBytes());
            out.flush();
            out.write(("rm " + getFilesDir() + "/rageagainstthecage\n").getBytes());
            out.flush();
            out.write("echo \"reboot now!\"\n".getBytes());
            out.flush();
            Thread.sleep(3000);
            out.write("sync\nsync\n".getBytes());
            out.flush();
            out.write("reboot\n".getBytes());
            out.flush();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static void SaveIncludedZippedFileIntoFilesFolder(int resourceid, String filename, Context ApplicationContext) throws Exception {
        InputStream is = ApplicationContext.getResources().openRawResource(resourceid);
        FileOutputStream fos = ApplicationContext.openFileOutput(filename, 1);
        GZIPInputStream gzis = new GZIPInputStream(is);
        byte[] bytebuf = new byte[1024];
        while (true) {
            int read = gzis.read(bytebuf);
            if (read < 0) {
                gzis.close();
                fos.getChannel().force(true);
                fos.flush();
                fos.close();
                return;
            }
            fos.write(bytebuf, 0, read);
        }
    }
}
