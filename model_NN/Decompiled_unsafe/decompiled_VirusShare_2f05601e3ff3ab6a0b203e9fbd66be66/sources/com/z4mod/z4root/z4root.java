package com.z4mod.z4root;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class z4root extends Activity {
    TextView detailtext;
    boolean disabled = false;
    Button rootbutton;
    Button unrootbutton;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.z4root);
        this.rootbutton = (Button) findViewById(R.id.rootbutton);
        this.unrootbutton = (Button) findViewById(R.id.unrootbutton);
        this.detailtext = (TextView) findViewById(R.id.detailtext);
        this.rootbutton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!z4root.this.disabled) {
                    z4root.this.disabled = true;
                    z4root.this.startActivity(new Intent(z4root.this, Phase1.class));
                    z4root.this.finish();
                }
            }
        });
        this.unrootbutton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!z4root.this.disabled) {
                    z4root.this.disabled = true;
                    z4root.this.startActivity(new Intent(z4root.this, PhaseRemove.class));
                    z4root.this.finish();
                }
            }
        });
        new Thread() {
            public void run() {
                z4root.this.dostuff();
            }
        }.start();
    }

    public void dostuff() {
        try {
            VirtualTerminal vt = new VirtualTerminal();
            if (vt.runCommand("id").success()) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        z4root.this.unrootbutton.setVisibility(0);
                        z4root.this.rootbutton.setText("Re-root");
                        z4root.this.detailtext.setText("Your device is already rooted. You can remove the root using the Un-root button, which will delete all of the files installed to root your device. You can also re-root your device if your root is malfunctioning.");
                    }
                });
            }
            vt.shutdown();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
