package com.z4mod.z4root;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class AlarmReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        try {
            Intent newIntent = new Intent(context, Phase2.class);
            newIntent.addFlags(268435456);
            context.startActivity(newIntent);
        } catch (Exception e) {
            Toast.makeText(context, "There was an error somewhere, but we still received an alarm", 0).show();
            e.printStackTrace();
        }
    }
}
