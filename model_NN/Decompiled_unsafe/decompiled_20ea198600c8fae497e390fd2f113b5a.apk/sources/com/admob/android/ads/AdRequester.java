package com.admob.android.ads;

import android.content.Context;
import android.util.Log;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

class AdRequester {
    private static final String ADMOB_ENDPOINT = "http://r.admob.com/ad_source.php";
    private static int REQUEST_TIMEOUT = 3000;

    AdRequester() {
    }

    private static /* synthetic */ void appendParams(StringBuilder sb, String str, String str2) {
        if (str2 != null && str2.length() > 0) {
            try {
                sb.append("&").append(URLEncoder.encode(str, "UTF-8")).append("=").append(URLEncoder.encode(str2, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                Log.e("AdMob SDK", "UTF-8 encoding is not supported on this device.  Ad requests are impossible.", e);
            }
        }
    }

    static String buildParamString(Context context, String str, String str2) {
        StringBuilder sb = new StringBuilder();
        long currentTimeMillis = System.currentTimeMillis();
        sb.append("z").append("=").append(currentTimeMillis / 1000).append(".").append(currentTimeMillis % 1000);
        appendParams(sb, "rt", "0");
        String publisherId = AdManager.getPublisherId(context);
        if (publisherId == null) {
            throw new IllegalStateException("Publisher ID is not set!  To serve ads you must set your publisher ID assigned from www.admob.com.  Either add it to AndroidManifest.xml under the <application> tag or call AdManager.setPublisherId().");
        }
        appendParams(sb, "s", publisherId);
        appendParams(sb, "f", "html_no_js");
        appendParams(sb, "y", "text");
        appendParams(sb, "client_sdk", "1");
        appendParams(sb, "ex", "1");
        appendParams(sb, "v", AdManager.SDK_VERSION);
        appendParams(sb, "t", AdManager.getUserId(context));
        appendParams(sb, "d[coord]", AdManager.getCoordinatesAsString(context));
        appendParams(sb, "d[dob]", AdManager.getBirthdayAsString());
        appendParams(sb, "k", str);
        appendParams(sb, "search", str2);
        if (AdManager.isInTestMode()) {
            appendParams(sb, "m", "test");
        }
        return sb.toString();
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x00af A[SYNTHETIC, Splitter:B:20:0x00af] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00b4 A[Catch:{ Exception -> 0x0142 }] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00c7  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00d4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.admob.android.ads.Ad requestAd(android.content.Context r10, java.lang.String r11, java.lang.String r12) {
        /*
            r1 = 0
            java.lang.String r0 = "android.permission.INTERNET"
            int r0 = r10.checkCallingOrSelfPermission(r0)
            r2 = -1
            if (r0 != r2) goto L_0x000f
            java.lang.String r0 = "Cannot request an ad without Internet permissions!  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"android.permission.INTERNET\" />"
            com.admob.android.ads.AdManager.clientError(r0)
        L_0x000f:
            com.admob.android.ads.AdMobLocalizer.init(r10)
            long r4 = java.lang.System.currentTimeMillis()
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r2 = buildParamString(r10, r11, r12)     // Catch:{ Exception -> 0x013f }
            java.net.URL r0 = new java.net.URL     // Catch:{ all -> 0x0134 }
            java.lang.String r3 = "http://r.admob.com/ad_source.php"
            r0.<init>(r3)     // Catch:{ all -> 0x0134 }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ all -> 0x0134 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ all -> 0x0134 }
            java.lang.String r3 = "POST"
            r0.setRequestMethod(r3)     // Catch:{ all -> 0x0134 }
            r3 = 1
            r0.setDoOutput(r3)     // Catch:{ all -> 0x0134 }
            java.lang.String r3 = "User-Agent"
            java.lang.String r7 = com.admob.android.ads.AdManager.getUserAgent()     // Catch:{ all -> 0x0134 }
            r0.setRequestProperty(r3, r7)     // Catch:{ all -> 0x0134 }
            java.lang.String r3 = "Content-Type"
            java.lang.String r7 = "application/x-www-form-urlencoded"
            r0.setRequestProperty(r3, r7)     // Catch:{ all -> 0x0134 }
            java.lang.String r3 = "Content-Length"
            int r7 = r2.length()     // Catch:{ all -> 0x0134 }
            java.lang.String r7 = java.lang.Integer.toString(r7)     // Catch:{ all -> 0x0134 }
            r0.setRequestProperty(r3, r7)     // Catch:{ all -> 0x0134 }
            int r3 = com.admob.android.ads.AdRequester.REQUEST_TIMEOUT     // Catch:{ all -> 0x0134 }
            r0.setConnectTimeout(r3)     // Catch:{ all -> 0x0134 }
            int r3 = com.admob.android.ads.AdRequester.REQUEST_TIMEOUT     // Catch:{ all -> 0x0134 }
            r0.setReadTimeout(r3)     // Catch:{ all -> 0x0134 }
            java.lang.String r3 = "AdMob SDK"
            r7 = 3
            boolean r3 = android.util.Log.isLoggable(r3, r7)     // Catch:{ all -> 0x0134 }
            if (r3 == 0) goto L_0x007d
            java.lang.String r3 = "AdMob SDK"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x0134 }
            r7.<init>()     // Catch:{ all -> 0x0134 }
            java.lang.String r8 = "Requesting an ad with POST parmams:  "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ all -> 0x0134 }
            java.lang.StringBuilder r7 = r7.append(r2)     // Catch:{ all -> 0x0134 }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x0134 }
            android.util.Log.d(r3, r7)     // Catch:{ all -> 0x0134 }
        L_0x007d:
            java.io.BufferedWriter r3 = new java.io.BufferedWriter     // Catch:{ all -> 0x0134 }
            java.io.OutputStreamWriter r7 = new java.io.OutputStreamWriter     // Catch:{ all -> 0x0134 }
            java.io.OutputStream r8 = r0.getOutputStream()     // Catch:{ all -> 0x0134 }
            r7.<init>(r8)     // Catch:{ all -> 0x0134 }
            r3.<init>(r7)     // Catch:{ all -> 0x0134 }
            r3.write(r2)     // Catch:{ all -> 0x013a }
            r3.close()     // Catch:{ all -> 0x013a }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ all -> 0x013a }
            java.io.InputStreamReader r7 = new java.io.InputStreamReader     // Catch:{ all -> 0x013a }
            java.io.InputStream r8 = r0.getInputStream()     // Catch:{ all -> 0x013a }
            r7.<init>(r8)     // Catch:{ all -> 0x013a }
            r2.<init>(r7)     // Catch:{ all -> 0x013a }
        L_0x009f:
            java.lang.String r7 = r2.readLine()     // Catch:{ all -> 0x00a9 }
            if (r7 == 0) goto L_0x00fa
            r6.append(r7)     // Catch:{ all -> 0x00a9 }
            goto L_0x009f
        L_0x00a9:
            r0 = move-exception
            r9 = r2
            r2 = r0
            r0 = r9
        L_0x00ad:
            if (r3 == 0) goto L_0x00b2
            r3.close()     // Catch:{ Exception -> 0x0142 }
        L_0x00b2:
            if (r0 == 0) goto L_0x00b7
            r0.close()     // Catch:{ Exception -> 0x0142 }
        L_0x00b7:
            throw r2     // Catch:{ Exception -> 0x00b8 }
        L_0x00b8:
            r0 = move-exception
        L_0x00b9:
            java.lang.String r2 = "AdMob SDK"
            java.lang.String r3 = "Could not get ad from AdMob servers."
            android.util.Log.w(r2, r3, r0)
            r0 = r1
        L_0x00c1:
            java.lang.String r2 = r6.toString()
            if (r2 == 0) goto L_0x00cb
            com.admob.android.ads.Ad r1 = com.admob.android.ads.Ad.createAd(r10, r2, r0)
        L_0x00cb:
            java.lang.String r0 = "AdMob SDK"
            r2 = 4
            boolean r0 = android.util.Log.isLoggable(r0, r2)
            if (r0 == 0) goto L_0x00f9
            long r2 = java.lang.System.currentTimeMillis()
            long r2 = r2 - r4
            if (r1 != 0) goto L_0x0111
            java.lang.String r0 = "AdMob SDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Server replied that no ads are available ("
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r3 = "ms)"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            android.util.Log.i(r0, r2)
        L_0x00f9:
            return r1
        L_0x00fa:
            java.lang.String r7 = "X-AdMob-Android-Category-Icon"
            java.lang.String r0 = r0.getHeaderField(r7)     // Catch:{ all -> 0x00a9 }
            if (r0 != 0) goto L_0x0104
            java.lang.String r0 = "http://mm.admob.com/static/android/tiles/default.png"
        L_0x0104:
            if (r3 == 0) goto L_0x0109
            r3.close()     // Catch:{ Exception -> 0x010f }
        L_0x0109:
            if (r2 == 0) goto L_0x00c1
            r2.close()     // Catch:{ Exception -> 0x010f }
            goto L_0x00c1
        L_0x010f:
            r2 = move-exception
            goto L_0x00c1
        L_0x0111:
            java.lang.String r0 = "AdMob SDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Ad returned in "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r3 = "ms:  "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r1)
            java.lang.String r2 = r2.toString()
            android.util.Log.i(r0, r2)
            goto L_0x00f9
        L_0x0134:
            r0 = move-exception
            r2 = r0
            r3 = r1
            r0 = r1
            goto L_0x00ad
        L_0x013a:
            r0 = move-exception
            r2 = r0
            r0 = r1
            goto L_0x00ad
        L_0x013f:
            r0 = move-exception
            goto L_0x00b9
        L_0x0142:
            r0 = move-exception
            goto L_0x00b7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.admob.android.ads.AdRequester.requestAd(android.content.Context, java.lang.String, java.lang.String):com.admob.android.ads.Ad");
    }
}
