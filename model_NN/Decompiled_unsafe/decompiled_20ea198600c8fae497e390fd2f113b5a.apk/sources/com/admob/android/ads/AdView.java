package com.admob.android.ads;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.RelativeLayout;
import java.util.Timer;
import java.util.TimerTask;

public class AdView extends RelativeLayout {
    private static final String ADMOB_EMULATOR_NOTICE = "http://api.admob.com/v1/pubcode/android_sdk_emulator_notice";
    private static final int ANIMATION_DURATION = 700;
    private static final float ANIMATION_Z_DEPTH_PERCENTAGE = -0.4f;
    public static final int HEIGHT = 48;
    private static boolean checkedForMessages = false;
    /* access modifiers changed from: private */
    public static Handler uiThreadHandler;
    /* access modifiers changed from: private */
    public AdContainer ad;
    private int backgroundColor;
    private boolean hideWhenNoAd;
    private boolean isOnScreen;
    /* access modifiers changed from: private */
    public String keywords;
    /* access modifiers changed from: private */
    public AdListener listener;
    /* access modifiers changed from: private */
    public int requestInterval;
    private Timer requestIntervalTimer;
    /* access modifiers changed from: private */
    public boolean requestingFreshAd;
    /* access modifiers changed from: private */
    public String searchQuery;
    private int textColor;

    public interface AdListener {
        void onFailedToReceiveAd(AdView adView);

        @Deprecated
        void onNewAd();

        void onReceiveAd(AdView adView);
    }

    private final class SwapViews implements Runnable {
        /* access modifiers changed from: private */
        public AdContainer newAd;
        /* access modifiers changed from: private */
        public AdContainer oldAd;

        public SwapViews(AdContainer adContainer) {
            this.newAd = adContainer;
        }

        public void run() {
            this.oldAd = AdView.this.ad;
            if (this.oldAd != null) {
                this.oldAd.setVisibility(8);
            }
            this.newAd.setVisibility(0);
            Rotate3dAnimation rotate3dAnimation = new Rotate3dAnimation(90.0f, 0.0f, ((float) AdView.this.getWidth()) / 2.0f, ((float) AdView.this.getHeight()) / 2.0f, AdView.ANIMATION_Z_DEPTH_PERCENTAGE * ((float) AdView.this.getWidth()), false);
            rotate3dAnimation.setDuration(700);
            rotate3dAnimation.setFillAfter(true);
            rotate3dAnimation.setInterpolator(new DecelerateInterpolator());
            rotate3dAnimation.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationEnd(Animation animation) {
                    if (SwapViews.this.oldAd != null) {
                        AdView.this.removeView(SwapViews.this.oldAd);
                        SwapViews.this.oldAd.recycleBitmaps();
                    }
                    AdContainer unused = AdView.this.ad = SwapViews.this.newAd;
                }

                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationStart(Animation animation) {
                }
            });
            AdView.this.startAnimation(rotate3dAnimation);
        }
    }

    public AdView(Context context) {
        this(context, null, 0);
    }

    public AdView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AdView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        int i2 = -1;
        int i3 = AdContainer.DEFAULT_BACKGROUND_COLOR;
        this.isOnScreen = false;
        setFocusable(true);
        setDescendantFocusability(262144);
        setClickable(true);
        if (attributeSet != null) {
            String str = "http://schemas.android.com/apk/res/" + context.getPackageName();
            boolean attributeBooleanValue = attributeSet.getAttributeBooleanValue(str, "testing", false);
            if (attributeBooleanValue) {
                AdManager.setInTestMode(attributeBooleanValue);
            }
            i2 = attributeSet.getAttributeUnsignedIntValue(str, "textColor", -1);
            i3 = attributeSet.getAttributeUnsignedIntValue(str, "backgroundColor", AdContainer.DEFAULT_BACKGROUND_COLOR);
            this.keywords = attributeSet.getAttributeValue(str, "keywords");
            setRequestInterval(attributeSet.getAttributeIntValue(str, "refreshInterval", 0));
            setGoneWithoutAd(attributeSet.getAttributeBooleanValue(str, "isGoneWithoutAd", isGoneWithoutAd()));
        }
        setTextColor(i2);
        setBackgroundColor(i3);
        if (super.getVisibility() == 0) {
            requestFreshAd();
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void applyFadeIn(AdContainer adContainer) {
        this.ad = adContainer;
        if (this.isOnScreen) {
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration(233);
            alphaAnimation.startNow();
            alphaAnimation.setFillAfter(true);
            alphaAnimation.setInterpolator(new AccelerateInterpolator());
            startAnimation(alphaAnimation);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void applyRotation(final AdContainer adContainer) {
        adContainer.setVisibility(8);
        Rotate3dAnimation rotate3dAnimation = new Rotate3dAnimation(0.0f, -90.0f, ((float) getWidth()) / 2.0f, ((float) getHeight()) / 2.0f, ANIMATION_Z_DEPTH_PERCENTAGE * ((float) getWidth()), true);
        rotate3dAnimation.setDuration(700);
        rotate3dAnimation.setFillAfter(true);
        rotate3dAnimation.setInterpolator(new AccelerateInterpolator());
        rotate3dAnimation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                AdView.this.post(new SwapViews(adContainer));
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });
        startAnimation(rotate3dAnimation);
    }

    private /* synthetic */ void manageRequestIntervalTimer(boolean z) {
        synchronized (this) {
            if (z) {
                if (this.requestInterval > 0) {
                    if (this.requestIntervalTimer == null) {
                        this.requestIntervalTimer = new Timer();
                        this.requestIntervalTimer.schedule(new TimerTask() {
                            public void run() {
                                if (Log.isLoggable("AdMob SDK", 3)) {
                                    int access$900 = AdView.this.requestInterval / 1000;
                                    if (Log.isLoggable("AdMob SDK", 3)) {
                                        Log.d("AdMob SDK", "Requesting a fresh ad because a request interval passed (" + access$900 + " seconds).");
                                    }
                                }
                                AdView.this.requestFreshAd();
                            }
                        }, (long) this.requestInterval, (long) this.requestInterval);
                    }
                }
            }
            if ((!z || this.requestInterval == 0) && this.requestIntervalTimer != null) {
                this.requestIntervalTimer.cancel();
                this.requestIntervalTimer = null;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x004a A[SYNTHETIC, Splitter:B:10:0x004a] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0075 A[SYNTHETIC, Splitter:B:23:0x0075] */
    /* JADX WARNING: Removed duplicated region for block: B:32:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private /* synthetic */ void retrieveDeveloperMessage(android.content.Context r6) {
        /*
            r5 = this;
            r2 = 0
            r0 = 0
            r1 = 0
            java.lang.String r0 = com.admob.android.ads.AdRequester.buildParamString(r6, r0, r1)     // Catch:{ Exception -> 0x0079, all -> 0x0071 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0079, all -> 0x0071 }
            r1.<init>()     // Catch:{ Exception -> 0x0079, all -> 0x0071 }
            java.lang.String r3 = "http://api.admob.com/v1/pubcode/android_sdk_emulator_notice"
            r1.append(r3)     // Catch:{ Exception -> 0x0079, all -> 0x0071 }
            java.lang.String r3 = "?"
            r1.append(r3)     // Catch:{ Exception -> 0x0079, all -> 0x0071 }
            r1.append(r0)     // Catch:{ Exception -> 0x0079, all -> 0x0071 }
            java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x0079, all -> 0x0071 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0079, all -> 0x0071 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x0079, all -> 0x0071 }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x0079, all -> 0x0071 }
            r0.connect()     // Catch:{ Exception -> 0x0079, all -> 0x0071 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0079, all -> 0x0071 }
            r3.<init>()     // Catch:{ Exception -> 0x0079, all -> 0x0071 }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0079, all -> 0x0071 }
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0079, all -> 0x0071 }
            java.io.InputStream r0 = r0.getInputStream()     // Catch:{ Exception -> 0x0079, all -> 0x0071 }
            r4.<init>(r0)     // Catch:{ Exception -> 0x0079, all -> 0x0071 }
            r1.<init>(r4)     // Catch:{ Exception -> 0x0079, all -> 0x0071 }
        L_0x003c:
            java.lang.String r0 = r1.readLine()     // Catch:{ Exception -> 0x0046, all -> 0x0080 }
            if (r0 == 0) goto L_0x004e
            r3.append(r0)     // Catch:{ Exception -> 0x0046, all -> 0x0080 }
            goto L_0x003c
        L_0x0046:
            r0 = move-exception
            r0 = r1
        L_0x0048:
            if (r0 == 0) goto L_0x004d
            r0.close()     // Catch:{ Exception -> 0x007c }
        L_0x004d:
            return
        L_0x004e:
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ Exception -> 0x0046, all -> 0x0080 }
            org.json.JSONTokener r2 = new org.json.JSONTokener     // Catch:{ Exception -> 0x0046, all -> 0x0080 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0046, all -> 0x0080 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0046, all -> 0x0080 }
            r0.<init>(r2)     // Catch:{ Exception -> 0x0046, all -> 0x0080 }
            java.lang.String r2 = "data"
            java.lang.String r0 = r0.getString(r2)     // Catch:{ Exception -> 0x0046, all -> 0x0080 }
            if (r0 == 0) goto L_0x0069
            java.lang.String r2 = "AdMob SDK"
            android.util.Log.e(r2, r0)     // Catch:{ Exception -> 0x0046, all -> 0x0080 }
        L_0x0069:
            if (r1 == 0) goto L_0x004d
            r1.close()     // Catch:{ Exception -> 0x006f }
            goto L_0x004d
        L_0x006f:
            r0 = move-exception
            goto L_0x004d
        L_0x0071:
            r0 = move-exception
            r1 = r2
        L_0x0073:
            if (r1 == 0) goto L_0x0078
            r1.close()     // Catch:{ Exception -> 0x007e }
        L_0x0078:
            throw r0
        L_0x0079:
            r0 = move-exception
            r0 = r2
            goto L_0x0048
        L_0x007c:
            r0 = move-exception
            goto L_0x004d
        L_0x007e:
            r1 = move-exception
            goto L_0x0078
        L_0x0080:
            r0 = move-exception
            goto L_0x0073
        */
        throw new UnsupportedOperationException("Method not decompiled: com.admob.android.ads.AdView.retrieveDeveloperMessage(android.content.Context):void");
    }

    public int getBackgroundColor() {
        return this.backgroundColor;
    }

    public String getKeywords() {
        return this.keywords;
    }

    public int getRequestInterval() {
        return this.requestInterval / 1000;
    }

    public String getSearchQuery() {
        return this.searchQuery;
    }

    public int getTextColor() {
        return this.textColor;
    }

    public int getVisibility() {
        if (!this.hideWhenNoAd || hasAd()) {
            return super.getVisibility();
        }
        return 8;
    }

    public boolean hasAd() {
        return this.ad != null;
    }

    public boolean isGoneWithoutAd() {
        return this.hideWhenNoAd;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        this.isOnScreen = true;
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        this.isOnScreen = false;
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        setMeasuredDimension(getMeasuredWidth(), 48);
    }

    public void onWindowFocusChanged(boolean z) {
        manageRequestIntervalTimer(z);
    }

    public void requestFreshAd() {
        Context context = getContext();
        if (AdManager.getUserId(context) == null && !checkedForMessages) {
            checkedForMessages = true;
            retrieveDeveloperMessage(context);
        }
        if (super.getVisibility() != 0) {
            Log.w("AdMob SDK", "Cannot requestFreshAd() when the AdView is not visible.  Call AdView.setVisibility(View.VISIBLE) first.");
        } else if (!this.requestingFreshAd) {
            this.requestingFreshAd = true;
            if (uiThreadHandler == null) {
                uiThreadHandler = new Handler();
            }
            new Thread() {
                public void run() {
                    try {
                        Context context = AdView.this.getContext();
                        Ad requestAd = AdRequester.requestAd(context, AdView.this.keywords, AdView.this.searchQuery);
                        if (requestAd != null) {
                            synchronized (this) {
                                if (AdView.this.ad == null || !requestAd.equals(AdView.this.ad.getAd()) || AdManager.isInTestMode()) {
                                    final boolean z = AdView.this.ad == null;
                                    final int access$401 = AdView.super.getVisibility();
                                    final AdContainer adContainer = new AdContainer(requestAd, context);
                                    adContainer.setBackgroundColor(AdView.this.getBackgroundColor());
                                    adContainer.setTextColor(AdView.this.getTextColor());
                                    adContainer.setVisibility(access$401);
                                    adContainer.setLayoutParams(new RelativeLayout.LayoutParams(-1, 48));
                                    if (AdView.this.listener != null) {
                                        try {
                                            AdView.this.listener.onNewAd();
                                            AdView.this.listener.onReceiveAd(AdView.this);
                                        } catch (Exception e) {
                                            Log.w("AdMob SDK", "Unhandled exception raised in your AdListener.onReceiveAd.", e);
                                        }
                                    }
                                    AdView.uiThreadHandler.post(new Runnable() {
                                        public void run() {
                                            try {
                                                AdView.this.addView(adContainer);
                                                if (access$401 != 0) {
                                                    AdContainer unused = AdView.this.ad = adContainer;
                                                } else if (z) {
                                                    AdView.this.applyFadeIn(adContainer);
                                                } else {
                                                    AdView.this.applyRotation(adContainer);
                                                }
                                            } catch (Exception e) {
                                                Log.e("AdMob SDK", "Unhandled exception placing AdContainer into AdView.", e);
                                            } finally {
                                                boolean unused2 = AdView.this.requestingFreshAd = false;
                                            }
                                        }
                                    });
                                } else {
                                    if (Log.isLoggable("AdMob SDK", 3)) {
                                        Log.d("AdMob SDK", "Received the same ad we already had.  Discarding it.");
                                    }
                                    boolean unused = AdView.this.requestingFreshAd = false;
                                }
                            }
                            return;
                        }
                        if (AdView.this.listener != null) {
                            try {
                                AdView.this.listener.onFailedToReceiveAd(AdView.this);
                            } catch (Exception e2) {
                                Log.w("AdMob SDK", "Unhandled exception raised in your AdListener.onFailedToReceiveAd.", e2);
                            }
                        }
                        boolean unused2 = AdView.this.requestingFreshAd = false;
                        return;
                    } catch (Exception e3) {
                        Log.e("AdMob SDK", "Unhandled exception requesting a fresh ad.", e3);
                        boolean unused3 = AdView.this.requestingFreshAd = false;
                    }
                }
            }.start();
        } else if (Log.isLoggable("AdMob SDK", 3)) {
            Log.d("AdMob SDK", "Ignoring requestFreshAd() because we are already getting a fresh ad.");
        }
    }

    public void setBackgroundColor(int i) {
        this.backgroundColor = -16777216 | i;
        if (this.ad != null) {
            this.ad.setBackgroundColor(i);
        }
        invalidate();
    }

    public void setGoneWithoutAd(boolean z) {
        this.hideWhenNoAd = z;
    }

    public void setKeywords(String str) {
        this.keywords = str;
    }

    public void setListener(AdListener adListener) {
        synchronized (this) {
            this.listener = adListener;
        }
    }

    public void setRequestInterval(int i) {
        if (i <= 0) {
            i = 0;
        } else if (i < 15) {
            AdManager.clientError("AdView.setRequestInterval(" + i + ") seconds must be >= " + 15);
        } else if (i > 600) {
            AdManager.clientError("AdView.setRequestInterval(" + i + ") seconds must be <= " + 600);
        }
        this.requestInterval = i * 1000;
        if (i == 0) {
            manageRequestIntervalTimer(false);
            return;
        }
        Log.i("AdMob SDK", "Requesting fresh ads every " + i + " seconds.");
        manageRequestIntervalTimer(true);
    }

    public void setSearchQuery(String str) {
        this.searchQuery = str;
    }

    public void setTextColor(int i) {
        this.textColor = -16777216 | i;
        if (this.ad != null) {
            this.ad.setTextColor(i);
        }
        invalidate();
    }

    public void setVisibility(int i) {
        if (super.getVisibility() != i) {
            synchronized (this) {
                int childCount = getChildCount();
                for (int i2 = 0; i2 < childCount; i2++) {
                    getChildAt(i2).setVisibility(i);
                }
                super.setVisibility(i);
                if (i == 0) {
                    requestFreshAd();
                } else {
                    removeView(this.ad);
                    this.ad = null;
                    invalidate();
                }
            }
        }
    }
}
