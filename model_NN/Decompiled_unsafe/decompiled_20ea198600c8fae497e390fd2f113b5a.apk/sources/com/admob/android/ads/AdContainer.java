package com.admob.android.ads;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.CornerPathEffect;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.admob.android.ads.Ad;

class AdContainer extends RelativeLayout implements Animation.AnimationListener, Ad.NetworkListener {
    private static final int ADMOB_TEXT_ID = 3;
    private static final String ADS_BY_ADMOB = "Ads by AdMob";
    private static final Typeface ADS_BY_ADMOB_FONT = Typeface.create(Typeface.SANS_SERIF, 0);
    private static final float ADS_BY_ADMOB_FONT_SIZE = 9.5f;
    private static final Typeface AD_FONT = Typeface.create(Typeface.SANS_SERIF, 1);
    private static final float AD_FONT_SIZE = 13.0f;
    private static final int AD_TEXT_ID = 2;
    public static final int DEFAULT_BACKGROUND_COLOR = -16777216;
    public static final int DEFAULT_TEXT_COLOR = -1;
    private static final int FOCUS_COLOR = -1147097;
    private static final float FOCUS_CORNER_ROUNDING = 3.0f;
    private static final float FOCUS_WIDTH = 3.0f;
    private static final int GRADIENT_BACKGROUND_COLOR = -1;
    private static final double GRADIENT_STOP = 0.4375d;
    private static final int GRADIENT_TOP_ALPHA = 127;
    private static final int HIGHLIGHT_BACKGROUND_COLOR = -1147097;
    private static final int HIGHLIGHT_COLOR = -19456;
    private static final int HIGHLIGHT_TEXT_COLOR = -16777216;
    private static final int ICON_ID = 1;
    public static final int MAX_WIDTH = 320;
    private static final int NUM_MILLIS_IN_SECS = 1000;
    private static final int PADDING_DEFAULT = 8;
    private static final float PULSE_ANIMATION_DURATION = 0.5f;
    private static final float PULSE_GROWN_SCALE = 1.2f;
    private static final float PULSE_GROW_KEY_TIME = 0.4f;
    private static final float PULSE_INITIAL_SCALE = 1.0f;
    private static final float PULSE_SHRUNKEN_SCALE = 0.001f;
    /* access modifiers changed from: private */
    public ProgressBar activityIndicator;
    /* access modifiers changed from: private */
    public Ad ad;
    private TextView adMobBrandingTextView;
    private TextView adTextView;
    private int backgroundColor;
    /* access modifiers changed from: private */
    public boolean clickInProgress;
    private BitmapDrawable defaultBackground;
    private BitmapDrawable focusedBackground;
    /* access modifiers changed from: private */
    public ImageView iconView;
    private Drawable lastBackground;
    private int padding;
    private BitmapDrawable pressedBackground;
    private int textColor;

    public AdContainer(Ad ad2, Context context) {
        super(context);
        this.backgroundColor = -16777216;
        this.textColor = -1;
        init(ad2, context, null, 0);
    }

    public AdContainer(Ad ad2, Context context, AttributeSet attributeSet) {
        this(ad2, context, attributeSet, 0);
    }

    public AdContainer(Ad ad2, Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.backgroundColor = -16777216;
        this.textColor = -1;
        init(ad2, context, attributeSet, i);
    }

    private /* synthetic */ void click() {
        if (this.ad != null && isPressed()) {
            setPressed(false);
            if (!this.clickInProgress) {
                this.clickInProgress = true;
                if (this.iconView != null) {
                    AnimationSet animationSet = new AnimationSet(true);
                    float width = ((float) this.iconView.getWidth()) / 2.0f;
                    float height = ((float) this.iconView.getHeight()) / 2.0f;
                    ScaleAnimation scaleAnimation = new ScaleAnimation(PULSE_INITIAL_SCALE, PULSE_GROWN_SCALE, PULSE_INITIAL_SCALE, PULSE_GROWN_SCALE, width, height);
                    scaleAnimation.setDuration(200);
                    animationSet.addAnimation(scaleAnimation);
                    ScaleAnimation scaleAnimation2 = new ScaleAnimation(PULSE_GROWN_SCALE, PULSE_SHRUNKEN_SCALE, PULSE_GROWN_SCALE, PULSE_SHRUNKEN_SCALE, width, height);
                    scaleAnimation2.setDuration(299);
                    scaleAnimation2.setStartOffset(200);
                    scaleAnimation2.setAnimationListener(this);
                    animationSet.addAnimation(scaleAnimation2);
                    postDelayed(new Thread() {
                        public void run() {
                            AdContainer.this.ad.clicked();
                        }
                    }, 500);
                    this.iconView.startAnimation(animationSet);
                    return;
                }
                this.ad.clicked();
            }
        }
    }

    private static /* synthetic */ void drawBackground(Canvas canvas, Rect rect, int i, int i2) {
        Paint paint = new Paint();
        paint.setColor(i);
        paint.setAntiAlias(true);
        canvas.drawRect(rect, paint);
        int argb = Color.argb((int) GRADIENT_TOP_ALPHA, Color.red(i2), Color.green(i2), Color.blue(i2));
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{argb, i2});
        int height = ((int) (((double) rect.height()) * GRADIENT_STOP)) + rect.top;
        gradientDrawable.setBounds(rect.left, rect.top, rect.right, height);
        gradientDrawable.draw(canvas);
        Rect rect2 = new Rect(rect.left, height, rect.right, rect.bottom);
        Paint paint2 = new Paint();
        paint2.setColor(i2);
        canvas.drawRect(rect2, paint2);
    }

    private static /* synthetic */ void drawFocusRing(Canvas canvas, Rect rect) {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(-1147097);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(3.0f);
        paint.setPathEffect(new CornerPathEffect(3.0f));
        Path path = new Path();
        path.addRoundRect(new RectF(rect), 3.0f, 3.0f, Path.Direction.CW);
        canvas.drawPath(path, paint);
    }

    private /* synthetic */ BitmapDrawable generateBackgroundDrawable(Rect rect, int i, int i2) {
        return generateBackgroundDrawable(rect, i, i2, false);
    }

    private /* synthetic */ BitmapDrawable generateBackgroundDrawable(Rect rect, int i, int i2, boolean z) {
        try {
            Bitmap createBitmap = Bitmap.createBitmap(rect.width(), rect.height(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(createBitmap);
            drawBackground(canvas, rect, i, i2);
            if (z) {
                drawFocusRing(canvas, rect);
            }
            return new BitmapDrawable(createBitmap);
        } catch (Throwable th) {
            return null;
        }
    }

    private /* synthetic */ void init(Ad ad2, Context context, AttributeSet attributeSet, int i) {
        int i2 = -16777216;
        int i3 = -1;
        this.ad = ad2;
        ad2.setNetworkListener(this);
        this.defaultBackground = null;
        this.pressedBackground = null;
        this.focusedBackground = null;
        this.activityIndicator = null;
        this.clickInProgress = false;
        if (ad2 != null) {
            setFocusable(true);
            setClickable(true);
            Bitmap icon = ad2.getIcon();
            this.iconView = null;
            this.padding = PADDING_DEFAULT;
            if (icon != null) {
                this.padding = (48 - icon.getHeight()) / 2;
                this.iconView = new ImageView(context);
                this.iconView.setImageBitmap(icon);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(icon.getWidth(), icon.getHeight());
                layoutParams.setMargins(this.padding, this.padding, 0, this.padding);
                this.iconView.setLayoutParams(layoutParams);
                this.iconView.setId(1);
                addView(this.iconView);
                this.activityIndicator = new ProgressBar(context);
                this.activityIndicator.setIndeterminate(true);
                this.activityIndicator.setId(1);
                this.activityIndicator.setLayoutParams(layoutParams);
                this.activityIndicator.setVisibility(4);
                addView(this.activityIndicator);
            }
            this.adTextView = new TextView(context);
            this.adTextView.setText(ad2.getText());
            this.adTextView.setTypeface(AD_FONT);
            this.adTextView.setTextColor(this.textColor);
            this.adTextView.setTextSize(AD_FONT_SIZE);
            this.adTextView.setId(2);
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -1);
            if (icon != null) {
                layoutParams2.addRule(1, 1);
            }
            layoutParams2.setMargins(this.padding, this.padding, this.padding, this.padding);
            layoutParams2.addRule(11);
            layoutParams2.addRule(10);
            this.adTextView.setLayoutParams(layoutParams2);
            addView(this.adTextView);
            this.adMobBrandingTextView = new TextView(context);
            this.adMobBrandingTextView.setGravity(5);
            this.adMobBrandingTextView.setText(AdMobLocalizer.localize(ADS_BY_ADMOB));
            this.adMobBrandingTextView.setTypeface(ADS_BY_ADMOB_FONT);
            this.adMobBrandingTextView.setTextColor(this.textColor);
            this.adMobBrandingTextView.setTextSize(ADS_BY_ADMOB_FONT_SIZE);
            this.adMobBrandingTextView.setId(3);
            RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams3.setMargins(0, 0, this.padding, this.padding);
            layoutParams3.addRule(11);
            layoutParams3.addRule(12);
            this.adMobBrandingTextView.setLayoutParams(layoutParams3);
            addView(this.adMobBrandingTextView);
        }
        if (attributeSet != null) {
            String str = "http://schemas.android.com/apk/res/" + context.getPackageName();
            i3 = attributeSet.getAttributeUnsignedIntValue(str, "textColor", -1);
            i2 = attributeSet.getAttributeUnsignedIntValue(str, "backgroundColor", -16777216);
        }
        setTextColor(i3);
        setBackgroundColor(i2);
    }

    private /* synthetic */ void recycleBitmapDrawable(BitmapDrawable bitmapDrawable) {
        Bitmap bitmap;
        if (bitmapDrawable != null && (bitmap = bitmapDrawable.getBitmap()) != null) {
            bitmap.recycle();
        }
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (Log.isLoggable("AdMob SDK", 2)) {
            Log.v("AdMob SDK", "dispatchTouchEvent: action=" + action + " x=" + motionEvent.getX() + " y=" + motionEvent.getY());
        }
        if (action == 0) {
            setPressed(true);
        } else if (action == 2) {
            float x = motionEvent.getX();
            float y = motionEvent.getY();
            int left = getLeft();
            int top = getTop();
            int right = getRight();
            int bottom = getBottom();
            if (x < ((float) left) || x > ((float) right) || y < ((float) top) || y > ((float) bottom)) {
                setPressed(false);
            } else {
                setPressed(true);
            }
        } else if (action == 1) {
            if (isPressed()) {
                click();
            }
            setPressed(false);
        } else if (action == 3) {
            setPressed(false);
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    public boolean dispatchTrackballEvent(MotionEvent motionEvent) {
        if (Log.isLoggable("AdMob SDK", 2)) {
            Log.v("AdMob SDK", "dispatchTrackballEvent: action=" + motionEvent.getAction());
        }
        if (motionEvent.getAction() == 0) {
            setPressed(true);
        } else if (motionEvent.getAction() == 1) {
            if (hasFocus()) {
                click();
            }
            setPressed(false);
        }
        return super.onTrackballEvent(motionEvent);
    }

    /* access modifiers changed from: protected */
    public Ad getAd() {
        return this.ad;
    }

    public int getBackgroundColor() {
        return this.backgroundColor;
    }

    public int getTextColor() {
        return this.textColor;
    }

    public void onAnimationEnd(Animation animation) {
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }

    /* access modifiers changed from: protected */
    public void onFocusChanged(boolean z, int i, Rect rect) {
        super.onFocusChanged(z, i, rect);
        if (z) {
            setBackgroundDrawable(this.focusedBackground);
        } else {
            setBackgroundDrawable(this.defaultBackground);
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (Log.isLoggable("AdMob SDK", 2)) {
            Log.v("AdMob SDK", "onKeyDown: keyCode=" + i);
        }
        if (i == 66 || i == 23) {
            setPressed(true);
        }
        return super.onKeyDown(i, keyEvent);
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (Log.isLoggable("AdMob SDK", 2)) {
            Log.v("AdMob SDK", "onKeyUp: keyCode=" + i);
        }
        if (i == 66 || i == 23) {
            click();
        }
        setPressed(false);
        return super.onKeyUp(i, keyEvent);
    }

    public void onNetworkActivityEnd() {
        post(new Thread() {
            public void run() {
                if (AdContainer.this.activityIndicator != null) {
                    AdContainer.this.activityIndicator.setVisibility(4);
                }
                if (AdContainer.this.iconView != null) {
                    AdContainer.this.iconView.setImageMatrix(new Matrix());
                    AdContainer.this.iconView.setVisibility(0);
                }
                boolean unused = AdContainer.this.clickInProgress = false;
            }
        });
    }

    public void onNetworkActivityStart() {
        post(new Thread() {
            public void run() {
                if (AdContainer.this.iconView != null) {
                    AdContainer.this.iconView.setVisibility(4);
                }
                if (AdContainer.this.activityIndicator != null) {
                    AdContainer.this.activityIndicator.setVisibility(0);
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0061, code lost:
        if (r4 > r2) goto L_0x0063;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onSizeChanged(int r9, int r10, int r11, int r12) {
        /*
            r8 = this;
            r0 = 8
            r7 = -1
            r3 = 0
            super.onSizeChanged(r9, r10, r11, r12)
            android.widget.TextView r1 = r8.adMobBrandingTextView
            if (r1 == 0) goto L_0x0068
            android.widget.TextView r1 = r8.adTextView
            if (r1 == 0) goto L_0x0068
            android.widget.TextView r1 = r8.adMobBrandingTextView
            int r1 = r1.getVisibility()
            if (r9 <= 0) goto L_0x0024
            r1 = 128(0x80, float:1.794E-43)
            if (r9 > r1) goto L_0x0093
            android.widget.TextView r1 = r8.adTextView
            r2 = 1091672473(0x41119999, float:9.099999)
            r1.setTextSize(r2)
            r1 = r0
        L_0x0024:
            if (r1 != 0) goto L_0x00bb
            android.widget.TextView r2 = r8.adTextView
            android.graphics.Typeface r2 = r2.getTypeface()
            com.admob.android.ads.Ad r4 = r8.ad
            java.lang.String r4 = r4.getText()
            if (r4 == 0) goto L_0x00bb
            android.graphics.Paint r5 = new android.graphics.Paint
            r5.<init>()
            r5.setTypeface(r2)
            android.widget.TextView r2 = r8.adTextView
            float r2 = r2.getTextSize()
            r5.setTextSize(r2)
            float r4 = r5.measureText(r4)
            int r2 = r8.padding
            int r2 = r2 * 2
            int r2 = r9 - r2
            float r2 = (float) r2
            android.widget.ImageView r5 = r8.iconView
            if (r5 == 0) goto L_0x005f
            android.widget.ImageView r5 = r8.iconView
            int r5 = r5.getWidth()
            int r6 = r8.padding
            int r5 = r5 + r6
            float r5 = (float) r5
            float r2 = r2 - r5
        L_0x005f:
            int r2 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r2 <= 0) goto L_0x00bb
        L_0x0063:
            android.widget.TextView r1 = r8.adMobBrandingTextView
            r1.setVisibility(r0)
        L_0x0068:
            if (r9 == 0) goto L_0x0092
            if (r10 == 0) goto L_0x0092
            android.graphics.Rect r0 = new android.graphics.Rect
            r0.<init>(r3, r3, r9, r10)
            int r1 = r8.backgroundColor
            android.graphics.drawable.BitmapDrawable r1 = r8.generateBackgroundDrawable(r0, r7, r1)
            r8.defaultBackground = r1
            r1 = -1147097(0xffffffffffee7f27, float:NaN)
            r2 = -19456(0xffffffffffffb400, float:NaN)
            android.graphics.drawable.BitmapDrawable r1 = r8.generateBackgroundDrawable(r0, r1, r2)
            r8.pressedBackground = r1
            int r1 = r8.backgroundColor
            r2 = 1
            android.graphics.drawable.BitmapDrawable r0 = r8.generateBackgroundDrawable(r0, r7, r1, r2)
            r8.focusedBackground = r0
            android.graphics.drawable.BitmapDrawable r0 = r8.defaultBackground
            r8.setBackgroundDrawable(r0)
        L_0x0092:
            return
        L_0x0093:
            r1 = 176(0xb0, float:2.47E-43)
            if (r9 > r1) goto L_0x00aa
            android.widget.TextView r1 = r8.adTextView
            r2 = 1093035623(0x41266667, float:10.400001)
            r1.setTextSize(r2)
            android.widget.TextView r1 = r8.adMobBrandingTextView
            r2 = 1089680179(0x40f33333, float:7.6)
            r1.setTextSize(r2)
            r1 = r3
            goto L_0x0024
        L_0x00aa:
            android.widget.TextView r1 = r8.adTextView
            r2 = 1095761920(0x41500000, float:13.0)
            r1.setTextSize(r2)
            android.widget.TextView r1 = r8.adMobBrandingTextView
            r2 = 1092091904(0x41180000, float:9.5)
            r1.setTextSize(r2)
            r1 = r3
            goto L_0x0024
        L_0x00bb:
            r0 = r1
            goto L_0x0063
        */
        throw new UnsupportedOperationException("Method not decompiled: com.admob.android.ads.AdContainer.onSizeChanged(int, int, int, int):void");
    }

    /* access modifiers changed from: package-private */
    public void recycleBitmaps() {
        if (this.defaultBackground != null) {
            BitmapDrawable bitmapDrawable = this.defaultBackground;
            this.defaultBackground = null;
            recycleBitmapDrawable(bitmapDrawable);
        }
        if (this.pressedBackground != null) {
            BitmapDrawable bitmapDrawable2 = this.pressedBackground;
            this.pressedBackground = null;
            recycleBitmapDrawable(bitmapDrawable2);
        }
        if (this.focusedBackground != null) {
            BitmapDrawable bitmapDrawable3 = this.focusedBackground;
            this.focusedBackground = null;
            recycleBitmapDrawable(bitmapDrawable3);
        }
    }

    public void setBackgroundColor(int i) {
        this.backgroundColor = -16777216 | i;
    }

    public void setPressed(boolean z) {
        Drawable drawable;
        if ((!z || !this.clickInProgress) && isPressed() != z) {
            BitmapDrawable bitmapDrawable = this.defaultBackground;
            int i = this.textColor;
            if (z) {
                this.lastBackground = getBackground();
                drawable = this.pressedBackground;
                i = -16777216;
            } else {
                drawable = this.lastBackground;
            }
            setBackgroundDrawable(drawable);
            if (this.adTextView != null) {
                this.adTextView.setTextColor(i);
            }
            if (this.adMobBrandingTextView != null) {
                this.adMobBrandingTextView.setTextColor(i);
            }
            super.setPressed(z);
            invalidate();
        }
    }

    public void setTextColor(int i) {
        this.textColor = -16777216 | i;
        if (this.adTextView != null) {
            this.adTextView.setTextColor(this.textColor);
        }
        if (this.adMobBrandingTextView != null) {
            this.adMobBrandingTextView.setTextColor(this.textColor);
        }
        postInvalidate();
    }
}
