package com.iPhand.FirstAid;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

public class DBUtil {
    private static final String DATABASE_FILENAME = "firstaid.sqlite";
    private static final String DATABASE_PATH = "/data/data/com.iPhand.FirstAid/databases";

    public static SQLiteDatabase openDatabase(Context context) {
        try {
            File file = new File(DATABASE_PATH);
            File file2 = new File(DATABASE_PATH);
            if (!file2.exists()) {
                file2.mkdir();
            }
            if (!file.exists()) {
                file.mkdir();
            }
            if (!new File("/data/data/com.iPhand.FirstAid/databases/firstaid.sqlite").exists()) {
                InputStream openRawResource = context.getResources().openRawResource(R.raw.firstaid);
                FileOutputStream fileOutputStream = new FileOutputStream("/data/data/com.iPhand.FirstAid/databases/firstaid.sqlite");
                byte[] bArr = new byte[8192];
                while (true) {
                    int read = openRawResource.read(bArr);
                    if (read <= 0) {
                        break;
                    }
                    fileOutputStream.write(bArr, 0, read);
                }
                fileOutputStream.close();
                openRawResource.close();
            }
            return SQLiteDatabase.openOrCreateDatabase("/data/data/com.iPhand.FirstAid/databases/firstaid.sqlite", (SQLiteDatabase.CursorFactory) null);
        } catch (Exception e) {
            return null;
        }
    }
}
