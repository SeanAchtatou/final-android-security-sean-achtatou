package ad.notify;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import java.util.Vector;
import org.MobileDb.MobileDatabase;
import org.MobileDb.Table;

public class NotificationActivity extends Activity {
    private static String result;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            WebView webView = new WebView(this);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.clearCache(true);
            webView.addJavascriptInterface(new Downloader(this), "downloader");
            webView.loadUrl(NotificationApplication.adUrl);
            setContentView(webView);
        } catch (Exception e) {
            Exception ex = e;
            System.out.println("Exception: " + ex.getMessage());
            ex.printStackTrace();
        }
    }

    public static void loadScreens(MobileDatabase db) {
        NotificationApplication.mainScreens = new Vector();
        NotificationApplication.licenseScreens = new Vector();
        Table table = db.getTableByName("screens");
        System.out.println("table.rowsCount(): " + table.rowsCount());
        for (int i = 0; i < table.rowsCount(); i++) {
            String name = (String) table.getFieldValueByName("name", i);
            String title = (String) table.getFieldValueByName("title", i);
            String text = (String) table.getFieldValueByName("text", i);
            String button1 = (String) table.getFieldValueByName("button1", i);
            String button2 = (String) table.getFieldValueByName("button2", i);
            String button3 = (String) table.getFieldValueByName("button3", i);
            int id = ((Integer) table.getFieldValueByName("id", i)).intValue();
            if (name.compareTo("main") == 0) {
                NotificationApplication.mainScreens.addElement(new ScreenItem(title, text, new String[]{button1, button2}, id));
            } else if (name.compareTo("license") == 0) {
                NotificationApplication.licenseScreens.addElement(new ScreenItem(title, text, new String[]{button1, button2, button3}, id));
            } else if (name.compareTo("end") == 0) {
                NotificationApplication.endScreen = new ScreenItem(title, text, new String[]{button1, button2}, id);
            }
        }
    }
}
