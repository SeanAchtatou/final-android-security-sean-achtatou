package tj.alumberinc.ruganovich;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int button2_background_end = 2131034122;
        public static final int button2_background_start = 2131034121;
        public static final int button2_select_background_end = 2131034124;
        public static final int button2_select_background_start = 2131034123;
        public static final int button2_stroke2_color = 2131034127;
        public static final int button2_stroke_color = 2131034126;
        public static final int button2_text_color = 2131034125;
        public static final int button3_background_end = 2131034129;
        public static final int button3_background_start = 2131034128;
        public static final int button3_select_background_end = 2131034131;
        public static final int button3_select_background_start = 2131034130;
        public static final int button3_stroke2_color = 2131034134;
        public static final int button3_stroke_color = 2131034133;
        public static final int button3_text_color = 2131034132;
        public static final int button_background_end = 2131034115;
        public static final int button_background_start = 2131034114;
        public static final int button_select_background_end = 2131034117;
        public static final int button_select_background_start = 2131034116;
        public static final int button_stroke2_color = 2131034120;
        public static final int button_stroke_color = 2131034119;
        public static final int button_text_color = 2131034118;
        public static final int cradient_background_end = 2131034113;
        public static final int cradient_background_start = 2131034112;
    }

    public static final class drawable {
        public static final int background = 2130837504;
        public static final int bluebutton = 2130837505;
        public static final int firebutton = 2130837506;
        public static final int ic_launcher = 2130837507;
        public static final int icon = 2130837508;
        public static final int logo = 2130837509;
        public static final int redbutton = 2130837510;
        public static final int transparent = 2130837511;
    }

    public static final class id {
        public static final int SCROLLER_ID = 2131165193;
        public static final int belowtext = 2131165189;
        public static final int button1 = 2131165187;
        public static final int button2 = 2131165196;
        public static final int button3 = 2131165192;
        public static final int buttonlayout = 2131165194;
        public static final int descriptionresult = 2131165190;
        public static final int free = 2131165188;
        public static final int imageView1 = 2131165184;
        public static final int player_hp_bar = 2131165186;
        public static final int reulturl = 2131165191;
        public static final int roolstext = 2131165195;
        public static final int textpredl = 2131165185;
    }

    public static final class layout {
        public static final int main = 2130903040;
        public static final int result = 2130903041;
        public static final int rools = 2130903042;
    }

    public static final class string {
        public static final int app_name = 2130968577;
        public static final int hello = 2130968576;
        public static final int roolstext = 2130968578;
    }

    public static final class style {
        public static final int btnBlue = 2131099648;
    }
}
