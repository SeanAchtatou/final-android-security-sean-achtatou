package com.sun.activation.registries;

import java.util.logging.Level;
import java.util.logging.Logger;

public class LogSupport {
    private static boolean debug;
    private static final Level level = Level.FINE;
    private static Logger logger = Logger.getLogger("javax.activation");

    static {
        debug = false;
        try {
            debug = Boolean.getBoolean("javax.activation.debug");
        } catch (Throwable th) {
        }
    }

    private LogSupport() {
    }

    public static void log(String str) {
        if (debug) {
            System.out.println(str);
        }
        logger.log(level, str);
    }

    public static void log(String str, Throwable th) {
        if (debug) {
            System.out.println(String.valueOf(str) + "; Exception: " + th);
        }
        logger.log(level, str, th);
    }

    public static boolean isLoggable() {
        return debug || logger.isLoggable(level);
    }
}
