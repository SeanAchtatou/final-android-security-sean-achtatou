package com.sun.activation.registries;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class MailcapFile {
    private static boolean addReverse;
    private Map fallback_hash = new HashMap();
    private Map native_commands = new HashMap();
    private Map type_hash = new HashMap();

    static {
        addReverse = false;
        try {
            addReverse = Boolean.getBoolean("javax.activation.addreverse");
        } catch (Throwable th) {
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0048 A[SYNTHETIC, Splitter:B:14:0x0048] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public MailcapFile(java.lang.String r4) throws java.io.IOException {
        /*
            r3 = this;
            r3.<init>()
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            r3.type_hash = r0
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            r3.fallback_hash = r0
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            r3.native_commands = r0
            boolean r0 = com.sun.activation.registries.LogSupport.isLoggable()
            if (r0 == 0) goto L_0x0030
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = "new MailcapFile: file "
            r0.<init>(r1)
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r0 = r0.toString()
            com.sun.activation.registries.LogSupport.log(r0)
        L_0x0030:
            r2 = 0
            java.io.FileReader r1 = new java.io.FileReader     // Catch:{ all -> 0x0044 }
            r1.<init>(r4)     // Catch:{ all -> 0x0044 }
            java.io.BufferedReader r0 = new java.io.BufferedReader     // Catch:{ all -> 0x0050 }
            r0.<init>(r1)     // Catch:{ all -> 0x0050 }
            r3.parse(r0)     // Catch:{ all -> 0x0050 }
            if (r1 == 0) goto L_0x0043
            r1.close()     // Catch:{ IOException -> 0x004e }
        L_0x0043:
            return
        L_0x0044:
            r0 = move-exception
            r1 = r2
        L_0x0046:
            if (r1 == 0) goto L_0x004b
            r1.close()     // Catch:{ IOException -> 0x004c }
        L_0x004b:
            throw r0
        L_0x004c:
            r1 = move-exception
            goto L_0x004b
        L_0x004e:
            r0 = move-exception
            goto L_0x0043
        L_0x0050:
            r0 = move-exception
            goto L_0x0046
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sun.activation.registries.MailcapFile.<init>(java.lang.String):void");
    }

    public MailcapFile(InputStream inputStream) throws IOException {
        if (LogSupport.isLoggable()) {
            LogSupport.log("new MailcapFile: InputStream");
        }
        parse(new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1")));
    }

    public MailcapFile() {
        if (LogSupport.isLoggable()) {
            LogSupport.log("new MailcapFile: default");
        }
    }

    public Map getMailcapList(String str) {
        Map map = (Map) this.type_hash.get(str);
        int indexOf = str.indexOf(47);
        if (str.substring(indexOf + 1).equals("*")) {
            return map;
        }
        Map map2 = (Map) this.type_hash.get(String.valueOf(str.substring(0, indexOf + 1)) + "*");
        if (map2 == null) {
            return map;
        }
        if (map != null) {
            return mergeResults(map, map2);
        }
        return map2;
    }

    public Map getMailcapFallbackList(String str) {
        Map map = (Map) this.fallback_hash.get(str);
        int indexOf = str.indexOf(47);
        if (str.substring(indexOf + 1).equals("*")) {
            return map;
        }
        Map map2 = (Map) this.fallback_hash.get(String.valueOf(str.substring(0, indexOf + 1)) + "*");
        if (map2 == null) {
            return map;
        }
        if (map != null) {
            return mergeResults(map, map2);
        }
        return map2;
    }

    public String[] getMimeTypes() {
        HashSet hashSet = new HashSet(this.type_hash.keySet());
        hashSet.addAll(this.fallback_hash.keySet());
        hashSet.addAll(this.native_commands.keySet());
        return (String[]) hashSet.toArray(new String[hashSet.size()]);
    }

    public String[] getNativeCommands(String str) {
        String[] strArr = null;
        List list = (List) this.native_commands.get(str.toLowerCase(Locale.ENGLISH));
        if (list != null) {
            return (String[]) list.toArray(new String[list.size()]);
        }
        return strArr;
    }

    private Map mergeResults(Map map, Map map2) {
        HashMap hashMap = new HashMap(map);
        for (String str : map2.keySet()) {
            List list = (List) hashMap.get(str);
            if (list == null) {
                hashMap.put(str, map2.get(str));
            } else {
                ArrayList arrayList = new ArrayList(list);
                arrayList.addAll((List) map2.get(str));
                hashMap.put(str, arrayList);
            }
        }
        return hashMap;
    }

    public void appendToMailcap(String str) {
        if (LogSupport.isLoggable()) {
            LogSupport.log("appendToMailcap: " + str);
        }
        try {
            parse(new StringReader(str));
        } catch (IOException e) {
        }
    }

    private void parse(Reader reader) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(reader);
        String str = null;
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine != null) {
                String trim = readLine.trim();
                try {
                    if (trim.charAt(0) != '#') {
                        if (trim.charAt(trim.length() - 1) == '\\') {
                            if (str != null) {
                                str = String.valueOf(str) + trim.substring(0, trim.length() - 1);
                            } else {
                                str = trim.substring(0, trim.length() - 1);
                            }
                        } else if (str != null) {
                            try {
                                parseLine(String.valueOf(str) + trim);
                            } catch (MailcapParseException e) {
                            }
                            str = null;
                        } else {
                            try {
                                parseLine(trim);
                            } catch (MailcapParseException e2) {
                            }
                        }
                    }
                } catch (StringIndexOutOfBoundsException e3) {
                }
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void parseLine(String str) throws MailcapParseException, IOException {
        int i;
        int i2;
        boolean z;
        MailcapTokenizer mailcapTokenizer = new MailcapTokenizer(str);
        mailcapTokenizer.setIsAutoquoting(false);
        if (LogSupport.isLoggable()) {
            LogSupport.log("parse: " + str);
        }
        int nextToken = mailcapTokenizer.nextToken();
        if (nextToken != 2) {
            reportParseError(2, nextToken, mailcapTokenizer.getCurrentTokenValue());
        }
        String lowerCase = mailcapTokenizer.getCurrentTokenValue().toLowerCase(Locale.ENGLISH);
        String str2 = "*";
        int nextToken2 = mailcapTokenizer.nextToken();
        if (!(nextToken2 == 47 || nextToken2 == 59)) {
            reportParseError(47, 59, nextToken2, mailcapTokenizer.getCurrentTokenValue());
        }
        if (nextToken2 == 47) {
            int nextToken3 = mailcapTokenizer.nextToken();
            if (nextToken3 != 2) {
                reportParseError(2, nextToken3, mailcapTokenizer.getCurrentTokenValue());
            }
            str2 = mailcapTokenizer.getCurrentTokenValue().toLowerCase(Locale.ENGLISH);
            nextToken2 = mailcapTokenizer.nextToken();
        }
        String str3 = String.valueOf(lowerCase) + "/" + str2;
        if (LogSupport.isLoggable()) {
            LogSupport.log("  Type: " + str3);
        }
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        if (nextToken2 != 59) {
            reportParseError(59, nextToken2, mailcapTokenizer.getCurrentTokenValue());
        }
        mailcapTokenizer.setIsAutoquoting(true);
        int nextToken4 = mailcapTokenizer.nextToken();
        mailcapTokenizer.setIsAutoquoting(false);
        if (!(nextToken4 == 2 || nextToken4 == 59)) {
            reportParseError(2, 59, nextToken4, mailcapTokenizer.getCurrentTokenValue());
        }
        if (nextToken4 == 2) {
            List list = (List) this.native_commands.get(str3);
            if (list == null) {
                ArrayList arrayList = new ArrayList();
                arrayList.add(str);
                this.native_commands.put(str3, arrayList);
            } else {
                list.add(str);
            }
        }
        if (nextToken4 != 59) {
            i = mailcapTokenizer.nextToken();
        } else {
            i = nextToken4;
        }
        if (i == 59) {
            boolean z2 = false;
            while (true) {
                int nextToken5 = mailcapTokenizer.nextToken();
                if (nextToken5 != 2) {
                    reportParseError(2, nextToken5, mailcapTokenizer.getCurrentTokenValue());
                }
                String lowerCase2 = mailcapTokenizer.getCurrentTokenValue().toLowerCase(Locale.ENGLISH);
                int nextToken6 = mailcapTokenizer.nextToken();
                if (!(nextToken6 == 61 || nextToken6 == 59 || nextToken6 == 5)) {
                    reportParseError(61, 59, 5, nextToken6, mailcapTokenizer.getCurrentTokenValue());
                }
                if (nextToken6 == 61) {
                    mailcapTokenizer.setIsAutoquoting(true);
                    int nextToken7 = mailcapTokenizer.nextToken();
                    mailcapTokenizer.setIsAutoquoting(false);
                    if (nextToken7 != 2) {
                        reportParseError(2, nextToken7, mailcapTokenizer.getCurrentTokenValue());
                    }
                    String currentTokenValue = mailcapTokenizer.getCurrentTokenValue();
                    if (lowerCase2.startsWith("x-java-")) {
                        String substring = lowerCase2.substring(7);
                        if (!substring.equals("fallback-entry") || !currentTokenValue.equalsIgnoreCase("true")) {
                            if (LogSupport.isLoggable()) {
                                LogSupport.log("    Command: " + substring + ", Class: " + currentTokenValue);
                            }
                            Object obj = (List) linkedHashMap.get(substring);
                            if (obj == null) {
                                obj = new ArrayList();
                                linkedHashMap.put(substring, obj);
                            }
                            if (addReverse) {
                                obj.add(0, currentTokenValue);
                            } else {
                                obj.add(currentTokenValue);
                            }
                        } else {
                            z2 = true;
                        }
                    }
                    boolean z3 = z2;
                    i2 = mailcapTokenizer.nextToken();
                    z = z3;
                } else {
                    boolean z4 = z2;
                    i2 = nextToken6;
                    z = z4;
                }
                if (i2 != 59) {
                    break;
                }
                z2 = z;
            }
            Map map = z ? this.fallback_hash : this.type_hash;
            Map map2 = (Map) map.get(str3);
            if (map2 == null) {
                map.put(str3, linkedHashMap);
                return;
            }
            if (LogSupport.isLoggable()) {
                LogSupport.log("Merging commands for type " + str3);
            }
            for (String str4 : map2.keySet()) {
                List list2 = (List) map2.get(str4);
                List<String> list3 = (List) linkedHashMap.get(str4);
                if (list3 != null) {
                    for (String str5 : list3) {
                        if (!list2.contains(str5)) {
                            if (addReverse) {
                                list2.add(0, str5);
                            } else {
                                list2.add(str5);
                            }
                        }
                    }
                }
            }
            for (String str6 : linkedHashMap.keySet()) {
                if (!map2.containsKey(str6)) {
                    map2.put(str6, (List) linkedHashMap.get(str6));
                }
            }
        } else if (i != 5) {
            reportParseError(5, 59, i, mailcapTokenizer.getCurrentTokenValue());
        }
    }

    protected static void reportParseError(int i, int i2, String str) throws MailcapParseException {
        throw new MailcapParseException("Encountered a " + MailcapTokenizer.nameForToken(i2) + " token (" + str + ") while expecting a " + MailcapTokenizer.nameForToken(i) + " token.");
    }

    protected static void reportParseError(int i, int i2, int i3, String str) throws MailcapParseException {
        throw new MailcapParseException("Encountered a " + MailcapTokenizer.nameForToken(i3) + " token (" + str + ") while expecting a " + MailcapTokenizer.nameForToken(i) + " or a " + MailcapTokenizer.nameForToken(i2) + " token.");
    }

    protected static void reportParseError(int i, int i2, int i3, int i4, String str) throws MailcapParseException {
        if (LogSupport.isLoggable()) {
            LogSupport.log("PARSE ERROR: Encountered a " + MailcapTokenizer.nameForToken(i4) + " token (" + str + ") while expecting a " + MailcapTokenizer.nameForToken(i) + ", a " + MailcapTokenizer.nameForToken(i2) + ", or a " + MailcapTokenizer.nameForToken(i3) + " token.");
        }
        throw new MailcapParseException("Encountered a " + MailcapTokenizer.nameForToken(i4) + " token (" + str + ") while expecting a " + MailcapTokenizer.nameForToken(i) + ", a " + MailcapTokenizer.nameForToken(i2) + ", or a " + MailcapTokenizer.nameForToken(i3) + " token.");
    }
}
