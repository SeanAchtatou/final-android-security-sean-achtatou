package com.sun.mail.pop3;

import java.io.EOFException;
import java.io.IOException;
import java.lang.reflect.Constructor;
import javax.mail.Folder;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.URLName;

public class POP3Store extends Store {
    private int defaultPort;
    boolean disableTop;
    boolean forgetTopHeaders;
    private String host;
    private boolean isSSL;
    Constructor messageConstructor;
    private String name;
    private String passwd;
    private Protocol port;
    private int portNum;
    private POP3Folder portOwner;
    boolean rsetBeforeQuit;
    private String user;

    public POP3Store(Session session, URLName uRLName) {
        this(session, uRLName, "pop3", 110, false);
    }

    public POP3Store(Session session, URLName uRLName, String str, int i, boolean z) {
        super(session, uRLName);
        Class<?> cls;
        this.name = "pop3";
        this.defaultPort = 110;
        this.isSSL = false;
        this.port = null;
        this.portOwner = null;
        this.host = null;
        this.portNum = -1;
        this.user = null;
        this.passwd = null;
        this.rsetBeforeQuit = false;
        this.disableTop = false;
        this.forgetTopHeaders = false;
        this.messageConstructor = null;
        str = uRLName != null ? uRLName.getProtocol() : str;
        this.name = str;
        this.defaultPort = i;
        this.isSSL = z;
        String property = session.getProperty("mail." + str + ".rsetbeforequit");
        if (property != null && property.equalsIgnoreCase("true")) {
            this.rsetBeforeQuit = true;
        }
        String property2 = session.getProperty("mail." + str + ".disabletop");
        if (property2 != null && property2.equalsIgnoreCase("true")) {
            this.disableTop = true;
        }
        String property3 = session.getProperty("mail." + str + ".forgettopheaders");
        if (property3 != null && property3.equalsIgnoreCase("true")) {
            this.forgetTopHeaders = true;
        }
        String property4 = session.getProperty("mail." + str + ".message.class");
        if (property4 != null) {
            if (session.getDebug()) {
                session.getDebugOut().println("DEBUG: POP3 message class: " + property4);
            }
            try {
                try {
                    cls = getClass().getClassLoader().loadClass(property4);
                } catch (ClassNotFoundException e) {
                    cls = Class.forName(property4);
                }
                this.messageConstructor = cls.getConstructor(Folder.class, Integer.TYPE);
            } catch (Exception e2) {
                if (session.getDebug()) {
                    session.getDebugOut().println("DEBUG: failed to load POP3 message class: " + e2);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0033 A[Catch:{ EOFException -> 0x0046, IOException -> 0x0054 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean protocolConnect(java.lang.String r5, int r6, java.lang.String r7, java.lang.String r8) throws javax.mail.MessagingException {
        /*
            r4 = this;
            r3 = -1
            monitor-enter(r4)
            if (r5 == 0) goto L_0x0008
            if (r8 == 0) goto L_0x0008
            if (r7 != 0) goto L_0x000b
        L_0x0008:
            r0 = 0
        L_0x0009:
            monitor-exit(r4)
            return r0
        L_0x000b:
            if (r6 != r3) goto L_0x005d
            javax.mail.Session r0 = r4.session     // Catch:{ all -> 0x0051 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0051 }
            java.lang.String r2 = "mail."
            r1.<init>(r2)     // Catch:{ all -> 0x0051 }
            java.lang.String r2 = r4.name     // Catch:{ all -> 0x0051 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0051 }
            java.lang.String r2 = ".port"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0051 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0051 }
            java.lang.String r0 = r0.getProperty(r1)     // Catch:{ all -> 0x0051 }
            if (r0 == 0) goto L_0x005d
            int r6 = java.lang.Integer.parseInt(r0)     // Catch:{ all -> 0x0051 }
            r0 = r6
        L_0x0031:
            if (r0 != r3) goto L_0x0035
            int r0 = r4.defaultPort     // Catch:{ all -> 0x0051 }
        L_0x0035:
            r4.host = r5     // Catch:{ all -> 0x0051 }
            r4.portNum = r0     // Catch:{ all -> 0x0051 }
            r4.user = r7     // Catch:{ all -> 0x0051 }
            r4.passwd = r8     // Catch:{ all -> 0x0051 }
            r0 = 0
            com.sun.mail.pop3.Protocol r0 = r4.getPort(r0)     // Catch:{ EOFException -> 0x0046, IOException -> 0x0054 }
            r4.port = r0     // Catch:{ EOFException -> 0x0046, IOException -> 0x0054 }
            r0 = 1
            goto L_0x0009
        L_0x0046:
            r0 = move-exception
            javax.mail.AuthenticationFailedException r1 = new javax.mail.AuthenticationFailedException     // Catch:{ all -> 0x0051 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0051 }
            r1.<init>(r0)     // Catch:{ all -> 0x0051 }
            throw r1     // Catch:{ all -> 0x0051 }
        L_0x0051:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x0054:
            r0 = move-exception
            javax.mail.MessagingException r1 = new javax.mail.MessagingException     // Catch:{ all -> 0x0051 }
            java.lang.String r2 = "Connect failed"
            r1.<init>(r2, r0)     // Catch:{ all -> 0x0051 }
            throw r1     // Catch:{ all -> 0x0051 }
        L_0x005d:
            r0 = r6
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sun.mail.pop3.POP3Store.protocolConnect(java.lang.String, int, java.lang.String, java.lang.String):boolean");
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:21:0x0023=Splitter:B:21:0x0023, B:13:0x0016=Splitter:B:13:0x0016} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean isConnected() {
        /*
            r2 = this;
            r0 = 0
            monitor-enter(r2)
            boolean r1 = super.isConnected()     // Catch:{ all -> 0x0028 }
            if (r1 != 0) goto L_0x000a
        L_0x0008:
            monitor-exit(r2)
            return r0
        L_0x000a:
            monitor-enter(r2)     // Catch:{ all -> 0x0028 }
            com.sun.mail.pop3.Protocol r1 = r2.port     // Catch:{ IOException -> 0x001f }
            if (r1 != 0) goto L_0x0019
            r1 = 0
            com.sun.mail.pop3.Protocol r1 = r2.getPort(r1)     // Catch:{ IOException -> 0x001f }
            r2.port = r1     // Catch:{ IOException -> 0x001f }
        L_0x0016:
            monitor-exit(r2)     // Catch:{ all -> 0x0025 }
            r0 = 1
            goto L_0x0008
        L_0x0019:
            com.sun.mail.pop3.Protocol r1 = r2.port     // Catch:{ IOException -> 0x001f }
            r1.noop()     // Catch:{ IOException -> 0x001f }
            goto L_0x0016
        L_0x001f:
            r1 = move-exception
            super.close()     // Catch:{ MessagingException -> 0x002b, all -> 0x002d }
        L_0x0023:
            monitor-exit(r2)     // Catch:{ all -> 0x0025 }
            goto L_0x0008
        L_0x0025:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0025 }
            throw r0     // Catch:{ all -> 0x0028 }
        L_0x0028:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x002b:
            r1 = move-exception
            goto L_0x0023
        L_0x002d:
            r1 = move-exception
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sun.mail.pop3.POP3Store.isConnected():boolean");
    }

    /* access modifiers changed from: package-private */
    public synchronized Protocol getPort(POP3Folder pOP3Folder) throws IOException {
        Protocol protocol;
        if (this.port == null || this.portOwner != null) {
            protocol = new Protocol(this.host, this.portNum, this.session.getDebug(), this.session.getDebugOut(), this.session.getProperties(), "mail." + this.name, this.isSSL);
            String login = protocol.login(this.user, this.passwd);
            if (login != null) {
                try {
                    protocol.quit();
                } catch (IOException e) {
                }
                throw new EOFException(login);
            }
            if (this.port == null && pOP3Folder != null) {
                this.port = protocol;
                this.portOwner = pOP3Folder;
            }
            if (this.portOwner == null) {
                this.portOwner = pOP3Folder;
            }
        } else {
            this.portOwner = pOP3Folder;
            protocol = this.port;
        }
        return protocol;
    }

    /* access modifiers changed from: package-private */
    public synchronized void closePort(POP3Folder pOP3Folder) {
        if (this.portOwner == pOP3Folder) {
            this.port = null;
            this.portOwner = null;
        }
    }

    public synchronized void close() throws MessagingException {
        try {
            if (this.port != null) {
                this.port.quit();
            }
            this.port = null;
            super.close();
        } catch (IOException e) {
            this.port = null;
            super.close();
        } catch (Throwable th) {
            this.port = null;
            super.close();
            throw th;
        }
        return;
    }

    public Folder getDefaultFolder() throws MessagingException {
        checkConnected();
        return new DefaultFolder(this);
    }

    public Folder getFolder(String str) throws MessagingException {
        checkConnected();
        return new POP3Folder(this, str);
    }

    public Folder getFolder(URLName uRLName) throws MessagingException {
        checkConnected();
        return new POP3Folder(this, uRLName.getFile());
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        super.finalize();
        if (this.port != null) {
            close();
        }
    }

    private void checkConnected() throws MessagingException {
        if (!super.isConnected()) {
            throw new MessagingException("Not connected");
        }
    }
}
