package com.sun.mail.pop3;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.FolderClosedException;
import javax.mail.IllegalWriteException;
import javax.mail.MessageRemovedException;
import javax.mail.MessagingException;
import javax.mail.internet.InternetHeaders;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.SharedInputStream;

public class POP3Message extends MimeMessage {
    static final String UNKNOWN = "UNKNOWN";
    private POP3Folder folder;
    private int hdrSize = -1;
    private int msgSize = -1;
    String uid = UNKNOWN;

    public POP3Message(Folder folder2, int i) throws MessagingException {
        super(folder2, i);
        this.folder = (POP3Folder) folder2;
    }

    public void setFlags(Flags flags, boolean z) throws MessagingException {
        super.setFlags(flags, z);
        if (!this.flags.equals((Flags) this.flags.clone())) {
            this.folder.notifyMessageChangedListeners(1, this);
        }
    }

    public int getSize() throws MessagingException {
        int i;
        try {
            synchronized (this) {
                if (this.msgSize >= 0) {
                    i = this.msgSize;
                } else {
                    if (this.msgSize < 0) {
                        if (this.headers == null) {
                            loadHeaders();
                        }
                        if (this.contentStream != null) {
                            this.msgSize = this.contentStream.available();
                        } else {
                            this.msgSize = this.folder.getProtocol().list(this.msgnum) - this.hdrSize;
                        }
                    }
                    i = this.msgSize;
                }
            }
            return i;
        } catch (EOFException e) {
            this.folder.close(false);
            throw new FolderClosedException(this.folder, e.toString());
        } catch (IOException e2) {
            throw new MessagingException("error getting size", e2);
        }
    }

    /* access modifiers changed from: protected */
    public InputStream getContentStream() throws MessagingException {
        int i;
        try {
            synchronized (this) {
                if (this.contentStream == null) {
                    InputStream retr = this.folder.getProtocol().retr(this.msgnum, this.msgSize > 0 ? this.msgSize + this.hdrSize : 0);
                    if (retr == null) {
                        this.expunged = true;
                        throw new MessageRemovedException();
                    }
                    if (this.headers == null || ((POP3Store) this.folder.getStore()).forgetTopHeaders) {
                        this.headers = new InternetHeaders(retr);
                        this.hdrSize = (int) ((SharedInputStream) retr).getPosition();
                    } else {
                        loop0:
                        do {
                            i = 0;
                            while (true) {
                                int read = retr.read();
                                if (read < 0 || read == 10) {
                                    break;
                                } else if (read != 13) {
                                    i++;
                                } else if (retr.available() > 0) {
                                    retr.mark(1);
                                    if (retr.read() != 10) {
                                        retr.reset();
                                    }
                                }
                            }
                            if (retr.available() != 0) {
                                break;
                                break;
                            }
                            break;
                        } while (i != 0);
                        this.hdrSize = (int) ((SharedInputStream) retr).getPosition();
                    }
                    this.contentStream = ((SharedInputStream) retr).newStream((long) this.hdrSize, -1);
                }
            }
            return super.getContentStream();
        } catch (EOFException e) {
            this.folder.close(false);
            throw new FolderClosedException(this.folder, e.toString());
        } catch (IOException e2) {
            throw new MessagingException("error fetching POP3 content", e2);
        }
    }

    public synchronized void invalidate(boolean z) {
        this.content = null;
        this.contentStream = null;
        this.msgSize = -1;
        if (z) {
            this.headers = null;
            this.hdrSize = -1;
        }
    }

    public InputStream top(int i) throws MessagingException {
        InputStream pVar;
        try {
            synchronized (this) {
                pVar = this.folder.getProtocol().top(this.msgnum, i);
            }
            return pVar;
        } catch (EOFException e) {
            this.folder.close(false);
            throw new FolderClosedException(this.folder, e.toString());
        } catch (IOException e2) {
            throw new MessagingException("error getting size", e2);
        }
    }

    public String[] getHeader(String str) throws MessagingException {
        if (this.headers == null) {
            loadHeaders();
        }
        return this.headers.getHeader(str);
    }

    public String getHeader(String str, String str2) throws MessagingException {
        if (this.headers == null) {
            loadHeaders();
        }
        return this.headers.getHeader(str, str2);
    }

    public void setHeader(String str, String str2) throws MessagingException {
        throw new IllegalWriteException("POP3 messages are read-only");
    }

    public void addHeader(String str, String str2) throws MessagingException {
        throw new IllegalWriteException("POP3 messages are read-only");
    }

    public void removeHeader(String str) throws MessagingException {
        throw new IllegalWriteException("POP3 messages are read-only");
    }

    public Enumeration getAllHeaders() throws MessagingException {
        if (this.headers == null) {
            loadHeaders();
        }
        return this.headers.getAllHeaders();
    }

    public Enumeration getMatchingHeaders(String[] strArr) throws MessagingException {
        if (this.headers == null) {
            loadHeaders();
        }
        return this.headers.getMatchingHeaders(strArr);
    }

    public Enumeration getNonMatchingHeaders(String[] strArr) throws MessagingException {
        if (this.headers == null) {
            loadHeaders();
        }
        return this.headers.getNonMatchingHeaders(strArr);
    }

    public void addHeaderLine(String str) throws MessagingException {
        throw new IllegalWriteException("POP3 messages are read-only");
    }

    public Enumeration getAllHeaderLines() throws MessagingException {
        if (this.headers == null) {
            loadHeaders();
        }
        return this.headers.getAllHeaderLines();
    }

    public Enumeration getMatchingHeaderLines(String[] strArr) throws MessagingException {
        if (this.headers == null) {
            loadHeaders();
        }
        return this.headers.getMatchingHeaderLines(strArr);
    }

    public Enumeration getNonMatchingHeaderLines(String[] strArr) throws MessagingException {
        if (this.headers == null) {
            loadHeaders();
        }
        return this.headers.getNonMatchingHeaderLines(strArr);
    }

    public void saveChanges() throws MessagingException {
        throw new IllegalWriteException("POP3 messages are read-only");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void loadHeaders() throws javax.mail.MessagingException {
        /*
            r4 = this;
            r3 = 0
            monitor-enter(r4)     // Catch:{ EOFException -> 0x002f, IOException -> 0x004f }
            javax.mail.internet.InternetHeaders r0 = r4.headers     // Catch:{ all -> 0x002c }
            if (r0 == 0) goto L_0x0008
            monitor-exit(r4)     // Catch:{ all -> 0x002c }
        L_0x0007:
            return
        L_0x0008:
            com.sun.mail.pop3.POP3Folder r0 = r4.folder     // Catch:{ all -> 0x002c }
            javax.mail.Store r0 = r0.getStore()     // Catch:{ all -> 0x002c }
            com.sun.mail.pop3.POP3Store r0 = (com.sun.mail.pop3.POP3Store) r0     // Catch:{ all -> 0x002c }
            boolean r0 = r0.disableTop     // Catch:{ all -> 0x002c }
            if (r0 != 0) goto L_0x0023
            com.sun.mail.pop3.POP3Folder r0 = r4.folder     // Catch:{ all -> 0x002c }
            com.sun.mail.pop3.Protocol r0 = r0.getProtocol()     // Catch:{ all -> 0x002c }
            int r1 = r4.msgnum     // Catch:{ all -> 0x002c }
            r2 = 0
            java.io.InputStream r0 = r0.top(r1, r2)     // Catch:{ all -> 0x002c }
            if (r0 != 0) goto L_0x0041
        L_0x0023:
            java.io.InputStream r0 = r4.getContentStream()     // Catch:{ all -> 0x002c }
            r0.close()     // Catch:{ all -> 0x002c }
        L_0x002a:
            monitor-exit(r4)     // Catch:{ all -> 0x002c }
            goto L_0x0007
        L_0x002c:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x002c }
            throw r0     // Catch:{ EOFException -> 0x002f, IOException -> 0x004f }
        L_0x002f:
            r0 = move-exception
            com.sun.mail.pop3.POP3Folder r1 = r4.folder
            r1.close(r3)
            javax.mail.FolderClosedException r1 = new javax.mail.FolderClosedException
            com.sun.mail.pop3.POP3Folder r2 = r4.folder
            java.lang.String r0 = r0.toString()
            r1.<init>(r2, r0)
            throw r1
        L_0x0041:
            int r1 = r0.available()     // Catch:{ all -> 0x002c }
            r4.hdrSize = r1     // Catch:{ all -> 0x002c }
            javax.mail.internet.InternetHeaders r1 = new javax.mail.internet.InternetHeaders     // Catch:{ all -> 0x002c }
            r1.<init>(r0)     // Catch:{ all -> 0x002c }
            r4.headers = r1     // Catch:{ all -> 0x002c }
            goto L_0x002a
        L_0x004f:
            r0 = move-exception
            javax.mail.MessagingException r1 = new javax.mail.MessagingException
            java.lang.String r2 = "error loading POP3 headers"
            r1.<init>(r2, r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sun.mail.pop3.POP3Message.loadHeaders():void");
    }
}
