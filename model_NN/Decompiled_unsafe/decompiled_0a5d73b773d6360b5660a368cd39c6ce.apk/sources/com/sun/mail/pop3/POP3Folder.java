package com.sun.mail.pop3;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.util.Vector;
import javax.mail.FetchProfile;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.FolderClosedException;
import javax.mail.FolderNotFoundException;
import javax.mail.Message;
import javax.mail.MessageRemovedException;
import javax.mail.MessagingException;
import javax.mail.MethodNotSupportedException;
import javax.mail.UIDFolder;

public class POP3Folder extends Folder {
    private boolean doneUidl = false;
    private boolean exists = false;
    private Vector message_cache;
    private String name;
    private boolean opened = false;
    private Protocol port;
    private int size;
    private int total;

    POP3Folder(POP3Store pOP3Store, String str) {
        super(pOP3Store);
        this.name = str;
        if (str.equalsIgnoreCase("INBOX")) {
            this.exists = true;
        }
    }

    public String getName() {
        return this.name;
    }

    public String getFullName() {
        return this.name;
    }

    public Folder getParent() {
        return new DefaultFolder((POP3Store) this.store);
    }

    public boolean exists() {
        return this.exists;
    }

    public Folder[] list(String str) throws MessagingException {
        throw new MessagingException("not a directory");
    }

    public char getSeparator() {
        return 0;
    }

    public int getType() {
        return 1;
    }

    public boolean create(int i) throws MessagingException {
        return false;
    }

    public boolean hasNewMessages() throws MessagingException {
        return false;
    }

    public Folder getFolder(String str) throws MessagingException {
        throw new MessagingException("not a directory");
    }

    public boolean delete(boolean z) throws MessagingException {
        throw new MethodNotSupportedException("delete");
    }

    public boolean renameTo(Folder folder) throws MessagingException {
        throw new MethodNotSupportedException("renameTo");
    }

    public synchronized void open(int i) throws MessagingException {
        IOException iOException;
        checkClosed();
        if (!this.exists) {
            throw new FolderNotFoundException(this, "folder is not INBOX");
        }
        try {
            this.port = ((POP3Store) this.store).getPort(this);
            Status stat = this.port.stat();
            this.total = stat.total;
            this.size = stat.size;
            this.mode = i;
            this.opened = true;
            this.message_cache = new Vector(this.total);
            this.message_cache.setSize(this.total);
            this.doneUidl = false;
            notifyConnectionListeners(1);
        } catch (IOException e) {
            this.port = null;
            ((POP3Store) this.store).closePort(this);
        } catch (IOException e2) {
            iOException = e2;
            if (this.port != null) {
                this.port.quit();
            }
            this.port = null;
            ((POP3Store) this.store).closePort(this);
        } catch (Throwable th) {
            Throwable th2 = th;
            this.port = null;
            ((POP3Store) this.store).closePort(this);
            throw th2;
        }
        return;
        throw new MessagingException("Open failed", iOException);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:38:0x007b, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x007c, code lost:
        r1 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:?, code lost:
        r3.port = null;
        ((com.sun.mail.pop3.POP3Store) r3.store).closePort(r3);
        r3.message_cache = null;
        r3.opened = false;
        notifyConnectionListeners(3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0091, code lost:
        throw r1;
     */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x007b A[ExcHandler: all (r0v1 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:4:0x0005] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void close(boolean r4) throws javax.mail.MessagingException {
        /*
            r3 = this;
            r1 = 0
            monitor-enter(r3)
            r3.checkOpen()     // Catch:{ all -> 0x0078 }
            javax.mail.Store r0 = r3.store     // Catch:{ IOException -> 0x0062, all -> 0x007b }
            com.sun.mail.pop3.POP3Store r0 = (com.sun.mail.pop3.POP3Store) r0     // Catch:{ IOException -> 0x0062, all -> 0x007b }
            boolean r0 = r0.rsetBeforeQuit     // Catch:{ IOException -> 0x0062, all -> 0x007b }
            if (r0 == 0) goto L_0x0012
            com.sun.mail.pop3.Protocol r0 = r3.port     // Catch:{ IOException -> 0x0062, all -> 0x007b }
            r0.rset()     // Catch:{ IOException -> 0x0062, all -> 0x007b }
        L_0x0012:
            if (r4 == 0) goto L_0x0021
            int r0 = r3.mode     // Catch:{ IOException -> 0x0062, all -> 0x007b }
            r2 = 2
            if (r0 != r2) goto L_0x0021
        L_0x0019:
            java.util.Vector r0 = r3.message_cache     // Catch:{ IOException -> 0x0062, all -> 0x007b }
            int r0 = r0.size()     // Catch:{ IOException -> 0x0062, all -> 0x007b }
            if (r1 < r0) goto L_0x003c
        L_0x0021:
            com.sun.mail.pop3.Protocol r0 = r3.port     // Catch:{ IOException -> 0x0062, all -> 0x007b }
            r0.quit()     // Catch:{ IOException -> 0x0062, all -> 0x007b }
            r0 = 0
            r3.port = r0     // Catch:{ all -> 0x0078 }
            javax.mail.Store r0 = r3.store     // Catch:{ all -> 0x0078 }
            com.sun.mail.pop3.POP3Store r0 = (com.sun.mail.pop3.POP3Store) r0     // Catch:{ all -> 0x0078 }
            r0.closePort(r3)     // Catch:{ all -> 0x0078 }
            r0 = 0
            r3.message_cache = r0     // Catch:{ all -> 0x0078 }
            r0 = 0
            r3.opened = r0     // Catch:{ all -> 0x0078 }
            r0 = 3
            r3.notifyConnectionListeners(r0)     // Catch:{ all -> 0x0078 }
        L_0x003a:
            monitor-exit(r3)
            return
        L_0x003c:
            java.util.Vector r0 = r3.message_cache     // Catch:{ IOException -> 0x0062, all -> 0x007b }
            java.lang.Object r0 = r0.elementAt(r1)     // Catch:{ IOException -> 0x0062, all -> 0x007b }
            com.sun.mail.pop3.POP3Message r0 = (com.sun.mail.pop3.POP3Message) r0     // Catch:{ IOException -> 0x0062, all -> 0x007b }
            if (r0 == 0) goto L_0x0055
            javax.mail.Flags$Flag r2 = javax.mail.Flags.Flag.DELETED     // Catch:{ IOException -> 0x0062, all -> 0x007b }
            boolean r0 = r0.isSet(r2)     // Catch:{ IOException -> 0x0062, all -> 0x007b }
            if (r0 == 0) goto L_0x0055
            com.sun.mail.pop3.Protocol r0 = r3.port     // Catch:{ IOException -> 0x0059, all -> 0x007b }
            int r2 = r1 + 1
            r0.dele(r2)     // Catch:{ IOException -> 0x0059, all -> 0x007b }
        L_0x0055:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x0019
        L_0x0059:
            r0 = move-exception
            javax.mail.MessagingException r1 = new javax.mail.MessagingException     // Catch:{ IOException -> 0x0062, all -> 0x007b }
            java.lang.String r2 = "Exception deleting messages during close"
            r1.<init>(r2, r0)     // Catch:{ IOException -> 0x0062, all -> 0x007b }
            throw r1     // Catch:{ IOException -> 0x0062, all -> 0x007b }
        L_0x0062:
            r0 = move-exception
            r0 = 0
            r3.port = r0     // Catch:{ all -> 0x0078 }
            javax.mail.Store r0 = r3.store     // Catch:{ all -> 0x0078 }
            com.sun.mail.pop3.POP3Store r0 = (com.sun.mail.pop3.POP3Store) r0     // Catch:{ all -> 0x0078 }
            r0.closePort(r3)     // Catch:{ all -> 0x0078 }
            r0 = 0
            r3.message_cache = r0     // Catch:{ all -> 0x0078 }
            r0 = 0
            r3.opened = r0     // Catch:{ all -> 0x0078 }
            r0 = 3
            r3.notifyConnectionListeners(r0)     // Catch:{ all -> 0x0078 }
            goto L_0x003a
        L_0x0078:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        L_0x007b:
            r0 = move-exception
            r1 = r0
            r0 = 0
            r3.port = r0     // Catch:{ all -> 0x0078 }
            javax.mail.Store r0 = r3.store     // Catch:{ all -> 0x0078 }
            com.sun.mail.pop3.POP3Store r0 = (com.sun.mail.pop3.POP3Store) r0     // Catch:{ all -> 0x0078 }
            r0.closePort(r3)     // Catch:{ all -> 0x0078 }
            r0 = 0
            r3.message_cache = r0     // Catch:{ all -> 0x0078 }
            r0 = 0
            r3.opened = r0     // Catch:{ all -> 0x0078 }
            r0 = 3
            r3.notifyConnectionListeners(r0)     // Catch:{ all -> 0x0078 }
            throw r1     // Catch:{ all -> 0x0078 }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sun.mail.pop3.POP3Folder.close(boolean):void");
    }

    public boolean isOpen() {
        if (!this.opened) {
            return false;
        }
        if (this.store.isConnected()) {
            return true;
        }
        try {
            close(false);
            return false;
        } catch (MessagingException e) {
            return false;
        }
    }

    public Flags getPermanentFlags() {
        return new Flags();
    }

    public synchronized int getMessageCount() throws MessagingException {
        int i;
        if (!this.opened) {
            i = -1;
        } else {
            checkReadable();
            i = this.total;
        }
        return i;
    }

    public synchronized Message getMessage(int i) throws MessagingException {
        POP3Message pOP3Message;
        checkOpen();
        pOP3Message = (POP3Message) this.message_cache.elementAt(i - 1);
        if (pOP3Message == null) {
            pOP3Message = createMessage(this, i);
            this.message_cache.setElementAt(pOP3Message, i - 1);
        }
        return pOP3Message;
    }

    /* access modifiers changed from: protected */
    public POP3Message createMessage(Folder folder, int i) throws MessagingException {
        POP3Message pOP3Message;
        Constructor constructor = ((POP3Store) this.store).messageConstructor;
        if (constructor != null) {
            try {
                pOP3Message = (POP3Message) constructor.newInstance(this, new Integer(i));
            } catch (Exception e) {
                pOP3Message = null;
            }
        } else {
            pOP3Message = null;
        }
        if (pOP3Message == null) {
            return new POP3Message(this, i);
        }
        return pOP3Message;
    }

    public void appendMessages(Message[] messageArr) throws MessagingException {
        throw new MethodNotSupportedException("Append not supported");
    }

    public Message[] expunge() throws MessagingException {
        throw new MethodNotSupportedException("Expunge not supported");
    }

    public synchronized void fetch(Message[] messageArr, FetchProfile fetchProfile) throws MessagingException {
        synchronized (this) {
            checkReadable();
            if (!this.doneUidl && fetchProfile.contains(UIDFolder.FetchProfileItem.UID)) {
                String[] strArr = new String[this.message_cache.size()];
                try {
                    if (this.port.uidl(strArr)) {
                        for (int i = 0; i < strArr.length; i++) {
                            if (strArr[i] != null) {
                                ((POP3Message) getMessage(i + 1)).uid = strArr[i];
                            }
                        }
                        this.doneUidl = true;
                    }
                } catch (EOFException e) {
                    close(false);
                    throw new FolderClosedException(this, e.toString());
                } catch (IOException e2) {
                    throw new MessagingException("error getting UIDL", e2);
                }
            }
            if (fetchProfile.contains(FetchProfile.Item.ENVELOPE)) {
                for (int i2 = 0; i2 < messageArr.length; i2++) {
                    try {
                        POP3Message pOP3Message = (POP3Message) messageArr[i2];
                        pOP3Message.getHeader("");
                        pOP3Message.getSize();
                    } catch (MessageRemovedException e3) {
                    }
                }
            }
        }
    }

    public synchronized String getUID(Message message) throws MessagingException {
        POP3Message pOP3Message;
        checkOpen();
        pOP3Message = (POP3Message) message;
        try {
            if (pOP3Message.uid == "UNKNOWN") {
                pOP3Message.uid = this.port.uidl(pOP3Message.getMessageNumber());
            }
        } catch (EOFException e) {
            close(false);
            throw new FolderClosedException(this, e.toString());
        } catch (IOException e2) {
            throw new MessagingException("error getting UIDL", e2);
        }
        return pOP3Message.uid;
    }

    public synchronized int getSize() throws MessagingException {
        checkOpen();
        return this.size;
    }

    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x005b A[SYNTHETIC, Splitter:B:38:0x005b] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0060 A[SYNTHETIC, Splitter:B:41:0x0060] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized int[] getSizes() throws javax.mail.MessagingException {
        /*
            r7 = this;
            r1 = 0
            monitor-enter(r7)
            r7.checkOpen()     // Catch:{ all -> 0x0064 }
            int r0 = r7.total     // Catch:{ all -> 0x0064 }
            int[] r3 = new int[r0]     // Catch:{ all -> 0x0064 }
            com.sun.mail.pop3.Protocol r0 = r7.port     // Catch:{ IOException -> 0x0048, all -> 0x0057 }
            java.io.InputStream r2 = r0.list()     // Catch:{ IOException -> 0x0048, all -> 0x0057 }
            com.sun.mail.util.LineInputStream r0 = new com.sun.mail.util.LineInputStream     // Catch:{ IOException -> 0x0078, all -> 0x0071 }
            r0.<init>(r2)     // Catch:{ IOException -> 0x0078, all -> 0x0071 }
        L_0x0014:
            java.lang.String r1 = r0.readLine()     // Catch:{ IOException -> 0x007c, all -> 0x0073 }
            if (r1 != 0) goto L_0x0026
            if (r0 == 0) goto L_0x001f
            r0.close()     // Catch:{ IOException -> 0x006d }
        L_0x001f:
            if (r2 == 0) goto L_0x0024
            r2.close()     // Catch:{ IOException -> 0x006f }
        L_0x0024:
            monitor-exit(r7)
            return r3
        L_0x0026:
            java.util.StringTokenizer r4 = new java.util.StringTokenizer     // Catch:{ Exception -> 0x0046 }
            r4.<init>(r1)     // Catch:{ Exception -> 0x0046 }
            java.lang.String r1 = r4.nextToken()     // Catch:{ Exception -> 0x0046 }
            int r1 = java.lang.Integer.parseInt(r1)     // Catch:{ Exception -> 0x0046 }
            java.lang.String r4 = r4.nextToken()     // Catch:{ Exception -> 0x0046 }
            int r4 = java.lang.Integer.parseInt(r4)     // Catch:{ Exception -> 0x0046 }
            if (r1 <= 0) goto L_0x0014
            int r5 = r7.total     // Catch:{ Exception -> 0x0046 }
            if (r1 > r5) goto L_0x0014
            int r1 = r1 + -1
            r3[r1] = r4     // Catch:{ Exception -> 0x0046 }
            goto L_0x0014
        L_0x0046:
            r1 = move-exception
            goto L_0x0014
        L_0x0048:
            r0 = move-exception
            r0 = r1
        L_0x004a:
            if (r0 == 0) goto L_0x004f
            r0.close()     // Catch:{ IOException -> 0x0067 }
        L_0x004f:
            if (r1 == 0) goto L_0x0024
            r1.close()     // Catch:{ IOException -> 0x0055 }
            goto L_0x0024
        L_0x0055:
            r0 = move-exception
            goto L_0x0024
        L_0x0057:
            r0 = move-exception
            r2 = r1
        L_0x0059:
            if (r1 == 0) goto L_0x005e
            r1.close()     // Catch:{ IOException -> 0x0069 }
        L_0x005e:
            if (r2 == 0) goto L_0x0063
            r2.close()     // Catch:{ IOException -> 0x006b }
        L_0x0063:
            throw r0     // Catch:{ all -> 0x0064 }
        L_0x0064:
            r0 = move-exception
            monitor-exit(r7)
            throw r0
        L_0x0067:
            r0 = move-exception
            goto L_0x004f
        L_0x0069:
            r1 = move-exception
            goto L_0x005e
        L_0x006b:
            r1 = move-exception
            goto L_0x0063
        L_0x006d:
            r0 = move-exception
            goto L_0x001f
        L_0x006f:
            r0 = move-exception
            goto L_0x0024
        L_0x0071:
            r0 = move-exception
            goto L_0x0059
        L_0x0073:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0059
        L_0x0078:
            r0 = move-exception
            r0 = r1
            r1 = r2
            goto L_0x004a
        L_0x007c:
            r1 = move-exception
            r1 = r2
            goto L_0x004a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sun.mail.pop3.POP3Folder.getSizes():int[]");
    }

    public synchronized InputStream listCommand() throws MessagingException, IOException {
        checkOpen();
        return this.port.list();
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        super.finalize();
        close(false);
    }

    /* access modifiers changed from: package-private */
    public void checkOpen() throws IllegalStateException {
        if (!this.opened) {
            throw new IllegalStateException("Folder is not Open");
        }
    }

    /* access modifiers changed from: package-private */
    public void checkClosed() throws IllegalStateException {
        if (this.opened) {
            throw new IllegalStateException("Folder is Open");
        }
    }

    /* access modifiers changed from: package-private */
    public void checkReadable() throws IllegalStateException {
        if (!this.opened || !(this.mode == 1 || this.mode == 2)) {
            throw new IllegalStateException("Folder is not Readable");
        }
    }

    /* access modifiers changed from: package-private */
    public void checkWritable() throws IllegalStateException {
        if (!this.opened || this.mode != 2) {
            throw new IllegalStateException("Folder is not Writable");
        }
    }

    /* access modifiers changed from: package-private */
    public Protocol getProtocol() throws MessagingException {
        checkOpen();
        return this.port;
    }

    /* access modifiers changed from: protected */
    public void notifyMessageChangedListeners(int i, Message message) {
        super.notifyMessageChangedListeners(i, message);
    }
}
