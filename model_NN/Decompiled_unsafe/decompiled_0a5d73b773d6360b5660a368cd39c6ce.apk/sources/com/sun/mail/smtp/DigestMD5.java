package com.sun.mail.smtp;

import com.sun.mail.util.ASCIIUtility;
import com.sun.mail.util.BASE64DecoderStream;
import com.sun.mail.util.BASE64EncoderStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.StreamTokenizer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Hashtable;
import java.util.StringTokenizer;
import javax.mail.internet.HeaderTokenizer;

public class DigestMD5 {
    private static char[] digits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    private String clientResponse;
    private PrintStream debugout;
    private MessageDigest md5;
    private String uri;

    public DigestMD5(PrintStream printStream) {
        this.debugout = printStream;
        if (printStream != null) {
            printStream.println("DEBUG DIGEST-MD5: Loaded");
        }
    }

    public byte[] authClient(String str, String str2, String str3, String str4, String str5) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        BASE64EncoderStream bASE64EncoderStream = new BASE64EncoderStream(byteArrayOutputStream, Integer.MAX_VALUE);
        try {
            SecureRandom secureRandom = new SecureRandom();
            this.md5 = MessageDigest.getInstance("MD5");
            StringBuffer stringBuffer = new StringBuffer();
            this.uri = "smtp/" + str;
            byte[] bArr = new byte[32];
            if (this.debugout != null) {
                this.debugout.println("DEBUG DIGEST-MD5: Begin authentication ...");
            }
            Hashtable hashtable = tokenize(str5);
            if (str4 == null) {
                String str6 = (String) hashtable.get("realm");
                if (str6 != null) {
                    str = new StringTokenizer(str6, ",").nextToken();
                }
            } else {
                str = str4;
            }
            String str7 = (String) hashtable.get("nonce");
            secureRandom.nextBytes(bArr);
            bASE64EncoderStream.write(bArr);
            bASE64EncoderStream.flush();
            String byteArrayOutputStream2 = byteArrayOutputStream.toString();
            byteArrayOutputStream.reset();
            this.md5.update(this.md5.digest(ASCIIUtility.getBytes(String.valueOf(str2) + ":" + str + ":" + str3)));
            this.md5.update(ASCIIUtility.getBytes(":" + str7 + ":" + byteArrayOutputStream2));
            this.clientResponse = String.valueOf(toHex(this.md5.digest())) + ":" + str7 + ":" + "00000001" + ":" + byteArrayOutputStream2 + ":" + "auth" + ":";
            this.md5.update(ASCIIUtility.getBytes("AUTHENTICATE:" + this.uri));
            this.md5.update(ASCIIUtility.getBytes(String.valueOf(this.clientResponse) + toHex(this.md5.digest())));
            stringBuffer.append("username=\"" + str2 + "\"");
            stringBuffer.append(",realm=\"" + str + "\"");
            stringBuffer.append(",qop=" + "auth");
            stringBuffer.append(",nc=" + "00000001");
            stringBuffer.append(",nonce=\"" + str7 + "\"");
            stringBuffer.append(",cnonce=\"" + byteArrayOutputStream2 + "\"");
            stringBuffer.append(",digest-uri=\"" + this.uri + "\"");
            stringBuffer.append(",response=" + toHex(this.md5.digest()));
            if (this.debugout != null) {
                this.debugout.println("DEBUG DIGEST-MD5: Response => " + stringBuffer.toString());
            }
            bASE64EncoderStream.write(ASCIIUtility.getBytes(stringBuffer.toString()));
            bASE64EncoderStream.flush();
            return byteArrayOutputStream.toByteArray();
        } catch (NoSuchAlgorithmException e) {
            if (this.debugout != null) {
                this.debugout.println("DEBUG DIGEST-MD5: " + e);
            }
            throw new IOException(e.toString());
        }
    }

    public boolean authServer(String str) throws IOException {
        Hashtable hashtable = tokenize(str);
        this.md5.update(ASCIIUtility.getBytes(":" + this.uri));
        this.md5.update(ASCIIUtility.getBytes(String.valueOf(this.clientResponse) + toHex(this.md5.digest())));
        String hex = toHex(this.md5.digest());
        if (hex.equals((String) hashtable.get("rspauth"))) {
            return true;
        }
        if (this.debugout != null) {
            this.debugout.println("DEBUG DIGEST-MD5: Expected => rspauth=" + hex);
        }
        return false;
    }

    private Hashtable tokenize(String str) throws IOException {
        Hashtable hashtable = new Hashtable();
        byte[] bytes = str.getBytes();
        StreamTokenizer streamTokenizer = new StreamTokenizer(new InputStreamReader(new BASE64DecoderStream(new ByteArrayInputStream(bytes, 4, bytes.length - 4))));
        streamTokenizer.ordinaryChars(48, 57);
        streamTokenizer.wordChars(48, 57);
        String str2 = null;
        while (true) {
            int nextToken = streamTokenizer.nextToken();
            if (nextToken == -1) {
                return hashtable;
            }
            switch (nextToken) {
                case HeaderTokenizer.Token.COMMENT /*-3*/:
                    if (str2 == null) {
                        str2 = streamTokenizer.sval;
                        continue;
                    }
                    break;
                case 34:
                    break;
            }
            if (this.debugout != null) {
                this.debugout.println("DEBUG DIGEST-MD5: Received => " + str2 + "='" + streamTokenizer.sval + "'");
            }
            if (hashtable.containsKey(str2)) {
                hashtable.put(str2, hashtable.get(str2) + "," + streamTokenizer.sval);
            } else {
                hashtable.put(str2, streamTokenizer.sval);
            }
            str2 = null;
        }
    }

    private static String toHex(byte[] bArr) {
        int i = 0;
        char[] cArr = new char[(bArr.length * 2)];
        for (byte b : bArr) {
            byte b2 = b & 255;
            int i2 = i + 1;
            cArr[i] = digits[b2 >> 4];
            i = i2 + 1;
            cArr[i2] = digits[b2 & 15];
        }
        return new String(cArr);
    }
}
