package com.sun.mail.smtp;

import com.sun.mail.util.CRLFOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class SMTPOutputStream extends CRLFOutputStream {
    public SMTPOutputStream(OutputStream outputStream) {
        super(outputStream);
    }

    public void write(int i) throws IOException {
        if ((this.lastb == 10 || this.lastb == 13 || this.lastb == -1) && i == 46) {
            this.out.write(46);
        }
        super.write(i);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v8, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v9, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void write(byte[] r8, int r9, int r10) throws java.io.IOException {
        /*
            r7 = this;
            r6 = 46
            r1 = 10
            int r0 = r7.lastb
            r2 = -1
            if (r0 != r2) goto L_0x001b
            r0 = r1
        L_0x000a:
            int r4 = r10 + r9
            r2 = r9
            r3 = r0
            r0 = r9
        L_0x000f:
            if (r2 < r4) goto L_0x001e
            int r1 = r4 - r0
            if (r1 <= 0) goto L_0x001a
            int r1 = r4 - r0
            super.write(r8, r0, r1)
        L_0x001a:
            return
        L_0x001b:
            int r0 = r7.lastb
            goto L_0x000a
        L_0x001e:
            if (r3 == r1) goto L_0x0024
            r5 = 13
            if (r3 != r5) goto L_0x0033
        L_0x0024:
            byte r3 = r8[r2]
            if (r3 != r6) goto L_0x0033
            int r3 = r2 - r0
            super.write(r8, r0, r3)
            java.io.OutputStream r0 = r7.out
            r0.write(r6)
            r0 = r2
        L_0x0033:
            byte r3 = r8[r2]
            int r2 = r2 + 1
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sun.mail.smtp.SMTPOutputStream.write(byte[], int, int):void");
    }

    public void flush() {
    }

    public void ensureAtBOL() throws IOException {
        if (!this.atBOL) {
            super.writeln();
        }
    }
}
