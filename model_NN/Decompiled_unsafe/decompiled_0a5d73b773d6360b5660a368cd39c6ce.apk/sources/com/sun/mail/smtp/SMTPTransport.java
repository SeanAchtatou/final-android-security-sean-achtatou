package com.sun.mail.smtp;

import com.sun.mail.util.ASCIIUtility;
import com.sun.mail.util.LineInputStream;
import com.sun.mail.util.SocketFetcher;
import com.sun.mail.util.TraceInputStream;
import com.sun.mail.util.TraceOutputStream;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.StringReader;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.URLName;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimePart;
import javax.mail.internet.ParseException;

public class SMTPTransport extends Transport {
    static final /* synthetic */ boolean $assertionsDisabled;
    private static final byte[] CRLF = {13, 10};
    private static final String UNKNOWN = "UNKNOWN";
    private static char[] hexchar = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    private static final String[] ignoreList = {"Bcc", "Content-Length"};
    private Address[] addresses;
    private SMTPOutputStream dataStream;
    private int defaultPort;
    private MessagingException exception;
    private Hashtable extMap;
    private Address[] invalidAddr;
    private boolean isSSL;
    private int lastReturnCode;
    private String lastServerResponse;
    private LineInputStream lineInputStream;
    private String localHostName;
    private DigestMD5 md5support;
    private MimeMessage message;
    private String name;
    private PrintStream out;
    private boolean quitWait;
    private boolean reportSuccess;
    private String saslRealm;
    private boolean sendPartiallyFailed;
    private BufferedInputStream serverInput;
    private OutputStream serverOutput;
    private Socket serverSocket;
    private boolean useRset;
    private boolean useStartTLS;
    private Address[] validSentAddr;
    private Address[] validUnsentAddr;

    static {
        boolean z;
        if (!SMTPTransport.class.desiredAssertionStatus()) {
            z = true;
        } else {
            z = false;
        }
        $assertionsDisabled = z;
    }

    public SMTPTransport(Session session, URLName uRLName) {
        this(session, uRLName, "smtp", 25, $assertionsDisabled);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    protected SMTPTransport(Session session, URLName uRLName, String str, int i, boolean z) {
        super(session, uRLName);
        boolean z2;
        boolean z3;
        boolean z4 = true;
        this.name = "smtp";
        this.defaultPort = 25;
        this.isSSL = $assertionsDisabled;
        this.sendPartiallyFailed = $assertionsDisabled;
        this.quitWait = $assertionsDisabled;
        this.saslRealm = UNKNOWN;
        str = uRLName != null ? uRLName.getProtocol() : str;
        this.name = str;
        this.defaultPort = i;
        this.isSSL = z;
        this.out = session.getDebugOut();
        String property = session.getProperty("mail." + str + ".quitwait");
        this.quitWait = property == null || property.equalsIgnoreCase("true");
        String property2 = session.getProperty("mail." + str + ".reportsuccess");
        if (property2 == null || !property2.equalsIgnoreCase("true")) {
            z2 = false;
        } else {
            z2 = true;
        }
        this.reportSuccess = z2;
        String property3 = session.getProperty("mail." + str + ".starttls.enable");
        if (property3 == null || !property3.equalsIgnoreCase("true")) {
            z3 = false;
        } else {
            z3 = true;
        }
        this.useStartTLS = z3;
        String property4 = session.getProperty("mail." + str + ".userset");
        this.useRset = (property4 == null || !property4.equalsIgnoreCase("true")) ? false : z4;
    }

    public synchronized String getLocalHost() {
        try {
            if (this.localHostName == null || this.localHostName.length() <= 0) {
                this.localHostName = this.session.getProperty("mail." + this.name + ".localhost");
            }
            if (this.localHostName == null || this.localHostName.length() <= 0) {
                this.localHostName = this.session.getProperty("mail." + this.name + ".localaddress");
            }
            if (this.localHostName == null || this.localHostName.length() <= 0) {
                InetAddress localHost = InetAddress.getLocalHost();
                this.localHostName = localHost.getHostName();
                if (this.localHostName == null) {
                    this.localHostName = "[" + localHost.getHostAddress() + "]";
                }
            }
        } catch (UnknownHostException e) {
        }
        return this.localHostName;
    }

    public synchronized void setLocalHost(String str) {
        this.localHostName = str;
    }

    public synchronized void connect(Socket socket) throws MessagingException {
        this.serverSocket = socket;
        super.connect();
    }

    public synchronized String getSASLRealm() {
        if (this.saslRealm == UNKNOWN) {
            this.saslRealm = this.session.getProperty("mail." + this.name + ".sasl.realm");
            if (this.saslRealm == null) {
                this.saslRealm = this.session.getProperty("mail." + this.name + ".saslrealm");
            }
        }
        return this.saslRealm;
    }

    public synchronized void setSASLRealm(String str) {
        this.saslRealm = str;
    }

    public synchronized boolean getReportSuccess() {
        return this.reportSuccess;
    }

    public synchronized void setReportSuccess(boolean z) {
        this.reportSuccess = z;
    }

    public synchronized boolean getStartTLS() {
        return this.useStartTLS;
    }

    public synchronized void setStartTLS(boolean z) {
        this.useStartTLS = z;
    }

    public synchronized boolean getUseRset() {
        return this.useRset;
    }

    public synchronized void setUseRset(boolean z) {
        this.useRset = z;
    }

    public synchronized String getLastServerResponse() {
        return this.lastServerResponse;
    }

    public synchronized int getLastReturnCode() {
        return this.lastReturnCode;
    }

    private synchronized DigestMD5 getMD5() {
        if (this.md5support == null) {
            this.md5support = new DigestMD5(this.debug ? this.out : null);
        }
        return this.md5support;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x027a  */
    /* JADX WARNING: Removed duplicated region for block: B:135:0x0280  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean protocolConnect(java.lang.String r9, int r10, java.lang.String r11, java.lang.String r12) throws javax.mail.MessagingException {
        /*
            r8 = this;
            javax.mail.Session r0 = r8.session
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "mail."
            r1.<init>(r2)
            java.lang.String r2 = r8.name
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = ".ehlo"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            java.lang.String r0 = r0.getProperty(r1)
            if (r0 == 0) goto L_0x007b
            java.lang.String r1 = "false"
            boolean r0 = r0.equalsIgnoreCase(r1)
            if (r0 == 0) goto L_0x007b
            r0 = 0
        L_0x0028:
            javax.mail.Session r1 = r8.session
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "mail."
            r2.<init>(r3)
            java.lang.String r3 = r8.name
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = ".auth"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            java.lang.String r1 = r1.getProperty(r2)
            if (r1 == 0) goto L_0x007d
            java.lang.String r2 = "true"
            boolean r1 = r1.equalsIgnoreCase(r2)
            if (r1 == 0) goto L_0x007d
            r1 = 1
            r2 = r1
        L_0x0051:
            boolean r1 = r8.debug
            if (r1 == 0) goto L_0x0073
            java.io.PrintStream r1 = r8.out
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "DEBUG SMTP: useEhlo "
            r3.<init>(r4)
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r4 = ", useAuth "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r2)
            java.lang.String r3 = r3.toString()
            r1.println(r3)
        L_0x0073:
            if (r2 == 0) goto L_0x0080
            if (r11 == 0) goto L_0x0079
            if (r12 != 0) goto L_0x0080
        L_0x0079:
            r0 = 0
        L_0x007a:
            return r0
        L_0x007b:
            r0 = 1
            goto L_0x0028
        L_0x007d:
            r1 = 0
            r2 = r1
            goto L_0x0051
        L_0x0080:
            r1 = -1
            if (r10 != r1) goto L_0x00a6
            javax.mail.Session r1 = r8.session
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "mail."
            r3.<init>(r4)
            java.lang.String r4 = r8.name
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = ".port"
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            java.lang.String r1 = r1.getProperty(r3)
            if (r1 == 0) goto L_0x0185
            int r10 = java.lang.Integer.parseInt(r1)
        L_0x00a6:
            if (r9 == 0) goto L_0x00ae
            int r1 = r9.length()
            if (r1 != 0) goto L_0x0290
        L_0x00ae:
            java.lang.String r1 = "localhost"
        L_0x00b0:
            r3 = 0
            java.net.Socket r4 = r8.serverSocket
            if (r4 == 0) goto L_0x0189
            r8.openServer()
        L_0x00b8:
            if (r0 == 0) goto L_0x028d
            java.lang.String r0 = r8.getLocalHost()
            boolean r0 = r8.ehlo(r0)
        L_0x00c2:
            if (r0 != 0) goto L_0x00cb
            java.lang.String r0 = r8.getLocalHost()
            r8.helo(r0)
        L_0x00cb:
            boolean r0 = r8.useStartTLS
            if (r0 == 0) goto L_0x00e1
            java.lang.String r0 = "STARTTLS"
            boolean r0 = r8.supportsExtension(r0)
            if (r0 == 0) goto L_0x00e1
            r8.startTLS()
            java.lang.String r0 = r8.getLocalHost()
            r8.ehlo(r0)
        L_0x00e1:
            if (r2 != 0) goto L_0x00e7
            if (r11 == 0) goto L_0x0281
            if (r12 == 0) goto L_0x0281
        L_0x00e7:
            java.lang.String r0 = "AUTH"
            boolean r0 = r8.supportsExtension(r0)
            if (r0 != 0) goto L_0x00f7
            java.lang.String r0 = "AUTH=LOGIN"
            boolean r0 = r8.supportsExtension(r0)
            if (r0 == 0) goto L_0x0281
        L_0x00f7:
            boolean r0 = r8.debug
            if (r0 == 0) goto L_0x0119
            java.io.PrintStream r0 = r8.out
            java.lang.String r2 = "DEBUG SMTP: Attempt to authenticate"
            r0.println(r2)
            java.lang.String r0 = "LOGIN"
            boolean r0 = r8.supportsAuthentication(r0)
            if (r0 != 0) goto L_0x0119
            java.lang.String r0 = "AUTH=LOGIN"
            boolean r0 = r8.supportsExtension(r0)
            if (r0 == 0) goto L_0x0119
            java.io.PrintStream r0 = r8.out
            java.lang.String r2 = "DEBUG SMTP: use AUTH=LOGIN hack"
            r0.println(r2)
        L_0x0119:
            java.lang.String r0 = "LOGIN"
            boolean r0 = r8.supportsAuthentication(r0)
            if (r0 != 0) goto L_0x0129
            java.lang.String r0 = "AUTH=LOGIN"
            boolean r0 = r8.supportsExtension(r0)
            if (r0 == 0) goto L_0x01a8
        L_0x0129:
            java.lang.String r0 = "AUTH LOGIN"
            int r0 = r8.simpleCommand(r0)
            r1 = 530(0x212, float:7.43E-43)
            if (r0 != r1) goto L_0x013c
            r8.startTLS()
            java.lang.String r0 = "AUTH LOGIN"
            int r0 = r8.simpleCommand(r0)
        L_0x013c:
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x018e, all -> 0x0199 }
            r1.<init>()     // Catch:{ IOException -> 0x018e, all -> 0x0199 }
            com.sun.mail.util.BASE64EncoderStream r2 = new com.sun.mail.util.BASE64EncoderStream     // Catch:{ IOException -> 0x018e, all -> 0x0199 }
            r3 = 2147483647(0x7fffffff, float:NaN)
            r2.<init>(r1, r3)     // Catch:{ IOException -> 0x018e, all -> 0x0199 }
            r3 = 334(0x14e, float:4.68E-43)
            if (r0 != r3) goto L_0x0162
            byte[] r3 = com.sun.mail.util.ASCIIUtility.getBytes(r11)     // Catch:{ IOException -> 0x018e, all -> 0x0199 }
            r2.write(r3)     // Catch:{ IOException -> 0x018e, all -> 0x0199 }
            r2.flush()     // Catch:{ IOException -> 0x018e, all -> 0x0199 }
            byte[] r3 = r1.toByteArray()     // Catch:{ IOException -> 0x018e, all -> 0x0199 }
            int r0 = r8.simpleCommand(r3)     // Catch:{ IOException -> 0x018e, all -> 0x0199 }
            r1.reset()     // Catch:{ IOException -> 0x018e, all -> 0x0199 }
        L_0x0162:
            r3 = 334(0x14e, float:4.68E-43)
            if (r0 != r3) goto L_0x017b
            byte[] r3 = com.sun.mail.util.ASCIIUtility.getBytes(r12)     // Catch:{ IOException -> 0x018e, all -> 0x0199 }
            r2.write(r3)     // Catch:{ IOException -> 0x018e, all -> 0x0199 }
            r2.flush()     // Catch:{ IOException -> 0x018e, all -> 0x0199 }
            byte[] r2 = r1.toByteArray()     // Catch:{ IOException -> 0x018e, all -> 0x0199 }
            int r0 = r8.simpleCommand(r2)     // Catch:{ IOException -> 0x018e, all -> 0x0199 }
            r1.reset()     // Catch:{ IOException -> 0x018e, all -> 0x0199 }
        L_0x017b:
            r1 = 235(0xeb, float:3.3E-43)
            if (r0 == r1) goto L_0x0281
            r8.closeConnection()
            r0 = 0
            goto L_0x007a
        L_0x0185:
            int r10 = r8.defaultPort
            goto L_0x00a6
        L_0x0189:
            r8.openServer(r1, r10)
            goto L_0x00b8
        L_0x018e:
            r1 = move-exception
            r1 = 235(0xeb, float:3.3E-43)
            if (r0 == r1) goto L_0x0281
            r8.closeConnection()
            r0 = 0
            goto L_0x007a
        L_0x0199:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            r2 = 235(0xeb, float:3.3E-43)
            if (r1 == r2) goto L_0x01a7
            r8.closeConnection()
            r0 = 0
            goto L_0x007a
        L_0x01a7:
            throw r0
        L_0x01a8:
            java.lang.String r0 = "PLAIN"
            boolean r0 = r8.supportsAuthentication(r0)
            if (r0 == 0) goto L_0x0209
            java.lang.String r0 = "AUTH PLAIN"
            int r1 = r8.simpleCommand(r0)
            java.io.ByteArrayOutputStream r0 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x01f2, all -> 0x01fd }
            r0.<init>()     // Catch:{ IOException -> 0x01f2, all -> 0x01fd }
            com.sun.mail.util.BASE64EncoderStream r2 = new com.sun.mail.util.BASE64EncoderStream     // Catch:{ IOException -> 0x01f2, all -> 0x01fd }
            r3 = 2147483647(0x7fffffff, float:NaN)
            r2.<init>(r0, r3)     // Catch:{ IOException -> 0x01f2, all -> 0x01fd }
            r3 = 334(0x14e, float:4.68E-43)
            if (r1 != r3) goto L_0x028a
            r3 = 0
            r2.write(r3)     // Catch:{ IOException -> 0x01f2, all -> 0x01fd }
            byte[] r3 = com.sun.mail.util.ASCIIUtility.getBytes(r11)     // Catch:{ IOException -> 0x01f2, all -> 0x01fd }
            r2.write(r3)     // Catch:{ IOException -> 0x01f2, all -> 0x01fd }
            r3 = 0
            r2.write(r3)     // Catch:{ IOException -> 0x01f2, all -> 0x01fd }
            byte[] r3 = com.sun.mail.util.ASCIIUtility.getBytes(r12)     // Catch:{ IOException -> 0x01f2, all -> 0x01fd }
            r2.write(r3)     // Catch:{ IOException -> 0x01f2, all -> 0x01fd }
            r2.flush()     // Catch:{ IOException -> 0x01f2, all -> 0x01fd }
            byte[] r0 = r0.toByteArray()     // Catch:{ IOException -> 0x01f2, all -> 0x01fd }
            int r0 = r8.simpleCommand(r0)     // Catch:{ IOException -> 0x01f2, all -> 0x01fd }
        L_0x01e8:
            r1 = 235(0xeb, float:3.3E-43)
            if (r0 == r1) goto L_0x0281
            r8.closeConnection()
            r0 = 0
            goto L_0x007a
        L_0x01f2:
            r0 = move-exception
            r0 = 235(0xeb, float:3.3E-43)
            if (r1 == r0) goto L_0x0281
            r8.closeConnection()
            r0 = 0
            goto L_0x007a
        L_0x01fd:
            r0 = move-exception
            r2 = 235(0xeb, float:3.3E-43)
            if (r1 == r2) goto L_0x0208
            r8.closeConnection()
            r0 = 0
            goto L_0x007a
        L_0x0208:
            throw r0
        L_0x0209:
            java.lang.String r0 = "DIGEST-MD5"
            boolean r0 = r8.supportsAuthentication(r0)
            if (r0 == 0) goto L_0x0281
            com.sun.mail.smtp.DigestMD5 r0 = r8.getMD5()
            if (r0 == 0) goto L_0x0281
            java.lang.String r2 = "AUTH DIGEST-MD5"
            int r6 = r8.simpleCommand(r2)
            r2 = 334(0x14e, float:4.68E-43)
            if (r6 != r2) goto L_0x0288
            java.lang.String r4 = r8.getSASLRealm()     // Catch:{ Exception -> 0x0250, all -> 0x0274 }
            java.lang.String r5 = r8.lastServerResponse     // Catch:{ Exception -> 0x0250, all -> 0x0274 }
            r2 = r11
            r3 = r12
            byte[] r1 = r0.authClient(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0250, all -> 0x0274 }
            int r1 = r8.simpleCommand(r1)     // Catch:{ Exception -> 0x0250, all -> 0x0274 }
            r2 = 334(0x14e, float:4.68E-43)
            if (r1 != r2) goto L_0x023e
            java.lang.String r2 = r8.lastServerResponse     // Catch:{ Exception -> 0x0286 }
            boolean r0 = r0.authServer(r2)     // Catch:{ Exception -> 0x0286 }
            if (r0 != 0) goto L_0x0248
            r1 = -1
        L_0x023e:
            r0 = 235(0xeb, float:3.3E-43)
            if (r1 == r0) goto L_0x0281
            r8.closeConnection()
            r0 = 0
            goto L_0x007a
        L_0x0248:
            r0 = 0
            byte[] r0 = new byte[r0]     // Catch:{ Exception -> 0x0286 }
            int r1 = r8.simpleCommand(r0)     // Catch:{ Exception -> 0x0286 }
            goto L_0x023e
        L_0x0250:
            r0 = move-exception
            r1 = r6
        L_0x0252:
            boolean r2 = r8.debug     // Catch:{ all -> 0x0284 }
            if (r2 == 0) goto L_0x026a
            java.io.PrintStream r2 = r8.out     // Catch:{ all -> 0x0284 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0284 }
            java.lang.String r4 = "DEBUG SMTP: DIGEST-MD5: "
            r3.<init>(r4)     // Catch:{ all -> 0x0284 }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ all -> 0x0284 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0284 }
            r2.println(r0)     // Catch:{ all -> 0x0284 }
        L_0x026a:
            r0 = 235(0xeb, float:3.3E-43)
            if (r1 == r0) goto L_0x0281
            r8.closeConnection()
            r0 = 0
            goto L_0x007a
        L_0x0274:
            r0 = move-exception
            r1 = r6
        L_0x0276:
            r2 = 235(0xeb, float:3.3E-43)
            if (r1 == r2) goto L_0x0280
            r8.closeConnection()
            r0 = 0
            goto L_0x007a
        L_0x0280:
            throw r0
        L_0x0281:
            r0 = 1
            goto L_0x007a
        L_0x0284:
            r0 = move-exception
            goto L_0x0276
        L_0x0286:
            r0 = move-exception
            goto L_0x0252
        L_0x0288:
            r1 = r6
            goto L_0x023e
        L_0x028a:
            r0 = r1
            goto L_0x01e8
        L_0x028d:
            r0 = r3
            goto L_0x00c2
        L_0x0290:
            r1 = r9
            goto L_0x00b0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sun.mail.smtp.SMTPTransport.protocolConnect(java.lang.String, int, java.lang.String, java.lang.String):boolean");
    }

    public synchronized void sendMessage(Message message2, Address[] addressArr) throws MessagingException, SendFailedException {
        boolean z;
        checkConnected();
        if (!(message2 instanceof MimeMessage)) {
            if (this.debug) {
                this.out.println("DEBUG SMTP: Can only send RFC822 msgs");
            }
            throw new MessagingException("SMTP can only send RFC822 messages");
        }
        for (int i = 0; i < addressArr.length; i++) {
            if (!(addressArr[i] instanceof InternetAddress)) {
                throw new MessagingException(addressArr[i] + " is not an InternetAddress");
            }
        }
        this.message = (MimeMessage) message2;
        this.addresses = addressArr;
        this.validUnsentAddr = addressArr;
        expandGroups();
        if (message2 instanceof SMTPMessage) {
            z = ((SMTPMessage) message2).getAllow8bitMIME();
        } else {
            z = false;
        }
        if (!z) {
            String property = this.session.getProperty("mail." + this.name + ".allow8bitmime");
            z = property != null && property.equalsIgnoreCase("true");
        }
        if (this.debug) {
            this.out.println("DEBUG SMTP: use8bit " + z);
        }
        if (z && supportsExtension("8BITMIME") && convertTo8Bit(this.message)) {
            try {
                this.message.saveChanges();
            } catch (MessagingException e) {
            }
        }
        try {
            mailFrom();
            rcptTo();
            this.message.writeTo(data(), ignoreList);
            finishData();
            if (this.sendPartiallyFailed) {
                if (this.debug) {
                    this.out.println("DEBUG SMTP: Sending partially failed because of invalid destination addresses");
                }
                notifyTransportListeners(3, this.validSentAddr, this.validUnsentAddr, this.invalidAddr, this.message);
                throw new SMTPSendFailedException(".", this.lastReturnCode, this.lastServerResponse, this.exception, this.validSentAddr, this.validUnsentAddr, this.invalidAddr);
            }
            notifyTransportListeners(1, this.validSentAddr, this.validUnsentAddr, this.invalidAddr, this.message);
            this.invalidAddr = null;
            this.validUnsentAddr = null;
            this.validSentAddr = null;
            this.addresses = null;
            this.message = null;
            this.exception = null;
            this.sendPartiallyFailed = $assertionsDisabled;
        } catch (MessagingException e2) {
            MessagingException messagingException = e2;
            if (this.debug) {
                messagingException.printStackTrace(this.out);
            }
            notifyTransportListeners(2, this.validSentAddr, this.validUnsentAddr, this.invalidAddr, this.message);
            throw messagingException;
        } catch (IOException e3) {
            IOException iOException = e3;
            if (this.debug) {
                iOException.printStackTrace(this.out);
            }
            try {
                closeConnection();
            } catch (MessagingException e4) {
            }
            notifyTransportListeners(2, this.validSentAddr, this.validUnsentAddr, this.invalidAddr, this.message);
            throw new MessagingException("IOException while sending message", iOException);
        } catch (Throwable th) {
            this.invalidAddr = null;
            this.validUnsentAddr = null;
            this.validSentAddr = null;
            this.addresses = null;
            this.message = null;
            this.exception = null;
            this.sendPartiallyFailed = $assertionsDisabled;
            throw th;
        }
    }

    public synchronized void close() throws MessagingException {
        int readServerResponse;
        if (super.isConnected()) {
            try {
                if (this.serverSocket != null) {
                    sendCommand("QUIT");
                    if (!(!this.quitWait || (readServerResponse = readServerResponse()) == 221 || readServerResponse == -1)) {
                        this.out.println("DEBUG SMTP: QUIT failed with " + readServerResponse);
                    }
                }
            } finally {
                closeConnection();
            }
        }
    }

    private void closeConnection() throws MessagingException {
        try {
            if (this.serverSocket != null) {
                this.serverSocket.close();
            }
            this.serverSocket = null;
            this.serverOutput = null;
            this.serverInput = null;
            this.lineInputStream = null;
            if (super.isConnected()) {
                super.close();
            }
        } catch (IOException e) {
            throw new MessagingException("Server Close Failed", e);
        } catch (Throwable th) {
            this.serverSocket = null;
            this.serverOutput = null;
            this.serverInput = null;
            this.lineInputStream = null;
            if (super.isConnected()) {
                super.close();
            }
            throw th;
        }
    }

    public synchronized boolean isConnected() {
        boolean z = $assertionsDisabled;
        synchronized (this) {
            if (super.isConnected()) {
                try {
                    if (this.useRset) {
                        sendCommand("RSET");
                    } else {
                        sendCommand("NOOP");
                    }
                    int readServerResponse = readServerResponse();
                    if (readServerResponse < 0 || readServerResponse == 421) {
                        try {
                            closeConnection();
                        } catch (MessagingException e) {
                        }
                    } else {
                        z = true;
                    }
                } catch (Exception e2) {
                    try {
                        closeConnection();
                    } catch (MessagingException e3) {
                    }
                }
            }
        }
        return z;
    }

    private void expandGroups() {
        Vector vector = null;
        int i = 0;
        while (i < this.addresses.length) {
            InternetAddress internetAddress = (InternetAddress) this.addresses[i];
            if (internetAddress.isGroup()) {
                if (vector == null) {
                    vector = new Vector();
                    for (int i2 = 0; i2 < i; i2++) {
                        vector.addElement(this.addresses[i2]);
                    }
                }
                try {
                    InternetAddress[] group = internetAddress.getGroup(true);
                    if (group != null) {
                        for (InternetAddress addElement : group) {
                            vector.addElement(addElement);
                        }
                    } else {
                        vector.addElement(internetAddress);
                    }
                } catch (ParseException e) {
                    vector.addElement(internetAddress);
                }
            } else if (vector != null) {
                vector.addElement(internetAddress);
            }
            i++;
            vector = vector;
        }
        if (vector != null) {
            InternetAddress[] internetAddressArr = new InternetAddress[vector.size()];
            vector.copyInto(internetAddressArr);
            this.addresses = internetAddressArr;
        }
    }

    private boolean convertTo8Bit(MimePart mimePart) {
        boolean z;
        boolean z2 = $assertionsDisabled;
        try {
            if (mimePart.isMimeType("text/*")) {
                String encoding = mimePart.getEncoding();
                if (encoding != null && ((encoding.equalsIgnoreCase("quoted-printable") || encoding.equalsIgnoreCase("base64")) && is8Bit(mimePart.getInputStream()))) {
                    mimePart.setContent(mimePart.getContent(), mimePart.getContentType());
                    mimePart.setHeader("Content-Transfer-Encoding", "8bit");
                    return true;
                }
            } else if (mimePart.isMimeType("multipart/*")) {
                MimeMultipart mimeMultipart = (MimeMultipart) mimePart.getContent();
                int count = mimeMultipart.getCount();
                int i = 0;
                while (i < count) {
                    if (convertTo8Bit((MimePart) mimeMultipart.getBodyPart(i))) {
                        z = true;
                    } else {
                        z = z2;
                    }
                    i++;
                    z2 = z;
                }
                return z2;
            }
            return $assertionsDisabled;
        } catch (IOException e) {
            return $assertionsDisabled;
        } catch (MessagingException e2) {
            return $assertionsDisabled;
        }
    }

    private boolean is8Bit(InputStream inputStream) {
        boolean z = false;
        int i = 0;
        while (true) {
            try {
                int read = inputStream.read();
                if (read < 0) {
                    if (this.debug && z) {
                        this.out.println("DEBUG SMTP: found an 8bit part");
                    }
                    return z;
                }
                int i2 = read & 255;
                if (i2 == 13 || i2 == 10) {
                    i = 0;
                } else if (i2 == 0 || (i = i + 1) > 998) {
                    return $assertionsDisabled;
                }
                if (i2 > 127) {
                    z = true;
                }
            } catch (IOException e) {
                return $assertionsDisabled;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        super.finalize();
        try {
            closeConnection();
        } catch (MessagingException e) {
        }
    }

    /* access modifiers changed from: protected */
    public void helo(String str) throws MessagingException {
        if (str != null) {
            issueCommand("HELO " + str, 250);
        } else {
            issueCommand("HELO", 250);
        }
    }

    /* access modifiers changed from: protected */
    public boolean ehlo(String str) throws MessagingException {
        String str2;
        if (str != null) {
            str2 = "EHLO " + str;
        } else {
            str2 = "EHLO";
        }
        sendCommand(str2);
        int readServerResponse = readServerResponse();
        if (readServerResponse == 250) {
            BufferedReader bufferedReader = new BufferedReader(new StringReader(this.lastServerResponse));
            this.extMap = new Hashtable();
            boolean z = true;
            while (true) {
                try {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        break;
                    } else if (z) {
                        z = false;
                    } else if (readLine.length() >= 5) {
                        String substring = readLine.substring(4);
                        int indexOf = substring.indexOf(32);
                        String str3 = "";
                        if (indexOf > 0) {
                            str3 = substring.substring(indexOf + 1);
                            substring = substring.substring(0, indexOf);
                        }
                        if (this.debug) {
                            this.out.println("DEBUG SMTP: Found extension \"" + substring + "\", arg \"" + str3 + "\"");
                        }
                        this.extMap.put(substring.toUpperCase(Locale.ENGLISH), str3);
                    }
                } catch (IOException e) {
                }
            }
        }
        if (readServerResponse == 250) {
            return true;
        }
        return $assertionsDisabled;
    }

    /* access modifiers changed from: protected */
    public void mailFrom() throws MessagingException {
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        Address localAddress;
        Address[] from;
        if (this.message instanceof SMTPMessage) {
            str = ((SMTPMessage) this.message).getEnvelopeFrom();
        } else {
            str = null;
        }
        if (str == null || str.length() <= 0) {
            str = this.session.getProperty("mail." + this.name + ".from");
        }
        if (str == null || str.length() <= 0) {
            if (this.message == null || (from = this.message.getFrom()) == null || from.length <= 0) {
                localAddress = InternetAddress.getLocalAddress(this.session);
            } else {
                localAddress = from[0];
            }
            if (localAddress != null) {
                str = ((InternetAddress) localAddress).getAddress();
            } else {
                throw new MessagingException("can't determine local email address");
            }
        }
        String str6 = "MAIL FROM:" + normalizeAddress(str);
        if (supportsExtension("DSN")) {
            if (this.message instanceof SMTPMessage) {
                str5 = ((SMTPMessage) this.message).getDSNRet();
            } else {
                str5 = null;
            }
            if (str5 == null) {
                str5 = this.session.getProperty("mail." + this.name + ".dsn.ret");
            }
            if (str5 != null) {
                str6 = String.valueOf(str6) + " RET=" + str5;
            }
        }
        if (supportsExtension("AUTH")) {
            if (this.message instanceof SMTPMessage) {
                str3 = ((SMTPMessage) this.message).getSubmitter();
            } else {
                str3 = null;
            }
            if (str3 == null) {
                str4 = this.session.getProperty("mail." + this.name + ".submitter");
            } else {
                str4 = str3;
            }
            if (str4 != null) {
                try {
                    str6 = String.valueOf(str6) + " AUTH=" + xtext(str4);
                } catch (IllegalArgumentException e) {
                    if (this.debug) {
                        this.out.println("DEBUG SMTP: ignoring invalid submitter: " + str4 + ", Exception: " + e);
                    }
                }
            }
        }
        if (this.message instanceof SMTPMessage) {
            str2 = ((SMTPMessage) this.message).getMailExtension();
        } else {
            str2 = null;
        }
        if (str2 == null) {
            str2 = this.session.getProperty("mail." + this.name + ".mailextension");
        }
        if (str2 != null && str2.length() > 0) {
            str6 = String.valueOf(str6) + " " + str2;
        }
        issueSendCommand(str6, 250);
    }

    /* access modifiers changed from: protected */
    public void rcptTo() throws MessagingException {
        boolean z;
        boolean z2;
        String str;
        String str2;
        boolean z3;
        SMTPAddressFailedException sMTPAddressFailedException;
        Vector vector = new Vector();
        Vector vector2 = new Vector();
        Vector vector3 = new Vector();
        SMTPAddressFailedException sMTPAddressFailedException2 = null;
        boolean z4 = $assertionsDisabled;
        this.invalidAddr = null;
        this.validUnsentAddr = null;
        this.validSentAddr = null;
        boolean z5 = $assertionsDisabled;
        if (this.message instanceof SMTPMessage) {
            z5 = ((SMTPMessage) this.message).getSendPartial();
        }
        if (!z5) {
            String property = this.session.getProperty("mail." + this.name + ".sendpartial");
            z = (property == null || !property.equalsIgnoreCase("true")) ? $assertionsDisabled : true;
        } else {
            z = z5;
        }
        if (this.debug && z) {
            this.out.println("DEBUG SMTP: sendPartial set");
        }
        String str3 = null;
        if (supportsExtension("DSN")) {
            if (this.message instanceof SMTPMessage) {
                str3 = ((SMTPMessage) this.message).getDSNNotify();
            }
            if (str3 == null) {
                str3 = this.session.getProperty("mail." + this.name + ".dsn.notify");
            }
            if (str3 != null) {
                z2 = true;
                str = str3;
            } else {
                z2 = false;
                str = str3;
            }
        } else {
            z2 = false;
            str = null;
        }
        int i = 0;
        while (i < this.addresses.length) {
            InternetAddress internetAddress = (InternetAddress) this.addresses[i];
            String str4 = "RCPT TO:" + normalizeAddress(internetAddress.getAddress());
            if (z2) {
                str2 = String.valueOf(str4) + " NOTIFY=" + str;
            } else {
                str2 = str4;
            }
            sendCommand(str2);
            int readServerResponse = readServerResponse();
            switch (readServerResponse) {
                case 250:
                case 251:
                    vector.addElement(internetAddress);
                    if (this.reportSuccess) {
                        SMTPAddressSucceededException sMTPAddressSucceededException = new SMTPAddressSucceededException(internetAddress, str2, readServerResponse, this.lastServerResponse);
                        if (sMTPAddressFailedException2 != null) {
                            sMTPAddressFailedException2.setNextException(sMTPAddressSucceededException);
                            z3 = z4;
                            sMTPAddressFailedException = sMTPAddressFailedException2;
                            break;
                        } else {
                            z3 = z4;
                            sMTPAddressFailedException = sMTPAddressSucceededException;
                            break;
                        }
                    } else {
                        z3 = z4;
                        sMTPAddressFailedException = sMTPAddressFailedException2;
                        break;
                    }
                case 450:
                case 451:
                case 452:
                case 552:
                    if (!z) {
                        z4 = true;
                    }
                    vector2.addElement(internetAddress);
                    SMTPAddressFailedException sMTPAddressFailedException3 = new SMTPAddressFailedException(internetAddress, str2, readServerResponse, this.lastServerResponse);
                    if (sMTPAddressFailedException2 != null) {
                        sMTPAddressFailedException2.setNextException(sMTPAddressFailedException3);
                        z3 = z4;
                        sMTPAddressFailedException = sMTPAddressFailedException2;
                        break;
                    } else {
                        z3 = z4;
                        sMTPAddressFailedException = sMTPAddressFailedException3;
                        break;
                    }
                case 501:
                case 503:
                case 550:
                case 551:
                case 553:
                    if (!z) {
                        z4 = true;
                    }
                    vector3.addElement(internetAddress);
                    SMTPAddressFailedException sMTPAddressFailedException4 = new SMTPAddressFailedException(internetAddress, str2, readServerResponse, this.lastServerResponse);
                    if (sMTPAddressFailedException2 != null) {
                        sMTPAddressFailedException2.setNextException(sMTPAddressFailedException4);
                        z3 = z4;
                        sMTPAddressFailedException = sMTPAddressFailedException2;
                        break;
                    } else {
                        z3 = z4;
                        sMTPAddressFailedException = sMTPAddressFailedException4;
                        break;
                    }
                default:
                    if (readServerResponse >= 400 && readServerResponse <= 499) {
                        vector2.addElement(internetAddress);
                    } else if (readServerResponse < 500 || readServerResponse > 599) {
                        if (this.debug) {
                            this.out.println("DEBUG SMTP: got response code " + readServerResponse + ", with response: " + this.lastServerResponse);
                        }
                        String str5 = this.lastServerResponse;
                        int i2 = this.lastReturnCode;
                        if (this.serverSocket != null) {
                            issueCommand("RSET", 250);
                        }
                        this.lastServerResponse = str5;
                        this.lastReturnCode = i2;
                        throw new SMTPAddressFailedException(internetAddress, str2, readServerResponse, str5);
                    } else {
                        vector3.addElement(internetAddress);
                    }
                    if (!z) {
                        z4 = true;
                    }
                    SMTPAddressFailedException sMTPAddressFailedException5 = new SMTPAddressFailedException(internetAddress, str2, readServerResponse, this.lastServerResponse);
                    if (sMTPAddressFailedException2 != null) {
                        sMTPAddressFailedException2.setNextException(sMTPAddressFailedException5);
                        z3 = z4;
                        sMTPAddressFailedException = sMTPAddressFailedException2;
                        break;
                    } else {
                        z3 = z4;
                        sMTPAddressFailedException = sMTPAddressFailedException5;
                        break;
                    }
                    break;
            }
            i++;
            sMTPAddressFailedException2 = sMTPAddressFailedException;
            z4 = z3;
        }
        if (z && vector.size() == 0) {
            z4 = true;
        }
        if (z4) {
            this.invalidAddr = new Address[vector3.size()];
            vector3.copyInto(this.invalidAddr);
            this.validUnsentAddr = new Address[(vector.size() + vector2.size())];
            int i3 = 0;
            int i4 = 0;
            while (i4 < vector.size()) {
                this.validUnsentAddr[i3] = (Address) vector.elementAt(i4);
                i4++;
                i3++;
            }
            int i5 = 0;
            while (i5 < vector2.size()) {
                this.validUnsentAddr[i3] = (Address) vector2.elementAt(i5);
                i5++;
                i3++;
            }
        } else if (this.reportSuccess || (z && (vector3.size() > 0 || vector2.size() > 0))) {
            this.sendPartiallyFailed = true;
            this.exception = sMTPAddressFailedException2;
            this.invalidAddr = new Address[vector3.size()];
            vector3.copyInto(this.invalidAddr);
            this.validUnsentAddr = new Address[vector2.size()];
            vector2.copyInto(this.validUnsentAddr);
            this.validSentAddr = new Address[vector.size()];
            vector.copyInto(this.validSentAddr);
        } else {
            this.validSentAddr = this.addresses;
        }
        if (this.debug) {
            if (this.validSentAddr != null && this.validSentAddr.length > 0) {
                this.out.println("DEBUG SMTP: Verified Addresses");
                for (int i6 = 0; i6 < this.validSentAddr.length; i6++) {
                    this.out.println("DEBUG SMTP:   " + this.validSentAddr[i6]);
                }
            }
            if (this.validUnsentAddr != null && this.validUnsentAddr.length > 0) {
                this.out.println("DEBUG SMTP: Valid Unsent Addresses");
                for (int i7 = 0; i7 < this.validUnsentAddr.length; i7++) {
                    this.out.println("DEBUG SMTP:   " + this.validUnsentAddr[i7]);
                }
            }
            if (this.invalidAddr != null && this.invalidAddr.length > 0) {
                this.out.println("DEBUG SMTP: Invalid Addresses");
                for (int i8 = 0; i8 < this.invalidAddr.length; i8++) {
                    this.out.println("DEBUG SMTP:   " + this.invalidAddr[i8]);
                }
            }
        }
        if (z4) {
            if (this.debug) {
                this.out.println("DEBUG SMTP: Sending failed because of invalid destination addresses");
            }
            notifyTransportListeners(2, this.validSentAddr, this.validUnsentAddr, this.invalidAddr, this.message);
            String str6 = this.lastServerResponse;
            int i9 = this.lastReturnCode;
            try {
                if (this.serverSocket != null) {
                    issueCommand("RSET", 250);
                }
                this.lastServerResponse = str6;
                this.lastReturnCode = i9;
            } catch (MessagingException e) {
                try {
                    close();
                } catch (MessagingException e2) {
                    if (this.debug) {
                        e2.printStackTrace(this.out);
                    }
                } catch (Throwable th) {
                    this.lastServerResponse = str6;
                    this.lastReturnCode = i9;
                    throw th;
                }
                this.lastServerResponse = str6;
                this.lastReturnCode = i9;
            }
            throw new SendFailedException("Invalid Addresses", sMTPAddressFailedException2, this.validSentAddr, this.validUnsentAddr, this.invalidAddr);
        }
    }

    /* access modifiers changed from: protected */
    public OutputStream data() throws MessagingException {
        if ($assertionsDisabled || Thread.holdsLock(this)) {
            issueSendCommand("DATA", 354);
            this.dataStream = new SMTPOutputStream(this.serverOutput);
            return this.dataStream;
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: protected */
    public void finishData() throws IOException, MessagingException {
        if ($assertionsDisabled || Thread.holdsLock(this)) {
            this.dataStream.ensureAtBOL();
            issueSendCommand(".", 250);
            return;
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: protected */
    public void startTLS() throws MessagingException {
        issueCommand("STARTTLS", 220);
        try {
            this.serverSocket = SocketFetcher.startTLS(this.serverSocket, this.session.getProperties(), "mail." + this.name);
            initStreams();
        } catch (IOException e) {
            closeConnection();
            throw new MessagingException("Could not convert socket to TLS", e);
        }
    }

    private void openServer(String str, int i) throws MessagingException {
        if (this.debug) {
            this.out.println("DEBUG SMTP: trying to connect to host \"" + str + "\", port " + i + ", isSSL " + this.isSSL);
        }
        try {
            this.serverSocket = SocketFetcher.getSocket(str, i, this.session.getProperties(), "mail." + this.name, this.isSSL);
            i = this.serverSocket.getPort();
            initStreams();
            int readServerResponse = readServerResponse();
            if (readServerResponse != 220) {
                this.serverSocket.close();
                this.serverSocket = null;
                this.serverOutput = null;
                this.serverInput = null;
                this.lineInputStream = null;
                if (this.debug) {
                    this.out.println("DEBUG SMTP: could not connect to host \"" + str + "\", port: " + i + ", response: " + readServerResponse + "\n");
                }
                throw new MessagingException("Could not connect to SMTP host: " + str + ", port: " + i + ", response: " + readServerResponse);
            } else if (this.debug) {
                this.out.println("DEBUG SMTP: connected to host \"" + str + "\", port: " + i + "\n");
            }
        } catch (UnknownHostException e) {
            throw new MessagingException("Unknown SMTP host: " + str, e);
        } catch (IOException e2) {
            throw new MessagingException("Could not connect to SMTP host: " + str + ", port: " + i, e2);
        }
    }

    private void openServer() throws MessagingException {
        int i = -1;
        String str = UNKNOWN;
        try {
            i = this.serverSocket.getPort();
            str = this.serverSocket.getInetAddress().getHostName();
            if (this.debug) {
                this.out.println("DEBUG SMTP: starting protocol to host \"" + str + "\", port " + i);
            }
            initStreams();
            int readServerResponse = readServerResponse();
            if (readServerResponse != 220) {
                this.serverSocket.close();
                this.serverSocket = null;
                this.serverOutput = null;
                this.serverInput = null;
                this.lineInputStream = null;
                if (this.debug) {
                    this.out.println("DEBUG SMTP: got bad greeting from host \"" + str + "\", port: " + i + ", response: " + readServerResponse + "\n");
                }
                throw new MessagingException("Got bad greeting from SMTP host: " + str + ", port: " + i + ", response: " + readServerResponse);
            } else if (this.debug) {
                this.out.println("DEBUG SMTP: protocol started to host \"" + str + "\", port: " + i + "\n");
            }
        } catch (IOException e) {
            throw new MessagingException("Could not start protocol to SMTP host: " + str + ", port: " + i, e);
        }
    }

    private void initStreams() throws IOException {
        Properties properties = this.session.getProperties();
        PrintStream debugOut = this.session.getDebugOut();
        boolean debug = this.session.getDebug();
        String property = properties.getProperty("mail.debug.quote");
        boolean z = (property == null || !property.equalsIgnoreCase("true")) ? $assertionsDisabled : true;
        TraceInputStream traceInputStream = new TraceInputStream(this.serverSocket.getInputStream(), debugOut);
        traceInputStream.setTrace(debug);
        traceInputStream.setQuote(z);
        TraceOutputStream traceOutputStream = new TraceOutputStream(this.serverSocket.getOutputStream(), debugOut);
        traceOutputStream.setTrace(debug);
        traceOutputStream.setQuote(z);
        this.serverOutput = new BufferedOutputStream(traceOutputStream);
        this.serverInput = new BufferedInputStream(traceInputStream);
        this.lineInputStream = new LineInputStream(this.serverInput);
    }

    public synchronized void issueCommand(String str, int i) throws MessagingException {
        sendCommand(str);
        if (readServerResponse() != i) {
            throw new MessagingException(this.lastServerResponse);
        }
    }

    private void issueSendCommand(String str, int i) throws MessagingException {
        int length;
        sendCommand(str);
        int readServerResponse = readServerResponse();
        if (readServerResponse != i) {
            int length2 = this.validSentAddr == null ? 0 : this.validSentAddr.length;
            if (this.validUnsentAddr == null) {
                length = 0;
            } else {
                length = this.validUnsentAddr.length;
            }
            Address[] addressArr = new Address[(length2 + length)];
            if (length2 > 0) {
                System.arraycopy(this.validSentAddr, 0, addressArr, 0, length2);
            }
            if (length > 0) {
                System.arraycopy(this.validUnsentAddr, 0, addressArr, length2, length);
            }
            this.validSentAddr = null;
            this.validUnsentAddr = addressArr;
            if (this.debug) {
                this.out.println("DEBUG SMTP: got response code " + readServerResponse + ", with response: " + this.lastServerResponse);
            }
            String str2 = this.lastServerResponse;
            int i2 = this.lastReturnCode;
            if (this.serverSocket != null) {
                issueCommand("RSET", 250);
            }
            this.lastServerResponse = str2;
            this.lastReturnCode = i2;
            throw new SMTPSendFailedException(str, readServerResponse, this.lastServerResponse, this.exception, this.validSentAddr, this.validUnsentAddr, this.invalidAddr);
        }
    }

    public synchronized int simpleCommand(String str) throws MessagingException {
        sendCommand(str);
        return readServerResponse();
    }

    /* access modifiers changed from: protected */
    public int simpleCommand(byte[] bArr) throws MessagingException {
        if ($assertionsDisabled || Thread.holdsLock(this)) {
            sendCommand(bArr);
            return readServerResponse();
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: protected */
    public void sendCommand(String str) throws MessagingException {
        sendCommand(ASCIIUtility.getBytes(str));
    }

    private void sendCommand(byte[] bArr) throws MessagingException {
        if ($assertionsDisabled || Thread.holdsLock(this)) {
            try {
                this.serverOutput.write(bArr);
                this.serverOutput.write(CRLF);
                this.serverOutput.flush();
            } catch (IOException e) {
                throw new MessagingException("Can't send command to SMTP host", e);
            }
        } else {
            throw new AssertionError();
        }
    }

    /* access modifiers changed from: protected */
    public int readServerResponse() throws MessagingException {
        String readLine;
        int i;
        if ($assertionsDisabled || Thread.holdsLock(this)) {
            StringBuffer stringBuffer = new StringBuffer(100);
            do {
                try {
                    readLine = this.lineInputStream.readLine();
                    if (readLine == null) {
                        String stringBuffer2 = stringBuffer.toString();
                        if (stringBuffer2.length() == 0) {
                            stringBuffer2 = "[EOF]";
                        }
                        this.lastServerResponse = stringBuffer2;
                        this.lastReturnCode = -1;
                        if (!this.debug) {
                            return -1;
                        }
                        this.out.println("DEBUG SMTP: EOF: " + stringBuffer2);
                        return -1;
                    }
                    stringBuffer.append(readLine);
                    stringBuffer.append("\n");
                } catch (IOException e) {
                    if (this.debug) {
                        this.out.println("DEBUG SMTP: exception reading response: " + e);
                    }
                    this.lastServerResponse = "";
                    this.lastReturnCode = 0;
                    throw new MessagingException("Exception reading response", e);
                }
            } while (isNotLastLine(readLine));
            String stringBuffer3 = stringBuffer.toString();
            if (stringBuffer3 == null || stringBuffer3.length() < 3) {
                i = -1;
            } else {
                try {
                    i = Integer.parseInt(stringBuffer3.substring(0, 3));
                } catch (NumberFormatException e2) {
                    try {
                        close();
                    } catch (MessagingException e3) {
                        if (this.debug) {
                            e3.printStackTrace(this.out);
                        }
                    }
                    i = -1;
                } catch (StringIndexOutOfBoundsException e4) {
                    try {
                        close();
                    } catch (MessagingException e5) {
                        if (this.debug) {
                            e5.printStackTrace(this.out);
                        }
                    }
                    i = -1;
                }
            }
            if (i == -1 && this.debug) {
                this.out.println("DEBUG SMTP: bad server response: " + stringBuffer3);
            }
            this.lastServerResponse = stringBuffer3;
            this.lastReturnCode = i;
            return i;
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: protected */
    public void checkConnected() {
        if (!super.isConnected()) {
            throw new IllegalStateException("Not connected");
        }
    }

    private boolean isNotLastLine(String str) {
        if (str == null || str.length() < 4 || str.charAt(3) != '-') {
            return $assertionsDisabled;
        }
        return true;
    }

    private String normalizeAddress(String str) {
        if (str.startsWith("<") || str.endsWith(">")) {
            return str;
        }
        return "<" + str + ">";
    }

    public boolean supportsExtension(String str) {
        if (this.extMap == null || this.extMap.get(str.toUpperCase(Locale.ENGLISH)) == null) {
            return $assertionsDisabled;
        }
        return true;
    }

    public String getExtensionParameter(String str) {
        if (this.extMap == null) {
            return null;
        }
        return (String) this.extMap.get(str.toUpperCase(Locale.ENGLISH));
    }

    /* access modifiers changed from: protected */
    public boolean supportsAuthentication(String str) {
        if (!$assertionsDisabled && !Thread.holdsLock(this)) {
            throw new AssertionError();
        } else if (this.extMap == null) {
            return $assertionsDisabled;
        } else {
            String str2 = (String) this.extMap.get("AUTH");
            if (str2 == null) {
                return $assertionsDisabled;
            }
            StringTokenizer stringTokenizer = new StringTokenizer(str2);
            while (stringTokenizer.hasMoreTokens()) {
                if (stringTokenizer.nextToken().equalsIgnoreCase(str)) {
                    return true;
                }
            }
            return $assertionsDisabled;
        }
    }

    protected static String xtext(String str) {
        StringBuffer stringBuffer = null;
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (charAt >= 128) {
                throw new IllegalArgumentException("Non-ASCII character in SMTP submitter: " + str);
            }
            if (charAt < '!' || charAt > '~' || charAt == '+' || charAt == '=') {
                if (stringBuffer == null) {
                    stringBuffer = new StringBuffer(str.length() + 4);
                    stringBuffer.append(str.substring(0, i));
                }
                stringBuffer.append('+');
                stringBuffer.append(hexchar[(charAt & 240) >> 4]);
                stringBuffer.append(hexchar[charAt & 15]);
            } else if (stringBuffer != null) {
                stringBuffer.append(charAt);
            }
        }
        return stringBuffer != null ? stringBuffer.toString() : str;
    }
}
