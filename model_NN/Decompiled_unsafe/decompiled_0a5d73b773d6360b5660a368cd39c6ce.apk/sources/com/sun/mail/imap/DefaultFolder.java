package com.sun.mail.imap;

import com.sun.mail.iap.ProtocolException;
import com.sun.mail.imap.IMAPFolder;
import com.sun.mail.imap.protocol.IMAPProtocol;
import com.sun.mail.imap.protocol.ListInfo;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.MethodNotSupportedException;

public class DefaultFolder extends IMAPFolder {
    protected DefaultFolder(IMAPStore iMAPStore) {
        super("", 65535, iMAPStore);
        this.exists = true;
        this.type = 2;
    }

    public String getName() {
        return this.fullName;
    }

    public Folder getParent() {
        return null;
    }

    public Folder[] list(final String str) throws MessagingException {
        int i = 0;
        ListInfo[] listInfoArr = (ListInfo[]) doCommand(new IMAPFolder.ProtocolCommand() {
            public Object doCommand(IMAPProtocol iMAPProtocol) throws ProtocolException {
                return iMAPProtocol.list("", str);
            }
        });
        if (listInfoArr == null) {
            return new Folder[0];
        }
        IMAPFolder[] iMAPFolderArr = new IMAPFolder[listInfoArr.length];
        while (true) {
            int i2 = i;
            if (i2 >= iMAPFolderArr.length) {
                return iMAPFolderArr;
            }
            iMAPFolderArr[i2] = new IMAPFolder(listInfoArr[i2], (IMAPStore) this.store);
            i = i2 + 1;
        }
    }

    public Folder[] listSubscribed(final String str) throws MessagingException {
        int i = 0;
        ListInfo[] listInfoArr = (ListInfo[]) doCommand(new IMAPFolder.ProtocolCommand() {
            public Object doCommand(IMAPProtocol iMAPProtocol) throws ProtocolException {
                return iMAPProtocol.lsub("", str);
            }
        });
        if (listInfoArr == null) {
            return new Folder[0];
        }
        IMAPFolder[] iMAPFolderArr = new IMAPFolder[listInfoArr.length];
        while (true) {
            int i2 = i;
            if (i2 >= iMAPFolderArr.length) {
                return iMAPFolderArr;
            }
            iMAPFolderArr[i2] = new IMAPFolder(listInfoArr[i2], (IMAPStore) this.store);
            i = i2 + 1;
        }
    }

    public boolean hasNewMessages() throws MessagingException {
        return false;
    }

    public Folder getFolder(String str) throws MessagingException {
        return new IMAPFolder(str, 65535, (IMAPStore) this.store);
    }

    public boolean delete(boolean z) throws MessagingException {
        throw new MethodNotSupportedException("Cannot delete Default Folder");
    }

    public boolean renameTo(Folder folder) throws MessagingException {
        throw new MethodNotSupportedException("Cannot rename Default Folder");
    }

    public void appendMessages(Message[] messageArr) throws MessagingException {
        throw new MethodNotSupportedException("Cannot append to Default Folder");
    }

    public Message[] expunge() throws MessagingException {
        throw new MethodNotSupportedException("Cannot expunge Default Folder");
    }
}
