package com.sun.mail.imap;

import com.sun.mail.iap.BadCommandException;
import com.sun.mail.iap.ConnectionException;
import com.sun.mail.iap.ProtocolException;
import com.sun.mail.iap.Response;
import com.sun.mail.iap.ResponseHandler;
import com.sun.mail.imap.protocol.IMAPProtocol;
import com.sun.mail.imap.protocol.Namespaces;
import java.io.PrintStream;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.mail.Folder;
import javax.mail.MessagingException;
import javax.mail.Quota;
import javax.mail.QuotaAwareStore;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.StoreClosedException;
import javax.mail.URLName;

public class IMAPStore extends Store implements QuotaAwareStore, ResponseHandler {
    static final /* synthetic */ boolean $assertionsDisabled = (!IMAPStore.class.desiredAssertionStatus() ? true : $assertionsDisabled);
    public static final int RESPONSE = 1000;
    private int appendBufferSize;
    private String authorizationID;
    private int blksize;
    private volatile boolean connected;
    private int defaultPort;
    private boolean disableAuthLogin;
    private boolean disableAuthPlain;
    private boolean enableImapEvents;
    private boolean enableSASL;
    private boolean enableStartTLS;
    private boolean forcePasswordRefresh;
    private String host;
    private boolean isSSL;
    private int minIdleTime;
    private String name;
    private Namespaces namespaces;
    private PrintStream out;
    private String password;
    private ConnectionPool pool;
    private int port;
    private String proxyAuthUser;
    private String[] saslMechanisms;
    private String saslRealm;
    private int statusCacheTimeout;
    private String user;

    static class ConnectionPool {
        private static final int ABORTING = 2;
        private static final int IDLE = 1;
        private static final int RUNNING = 0;
        /* access modifiers changed from: private */
        public Vector authenticatedConnections = new Vector();
        /* access modifiers changed from: private */
        public long clientTimeoutInterval = 45000;
        /* access modifiers changed from: private */
        public boolean debug = IMAPStore.$assertionsDisabled;
        /* access modifiers changed from: private */
        public Vector folders;
        /* access modifiers changed from: private */
        public IMAPProtocol idleProtocol;
        /* access modifiers changed from: private */
        public int idleState = 0;
        /* access modifiers changed from: private */
        public long lastTimePruned;
        /* access modifiers changed from: private */
        public int poolSize = 1;
        /* access modifiers changed from: private */
        public long pruningInterval = 60000;
        /* access modifiers changed from: private */
        public boolean separateStoreConnection = IMAPStore.$assertionsDisabled;
        /* access modifiers changed from: private */
        public long serverTimeoutInterval = 1800000;
        /* access modifiers changed from: private */
        public boolean storeConnectionInUse = IMAPStore.$assertionsDisabled;

        ConnectionPool() {
        }
    }

    public IMAPStore(Session session, URLName uRLName) {
        this(session, uRLName, "imap", 143, $assertionsDisabled);
    }

    protected IMAPStore(Session session, URLName uRLName, String str, int i, boolean z) {
        super(session, uRLName);
        String property;
        this.name = "imap";
        this.defaultPort = 143;
        this.isSSL = $assertionsDisabled;
        this.port = -1;
        this.blksize = 16384;
        this.statusCacheTimeout = RESPONSE;
        this.appendBufferSize = -1;
        this.minIdleTime = 10;
        this.disableAuthLogin = $assertionsDisabled;
        this.disableAuthPlain = $assertionsDisabled;
        this.enableStartTLS = $assertionsDisabled;
        this.enableSASL = $assertionsDisabled;
        this.forcePasswordRefresh = $assertionsDisabled;
        this.enableImapEvents = $assertionsDisabled;
        this.connected = $assertionsDisabled;
        this.pool = new ConnectionPool();
        str = uRLName != null ? uRLName.getProtocol() : str;
        this.name = str;
        this.defaultPort = i;
        this.isSSL = z;
        this.pool.lastTimePruned = System.currentTimeMillis();
        this.debug = session.getDebug();
        this.out = session.getDebugOut();
        if (this.out == null) {
            this.out = System.out;
        }
        String property2 = session.getProperty("mail." + str + ".connectionpool.debug");
        if (property2 != null && property2.equalsIgnoreCase("true")) {
            this.pool.debug = true;
        }
        String property3 = session.getProperty("mail." + str + ".partialfetch");
        if (property3 == null || !property3.equalsIgnoreCase("false")) {
            String property4 = session.getProperty("mail." + str + ".fetchsize");
            if (property4 != null) {
                this.blksize = Integer.parseInt(property4);
            }
            if (this.debug) {
                this.out.println("DEBUG: mail.imap.fetchsize: " + this.blksize);
            }
        } else {
            this.blksize = -1;
            if (this.debug) {
                this.out.println("DEBUG: mail.imap.partialfetch: false");
            }
        }
        String property5 = session.getProperty("mail." + str + ".statuscachetimeout");
        if (property5 != null) {
            this.statusCacheTimeout = Integer.parseInt(property5);
            if (this.debug) {
                this.out.println("DEBUG: mail.imap.statuscachetimeout: " + this.statusCacheTimeout);
            }
        }
        String property6 = session.getProperty("mail." + str + ".appendbuffersize");
        if (property6 != null) {
            this.appendBufferSize = Integer.parseInt(property6);
            if (this.debug) {
                this.out.println("DEBUG: mail.imap.appendbuffersize: " + this.appendBufferSize);
            }
        }
        String property7 = session.getProperty("mail." + str + ".minidletime");
        if (property7 != null) {
            this.minIdleTime = Integer.parseInt(property7);
            if (this.debug) {
                this.out.println("DEBUG: mail.imap.minidletime: " + this.minIdleTime);
            }
        }
        String property8 = session.getProperty("mail." + str + ".connectionpoolsize");
        if (property8 != null) {
            try {
                int parseInt = Integer.parseInt(property8);
                if (parseInt > 0) {
                    this.pool.poolSize = parseInt;
                }
            } catch (NumberFormatException e) {
            }
            if (this.pool.debug) {
                this.out.println("DEBUG: mail.imap.connectionpoolsize: " + this.pool.poolSize);
            }
        }
        String property9 = session.getProperty("mail." + str + ".connectionpooltimeout");
        if (property9 != null) {
            try {
                int parseInt2 = Integer.parseInt(property9);
                if (parseInt2 > 0) {
                    this.pool.clientTimeoutInterval = (long) parseInt2;
                }
            } catch (NumberFormatException e2) {
            }
            if (this.pool.debug) {
                this.out.println("DEBUG: mail.imap.connectionpooltimeout: " + this.pool.clientTimeoutInterval);
            }
        }
        String property10 = session.getProperty("mail." + str + ".servertimeout");
        if (property10 != null) {
            try {
                int parseInt3 = Integer.parseInt(property10);
                if (parseInt3 > 0) {
                    this.pool.serverTimeoutInterval = (long) parseInt3;
                }
            } catch (NumberFormatException e3) {
            }
            if (this.pool.debug) {
                this.out.println("DEBUG: mail.imap.servertimeout: " + this.pool.serverTimeoutInterval);
            }
        }
        String property11 = session.getProperty("mail." + str + ".separatestoreconnection");
        if (property11 != null && property11.equalsIgnoreCase("true")) {
            if (this.pool.debug) {
                this.out.println("DEBUG: dedicate a store connection");
            }
            this.pool.separateStoreConnection = true;
        }
        String property12 = session.getProperty("mail." + str + ".proxyauth.user");
        if (property12 != null) {
            this.proxyAuthUser = property12;
            if (this.debug) {
                this.out.println("DEBUG: mail.imap.proxyauth.user: " + this.proxyAuthUser);
            }
        }
        String property13 = session.getProperty("mail." + str + ".auth.login.disable");
        if (property13 != null && property13.equalsIgnoreCase("true")) {
            if (this.debug) {
                this.out.println("DEBUG: disable AUTH=LOGIN");
            }
            this.disableAuthLogin = true;
        }
        String property14 = session.getProperty("mail." + str + ".auth.plain.disable");
        if (property14 != null && property14.equalsIgnoreCase("true")) {
            if (this.debug) {
                this.out.println("DEBUG: disable AUTH=PLAIN");
            }
            this.disableAuthPlain = true;
        }
        String property15 = session.getProperty("mail." + str + ".starttls.enable");
        if (property15 != null && property15.equalsIgnoreCase("true")) {
            if (this.debug) {
                this.out.println("DEBUG: enable STARTTLS");
            }
            this.enableStartTLS = true;
        }
        String property16 = session.getProperty("mail." + str + ".sasl.enable");
        if (property16 != null && property16.equalsIgnoreCase("true")) {
            if (this.debug) {
                this.out.println("DEBUG: enable SASL");
            }
            this.enableSASL = true;
        }
        if (this.enableSASL && (property = session.getProperty("mail." + str + ".sasl.mechanisms")) != null && property.length() > 0) {
            if (this.debug) {
                this.out.println("DEBUG: SASL mechanisms allowed: " + property);
            }
            Vector vector = new Vector(5);
            StringTokenizer stringTokenizer = new StringTokenizer(property, " ,");
            while (stringTokenizer.hasMoreTokens()) {
                String nextToken = stringTokenizer.nextToken();
                if (nextToken.length() > 0) {
                    vector.addElement(nextToken);
                }
            }
            this.saslMechanisms = new String[vector.size()];
            vector.copyInto(this.saslMechanisms);
        }
        String property17 = session.getProperty("mail." + str + ".sasl.authorizationid");
        if (property17 != null) {
            this.authorizationID = property17;
            if (this.debug) {
                this.out.println("DEBUG: mail.imap.sasl.authorizationid: " + this.authorizationID);
            }
        }
        String property18 = session.getProperty("mail." + str + ".sasl.realm");
        if (property18 != null) {
            this.saslRealm = property18;
            if (this.debug) {
                this.out.println("DEBUG: mail.imap.sasl.realm: " + this.saslRealm);
            }
        }
        String property19 = session.getProperty("mail." + str + ".forcepasswordrefresh");
        if (property19 != null && property19.equalsIgnoreCase("true")) {
            if (this.debug) {
                this.out.println("DEBUG: enable forcePasswordRefresh");
            }
            this.forcePasswordRefresh = true;
        }
        String property20 = session.getProperty("mail." + str + ".enableimapevents");
        if (property20 != null && property20.equalsIgnoreCase("true")) {
            if (this.debug) {
                this.out.println("DEBUG: enable IMAP events");
            }
            this.enableImapEvents = true;
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00ee, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00ef, code lost:
        r1 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x010b, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0115, code lost:
        throw new javax.mail.MessagingException(r0.getMessage(), r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0116, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0120, code lost:
        throw new javax.mail.MessagingException(r0.getMessage(), r0);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x010b A[ExcHandler: ProtocolException (r0v18 'e' com.sun.mail.iap.ProtocolException A[CUSTOM_DECLARE]), Splitter:B:23:0x004c] */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0116 A[ExcHandler: IOException (r0v17 'e' java.io.IOException A[CUSTOM_DECLARE, Catch:{  }]), Splitter:B:23:0x004c] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:6:0x000a=Splitter:B:6:0x000a, B:23:0x004c=Splitter:B:23:0x004c} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean protocolConnect(java.lang.String r12, int r13, java.lang.String r14, java.lang.String r15) throws javax.mail.MessagingException {
        /*
            r11 = this;
            r8 = 1
            r3 = -1
            monitor-enter(r11)
            r9 = 0
            if (r12 == 0) goto L_0x000a
            if (r15 == 0) goto L_0x000a
            if (r14 != 0) goto L_0x0040
        L_0x000a:
            boolean r0 = r11.debug     // Catch:{ all -> 0x00e8 }
            if (r0 == 0) goto L_0x003a
            java.io.PrintStream r1 = r11.out     // Catch:{ all -> 0x00e8 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x00e8 }
            java.lang.String r2 = "DEBUG: protocolConnect returning false, host="
            r0.<init>(r2)     // Catch:{ all -> 0x00e8 }
            java.lang.StringBuilder r0 = r0.append(r12)     // Catch:{ all -> 0x00e8 }
            java.lang.String r2 = ", user="
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ all -> 0x00e8 }
            java.lang.StringBuilder r0 = r0.append(r14)     // Catch:{ all -> 0x00e8 }
            java.lang.String r2 = ", password="
            java.lang.StringBuilder r2 = r0.append(r2)     // Catch:{ all -> 0x00e8 }
            if (r15 == 0) goto L_0x003d
            java.lang.String r0 = "<non-null>"
        L_0x002f:
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ all -> 0x00e8 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00e8 }
            r1.println(r0)     // Catch:{ all -> 0x00e8 }
        L_0x003a:
            r0 = 0
        L_0x003b:
            monitor-exit(r11)
            return r0
        L_0x003d:
            java.lang.String r0 = "<null>"
            goto L_0x002f
        L_0x0040:
            if (r13 == r3) goto L_0x00c1
            r11.port = r13     // Catch:{ all -> 0x00e8 }
        L_0x0044:
            int r0 = r11.port     // Catch:{ all -> 0x00e8 }
            if (r0 != r3) goto L_0x004c
            int r0 = r11.defaultPort     // Catch:{ all -> 0x00e8 }
            r11.port = r0     // Catch:{ all -> 0x00e8 }
        L_0x004c:
            com.sun.mail.imap.IMAPStore$ConnectionPool r1 = r11.pool     // Catch:{ CommandFailedException -> 0x00ee, ProtocolException -> 0x010b, IOException -> 0x0116 }
            monitor-enter(r1)     // Catch:{ CommandFailedException -> 0x00ee, ProtocolException -> 0x010b, IOException -> 0x0116 }
            com.sun.mail.imap.IMAPStore$ConnectionPool r0 = r11.pool     // Catch:{ all -> 0x00eb }
            java.util.Vector r0 = r0.authenticatedConnections     // Catch:{ all -> 0x00eb }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x00eb }
            monitor-exit(r1)     // Catch:{ all -> 0x00eb }
            if (r0 == 0) goto L_0x00bb
            com.sun.mail.imap.protocol.IMAPProtocol r0 = new com.sun.mail.imap.protocol.IMAPProtocol     // Catch:{ CommandFailedException -> 0x00ee, ProtocolException -> 0x010b, IOException -> 0x0116 }
            java.lang.String r1 = r11.name     // Catch:{ CommandFailedException -> 0x00ee, ProtocolException -> 0x010b, IOException -> 0x0116 }
            int r3 = r11.port     // Catch:{ CommandFailedException -> 0x00ee, ProtocolException -> 0x010b, IOException -> 0x0116 }
            javax.mail.Session r2 = r11.session     // Catch:{ CommandFailedException -> 0x00ee, ProtocolException -> 0x010b, IOException -> 0x0116 }
            boolean r4 = r2.getDebug()     // Catch:{ CommandFailedException -> 0x00ee, ProtocolException -> 0x010b, IOException -> 0x0116 }
            javax.mail.Session r2 = r11.session     // Catch:{ CommandFailedException -> 0x00ee, ProtocolException -> 0x010b, IOException -> 0x0116 }
            java.io.PrintStream r5 = r2.getDebugOut()     // Catch:{ CommandFailedException -> 0x00ee, ProtocolException -> 0x010b, IOException -> 0x0116 }
            javax.mail.Session r2 = r11.session     // Catch:{ CommandFailedException -> 0x00ee, ProtocolException -> 0x010b, IOException -> 0x0116 }
            java.util.Properties r6 = r2.getProperties()     // Catch:{ CommandFailedException -> 0x00ee, ProtocolException -> 0x010b, IOException -> 0x0116 }
            boolean r7 = r11.isSSL     // Catch:{ CommandFailedException -> 0x00ee, ProtocolException -> 0x010b, IOException -> 0x0116 }
            r2 = r12
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ CommandFailedException -> 0x00ee, ProtocolException -> 0x010b, IOException -> 0x0116 }
            boolean r1 = r11.debug     // Catch:{ CommandFailedException -> 0x0106, ProtocolException -> 0x010b, IOException -> 0x0116 }
            if (r1 == 0) goto L_0x00a2
            java.io.PrintStream r1 = r11.out     // Catch:{ CommandFailedException -> 0x0106, ProtocolException -> 0x010b, IOException -> 0x0116 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ CommandFailedException -> 0x0106, ProtocolException -> 0x010b, IOException -> 0x0116 }
            java.lang.String r3 = "DEBUG: protocolConnect login, host="
            r2.<init>(r3)     // Catch:{ CommandFailedException -> 0x0106, ProtocolException -> 0x010b, IOException -> 0x0116 }
            java.lang.StringBuilder r2 = r2.append(r12)     // Catch:{ CommandFailedException -> 0x0106, ProtocolException -> 0x010b, IOException -> 0x0116 }
            java.lang.String r3 = ", user="
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ CommandFailedException -> 0x0106, ProtocolException -> 0x010b, IOException -> 0x0116 }
            java.lang.StringBuilder r2 = r2.append(r14)     // Catch:{ CommandFailedException -> 0x0106, ProtocolException -> 0x010b, IOException -> 0x0116 }
            java.lang.String r3 = ", password=<non-null>"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ CommandFailedException -> 0x0106, ProtocolException -> 0x010b, IOException -> 0x0116 }
            java.lang.String r2 = r2.toString()     // Catch:{ CommandFailedException -> 0x0106, ProtocolException -> 0x010b, IOException -> 0x0116 }
            r1.println(r2)     // Catch:{ CommandFailedException -> 0x0106, ProtocolException -> 0x010b, IOException -> 0x0116 }
        L_0x00a2:
            r11.login(r0, r14, r15)     // Catch:{ CommandFailedException -> 0x0106, ProtocolException -> 0x010b, IOException -> 0x0116 }
            r0.addResponseHandler(r11)     // Catch:{ CommandFailedException -> 0x0106, ProtocolException -> 0x010b, IOException -> 0x0116 }
            r11.host = r12     // Catch:{ CommandFailedException -> 0x0106, ProtocolException -> 0x010b, IOException -> 0x0116 }
            r11.user = r14     // Catch:{ CommandFailedException -> 0x0106, ProtocolException -> 0x010b, IOException -> 0x0116 }
            r11.password = r15     // Catch:{ CommandFailedException -> 0x0106, ProtocolException -> 0x010b, IOException -> 0x0116 }
            com.sun.mail.imap.IMAPStore$ConnectionPool r2 = r11.pool     // Catch:{ CommandFailedException -> 0x0106, ProtocolException -> 0x010b, IOException -> 0x0116 }
            monitor-enter(r2)     // Catch:{ CommandFailedException -> 0x0106, ProtocolException -> 0x010b, IOException -> 0x0116 }
            com.sun.mail.imap.IMAPStore$ConnectionPool r1 = r11.pool     // Catch:{ all -> 0x0103 }
            java.util.Vector r1 = r1.authenticatedConnections     // Catch:{ all -> 0x0103 }
            r1.addElement(r0)     // Catch:{ all -> 0x0103 }
            monitor-exit(r2)     // Catch:{ all -> 0x0103 }
        L_0x00bb:
            r0 = 1
            r11.connected = r0     // Catch:{ all -> 0x00e8 }
            r0 = r8
            goto L_0x003b
        L_0x00c1:
            javax.mail.Session r0 = r11.session     // Catch:{ all -> 0x00e8 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00e8 }
            java.lang.String r2 = "mail."
            r1.<init>(r2)     // Catch:{ all -> 0x00e8 }
            java.lang.String r2 = r11.name     // Catch:{ all -> 0x00e8 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00e8 }
            java.lang.String r2 = ".port"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00e8 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00e8 }
            java.lang.String r0 = r0.getProperty(r1)     // Catch:{ all -> 0x00e8 }
            if (r0 == 0) goto L_0x0044
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ all -> 0x00e8 }
            r11.port = r0     // Catch:{ all -> 0x00e8 }
            goto L_0x0044
        L_0x00e8:
            r0 = move-exception
            monitor-exit(r11)
            throw r0
        L_0x00eb:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00eb }
            throw r0     // Catch:{ CommandFailedException -> 0x00ee, ProtocolException -> 0x010b, IOException -> 0x0116 }
        L_0x00ee:
            r0 = move-exception
            r1 = r9
        L_0x00f0:
            if (r1 == 0) goto L_0x00f5
            r1.disconnect()     // Catch:{ all -> 0x00e8 }
        L_0x00f5:
            javax.mail.AuthenticationFailedException r1 = new javax.mail.AuthenticationFailedException     // Catch:{ all -> 0x00e8 }
            com.sun.mail.iap.Response r0 = r0.getResponse()     // Catch:{ all -> 0x00e8 }
            java.lang.String r0 = r0.getRest()     // Catch:{ all -> 0x00e8 }
            r1.<init>(r0)     // Catch:{ all -> 0x00e8 }
            throw r1     // Catch:{ all -> 0x00e8 }
        L_0x0103:
            r1 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0103 }
            throw r1     // Catch:{ CommandFailedException -> 0x0106, ProtocolException -> 0x010b, IOException -> 0x0116 }
        L_0x0106:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x00f0
        L_0x010b:
            r0 = move-exception
            javax.mail.MessagingException r1 = new javax.mail.MessagingException     // Catch:{ all -> 0x00e8 }
            java.lang.String r2 = r0.getMessage()     // Catch:{ all -> 0x00e8 }
            r1.<init>(r2, r0)     // Catch:{ all -> 0x00e8 }
            throw r1     // Catch:{ all -> 0x00e8 }
        L_0x0116:
            r0 = move-exception
            javax.mail.MessagingException r1 = new javax.mail.MessagingException     // Catch:{ all -> 0x00e8 }
            java.lang.String r2 = r0.getMessage()     // Catch:{ all -> 0x00e8 }
            r1.<init>(r2, r0)     // Catch:{ all -> 0x00e8 }
            throw r1     // Catch:{ all -> 0x00e8 }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sun.mail.imap.IMAPStore.protocolConnect(java.lang.String, int, java.lang.String, java.lang.String):boolean");
    }

    private void login(IMAPProtocol iMAPProtocol, String str, String str2) throws ProtocolException {
        String str3;
        if (this.enableStartTLS && iMAPProtocol.hasCapability("STARTTLS")) {
            iMAPProtocol.startTLS();
            iMAPProtocol.capability();
        }
        if (!iMAPProtocol.isAuthenticated()) {
            iMAPProtocol.getCapabilities().put("__PRELOGIN__", "");
            if (this.authorizationID != null) {
                str3 = this.authorizationID;
            } else {
                str3 = this.proxyAuthUser != null ? this.proxyAuthUser : str;
            }
            if (this.enableSASL) {
                iMAPProtocol.sasllogin(this.saslMechanisms, this.saslRealm, str3, str, str2);
            }
            if (!iMAPProtocol.isAuthenticated()) {
                if (iMAPProtocol.hasCapability("AUTH=PLAIN") && !this.disableAuthPlain) {
                    iMAPProtocol.authplain(str3, str, str2);
                } else if ((iMAPProtocol.hasCapability("AUTH-LOGIN") || iMAPProtocol.hasCapability("AUTH=LOGIN")) && !this.disableAuthLogin) {
                    iMAPProtocol.authlogin(str, str2);
                } else if (!iMAPProtocol.hasCapability("LOGINDISABLED")) {
                    iMAPProtocol.login(str, str2);
                } else {
                    throw new ProtocolException("No login methods supported!");
                }
            }
            if (this.proxyAuthUser != null) {
                iMAPProtocol.proxyauth(this.proxyAuthUser);
            }
            if (iMAPProtocol.hasCapability("__PRELOGIN__")) {
                try {
                    iMAPProtocol.capability();
                } catch (ConnectionException e) {
                    throw e;
                } catch (ProtocolException e2) {
                }
            }
        }
    }

    public synchronized void setUsername(String str) {
        this.user = str;
    }

    public synchronized void setPassword(String str) {
        this.password = str;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x008a A[SYNTHETIC, Splitter:B:31:0x008a] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x009b A[SYNTHETIC, Splitter:B:41:0x009b] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00f6  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.sun.mail.imap.protocol.IMAPProtocol getProtocol(com.sun.mail.imap.IMAPFolder r12) throws javax.mail.MessagingException {
        /*
            r11 = this;
            r9 = 0
            r8 = r9
        L_0x0002:
            if (r8 == 0) goto L_0x0005
            return r8
        L_0x0005:
            com.sun.mail.imap.IMAPStore$ConnectionPool r10 = r11.pool
            monitor-enter(r10)
            com.sun.mail.imap.IMAPStore$ConnectionPool r0 = r11.pool     // Catch:{ all -> 0x0092 }
            java.util.Vector r0 = r0.authenticatedConnections     // Catch:{ all -> 0x0092 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0092 }
            if (r0 != 0) goto L_0x0031
            com.sun.mail.imap.IMAPStore$ConnectionPool r0 = r11.pool     // Catch:{ all -> 0x0092 }
            java.util.Vector r0 = r0.authenticatedConnections     // Catch:{ all -> 0x0092 }
            int r0 = r0.size()     // Catch:{ all -> 0x0092 }
            r1 = 1
            if (r0 != r1) goto L_0x00a0
            com.sun.mail.imap.IMAPStore$ConnectionPool r0 = r11.pool     // Catch:{ all -> 0x0092 }
            boolean r0 = r0.separateStoreConnection     // Catch:{ all -> 0x0092 }
            if (r0 != 0) goto L_0x0031
            com.sun.mail.imap.IMAPStore$ConnectionPool r0 = r11.pool     // Catch:{ all -> 0x0092 }
            boolean r0 = r0.storeConnectionInUse     // Catch:{ all -> 0x0092 }
            if (r0 == 0) goto L_0x00a0
        L_0x0031:
            boolean r0 = r11.debug     // Catch:{ all -> 0x0092 }
            if (r0 == 0) goto L_0x003c
            java.io.PrintStream r0 = r11.out     // Catch:{ all -> 0x0092 }
            java.lang.String r1 = "DEBUG: no connections in the pool, creating a new one"
            r0.println(r1)     // Catch:{ all -> 0x0092 }
        L_0x003c:
            boolean r0 = r11.forcePasswordRefresh     // Catch:{ Exception -> 0x0124 }
            if (r0 == 0) goto L_0x0061
            java.lang.String r0 = r11.host     // Catch:{ UnknownHostException -> 0x0095 }
            java.net.InetAddress r1 = java.net.InetAddress.getByName(r0)     // Catch:{ UnknownHostException -> 0x0095 }
        L_0x0046:
            javax.mail.Session r0 = r11.session     // Catch:{ Exception -> 0x0124 }
            int r2 = r11.port     // Catch:{ Exception -> 0x0124 }
            java.lang.String r3 = r11.name     // Catch:{ Exception -> 0x0124 }
            r4 = 0
            java.lang.String r5 = r11.user     // Catch:{ Exception -> 0x0124 }
            javax.mail.PasswordAuthentication r0 = r0.requestPasswordAuthentication(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0124 }
            if (r0 == 0) goto L_0x0061
            java.lang.String r1 = r0.getUserName()     // Catch:{ Exception -> 0x0124 }
            r11.user = r1     // Catch:{ Exception -> 0x0124 }
            java.lang.String r0 = r0.getPassword()     // Catch:{ Exception -> 0x0124 }
            r11.password = r0     // Catch:{ Exception -> 0x0124 }
        L_0x0061:
            com.sun.mail.imap.protocol.IMAPProtocol r0 = new com.sun.mail.imap.protocol.IMAPProtocol     // Catch:{ Exception -> 0x0124 }
            java.lang.String r1 = r11.name     // Catch:{ Exception -> 0x0124 }
            java.lang.String r2 = r11.host     // Catch:{ Exception -> 0x0124 }
            int r3 = r11.port     // Catch:{ Exception -> 0x0124 }
            javax.mail.Session r4 = r11.session     // Catch:{ Exception -> 0x0124 }
            boolean r4 = r4.getDebug()     // Catch:{ Exception -> 0x0124 }
            javax.mail.Session r5 = r11.session     // Catch:{ Exception -> 0x0124 }
            java.io.PrintStream r5 = r5.getDebugOut()     // Catch:{ Exception -> 0x0124 }
            javax.mail.Session r6 = r11.session     // Catch:{ Exception -> 0x0124 }
            java.util.Properties r6 = r6.getProperties()     // Catch:{ Exception -> 0x0124 }
            boolean r7 = r11.isSSL     // Catch:{ Exception -> 0x0124 }
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x0124 }
            java.lang.String r1 = r11.user     // Catch:{ Exception -> 0x0098 }
            java.lang.String r2 = r11.password     // Catch:{ Exception -> 0x0098 }
            r11.login(r0, r1, r2)     // Catch:{ Exception -> 0x0098 }
            r8 = r0
        L_0x0088:
            if (r8 != 0) goto L_0x00f1
            javax.mail.MessagingException r0 = new javax.mail.MessagingException     // Catch:{ all -> 0x0092 }
            java.lang.String r1 = "connection failure"
            r0.<init>(r1)     // Catch:{ all -> 0x0092 }
            throw r0     // Catch:{ all -> 0x0092 }
        L_0x0092:
            r0 = move-exception
            monitor-exit(r10)     // Catch:{ all -> 0x0092 }
            throw r0
        L_0x0095:
            r0 = move-exception
            r1 = r9
            goto L_0x0046
        L_0x0098:
            r1 = move-exception
        L_0x0099:
            if (r0 == 0) goto L_0x009e
            r0.disconnect()     // Catch:{ Exception -> 0x011f }
        L_0x009e:
            r8 = r9
            goto L_0x0088
        L_0x00a0:
            boolean r0 = r11.debug     // Catch:{ all -> 0x0092 }
            if (r0 == 0) goto L_0x00c2
            java.io.PrintStream r0 = r11.out     // Catch:{ all -> 0x0092 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0092 }
            java.lang.String r2 = "DEBUG: connection available -- size: "
            r1.<init>(r2)     // Catch:{ all -> 0x0092 }
            com.sun.mail.imap.IMAPStore$ConnectionPool r2 = r11.pool     // Catch:{ all -> 0x0092 }
            java.util.Vector r2 = r2.authenticatedConnections     // Catch:{ all -> 0x0092 }
            int r2 = r2.size()     // Catch:{ all -> 0x0092 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0092 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0092 }
            r0.println(r1)     // Catch:{ all -> 0x0092 }
        L_0x00c2:
            com.sun.mail.imap.IMAPStore$ConnectionPool r0 = r11.pool     // Catch:{ all -> 0x0092 }
            java.util.Vector r0 = r0.authenticatedConnections     // Catch:{ all -> 0x0092 }
            java.lang.Object r0 = r0.lastElement()     // Catch:{ all -> 0x0092 }
            com.sun.mail.imap.protocol.IMAPProtocol r0 = (com.sun.mail.imap.protocol.IMAPProtocol) r0     // Catch:{ all -> 0x0092 }
            com.sun.mail.imap.IMAPStore$ConnectionPool r1 = r11.pool     // Catch:{ all -> 0x0092 }
            java.util.Vector r1 = r1.authenticatedConnections     // Catch:{ all -> 0x0092 }
            r1.removeElement(r0)     // Catch:{ all -> 0x0092 }
            long r1 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0092 }
            long r3 = r0.getTimestamp()     // Catch:{ all -> 0x0092 }
            long r1 = r1 - r3
            com.sun.mail.imap.IMAPStore$ConnectionPool r3 = r11.pool     // Catch:{ all -> 0x0092 }
            long r3 = r3.serverTimeoutInterval     // Catch:{ all -> 0x0092 }
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 <= 0) goto L_0x00ed
            r0.noop()     // Catch:{ ProtocolException -> 0x0114 }
        L_0x00ed:
            r0.removeResponseHandler(r11)     // Catch:{ all -> 0x0092 }
            r8 = r0
        L_0x00f1:
            r11.timeoutConnections()     // Catch:{ all -> 0x0092 }
            if (r12 == 0) goto L_0x0111
            com.sun.mail.imap.IMAPStore$ConnectionPool r0 = r11.pool     // Catch:{ all -> 0x0092 }
            java.util.Vector r0 = r0.folders     // Catch:{ all -> 0x0092 }
            if (r0 != 0) goto L_0x0108
            com.sun.mail.imap.IMAPStore$ConnectionPool r0 = r11.pool     // Catch:{ all -> 0x0092 }
            java.util.Vector r1 = new java.util.Vector     // Catch:{ all -> 0x0092 }
            r1.<init>()     // Catch:{ all -> 0x0092 }
            r0.folders = r1     // Catch:{ all -> 0x0092 }
        L_0x0108:
            com.sun.mail.imap.IMAPStore$ConnectionPool r0 = r11.pool     // Catch:{ all -> 0x0092 }
            java.util.Vector r0 = r0.folders     // Catch:{ all -> 0x0092 }
            r0.addElement(r12)     // Catch:{ all -> 0x0092 }
        L_0x0111:
            monitor-exit(r10)     // Catch:{ all -> 0x0092 }
            goto L_0x0002
        L_0x0114:
            r1 = move-exception
            r0.removeResponseHandler(r11)     // Catch:{ all -> 0x0122 }
            r0.disconnect()     // Catch:{ all -> 0x0122 }
        L_0x011b:
            monitor-exit(r10)     // Catch:{ all -> 0x0092 }
            r8 = r9
            goto L_0x0002
        L_0x011f:
            r0 = move-exception
            goto L_0x009e
        L_0x0122:
            r0 = move-exception
            goto L_0x011b
        L_0x0124:
            r0 = move-exception
            r0 = r8
            goto L_0x0099
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sun.mail.imap.IMAPStore.getProtocol(com.sun.mail.imap.IMAPFolder):com.sun.mail.imap.protocol.IMAPProtocol");
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x004f A[SYNTHETIC, Splitter:B:17:0x004f] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x005d A[SYNTHETIC, Splitter:B:25:0x005d] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0062 A[SYNTHETIC, Splitter:B:28:0x0062] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.sun.mail.imap.protocol.IMAPProtocol getStoreProtocol() throws com.sun.mail.iap.ProtocolException {
        /*
            r11 = this;
            r9 = 0
            r8 = r9
        L_0x0002:
            if (r8 == 0) goto L_0x0005
            return r8
        L_0x0005:
            com.sun.mail.imap.IMAPStore$ConnectionPool r10 = r11.pool
            monitor-enter(r10)
            r11.waitIfIdle()     // Catch:{ all -> 0x0057 }
            com.sun.mail.imap.IMAPStore$ConnectionPool r0 = r11.pool     // Catch:{ all -> 0x0057 }
            java.util.Vector r0 = r0.authenticatedConnections     // Catch:{ all -> 0x0057 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0057 }
            if (r0 == 0) goto L_0x0081
            com.sun.mail.imap.IMAPStore$ConnectionPool r0 = r11.pool     // Catch:{ all -> 0x0057 }
            boolean r0 = r0.debug     // Catch:{ all -> 0x0057 }
            if (r0 == 0) goto L_0x0026
            java.io.PrintStream r0 = r11.out     // Catch:{ all -> 0x0057 }
            java.lang.String r1 = "DEBUG: getStoreProtocol() - no connections in the pool, creating a new one"
            r0.println(r1)     // Catch:{ all -> 0x0057 }
        L_0x0026:
            com.sun.mail.imap.protocol.IMAPProtocol r0 = new com.sun.mail.imap.protocol.IMAPProtocol     // Catch:{ Exception -> 0x00d0 }
            java.lang.String r1 = r11.name     // Catch:{ Exception -> 0x00d0 }
            java.lang.String r2 = r11.host     // Catch:{ Exception -> 0x00d0 }
            int r3 = r11.port     // Catch:{ Exception -> 0x00d0 }
            javax.mail.Session r4 = r11.session     // Catch:{ Exception -> 0x00d0 }
            boolean r4 = r4.getDebug()     // Catch:{ Exception -> 0x00d0 }
            javax.mail.Session r5 = r11.session     // Catch:{ Exception -> 0x00d0 }
            java.io.PrintStream r5 = r5.getDebugOut()     // Catch:{ Exception -> 0x00d0 }
            javax.mail.Session r6 = r11.session     // Catch:{ Exception -> 0x00d0 }
            java.util.Properties r6 = r6.getProperties()     // Catch:{ Exception -> 0x00d0 }
            boolean r7 = r11.isSSL     // Catch:{ Exception -> 0x00d0 }
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x00d0 }
            java.lang.String r1 = r11.user     // Catch:{ Exception -> 0x005a }
            java.lang.String r2 = r11.password     // Catch:{ Exception -> 0x005a }
            r11.login(r0, r1, r2)     // Catch:{ Exception -> 0x005a }
            r8 = r0
        L_0x004d:
            if (r8 != 0) goto L_0x0062
            com.sun.mail.iap.ConnectionException r0 = new com.sun.mail.iap.ConnectionException     // Catch:{ all -> 0x0057 }
            java.lang.String r1 = "failed to create new store connection"
            r0.<init>(r1)     // Catch:{ all -> 0x0057 }
            throw r0     // Catch:{ all -> 0x0057 }
        L_0x0057:
            r0 = move-exception
            monitor-exit(r10)     // Catch:{ all -> 0x0057 }
            throw r0
        L_0x005a:
            r1 = move-exception
        L_0x005b:
            if (r0 == 0) goto L_0x0060
            r0.logout()     // Catch:{ Exception -> 0x00ce }
        L_0x0060:
            r8 = r9
            goto L_0x004d
        L_0x0062:
            r8.addResponseHandler(r11)     // Catch:{ all -> 0x0057 }
            com.sun.mail.imap.IMAPStore$ConnectionPool r0 = r11.pool     // Catch:{ all -> 0x0057 }
            java.util.Vector r0 = r0.authenticatedConnections     // Catch:{ all -> 0x0057 }
            r0.addElement(r8)     // Catch:{ all -> 0x0057 }
        L_0x006e:
            com.sun.mail.imap.IMAPStore$ConnectionPool r0 = r11.pool     // Catch:{ all -> 0x0057 }
            boolean r0 = r0.storeConnectionInUse     // Catch:{ all -> 0x0057 }
            if (r0 == 0) goto L_0x00b8
            com.sun.mail.imap.IMAPStore$ConnectionPool r0 = r11.pool     // Catch:{ InterruptedException -> 0x00b5 }
            r0.wait()     // Catch:{ InterruptedException -> 0x00b5 }
            r8 = r9
        L_0x007c:
            r11.timeoutConnections()     // Catch:{ all -> 0x0057 }
            monitor-exit(r10)     // Catch:{ all -> 0x0057 }
            goto L_0x0002
        L_0x0081:
            com.sun.mail.imap.IMAPStore$ConnectionPool r0 = r11.pool     // Catch:{ all -> 0x0057 }
            boolean r0 = r0.debug     // Catch:{ all -> 0x0057 }
            if (r0 == 0) goto L_0x00a7
            java.io.PrintStream r0 = r11.out     // Catch:{ all -> 0x0057 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0057 }
            java.lang.String r2 = "DEBUG: getStoreProtocol() - connection available -- size: "
            r1.<init>(r2)     // Catch:{ all -> 0x0057 }
            com.sun.mail.imap.IMAPStore$ConnectionPool r2 = r11.pool     // Catch:{ all -> 0x0057 }
            java.util.Vector r2 = r2.authenticatedConnections     // Catch:{ all -> 0x0057 }
            int r2 = r2.size()     // Catch:{ all -> 0x0057 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0057 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0057 }
            r0.println(r1)     // Catch:{ all -> 0x0057 }
        L_0x00a7:
            com.sun.mail.imap.IMAPStore$ConnectionPool r0 = r11.pool     // Catch:{ all -> 0x0057 }
            java.util.Vector r0 = r0.authenticatedConnections     // Catch:{ all -> 0x0057 }
            java.lang.Object r0 = r0.firstElement()     // Catch:{ all -> 0x0057 }
            com.sun.mail.imap.protocol.IMAPProtocol r0 = (com.sun.mail.imap.protocol.IMAPProtocol) r0     // Catch:{ all -> 0x0057 }
            r8 = r0
            goto L_0x006e
        L_0x00b5:
            r0 = move-exception
            r8 = r9
            goto L_0x007c
        L_0x00b8:
            com.sun.mail.imap.IMAPStore$ConnectionPool r0 = r11.pool     // Catch:{ all -> 0x0057 }
            r1 = 1
            r0.storeConnectionInUse = r1     // Catch:{ all -> 0x0057 }
            com.sun.mail.imap.IMAPStore$ConnectionPool r0 = r11.pool     // Catch:{ all -> 0x0057 }
            boolean r0 = r0.debug     // Catch:{ all -> 0x0057 }
            if (r0 == 0) goto L_0x007c
            java.io.PrintStream r0 = r11.out     // Catch:{ all -> 0x0057 }
            java.lang.String r1 = "DEBUG: getStoreProtocol() -- storeConnectionInUse"
            r0.println(r1)     // Catch:{ all -> 0x0057 }
            goto L_0x007c
        L_0x00ce:
            r0 = move-exception
            goto L_0x0060
        L_0x00d0:
            r0 = move-exception
            r0 = r8
            goto L_0x005b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sun.mail.imap.IMAPStore.getStoreProtocol():com.sun.mail.imap.protocol.IMAPProtocol");
    }

    /* access modifiers changed from: package-private */
    public boolean allowReadOnlySelect() {
        String property = this.session.getProperty("mail." + this.name + ".allowreadonlyselect");
        if (property == null || !property.equalsIgnoreCase("true")) {
            return $assertionsDisabled;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean hasSeparateStoreConnection() {
        return this.pool.separateStoreConnection;
    }

    /* access modifiers changed from: package-private */
    public boolean getConnectionPoolDebug() {
        return this.pool.debug;
    }

    /* access modifiers changed from: package-private */
    public boolean isConnectionPoolFull() {
        boolean z;
        synchronized (this.pool) {
            if (this.pool.debug) {
                this.out.println("DEBUG: current size: " + this.pool.authenticatedConnections.size() + "   pool size: " + this.pool.poolSize);
            }
            z = this.pool.authenticatedConnections.size() >= this.pool.poolSize ? true : $assertionsDisabled;
        }
        return z;
    }

    /* access modifiers changed from: package-private */
    public void releaseProtocol(IMAPFolder iMAPFolder, IMAPProtocol iMAPProtocol) {
        synchronized (this.pool) {
            if (iMAPProtocol != null) {
                if (!isConnectionPoolFull()) {
                    iMAPProtocol.addResponseHandler(this);
                    this.pool.authenticatedConnections.addElement(iMAPProtocol);
                    if (this.debug) {
                        this.out.println("DEBUG: added an Authenticated connection -- size: " + this.pool.authenticatedConnections.size());
                    }
                } else {
                    if (this.debug) {
                        this.out.println("DEBUG: pool is full, not adding an Authenticated connection");
                    }
                    try {
                        iMAPProtocol.logout();
                    } catch (ProtocolException e) {
                    }
                }
            }
            if (this.pool.folders != null) {
                this.pool.folders.removeElement(iMAPFolder);
            }
            timeoutConnections();
        }
    }

    /* access modifiers changed from: package-private */
    public void releaseStoreProtocol(IMAPProtocol iMAPProtocol) {
        if (iMAPProtocol != null) {
            synchronized (this.pool) {
                this.pool.storeConnectionInUse = $assertionsDisabled;
                this.pool.notifyAll();
                if (this.pool.debug) {
                    this.out.println("DEBUG: releaseStoreProtocol()");
                }
                timeoutConnections();
            }
        }
    }

    private void emptyConnectionPool(boolean z) {
        synchronized (this.pool) {
            for (int size = this.pool.authenticatedConnections.size() - 1; size >= 0; size--) {
                try {
                    IMAPProtocol iMAPProtocol = (IMAPProtocol) this.pool.authenticatedConnections.elementAt(size);
                    iMAPProtocol.removeResponseHandler(this);
                    if (z) {
                        iMAPProtocol.disconnect();
                    } else {
                        iMAPProtocol.logout();
                    }
                } catch (ProtocolException e) {
                }
            }
            this.pool.authenticatedConnections.removeAllElements();
        }
        if (this.pool.debug) {
            this.out.println("DEBUG: removed all authenticated connections");
        }
    }

    private void timeoutConnections() {
        synchronized (this.pool) {
            if (System.currentTimeMillis() - this.pool.lastTimePruned > this.pool.pruningInterval && this.pool.authenticatedConnections.size() > 1) {
                if (this.pool.debug) {
                    this.out.println("DEBUG: checking for connections to prune: " + (System.currentTimeMillis() - this.pool.lastTimePruned));
                    this.out.println("DEBUG: clientTimeoutInterval: " + this.pool.clientTimeoutInterval);
                }
                for (int size = this.pool.authenticatedConnections.size() - 1; size > 0; size--) {
                    IMAPProtocol iMAPProtocol = (IMAPProtocol) this.pool.authenticatedConnections.elementAt(size);
                    if (this.pool.debug) {
                        this.out.println("DEBUG: protocol last used: " + (System.currentTimeMillis() - iMAPProtocol.getTimestamp()));
                    }
                    if (System.currentTimeMillis() - iMAPProtocol.getTimestamp() > this.pool.clientTimeoutInterval) {
                        if (this.pool.debug) {
                            this.out.println("DEBUG: authenticated connection timed out");
                            this.out.println("DEBUG: logging out the connection");
                        }
                        iMAPProtocol.removeResponseHandler(this);
                        this.pool.authenticatedConnections.removeElementAt(size);
                        try {
                            iMAPProtocol.logout();
                        } catch (ProtocolException e) {
                        }
                    }
                }
                this.pool.lastTimePruned = System.currentTimeMillis();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public int getFetchBlockSize() {
        return this.blksize;
    }

    /* access modifiers changed from: package-private */
    public Session getSession() {
        return this.session;
    }

    /* access modifiers changed from: package-private */
    public int getStatusCacheTimeout() {
        return this.statusCacheTimeout;
    }

    /* access modifiers changed from: package-private */
    public int getAppendBufferSize() {
        return this.appendBufferSize;
    }

    /* access modifiers changed from: package-private */
    public int getMinIdleTime() {
        return this.minIdleTime;
    }

    public synchronized boolean hasCapability(String str) throws MessagingException {
        boolean hasCapability;
        IMAPProtocol iMAPProtocol = null;
        try {
            iMAPProtocol = getStoreProtocol();
            hasCapability = iMAPProtocol.hasCapability(str);
            releaseStoreProtocol(iMAPProtocol);
        } catch (ProtocolException e) {
            if (iMAPProtocol == null) {
                cleanup();
            }
            throw new MessagingException(e.getMessage(), e);
        } catch (Throwable th) {
            releaseStoreProtocol(iMAPProtocol);
            throw th;
        }
        return hasCapability;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x001d, code lost:
        if (0 == 0) goto L_0x001f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        cleanup();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        releaseStoreProtocol(null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0029, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x002a, code lost:
        r2 = r1;
        r1 = null;
        r0 = r2;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x001c A[ExcHandler: ProtocolException (e com.sun.mail.iap.ProtocolException), Splitter:B:9:0x000d] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:20:0x0022=Splitter:B:20:0x0022, B:27:0x002d=Splitter:B:27:0x002d} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean isConnected() {
        /*
            r3 = this;
            r0 = 0
            monitor-enter(r3)
            boolean r1 = r3.connected     // Catch:{ all -> 0x0026 }
            if (r1 != 0) goto L_0x000c
            r1 = 0
            super.setConnected(r1)     // Catch:{ all -> 0x0026 }
        L_0x000a:
            monitor-exit(r3)
            return r0
        L_0x000c:
            r0 = 0
            com.sun.mail.imap.protocol.IMAPProtocol r0 = r3.getStoreProtocol()     // Catch:{ ProtocolException -> 0x001c, all -> 0x0029 }
            r0.noop()     // Catch:{ ProtocolException -> 0x001c }
            r3.releaseStoreProtocol(r0)     // Catch:{ all -> 0x0026 }
        L_0x0017:
            boolean r0 = super.isConnected()     // Catch:{ all -> 0x0026 }
            goto L_0x000a
        L_0x001c:
            r1 = move-exception
            if (r0 != 0) goto L_0x0022
            r3.cleanup()     // Catch:{ all -> 0x0031 }
        L_0x0022:
            r3.releaseStoreProtocol(r0)     // Catch:{ all -> 0x0026 }
            goto L_0x0017
        L_0x0026:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        L_0x0029:
            r1 = move-exception
            r2 = r1
            r1 = r0
            r0 = r2
        L_0x002d:
            r3.releaseStoreProtocol(r1)     // Catch:{ all -> 0x0026 }
            throw r0     // Catch:{ all -> 0x0026 }
        L_0x0031:
            r1 = move-exception
            r2 = r1
            r1 = r0
            r0 = r2
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sun.mail.imap.IMAPStore.isConnected():boolean");
    }

    public synchronized void close() throws MessagingException {
        boolean isEmpty;
        if (super.isConnected()) {
            IMAPProtocol iMAPProtocol = null;
            try {
                synchronized (this.pool) {
                    isEmpty = this.pool.authenticatedConnections.isEmpty();
                }
                if (isEmpty) {
                    if (this.pool.debug) {
                        this.out.println("DEBUG: close() - no connections ");
                    }
                    cleanup();
                    releaseStoreProtocol(null);
                } else {
                    iMAPProtocol = getStoreProtocol();
                    synchronized (this.pool) {
                        this.pool.authenticatedConnections.removeElement(iMAPProtocol);
                    }
                    iMAPProtocol.logout();
                    releaseStoreProtocol(iMAPProtocol);
                }
            } catch (ProtocolException e) {
                try {
                    cleanup();
                    throw new MessagingException(e.getMessage(), e);
                } catch (Throwable th) {
                    releaseStoreProtocol(iMAPProtocol);
                    throw th;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        super.finalize();
        close();
    }

    private void cleanup() {
        cleanup($assertionsDisabled);
    }

    private void cleanup(boolean z) {
        Vector vector;
        boolean z2;
        Vector vector2 = null;
        if (this.debug) {
            this.out.println("DEBUG: IMAPStore cleanup, force " + z);
        }
        while (true) {
            synchronized (this.pool) {
                if (this.pool.folders != null) {
                    vector = this.pool.folders;
                    this.pool.folders = null;
                    z2 = false;
                } else {
                    vector = vector2;
                    z2 = true;
                }
            }
            if (z2) {
                synchronized (this.pool) {
                    emptyConnectionPool(z);
                }
                this.connected = $assertionsDisabled;
                notifyConnectionListeners(3);
                if (this.debug) {
                    this.out.println("DEBUG: IMAPStore cleanup done");
                    return;
                }
                return;
            }
            int size = vector.size();
            for (int i = 0; i < size; i++) {
                IMAPFolder iMAPFolder = (IMAPFolder) vector.elementAt(i);
                if (z) {
                    try {
                        if (this.debug) {
                            this.out.println("DEBUG: force folder to close");
                        }
                        iMAPFolder.forceClose();
                    } catch (IllegalStateException | MessagingException e) {
                    }
                } else {
                    if (this.debug) {
                        this.out.println("DEBUG: close folder");
                    }
                    iMAPFolder.close($assertionsDisabled);
                }
            }
            vector2 = vector;
        }
        while (true) {
        }
    }

    public synchronized Folder getDefaultFolder() throws MessagingException {
        checkConnected();
        return new DefaultFolder(this);
    }

    public synchronized Folder getFolder(String str) throws MessagingException {
        checkConnected();
        return new IMAPFolder(str, 65535, this);
    }

    public synchronized Folder getFolder(URLName uRLName) throws MessagingException {
        checkConnected();
        return new IMAPFolder(uRLName.getFile(), 65535, this);
    }

    public Folder[] getPersonalNamespaces() throws MessagingException {
        Namespaces namespaces2 = getNamespaces();
        if (namespaces2 == null || namespaces2.personal == null) {
            return super.getPersonalNamespaces();
        }
        return namespaceToFolders(namespaces2.personal, null);
    }

    public Folder[] getUserNamespaces(String str) throws MessagingException {
        Namespaces namespaces2 = getNamespaces();
        if (namespaces2 == null || namespaces2.otherUsers == null) {
            return super.getUserNamespaces(str);
        }
        return namespaceToFolders(namespaces2.otherUsers, str);
    }

    public Folder[] getSharedNamespaces() throws MessagingException {
        Namespaces namespaces2 = getNamespaces();
        if (namespaces2 == null || namespaces2.shared == null) {
            return super.getSharedNamespaces();
        }
        return namespaceToFolders(namespaces2.shared, null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        releaseStoreProtocol(null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0023, code lost:
        if (0 == 0) goto L_0x0025;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0025, code lost:
        cleanup();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x002c, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x002d, code lost:
        r4 = r1;
        r1 = null;
        r0 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0040, code lost:
        cleanup();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0044, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0045, code lost:
        r4 = r1;
        r1 = null;
        r0 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0052, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0053, code lost:
        r4 = r1;
        r1 = null;
        r0 = r4;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x001f A[ExcHandler: BadCommandException (e com.sun.mail.iap.BadCommandException), Splitter:B:4:0x0009] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0040 A[Catch:{ BadCommandException -> 0x001f, ConnectionException -> 0x0061, ProtocolException -> 0x005c, all -> 0x0057 }] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:25:0x0030=Splitter:B:25:0x0030, B:36:0x0048=Splitter:B:36:0x0048} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized com.sun.mail.imap.protocol.Namespaces getNamespaces() throws javax.mail.MessagingException {
        /*
            r5 = this;
            monitor-enter(r5)
            r5.checkConnected()     // Catch:{ all -> 0x0029 }
            r0 = 0
            com.sun.mail.imap.protocol.Namespaces r1 = r5.namespaces     // Catch:{ all -> 0x0029 }
            if (r1 != 0) goto L_0x001b
            com.sun.mail.imap.protocol.IMAPProtocol r0 = r5.getStoreProtocol()     // Catch:{ BadCommandException -> 0x001f, ConnectionException -> 0x002c, ProtocolException -> 0x0044, all -> 0x0052 }
            com.sun.mail.imap.protocol.Namespaces r1 = r0.namespace()     // Catch:{ BadCommandException -> 0x001f, ConnectionException -> 0x0061, ProtocolException -> 0x005c, all -> 0x0057 }
            r5.namespaces = r1     // Catch:{ BadCommandException -> 0x001f, ConnectionException -> 0x0061, ProtocolException -> 0x005c, all -> 0x0057 }
            r5.releaseStoreProtocol(r0)     // Catch:{ all -> 0x0029 }
            if (r0 != 0) goto L_0x001b
            r5.cleanup()     // Catch:{ all -> 0x0029 }
        L_0x001b:
            com.sun.mail.imap.protocol.Namespaces r0 = r5.namespaces     // Catch:{ all -> 0x0029 }
            monitor-exit(r5)
            return r0
        L_0x001f:
            r1 = move-exception
            r5.releaseStoreProtocol(r0)     // Catch:{ all -> 0x0029 }
            if (r0 != 0) goto L_0x001b
            r5.cleanup()     // Catch:{ all -> 0x0029 }
            goto L_0x001b
        L_0x0029:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        L_0x002c:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x0030:
            javax.mail.StoreClosedException r2 = new javax.mail.StoreClosedException     // Catch:{ all -> 0x003a }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x003a }
            r2.<init>(r5, r0)     // Catch:{ all -> 0x003a }
            throw r2     // Catch:{ all -> 0x003a }
        L_0x003a:
            r0 = move-exception
        L_0x003b:
            r5.releaseStoreProtocol(r1)     // Catch:{ all -> 0x0029 }
            if (r1 != 0) goto L_0x0043
            r5.cleanup()     // Catch:{ all -> 0x0029 }
        L_0x0043:
            throw r0     // Catch:{ all -> 0x0029 }
        L_0x0044:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x0048:
            javax.mail.MessagingException r2 = new javax.mail.MessagingException     // Catch:{ all -> 0x003a }
            java.lang.String r3 = r0.getMessage()     // Catch:{ all -> 0x003a }
            r2.<init>(r3, r0)     // Catch:{ all -> 0x003a }
            throw r2     // Catch:{ all -> 0x003a }
        L_0x0052:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x003b
        L_0x0057:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x003b
        L_0x005c:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0048
        L_0x0061:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0030
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sun.mail.imap.IMAPStore.getNamespaces():com.sun.mail.imap.protocol.Namespaces");
    }

    private Folder[] namespaceToFolders(Namespaces.Namespace[] namespaceArr, String str) {
        boolean z;
        Folder[] folderArr = new Folder[namespaceArr.length];
        for (int i = 0; i < folderArr.length; i++) {
            String str2 = namespaceArr[i].prefix;
            if (str == null) {
                int length = str2.length();
                if (length > 0 && str2.charAt(length - 1) == namespaceArr[i].delimiter) {
                    str2 = str2.substring(0, length - 1);
                }
            } else {
                str2 = String.valueOf(str2) + str;
            }
            char c = namespaceArr[i].delimiter;
            if (str == null) {
                z = true;
            } else {
                z = false;
            }
            folderArr[i] = new IMAPFolder(str2, c, this, z);
        }
        return folderArr;
    }

    public synchronized Quota[] getQuota(String str) throws MessagingException {
        Quota[] quotaRoot;
        IMAPProtocol iMAPProtocol = null;
        synchronized (this) {
            checkConnected();
            Quota[] quotaArr = null;
            try {
                iMAPProtocol = getStoreProtocol();
                quotaRoot = iMAPProtocol.getQuotaRoot(str);
                releaseStoreProtocol(iMAPProtocol);
                if (iMAPProtocol == null) {
                    cleanup();
                }
            } catch (BadCommandException e) {
                throw new MessagingException("QUOTA not supported", e);
            } catch (ConnectionException e2) {
                throw new StoreClosedException(this, e2.getMessage());
            } catch (ProtocolException e3) {
                throw new MessagingException(e3.getMessage(), e3);
            } catch (Throwable th) {
                releaseStoreProtocol(iMAPProtocol);
                if (iMAPProtocol == null) {
                    cleanup();
                }
                throw th;
            }
        }
        return quotaRoot;
    }

    public synchronized void setQuota(Quota quota) throws MessagingException {
        checkConnected();
        IMAPProtocol iMAPProtocol = null;
        try {
            iMAPProtocol = getStoreProtocol();
            iMAPProtocol.setQuota(quota);
            releaseStoreProtocol(iMAPProtocol);
            if (iMAPProtocol == null) {
                cleanup();
            }
        } catch (BadCommandException e) {
            throw new MessagingException("QUOTA not supported", e);
        } catch (ConnectionException e2) {
            throw new StoreClosedException(this, e2.getMessage());
        } catch (ProtocolException e3) {
            throw new MessagingException(e3.getMessage(), e3);
        } catch (Throwable th) {
            releaseStoreProtocol(iMAPProtocol);
            if (iMAPProtocol == null) {
                cleanup();
            }
            throw th;
        }
    }

    private void checkConnected() {
        if (!$assertionsDisabled && !Thread.holdsLock(this)) {
            throw new AssertionError();
        } else if (!this.connected) {
            super.setConnected($assertionsDisabled);
            throw new IllegalStateException("Not connected");
        }
    }

    public void handleResponse(Response response) {
        if (response.isOK() || response.isNO() || response.isBAD() || response.isBYE()) {
            handleResponseCode(response);
        }
        if (response.isBYE()) {
            if (this.debug) {
                this.out.println("DEBUG: IMAPStore connection dead");
            }
            if (this.connected) {
                cleanup(response.isSynthetic());
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:137:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:138:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        r0 = r1.readIdleResponse();
        r2 = r5.pool;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x003c, code lost:
        monitor-enter(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x003d, code lost:
        if (r0 == null) goto L_0x0045;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0043, code lost:
        if (r1.processIdleResponse(r0) != false) goto L_0x00ad;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0045, code lost:
        com.sun.mail.imap.IMAPStore.ConnectionPool.access$20(r5.pool, 0);
        r5.pool.notifyAll();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0050, code lost:
        monitor-exit(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        r0 = getMinIdleTime();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0055, code lost:
        if (r0 <= 0) goto L_0x005b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
        java.lang.Thread.sleep((long) r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0077, code lost:
        r2 = r5.pool;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0079, code lost:
        monitor-enter(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:?, code lost:
        com.sun.mail.imap.IMAPStore.ConnectionPool.access$18(r5.pool, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0080, code lost:
        monitor-exit(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0081, code lost:
        releaseStoreProtocol(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0084, code lost:
        if (r1 != null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0086, code lost:
        cleanup();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:?, code lost:
        monitor-exit(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x00b0, code lost:
        if (r5.enableImapEvents == false) goto L_0x0036;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x00b6, code lost:
        if (r0.isUnTagged() == false) goto L_0x0036;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x00b8, code lost:
        notifyStoreListeners(com.sun.mail.imap.IMAPStore.RESPONSE, r0.toString());
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void idle() throws javax.mail.MessagingException {
        /*
            r5 = this;
            r1 = 0
            boolean r0 = com.sun.mail.imap.IMAPStore.$assertionsDisabled
            if (r0 != 0) goto L_0x0013
            com.sun.mail.imap.IMAPStore$ConnectionPool r0 = r5.pool
            boolean r0 = java.lang.Thread.holdsLock(r0)
            if (r0 == 0) goto L_0x0013
            java.lang.AssertionError r0 = new java.lang.AssertionError
            r0.<init>()
            throw r0
        L_0x0013:
            monitor-enter(r5)
            r5.checkConnected()     // Catch:{ all -> 0x006e }
            monitor-exit(r5)     // Catch:{ all -> 0x006e }
            com.sun.mail.imap.IMAPStore$ConnectionPool r2 = r5.pool     // Catch:{ BadCommandException -> 0x0090, ConnectionException -> 0x00c3, ProtocolException -> 0x00d1 }
            monitor-enter(r2)     // Catch:{ BadCommandException -> 0x0090, ConnectionException -> 0x00c3, ProtocolException -> 0x00d1 }
            com.sun.mail.imap.protocol.IMAPProtocol r1 = r5.getStoreProtocol()     // Catch:{ all -> 0x008d }
            com.sun.mail.imap.IMAPStore$ConnectionPool r0 = r5.pool     // Catch:{ all -> 0x008d }
            int r0 = r0.idleState     // Catch:{ all -> 0x008d }
            if (r0 != 0) goto L_0x0071
            r1.idleStart()     // Catch:{ all -> 0x008d }
            com.sun.mail.imap.IMAPStore$ConnectionPool r0 = r5.pool     // Catch:{ all -> 0x008d }
            r3 = 1
            r0.idleState = r3     // Catch:{ all -> 0x008d }
            com.sun.mail.imap.IMAPStore$ConnectionPool r0 = r5.pool     // Catch:{ all -> 0x008d }
            r0.idleProtocol = r1     // Catch:{ all -> 0x008d }
            monitor-exit(r2)     // Catch:{ all -> 0x008d }
        L_0x0036:
            com.sun.mail.iap.Response r0 = r1.readIdleResponse()     // Catch:{ BadCommandException -> 0x0090, ConnectionException -> 0x00c3, ProtocolException -> 0x00d1 }
            com.sun.mail.imap.IMAPStore$ConnectionPool r2 = r5.pool     // Catch:{ BadCommandException -> 0x0090, ConnectionException -> 0x00c3, ProtocolException -> 0x00d1 }
            monitor-enter(r2)     // Catch:{ BadCommandException -> 0x0090, ConnectionException -> 0x00c3, ProtocolException -> 0x00d1 }
            if (r0 == 0) goto L_0x0045
            boolean r3 = r1.processIdleResponse(r0)     // Catch:{ all -> 0x00ce }
            if (r3 != 0) goto L_0x00ad
        L_0x0045:
            com.sun.mail.imap.IMAPStore$ConnectionPool r0 = r5.pool     // Catch:{ all -> 0x00ce }
            r3 = 0
            r0.idleState = r3     // Catch:{ all -> 0x00ce }
            com.sun.mail.imap.IMAPStore$ConnectionPool r0 = r5.pool     // Catch:{ all -> 0x00ce }
            r0.notifyAll()     // Catch:{ all -> 0x00ce }
            monitor-exit(r2)     // Catch:{ all -> 0x00ce }
            int r0 = r5.getMinIdleTime()     // Catch:{ BadCommandException -> 0x0090, ConnectionException -> 0x00c3, ProtocolException -> 0x00d1 }
            if (r0 <= 0) goto L_0x005b
            long r2 = (long) r0
            java.lang.Thread.sleep(r2)     // Catch:{ InterruptedException -> 0x00e2 }
        L_0x005b:
            com.sun.mail.imap.IMAPStore$ConnectionPool r2 = r5.pool
            monitor-enter(r2)
            com.sun.mail.imap.IMAPStore$ConnectionPool r0 = r5.pool     // Catch:{ all -> 0x00df }
            r3 = 0
            r0.idleProtocol = r3     // Catch:{ all -> 0x00df }
            monitor-exit(r2)     // Catch:{ all -> 0x00df }
            r5.releaseStoreProtocol(r1)
            if (r1 != 0) goto L_0x006d
            r5.cleanup()
        L_0x006d:
            return
        L_0x006e:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x006e }
            throw r0
        L_0x0071:
            com.sun.mail.imap.IMAPStore$ConnectionPool r0 = r5.pool     // Catch:{ InterruptedException -> 0x00e5 }
            r0.wait()     // Catch:{ InterruptedException -> 0x00e5 }
        L_0x0076:
            monitor-exit(r2)     // Catch:{ all -> 0x008d }
            com.sun.mail.imap.IMAPStore$ConnectionPool r2 = r5.pool
            monitor-enter(r2)
            com.sun.mail.imap.IMAPStore$ConnectionPool r0 = r5.pool     // Catch:{ all -> 0x008a }
            r3 = 0
            r0.idleProtocol = r3     // Catch:{ all -> 0x008a }
            monitor-exit(r2)     // Catch:{ all -> 0x008a }
            r5.releaseStoreProtocol(r1)
            if (r1 != 0) goto L_0x006d
            r5.cleanup()
            goto L_0x006d
        L_0x008a:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x008a }
            throw r0
        L_0x008d:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x008d }
            throw r0     // Catch:{ BadCommandException -> 0x0090, ConnectionException -> 0x00c3, ProtocolException -> 0x00d1 }
        L_0x0090:
            r0 = move-exception
            javax.mail.MessagingException r2 = new javax.mail.MessagingException     // Catch:{ all -> 0x0099 }
            java.lang.String r3 = "IDLE not supported"
            r2.<init>(r3, r0)     // Catch:{ all -> 0x0099 }
            throw r2     // Catch:{ all -> 0x0099 }
        L_0x0099:
            r0 = move-exception
            com.sun.mail.imap.IMAPStore$ConnectionPool r2 = r5.pool
            monitor-enter(r2)
            com.sun.mail.imap.IMAPStore$ConnectionPool r3 = r5.pool     // Catch:{ all -> 0x00dc }
            r4 = 0
            r3.idleProtocol = r4     // Catch:{ all -> 0x00dc }
            monitor-exit(r2)     // Catch:{ all -> 0x00dc }
            r5.releaseStoreProtocol(r1)
            if (r1 != 0) goto L_0x00ac
            r5.cleanup()
        L_0x00ac:
            throw r0
        L_0x00ad:
            monitor-exit(r2)     // Catch:{ all -> 0x00ce }
            boolean r2 = r5.enableImapEvents     // Catch:{ BadCommandException -> 0x0090, ConnectionException -> 0x00c3, ProtocolException -> 0x00d1 }
            if (r2 == 0) goto L_0x0036
            boolean r2 = r0.isUnTagged()     // Catch:{ BadCommandException -> 0x0090, ConnectionException -> 0x00c3, ProtocolException -> 0x00d1 }
            if (r2 == 0) goto L_0x0036
            r2 = 1000(0x3e8, float:1.401E-42)
            java.lang.String r0 = r0.toString()     // Catch:{ BadCommandException -> 0x0090, ConnectionException -> 0x00c3, ProtocolException -> 0x00d1 }
            r5.notifyStoreListeners(r2, r0)     // Catch:{ BadCommandException -> 0x0090, ConnectionException -> 0x00c3, ProtocolException -> 0x00d1 }
            goto L_0x0036
        L_0x00c3:
            r0 = move-exception
            javax.mail.StoreClosedException r2 = new javax.mail.StoreClosedException     // Catch:{ all -> 0x0099 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0099 }
            r2.<init>(r5, r0)     // Catch:{ all -> 0x0099 }
            throw r2     // Catch:{ all -> 0x0099 }
        L_0x00ce:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x00ce }
            throw r0     // Catch:{ BadCommandException -> 0x0090, ConnectionException -> 0x00c3, ProtocolException -> 0x00d1 }
        L_0x00d1:
            r0 = move-exception
            javax.mail.MessagingException r2 = new javax.mail.MessagingException     // Catch:{ all -> 0x0099 }
            java.lang.String r3 = r0.getMessage()     // Catch:{ all -> 0x0099 }
            r2.<init>(r3, r0)     // Catch:{ all -> 0x0099 }
            throw r2     // Catch:{ all -> 0x0099 }
        L_0x00dc:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x00dc }
            throw r0
        L_0x00df:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x00df }
            throw r0
        L_0x00e2:
            r0 = move-exception
            goto L_0x005b
        L_0x00e5:
            r0 = move-exception
            goto L_0x0076
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sun.mail.imap.IMAPStore.idle():void");
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 117 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void waitIfIdle() throws com.sun.mail.iap.ProtocolException {
        /*
            r2 = this;
            boolean r0 = com.sun.mail.imap.IMAPStore.$assertionsDisabled
            if (r0 != 0) goto L_0x002f
            com.sun.mail.imap.IMAPStore$ConnectionPool r0 = r2.pool
            boolean r0 = java.lang.Thread.holdsLock(r0)
            if (r0 != 0) goto L_0x002f
            java.lang.AssertionError r0 = new java.lang.AssertionError
            r0.<init>()
            throw r0
        L_0x0012:
            com.sun.mail.imap.IMAPStore$ConnectionPool r0 = r2.pool
            int r0 = r0.idleState
            r1 = 1
            if (r0 != r1) goto L_0x002a
            com.sun.mail.imap.IMAPStore$ConnectionPool r0 = r2.pool
            com.sun.mail.imap.protocol.IMAPProtocol r0 = r0.idleProtocol
            r0.idleAbort()
            com.sun.mail.imap.IMAPStore$ConnectionPool r0 = r2.pool
            r1 = 2
            r0.idleState = r1
        L_0x002a:
            com.sun.mail.imap.IMAPStore$ConnectionPool r0 = r2.pool     // Catch:{ InterruptedException -> 0x0038 }
            r0.wait()     // Catch:{ InterruptedException -> 0x0038 }
        L_0x002f:
            com.sun.mail.imap.IMAPStore$ConnectionPool r0 = r2.pool
            int r0 = r0.idleState
            if (r0 != 0) goto L_0x0012
            return
        L_0x0038:
            r0 = move-exception
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sun.mail.imap.IMAPStore.waitIfIdle():void");
    }

    /* access modifiers changed from: package-private */
    public void handleResponseCode(Response response) {
        boolean z = $assertionsDisabled;
        String rest = response.getRest();
        if (rest.startsWith("[")) {
            int indexOf = rest.indexOf(93);
            if (indexOf > 0 && rest.substring(0, indexOf + 1).equalsIgnoreCase("[ALERT]")) {
                z = true;
            }
            rest = rest.substring(indexOf + 1).trim();
        }
        if (z) {
            notifyStoreListeners(1, rest);
        } else if (response.isUnTagged() && rest.length() > 0) {
            notifyStoreListeners(2, rest);
        }
    }
}
