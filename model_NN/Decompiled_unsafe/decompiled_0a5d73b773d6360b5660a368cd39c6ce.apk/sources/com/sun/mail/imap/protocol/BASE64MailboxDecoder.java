package com.sun.mail.imap.protocol;

import java.text.CharacterIterator;
import java.text.StringCharacterIterator;

public class BASE64MailboxDecoder {
    static final char[] pem_array = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', ','};
    private static final byte[] pem_convert_array = new byte[256];

    public static String decode(String str) {
        int i;
        if (str == null || str.length() == 0) {
            return str;
        }
        char[] cArr = new char[str.length()];
        StringCharacterIterator stringCharacterIterator = new StringCharacterIterator(str);
        char first = stringCharacterIterator.first();
        int i2 = 0;
        boolean z = false;
        while (first != 65535) {
            if (first == '&') {
                z = true;
                i = base64decode(cArr, i2, stringCharacterIterator);
            } else {
                i = i2 + 1;
                cArr[i2] = first;
            }
            first = stringCharacterIterator.next();
            i2 = i;
        }
        if (z) {
            return new String(cArr, 0, i2);
        }
        return str;
    }

    protected static int base64decode(char[] cArr, int i, CharacterIterator characterIterator) {
        byte b;
        boolean z = true;
        byte b2 = -1;
        while (true) {
            byte next = (byte) characterIterator.next();
            if (next == -1) {
                return i;
            }
            if (next != 45) {
                z = false;
                byte next2 = (byte) characterIterator.next();
                if (next2 == -1 || next2 == 45) {
                    return i;
                }
                byte b3 = pem_convert_array[next & 255];
                byte b4 = pem_convert_array[next2 & 255];
                byte b5 = (byte) (((b3 << 2) & 252) | ((b4 >>> 4) & 3));
                if (b2 != -1) {
                    cArr[i] = (char) ((b2 << 8) | (b5 & 255));
                    b = -1;
                    i++;
                } else {
                    b = b5 & 255;
                }
                byte next3 = (byte) characterIterator.next();
                if (next3 != 61) {
                    if (next3 == -1 || next3 == 45) {
                        return i;
                    }
                    byte b6 = pem_convert_array[next3 & 255];
                    byte b7 = (byte) (((b4 << 4) & 240) | ((b6 >>> 2) & 15));
                    if (b2 != -1) {
                        cArr[i] = (char) ((b2 << 8) | (b7 & 255));
                        b2 = -1;
                        i++;
                    } else {
                        b2 = b7 & 255;
                    }
                    byte next4 = (byte) characterIterator.next();
                    if (next4 == 61) {
                        continue;
                    } else if (next4 == -1 || next4 == 45) {
                        return i;
                    } else {
                        byte b8 = (byte) ((pem_convert_array[next4 & 255] & 63) | ((b6 << 6) & 192));
                        if (b2 != -1) {
                            char c = (char) ((b2 << 8) | (b8 & 255));
                            cArr[i] = (char) ((b2 << 8) | (b8 & 255));
                            b2 = -1;
                            i++;
                        } else {
                            b2 = b8 & 255;
                        }
                    }
                }
            } else if (!z) {
                return i;
            } else {
                cArr[i] = '&';
                return i + 1;
            }
        }
    }

    static {
        for (int i = 0; i < 255; i++) {
            pem_convert_array[i] = -1;
        }
        for (int i2 = 0; i2 < pem_array.length; i2++) {
            pem_convert_array[pem_array[i2]] = (byte) i2;
        }
    }
}
