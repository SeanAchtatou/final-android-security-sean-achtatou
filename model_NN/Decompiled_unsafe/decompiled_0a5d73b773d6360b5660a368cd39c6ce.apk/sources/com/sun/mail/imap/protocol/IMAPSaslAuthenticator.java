package com.sun.mail.imap.protocol;

import java.io.PrintStream;
import java.util.Properties;

public class IMAPSaslAuthenticator implements SaslAuthenticator {
    /* access modifiers changed from: private */
    public boolean debug;
    private String host;
    private String name;
    /* access modifiers changed from: private */
    public PrintStream out;
    private IMAPProtocol pr;
    private Properties props;

    public IMAPSaslAuthenticator(IMAPProtocol iMAPProtocol, String str, Properties properties, boolean z, PrintStream printStream, String str2) {
        this.pr = iMAPProtocol;
        this.name = str;
        this.props = properties;
        this.debug = z;
        this.out = printStream;
        this.host = str2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:124:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x005b, code lost:
        return false;
     */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean authenticate(java.lang.String[] r20, java.lang.String r21, java.lang.String r22, java.lang.String r23, java.lang.String r24) throws com.sun.mail.iap.ProtocolException {
        /*
            r19 = this;
            r0 = r19
            com.sun.mail.imap.protocol.IMAPProtocol r12 = r0.pr
            monitor-enter(r12)
            java.util.Vector r13 = new java.util.Vector     // Catch:{ all -> 0x0230 }
            r13.<init>()     // Catch:{ all -> 0x0230 }
            r11 = 0
            r10 = 0
            r0 = r19
            boolean r4 = r0.debug     // Catch:{ all -> 0x0230 }
            if (r4 == 0) goto L_0x0028
            r0 = r19
            java.io.PrintStream r4 = r0.out     // Catch:{ all -> 0x0230 }
            java.lang.String r5 = "IMAP SASL DEBUG: Mechanisms:"
            r4.print(r5)     // Catch:{ all -> 0x0230 }
            r4 = 0
        L_0x001c:
            r0 = r20
            int r5 = r0.length     // Catch:{ all -> 0x0230 }
            if (r4 < r5) goto L_0x005d
            r0 = r19
            java.io.PrintStream r4 = r0.out     // Catch:{ all -> 0x0230 }
            r4.println()     // Catch:{ all -> 0x0230 }
        L_0x0028:
            com.sun.mail.imap.protocol.IMAPSaslAuthenticator$1 r9 = new com.sun.mail.imap.protocol.IMAPSaslAuthenticator$1     // Catch:{ all -> 0x0230 }
            r0 = r19
            r1 = r23
            r2 = r24
            r3 = r21
            r9.<init>(r1, r2, r3)     // Catch:{ all -> 0x0230 }
            r0 = r19
            java.lang.String r6 = r0.name     // Catch:{ SaslException -> 0x0078 }
            r0 = r19
            java.lang.String r7 = r0.host     // Catch:{ SaslException -> 0x0078 }
            r0 = r19
            java.util.Properties r8 = r0.props     // Catch:{ SaslException -> 0x0078 }
            r4 = r20
            r5 = r22
            javax.security.sasl.SaslClient r7 = javax.security.sasl.Sasl.createSaslClient(r4, r5, r6, r7, r8, r9)     // Catch:{ SaslException -> 0x0078 }
            if (r7 != 0) goto L_0x0098
            r0 = r19
            boolean r4 = r0.debug     // Catch:{ all -> 0x0230 }
            if (r4 == 0) goto L_0x005a
            r0 = r19
            java.io.PrintStream r4 = r0.out     // Catch:{ all -> 0x0230 }
            java.lang.String r5 = "IMAP SASL DEBUG: No SASL support"
            r4.println(r5)     // Catch:{ all -> 0x0230 }
        L_0x005a:
            monitor-exit(r12)     // Catch:{ all -> 0x0230 }
            r4 = 0
        L_0x005c:
            return r4
        L_0x005d:
            r0 = r19
            java.io.PrintStream r5 = r0.out     // Catch:{ all -> 0x0230 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x0230 }
            java.lang.String r7 = " "
            r6.<init>(r7)     // Catch:{ all -> 0x0230 }
            r7 = r20[r4]     // Catch:{ all -> 0x0230 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0230 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x0230 }
            r5.print(r6)     // Catch:{ all -> 0x0230 }
            int r4 = r4 + 1
            goto L_0x001c
        L_0x0078:
            r4 = move-exception
            r0 = r19
            boolean r5 = r0.debug     // Catch:{ all -> 0x0230 }
            if (r5 == 0) goto L_0x0095
            r0 = r19
            java.io.PrintStream r5 = r0.out     // Catch:{ all -> 0x0230 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x0230 }
            java.lang.String r7 = "IMAP SASL DEBUG: Failed to create SASL client: "
            r6.<init>(r7)     // Catch:{ all -> 0x0230 }
            java.lang.StringBuilder r4 = r6.append(r4)     // Catch:{ all -> 0x0230 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0230 }
            r5.println(r4)     // Catch:{ all -> 0x0230 }
        L_0x0095:
            monitor-exit(r12)     // Catch:{ all -> 0x0230 }
            r4 = 0
            goto L_0x005c
        L_0x0098:
            r0 = r19
            boolean r4 = r0.debug     // Catch:{ all -> 0x0230 }
            if (r4 == 0) goto L_0x00b8
            r0 = r19
            java.io.PrintStream r4 = r0.out     // Catch:{ all -> 0x0230 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0230 }
            java.lang.String r6 = "IMAP SASL DEBUG: SASL client "
            r5.<init>(r6)     // Catch:{ all -> 0x0230 }
            java.lang.String r6 = r7.getMechanismName()     // Catch:{ all -> 0x0230 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x0230 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0230 }
            r4.println(r5)     // Catch:{ all -> 0x0230 }
        L_0x00b8:
            r0 = r19
            com.sun.mail.imap.protocol.IMAPProtocol r4 = r0.pr     // Catch:{ Exception -> 0x0128 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0128 }
            java.lang.String r6 = "AUTHENTICATE "
            r5.<init>(r6)     // Catch:{ Exception -> 0x0128 }
            java.lang.String r6 = r7.getMechanismName()     // Catch:{ Exception -> 0x0128 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x0128 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0128 }
            r6 = 0
            java.lang.String r8 = r4.writeCommand(r5, r6)     // Catch:{ Exception -> 0x0128 }
            r0 = r19
            com.sun.mail.imap.protocol.IMAPProtocol r4 = r0.pr     // Catch:{ all -> 0x0230 }
            java.io.OutputStream r9 = r4.getIMAPOutputStream()     // Catch:{ all -> 0x0230 }
            java.io.ByteArrayOutputStream r14 = new java.io.ByteArrayOutputStream     // Catch:{ all -> 0x0230 }
            r14.<init>()     // Catch:{ all -> 0x0230 }
            r4 = 2
            byte[] r15 = new byte[r4]     // Catch:{ all -> 0x0230 }
            r15 = {13, 10} // fill-array     // Catch:{ all -> 0x0230 }
            java.lang.String r4 = r7.getMechanismName()     // Catch:{ all -> 0x0230 }
            java.lang.String r5 = "XGWTRUSTEDAPP"
            boolean r16 = r4.equals(r5)     // Catch:{ all -> 0x0230 }
            r5 = r10
            r6 = r11
        L_0x00f3:
            if (r5 == 0) goto L_0x0149
            boolean r4 = r7.isComplete()     // Catch:{ all -> 0x0230 }
            if (r4 == 0) goto L_0x0256
            java.lang.String r4 = "javax.security.sasl.qop"
            java.lang.Object r4 = r7.getNegotiatedProperty(r4)     // Catch:{ all -> 0x0230 }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ all -> 0x0230 }
            if (r4 == 0) goto L_0x0256
            java.lang.String r5 = "auth-int"
            boolean r5 = r4.equalsIgnoreCase(r5)     // Catch:{ all -> 0x0230 }
            if (r5 != 0) goto L_0x0115
            java.lang.String r5 = "auth-conf"
            boolean r4 = r4.equalsIgnoreCase(r5)     // Catch:{ all -> 0x0230 }
            if (r4 == 0) goto L_0x0256
        L_0x0115:
            r0 = r19
            boolean r4 = r0.debug     // Catch:{ all -> 0x0230 }
            if (r4 == 0) goto L_0x0124
            r0 = r19
            java.io.PrintStream r4 = r0.out     // Catch:{ all -> 0x0230 }
            java.lang.String r5 = "IMAP SASL DEBUG: Mechanism requires integrity or confidentiality"
            r4.println(r5)     // Catch:{ all -> 0x0230 }
        L_0x0124:
            monitor-exit(r12)     // Catch:{ all -> 0x0230 }
            r4 = 0
            goto L_0x005c
        L_0x0128:
            r4 = move-exception
            r0 = r19
            boolean r5 = r0.debug     // Catch:{ all -> 0x0230 }
            if (r5 == 0) goto L_0x0145
            r0 = r19
            java.io.PrintStream r5 = r0.out     // Catch:{ all -> 0x0230 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x0230 }
            java.lang.String r7 = "IMAP SASL DEBUG: AUTHENTICATE Exception: "
            r6.<init>(r7)     // Catch:{ all -> 0x0230 }
            java.lang.StringBuilder r4 = r6.append(r4)     // Catch:{ all -> 0x0230 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0230 }
            r5.println(r4)     // Catch:{ all -> 0x0230 }
        L_0x0145:
            monitor-exit(r12)     // Catch:{ all -> 0x0230 }
            r4 = 0
            goto L_0x005c
        L_0x0149:
            r0 = r19
            com.sun.mail.imap.protocol.IMAPProtocol r4 = r0.pr     // Catch:{ Exception -> 0x01c4 }
            com.sun.mail.iap.Response r6 = r4.readResponse()     // Catch:{ Exception -> 0x01c4 }
            boolean r4 = r6.isContinuation()     // Catch:{ Exception -> 0x01c4 }
            if (r4 == 0) goto L_0x0233
            r4 = 0
            byte[] r4 = (byte[]) r4     // Catch:{ Exception -> 0x01c4 }
            boolean r10 = r7.isComplete()     // Catch:{ Exception -> 0x01c4 }
            if (r10 != 0) goto L_0x01a8
            com.sun.mail.iap.ByteArray r4 = r6.readByteArray()     // Catch:{ Exception -> 0x01c4 }
            byte[] r4 = r4.getNewBytes()     // Catch:{ Exception -> 0x01c4 }
            int r10 = r4.length     // Catch:{ Exception -> 0x01c4 }
            if (r10 <= 0) goto L_0x016f
            byte[] r4 = com.sun.mail.util.BASE64DecoderStream.decode(r4)     // Catch:{ Exception -> 0x01c4 }
        L_0x016f:
            r0 = r19
            boolean r10 = r0.debug     // Catch:{ Exception -> 0x01c4 }
            if (r10 == 0) goto L_0x01a4
            r0 = r19
            java.io.PrintStream r10 = r0.out     // Catch:{ Exception -> 0x01c4 }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01c4 }
            java.lang.String r17 = "IMAP SASL DEBUG: challenge: "
            r0 = r17
            r11.<init>(r0)     // Catch:{ Exception -> 0x01c4 }
            r17 = 0
            int r0 = r4.length     // Catch:{ Exception -> 0x01c4 }
            r18 = r0
            r0 = r17
            r1 = r18
            java.lang.String r17 = com.sun.mail.util.ASCIIUtility.toString(r4, r0, r1)     // Catch:{ Exception -> 0x01c4 }
            r0 = r17
            java.lang.StringBuilder r11 = r11.append(r0)     // Catch:{ Exception -> 0x01c4 }
            java.lang.String r17 = " :"
            r0 = r17
            java.lang.StringBuilder r11 = r11.append(r0)     // Catch:{ Exception -> 0x01c4 }
            java.lang.String r11 = r11.toString()     // Catch:{ Exception -> 0x01c4 }
            r10.println(r11)     // Catch:{ Exception -> 0x01c4 }
        L_0x01a4:
            byte[] r4 = r7.evaluateChallenge(r4)     // Catch:{ Exception -> 0x01c4 }
        L_0x01a8:
            if (r4 != 0) goto L_0x01d7
            r0 = r19
            boolean r4 = r0.debug     // Catch:{ Exception -> 0x01c4 }
            if (r4 == 0) goto L_0x01b9
            r0 = r19
            java.io.PrintStream r4 = r0.out     // Catch:{ Exception -> 0x01c4 }
            java.lang.String r10 = "IMAP SASL DEBUG: no response"
            r4.println(r10)     // Catch:{ Exception -> 0x01c4 }
        L_0x01b9:
            r9.write(r15)     // Catch:{ Exception -> 0x01c4 }
            r9.flush()     // Catch:{ Exception -> 0x01c4 }
            r14.reset()     // Catch:{ Exception -> 0x01c4 }
            goto L_0x00f3
        L_0x01c4:
            r4 = move-exception
            r0 = r19
            boolean r5 = r0.debug     // Catch:{ all -> 0x0230 }
            if (r5 == 0) goto L_0x01ce
            r4.printStackTrace()     // Catch:{ all -> 0x0230 }
        L_0x01ce:
            com.sun.mail.iap.Response r5 = com.sun.mail.iap.Response.byeResponse(r4)     // Catch:{ all -> 0x0230 }
            r4 = 1
            r6 = r5
            r5 = r4
            goto L_0x00f3
        L_0x01d7:
            r0 = r19
            boolean r10 = r0.debug     // Catch:{ Exception -> 0x01c4 }
            if (r10 == 0) goto L_0x020c
            r0 = r19
            java.io.PrintStream r10 = r0.out     // Catch:{ Exception -> 0x01c4 }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01c4 }
            java.lang.String r17 = "IMAP SASL DEBUG: response: "
            r0 = r17
            r11.<init>(r0)     // Catch:{ Exception -> 0x01c4 }
            r17 = 0
            int r0 = r4.length     // Catch:{ Exception -> 0x01c4 }
            r18 = r0
            r0 = r17
            r1 = r18
            java.lang.String r17 = com.sun.mail.util.ASCIIUtility.toString(r4, r0, r1)     // Catch:{ Exception -> 0x01c4 }
            r0 = r17
            java.lang.StringBuilder r11 = r11.append(r0)     // Catch:{ Exception -> 0x01c4 }
            java.lang.String r17 = " :"
            r0 = r17
            java.lang.StringBuilder r11 = r11.append(r0)     // Catch:{ Exception -> 0x01c4 }
            java.lang.String r11 = r11.toString()     // Catch:{ Exception -> 0x01c4 }
            r10.println(r11)     // Catch:{ Exception -> 0x01c4 }
        L_0x020c:
            byte[] r4 = com.sun.mail.util.BASE64EncoderStream.encode(r4)     // Catch:{ Exception -> 0x01c4 }
            if (r16 == 0) goto L_0x021b
            java.lang.String r10 = "XGWTRUSTEDAPP "
            byte[] r10 = r10.getBytes()     // Catch:{ Exception -> 0x01c4 }
            r14.write(r10)     // Catch:{ Exception -> 0x01c4 }
        L_0x021b:
            r14.write(r4)     // Catch:{ Exception -> 0x01c4 }
            r14.write(r15)     // Catch:{ Exception -> 0x01c4 }
            byte[] r4 = r14.toByteArray()     // Catch:{ Exception -> 0x01c4 }
            r9.write(r4)     // Catch:{ Exception -> 0x01c4 }
            r9.flush()     // Catch:{ Exception -> 0x01c4 }
            r14.reset()     // Catch:{ Exception -> 0x01c4 }
            goto L_0x00f3
        L_0x0230:
            r4 = move-exception
            monitor-exit(r12)     // Catch:{ all -> 0x0230 }
            throw r4
        L_0x0233:
            boolean r4 = r6.isTagged()     // Catch:{ Exception -> 0x01c4 }
            if (r4 == 0) goto L_0x0247
            java.lang.String r4 = r6.getTag()     // Catch:{ Exception -> 0x01c4 }
            boolean r4 = r4.equals(r8)     // Catch:{ Exception -> 0x01c4 }
            if (r4 == 0) goto L_0x0247
            r4 = 1
            r5 = r4
            goto L_0x00f3
        L_0x0247:
            boolean r4 = r6.isBYE()     // Catch:{ Exception -> 0x01c4 }
            if (r4 == 0) goto L_0x0251
            r4 = 1
            r5 = r4
            goto L_0x00f3
        L_0x0251:
            r13.addElement(r6)     // Catch:{ Exception -> 0x01c4 }
            goto L_0x00f3
        L_0x0256:
            int r4 = r13.size()     // Catch:{ all -> 0x0230 }
            com.sun.mail.iap.Response[] r4 = new com.sun.mail.iap.Response[r4]     // Catch:{ all -> 0x0230 }
            r13.copyInto(r4)     // Catch:{ all -> 0x0230 }
            r0 = r19
            com.sun.mail.imap.protocol.IMAPProtocol r5 = r0.pr     // Catch:{ all -> 0x0230 }
            r5.notifyResponseHandlers(r4)     // Catch:{ all -> 0x0230 }
            r0 = r19
            com.sun.mail.imap.protocol.IMAPProtocol r4 = r0.pr     // Catch:{ all -> 0x0230 }
            r4.handleResult(r6)     // Catch:{ all -> 0x0230 }
            r0 = r19
            com.sun.mail.imap.protocol.IMAPProtocol r4 = r0.pr     // Catch:{ all -> 0x0230 }
            r4.setCapabilities(r6)     // Catch:{ all -> 0x0230 }
            monitor-exit(r12)     // Catch:{ all -> 0x0230 }
            r4 = 1
            goto L_0x005c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sun.mail.imap.protocol.IMAPSaslAuthenticator.authenticate(java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.String):boolean");
    }
}
