package com.sun.mail.imap.protocol;

import com.sun.mail.iap.ParsingException;
import javax.mail.Flags;

public class FLAGS extends Flags implements Item {
    static final char[] name = {'F', 'L', 'A', 'G', 'S'};
    private static final long serialVersionUID = 439049847053756670L;
    public int msgno;

    public FLAGS(IMAPResponse iMAPResponse) throws ParsingException {
        this.msgno = iMAPResponse.getNumber();
        iMAPResponse.skipSpaces();
        String[] readSimpleList = iMAPResponse.readSimpleList();
        if (readSimpleList != null) {
            for (String str : readSimpleList) {
                if (str.length() < 2 || str.charAt(0) != '\\') {
                    add(str);
                } else {
                    switch (Character.toUpperCase(str.charAt(1))) {
                        case '*':
                            add(Flags.Flag.USER);
                            continue;
                        case 'A':
                            add(Flags.Flag.ANSWERED);
                            continue;
                        case 'D':
                            if (str.length() < 3) {
                                add(str);
                                break;
                            } else {
                                char charAt = str.charAt(2);
                                if (charAt != 'e' && charAt != 'E') {
                                    if (charAt != 'r' && charAt != 'R') {
                                        break;
                                    } else {
                                        add(Flags.Flag.DRAFT);
                                        break;
                                    }
                                } else {
                                    add(Flags.Flag.DELETED);
                                    continue;
                                }
                            }
                            break;
                        case 'F':
                            add(Flags.Flag.FLAGGED);
                            continue;
                        case 'R':
                            add(Flags.Flag.RECENT);
                            continue;
                        case 'S':
                            add(Flags.Flag.SEEN);
                            continue;
                        default:
                            add(str);
                            continue;
                    }
                }
            }
        }
    }
}
