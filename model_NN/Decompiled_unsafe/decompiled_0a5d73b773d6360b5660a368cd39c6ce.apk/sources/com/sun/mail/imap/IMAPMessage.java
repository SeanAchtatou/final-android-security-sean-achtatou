package com.sun.mail.imap;

import com.sun.mail.iap.ConnectionException;
import com.sun.mail.iap.ProtocolException;
import com.sun.mail.iap.Response;
import com.sun.mail.imap.protocol.BODY;
import com.sun.mail.imap.protocol.BODYSTRUCTURE;
import com.sun.mail.imap.protocol.ENVELOPE;
import com.sun.mail.imap.protocol.FetchResponse;
import com.sun.mail.imap.protocol.IMAPProtocol;
import com.sun.mail.imap.protocol.INTERNALDATE;
import com.sun.mail.imap.protocol.Item;
import com.sun.mail.imap.protocol.RFC822DATA;
import com.sun.mail.imap.protocol.RFC822SIZE;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Locale;
import javax.activation.DataHandler;
import javax.mail.Address;
import javax.mail.Flags;
import javax.mail.FolderClosedException;
import javax.mail.IllegalWriteException;
import javax.mail.Message;
import javax.mail.MessageRemovedException;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.ContentType;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.InternetHeaders;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;

public class IMAPMessage extends MimeMessage {
    private static String EnvelopeCmd = "ENVELOPE INTERNALDATE RFC822.SIZE";
    protected BODYSTRUCTURE bs;
    private String description;
    protected ENVELOPE envelope;
    private boolean headersLoaded = false;
    private Hashtable loadedHeaders;
    private boolean peek;
    private Date receivedDate;
    protected String sectionId;
    private int seqnum;
    /* access modifiers changed from: private */
    public int size = -1;
    private String subject;
    private String type;
    private long uid = -1;

    protected IMAPMessage(IMAPFolder iMAPFolder, int i, int i2) {
        super(iMAPFolder, i);
        this.seqnum = i2;
        this.flags = null;
    }

    protected IMAPMessage(Session session) {
        super(session);
    }

    /* access modifiers changed from: protected */
    public IMAPProtocol getProtocol() throws ProtocolException, FolderClosedException {
        ((IMAPFolder) this.folder).waitIfIdle();
        IMAPProtocol iMAPProtocol = ((IMAPFolder) this.folder).protocol;
        if (iMAPProtocol != null) {
            return iMAPProtocol;
        }
        throw new FolderClosedException(this.folder);
    }

    /* access modifiers changed from: protected */
    public boolean isREV1() throws FolderClosedException {
        IMAPProtocol iMAPProtocol = ((IMAPFolder) this.folder).protocol;
        if (iMAPProtocol != null) {
            return iMAPProtocol.isREV1();
        }
        throw new FolderClosedException(this.folder);
    }

    /* access modifiers changed from: protected */
    public Object getMessageCacheLock() {
        return ((IMAPFolder) this.folder).messageCacheLock;
    }

    /* access modifiers changed from: protected */
    public int getSequenceNumber() {
        return this.seqnum;
    }

    /* access modifiers changed from: protected */
    public void setSequenceNumber(int i) {
        this.seqnum = i;
    }

    /* access modifiers changed from: protected */
    public void setMessageNumber(int i) {
        super.setMessageNumber(i);
    }

    /* access modifiers changed from: protected */
    public long getUID() {
        return this.uid;
    }

    /* access modifiers changed from: protected */
    public void setUID(long j) {
        this.uid = j;
    }

    /* access modifiers changed from: protected */
    public void setExpunged(boolean z) {
        super.setExpunged(z);
        this.seqnum = -1;
    }

    /* access modifiers changed from: protected */
    public void checkExpunged() throws MessageRemovedException {
        if (this.expunged) {
            throw new MessageRemovedException();
        }
    }

    /* access modifiers changed from: protected */
    public void forceCheckExpunged() throws MessageRemovedException, FolderClosedException {
        synchronized (getMessageCacheLock()) {
            try {
                getProtocol().noop();
            } catch (ConnectionException e) {
                throw new FolderClosedException(this.folder, e.getMessage());
            } catch (ProtocolException e2) {
            }
        }
        if (this.expunged) {
            throw new MessageRemovedException();
        }
    }

    /* access modifiers changed from: protected */
    public int getFetchBlockSize() {
        return ((IMAPStore) this.folder.getStore()).getFetchBlockSize();
    }

    public Address[] getFrom() throws MessagingException {
        checkExpunged();
        loadEnvelope();
        return aaclone(this.envelope.from);
    }

    public void setFrom(Address address) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }

    public void addFrom(Address[] addressArr) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }

    public Address getSender() throws MessagingException {
        checkExpunged();
        loadEnvelope();
        if (this.envelope.sender != null) {
            return this.envelope.sender[0];
        }
        return null;
    }

    public void setSender(Address address) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }

    public Address[] getRecipients(Message.RecipientType recipientType) throws MessagingException {
        checkExpunged();
        loadEnvelope();
        if (recipientType == Message.RecipientType.TO) {
            return aaclone(this.envelope.to);
        }
        if (recipientType == Message.RecipientType.CC) {
            return aaclone(this.envelope.cc);
        }
        if (recipientType == Message.RecipientType.BCC) {
            return aaclone(this.envelope.bcc);
        }
        return super.getRecipients(recipientType);
    }

    public void setRecipients(Message.RecipientType recipientType, Address[] addressArr) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }

    public void addRecipients(Message.RecipientType recipientType, Address[] addressArr) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }

    public Address[] getReplyTo() throws MessagingException {
        checkExpunged();
        loadEnvelope();
        return aaclone(this.envelope.replyTo);
    }

    public void setReplyTo(Address[] addressArr) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }

    public String getSubject() throws MessagingException {
        checkExpunged();
        if (this.subject != null) {
            return this.subject;
        }
        loadEnvelope();
        if (this.envelope.subject == null) {
            return null;
        }
        try {
            this.subject = MimeUtility.decodeText(this.envelope.subject);
        } catch (UnsupportedEncodingException e) {
            this.subject = this.envelope.subject;
        }
        return this.subject;
    }

    public void setSubject(String str, String str2) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }

    public Date getSentDate() throws MessagingException {
        checkExpunged();
        loadEnvelope();
        if (this.envelope.date == null) {
            return null;
        }
        return new Date(this.envelope.date.getTime());
    }

    public void setSentDate(Date date) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }

    public Date getReceivedDate() throws MessagingException {
        checkExpunged();
        loadEnvelope();
        if (this.receivedDate == null) {
            return null;
        }
        return new Date(this.receivedDate.getTime());
    }

    public int getSize() throws MessagingException {
        checkExpunged();
        if (this.size == -1) {
            loadEnvelope();
        }
        return this.size;
    }

    public int getLineCount() throws MessagingException {
        checkExpunged();
        loadBODYSTRUCTURE();
        return this.bs.lines;
    }

    public String[] getContentLanguage() throws MessagingException {
        checkExpunged();
        loadBODYSTRUCTURE();
        if (this.bs.language != null) {
            return (String[]) this.bs.language.clone();
        }
        return null;
    }

    public void setContentLanguage(String[] strArr) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }

    public String getInReplyTo() throws MessagingException {
        checkExpunged();
        loadEnvelope();
        return this.envelope.inReplyTo;
    }

    public String getContentType() throws MessagingException {
        checkExpunged();
        if (this.type == null) {
            loadBODYSTRUCTURE();
            this.type = new ContentType(this.bs.type, this.bs.subtype, this.bs.cParams).toString();
        }
        return this.type;
    }

    public String getDisposition() throws MessagingException {
        checkExpunged();
        loadBODYSTRUCTURE();
        return this.bs.disposition;
    }

    public void setDisposition(String str) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }

    public String getEncoding() throws MessagingException {
        checkExpunged();
        loadBODYSTRUCTURE();
        return this.bs.encoding;
    }

    public String getContentID() throws MessagingException {
        checkExpunged();
        loadBODYSTRUCTURE();
        return this.bs.id;
    }

    public void setContentID(String str) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }

    public String getContentMD5() throws MessagingException {
        checkExpunged();
        loadBODYSTRUCTURE();
        return this.bs.md5;
    }

    public void setContentMD5(String str) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }

    public String getDescription() throws MessagingException {
        checkExpunged();
        if (this.description != null) {
            return this.description;
        }
        loadBODYSTRUCTURE();
        if (this.bs.description == null) {
            return null;
        }
        try {
            this.description = MimeUtility.decodeText(this.bs.description);
        } catch (UnsupportedEncodingException e) {
            this.description = this.bs.description;
        }
        return this.description;
    }

    public void setDescription(String str, String str2) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }

    public String getMessageID() throws MessagingException {
        checkExpunged();
        loadEnvelope();
        return this.envelope.messageId;
    }

    public String getFileName() throws MessagingException {
        checkExpunged();
        String str = null;
        loadBODYSTRUCTURE();
        if (this.bs.dParams != null) {
            str = this.bs.dParams.get("filename");
        }
        if (str != null || this.bs.cParams == null) {
            return str;
        }
        return this.bs.cParams.get("name");
    }

    public void setFileName(String str) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }

    /* access modifiers changed from: protected */
    public InputStream getContentStream() throws MessagingException {
        BODY fetchBody;
        int i = -1;
        InputStream inputStream = null;
        boolean peek2 = getPeek();
        synchronized (getMessageCacheLock()) {
            try {
                IMAPProtocol protocol = getProtocol();
                checkExpunged();
                if (!protocol.isREV1() || getFetchBlockSize() == -1) {
                    if (protocol.isREV1()) {
                        if (peek2) {
                            fetchBody = protocol.peekBody(getSequenceNumber(), toSection("TEXT"));
                        } else {
                            fetchBody = protocol.fetchBody(getSequenceNumber(), toSection("TEXT"));
                        }
                        if (fetchBody != null) {
                            inputStream = fetchBody.getByteArrayInputStream();
                        }
                    } else {
                        RFC822DATA fetchRFC822 = protocol.fetchRFC822(getSequenceNumber(), "TEXT");
                        if (fetchRFC822 != null) {
                            inputStream = fetchRFC822.getByteArrayInputStream();
                        }
                    }
                    if (inputStream == null) {
                        throw new MessagingException("No content");
                    }
                } else {
                    String section = toSection("TEXT");
                    if (this.bs != null) {
                        i = this.bs.size;
                    }
                    inputStream = new IMAPInputStream(this, section, i, peek2);
                }
            } catch (ConnectionException e) {
                throw new FolderClosedException(this.folder, e.getMessage());
            } catch (ProtocolException e2) {
                forceCheckExpunged();
                throw new MessagingException(e2.getMessage(), e2);
            }
        }
        return inputStream;
    }

    public synchronized DataHandler getDataHandler() throws MessagingException {
        checkExpunged();
        if (this.dh == null) {
            loadBODYSTRUCTURE();
            if (this.type == null) {
                this.type = new ContentType(this.bs.type, this.bs.subtype, this.bs.cParams).toString();
            }
            if (this.bs.isMulti()) {
                this.dh = new DataHandler(new IMAPMultipartDataSource(this, this.bs.bodies, this.sectionId, this));
            } else if (this.bs.isNested() && isREV1()) {
                this.dh = new DataHandler(new IMAPNestedMessage(this, this.bs.bodies[0], this.bs.envelope, this.sectionId == null ? "1" : String.valueOf(this.sectionId) + ".1"), this.type);
            }
        }
        return super.getDataHandler();
    }

    public void setDataHandler(DataHandler dataHandler) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }

    public void writeTo(OutputStream outputStream) throws IOException, MessagingException {
        BODY fetchBody;
        ByteArrayInputStream byteArrayInputStream = null;
        boolean peek2 = getPeek();
        synchronized (getMessageCacheLock()) {
            try {
                IMAPProtocol protocol = getProtocol();
                checkExpunged();
                if (protocol.isREV1()) {
                    if (peek2) {
                        fetchBody = protocol.peekBody(getSequenceNumber(), this.sectionId);
                    } else {
                        fetchBody = protocol.fetchBody(getSequenceNumber(), this.sectionId);
                    }
                    if (fetchBody != null) {
                        byteArrayInputStream = fetchBody.getByteArrayInputStream();
                    }
                } else {
                    RFC822DATA fetchRFC822 = protocol.fetchRFC822(getSequenceNumber(), null);
                    if (fetchRFC822 != null) {
                        byteArrayInputStream = fetchRFC822.getByteArrayInputStream();
                    }
                }
            } catch (ConnectionException e) {
                throw new FolderClosedException(this.folder, e.getMessage());
            } catch (ProtocolException e2) {
                forceCheckExpunged();
                throw new MessagingException(e2.getMessage(), e2);
            }
        }
        if (byteArrayInputStream == null) {
            throw new MessagingException("No content");
        }
        byte[] bArr = new byte[1024];
        while (true) {
            int read = byteArrayInputStream.read(bArr);
            if (read != -1) {
                outputStream.write(bArr, 0, read);
            } else {
                return;
            }
        }
    }

    public String[] getHeader(String str) throws MessagingException {
        ByteArrayInputStream byteArrayInputStream;
        checkExpunged();
        if (isHeaderLoaded(str)) {
            return this.headers.getHeader(str);
        }
        synchronized (getMessageCacheLock()) {
            try {
                IMAPProtocol protocol = getProtocol();
                checkExpunged();
                if (protocol.isREV1()) {
                    BODY peekBody = protocol.peekBody(getSequenceNumber(), toSection("HEADER.FIELDS (" + str + ")"));
                    if (peekBody != null) {
                        byteArrayInputStream = peekBody.getByteArrayInputStream();
                    }
                    byteArrayInputStream = null;
                } else {
                    RFC822DATA fetchRFC822 = protocol.fetchRFC822(getSequenceNumber(), "HEADER.LINES (" + str + ")");
                    if (fetchRFC822 != null) {
                        byteArrayInputStream = fetchRFC822.getByteArrayInputStream();
                    }
                    byteArrayInputStream = null;
                }
            } catch (ConnectionException e) {
                throw new FolderClosedException(this.folder, e.getMessage());
            } catch (ProtocolException e2) {
                forceCheckExpunged();
                throw new MessagingException(e2.getMessage(), e2);
            }
        }
        if (byteArrayInputStream == null) {
            return null;
        }
        if (this.headers == null) {
            this.headers = new InternetHeaders();
        }
        this.headers.load(byteArrayInputStream);
        setHeaderLoaded(str);
        return this.headers.getHeader(str);
    }

    public String getHeader(String str, String str2) throws MessagingException {
        checkExpunged();
        if (getHeader(str) == null) {
            return null;
        }
        return this.headers.getHeader(str, str2);
    }

    public void setHeader(String str, String str2) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }

    public void addHeader(String str, String str2) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }

    public void removeHeader(String str) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }

    public Enumeration getAllHeaders() throws MessagingException {
        checkExpunged();
        loadHeaders();
        return super.getAllHeaders();
    }

    public Enumeration getMatchingHeaders(String[] strArr) throws MessagingException {
        checkExpunged();
        loadHeaders();
        return super.getMatchingHeaders(strArr);
    }

    public Enumeration getNonMatchingHeaders(String[] strArr) throws MessagingException {
        checkExpunged();
        loadHeaders();
        return super.getNonMatchingHeaders(strArr);
    }

    public void addHeaderLine(String str) throws MessagingException {
        throw new IllegalWriteException("IMAPMessage is read-only");
    }

    public Enumeration getAllHeaderLines() throws MessagingException {
        checkExpunged();
        loadHeaders();
        return super.getAllHeaderLines();
    }

    public Enumeration getMatchingHeaderLines(String[] strArr) throws MessagingException {
        checkExpunged();
        loadHeaders();
        return super.getMatchingHeaderLines(strArr);
    }

    public Enumeration getNonMatchingHeaderLines(String[] strArr) throws MessagingException {
        checkExpunged();
        loadHeaders();
        return super.getNonMatchingHeaderLines(strArr);
    }

    public synchronized Flags getFlags() throws MessagingException {
        checkExpunged();
        loadFlags();
        return super.getFlags();
    }

    public synchronized boolean isSet(Flags.Flag flag) throws MessagingException {
        checkExpunged();
        loadFlags();
        return super.isSet(flag);
    }

    public synchronized void setFlags(Flags flags, boolean z) throws MessagingException {
        synchronized (getMessageCacheLock()) {
            try {
                IMAPProtocol protocol = getProtocol();
                checkExpunged();
                protocol.storeFlags(getSequenceNumber(), flags, z);
            } catch (ConnectionException e) {
                throw new FolderClosedException(this.folder, e.getMessage());
            } catch (ProtocolException e2) {
                throw new MessagingException(e2.getMessage(), e2);
            }
        }
    }

    public synchronized void setPeek(boolean z) {
        this.peek = z;
    }

    public synchronized boolean getPeek() {
        return this.peek;
    }

    public synchronized void invalidateHeaders() {
        this.headersLoaded = false;
        this.loadedHeaders = null;
        this.envelope = null;
        this.bs = null;
        this.receivedDate = null;
        this.size = -1;
        this.type = null;
        this.subject = null;
        this.description = null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:171:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void fetch(com.sun.mail.imap.IMAPFolder r17, javax.mail.Message[] r18, javax.mail.FetchProfile r19) throws javax.mail.MessagingException {
        /*
            java.lang.StringBuffer r5 = new java.lang.StringBuffer
            r5.<init>()
            r1 = 1
            r2 = 0
            javax.mail.FetchProfile$Item r3 = javax.mail.FetchProfile.Item.ENVELOPE
            r0 = r19
            boolean r3 = r0.contains(r3)
            if (r3 == 0) goto L_0x0017
            java.lang.String r1 = com.sun.mail.imap.IMAPMessage.EnvelopeCmd
            r5.append(r1)
            r1 = 0
        L_0x0017:
            javax.mail.FetchProfile$Item r3 = javax.mail.FetchProfile.Item.FLAGS
            r0 = r19
            boolean r3 = r0.contains(r3)
            if (r3 == 0) goto L_0x0029
            if (r1 == 0) goto L_0x00b3
            java.lang.String r1 = "FLAGS"
        L_0x0025:
            r5.append(r1)
            r1 = 0
        L_0x0029:
            javax.mail.FetchProfile$Item r3 = javax.mail.FetchProfile.Item.CONTENT_INFO
            r0 = r19
            boolean r3 = r0.contains(r3)
            if (r3 == 0) goto L_0x003b
            if (r1 == 0) goto L_0x00b7
            java.lang.String r1 = "BODYSTRUCTURE"
        L_0x0037:
            r5.append(r1)
            r1 = 0
        L_0x003b:
            javax.mail.UIDFolder$FetchProfileItem r3 = javax.mail.UIDFolder.FetchProfileItem.UID
            r0 = r19
            boolean r3 = r0.contains(r3)
            if (r3 == 0) goto L_0x004d
            if (r1 == 0) goto L_0x00bb
            java.lang.String r1 = "UID"
        L_0x0049:
            r5.append(r1)
            r1 = 0
        L_0x004d:
            com.sun.mail.imap.IMAPFolder$FetchProfileItem r3 = com.sun.mail.imap.IMAPFolder.FetchProfileItem.HEADERS
            r0 = r19
            boolean r3 = r0.contains(r3)
            if (r3 == 0) goto L_0x0231
            r2 = 1
            r0 = r17
            com.sun.mail.imap.protocol.IMAPProtocol r3 = r0.protocol
            boolean r3 = r3.isREV1()
            if (r3 == 0) goto L_0x00c1
            if (r1 == 0) goto L_0x00be
            java.lang.String r1 = "BODY.PEEK[HEADER]"
        L_0x0066:
            r5.append(r1)
        L_0x0069:
            r1 = 0
            r3 = r2
        L_0x006b:
            com.sun.mail.imap.IMAPFolder$FetchProfileItem r2 = com.sun.mail.imap.IMAPFolder.FetchProfileItem.SIZE
            r0 = r19
            boolean r2 = r0.contains(r2)
            if (r2 == 0) goto L_0x022e
            if (r1 == 0) goto L_0x00cc
            java.lang.String r1 = "RFC822.SIZE"
        L_0x0079:
            r5.append(r1)
            r1 = 0
            r2 = r1
        L_0x007e:
            r1 = 0
            java.lang.String[] r1 = (java.lang.String[]) r1
            if (r3 != 0) goto L_0x009c
            java.lang.String[] r1 = r19.getHeaderNames()
            int r4 = r1.length
            if (r4 <= 0) goto L_0x009c
            if (r2 != 0) goto L_0x0091
            java.lang.String r2 = " "
            r5.append(r2)
        L_0x0091:
            r0 = r17
            com.sun.mail.imap.protocol.IMAPProtocol r2 = r0.protocol
            java.lang.String r2 = craftHeaderCmd(r2, r1)
            r5.append(r2)
        L_0x009c:
            r4 = r1
            com.sun.mail.imap.IMAPMessage$1FetchProfileCondition r1 = new com.sun.mail.imap.IMAPMessage$1FetchProfileCondition
            r0 = r19
            r1.<init>(r0)
            r0 = r17
            java.lang.Object r9 = r0.messageCacheLock
            monitor-enter(r9)
            r0 = r18
            com.sun.mail.imap.protocol.MessageSet[] r2 = com.sun.mail.imap.Utility.toMessageSet(r0, r1)     // Catch:{ all -> 0x00e8 }
            if (r2 != 0) goto L_0x00cf
            monitor-exit(r9)     // Catch:{ all -> 0x00e8 }
        L_0x00b2:
            return
        L_0x00b3:
            java.lang.String r1 = " FLAGS"
            goto L_0x0025
        L_0x00b7:
            java.lang.String r1 = " BODYSTRUCTURE"
            goto L_0x0037
        L_0x00bb:
            java.lang.String r1 = " UID"
            goto L_0x0049
        L_0x00be:
            java.lang.String r1 = " BODY.PEEK[HEADER]"
            goto L_0x0066
        L_0x00c1:
            if (r1 == 0) goto L_0x00c9
            java.lang.String r1 = "RFC822.HEADER"
        L_0x00c5:
            r5.append(r1)
            goto L_0x0069
        L_0x00c9:
            java.lang.String r1 = " RFC822.HEADER"
            goto L_0x00c5
        L_0x00cc:
            java.lang.String r1 = " RFC822.SIZE"
            goto L_0x0079
        L_0x00cf:
            r1 = 0
            com.sun.mail.iap.Response[] r1 = (com.sun.mail.iap.Response[]) r1     // Catch:{ all -> 0x00e8 }
            java.util.Vector r10 = new java.util.Vector     // Catch:{ all -> 0x00e8 }
            r10.<init>()     // Catch:{ all -> 0x00e8 }
            r0 = r17
            com.sun.mail.imap.protocol.IMAPProtocol r6 = r0.protocol     // Catch:{ ConnectionException -> 0x00eb, CommandFailedException -> 0x00f8, ProtocolException -> 0x00fb }
            java.lang.String r5 = r5.toString()     // Catch:{ ConnectionException -> 0x00eb, CommandFailedException -> 0x00f8, ProtocolException -> 0x00fb }
            com.sun.mail.iap.Response[] r1 = r6.fetch(r2, r5)     // Catch:{ ConnectionException -> 0x00eb, CommandFailedException -> 0x00f8, ProtocolException -> 0x00fb }
            r8 = r1
        L_0x00e4:
            if (r8 != 0) goto L_0x0106
            monitor-exit(r9)     // Catch:{ all -> 0x00e8 }
            goto L_0x00b2
        L_0x00e8:
            r1 = move-exception
            monitor-exit(r9)     // Catch:{ all -> 0x00e8 }
            throw r1
        L_0x00eb:
            r1 = move-exception
            javax.mail.FolderClosedException r2 = new javax.mail.FolderClosedException     // Catch:{ all -> 0x00e8 }
            java.lang.String r1 = r1.getMessage()     // Catch:{ all -> 0x00e8 }
            r0 = r17
            r2.<init>(r0, r1)     // Catch:{ all -> 0x00e8 }
            throw r2     // Catch:{ all -> 0x00e8 }
        L_0x00f8:
            r2 = move-exception
            r8 = r1
            goto L_0x00e4
        L_0x00fb:
            r1 = move-exception
            javax.mail.MessagingException r2 = new javax.mail.MessagingException     // Catch:{ all -> 0x00e8 }
            java.lang.String r3 = r1.getMessage()     // Catch:{ all -> 0x00e8 }
            r2.<init>(r3, r1)     // Catch:{ all -> 0x00e8 }
            throw r2     // Catch:{ all -> 0x00e8 }
        L_0x0106:
            r1 = 0
            r7 = r1
        L_0x0108:
            int r1 = r8.length     // Catch:{ all -> 0x00e8 }
            if (r7 < r1) goto L_0x011d
            int r1 = r10.size()     // Catch:{ all -> 0x00e8 }
            if (r1 == 0) goto L_0x011b
            com.sun.mail.iap.Response[] r1 = new com.sun.mail.iap.Response[r1]     // Catch:{ all -> 0x00e8 }
            r10.copyInto(r1)     // Catch:{ all -> 0x00e8 }
            r0 = r17
            r0.handleResponses(r1)     // Catch:{ all -> 0x00e8 }
        L_0x011b:
            monitor-exit(r9)     // Catch:{ all -> 0x00e8 }
            goto L_0x00b2
        L_0x011d:
            r1 = r8[r7]     // Catch:{ all -> 0x00e8 }
            if (r1 != 0) goto L_0x0125
        L_0x0121:
            int r1 = r7 + 1
            r7 = r1
            goto L_0x0108
        L_0x0125:
            r1 = r8[r7]     // Catch:{ all -> 0x00e8 }
            boolean r1 = r1 instanceof com.sun.mail.imap.protocol.FetchResponse     // Catch:{ all -> 0x00e8 }
            if (r1 != 0) goto L_0x0131
            r1 = r8[r7]     // Catch:{ all -> 0x00e8 }
            r10.addElement(r1)     // Catch:{ all -> 0x00e8 }
            goto L_0x0121
        L_0x0131:
            r1 = r8[r7]     // Catch:{ all -> 0x00e8 }
            com.sun.mail.imap.protocol.FetchResponse r1 = (com.sun.mail.imap.protocol.FetchResponse) r1     // Catch:{ all -> 0x00e8 }
            int r2 = r1.getNumber()     // Catch:{ all -> 0x00e8 }
            r0 = r17
            com.sun.mail.imap.IMAPMessage r11 = r0.getMessageBySeqNumber(r2)     // Catch:{ all -> 0x00e8 }
            int r12 = r1.getItemCount()     // Catch:{ all -> 0x00e8 }
            r5 = 0
            r2 = 0
            r6 = r2
        L_0x0146:
            if (r6 < r12) goto L_0x014e
            if (r5 == 0) goto L_0x0121
            r10.addElement(r1)     // Catch:{ all -> 0x00e8 }
            goto L_0x0121
        L_0x014e:
            com.sun.mail.imap.protocol.Item r2 = r1.getItem(r6)     // Catch:{ all -> 0x00e8 }
            boolean r13 = r2 instanceof javax.mail.Flags     // Catch:{ all -> 0x00e8 }
            if (r13 == 0) goto L_0x016e
            javax.mail.FetchProfile$Item r13 = javax.mail.FetchProfile.Item.FLAGS     // Catch:{ all -> 0x00e8 }
            r0 = r19
            boolean r13 = r0.contains(r13)     // Catch:{ all -> 0x00e8 }
            if (r13 == 0) goto L_0x0162
            if (r11 != 0) goto L_0x0168
        L_0x0162:
            r2 = 1
        L_0x0163:
            int r5 = r6 + 1
            r6 = r5
            r5 = r2
            goto L_0x0146
        L_0x0168:
            javax.mail.Flags r2 = (javax.mail.Flags) r2     // Catch:{ all -> 0x00e8 }
            r11.flags = r2     // Catch:{ all -> 0x00e8 }
            r2 = r5
            goto L_0x0163
        L_0x016e:
            boolean r13 = r2 instanceof com.sun.mail.imap.protocol.ENVELOPE     // Catch:{ all -> 0x00e8 }
            if (r13 == 0) goto L_0x0178
            com.sun.mail.imap.protocol.ENVELOPE r2 = (com.sun.mail.imap.protocol.ENVELOPE) r2     // Catch:{ all -> 0x00e8 }
            r11.envelope = r2     // Catch:{ all -> 0x00e8 }
            r2 = r5
            goto L_0x0163
        L_0x0178:
            boolean r13 = r2 instanceof com.sun.mail.imap.protocol.INTERNALDATE     // Catch:{ all -> 0x00e8 }
            if (r13 == 0) goto L_0x0186
            com.sun.mail.imap.protocol.INTERNALDATE r2 = (com.sun.mail.imap.protocol.INTERNALDATE) r2     // Catch:{ all -> 0x00e8 }
            java.util.Date r2 = r2.getDate()     // Catch:{ all -> 0x00e8 }
            r11.receivedDate = r2     // Catch:{ all -> 0x00e8 }
            r2 = r5
            goto L_0x0163
        L_0x0186:
            boolean r13 = r2 instanceof com.sun.mail.imap.protocol.RFC822SIZE     // Catch:{ all -> 0x00e8 }
            if (r13 == 0) goto L_0x0192
            com.sun.mail.imap.protocol.RFC822SIZE r2 = (com.sun.mail.imap.protocol.RFC822SIZE) r2     // Catch:{ all -> 0x00e8 }
            int r2 = r2.size     // Catch:{ all -> 0x00e8 }
            r11.size = r2     // Catch:{ all -> 0x00e8 }
            r2 = r5
            goto L_0x0163
        L_0x0192:
            boolean r13 = r2 instanceof com.sun.mail.imap.protocol.BODYSTRUCTURE     // Catch:{ all -> 0x00e8 }
            if (r13 == 0) goto L_0x019c
            com.sun.mail.imap.protocol.BODYSTRUCTURE r2 = (com.sun.mail.imap.protocol.BODYSTRUCTURE) r2     // Catch:{ all -> 0x00e8 }
            r11.bs = r2     // Catch:{ all -> 0x00e8 }
            r2 = r5
            goto L_0x0163
        L_0x019c:
            boolean r13 = r2 instanceof com.sun.mail.imap.protocol.UID     // Catch:{ all -> 0x00e8 }
            if (r13 == 0) goto L_0x01c5
            com.sun.mail.imap.protocol.UID r2 = (com.sun.mail.imap.protocol.UID) r2     // Catch:{ all -> 0x00e8 }
            long r13 = r2.uid     // Catch:{ all -> 0x00e8 }
            r11.uid = r13     // Catch:{ all -> 0x00e8 }
            r0 = r17
            java.util.Hashtable r13 = r0.uidTable     // Catch:{ all -> 0x00e8 }
            if (r13 != 0) goto L_0x01b5
            java.util.Hashtable r13 = new java.util.Hashtable     // Catch:{ all -> 0x00e8 }
            r13.<init>()     // Catch:{ all -> 0x00e8 }
            r0 = r17
            r0.uidTable = r13     // Catch:{ all -> 0x00e8 }
        L_0x01b5:
            r0 = r17
            java.util.Hashtable r13 = r0.uidTable     // Catch:{ all -> 0x00e8 }
            java.lang.Long r14 = new java.lang.Long     // Catch:{ all -> 0x00e8 }
            long r15 = r2.uid     // Catch:{ all -> 0x00e8 }
            r14.<init>(r15)     // Catch:{ all -> 0x00e8 }
            r13.put(r14, r11)     // Catch:{ all -> 0x00e8 }
            r2 = r5
            goto L_0x0163
        L_0x01c5:
            boolean r13 = r2 instanceof com.sun.mail.imap.protocol.RFC822DATA     // Catch:{ all -> 0x00e8 }
            if (r13 != 0) goto L_0x01cd
            boolean r13 = r2 instanceof com.sun.mail.imap.protocol.BODY     // Catch:{ all -> 0x00e8 }
            if (r13 == 0) goto L_0x0223
        L_0x01cd:
            boolean r13 = r2 instanceof com.sun.mail.imap.protocol.RFC822DATA     // Catch:{ all -> 0x00e8 }
            if (r13 == 0) goto L_0x01f0
            com.sun.mail.imap.protocol.RFC822DATA r2 = (com.sun.mail.imap.protocol.RFC822DATA) r2     // Catch:{ all -> 0x00e8 }
            java.io.ByteArrayInputStream r2 = r2.getByteArrayInputStream()     // Catch:{ all -> 0x00e8 }
        L_0x01d7:
            javax.mail.internet.InternetHeaders r13 = new javax.mail.internet.InternetHeaders     // Catch:{ all -> 0x00e8 }
            r13.<init>()     // Catch:{ all -> 0x00e8 }
            r13.load(r2)     // Catch:{ all -> 0x00e8 }
            javax.mail.internet.InternetHeaders r2 = r11.headers     // Catch:{ all -> 0x00e8 }
            if (r2 == 0) goto L_0x01e5
            if (r3 == 0) goto L_0x01f7
        L_0x01e5:
            r11.headers = r13     // Catch:{ all -> 0x00e8 }
        L_0x01e7:
            if (r3 == 0) goto L_0x021f
            r2 = 1
            r11.setHeadersLoaded(r2)     // Catch:{ all -> 0x00e8 }
            r2 = r5
            goto L_0x0163
        L_0x01f0:
            com.sun.mail.imap.protocol.BODY r2 = (com.sun.mail.imap.protocol.BODY) r2     // Catch:{ all -> 0x00e8 }
            java.io.ByteArrayInputStream r2 = r2.getByteArrayInputStream()     // Catch:{ all -> 0x00e8 }
            goto L_0x01d7
        L_0x01f7:
            java.util.Enumeration r13 = r13.getAllHeaders()     // Catch:{ all -> 0x00e8 }
        L_0x01fb:
            boolean r2 = r13.hasMoreElements()     // Catch:{ all -> 0x00e8 }
            if (r2 == 0) goto L_0x01e7
            java.lang.Object r2 = r13.nextElement()     // Catch:{ all -> 0x00e8 }
            javax.mail.Header r2 = (javax.mail.Header) r2     // Catch:{ all -> 0x00e8 }
            java.lang.String r14 = r2.getName()     // Catch:{ all -> 0x00e8 }
            boolean r14 = r11.isHeaderLoaded(r14)     // Catch:{ all -> 0x00e8 }
            if (r14 != 0) goto L_0x01fb
            javax.mail.internet.InternetHeaders r14 = r11.headers     // Catch:{ all -> 0x00e8 }
            java.lang.String r15 = r2.getName()     // Catch:{ all -> 0x00e8 }
            java.lang.String r2 = r2.getValue()     // Catch:{ all -> 0x00e8 }
            r14.addHeader(r15, r2)     // Catch:{ all -> 0x00e8 }
            goto L_0x01fb
        L_0x021f:
            r2 = 0
        L_0x0220:
            int r13 = r4.length     // Catch:{ all -> 0x00e8 }
            if (r2 < r13) goto L_0x0226
        L_0x0223:
            r2 = r5
            goto L_0x0163
        L_0x0226:
            r13 = r4[r2]     // Catch:{ all -> 0x00e8 }
            r11.setHeaderLoaded(r13)     // Catch:{ all -> 0x00e8 }
            int r2 = r2 + 1
            goto L_0x0220
        L_0x022e:
            r2 = r1
            goto L_0x007e
        L_0x0231:
            r3 = r2
            goto L_0x006b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sun.mail.imap.IMAPMessage.fetch(com.sun.mail.imap.IMAPFolder, javax.mail.Message[], javax.mail.FetchProfile):void");
    }

    private synchronized void loadEnvelope() throws MessagingException {
        if (this.envelope == null) {
            Response[] responseArr = null;
            synchronized (getMessageCacheLock()) {
                try {
                    IMAPProtocol protocol = getProtocol();
                    checkExpunged();
                    int sequenceNumber = getSequenceNumber();
                    Response[] fetch = protocol.fetch(sequenceNumber, EnvelopeCmd);
                    for (int i = 0; i < fetch.length; i++) {
                        if (fetch[i] != null && (fetch[i] instanceof FetchResponse) && ((FetchResponse) fetch[i]).getNumber() == sequenceNumber) {
                            FetchResponse fetchResponse = (FetchResponse) fetch[i];
                            int itemCount = fetchResponse.getItemCount();
                            for (int i2 = 0; i2 < itemCount; i2++) {
                                Item item = fetchResponse.getItem(i2);
                                if (item instanceof ENVELOPE) {
                                    this.envelope = (ENVELOPE) item;
                                } else if (item instanceof INTERNALDATE) {
                                    this.receivedDate = ((INTERNALDATE) item).getDate();
                                } else if (item instanceof RFC822SIZE) {
                                    this.size = ((RFC822SIZE) item).size;
                                }
                            }
                        }
                    }
                    protocol.notifyResponseHandlers(fetch);
                    protocol.handleResult(fetch[fetch.length - 1]);
                } catch (ConnectionException e) {
                    throw new FolderClosedException(this.folder, e.getMessage());
                } catch (ProtocolException e2) {
                    forceCheckExpunged();
                    throw new MessagingException(e2.getMessage(), e2);
                }
            }
            if (this.envelope == null) {
                throw new MessagingException("Failed to load IMAP envelope");
            }
        }
    }

    private static String craftHeaderCmd(IMAPProtocol iMAPProtocol, String[] strArr) {
        StringBuffer stringBuffer;
        if (iMAPProtocol.isREV1()) {
            stringBuffer = new StringBuffer("BODY.PEEK[HEADER.FIELDS (");
        } else {
            stringBuffer = new StringBuffer("RFC822.HEADER.LINES (");
        }
        for (int i = 0; i < strArr.length; i++) {
            if (i > 0) {
                stringBuffer.append(" ");
            }
            stringBuffer.append(strArr[i]);
        }
        if (iMAPProtocol.isREV1()) {
            stringBuffer.append(")]");
        } else {
            stringBuffer.append(")");
        }
        return stringBuffer.toString();
    }

    private synchronized void loadBODYSTRUCTURE() throws MessagingException {
        if (this.bs == null) {
            synchronized (getMessageCacheLock()) {
                try {
                    IMAPProtocol protocol = getProtocol();
                    checkExpunged();
                    this.bs = protocol.fetchBodyStructure(getSequenceNumber());
                    if (this.bs == null) {
                        forceCheckExpunged();
                        throw new MessagingException("Unable to load BODYSTRUCTURE");
                    }
                } catch (ConnectionException e) {
                    throw new FolderClosedException(this.folder, e.getMessage());
                } catch (ProtocolException e2) {
                    forceCheckExpunged();
                    throw new MessagingException(e2.getMessage(), e2);
                }
            }
        }
    }

    private synchronized void loadHeaders() throws MessagingException {
        if (!this.headersLoaded) {
            ByteArrayInputStream byteArrayInputStream = null;
            synchronized (getMessageCacheLock()) {
                try {
                    IMAPProtocol protocol = getProtocol();
                    checkExpunged();
                    if (protocol.isREV1()) {
                        BODY peekBody = protocol.peekBody(getSequenceNumber(), toSection("HEADER"));
                        if (peekBody != null) {
                            byteArrayInputStream = peekBody.getByteArrayInputStream();
                        }
                    } else {
                        RFC822DATA fetchRFC822 = protocol.fetchRFC822(getSequenceNumber(), "HEADER");
                        if (fetchRFC822 != null) {
                            byteArrayInputStream = fetchRFC822.getByteArrayInputStream();
                        }
                    }
                } catch (ConnectionException e) {
                    throw new FolderClosedException(this.folder, e.getMessage());
                } catch (ProtocolException e2) {
                    forceCheckExpunged();
                    throw new MessagingException(e2.getMessage(), e2);
                }
            }
            if (byteArrayInputStream == null) {
                throw new MessagingException("Cannot load header");
            }
            this.headers = new InternetHeaders(byteArrayInputStream);
            this.headersLoaded = true;
        }
    }

    private synchronized void loadFlags() throws MessagingException {
        if (this.flags == null) {
            synchronized (getMessageCacheLock()) {
                try {
                    IMAPProtocol protocol = getProtocol();
                    checkExpunged();
                    this.flags = protocol.fetchFlags(getSequenceNumber());
                } catch (ConnectionException e) {
                    throw new FolderClosedException(this.folder, e.getMessage());
                } catch (ProtocolException e2) {
                    forceCheckExpunged();
                    throw new MessagingException(e2.getMessage(), e2);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public synchronized boolean areHeadersLoaded() {
        return this.headersLoaded;
    }

    private synchronized void setHeadersLoaded(boolean z) {
        this.headersLoaded = z;
    }

    /* access modifiers changed from: private */
    public synchronized boolean isHeaderLoaded(String str) {
        boolean z;
        if (this.headersLoaded) {
            z = true;
        } else if (this.loadedHeaders != null) {
            z = this.loadedHeaders.containsKey(str.toUpperCase(Locale.ENGLISH));
        } else {
            z = false;
        }
        return z;
    }

    private synchronized void setHeaderLoaded(String str) {
        if (this.loadedHeaders == null) {
            this.loadedHeaders = new Hashtable(1);
        }
        this.loadedHeaders.put(str.toUpperCase(Locale.ENGLISH), str);
    }

    private String toSection(String str) {
        return this.sectionId == null ? str : String.valueOf(this.sectionId) + "." + str;
    }

    private InternetAddress[] aaclone(InternetAddress[] internetAddressArr) {
        if (internetAddressArr == null) {
            return null;
        }
        return (InternetAddress[]) internetAddressArr.clone();
    }

    /* access modifiers changed from: private */
    public Flags _getFlags() {
        return this.flags;
    }

    /* access modifiers changed from: private */
    public ENVELOPE _getEnvelope() {
        return this.envelope;
    }

    /* access modifiers changed from: private */
    public BODYSTRUCTURE _getBodyStructure() {
        return this.bs;
    }

    /* access modifiers changed from: package-private */
    public void _setFlags(Flags flags) {
        this.flags = flags;
    }

    /* access modifiers changed from: package-private */
    public Session _getSession() {
        return this.session;
    }
}
