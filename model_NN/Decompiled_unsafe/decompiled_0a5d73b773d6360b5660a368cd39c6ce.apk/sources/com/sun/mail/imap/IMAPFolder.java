package com.sun.mail.imap;

import com.sun.mail.iap.BadCommandException;
import com.sun.mail.iap.CommandFailedException;
import com.sun.mail.iap.ConnectionException;
import com.sun.mail.iap.ProtocolException;
import com.sun.mail.iap.Response;
import com.sun.mail.iap.ResponseHandler;
import com.sun.mail.imap.protocol.FetchResponse;
import com.sun.mail.imap.protocol.IMAPProtocol;
import com.sun.mail.imap.protocol.IMAPResponse;
import com.sun.mail.imap.protocol.ListInfo;
import com.sun.mail.imap.protocol.MailboxInfo;
import com.sun.mail.imap.protocol.MessageSet;
import com.sun.mail.imap.protocol.Status;
import com.sun.mail.imap.protocol.UID;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Date;
import java.util.Hashtable;
import java.util.NoSuchElementException;
import java.util.Vector;
import javax.mail.FetchProfile;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.FolderClosedException;
import javax.mail.FolderNotFoundException;
import javax.mail.Message;
import javax.mail.MessageRemovedException;
import javax.mail.MessagingException;
import javax.mail.Quota;
import javax.mail.ReadOnlyFolderException;
import javax.mail.StoreClosedException;
import javax.mail.UIDFolder;
import javax.mail.search.FlagTerm;
import javax.mail.search.SearchException;
import javax.mail.search.SearchTerm;

public class IMAPFolder extends Folder implements UIDFolder, ResponseHandler {
    static final /* synthetic */ boolean $assertionsDisabled = (!IMAPFolder.class.desiredAssertionStatus() ? true : $assertionsDisabled);
    private static final int ABORTING = 2;
    private static final int IDLE = 1;
    private static final int RUNNING = 0;
    protected static final char UNKNOWN_SEPARATOR = '￿';
    protected String[] attributes;
    protected Flags availableFlags;
    private Status cachedStatus;
    private long cachedStatusTime;
    private boolean connectionPoolDebug;
    private boolean debug;
    private boolean doExpungeNotification;
    protected boolean exists;
    protected String fullName;
    /* access modifiers changed from: private */
    public int idleState;
    protected boolean isNamespace;
    protected Vector messageCache;
    protected Object messageCacheLock;
    protected String name;
    private boolean opened;
    private PrintStream out;
    protected Flags permanentFlags;
    protected IMAPProtocol protocol;
    private int realTotal;
    private boolean reallyClosed;
    private int recent;
    protected char separator;
    private int total;
    protected int type;
    protected Hashtable uidTable;
    private long uidnext;
    private long uidvalidity;

    public interface ProtocolCommand {
        Object doCommand(IMAPProtocol iMAPProtocol) throws ProtocolException;
    }

    public static class FetchProfileItem extends FetchProfile.Item {
        public static final FetchProfileItem HEADERS = new FetchProfileItem("HEADERS");
        public static final FetchProfileItem SIZE = new FetchProfileItem("SIZE");

        protected FetchProfileItem(String str) {
            super(str);
        }
    }

    protected IMAPFolder(String str, char c, IMAPStore iMAPStore) {
        super(iMAPStore);
        int indexOf;
        this.exists = $assertionsDisabled;
        this.isNamespace = $assertionsDisabled;
        this.opened = $assertionsDisabled;
        this.reallyClosed = true;
        this.idleState = RUNNING;
        this.total = -1;
        this.recent = -1;
        this.realTotal = -1;
        this.uidvalidity = -1;
        this.uidnext = -1;
        this.doExpungeNotification = true;
        this.cachedStatus = null;
        this.cachedStatusTime = 0;
        this.debug = $assertionsDisabled;
        if (str == null) {
            throw new NullPointerException("Folder name is null");
        }
        this.fullName = str;
        this.separator = c;
        this.messageCacheLock = new Object();
        this.debug = iMAPStore.getSession().getDebug();
        this.connectionPoolDebug = iMAPStore.getConnectionPoolDebug();
        this.out = iMAPStore.getSession().getDebugOut();
        if (this.out == null) {
            this.out = System.out;
        }
        this.isNamespace = $assertionsDisabled;
        if (c != 65535 && c != 0 && (indexOf = this.fullName.indexOf(c)) > 0 && indexOf == this.fullName.length() - 1) {
            this.fullName = this.fullName.substring(RUNNING, indexOf);
            this.isNamespace = true;
        }
    }

    protected IMAPFolder(String str, char c, IMAPStore iMAPStore, boolean z) {
        this(str, c, iMAPStore);
        this.isNamespace = z;
    }

    protected IMAPFolder(ListInfo listInfo, IMAPStore iMAPStore) {
        this(listInfo.name, listInfo.separator, iMAPStore);
        if (listInfo.hasInferiors) {
            this.type |= 2;
        }
        if (listInfo.canOpen) {
            this.type |= 1;
        }
        this.exists = true;
        this.attributes = listInfo.attrs;
    }

    private void checkExists() throws MessagingException {
        if (!this.exists && !exists()) {
            throw new FolderNotFoundException(this, String.valueOf(this.fullName) + " not found");
        }
    }

    private void checkClosed() {
        if (this.opened) {
            throw new IllegalStateException("This operation is not allowed on an open folder");
        }
    }

    private void checkOpened() throws FolderClosedException {
        if (!$assertionsDisabled && !Thread.holdsLock(this)) {
            throw new AssertionError();
        } else if (this.opened) {
        } else {
            if (this.reallyClosed) {
                throw new IllegalStateException("This operation is not allowed on a closed folder");
            }
            throw new FolderClosedException(this, "Lost folder connection to server");
        }
    }

    private void checkRange(int i) throws MessagingException {
        if (i < 1) {
            throw new IndexOutOfBoundsException();
        } else if (i > this.total) {
            synchronized (this.messageCacheLock) {
                try {
                    keepConnectionAlive($assertionsDisabled);
                } catch (ConnectionException e) {
                    throw new FolderClosedException(this, e.getMessage());
                } catch (ProtocolException e2) {
                    throw new MessagingException(e2.getMessage(), e2);
                }
            }
            if (i > this.total) {
                throw new IndexOutOfBoundsException();
            }
        }
    }

    private void checkFlags(Flags flags) throws MessagingException {
        if (!$assertionsDisabled && !Thread.holdsLock(this)) {
            throw new AssertionError();
        } else if (this.mode != 2) {
            throw new IllegalStateException("Cannot change flags on READ_ONLY folder: " + this.fullName);
        }
    }

    public synchronized String getName() {
        if (this.name == null) {
            try {
                this.name = this.fullName.substring(this.fullName.lastIndexOf(getSeparator()) + 1);
            } catch (MessagingException e) {
            }
        }
        return this.name;
    }

    public synchronized String getFullName() {
        return this.fullName;
    }

    public synchronized Folder getParent() throws MessagingException {
        IMAPFolder defaultFolder;
        char separator2 = getSeparator();
        int lastIndexOf = this.fullName.lastIndexOf(separator2);
        if (lastIndexOf != -1) {
            defaultFolder = new IMAPFolder(this.fullName.substring(RUNNING, lastIndexOf), separator2, (IMAPStore) this.store);
        } else {
            defaultFolder = new DefaultFolder((IMAPStore) this.store);
        }
        return defaultFolder;
    }

    public synchronized boolean exists() throws MessagingException {
        final String str;
        ListInfo[] listInfoArr = null;
        if (!this.isNamespace || this.separator == 0) {
            str = this.fullName;
        } else {
            str = String.valueOf(this.fullName) + this.separator;
        }
        ListInfo[] listInfoArr2 = (ListInfo[]) doCommand(new ProtocolCommand() {
            public Object doCommand(IMAPProtocol iMAPProtocol) throws ProtocolException {
                return iMAPProtocol.list("", str);
            }
        });
        if (listInfoArr2 != null) {
            int findName = findName(listInfoArr2, str);
            this.fullName = listInfoArr2[findName].name;
            this.separator = listInfoArr2[findName].separator;
            int length = this.fullName.length();
            if (this.separator != 0 && length > 0 && this.fullName.charAt(length - 1) == this.separator) {
                this.fullName = this.fullName.substring(RUNNING, length - 1);
            }
            this.type = RUNNING;
            if (listInfoArr2[findName].hasInferiors) {
                this.type |= 2;
            }
            if (listInfoArr2[findName].canOpen) {
                this.type |= 1;
            }
            this.exists = true;
            this.attributes = listInfoArr2[findName].attrs;
        } else {
            this.exists = this.opened;
            this.attributes = null;
        }
        return this.exists;
    }

    private int findName(ListInfo[] listInfoArr, String str) {
        int i = RUNNING;
        while (i < listInfoArr.length && !listInfoArr[i].name.equals(str)) {
            i++;
        }
        return i >= listInfoArr.length ? RUNNING : i;
    }

    public Folder[] list(String str) throws MessagingException {
        return doList(str, $assertionsDisabled);
    }

    public Folder[] listSubscribed(String str) throws MessagingException {
        return doList(str, true);
    }

    private synchronized Folder[] doList(final String str, final boolean z) throws MessagingException {
        Folder[] folderArr;
        int i = RUNNING;
        synchronized (this) {
            checkExists();
            if (!isDirectory()) {
                folderArr = new Folder[RUNNING];
            } else {
                final char separator2 = getSeparator();
                ListInfo[] listInfoArr = (ListInfo[]) doCommandIgnoreFailure(new ProtocolCommand() {
                    public Object doCommand(IMAPProtocol iMAPProtocol) throws ProtocolException {
                        if (z) {
                            return iMAPProtocol.lsub("", String.valueOf(IMAPFolder.this.fullName) + separator2 + str);
                        }
                        return iMAPProtocol.list("", String.valueOf(IMAPFolder.this.fullName) + separator2 + str);
                    }
                });
                if (listInfoArr == null) {
                    folderArr = new Folder[RUNNING];
                } else {
                    if (listInfoArr.length > 0 && listInfoArr[RUNNING].name.equals(String.valueOf(this.fullName) + separator2)) {
                        i = 1;
                    }
                    IMAPFolder[] iMAPFolderArr = new IMAPFolder[(listInfoArr.length - i)];
                    for (int i2 = i; i2 < listInfoArr.length; i2++) {
                        iMAPFolderArr[i2 - i] = new IMAPFolder(listInfoArr[i2], (IMAPStore) this.store);
                    }
                    folderArr = iMAPFolderArr;
                }
            }
        }
        return folderArr;
    }

    public synchronized char getSeparator() throws MessagingException {
        if (this.separator == 65535) {
            ListInfo[] listInfoArr = (ListInfo[]) doCommand(new ProtocolCommand() {
                public Object doCommand(IMAPProtocol iMAPProtocol) throws ProtocolException {
                    if (iMAPProtocol.isREV1()) {
                        return iMAPProtocol.list(IMAPFolder.this.fullName, "");
                    }
                    return iMAPProtocol.list("", IMAPFolder.this.fullName);
                }
            });
            if (listInfoArr != null) {
                this.separator = listInfoArr[RUNNING].separator;
            } else {
                this.separator = '/';
            }
        }
        return this.separator;
    }

    public synchronized int getType() throws MessagingException {
        if (!this.opened) {
            checkExists();
        } else if (this.attributes == null) {
            exists();
        }
        return this.type;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v5, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: com.sun.mail.imap.protocol.ListInfo[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean isSubscribed() {
        /*
            r4 = this;
            monitor-enter(r4)
            r1 = 0
            com.sun.mail.imap.protocol.ListInfo[] r1 = (com.sun.mail.imap.protocol.ListInfo[]) r1     // Catch:{ all -> 0x0041 }
            boolean r2 = r4.isNamespace     // Catch:{ all -> 0x0041 }
            if (r2 == 0) goto L_0x003b
            char r2 = r4.separator     // Catch:{ all -> 0x0041 }
            if (r2 == 0) goto L_0x003b
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0041 }
            java.lang.String r3 = r4.fullName     // Catch:{ all -> 0x0041 }
            java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ all -> 0x0041 }
            r2.<init>(r3)     // Catch:{ all -> 0x0041 }
            char r3 = r4.separator     // Catch:{ all -> 0x0041 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0041 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0041 }
            r3 = r2
        L_0x0022:
            com.sun.mail.imap.IMAPFolder$4 r2 = new com.sun.mail.imap.IMAPFolder$4     // Catch:{ ProtocolException -> 0x0044 }
            r2.<init>(r3)     // Catch:{ ProtocolException -> 0x0044 }
            java.lang.Object r2 = r4.doProtocolCommand(r2)     // Catch:{ ProtocolException -> 0x0044 }
            r0 = r2
            com.sun.mail.imap.protocol.ListInfo[] r0 = (com.sun.mail.imap.protocol.ListInfo[]) r0     // Catch:{ ProtocolException -> 0x0044 }
            r1 = r0
        L_0x002f:
            if (r1 == 0) goto L_0x003f
            int r2 = r4.findName(r1, r3)     // Catch:{ all -> 0x0041 }
            r1 = r1[r2]     // Catch:{ all -> 0x0041 }
            boolean r1 = r1.canOpen     // Catch:{ all -> 0x0041 }
        L_0x0039:
            monitor-exit(r4)
            return r1
        L_0x003b:
            java.lang.String r2 = r4.fullName     // Catch:{ all -> 0x0041 }
            r3 = r2
            goto L_0x0022
        L_0x003f:
            r1 = 0
            goto L_0x0039
        L_0x0041:
            r1 = move-exception
            monitor-exit(r4)
            throw r1
        L_0x0044:
            r2 = move-exception
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sun.mail.imap.IMAPFolder.isSubscribed():boolean");
    }

    public synchronized void setSubscribed(final boolean z) throws MessagingException {
        doCommandIgnoreFailure(new ProtocolCommand() {
            public Object doCommand(IMAPProtocol iMAPProtocol) throws ProtocolException {
                if (z) {
                    iMAPProtocol.subscribe(IMAPFolder.this.fullName);
                    return null;
                }
                iMAPProtocol.unsubscribe(IMAPFolder.this.fullName);
                return null;
            }
        });
    }

    public synchronized boolean create(final int i) throws MessagingException {
        boolean z = $assertionsDisabled;
        synchronized (this) {
            final char separator2 = (i & 1) == 0 ? getSeparator() : RUNNING;
            if (doCommandIgnoreFailure(new ProtocolCommand() {
                public Object doCommand(IMAPProtocol iMAPProtocol) throws ProtocolException {
                    ListInfo[] list;
                    if ((i & 1) == 0) {
                        iMAPProtocol.create(String.valueOf(IMAPFolder.this.fullName) + separator2);
                    } else {
                        iMAPProtocol.create(IMAPFolder.this.fullName);
                        if (!((i & 2) == 0 || (list = iMAPProtocol.list("", IMAPFolder.this.fullName)) == null || list[IMAPFolder.RUNNING].hasInferiors)) {
                            iMAPProtocol.delete(IMAPFolder.this.fullName);
                            throw new ProtocolException("Unsupported type");
                        }
                    }
                    return Boolean.TRUE;
                }
            }) != null) {
                z = exists();
                if (z) {
                    notifyFolderListeners(1);
                }
            }
        }
        return z;
    }

    public synchronized boolean hasNewMessages() throws MessagingException {
        boolean z = true;
        synchronized (this) {
            if (this.opened) {
                synchronized (this.messageCacheLock) {
                    try {
                        keepConnectionAlive(true);
                        if (this.recent <= 0) {
                            z = RUNNING;
                        }
                    } catch (ConnectionException e) {
                        throw new FolderClosedException(this, e.getMessage());
                    } catch (ProtocolException e2) {
                        throw new MessagingException(e2.getMessage(), e2);
                    }
                }
            } else {
                checkExists();
                Boolean bool = (Boolean) doCommandIgnoreFailure(new ProtocolCommand() {
                    public Object doCommand(IMAPProtocol iMAPProtocol) throws ProtocolException {
                        ListInfo[] list = iMAPProtocol.list("", IMAPFolder.this.fullName);
                        if (list != null) {
                            if (list[IMAPFolder.RUNNING].changeState == 1) {
                                return Boolean.TRUE;
                            }
                            if (list[IMAPFolder.RUNNING].changeState == 2) {
                                return Boolean.FALSE;
                            }
                        }
                        if (IMAPFolder.this.getStatus().recent > 0) {
                            return Boolean.TRUE;
                        }
                        return Boolean.FALSE;
                    }
                });
                z = bool == null ? RUNNING : bool.booleanValue();
            }
        }
        return z;
    }

    public Folder getFolder(String str) throws MessagingException {
        if (this.attributes == null || isDirectory()) {
            char separator2 = getSeparator();
            return new IMAPFolder(String.valueOf(this.fullName) + separator2 + str, separator2, (IMAPStore) this.store);
        }
        throw new MessagingException("Cannot contain subfolders");
    }

    public synchronized boolean delete(boolean z) throws MessagingException {
        boolean z2 = $assertionsDisabled;
        synchronized (this) {
            checkClosed();
            if (z) {
                Folder[] list = list();
                for (int i = RUNNING; i < list.length; i++) {
                    list[i].delete(z);
                }
            }
            if (doCommandIgnoreFailure(new ProtocolCommand() {
                public Object doCommand(IMAPProtocol iMAPProtocol) throws ProtocolException {
                    iMAPProtocol.delete(IMAPFolder.this.fullName);
                    return Boolean.TRUE;
                }
            }) != null) {
                this.exists = $assertionsDisabled;
                this.attributes = null;
                notifyFolderListeners(2);
                z2 = true;
            }
        }
        return z2;
    }

    public synchronized boolean renameTo(final Folder folder) throws MessagingException {
        boolean z = $assertionsDisabled;
        synchronized (this) {
            checkClosed();
            checkExists();
            if (folder.getStore() != this.store) {
                throw new MessagingException("Can't rename across Stores");
            } else if (doCommandIgnoreFailure(new ProtocolCommand() {
                public Object doCommand(IMAPProtocol iMAPProtocol) throws ProtocolException {
                    iMAPProtocol.rename(IMAPFolder.this.fullName, folder.getFullName());
                    return Boolean.TRUE;
                }
            }) != null) {
                this.exists = $assertionsDisabled;
                this.attributes = null;
                notifyFolderRenamedListeners(folder);
                z = true;
            }
        }
        return z;
    }

    public synchronized void open(int i) throws MessagingException {
        MailboxInfo select;
        checkClosed();
        this.protocol = ((IMAPStore) this.store).getProtocol(this);
        synchronized (this.messageCacheLock) {
            this.protocol.addResponseHandler(this);
            if (i == 1) {
                try {
                    select = this.protocol.examine(this.fullName);
                } catch (CommandFailedException e) {
                    e = e;
                    releaseProtocol(true);
                    this.protocol = null;
                } catch (ProtocolException e2) {
                    try {
                        this.protocol.logout();
                    } catch (ProtocolException e3) {
                    }
                    releaseProtocol($assertionsDisabled);
                    this.protocol = null;
                    throw new MessagingException(e2.getMessage(), e2);
                }
            } else {
                select = this.protocol.select(this.fullName);
            }
            if (select.mode == i || (i == 2 && select.mode == 1 && ((IMAPStore) this.store).allowReadOnlySelect())) {
                this.opened = true;
                this.reallyClosed = $assertionsDisabled;
                this.mode = select.mode;
                this.availableFlags = select.availableFlags;
                this.permanentFlags = select.permanentFlags;
                int i2 = select.total;
                this.realTotal = i2;
                this.total = i2;
                this.recent = select.recent;
                this.uidvalidity = select.uidvalidity;
                this.uidnext = select.uidnext;
                this.messageCache = new Vector(this.total);
                for (int i3 = RUNNING; i3 < this.total; i3++) {
                    this.messageCache.addElement(new IMAPMessage(this, i3 + 1, i3 + 1));
                }
            } else {
                try {
                    this.protocol.close();
                    releaseProtocol(true);
                } catch (ProtocolException e4) {
                    releaseProtocol($assertionsDisabled);
                } catch (ProtocolException e5) {
                    this.protocol.logout();
                    releaseProtocol($assertionsDisabled);
                } catch (Throwable th) {
                }
                this.protocol = null;
                throw new ReadOnlyFolderException(this, "Cannot open in desired mode");
            }
        }
        e = null;
        if (e != null) {
            checkExists();
            if ((this.type & 1) == 0) {
                throw new MessagingException("folder cannot contain messages");
            }
            throw new MessagingException(e.getMessage(), e);
        }
        this.exists = true;
        this.attributes = null;
        this.type = 1;
        notifyConnectionListeners(1);
    }

    public synchronized void fetch(Message[] messageArr, FetchProfile fetchProfile) throws MessagingException {
        checkOpened();
        IMAPMessage.fetch(this, messageArr, fetchProfile);
    }

    public synchronized void setFlags(Message[] messageArr, Flags flags, boolean z) throws MessagingException {
        checkOpened();
        checkFlags(flags);
        if (messageArr.length != 0) {
            synchronized (this.messageCacheLock) {
                try {
                    IMAPProtocol protocol2 = getProtocol();
                    MessageSet[] messageSet = Utility.toMessageSet(messageArr, null);
                    if (messageSet == null) {
                        throw new MessageRemovedException("Messages have been removed");
                    }
                    protocol2.storeFlags(messageSet, flags, z);
                } catch (ConnectionException e) {
                    throw new FolderClosedException(this, e.getMessage());
                } catch (ProtocolException e2) {
                    throw new MessagingException(e2.getMessage(), e2);
                }
            }
        }
    }

    public synchronized void close(boolean z) throws MessagingException {
        close(z, $assertionsDisabled);
    }

    public synchronized void forceClose() throws MessagingException {
        close($assertionsDisabled, true);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:74:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void close(boolean r5, boolean r6) throws javax.mail.MessagingException {
        /*
            r4 = this;
            boolean r0 = com.sun.mail.imap.IMAPFolder.$assertionsDisabled
            if (r0 != 0) goto L_0x0010
            boolean r0 = java.lang.Thread.holdsLock(r4)
            if (r0 != 0) goto L_0x0010
            java.lang.AssertionError r0 = new java.lang.AssertionError
            r0.<init>()
            throw r0
        L_0x0010:
            java.lang.Object r1 = r4.messageCacheLock
            monitor-enter(r1)
            boolean r0 = r4.opened     // Catch:{ all -> 0x0023 }
            if (r0 != 0) goto L_0x0026
            boolean r0 = r4.reallyClosed     // Catch:{ all -> 0x0023 }
            if (r0 == 0) goto L_0x0026
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0023 }
            java.lang.String r2 = "This operation is not allowed on a closed folder"
            r0.<init>(r2)     // Catch:{ all -> 0x0023 }
            throw r0     // Catch:{ all -> 0x0023 }
        L_0x0023:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0023 }
            throw r0
        L_0x0026:
            r0 = 1
            r4.reallyClosed = r0     // Catch:{ all -> 0x0023 }
            boolean r0 = r4.opened     // Catch:{ all -> 0x0023 }
            if (r0 != 0) goto L_0x002f
            monitor-exit(r1)     // Catch:{ all -> 0x0023 }
        L_0x002e:
            return
        L_0x002f:
            r4.waitIfIdle()     // Catch:{ ProtocolException -> 0x008d }
            if (r6 == 0) goto L_0x0067
            boolean r0 = r4.debug     // Catch:{ ProtocolException -> 0x008d }
            if (r0 == 0) goto L_0x0054
            java.io.PrintStream r0 = r4.out     // Catch:{ ProtocolException -> 0x008d }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ ProtocolException -> 0x008d }
            java.lang.String r3 = "DEBUG: forcing folder "
            r2.<init>(r3)     // Catch:{ ProtocolException -> 0x008d }
            java.lang.String r3 = r4.fullName     // Catch:{ ProtocolException -> 0x008d }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ ProtocolException -> 0x008d }
            java.lang.String r3 = " to close"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ ProtocolException -> 0x008d }
            java.lang.String r2 = r2.toString()     // Catch:{ ProtocolException -> 0x008d }
            r0.println(r2)     // Catch:{ ProtocolException -> 0x008d }
        L_0x0054:
            com.sun.mail.imap.protocol.IMAPProtocol r0 = r4.protocol     // Catch:{ ProtocolException -> 0x008d }
            if (r0 == 0) goto L_0x005d
            com.sun.mail.imap.protocol.IMAPProtocol r0 = r4.protocol     // Catch:{ ProtocolException -> 0x008d }
            r0.disconnect()     // Catch:{ ProtocolException -> 0x008d }
        L_0x005d:
            boolean r0 = r4.opened     // Catch:{ all -> 0x0023 }
            if (r0 == 0) goto L_0x0065
            r0 = 1
            r4.cleanup(r0)     // Catch:{ all -> 0x0023 }
        L_0x0065:
            monitor-exit(r1)     // Catch:{ all -> 0x0023 }
            goto L_0x002e
        L_0x0067:
            javax.mail.Store r0 = r4.store     // Catch:{ ProtocolException -> 0x008d }
            com.sun.mail.imap.IMAPStore r0 = (com.sun.mail.imap.IMAPStore) r0     // Catch:{ ProtocolException -> 0x008d }
            boolean r0 = r0.isConnectionPoolFull()     // Catch:{ ProtocolException -> 0x008d }
            if (r0 == 0) goto L_0x00a2
            boolean r0 = r4.debug     // Catch:{ ProtocolException -> 0x008d }
            if (r0 == 0) goto L_0x007c
            java.io.PrintStream r0 = r4.out     // Catch:{ ProtocolException -> 0x008d }
            java.lang.String r2 = "DEBUG: pool is full, not adding an Authenticated connection"
            r0.println(r2)     // Catch:{ ProtocolException -> 0x008d }
        L_0x007c:
            if (r5 == 0) goto L_0x0083
            com.sun.mail.imap.protocol.IMAPProtocol r0 = r4.protocol     // Catch:{ ProtocolException -> 0x008d }
            r0.close()     // Catch:{ ProtocolException -> 0x008d }
        L_0x0083:
            com.sun.mail.imap.protocol.IMAPProtocol r0 = r4.protocol     // Catch:{ ProtocolException -> 0x008d }
            if (r0 == 0) goto L_0x005d
            com.sun.mail.imap.protocol.IMAPProtocol r0 = r4.protocol     // Catch:{ ProtocolException -> 0x008d }
            r0.logout()     // Catch:{ ProtocolException -> 0x008d }
            goto L_0x005d
        L_0x008d:
            r0 = move-exception
            javax.mail.MessagingException r2 = new javax.mail.MessagingException     // Catch:{ all -> 0x0098 }
            java.lang.String r3 = r0.getMessage()     // Catch:{ all -> 0x0098 }
            r2.<init>(r3, r0)     // Catch:{ all -> 0x0098 }
            throw r2     // Catch:{ all -> 0x0098 }
        L_0x0098:
            r0 = move-exception
            boolean r2 = r4.opened     // Catch:{ all -> 0x0023 }
            if (r2 == 0) goto L_0x00a1
            r2 = 1
            r4.cleanup(r2)     // Catch:{ all -> 0x0023 }
        L_0x00a1:
            throw r0     // Catch:{ all -> 0x0023 }
        L_0x00a2:
            if (r5 != 0) goto L_0x00b0
            int r0 = r4.mode     // Catch:{ ProtocolException -> 0x008d }
            r2 = 2
            if (r0 != r2) goto L_0x00b0
            com.sun.mail.imap.protocol.IMAPProtocol r0 = r4.protocol     // Catch:{ ProtocolException -> 0x00ba }
            java.lang.String r2 = r4.fullName     // Catch:{ ProtocolException -> 0x00ba }
            r0.examine(r2)     // Catch:{ ProtocolException -> 0x00ba }
        L_0x00b0:
            com.sun.mail.imap.protocol.IMAPProtocol r0 = r4.protocol     // Catch:{ ProtocolException -> 0x008d }
            if (r0 == 0) goto L_0x005d
            com.sun.mail.imap.protocol.IMAPProtocol r0 = r4.protocol     // Catch:{ ProtocolException -> 0x008d }
            r0.close()     // Catch:{ ProtocolException -> 0x008d }
            goto L_0x005d
        L_0x00ba:
            r0 = move-exception
            com.sun.mail.imap.protocol.IMAPProtocol r0 = r4.protocol     // Catch:{ ProtocolException -> 0x008d }
            if (r0 == 0) goto L_0x00b0
            com.sun.mail.imap.protocol.IMAPProtocol r0 = r4.protocol     // Catch:{ ProtocolException -> 0x008d }
            r0.disconnect()     // Catch:{ ProtocolException -> 0x008d }
            goto L_0x00b0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sun.mail.imap.IMAPFolder.close(boolean, boolean):void");
    }

    private void cleanup(boolean z) {
        releaseProtocol(z);
        this.protocol = null;
        this.messageCache = null;
        this.uidTable = null;
        this.exists = $assertionsDisabled;
        this.attributes = null;
        this.opened = $assertionsDisabled;
        this.idleState = RUNNING;
        notifyConnectionListeners(3);
    }

    public synchronized boolean isOpen() {
        synchronized (this.messageCacheLock) {
            if (this.opened) {
                try {
                    keepConnectionAlive($assertionsDisabled);
                } catch (ProtocolException e) {
                }
            }
        }
        return this.opened;
    }

    public synchronized Flags getPermanentFlags() {
        return (Flags) this.permanentFlags.clone();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0011, code lost:
        r1 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        r1 = getStoreProtocol();
        r0 = r1.examine(r4.fullName);
        r1.close();
        r0 = r0.total;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        releaseStoreProtocol(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0028, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0032, code lost:
        throw new javax.mail.MessagingException(r0.getMessage(), r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0033, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        releaseStoreProtocol(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0037, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0038, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0044, code lost:
        throw new javax.mail.StoreClosedException(r4.store, r0.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0045, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x004f, code lost:
        throw new javax.mail.MessagingException(r0.getMessage(), r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x005e, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0068, code lost:
        throw new javax.mail.FolderClosedException(r4, r0.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0069, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0073, code lost:
        throw new javax.mail.MessagingException(r0.getMessage(), r0);
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:5:0x0008, B:35:0x0054] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized int getMessageCount() throws javax.mail.MessagingException {
        /*
            r4 = this;
            monitor-enter(r4)
            boolean r0 = r4.opened     // Catch:{ all -> 0x0025 }
            if (r0 != 0) goto L_0x0050
            r4.checkExists()     // Catch:{ all -> 0x0025 }
            com.sun.mail.imap.protocol.Status r0 = r4.getStatus()     // Catch:{ BadCommandException -> 0x0010, ConnectionException -> 0x0038, ProtocolException -> 0x0045 }
            int r0 = r0.total     // Catch:{ BadCommandException -> 0x0010, ConnectionException -> 0x0038, ProtocolException -> 0x0045 }
        L_0x000e:
            monitor-exit(r4)
            return r0
        L_0x0010:
            r0 = move-exception
            r1 = 0
            com.sun.mail.imap.protocol.IMAPProtocol r1 = r4.getStoreProtocol()     // Catch:{ ProtocolException -> 0x0028 }
            java.lang.String r0 = r4.fullName     // Catch:{ ProtocolException -> 0x0028 }
            com.sun.mail.imap.protocol.MailboxInfo r0 = r1.examine(r0)     // Catch:{ ProtocolException -> 0x0028 }
            r1.close()     // Catch:{ ProtocolException -> 0x0028 }
            int r0 = r0.total     // Catch:{ ProtocolException -> 0x0028 }
            r4.releaseStoreProtocol(r1)     // Catch:{ all -> 0x0025 }
            goto L_0x000e
        L_0x0025:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x0028:
            r0 = move-exception
            javax.mail.MessagingException r2 = new javax.mail.MessagingException     // Catch:{ all -> 0x0033 }
            java.lang.String r3 = r0.getMessage()     // Catch:{ all -> 0x0033 }
            r2.<init>(r3, r0)     // Catch:{ all -> 0x0033 }
            throw r2     // Catch:{ all -> 0x0033 }
        L_0x0033:
            r0 = move-exception
            r4.releaseStoreProtocol(r1)     // Catch:{ all -> 0x0025 }
            throw r0     // Catch:{ all -> 0x0025 }
        L_0x0038:
            r0 = move-exception
            javax.mail.StoreClosedException r1 = new javax.mail.StoreClosedException     // Catch:{ all -> 0x0025 }
            javax.mail.Store r2 = r4.store     // Catch:{ all -> 0x0025 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0025 }
            r1.<init>(r2, r0)     // Catch:{ all -> 0x0025 }
            throw r1     // Catch:{ all -> 0x0025 }
        L_0x0045:
            r0 = move-exception
            javax.mail.MessagingException r1 = new javax.mail.MessagingException     // Catch:{ all -> 0x0025 }
            java.lang.String r2 = r0.getMessage()     // Catch:{ all -> 0x0025 }
            r1.<init>(r2, r0)     // Catch:{ all -> 0x0025 }
            throw r1     // Catch:{ all -> 0x0025 }
        L_0x0050:
            java.lang.Object r1 = r4.messageCacheLock     // Catch:{ all -> 0x0025 }
            monitor-enter(r1)     // Catch:{ all -> 0x0025 }
            r0 = 1
            r4.keepConnectionAlive(r0)     // Catch:{ ConnectionException -> 0x005e, ProtocolException -> 0x0069 }
            int r0 = r4.total     // Catch:{ ConnectionException -> 0x005e, ProtocolException -> 0x0069 }
            monitor-exit(r1)     // Catch:{ all -> 0x005b }
            goto L_0x000e
        L_0x005b:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x005b }
            throw r0     // Catch:{ all -> 0x0025 }
        L_0x005e:
            r0 = move-exception
            javax.mail.FolderClosedException r2 = new javax.mail.FolderClosedException     // Catch:{ all -> 0x005b }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x005b }
            r2.<init>(r4, r0)     // Catch:{ all -> 0x005b }
            throw r2     // Catch:{ all -> 0x005b }
        L_0x0069:
            r0 = move-exception
            javax.mail.MessagingException r2 = new javax.mail.MessagingException     // Catch:{ all -> 0x005b }
            java.lang.String r3 = r0.getMessage()     // Catch:{ all -> 0x005b }
            r2.<init>(r3, r0)     // Catch:{ all -> 0x005b }
            throw r2     // Catch:{ all -> 0x005b }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sun.mail.imap.IMAPFolder.getMessageCount():int");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0011, code lost:
        r1 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        r1 = getStoreProtocol();
        r0 = r1.examine(r4.fullName);
        r1.close();
        r0 = r0.recent;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        releaseStoreProtocol(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0028, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0032, code lost:
        throw new javax.mail.MessagingException(r0.getMessage(), r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0033, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        releaseStoreProtocol(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0037, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0038, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0044, code lost:
        throw new javax.mail.StoreClosedException(r4.store, r0.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0045, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x004f, code lost:
        throw new javax.mail.MessagingException(r0.getMessage(), r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x005e, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0068, code lost:
        throw new javax.mail.FolderClosedException(r4, r0.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0069, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0073, code lost:
        throw new javax.mail.MessagingException(r0.getMessage(), r0);
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:5:0x0008, B:35:0x0054] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized int getNewMessageCount() throws javax.mail.MessagingException {
        /*
            r4 = this;
            monitor-enter(r4)
            boolean r0 = r4.opened     // Catch:{ all -> 0x0025 }
            if (r0 != 0) goto L_0x0050
            r4.checkExists()     // Catch:{ all -> 0x0025 }
            com.sun.mail.imap.protocol.Status r0 = r4.getStatus()     // Catch:{ BadCommandException -> 0x0010, ConnectionException -> 0x0038, ProtocolException -> 0x0045 }
            int r0 = r0.recent     // Catch:{ BadCommandException -> 0x0010, ConnectionException -> 0x0038, ProtocolException -> 0x0045 }
        L_0x000e:
            monitor-exit(r4)
            return r0
        L_0x0010:
            r0 = move-exception
            r1 = 0
            com.sun.mail.imap.protocol.IMAPProtocol r1 = r4.getStoreProtocol()     // Catch:{ ProtocolException -> 0x0028 }
            java.lang.String r0 = r4.fullName     // Catch:{ ProtocolException -> 0x0028 }
            com.sun.mail.imap.protocol.MailboxInfo r0 = r1.examine(r0)     // Catch:{ ProtocolException -> 0x0028 }
            r1.close()     // Catch:{ ProtocolException -> 0x0028 }
            int r0 = r0.recent     // Catch:{ ProtocolException -> 0x0028 }
            r4.releaseStoreProtocol(r1)     // Catch:{ all -> 0x0025 }
            goto L_0x000e
        L_0x0025:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x0028:
            r0 = move-exception
            javax.mail.MessagingException r2 = new javax.mail.MessagingException     // Catch:{ all -> 0x0033 }
            java.lang.String r3 = r0.getMessage()     // Catch:{ all -> 0x0033 }
            r2.<init>(r3, r0)     // Catch:{ all -> 0x0033 }
            throw r2     // Catch:{ all -> 0x0033 }
        L_0x0033:
            r0 = move-exception
            r4.releaseStoreProtocol(r1)     // Catch:{ all -> 0x0025 }
            throw r0     // Catch:{ all -> 0x0025 }
        L_0x0038:
            r0 = move-exception
            javax.mail.StoreClosedException r1 = new javax.mail.StoreClosedException     // Catch:{ all -> 0x0025 }
            javax.mail.Store r2 = r4.store     // Catch:{ all -> 0x0025 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0025 }
            r1.<init>(r2, r0)     // Catch:{ all -> 0x0025 }
            throw r1     // Catch:{ all -> 0x0025 }
        L_0x0045:
            r0 = move-exception
            javax.mail.MessagingException r1 = new javax.mail.MessagingException     // Catch:{ all -> 0x0025 }
            java.lang.String r2 = r0.getMessage()     // Catch:{ all -> 0x0025 }
            r1.<init>(r2, r0)     // Catch:{ all -> 0x0025 }
            throw r1     // Catch:{ all -> 0x0025 }
        L_0x0050:
            java.lang.Object r1 = r4.messageCacheLock     // Catch:{ all -> 0x0025 }
            monitor-enter(r1)     // Catch:{ all -> 0x0025 }
            r0 = 1
            r4.keepConnectionAlive(r0)     // Catch:{ ConnectionException -> 0x005e, ProtocolException -> 0x0069 }
            int r0 = r4.recent     // Catch:{ ConnectionException -> 0x005e, ProtocolException -> 0x0069 }
            monitor-exit(r1)     // Catch:{ all -> 0x005b }
            goto L_0x000e
        L_0x005b:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x005b }
            throw r0     // Catch:{ all -> 0x0025 }
        L_0x005e:
            r0 = move-exception
            javax.mail.FolderClosedException r2 = new javax.mail.FolderClosedException     // Catch:{ all -> 0x005b }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x005b }
            r2.<init>(r4, r0)     // Catch:{ all -> 0x005b }
            throw r2     // Catch:{ all -> 0x005b }
        L_0x0069:
            r0 = move-exception
            javax.mail.MessagingException r2 = new javax.mail.MessagingException     // Catch:{ all -> 0x005b }
            java.lang.String r3 = r0.getMessage()     // Catch:{ all -> 0x005b }
            r2.<init>(r3, r0)     // Catch:{ all -> 0x005b }
            throw r2     // Catch:{ all -> 0x005b }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sun.mail.imap.IMAPFolder.getNewMessageCount():int");
    }

    public synchronized int getUnreadMessageCount() throws MessagingException {
        int length;
        if (!this.opened) {
            checkExists();
            try {
                length = getStatus().unseen;
            } catch (BadCommandException e) {
                length = -1;
            } catch (ConnectionException e2) {
                throw new StoreClosedException(this.store, e2.getMessage());
            } catch (ProtocolException e3) {
                throw new MessagingException(e3.getMessage(), e3);
            }
        } else {
            Flags flags = new Flags();
            flags.add(Flags.Flag.SEEN);
            try {
                synchronized (this.messageCacheLock) {
                    length = getProtocol().search(new FlagTerm(flags, $assertionsDisabled)).length;
                }
            } catch (ConnectionException e4) {
                throw new FolderClosedException(this, e4.getMessage());
            } catch (ProtocolException e5) {
                throw new MessagingException(e5.getMessage(), e5);
            }
        }
        return length;
    }

    public synchronized int getDeletedMessageCount() throws MessagingException {
        int length;
        if (!this.opened) {
            checkExists();
            length = -1;
        } else {
            Flags flags = new Flags();
            flags.add(Flags.Flag.DELETED);
            try {
                synchronized (this.messageCacheLock) {
                    length = getProtocol().search(new FlagTerm(flags, true)).length;
                }
            } catch (ConnectionException e) {
                throw new FolderClosedException(this, e.getMessage());
            } catch (ProtocolException e2) {
                throw new MessagingException(e2.getMessage(), e2);
            }
        }
        return length;
    }

    /* access modifiers changed from: private */
    public Status getStatus() throws ProtocolException {
        IMAPProtocol iMAPProtocol = null;
        int statusCacheTimeout = ((IMAPStore) this.store).getStatusCacheTimeout();
        if (statusCacheTimeout > 0 && this.cachedStatus != null && System.currentTimeMillis() - this.cachedStatusTime < ((long) statusCacheTimeout)) {
            return this.cachedStatus;
        }
        try {
            iMAPProtocol = getStoreProtocol();
            Status status = iMAPProtocol.status(this.fullName, null);
            if (statusCacheTimeout > 0) {
                this.cachedStatus = status;
                this.cachedStatusTime = System.currentTimeMillis();
            }
            return status;
        } finally {
            releaseStoreProtocol(iMAPProtocol);
        }
    }

    public synchronized Message getMessage(int i) throws MessagingException {
        checkOpened();
        checkRange(i);
        return (Message) this.messageCache.elementAt(i - 1);
    }

    public synchronized void appendMessages(Message[] messageArr) throws MessagingException {
        checkExists();
        int appendBufferSize = ((IMAPStore) this.store).getAppendBufferSize();
        for (int i = RUNNING; i < messageArr.length; i++) {
            Message message = messageArr[i];
            try {
                final MessageLiteral messageLiteral = new MessageLiteral(message, message.getSize() > appendBufferSize ? RUNNING : appendBufferSize);
                final Date receivedDate = message.getReceivedDate();
                if (receivedDate == null) {
                    receivedDate = message.getSentDate();
                }
                final Flags flags = message.getFlags();
                doCommand(new ProtocolCommand() {
                    public Object doCommand(IMAPProtocol iMAPProtocol) throws ProtocolException {
                        iMAPProtocol.append(IMAPFolder.this.fullName, flags, receivedDate, messageLiteral);
                        return null;
                    }
                });
            } catch (IOException e) {
                throw new MessagingException("IOException while appending messages", e);
            } catch (MessageRemovedException e2) {
            }
        }
    }

    public synchronized AppendUID[] appendUIDMessages(Message[] messageArr) throws MessagingException {
        AppendUID[] appendUIDArr;
        checkExists();
        int appendBufferSize = ((IMAPStore) this.store).getAppendBufferSize();
        appendUIDArr = new AppendUID[messageArr.length];
        for (int i = RUNNING; i < messageArr.length; i++) {
            Message message = messageArr[i];
            try {
                final MessageLiteral messageLiteral = new MessageLiteral(message, message.getSize() > appendBufferSize ? RUNNING : appendBufferSize);
                final Date receivedDate = message.getReceivedDate();
                if (receivedDate == null) {
                    receivedDate = message.getSentDate();
                }
                final Flags flags = message.getFlags();
                appendUIDArr[i] = (AppendUID) doCommand(new ProtocolCommand() {
                    public Object doCommand(IMAPProtocol iMAPProtocol) throws ProtocolException {
                        return iMAPProtocol.appenduid(IMAPFolder.this.fullName, flags, receivedDate, messageLiteral);
                    }
                });
            } catch (IOException e) {
                throw new MessagingException("IOException while appending messages", e);
            } catch (MessageRemovedException e2) {
            }
        }
        return appendUIDArr;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v0, resolved type: javax.mail.internet.MimeMessage[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized javax.mail.Message[] addMessages(javax.mail.Message[] r9) throws javax.mail.MessagingException {
        /*
            r8 = this;
            monitor-enter(r8)
            r8.checkOpened()     // Catch:{ all -> 0x0028 }
            int r0 = r9.length     // Catch:{ all -> 0x0028 }
            javax.mail.internet.MimeMessage[] r1 = new javax.mail.internet.MimeMessage[r0]     // Catch:{ all -> 0x0028 }
            com.sun.mail.imap.AppendUID[] r2 = r8.appendUIDMessages(r9)     // Catch:{ all -> 0x0028 }
            r0 = 0
        L_0x000c:
            int r3 = r2.length     // Catch:{ all -> 0x0028 }
            if (r0 < r3) goto L_0x0011
            monitor-exit(r8)
            return r1
        L_0x0011:
            r3 = r2[r0]     // Catch:{ all -> 0x0028 }
            if (r3 == 0) goto L_0x0025
            long r4 = r3.uidvalidity     // Catch:{ all -> 0x0028 }
            long r6 = r8.uidvalidity     // Catch:{ all -> 0x0028 }
            int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r4 != 0) goto L_0x0025
            long r3 = r3.uid     // Catch:{ MessagingException -> 0x002b }
            javax.mail.Message r3 = r8.getMessageByUID(r3)     // Catch:{ MessagingException -> 0x002b }
            r1[r0] = r3     // Catch:{ MessagingException -> 0x002b }
        L_0x0025:
            int r0 = r0 + 1
            goto L_0x000c
        L_0x0028:
            r0 = move-exception
            monitor-exit(r8)
            throw r0
        L_0x002b:
            r3 = move-exception
            goto L_0x0025
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sun.mail.imap.IMAPFolder.addMessages(javax.mail.Message[]):javax.mail.Message[]");
    }

    public synchronized void copyMessages(Message[] messageArr, Folder folder) throws MessagingException {
        checkOpened();
        if (messageArr.length != 0) {
            if (folder.getStore() == this.store) {
                synchronized (this.messageCacheLock) {
                    try {
                        IMAPProtocol protocol2 = getProtocol();
                        MessageSet[] messageSet = Utility.toMessageSet(messageArr, null);
                        if (messageSet == null) {
                            throw new MessageRemovedException("Messages have been removed");
                        }
                        protocol2.copy(messageSet, folder.getFullName());
                    } catch (CommandFailedException e) {
                        if (e.getMessage().indexOf("TRYCREATE") != -1) {
                            throw new FolderNotFoundException(folder, String.valueOf(folder.getFullName()) + " does not exist");
                        }
                        throw new MessagingException(e.getMessage(), e);
                    } catch (ConnectionException e2) {
                        throw new FolderClosedException(this, e2.getMessage());
                    } catch (ProtocolException e3) {
                        throw new MessagingException(e3.getMessage(), e3);
                    }
                }
            } else {
                super.copyMessages(messageArr, folder);
            }
        }
    }

    public synchronized Message[] expunge() throws MessagingException {
        return expunge(null);
    }

    public synchronized Message[] expunge(Message[] messageArr) throws MessagingException {
        Message[] messageArr2;
        checkOpened();
        Vector vector = new Vector();
        if (messageArr != null) {
            FetchProfile fetchProfile = new FetchProfile();
            fetchProfile.add(UIDFolder.FetchProfileItem.UID);
            fetch(messageArr, fetchProfile);
        }
        synchronized (this.messageCacheLock) {
            this.doExpungeNotification = $assertionsDisabled;
            try {
                IMAPProtocol protocol2 = getProtocol();
                if (messageArr != null) {
                    protocol2.uidexpunge(Utility.toUIDSet(messageArr));
                } else {
                    protocol2.expunge();
                }
                this.doExpungeNotification = true;
                int i = RUNNING;
                while (i < this.messageCache.size()) {
                    IMAPMessage iMAPMessage = (IMAPMessage) this.messageCache.elementAt(i);
                    if (iMAPMessage.isExpunged()) {
                        vector.addElement(iMAPMessage);
                        this.messageCache.removeElementAt(i);
                        if (this.uidTable != null) {
                            long uid = iMAPMessage.getUID();
                            if (uid != -1) {
                                this.uidTable.remove(new Long(uid));
                            }
                        }
                    } else {
                        iMAPMessage.setMessageNumber(iMAPMessage.getSequenceNumber());
                        i++;
                    }
                }
            } catch (CommandFailedException e) {
                if (this.mode != 2) {
                    throw new IllegalStateException("Cannot expunge READ_ONLY folder: " + this.fullName);
                }
                throw new MessagingException(e.getMessage(), e);
            } catch (ConnectionException e2) {
                throw new FolderClosedException(this, e2.getMessage());
            } catch (ProtocolException e3) {
                throw new MessagingException(e3.getMessage(), e3);
            } catch (Throwable th) {
                this.doExpungeNotification = true;
                throw th;
            }
        }
        this.total = this.messageCache.size();
        messageArr2 = new Message[vector.size()];
        vector.copyInto(messageArr2);
        if (messageArr2.length > 0) {
            notifyMessageRemovedListeners(true, messageArr2);
        }
        return messageArr2;
    }

    public synchronized Message[] search(SearchTerm searchTerm) throws MessagingException {
        Message[] search;
        checkOpened();
        try {
            search = null;
            synchronized (this.messageCacheLock) {
                int[] search2 = getProtocol().search(searchTerm);
                if (search2 != null) {
                    search = new IMAPMessage[search2.length];
                    for (int i = RUNNING; i < search2.length; i++) {
                        search[i] = getMessageBySeqNumber(search2[i]);
                    }
                }
            }
        } catch (CommandFailedException e) {
            search = super.search(searchTerm);
        } catch (SearchException e2) {
            search = super.search(searchTerm);
        } catch (ConnectionException e3) {
            throw new FolderClosedException(this, e3.getMessage());
        } catch (ProtocolException e4) {
            throw new MessagingException(e4.getMessage(), e4);
        }
        return search;
    }

    public synchronized Message[] search(SearchTerm searchTerm, Message[] messageArr) throws MessagingException {
        checkOpened();
        if (messageArr.length != 0) {
            try {
                Message[] messageArr2 = null;
                synchronized (this.messageCacheLock) {
                    IMAPProtocol protocol2 = getProtocol();
                    MessageSet[] messageSet = Utility.toMessageSet(messageArr, null);
                    if (messageSet == null) {
                        throw new MessageRemovedException("Messages have been removed");
                    }
                    int[] search = protocol2.search(messageSet, searchTerm);
                    if (search != null) {
                        messageArr2 = new IMAPMessage[search.length];
                        for (int i = RUNNING; i < search.length; i++) {
                            messageArr2[i] = getMessageBySeqNumber(search[i]);
                        }
                    }
                }
                messageArr = messageArr2;
            } catch (CommandFailedException e) {
                messageArr = super.search(searchTerm, messageArr);
            } catch (SearchException e2) {
                messageArr = super.search(searchTerm, messageArr);
            } catch (ConnectionException e3) {
                throw new FolderClosedException(this, e3.getMessage());
            } catch (ProtocolException e4) {
                throw new MessagingException(e4.getMessage(), e4);
            }
        }
        return messageArr;
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:18:0x0024=Splitter:B:18:0x0024, B:30:0x0036=Splitter:B:30:0x0036, B:36:0x003f=Splitter:B:36:0x003f} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized long getUIDValidity() throws javax.mail.MessagingException {
        /*
            r5 = this;
            r2 = 0
            monitor-enter(r5)
            boolean r0 = r5.opened     // Catch:{ all -> 0x0031 }
            if (r0 == 0) goto L_0x000a
            long r0 = r5.uidvalidity     // Catch:{ all -> 0x0031 }
        L_0x0008:
            monitor-exit(r5)
            return r0
        L_0x000a:
            com.sun.mail.imap.protocol.IMAPProtocol r1 = r5.getStoreProtocol()     // Catch:{ BadCommandException -> 0x0022, ConnectionException -> 0x0034, ProtocolException -> 0x003d, all -> 0x0049 }
            r0 = 1
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ BadCommandException -> 0x0050, ConnectionException -> 0x004e, ProtocolException -> 0x004c }
            r3 = 0
            java.lang.String r4 = "UIDVALIDITY"
            r0[r3] = r4     // Catch:{ BadCommandException -> 0x0050, ConnectionException -> 0x004e, ProtocolException -> 0x004c }
            java.lang.String r3 = r5.fullName     // Catch:{ BadCommandException -> 0x0050, ConnectionException -> 0x004e, ProtocolException -> 0x004c }
            com.sun.mail.imap.protocol.Status r2 = r1.status(r3, r0)     // Catch:{ BadCommandException -> 0x0050, ConnectionException -> 0x004e, ProtocolException -> 0x004c }
            r5.releaseStoreProtocol(r1)     // Catch:{ all -> 0x0031 }
        L_0x001f:
            long r0 = r2.uidvalidity     // Catch:{ all -> 0x0031 }
            goto L_0x0008
        L_0x0022:
            r0 = move-exception
            r1 = r2
        L_0x0024:
            javax.mail.MessagingException r2 = new javax.mail.MessagingException     // Catch:{ all -> 0x002c }
            java.lang.String r3 = "Cannot obtain UIDValidity"
            r2.<init>(r3, r0)     // Catch:{ all -> 0x002c }
            throw r2     // Catch:{ all -> 0x002c }
        L_0x002c:
            r0 = move-exception
        L_0x002d:
            r5.releaseStoreProtocol(r1)     // Catch:{ all -> 0x0031 }
            throw r0     // Catch:{ all -> 0x0031 }
        L_0x0031:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        L_0x0034:
            r0 = move-exception
            r1 = r2
        L_0x0036:
            r5.throwClosedException(r0)     // Catch:{ all -> 0x002c }
            r5.releaseStoreProtocol(r1)     // Catch:{ all -> 0x0031 }
            goto L_0x001f
        L_0x003d:
            r0 = move-exception
            r1 = r2
        L_0x003f:
            javax.mail.MessagingException r2 = new javax.mail.MessagingException     // Catch:{ all -> 0x002c }
            java.lang.String r3 = r0.getMessage()     // Catch:{ all -> 0x002c }
            r2.<init>(r3, r0)     // Catch:{ all -> 0x002c }
            throw r2     // Catch:{ all -> 0x002c }
        L_0x0049:
            r0 = move-exception
            r1 = r2
            goto L_0x002d
        L_0x004c:
            r0 = move-exception
            goto L_0x003f
        L_0x004e:
            r0 = move-exception
            goto L_0x0036
        L_0x0050:
            r0 = move-exception
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sun.mail.imap.IMAPFolder.getUIDValidity():long");
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:18:0x0024=Splitter:B:18:0x0024, B:30:0x0036=Splitter:B:30:0x0036, B:36:0x003f=Splitter:B:36:0x003f} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized long getUIDNext() throws javax.mail.MessagingException {
        /*
            r5 = this;
            r2 = 0
            monitor-enter(r5)
            boolean r0 = r5.opened     // Catch:{ all -> 0x0031 }
            if (r0 == 0) goto L_0x000a
            long r0 = r5.uidnext     // Catch:{ all -> 0x0031 }
        L_0x0008:
            monitor-exit(r5)
            return r0
        L_0x000a:
            com.sun.mail.imap.protocol.IMAPProtocol r1 = r5.getStoreProtocol()     // Catch:{ BadCommandException -> 0x0022, ConnectionException -> 0x0034, ProtocolException -> 0x003d, all -> 0x0049 }
            r0 = 1
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ BadCommandException -> 0x0050, ConnectionException -> 0x004e, ProtocolException -> 0x004c }
            r3 = 0
            java.lang.String r4 = "UIDNEXT"
            r0[r3] = r4     // Catch:{ BadCommandException -> 0x0050, ConnectionException -> 0x004e, ProtocolException -> 0x004c }
            java.lang.String r3 = r5.fullName     // Catch:{ BadCommandException -> 0x0050, ConnectionException -> 0x004e, ProtocolException -> 0x004c }
            com.sun.mail.imap.protocol.Status r2 = r1.status(r3, r0)     // Catch:{ BadCommandException -> 0x0050, ConnectionException -> 0x004e, ProtocolException -> 0x004c }
            r5.releaseStoreProtocol(r1)     // Catch:{ all -> 0x0031 }
        L_0x001f:
            long r0 = r2.uidnext     // Catch:{ all -> 0x0031 }
            goto L_0x0008
        L_0x0022:
            r0 = move-exception
            r1 = r2
        L_0x0024:
            javax.mail.MessagingException r2 = new javax.mail.MessagingException     // Catch:{ all -> 0x002c }
            java.lang.String r3 = "Cannot obtain UIDNext"
            r2.<init>(r3, r0)     // Catch:{ all -> 0x002c }
            throw r2     // Catch:{ all -> 0x002c }
        L_0x002c:
            r0 = move-exception
        L_0x002d:
            r5.releaseStoreProtocol(r1)     // Catch:{ all -> 0x0031 }
            throw r0     // Catch:{ all -> 0x0031 }
        L_0x0031:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        L_0x0034:
            r0 = move-exception
            r1 = r2
        L_0x0036:
            r5.throwClosedException(r0)     // Catch:{ all -> 0x002c }
            r5.releaseStoreProtocol(r1)     // Catch:{ all -> 0x0031 }
            goto L_0x001f
        L_0x003d:
            r0 = move-exception
            r1 = r2
        L_0x003f:
            javax.mail.MessagingException r2 = new javax.mail.MessagingException     // Catch:{ all -> 0x002c }
            java.lang.String r3 = r0.getMessage()     // Catch:{ all -> 0x002c }
            r2.<init>(r3, r0)     // Catch:{ all -> 0x002c }
            throw r2     // Catch:{ all -> 0x002c }
        L_0x0049:
            r0 = move-exception
            r1 = r2
            goto L_0x002d
        L_0x004c:
            r0 = move-exception
            goto L_0x003f
        L_0x004e:
            r0 = move-exception
            goto L_0x0036
        L_0x0050:
            r0 = move-exception
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sun.mail.imap.IMAPFolder.getUIDNext():long");
    }

    public synchronized Message getMessageByUID(long j) throws MessagingException {
        IMAPMessage iMAPMessage;
        UID fetchSequenceNumber;
        checkOpened();
        iMAPMessage = null;
        try {
            synchronized (this.messageCacheLock) {
                Long l = new Long(j);
                if (this.uidTable != null) {
                    iMAPMessage = (IMAPMessage) this.uidTable.get(l);
                    if (iMAPMessage != null) {
                    }
                    fetchSequenceNumber = getProtocol().fetchSequenceNumber(j);
                    if (fetchSequenceNumber != null && fetchSequenceNumber.seqnum <= this.total) {
                        iMAPMessage = getMessageBySeqNumber(fetchSequenceNumber.seqnum);
                        iMAPMessage.setUID(fetchSequenceNumber.uid);
                        this.uidTable.put(l, iMAPMessage);
                    }
                } else {
                    this.uidTable = new Hashtable();
                    fetchSequenceNumber = getProtocol().fetchSequenceNumber(j);
                    iMAPMessage = getMessageBySeqNumber(fetchSequenceNumber.seqnum);
                    iMAPMessage.setUID(fetchSequenceNumber.uid);
                    this.uidTable.put(l, iMAPMessage);
                }
            }
        } catch (ConnectionException e) {
            throw new FolderClosedException(this, e.getMessage());
        } catch (ProtocolException e2) {
            throw new MessagingException(e2.getMessage(), e2);
        }
        return iMAPMessage;
    }

    public synchronized Message[] getMessagesByUID(long j, long j2) throws MessagingException {
        Message[] messageArr;
        checkOpened();
        try {
            synchronized (this.messageCacheLock) {
                if (this.uidTable == null) {
                    this.uidTable = new Hashtable();
                }
                UID[] fetchSequenceNumbers = getProtocol().fetchSequenceNumbers(j, j2);
                messageArr = new Message[fetchSequenceNumbers.length];
                for (int i = RUNNING; i < fetchSequenceNumbers.length; i++) {
                    IMAPMessage messageBySeqNumber = getMessageBySeqNumber(fetchSequenceNumbers[i].seqnum);
                    messageBySeqNumber.setUID(fetchSequenceNumbers[i].uid);
                    messageArr[i] = messageBySeqNumber;
                    this.uidTable.put(new Long(fetchSequenceNumbers[i].uid), messageBySeqNumber);
                }
            }
        } catch (ConnectionException e) {
            throw new FolderClosedException(this, e.getMessage());
        } catch (ProtocolException e2) {
            throw new MessagingException(e2.getMessage(), e2);
        }
        return messageArr;
    }

    public synchronized Message[] getMessagesByUID(long[] jArr) throws MessagingException {
        long[] jArr2;
        Message[] messageArr;
        synchronized (this) {
            checkOpened();
            try {
                synchronized (this.messageCacheLock) {
                    if (this.uidTable != null) {
                        Vector vector = new Vector();
                        for (int i = RUNNING; i < jArr.length; i++) {
                            Hashtable hashtable = this.uidTable;
                            Long l = new Long(jArr[i]);
                            if (!hashtable.containsKey(l)) {
                                vector.addElement(l);
                            }
                        }
                        int size = vector.size();
                        long[] jArr3 = new long[size];
                        for (int i2 = RUNNING; i2 < size; i2++) {
                            jArr3[i2] = ((Long) vector.elementAt(i2)).longValue();
                        }
                        jArr2 = jArr3;
                    } else {
                        this.uidTable = new Hashtable();
                        jArr2 = jArr;
                    }
                    if (jArr2.length > 0) {
                        UID[] fetchSequenceNumbers = getProtocol().fetchSequenceNumbers(jArr2);
                        for (int i3 = RUNNING; i3 < fetchSequenceNumbers.length; i3++) {
                            IMAPMessage messageBySeqNumber = getMessageBySeqNumber(fetchSequenceNumbers[i3].seqnum);
                            messageBySeqNumber.setUID(fetchSequenceNumbers[i3].uid);
                            this.uidTable.put(new Long(fetchSequenceNumbers[i3].uid), messageBySeqNumber);
                        }
                    }
                    messageArr = new Message[jArr.length];
                    for (int i4 = RUNNING; i4 < jArr.length; i4++) {
                        messageArr[i4] = (Message) this.uidTable.get(new Long(jArr[i4]));
                    }
                }
            } catch (ConnectionException e) {
                throw new FolderClosedException(this, e.getMessage());
            } catch (ProtocolException e2) {
                throw new MessagingException(e2.getMessage(), e2);
            }
        }
        return messageArr;
    }

    public synchronized long getUID(Message message) throws MessagingException {
        long uid;
        if (message.getFolder() != this) {
            throw new NoSuchElementException("Message does not belong to this folder");
        }
        checkOpened();
        IMAPMessage iMAPMessage = (IMAPMessage) message;
        uid = iMAPMessage.getUID();
        if (uid == -1) {
            synchronized (this.messageCacheLock) {
                try {
                    IMAPProtocol protocol2 = getProtocol();
                    iMAPMessage.checkExpunged();
                    UID fetchUID = protocol2.fetchUID(iMAPMessage.getSequenceNumber());
                    if (fetchUID != null) {
                        uid = fetchUID.uid;
                        iMAPMessage.setUID(uid);
                        if (this.uidTable == null) {
                            this.uidTable = new Hashtable();
                        }
                        this.uidTable.put(new Long(uid), iMAPMessage);
                    }
                } catch (ConnectionException e) {
                    throw new FolderClosedException(this, e.getMessage());
                } catch (ProtocolException e2) {
                    throw new MessagingException(e2.getMessage(), e2);
                }
            }
        }
        return uid;
    }

    public Quota[] getQuota() throws MessagingException {
        return (Quota[]) doOptionalCommand("QUOTA not supported", new ProtocolCommand() {
            public Object doCommand(IMAPProtocol iMAPProtocol) throws ProtocolException {
                return iMAPProtocol.getQuotaRoot(IMAPFolder.this.fullName);
            }
        });
    }

    public void setQuota(final Quota quota) throws MessagingException {
        doOptionalCommand("QUOTA not supported", new ProtocolCommand() {
            public Object doCommand(IMAPProtocol iMAPProtocol) throws ProtocolException {
                iMAPProtocol.setQuota(quota);
                return null;
            }
        });
    }

    public ACL[] getACL() throws MessagingException {
        return (ACL[]) doOptionalCommand("ACL not supported", new ProtocolCommand() {
            public Object doCommand(IMAPProtocol iMAPProtocol) throws ProtocolException {
                return iMAPProtocol.getACL(IMAPFolder.this.fullName);
            }
        });
    }

    public void addACL(ACL acl) throws MessagingException {
        setACL(acl, 0);
    }

    public void removeACL(final String str) throws MessagingException {
        doOptionalCommand("ACL not supported", new ProtocolCommand() {
            public Object doCommand(IMAPProtocol iMAPProtocol) throws ProtocolException {
                iMAPProtocol.deleteACL(IMAPFolder.this.fullName, str);
                return null;
            }
        });
    }

    public void addRights(ACL acl) throws MessagingException {
        setACL(acl, '+');
    }

    public void removeRights(ACL acl) throws MessagingException {
        setACL(acl, '-');
    }

    public Rights[] listRights(final String str) throws MessagingException {
        return (Rights[]) doOptionalCommand("ACL not supported", new ProtocolCommand() {
            public Object doCommand(IMAPProtocol iMAPProtocol) throws ProtocolException {
                return iMAPProtocol.listRights(IMAPFolder.this.fullName, str);
            }
        });
    }

    public Rights myRights() throws MessagingException {
        return (Rights) doOptionalCommand("ACL not supported", new ProtocolCommand() {
            public Object doCommand(IMAPProtocol iMAPProtocol) throws ProtocolException {
                return iMAPProtocol.myRights(IMAPFolder.this.fullName);
            }
        });
    }

    private void setACL(final ACL acl, final char c) throws MessagingException {
        doOptionalCommand("ACL not supported", new ProtocolCommand() {
            public Object doCommand(IMAPProtocol iMAPProtocol) throws ProtocolException {
                iMAPProtocol.setACL(IMAPFolder.this.fullName, c, acl);
                return null;
            }
        });
    }

    public String[] getAttributes() throws MessagingException {
        if (this.attributes == null) {
            exists();
        }
        return (String[]) this.attributes.clone();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002a, code lost:
        r0 = r3.protocol.readIdleResponse();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        r1 = r3.messageCacheLock;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0032, code lost:
        monitor-enter(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0033, code lost:
        if (r0 == null) goto L_0x0041;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0037, code lost:
        if (r3.protocol == null) goto L_0x0041;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x003f, code lost:
        if (r3.protocol.processIdleResponse(r0) != false) goto L_0x005e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0041, code lost:
        r3.idleState = com.sun.mail.imap.IMAPFolder.RUNNING;
        r3.messageCacheLock.notifyAll();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0049, code lost:
        monitor-exit(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x004a, code lost:
        r0 = ((com.sun.mail.imap.IMAPStore) r3.store).getMinIdleTime();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0052, code lost:
        if (r0 <= 0) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        java.lang.Thread.sleep((long) r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        monitor-exit(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0063, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0064, code lost:
        throwClosedException(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0068, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0072, code lost:
        throw new javax.mail.MessagingException(r0.getMessage(), r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:?, code lost:
        return;
     */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void idle() throws javax.mail.MessagingException {
        /*
            r3 = this;
            boolean r0 = com.sun.mail.imap.IMAPFolder.$assertionsDisabled
            if (r0 != 0) goto L_0x0010
            boolean r0 = java.lang.Thread.holdsLock(r3)
            if (r0 == 0) goto L_0x0010
            java.lang.AssertionError r0 = new java.lang.AssertionError
            r0.<init>()
            throw r0
        L_0x0010:
            monitor-enter(r3)
            r3.checkOpened()     // Catch:{ all -> 0x005b }
            java.lang.String r0 = "IDLE not supported"
            com.sun.mail.imap.IMAPFolder$19 r1 = new com.sun.mail.imap.IMAPFolder$19     // Catch:{ all -> 0x005b }
            r1.<init>()     // Catch:{ all -> 0x005b }
            java.lang.Object r0 = r3.doOptionalCommand(r0, r1)     // Catch:{ all -> 0x005b }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ all -> 0x005b }
            boolean r0 = r0.booleanValue()     // Catch:{ all -> 0x005b }
            if (r0 != 0) goto L_0x0029
            monitor-exit(r3)     // Catch:{ all -> 0x005b }
        L_0x0028:
            return
        L_0x0029:
            monitor-exit(r3)     // Catch:{ all -> 0x005b }
        L_0x002a:
            com.sun.mail.imap.protocol.IMAPProtocol r0 = r3.protocol
            com.sun.mail.iap.Response r0 = r0.readIdleResponse()
            java.lang.Object r1 = r3.messageCacheLock     // Catch:{ ConnectionException -> 0x0063, ProtocolException -> 0x0068 }
            monitor-enter(r1)     // Catch:{ ConnectionException -> 0x0063, ProtocolException -> 0x0068 }
            if (r0 == 0) goto L_0x0041
            com.sun.mail.imap.protocol.IMAPProtocol r2 = r3.protocol     // Catch:{ all -> 0x0060 }
            if (r2 == 0) goto L_0x0041
            com.sun.mail.imap.protocol.IMAPProtocol r2 = r3.protocol     // Catch:{ all -> 0x0060 }
            boolean r0 = r2.processIdleResponse(r0)     // Catch:{ all -> 0x0060 }
            if (r0 != 0) goto L_0x005e
        L_0x0041:
            r0 = 0
            r3.idleState = r0     // Catch:{ all -> 0x0060 }
            java.lang.Object r0 = r3.messageCacheLock     // Catch:{ all -> 0x0060 }
            r0.notifyAll()     // Catch:{ all -> 0x0060 }
            monitor-exit(r1)     // Catch:{ all -> 0x0060 }
            javax.mail.Store r0 = r3.store
            com.sun.mail.imap.IMAPStore r0 = (com.sun.mail.imap.IMAPStore) r0
            int r0 = r0.getMinIdleTime()
            if (r0 <= 0) goto L_0x0028
            long r0 = (long) r0
            java.lang.Thread.sleep(r0)     // Catch:{ InterruptedException -> 0x0059 }
            goto L_0x0028
        L_0x0059:
            r0 = move-exception
            goto L_0x0028
        L_0x005b:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x005b }
            throw r0
        L_0x005e:
            monitor-exit(r1)     // Catch:{ all -> 0x0060 }
            goto L_0x002a
        L_0x0060:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0060 }
            throw r0     // Catch:{ ConnectionException -> 0x0063, ProtocolException -> 0x0068 }
        L_0x0063:
            r0 = move-exception
            r3.throwClosedException(r0)
            goto L_0x002a
        L_0x0068:
            r0 = move-exception
            javax.mail.MessagingException r1 = new javax.mail.MessagingException
            java.lang.String r2 = r0.getMessage()
            r1.<init>(r2, r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sun.mail.imap.IMAPFolder.idle():void");
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: CFG modification limit reached, blocks count: 117 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void waitIfIdle() throws com.sun.mail.iap.ProtocolException {
        /*
            r2 = this;
            boolean r0 = com.sun.mail.imap.IMAPFolder.$assertionsDisabled
            if (r0 != 0) goto L_0x0024
            java.lang.Object r0 = r2.messageCacheLock
            boolean r0 = java.lang.Thread.holdsLock(r0)
            if (r0 != 0) goto L_0x0024
            java.lang.AssertionError r0 = new java.lang.AssertionError
            r0.<init>()
            throw r0
        L_0x0012:
            int r0 = r2.idleState
            r1 = 1
            if (r0 != r1) goto L_0x001f
            com.sun.mail.imap.protocol.IMAPProtocol r0 = r2.protocol
            r0.idleAbort()
            r0 = 2
            r2.idleState = r0
        L_0x001f:
            java.lang.Object r0 = r2.messageCacheLock     // Catch:{ InterruptedException -> 0x0029 }
            r0.wait()     // Catch:{ InterruptedException -> 0x0029 }
        L_0x0024:
            int r0 = r2.idleState
            if (r0 != 0) goto L_0x0012
            return
        L_0x0029:
            r0 = move-exception
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sun.mail.imap.IMAPFolder.waitIfIdle():void");
    }

    public void handleResponse(Response response) {
        IMAPMessage messageBySeqNumber;
        if ($assertionsDisabled || Thread.holdsLock(this.messageCacheLock)) {
            if (response.isOK() || response.isNO() || response.isBAD() || response.isBYE()) {
                ((IMAPStore) this.store).handleResponseCode(response);
            }
            if (response.isBYE()) {
                if (this.opened) {
                    cleanup($assertionsDisabled);
                }
            } else if (!response.isOK() && response.isUnTagged()) {
                if (!(response instanceof IMAPResponse)) {
                    this.out.println("UNEXPECTED RESPONSE : " + response.toString());
                    this.out.println("CONTACT javamail@sun.com");
                    return;
                }
                IMAPResponse iMAPResponse = (IMAPResponse) response;
                if (iMAPResponse.keyEquals("EXISTS")) {
                    int number = iMAPResponse.getNumber();
                    if (number > this.realTotal) {
                        int i = number - this.realTotal;
                        Message[] messageArr = new Message[i];
                        for (int i2 = RUNNING; i2 < i; i2++) {
                            int i3 = this.total + 1;
                            this.total = i3;
                            int i4 = this.realTotal + 1;
                            this.realTotal = i4;
                            IMAPMessage iMAPMessage = new IMAPMessage(this, i3, i4);
                            messageArr[i2] = iMAPMessage;
                            this.messageCache.addElement(iMAPMessage);
                        }
                        notifyMessageAddedListeners(messageArr);
                    }
                } else if (iMAPResponse.keyEquals("EXPUNGE")) {
                    IMAPMessage messageBySeqNumber2 = getMessageBySeqNumber(iMAPResponse.getNumber());
                    messageBySeqNumber2.setExpunged(true);
                    int messageNumber = messageBySeqNumber2.getMessageNumber();
                    while (true) {
                        int i5 = messageNumber;
                        if (i5 >= this.total) {
                            break;
                        }
                        IMAPMessage iMAPMessage2 = (IMAPMessage) this.messageCache.elementAt(i5);
                        if (!iMAPMessage2.isExpunged()) {
                            iMAPMessage2.setSequenceNumber(iMAPMessage2.getSequenceNumber() - 1);
                        }
                        messageNumber = i5 + 1;
                    }
                    this.realTotal--;
                    if (this.doExpungeNotification) {
                        Message[] messageArr2 = new Message[1];
                        messageArr2[RUNNING] = messageBySeqNumber2;
                        notifyMessageRemovedListeners($assertionsDisabled, messageArr2);
                    }
                } else if (iMAPResponse.keyEquals("FETCH")) {
                    if ($assertionsDisabled || (iMAPResponse instanceof FetchResponse)) {
                        FetchResponse fetchResponse = (FetchResponse) iMAPResponse;
                        Flags flags = (Flags) fetchResponse.getItem(Flags.class);
                        if (flags != null && (messageBySeqNumber = getMessageBySeqNumber(fetchResponse.getNumber())) != null) {
                            messageBySeqNumber._setFlags(flags);
                            notifyMessageChangedListeners(1, messageBySeqNumber);
                            return;
                        }
                        return;
                    }
                    throw new AssertionError("!ir instanceof FetchResponse");
                } else if (iMAPResponse.keyEquals("RECENT")) {
                    this.recent = iMAPResponse.getNumber();
                }
            }
        } else {
            throw new AssertionError();
        }
    }

    /* access modifiers changed from: package-private */
    public void handleResponses(Response[] responseArr) {
        for (int i = RUNNING; i < responseArr.length; i++) {
            if (responseArr[i] != null) {
                handleResponse(responseArr[i]);
            }
        }
    }

    /* access modifiers changed from: protected */
    public synchronized IMAPProtocol getStoreProtocol() throws ProtocolException {
        if (this.connectionPoolDebug) {
            this.out.println("DEBUG: getStoreProtocol() - borrowing a connection");
        }
        return ((IMAPStore) this.store).getStoreProtocol();
    }

    private synchronized void throwClosedException(ConnectionException connectionException) throws FolderClosedException, StoreClosedException {
        if ((this.protocol == null || connectionException.getProtocol() != this.protocol) && (this.protocol != null || this.reallyClosed)) {
            throw new StoreClosedException(this.store, connectionException.getMessage());
        }
        throw new FolderClosedException(this, connectionException.getMessage());
    }

    private IMAPProtocol getProtocol() throws ProtocolException {
        if ($assertionsDisabled || Thread.holdsLock(this.messageCacheLock)) {
            waitIfIdle();
            return this.protocol;
        }
        throw new AssertionError();
    }

    public Object doCommand(ProtocolCommand protocolCommand) throws MessagingException {
        try {
            return doProtocolCommand(protocolCommand);
        } catch (ConnectionException e) {
            throwClosedException(e);
            return null;
        } catch (ProtocolException e2) {
            throw new MessagingException(e2.getMessage(), e2);
        }
    }

    public Object doOptionalCommand(String str, ProtocolCommand protocolCommand) throws MessagingException {
        try {
            return doProtocolCommand(protocolCommand);
        } catch (BadCommandException e) {
            throw new MessagingException(str, e);
        } catch (ConnectionException e2) {
            throwClosedException(e2);
            return null;
        } catch (ProtocolException e3) {
            throw new MessagingException(e3.getMessage(), e3);
        }
    }

    public Object doCommandIgnoreFailure(ProtocolCommand protocolCommand) throws MessagingException {
        try {
            return doProtocolCommand(protocolCommand);
        } catch (CommandFailedException e) {
            return null;
        } catch (ConnectionException e2) {
            throwClosedException(e2);
            return null;
        } catch (ProtocolException e3) {
            throw new MessagingException(e3.getMessage(), e3);
        }
    }

    /* access modifiers changed from: protected */
    public Object doProtocolCommand(ProtocolCommand protocolCommand) throws ProtocolException {
        Object doCommand;
        synchronized (this) {
            if (!this.opened || ((IMAPStore) this.store).hasSeparateStoreConnection()) {
                IMAPProtocol iMAPProtocol = null;
                try {
                    iMAPProtocol = getStoreProtocol();
                    doCommand = protocolCommand.doCommand(iMAPProtocol);
                } finally {
                    releaseStoreProtocol(iMAPProtocol);
                }
            } else {
                synchronized (this.messageCacheLock) {
                    doCommand = protocolCommand.doCommand(getProtocol());
                }
            }
        }
        return doCommand;
    }

    /* access modifiers changed from: protected */
    public synchronized void releaseStoreProtocol(IMAPProtocol iMAPProtocol) {
        if (iMAPProtocol != this.protocol) {
            ((IMAPStore) this.store).releaseStoreProtocol(iMAPProtocol);
        }
    }

    private void releaseProtocol(boolean z) {
        if (this.protocol != null) {
            this.protocol.removeResponseHandler(this);
            if (z) {
                ((IMAPStore) this.store).releaseProtocol(this, this.protocol);
            } else {
                ((IMAPStore) this.store).releaseProtocol(this, null);
            }
        }
    }

    private void keepConnectionAlive(boolean z) throws ProtocolException {
        IMAPProtocol iMAPProtocol;
        Throwable th;
        if (System.currentTimeMillis() - this.protocol.getTimestamp() > 1000) {
            waitIfIdle();
            this.protocol.noop();
        }
        if (z && ((IMAPStore) this.store).hasSeparateStoreConnection()) {
            try {
                IMAPProtocol storeProtocol = ((IMAPStore) this.store).getStoreProtocol();
                try {
                    if (System.currentTimeMillis() - storeProtocol.getTimestamp() > 1000) {
                        storeProtocol.noop();
                    }
                    ((IMAPStore) this.store).releaseStoreProtocol(storeProtocol);
                } catch (Throwable th2) {
                    iMAPProtocol = storeProtocol;
                    th = th2;
                    ((IMAPStore) this.store).releaseStoreProtocol(iMAPProtocol);
                    throw th;
                }
            } catch (Throwable th3) {
                iMAPProtocol = null;
                th = th3;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public IMAPMessage getMessageBySeqNumber(int i) {
        int i2 = i - 1;
        while (true) {
            int i3 = i2;
            if (i3 >= this.total) {
                return null;
            }
            IMAPMessage iMAPMessage = (IMAPMessage) this.messageCache.elementAt(i3);
            if (iMAPMessage.getSequenceNumber() == i) {
                return iMAPMessage;
            }
            i2 = i3 + 1;
        }
    }

    private boolean isDirectory() {
        if ((this.type & 2) != 0) {
            return true;
        }
        return $assertionsDisabled;
    }
}
