package com.sun.mail.handlers;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import javax.activation.ActivationDataFlavor;
import javax.activation.DataContentHandler;
import javax.activation.DataSource;
import javax.mail.internet.ContentType;
import javax.mail.internet.MimeUtility;
import myjava.awt.datatransfer.DataFlavor;

public class text_plain implements DataContentHandler {
    private static ActivationDataFlavor myDF = new ActivationDataFlavor(String.class, "text/plain", "Text String");

    /* access modifiers changed from: protected */
    public ActivationDataFlavor getDF() {
        return myDF;
    }

    public DataFlavor[] getTransferDataFlavors() {
        return new DataFlavor[]{getDF()};
    }

    public Object getTransferData(DataFlavor dataFlavor, DataSource dataSource) throws IOException {
        if (getDF().equals(dataFlavor)) {
            return getContent(dataSource);
        }
        return null;
    }

    public Object getContent(DataSource dataSource) throws IOException {
        int i;
        String str = null;
        try {
            str = getCharset(dataSource.getContentType());
            InputStreamReader inputStreamReader = new InputStreamReader(dataSource.getInputStream(), str);
            try {
                int i2 = 0;
                char[] cArr = new char[1024];
                while (true) {
                    int read = inputStreamReader.read(cArr, i2, cArr.length - i2);
                    if (read == -1) {
                        break;
                    }
                    int i3 = read + i2;
                    if (i3 >= cArr.length) {
                        int length = cArr.length;
                        if (length < 262144) {
                            i = length + length;
                        } else {
                            i = length + 262144;
                        }
                        char[] cArr2 = new char[i];
                        System.arraycopy(cArr, 0, cArr2, 0, i3);
                        cArr = cArr2;
                        i2 = i3;
                    } else {
                        i2 = i3;
                    }
                }
                return new String(cArr, 0, i2);
            } finally {
                try {
                    inputStreamReader.close();
                } catch (IOException e) {
                }
            }
        } catch (IllegalArgumentException e2) {
            throw new UnsupportedEncodingException(str);
        }
    }

    public void writeTo(Object obj, String str, OutputStream outputStream) throws IOException {
        if (!(obj instanceof String)) {
            throw new IOException("\"" + getDF().getMimeType() + "\" DataContentHandler requires String object, " + "was given object of type " + obj.getClass().toString());
        }
        String str2 = null;
        try {
            str2 = getCharset(str);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, str2);
            String str3 = (String) obj;
            outputStreamWriter.write(str3, 0, str3.length());
            outputStreamWriter.flush();
        } catch (IllegalArgumentException e) {
            throw new UnsupportedEncodingException(str2);
        }
    }

    private String getCharset(String str) {
        try {
            String parameter = new ContentType(str).getParameter("charset");
            if (parameter == null) {
                parameter = "us-ascii";
            }
            return MimeUtility.javaCharset(parameter);
        } catch (Exception e) {
            return null;
        }
    }
}
