package com.sun.mail.iap;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

public class ResponseInputStream {
    private static final int incrementSlop = 16;
    private static final int maxIncrement = 262144;
    private static final int minIncrement = 256;
    private BufferedInputStream bin;

    public ResponseInputStream(InputStream inputStream) {
        this.bin = new BufferedInputStream(inputStream, 2048);
    }

    public ByteArray readResponse() throws IOException {
        return readResponse(null);
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0026 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.sun.mail.iap.ByteArray readResponse(com.sun.mail.iap.ByteArray r11) throws java.io.IOException {
        /*
            r10 = this;
            r4 = 262144(0x40000, float:3.67342E-40)
            r5 = 256(0x100, float:3.59E-43)
            r2 = 128(0x80, float:1.794E-43)
            r9 = -1
            r1 = 0
            if (r11 != 0) goto L_0x0011
            com.sun.mail.iap.ByteArray r11 = new com.sun.mail.iap.ByteArray
            byte[] r0 = new byte[r2]
            r11.<init>(r0, r1, r2)
        L_0x0011:
            byte[] r0 = r11.getBytes()
            r2 = r0
            r0 = r1
        L_0x0017:
            r3 = r1
            r6 = r1
        L_0x0019:
            if (r3 != 0) goto L_0x0023
            java.io.BufferedInputStream r6 = r10.bin
            int r6 = r6.read()
            if (r6 != r9) goto L_0x002c
        L_0x0023:
            r3 = r6
            if (r3 != r9) goto L_0x0050
            java.io.IOException r0 = new java.io.IOException
            r0.<init>()
            throw r0
        L_0x002c:
            switch(r6) {
                case 10: goto L_0x0044;
                default: goto L_0x002f;
            }
        L_0x002f:
            int r7 = r2.length
            if (r0 < r7) goto L_0x003d
            int r2 = r2.length
            if (r2 <= r4) goto L_0x0036
            r2 = r4
        L_0x0036:
            r11.grow(r2)
            byte[] r2 = r11.getBytes()
        L_0x003d:
            int r7 = r0 + 1
            byte r8 = (byte) r6
            r2[r0] = r8
            r0 = r7
            goto L_0x0019
        L_0x0044:
            if (r0 <= 0) goto L_0x002f
            int r7 = r0 + -1
            byte r7 = r2[r7]
            r8 = 13
            if (r7 != r8) goto L_0x002f
            r3 = 1
            goto L_0x002f
        L_0x0050:
            r3 = 5
            if (r0 < r3) goto L_0x005b
            int r3 = r0 + -3
            byte r3 = r2[r3]
            r6 = 125(0x7d, float:1.75E-43)
            if (r3 == r6) goto L_0x005f
        L_0x005b:
            r11.setCount(r0)
            return r11
        L_0x005f:
            int r3 = r0 + -4
        L_0x0061:
            if (r3 >= 0) goto L_0x008d
        L_0x0063:
            if (r3 < 0) goto L_0x005b
            int r3 = r3 + 1
            int r6 = r0 + -3
            int r3 = com.sun.mail.util.ASCIIUtility.parseInt(r2, r3, r6)     // Catch:{ NumberFormatException -> 0x009a }
            if (r3 <= 0) goto L_0x0017
            int r6 = r2.length
            int r6 = r6 - r0
            int r7 = r3 + 16
            if (r7 <= r6) goto L_0x0082
            int r2 = r3 + 16
            int r2 = r2 - r6
            if (r5 <= r2) goto L_0x0096
            r2 = r5
        L_0x007b:
            r11.grow(r2)
            byte[] r2 = r11.getBytes()
        L_0x0082:
            if (r3 <= 0) goto L_0x0017
            java.io.BufferedInputStream r6 = r10.bin
            int r6 = r6.read(r2, r0, r3)
            int r3 = r3 - r6
            int r0 = r0 + r6
            goto L_0x0082
        L_0x008d:
            byte r6 = r2[r3]
            r7 = 123(0x7b, float:1.72E-43)
            if (r6 == r7) goto L_0x0063
            int r3 = r3 + -1
            goto L_0x0061
        L_0x0096:
            int r2 = r3 + 16
            int r2 = r2 - r6
            goto L_0x007b
        L_0x009a:
            r1 = move-exception
            goto L_0x005b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sun.mail.iap.ResponseInputStream.readResponse(com.sun.mail.iap.ByteArray):com.sun.mail.iap.ByteArray");
    }
}
