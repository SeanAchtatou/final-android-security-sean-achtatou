package com.sun.mail.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class ASCIIUtility {
    private ASCIIUtility() {
    }

    public static int parseInt(byte[] bArr, int i, int i2, int i3) throws NumberFormatException {
        int i4;
        int i5;
        boolean z;
        int i6;
        int i7;
        if (bArr == null) {
            throw new NumberFormatException("null");
        } else if (i2 > i) {
            if (bArr[i] == 45) {
                z = true;
                i4 = Integer.MIN_VALUE;
                i5 = i + 1;
            } else {
                i4 = -2147483647;
                i5 = i;
                z = false;
            }
            int i8 = i4 / i3;
            if (i5 < i2) {
                i6 = i5 + 1;
                int digit = Character.digit((char) bArr[i5], i3);
                if (digit < 0) {
                    throw new NumberFormatException("illegal number: " + toString(bArr, i, i2));
                }
                i7 = -digit;
            } else {
                int i9 = i5;
                i7 = 0;
                i6 = i9;
            }
            while (i6 < i2) {
                int i10 = i6 + 1;
                int digit2 = Character.digit((char) bArr[i6], i3);
                if (digit2 < 0) {
                    throw new NumberFormatException("illegal number");
                } else if (i7 < i8) {
                    throw new NumberFormatException("illegal number");
                } else {
                    int i11 = i7 * i3;
                    if (i11 < i4 + digit2) {
                        throw new NumberFormatException("illegal number");
                    }
                    i7 = i11 - digit2;
                    i6 = i10;
                }
            }
            if (!z) {
                return -i7;
            }
            if (i6 > i + 1) {
                return i7;
            }
            throw new NumberFormatException("illegal number");
        } else {
            throw new NumberFormatException("illegal number");
        }
    }

    public static int parseInt(byte[] bArr, int i, int i2) throws NumberFormatException {
        return parseInt(bArr, i, i2, 10);
    }

    public static long parseLong(byte[] bArr, int i, int i2, int i3) throws NumberFormatException {
        long j;
        boolean z;
        int i4;
        if (bArr == null) {
            throw new NumberFormatException("null");
        }
        long j2 = 0;
        if (i2 > i) {
            if (bArr[i] == 45) {
                z = true;
                j = Long.MIN_VALUE;
                i4 = i + 1;
            } else {
                j = -9223372036854775807L;
                z = false;
                i4 = i;
            }
            long j3 = j / ((long) i3);
            if (i4 < i2) {
                int i5 = i4 + 1;
                int digit = Character.digit((char) bArr[i4], i3);
                if (digit < 0) {
                    throw new NumberFormatException("illegal number: " + toString(bArr, i, i2));
                }
                long j4 = (long) (-digit);
                i4 = i5;
                j2 = j4;
            }
            while (i4 < i2) {
                int i6 = i4 + 1;
                int digit2 = Character.digit((char) bArr[i4], i3);
                if (digit2 < 0) {
                    throw new NumberFormatException("illegal number");
                } else if (j2 < j3) {
                    throw new NumberFormatException("illegal number");
                } else {
                    long j5 = j2 * ((long) i3);
                    if (j5 < ((long) digit2) + j) {
                        throw new NumberFormatException("illegal number");
                    }
                    j2 = j5 - ((long) digit2);
                    i4 = i6;
                }
            }
            if (!z) {
                return -j2;
            }
            if (i4 > i + 1) {
                return j2;
            }
            throw new NumberFormatException("illegal number");
        }
        throw new NumberFormatException("illegal number");
    }

    public static long parseLong(byte[] bArr, int i, int i2) throws NumberFormatException {
        return parseLong(bArr, i, i2, 10);
    }

    public static String toString(byte[] bArr, int i, int i2) {
        int i3 = i2 - i;
        char[] cArr = new char[i3];
        for (int i4 = 0; i4 < i3; i4++) {
            cArr[i4] = (char) (bArr[i] & 255);
            i++;
        }
        return new String(cArr);
    }

    public static String toString(ByteArrayInputStream byteArrayInputStream) {
        int available = byteArrayInputStream.available();
        char[] cArr = new char[available];
        byte[] bArr = new byte[available];
        byteArrayInputStream.read(bArr, 0, available);
        for (int i = 0; i < available; i++) {
            cArr[i] = (char) (bArr[i] & 255);
        }
        return new String(cArr);
    }

    public static byte[] getBytes(String str) {
        char[] charArray = str.toCharArray();
        int length = charArray.length;
        byte[] bArr = new byte[length];
        for (int i = 0; i < length; i++) {
            bArr[i] = (byte) charArray[i];
        }
        return bArr;
    }

    public static byte[] getBytes(InputStream inputStream) throws IOException {
        if (inputStream instanceof ByteArrayInputStream) {
            int available = inputStream.available();
            byte[] bArr = new byte[available];
            inputStream.read(bArr, 0, available);
            return bArr;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr2 = new byte[1024];
        while (true) {
            int read = inputStream.read(bArr2, 0, 1024);
            if (read == -1) {
                return byteArrayOutputStream.toByteArray();
            }
            byteArrayOutputStream.write(bArr2, 0, read);
        }
    }
}
