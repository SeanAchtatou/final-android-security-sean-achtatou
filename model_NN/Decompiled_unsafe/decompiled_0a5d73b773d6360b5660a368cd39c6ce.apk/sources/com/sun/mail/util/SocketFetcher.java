package com.sun.mail.util;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.ArrayList;
import java.util.Properties;
import java.util.StringTokenizer;
import javax.net.SocketFactory;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

public class SocketFetcher {
    private SocketFetcher() {
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x00ee  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00fa A[SYNTHETIC, Splitter:B:38:0x00fa] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0100  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0124  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0127  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0168  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.net.Socket getSocket(java.lang.String r11, int r12, java.util.Properties r13, java.lang.String r14, boolean r15) throws java.io.IOException {
        /*
            if (r14 != 0) goto L_0x0004
            java.lang.String r14 = "socket"
        L_0x0004:
            if (r13 != 0) goto L_0x000b
            java.util.Properties r13 = new java.util.Properties
            r13.<init>()
        L_0x000b:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = java.lang.String.valueOf(r14)
            r0.<init>(r1)
            java.lang.String r1 = ".connectiontimeout"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r1 = 0
            java.lang.String r0 = r13.getProperty(r0, r1)
            r4 = -1
            if (r0 == 0) goto L_0x002a
            int r4 = java.lang.Integer.parseInt(r0)     // Catch:{ NumberFormatException -> 0x015d }
        L_0x002a:
            r8 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = java.lang.String.valueOf(r14)
            r0.<init>(r1)
            java.lang.String r1 = ".timeout"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r1 = 0
            java.lang.String r9 = r13.getProperty(r0, r1)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = java.lang.String.valueOf(r14)
            r0.<init>(r1)
            java.lang.String r1 = ".localaddress"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r1 = 0
            java.lang.String r1 = r13.getProperty(r0, r1)
            r0 = 0
            if (r1 == 0) goto L_0x0062
            java.net.InetAddress r0 = java.net.InetAddress.getByName(r1)
        L_0x0062:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = java.lang.String.valueOf(r14)
            r1.<init>(r2)
            java.lang.String r2 = ".localport"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r2 = 0
            java.lang.String r2 = r13.getProperty(r1, r2)
            r1 = 0
            if (r2 == 0) goto L_0x0081
            int r1 = java.lang.Integer.parseInt(r2)     // Catch:{ NumberFormatException -> 0x0160 }
        L_0x0081:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = java.lang.String.valueOf(r14)
            r2.<init>(r3)
            java.lang.String r3 = ".socketFactory.fallback"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r3 = 0
            java.lang.String r2 = r13.getProperty(r2, r3)
            if (r2 == 0) goto L_0x0107
            java.lang.String r3 = "false"
            boolean r2 = r2.equalsIgnoreCase(r3)
            if (r2 == 0) goto L_0x0107
            r2 = 0
            r7 = r2
        L_0x00a5:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = java.lang.String.valueOf(r14)
            r2.<init>(r3)
            java.lang.String r3 = ".socketFactory.class"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r3 = 0
            java.lang.String r10 = r13.getProperty(r2, r3)
            r3 = -1
            javax.net.SocketFactory r5 = getSocketFactory(r10)     // Catch:{ SocketTimeoutException -> 0x010a, Exception -> 0x010c }
            if (r5 == 0) goto L_0x016c
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ SocketTimeoutException -> 0x010a, Exception -> 0x010c }
            java.lang.String r6 = java.lang.String.valueOf(r14)     // Catch:{ SocketTimeoutException -> 0x010a, Exception -> 0x010c }
            r2.<init>(r6)     // Catch:{ SocketTimeoutException -> 0x010a, Exception -> 0x010c }
            java.lang.String r6 = ".socketFactory.port"
            java.lang.StringBuilder r2 = r2.append(r6)     // Catch:{ SocketTimeoutException -> 0x010a, Exception -> 0x010c }
            java.lang.String r2 = r2.toString()     // Catch:{ SocketTimeoutException -> 0x010a, Exception -> 0x010c }
            r6 = 0
            java.lang.String r2 = r13.getProperty(r2, r6)     // Catch:{ SocketTimeoutException -> 0x010a, Exception -> 0x010c }
            if (r2 == 0) goto L_0x00e2
            int r3 = java.lang.Integer.parseInt(r2)     // Catch:{ NumberFormatException -> 0x0163 }
        L_0x00e2:
            r2 = -1
            if (r3 != r2) goto L_0x00e6
            r3 = r12
        L_0x00e6:
            r2 = r11
            r6 = r15
            java.net.Socket r2 = createSocket(r0, r1, r2, r3, r4, r5, r6)     // Catch:{ SocketTimeoutException -> 0x010a, Exception -> 0x010c }
        L_0x00ec:
            if (r2 != 0) goto L_0x0168
            r5 = 0
            r2 = r11
            r3 = r12
            r6 = r15
            java.net.Socket r0 = createSocket(r0, r1, r2, r3, r4, r5, r6)
            r1 = r0
        L_0x00f7:
            r0 = -1
            if (r9 == 0) goto L_0x00fe
            int r0 = java.lang.Integer.parseInt(r9)     // Catch:{ NumberFormatException -> 0x0166 }
        L_0x00fe:
            if (r0 < 0) goto L_0x0103
            r1.setSoTimeout(r0)
        L_0x0103:
            configureSSLSocket(r1, r13, r14)
            return r1
        L_0x0107:
            r2 = 1
            r7 = r2
            goto L_0x00a5
        L_0x010a:
            r0 = move-exception
            throw r0
        L_0x010c:
            r2 = move-exception
            if (r7 != 0) goto L_0x016c
            boolean r0 = r2 instanceof java.lang.reflect.InvocationTargetException
            if (r0 == 0) goto L_0x016a
            r0 = r2
            java.lang.reflect.InvocationTargetException r0 = (java.lang.reflect.InvocationTargetException) r0
            java.lang.Throwable r0 = r0.getTargetException()
            boolean r1 = r0 instanceof java.lang.Exception
            if (r1 == 0) goto L_0x016a
            java.lang.Exception r0 = (java.lang.Exception) r0
        L_0x0120:
            boolean r1 = r0 instanceof java.io.IOException
            if (r1 == 0) goto L_0x0127
            java.io.IOException r0 = (java.io.IOException) r0
            throw r0
        L_0x0127:
            java.io.IOException r1 = new java.io.IOException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r4 = "Couldn't connect using \""
            r2.<init>(r4)
            java.lang.StringBuilder r2 = r2.append(r10)
            java.lang.String r4 = "\" socket factory to host, port: "
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.StringBuilder r2 = r2.append(r11)
            java.lang.String r4 = ", "
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "; Exception: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2)
            r1.initCause(r0)
            throw r1
        L_0x015d:
            r0 = move-exception
            goto L_0x002a
        L_0x0160:
            r2 = move-exception
            goto L_0x0081
        L_0x0163:
            r2 = move-exception
            goto L_0x00e2
        L_0x0166:
            r2 = move-exception
            goto L_0x00fe
        L_0x0168:
            r1 = r2
            goto L_0x00f7
        L_0x016a:
            r0 = r2
            goto L_0x0120
        L_0x016c:
            r2 = r8
            goto L_0x00ec
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sun.mail.util.SocketFetcher.getSocket(java.lang.String, int, java.util.Properties, java.lang.String, boolean):java.net.Socket");
    }

    public static Socket getSocket(String str, int i, Properties properties, String str2) throws IOException {
        return getSocket(str, i, properties, str2, false);
    }

    private static Socket createSocket(InetAddress inetAddress, int i, String str, int i2, int i3, SocketFactory socketFactory, boolean z) throws IOException {
        Socket socket;
        if (socketFactory != null) {
            socket = socketFactory.createSocket();
        } else if (z) {
            socket = SSLSocketFactory.getDefault().createSocket();
        } else {
            socket = new Socket();
        }
        if (inetAddress != null) {
            socket.bind(new InetSocketAddress(inetAddress, i));
        }
        if (i3 >= 0) {
            socket.connect(new InetSocketAddress(str, i2), i3);
        } else {
            socket.connect(new InetSocketAddress(str, i2));
        }
        return socket;
    }

    private static SocketFactory getSocketFactory(String str) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        Class<?> cls = null;
        if (str == null || str.length() == 0) {
            return null;
        }
        ClassLoader contextClassLoader = getContextClassLoader();
        if (contextClassLoader != null) {
            try {
                cls = contextClassLoader.loadClass(str);
            } catch (ClassNotFoundException e) {
            }
        }
        if (cls == null) {
            cls = Class.forName(str);
        }
        return (SocketFactory) cls.getMethod("getDefault", new Class[0]).invoke(new Object(), new Object[0]);
    }

    public static Socket startTLS(Socket socket) throws IOException {
        return startTLS(socket, new Properties(), "socket");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{javax.net.ssl.SSLSocketFactory.createSocket(java.net.Socket, java.lang.String, int, boolean):java.net.Socket throws java.io.IOException}
     arg types: [java.net.Socket, java.lang.String, int, int]
     candidates:
      ClspMth{javax.net.SocketFactory.createSocket(java.lang.String, int, java.net.InetAddress, int):java.net.Socket throws java.io.IOException, java.net.UnknownHostException}
      ClspMth{javax.net.SocketFactory.createSocket(java.net.InetAddress, int, java.net.InetAddress, int):java.net.Socket throws java.io.IOException}
      ClspMth{javax.net.ssl.SSLSocketFactory.createSocket(java.net.Socket, java.lang.String, int, boolean):java.net.Socket throws java.io.IOException} */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0059  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.net.Socket startTLS(java.net.Socket r6, java.util.Properties r7, java.lang.String r8) throws java.io.IOException {
        /*
            java.net.InetAddress r0 = r6.getInetAddress()
            java.lang.String r2 = r0.getHostName()
            int r3 = r6.getPort()
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0040 }
            java.lang.String r1 = java.lang.String.valueOf(r8)     // Catch:{ Exception -> 0x0040 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x0040 }
            java.lang.String r1 = ".socketFactory.class"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0040 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0040 }
            r1 = 0
            java.lang.String r0 = r7.getProperty(r0, r1)     // Catch:{ Exception -> 0x0040 }
            javax.net.SocketFactory r0 = getSocketFactory(r0)     // Catch:{ Exception -> 0x0040 }
            if (r0 == 0) goto L_0x0039
            boolean r1 = r0 instanceof javax.net.ssl.SSLSocketFactory     // Catch:{ Exception -> 0x0040 }
            if (r1 == 0) goto L_0x0039
            javax.net.ssl.SSLSocketFactory r0 = (javax.net.ssl.SSLSocketFactory) r0     // Catch:{ Exception -> 0x0040 }
        L_0x0030:
            r1 = 1
            java.net.Socket r0 = r0.createSocket(r6, r2, r3, r1)     // Catch:{ Exception -> 0x0040 }
            configureSSLSocket(r0, r7, r8)     // Catch:{ Exception -> 0x0040 }
            return r0
        L_0x0039:
            javax.net.SocketFactory r0 = javax.net.ssl.SSLSocketFactory.getDefault()     // Catch:{ Exception -> 0x0040 }
            javax.net.ssl.SSLSocketFactory r0 = (javax.net.ssl.SSLSocketFactory) r0     // Catch:{ Exception -> 0x0040 }
            goto L_0x0030
        L_0x0040:
            r1 = move-exception
            boolean r0 = r1 instanceof java.lang.reflect.InvocationTargetException
            if (r0 == 0) goto L_0x0085
            r0 = r1
            java.lang.reflect.InvocationTargetException r0 = (java.lang.reflect.InvocationTargetException) r0
            java.lang.Throwable r0 = r0.getTargetException()
            boolean r4 = r0 instanceof java.lang.Exception
            if (r4 == 0) goto L_0x0085
            java.lang.Exception r0 = (java.lang.Exception) r0
        L_0x0052:
            boolean r1 = r0 instanceof java.io.IOException
            if (r1 == 0) goto L_0x0059
            java.io.IOException r0 = (java.io.IOException) r0
            throw r0
        L_0x0059:
            java.io.IOException r1 = new java.io.IOException
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = "Exception in startTLS: host "
            r4.<init>(r5)
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r4 = ", port "
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "; Exception: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2)
            r1.initCause(r0)
            throw r1
        L_0x0085:
            r0 = r1
            goto L_0x0052
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sun.mail.util.SocketFetcher.startTLS(java.net.Socket, java.util.Properties, java.lang.String):java.net.Socket");
    }

    private static void configureSSLSocket(Socket socket, Properties properties, String str) {
        if (socket instanceof SSLSocket) {
            SSLSocket sSLSocket = (SSLSocket) socket;
            String property = properties.getProperty(String.valueOf(str) + ".ssl.protocols", null);
            if (property != null) {
                sSLSocket.setEnabledProtocols(stringArray(property));
            } else {
                sSLSocket.setEnabledProtocols(new String[]{"TLSv1"});
            }
            String property2 = properties.getProperty(String.valueOf(str) + ".ssl.ciphersuites", null);
            if (property2 != null) {
                sSLSocket.setEnabledCipherSuites(stringArray(property2));
            }
        }
    }

    private static String[] stringArray(String str) {
        StringTokenizer stringTokenizer = new StringTokenizer(str);
        ArrayList arrayList = new ArrayList();
        while (stringTokenizer.hasMoreTokens()) {
            arrayList.add(stringTokenizer.nextToken());
        }
        return (String[]) arrayList.toArray(new String[arrayList.size()]);
    }

    private static ClassLoader getContextClassLoader() {
        return (ClassLoader) AccessController.doPrivileged(new PrivilegedAction() {
            public Object run() {
                try {
                    return Thread.currentThread().getContextClassLoader();
                } catch (SecurityException e) {
                    return null;
                }
            }
        });
    }
}
