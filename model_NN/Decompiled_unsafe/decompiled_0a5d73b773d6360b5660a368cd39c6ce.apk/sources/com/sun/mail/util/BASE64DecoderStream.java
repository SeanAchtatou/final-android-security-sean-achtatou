package com.sun.mail.util;

import com.sun.mail.iap.Response;
import java.io.EOFException;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class BASE64DecoderStream extends FilterInputStream {
    private static final char[] pem_array = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};
    private static final byte[] pem_convert_array = new byte[256];
    private byte[] buffer = new byte[3];
    private int bufsize = 0;
    private boolean ignoreErrors = false;
    private int index = 0;
    private byte[] input_buffer = new byte[8190];
    private int input_len = 0;
    private int input_pos = 0;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BASE64DecoderStream(InputStream inputStream) {
        super(inputStream);
        boolean z = false;
        try {
            String property = System.getProperty("mail.mime.base64.ignoreerrors");
            if (property != null && !property.equalsIgnoreCase("false")) {
                z = true;
            }
            this.ignoreErrors = z;
        } catch (SecurityException e) {
        }
    }

    public BASE64DecoderStream(InputStream inputStream, boolean z) {
        super(inputStream);
        this.ignoreErrors = z;
    }

    public int read() throws IOException {
        if (this.index >= this.bufsize) {
            this.bufsize = decode(this.buffer, 0, this.buffer.length);
            if (this.bufsize <= 0) {
                return -1;
            }
            this.index = 0;
        }
        byte[] bArr = this.buffer;
        int i = this.index;
        this.index = i + 1;
        return bArr[i] & 255;
    }

    public int read(byte[] bArr, int i, int i2) throws IOException {
        int i3 = i;
        while (this.index < this.bufsize && i2 > 0) {
            byte[] bArr2 = this.buffer;
            int i4 = this.index;
            this.index = i4 + 1;
            bArr[i3] = bArr2[i4];
            i2--;
            i3++;
        }
        if (this.index >= this.bufsize) {
            this.index = 0;
            this.bufsize = 0;
        }
        int i5 = (i2 / 3) * 3;
        if (i5 > 0) {
            int decode = decode(bArr, i3, i5);
            i3 += decode;
            i2 -= decode;
            if (decode != i5) {
                if (i3 == i) {
                    return -1;
                }
                return i3 - i;
            }
        }
        while (i2 > 0) {
            int read = read();
            if (read == -1) {
                break;
            }
            bArr[i3] = (byte) read;
            i2--;
            i3++;
        }
        if (i3 == i) {
            return -1;
        }
        return i3 - i;
    }

    public boolean markSupported() {
        return false;
    }

    public int available() throws IOException {
        return ((this.in.available() * 3) / 4) + (this.bufsize - this.index);
    }

    static {
        for (int i = 0; i < 255; i++) {
            pem_convert_array[i] = -1;
        }
        for (int i2 = 0; i2 < pem_array.length; i2++) {
            pem_convert_array[pem_array[i2]] = (byte) i2;
        }
    }

    private int decode(byte[] bArr, int i, int i2) throws IOException {
        boolean z;
        int i3 = 1;
        int i4 = i;
        while (i2 >= 3) {
            int i5 = 0;
            for (int i6 = 0; i6 < 4; i6++) {
                int i7 = getByte();
                if (i7 == -1 || i7 == -2) {
                    if (i7 == -1) {
                        if (i6 == 0) {
                            return i4 - i;
                        }
                        if (!this.ignoreErrors) {
                            throw new IOException("Error in encoded stream: needed 4 valid base64 characters but only got " + i6 + " before EOF" + recentChars());
                        }
                        z = true;
                    } else if (i6 < 2 && !this.ignoreErrors) {
                        throw new IOException("Error in encoded stream: needed at least 2 valid base64 characters, but only got " + i6 + " before padding character (=)" + recentChars());
                    } else if (i6 == 0) {
                        return i4 - i;
                    } else {
                        z = false;
                    }
                    int i8 = i6 - 1;
                    if (i8 != 0) {
                        i3 = i8;
                    }
                    int i9 = i5 << 6;
                    for (int i10 = i6 + 1; i10 < 4; i10++) {
                        if (!z) {
                            int i11 = getByte();
                            if (i11 == -1) {
                                if (!this.ignoreErrors) {
                                    throw new IOException("Error in encoded stream: hit EOF while looking for padding characters (=)" + recentChars());
                                }
                            } else if (i11 != -2 && !this.ignoreErrors) {
                                throw new IOException("Error in encoded stream: found valid base64 character after a padding character (=)" + recentChars());
                            }
                        }
                        i9 <<= 6;
                    }
                    int i12 = i9 >> 8;
                    if (i3 == 2) {
                        bArr[i4 + 1] = (byte) (i12 & 255);
                    }
                    bArr[i4] = (byte) ((i12 >> 8) & 255);
                    return (i3 + i4) - i;
                }
                i5 = (i5 << 6) | i7;
            }
            bArr[i4 + 2] = (byte) (i5 & 255);
            int i13 = i5 >> 8;
            bArr[i4 + 1] = (byte) (i13 & 255);
            bArr[i4] = (byte) ((i13 >> 8) & 255);
            i2 -= 3;
            i4 += 3;
        }
        return i4 - i;
    }

    private int getByte() throws IOException {
        byte b;
        do {
            if (this.input_pos >= this.input_len) {
                try {
                    this.input_len = this.in.read(this.input_buffer);
                    if (this.input_len <= 0) {
                        return -1;
                    }
                    this.input_pos = 0;
                } catch (EOFException e) {
                    return -1;
                }
            }
            byte[] bArr = this.input_buffer;
            int i = this.input_pos;
            this.input_pos = i + 1;
            byte b2 = bArr[i] & 255;
            if (b2 == 61) {
                return -2;
            }
            b = pem_convert_array[b2];
        } while (b == -1);
        return b;
    }

    private String recentChars() {
        int i = this.input_pos > 10 ? 10 : this.input_pos;
        if (i <= 0) {
            return "";
        }
        String str = String.valueOf("") + ", the " + i + " most recent characters were: \"";
        for (int i2 = this.input_pos - i; i2 < this.input_pos; i2++) {
            char c = (char) (this.input_buffer[i2] & 255);
            switch (c) {
                case 9:
                    str = String.valueOf(str) + "\\t";
                    break;
                case 10:
                    str = String.valueOf(str) + "\\n";
                    break;
                case 11:
                case Response.BAD:
                default:
                    if (c >= ' ' && c < 127) {
                        str = String.valueOf(str) + c;
                        break;
                    } else {
                        str = String.valueOf(str) + "\\" + ((int) c);
                        break;
                    }
                    break;
                case 13:
                    str = String.valueOf(str) + "\\r";
                    break;
            }
        }
        return String.valueOf(str) + "\"";
    }

    public static byte[] decode(byte[] bArr) {
        int i;
        int i2 = 0;
        int length = (bArr.length / 4) * 3;
        if (length == 0) {
            return bArr;
        }
        if (bArr[bArr.length - 1] == 61) {
            length--;
            if (bArr[bArr.length - 2] == 61) {
                length--;
            }
        }
        byte[] bArr2 = new byte[length];
        int i3 = 0;
        int length2 = bArr.length;
        while (length2 > 0) {
            int i4 = 3;
            int i5 = i2 + 1;
            int i6 = i5 + 1;
            int i7 = ((pem_convert_array[bArr[i2] & 255] << 6) | pem_convert_array[bArr[i5] & 255]) << 6;
            if (bArr[i6] != 61) {
                i = i6 + 1;
                i7 |= pem_convert_array[bArr[i6] & 255];
            } else {
                i4 = 2;
                i = i6;
            }
            int i8 = i7 << 6;
            if (bArr[i] != 61) {
                i8 |= pem_convert_array[bArr[i] & 255];
                i++;
            } else {
                i4--;
            }
            if (i4 > 2) {
                bArr2[i3 + 2] = (byte) (i8 & 255);
            }
            int i9 = i8 >> 8;
            if (i4 > 1) {
                bArr2[i3 + 1] = (byte) (i9 & 255);
            }
            bArr2[i3] = (byte) ((i9 >> 8) & 255);
            i3 = i4 + i3;
            length2 -= 4;
            i2 = i;
        }
        return bArr2;
    }
}
