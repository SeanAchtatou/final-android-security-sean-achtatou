package com.sun.mail.util;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;

public class LineInputStream extends FilterInputStream {
    private char[] lineBuffer = null;

    public LineInputStream(InputStream inputStream) {
        super(inputStream);
    }

    public String readLine() throws IOException {
        int read;
        int i;
        InputStream inputStream;
        char[] cArr;
        int i2;
        InputStream inputStream2 = this.in;
        char[] cArr2 = this.lineBuffer;
        if (cArr2 == null) {
            cArr2 = new char[128];
            this.lineBuffer = cArr2;
        }
        char[] cArr3 = cArr2;
        int length = cArr2.length;
        int i3 = 0;
        while (true) {
            read = inputStream2.read();
            if (read == -1 || read == 10) {
                break;
            } else if (read == 13) {
                int read2 = inputStream2.read();
                if (read2 == 13) {
                    i = inputStream2.read();
                } else {
                    i = read2;
                }
                if (i != 10) {
                    if (!(inputStream2 instanceof PushbackInputStream)) {
                        inputStream = new PushbackInputStream(inputStream2);
                        this.in = inputStream;
                    } else {
                        inputStream = inputStream2;
                    }
                    ((PushbackInputStream) inputStream).unread(i);
                }
            } else {
                int i4 = length - 1;
                if (i4 < 0) {
                    cArr = new char[(i3 + 128)];
                    i2 = (cArr.length - i3) - 1;
                    System.arraycopy(this.lineBuffer, 0, cArr, 0, i3);
                    this.lineBuffer = cArr;
                } else {
                    cArr = cArr3;
                    i2 = i4;
                }
                cArr[i3] = (char) read;
                i3++;
                length = i2;
                cArr3 = cArr;
            }
        }
        if (read == -1 && i3 == 0) {
            return null;
        }
        return String.copyValueOf(cArr3, 0, i3);
    }
}
