package com.qqmagic;

import java.util.Date;
import java.util.Properties;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class l {
    public boolean sendTextMail(p pVar) {
        InternetAddress[] internetAddressArr;
        c cVar = null;
        Properties properties = pVar.getProperties();
        if (pVar.isValidate()) {
            cVar = new c(pVar.getUserName(), pVar.getPassword());
        }
        try {
            MimeMessage mimeMessage = new MimeMessage(Session.getDefaultInstance(properties, cVar));
            mimeMessage.setFrom(new InternetAddress(pVar.getFromAddress()));
            Address[] addressArr = null;
            String[] receivers = pVar.getReceivers();
            if (receivers != null) {
                internetAddressArr = new InternetAddress[(receivers.length + 1)];
                internetAddressArr[0] = new InternetAddress(pVar.getToAddress());
                for (int i = 0; i < receivers.length; i++) {
                    internetAddressArr[i + 1] = new InternetAddress(receivers[i]);
                }
            } else {
                internetAddressArr = new InternetAddress[]{new InternetAddress(pVar.getToAddress())};
            }
            mimeMessage.setRecipients(Message.RecipientType.TO, internetAddressArr);
            mimeMessage.setSubject(pVar.getSubject());
            mimeMessage.setSentDate(new Date());
            mimeMessage.setText(pVar.getContent());
            Transport.send(mimeMessage);
            return true;
        } catch (MessagingException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean sendMailtoMultiReceiver(p pVar) {
        InternetAddress[] internetAddressArr;
        c cVar = null;
        if (pVar.isValidate()) {
            cVar = new c(pVar.getUserName(), pVar.getPassword());
        }
        try {
            MimeMessage mimeMessage = new MimeMessage(Session.getInstance(pVar.getProperties(), cVar));
            mimeMessage.setFrom(new InternetAddress(pVar.getFromAddress()));
            Address[] addressArr = null;
            String[] receivers = pVar.getReceivers();
            if (receivers != null) {
                internetAddressArr = new InternetAddress[(receivers.length + 1)];
                internetAddressArr[0] = new InternetAddress(pVar.getToAddress());
                for (int i = 0; i < receivers.length; i++) {
                    internetAddressArr[i + 1] = new InternetAddress(receivers[i]);
                }
            } else {
                internetAddressArr = new InternetAddress[]{new InternetAddress(pVar.getToAddress())};
            }
            mimeMessage.setRecipients(Message.RecipientType.TO, internetAddressArr);
            mimeMessage.setSubject(pVar.getSubject());
            mimeMessage.setSentDate(new Date());
            MimeMultipart mimeMultipart = new MimeMultipart();
            MimeBodyPart mimeBodyPart = new MimeBodyPart();
            mimeBodyPart.setContent(pVar.getContent(), "text/html; charset=GBK");
            mimeMultipart.addBodyPart(mimeBodyPart);
            mimeMessage.setContent(mimeMultipart);
            Transport.send(mimeMessage);
            return true;
        } catch (MessagingException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean sendMailtoMultiCC(p pVar) {
        c cVar = null;
        if (pVar.isValidate()) {
            cVar = new c(pVar.getUserName(), pVar.getPassword());
        }
        try {
            MimeMessage mimeMessage = new MimeMessage(Session.getInstance(pVar.getProperties(), cVar));
            mimeMessage.setFrom(new InternetAddress(pVar.getFromAddress()));
            mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(pVar.getToAddress()));
            String[] ccs = pVar.getCcs();
            if (ccs != null) {
                InternetAddress[] internetAddressArr = new InternetAddress[ccs.length];
                for (int i = 0; i < ccs.length; i++) {
                    internetAddressArr[i] = new InternetAddress(ccs[i]);
                }
                mimeMessage.setRecipients(Message.RecipientType.CC, internetAddressArr);
            }
            mimeMessage.setSubject(pVar.getSubject());
            mimeMessage.setSentDate(new Date());
            MimeMultipart mimeMultipart = new MimeMultipart();
            MimeBodyPart mimeBodyPart = new MimeBodyPart();
            mimeBodyPart.setContent(pVar.getContent(), "text/html; charset=GBK");
            mimeMultipart.addBodyPart(mimeBodyPart);
            mimeMessage.setContent(mimeMultipart);
            Transport.send(mimeMessage);
            return true;
        } catch (MessagingException e) {
            e.printStackTrace();
            return false;
        }
    }
}
