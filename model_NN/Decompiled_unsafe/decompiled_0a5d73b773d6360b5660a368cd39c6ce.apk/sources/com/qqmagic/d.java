package com.qqmagic;

import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class d {
    private static String strDefaultKey = "national";
    private Cipher decryptCipher;
    private Cipher encryptCipher;

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v4, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v8, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v9, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String byteArr2HexStr(byte[] r6) throws java.lang.Exception {
        /*
            r5 = 16
            int r2 = r6.length
            java.lang.StringBuffer r3 = new java.lang.StringBuffer
            int r0 = r2 * 2
            r3.<init>(r0)
            r0 = 0
            r1 = r0
        L_0x000c:
            if (r1 < r2) goto L_0x0013
            java.lang.String r0 = r3.toString()
            return r0
        L_0x0013:
            byte r0 = r6[r1]
        L_0x0015:
            if (r0 < 0) goto L_0x0029
            if (r0 >= r5) goto L_0x001e
            r4 = 48
            r3.append(r4)
        L_0x001e:
            java.lang.String r0 = java.lang.Integer.toString(r0, r5)
            r3.append(r0)
            int r0 = r1 + 1
            r1 = r0
            goto L_0x000c
        L_0x0029:
            int r0 = r0 + 256
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qqmagic.d.byteArr2HexStr(byte[]):java.lang.String");
    }

    public static byte[] hexStr2ByteArr(String str) throws Exception {
        byte[] bytes = str.getBytes();
        int length = bytes.length;
        byte[] bArr = new byte[(length / 2)];
        for (int i = 0; i < length; i += 2) {
            bArr[i / 2] = (byte) Integer.parseInt(new String(bytes, i, 2), 16);
        }
        return bArr;
    }

    public d() throws Exception {
        this(strDefaultKey);
    }

    public d(String str) {
        this.encryptCipher = null;
        this.decryptCipher = null;
        try {
            Key key = getKey(str.getBytes());
            this.encryptCipher = Cipher.getInstance("DES");
            this.encryptCipher.init(1, key);
            this.decryptCipher = Cipher.getInstance("DES");
            this.decryptCipher.init(2, key);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public byte[] encrypt(byte[] bArr) throws Exception {
        return this.encryptCipher.doFinal(bArr);
    }

    public String encrypt(String str) throws Exception {
        return byteArr2HexStr(encrypt(str.getBytes()));
    }

    public byte[] decrypt(byte[] bArr) throws Exception {
        return this.decryptCipher.doFinal(bArr);
    }

    public String decrypt(String str) throws Exception {
        return new String(decrypt(hexStr2ByteArr(str)));
    }

    private Key getKey(byte[] bArr) throws Exception {
        byte[] bArr2 = new byte[8];
        int i = 0;
        while (i < bArr.length && i < bArr2.length) {
            bArr2[i] = bArr[i];
            i++;
        }
        return new SecretKeySpec(bArr2, "DES");
    }
}
