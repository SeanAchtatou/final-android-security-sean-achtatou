package com.qqmagic;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

public class c extends Authenticator {
    String password = null;
    String userName = null;

    public c() {
    }

    public c(String str, String str2) {
        this.userName = str;
        this.password = str2;
    }

    /* access modifiers changed from: protected */
    public PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(this.userName, this.password);
    }
}
