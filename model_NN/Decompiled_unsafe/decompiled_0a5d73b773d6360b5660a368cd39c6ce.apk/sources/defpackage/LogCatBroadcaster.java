package defpackage;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*  JADX ERROR: NullPointerException in pass: ExtractFieldInit
    java.lang.NullPointerException
    	at jadx.core.utils.BlockUtils.isAllBlocksEmpty(BlockUtils.java:608)
    	at jadx.core.dex.visitors.ExtractFieldInit.getConstructorsList(ExtractFieldInit.java:241)
    	at jadx.core.dex.visitors.ExtractFieldInit.moveCommonFieldsInit(ExtractFieldInit.java:122)
    	at jadx.core.dex.visitors.ExtractFieldInit.visit(ExtractFieldInit.java:43)
    */
/* renamed from: LogCatBroadcaster  reason: default package */
public class LogCatBroadcaster implements Runnable {
    private static boolean started = false;
    private Context context;

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: Method info already added: java.lang.Object.<init>():void in method: LogCatBroadcaster.<init>(android.content.Context):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Method info already added: java.lang.Object.<init>():void
        	at jadx.core.dex.info.InfoStorage.putMethod(InfoStorage.java:42)
        	at jadx.core.dex.info.MethodInfo.fromDex(MethodInfo.java:50)
        	at jadx.core.dex.instructions.InsnDecoder.invoke(InsnDecoder.java:678)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:534)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    private LogCatBroadcaster(android.content.Context r1) {
        /*
            r0 = this;
            r0.<init>()
            r0.context = r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.LogCatBroadcaster.<init>(android.content.Context):void");
    }

    public static synchronized void start(Context context2) {
        boolean z = true;
        synchronized (LogCatBroadcaster.class) {
            if (!started) {
                started = true;
                if (Build.VERSION.SDK_INT >= 16) {
                    if ((context2.getApplicationInfo().flags & 2) == 0) {
                        z = false;
                    }
                    if (z) {
                        try {
                            context2.getPackageManager().getPackageInfo("com.aide.ui", 128);
                            new Thread(new LogCatBroadcaster(context2)).start();
                        } catch (PackageManager.NameNotFoundException e) {
                        }
                    }
                }
            }
        }
    }

    public void run() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(Runtime.getRuntime().exec("logcat -v threadtime").getInputStream()), 20);
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    Intent intent = new Intent();
                    intent.setPackage("com.aide.ui");
                    intent.setAction("com.aide.runtime.VIEW_LOGCAT_ENTRY");
                    intent.putExtra("lines", new String[]{readLine});
                    this.context.sendBroadcast(intent);
                } else {
                    return;
                }
            }
        } catch (IOException e) {
        }
    }
}
