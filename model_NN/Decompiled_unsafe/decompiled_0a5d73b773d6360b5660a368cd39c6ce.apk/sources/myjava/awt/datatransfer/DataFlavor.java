package myjava.awt.datatransfer;

import java.io.ByteArrayInputStream;
import java.io.CharArrayReader;
import java.io.Externalizable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.Reader;
import java.io.Serializable;
import java.io.StringReader;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import myjava.awt.datatransfer.MimeTypeProcessor;
import org.apache.harmony.awt.datatransfer.DTK;
import org.apache.harmony.awt.datatransfer.DataProvider;
import org.apache.harmony.awt.internal.nls.Messages;

public class DataFlavor implements Externalizable, Cloneable {
    public static final DataFlavor javaFileListFlavor = new DataFlavor("application/x-java-file-list; class=java.util.List", "application/x-java-file-list");
    public static final String javaJVMLocalObjectMimeType = "application/x-java-jvm-local-objectref";
    public static final String javaRemoteObjectMimeType = "application/x-java-remote-object";
    public static final String javaSerializedObjectMimeType = "application/x-java-serialized-object";
    @Deprecated
    public static final DataFlavor plainTextFlavor = new DataFlavor("text/plain; charset=unicode; class=java.io.InputStream", "Plain Text");
    private static DataFlavor plainUnicodeFlavor = null;
    private static final long serialVersionUID = 8367026044764648243L;
    private static final String[] sortedTextFlavors = {"text/sgml", "text/xml", "text/html", "text/rtf", "text/enriched", "text/richtext", DataProvider.TYPE_URILIST, "text/tab-separated-values", "text/t140", "text/rfc822-headers", "text/parityfec", "text/directory", "text/css", "text/calendar", "application/x-java-serialized-object", "text/plain"};
    public static final DataFlavor stringFlavor = new DataFlavor("application/x-java-serialized-object; class=java.lang.String", "Unicode String");
    private String humanPresentableName;
    private MimeTypeProcessor.MimeType mimeInfo;
    private Class<?> representationClass;

    public static final DataFlavor getTextPlainUnicodeFlavor() {
        if (plainUnicodeFlavor == null) {
            plainUnicodeFlavor = new DataFlavor("text/plain; charset=" + DTK.getDTK().getDefaultCharset() + "; class=java.io.InputStream", "Plain Text");
        }
        return plainUnicodeFlavor;
    }

    protected static final Class<?> tryToLoadClass(String str, ClassLoader classLoader) throws ClassNotFoundException {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            try {
                return ClassLoader.getSystemClassLoader().loadClass(str);
            } catch (ClassNotFoundException e2) {
                ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
                if (contextClassLoader != null) {
                    try {
                        return contextClassLoader.loadClass(str);
                    } catch (ClassNotFoundException e3) {
                    }
                }
                return classLoader.loadClass(str);
            }
        }
    }

    private static boolean isCharsetSupported(String str) {
        try {
            return Charset.isSupported(str);
        } catch (IllegalCharsetNameException e) {
            return false;
        }
    }

    public DataFlavor() {
        this.mimeInfo = null;
        this.humanPresentableName = null;
        this.representationClass = null;
    }

    public DataFlavor(Class<?> cls, String str) {
        this.mimeInfo = new MimeTypeProcessor.MimeType("application", "x-java-serialized-object");
        if (str != null) {
            this.humanPresentableName = str;
        } else {
            this.humanPresentableName = "application/x-java-serialized-object";
        }
        this.mimeInfo.addParameter("class", cls.getName());
        this.representationClass = cls;
    }

    public DataFlavor(String str, String str2) {
        try {
            init(str, str2, null);
        } catch (ClassNotFoundException e) {
            throw new IllegalArgumentException(Messages.getString("awt.16C", this.mimeInfo.getParameter("class")), e);
        }
    }

    public DataFlavor(String str) throws ClassNotFoundException {
        init(str, null, null);
    }

    public DataFlavor(String str, String str2, ClassLoader classLoader) throws ClassNotFoundException {
        init(str, str2, classLoader);
    }

    private void init(String str, String str2, ClassLoader classLoader) throws ClassNotFoundException {
        Class<?> loadClass;
        try {
            this.mimeInfo = MimeTypeProcessor.parse(str);
            if (str2 != null) {
                this.humanPresentableName = str2;
            } else {
                this.humanPresentableName = String.valueOf(this.mimeInfo.getPrimaryType()) + '/' + this.mimeInfo.getSubType();
            }
            String parameter = this.mimeInfo.getParameter("class");
            if (parameter == null) {
                parameter = "java.io.InputStream";
                this.mimeInfo.addParameter("class", parameter);
            }
            if (classLoader == null) {
                loadClass = Class.forName(parameter);
            } else {
                loadClass = classLoader.loadClass(parameter);
            }
            this.representationClass = loadClass;
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(Messages.getString("awt.16D", str));
        }
    }

    private String getCharset() {
        if (this.mimeInfo == null || isCharsetRedundant()) {
            return "";
        }
        String parameter = this.mimeInfo.getParameter("charset");
        if (isCharsetRequired() && (parameter == null || parameter.length() == 0)) {
            return DTK.getDTK().getDefaultCharset();
        }
        if (parameter == null) {
            return "";
        }
        return parameter;
    }

    private boolean isCharsetRequired() {
        String fullType = this.mimeInfo.getFullType();
        return fullType.equals("text/sgml") || fullType.equals("text/xml") || fullType.equals("text/html") || fullType.equals("text/enriched") || fullType.equals("text/richtext") || fullType.equals(DataProvider.TYPE_URILIST) || fullType.equals("text/directory") || fullType.equals("text/css") || fullType.equals("text/calendar") || fullType.equals("application/x-java-serialized-object") || fullType.equals("text/plain");
    }

    private boolean isCharsetRedundant() {
        String fullType = this.mimeInfo.getFullType();
        return fullType.equals("text/rtf") || fullType.equals("text/tab-separated-values") || fullType.equals("text/t140") || fullType.equals("text/rfc822-headers") || fullType.equals("text/parityfec");
    }

    /* access modifiers changed from: package-private */
    public MimeTypeProcessor.MimeType getMimeInfo() {
        return this.mimeInfo;
    }

    public String getPrimaryType() {
        if (this.mimeInfo != null) {
            return this.mimeInfo.getPrimaryType();
        }
        return null;
    }

    public String getSubType() {
        if (this.mimeInfo != null) {
            return this.mimeInfo.getSubType();
        }
        return null;
    }

    public String getMimeType() {
        if (this.mimeInfo != null) {
            return MimeTypeProcessor.assemble(this.mimeInfo);
        }
        return null;
    }

    public String getParameter(String str) {
        String lowerCase = str.toLowerCase();
        if (lowerCase.equals("humanpresentablename")) {
            return this.humanPresentableName;
        }
        if (this.mimeInfo != null) {
            return this.mimeInfo.getParameter(lowerCase);
        }
        return null;
    }

    public String getHumanPresentableName() {
        return this.humanPresentableName;
    }

    public void setHumanPresentableName(String str) {
        this.humanPresentableName = str;
    }

    public Class<?> getRepresentationClass() {
        return this.representationClass;
    }

    public final Class<?> getDefaultRepresentationClass() {
        return InputStream.class;
    }

    public final String getDefaultRepresentationClassAsString() {
        return getDefaultRepresentationClass().getName();
    }

    public boolean isRepresentationClassSerializable() {
        return Serializable.class.isAssignableFrom(this.representationClass);
    }

    public boolean isRepresentationClassRemote() {
        return false;
    }

    public boolean isRepresentationClassReader() {
        return Reader.class.isAssignableFrom(this.representationClass);
    }

    public boolean isRepresentationClassInputStream() {
        return InputStream.class.isAssignableFrom(this.representationClass);
    }

    public boolean isRepresentationClassCharBuffer() {
        return CharBuffer.class.isAssignableFrom(this.representationClass);
    }

    public boolean isRepresentationClassByteBuffer() {
        return ByteBuffer.class.isAssignableFrom(this.representationClass);
    }

    /* access modifiers changed from: protected */
    @Deprecated
    public String normalizeMimeTypeParameter(String str, String str2) {
        return str2;
    }

    /* access modifiers changed from: protected */
    @Deprecated
    public String normalizeMimeType(String str) {
        return str;
    }

    public final boolean isMimeTypeEqual(DataFlavor dataFlavor) {
        if (this.mimeInfo != null) {
            return this.mimeInfo.equals(dataFlavor.mimeInfo);
        }
        return dataFlavor.mimeInfo == null;
    }

    public boolean isMimeTypeEqual(String str) {
        try {
            return this.mimeInfo.equals(MimeTypeProcessor.parse(str));
        } catch (IllegalArgumentException e) {
            return false;
        }
    }

    public synchronized void writeExternal(ObjectOutput objectOutput) throws IOException {
        objectOutput.writeObject(this.humanPresentableName);
        objectOutput.writeObject(this.mimeInfo);
    }

    public synchronized void readExternal(ObjectInput objectInput) throws IOException, ClassNotFoundException {
        this.humanPresentableName = (String) objectInput.readObject();
        this.mimeInfo = (MimeTypeProcessor.MimeType) objectInput.readObject();
        this.representationClass = this.mimeInfo != null ? Class.forName(this.mimeInfo.getParameter("class")) : null;
    }

    public Object clone() throws CloneNotSupportedException {
        MimeTypeProcessor.MimeType mimeType;
        DataFlavor dataFlavor = new DataFlavor();
        dataFlavor.humanPresentableName = this.humanPresentableName;
        dataFlavor.representationClass = this.representationClass;
        if (this.mimeInfo != null) {
            mimeType = (MimeTypeProcessor.MimeType) this.mimeInfo.clone();
        } else {
            mimeType = null;
        }
        dataFlavor.mimeInfo = mimeType;
        return dataFlavor;
    }

    public String toString() {
        return String.valueOf(getClass().getName()) + "[MimeType=(" + getMimeType() + ");humanPresentableName=" + this.humanPresentableName + "]";
    }

    public boolean isMimeTypeSerializedObject() {
        return isMimeTypeEqual("application/x-java-serialized-object");
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof DataFlavor)) {
            return false;
        }
        return equals((DataFlavor) obj);
    }

    public boolean equals(DataFlavor dataFlavor) {
        if (dataFlavor == this) {
            return true;
        }
        if (dataFlavor == null) {
            return false;
        }
        if (this.mimeInfo == null) {
            if (dataFlavor.mimeInfo != null) {
                return false;
            }
            return true;
        } else if (!this.mimeInfo.equals(dataFlavor.mimeInfo) || !this.representationClass.equals(dataFlavor.representationClass)) {
            return false;
        } else {
            if (!this.mimeInfo.getPrimaryType().equals("text") || isUnicodeFlavor()) {
                return true;
            }
            String charset = getCharset();
            String charset2 = dataFlavor.getCharset();
            if (!isCharsetSupported(charset) || !isCharsetSupported(charset2)) {
                return charset.equalsIgnoreCase(charset2);
            }
            return Charset.forName(charset).equals(Charset.forName(charset2));
        }
    }

    @Deprecated
    public boolean equals(String str) {
        if (str == null) {
            return false;
        }
        return isMimeTypeEqual(str);
    }

    public boolean match(DataFlavor dataFlavor) {
        return equals(dataFlavor);
    }

    public int hashCode() {
        return getKeyInfo().hashCode();
    }

    private String getKeyInfo() {
        String str = String.valueOf(this.mimeInfo.getFullType()) + ";class=" + this.representationClass.getName();
        return (!this.mimeInfo.getPrimaryType().equals("text") || isUnicodeFlavor()) ? str : String.valueOf(str) + ";charset=" + getCharset().toLowerCase();
    }

    public boolean isFlavorSerializedObjectType() {
        return isMimeTypeSerializedObject() && isRepresentationClassSerializable();
    }

    public boolean isFlavorRemoteObjectType() {
        return isMimeTypeEqual(javaRemoteObjectMimeType) && isRepresentationClassRemote();
    }

    public boolean isFlavorJavaFileListType() {
        return List.class.isAssignableFrom(this.representationClass) && isMimeTypeEqual(javaFileListFlavor);
    }

    public boolean isFlavorTextType() {
        if (equals(stringFlavor) || equals(plainTextFlavor)) {
            return true;
        }
        if (this.mimeInfo != null && !this.mimeInfo.getPrimaryType().equals("text")) {
            return false;
        }
        String charset = getCharset();
        if (!isByteCodeFlavor()) {
            return isUnicodeFlavor();
        }
        if (charset.length() != 0) {
            return isCharsetSupported(charset);
        }
        return true;
    }

    public Reader getReaderForText(Transferable transferable) throws UnsupportedFlavorException, IOException {
        InputStream byteArrayInputStream;
        Object transferData = transferable.getTransferData(this);
        if (transferData == null) {
            throw new IllegalArgumentException(Messages.getString("awt.16E"));
        } else if (transferData instanceof Reader) {
            Reader reader = (Reader) transferData;
            reader.reset();
            return reader;
        } else if (transferData instanceof String) {
            return new StringReader((String) transferData);
        } else {
            if (transferData instanceof CharBuffer) {
                return new CharArrayReader(((CharBuffer) transferData).array());
            }
            if (transferData instanceof char[]) {
                return new CharArrayReader((char[]) transferData);
            }
            String charset = getCharset();
            if (transferData instanceof InputStream) {
                byteArrayInputStream = (InputStream) transferData;
                byteArrayInputStream.reset();
            } else if (transferData instanceof ByteBuffer) {
                byteArrayInputStream = new ByteArrayInputStream(((ByteBuffer) transferData).array());
            } else if (transferData instanceof byte[]) {
                byteArrayInputStream = new ByteArrayInputStream((byte[]) transferData);
            } else {
                throw new IllegalArgumentException(Messages.getString("awt.16F"));
            }
            if (charset.length() == 0) {
                return new InputStreamReader(byteArrayInputStream);
            }
            return new InputStreamReader(byteArrayInputStream, charset);
        }
    }

    public static final DataFlavor selectBestTextFlavor(DataFlavor[] dataFlavorArr) {
        if (dataFlavorArr == null) {
            return null;
        }
        List<List<DataFlavor>> sortTextFlavorsByType = sortTextFlavorsByType(new LinkedList(Arrays.asList(dataFlavorArr)));
        if (sortTextFlavorsByType.isEmpty()) {
            return null;
        }
        List list = sortTextFlavorsByType.get(0);
        if (list.size() == 1) {
            return (DataFlavor) list.get(0);
        }
        if (((DataFlavor) list.get(0)).getCharset().length() == 0) {
            return selectBestFlavorWOCharset(list);
        }
        return selectBestFlavorWCharset(list);
    }

    private static DataFlavor selectBestFlavorWCharset(List<DataFlavor> list) {
        List<DataFlavor> flavors = getFlavors(list, Reader.class);
        if (flavors != null) {
            return flavors.get(0);
        }
        List<DataFlavor> flavors2 = getFlavors(list, String.class);
        if (flavors2 != null) {
            return flavors2.get(0);
        }
        List<DataFlavor> flavors3 = getFlavors(list, CharBuffer.class);
        if (flavors3 != null) {
            return flavors3.get(0);
        }
        List<DataFlavor> flavors4 = getFlavors(list, char[].class);
        if (flavors4 != null) {
            return flavors4.get(0);
        }
        return selectBestByCharset(list);
    }

    private static DataFlavor selectBestByCharset(List<DataFlavor> list) {
        List<DataFlavor> flavors = getFlavors(list, new String[]{"UTF-16", "UTF-8", "UTF-16BE", "UTF-16LE"});
        if (flavors == null && (flavors = getFlavors(list, new String[]{DTK.getDTK().getDefaultCharset()})) == null && (flavors = getFlavors(list, new String[]{"US-ASCII"})) == null) {
            flavors = selectBestByAlphabet(list);
        }
        if (flavors == null) {
            return null;
        }
        if (flavors.size() == 1) {
            return flavors.get(0);
        }
        return selectBestFlavorWOCharset(flavors);
    }

    private static List<DataFlavor> selectBestByAlphabet(List<DataFlavor> list) {
        String[] strArr = new String[list.size()];
        LinkedList linkedList = new LinkedList();
        for (int i = 0; i < strArr.length; i++) {
            strArr[i] = list.get(i).getCharset();
        }
        Arrays.sort(strArr, String.CASE_INSENSITIVE_ORDER);
        for (DataFlavor next : list) {
            if (strArr[0].equalsIgnoreCase(next.getCharset())) {
                linkedList.add(next);
            }
        }
        if (linkedList.isEmpty()) {
            return null;
        }
        return linkedList;
    }

    private static List<DataFlavor> getFlavors(List<DataFlavor> list, String[] strArr) {
        LinkedList linkedList = new LinkedList();
        Iterator<DataFlavor> it = list.iterator();
        while (it.hasNext()) {
            DataFlavor next = it.next();
            if (isCharsetSupported(next.getCharset())) {
                for (String forName : strArr) {
                    if (Charset.forName(forName).equals(Charset.forName(next.getCharset()))) {
                        linkedList.add(next);
                    }
                }
            } else {
                it.remove();
            }
        }
        if (linkedList.isEmpty()) {
            return null;
        }
        return list;
    }

    private static DataFlavor selectBestFlavorWOCharset(List<DataFlavor> list) {
        List<DataFlavor> flavors = getFlavors(list, InputStream.class);
        if (flavors != null) {
            return flavors.get(0);
        }
        List<DataFlavor> flavors2 = getFlavors(list, ByteBuffer.class);
        if (flavors2 != null) {
            return flavors2.get(0);
        }
        List<DataFlavor> flavors3 = getFlavors(list, byte[].class);
        if (flavors3 != null) {
            return flavors3.get(0);
        }
        return list.get(0);
    }

    private static List<DataFlavor> getFlavors(List<DataFlavor> list, Class<?> cls) {
        LinkedList linkedList = new LinkedList();
        for (DataFlavor next : list) {
            if (next.representationClass.equals(cls)) {
                linkedList.add(next);
            }
        }
        if (linkedList.isEmpty()) {
            return null;
        }
        return list;
    }

    private static List<List<DataFlavor>> sortTextFlavorsByType(List<DataFlavor> list) {
        LinkedList linkedList = new LinkedList();
        for (String fetchTextFlavors : sortedTextFlavors) {
            List<DataFlavor> fetchTextFlavors2 = fetchTextFlavors(list, fetchTextFlavors);
            if (fetchTextFlavors2 != null) {
                linkedList.addLast(fetchTextFlavors2);
            }
        }
        if (!list.isEmpty()) {
            linkedList.addLast(list);
        }
        return linkedList;
    }

    private static List<DataFlavor> fetchTextFlavors(List<DataFlavor> list, String str) {
        LinkedList linkedList = new LinkedList();
        Iterator<DataFlavor> it = list.iterator();
        while (it.hasNext()) {
            DataFlavor next = it.next();
            if (!next.isFlavorTextType()) {
                it.remove();
            } else if (next.mimeInfo.getFullType().equals(str)) {
                if (!linkedList.contains(next)) {
                    linkedList.add(next);
                }
                it.remove();
            }
        }
        if (linkedList.isEmpty()) {
            return null;
        }
        return linkedList;
    }

    private boolean isUnicodeFlavor() {
        return this.representationClass != null && (this.representationClass.equals(Reader.class) || this.representationClass.equals(String.class) || this.representationClass.equals(CharBuffer.class) || this.representationClass.equals(char[].class));
    }

    private boolean isByteCodeFlavor() {
        return this.representationClass != null && (this.representationClass.equals(InputStream.class) || this.representationClass.equals(ByteBuffer.class) || this.representationClass.equals(byte[].class));
    }
}
