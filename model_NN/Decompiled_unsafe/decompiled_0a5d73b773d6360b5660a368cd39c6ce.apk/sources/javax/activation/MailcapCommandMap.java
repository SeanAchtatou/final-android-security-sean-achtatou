package javax.activation;

import com.sun.activation.registries.LogSupport;
import com.sun.activation.registries.MailcapFile;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class MailcapCommandMap extends CommandMap {
    private static final int PROG = 0;
    private static MailcapFile defDB = null;
    private MailcapFile[] DB;

    public MailcapCommandMap() {
        MailcapFile loadFile;
        ArrayList arrayList = new ArrayList(5);
        arrayList.add(null);
        LogSupport.log("MailcapCommandMap: load HOME");
        try {
            String property = System.getProperty("user.home");
            if (!(property == null || (loadFile = loadFile(String.valueOf(property) + File.separator + ".mailcap")) == null)) {
                arrayList.add(loadFile);
            }
        } catch (SecurityException e) {
        }
        LogSupport.log("MailcapCommandMap: load SYS");
        try {
            MailcapFile loadFile2 = loadFile(String.valueOf(System.getProperty("java.home")) + File.separator + "lib" + File.separator + "mailcap");
            if (loadFile2 != null) {
                arrayList.add(loadFile2);
            }
        } catch (SecurityException e2) {
        }
        LogSupport.log("MailcapCommandMap: load JAR");
        loadAllResources(arrayList, "mailcap");
        LogSupport.log("MailcapCommandMap: load DEF");
        synchronized (MailcapCommandMap.class) {
            if (defDB == null) {
                defDB = loadResource("mailcap.default");
            }
        }
        if (defDB != null) {
            arrayList.add(defDB);
        }
        this.DB = new MailcapFile[arrayList.size()];
        this.DB = (MailcapFile[]) arrayList.toArray(this.DB);
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x0077 A[Catch:{ all -> 0x009f }] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x008b A[SYNTHETIC, Splitter:B:36:0x008b] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0095 A[SYNTHETIC, Splitter:B:42:0x0095] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:31:0x0071=Splitter:B:31:0x0071, B:21:0x004f=Splitter:B:21:0x004f} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.sun.activation.registries.MailcapFile loadResource(java.lang.String r6) {
        /*
            r5 = this;
            r1 = 0
            java.lang.Class r0 = r5.getClass()     // Catch:{ IOException -> 0x004d, SecurityException -> 0x006f, all -> 0x0091 }
            java.io.InputStream r2 = javax.activation.SecuritySupport.getResourceAsStream(r0, r6)     // Catch:{ IOException -> 0x004d, SecurityException -> 0x006f, all -> 0x0091 }
            if (r2 == 0) goto L_0x002e
            com.sun.activation.registries.MailcapFile r0 = new com.sun.activation.registries.MailcapFile     // Catch:{ IOException -> 0x00a3, SecurityException -> 0x00a1 }
            r0.<init>(r2)     // Catch:{ IOException -> 0x00a3, SecurityException -> 0x00a1 }
            boolean r3 = com.sun.activation.registries.LogSupport.isLoggable()     // Catch:{ IOException -> 0x00a3, SecurityException -> 0x00a1 }
            if (r3 == 0) goto L_0x0028
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00a3, SecurityException -> 0x00a1 }
            java.lang.String r4 = "MailcapCommandMap: successfully loaded mailcap file: "
            r3.<init>(r4)     // Catch:{ IOException -> 0x00a3, SecurityException -> 0x00a1 }
            java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ IOException -> 0x00a3, SecurityException -> 0x00a1 }
            java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x00a3, SecurityException -> 0x00a1 }
            com.sun.activation.registries.LogSupport.log(r3)     // Catch:{ IOException -> 0x00a3, SecurityException -> 0x00a1 }
        L_0x0028:
            if (r2 == 0) goto L_0x002d
            r2.close()     // Catch:{ IOException -> 0x0099 }
        L_0x002d:
            return r0
        L_0x002e:
            boolean r0 = com.sun.activation.registries.LogSupport.isLoggable()     // Catch:{ IOException -> 0x00a3, SecurityException -> 0x00a1 }
            if (r0 == 0) goto L_0x0046
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00a3, SecurityException -> 0x00a1 }
            java.lang.String r3 = "MailcapCommandMap: not loading mailcap file: "
            r0.<init>(r3)     // Catch:{ IOException -> 0x00a3, SecurityException -> 0x00a1 }
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ IOException -> 0x00a3, SecurityException -> 0x00a1 }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x00a3, SecurityException -> 0x00a1 }
            com.sun.activation.registries.LogSupport.log(r0)     // Catch:{ IOException -> 0x00a3, SecurityException -> 0x00a1 }
        L_0x0046:
            if (r2 == 0) goto L_0x004b
            r2.close()     // Catch:{ IOException -> 0x009d }
        L_0x004b:
            r0 = r1
            goto L_0x002d
        L_0x004d:
            r0 = move-exception
            r2 = r1
        L_0x004f:
            boolean r3 = com.sun.activation.registries.LogSupport.isLoggable()     // Catch:{ all -> 0x009f }
            if (r3 == 0) goto L_0x0067
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x009f }
            java.lang.String r4 = "MailcapCommandMap: can't load "
            r3.<init>(r4)     // Catch:{ all -> 0x009f }
            java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ all -> 0x009f }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x009f }
            com.sun.activation.registries.LogSupport.log(r3, r0)     // Catch:{ all -> 0x009f }
        L_0x0067:
            if (r2 == 0) goto L_0x004b
            r2.close()     // Catch:{ IOException -> 0x006d }
            goto L_0x004b
        L_0x006d:
            r0 = move-exception
            goto L_0x004b
        L_0x006f:
            r0 = move-exception
            r2 = r1
        L_0x0071:
            boolean r3 = com.sun.activation.registries.LogSupport.isLoggable()     // Catch:{ all -> 0x009f }
            if (r3 == 0) goto L_0x0089
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x009f }
            java.lang.String r4 = "MailcapCommandMap: can't load "
            r3.<init>(r4)     // Catch:{ all -> 0x009f }
            java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ all -> 0x009f }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x009f }
            com.sun.activation.registries.LogSupport.log(r3, r0)     // Catch:{ all -> 0x009f }
        L_0x0089:
            if (r2 == 0) goto L_0x004b
            r2.close()     // Catch:{ IOException -> 0x008f }
            goto L_0x004b
        L_0x008f:
            r0 = move-exception
            goto L_0x004b
        L_0x0091:
            r0 = move-exception
            r2 = r1
        L_0x0093:
            if (r2 == 0) goto L_0x0098
            r2.close()     // Catch:{ IOException -> 0x009b }
        L_0x0098:
            throw r0
        L_0x0099:
            r1 = move-exception
            goto L_0x002d
        L_0x009b:
            r1 = move-exception
            goto L_0x0098
        L_0x009d:
            r0 = move-exception
            goto L_0x004b
        L_0x009f:
            r0 = move-exception
            goto L_0x0093
        L_0x00a1:
            r0 = move-exception
            goto L_0x0071
        L_0x00a3:
            r0 = move-exception
            goto L_0x004f
        */
        throw new UnsupportedOperationException("Method not decompiled: javax.activation.MailcapCommandMap.loadResource(java.lang.String):com.sun.activation.registries.MailcapFile");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00ba, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00bb, code lost:
        r8 = r0;
        r0 = r2;
        r2 = null;
        r1 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00df, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00e0, code lost:
        r8 = r0;
        r0 = r2;
        r2 = null;
        r1 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x00ea, code lost:
        com.sun.activation.registries.LogSupport.log("MailcapCommandMap: can't load " + r5, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0104, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0112, code lost:
        com.sun.activation.registries.LogSupport.log("MailcapCommandMap: can't load " + r11, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x012b, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x012c, code lost:
        r2 = r0;
        r0 = r1;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0029  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00ea A[Catch:{ all -> 0x012f }] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00fe A[SYNTHETIC, Splitter:B:60:0x00fe] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0104 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:26:0x006e] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0107 A[SYNTHETIC, Splitter:B:65:0x0107] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0112  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x012b A[ExcHandler: Exception (r1v8 'e' java.lang.Exception A[CUSTOM_DECLARE]), PHI: r0 
      PHI: (r0v16 boolean) = (r0v20 boolean), (r0v20 boolean), (r0v23 boolean), (r0v23 boolean), (r0v37 boolean), (r0v37 boolean) binds: [B:60:0x00fe, B:61:?, B:50:0x00d9, B:51:?, B:35:0x0098, B:36:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:35:0x0098] */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x009b A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:95:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void loadAllResources(java.util.List r10, java.lang.String r11) {
        /*
            r9 = this;
            r2 = 0
            java.lang.ClassLoader r0 = javax.activation.SecuritySupport.getContextClassLoader()     // Catch:{ Exception -> 0x010b }
            if (r0 != 0) goto L_0x000f
            java.lang.Class r0 = r9.getClass()     // Catch:{ Exception -> 0x010b }
            java.lang.ClassLoader r0 = r0.getClassLoader()     // Catch:{ Exception -> 0x010b }
        L_0x000f:
            if (r0 == 0) goto L_0x004d
            java.net.URL[] r0 = javax.activation.SecuritySupport.getResources(r0, r11)     // Catch:{ Exception -> 0x010b }
            r4 = r0
        L_0x0016:
            if (r4 == 0) goto L_0x0027
            boolean r0 = com.sun.activation.registries.LogSupport.isLoggable()     // Catch:{ Exception -> 0x010b }
            if (r0 == 0) goto L_0x0023
            java.lang.String r0 = "MailcapCommandMap: getResources"
            com.sun.activation.registries.LogSupport.log(r0)     // Catch:{ Exception -> 0x010b }
        L_0x0023:
            r3 = r2
        L_0x0024:
            int r0 = r4.length     // Catch:{ Exception -> 0x010b }
            if (r3 < r0) goto L_0x0053
        L_0x0027:
            if (r2 != 0) goto L_0x004c
            boolean r0 = com.sun.activation.registries.LogSupport.isLoggable()
            if (r0 == 0) goto L_0x0034
            java.lang.String r0 = "MailcapCommandMap: !anyLoaded"
            com.sun.activation.registries.LogSupport.log(r0)
        L_0x0034:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = "/"
            r0.<init>(r1)
            java.lang.StringBuilder r0 = r0.append(r11)
            java.lang.String r0 = r0.toString()
            com.sun.activation.registries.MailcapFile r0 = r9.loadResource(r0)
            if (r0 == 0) goto L_0x004c
            r10.add(r0)
        L_0x004c:
            return
        L_0x004d:
            java.net.URL[] r0 = javax.activation.SecuritySupport.getSystemResources(r11)     // Catch:{ Exception -> 0x010b }
            r4 = r0
            goto L_0x0016
        L_0x0053:
            r5 = r4[r3]     // Catch:{ Exception -> 0x010b }
            r1 = 0
            boolean r0 = com.sun.activation.registries.LogSupport.isLoggable()     // Catch:{ Exception -> 0x010b }
            if (r0 == 0) goto L_0x006e
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x010b }
            java.lang.String r6 = "MailcapCommandMap: URL "
            r0.<init>(r6)     // Catch:{ Exception -> 0x010b }
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ Exception -> 0x010b }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x010b }
            com.sun.activation.registries.LogSupport.log(r0)     // Catch:{ Exception -> 0x010b }
        L_0x006e:
            java.io.InputStream r1 = javax.activation.SecuritySupport.openStream(r5)     // Catch:{ IOException -> 0x00ba, SecurityException -> 0x00df, all -> 0x0104 }
            if (r1 == 0) goto L_0x00a0
            com.sun.activation.registries.MailcapFile r0 = new com.sun.activation.registries.MailcapFile     // Catch:{ IOException -> 0x013b, SecurityException -> 0x0135, all -> 0x0104 }
            r0.<init>(r1)     // Catch:{ IOException -> 0x013b, SecurityException -> 0x0135, all -> 0x0104 }
            r10.add(r0)     // Catch:{ IOException -> 0x013b, SecurityException -> 0x0135, all -> 0x0104 }
            r2 = 1
            boolean r0 = com.sun.activation.registries.LogSupport.isLoggable()     // Catch:{ IOException -> 0x013b, SecurityException -> 0x0135, all -> 0x0104 }
            if (r0 == 0) goto L_0x0142
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x013b, SecurityException -> 0x0135, all -> 0x0104 }
            java.lang.String r6 = "MailcapCommandMap: successfully loaded mailcap file from URL: "
            r0.<init>(r6)     // Catch:{ IOException -> 0x013b, SecurityException -> 0x0135, all -> 0x0104 }
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ IOException -> 0x013b, SecurityException -> 0x0135, all -> 0x0104 }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x013b, SecurityException -> 0x0135, all -> 0x0104 }
            com.sun.activation.registries.LogSupport.log(r0)     // Catch:{ IOException -> 0x013b, SecurityException -> 0x0135, all -> 0x0104 }
            r0 = r2
        L_0x0096:
            if (r1 == 0) goto L_0x009b
            r1.close()     // Catch:{ IOException -> 0x0128, Exception -> 0x012b }
        L_0x009b:
            int r2 = r3 + 1
            r3 = r2
            r2 = r0
            goto L_0x0024
        L_0x00a0:
            boolean r0 = com.sun.activation.registries.LogSupport.isLoggable()     // Catch:{ IOException -> 0x013b, SecurityException -> 0x0135, all -> 0x0104 }
            if (r0 == 0) goto L_0x0142
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x013b, SecurityException -> 0x0135, all -> 0x0104 }
            java.lang.String r6 = "MailcapCommandMap: not loading mailcap file from URL: "
            r0.<init>(r6)     // Catch:{ IOException -> 0x013b, SecurityException -> 0x0135, all -> 0x0104 }
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ IOException -> 0x013b, SecurityException -> 0x0135, all -> 0x0104 }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x013b, SecurityException -> 0x0135, all -> 0x0104 }
            com.sun.activation.registries.LogSupport.log(r0)     // Catch:{ IOException -> 0x013b, SecurityException -> 0x0135, all -> 0x0104 }
            r0 = r2
            goto L_0x0096
        L_0x00ba:
            r0 = move-exception
            r8 = r0
            r0 = r2
            r2 = r1
            r1 = r8
        L_0x00bf:
            boolean r6 = com.sun.activation.registries.LogSupport.isLoggable()     // Catch:{ all -> 0x012f }
            if (r6 == 0) goto L_0x00d7
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x012f }
            java.lang.String r7 = "MailcapCommandMap: can't load "
            r6.<init>(r7)     // Catch:{ all -> 0x012f }
            java.lang.StringBuilder r5 = r6.append(r5)     // Catch:{ all -> 0x012f }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x012f }
            com.sun.activation.registries.LogSupport.log(r5, r1)     // Catch:{ all -> 0x012f }
        L_0x00d7:
            if (r2 == 0) goto L_0x009b
            r2.close()     // Catch:{ IOException -> 0x00dd, Exception -> 0x012b }
            goto L_0x009b
        L_0x00dd:
            r1 = move-exception
            goto L_0x009b
        L_0x00df:
            r0 = move-exception
            r8 = r0
            r0 = r2
            r2 = r1
            r1 = r8
        L_0x00e4:
            boolean r6 = com.sun.activation.registries.LogSupport.isLoggable()     // Catch:{ all -> 0x012f }
            if (r6 == 0) goto L_0x00fc
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x012f }
            java.lang.String r7 = "MailcapCommandMap: can't load "
            r6.<init>(r7)     // Catch:{ all -> 0x012f }
            java.lang.StringBuilder r5 = r6.append(r5)     // Catch:{ all -> 0x012f }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x012f }
            com.sun.activation.registries.LogSupport.log(r5, r1)     // Catch:{ all -> 0x012f }
        L_0x00fc:
            if (r2 == 0) goto L_0x009b
            r2.close()     // Catch:{ IOException -> 0x0102, Exception -> 0x012b }
            goto L_0x009b
        L_0x0102:
            r1 = move-exception
            goto L_0x009b
        L_0x0104:
            r0 = move-exception
        L_0x0105:
            if (r1 == 0) goto L_0x010a
            r1.close()     // Catch:{ IOException -> 0x0126 }
        L_0x010a:
            throw r0     // Catch:{ Exception -> 0x010b }
        L_0x010b:
            r0 = move-exception
        L_0x010c:
            boolean r1 = com.sun.activation.registries.LogSupport.isLoggable()
            if (r1 == 0) goto L_0x0027
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r3 = "MailcapCommandMap: can't load "
            r1.<init>(r3)
            java.lang.StringBuilder r1 = r1.append(r11)
            java.lang.String r1 = r1.toString()
            com.sun.activation.registries.LogSupport.log(r1, r0)
            goto L_0x0027
        L_0x0126:
            r1 = move-exception
            goto L_0x010a
        L_0x0128:
            r1 = move-exception
            goto L_0x009b
        L_0x012b:
            r1 = move-exception
            r2 = r0
            r0 = r1
            goto L_0x010c
        L_0x012f:
            r1 = move-exception
            r8 = r1
            r1 = r2
            r2 = r0
            r0 = r8
            goto L_0x0105
        L_0x0135:
            r0 = move-exception
            r8 = r0
            r0 = r2
            r2 = r1
            r1 = r8
            goto L_0x00e4
        L_0x013b:
            r0 = move-exception
            r8 = r0
            r0 = r2
            r2 = r1
            r1 = r8
            goto L_0x00bf
        L_0x0142:
            r0 = r2
            goto L_0x0096
        */
        throw new UnsupportedOperationException("Method not decompiled: javax.activation.MailcapCommandMap.loadAllResources(java.util.List, java.lang.String):void");
    }

    private MailcapFile loadFile(String str) {
        try {
            return new MailcapFile(str);
        } catch (IOException e) {
            return null;
        }
    }

    public MailcapCommandMap(String str) throws IOException {
        this();
        if (LogSupport.isLoggable()) {
            LogSupport.log("MailcapCommandMap: load PROG from " + str);
        }
        if (this.DB[0] == null) {
            this.DB[0] = new MailcapFile(str);
        }
    }

    public MailcapCommandMap(InputStream inputStream) {
        this();
        LogSupport.log("MailcapCommandMap: load PROG");
        if (this.DB[0] == null) {
            try {
                this.DB[0] = new MailcapFile(inputStream);
            } catch (IOException e) {
            }
        }
    }

    public synchronized CommandInfo[] getPreferredCommands(String str) {
        CommandInfo[] commandInfoArr;
        Map mailcapFallbackList;
        Map mailcapList;
        synchronized (this) {
            ArrayList arrayList = new ArrayList();
            if (str != null) {
                str = str.toLowerCase(Locale.ENGLISH);
            }
            for (int i = 0; i < this.DB.length; i++) {
                if (!(this.DB[i] == null || (mailcapList = this.DB[i].getMailcapList(str)) == null)) {
                    appendPrefCmdsToList(mailcapList, arrayList);
                }
            }
            for (int i2 = 0; i2 < this.DB.length; i2++) {
                if (!(this.DB[i2] == null || (mailcapFallbackList = this.DB[i2].getMailcapFallbackList(str)) == null)) {
                    appendPrefCmdsToList(mailcapFallbackList, arrayList);
                }
            }
            commandInfoArr = (CommandInfo[]) arrayList.toArray(new CommandInfo[arrayList.size()]);
        }
        return commandInfoArr;
    }

    private void appendPrefCmdsToList(Map map, List list) {
        for (String str : map.keySet()) {
            if (!checkForVerb(list, str)) {
                list.add(new CommandInfo(str, (String) ((List) map.get(str)).get(0)));
            }
        }
    }

    private boolean checkForVerb(List list, String str) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            if (((CommandInfo) it.next()).getCommandName().equals(str)) {
                return true;
            }
        }
        return false;
    }

    public synchronized CommandInfo[] getAllCommands(String str) {
        CommandInfo[] commandInfoArr;
        Map mailcapFallbackList;
        Map mailcapList;
        synchronized (this) {
            ArrayList arrayList = new ArrayList();
            if (str != null) {
                str = str.toLowerCase(Locale.ENGLISH);
            }
            for (int i = 0; i < this.DB.length; i++) {
                if (!(this.DB[i] == null || (mailcapList = this.DB[i].getMailcapList(str)) == null)) {
                    appendCmdsToList(mailcapList, arrayList);
                }
            }
            for (int i2 = 0; i2 < this.DB.length; i2++) {
                if (!(this.DB[i2] == null || (mailcapFallbackList = this.DB[i2].getMailcapFallbackList(str)) == null)) {
                    appendCmdsToList(mailcapFallbackList, arrayList);
                }
            }
            commandInfoArr = (CommandInfo[]) arrayList.toArray(new CommandInfo[arrayList.size()]);
        }
        return commandInfoArr;
    }

    private void appendCmdsToList(Map map, List list) {
        for (String str : map.keySet()) {
            for (String commandInfo : (List) map.get(str)) {
                list.add(new CommandInfo(str, commandInfo));
            }
        }
    }

    public synchronized CommandInfo getCommand(String str, String str2) {
        CommandInfo commandInfo;
        Map mailcapFallbackList;
        List list;
        String str3;
        Map mailcapList;
        List list2;
        String str4;
        int i = 0;
        synchronized (this) {
            if (str != null) {
                str = str.toLowerCase(Locale.ENGLISH);
            }
            int i2 = 0;
            while (true) {
                if (i2 >= this.DB.length) {
                    while (true) {
                        if (i >= this.DB.length) {
                            commandInfo = null;
                            break;
                        } else if (this.DB[i] != null && (mailcapFallbackList = this.DB[i].getMailcapFallbackList(str)) != null && (list = (List) mailcapFallbackList.get(str2)) != null && (str3 = (String) list.get(0)) != null) {
                            commandInfo = new CommandInfo(str2, str3);
                            break;
                        } else {
                            i++;
                        }
                    }
                } else if (this.DB[i2] != null && (mailcapList = this.DB[i2].getMailcapList(str)) != null && (list2 = (List) mailcapList.get(str2)) != null && (str4 = (String) list2.get(0)) != null) {
                    commandInfo = new CommandInfo(str2, str4);
                    break;
                } else {
                    i2++;
                }
            }
        }
        return commandInfo;
    }

    public synchronized void addMailcap(String str) {
        LogSupport.log("MailcapCommandMap: add to PROG");
        if (this.DB[0] == null) {
            this.DB[0] = new MailcapFile();
        }
        this.DB[0].appendToMailcap(str);
    }

    public synchronized DataContentHandler createDataContentHandler(String str) {
        DataContentHandler dataContentHandler;
        List list;
        List list2;
        int i = 0;
        synchronized (this) {
            if (LogSupport.isLoggable()) {
                LogSupport.log("MailcapCommandMap: createDataContentHandler for " + str);
            }
            if (str != null) {
                str = str.toLowerCase(Locale.ENGLISH);
            }
            int i2 = 0;
            while (true) {
                if (i2 >= this.DB.length) {
                    while (true) {
                        if (i >= this.DB.length) {
                            dataContentHandler = null;
                            break;
                        }
                        if (this.DB[i] != null) {
                            if (LogSupport.isLoggable()) {
                                LogSupport.log("  search fallback DB #" + i);
                            }
                            Map mailcapFallbackList = this.DB[i].getMailcapFallbackList(str);
                            if (!(mailcapFallbackList == null || (list = (List) mailcapFallbackList.get("content-handler")) == null || (dataContentHandler = getDataContentHandler((String) list.get(0))) == null)) {
                                break;
                            }
                        }
                        i++;
                    }
                } else {
                    if (this.DB[i2] != null) {
                        if (LogSupport.isLoggable()) {
                            LogSupport.log("  search DB #" + i2);
                        }
                        Map mailcapList = this.DB[i2].getMailcapList(str);
                        if (!(mailcapList == null || (list2 = (List) mailcapList.get("content-handler")) == null || (dataContentHandler = getDataContentHandler((String) list2.get(0))) == null)) {
                            break;
                        }
                    }
                    i2++;
                }
            }
        }
        return dataContentHandler;
    }

    private DataContentHandler getDataContentHandler(String str) {
        Class<?> cls;
        if (LogSupport.isLoggable()) {
            LogSupport.log("    got content-handler");
        }
        if (LogSupport.isLoggable()) {
            LogSupport.log("      class " + str);
        }
        try {
            ClassLoader contextClassLoader = SecuritySupport.getContextClassLoader();
            if (contextClassLoader == null) {
                contextClassLoader = getClass().getClassLoader();
            }
            try {
                cls = contextClassLoader.loadClass(str);
            } catch (Exception e) {
                cls = Class.forName(str);
            }
            if (cls != null) {
                return (DataContentHandler) cls.newInstance();
            }
        } catch (IllegalAccessException e2) {
            if (LogSupport.isLoggable()) {
                LogSupport.log("Can't load DCH " + str, e2);
            }
        } catch (ClassNotFoundException e3) {
            if (LogSupport.isLoggable()) {
                LogSupport.log("Can't load DCH " + str, e3);
            }
        } catch (InstantiationException e4) {
            if (LogSupport.isLoggable()) {
                LogSupport.log("Can't load DCH " + str, e4);
            }
        }
        return null;
    }

    public synchronized String[] getMimeTypes() {
        ArrayList arrayList;
        String[] mimeTypes;
        arrayList = new ArrayList();
        for (int i = 0; i < this.DB.length; i++) {
            if (!(this.DB[i] == null || (mimeTypes = this.DB[i].getMimeTypes()) == null)) {
                for (int i2 = 0; i2 < mimeTypes.length; i2++) {
                    if (!arrayList.contains(mimeTypes[i2])) {
                        arrayList.add(mimeTypes[i2]);
                    }
                }
            }
        }
        return (String[]) arrayList.toArray(new String[arrayList.size()]);
    }

    public synchronized String[] getNativeCommands(String str) {
        ArrayList arrayList;
        String[] nativeCommands;
        arrayList = new ArrayList();
        if (str != null) {
            str = str.toLowerCase(Locale.ENGLISH);
        }
        for (int i = 0; i < this.DB.length; i++) {
            if (!(this.DB[i] == null || (nativeCommands = this.DB[i].getNativeCommands(str)) == null)) {
                for (int i2 = 0; i2 < nativeCommands.length; i2++) {
                    if (!arrayList.contains(nativeCommands[i2])) {
                        arrayList.add(nativeCommands[i2]);
                    }
                }
            }
        }
        return (String[]) arrayList.toArray(new String[arrayList.size()]);
    }
}
