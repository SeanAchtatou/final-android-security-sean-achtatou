package javax.activation;

import com.sun.activation.registries.LogSupport;
import com.sun.activation.registries.MimeTypeFile;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;

public class MimetypesFileTypeMap extends FileTypeMap {
    private static final int PROG = 0;
    private static MimeTypeFile defDB = null;
    private static String defaultType = "application/octet-stream";
    private MimeTypeFile[] DB;

    public MimetypesFileTypeMap() {
        MimeTypeFile loadFile;
        Vector vector = new Vector(5);
        vector.addElement(null);
        LogSupport.log("MimetypesFileTypeMap: load HOME");
        try {
            String property = System.getProperty("user.home");
            if (!(property == null || (loadFile = loadFile(String.valueOf(property) + File.separator + ".mime.types")) == null)) {
                vector.addElement(loadFile);
            }
        } catch (SecurityException e) {
        }
        LogSupport.log("MimetypesFileTypeMap: load SYS");
        try {
            MimeTypeFile loadFile2 = loadFile(String.valueOf(System.getProperty("java.home")) + File.separator + "lib" + File.separator + "mime.types");
            if (loadFile2 != null) {
                vector.addElement(loadFile2);
            }
        } catch (SecurityException e2) {
        }
        LogSupport.log("MimetypesFileTypeMap: load JAR");
        loadAllResources(vector, "mime.types");
        LogSupport.log("MimetypesFileTypeMap: load DEF");
        synchronized (MimetypesFileTypeMap.class) {
            if (defDB == null) {
                defDB = loadResource("/mimetypes.default");
            }
        }
        if (defDB != null) {
            vector.addElement(defDB);
        }
        this.DB = new MimeTypeFile[vector.size()];
        vector.copyInto(this.DB);
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x0077 A[Catch:{ all -> 0x009f }] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x008b A[SYNTHETIC, Splitter:B:36:0x008b] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0095 A[SYNTHETIC, Splitter:B:42:0x0095] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:31:0x0071=Splitter:B:31:0x0071, B:21:0x004f=Splitter:B:21:0x004f} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.sun.activation.registries.MimeTypeFile loadResource(java.lang.String r6) {
        /*
            r5 = this;
            r1 = 0
            java.lang.Class r0 = r5.getClass()     // Catch:{ IOException -> 0x004d, SecurityException -> 0x006f, all -> 0x0091 }
            java.io.InputStream r2 = javax.activation.SecuritySupport.getResourceAsStream(r0, r6)     // Catch:{ IOException -> 0x004d, SecurityException -> 0x006f, all -> 0x0091 }
            if (r2 == 0) goto L_0x002e
            com.sun.activation.registries.MimeTypeFile r0 = new com.sun.activation.registries.MimeTypeFile     // Catch:{ IOException -> 0x00a3, SecurityException -> 0x00a1 }
            r0.<init>(r2)     // Catch:{ IOException -> 0x00a3, SecurityException -> 0x00a1 }
            boolean r3 = com.sun.activation.registries.LogSupport.isLoggable()     // Catch:{ IOException -> 0x00a3, SecurityException -> 0x00a1 }
            if (r3 == 0) goto L_0x0028
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00a3, SecurityException -> 0x00a1 }
            java.lang.String r4 = "MimetypesFileTypeMap: successfully loaded mime types file: "
            r3.<init>(r4)     // Catch:{ IOException -> 0x00a3, SecurityException -> 0x00a1 }
            java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ IOException -> 0x00a3, SecurityException -> 0x00a1 }
            java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x00a3, SecurityException -> 0x00a1 }
            com.sun.activation.registries.LogSupport.log(r3)     // Catch:{ IOException -> 0x00a3, SecurityException -> 0x00a1 }
        L_0x0028:
            if (r2 == 0) goto L_0x002d
            r2.close()     // Catch:{ IOException -> 0x0099 }
        L_0x002d:
            return r0
        L_0x002e:
            boolean r0 = com.sun.activation.registries.LogSupport.isLoggable()     // Catch:{ IOException -> 0x00a3, SecurityException -> 0x00a1 }
            if (r0 == 0) goto L_0x0046
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00a3, SecurityException -> 0x00a1 }
            java.lang.String r3 = "MimetypesFileTypeMap: not loading mime types file: "
            r0.<init>(r3)     // Catch:{ IOException -> 0x00a3, SecurityException -> 0x00a1 }
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ IOException -> 0x00a3, SecurityException -> 0x00a1 }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x00a3, SecurityException -> 0x00a1 }
            com.sun.activation.registries.LogSupport.log(r0)     // Catch:{ IOException -> 0x00a3, SecurityException -> 0x00a1 }
        L_0x0046:
            if (r2 == 0) goto L_0x004b
            r2.close()     // Catch:{ IOException -> 0x009d }
        L_0x004b:
            r0 = r1
            goto L_0x002d
        L_0x004d:
            r0 = move-exception
            r2 = r1
        L_0x004f:
            boolean r3 = com.sun.activation.registries.LogSupport.isLoggable()     // Catch:{ all -> 0x009f }
            if (r3 == 0) goto L_0x0067
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x009f }
            java.lang.String r4 = "MimetypesFileTypeMap: can't load "
            r3.<init>(r4)     // Catch:{ all -> 0x009f }
            java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ all -> 0x009f }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x009f }
            com.sun.activation.registries.LogSupport.log(r3, r0)     // Catch:{ all -> 0x009f }
        L_0x0067:
            if (r2 == 0) goto L_0x004b
            r2.close()     // Catch:{ IOException -> 0x006d }
            goto L_0x004b
        L_0x006d:
            r0 = move-exception
            goto L_0x004b
        L_0x006f:
            r0 = move-exception
            r2 = r1
        L_0x0071:
            boolean r3 = com.sun.activation.registries.LogSupport.isLoggable()     // Catch:{ all -> 0x009f }
            if (r3 == 0) goto L_0x0089
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x009f }
            java.lang.String r4 = "MimetypesFileTypeMap: can't load "
            r3.<init>(r4)     // Catch:{ all -> 0x009f }
            java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ all -> 0x009f }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x009f }
            com.sun.activation.registries.LogSupport.log(r3, r0)     // Catch:{ all -> 0x009f }
        L_0x0089:
            if (r2 == 0) goto L_0x004b
            r2.close()     // Catch:{ IOException -> 0x008f }
            goto L_0x004b
        L_0x008f:
            r0 = move-exception
            goto L_0x004b
        L_0x0091:
            r0 = move-exception
            r2 = r1
        L_0x0093:
            if (r2 == 0) goto L_0x0098
            r2.close()     // Catch:{ IOException -> 0x009b }
        L_0x0098:
            throw r0
        L_0x0099:
            r1 = move-exception
            goto L_0x002d
        L_0x009b:
            r1 = move-exception
            goto L_0x0098
        L_0x009d:
            r0 = move-exception
            goto L_0x004b
        L_0x009f:
            r0 = move-exception
            goto L_0x0093
        L_0x00a1:
            r0 = move-exception
            goto L_0x0071
        L_0x00a3:
            r0 = move-exception
            goto L_0x004f
        */
        throw new UnsupportedOperationException("Method not decompiled: javax.activation.MimetypesFileTypeMap.loadResource(java.lang.String):com.sun.activation.registries.MimeTypeFile");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00b4, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00b5, code lost:
        r8 = r0;
        r0 = r2;
        r2 = null;
        r1 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00d9, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00da, code lost:
        r8 = r0;
        r0 = r2;
        r2 = null;
        r1 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00e4, code lost:
        com.sun.activation.registries.LogSupport.log("MimetypesFileTypeMap: can't load " + r5, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00fe, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x010c, code lost:
        com.sun.activation.registries.LogSupport.log("MimetypesFileTypeMap: can't load " + r11, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0125, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0126, code lost:
        r2 = r0;
        r0 = r1;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0029  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00e4 A[Catch:{ all -> 0x0129 }] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00f8 A[SYNTHETIC, Splitter:B:57:0x00f8] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00fe A[ExcHandler: all (th java.lang.Throwable), Splitter:B:23:0x0068] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0101 A[SYNTHETIC, Splitter:B:62:0x0101] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x010c  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0125 A[ExcHandler: Exception (r1v8 'e' java.lang.Exception A[CUSTOM_DECLARE]), PHI: r0 
      PHI: (r0v15 boolean) = (r0v19 boolean), (r0v19 boolean), (r0v22 boolean), (r0v22 boolean), (r0v36 boolean), (r0v36 boolean) binds: [B:57:0x00f8, B:58:?, B:47:0x00d3, B:48:?, B:32:0x0092, B:33:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:32:0x0092] */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x0095 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:92:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void loadAllResources(java.util.Vector r10, java.lang.String r11) {
        /*
            r9 = this;
            r2 = 0
            java.lang.ClassLoader r0 = javax.activation.SecuritySupport.getContextClassLoader()     // Catch:{ Exception -> 0x0105 }
            if (r0 != 0) goto L_0x000f
            java.lang.Class r0 = r9.getClass()     // Catch:{ Exception -> 0x0105 }
            java.lang.ClassLoader r0 = r0.getClassLoader()     // Catch:{ Exception -> 0x0105 }
        L_0x000f:
            if (r0 == 0) goto L_0x0047
            java.net.URL[] r0 = javax.activation.SecuritySupport.getResources(r0, r11)     // Catch:{ Exception -> 0x0105 }
            r4 = r0
        L_0x0016:
            if (r4 == 0) goto L_0x0027
            boolean r0 = com.sun.activation.registries.LogSupport.isLoggable()     // Catch:{ Exception -> 0x0105 }
            if (r0 == 0) goto L_0x0023
            java.lang.String r0 = "MimetypesFileTypeMap: getResources"
            com.sun.activation.registries.LogSupport.log(r0)     // Catch:{ Exception -> 0x0105 }
        L_0x0023:
            r3 = r2
        L_0x0024:
            int r0 = r4.length     // Catch:{ Exception -> 0x0105 }
            if (r3 < r0) goto L_0x004d
        L_0x0027:
            if (r2 != 0) goto L_0x0046
            java.lang.String r0 = "MimetypesFileTypeMap: !anyLoaded"
            com.sun.activation.registries.LogSupport.log(r0)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = "/"
            r0.<init>(r1)
            java.lang.StringBuilder r0 = r0.append(r11)
            java.lang.String r0 = r0.toString()
            com.sun.activation.registries.MimeTypeFile r0 = r9.loadResource(r0)
            if (r0 == 0) goto L_0x0046
            r10.addElement(r0)
        L_0x0046:
            return
        L_0x0047:
            java.net.URL[] r0 = javax.activation.SecuritySupport.getSystemResources(r11)     // Catch:{ Exception -> 0x0105 }
            r4 = r0
            goto L_0x0016
        L_0x004d:
            r5 = r4[r3]     // Catch:{ Exception -> 0x0105 }
            r1 = 0
            boolean r0 = com.sun.activation.registries.LogSupport.isLoggable()     // Catch:{ Exception -> 0x0105 }
            if (r0 == 0) goto L_0x0068
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0105 }
            java.lang.String r6 = "MimetypesFileTypeMap: URL "
            r0.<init>(r6)     // Catch:{ Exception -> 0x0105 }
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ Exception -> 0x0105 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0105 }
            com.sun.activation.registries.LogSupport.log(r0)     // Catch:{ Exception -> 0x0105 }
        L_0x0068:
            java.io.InputStream r1 = javax.activation.SecuritySupport.openStream(r5)     // Catch:{ IOException -> 0x00b4, SecurityException -> 0x00d9, all -> 0x00fe }
            if (r1 == 0) goto L_0x009a
            com.sun.activation.registries.MimeTypeFile r0 = new com.sun.activation.registries.MimeTypeFile     // Catch:{ IOException -> 0x0135, SecurityException -> 0x012f, all -> 0x00fe }
            r0.<init>(r1)     // Catch:{ IOException -> 0x0135, SecurityException -> 0x012f, all -> 0x00fe }
            r10.addElement(r0)     // Catch:{ IOException -> 0x0135, SecurityException -> 0x012f, all -> 0x00fe }
            r2 = 1
            boolean r0 = com.sun.activation.registries.LogSupport.isLoggable()     // Catch:{ IOException -> 0x0135, SecurityException -> 0x012f, all -> 0x00fe }
            if (r0 == 0) goto L_0x013c
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0135, SecurityException -> 0x012f, all -> 0x00fe }
            java.lang.String r6 = "MimetypesFileTypeMap: successfully loaded mime types from URL: "
            r0.<init>(r6)     // Catch:{ IOException -> 0x0135, SecurityException -> 0x012f, all -> 0x00fe }
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ IOException -> 0x0135, SecurityException -> 0x012f, all -> 0x00fe }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x0135, SecurityException -> 0x012f, all -> 0x00fe }
            com.sun.activation.registries.LogSupport.log(r0)     // Catch:{ IOException -> 0x0135, SecurityException -> 0x012f, all -> 0x00fe }
            r0 = r2
        L_0x0090:
            if (r1 == 0) goto L_0x0095
            r1.close()     // Catch:{ IOException -> 0x0122, Exception -> 0x0125 }
        L_0x0095:
            int r2 = r3 + 1
            r3 = r2
            r2 = r0
            goto L_0x0024
        L_0x009a:
            boolean r0 = com.sun.activation.registries.LogSupport.isLoggable()     // Catch:{ IOException -> 0x0135, SecurityException -> 0x012f, all -> 0x00fe }
            if (r0 == 0) goto L_0x013c
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0135, SecurityException -> 0x012f, all -> 0x00fe }
            java.lang.String r6 = "MimetypesFileTypeMap: not loading mime types from URL: "
            r0.<init>(r6)     // Catch:{ IOException -> 0x0135, SecurityException -> 0x012f, all -> 0x00fe }
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ IOException -> 0x0135, SecurityException -> 0x012f, all -> 0x00fe }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x0135, SecurityException -> 0x012f, all -> 0x00fe }
            com.sun.activation.registries.LogSupport.log(r0)     // Catch:{ IOException -> 0x0135, SecurityException -> 0x012f, all -> 0x00fe }
            r0 = r2
            goto L_0x0090
        L_0x00b4:
            r0 = move-exception
            r8 = r0
            r0 = r2
            r2 = r1
            r1 = r8
        L_0x00b9:
            boolean r6 = com.sun.activation.registries.LogSupport.isLoggable()     // Catch:{ all -> 0x0129 }
            if (r6 == 0) goto L_0x00d1
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x0129 }
            java.lang.String r7 = "MimetypesFileTypeMap: can't load "
            r6.<init>(r7)     // Catch:{ all -> 0x0129 }
            java.lang.StringBuilder r5 = r6.append(r5)     // Catch:{ all -> 0x0129 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0129 }
            com.sun.activation.registries.LogSupport.log(r5, r1)     // Catch:{ all -> 0x0129 }
        L_0x00d1:
            if (r2 == 0) goto L_0x0095
            r2.close()     // Catch:{ IOException -> 0x00d7, Exception -> 0x0125 }
            goto L_0x0095
        L_0x00d7:
            r1 = move-exception
            goto L_0x0095
        L_0x00d9:
            r0 = move-exception
            r8 = r0
            r0 = r2
            r2 = r1
            r1 = r8
        L_0x00de:
            boolean r6 = com.sun.activation.registries.LogSupport.isLoggable()     // Catch:{ all -> 0x0129 }
            if (r6 == 0) goto L_0x00f6
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x0129 }
            java.lang.String r7 = "MimetypesFileTypeMap: can't load "
            r6.<init>(r7)     // Catch:{ all -> 0x0129 }
            java.lang.StringBuilder r5 = r6.append(r5)     // Catch:{ all -> 0x0129 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0129 }
            com.sun.activation.registries.LogSupport.log(r5, r1)     // Catch:{ all -> 0x0129 }
        L_0x00f6:
            if (r2 == 0) goto L_0x0095
            r2.close()     // Catch:{ IOException -> 0x00fc, Exception -> 0x0125 }
            goto L_0x0095
        L_0x00fc:
            r1 = move-exception
            goto L_0x0095
        L_0x00fe:
            r0 = move-exception
        L_0x00ff:
            if (r1 == 0) goto L_0x0104
            r1.close()     // Catch:{ IOException -> 0x0120 }
        L_0x0104:
            throw r0     // Catch:{ Exception -> 0x0105 }
        L_0x0105:
            r0 = move-exception
        L_0x0106:
            boolean r1 = com.sun.activation.registries.LogSupport.isLoggable()
            if (r1 == 0) goto L_0x0027
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r3 = "MimetypesFileTypeMap: can't load "
            r1.<init>(r3)
            java.lang.StringBuilder r1 = r1.append(r11)
            java.lang.String r1 = r1.toString()
            com.sun.activation.registries.LogSupport.log(r1, r0)
            goto L_0x0027
        L_0x0120:
            r1 = move-exception
            goto L_0x0104
        L_0x0122:
            r1 = move-exception
            goto L_0x0095
        L_0x0125:
            r1 = move-exception
            r2 = r0
            r0 = r1
            goto L_0x0106
        L_0x0129:
            r1 = move-exception
            r8 = r1
            r1 = r2
            r2 = r0
            r0 = r8
            goto L_0x00ff
        L_0x012f:
            r0 = move-exception
            r8 = r0
            r0 = r2
            r2 = r1
            r1 = r8
            goto L_0x00de
        L_0x0135:
            r0 = move-exception
            r8 = r0
            r0 = r2
            r2 = r1
            r1 = r8
            goto L_0x00b9
        L_0x013c:
            r0 = r2
            goto L_0x0090
        */
        throw new UnsupportedOperationException("Method not decompiled: javax.activation.MimetypesFileTypeMap.loadAllResources(java.util.Vector, java.lang.String):void");
    }

    private MimeTypeFile loadFile(String str) {
        try {
            return new MimeTypeFile(str);
        } catch (IOException e) {
            return null;
        }
    }

    public MimetypesFileTypeMap(String str) throws IOException {
        this();
        this.DB[0] = new MimeTypeFile(str);
    }

    public MimetypesFileTypeMap(InputStream inputStream) {
        this();
        try {
            this.DB[0] = new MimeTypeFile(inputStream);
        } catch (IOException e) {
        }
    }

    public synchronized void addMimeTypes(String str) {
        if (this.DB[0] == null) {
            this.DB[0] = new MimeTypeFile();
        }
        this.DB[0].appendToRegistry(str);
    }

    public String getContentType(File file) {
        return getContentType(file.getName());
    }

    public synchronized String getContentType(String str) {
        String str2;
        int lastIndexOf = str.lastIndexOf(".");
        if (lastIndexOf < 0) {
            str2 = defaultType;
        } else {
            String substring = str.substring(lastIndexOf + 1);
            if (substring.length() == 0) {
                str2 = defaultType;
            } else {
                int i = 0;
                while (true) {
                    if (i >= this.DB.length) {
                        str2 = defaultType;
                        break;
                    } else if (this.DB[i] != null && (str2 = this.DB[i].getMIMETypeString(substring)) != null) {
                        break;
                    } else {
                        i++;
                    }
                }
            }
        }
        return str2;
    }
}
