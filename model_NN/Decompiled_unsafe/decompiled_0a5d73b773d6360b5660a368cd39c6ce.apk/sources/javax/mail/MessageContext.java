package javax.mail;

public class MessageContext {
    private Part part;

    public MessageContext(Part part2) {
        this.part = part2;
    }

    public Part getPart() {
        return this.part;
    }

    public Message getMessage() {
        try {
            return getMessage(this.part);
        } catch (MessagingException e) {
            return null;
        }
    }

    private static Message getMessage(Part part2) throws MessagingException {
        Part part3 = part2;
        while (part3 != null) {
            if (part3 instanceof Message) {
                return (Message) part3;
            }
            Multipart parent = ((BodyPart) part3).getParent();
            if (parent == null) {
                return null;
            }
            part3 = parent.getParent();
        }
        return null;
    }

    public Session getSession() {
        Message message = getMessage();
        if (message != null) {
            return message.session;
        }
        return null;
    }
}
