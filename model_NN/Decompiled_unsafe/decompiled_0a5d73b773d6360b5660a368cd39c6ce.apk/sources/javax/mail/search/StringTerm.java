package javax.mail.search;

public abstract class StringTerm extends SearchTerm {
    private static final long serialVersionUID = 1274042129007696269L;
    protected boolean ignoreCase;
    protected String pattern;

    protected StringTerm(String str) {
        this.pattern = str;
        this.ignoreCase = true;
    }

    protected StringTerm(String str, boolean z) {
        this.pattern = str;
        this.ignoreCase = z;
    }

    public String getPattern() {
        return this.pattern;
    }

    public boolean getIgnoreCase() {
        return this.ignoreCase;
    }

    /* access modifiers changed from: protected */
    public boolean match(String str) {
        int length = str.length() - this.pattern.length();
        for (int i = 0; i <= length; i++) {
            if (str.regionMatches(this.ignoreCase, i, this.pattern, 0, this.pattern.length())) {
                return true;
            }
        }
        return false;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof StringTerm)) {
            return false;
        }
        StringTerm stringTerm = (StringTerm) obj;
        if (this.ignoreCase) {
            if (!stringTerm.pattern.equalsIgnoreCase(this.pattern) || stringTerm.ignoreCase != this.ignoreCase) {
                return false;
            }
            return true;
        } else if (!stringTerm.pattern.equals(this.pattern) || stringTerm.ignoreCase != this.ignoreCase) {
            return false;
        } else {
            return true;
        }
    }

    public int hashCode() {
        return this.ignoreCase ? this.pattern.hashCode() : this.pattern.hashCode() ^ -1;
    }
}
