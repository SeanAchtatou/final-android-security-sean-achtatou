package javax.mail;

public class MessagingException extends Exception {
    private static final long serialVersionUID = -7569192289819959253L;
    private Exception next;

    public MessagingException() {
        initCause(null);
    }

    public MessagingException(String str) {
        super(str);
        initCause(null);
    }

    public MessagingException(String str, Exception exc) {
        super(str);
        this.next = exc;
        initCause(null);
    }

    public synchronized Exception getNextException() {
        return this.next;
    }

    public synchronized Throwable getCause() {
        return this.next;
    }

    public synchronized boolean setNextException(Exception exc) {
        boolean z;
        Exception exc2 = this;
        while ((exc2 instanceof MessagingException) && ((MessagingException) exc2).next != null) {
            exc2 = ((MessagingException) exc2).next;
        }
        if (exc2 instanceof MessagingException) {
            ((MessagingException) exc2).next = exc;
            z = true;
        } else {
            z = false;
        }
        return z;
    }

    public synchronized String toString() {
        String exc;
        exc = super.toString();
        Exception exc2 = this.next;
        if (exc2 != null) {
            if (exc == null) {
                exc = "";
            }
            StringBuffer stringBuffer = new StringBuffer(exc);
            Exception exc3 = exc2;
            while (exc3 != null) {
                stringBuffer.append(";\n  nested exception is:\n\t");
                if (exc3 instanceof MessagingException) {
                    MessagingException messagingException = (MessagingException) exc3;
                    stringBuffer.append(messagingException.superToString());
                    exc3 = messagingException.next;
                } else {
                    stringBuffer.append(exc3.toString());
                    exc3 = null;
                }
            }
            exc = stringBuffer.toString();
        }
        return exc;
    }

    private final String superToString() {
        return super.toString();
    }
}
