package javax.mail;

import java.util.Vector;
import javax.mail.event.ConnectionEvent;
import javax.mail.event.ConnectionListener;
import javax.mail.event.MailEvent;

public abstract class Service {
    private boolean connected = false;
    private Vector connectionListeners = null;
    protected boolean debug = false;
    private EventQueue q;
    private Object qLock = new Object();
    protected Session session;
    protected URLName url = null;

    protected Service(Session session2, URLName uRLName) {
        this.session = session2;
        this.url = uRLName;
        this.debug = session2.getDebug();
    }

    public void connect() throws MessagingException {
        connect(null, null, null);
    }

    public void connect(String str, String str2, String str3) throws MessagingException {
        connect(str, -1, str2, str3);
    }

    public void connect(String str, String str2) throws MessagingException {
        connect(null, str, str2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:51:0x00d4 A[SYNTHETIC, Splitter:B:51:0x00d4] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00f2 A[Catch:{ SecurityException -> 0x0111 }] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0142 A[Catch:{ SecurityException -> 0x0111 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void connect(java.lang.String r16, int r17, java.lang.String r18, java.lang.String r19) throws javax.mail.MessagingException {
        /*
            r15 = this;
            monitor-enter(r15)
            boolean r1 = r15.isConnected()     // Catch:{ all -> 0x000f }
            if (r1 == 0) goto L_0x0012
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException     // Catch:{ all -> 0x000f }
            java.lang.String r2 = "already connected"
            r1.<init>(r2)     // Catch:{ all -> 0x000f }
            throw r1     // Catch:{ all -> 0x000f }
        L_0x000f:
            r1 = move-exception
            monitor-exit(r15)
            throw r1
        L_0x0012:
            r13 = 0
            r8 = 0
            r2 = 0
            r5 = 0
            javax.mail.URLName r1 = r15.url     // Catch:{ all -> 0x000f }
            if (r1 == 0) goto L_0x017b
            javax.mail.URLName r1 = r15.url     // Catch:{ all -> 0x000f }
            java.lang.String r2 = r1.getProtocol()     // Catch:{ all -> 0x000f }
            if (r16 != 0) goto L_0x0177
            javax.mail.URLName r1 = r15.url     // Catch:{ all -> 0x000f }
            java.lang.String r3 = r1.getHost()     // Catch:{ all -> 0x000f }
        L_0x0028:
            r1 = -1
            r0 = r17
            if (r0 != r1) goto L_0x0173
            javax.mail.URLName r1 = r15.url     // Catch:{ all -> 0x000f }
            int r4 = r1.getPort()     // Catch:{ all -> 0x000f }
        L_0x0033:
            if (r18 != 0) goto L_0x00f5
            javax.mail.URLName r1 = r15.url     // Catch:{ all -> 0x000f }
            java.lang.String r18 = r1.getUsername()     // Catch:{ all -> 0x000f }
            if (r19 != 0) goto L_0x016d
            javax.mail.URLName r1 = r15.url     // Catch:{ all -> 0x000f }
            java.lang.String r19 = r1.getPassword()     // Catch:{ all -> 0x000f }
            r9 = r19
            r6 = r18
        L_0x0047:
            javax.mail.URLName r1 = r15.url     // Catch:{ all -> 0x000f }
            java.lang.String r5 = r1.getFile()     // Catch:{ all -> 0x000f }
        L_0x004d:
            if (r2 == 0) goto L_0x0089
            if (r3 != 0) goto L_0x006c
            javax.mail.Session r1 = r15.session     // Catch:{ all -> 0x000f }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x000f }
            java.lang.String r7 = "mail."
            r3.<init>(r7)     // Catch:{ all -> 0x000f }
            java.lang.StringBuilder r3 = r3.append(r2)     // Catch:{ all -> 0x000f }
            java.lang.String r7 = ".host"
            java.lang.StringBuilder r3 = r3.append(r7)     // Catch:{ all -> 0x000f }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x000f }
            java.lang.String r3 = r1.getProperty(r3)     // Catch:{ all -> 0x000f }
        L_0x006c:
            if (r6 != 0) goto L_0x0089
            javax.mail.Session r1 = r15.session     // Catch:{ all -> 0x000f }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x000f }
            java.lang.String r7 = "mail."
            r6.<init>(r7)     // Catch:{ all -> 0x000f }
            java.lang.StringBuilder r6 = r6.append(r2)     // Catch:{ all -> 0x000f }
            java.lang.String r7 = ".user"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x000f }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x000f }
            java.lang.String r6 = r1.getProperty(r6)     // Catch:{ all -> 0x000f }
        L_0x0089:
            if (r3 != 0) goto L_0x0093
            javax.mail.Session r1 = r15.session     // Catch:{ all -> 0x000f }
            java.lang.String r3 = "mail.host"
            java.lang.String r3 = r1.getProperty(r3)     // Catch:{ all -> 0x000f }
        L_0x0093:
            if (r6 != 0) goto L_0x009d
            javax.mail.Session r1 = r15.session     // Catch:{ all -> 0x000f }
            java.lang.String r6 = "mail.user"
            java.lang.String r6 = r1.getProperty(r6)     // Catch:{ all -> 0x000f }
        L_0x009d:
            if (r6 != 0) goto L_0x00a5
            java.lang.String r1 = "user.name"
            java.lang.String r6 = java.lang.System.getProperty(r1)     // Catch:{ SecurityException -> 0x0111 }
        L_0x00a5:
            if (r9 != 0) goto L_0x0168
            javax.mail.URLName r1 = r15.url     // Catch:{ all -> 0x000f }
            if (r1 == 0) goto L_0x0168
            javax.mail.URLName r1 = new javax.mail.URLName     // Catch:{ all -> 0x000f }
            r7 = 0
            r1.<init>(r2, r3, r4, r5, r6, r7)     // Catch:{ all -> 0x000f }
            r15.setURLName(r1)     // Catch:{ all -> 0x000f }
            javax.mail.Session r1 = r15.session     // Catch:{ all -> 0x000f }
            javax.mail.URLName r7 = r15.getURLName()     // Catch:{ all -> 0x000f }
            javax.mail.PasswordAuthentication r1 = r1.getPasswordAuthentication(r7)     // Catch:{ all -> 0x000f }
            if (r1 == 0) goto L_0x0131
            if (r6 != 0) goto L_0x0120
            java.lang.String r6 = r1.getUserName()     // Catch:{ all -> 0x000f }
            java.lang.String r1 = r1.getPassword()     // Catch:{ all -> 0x000f }
            r12 = r8
            r11 = r6
        L_0x00cc:
            r6 = 0
            boolean r13 = r15.protocolConnect(r3, r4, r11, r1)     // Catch:{ AuthenticationFailedException -> 0x0136 }
            r14 = r6
        L_0x00d2:
            if (r13 != 0) goto L_0x0164
            java.net.InetAddress r7 = java.net.InetAddress.getByName(r3)     // Catch:{ UnknownHostException -> 0x0139 }
        L_0x00d8:
            javax.mail.Session r6 = r15.session     // Catch:{ all -> 0x000f }
            r10 = 0
            r8 = r4
            r9 = r2
            javax.mail.PasswordAuthentication r6 = r6.requestPasswordAuthentication(r7, r8, r9, r10, r11)     // Catch:{ all -> 0x000f }
            if (r6 == 0) goto L_0x0164
            java.lang.String r11 = r6.getUserName()     // Catch:{ all -> 0x000f }
            java.lang.String r7 = r6.getPassword()     // Catch:{ all -> 0x000f }
            boolean r1 = r15.protocolConnect(r3, r4, r11, r7)     // Catch:{ all -> 0x000f }
            r6 = r11
        L_0x00f0:
            if (r1 != 0) goto L_0x0142
            if (r14 == 0) goto L_0x013c
            throw r14     // Catch:{ all -> 0x000f }
        L_0x00f5:
            if (r19 != 0) goto L_0x016d
            javax.mail.URLName r1 = r15.url     // Catch:{ all -> 0x000f }
            java.lang.String r1 = r1.getUsername()     // Catch:{ all -> 0x000f }
            r0 = r18
            boolean r1 = r0.equals(r1)     // Catch:{ all -> 0x000f }
            if (r1 == 0) goto L_0x016d
            javax.mail.URLName r1 = r15.url     // Catch:{ all -> 0x000f }
            java.lang.String r19 = r1.getPassword()     // Catch:{ all -> 0x000f }
            r9 = r19
            r6 = r18
            goto L_0x0047
        L_0x0111:
            r1 = move-exception
            boolean r7 = r15.debug     // Catch:{ all -> 0x000f }
            if (r7 == 0) goto L_0x00a5
            javax.mail.Session r7 = r15.session     // Catch:{ all -> 0x000f }
            java.io.PrintStream r7 = r7.getDebugOut()     // Catch:{ all -> 0x000f }
            r1.printStackTrace(r7)     // Catch:{ all -> 0x000f }
            goto L_0x00a5
        L_0x0120:
            java.lang.String r7 = r1.getUserName()     // Catch:{ all -> 0x000f }
            boolean r7 = r6.equals(r7)     // Catch:{ all -> 0x000f }
            if (r7 == 0) goto L_0x0168
            java.lang.String r1 = r1.getPassword()     // Catch:{ all -> 0x000f }
            r12 = r8
            r11 = r6
            goto L_0x00cc
        L_0x0131:
            r1 = 1
            r12 = r1
            r11 = r6
            r1 = r9
            goto L_0x00cc
        L_0x0136:
            r6 = move-exception
            r14 = r6
            goto L_0x00d2
        L_0x0139:
            r6 = move-exception
            r7 = 0
            goto L_0x00d8
        L_0x013c:
            javax.mail.AuthenticationFailedException r1 = new javax.mail.AuthenticationFailedException     // Catch:{ all -> 0x000f }
            r1.<init>()     // Catch:{ all -> 0x000f }
            throw r1     // Catch:{ all -> 0x000f }
        L_0x0142:
            javax.mail.URLName r1 = new javax.mail.URLName     // Catch:{ all -> 0x000f }
            r1.<init>(r2, r3, r4, r5, r6, r7)     // Catch:{ all -> 0x000f }
            r15.setURLName(r1)     // Catch:{ all -> 0x000f }
            if (r12 == 0) goto L_0x015a
            javax.mail.Session r1 = r15.session     // Catch:{ all -> 0x000f }
            javax.mail.URLName r2 = r15.getURLName()     // Catch:{ all -> 0x000f }
            javax.mail.PasswordAuthentication r3 = new javax.mail.PasswordAuthentication     // Catch:{ all -> 0x000f }
            r3.<init>(r6, r7)     // Catch:{ all -> 0x000f }
            r1.setPasswordAuthentication(r2, r3)     // Catch:{ all -> 0x000f }
        L_0x015a:
            r1 = 1
            r15.setConnected(r1)     // Catch:{ all -> 0x000f }
            r1 = 1
            r15.notifyConnectionListeners(r1)     // Catch:{ all -> 0x000f }
            monitor-exit(r15)
            return
        L_0x0164:
            r7 = r1
            r6 = r11
            r1 = r13
            goto L_0x00f0
        L_0x0168:
            r12 = r8
            r1 = r9
            r11 = r6
            goto L_0x00cc
        L_0x016d:
            r9 = r19
            r6 = r18
            goto L_0x0047
        L_0x0173:
            r4 = r17
            goto L_0x0033
        L_0x0177:
            r3 = r16
            goto L_0x0028
        L_0x017b:
            r9 = r19
            r6 = r18
            r4 = r17
            r3 = r16
            goto L_0x004d
        */
        throw new UnsupportedOperationException("Method not decompiled: javax.mail.Service.connect(java.lang.String, int, java.lang.String, java.lang.String):void");
    }

    /* access modifiers changed from: protected */
    public boolean protocolConnect(String str, int i, String str2, String str3) throws MessagingException {
        return false;
    }

    public synchronized boolean isConnected() {
        return this.connected;
    }

    /* access modifiers changed from: protected */
    public synchronized void setConnected(boolean z) {
        this.connected = z;
    }

    public synchronized void close() throws MessagingException {
        setConnected(false);
        notifyConnectionListeners(3);
    }

    public synchronized URLName getURLName() {
        URLName uRLName;
        if (this.url == null || (this.url.getPassword() == null && this.url.getFile() == null)) {
            uRLName = this.url;
        } else {
            uRLName = new URLName(this.url.getProtocol(), this.url.getHost(), this.url.getPort(), null, this.url.getUsername(), null);
        }
        return uRLName;
    }

    /* access modifiers changed from: protected */
    public synchronized void setURLName(URLName uRLName) {
        this.url = uRLName;
    }

    public synchronized void addConnectionListener(ConnectionListener connectionListener) {
        if (this.connectionListeners == null) {
            this.connectionListeners = new Vector();
        }
        this.connectionListeners.addElement(connectionListener);
    }

    public synchronized void removeConnectionListener(ConnectionListener connectionListener) {
        if (this.connectionListeners != null) {
            this.connectionListeners.removeElement(connectionListener);
        }
    }

    /* access modifiers changed from: protected */
    public synchronized void notifyConnectionListeners(int i) {
        if (this.connectionListeners != null) {
            queueEvent(new ConnectionEvent(this, i), this.connectionListeners);
        }
        if (i == 3) {
            terminateQueue();
        }
    }

    public String toString() {
        URLName uRLName = getURLName();
        if (uRLName != null) {
            return uRLName.toString();
        }
        return super.toString();
    }

    /* access modifiers changed from: protected */
    public void queueEvent(MailEvent mailEvent, Vector vector) {
        synchronized (this.qLock) {
            if (this.q == null) {
                this.q = new EventQueue();
            }
        }
        this.q.enqueue(mailEvent, (Vector) vector.clone());
    }

    static class TerminatorEvent extends MailEvent {
        private static final long serialVersionUID = 5542172141759168416L;

        TerminatorEvent() {
            super(new Object());
        }

        public void dispatch(Object obj) {
            Thread.currentThread().interrupt();
        }
    }

    private void terminateQueue() {
        synchronized (this.qLock) {
            if (this.q != null) {
                Vector vector = new Vector();
                vector.setSize(1);
                this.q.enqueue(new TerminatorEvent(), vector);
                this.q = null;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        super.finalize();
        terminateQueue();
    }
}
