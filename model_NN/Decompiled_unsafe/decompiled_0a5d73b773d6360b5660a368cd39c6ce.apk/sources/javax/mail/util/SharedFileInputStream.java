package javax.mail.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import javax.mail.internet.SharedInputStream;

public class SharedFileInputStream extends BufferedInputStream implements SharedInputStream {
    private static int defaultBufferSize = 2048;
    protected long bufpos;
    protected int bufsize;
    protected long datalen;
    protected RandomAccessFile in;
    private boolean master;
    private SharedFile sf;
    protected long start;

    static class SharedFile {
        private int cnt;
        private RandomAccessFile in;

        SharedFile(String str) throws IOException {
            this.in = new RandomAccessFile(str, "r");
        }

        SharedFile(File file) throws IOException {
            this.in = new RandomAccessFile(file, "r");
        }

        public RandomAccessFile open() {
            this.cnt++;
            return this.in;
        }

        public synchronized void close() throws IOException {
            if (this.cnt > 0) {
                int i = this.cnt - 1;
                this.cnt = i;
                if (i <= 0) {
                    this.in.close();
                }
            }
        }

        public synchronized void forceClose() throws IOException {
            if (this.cnt > 0) {
                this.cnt = 0;
                this.in.close();
            } else {
                try {
                    this.in.close();
                } catch (IOException e) {
                }
            }
        }

        /* access modifiers changed from: protected */
        public void finalize() throws Throwable {
            super.finalize();
            this.in.close();
        }
    }

    private void ensureOpen() throws IOException {
        if (this.in == null) {
            throw new IOException("Stream closed");
        }
    }

    public SharedFileInputStream(File file) throws IOException {
        this(file, defaultBufferSize);
    }

    public SharedFileInputStream(String str) throws IOException {
        this(str, defaultBufferSize);
    }

    public SharedFileInputStream(File file, int i) throws IOException {
        super(null);
        this.start = 0;
        this.master = true;
        if (i <= 0) {
            throw new IllegalArgumentException("Buffer size <= 0");
        }
        init(new SharedFile(file), i);
    }

    public SharedFileInputStream(String str, int i) throws IOException {
        super(null);
        this.start = 0;
        this.master = true;
        if (i <= 0) {
            throw new IllegalArgumentException("Buffer size <= 0");
        }
        init(new SharedFile(str), i);
    }

    private void init(SharedFile sharedFile, int i) throws IOException {
        this.sf = sharedFile;
        this.in = sharedFile.open();
        this.start = 0;
        this.datalen = this.in.length();
        this.bufsize = i;
        this.buf = new byte[i];
    }

    private SharedFileInputStream(SharedFile sharedFile, long j, long j2, int i) {
        super(null);
        this.start = 0;
        this.master = true;
        this.master = false;
        this.sf = sharedFile;
        this.in = sharedFile.open();
        this.start = j;
        this.bufpos = j;
        this.datalen = j2;
        this.bufsize = i;
        this.buf = new byte[i];
    }

    private void fill() throws IOException {
        if (this.markpos < 0) {
            this.pos = 0;
            this.bufpos += (long) this.count;
        } else if (this.pos >= this.buf.length) {
            if (this.markpos > 0) {
                int i = this.pos - this.markpos;
                System.arraycopy(this.buf, this.markpos, this.buf, 0, i);
                this.pos = i;
                this.bufpos += (long) this.markpos;
                this.markpos = 0;
            } else if (this.buf.length >= this.marklimit) {
                this.markpos = -1;
                this.pos = 0;
                this.bufpos += (long) this.count;
            } else {
                int i2 = this.pos * 2;
                if (i2 > this.marklimit) {
                    i2 = this.marklimit;
                }
                byte[] bArr = new byte[i2];
                System.arraycopy(this.buf, 0, bArr, 0, this.pos);
                this.buf = bArr;
            }
        }
        this.count = this.pos;
        this.in.seek(this.bufpos + ((long) this.pos));
        int length = this.buf.length - this.pos;
        if ((this.bufpos - this.start) + ((long) this.pos) + ((long) length) > this.datalen) {
            length = (int) (this.datalen - ((this.bufpos - this.start) + ((long) this.pos)));
        }
        int read = this.in.read(this.buf, this.pos, length);
        if (read > 0) {
            this.count = read + this.pos;
        }
    }

    public synchronized int read() throws IOException {
        byte b;
        ensureOpen();
        if (this.pos >= this.count) {
            fill();
            if (this.pos >= this.count) {
                b = -1;
            }
        }
        byte[] bArr = this.buf;
        int i = this.pos;
        this.pos = i + 1;
        b = bArr[i] & 255;
        return b;
    }

    private int read1(byte[] bArr, int i, int i2) throws IOException {
        int i3 = this.count - this.pos;
        if (i3 <= 0) {
            fill();
            i3 = this.count - this.pos;
            if (i3 <= 0) {
                return -1;
            }
        }
        if (i3 < i2) {
            i2 = i3;
        }
        System.arraycopy(this.buf, this.pos, bArr, i, i2);
        this.pos += i2;
        return i2;
    }

    public synchronized int read(byte[] bArr, int i, int i2) throws IOException {
        int read1;
        ensureOpen();
        if ((i | i2 | (i + i2) | (bArr.length - (i + i2))) < 0) {
            throw new IndexOutOfBoundsException();
        } else if (i2 == 0) {
            read1 = 0;
        } else {
            read1 = read1(bArr, i, i2);
            if (read1 > 0) {
                while (read1 < i2) {
                    int read12 = read1(bArr, i + read1, i2 - read1);
                    if (read12 <= 0) {
                        break;
                    }
                    read1 += read12;
                }
            }
        }
        return read1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0021, code lost:
        if (r2 > 0) goto L_0x0023;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized long skip(long r6) throws java.io.IOException {
        /*
            r5 = this;
            r0 = 0
            monitor-enter(r5)
            r5.ensureOpen()     // Catch:{ all -> 0x0030 }
            int r2 = (r6 > r0 ? 1 : (r6 == r0 ? 0 : -1))
            if (r2 > 0) goto L_0x000c
        L_0x000a:
            monitor-exit(r5)
            return r0
        L_0x000c:
            int r2 = r5.count     // Catch:{ all -> 0x0030 }
            int r3 = r5.pos     // Catch:{ all -> 0x0030 }
            int r2 = r2 - r3
            long r2 = (long) r2     // Catch:{ all -> 0x0030 }
            int r4 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r4 > 0) goto L_0x0023
            r5.fill()     // Catch:{ all -> 0x0030 }
            int r2 = r5.count     // Catch:{ all -> 0x0030 }
            int r3 = r5.pos     // Catch:{ all -> 0x0030 }
            int r2 = r2 - r3
            long r2 = (long) r2     // Catch:{ all -> 0x0030 }
            int r4 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r4 <= 0) goto L_0x000a
        L_0x0023:
            r0 = r2
            int r2 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r2 >= 0) goto L_0x0033
        L_0x0028:
            int r2 = r5.pos     // Catch:{ all -> 0x0030 }
            long r2 = (long) r2     // Catch:{ all -> 0x0030 }
            long r2 = r2 + r0
            int r2 = (int) r2     // Catch:{ all -> 0x0030 }
            r5.pos = r2     // Catch:{ all -> 0x0030 }
            goto L_0x000a
        L_0x0030:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        L_0x0033:
            r0 = r6
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: javax.mail.util.SharedFileInputStream.skip(long):long");
    }

    public synchronized int available() throws IOException {
        ensureOpen();
        return (this.count - this.pos) + in_available();
    }

    private int in_available() throws IOException {
        return (int) ((this.start + this.datalen) - (this.bufpos + ((long) this.count)));
    }

    public synchronized void mark(int i) {
        this.marklimit = i;
        this.markpos = this.pos;
    }

    public synchronized void reset() throws IOException {
        ensureOpen();
        if (this.markpos < 0) {
            throw new IOException("Resetting to invalid mark");
        }
        this.pos = this.markpos;
    }

    public boolean markSupported() {
        return true;
    }

    public void close() throws IOException {
        if (this.in != null) {
            try {
                if (this.master) {
                    this.sf.forceClose();
                } else {
                    this.sf.close();
                }
            } finally {
                this.sf = null;
                this.in = null;
                this.buf = null;
            }
        }
    }

    public long getPosition() {
        if (this.in != null) {
            return (this.bufpos + ((long) this.pos)) - this.start;
        }
        throw new RuntimeException("Stream closed");
    }

    public InputStream newStream(long j, long j2) {
        if (this.in == null) {
            throw new RuntimeException("Stream closed");
        } else if (j < 0) {
            throw new IllegalArgumentException("start < 0");
        } else {
            if (j2 == -1) {
                j2 = this.datalen;
            }
            return new SharedFileInputStream(this.sf, this.start + ((long) ((int) j)), (long) ((int) (j2 - j)), this.bufsize);
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        super.finalize();
        close();
    }
}
