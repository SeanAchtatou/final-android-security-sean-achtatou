package javax.mail;

import com.sun.mail.util.LineInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.URL;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.mail.Provider;

public final class Session {
    private static Session defaultSession = null;
    /* access modifiers changed from: private */
    public final Properties addressMap = new Properties();
    private final Hashtable authTable = new Hashtable();
    private final Authenticator authenticator;
    private boolean debug = false;
    private PrintStream out;
    private final Properties props;
    private final Vector providers = new Vector();
    private final Hashtable providersByClassName = new Hashtable();
    private final Hashtable providersByProtocol = new Hashtable();

    private Session(Properties properties, Authenticator authenticator2) {
        Class<?> cls;
        this.props = properties;
        this.authenticator = authenticator2;
        if (Boolean.valueOf(properties.getProperty("mail.debug")).booleanValue()) {
            this.debug = true;
        }
        if (this.debug) {
            pr("DEBUG: JavaMail version 1.4.1");
        }
        if (authenticator2 != null) {
            cls = authenticator2.getClass();
        } else {
            cls = getClass();
        }
        loadProviders(cls);
        loadAddressMap(cls);
    }

    public static Session getInstance(Properties properties, Authenticator authenticator2) {
        return new Session(properties, authenticator2);
    }

    public static Session getInstance(Properties properties) {
        return new Session(properties, null);
    }

    public static synchronized Session getDefaultInstance(Properties properties, Authenticator authenticator2) {
        Session session;
        synchronized (Session.class) {
            if (defaultSession == null) {
                defaultSession = new Session(properties, authenticator2);
            } else if (defaultSession.authenticator != authenticator2 && (defaultSession.authenticator == null || authenticator2 == null || defaultSession.authenticator.getClass().getClassLoader() != authenticator2.getClass().getClassLoader())) {
                throw new SecurityException("Access to default session denied");
            }
            session = defaultSession;
        }
        return session;
    }

    public static Session getDefaultInstance(Properties properties) {
        return getDefaultInstance(properties, null);
    }

    public synchronized void setDebug(boolean z) {
        this.debug = z;
        if (z) {
            pr("DEBUG: setDebug: JavaMail version 1.4.1");
        }
    }

    public synchronized boolean getDebug() {
        return this.debug;
    }

    public synchronized void setDebugOut(PrintStream printStream) {
        this.out = printStream;
    }

    public synchronized PrintStream getDebugOut() {
        PrintStream printStream;
        if (this.out == null) {
            printStream = System.out;
        } else {
            printStream = this.out;
        }
        return printStream;
    }

    public synchronized Provider[] getProviders() {
        Provider[] providerArr;
        providerArr = new Provider[this.providers.size()];
        this.providers.copyInto(providerArr);
        return providerArr;
    }

    public synchronized Provider getProvider(String str) throws NoSuchProviderException {
        Provider provider;
        if (str != null) {
            if (str.length() > 0) {
                provider = null;
                String property = this.props.getProperty("mail." + str + ".class");
                if (property != null) {
                    if (this.debug) {
                        pr("DEBUG: mail." + str + ".class property exists and points to " + property);
                    }
                    provider = (Provider) this.providersByClassName.get(property);
                }
                if (provider == null) {
                    provider = (Provider) this.providersByProtocol.get(str);
                    if (provider == null) {
                        throw new NoSuchProviderException("No provider for " + str);
                    } else if (this.debug) {
                        pr("DEBUG: getProvider() returning " + provider.toString());
                    }
                }
            }
        }
        throw new NoSuchProviderException("Invalid protocol: null");
        return provider;
    }

    public synchronized void setProvider(Provider provider) throws NoSuchProviderException {
        if (provider == null) {
            throw new NoSuchProviderException("Can't set null provider");
        }
        this.providersByProtocol.put(provider.getProtocol(), provider);
        this.props.put("mail." + provider.getProtocol() + ".class", provider.getClassName());
    }

    public Store getStore() throws NoSuchProviderException {
        return getStore(getProperty("mail.store.protocol"));
    }

    public Store getStore(String str) throws NoSuchProviderException {
        return getStore(new URLName(str, null, -1, null, null, null));
    }

    public Store getStore(URLName uRLName) throws NoSuchProviderException {
        return getStore(getProvider(uRLName.getProtocol()), uRLName);
    }

    public Store getStore(Provider provider) throws NoSuchProviderException {
        return getStore(provider, null);
    }

    private Store getStore(Provider provider, URLName uRLName) throws NoSuchProviderException {
        if (provider == null || provider.getType() != Provider.Type.STORE) {
            throw new NoSuchProviderException("invalid provider");
        }
        try {
            return (Store) getService(provider, uRLName);
        } catch (ClassCastException e) {
            throw new NoSuchProviderException("incorrect class");
        }
    }

    public Folder getFolder(URLName uRLName) throws MessagingException {
        Store store = getStore(uRLName);
        store.connect();
        return store.getFolder(uRLName);
    }

    public Transport getTransport() throws NoSuchProviderException {
        return getTransport(getProperty("mail.transport.protocol"));
    }

    public Transport getTransport(String str) throws NoSuchProviderException {
        return getTransport(new URLName(str, null, -1, null, null, null));
    }

    public Transport getTransport(URLName uRLName) throws NoSuchProviderException {
        return getTransport(getProvider(uRLName.getProtocol()), uRLName);
    }

    public Transport getTransport(Provider provider) throws NoSuchProviderException {
        return getTransport(provider, null);
    }

    public Transport getTransport(Address address) throws NoSuchProviderException {
        String str = (String) this.addressMap.get(address.getType());
        if (str != null) {
            return getTransport(str);
        }
        throw new NoSuchProviderException("No provider for Address type: " + address.getType());
    }

    private Transport getTransport(Provider provider, URLName uRLName) throws NoSuchProviderException {
        if (provider == null || provider.getType() != Provider.Type.TRANSPORT) {
            throw new NoSuchProviderException("invalid provider");
        }
        try {
            return (Transport) getService(provider, uRLName);
        } catch (ClassCastException e) {
            throw new NoSuchProviderException("incorrect class");
        }
    }

    private Object getService(Provider provider, URLName uRLName) throws NoSuchProviderException {
        URLName uRLName2;
        ClassLoader classLoader;
        Class<?> cls;
        Class<?> cls2 = null;
        if (provider == null) {
            throw new NoSuchProviderException("null");
        }
        if (uRLName == null) {
            uRLName2 = new URLName(provider.getProtocol(), null, -1, null, null, null);
        } else {
            uRLName2 = uRLName;
        }
        if (this.authenticator != null) {
            classLoader = this.authenticator.getClass().getClassLoader();
        } else {
            classLoader = getClass().getClassLoader();
        }
        try {
            ClassLoader contextClassLoader = getContextClassLoader();
            if (contextClassLoader != null) {
                try {
                    cls2 = contextClassLoader.loadClass(provider.getClassName());
                } catch (ClassNotFoundException e) {
                }
            }
            if (cls2 == null) {
                cls = classLoader.loadClass(provider.getClassName());
            } else {
                cls = cls2;
            }
        } catch (Exception e2) {
            try {
                cls = Class.forName(provider.getClassName());
            } catch (Exception e3) {
                if (this.debug) {
                    e3.printStackTrace(getDebugOut());
                }
                throw new NoSuchProviderException(provider.getProtocol());
            }
        }
        try {
            return cls.getConstructor(Session.class, URLName.class).newInstance(this, uRLName2);
        } catch (Exception e4) {
            if (this.debug) {
                e4.printStackTrace(getDebugOut());
            }
            throw new NoSuchProviderException(provider.getProtocol());
        }
    }

    public void setPasswordAuthentication(URLName uRLName, PasswordAuthentication passwordAuthentication) {
        if (passwordAuthentication == null) {
            this.authTable.remove(uRLName);
        } else {
            this.authTable.put(uRLName, passwordAuthentication);
        }
    }

    public PasswordAuthentication getPasswordAuthentication(URLName uRLName) {
        return (PasswordAuthentication) this.authTable.get(uRLName);
    }

    public PasswordAuthentication requestPasswordAuthentication(InetAddress inetAddress, int i, String str, String str2, String str3) {
        if (this.authenticator != null) {
            return this.authenticator.requestPasswordAuthentication(inetAddress, i, str, str2, str3);
        }
        return null;
    }

    public Properties getProperties() {
        return this.props;
    }

    public String getProperty(String str) {
        return this.props.getProperty(str);
    }

    private void loadProviders(Class cls) {
        AnonymousClass1 r1 = new StreamLoader() {
            public void load(InputStream inputStream) throws IOException {
                Session.this.loadProvidersFromStream(inputStream);
            }
        };
        try {
            loadFile(String.valueOf(System.getProperty("java.home")) + File.separator + "lib" + File.separator + "javamail.providers", r1);
        } catch (SecurityException e) {
            if (this.debug) {
                pr("DEBUG: can't get java.home: " + e);
            }
        }
        loadAllResources("META-INF/javamail.providers", cls, r1);
        loadResource("/META-INF/javamail.default.providers", cls, r1);
        if (this.providers.size() == 0) {
            if (this.debug) {
                pr("DEBUG: failed to load any providers, using defaults");
            }
            addProvider(new Provider(Provider.Type.STORE, "imap", "com.sun.mail.imap.IMAPStore", "Sun Microsystems, Inc.", Version.version));
            addProvider(new Provider(Provider.Type.STORE, "imaps", "com.sun.mail.imap.IMAPSSLStore", "Sun Microsystems, Inc.", Version.version));
            addProvider(new Provider(Provider.Type.STORE, "pop3", "com.sun.mail.pop3.POP3Store", "Sun Microsystems, Inc.", Version.version));
            addProvider(new Provider(Provider.Type.STORE, "pop3s", "com.sun.mail.pop3.POP3SSLStore", "Sun Microsystems, Inc.", Version.version));
            addProvider(new Provider(Provider.Type.TRANSPORT, "smtp", "com.sun.mail.smtp.SMTPTransport", "Sun Microsystems, Inc.", Version.version));
            addProvider(new Provider(Provider.Type.TRANSPORT, "smtps", "com.sun.mail.smtp.SMTPSSLTransport", "Sun Microsystems, Inc.", Version.version));
        }
        if (this.debug) {
            pr("DEBUG: Tables of loaded providers");
            pr("DEBUG: Providers Listed By Class Name: " + this.providersByClassName.toString());
            pr("DEBUG: Providers Listed By Protocol: " + this.providersByProtocol.toString());
        }
    }

    /* access modifiers changed from: private */
    public void loadProvidersFromStream(InputStream inputStream) throws IOException {
        if (inputStream != null) {
            LineInputStream lineInputStream = new LineInputStream(inputStream);
            while (true) {
                String readLine = lineInputStream.readLine();
                if (readLine != null) {
                    if (!readLine.startsWith("#")) {
                        StringTokenizer stringTokenizer = new StringTokenizer(readLine, ";");
                        String str = null;
                        String str2 = null;
                        String str3 = null;
                        String str4 = null;
                        Provider.Type type = null;
                        while (stringTokenizer.hasMoreTokens()) {
                            String trim = stringTokenizer.nextToken().trim();
                            int indexOf = trim.indexOf("=");
                            if (trim.startsWith("protocol=")) {
                                str4 = trim.substring(indexOf + 1);
                            } else if (trim.startsWith("type=")) {
                                String substring = trim.substring(indexOf + 1);
                                if (substring.equalsIgnoreCase("store")) {
                                    type = Provider.Type.STORE;
                                } else if (substring.equalsIgnoreCase("transport")) {
                                    type = Provider.Type.TRANSPORT;
                                }
                            } else if (trim.startsWith("class=")) {
                                str3 = trim.substring(indexOf + 1);
                            } else if (trim.startsWith("vendor=")) {
                                str2 = trim.substring(indexOf + 1);
                            } else if (trim.startsWith("version=")) {
                                str = trim.substring(indexOf + 1);
                            }
                        }
                        if (type != null && str4 != null && str3 != null && str4.length() > 0 && str3.length() > 0) {
                            addProvider(new Provider(type, str4, str3, str2, str));
                        } else if (this.debug) {
                            pr("DEBUG: Bad provider entry: " + readLine);
                        }
                    }
                } else {
                    return;
                }
            }
        }
    }

    public synchronized void addProvider(Provider provider) {
        this.providers.addElement(provider);
        this.providersByClassName.put(provider.getClassName(), provider);
        if (!this.providersByProtocol.containsKey(provider.getProtocol())) {
            this.providersByProtocol.put(provider.getProtocol(), provider);
        }
    }

    private void loadAddressMap(Class cls) {
        AnonymousClass2 r0 = new StreamLoader() {
            public void load(InputStream inputStream) throws IOException {
                Session.this.addressMap.load(inputStream);
            }
        };
        loadResource("/META-INF/javamail.default.address.map", cls, r0);
        loadAllResources("META-INF/javamail.address.map", cls, r0);
        try {
            loadFile(String.valueOf(System.getProperty("java.home")) + File.separator + "lib" + File.separator + "javamail.address.map", r0);
        } catch (SecurityException e) {
            if (this.debug) {
                pr("DEBUG: can't get java.home: " + e);
            }
        }
        if (this.addressMap.isEmpty()) {
            if (this.debug) {
                pr("DEBUG: failed to load address map, using defaults");
            }
            this.addressMap.put("rfc822", "smtp");
        }
    }

    public synchronized void setProtocolForAddress(String str, String str2) {
        if (str2 == null) {
            this.addressMap.remove(str);
        } else {
            this.addressMap.put(str, str2);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0030 A[Catch:{ all -> 0x009a }] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0056 A[SYNTHETIC, Splitter:B:17:0x0056] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0062 A[Catch:{ all -> 0x009a }] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0088 A[SYNTHETIC, Splitter:B:27:0x0088] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0092 A[SYNTHETIC, Splitter:B:33:0x0092] */
    /* JADX WARNING: Removed duplicated region for block: B:43:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:44:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:12:0x002c=Splitter:B:12:0x002c, B:22:0x005e=Splitter:B:22:0x005e} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void loadFile(java.lang.String r5, javax.mail.StreamLoader r6) {
        /*
            r4 = this;
            r2 = 0
            java.io.BufferedInputStream r1 = new java.io.BufferedInputStream     // Catch:{ IOException -> 0x002a, SecurityException -> 0x005c, all -> 0x008e }
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ IOException -> 0x002a, SecurityException -> 0x005c, all -> 0x008e }
            r0.<init>(r5)     // Catch:{ IOException -> 0x002a, SecurityException -> 0x005c, all -> 0x008e }
            r1.<init>(r0)     // Catch:{ IOException -> 0x002a, SecurityException -> 0x005c, all -> 0x008e }
            r6.load(r1)     // Catch:{ IOException -> 0x009e, SecurityException -> 0x009c }
            boolean r0 = r4.debug     // Catch:{ IOException -> 0x009e, SecurityException -> 0x009c }
            if (r0 == 0) goto L_0x0024
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x009e, SecurityException -> 0x009c }
            java.lang.String r2 = "DEBUG: successfully loaded file: "
            r0.<init>(r2)     // Catch:{ IOException -> 0x009e, SecurityException -> 0x009c }
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ IOException -> 0x009e, SecurityException -> 0x009c }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x009e, SecurityException -> 0x009c }
            r4.pr(r0)     // Catch:{ IOException -> 0x009e, SecurityException -> 0x009c }
        L_0x0024:
            if (r1 == 0) goto L_0x0029
            r1.close()     // Catch:{ IOException -> 0x0098 }
        L_0x0029:
            return
        L_0x002a:
            r0 = move-exception
            r1 = r2
        L_0x002c:
            boolean r2 = r4.debug     // Catch:{ all -> 0x009a }
            if (r2 == 0) goto L_0x0054
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x009a }
            java.lang.String r3 = "DEBUG: not loading file: "
            r2.<init>(r3)     // Catch:{ all -> 0x009a }
            java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ all -> 0x009a }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x009a }
            r4.pr(r2)     // Catch:{ all -> 0x009a }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x009a }
            java.lang.String r3 = "DEBUG: "
            r2.<init>(r3)     // Catch:{ all -> 0x009a }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ all -> 0x009a }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x009a }
            r4.pr(r0)     // Catch:{ all -> 0x009a }
        L_0x0054:
            if (r1 == 0) goto L_0x0029
            r1.close()     // Catch:{ IOException -> 0x005a }
            goto L_0x0029
        L_0x005a:
            r0 = move-exception
            goto L_0x0029
        L_0x005c:
            r0 = move-exception
            r1 = r2
        L_0x005e:
            boolean r2 = r4.debug     // Catch:{ all -> 0x009a }
            if (r2 == 0) goto L_0x0086
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x009a }
            java.lang.String r3 = "DEBUG: not loading file: "
            r2.<init>(r3)     // Catch:{ all -> 0x009a }
            java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ all -> 0x009a }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x009a }
            r4.pr(r2)     // Catch:{ all -> 0x009a }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x009a }
            java.lang.String r3 = "DEBUG: "
            r2.<init>(r3)     // Catch:{ all -> 0x009a }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ all -> 0x009a }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x009a }
            r4.pr(r0)     // Catch:{ all -> 0x009a }
        L_0x0086:
            if (r1 == 0) goto L_0x0029
            r1.close()     // Catch:{ IOException -> 0x008c }
            goto L_0x0029
        L_0x008c:
            r0 = move-exception
            goto L_0x0029
        L_0x008e:
            r0 = move-exception
            r1 = r2
        L_0x0090:
            if (r1 == 0) goto L_0x0095
            r1.close()     // Catch:{ IOException -> 0x0096 }
        L_0x0095:
            throw r0
        L_0x0096:
            r1 = move-exception
            goto L_0x0095
        L_0x0098:
            r0 = move-exception
            goto L_0x0029
        L_0x009a:
            r0 = move-exception
            goto L_0x0090
        L_0x009c:
            r0 = move-exception
            goto L_0x005e
        L_0x009e:
            r0 = move-exception
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: javax.mail.Session.loadFile(java.lang.String, javax.mail.StreamLoader):void");
    }

    private void loadResource(String str, Class cls, StreamLoader streamLoader) {
        InputStream inputStream = null;
        try {
            inputStream = getResourceAsStream(cls, str);
            if (inputStream != null) {
                streamLoader.load(inputStream);
                if (this.debug) {
                    pr("DEBUG: successfully loaded resource: " + str);
                }
            } else if (this.debug) {
                pr("DEBUG: not loading resource: " + str);
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                }
            }
        } catch (IOException e2) {
            if (this.debug) {
                pr("DEBUG: " + e2);
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e3) {
                }
            }
        } catch (SecurityException e4) {
            if (this.debug) {
                pr("DEBUG: " + e4);
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e5) {
                }
            }
        } catch (Throwable th) {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e6) {
                }
            }
            throw th;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0098, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0099, code lost:
        r7 = r0;
        r0 = r2;
        r2 = null;
        r1 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00bb, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00bc, code lost:
        r7 = r0;
        r0 = r2;
        r2 = null;
        r1 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00c4, code lost:
        pr("DEBUG: " + r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x00de, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x00ea, code lost:
        pr("DEBUG: " + r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0103, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0104, code lost:
        r2 = r0;
        r0 = r1;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x001a  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00c4 A[Catch:{ all -> 0x0107 }] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00d8 A[SYNTHETIC, Splitter:B:55:0x00d8] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00de A[ExcHandler: all (th java.lang.Throwable), Splitter:B:21:0x0055] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00e1 A[SYNTHETIC, Splitter:B:60:0x00e1] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x00ea  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0103 A[ExcHandler: Exception (r1v6 'e' java.lang.Exception A[CUSTOM_DECLARE]), PHI: r0 
      PHI: (r0v16 boolean) = (r0v20 boolean), (r0v20 boolean), (r0v23 boolean), (r0v23 boolean), (r0v35 boolean), (r0v35 boolean) binds: [B:55:0x00d8, B:56:?, B:45:0x00b5, B:46:?, B:30:0x0078, B:31:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:30:0x0078] */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x007b A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:90:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void loadAllResources(java.lang.String r9, java.lang.Class r10, javax.mail.StreamLoader r11) {
        /*
            r8 = this;
            r2 = 0
            java.lang.ClassLoader r0 = getContextClassLoader()     // Catch:{ Exception -> 0x00e5 }
            if (r0 != 0) goto L_0x000b
            java.lang.ClassLoader r0 = r10.getClassLoader()     // Catch:{ Exception -> 0x00e5 }
        L_0x000b:
            if (r0 == 0) goto L_0x0036
            java.net.URL[] r0 = getResources(r0, r9)     // Catch:{ Exception -> 0x00e5 }
            r4 = r0
        L_0x0012:
            if (r4 == 0) goto L_0x0018
            r3 = r2
        L_0x0015:
            int r0 = r4.length     // Catch:{ Exception -> 0x00e5 }
            if (r3 < r0) goto L_0x003c
        L_0x0018:
            if (r2 != 0) goto L_0x0035
            boolean r0 = r8.debug
            if (r0 == 0) goto L_0x0023
            java.lang.String r0 = "DEBUG: !anyLoaded"
            r8.pr(r0)
        L_0x0023:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = "/"
            r0.<init>(r1)
            java.lang.StringBuilder r0 = r0.append(r9)
            java.lang.String r0 = r0.toString()
            r8.loadResource(r0, r10, r11)
        L_0x0035:
            return
        L_0x0036:
            java.net.URL[] r0 = getSystemResources(r9)     // Catch:{ Exception -> 0x00e5 }
            r4 = r0
            goto L_0x0012
        L_0x003c:
            r0 = r4[r3]     // Catch:{ Exception -> 0x00e5 }
            r1 = 0
            boolean r5 = r8.debug     // Catch:{ Exception -> 0x00e5 }
            if (r5 == 0) goto L_0x0055
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00e5 }
            java.lang.String r6 = "DEBUG: URL "
            r5.<init>(r6)     // Catch:{ Exception -> 0x00e5 }
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ Exception -> 0x00e5 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x00e5 }
            r8.pr(r5)     // Catch:{ Exception -> 0x00e5 }
        L_0x0055:
            java.io.InputStream r1 = openStream(r0)     // Catch:{ IOException -> 0x0098, SecurityException -> 0x00bb, all -> 0x00de }
            if (r1 == 0) goto L_0x0080
            r11.load(r1)     // Catch:{ IOException -> 0x0113, SecurityException -> 0x010d, all -> 0x00de }
            r2 = 1
            boolean r5 = r8.debug     // Catch:{ IOException -> 0x0113, SecurityException -> 0x010d, all -> 0x00de }
            if (r5 == 0) goto L_0x0119
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0113, SecurityException -> 0x010d, all -> 0x00de }
            java.lang.String r6 = "DEBUG: successfully loaded resource: "
            r5.<init>(r6)     // Catch:{ IOException -> 0x0113, SecurityException -> 0x010d, all -> 0x00de }
            java.lang.StringBuilder r0 = r5.append(r0)     // Catch:{ IOException -> 0x0113, SecurityException -> 0x010d, all -> 0x00de }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x0113, SecurityException -> 0x010d, all -> 0x00de }
            r8.pr(r0)     // Catch:{ IOException -> 0x0113, SecurityException -> 0x010d, all -> 0x00de }
            r0 = r2
        L_0x0076:
            if (r1 == 0) goto L_0x007b
            r1.close()     // Catch:{ IOException -> 0x0100, Exception -> 0x0103 }
        L_0x007b:
            int r2 = r3 + 1
            r3 = r2
            r2 = r0
            goto L_0x0015
        L_0x0080:
            boolean r5 = r8.debug     // Catch:{ IOException -> 0x0113, SecurityException -> 0x010d, all -> 0x00de }
            if (r5 == 0) goto L_0x0119
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0113, SecurityException -> 0x010d, all -> 0x00de }
            java.lang.String r6 = "DEBUG: not loading resource: "
            r5.<init>(r6)     // Catch:{ IOException -> 0x0113, SecurityException -> 0x010d, all -> 0x00de }
            java.lang.StringBuilder r0 = r5.append(r0)     // Catch:{ IOException -> 0x0113, SecurityException -> 0x010d, all -> 0x00de }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x0113, SecurityException -> 0x010d, all -> 0x00de }
            r8.pr(r0)     // Catch:{ IOException -> 0x0113, SecurityException -> 0x010d, all -> 0x00de }
            r0 = r2
            goto L_0x0076
        L_0x0098:
            r0 = move-exception
            r7 = r0
            r0 = r2
            r2 = r1
            r1 = r7
        L_0x009d:
            boolean r5 = r8.debug     // Catch:{ all -> 0x0107 }
            if (r5 == 0) goto L_0x00b3
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0107 }
            java.lang.String r6 = "DEBUG: "
            r5.<init>(r6)     // Catch:{ all -> 0x0107 }
            java.lang.StringBuilder r1 = r5.append(r1)     // Catch:{ all -> 0x0107 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0107 }
            r8.pr(r1)     // Catch:{ all -> 0x0107 }
        L_0x00b3:
            if (r2 == 0) goto L_0x007b
            r2.close()     // Catch:{ IOException -> 0x00b9, Exception -> 0x0103 }
            goto L_0x007b
        L_0x00b9:
            r1 = move-exception
            goto L_0x007b
        L_0x00bb:
            r0 = move-exception
            r7 = r0
            r0 = r2
            r2 = r1
            r1 = r7
        L_0x00c0:
            boolean r5 = r8.debug     // Catch:{ all -> 0x0107 }
            if (r5 == 0) goto L_0x00d6
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0107 }
            java.lang.String r6 = "DEBUG: "
            r5.<init>(r6)     // Catch:{ all -> 0x0107 }
            java.lang.StringBuilder r1 = r5.append(r1)     // Catch:{ all -> 0x0107 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0107 }
            r8.pr(r1)     // Catch:{ all -> 0x0107 }
        L_0x00d6:
            if (r2 == 0) goto L_0x007b
            r2.close()     // Catch:{ IOException -> 0x00dc, Exception -> 0x0103 }
            goto L_0x007b
        L_0x00dc:
            r1 = move-exception
            goto L_0x007b
        L_0x00de:
            r0 = move-exception
        L_0x00df:
            if (r1 == 0) goto L_0x00e4
            r1.close()     // Catch:{ IOException -> 0x00fe }
        L_0x00e4:
            throw r0     // Catch:{ Exception -> 0x00e5 }
        L_0x00e5:
            r0 = move-exception
        L_0x00e6:
            boolean r1 = r8.debug
            if (r1 == 0) goto L_0x0018
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r3 = "DEBUG: "
            r1.<init>(r3)
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r0 = r0.toString()
            r8.pr(r0)
            goto L_0x0018
        L_0x00fe:
            r1 = move-exception
            goto L_0x00e4
        L_0x0100:
            r1 = move-exception
            goto L_0x007b
        L_0x0103:
            r1 = move-exception
            r2 = r0
            r0 = r1
            goto L_0x00e6
        L_0x0107:
            r1 = move-exception
            r7 = r1
            r1 = r2
            r2 = r0
            r0 = r7
            goto L_0x00df
        L_0x010d:
            r0 = move-exception
            r7 = r0
            r0 = r2
            r2 = r1
            r1 = r7
            goto L_0x00c0
        L_0x0113:
            r0 = move-exception
            r7 = r0
            r0 = r2
            r2 = r1
            r1 = r7
            goto L_0x009d
        L_0x0119:
            r0 = r2
            goto L_0x0076
        */
        throw new UnsupportedOperationException("Method not decompiled: javax.mail.Session.loadAllResources(java.lang.String, java.lang.Class, javax.mail.StreamLoader):void");
    }

    private void pr(String str) {
        getDebugOut().println(str);
    }

    private static ClassLoader getContextClassLoader() {
        return (ClassLoader) AccessController.doPrivileged(new PrivilegedAction() {
            public Object run() {
                try {
                    return Thread.currentThread().getContextClassLoader();
                } catch (SecurityException e) {
                    return null;
                }
            }
        });
    }

    private static InputStream getResourceAsStream(final Class cls, final String str) throws IOException {
        try {
            return (InputStream) AccessController.doPrivileged(new PrivilegedExceptionAction() {
                public Object run() throws IOException {
                    return cls.getResourceAsStream(str);
                }
            });
        } catch (PrivilegedActionException e) {
            throw ((IOException) e.getException());
        }
    }

    private static URL[] getResources(final ClassLoader classLoader, final String str) {
        return (URL[]) AccessController.doPrivileged(new PrivilegedAction() {
            public Object run() {
                URL[] urlArr = null;
                try {
                    Vector vector = new Vector();
                    Enumeration<URL> resources = classLoader.getResources(str);
                    while (resources != null && resources.hasMoreElements()) {
                        URL nextElement = resources.nextElement();
                        if (nextElement != null) {
                            vector.addElement(nextElement);
                        }
                    }
                    if (vector.size() <= 0) {
                        return urlArr;
                    }
                    URL[] urlArr2 = new URL[vector.size()];
                    vector.copyInto(urlArr2);
                    return urlArr2;
                } catch (IOException | SecurityException e) {
                    return urlArr;
                }
            }
        });
    }

    private static URL[] getSystemResources(final String str) {
        return (URL[]) AccessController.doPrivileged(new PrivilegedAction() {
            public Object run() {
                URL[] urlArr = null;
                try {
                    Vector vector = new Vector();
                    Enumeration<URL> systemResources = ClassLoader.getSystemResources(str);
                    while (systemResources != null && systemResources.hasMoreElements()) {
                        URL nextElement = systemResources.nextElement();
                        if (nextElement != null) {
                            vector.addElement(nextElement);
                        }
                    }
                    if (vector.size() <= 0) {
                        return urlArr;
                    }
                    URL[] urlArr2 = new URL[vector.size()];
                    vector.copyInto(urlArr2);
                    return urlArr2;
                } catch (IOException | SecurityException e) {
                    return urlArr;
                }
            }
        });
    }

    private static InputStream openStream(final URL url) throws IOException {
        try {
            return (InputStream) AccessController.doPrivileged(new PrivilegedExceptionAction() {
                public Object run() throws IOException {
                    return url.openStream();
                }
            });
        } catch (PrivilegedActionException e) {
            throw ((IOException) e.getException());
        }
    }
}
