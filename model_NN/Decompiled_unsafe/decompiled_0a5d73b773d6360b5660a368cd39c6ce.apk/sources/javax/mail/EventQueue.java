package javax.mail;

import java.util.Vector;
import javax.mail.event.MailEvent;

class EventQueue implements Runnable {
    private QueueElement head = null;
    private Thread qThread = new Thread(this, "JavaMail-EventQueue");
    private QueueElement tail = null;

    static class QueueElement {
        MailEvent event = null;
        QueueElement next = null;
        QueueElement prev = null;
        Vector vector = null;

        QueueElement(MailEvent mailEvent, Vector vector2) {
            this.event = mailEvent;
            this.vector = vector2;
        }
    }

    public EventQueue() {
        this.qThread.setDaemon(true);
        this.qThread.start();
    }

    public synchronized void enqueue(MailEvent mailEvent, Vector vector) {
        QueueElement queueElement = new QueueElement(mailEvent, vector);
        if (this.head == null) {
            this.head = queueElement;
            this.tail = queueElement;
        } else {
            queueElement.next = this.head;
            this.head.prev = queueElement;
            this.head = queueElement;
        }
        notifyAll();
    }

    private synchronized QueueElement dequeue() throws InterruptedException {
        QueueElement queueElement;
        while (this.tail == null) {
            wait();
        }
        queueElement = this.tail;
        this.tail = queueElement.prev;
        if (this.tail == null) {
            this.head = null;
        } else {
            this.tail.next = null;
        }
        queueElement.next = null;
        queueElement.prev = null;
        return queueElement;
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0006 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:3:0x0007 A[Catch:{ InterruptedException -> 0x0024 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r4 = this;
        L_0x0000:
            javax.mail.EventQueue$QueueElement r0 = r4.dequeue()     // Catch:{ InterruptedException -> 0x0024 }
            if (r0 != 0) goto L_0x0007
        L_0x0006:
            return
        L_0x0007:
            javax.mail.event.MailEvent r2 = r0.event     // Catch:{ InterruptedException -> 0x0024 }
            java.util.Vector r3 = r0.vector     // Catch:{ InterruptedException -> 0x0024 }
            r0 = 0
            r1 = r0
        L_0x000d:
            int r0 = r3.size()     // Catch:{ InterruptedException -> 0x0024 }
            if (r1 >= r0) goto L_0x0000
            java.lang.Object r0 = r3.elementAt(r1)     // Catch:{ Throwable -> 0x001e }
            r2.dispatch(r0)     // Catch:{ Throwable -> 0x001e }
        L_0x001a:
            int r0 = r1 + 1
            r1 = r0
            goto L_0x000d
        L_0x001e:
            r0 = move-exception
            boolean r0 = r0 instanceof java.lang.InterruptedException     // Catch:{ InterruptedException -> 0x0024 }
            if (r0 == 0) goto L_0x001a
            goto L_0x0006
        L_0x0024:
            r0 = move-exception
            goto L_0x0006
        */
        throw new UnsupportedOperationException("Method not decompiled: javax.mail.EventQueue.run():void");
    }

    /* access modifiers changed from: package-private */
    public void stop() {
        if (this.qThread != null) {
            this.qThread.interrupt();
            this.qThread = null;
        }
    }
}
