package javax.mail;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import javax.mail.event.TransportEvent;
import javax.mail.event.TransportListener;

public abstract class Transport extends Service {
    private Vector transportListeners = null;

    public abstract void sendMessage(Message message, Address[] addressArr) throws MessagingException;

    public Transport(Session session, URLName uRLName) {
        super(session, uRLName);
    }

    public static void send(Message message) throws MessagingException {
        message.saveChanges();
        send0(message, message.getAllRecipients());
    }

    public static void send(Message message, Address[] addressArr) throws MessagingException {
        message.saveChanges();
        send0(message, addressArr);
    }

    private static void send0(Message message, Address[] addressArr) throws MessagingException {
        Session defaultInstance;
        Address[] addressArr2;
        Address[] addressArr3;
        if (addressArr == null || addressArr.length == 0) {
            throw new SendFailedException("No recipient addresses");
        }
        Hashtable hashtable = new Hashtable();
        Vector vector = new Vector();
        Vector vector2 = new Vector();
        Vector vector3 = new Vector();
        for (int i = 0; i < addressArr.length; i++) {
            if (hashtable.containsKey(addressArr[i].getType())) {
                ((Vector) hashtable.get(addressArr[i].getType())).addElement(addressArr[i]);
            } else {
                Vector vector4 = new Vector();
                vector4.addElement(addressArr[i]);
                hashtable.put(addressArr[i].getType(), vector4);
            }
        }
        int size = hashtable.size();
        if (size == 0) {
            throw new SendFailedException("No recipient addresses");
        }
        if (message.session != null) {
            defaultInstance = message.session;
        } else {
            defaultInstance = Session.getDefaultInstance(System.getProperties(), null);
        }
        if (size == 1) {
            Transport transport = defaultInstance.getTransport(addressArr[0]);
            try {
                transport.connect();
                transport.sendMessage(message, addressArr);
            } finally {
                transport.close();
            }
        } else {
            Enumeration elements = hashtable.elements();
            boolean z = false;
            SendFailedException sendFailedException = null;
            while (elements.hasMoreElements()) {
                Vector vector5 = (Vector) elements.nextElement();
                Address[] addressArr4 = new Address[vector5.size()];
                vector5.copyInto(addressArr4);
                Transport transport2 = defaultInstance.getTransport(addressArr4[0]);
                if (transport2 == null) {
                    for (Address addElement : addressArr4) {
                        vector.addElement(addElement);
                    }
                } else {
                    try {
                        transport2.connect();
                        transport2.sendMessage(message, addressArr4);
                        transport2.close();
                    } catch (SendFailedException e) {
                        if (sendFailedException == null) {
                            sendFailedException = e;
                        } else {
                            sendFailedException.setNextException(e);
                        }
                        Address[] invalidAddresses = e.getInvalidAddresses();
                        if (invalidAddresses != null) {
                            for (Address addElement2 : invalidAddresses) {
                                vector.addElement(addElement2);
                            }
                        }
                        Address[] validSentAddresses = e.getValidSentAddresses();
                        if (validSentAddresses != null) {
                            for (Address addElement3 : validSentAddresses) {
                                vector2.addElement(addElement3);
                            }
                        }
                        Address[] validUnsentAddresses = e.getValidUnsentAddresses();
                        if (validUnsentAddresses != null) {
                            for (Address addElement4 : validUnsentAddresses) {
                                vector3.addElement(addElement4);
                            }
                        }
                        transport2.close();
                        z = true;
                    } catch (MessagingException e2) {
                        if (sendFailedException == null) {
                            sendFailedException = e2;
                        } else {
                            sendFailedException.setNextException(e2);
                        }
                        transport2.close();
                        z = true;
                    } catch (Throwable th) {
                        transport2.close();
                        throw th;
                    }
                }
            }
            if (z || vector.size() != 0 || vector3.size() != 0) {
                Address[] addressArr5 = null;
                Address[] addressArr6 = null;
                Address[] addressArr7 = null;
                if (vector2.size() > 0) {
                    addressArr2 = new Address[vector2.size()];
                    vector2.copyInto(addressArr2);
                } else {
                    addressArr2 = addressArr5;
                }
                if (vector3.size() > 0) {
                    addressArr6 = new Address[vector3.size()];
                    vector3.copyInto(addressArr6);
                }
                if (vector.size() > 0) {
                    addressArr3 = new Address[vector.size()];
                    vector.copyInto(addressArr3);
                } else {
                    addressArr3 = addressArr7;
                }
                throw new SendFailedException("Sending failed", sendFailedException, addressArr2, addressArr6, addressArr3);
            }
        }
    }

    public synchronized void addTransportListener(TransportListener transportListener) {
        if (this.transportListeners == null) {
            this.transportListeners = new Vector();
        }
        this.transportListeners.addElement(transportListener);
    }

    public synchronized void removeTransportListener(TransportListener transportListener) {
        if (this.transportListeners != null) {
            this.transportListeners.removeElement(transportListener);
        }
    }

    /* access modifiers changed from: protected */
    public void notifyTransportListeners(int i, Address[] addressArr, Address[] addressArr2, Address[] addressArr3, Message message) {
        if (this.transportListeners != null) {
            queueEvent(new TransportEvent(this, i, addressArr, addressArr2, addressArr3, message), this.transportListeners);
        }
    }
}
