package javax.mail.internet;

import java.text.ParseException;

/* compiled from: MailDateFormat */
class MailDateParser {
    int index = 0;
    char[] orig = null;

    public MailDateParser(char[] cArr) {
        this.orig = cArr;
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:3:0x0009 A[Catch:{ ArrayIndexOutOfBoundsException -> 0x0010 }, FALL_THROUGH, LOOP:0: B:0:0x0000->B:3:0x0009, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x001b A[SYNTHETIC] */
    public void skipUntilNumber() throws java.text.ParseException {
        /*
            r3 = this;
        L_0x0000:
            char[] r0 = r3.orig     // Catch:{ ArrayIndexOutOfBoundsException -> 0x0010 }
            int r1 = r3.index     // Catch:{ ArrayIndexOutOfBoundsException -> 0x0010 }
            char r0 = r0[r1]     // Catch:{ ArrayIndexOutOfBoundsException -> 0x0010 }
            switch(r0) {
                case 48: goto L_0x001b;
                case 49: goto L_0x001b;
                case 50: goto L_0x001b;
                case 51: goto L_0x001b;
                case 52: goto L_0x001b;
                case 53: goto L_0x001b;
                case 54: goto L_0x001b;
                case 55: goto L_0x001b;
                case 56: goto L_0x001b;
                case 57: goto L_0x001b;
                default: goto L_0x0009;
            }     // Catch:{ ArrayIndexOutOfBoundsException -> 0x0010 }
        L_0x0009:
            int r0 = r3.index     // Catch:{ ArrayIndexOutOfBoundsException -> 0x0010 }
            int r0 = r0 + 1
            r3.index = r0     // Catch:{ ArrayIndexOutOfBoundsException -> 0x0010 }
            goto L_0x0000
        L_0x0010:
            r0 = move-exception
            java.text.ParseException r0 = new java.text.ParseException
            java.lang.String r1 = "No Number Found"
            int r2 = r3.index
            r0.<init>(r1, r2)
            throw r0
        L_0x001b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: javax.mail.internet.MailDateParser.skipUntilNumber():void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:3:0x0008  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void skipWhiteSpace() {
        /*
            r3 = this;
            char[] r0 = r3.orig
            int r0 = r0.length
        L_0x0003:
            int r1 = r3.index
            if (r1 < r0) goto L_0x0008
        L_0x0007:
            return
        L_0x0008:
            char[] r1 = r3.orig
            int r2 = r3.index
            char r1 = r1[r2]
            switch(r1) {
                case 9: goto L_0x0012;
                case 10: goto L_0x0012;
                case 13: goto L_0x0012;
                case 32: goto L_0x0012;
                default: goto L_0x0011;
            }
        L_0x0011:
            goto L_0x0007
        L_0x0012:
            int r1 = r3.index
            int r1 = r1 + 1
            r3.index = r1
            goto L_0x0003
        */
        throw new UnsupportedOperationException("Method not decompiled: javax.mail.internet.MailDateParser.skipWhiteSpace():void");
    }

    public int peekChar() throws ParseException {
        if (this.index < this.orig.length) {
            return this.orig[this.index];
        }
        throw new ParseException("No more characters", this.index);
    }

    public void skipChar(char c) throws ParseException {
        if (this.index >= this.orig.length) {
            throw new ParseException("No more characters", this.index);
        } else if (this.orig[this.index] == c) {
            this.index++;
        } else {
            throw new ParseException("Wrong char", this.index);
        }
    }

    public boolean skipIfChar(char c) throws ParseException {
        if (this.index >= this.orig.length) {
            throw new ParseException("No more characters", this.index);
        } else if (this.orig[this.index] != c) {
            return false;
        } else {
            this.index++;
            return true;
        }
    }

    public int parseNumber() throws ParseException {
        int i = 0;
        int length = this.orig.length;
        boolean z = false;
        while (true) {
            if (this.index < length) {
                switch (this.orig[this.index]) {
                    case '0':
                        i *= 10;
                        break;
                    case '1':
                        i = (i * 10) + 1;
                        break;
                    case '2':
                        i = (i * 10) + 2;
                        break;
                    case '3':
                        i = (i * 10) + 3;
                        break;
                    case '4':
                        i = (i * 10) + 4;
                        break;
                    case '5':
                        i = (i * 10) + 5;
                        break;
                    case '6':
                        i = (i * 10) + 6;
                        break;
                    case '7':
                        i = (i * 10) + 7;
                        break;
                    case '8':
                        i = (i * 10) + 8;
                        break;
                    case '9':
                        i = (i * 10) + 9;
                        break;
                    default:
                        if (!z) {
                            throw new ParseException("No Number found", this.index);
                        }
                        break;
                }
                this.index++;
                z = true;
            } else if (!z) {
                throw new ParseException("No Number found", this.index);
            }
        }
        return i;
    }

    public int parseMonth() throws ParseException {
        try {
            char[] cArr = this.orig;
            int i = this.index;
            this.index = i + 1;
            switch (cArr[i]) {
                case 'A':
                case 'a':
                    char[] cArr2 = this.orig;
                    int i2 = this.index;
                    this.index = i2 + 1;
                    char c = cArr2[i2];
                    if (c == 'P' || c == 'p') {
                        char[] cArr3 = this.orig;
                        int i3 = this.index;
                        this.index = i3 + 1;
                        char c2 = cArr3[i3];
                        if (c2 == 'R' || c2 == 'r') {
                            return 3;
                        }
                        throw new ParseException("Bad Month", this.index);
                    }
                    if (c == 'U' || c == 'u') {
                        char[] cArr4 = this.orig;
                        int i4 = this.index;
                        this.index = i4 + 1;
                        char c3 = cArr4[i4];
                        if (c3 == 'G' || c3 == 'g') {
                            return 7;
                        }
                    }
                    throw new ParseException("Bad Month", this.index);
                case 'D':
                case 'd':
                    char[] cArr5 = this.orig;
                    int i5 = this.index;
                    this.index = i5 + 1;
                    char c4 = cArr5[i5];
                    if (c4 == 'E' || c4 == 'e') {
                        char[] cArr6 = this.orig;
                        int i6 = this.index;
                        this.index = i6 + 1;
                        char c5 = cArr6[i6];
                        if (c5 == 'C' || c5 == 'c') {
                            return 11;
                        }
                    }
                    throw new ParseException("Bad Month", this.index);
                case 'F':
                case 'f':
                    char[] cArr7 = this.orig;
                    int i7 = this.index;
                    this.index = i7 + 1;
                    char c6 = cArr7[i7];
                    if (c6 == 'E' || c6 == 'e') {
                        char[] cArr8 = this.orig;
                        int i8 = this.index;
                        this.index = i8 + 1;
                        char c7 = cArr8[i8];
                        if (c7 == 'B' || c7 == 'b') {
                            return 1;
                        }
                    }
                    throw new ParseException("Bad Month", this.index);
                case 'J':
                case 'j':
                    char[] cArr9 = this.orig;
                    int i9 = this.index;
                    this.index = i9 + 1;
                    switch (cArr9[i9]) {
                        case 'A':
                        case 'a':
                            char[] cArr10 = this.orig;
                            int i10 = this.index;
                            this.index = i10 + 1;
                            char c8 = cArr10[i10];
                            if (c8 == 'N' || c8 == 'n') {
                                return 0;
                            }
                        case 'U':
                        case 'u':
                            char[] cArr11 = this.orig;
                            int i11 = this.index;
                            this.index = i11 + 1;
                            char c9 = cArr11[i11];
                            if (c9 == 'N' || c9 == 'n') {
                                return 5;
                            }
                            if (c9 == 'L' || c9 == 'l') {
                                return 6;
                            }
                            break;
                    }
                    throw new ParseException("Bad Month", this.index);
                case 'M':
                case 'm':
                    char[] cArr12 = this.orig;
                    int i12 = this.index;
                    this.index = i12 + 1;
                    char c10 = cArr12[i12];
                    if (c10 == 'A' || c10 == 'a') {
                        char[] cArr13 = this.orig;
                        int i13 = this.index;
                        this.index = i13 + 1;
                        char c11 = cArr13[i13];
                        if (c11 == 'R' || c11 == 'r') {
                            return 2;
                        }
                        if (c11 == 'Y' || c11 == 'y') {
                            return 4;
                        }
                    }
                    throw new ParseException("Bad Month", this.index);
                case 'N':
                case 'n':
                    char[] cArr14 = this.orig;
                    int i14 = this.index;
                    this.index = i14 + 1;
                    char c12 = cArr14[i14];
                    if (c12 == 'O' || c12 == 'o') {
                        char[] cArr15 = this.orig;
                        int i15 = this.index;
                        this.index = i15 + 1;
                        char c13 = cArr15[i15];
                        if (c13 == 'V' || c13 == 'v') {
                            return 10;
                        }
                    }
                    throw new ParseException("Bad Month", this.index);
                case 'O':
                case 'o':
                    char[] cArr16 = this.orig;
                    int i16 = this.index;
                    this.index = i16 + 1;
                    char c14 = cArr16[i16];
                    if (c14 == 'C' || c14 == 'c') {
                        char[] cArr17 = this.orig;
                        int i17 = this.index;
                        this.index = i17 + 1;
                        char c15 = cArr17[i17];
                        if (c15 == 'T' || c15 == 't') {
                            return 9;
                        }
                    }
                    throw new ParseException("Bad Month", this.index);
                case 'S':
                case 's':
                    char[] cArr18 = this.orig;
                    int i18 = this.index;
                    this.index = i18 + 1;
                    char c16 = cArr18[i18];
                    if (c16 == 'E' || c16 == 'e') {
                        char[] cArr19 = this.orig;
                        int i19 = this.index;
                        this.index = i19 + 1;
                        char c17 = cArr19[i19];
                        if (c17 == 'P' || c17 == 'p') {
                            return 8;
                        }
                    }
                    throw new ParseException("Bad Month", this.index);
                default:
                    throw new ParseException("Bad Month", this.index);
            }
        } catch (ArrayIndexOutOfBoundsException e) {
        }
    }

    public int parseTimeZone() throws ParseException {
        if (this.index >= this.orig.length) {
            throw new ParseException("No more characters", this.index);
        }
        char c = this.orig[this.index];
        if (c == '+' || c == '-') {
            return parseNumericTimeZone();
        }
        return parseAlphaTimeZone();
    }

    public int parseNumericTimeZone() throws ParseException {
        boolean z = false;
        char[] cArr = this.orig;
        int i = this.index;
        this.index = i + 1;
        char c = cArr[i];
        if (c == '+') {
            z = true;
        } else if (c != '-') {
            throw new ParseException("Bad Numeric TimeZone", this.index);
        }
        int parseNumber = parseNumber();
        int i2 = (parseNumber % 100) + ((parseNumber / 100) * 60);
        if (z) {
            return -i2;
        }
        return i2;
    }

    public int parseAlphaTimeZone() throws ParseException {
        int i = 0;
        boolean z = true;
        try {
            char[] cArr = this.orig;
            int i2 = this.index;
            this.index = i2 + 1;
            switch (cArr[i2]) {
                case 'C':
                case 'c':
                    i = 360;
                    break;
                case 'E':
                case 'e':
                    i = 300;
                    break;
                case 'G':
                case 'g':
                    char[] cArr2 = this.orig;
                    int i3 = this.index;
                    this.index = i3 + 1;
                    char c = cArr2[i3];
                    if (c == 'M' || c == 'm') {
                        char[] cArr3 = this.orig;
                        int i4 = this.index;
                        this.index = i4 + 1;
                        char c2 = cArr3[i4];
                        if (c2 == 'T' || c2 == 't') {
                            z = false;
                            break;
                        }
                    }
                    throw new ParseException("Bad Alpha TimeZone", this.index);
                case 'M':
                case 'm':
                    i = 420;
                    break;
                case 'P':
                case 'p':
                    i = 480;
                    break;
                case 'U':
                case 'u':
                    char[] cArr4 = this.orig;
                    int i5 = this.index;
                    this.index = i5 + 1;
                    char c3 = cArr4[i5];
                    if (c3 == 'T' || c3 == 't') {
                        z = false;
                        break;
                    } else {
                        throw new ParseException("Bad Alpha TimeZone", this.index);
                    }
                default:
                    throw new ParseException("Bad Alpha TimeZone", this.index);
            }
            if (!z) {
                return i;
            }
            char[] cArr5 = this.orig;
            int i6 = this.index;
            this.index = i6 + 1;
            char c4 = cArr5[i6];
            if (c4 == 'S' || c4 == 's') {
                char[] cArr6 = this.orig;
                int i7 = this.index;
                this.index = i7 + 1;
                char c5 = cArr6[i7];
                if (c5 == 'T' || c5 == 't') {
                    return i;
                }
                throw new ParseException("Bad Alpha TimeZone", this.index);
            } else if (c4 != 'D' && c4 != 'd') {
                return i;
            } else {
                char[] cArr7 = this.orig;
                int i8 = this.index;
                this.index = i8 + 1;
                char c6 = cArr7[i8];
                if (c6 == 'T' || c6 != 't') {
                    return i - 60;
                }
                throw new ParseException("Bad Alpha TimeZone", this.index);
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new ParseException("Bad Alpha TimeZone", this.index);
        }
    }

    /* access modifiers changed from: package-private */
    public int getIndex() {
        return this.index;
    }
}
