package javax.mail.internet;

import com.sun.mail.util.ASCIIUtility;
import com.sun.mail.util.BASE64DecoderStream;
import com.sun.mail.util.BASE64EncoderStream;
import com.sun.mail.util.BEncoderStream;
import com.sun.mail.util.LineInputStream;
import com.sun.mail.util.QDecoderStream;
import com.sun.mail.util.QEncoderStream;
import com.sun.mail.util.QPDecoderStream;
import com.sun.mail.util.QPEncoderStream;
import com.sun.mail.util.UUDecoderStream;
import com.sun.mail.util.UUEncoderStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Hashtable;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.MessagingException;

public class MimeUtility {
    public static final int ALL = -1;
    static final int ALL_ASCII = 1;
    static final int MOSTLY_ASCII = 2;
    static final int MOSTLY_NONASCII = 3;
    private static boolean decodeStrict;
    private static String defaultJavaCharset;
    private static String defaultMIMECharset;
    private static boolean encodeEolStrict;
    private static boolean foldEncodedWords;
    private static boolean foldText;
    private static Hashtable java2mime = new Hashtable(40);
    private static Hashtable mime2java = new Hashtable(10);

    private MimeUtility() {
    }

    static {
        LineInputStream lineInputStream;
        boolean z;
        boolean z2;
        boolean z3 = false;
        decodeStrict = true;
        encodeEolStrict = false;
        foldEncodedWords = false;
        foldText = true;
        try {
            String property = System.getProperty("mail.mime.decodetext.strict");
            decodeStrict = property == null || !property.equalsIgnoreCase("false");
            String property2 = System.getProperty("mail.mime.encodeeol.strict");
            if (property2 == null || !property2.equalsIgnoreCase("true")) {
                z = false;
            } else {
                z = true;
            }
            encodeEolStrict = z;
            String property3 = System.getProperty("mail.mime.foldencodedwords");
            if (property3 == null || !property3.equalsIgnoreCase("true")) {
                z2 = false;
            } else {
                z2 = true;
            }
            foldEncodedWords = z2;
            String property4 = System.getProperty("mail.mime.foldtext");
            if (property4 == null || !property4.equalsIgnoreCase("false")) {
                z3 = true;
            }
            foldText = z3;
        } catch (SecurityException e) {
        }
        try {
            InputStream resourceAsStream = MimeUtility.class.getResourceAsStream("/META-INF/javamail.charset.map");
            if (resourceAsStream != null) {
                try {
                    lineInputStream = new LineInputStream(resourceAsStream);
                    try {
                        loadMappings(lineInputStream, java2mime);
                        loadMappings(lineInputStream, mime2java);
                        try {
                            lineInputStream.close();
                        } catch (Exception e2) {
                        }
                    } catch (Throwable th) {
                        th = th;
                        try {
                            lineInputStream.close();
                        } catch (Exception e3) {
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    lineInputStream = resourceAsStream;
                    lineInputStream.close();
                    throw th;
                }
            }
        } catch (Exception e4) {
        }
        if (java2mime.isEmpty()) {
            java2mime.put("8859_1", "ISO-8859-1");
            java2mime.put("iso8859_1", "ISO-8859-1");
            java2mime.put("iso8859-1", "ISO-8859-1");
            java2mime.put("8859_2", "ISO-8859-2");
            java2mime.put("iso8859_2", "ISO-8859-2");
            java2mime.put("iso8859-2", "ISO-8859-2");
            java2mime.put("8859_3", "ISO-8859-3");
            java2mime.put("iso8859_3", "ISO-8859-3");
            java2mime.put("iso8859-3", "ISO-8859-3");
            java2mime.put("8859_4", "ISO-8859-4");
            java2mime.put("iso8859_4", "ISO-8859-4");
            java2mime.put("iso8859-4", "ISO-8859-4");
            java2mime.put("8859_5", "ISO-8859-5");
            java2mime.put("iso8859_5", "ISO-8859-5");
            java2mime.put("iso8859-5", "ISO-8859-5");
            java2mime.put("8859_6", "ISO-8859-6");
            java2mime.put("iso8859_6", "ISO-8859-6");
            java2mime.put("iso8859-6", "ISO-8859-6");
            java2mime.put("8859_7", "ISO-8859-7");
            java2mime.put("iso8859_7", "ISO-8859-7");
            java2mime.put("iso8859-7", "ISO-8859-7");
            java2mime.put("8859_8", "ISO-8859-8");
            java2mime.put("iso8859_8", "ISO-8859-8");
            java2mime.put("iso8859-8", "ISO-8859-8");
            java2mime.put("8859_9", "ISO-8859-9");
            java2mime.put("iso8859_9", "ISO-8859-9");
            java2mime.put("iso8859-9", "ISO-8859-9");
            java2mime.put("sjis", "Shift_JIS");
            java2mime.put("jis", "ISO-2022-JP");
            java2mime.put("iso2022jp", "ISO-2022-JP");
            java2mime.put("euc_jp", "euc-jp");
            java2mime.put("koi8_r", "koi8-r");
            java2mime.put("euc_cn", "euc-cn");
            java2mime.put("euc_tw", "euc-tw");
            java2mime.put("euc_kr", "euc-kr");
        }
        if (mime2java.isEmpty()) {
            mime2java.put("iso-2022-cn", "ISO2022CN");
            mime2java.put("iso-2022-kr", "ISO2022KR");
            mime2java.put("utf-8", "UTF8");
            mime2java.put("utf8", "UTF8");
            mime2java.put("ja_jp.iso2022-7", "ISO2022JP");
            mime2java.put("ja_jp.eucjp", "EUCJIS");
            mime2java.put("euc-kr", "KSC5601");
            mime2java.put("euckr", "KSC5601");
            mime2java.put("us-ascii", "ISO-8859-1");
            mime2java.put("x-us-ascii", "ISO-8859-1");
        }
    }

    public static String getEncoding(DataSource dataSource) {
        String str;
        try {
            ContentType contentType = new ContentType(dataSource.getContentType());
            InputStream inputStream = dataSource.getInputStream();
            switch (checkAscii(inputStream, -1, !contentType.match("text/*"))) {
                case 1:
                    str = "7bit";
                    break;
                case 2:
                    str = "quoted-printable";
                    break;
                default:
                    str = "base64";
                    break;
            }
            try {
                inputStream.close();
                return str;
            } catch (IOException e) {
                return str;
            }
        } catch (Exception e2) {
            return "base64";
        }
    }

    public static String getEncoding(DataHandler dataHandler) {
        if (dataHandler.getName() != null) {
            return getEncoding(dataHandler.getDataSource());
        }
        try {
            if (new ContentType(dataHandler.getContentType()).match("text/*")) {
                AsciiOutputStream asciiOutputStream = new AsciiOutputStream(false, false);
                try {
                    dataHandler.writeTo(asciiOutputStream);
                } catch (IOException e) {
                }
                switch (asciiOutputStream.getAscii()) {
                    case 1:
                        return "7bit";
                    case 2:
                        return "quoted-printable";
                    default:
                        return "base64";
                }
            } else {
                AsciiOutputStream asciiOutputStream2 = new AsciiOutputStream(true, encodeEolStrict);
                try {
                    dataHandler.writeTo(asciiOutputStream2);
                } catch (IOException e2) {
                }
                if (asciiOutputStream2.getAscii() == 1) {
                    return "7bit";
                }
                return "base64";
            }
        } catch (Exception e3) {
            return "base64";
        }
    }

    public static InputStream decode(InputStream inputStream, String str) throws MessagingException {
        if (str.equalsIgnoreCase("base64")) {
            return new BASE64DecoderStream(inputStream);
        }
        if (str.equalsIgnoreCase("quoted-printable")) {
            return new QPDecoderStream(inputStream);
        }
        if (str.equalsIgnoreCase("uuencode") || str.equalsIgnoreCase("x-uuencode") || str.equalsIgnoreCase("x-uue")) {
            return new UUDecoderStream(inputStream);
        }
        if (str.equalsIgnoreCase("binary") || str.equalsIgnoreCase("7bit") || str.equalsIgnoreCase("8bit")) {
            return inputStream;
        }
        throw new MessagingException("Unknown encoding: " + str);
    }

    public static OutputStream encode(OutputStream outputStream, String str) throws MessagingException {
        if (str == null) {
            return outputStream;
        }
        if (str.equalsIgnoreCase("base64")) {
            return new BASE64EncoderStream(outputStream);
        }
        if (str.equalsIgnoreCase("quoted-printable")) {
            return new QPEncoderStream(outputStream);
        }
        if (str.equalsIgnoreCase("uuencode") || str.equalsIgnoreCase("x-uuencode") || str.equalsIgnoreCase("x-uue")) {
            return new UUEncoderStream(outputStream);
        }
        if (str.equalsIgnoreCase("binary") || str.equalsIgnoreCase("7bit") || str.equalsIgnoreCase("8bit")) {
            return outputStream;
        }
        throw new MessagingException("Unknown encoding: " + str);
    }

    public static OutputStream encode(OutputStream outputStream, String str, String str2) throws MessagingException {
        if (str == null) {
            return outputStream;
        }
        if (str.equalsIgnoreCase("base64")) {
            return new BASE64EncoderStream(outputStream);
        }
        if (str.equalsIgnoreCase("quoted-printable")) {
            return new QPEncoderStream(outputStream);
        }
        if (str.equalsIgnoreCase("uuencode") || str.equalsIgnoreCase("x-uuencode") || str.equalsIgnoreCase("x-uue")) {
            return new UUEncoderStream(outputStream, str2);
        }
        if (str.equalsIgnoreCase("binary") || str.equalsIgnoreCase("7bit") || str.equalsIgnoreCase("8bit")) {
            return outputStream;
        }
        throw new MessagingException("Unknown encoding: " + str);
    }

    public static String encodeText(String str) throws UnsupportedEncodingException {
        return encodeText(str, null, null);
    }

    public static String encodeText(String str, String str2, String str3) throws UnsupportedEncodingException {
        return encodeWord(str, str2, str3, false);
    }

    public static String decodeText(String str) throws UnsupportedEncodingException {
        String str2;
        if (str.indexOf("=?") == -1) {
            return str;
        }
        StringTokenizer stringTokenizer = new StringTokenizer(str, " \t\n\r", true);
        StringBuffer stringBuffer = new StringBuffer();
        StringBuffer stringBuffer2 = new StringBuffer();
        boolean z = false;
        while (stringTokenizer.hasMoreTokens()) {
            String nextToken = stringTokenizer.nextToken();
            char charAt = nextToken.charAt(0);
            if (charAt == ' ' || charAt == 9 || charAt == 13 || charAt == 10) {
                stringBuffer2.append(charAt);
            } else {
                try {
                    str2 = decodeWord(nextToken);
                    if (!z && stringBuffer2.length() > 0) {
                        stringBuffer.append(stringBuffer2);
                    }
                    z = true;
                } catch (ParseException e) {
                    if (!decodeStrict) {
                        str2 = decodeInnerWords(nextToken);
                        if (str2 != nextToken) {
                            if ((!z || !nextToken.startsWith("=?")) && stringBuffer2.length() > 0) {
                                stringBuffer.append(stringBuffer2);
                            }
                            z = nextToken.endsWith("?=");
                        } else {
                            if (stringBuffer2.length() > 0) {
                                stringBuffer.append(stringBuffer2);
                            }
                            str2 = nextToken;
                            z = false;
                        }
                    } else {
                        if (stringBuffer2.length() > 0) {
                            stringBuffer.append(stringBuffer2);
                        }
                        str2 = nextToken;
                        z = false;
                    }
                }
                stringBuffer.append(str2);
                stringBuffer2.setLength(0);
            }
        }
        stringBuffer.append(stringBuffer2);
        return stringBuffer.toString();
    }

    public static String encodeWord(String str) throws UnsupportedEncodingException {
        return encodeWord(str, null, null);
    }

    public static String encodeWord(String str, String str2, String str3) throws UnsupportedEncodingException {
        return encodeWord(str, str2, str3, true);
    }

    private static String encodeWord(String str, String str2, String str3, boolean z) throws UnsupportedEncodingException {
        String javaCharset;
        boolean z2;
        int checkAscii = checkAscii(str);
        if (checkAscii == 1) {
            return str;
        }
        if (str2 == null) {
            javaCharset = getDefaultJavaCharset();
            str2 = getDefaultMIMECharset();
        } else {
            javaCharset = javaCharset(str2);
        }
        if (str3 == null) {
            if (checkAscii != 3) {
                str3 = "Q";
            } else {
                str3 = "B";
            }
        }
        if (str3.equalsIgnoreCase("B")) {
            z2 = true;
        } else if (str3.equalsIgnoreCase("Q")) {
            z2 = false;
        } else {
            throw new UnsupportedEncodingException("Unknown transfer encoding: " + str3);
        }
        StringBuffer stringBuffer = new StringBuffer();
        doEncode(str, z2, javaCharset, 68 - str2.length(), "=?" + str2 + "?" + str3 + "?", true, z, stringBuffer);
        return stringBuffer.toString();
    }

    private static void doEncode(String str, boolean z, String str2, int i, String str3, boolean z2, boolean z3, StringBuffer stringBuffer) throws UnsupportedEncodingException {
        int encodedLength;
        OutputStream qEncoderStream;
        int length;
        byte[] bytes = str.getBytes(str2);
        if (z) {
            encodedLength = BEncoderStream.encodedLength(bytes);
        } else {
            encodedLength = QEncoderStream.encodedLength(bytes, z3);
        }
        if (encodedLength <= i || (length = str.length()) <= 1) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            if (z) {
                qEncoderStream = new BEncoderStream(byteArrayOutputStream);
            } else {
                qEncoderStream = new QEncoderStream(byteArrayOutputStream, z3);
            }
            try {
                qEncoderStream.write(bytes);
                qEncoderStream.close();
            } catch (IOException e) {
            }
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            if (!z2) {
                if (foldEncodedWords) {
                    stringBuffer.append("\r\n ");
                } else {
                    stringBuffer.append(" ");
                }
            }
            stringBuffer.append(str3);
            for (byte b : byteArray) {
                stringBuffer.append((char) b);
            }
            stringBuffer.append("?=");
            return;
        }
        doEncode(str.substring(0, length / 2), z, str2, i, str3, z2, z3, stringBuffer);
        doEncode(str.substring(length / 2, length), z, str2, i, str3, false, z3, stringBuffer);
    }

    public static String decodeWord(String str) throws ParseException, UnsupportedEncodingException {
        String str2;
        InputStream qDecoderStream;
        if (!str.startsWith("=?")) {
            throw new ParseException("encoded word does not start with \"=?\": " + str);
        }
        int indexOf = str.indexOf(63, 2);
        if (indexOf == -1) {
            throw new ParseException("encoded word does not include charset: " + str);
        }
        String javaCharset = javaCharset(str.substring(2, indexOf));
        int i = indexOf + 1;
        int indexOf2 = str.indexOf(63, i);
        if (indexOf2 == -1) {
            throw new ParseException("encoded word does not include encoding: " + str);
        }
        String substring = str.substring(i, indexOf2);
        int i2 = indexOf2 + 1;
        int indexOf3 = str.indexOf("?=", i2);
        if (indexOf3 == -1) {
            throw new ParseException("encoded word does not end with \"?=\": " + str);
        }
        String substring2 = str.substring(i2, indexOf3);
        try {
            if (substring2.length() > 0) {
                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(ASCIIUtility.getBytes(substring2));
                if (substring.equalsIgnoreCase("B")) {
                    qDecoderStream = new BASE64DecoderStream(byteArrayInputStream);
                } else if (substring.equalsIgnoreCase("Q")) {
                    qDecoderStream = new QDecoderStream(byteArrayInputStream);
                } else {
                    throw new UnsupportedEncodingException("unknown encoding: " + substring);
                }
                int available = byteArrayInputStream.available();
                byte[] bArr = new byte[available];
                int read = qDecoderStream.read(bArr, 0, available);
                if (read <= 0) {
                    str2 = "";
                } else {
                    str2 = new String(bArr, 0, read, javaCharset);
                }
            } else {
                str2 = "";
            }
            if (indexOf3 + 2 >= str.length()) {
                return str2;
            }
            String substring3 = str.substring(indexOf3 + 2);
            if (!decodeStrict) {
                substring3 = decodeInnerWords(substring3);
            }
            return String.valueOf(str2) + substring3;
        } catch (UnsupportedEncodingException e) {
            throw e;
        } catch (IOException e2) {
            throw new ParseException(e2.toString());
        } catch (IllegalArgumentException e3) {
            throw new UnsupportedEncodingException(javaCharset);
        }
    }

    private static String decodeInnerWords(String str) throws UnsupportedEncodingException {
        int indexOf;
        int indexOf2;
        int i = 0;
        StringBuffer stringBuffer = new StringBuffer();
        while (true) {
            int indexOf3 = str.indexOf("=?", i);
            if (indexOf3 >= 0) {
                stringBuffer.append(str.substring(i, indexOf3));
                int indexOf4 = str.indexOf(63, indexOf3 + 2);
                if (indexOf4 < 0 || (indexOf = str.indexOf(63, indexOf4 + 1)) < 0 || (indexOf2 = str.indexOf("?=", indexOf + 1)) < 0) {
                    break;
                }
                String substring = str.substring(indexOf3, indexOf2 + 2);
                try {
                    substring = decodeWord(substring);
                } catch (ParseException e) {
                }
                stringBuffer.append(substring);
                i = indexOf2 + 2;
            } else {
                break;
            }
        }
        if (i == 0) {
            return str;
        }
        if (i < str.length()) {
            stringBuffer.append(str.substring(i));
        }
        return stringBuffer.toString();
    }

    public static String quote(String str, String str2) {
        int length = str.length();
        boolean z = false;
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if (charAt == '\"' || charAt == '\\' || charAt == 13 || charAt == 10) {
                StringBuffer stringBuffer = new StringBuffer(length + 3);
                stringBuffer.append('\"');
                stringBuffer.append(str.substring(0, i));
                int i2 = i;
                char c = 0;
                while (i2 < length) {
                    char charAt2 = str.charAt(i2);
                    if ((charAt2 == '\"' || charAt2 == '\\' || charAt2 == 13 || charAt2 == 10) && !(charAt2 == 10 && c == 13)) {
                        stringBuffer.append('\\');
                    }
                    stringBuffer.append(charAt2);
                    i2++;
                    c = charAt2;
                }
                stringBuffer.append('\"');
                return stringBuffer.toString();
            }
            if (charAt < ' ' || charAt >= 127 || str2.indexOf(charAt) >= 0) {
                z = true;
            }
        }
        if (!z) {
            return str;
        }
        StringBuffer stringBuffer2 = new StringBuffer(length + 2);
        stringBuffer2.append('\"').append(str).append('\"');
        return stringBuffer2.toString();
    }

    public static String fold(int i, String str) {
        if (!foldText) {
            return str;
        }
        int length = str.length() - 1;
        while (length >= 0) {
            char charAt = str.charAt(length);
            if (charAt != ' ' && charAt != 9 && charAt != 13 && charAt != 10) {
                break;
            }
            length--;
        }
        if (length != str.length() - 1) {
            str = str.substring(0, length + 1);
        }
        if (str.length() + i <= 76) {
            return str;
        }
        StringBuffer stringBuffer = new StringBuffer(str.length() + 4);
        char c = 0;
        while (true) {
            if (str.length() + i <= 76) {
                break;
            }
            int i2 = -1;
            char c2 = c;
            int i3 = 0;
            while (i3 < str.length() && (i2 == -1 || i + i3 <= 76)) {
                char charAt2 = str.charAt(i3);
                if (!((charAt2 != ' ' && charAt2 != 9) || c2 == ' ' || c2 == 9)) {
                    i2 = i3;
                }
                i3++;
                c2 = charAt2;
            }
            if (i2 == -1) {
                stringBuffer.append(str);
                str = "";
                break;
            }
            stringBuffer.append(str.substring(0, i2));
            stringBuffer.append("\r\n");
            c = str.charAt(i2);
            stringBuffer.append(c);
            str = str.substring(i2 + 1);
            i = 1;
        }
        stringBuffer.append(str);
        return stringBuffer.toString();
    }

    public static String unfold(String str) {
        char charAt;
        if (!foldText) {
            return str;
        }
        StringBuffer stringBuffer = null;
        while (true) {
            int indexOfAny = indexOfAny(str, "\r\n");
            if (indexOfAny < 0) {
                break;
            }
            int length = str.length();
            int i = indexOfAny + 1;
            if (i < length && str.charAt(i - 1) == 13 && str.charAt(i) == 10) {
                i++;
            }
            if (indexOfAny != 0 && str.charAt(indexOfAny - 1) == '\\') {
                if (stringBuffer == null) {
                    stringBuffer = new StringBuffer(str.length());
                }
                stringBuffer.append(str.substring(0, indexOfAny - 1));
                stringBuffer.append(str.substring(indexOfAny, i));
                str = str.substring(i);
            } else if (i >= length || !((charAt = str.charAt(i)) == ' ' || charAt == 9)) {
                if (stringBuffer == null) {
                    stringBuffer = new StringBuffer(str.length());
                }
                stringBuffer.append(str.substring(0, i));
                str = str.substring(i);
            } else {
                int i2 = i + 1;
                while (i2 < length) {
                    char charAt2 = str.charAt(i2);
                    if (charAt2 != ' ' && charAt2 != 9) {
                        break;
                    }
                    i2++;
                }
                if (stringBuffer == null) {
                    stringBuffer = new StringBuffer(str.length());
                }
                if (indexOfAny != 0) {
                    stringBuffer.append(str.substring(0, indexOfAny));
                    stringBuffer.append(' ');
                }
                str = str.substring(i2);
            }
        }
        if (stringBuffer == null) {
            return str;
        }
        stringBuffer.append(str);
        return stringBuffer.toString();
    }

    private static int indexOfAny(String str, String str2) {
        return indexOfAny(str, str2, 0);
    }

    private static int indexOfAny(String str, String str2, int i) {
        try {
            int length = str.length();
            for (int i2 = i; i2 < length; i2++) {
                if (str2.indexOf(str.charAt(i2)) >= 0) {
                    return i2;
                }
            }
            return -1;
        } catch (StringIndexOutOfBoundsException e) {
            return -1;
        }
    }

    public static String javaCharset(String str) {
        String str2;
        return (mime2java == null || str == null || (str2 = (String) mime2java.get(str.toLowerCase(Locale.ENGLISH))) == null) ? str : str2;
    }

    public static String mimeCharset(String str) {
        String str2;
        return (java2mime == null || str == null || (str2 = (String) java2mime.get(str.toLowerCase(Locale.ENGLISH))) == null) ? str : str2;
    }

    public static String getDefaultJavaCharset() {
        if (defaultJavaCharset == null) {
            String str = null;
            try {
                str = System.getProperty("mail.mime.charset");
            } catch (SecurityException e) {
            }
            if (str == null || str.length() <= 0) {
                try {
                    defaultJavaCharset = System.getProperty("file.encoding", "8859_1");
                } catch (SecurityException e2) {
                    defaultJavaCharset = new InputStreamReader(new InputStream() {
                        public int read() {
                            return 0;
                        }
                    }).getEncoding();
                    if (defaultJavaCharset == null) {
                        defaultJavaCharset = "8859_1";
                    }
                }
            } else {
                defaultJavaCharset = javaCharset(str);
                return defaultJavaCharset;
            }
        }
        return defaultJavaCharset;
    }

    static String getDefaultMIMECharset() {
        if (defaultMIMECharset == null) {
            try {
                defaultMIMECharset = System.getProperty("mail.mime.charset");
            } catch (SecurityException e) {
            }
        }
        if (defaultMIMECharset == null) {
            defaultMIMECharset = mimeCharset(getDefaultJavaCharset());
        }
        return defaultMIMECharset;
    }

    private static void loadMappings(LineInputStream lineInputStream, Hashtable hashtable) {
        while (true) {
            try {
                String readLine = lineInputStream.readLine();
                if (readLine != null) {
                    if (readLine.startsWith("--") && readLine.endsWith("--")) {
                        return;
                    }
                    if (readLine.trim().length() != 0 && !readLine.startsWith("#")) {
                        StringTokenizer stringTokenizer = new StringTokenizer(readLine, " \t");
                        try {
                            String nextToken = stringTokenizer.nextToken();
                            hashtable.put(nextToken.toLowerCase(Locale.ENGLISH), stringTokenizer.nextToken());
                        } catch (NoSuchElementException e) {
                        }
                    }
                } else {
                    return;
                }
            } catch (IOException e2) {
                return;
            }
        }
    }

    static int checkAscii(String str) {
        int i = 0;
        int length = str.length();
        int i2 = 0;
        for (int i3 = 0; i3 < length; i3++) {
            if (nonascii(str.charAt(i3))) {
                i++;
            } else {
                i2++;
            }
        }
        if (i == 0) {
            return 1;
        }
        if (i2 > i) {
            return 2;
        }
        return 3;
    }

    static int checkAscii(byte[] bArr) {
        int i = 0;
        int i2 = 0;
        for (byte b : bArr) {
            if (nonascii(b & 255)) {
                i++;
            } else {
                i2++;
            }
        }
        if (i == 0) {
            return 1;
        }
        if (i2 > i) {
            return 2;
        }
        return 3;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0028 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00a7  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x00b3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static int checkAscii(java.io.InputStream r17, int r18, boolean r19) {
        /*
            r7 = 0
            r6 = 0
            r3 = 4096(0x1000, float:5.74E-42)
            r9 = 0
            r5 = 0
            r4 = 0
            boolean r1 = javax.mail.internet.MimeUtility.encodeEolStrict
            if (r1 == 0) goto L_0x002c
            if (r19 == 0) goto L_0x002c
            r1 = 1
            r2 = r1
        L_0x000f:
            r1 = 0
            byte[] r1 = (byte[]) r1
            if (r18 == 0) goto L_0x00cb
            r1 = -1
            r0 = r18
            if (r0 != r1) goto L_0x002f
            r1 = 4096(0x1000, float:5.74E-42)
        L_0x001b:
            byte[] r3 = new byte[r1]
            r10 = r3
            r8 = r9
            r11 = r1
            r1 = r4
            r3 = r5
            r4 = r6
            r5 = r7
        L_0x0024:
            if (r18 != 0) goto L_0x0038
        L_0x0026:
            if (r18 != 0) goto L_0x00a5
            if (r19 == 0) goto L_0x00a5
            r1 = 3
        L_0x002b:
            return r1
        L_0x002c:
            r1 = 0
            r2 = r1
            goto L_0x000f
        L_0x002f:
            r1 = 4096(0x1000, float:5.74E-42)
            r0 = r18
            int r1 = java.lang.Math.min(r0, r1)
            goto L_0x001b
        L_0x0038:
            r6 = 0
            r0 = r17
            int r12 = r0.read(r10, r6, r11)     // Catch:{ IOException -> 0x009d }
            r6 = -1
            if (r12 == r6) goto L_0x0026
            r7 = 0
            r6 = 0
            r9 = r8
            r14 = r7
            r7 = r6
            r6 = r14
            r15 = r3
            r3 = r5
            r5 = r15
            r16 = r4
            r4 = r1
            r1 = r16
        L_0x0050:
            if (r7 < r12) goto L_0x0061
            r6 = -1
            r0 = r18
            if (r0 == r6) goto L_0x00c2
            int r18 = r18 - r12
            r8 = r9
            r14 = r5
            r5 = r3
            r3 = r14
            r15 = r1
            r1 = r4
            r4 = r15
            goto L_0x0024
        L_0x0061:
            byte r8 = r10[r7]     // Catch:{ IOException -> 0x00bb }
            r8 = r8 & 255(0xff, float:3.57E-43)
            if (r2 == 0) goto L_0x0078
            r13 = 13
            if (r6 != r13) goto L_0x006f
            r13 = 10
            if (r8 != r13) goto L_0x0077
        L_0x006f:
            r13 = 13
            if (r6 == r13) goto L_0x0078
            r6 = 10
            if (r8 != r6) goto L_0x0078
        L_0x0077:
            r4 = 1
        L_0x0078:
            r6 = 13
            if (r8 == r6) goto L_0x0080
            r6 = 10
            if (r8 != r6) goto L_0x008b
        L_0x0080:
            r6 = 0
        L_0x0081:
            boolean r9 = nonascii(r8)     // Catch:{ IOException -> 0x00bb }
            if (r9 == 0) goto L_0x009a
            if (r19 == 0) goto L_0x0093
            r1 = 3
            goto L_0x002b
        L_0x008b:
            int r6 = r9 + 1
            r9 = 998(0x3e6, float:1.398E-42)
            if (r6 <= r9) goto L_0x0081
            r5 = 1
            goto L_0x0081
        L_0x0093:
            int r1 = r1 + 1
        L_0x0095:
            int r7 = r7 + 1
            r9 = r6
            r6 = r8
            goto L_0x0050
        L_0x009a:
            int r3 = r3 + 1
            goto L_0x0095
        L_0x009d:
            r2 = move-exception
            r2 = r3
            r3 = r4
            r4 = r5
        L_0x00a1:
            r5 = r4
            r4 = r3
            r3 = r2
            goto L_0x0026
        L_0x00a5:
            if (r4 != 0) goto L_0x00b3
            if (r1 == 0) goto L_0x00ab
            r1 = 3
            goto L_0x002b
        L_0x00ab:
            if (r3 == 0) goto L_0x00b0
            r1 = 2
            goto L_0x002b
        L_0x00b0:
            r1 = 1
            goto L_0x002b
        L_0x00b3:
            if (r5 <= r4) goto L_0x00b8
            r1 = 2
            goto L_0x002b
        L_0x00b8:
            r1 = 3
            goto L_0x002b
        L_0x00bb:
            r2 = move-exception
            r2 = r5
            r14 = r4
            r4 = r3
            r3 = r1
            r1 = r14
            goto L_0x00a1
        L_0x00c2:
            r8 = r9
            r14 = r5
            r5 = r3
            r3 = r14
            r15 = r1
            r1 = r4
            r4 = r15
            goto L_0x0024
        L_0x00cb:
            r10 = r1
            r8 = r9
            r11 = r3
            r1 = r4
            r3 = r5
            r4 = r6
            r5 = r7
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: javax.mail.internet.MimeUtility.checkAscii(java.io.InputStream, int, boolean):int");
    }

    static final boolean nonascii(int i) {
        return i >= 127 || !(i >= 32 || i == 13 || i == 10 || i == 9);
    }
}
