package javax.mail.internet;

import com.sun.mail.imap.IMAPStore;
import com.sun.mail.util.ASCIIUtility;
import com.sun.mail.util.LineInputStream;
import com.sun.mail.util.LineOutputStream;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.activation.DataSource;
import javax.mail.BodyPart;
import javax.mail.MessageAware;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.MultipartDataSource;

public class MimeMultipart extends Multipart {
    private static boolean bmparse;
    private static boolean ignoreMissingBoundaryParameter;
    private static boolean ignoreMissingEndBoundary;
    private boolean complete;
    protected DataSource ds;
    protected boolean parsed;
    private String preamble;

    static {
        boolean z;
        boolean z2 = false;
        ignoreMissingEndBoundary = true;
        ignoreMissingBoundaryParameter = true;
        bmparse = true;
        try {
            String property = System.getProperty("mail.mime.multipart.ignoremissingendboundary");
            ignoreMissingEndBoundary = property == null || !property.equalsIgnoreCase("false");
            String property2 = System.getProperty("mail.mime.multipart.ignoremissingboundaryparameter");
            if (property2 == null || !property2.equalsIgnoreCase("false")) {
                z = true;
            } else {
                z = false;
            }
            ignoreMissingBoundaryParameter = z;
            String property3 = System.getProperty("mail.mime.multipart.bmparse");
            if (property3 == null || !property3.equalsIgnoreCase("false")) {
                z2 = true;
            }
            bmparse = z2;
        } catch (SecurityException e) {
        }
    }

    public MimeMultipart() {
        this("mixed");
    }

    public MimeMultipart(String str) {
        this.ds = null;
        this.parsed = true;
        this.complete = true;
        this.preamble = null;
        String uniqueBoundaryValue = UniqueValue.getUniqueBoundaryValue();
        ContentType contentType = new ContentType("multipart", str, null);
        contentType.setParameter("boundary", uniqueBoundaryValue);
        this.contentType = contentType.toString();
    }

    public MimeMultipart(DataSource dataSource) throws MessagingException {
        this.ds = null;
        this.parsed = true;
        this.complete = true;
        this.preamble = null;
        if (dataSource instanceof MessageAware) {
            setParent(((MessageAware) dataSource).getMessageContext().getPart());
        }
        if (dataSource instanceof MultipartDataSource) {
            setMultipartDataSource((MultipartDataSource) dataSource);
            return;
        }
        this.parsed = false;
        this.ds = dataSource;
        this.contentType = dataSource.getContentType();
    }

    public synchronized void setSubType(String str) throws MessagingException {
        ContentType contentType = new ContentType(this.contentType);
        contentType.setSubType(str);
        this.contentType = contentType.toString();
    }

    public synchronized int getCount() throws MessagingException {
        parse();
        return super.getCount();
    }

    public synchronized BodyPart getBodyPart(int i) throws MessagingException {
        parse();
        return super.getBodyPart(i);
    }

    public synchronized BodyPart getBodyPart(String str) throws MessagingException {
        MimeBodyPart mimeBodyPart;
        parse();
        int count = getCount();
        int i = 0;
        while (true) {
            if (i < count) {
                mimeBodyPart = (MimeBodyPart) getBodyPart(i);
                String contentID = mimeBodyPart.getContentID();
                if (contentID != null && contentID.equals(str)) {
                    break;
                }
                i++;
            } else {
                mimeBodyPart = null;
                break;
            }
        }
        return mimeBodyPart;
    }

    public boolean removeBodyPart(BodyPart bodyPart) throws MessagingException {
        parse();
        return super.removeBodyPart(bodyPart);
    }

    public void removeBodyPart(int i) throws MessagingException {
        parse();
        super.removeBodyPart(i);
    }

    public synchronized void addBodyPart(BodyPart bodyPart) throws MessagingException {
        parse();
        super.addBodyPart(bodyPart);
    }

    public synchronized void addBodyPart(BodyPart bodyPart, int i) throws MessagingException {
        parse();
        super.addBodyPart(bodyPart, i);
    }

    public synchronized boolean isComplete() throws MessagingException {
        parse();
        return this.complete;
    }

    public synchronized String getPreamble() throws MessagingException {
        parse();
        return this.preamble;
    }

    public synchronized void setPreamble(String str) throws MessagingException {
        this.preamble = str;
    }

    /* access modifiers changed from: protected */
    public void updateHeaders() throws MessagingException {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.parts.size()) {
                ((MimeBodyPart) this.parts.elementAt(i2)).updateHeaders();
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public synchronized void writeTo(OutputStream outputStream) throws IOException, MessagingException {
        parse();
        String str = "--" + new ContentType(this.contentType).getParameter("boundary");
        LineOutputStream lineOutputStream = new LineOutputStream(outputStream);
        if (this.preamble != null) {
            byte[] bytes = ASCIIUtility.getBytes(this.preamble);
            lineOutputStream.write(bytes);
            if (!(bytes.length <= 0 || bytes[bytes.length - 1] == 13 || bytes[bytes.length - 1] == 10)) {
                lineOutputStream.writeln();
            }
        }
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.parts.size()) {
                lineOutputStream.writeln(String.valueOf(str) + "--");
            } else {
                lineOutputStream.writeln(str);
                ((MimeBodyPart) this.parts.elementAt(i2)).writeTo(outputStream);
                lineOutputStream.writeln();
                i = i2 + 1;
            }
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:159:0x0207 A[Catch:{ IOException -> 0x0074, all -> 0x007d }] */
    /* JADX WARNING: Removed duplicated region for block: B:208:0x01f4 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void parse() throws javax.mail.MessagingException {
        /*
            r20 = this;
            monitor-enter(r20)
            r0 = r20
            boolean r1 = r0.parsed     // Catch:{ all -> 0x0011 }
            if (r1 == 0) goto L_0x0009
        L_0x0007:
            monitor-exit(r20)
            return
        L_0x0009:
            boolean r1 = javax.mail.internet.MimeMultipart.bmparse     // Catch:{ all -> 0x0011 }
            if (r1 == 0) goto L_0x0014
            r20.parsebm()     // Catch:{ all -> 0x0011 }
            goto L_0x0007
        L_0x0011:
            r1 = move-exception
            monitor-exit(r20)
            throw r1
        L_0x0014:
            r1 = 0
            r6 = 0
            r3 = 0
            r0 = r20
            javax.activation.DataSource r2 = r0.ds     // Catch:{ Exception -> 0x0082 }
            java.io.InputStream r5 = r2.getInputStream()     // Catch:{ Exception -> 0x0082 }
            boolean r2 = r5 instanceof java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x0082 }
            if (r2 != 0) goto L_0x027b
            boolean r2 = r5 instanceof java.io.BufferedInputStream     // Catch:{ Exception -> 0x0082 }
            if (r2 != 0) goto L_0x027b
            boolean r2 = r5 instanceof javax.mail.internet.SharedInputStream     // Catch:{ Exception -> 0x0082 }
            if (r2 != 0) goto L_0x027b
            java.io.BufferedInputStream r2 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x0082 }
            r2.<init>(r5)     // Catch:{ Exception -> 0x0082 }
        L_0x0032:
            boolean r5 = r2 instanceof javax.mail.internet.SharedInputStream     // Catch:{ all -> 0x0011 }
            if (r5 == 0) goto L_0x0278
            r0 = r2
            javax.mail.internet.SharedInputStream r0 = (javax.mail.internet.SharedInputStream) r0     // Catch:{ all -> 0x0011 }
            r1 = r0
            r14 = r1
        L_0x003b:
            javax.mail.internet.ContentType r1 = new javax.mail.internet.ContentType     // Catch:{ all -> 0x0011 }
            r0 = r20
            java.lang.String r5 = r0.contentType     // Catch:{ all -> 0x0011 }
            r1.<init>(r5)     // Catch:{ all -> 0x0011 }
            r5 = 0
            java.lang.String r8 = "boundary"
            java.lang.String r1 = r1.getParameter(r8)     // Catch:{ all -> 0x0011 }
            if (r1 == 0) goto L_0x008b
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0011 }
            java.lang.String r8 = "--"
            r5.<init>(r8)     // Catch:{ all -> 0x0011 }
            java.lang.StringBuilder r1 = r5.append(r1)     // Catch:{ all -> 0x0011 }
            java.lang.String r5 = r1.toString()     // Catch:{ all -> 0x0011 }
        L_0x005c:
            com.sun.mail.util.LineInputStream r15 = new com.sun.mail.util.LineInputStream     // Catch:{ IOException -> 0x0074 }
            r15.<init>(r2)     // Catch:{ IOException -> 0x0074 }
            r1 = 0
            r8 = 0
            r10 = r1
        L_0x0064:
            java.lang.String r1 = r15.readLine()     // Catch:{ IOException -> 0x0074 }
            if (r1 != 0) goto L_0x0097
        L_0x006a:
            if (r1 != 0) goto L_0x00f2
            javax.mail.MessagingException r1 = new javax.mail.MessagingException     // Catch:{ IOException -> 0x0074 }
            java.lang.String r3 = "Missing start boundary"
            r1.<init>(r3)     // Catch:{ IOException -> 0x0074 }
            throw r1     // Catch:{ IOException -> 0x0074 }
        L_0x0074:
            r1 = move-exception
            javax.mail.MessagingException r3 = new javax.mail.MessagingException     // Catch:{ all -> 0x007d }
            java.lang.String r4 = "IO Error"
            r3.<init>(r4, r1)     // Catch:{ all -> 0x007d }
            throw r3     // Catch:{ all -> 0x007d }
        L_0x007d:
            r1 = move-exception
            r2.close()     // Catch:{ IOException -> 0x0255 }
        L_0x0081:
            throw r1     // Catch:{ all -> 0x0011 }
        L_0x0082:
            r1 = move-exception
            javax.mail.MessagingException r2 = new javax.mail.MessagingException     // Catch:{ all -> 0x0011 }
            java.lang.String r3 = "No inputstream from datasource"
            r2.<init>(r3, r1)     // Catch:{ all -> 0x0011 }
            throw r2     // Catch:{ all -> 0x0011 }
        L_0x008b:
            boolean r1 = javax.mail.internet.MimeMultipart.ignoreMissingBoundaryParameter     // Catch:{ all -> 0x0011 }
            if (r1 != 0) goto L_0x005c
            javax.mail.MessagingException r1 = new javax.mail.MessagingException     // Catch:{ all -> 0x0011 }
            java.lang.String r2 = "Missing boundary parameter"
            r1.<init>(r2)     // Catch:{ all -> 0x0011 }
            throw r1     // Catch:{ all -> 0x0011 }
        L_0x0097:
            int r9 = r1.length()     // Catch:{ IOException -> 0x0074 }
            int r9 = r9 + -1
        L_0x009d:
            if (r9 >= 0) goto L_0x00d4
        L_0x009f:
            r11 = 0
            int r9 = r9 + 1
            java.lang.String r1 = r1.substring(r11, r9)     // Catch:{ IOException -> 0x0074 }
            if (r5 == 0) goto L_0x00e3
            boolean r9 = r1.equals(r5)     // Catch:{ IOException -> 0x0074 }
            if (r9 != 0) goto L_0x006a
        L_0x00ae:
            int r9 = r1.length()     // Catch:{ IOException -> 0x0074 }
            if (r9 <= 0) goto L_0x0064
            if (r8 != 0) goto L_0x00be
            java.lang.String r8 = "line.separator"
            java.lang.String r9 = "\n"
            java.lang.String r8 = java.lang.System.getProperty(r8, r9)     // Catch:{ SecurityException -> 0x00ee }
        L_0x00be:
            if (r10 != 0) goto L_0x0275
            java.lang.StringBuffer r9 = new java.lang.StringBuffer     // Catch:{ IOException -> 0x0074 }
            int r10 = r1.length()     // Catch:{ IOException -> 0x0074 }
            int r10 = r10 + 2
            r9.<init>(r10)     // Catch:{ IOException -> 0x0074 }
        L_0x00cb:
            java.lang.StringBuffer r1 = r9.append(r1)     // Catch:{ IOException -> 0x0074 }
            r1.append(r8)     // Catch:{ IOException -> 0x0074 }
            r10 = r9
            goto L_0x0064
        L_0x00d4:
            char r11 = r1.charAt(r9)     // Catch:{ IOException -> 0x0074 }
            r12 = 32
            if (r11 == r12) goto L_0x00e0
            r12 = 9
            if (r11 != r12) goto L_0x009f
        L_0x00e0:
            int r9 = r9 + -1
            goto L_0x009d
        L_0x00e3:
            java.lang.String r9 = "--"
            boolean r9 = r1.startsWith(r9)     // Catch:{ IOException -> 0x0074 }
            if (r9 == 0) goto L_0x00ae
            r5 = r1
            goto L_0x006a
        L_0x00ee:
            r8 = move-exception
            java.lang.String r8 = "\n"
            goto L_0x00be
        L_0x00f2:
            if (r10 == 0) goto L_0x00fc
            java.lang.String r1 = r10.toString()     // Catch:{ IOException -> 0x0074 }
            r0 = r20
            r0.preamble = r1     // Catch:{ IOException -> 0x0074 }
        L_0x00fc:
            byte[] r16 = com.sun.mail.util.ASCIIUtility.getBytes(r5)     // Catch:{ IOException -> 0x0074 }
            r0 = r16
            int r0 = r0.length     // Catch:{ IOException -> 0x0074 }
            r17 = r0
            r5 = 0
        L_0x0106:
            if (r5 == 0) goto L_0x0112
        L_0x0108:
            r2.close()     // Catch:{ IOException -> 0x0258 }
        L_0x010b:
            r1 = 1
            r0 = r20
            r0.parsed = r1     // Catch:{ all -> 0x0011 }
            goto L_0x0007
        L_0x0112:
            r1 = 0
            if (r14 == 0) goto L_0x0139
            long r6 = r14.getPosition()     // Catch:{ IOException -> 0x0074 }
        L_0x0119:
            java.lang.String r8 = r15.readLine()     // Catch:{ IOException -> 0x0074 }
            if (r8 == 0) goto L_0x0125
            int r9 = r8.length()     // Catch:{ IOException -> 0x0074 }
            if (r9 > 0) goto L_0x0119
        L_0x0125:
            if (r8 != 0) goto L_0x0271
            boolean r1 = javax.mail.internet.MimeMultipart.ignoreMissingEndBoundary     // Catch:{ IOException -> 0x0074 }
            if (r1 != 0) goto L_0x0133
            javax.mail.MessagingException r1 = new javax.mail.MessagingException     // Catch:{ IOException -> 0x0074 }
            java.lang.String r3 = "missing multipart end boundary"
            r1.<init>(r3)     // Catch:{ IOException -> 0x0074 }
            throw r1     // Catch:{ IOException -> 0x0074 }
        L_0x0133:
            r1 = 0
            r0 = r20
            r0.complete = r1     // Catch:{ IOException -> 0x0074 }
            goto L_0x0108
        L_0x0139:
            r0 = r20
            javax.mail.internet.InternetHeaders r1 = r0.createInternetHeaders(r2)     // Catch:{ IOException -> 0x0074 }
            r11 = r1
            r12 = r6
        L_0x0141:
            boolean r1 = r2.markSupported()     // Catch:{ IOException -> 0x0074 }
            if (r1 != 0) goto L_0x014f
            javax.mail.MessagingException r1 = new javax.mail.MessagingException     // Catch:{ IOException -> 0x0074 }
            java.lang.String r3 = "Stream doesn't support mark"
            r1.<init>(r3)     // Catch:{ IOException -> 0x0074 }
            throw r1     // Catch:{ IOException -> 0x0074 }
        L_0x014f:
            r1 = 0
            if (r14 != 0) goto L_0x01a2
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x0074 }
            r1.<init>()     // Catch:{ IOException -> 0x0074 }
        L_0x0157:
            r7 = 1
            r6 = -1
            r8 = -1
            r18 = r8
            r8 = r7
            r19 = r6
            r6 = r3
            r3 = r18
            r4 = r19
        L_0x0164:
            if (r8 == 0) goto L_0x026b
            int r8 = r17 + 4
            int r8 = r8 + 1000
            r2.mark(r8)     // Catch:{ IOException -> 0x0074 }
            r8 = 0
        L_0x016e:
            r0 = r17
            if (r8 < r0) goto L_0x01a7
        L_0x0172:
            r0 = r17
            if (r8 != r0) goto L_0x01db
            int r8 = r2.read()     // Catch:{ IOException -> 0x0074 }
            r9 = 45
            if (r8 != r9) goto L_0x01b8
            int r9 = r2.read()     // Catch:{ IOException -> 0x0074 }
            r10 = 45
            if (r9 != r10) goto L_0x01b8
            r3 = 1
            r0 = r20
            r0.complete = r3     // Catch:{ IOException -> 0x0074 }
            r3 = 1
        L_0x018c:
            if (r14 == 0) goto L_0x0249
            java.io.InputStream r1 = r14.newStream(r12, r6)     // Catch:{ IOException -> 0x0074 }
            r0 = r20
            javax.mail.internet.MimeBodyPart r1 = r0.createMimeBodyPart(r1)     // Catch:{ IOException -> 0x0074 }
        L_0x0198:
            r0 = r20
            super.addBodyPart(r1)     // Catch:{ IOException -> 0x0074 }
            r5 = r3
            r3 = r6
            r6 = r12
            goto L_0x0106
        L_0x01a2:
            long r3 = r14.getPosition()     // Catch:{ IOException -> 0x0074 }
            goto L_0x0157
        L_0x01a7:
            int r9 = r2.read()     // Catch:{ IOException -> 0x0074 }
            byte r10 = r16[r8]     // Catch:{ IOException -> 0x0074 }
            r10 = r10 & 255(0xff, float:3.57E-43)
            if (r9 != r10) goto L_0x0172
            int r8 = r8 + 1
            goto L_0x016e
        L_0x01b4:
            int r8 = r2.read()     // Catch:{ IOException -> 0x0074 }
        L_0x01b8:
            r9 = 32
            if (r8 == r9) goto L_0x01b4
            r9 = 9
            if (r8 == r9) goto L_0x01b4
            r9 = 10
            if (r8 != r9) goto L_0x01c6
            r3 = r5
            goto L_0x018c
        L_0x01c6:
            r9 = 13
            if (r8 != r9) goto L_0x01db
            r3 = 1
            r2.mark(r3)     // Catch:{ IOException -> 0x0074 }
            int r3 = r2.read()     // Catch:{ IOException -> 0x0074 }
            r4 = 10
            if (r3 == r4) goto L_0x026e
            r2.reset()     // Catch:{ IOException -> 0x0074 }
            r3 = r5
            goto L_0x018c
        L_0x01db:
            r2.reset()     // Catch:{ IOException -> 0x0074 }
            if (r1 == 0) goto L_0x026b
            r8 = -1
            if (r4 == r8) goto L_0x026b
            r1.write(r4)     // Catch:{ IOException -> 0x0074 }
            r4 = -1
            if (r3 == r4) goto L_0x01ec
            r1.write(r3)     // Catch:{ IOException -> 0x0074 }
        L_0x01ec:
            r3 = -1
            r8 = r3
        L_0x01ee:
            int r9 = r2.read()     // Catch:{ IOException -> 0x0074 }
            if (r9 >= 0) goto L_0x0207
            boolean r3 = javax.mail.internet.MimeMultipart.ignoreMissingEndBoundary     // Catch:{ IOException -> 0x0074 }
            if (r3 != 0) goto L_0x0200
            javax.mail.MessagingException r1 = new javax.mail.MessagingException     // Catch:{ IOException -> 0x0074 }
            java.lang.String r3 = "missing multipart end boundary"
            r1.<init>(r3)     // Catch:{ IOException -> 0x0074 }
            throw r1     // Catch:{ IOException -> 0x0074 }
        L_0x0200:
            r3 = 0
            r0 = r20
            r0.complete = r3     // Catch:{ IOException -> 0x0074 }
            r3 = 1
            goto L_0x018c
        L_0x0207:
            r4 = 13
            if (r9 == r4) goto L_0x020f
            r4 = 10
            if (r9 != r4) goto L_0x023b
        L_0x020f:
            r10 = 1
            if (r14 == 0) goto L_0x0269
            long r3 = r14.getPosition()     // Catch:{ IOException -> 0x0074 }
            r6 = 1
            long r3 = r3 - r6
        L_0x0219:
            r6 = 13
            if (r9 != r6) goto L_0x0263
            r6 = 1
            r2.mark(r6)     // Catch:{ IOException -> 0x0074 }
            int r6 = r2.read()     // Catch:{ IOException -> 0x0074 }
            r7 = 10
            if (r6 != r7) goto L_0x0232
            r8 = r10
            r18 = r6
            r6 = r3
            r4 = r9
            r3 = r18
            goto L_0x0164
        L_0x0232:
            r2.reset()     // Catch:{ IOException -> 0x0074 }
            r6 = r3
            r4 = r9
            r3 = r8
            r8 = r10
            goto L_0x0164
        L_0x023b:
            r4 = 0
            if (r1 == 0) goto L_0x025b
            r1.write(r9)     // Catch:{ IOException -> 0x0074 }
            r18 = r8
            r8 = r4
            r4 = r3
            r3 = r18
            goto L_0x0164
        L_0x0249:
            byte[] r1 = r1.toByteArray()     // Catch:{ IOException -> 0x0074 }
            r0 = r20
            javax.mail.internet.MimeBodyPart r1 = r0.createMimeBodyPart(r11, r1)     // Catch:{ IOException -> 0x0074 }
            goto L_0x0198
        L_0x0255:
            r2 = move-exception
            goto L_0x0081
        L_0x0258:
            r1 = move-exception
            goto L_0x010b
        L_0x025b:
            r18 = r8
            r8 = r4
            r4 = r3
            r3 = r18
            goto L_0x0164
        L_0x0263:
            r6 = r3
            r4 = r9
            r3 = r8
            r8 = r10
            goto L_0x0164
        L_0x0269:
            r3 = r6
            goto L_0x0219
        L_0x026b:
            r8 = r3
            r3 = r4
            goto L_0x01ee
        L_0x026e:
            r3 = r5
            goto L_0x018c
        L_0x0271:
            r11 = r1
            r12 = r6
            goto L_0x0141
        L_0x0275:
            r9 = r10
            goto L_0x00cb
        L_0x0278:
            r14 = r1
            goto L_0x003b
        L_0x027b:
            r2 = r5
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: javax.mail.internet.MimeMultipart.parse():void");
    }

    private synchronized void parsebm() throws MessagingException {
        InputStream inputStream;
        SharedInputStream sharedInputStream;
        String readLine;
        StringBuffer stringBuffer;
        long j;
        InternetHeaders internetHeaders;
        ByteArrayOutputStream byteArrayOutputStream;
        int i;
        int readFully;
        long j2;
        boolean z;
        long j3;
        byte[] bArr;
        byte b;
        MimeBodyPart createMimeBodyPart;
        String readLine2;
        if (!this.parsed) {
            long j4 = 0;
            long j5 = 0;
            try {
                InputStream inputStream2 = this.ds.getInputStream();
                if ((inputStream2 instanceof ByteArrayInputStream) || (inputStream2 instanceof BufferedInputStream) || (inputStream2 instanceof SharedInputStream)) {
                    inputStream = inputStream2;
                } else {
                    inputStream = new BufferedInputStream(inputStream2);
                }
                if (inputStream instanceof SharedInputStream) {
                    sharedInputStream = (SharedInputStream) inputStream;
                } else {
                    sharedInputStream = null;
                }
                String str = null;
                String parameter = new ContentType(this.contentType).getParameter("boundary");
                if (parameter != null) {
                    str = "--" + parameter;
                } else if (!ignoreMissingBoundaryParameter) {
                    throw new MessagingException("Missing boundary parameter");
                }
                try {
                    LineInputStream lineInputStream = new LineInputStream(inputStream);
                    String str2 = null;
                    StringBuffer stringBuffer2 = null;
                    while (true) {
                        readLine = lineInputStream.readLine();
                        if (readLine == null) {
                            break;
                        }
                        int length = readLine.length() - 1;
                        while (length >= 0) {
                            char charAt = readLine.charAt(length);
                            if (charAt != ' ' && charAt != 9) {
                                break;
                            }
                            length--;
                        }
                        readLine = readLine.substring(0, length + 1);
                        if (str == null) {
                            if (readLine.startsWith("--")) {
                                str = readLine;
                                break;
                            }
                        } else if (readLine.equals(str)) {
                            break;
                        }
                        if (readLine.length() > 0) {
                            if (str2 == null) {
                                try {
                                    str2 = System.getProperty("line.separator", "\n");
                                } catch (SecurityException e) {
                                    str2 = "\n";
                                }
                            }
                            if (stringBuffer2 == null) {
                                stringBuffer = new StringBuffer(readLine.length() + 2);
                            } else {
                                stringBuffer = stringBuffer2;
                            }
                            stringBuffer.append(readLine).append(str2);
                            stringBuffer2 = stringBuffer;
                        }
                    }
                    if (readLine != null) {
                        if (stringBuffer2 != null) {
                            this.preamble = stringBuffer2.toString();
                        }
                        byte[] bytes = ASCIIUtility.getBytes(str);
                        int length2 = bytes.length;
                        int[] iArr = new int[256];
                        for (int i2 = 0; i2 < length2; i2++) {
                            iArr[bytes[i2]] = i2 + 1;
                        }
                        int[] iArr2 = new int[length2];
                        for (int i3 = length2; i3 > 0; i3--) {
                            int i4 = length2 - 1;
                            while (true) {
                                if (i4 >= i3) {
                                    if (bytes[i4] != bytes[i4 - i3]) {
                                        break;
                                    }
                                    iArr2[i4 - 1] = i3;
                                    i4--;
                                } else {
                                    while (i4 > 0) {
                                        i4--;
                                        iArr2[i4] = i3;
                                    }
                                }
                            }
                        }
                        iArr2[length2 - 1] = 1;
                        boolean z2 = false;
                        while (true) {
                            if (!z2) {
                                if (sharedInputStream != null) {
                                    long position = sharedInputStream.getPosition();
                                    do {
                                        readLine2 = lineInputStream.readLine();
                                        if (readLine2 == null) {
                                            break;
                                        }
                                    } while (readLine2.length() > 0);
                                    if (readLine2 != null) {
                                        internetHeaders = null;
                                        j = position;
                                    } else if (!ignoreMissingEndBoundary) {
                                        throw new MessagingException("missing multipart end boundary");
                                    } else {
                                        this.complete = false;
                                    }
                                } else {
                                    internetHeaders = createInternetHeaders(inputStream);
                                    j = j4;
                                }
                                if (!inputStream.markSupported()) {
                                    throw new MessagingException("Stream doesn't support mark");
                                }
                                if (sharedInputStream == null) {
                                    byteArrayOutputStream = new ByteArrayOutputStream();
                                } else {
                                    j5 = sharedInputStream.getPosition();
                                    byteArrayOutputStream = null;
                                }
                                byte[] bArr2 = new byte[length2];
                                byte[] bArr3 = new byte[length2];
                                int i5 = 0;
                                long j6 = j5;
                                boolean z3 = true;
                                while (true) {
                                    inputStream.mark(length2 + 4 + IMAPStore.RESPONSE);
                                    i = 0;
                                    readFully = readFully(inputStream, bArr2, 0, length2);
                                    if (readFully >= length2) {
                                        int i6 = length2 - 1;
                                        while (i6 >= 0 && bArr2[i6] == bytes[i6]) {
                                            i6--;
                                        }
                                        if (i6 < 0) {
                                            i = 0;
                                            if (!z3 && ((b = bArr3[i5 - 1]) == 13 || b == 10)) {
                                                i = 1;
                                                if (b == 10 && i5 >= 2 && bArr3[i5 - 2] == 13) {
                                                    i = 2;
                                                }
                                            }
                                            if (z3 || i > 0) {
                                                if (sharedInputStream != null) {
                                                    j6 = (sharedInputStream.getPosition() - ((long) length2)) - ((long) i);
                                                }
                                                int read = inputStream.read();
                                                if (read == 45 && inputStream.read() == 45) {
                                                    this.complete = true;
                                                    j2 = j6;
                                                    z = true;
                                                    break;
                                                }
                                                while (true) {
                                                    if (read != 32 && read != 9) {
                                                        break;
                                                    }
                                                    read = inputStream.read();
                                                }
                                                if (read == 10) {
                                                    boolean z4 = z2;
                                                    j2 = j6;
                                                    z = z4;
                                                    break;
                                                } else if (read == 13) {
                                                    inputStream.mark(1);
                                                    if (inputStream.read() != 10) {
                                                        inputStream.reset();
                                                        boolean z5 = z2;
                                                        j2 = j6;
                                                        z = z5;
                                                    } else {
                                                        boolean z6 = z2;
                                                        j2 = j6;
                                                        z = z6;
                                                    }
                                                }
                                            }
                                            i6 = 0;
                                            j3 = j6;
                                        } else {
                                            j3 = j6;
                                        }
                                        int max = Math.max((i6 + 1) - iArr[bArr2[i6] & Byte.MAX_VALUE], iArr2[i6]);
                                        if (max < 2) {
                                            if (sharedInputStream == null && i5 > 1) {
                                                byteArrayOutputStream.write(bArr3, 0, i5 - 1);
                                            }
                                            inputStream.reset();
                                            skipFully(inputStream, 1);
                                            if (i5 >= 1) {
                                                bArr3[0] = bArr3[i5 - 1];
                                                bArr3[1] = bArr2[0];
                                                max = 2;
                                                bArr = bArr3;
                                            } else {
                                                bArr3[0] = bArr2[0];
                                                max = 1;
                                                bArr = bArr3;
                                            }
                                        } else {
                                            if (i5 > 0 && sharedInputStream == null) {
                                                byteArrayOutputStream.write(bArr3, 0, i5);
                                            }
                                            inputStream.reset();
                                            skipFully(inputStream, (long) max);
                                            bArr = bArr2;
                                            bArr2 = bArr3;
                                        }
                                        i5 = max;
                                        bArr3 = bArr;
                                        j6 = j3;
                                        z3 = false;
                                    } else if (!ignoreMissingEndBoundary) {
                                        throw new MessagingException("missing multipart end boundary");
                                    } else {
                                        if (sharedInputStream != null) {
                                            j2 = sharedInputStream.getPosition();
                                        } else {
                                            j2 = j6;
                                        }
                                        this.complete = false;
                                        z = true;
                                    }
                                }
                                if (sharedInputStream != null) {
                                    createMimeBodyPart = createMimeBodyPart(sharedInputStream.newStream(j, j2));
                                } else {
                                    if (i5 - i > 0) {
                                        byteArrayOutputStream.write(bArr3, 0, i5 - i);
                                    }
                                    if (!this.complete && readFully > 0) {
                                        byteArrayOutputStream.write(bArr2, 0, readFully);
                                    }
                                    createMimeBodyPart = createMimeBodyPart(internetHeaders, byteArrayOutputStream.toByteArray());
                                }
                                super.addBodyPart(createMimeBodyPart);
                                boolean z7 = z;
                                j4 = j;
                                j5 = j2;
                                z2 = z7;
                            }
                        }
                        try {
                            inputStream.close();
                        } catch (IOException e2) {
                        }
                        this.parsed = true;
                        break;
                    } else {
                        throw new MessagingException("Missing start boundary");
                    }
                } catch (IOException e3) {
                    throw new MessagingException("IO Error", e3);
                } catch (Throwable th) {
                    try {
                        inputStream.close();
                    } catch (IOException e4) {
                    }
                    throw th;
                }
            } catch (Exception e5) {
                throw new MessagingException("No inputstream from datasource", e5);
            }
        }
        return;
    }

    private static int readFully(InputStream inputStream, byte[] bArr, int i, int i2) throws IOException {
        int i3 = 0;
        if (i2 == 0) {
            return 0;
        }
        while (i2 > 0) {
            int read = inputStream.read(bArr, i, i2);
            if (read <= 0) {
                break;
            }
            i += read;
            i3 += read;
            i2 -= read;
        }
        if (i3 <= 0) {
            return -1;
        }
        return i3;
    }

    private void skipFully(InputStream inputStream, long j) throws IOException {
        while (j > 0) {
            long skip = inputStream.skip(j);
            if (skip <= 0) {
                throw new EOFException("can't skip");
            }
            j -= skip;
        }
    }

    /* access modifiers changed from: protected */
    public InternetHeaders createInternetHeaders(InputStream inputStream) throws MessagingException {
        return new InternetHeaders(inputStream);
    }

    /* access modifiers changed from: protected */
    public MimeBodyPart createMimeBodyPart(InternetHeaders internetHeaders, byte[] bArr) throws MessagingException {
        return new MimeBodyPart(internetHeaders, bArr);
    }

    /* access modifiers changed from: protected */
    public MimeBodyPart createMimeBodyPart(InputStream inputStream) throws MessagingException {
        return new MimeBodyPart(inputStream);
    }
}
