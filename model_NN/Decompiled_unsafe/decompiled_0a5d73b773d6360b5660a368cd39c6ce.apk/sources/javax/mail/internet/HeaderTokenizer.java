package javax.mail.internet;

public class HeaderTokenizer {
    private static final Token EOFToken = new Token(-4, null);
    public static final String MIME = "()<>@,;:\\\"\t []/?=";
    public static final String RFC822 = "()<>@,;:\\\"\t .[]";
    private int currentPos;
    private String delimiters;
    private int maxPos;
    private int nextPos;
    private int peekPos;
    private boolean skipComments;
    private String string;

    public static class Token {
        public static final int ATOM = -1;
        public static final int COMMENT = -3;
        public static final int EOF = -4;
        public static final int QUOTEDSTRING = -2;
        private int type;
        private String value;

        public Token(int i, String str) {
            this.type = i;
            this.value = str;
        }

        public int getType() {
            return this.type;
        }

        public String getValue() {
            return this.value;
        }
    }

    public HeaderTokenizer(String str, String str2, boolean z) {
        this.string = str == null ? "" : str;
        this.skipComments = z;
        this.delimiters = str2;
        this.peekPos = 0;
        this.nextPos = 0;
        this.currentPos = 0;
        this.maxPos = this.string.length();
    }

    public HeaderTokenizer(String str, String str2) {
        this(str, str2, true);
    }

    public HeaderTokenizer(String str) {
        this(str, RFC822);
    }

    public Token next() throws ParseException {
        this.currentPos = this.nextPos;
        Token next = getNext();
        int i = this.currentPos;
        this.peekPos = i;
        this.nextPos = i;
        return next;
    }

    public Token peek() throws ParseException {
        this.currentPos = this.peekPos;
        Token next = getNext();
        this.peekPos = this.currentPos;
        return next;
    }

    public String getRemainder() {
        return this.string.substring(this.nextPos);
    }

    private Token getNext() throws ParseException {
        String substring;
        String substring2;
        if (this.currentPos >= this.maxPos) {
            return EOFToken;
        }
        if (skipWhiteSpace() == -4) {
            return EOFToken;
        }
        boolean z = false;
        char charAt = this.string.charAt(this.currentPos);
        while (charAt == '(') {
            int i = this.currentPos + 1;
            this.currentPos = i;
            boolean z2 = z;
            int i2 = 1;
            while (i2 > 0 && this.currentPos < this.maxPos) {
                char charAt2 = this.string.charAt(this.currentPos);
                if (charAt2 == '\\') {
                    this.currentPos++;
                    z2 = true;
                } else if (charAt2 == 13) {
                    z2 = true;
                } else if (charAt2 == '(') {
                    i2++;
                } else if (charAt2 == ')') {
                    i2--;
                }
                this.currentPos++;
            }
            if (i2 != 0) {
                throw new ParseException("Unbalanced comments");
            } else if (!this.skipComments) {
                if (z2) {
                    substring2 = filterToken(this.string, i, this.currentPos - 1);
                } else {
                    substring2 = this.string.substring(i, this.currentPos - 1);
                }
                return new Token(-3, substring2);
            } else if (skipWhiteSpace() == -4) {
                return EOFToken;
            } else {
                boolean z3 = z2;
                charAt = this.string.charAt(this.currentPos);
                z = z3;
            }
        }
        if (charAt == '\"') {
            int i3 = this.currentPos + 1;
            this.currentPos = i3;
            while (this.currentPos < this.maxPos) {
                char charAt3 = this.string.charAt(this.currentPos);
                if (charAt3 == '\\') {
                    this.currentPos++;
                    z = true;
                } else if (charAt3 == 13) {
                    z = true;
                } else if (charAt3 == '\"') {
                    this.currentPos++;
                    if (z) {
                        substring = filterToken(this.string, i3, this.currentPos - 1);
                    } else {
                        substring = this.string.substring(i3, this.currentPos - 1);
                    }
                    return new Token(-2, substring);
                }
                this.currentPos++;
            }
            throw new ParseException("Unbalanced quoted string");
        } else if (charAt < ' ' || charAt >= 127 || this.delimiters.indexOf(charAt) >= 0) {
            this.currentPos++;
            return new Token(charAt, new String(new char[]{charAt}));
        } else {
            int i4 = this.currentPos;
            while (this.currentPos < this.maxPos && (r0 = this.string.charAt(this.currentPos)) >= ' ' && r0 < 127 && r0 != '(' && r0 != ' ' && r0 != '\"' && this.delimiters.indexOf(r0) < 0) {
                this.currentPos++;
            }
            return new Token(-1, this.string.substring(i4, this.currentPos));
        }
    }

    private int skipWhiteSpace() {
        while (this.currentPos < this.maxPos) {
            char charAt = this.string.charAt(this.currentPos);
            if (charAt != ' ' && charAt != 9 && charAt != 13 && charAt != 10) {
                return this.currentPos;
            }
            this.currentPos++;
        }
        return -4;
    }

    private static String filterToken(String str, int i, int i2) {
        boolean z;
        boolean z2;
        StringBuffer stringBuffer = new StringBuffer();
        boolean z3 = false;
        boolean z4 = false;
        while (i < i2) {
            char charAt = str.charAt(i);
            if (charAt == 10 && z3) {
                z2 = z4;
                z = false;
            } else if (z4) {
                stringBuffer.append(charAt);
                z = false;
                z2 = false;
            } else if (charAt == '\\') {
                z = false;
                z2 = true;
            } else if (charAt == 13) {
                z2 = z4;
                z = true;
            } else {
                stringBuffer.append(charAt);
                z2 = z4;
                z = false;
            }
            i++;
            boolean z5 = z;
            z4 = z2;
            z3 = z5;
        }
        return stringBuffer.toString();
    }
}
