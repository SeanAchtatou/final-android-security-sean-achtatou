package javax.mail.internet;

import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Locale;
import javax.mail.Address;
import javax.mail.Session;

public class InternetAddress extends Address implements Cloneable {
    private static final String rfc822phrase = HeaderTokenizer.RFC822.replace(' ', 0).replace(9, 0);
    private static final long serialVersionUID = -7507595530758302903L;
    private static final String specialsNoDot = "()<>,;:\\\"[]@";
    private static final String specialsNoDotNoAt = "()<>,;:\\\"[]";
    protected String address;
    protected String encodedPersonal;
    protected String personal;

    public InternetAddress() {
    }

    public InternetAddress(String str) throws AddressException {
        InternetAddress[] parse = parse(str, true);
        if (parse.length != 1) {
            throw new AddressException("Illegal address", str);
        }
        this.address = parse[0].address;
        this.personal = parse[0].personal;
        this.encodedPersonal = parse[0].encodedPersonal;
    }

    public InternetAddress(String str, boolean z) throws AddressException {
        this(str);
        if (z) {
            checkAddress(this.address, true, true);
        }
    }

    public InternetAddress(String str, String str2) throws UnsupportedEncodingException {
        this(str, str2, null);
    }

    public InternetAddress(String str, String str2, String str3) throws UnsupportedEncodingException {
        this.address = str;
        setPersonal(str2, str3);
    }

    public Object clone() {
        try {
            return (InternetAddress) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    public String getType() {
        return "rfc822";
    }

    public void setAddress(String str) {
        this.address = str;
    }

    public void setPersonal(String str, String str2) throws UnsupportedEncodingException {
        this.personal = str;
        if (str != null) {
            this.encodedPersonal = MimeUtility.encodeWord(str, str2, null);
        } else {
            this.encodedPersonal = null;
        }
    }

    public void setPersonal(String str) throws UnsupportedEncodingException {
        this.personal = str;
        if (str != null) {
            this.encodedPersonal = MimeUtility.encodeWord(str);
        } else {
            this.encodedPersonal = null;
        }
    }

    public String getAddress() {
        return this.address;
    }

    public String getPersonal() {
        if (this.personal != null) {
            return this.personal;
        }
        if (this.encodedPersonal == null) {
            return null;
        }
        try {
            this.personal = MimeUtility.decodeText(this.encodedPersonal);
            return this.personal;
        } catch (Exception e) {
            return this.encodedPersonal;
        }
    }

    public String toString() {
        if (this.encodedPersonal == null && this.personal != null) {
            try {
                this.encodedPersonal = MimeUtility.encodeWord(this.personal);
            } catch (UnsupportedEncodingException e) {
            }
        }
        if (this.encodedPersonal != null) {
            return String.valueOf(quotePhrase(this.encodedPersonal)) + " <" + this.address + ">";
        }
        if (isGroup() || isSimple()) {
            return this.address;
        }
        return "<" + this.address + ">";
    }

    public String toUnicodeString() {
        String personal2 = getPersonal();
        if (personal2 != null) {
            return String.valueOf(quotePhrase(personal2)) + " <" + this.address + ">";
        }
        if (isGroup() || isSimple()) {
            return this.address;
        }
        return "<" + this.address + ">";
    }

    private static String quotePhrase(String str) {
        int length = str.length();
        boolean z = false;
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if (charAt == '\"' || charAt == '\\') {
                StringBuffer stringBuffer = new StringBuffer(length + 3);
                stringBuffer.append('\"');
                for (int i2 = 0; i2 < length; i2++) {
                    char charAt2 = str.charAt(i2);
                    if (charAt2 == '\"' || charAt2 == '\\') {
                        stringBuffer.append('\\');
                    }
                    stringBuffer.append(charAt2);
                }
                stringBuffer.append('\"');
                return stringBuffer.toString();
            }
            if ((charAt < ' ' && charAt != 13 && charAt != 10 && charAt != 9) || charAt >= 127 || rfc822phrase.indexOf(charAt) >= 0) {
                z = true;
            }
        }
        if (!z) {
            return str;
        }
        StringBuffer stringBuffer2 = new StringBuffer(length + 2);
        stringBuffer2.append('\"').append(str).append('\"');
        return stringBuffer2.toString();
    }

    private static String unquote(String str) {
        int i;
        char c;
        if (!str.startsWith("\"") || !str.endsWith("\"")) {
            return str;
        }
        String substring = str.substring(1, str.length() - 1);
        if (substring.indexOf(92) < 0) {
            return substring;
        }
        StringBuffer stringBuffer = new StringBuffer(substring.length());
        int i2 = 0;
        while (i2 < substring.length()) {
            char charAt = substring.charAt(i2);
            if (charAt != '\\' || i2 >= substring.length() - 1) {
                char c2 = charAt;
                i = i2;
                c = c2;
            } else {
                i = i2 + 1;
                c = substring.charAt(i);
            }
            stringBuffer.append(c);
            i2 = i + 1;
        }
        return stringBuffer.toString();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof InternetAddress)) {
            return false;
        }
        String address2 = ((InternetAddress) obj).getAddress();
        if (address2 == this.address) {
            return true;
        }
        if (this.address == null || !this.address.equalsIgnoreCase(address2)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        if (this.address == null) {
            return 0;
        }
        return this.address.toLowerCase(Locale.ENGLISH).hashCode();
    }

    public static String toString(Address[] addressArr) {
        return toString(addressArr, 0);
    }

    public static String toString(Address[] addressArr, int i) {
        if (addressArr == null || addressArr.length == 0) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (int i2 = 0; i2 < addressArr.length; i2++) {
            if (i2 != 0) {
                stringBuffer.append(", ");
                i += 2;
            }
            String address2 = addressArr[i2].toString();
            if (lengthOfFirstSegment(address2) + i > 76) {
                stringBuffer.append("\r\n\t");
                i = 8;
            }
            stringBuffer.append(address2);
            i = lengthOfLastSegment(address2, i);
        }
        return stringBuffer.toString();
    }

    private static int lengthOfFirstSegment(String str) {
        int indexOf = str.indexOf("\r\n");
        return indexOf != -1 ? indexOf : str.length();
    }

    private static int lengthOfLastSegment(String str, int i) {
        int lastIndexOf = str.lastIndexOf("\r\n");
        if (lastIndexOf != -1) {
            return (str.length() - lastIndexOf) - 2;
        }
        return str.length() + i;
    }

    public static InternetAddress getLocalAddress(Session session) {
        String property;
        String str;
        String str2;
        InetAddress localHost;
        String str3;
        if (session == null) {
            try {
                str2 = System.getProperty("user.name");
                str = InetAddress.getLocalHost().getHostName();
                property = null;
            } catch (SecurityException | UnknownHostException | AddressException e) {
                return null;
            }
        } else {
            property = session.getProperty("mail.from");
            if (property == null) {
                str2 = session.getProperty("mail.user");
                if (str2 == null || str2.length() == 0) {
                    str2 = session.getProperty("user.name");
                }
                if (str2 == null || str2.length() == 0) {
                    str2 = System.getProperty("user.name");
                }
                str = session.getProperty("mail.host");
                if ((str == null || str.length() == 0) && (localHost = InetAddress.getLocalHost()) != null) {
                    str = localHost.getHostName();
                }
            } else {
                str = null;
                str2 = null;
            }
        }
        if (property != null || str2 == null || str2.length() == 0 || str == null || str.length() == 0) {
            str3 = property;
        } else {
            str3 = String.valueOf(str2) + "@" + str;
        }
        if (str3 != null) {
            return new InternetAddress(str3);
        }
        return null;
    }

    public static InternetAddress[] parse(String str) throws AddressException {
        return parse(str, true);
    }

    public static InternetAddress[] parse(String str, boolean z) throws AddressException {
        return parse(str, z, false);
    }

    public static InternetAddress[] parseHeader(String str, boolean z) throws AddressException {
        return parse(str, z, true);
    }

    /* JADX WARNING: Removed duplicated region for block: B:151:0x011c A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:152:0x0138 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:158:0x00d5 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x00fd  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static javax.mail.internet.InternetAddress[] parse(java.lang.String r14, boolean r15, boolean r16) throws javax.mail.internet.AddressException {
        /*
            r0 = -1
            r3 = -1
            int r9 = r14.length()
            r7 = 0
            r6 = 0
            r5 = 0
            java.util.Vector r10 = new java.util.Vector
            r10.<init>()
            r2 = -1
            r1 = 0
            r4 = r2
        L_0x0011:
            if (r1 < r9) goto L_0x0053
            if (r4 < 0) goto L_0x0049
            r7 = -1
            if (r2 != r7) goto L_0x0244
        L_0x0018:
            java.lang.String r1 = r14.substring(r4, r1)
            java.lang.String r1 = r1.trim()
            if (r5 != 0) goto L_0x0026
            if (r15 != 0) goto L_0x0026
            if (r16 == 0) goto L_0x0224
        L_0x0026:
            if (r15 != 0) goto L_0x002a
            if (r16 != 0) goto L_0x002e
        L_0x002a:
            r2 = 0
            checkAddress(r1, r6, r2)
        L_0x002e:
            javax.mail.internet.InternetAddress r2 = new javax.mail.internet.InternetAddress
            r2.<init>()
            r2.setAddress(r1)
            if (r0 < 0) goto L_0x0046
            java.lang.String r0 = r14.substring(r0, r3)
            java.lang.String r0 = r0.trim()
            java.lang.String r0 = unquote(r0)
            r2.encodedPersonal = r0
        L_0x0046:
            r10.addElement(r2)
        L_0x0049:
            int r0 = r10.size()
            javax.mail.internet.InternetAddress[] r0 = new javax.mail.internet.InternetAddress[r0]
            r10.copyInto(r0)
            return r0
        L_0x0053:
            char r8 = r14.charAt(r1)
            switch(r8) {
                case 9: goto L_0x021a;
                case 10: goto L_0x021a;
                case 13: goto L_0x021a;
                case 32: goto L_0x021a;
                case 34: goto L_0x0111;
                case 40: goto L_0x0070;
                case 41: goto L_0x00ae;
                case 44: goto L_0x014d;
                case 58: goto L_0x01ce;
                case 59: goto L_0x01e7;
                case 60: goto L_0x00b6;
                case 62: goto L_0x0109;
                case 91: goto L_0x0131;
                default: goto L_0x005a;
            }
        L_0x005a:
            r8 = -1
            if (r4 != r8) goto L_0x0247
            r4 = r0
            r0 = r5
            r5 = r1
            r12 = r6
            r6 = r2
            r2 = r7
            r7 = r1
            r1 = r12
        L_0x0065:
            int r5 = r5 + 1
            r12 = r0
            r0 = r4
            r4 = r7
            r7 = r2
            r2 = r6
            r6 = r1
            r1 = r5
            r5 = r12
            goto L_0x0011
        L_0x0070:
            r5 = 1
            if (r4 < 0) goto L_0x0263
            r8 = -1
            if (r2 != r8) goto L_0x0263
            r8 = r1
        L_0x0077:
            r2 = -1
            if (r0 != r2) goto L_0x007c
            int r0 = r1 + 1
        L_0x007c:
            int r2 = r1 + 1
            r1 = 1
        L_0x007f:
            if (r2 >= r9) goto L_0x0083
            if (r1 > 0) goto L_0x008d
        L_0x0083:
            if (r1 <= 0) goto L_0x00a0
            javax.mail.internet.AddressException r0 = new javax.mail.internet.AddressException
            java.lang.String r1 = "Missing ')'"
            r0.<init>(r1, r14, r2)
            throw r0
        L_0x008d:
            char r11 = r14.charAt(r2)
            switch(r11) {
                case 40: goto L_0x009a;
                case 41: goto L_0x009d;
                case 92: goto L_0x0097;
                default: goto L_0x0094;
            }
        L_0x0094:
            int r2 = r2 + 1
            goto L_0x007f
        L_0x0097:
            int r2 = r2 + 1
            goto L_0x0094
        L_0x009a:
            int r1 = r1 + 1
            goto L_0x0094
        L_0x009d:
            int r1 = r1 + -1
            goto L_0x0094
        L_0x00a0:
            int r1 = r2 + -1
            r2 = -1
            if (r3 != r2) goto L_0x025a
            r2 = r7
            r3 = r1
            r7 = r4
            r4 = r0
            r0 = r5
            r5 = r1
            r1 = r6
            r6 = r8
            goto L_0x0065
        L_0x00ae:
            javax.mail.internet.AddressException r0 = new javax.mail.internet.AddressException
            java.lang.String r2 = "Missing '('"
            r0.<init>(r2, r14, r1)
            throw r0
        L_0x00b6:
            r5 = 1
            if (r6 == 0) goto L_0x00c1
            javax.mail.internet.AddressException r0 = new javax.mail.internet.AddressException
            java.lang.String r2 = "Extra route-addr"
            r0.<init>(r2, r14, r1)
            throw r0
        L_0x00c1:
            if (r7 != 0) goto L_0x0256
            if (r4 < 0) goto L_0x0253
            r0 = r1
        L_0x00c6:
            int r2 = r1 + 1
            r12 = r4
            r4 = r2
            r2 = r12
        L_0x00cb:
            r3 = 0
            int r1 = r1 + 1
            r12 = r3
            r3 = r1
            r1 = r12
        L_0x00d1:
            if (r3 < r9) goto L_0x00df
        L_0x00d3:
            if (r3 < r9) goto L_0x00fd
            if (r1 == 0) goto L_0x00f5
            javax.mail.internet.AddressException r0 = new javax.mail.internet.AddressException
            java.lang.String r1 = "Missing '\"'"
            r0.<init>(r1, r14, r3)
            throw r0
        L_0x00df:
            char r6 = r14.charAt(r3)
            switch(r6) {
                case 34: goto L_0x00ec;
                case 62: goto L_0x00f2;
                case 92: goto L_0x00e9;
                default: goto L_0x00e6;
            }
        L_0x00e6:
            int r3 = r3 + 1
            goto L_0x00d1
        L_0x00e9:
            int r3 = r3 + 1
            goto L_0x00e6
        L_0x00ec:
            if (r1 == 0) goto L_0x00f0
            r1 = 0
            goto L_0x00e6
        L_0x00f0:
            r1 = 1
            goto L_0x00e6
        L_0x00f2:
            if (r1 == 0) goto L_0x00d3
            goto L_0x00e6
        L_0x00f5:
            javax.mail.internet.AddressException r0 = new javax.mail.internet.AddressException
            java.lang.String r1 = "Missing '>'"
            r0.<init>(r1, r14, r3)
            throw r0
        L_0x00fd:
            r1 = 1
            r6 = r3
            r12 = r7
            r7 = r4
            r4 = r2
            r2 = r12
            r13 = r3
            r3 = r0
            r0 = r5
            r5 = r13
            goto L_0x0065
        L_0x0109:
            javax.mail.internet.AddressException r0 = new javax.mail.internet.AddressException
            java.lang.String r2 = "Missing '<'"
            r0.<init>(r2, r14, r1)
            throw r0
        L_0x0111:
            r5 = 1
            r8 = -1
            if (r4 != r8) goto L_0x0116
            r4 = r1
        L_0x0116:
            int r1 = r1 + 1
        L_0x0118:
            if (r1 < r9) goto L_0x0124
        L_0x011a:
            if (r1 < r9) goto L_0x0247
            javax.mail.internet.AddressException r0 = new javax.mail.internet.AddressException
            java.lang.String r2 = "Missing '\"'"
            r0.<init>(r2, r14, r1)
            throw r0
        L_0x0124:
            char r8 = r14.charAt(r1)
            switch(r8) {
                case 34: goto L_0x011a;
                case 92: goto L_0x012e;
                default: goto L_0x012b;
            }
        L_0x012b:
            int r1 = r1 + 1
            goto L_0x0118
        L_0x012e:
            int r1 = r1 + 1
            goto L_0x012b
        L_0x0131:
            r5 = 1
            int r1 = r1 + 1
        L_0x0134:
            if (r1 < r9) goto L_0x0140
        L_0x0136:
            if (r1 < r9) goto L_0x0247
            javax.mail.internet.AddressException r0 = new javax.mail.internet.AddressException
            java.lang.String r2 = "Missing ']'"
            r0.<init>(r2, r14, r1)
            throw r0
        L_0x0140:
            char r8 = r14.charAt(r1)
            switch(r8) {
                case 92: goto L_0x014a;
                case 93: goto L_0x0136;
                default: goto L_0x0147;
            }
        L_0x0147:
            int r1 = r1 + 1
            goto L_0x0134
        L_0x014a:
            int r1 = r1 + 1
            goto L_0x0147
        L_0x014d:
            r8 = -1
            if (r4 != r8) goto L_0x015d
            r4 = 0
            r2 = 0
            r5 = -1
            r6 = r5
            r12 = r4
            r4 = r0
            r0 = r2
            r2 = r7
            r7 = r5
            r5 = r1
            r1 = r12
            goto L_0x0065
        L_0x015d:
            if (r7 == 0) goto L_0x016a
            r6 = 0
            r12 = r5
            r5 = r1
            r1 = r6
            r6 = r2
            r2 = r7
            r7 = r4
            r4 = r0
            r0 = r12
            goto L_0x0065
        L_0x016a:
            r8 = -1
            if (r2 != r8) goto L_0x016e
            r2 = r1
        L_0x016e:
            java.lang.String r2 = r14.substring(r4, r2)
            java.lang.String r2 = r2.trim()
            if (r5 != 0) goto L_0x017c
            if (r15 != 0) goto L_0x017c
            if (r16 == 0) goto L_0x01ae
        L_0x017c:
            if (r15 != 0) goto L_0x0180
            if (r16 != 0) goto L_0x0184
        L_0x0180:
            r4 = 0
            checkAddress(r2, r6, r4)
        L_0x0184:
            javax.mail.internet.InternetAddress r4 = new javax.mail.internet.InternetAddress
            r4.<init>()
            r4.setAddress(r2)
            if (r0 < 0) goto L_0x019e
            java.lang.String r0 = r14.substring(r0, r3)
            java.lang.String r0 = r0.trim()
            java.lang.String r0 = unquote(r0)
            r4.encodedPersonal = r0
            r3 = -1
            r0 = r3
        L_0x019e:
            r10.addElement(r4)
        L_0x01a1:
            r4 = 0
            r2 = 0
            r5 = -1
            r6 = r5
            r12 = r4
            r4 = r0
            r0 = r2
            r2 = r7
            r7 = r5
            r5 = r1
            r1 = r12
            goto L_0x0065
        L_0x01ae:
            java.util.StringTokenizer r4 = new java.util.StringTokenizer
            r4.<init>(r2)
        L_0x01b3:
            boolean r2 = r4.hasMoreTokens()
            if (r2 == 0) goto L_0x01a1
            java.lang.String r2 = r4.nextToken()
            r5 = 0
            r6 = 0
            checkAddress(r2, r5, r6)
            javax.mail.internet.InternetAddress r5 = new javax.mail.internet.InternetAddress
            r5.<init>()
            r5.setAddress(r2)
            r10.addElement(r5)
            goto L_0x01b3
        L_0x01ce:
            r5 = 1
            if (r7 == 0) goto L_0x01d9
            javax.mail.internet.AddressException r0 = new javax.mail.internet.AddressException
            java.lang.String r2 = "Nested group"
            r0.<init>(r2, r14, r1)
            throw r0
        L_0x01d9:
            r7 = 1
            r8 = -1
            if (r4 != r8) goto L_0x0247
            r4 = r0
            r0 = r5
            r5 = r1
            r12 = r6
            r6 = r2
            r2 = r7
            r7 = r1
            r1 = r12
            goto L_0x0065
        L_0x01e7:
            r2 = -1
            if (r4 != r2) goto L_0x0251
            r2 = r1
        L_0x01eb:
            if (r7 != 0) goto L_0x01f5
            javax.mail.internet.AddressException r0 = new javax.mail.internet.AddressException
            java.lang.String r2 = "Illegal semicolon, not in group"
            r0.<init>(r2, r14, r1)
            throw r0
        L_0x01f5:
            r4 = 0
            r6 = -1
            if (r2 != r6) goto L_0x01fa
            r2 = r1
        L_0x01fa:
            javax.mail.internet.InternetAddress r6 = new javax.mail.internet.InternetAddress
            r6.<init>()
            int r7 = r1 + 1
            java.lang.String r2 = r14.substring(r2, r7)
            java.lang.String r2 = r2.trim()
            r6.setAddress(r2)
            r10.addElement(r6)
            r2 = 0
            r6 = -1
            r7 = r6
            r12 = r2
            r2 = r4
            r4 = r0
            r0 = r5
            r5 = r1
            r1 = r12
            goto L_0x0065
        L_0x021a:
            r12 = r5
            r5 = r1
            r1 = r6
            r6 = r2
            r2 = r7
            r7 = r4
            r4 = r0
            r0 = r12
            goto L_0x0065
        L_0x0224:
            java.util.StringTokenizer r0 = new java.util.StringTokenizer
            r0.<init>(r1)
        L_0x0229:
            boolean r1 = r0.hasMoreTokens()
            if (r1 == 0) goto L_0x0049
            java.lang.String r1 = r0.nextToken()
            r2 = 0
            r3 = 0
            checkAddress(r1, r2, r3)
            javax.mail.internet.InternetAddress r2 = new javax.mail.internet.InternetAddress
            r2.<init>()
            r2.setAddress(r1)
            r10.addElement(r2)
            goto L_0x0229
        L_0x0244:
            r1 = r2
            goto L_0x0018
        L_0x0247:
            r12 = r5
            r5 = r1
            r1 = r6
            r6 = r2
            r2 = r7
            r7 = r4
            r4 = r0
            r0 = r12
            goto L_0x0065
        L_0x0251:
            r2 = r4
            goto L_0x01eb
        L_0x0253:
            r0 = r3
            goto L_0x00c6
        L_0x0256:
            r2 = r0
            r0 = r3
            goto L_0x00cb
        L_0x025a:
            r2 = r7
            r7 = r4
            r4 = r0
            r0 = r5
            r5 = r1
            r1 = r6
            r6 = r8
            goto L_0x0065
        L_0x0263:
            r8 = r2
            goto L_0x0077
        */
        throw new UnsupportedOperationException("Method not decompiled: javax.mail.internet.InternetAddress.parse(java.lang.String, boolean, boolean):javax.mail.internet.InternetAddress[]");
    }

    public void validate() throws AddressException {
        checkAddress(getAddress(), true, true);
    }

    private static void checkAddress(String str, boolean z, boolean z2) throws AddressException {
        String str2;
        String str3;
        int i = 0;
        if (str.indexOf(34) < 0) {
            if (z) {
                while (true) {
                    int indexOfAny = indexOfAny(str, ",:", i);
                    if (indexOfAny < 0) {
                        break;
                    } else if (str.charAt(i) != '@') {
                        throw new AddressException("Illegal route-addr", str);
                    } else if (str.charAt(indexOfAny) == ':') {
                        i = indexOfAny + 1;
                        break;
                    } else {
                        i = indexOfAny + 1;
                    }
                }
            }
            int indexOf = str.indexOf(64, i);
            if (indexOf >= 0) {
                if (indexOf == i) {
                    throw new AddressException("Missing local name", str);
                } else if (indexOf == str.length() - 1) {
                    throw new AddressException("Missing domain", str);
                } else {
                    str3 = str.substring(i, indexOf);
                    str2 = str.substring(indexOf + 1);
                }
            } else if (z2) {
                throw new AddressException("Missing final '@domain'", str);
            } else {
                str2 = null;
                str3 = str;
            }
            if (indexOfAny(str, " \t\n\r") >= 0) {
                throw new AddressException("Illegal whitespace in address", str);
            } else if (indexOfAny(str3, specialsNoDot) >= 0) {
                throw new AddressException("Illegal character in local name", str);
            } else if (str2 != null && str2.indexOf(91) < 0 && indexOfAny(str2, specialsNoDot) >= 0) {
                throw new AddressException("Illegal character in domain", str);
            }
        }
    }

    private boolean isSimple() {
        return this.address == null || indexOfAny(this.address, specialsNoDotNoAt) < 0;
    }

    public boolean isGroup() {
        return this.address != null && this.address.endsWith(";") && this.address.indexOf(58) > 0;
    }

    public InternetAddress[] getGroup(boolean z) throws AddressException {
        int indexOf;
        String address2 = getAddress();
        if (address2.endsWith(";") && (indexOf = address2.indexOf(58)) >= 0) {
            return parseHeader(address2.substring(indexOf + 1, address2.length() - 1), z);
        }
        return null;
    }

    private static int indexOfAny(String str, String str2) {
        return indexOfAny(str, str2, 0);
    }

    private static int indexOfAny(String str, String str2, int i) {
        try {
            int length = str.length();
            for (int i2 = i; i2 < length; i2++) {
                if (str2.indexOf(str.charAt(i2)) >= 0) {
                    return i2;
                }
            }
            return -1;
        } catch (StringIndexOutOfBoundsException e) {
            return -1;
        }
    }
}
