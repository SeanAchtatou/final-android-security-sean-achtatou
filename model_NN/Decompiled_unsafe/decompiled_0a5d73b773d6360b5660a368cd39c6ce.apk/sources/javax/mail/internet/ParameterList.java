package javax.mail.internet;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class ParameterList {
    private static boolean applehack;
    private static boolean decodeParameters;
    private static boolean decodeParametersStrict;
    private static boolean encodeParameters;
    private static final char[] hex = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    private String lastName;
    private Map list;
    private Set multisegmentNames;
    private Map slist;

    static {
        boolean z;
        boolean z2;
        boolean z3 = true;
        encodeParameters = false;
        decodeParameters = false;
        decodeParametersStrict = false;
        applehack = false;
        try {
            String property = System.getProperty("mail.mime.encodeparameters");
            encodeParameters = property != null && property.equalsIgnoreCase("true");
            String property2 = System.getProperty("mail.mime.decodeparameters");
            if (property2 == null || !property2.equalsIgnoreCase("true")) {
                z = false;
            } else {
                z = true;
            }
            decodeParameters = z;
            String property3 = System.getProperty("mail.mime.decodeparameters.strict");
            if (property3 == null || !property3.equalsIgnoreCase("true")) {
                z2 = false;
            } else {
                z2 = true;
            }
            decodeParametersStrict = z2;
            String property4 = System.getProperty("mail.mime.applefilenames");
            if (property4 == null || !property4.equalsIgnoreCase("true")) {
                z3 = false;
            }
            applehack = z3;
        } catch (SecurityException e) {
        }
    }

    private static class Value {
        String charset;
        String encodedValue;
        String value;

        private Value() {
        }

        /* synthetic */ Value(Value value2) {
            this();
        }
    }

    private static class MultiValue extends ArrayList {
        String value;

        private MultiValue() {
        }

        /* synthetic */ MultiValue(MultiValue multiValue) {
            this();
        }
    }

    private static class ParamEnum implements Enumeration {
        private Iterator it;

        ParamEnum(Iterator it2) {
            this.it = it2;
        }

        public boolean hasMoreElements() {
            return this.it.hasNext();
        }

        public Object nextElement() {
            return this.it.next();
        }
    }

    public ParameterList() {
        this.list = new LinkedHashMap();
        this.lastName = null;
        if (decodeParameters) {
            this.multisegmentNames = new HashSet();
            this.slist = new HashMap();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0135, code lost:
        throw new javax.mail.internet.ParseException("Expected ';', got \"" + r2.getValue() + "\"");
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ParameterList(java.lang.String r8) throws javax.mail.internet.ParseException {
        /*
            r7 = this;
            r6 = -4
            r5 = -1
            r7.<init>()
            javax.mail.internet.HeaderTokenizer r1 = new javax.mail.internet.HeaderTokenizer
            java.lang.String r0 = "()<>@,;:\\\"\t []/?="
            r1.<init>(r8, r0)
        L_0x000c:
            javax.mail.internet.HeaderTokenizer$Token r2 = r1.next()
            int r0 = r2.getType()
            if (r0 != r6) goto L_0x001f
        L_0x0016:
            boolean r0 = javax.mail.internet.ParameterList.decodeParameters
            if (r0 == 0) goto L_0x001e
            r0 = 0
            r7.combineMultisegmentNames(r0)
        L_0x001e:
            return
        L_0x001f:
            char r3 = (char) r0
            r4 = 59
            if (r3 != r4) goto L_0x00cb
            javax.mail.internet.HeaderTokenizer$Token r0 = r1.next()
            int r2 = r0.getType()
            if (r2 == r6) goto L_0x0016
            int r2 = r0.getType()
            if (r2 == r5) goto L_0x0053
            javax.mail.internet.ParseException r1 = new javax.mail.internet.ParseException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Expected parameter name, got \""
            r2.<init>(r3)
            java.lang.String r0 = r0.getValue()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r2 = "\""
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            throw r1
        L_0x0053:
            java.lang.String r0 = r0.getValue()
            java.util.Locale r2 = java.util.Locale.ENGLISH
            java.lang.String r0 = r0.toLowerCase(r2)
            javax.mail.internet.HeaderTokenizer$Token r2 = r1.next()
            int r3 = r2.getType()
            char r3 = (char) r3
            r4 = 61
            if (r3 == r4) goto L_0x0089
            javax.mail.internet.ParseException r0 = new javax.mail.internet.ParseException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r3 = "Expected '=', got \""
            r1.<init>(r3)
            java.lang.String r2 = r2.getValue()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "\""
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0089:
            javax.mail.internet.HeaderTokenizer$Token r2 = r1.next()
            int r3 = r2.getType()
            if (r3 == r5) goto L_0x00b5
            r4 = -2
            if (r3 == r4) goto L_0x00b5
            javax.mail.internet.ParseException r0 = new javax.mail.internet.ParseException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r3 = "Expected parameter value, got \""
            r1.<init>(r3)
            java.lang.String r2 = r2.getValue()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "\""
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x00b5:
            java.lang.String r2 = r2.getValue()
            r7.lastName = r0
            boolean r3 = javax.mail.internet.ParameterList.decodeParameters
            if (r3 == 0) goto L_0x00c4
            r7.putEncodedName(r0, r2)
            goto L_0x000c
        L_0x00c4:
            java.util.Map r3 = r7.list
            r3.put(r0, r2)
            goto L_0x000c
        L_0x00cb:
            boolean r3 = javax.mail.internet.ParameterList.applehack
            if (r3 == 0) goto L_0x0117
            if (r0 != r5) goto L_0x0117
            java.lang.String r0 = r7.lastName
            if (r0 == 0) goto L_0x0117
            java.lang.String r0 = r7.lastName
            java.lang.String r3 = "name"
            boolean r0 = r0.equals(r3)
            if (r0 != 0) goto L_0x00e9
            java.lang.String r0 = r7.lastName
            java.lang.String r3 = "filename"
            boolean r0 = r0.equals(r3)
            if (r0 == 0) goto L_0x0117
        L_0x00e9:
            java.util.Map r0 = r7.list
            java.lang.String r3 = r7.lastName
            java.lang.Object r0 = r0.get(r3)
            java.lang.String r0 = (java.lang.String) r0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r0 = java.lang.String.valueOf(r0)
            r3.<init>(r0)
            java.lang.String r0 = " "
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r2 = r2.getValue()
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            java.util.Map r2 = r7.list
            java.lang.String r3 = r7.lastName
            r2.put(r3, r0)
            goto L_0x000c
        L_0x0117:
            javax.mail.internet.ParseException r0 = new javax.mail.internet.ParseException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r3 = "Expected ';', got \""
            r1.<init>(r3)
            java.lang.String r2 = r2.getValue()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "\""
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: javax.mail.internet.ParameterList.<init>(java.lang.String):void");
    }

    /* JADX WARN: Type inference failed for: r1v4 */
    /* JADX WARN: Type inference failed for: r1v5, types: [java.lang.Object] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void putEncodedName(java.lang.String r5, java.lang.String r6) throws javax.mail.internet.ParseException {
        /*
            r4 = this;
            r3 = 0
            r0 = 42
            int r0 = r5.indexOf(r0)
            if (r0 >= 0) goto L_0x000f
            java.util.Map r0 = r4.list
            r0.put(r5, r6)
        L_0x000e:
            return
        L_0x000f:
            int r1 = r5.length()
            int r1 = r1 + -1
            if (r0 != r1) goto L_0x0025
            java.lang.String r0 = r5.substring(r3, r0)
            java.util.Map r1 = r4.list
            javax.mail.internet.ParameterList$Value r2 = decodeValue(r6)
            r1.put(r0, r2)
            goto L_0x000e
        L_0x0025:
            java.lang.String r0 = r5.substring(r3, r0)
            java.util.Set r1 = r4.multisegmentNames
            r1.add(r0)
            java.util.Map r1 = r4.list
            java.lang.String r2 = ""
            r1.put(r0, r2)
            java.lang.String r0 = "*"
            boolean r0 = r5.endsWith(r0)
            if (r0 == 0) goto L_0x005d
            javax.mail.internet.ParameterList$Value r1 = new javax.mail.internet.ParameterList$Value
            r0 = 0
            r1.<init>(r0)
            r0 = r1
            javax.mail.internet.ParameterList$Value r0 = (javax.mail.internet.ParameterList.Value) r0
            r0.encodedValue = r6
            r0 = r1
            javax.mail.internet.ParameterList$Value r0 = (javax.mail.internet.ParameterList.Value) r0
            r0.value = r6
            int r0 = r5.length()
            int r0 = r0 + -1
            java.lang.String r5 = r5.substring(r3, r0)
        L_0x0057:
            java.util.Map r0 = r4.slist
            r0.put(r5, r1)
            goto L_0x000e
        L_0x005d:
            r1 = r6
            goto L_0x0057
        */
        throw new UnsupportedOperationException("Method not decompiled: javax.mail.internet.ParameterList.putEncodedName(java.lang.String, java.lang.String):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:88:0x0164  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x00ae A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x00c6 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x00d6 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void combineMultisegmentNames(boolean r13) throws javax.mail.internet.ParseException {
        /*
            r12 = this;
            r3 = 0
            java.util.Set r0 = r12.multisegmentNames     // Catch:{ all -> 0x0074 }
            java.util.Iterator r7 = r0.iterator()     // Catch:{ all -> 0x0074 }
        L_0x0007:
            boolean r0 = r7.hasNext()     // Catch:{ all -> 0x0074 }
            if (r0 != 0) goto L_0x0039
            if (r13 != 0) goto L_0x000f
        L_0x000f:
            java.util.Map r0 = r12.slist
            int r0 = r0.size()
            if (r0 <= 0) goto L_0x002e
            java.util.Map r0 = r12.slist
            java.util.Collection r0 = r0.values()
            java.util.Iterator r1 = r0.iterator()
        L_0x0021:
            boolean r0 = r1.hasNext()
            if (r0 != 0) goto L_0x0135
            java.util.Map r0 = r12.list
            java.util.Map r1 = r12.slist
            r0.putAll(r1)
        L_0x002e:
            java.util.Set r0 = r12.multisegmentNames
            r0.clear()
            java.util.Map r0 = r12.slist
            r0.clear()
            return
        L_0x0039:
            java.lang.Object r0 = r7.next()     // Catch:{ all -> 0x0074 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x0074 }
            java.lang.StringBuffer r8 = new java.lang.StringBuffer     // Catch:{ all -> 0x0074 }
            r8.<init>()     // Catch:{ all -> 0x0074 }
            javax.mail.internet.ParameterList$MultiValue r9 = new javax.mail.internet.ParameterList$MultiValue     // Catch:{ all -> 0x0074 }
            r1 = 0
            r9.<init>(r1)     // Catch:{ all -> 0x0074 }
            r1 = 0
            r6 = r1
            r5 = r3
        L_0x004d:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0074 }
            java.lang.String r2 = java.lang.String.valueOf(r0)     // Catch:{ all -> 0x0074 }
            r1.<init>(r2)     // Catch:{ all -> 0x0074 }
            java.lang.String r2 = "*"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0074 }
            java.lang.StringBuilder r1 = r1.append(r6)     // Catch:{ all -> 0x0074 }
            java.lang.String r10 = r1.toString()     // Catch:{ all -> 0x0074 }
            java.util.Map r1 = r12.slist     // Catch:{ all -> 0x0074 }
            java.lang.Object r1 = r1.get(r10)     // Catch:{ all -> 0x0074 }
            if (r1 != 0) goto L_0x0079
        L_0x006c:
            if (r6 != 0) goto L_0x00e4
            java.util.Map r1 = r12.list     // Catch:{ all -> 0x0074 }
            r1.remove(r0)     // Catch:{ all -> 0x0074 }
            goto L_0x0007
        L_0x0074:
            r0 = move-exception
            r1 = r0
            if (r13 != 0) goto L_0x00f1
        L_0x0078:
            throw r1
        L_0x0079:
            r9.add(r1)     // Catch:{ all -> 0x0074 }
            boolean r2 = r1 instanceof javax.mail.internet.ParameterList.Value     // Catch:{ all -> 0x0074 }
            if (r2 == 0) goto L_0x00e0
            javax.mail.internet.ParameterList$Value r1 = (javax.mail.internet.ParameterList.Value) r1     // Catch:{ NumberFormatException -> 0x015f, UnsupportedEncodingException -> 0x0155, StringIndexOutOfBoundsException -> 0x014f }
            java.lang.String r2 = r1.encodedValue     // Catch:{ NumberFormatException -> 0x015f, UnsupportedEncodingException -> 0x0155, StringIndexOutOfBoundsException -> 0x014f }
            if (r6 != 0) goto L_0x00a0
            javax.mail.internet.ParameterList$Value r11 = decodeValue(r2)     // Catch:{ NumberFormatException -> 0x00a8, UnsupportedEncodingException -> 0x0159, StringIndexOutOfBoundsException -> 0x0152 }
            java.lang.String r4 = r11.charset     // Catch:{ NumberFormatException -> 0x00a8, UnsupportedEncodingException -> 0x0159, StringIndexOutOfBoundsException -> 0x0152 }
            r1.charset = r4     // Catch:{ NumberFormatException -> 0x00a8, UnsupportedEncodingException -> 0x0159, StringIndexOutOfBoundsException -> 0x0152 }
            java.lang.String r5 = r11.value     // Catch:{ NumberFormatException -> 0x015c, UnsupportedEncodingException -> 0x00c0, StringIndexOutOfBoundsException -> 0x00d0 }
            r1.value = r5     // Catch:{ NumberFormatException -> 0x015c, UnsupportedEncodingException -> 0x00c0, StringIndexOutOfBoundsException -> 0x00d0 }
            r2 = r5
            r5 = r4
        L_0x0094:
            r8.append(r2)     // Catch:{ all -> 0x0074 }
            java.util.Map r1 = r12.slist     // Catch:{ all -> 0x0074 }
            r1.remove(r10)     // Catch:{ all -> 0x0074 }
            int r1 = r6 + 1
            r6 = r1
            goto L_0x004d
        L_0x00a0:
            if (r5 != 0) goto L_0x00b8
            java.util.Set r1 = r12.multisegmentNames     // Catch:{ NumberFormatException -> 0x00a8, UnsupportedEncodingException -> 0x0159, StringIndexOutOfBoundsException -> 0x0152 }
            r1.remove(r0)     // Catch:{ NumberFormatException -> 0x00a8, UnsupportedEncodingException -> 0x0159, StringIndexOutOfBoundsException -> 0x0152 }
            goto L_0x006c
        L_0x00a8:
            r1 = move-exception
            r4 = r5
        L_0x00aa:
            boolean r5 = javax.mail.internet.ParameterList.decodeParametersStrict     // Catch:{ all -> 0x0074 }
            if (r5 == 0) goto L_0x0164
            javax.mail.internet.ParseException r0 = new javax.mail.internet.ParseException     // Catch:{ all -> 0x0074 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0074 }
            r0.<init>(r1)     // Catch:{ all -> 0x0074 }
            throw r0     // Catch:{ all -> 0x0074 }
        L_0x00b8:
            java.lang.String r4 = decodeBytes(r2, r5)     // Catch:{ NumberFormatException -> 0x00a8, UnsupportedEncodingException -> 0x0159, StringIndexOutOfBoundsException -> 0x0152 }
            r1.value = r4     // Catch:{ NumberFormatException -> 0x00a8, UnsupportedEncodingException -> 0x0159, StringIndexOutOfBoundsException -> 0x0152 }
            r2 = r4
            goto L_0x0094
        L_0x00c0:
            r1 = move-exception
            r5 = r4
        L_0x00c2:
            boolean r4 = javax.mail.internet.ParameterList.decodeParametersStrict     // Catch:{ all -> 0x0074 }
            if (r4 == 0) goto L_0x0094
            javax.mail.internet.ParseException r0 = new javax.mail.internet.ParseException     // Catch:{ all -> 0x0074 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0074 }
            r0.<init>(r1)     // Catch:{ all -> 0x0074 }
            throw r0     // Catch:{ all -> 0x0074 }
        L_0x00d0:
            r1 = move-exception
            r5 = r4
        L_0x00d2:
            boolean r4 = javax.mail.internet.ParameterList.decodeParametersStrict     // Catch:{ all -> 0x0074 }
            if (r4 == 0) goto L_0x0094
            javax.mail.internet.ParseException r0 = new javax.mail.internet.ParseException     // Catch:{ all -> 0x0074 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0074 }
            r0.<init>(r1)     // Catch:{ all -> 0x0074 }
            throw r0     // Catch:{ all -> 0x0074 }
        L_0x00e0:
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ all -> 0x0074 }
            r2 = r1
            goto L_0x0094
        L_0x00e4:
            java.lang.String r1 = r8.toString()     // Catch:{ all -> 0x0074 }
            r9.value = r1     // Catch:{ all -> 0x0074 }
            java.util.Map r1 = r12.list     // Catch:{ all -> 0x0074 }
            r1.put(r0, r9)     // Catch:{ all -> 0x0074 }
            goto L_0x0007
        L_0x00f1:
            java.util.Map r0 = r12.slist
            int r0 = r0.size()
            if (r0 <= 0) goto L_0x0110
            java.util.Map r0 = r12.slist
            java.util.Collection r0 = r0.values()
            java.util.Iterator r2 = r0.iterator()
        L_0x0103:
            boolean r0 = r2.hasNext()
            if (r0 != 0) goto L_0x011c
            java.util.Map r0 = r12.list
            java.util.Map r2 = r12.slist
            r0.putAll(r2)
        L_0x0110:
            java.util.Set r0 = r12.multisegmentNames
            r0.clear()
            java.util.Map r0 = r12.slist
            r0.clear()
            goto L_0x0078
        L_0x011c:
            java.lang.Object r0 = r2.next()
            boolean r3 = r0 instanceof javax.mail.internet.ParameterList.Value
            if (r3 == 0) goto L_0x0103
            javax.mail.internet.ParameterList$Value r0 = (javax.mail.internet.ParameterList.Value) r0
            java.lang.String r3 = r0.encodedValue
            javax.mail.internet.ParameterList$Value r3 = decodeValue(r3)
            java.lang.String r4 = r3.charset
            r0.charset = r4
            java.lang.String r3 = r3.value
            r0.value = r3
            goto L_0x0103
        L_0x0135:
            java.lang.Object r0 = r1.next()
            boolean r2 = r0 instanceof javax.mail.internet.ParameterList.Value
            if (r2 == 0) goto L_0x0021
            javax.mail.internet.ParameterList$Value r0 = (javax.mail.internet.ParameterList.Value) r0
            java.lang.String r2 = r0.encodedValue
            javax.mail.internet.ParameterList$Value r2 = decodeValue(r2)
            java.lang.String r3 = r2.charset
            r0.charset = r3
            java.lang.String r2 = r2.value
            r0.value = r2
            goto L_0x0021
        L_0x014f:
            r1 = move-exception
            r2 = r3
            goto L_0x00d2
        L_0x0152:
            r1 = move-exception
            goto L_0x00d2
        L_0x0155:
            r1 = move-exception
            r2 = r3
            goto L_0x00c2
        L_0x0159:
            r1 = move-exception
            goto L_0x00c2
        L_0x015c:
            r1 = move-exception
            goto L_0x00aa
        L_0x015f:
            r1 = move-exception
            r2 = r3
            r4 = r5
            goto L_0x00aa
        L_0x0164:
            r5 = r4
            goto L_0x0094
        */
        throw new UnsupportedOperationException("Method not decompiled: javax.mail.internet.ParameterList.combineMultisegmentNames(boolean):void");
    }

    public int size() {
        return this.list.size();
    }

    public String get(String str) {
        Object obj = this.list.get(str.trim().toLowerCase(Locale.ENGLISH));
        if (obj instanceof MultiValue) {
            return ((MultiValue) obj).value;
        }
        if (obj instanceof Value) {
            return ((Value) obj).value;
        }
        return (String) obj;
    }

    public void set(String str, String str2) {
        if (str != null || str2 == null || !str2.equals("DONE")) {
            String lowerCase = str.trim().toLowerCase(Locale.ENGLISH);
            if (decodeParameters) {
                try {
                    putEncodedName(lowerCase, str2);
                } catch (ParseException e) {
                    this.list.put(lowerCase, str2);
                }
            } else {
                this.list.put(lowerCase, str2);
            }
        } else if (decodeParameters && this.multisegmentNames.size() > 0) {
            try {
                combineMultisegmentNames(true);
            } catch (ParseException e2) {
            }
        }
    }

    public void set(String str, String str2, String str3) {
        if (encodeParameters) {
            Value encodeValue = encodeValue(str2, str3);
            if (encodeValue != null) {
                this.list.put(str.trim().toLowerCase(Locale.ENGLISH), encodeValue);
            } else {
                set(str, str2);
            }
        } else {
            set(str, str2);
        }
    }

    public void remove(String str) {
        this.list.remove(str.trim().toLowerCase(Locale.ENGLISH));
    }

    public Enumeration getNames() {
        return new ParamEnum(this.list.keySet().iterator());
    }

    public String toString() {
        return toString(0);
    }

    public String toString(int i) {
        ToStringBuffer toStringBuffer = new ToStringBuffer(i);
        for (String str : this.list.keySet()) {
            Object obj = this.list.get(str);
            if (obj instanceof MultiValue) {
                MultiValue multiValue = (MultiValue) obj;
                String str2 = String.valueOf(str) + "*";
                int i2 = 0;
                while (true) {
                    int i3 = i2;
                    if (i3 >= multiValue.size()) {
                        break;
                    }
                    Object obj2 = multiValue.get(i3);
                    if (obj2 instanceof Value) {
                        toStringBuffer.addNV(String.valueOf(str2) + i3 + "*", ((Value) obj2).encodedValue);
                    } else {
                        toStringBuffer.addNV(String.valueOf(str2) + i3, (String) obj2);
                    }
                    i2 = i3 + 1;
                }
            } else if (obj instanceof Value) {
                toStringBuffer.addNV(String.valueOf(str) + "*", ((Value) obj).encodedValue);
            } else {
                toStringBuffer.addNV(str, (String) obj);
            }
        }
        return toStringBuffer.toString();
    }

    private static class ToStringBuffer {
        private StringBuffer sb = new StringBuffer();
        private int used;

        public ToStringBuffer(int i) {
            this.used = i;
        }

        public void addNV(String str, String str2) {
            String access$0 = ParameterList.quote(str2);
            this.sb.append("; ");
            this.used += 2;
            if (str.length() + access$0.length() + 1 + this.used > 76) {
                this.sb.append("\r\n\t");
                this.used = 8;
            }
            this.sb.append(str).append('=');
            this.used += str.length() + 1;
            if (this.used + access$0.length() > 76) {
                String fold = MimeUtility.fold(this.used, access$0);
                this.sb.append(fold);
                int lastIndexOf = fold.lastIndexOf(10);
                if (lastIndexOf >= 0) {
                    this.used = ((fold.length() - lastIndexOf) - 1) + this.used;
                    return;
                }
                this.used = fold.length() + this.used;
                return;
            }
            this.sb.append(access$0);
            this.used = access$0.length() + this.used;
        }

        public String toString() {
            return this.sb.toString();
        }
    }

    /* access modifiers changed from: private */
    public static String quote(String str) {
        return MimeUtility.quote(str, HeaderTokenizer.MIME);
    }

    private static Value encodeValue(String str, String str2) {
        if (MimeUtility.checkAscii(str) == 1) {
            return null;
        }
        try {
            byte[] bytes = str.getBytes(MimeUtility.javaCharset(str2));
            StringBuffer stringBuffer = new StringBuffer(bytes.length + str2.length() + 2);
            stringBuffer.append(str2).append("''");
            for (byte b : bytes) {
                char c = (char) (b & 255);
                if (c <= ' ' || c >= 127 || c == '*' || c == '\'' || c == '%' || HeaderTokenizer.MIME.indexOf(c) >= 0) {
                    stringBuffer.append('%').append(hex[c >> 4]).append(hex[c & 15]);
                } else {
                    stringBuffer.append(c);
                }
            }
            Value value = new Value(null);
            value.charset = str2;
            value.value = str;
            value.encodedValue = stringBuffer.toString();
            return value;
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    private static Value decodeValue(String str) throws ParseException {
        Value value = new Value(null);
        value.encodedValue = str;
        value.value = str;
        try {
            int indexOf = str.indexOf(39);
            if (indexOf > 0) {
                String substring = str.substring(0, indexOf);
                int indexOf2 = str.indexOf(39, indexOf + 1);
                if (indexOf2 >= 0) {
                    str.substring(indexOf + 1, indexOf2);
                    String substring2 = str.substring(indexOf2 + 1);
                    value.charset = substring;
                    value.value = decodeBytes(substring2, substring);
                } else if (decodeParametersStrict) {
                    throw new ParseException("Missing language in encoded value: " + str);
                }
            } else if (decodeParametersStrict) {
                throw new ParseException("Missing charset in encoded value: " + str);
            }
        } catch (NumberFormatException e) {
            if (decodeParametersStrict) {
                throw new ParseException(e.toString());
            }
        } catch (UnsupportedEncodingException e2) {
            if (decodeParametersStrict) {
                throw new ParseException(e2.toString());
            }
        } catch (StringIndexOutOfBoundsException e3) {
            if (decodeParametersStrict) {
                throw new ParseException(e3.toString());
            }
        }
        return value;
    }

    private static String decodeBytes(String str, String str2) throws UnsupportedEncodingException {
        byte[] bArr = new byte[str.length()];
        int i = 0;
        int i2 = 0;
        while (i2 < str.length()) {
            char charAt = str.charAt(i2);
            if (charAt == '%') {
                charAt = (char) Integer.parseInt(str.substring(i2 + 1, i2 + 3), 16);
                i2 += 2;
            }
            bArr[i] = (byte) charAt;
            i2++;
            i++;
        }
        return new String(bArr, 0, i, MimeUtility.javaCharset(str2));
    }
}
