package javax.mail.internet;

import com.sun.mail.imap.IMAPStore;
import java.text.FieldPosition;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

public class MailDateFormat extends SimpleDateFormat {
    private static Calendar cal = new GregorianCalendar(tz);
    static boolean debug = false;
    private static final long serialVersionUID = -8148227605210628779L;
    private static TimeZone tz = TimeZone.getTimeZone("GMT");

    public MailDateFormat() {
        super("EEE, d MMM yyyy HH:mm:ss 'XXXXX' (z)", Locale.US);
    }

    public StringBuffer format(Date date, StringBuffer stringBuffer, FieldPosition fieldPosition) {
        int i;
        int i2;
        int length = stringBuffer.length();
        super.format(date, stringBuffer, fieldPosition);
        int i3 = length + 25;
        while (stringBuffer.charAt(i3) != 'X') {
            i3++;
        }
        this.calendar.clear();
        this.calendar.setTime(date);
        int i4 = this.calendar.get(15) + this.calendar.get(16);
        if (i4 < 0) {
            int i5 = i3 + 1;
            stringBuffer.setCharAt(i3, '-');
            i = -i4;
            i2 = i5;
        } else {
            int i6 = i3 + 1;
            stringBuffer.setCharAt(i3, '+');
            i = i4;
            i2 = i6;
        }
        int i7 = (i / 60) / IMAPStore.RESPONSE;
        int i8 = i7 / 60;
        int i9 = i7 % 60;
        int i10 = i2 + 1;
        stringBuffer.setCharAt(i2, Character.forDigit(i8 / 10, 10));
        int i11 = i10 + 1;
        stringBuffer.setCharAt(i10, Character.forDigit(i8 % 10, 10));
        int i12 = i11 + 1;
        stringBuffer.setCharAt(i11, Character.forDigit(i9 / 10, 10));
        int i13 = i12 + 1;
        stringBuffer.setCharAt(i12, Character.forDigit(i9 % 10, 10));
        return stringBuffer;
    }

    public Date parse(String str, ParsePosition parsePosition) {
        return parseDate(str.toCharArray(), parsePosition, isLenient());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x008d, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0090, code lost:
        if (javax.mail.internet.MailDateFormat.debug != false) goto L_0x0092;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0092, code lost:
        java.lang.System.out.println("Bad date: '" + new java.lang.String(r11) + "'");
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00b4, code lost:
        r12.setIndex(1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
        return null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.util.Date parseDate(char[] r11, java.text.ParsePosition r12, boolean r13) {
        /*
            r6 = 0
            javax.mail.internet.MailDateParser r7 = new javax.mail.internet.MailDateParser     // Catch:{ Exception -> 0x008d }
            r7.<init>(r11)     // Catch:{ Exception -> 0x008d }
            r7.skipUntilNumber()     // Catch:{ Exception -> 0x008d }
            int r2 = r7.parseNumber()     // Catch:{ Exception -> 0x008d }
            r0 = 45
            boolean r0 = r7.skipIfChar(r0)     // Catch:{ Exception -> 0x008d }
            if (r0 != 0) goto L_0x0018
            r7.skipWhiteSpace()     // Catch:{ Exception -> 0x008d }
        L_0x0018:
            int r1 = r7.parseMonth()     // Catch:{ Exception -> 0x008d }
            r0 = 45
            boolean r0 = r7.skipIfChar(r0)     // Catch:{ Exception -> 0x008d }
            if (r0 != 0) goto L_0x0027
            r7.skipWhiteSpace()     // Catch:{ Exception -> 0x008d }
        L_0x0027:
            int r0 = r7.parseNumber()     // Catch:{ Exception -> 0x008d }
            r3 = 50
            if (r0 >= r3) goto L_0x0061
            int r0 = r0 + 2000
        L_0x0031:
            r7.skipWhiteSpace()     // Catch:{ Exception -> 0x008d }
            int r3 = r7.parseNumber()     // Catch:{ Exception -> 0x008d }
            r4 = 58
            r7.skipChar(r4)     // Catch:{ Exception -> 0x008d }
            int r4 = r7.parseNumber()     // Catch:{ Exception -> 0x008d }
            r5 = 58
            boolean r5 = r7.skipIfChar(r5)     // Catch:{ Exception -> 0x008d }
            if (r5 == 0) goto L_0x00ba
            int r5 = r7.parseNumber()     // Catch:{ Exception -> 0x008d }
        L_0x004d:
            r7.skipWhiteSpace()     // Catch:{ ParseException -> 0x0068 }
            int r6 = r7.parseTimeZone()     // Catch:{ ParseException -> 0x0068 }
        L_0x0054:
            int r7 = r7.getIndex()     // Catch:{ Exception -> 0x008d }
            r12.setIndex(r7)     // Catch:{ Exception -> 0x008d }
            r7 = r13
            java.util.Date r0 = ourUTC(r0, r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x008d }
        L_0x0060:
            return r0
        L_0x0061:
            r3 = 100
            if (r0 >= r3) goto L_0x0031
            int r0 = r0 + 1900
            goto L_0x0031
        L_0x0068:
            r8 = move-exception
            boolean r8 = javax.mail.internet.MailDateFormat.debug     // Catch:{ Exception -> 0x008d }
            if (r8 == 0) goto L_0x0054
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ Exception -> 0x008d }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x008d }
            java.lang.String r10 = "No timezone? : '"
            r9.<init>(r10)     // Catch:{ Exception -> 0x008d }
            java.lang.String r10 = new java.lang.String     // Catch:{ Exception -> 0x008d }
            r10.<init>(r11)     // Catch:{ Exception -> 0x008d }
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x008d }
            java.lang.String r10 = "'"
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x008d }
            java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x008d }
            r8.println(r9)     // Catch:{ Exception -> 0x008d }
            goto L_0x0054
        L_0x008d:
            r0 = move-exception
            boolean r1 = javax.mail.internet.MailDateFormat.debug
            if (r1 == 0) goto L_0x00b4
            java.io.PrintStream r1 = java.lang.System.out
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Bad date: '"
            r2.<init>(r3)
            java.lang.String r3 = new java.lang.String
            r3.<init>(r11)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "'"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.println(r2)
            r0.printStackTrace()
        L_0x00b4:
            r0 = 1
            r12.setIndex(r0)
            r0 = 0
            goto L_0x0060
        L_0x00ba:
            r5 = r6
            goto L_0x004d
        */
        throw new UnsupportedOperationException("Method not decompiled: javax.mail.internet.MailDateFormat.parseDate(char[], java.text.ParsePosition, boolean):java.util.Date");
    }

    private static synchronized Date ourUTC(int i, int i2, int i3, int i4, int i5, int i6, int i7, boolean z) {
        Date time;
        synchronized (MailDateFormat.class) {
            cal.clear();
            cal.setLenient(z);
            cal.set(1, i);
            cal.set(2, i2);
            cal.set(5, i3);
            cal.set(11, i4);
            cal.set(12, i5 + i7);
            cal.set(13, i6);
            time = cal.getTime();
        }
        return time;
    }

    public void setCalendar(Calendar calendar) {
        throw new RuntimeException("Method setCalendar() shouldn't be called");
    }

    public void setNumberFormat(NumberFormat numberFormat) {
        throw new RuntimeException("Method setNumberFormat() shouldn't be called");
    }
}
