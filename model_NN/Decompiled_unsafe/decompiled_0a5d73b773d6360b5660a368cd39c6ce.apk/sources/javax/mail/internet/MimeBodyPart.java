package javax.mail.internet;

import com.sun.mail.util.ASCIIUtility;
import com.sun.mail.util.FolderClosedIOException;
import com.sun.mail.util.LineOutputStream;
import com.sun.mail.util.MessageRemovedIOException;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Enumeration;
import java.util.Vector;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.FolderClosedException;
import javax.mail.Message;
import javax.mail.MessageRemovedException;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.HeaderTokenizer;

public class MimeBodyPart extends BodyPart implements MimePart {
    static boolean cacheMultipart;
    private static boolean decodeFileName;
    private static boolean encodeFileName;
    private static boolean setContentTypeFileName;
    private static boolean setDefaultTextCharset;
    private Object cachedContent;
    protected byte[] content;
    protected InputStream contentStream;
    protected DataHandler dh;
    protected InternetHeaders headers;

    static {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4 = false;
        setDefaultTextCharset = true;
        setContentTypeFileName = true;
        encodeFileName = false;
        decodeFileName = false;
        cacheMultipart = true;
        try {
            String property = System.getProperty("mail.mime.setdefaulttextcharset");
            setDefaultTextCharset = property == null || !property.equalsIgnoreCase("false");
            String property2 = System.getProperty("mail.mime.setcontenttypefilename");
            if (property2 == null || !property2.equalsIgnoreCase("false")) {
                z = true;
            } else {
                z = false;
            }
            setContentTypeFileName = z;
            String property3 = System.getProperty("mail.mime.encodefilename");
            if (property3 == null || property3.equalsIgnoreCase("false")) {
                z2 = false;
            } else {
                z2 = true;
            }
            encodeFileName = z2;
            String property4 = System.getProperty("mail.mime.decodefilename");
            if (property4 == null || property4.equalsIgnoreCase("false")) {
                z3 = false;
            } else {
                z3 = true;
            }
            decodeFileName = z3;
            String property5 = System.getProperty("mail.mime.cachemultipart");
            if (property5 == null || !property5.equalsIgnoreCase("false")) {
                z4 = true;
            }
            cacheMultipart = z4;
        } catch (SecurityException e) {
        }
    }

    public MimeBodyPart() {
        this.headers = new InternetHeaders();
    }

    public MimeBodyPart(InputStream inputStream) throws MessagingException {
        InputStream inputStream2;
        if ((inputStream instanceof ByteArrayInputStream) || (inputStream instanceof BufferedInputStream) || (inputStream instanceof SharedInputStream)) {
            inputStream2 = inputStream;
        } else {
            inputStream2 = new BufferedInputStream(inputStream);
        }
        this.headers = new InternetHeaders(inputStream2);
        if (inputStream2 instanceof SharedInputStream) {
            SharedInputStream sharedInputStream = (SharedInputStream) inputStream2;
            this.contentStream = sharedInputStream.newStream(sharedInputStream.getPosition(), -1);
            return;
        }
        try {
            this.content = ASCIIUtility.getBytes(inputStream2);
        } catch (IOException e) {
            throw new MessagingException("Error reading input stream", e);
        }
    }

    public MimeBodyPart(InternetHeaders internetHeaders, byte[] bArr) throws MessagingException {
        this.headers = internetHeaders;
        this.content = bArr;
    }

    public int getSize() throws MessagingException {
        if (this.content != null) {
            return this.content.length;
        }
        if (this.contentStream != null) {
            try {
                int available = this.contentStream.available();
                if (available <= 0) {
                    return -1;
                }
                return available;
            } catch (IOException e) {
            }
        }
        return -1;
    }

    public int getLineCount() throws MessagingException {
        return -1;
    }

    public String getContentType() throws MessagingException {
        String header = getHeader("Content-Type", null);
        if (header == null) {
            return "text/plain";
        }
        return header;
    }

    public boolean isMimeType(String str) throws MessagingException {
        return isMimeType(this, str);
    }

    public String getDisposition() throws MessagingException {
        return getDisposition(this);
    }

    public void setDisposition(String str) throws MessagingException {
        setDisposition(this, str);
    }

    public String getEncoding() throws MessagingException {
        return getEncoding(this);
    }

    public String getContentID() throws MessagingException {
        return getHeader("Content-Id", null);
    }

    public void setContentID(String str) throws MessagingException {
        if (str == null) {
            removeHeader("Content-ID");
        } else {
            setHeader("Content-ID", str);
        }
    }

    public String getContentMD5() throws MessagingException {
        return getHeader("Content-MD5", null);
    }

    public void setContentMD5(String str) throws MessagingException {
        setHeader("Content-MD5", str);
    }

    public String[] getContentLanguage() throws MessagingException {
        return getContentLanguage(this);
    }

    public void setContentLanguage(String[] strArr) throws MessagingException {
        setContentLanguage(this, strArr);
    }

    public String getDescription() throws MessagingException {
        return getDescription(this);
    }

    public void setDescription(String str) throws MessagingException {
        setDescription(str, null);
    }

    public void setDescription(String str, String str2) throws MessagingException {
        setDescription(this, str, str2);
    }

    public String getFileName() throws MessagingException {
        return getFileName(this);
    }

    public void setFileName(String str) throws MessagingException {
        setFileName(this, str);
    }

    public InputStream getInputStream() throws IOException, MessagingException {
        return getDataHandler().getInputStream();
    }

    /* access modifiers changed from: protected */
    public InputStream getContentStream() throws MessagingException {
        if (this.contentStream != null) {
            return ((SharedInputStream) this.contentStream).newStream(0, -1);
        }
        if (this.content != null) {
            return new ByteArrayInputStream(this.content);
        }
        throw new MessagingException("No content");
    }

    public InputStream getRawInputStream() throws MessagingException {
        return getContentStream();
    }

    public DataHandler getDataHandler() throws MessagingException {
        if (this.dh == null) {
            this.dh = new DataHandler(new MimePartDataSource(this));
        }
        return this.dh;
    }

    public Object getContent() throws IOException, MessagingException {
        if (this.cachedContent != null) {
            return this.cachedContent;
        }
        try {
            Object content2 = getDataHandler().getContent();
            if (!cacheMultipart) {
                return content2;
            }
            if (!(content2 instanceof Multipart) && !(content2 instanceof Message)) {
                return content2;
            }
            if (this.content == null && this.contentStream == null) {
                return content2;
            }
            this.cachedContent = content2;
            return content2;
        } catch (FolderClosedIOException e) {
            throw new FolderClosedException(e.getFolder(), e.getMessage());
        } catch (MessageRemovedIOException e2) {
            throw new MessageRemovedException(e2.getMessage());
        }
    }

    public void setDataHandler(DataHandler dataHandler) throws MessagingException {
        this.dh = dataHandler;
        this.cachedContent = null;
        invalidateContentHeaders(this);
    }

    public void setContent(Object obj, String str) throws MessagingException {
        if (obj instanceof Multipart) {
            setContent((Multipart) obj);
        } else {
            setDataHandler(new DataHandler(obj, str));
        }
    }

    public void setText(String str) throws MessagingException {
        setText(str, null);
    }

    public void setText(String str, String str2) throws MessagingException {
        setText(this, str, str2, "plain");
    }

    public void setText(String str, String str2, String str3) throws MessagingException {
        setText(this, str, str2, str3);
    }

    public void setContent(Multipart multipart) throws MessagingException {
        setDataHandler(new DataHandler(multipart, multipart.getContentType()));
        multipart.setParent(this);
    }

    public void attachFile(File file) throws IOException, MessagingException {
        FileDataSource fileDataSource = new FileDataSource(file);
        setDataHandler(new DataHandler(fileDataSource));
        setFileName(fileDataSource.getName());
    }

    public void attachFile(String str) throws IOException, MessagingException {
        attachFile(new File(str));
    }

    public void saveFile(File file) throws IOException, MessagingException {
        BufferedOutputStream bufferedOutputStream;
        InputStream inputStream = null;
        try {
            bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file));
            try {
                inputStream = getInputStream();
                byte[] bArr = new byte[8192];
                while (true) {
                    int read = inputStream.read(bArr);
                    if (read <= 0) {
                        break;
                    }
                    bufferedOutputStream.write(bArr, 0, read);
                }
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                    }
                }
                if (bufferedOutputStream != null) {
                    try {
                        bufferedOutputStream.close();
                    } catch (IOException e2) {
                    }
                }
            } catch (Throwable th) {
                th = th;
            }
        } catch (Throwable th2) {
            th = th2;
            bufferedOutputStream = null;
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e3) {
                }
            }
            if (bufferedOutputStream != null) {
                try {
                    bufferedOutputStream.close();
                } catch (IOException e4) {
                }
            }
            throw th;
        }
    }

    public void saveFile(String str) throws IOException, MessagingException {
        saveFile(new File(str));
    }

    public void writeTo(OutputStream outputStream) throws IOException, MessagingException {
        writeTo(this, outputStream, null);
    }

    public String[] getHeader(String str) throws MessagingException {
        return this.headers.getHeader(str);
    }

    public String getHeader(String str, String str2) throws MessagingException {
        return this.headers.getHeader(str, str2);
    }

    public void setHeader(String str, String str2) throws MessagingException {
        this.headers.setHeader(str, str2);
    }

    public void addHeader(String str, String str2) throws MessagingException {
        this.headers.addHeader(str, str2);
    }

    public void removeHeader(String str) throws MessagingException {
        this.headers.removeHeader(str);
    }

    public Enumeration getAllHeaders() throws MessagingException {
        return this.headers.getAllHeaders();
    }

    public Enumeration getMatchingHeaders(String[] strArr) throws MessagingException {
        return this.headers.getMatchingHeaders(strArr);
    }

    public Enumeration getNonMatchingHeaders(String[] strArr) throws MessagingException {
        return this.headers.getNonMatchingHeaders(strArr);
    }

    public void addHeaderLine(String str) throws MessagingException {
        this.headers.addHeaderLine(str);
    }

    public Enumeration getAllHeaderLines() throws MessagingException {
        return this.headers.getAllHeaderLines();
    }

    public Enumeration getMatchingHeaderLines(String[] strArr) throws MessagingException {
        return this.headers.getMatchingHeaderLines(strArr);
    }

    public Enumeration getNonMatchingHeaderLines(String[] strArr) throws MessagingException {
        return this.headers.getNonMatchingHeaderLines(strArr);
    }

    /* access modifiers changed from: protected */
    public void updateHeaders() throws MessagingException {
        updateHeaders(this);
        if (this.cachedContent != null) {
            this.dh = new DataHandler(this.cachedContent, getContentType());
            this.cachedContent = null;
            this.content = null;
            if (this.contentStream != null) {
                try {
                    this.contentStream.close();
                } catch (IOException e) {
                }
            }
            this.contentStream = null;
        }
    }

    static boolean isMimeType(MimePart mimePart, String str) throws MessagingException {
        try {
            return new ContentType(mimePart.getContentType()).match(str);
        } catch (ParseException e) {
            return mimePart.getContentType().equalsIgnoreCase(str);
        }
    }

    static void setText(MimePart mimePart, String str, String str2, String str3) throws MessagingException {
        if (str2 == null) {
            if (MimeUtility.checkAscii(str) != 1) {
                str2 = MimeUtility.getDefaultMIMECharset();
            } else {
                str2 = "us-ascii";
            }
        }
        mimePart.setContent(str, "text/" + str3 + "; charset=" + MimeUtility.quote(str2, HeaderTokenizer.MIME));
    }

    static String getDisposition(MimePart mimePart) throws MessagingException {
        String header = mimePart.getHeader("Content-Disposition", null);
        if (header == null) {
            return null;
        }
        return new ContentDisposition(header).getDisposition();
    }

    static void setDisposition(MimePart mimePart, String str) throws MessagingException {
        if (str == null) {
            mimePart.removeHeader("Content-Disposition");
            return;
        }
        String header = mimePart.getHeader("Content-Disposition", null);
        if (header != null) {
            ContentDisposition contentDisposition = new ContentDisposition(header);
            contentDisposition.setDisposition(str);
            str = contentDisposition.toString();
        }
        mimePart.setHeader("Content-Disposition", str);
    }

    static String getDescription(MimePart mimePart) throws MessagingException {
        String header = mimePart.getHeader("Content-Description", null);
        if (header == null) {
            return null;
        }
        try {
            return MimeUtility.decodeText(MimeUtility.unfold(header));
        } catch (UnsupportedEncodingException e) {
            return header;
        }
    }

    static void setDescription(MimePart mimePart, String str, String str2) throws MessagingException {
        if (str == null) {
            mimePart.removeHeader("Content-Description");
            return;
        }
        try {
            mimePart.setHeader("Content-Description", MimeUtility.fold(21, MimeUtility.encodeText(str, str2, null)));
        } catch (UnsupportedEncodingException e) {
            throw new MessagingException("Encoding error", e);
        }
    }

    static String getFileName(MimePart mimePart) throws MessagingException {
        String str;
        String header;
        String header2 = mimePart.getHeader("Content-Disposition", null);
        if (header2 != null) {
            str = new ContentDisposition(header2).getParameter("filename");
        } else {
            str = null;
        }
        if (str == null && (header = mimePart.getHeader("Content-Type", null)) != null) {
            try {
                str = new ContentType(header).getParameter("name");
            } catch (ParseException e) {
            }
        }
        if (!decodeFileName || str == null) {
            return str;
        }
        try {
            return MimeUtility.decodeText(str);
        } catch (UnsupportedEncodingException e2) {
            throw new MessagingException("Can't decode filename", e2);
        }
    }

    static void setFileName(MimePart mimePart, String str) throws MessagingException {
        String header;
        if (encodeFileName && str != null) {
            try {
                str = MimeUtility.encodeText(str);
            } catch (UnsupportedEncodingException e) {
                throw new MessagingException("Can't encode filename", e);
            }
        }
        String header2 = mimePart.getHeader("Content-Disposition", null);
        if (header2 == null) {
            header2 = Part.ATTACHMENT;
        }
        ContentDisposition contentDisposition = new ContentDisposition(header2);
        contentDisposition.setParameter("filename", str);
        mimePart.setHeader("Content-Disposition", contentDisposition.toString());
        if (setContentTypeFileName && (header = mimePart.getHeader("Content-Type", null)) != null) {
            try {
                ContentType contentType = new ContentType(header);
                contentType.setParameter("name", str);
                mimePart.setHeader("Content-Type", contentType.toString());
            } catch (ParseException e2) {
            }
        }
    }

    static String[] getContentLanguage(MimePart mimePart) throws MessagingException {
        String header = mimePart.getHeader("Content-Language", null);
        if (header == null) {
            return null;
        }
        HeaderTokenizer headerTokenizer = new HeaderTokenizer(header, HeaderTokenizer.MIME);
        Vector vector = new Vector();
        while (true) {
            HeaderTokenizer.Token next = headerTokenizer.next();
            int type = next.getType();
            if (type == -4) {
                break;
            } else if (type == -1) {
                vector.addElement(next.getValue());
            }
        }
        if (vector.size() == 0) {
            return null;
        }
        String[] strArr = new String[vector.size()];
        vector.copyInto(strArr);
        return strArr;
    }

    static void setContentLanguage(MimePart mimePart, String[] strArr) throws MessagingException {
        StringBuffer stringBuffer = new StringBuffer(strArr[0]);
        for (int i = 1; i < strArr.length; i++) {
            stringBuffer.append(',').append(strArr[i]);
        }
        mimePart.setHeader("Content-Language", stringBuffer.toString());
    }

    static String getEncoding(MimePart mimePart) throws MessagingException {
        HeaderTokenizer.Token next;
        int type;
        String header = mimePart.getHeader("Content-Transfer-Encoding", null);
        if (header == null) {
            return null;
        }
        String trim = header.trim();
        if (trim.equalsIgnoreCase("7bit") || trim.equalsIgnoreCase("8bit") || trim.equalsIgnoreCase("quoted-printable") || trim.equalsIgnoreCase("binary") || trim.equalsIgnoreCase("base64")) {
            return trim;
        }
        HeaderTokenizer headerTokenizer = new HeaderTokenizer(trim, HeaderTokenizer.MIME);
        do {
            next = headerTokenizer.next();
            type = next.getType();
            if (type == -4) {
                return trim;
            }
        } while (type != -1);
        return next.getValue();
    }

    static void setEncoding(MimePart mimePart, String str) throws MessagingException {
        mimePart.setHeader("Content-Transfer-Encoding", str);
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x007d A[Catch:{ IOException -> 0x00a3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:61:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void updateHeaders(javax.mail.internet.MimePart r8) throws javax.mail.MessagingException {
        /*
            r1 = 0
            r3 = 1
            javax.activation.DataHandler r5 = r8.getDataHandler()
            if (r5 != 0) goto L_0x0009
        L_0x0008:
            return
        L_0x0009:
            java.lang.String r2 = r5.getContentType()     // Catch:{ IOException -> 0x00a3 }
            java.lang.String r4 = "Content-Type"
            java.lang.String[] r4 = r8.getHeader(r4)     // Catch:{ IOException -> 0x00a3 }
            if (r4 != 0) goto L_0x00ac
            r4 = r3
        L_0x0016:
            javax.mail.internet.ContentType r6 = new javax.mail.internet.ContentType     // Catch:{ IOException -> 0x00a3 }
            r6.<init>(r2)     // Catch:{ IOException -> 0x00a3 }
            java.lang.String r7 = "multipart/*"
            boolean r7 = r6.match(r7)     // Catch:{ IOException -> 0x00a3 }
            if (r7 == 0) goto L_0x00fe
            boolean r1 = r8 instanceof javax.mail.internet.MimeBodyPart     // Catch:{ IOException -> 0x00a3 }
            if (r1 == 0) goto L_0x00b5
            r0 = r8
            javax.mail.internet.MimeBodyPart r0 = (javax.mail.internet.MimeBodyPart) r0     // Catch:{ IOException -> 0x00a3 }
            r1 = r0
            java.lang.Object r7 = r1.cachedContent     // Catch:{ IOException -> 0x00a3 }
            if (r7 == 0) goto L_0x00af
            java.lang.Object r1 = r1.cachedContent     // Catch:{ IOException -> 0x00a3 }
        L_0x0031:
            boolean r7 = r1 instanceof javax.mail.internet.MimeMultipart     // Catch:{ IOException -> 0x00a3 }
            if (r7 == 0) goto L_0x00d1
            javax.mail.internet.MimeMultipart r1 = (javax.mail.internet.MimeMultipart) r1     // Catch:{ IOException -> 0x00a3 }
            r1.updateHeaders()     // Catch:{ IOException -> 0x00a3 }
            r1 = r3
        L_0x003b:
            if (r1 != 0) goto L_0x010f
            java.lang.String r1 = "Content-Transfer-Encoding"
            java.lang.String[] r1 = r8.getHeader(r1)     // Catch:{ IOException -> 0x00a3 }
            if (r1 != 0) goto L_0x004c
            java.lang.String r1 = javax.mail.internet.MimeUtility.getEncoding(r5)     // Catch:{ IOException -> 0x00a3 }
            setEncoding(r8, r1)     // Catch:{ IOException -> 0x00a3 }
        L_0x004c:
            if (r4 == 0) goto L_0x010f
            boolean r1 = javax.mail.internet.MimeBodyPart.setDefaultTextCharset     // Catch:{ IOException -> 0x00a3 }
            if (r1 == 0) goto L_0x010f
            java.lang.String r1 = "text/*"
            boolean r1 = r6.match(r1)     // Catch:{ IOException -> 0x00a3 }
            if (r1 == 0) goto L_0x010f
            java.lang.String r1 = "charset"
            java.lang.String r1 = r6.getParameter(r1)     // Catch:{ IOException -> 0x00a3 }
            if (r1 != 0) goto L_0x010f
            java.lang.String r1 = r8.getEncoding()     // Catch:{ IOException -> 0x00a3 }
            if (r1 == 0) goto L_0x0109
            java.lang.String r2 = "7bit"
            boolean r1 = r1.equalsIgnoreCase(r2)     // Catch:{ IOException -> 0x00a3 }
            if (r1 == 0) goto L_0x0109
            java.lang.String r1 = "us-ascii"
        L_0x0072:
            java.lang.String r2 = "charset"
            r6.setParameter(r2, r1)     // Catch:{ IOException -> 0x00a3 }
            java.lang.String r1 = r6.toString()     // Catch:{ IOException -> 0x00a3 }
        L_0x007b:
            if (r4 == 0) goto L_0x0008
            java.lang.String r2 = "Content-Disposition"
            r3 = 0
            java.lang.String r2 = r8.getHeader(r2, r3)     // Catch:{ IOException -> 0x00a3 }
            if (r2 == 0) goto L_0x009c
            javax.mail.internet.ContentDisposition r3 = new javax.mail.internet.ContentDisposition     // Catch:{ IOException -> 0x00a3 }
            r3.<init>(r2)     // Catch:{ IOException -> 0x00a3 }
            java.lang.String r2 = "filename"
            java.lang.String r2 = r3.getParameter(r2)     // Catch:{ IOException -> 0x00a3 }
            if (r2 == 0) goto L_0x009c
            java.lang.String r1 = "name"
            r6.setParameter(r1, r2)     // Catch:{ IOException -> 0x00a3 }
            java.lang.String r1 = r6.toString()     // Catch:{ IOException -> 0x00a3 }
        L_0x009c:
            java.lang.String r2 = "Content-Type"
            r8.setHeader(r2, r1)     // Catch:{ IOException -> 0x00a3 }
            goto L_0x0008
        L_0x00a3:
            r1 = move-exception
            javax.mail.MessagingException r2 = new javax.mail.MessagingException
            java.lang.String r3 = "IOException updating headers"
            r2.<init>(r3, r1)
            throw r2
        L_0x00ac:
            r4 = r1
            goto L_0x0016
        L_0x00af:
            java.lang.Object r1 = r5.getContent()     // Catch:{ IOException -> 0x00a3 }
            goto L_0x0031
        L_0x00b5:
            boolean r1 = r8 instanceof javax.mail.internet.MimeMessage     // Catch:{ IOException -> 0x00a3 }
            if (r1 == 0) goto L_0x00cb
            r0 = r8
            javax.mail.internet.MimeMessage r0 = (javax.mail.internet.MimeMessage) r0     // Catch:{ IOException -> 0x00a3 }
            r1 = r0
            java.lang.Object r7 = r1.cachedContent     // Catch:{ IOException -> 0x00a3 }
            if (r7 == 0) goto L_0x00c5
            java.lang.Object r1 = r1.cachedContent     // Catch:{ IOException -> 0x00a3 }
            goto L_0x0031
        L_0x00c5:
            java.lang.Object r1 = r5.getContent()     // Catch:{ IOException -> 0x00a3 }
            goto L_0x0031
        L_0x00cb:
            java.lang.Object r1 = r5.getContent()     // Catch:{ IOException -> 0x00a3 }
            goto L_0x0031
        L_0x00d1:
            javax.mail.MessagingException r3 = new javax.mail.MessagingException     // Catch:{ IOException -> 0x00a3 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00a3 }
            java.lang.String r5 = "MIME part of type \""
            r4.<init>(r5)     // Catch:{ IOException -> 0x00a3 }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ IOException -> 0x00a3 }
            java.lang.String r4 = "\" contains object of type "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ IOException -> 0x00a3 }
            java.lang.Class r1 = r1.getClass()     // Catch:{ IOException -> 0x00a3 }
            java.lang.String r1 = r1.getName()     // Catch:{ IOException -> 0x00a3 }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ IOException -> 0x00a3 }
            java.lang.String r2 = " instead of MimeMultipart"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ IOException -> 0x00a3 }
            java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x00a3 }
            r3.<init>(r1)     // Catch:{ IOException -> 0x00a3 }
            throw r3     // Catch:{ IOException -> 0x00a3 }
        L_0x00fe:
            java.lang.String r7 = "message/rfc822"
            boolean r7 = r6.match(r7)     // Catch:{ IOException -> 0x00a3 }
            if (r7 == 0) goto L_0x003b
            r1 = r3
            goto L_0x003b
        L_0x0109:
            java.lang.String r1 = javax.mail.internet.MimeUtility.getDefaultMIMECharset()     // Catch:{ IOException -> 0x00a3 }
            goto L_0x0072
        L_0x010f:
            r1 = r2
            goto L_0x007b
        */
        throw new UnsupportedOperationException("Method not decompiled: javax.mail.internet.MimeBodyPart.updateHeaders(javax.mail.internet.MimePart):void");
    }

    static void invalidateContentHeaders(MimePart mimePart) throws MessagingException {
        mimePart.removeHeader("Content-Type");
        mimePart.removeHeader("Content-Transfer-Encoding");
    }

    static void writeTo(MimePart mimePart, OutputStream outputStream, String[] strArr) throws IOException, MessagingException {
        LineOutputStream lineOutputStream;
        if (outputStream instanceof LineOutputStream) {
            lineOutputStream = (LineOutputStream) outputStream;
        } else {
            lineOutputStream = new LineOutputStream(outputStream);
        }
        Enumeration nonMatchingHeaderLines = mimePart.getNonMatchingHeaderLines(strArr);
        while (nonMatchingHeaderLines.hasMoreElements()) {
            lineOutputStream.writeln((String) nonMatchingHeaderLines.nextElement());
        }
        lineOutputStream.writeln();
        OutputStream encode = MimeUtility.encode(outputStream, mimePart.getEncoding());
        mimePart.getDataHandler().writeTo(encode);
        encode.flush();
    }
}
