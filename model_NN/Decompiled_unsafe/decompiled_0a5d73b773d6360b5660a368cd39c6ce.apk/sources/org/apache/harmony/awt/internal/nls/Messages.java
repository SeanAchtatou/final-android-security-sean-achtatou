package org.apache.harmony.awt.internal.nls;

import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class Messages {
    private static ResourceBundle bundle;

    static {
        bundle = null;
        try {
            bundle = setLocale(Locale.getDefault(), "org.apache.harmony.awt.internal.nls.messages");
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public static String getString(String str) {
        if (bundle == null) {
            return str;
        }
        try {
            return bundle.getString(str);
        } catch (MissingResourceException e) {
            return "Missing message: " + str;
        }
    }

    public static String getString(String str, Object obj) {
        return getString(str, new Object[]{obj});
    }

    public static String getString(String str, int i) {
        return getString(str, new Object[]{Integer.toString(i)});
    }

    public static String getString(String str, char c) {
        return getString(str, new Object[]{String.valueOf(c)});
    }

    public static String getString(String str, Object obj, Object obj2) {
        return getString(str, new Object[]{obj, obj2});
    }

    public static String getString(String str, Object[] objArr) {
        if (bundle != null) {
            try {
                str = bundle.getString(str);
            } catch (MissingResourceException e) {
            }
        }
        return format(str, objArr);
    }

    public static String format(String str, Object[] objArr) {
        int i;
        int i2 = 0;
        StringBuilder sb = new StringBuilder(str.length() + (objArr.length * 20));
        String[] strArr = new String[objArr.length];
        for (int i3 = 0; i3 < objArr.length; i3++) {
            if (objArr[i3] == null) {
                strArr[i3] = "<null>";
            } else {
                strArr[i3] = objArr[i3].toString();
            }
        }
        int indexOf = str.indexOf(123, 0);
        while (indexOf >= 0) {
            if (indexOf != 0 && str.charAt(indexOf - 1) == '\\') {
                if (indexOf != 1) {
                    sb.append(str.substring(i2, indexOf - 1));
                }
                sb.append('{');
                i = indexOf + 1;
            } else if (indexOf > str.length() - 3) {
                sb.append(str.substring(i2, str.length()));
                i = str.length();
            } else {
                byte digit = (byte) Character.digit(str.charAt(indexOf + 1), 10);
                if (digit < 0 || str.charAt(indexOf + 2) != '}') {
                    sb.append(str.substring(i2, indexOf + 1));
                    i = indexOf + 1;
                } else {
                    sb.append(str.substring(i2, indexOf));
                    if (digit >= strArr.length) {
                        sb.append("<missing argument>");
                    } else {
                        sb.append(strArr[digit]);
                    }
                    i = indexOf + 3;
                }
            }
            i2 = i;
            indexOf = str.indexOf(123, i);
        }
        if (i2 < str.length()) {
            sb.append(str.substring(i2, str.length()));
        }
        return sb.toString();
    }

    public static ResourceBundle setLocale(final Locale locale, final String str) {
        try {
            return (ResourceBundle) AccessController.doPrivileged(new PrivilegedAction<Object>(null) {
                public Object run() {
                    return ResourceBundle.getBundle(str, locale, null != null ? null : ClassLoader.getSystemClassLoader());
                }
            });
        } catch (MissingResourceException e) {
            return null;
        }
    }
}
