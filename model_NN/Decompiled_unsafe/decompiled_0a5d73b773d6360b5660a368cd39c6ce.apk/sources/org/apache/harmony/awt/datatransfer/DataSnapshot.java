package org.apache.harmony.awt.datatransfer;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.SystemFlavorMap;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class DataSnapshot implements DataProvider {
    private final String[] fileList;
    private final String html;
    private final String[] nativeFormats;
    private final RawBitmap rawBitmap;
    private final Map<Class<?>, byte[]> serializedObjects = Collections.synchronizedMap(new HashMap());
    private final String text;
    private final String url;

    public DataSnapshot(DataProvider dataProvider) {
        Class representationClass;
        byte[] serializedObject;
        this.nativeFormats = dataProvider.getNativeFormats();
        this.text = dataProvider.getText();
        this.fileList = dataProvider.getFileList();
        this.url = dataProvider.getURL();
        this.html = dataProvider.getHTML();
        this.rawBitmap = dataProvider.getRawBitmap();
        for (int i = 0; i < this.nativeFormats.length; i++) {
            DataFlavor dataFlavor = null;
            try {
                dataFlavor = SystemFlavorMap.decodeDataFlavor(this.nativeFormats[i]);
            } catch (ClassNotFoundException e) {
            }
            if (!(dataFlavor == null || (serializedObject = dataProvider.getSerializedObject((representationClass = dataFlavor.getRepresentationClass()))) == null)) {
                this.serializedObjects.put(representationClass, serializedObject);
            }
        }
    }

    public boolean isNativeFormatAvailable(String str) {
        if (str == null) {
            return false;
        }
        if (str.equals("text/plain")) {
            if (this.text != null) {
                return true;
            }
            return false;
        } else if (str.equals("application/x-java-file-list")) {
            if (this.fileList != null) {
                return true;
            }
            return false;
        } else if (str.equals("application/x-java-url")) {
            if (this.url != null) {
                return true;
            }
            return false;
        } else if (str.equals("text/html")) {
            if (this.html != null) {
                return true;
            }
            return false;
        } else if (!str.equals("image/x-java-image")) {
            try {
                return this.serializedObjects.containsKey(SystemFlavorMap.decodeDataFlavor(str).getRepresentationClass());
            } catch (Exception e) {
                return false;
            }
        } else if (this.rawBitmap != null) {
            return true;
        } else {
            return false;
        }
    }

    public String getText() {
        return this.text;
    }

    public String[] getFileList() {
        return this.fileList;
    }

    public String getURL() {
        return this.url;
    }

    public String getHTML() {
        return this.html;
    }

    public RawBitmap getRawBitmap() {
        return this.rawBitmap;
    }

    public int[] getRawBitmapHeader() {
        if (this.rawBitmap != null) {
            return this.rawBitmap.getHeader();
        }
        return null;
    }

    public byte[] getRawBitmapBuffer8() {
        if (this.rawBitmap == null || !(this.rawBitmap.buffer instanceof byte[])) {
            return null;
        }
        return (byte[]) this.rawBitmap.buffer;
    }

    public short[] getRawBitmapBuffer16() {
        if (this.rawBitmap == null || !(this.rawBitmap.buffer instanceof short[])) {
            return null;
        }
        return (short[]) this.rawBitmap.buffer;
    }

    public int[] getRawBitmapBuffer32() {
        if (this.rawBitmap == null || !(this.rawBitmap.buffer instanceof int[])) {
            return null;
        }
        return (int[]) this.rawBitmap.buffer;
    }

    public byte[] getSerializedObject(Class<?> cls) {
        return this.serializedObjects.get(cls);
    }

    public byte[] getSerializedObject(String str) {
        try {
            return getSerializedObject(SystemFlavorMap.decodeDataFlavor(str).getRepresentationClass());
        } catch (Exception e) {
            return null;
        }
    }

    public String[] getNativeFormats() {
        return this.nativeFormats;
    }
}
