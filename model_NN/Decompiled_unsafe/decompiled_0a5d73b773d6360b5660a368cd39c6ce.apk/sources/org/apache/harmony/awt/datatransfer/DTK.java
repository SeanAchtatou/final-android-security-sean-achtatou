package org.apache.harmony.awt.datatransfer;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.SystemFlavorMap;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DropTargetContext;
import java.awt.dnd.peer.DragSourceContextPeer;
import java.awt.dnd.peer.DropTargetContextPeer;
import java.nio.charset.Charset;
import org.apache.harmony.awt.internal.nls.Messages;
import org.apache.harmony.misc.SystemUtils;

public abstract class DTK {
    protected final DataTransferThread dataTransferThread = new DataTransferThread(this);
    private NativeClipboard nativeClipboard = null;
    private NativeClipboard nativeSelection = null;
    protected SystemFlavorMap systemFlavorMap;

    public abstract DragSourceContextPeer createDragSourceContextPeer(DragGestureEvent dragGestureEvent);

    public abstract DropTargetContextPeer createDropTargetContextPeer(DropTargetContext dropTargetContext);

    public abstract void initDragAndDrop();

    /* access modifiers changed from: protected */
    public abstract NativeClipboard newNativeClipboard();

    /* access modifiers changed from: protected */
    public abstract NativeClipboard newNativeSelection();

    public abstract void runEventLoop();

    protected DTK() {
        this.dataTransferThread.start();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        return r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static org.apache.harmony.awt.datatransfer.DTK getDTK() {
        /*
            java.lang.Object r1 = org.apache.harmony.awt.ContextStorage.getContextLock()
            monitor-enter(r1)
            boolean r0 = org.apache.harmony.awt.ContextStorage.shutdownPending()     // Catch:{ all -> 0x001d }
            if (r0 == 0) goto L_0x000e
            monitor-exit(r1)     // Catch:{ all -> 0x001d }
            r0 = 0
        L_0x000d:
            return r0
        L_0x000e:
            org.apache.harmony.awt.datatransfer.DTK r0 = org.apache.harmony.awt.ContextStorage.getDTK()     // Catch:{ all -> 0x001d }
            if (r0 != 0) goto L_0x001b
            org.apache.harmony.awt.datatransfer.DTK r0 = createDTK()     // Catch:{ all -> 0x001d }
            org.apache.harmony.awt.ContextStorage.setDTK(r0)     // Catch:{ all -> 0x001d }
        L_0x001b:
            monitor-exit(r1)     // Catch:{ all -> 0x001d }
            goto L_0x000d
        L_0x001d:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x001d }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.harmony.awt.datatransfer.DTK.getDTK():org.apache.harmony.awt.datatransfer.DTK");
    }

    public synchronized SystemFlavorMap getSystemFlavorMap() {
        return this.systemFlavorMap;
    }

    public synchronized void setSystemFlavorMap(SystemFlavorMap systemFlavorMap2) {
        this.systemFlavorMap = systemFlavorMap2;
    }

    public NativeClipboard getNativeClipboard() {
        if (this.nativeClipboard == null) {
            this.nativeClipboard = newNativeClipboard();
        }
        return this.nativeClipboard;
    }

    public NativeClipboard getNativeSelection() {
        if (this.nativeSelection == null) {
            this.nativeSelection = newNativeSelection();
        }
        return this.nativeSelection;
    }

    private static DTK createDTK() {
        String str;
        switch (SystemUtils.getOS()) {
            case 1:
                str = "org.apache.harmony.awt.datatransfer.windows.WinDTK";
                break;
            case 2:
                str = "org.apache.harmony.awt.datatransfer.linux.LinuxDTK";
                break;
            default:
                throw new RuntimeException(Messages.getString("awt.4E"));
        }
        try {
            return (DTK) Class.forName(str).newInstance();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public String getDefaultCharset() {
        return "unicode";
    }

    /* access modifiers changed from: protected */
    public String[] getCharsets() {
        return new String[]{"UTF-16", "UTF-8", "unicode", "ISO-8859-1", "US-ASCII"};
    }

    public void initSystemFlavorMap(SystemFlavorMap systemFlavorMap2) {
        String[] charsets = getCharsets();
        appendSystemFlavorMap(systemFlavorMap2, DataFlavor.stringFlavor, "text/plain");
        appendSystemFlavorMap(systemFlavorMap2, charsets, "plain", "text/plain");
        appendSystemFlavorMap(systemFlavorMap2, charsets, "html", "text/html");
        appendSystemFlavorMap(systemFlavorMap2, DataProvider.urlFlavor, "application/x-java-url");
        appendSystemFlavorMap(systemFlavorMap2, charsets, "uri-list", "application/x-java-url");
        appendSystemFlavorMap(systemFlavorMap2, DataFlavor.javaFileListFlavor, "application/x-java-file-list");
        appendSystemFlavorMap(systemFlavorMap2, DataFlavor.imageFlavor, "image/x-java-image");
    }

    /* access modifiers changed from: protected */
    public void appendSystemFlavorMap(SystemFlavorMap systemFlavorMap2, DataFlavor dataFlavor, String str) {
        systemFlavorMap2.addFlavorForUnencodedNative(str, dataFlavor);
        systemFlavorMap2.addUnencodedNativeForFlavor(dataFlavor, str);
    }

    /* access modifiers changed from: protected */
    public void appendSystemFlavorMap(SystemFlavorMap systemFlavorMap2, String[] strArr, String str, String str2) {
        TextFlavor.addUnicodeClasses(systemFlavorMap2, str2, str);
        for (int i = 0; i < strArr.length; i++) {
            if (strArr[i] != null && Charset.isSupported(strArr[i])) {
                TextFlavor.addCharsetClasses(systemFlavorMap2, str2, str, strArr[i]);
            }
        }
    }
}
