package org.apache.harmony.awt.datatransfer;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.SystemFlavorMap;
import java.awt.datatransfer.Transferable;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.awt.image.ImageObserver;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Reader;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class DataSource implements DataProvider {
    protected final Transferable contents;
    private DataFlavor[] flavors;
    private List<String> nativeFormats;

    public DataSource(Transferable transferable) {
        this.contents = transferable;
    }

    private boolean isHtmlFlavor(DataFlavor dataFlavor) {
        return "html".equalsIgnoreCase(dataFlavor.getSubType());
    }

    /* access modifiers changed from: protected */
    public DataFlavor[] getDataFlavors() {
        if (this.flavors == null) {
            this.flavors = this.contents.getTransferDataFlavors();
        }
        return this.flavors;
    }

    public String[] getNativeFormats() {
        return (String[]) getNativeFormatsList().toArray(new String[0]);
    }

    public List<String> getNativeFormatsList() {
        if (this.nativeFormats == null) {
            this.nativeFormats = getNativesForFlavors(getDataFlavors());
        }
        return this.nativeFormats;
    }

    private static List<String> getNativesForFlavors(DataFlavor[] dataFlavorArr) {
        ArrayList arrayList = new ArrayList();
        SystemFlavorMap defaultFlavorMap = SystemFlavorMap.getDefaultFlavorMap();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= dataFlavorArr.length) {
                return arrayList;
            }
            for (String str : defaultFlavorMap.getNativesForFlavor(dataFlavorArr[i2])) {
                if (!arrayList.contains(str)) {
                    arrayList.add(str);
                }
            }
            i = i2 + 1;
        }
    }

    private String getTextFromReader(Reader reader) throws IOException {
        StringBuilder sb = new StringBuilder();
        char[] cArr = new char[1024];
        while (true) {
            int read = reader.read(cArr);
            if (read <= 0) {
                return sb.toString();
            }
            sb.append(cArr, 0, read);
        }
    }

    private String getText(boolean z) {
        DataFlavor[] transferDataFlavors = this.contents.getTransferDataFlavors();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= transferDataFlavors.length) {
                return null;
            }
            DataFlavor dataFlavor = transferDataFlavors[i2];
            if (dataFlavor.isFlavorTextType() && (!z || isHtmlFlavor(dataFlavor))) {
                try {
                    if (String.class.isAssignableFrom(dataFlavor.getRepresentationClass())) {
                        return (String) this.contents.getTransferData(dataFlavor);
                    }
                    return getTextFromReader(dataFlavor.getReaderForText(this.contents));
                } catch (Exception e) {
                }
            }
            i = i2 + 1;
        }
    }

    public String getText() {
        return getText(false);
    }

    public String[] getFileList() {
        try {
            List list = (List) this.contents.getTransferData(DataFlavor.javaFileListFlavor);
            return (String[]) list.toArray(new String[list.size()]);
        } catch (Exception e) {
            return null;
        }
    }

    public String getURL() {
        try {
            return ((URL) this.contents.getTransferData(urlFlavor)).toString();
        } catch (Exception e) {
            try {
                return ((URL) this.contents.getTransferData(uriFlavor)).toString();
            } catch (Exception e2) {
                try {
                    return new URL(getText()).toString();
                } catch (Exception e3) {
                    return null;
                }
            }
        }
    }

    public String getHTML() {
        return getText(true);
    }

    public RawBitmap getRawBitmap() {
        DataFlavor[] transferDataFlavors = this.contents.getTransferDataFlavors();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= transferDataFlavors.length) {
                return null;
            }
            DataFlavor dataFlavor = transferDataFlavors[i2];
            Class representationClass = dataFlavor.getRepresentationClass();
            if (representationClass != null && Image.class.isAssignableFrom(representationClass) && (dataFlavor.isMimeTypeEqual(DataFlavor.imageFlavor) || dataFlavor.isFlavorSerializedObjectType())) {
                try {
                    return getImageBitmap((Image) this.contents.getTransferData(dataFlavor));
                } catch (Throwable th) {
                }
            }
            i = i2 + 1;
        }
    }

    private RawBitmap getImageBitmap(Image image) {
        if (image instanceof BufferedImage) {
            BufferedImage bufferedImage = (BufferedImage) image;
            if (bufferedImage.getType() == 1) {
                return getImageBitmap32(bufferedImage);
            }
        }
        int width = image.getWidth((ImageObserver) null);
        int height = image.getHeight((ImageObserver) null);
        if (width <= 0 || height <= 0) {
            return null;
        }
        BufferedImage bufferedImage2 = new BufferedImage(width, height, 1);
        Graphics graphics = bufferedImage2.getGraphics();
        graphics.drawImage(image, 0, 0, (ImageObserver) null);
        graphics.dispose();
        return getImageBitmap32(bufferedImage2);
    }

    private RawBitmap getImageBitmap32(BufferedImage bufferedImage) {
        int[] iArr = new int[(bufferedImage.getWidth() * bufferedImage.getHeight())];
        DataBufferInt dataBuffer = bufferedImage.getRaster().getDataBuffer();
        int numBanks = dataBuffer.getNumBanks();
        int[] offsets = dataBuffer.getOffsets();
        int i = 0;
        for (int i2 = 0; i2 < numBanks; i2++) {
            int[] data = dataBuffer.getData(i2);
            System.arraycopy(data, offsets[i2], iArr, i, data.length - offsets[i2]);
            i += data.length - offsets[i2];
        }
        return new RawBitmap(bufferedImage.getWidth(), bufferedImage.getHeight(), bufferedImage.getWidth(), 32, 16711680, 65280, 255, iArr);
    }

    public byte[] getSerializedObject(Class<?> cls) {
        try {
            DataFlavor dataFlavor = new DataFlavor(cls, (String) null);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            new ObjectOutputStream(byteArrayOutputStream).writeObject((Serializable) this.contents.getTransferData(dataFlavor));
            return byteArrayOutputStream.toByteArray();
        } catch (Throwable th) {
            return null;
        }
    }

    public boolean isNativeFormatAvailable(String str) {
        return getNativeFormatsList().contains(str);
    }
}
