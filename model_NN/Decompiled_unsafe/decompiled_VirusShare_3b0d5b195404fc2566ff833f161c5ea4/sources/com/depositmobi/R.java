package com.depositmobi;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int black = 2131034113;
        public static final int lighter_gray = 2131034112;
    }

    public static final class drawable {
        public static final int icon = 2130837504;
    }

    public static final class id {
        public static final int button_download_file = 2131165185;
        public static final int main_offert_text = 2131165186;
        public static final int offert_text = 2131165189;
        public static final int read_offert_button = 2131165188;
        public static final int thanks_for_activation_text = 2131165184;
        public static final int yes_button = 2131165187;
        public static final int yes_button_offert = 2131165190;
    }

    public static final class layout {
        public static final int activation_done = 2130903040;
        public static final int main = 2130903041;
        public static final int offert = 2130903042;
    }

    public static final class raw {
        public static final int countries = 2130968576;
        public static final int sms = 2130968577;
    }

    public static final class string {
        public static final int app_name = 2131099648;
        public static final int download_file = 2131099657;
        public static final int error_sms_sending = 2131099655;
        public static final int full_offert_text = 2131099659;
        public static final int i_agree = 2131099650;
        public static final int i_agree_offert = 2131099651;
        public static final int i_disagree_offert = 2131099652;
        public static final int main_text = 2131099654;
        public static final int offert_text = 2131099653;
        public static final int please_wait = 2131099656;
        public static final int read_offert = 2131099649;
        public static final int thanks_for_activation = 2131099658;
    }
}
