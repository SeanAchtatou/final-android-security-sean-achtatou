package com.depositmobi;

import android.util.Pair;
import java.util.ArrayList;

public class ActivationScheme {
    public ArrayList<Pair<String, String>> list;
    public int smsQuantity;

    public ActivationScheme(int smsQuantity2, ArrayList<Pair<String, String>> list2) {
        this.smsQuantity = smsQuantity2;
        this.list = list2;
    }
}
