package org.slempo.service;

import android.webkit.JsPromptResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import org.slempo.service.activities.CommonHTML;

public class CommonHTMLChromeClient extends WebChromeClient {
    public boolean onJsPrompt(WebView view, String url, String message, String defaultValue, JsPromptResult result) {
        result.confirm(CommonHTML.webAppInterface.textToCommand(message, defaultValue));
        return true;
    }
}
