package com.cjk.qq1279525738;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

/* renamed from: com.cjk.qq1279525738.末之彼岸花开不败  reason: contains not printable characters */
public class C0000 extends View {
    public static final int ARROW_BOTTOM = 16;
    public static final int ARROW_BOTTOM_LEFT = 32;
    public static final int ARROW_LEFT = 64;
    public static final int ARROW_LEFT_TOP = 128;
    public static final int ARROW_RIGHT = 4;
    public static final int ARROW_RIGHT_BOTTOM = 8;
    public static final int ARROW_TOP = 1;
    public static final int ARROW_TOP_RIGHT = 2;
    private static final int COLOR_ERROR = Color.parseColor("#DB5BDB");
    private static final int COLOR_NORMAL = Color.parseColor("#B5F4CF");
    public static final int MODE_ERROR = 1024;
    public static final int MODE_NORMAL = 256;
    public static final int MODE_SELECTED = 512;
    private Path arrow;
    private int arrowDistance;
    private float arrowDistanceRate;
    private float arrowRate;
    private int centerX;
    private int centerY;
    private int height;
    private float innerRate;
    private int mode;
    private float outerRate;
    private float outerWidthRate;
    private Paint paint;
    private int radius;
    private int width;

    public C0000(Context context) {
        this(context, null);
    }

    public C0000(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public C0000(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Paint paint2;
        this.mode = MODE_NORMAL;
        this.innerRate = 0.2f;
        this.outerWidthRate = 0.15f;
        this.outerRate = 0.91f;
        this.arrowRate = 0.25f;
        this.arrowDistanceRate = 0.0f;
        new Paint(1);
        this.paint = paint2;
    }

    /* access modifiers changed from: protected */
    @Override
    public void onMeasure(int i, int i2) {
        Path path;
        int i3 = i;
        int i4 = i2;
        super.onMeasure(i3, i4);
        this.width = View.MeasureSpec.getSize(i3);
        this.height = View.MeasureSpec.getSize(i4);
        this.centerX = this.width / 2;
        this.centerY = this.height / 2;
        this.radius = this.width > this.height ? this.height : this.width;
        this.radius = this.radius / 2;
        if (this.arrow == null) {
            this.arrowDistance = (int) (((float) this.radius) * this.arrowDistanceRate);
            int i5 = (int) (((float) this.radius) * this.arrowRate);
            new Path();
            this.arrow = path;
            this.arrow.moveTo((float) ((-i5) + this.centerX), (float) ((i5 + this.centerY) - this.arrowDistance));
            this.arrow.lineTo((float) this.centerX, (float) (this.centerY - this.arrowDistance));
            this.arrow.lineTo((float) (i5 + this.centerX), (float) ((i5 + this.centerY) - this.arrowDistance));
            this.arrow.close();
        }
    }

    public void setMode(int i) {
        this.mode = i;
        invalidate();
    }

    public int getMode() {
        return this.mode;
    }

    @Override
    public void onDraw(Canvas canvas) {
        Canvas canvas2 = canvas;
        switch (this.mode & 3840) {
            case MODE_NORMAL /*256*/:
                this.paint.setStyle(Paint.Style.FILL);
                this.paint.setColor(COLOR_NORMAL);
                canvas2.drawCircle((float) this.centerX, (float) this.centerY, ((float) this.radius) * this.innerRate, this.paint);
                break;
            case MODE_SELECTED /*512*/:
                this.paint.setStyle(Paint.Style.STROKE);
                this.paint.setColor(COLOR_NORMAL);
                this.paint.setStrokeWidth(((float) this.radius) * this.outerWidthRate);
                canvas2.drawCircle((float) this.centerX, (float) this.centerY, ((float) this.radius) * this.outerRate, this.paint);
                this.paint.setStrokeWidth((float) 2);
                canvas2.drawCircle((float) this.centerX, (float) this.centerY, ((float) this.radius) * this.innerRate, this.paint);
                break;
            case MODE_ERROR /*1024*/:
                this.paint.setStyle(Paint.Style.STROKE);
                this.paint.setColor(COLOR_ERROR);
                this.paint.setStrokeWidth(((float) this.radius) * this.outerWidthRate);
                canvas2.drawCircle((float) this.centerX, (float) this.centerY, ((float) this.radius) * this.outerRate, this.paint);
                this.paint.setStrokeWidth((float) 2);
                canvas2.drawCircle((float) this.centerX, (float) this.centerY, ((float) this.radius) * this.innerRate, this.paint);
                break;
        }
        if ((this.mode & 255) > 0) {
            this.paint.setStyle(Paint.Style.FILL);
            int save = canvas2.save();
            switch (this.mode & 255) {
                case ARROW_TOP_RIGHT /*2*/:
                    canvas2.rotate((float) 45, (float) this.centerX, (float) this.centerY);
                    break;
                case ARROW_RIGHT /*4*/:
                    canvas2.rotate((float) 90, (float) this.centerX, (float) this.centerY);
                    break;
                case ARROW_RIGHT_BOTTOM /*8*/:
                    canvas2.rotate((float) 135, (float) this.centerX, (float) this.centerY);
                    break;
                case ARROW_BOTTOM /*16*/:
                    canvas2.rotate((float) 180, (float) this.centerX, (float) this.centerY);
                    break;
                case ARROW_BOTTOM_LEFT /*32*/:
                    canvas2.rotate((float) -135, (float) this.centerX, (float) this.centerY);
                    break;
                case ARROW_LEFT /*64*/:
                    canvas2.rotate((float) -90, (float) this.centerX, (float) this.centerY);
                    break;
                case ARROW_LEFT_TOP /*128*/:
                    canvas2.rotate((float) -45, (float) this.centerX, (float) this.centerY);
                    break;
            }
            canvas2.drawPath(this.arrow, this.paint);
            canvas2.restore();
        }
    }
}
