package com.cjk.qq1279525738;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;

public class sssp extends RelativeLayout {
    public static final int MODE_EDIT = 1;
    public static final int MODE_NORMAL = 0;
    private static final int depth = 3;
    private static final int unmatchedBoundary = 3;
    private int blockGap;
    private int blockWidth;
    private int[] defaultGestures;
    private int gestureCursor;
    private Path gesturePath;
    private int gestureWidth;
    private int[] gesturesContainer;
    private int lastPathX;
    private int lastPathY;
    private int lastX;
    private int lastY;
    private C0000[] lockers;
    private int mode;
    private int[] negativeGestures;
    private OnGestureEventListener onGestureEventListener;
    private Paint paint;
    private boolean touchable;
    private int unmatchedCount;

    public interface OnGestureEventListener {
        void onBlockSelected(int i);

        void onGestureEvent(boolean z);

        void onUnmatchedExceedBoundary();
    }

    public sssp(Context context) {
        this(context, null);
    }

    public sssp(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public sssp(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Paint paint2;
        this.mode = 0;
        this.defaultGestures = new int[]{0, 1, 2, 4, 6};
        this.gestureCursor = 0;
        this.blockWidth = 190;
        this.blockGap = 70;
        this.negativeGestures = new int[9];
        for (int i2 = 0; i2 < this.negativeGestures.length; i2++) {
            this.negativeGestures[i2] = -1;
        }
        this.gesturesContainer = (int[]) this.negativeGestures.clone();
        new Paint(1);
        this.paint = paint2;
        this.paint.setStyle(Paint.Style.STROKE);
        this.paint.setStrokeWidth((float) 20);
        this.paint.setStrokeCap(Paint.Cap.ROUND);
        this.paint.setStrokeJoin(Paint.Join.ROUND);
        this.unmatchedCount = 0;
        this.touchable = true;
    }

    public void setTouchable(boolean z) {
        this.touchable = z;
    }

    public void rewindUnmatchedCount() {
        this.unmatchedCount = 0;
    }

    public void setOnGestureEventListener(OnGestureEventListener onGestureEventListener2) {
        this.onGestureEventListener = onGestureEventListener2;
    }

    /* access modifiers changed from: protected */
    @Override
    public void onMeasure(int i, int i2) {
        C0000 r18;
        RelativeLayout.LayoutParams layoutParams;
        int i3 = i;
        int i4 = i2;
        super.onMeasure(i3, i4);
        int size = View.MeasureSpec.getSize(i3);
        int size2 = View.MeasureSpec.getSize(i4);
        int i5 = size > size2 ? size2 : size;
        if (this.lockers == null) {
            this.blockWidth = (i5 - (this.blockGap * 2)) / 3;
            this.gestureWidth = (this.blockWidth * 3) + (this.blockGap * 2);
            this.lockers = new C0000[9];
            for (int i6 = 0; i6 < this.lockers.length; i6++) {
                new C0000(getContext());
                this.lockers[i6] = r18;
                this.lockers[i6].setId(i6 + 1);
                new RelativeLayout.LayoutParams(this.blockWidth, this.blockWidth);
                RelativeLayout.LayoutParams layoutParams2 = layoutParams;
                if (i6 % 3 != 0) {
                    layoutParams2.addRule(1, this.lockers[i6 - 1].getId());
                }
                if (i6 > 2) {
                    layoutParams2.addRule(3, this.lockers[i6 - 3].getId());
                }
                int i7 = 0;
                int i8 = 0;
                if ((i6 + 1) % 3 != 0) {
                    i7 = this.blockGap;
                }
                if (i6 < 6) {
                    i8 = this.blockGap;
                }
                layoutParams2.setMargins(0, 0, i7, i8);
                addView(this.lockers[i6], layoutParams2);
                this.lockers[i6].setMode(C0000.MODE_NORMAL);
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent motionEvent) {
        Path path;
        MotionEvent motionEvent2 = motionEvent;
        if (this.touchable) {
            switch (motionEvent2.getActionMasked()) {
                case MODE_NORMAL /*0*/:
                    for (int i = 0; i < getChildCount(); i++) {
                        View childAt = getChildAt(i);
                        if (childAt instanceof C0000) {
                            ((C0000) childAt).setMode(C0000.MODE_NORMAL);
                        }
                    }
                    this.gesturePath = null;
                    this.lastX = (int) motionEvent2.getX();
                    this.lastY = (int) motionEvent2.getY();
                    this.lastPathX = this.lastX;
                    this.lastPathY = this.lastY;
                    this.paint.setColor(Color.parseColor("#41D3E0"));
                    break;
                case 1:
                case 3:
                    if (this.gesturesContainer[0] != -1) {
                        boolean z = false;
                        int i2 = 0;
                        while (true) {
                            if (i2 < this.defaultGestures.length) {
                                if (this.gesturesContainer[i2] == this.defaultGestures[i2]) {
                                    z = true;
                                    i2++;
                                } else {
                                    z = false;
                                }
                            }
                        }
                        if (z || this.mode == 1) {
                            this.unmatchedCount = 0;
                        } else {
                            this.unmatchedCount = this.unmatchedCount + 1;
                            this.paint.setColor(Color.parseColor("#FF4F75"));
                            int[] iArr = this.gesturesContainer;
                            for (int i3 = 0; i3 < iArr.length; i3++) {
                                View findViewById = findViewById(iArr[i3] + 1);
                                if (findViewById != null && (findViewById instanceof C0000)) {
                                    ((C0000) findViewById).setMode(C0000.MODE_ERROR);
                                }
                            }
                        }
                        if (this.onGestureEventListener != null) {
                            this.onGestureEventListener.onGestureEvent(z);
                            if (this.unmatchedCount >= 3) {
                                this.onGestureEventListener.onUnmatchedExceedBoundary();
                                this.unmatchedCount = 0;
                            }
                        }
                    }
                    this.gestureCursor = 0;
                    this.gesturesContainer = (int[]) this.negativeGestures.clone();
                    this.lastX = this.lastPathX;
                    this.lastY = this.lastPathY;
                    invalidate();
                    break;
                case C0000.ARROW_TOP_RIGHT:
                    this.lastX = (int) motionEvent2.getX();
                    this.lastY = (int) motionEvent2.getY();
                    int calculateChildIdByCoords = calculateChildIdByCoords(this.lastX, this.lastY);
                    View findViewById2 = findViewById(calculateChildIdByCoords + 1);
                    boolean z2 = false;
                    int[] iArr2 = this.gesturesContainer;
                    int i4 = 0;
                    while (true) {
                        if (i4 < iArr2.length) {
                            if (iArr2[i4] == calculateChildIdByCoords) {
                                z2 = true;
                            } else {
                                i4++;
                            }
                        }
                    }
                    if (findViewById2 != null && (findViewById2 instanceof C0000) && checkChildInCoords(this.lastX, this.lastY, findViewById2)) {
                        ((C0000) findViewById2).setMode(C0000.MODE_SELECTED);
                        if (!z2) {
                            int left = findViewById2.getLeft() + (findViewById2.getWidth() / 2);
                            int top = findViewById2.getTop() + (findViewById2.getHeight() / 2);
                            if (this.gesturePath == null) {
                                new Path();
                                this.gesturePath = path;
                                this.gesturePath.moveTo((float) left, (float) top);
                            } else {
                                this.gesturePath.lineTo((float) left, (float) top);
                            }
                            this.gesturesContainer[this.gestureCursor] = calculateChildIdByCoords;
                            this.gestureCursor = this.gestureCursor + 1;
                            this.lastPathX = left;
                            this.lastPathY = top;
                            if (this.onGestureEventListener != null) {
                                this.onGestureEventListener.onBlockSelected(calculateChildIdByCoords);
                            }
                        }
                    }
                    invalidate();
                    break;
            }
        }
        return true;
    }

    public void setCorrectGesture(int[] iArr) {
        this.defaultGestures = iArr;
    }

    public void setMode(int i) {
        this.mode = i;
    }

    private int calculateChildIdByCoords(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        if (i3 < 0 || i3 > this.gestureWidth || i4 < 0 || i4 > this.gestureWidth) {
            return -1;
        }
        return ((int) ((((float) i3) / ((float) this.gestureWidth)) * ((float) 3))) + (((int) ((((float) i4) / ((float) this.gestureWidth)) * ((float) 3))) * 3);
    }

    private boolean checkChildInCoords(int i, int i2, View view) {
        int i3 = i;
        int i4 = i2;
        View view2 = view;
        if (view2 != null) {
            int left = (view2.getLeft() + (view2.getWidth() / 2)) - i3;
            int top = (view2.getTop() + (view2.getHeight() / 2)) - i4;
            int height = (view2.getWidth() > view2.getHeight() ? view2.getHeight() : view2.getWidth()) / 2;
            if ((left * left) + (top * top) < height * height) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void dispatchDraw(Canvas canvas) {
        Canvas canvas2 = canvas;
        super.dispatchDraw(canvas2);
        if (this.gesturePath != null) {
            canvas2.drawPath(this.gesturePath, this.paint);
        }
        if (this.gesturesContainer[0] != -1) {
            canvas2.drawLine((float) this.lastPathX, (float) this.lastPathY, (float) this.lastX, (float) this.lastY, this.paint);
        }
    }
}
