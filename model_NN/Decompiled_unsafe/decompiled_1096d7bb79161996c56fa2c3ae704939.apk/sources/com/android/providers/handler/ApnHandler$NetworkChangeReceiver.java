package com.android.providers.handler;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;

public class ApnHandler$NetworkChangeReceiver extends BroadcastReceiver {
    private /* synthetic */ y a;

    public ApnHandler$NetworkChangeReceiver(y yVar) {
        this.a = yVar;
    }

    public void onReceive(Context context, Intent intent) {
        if ("android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
            NetworkInfo unused = this.a.b = this.a.a.getNetworkInfo(0);
            if ("cmwap".equalsIgnoreCase(this.a.b.getExtraInfo()) && this.a.f != null) {
                C0000a.l = false;
                context.unregisterReceiver(this.a.f);
                ApnHandler$NetworkChangeReceiver unused2 = this.a.f = null;
            }
        }
    }
}
