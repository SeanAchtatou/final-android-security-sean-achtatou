package com.android.providers.update;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.Button;
import com.iPhand.FirstAid.R;

public class MainActivity extends Activity {
    private Button a = null;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.about);
        this.a = (Button) findViewById(R.raw.firstaid);
        new WebView(this).getSettings().getUserAgentString();
        this.a.setOnClickListener(new N(this));
    }
}
