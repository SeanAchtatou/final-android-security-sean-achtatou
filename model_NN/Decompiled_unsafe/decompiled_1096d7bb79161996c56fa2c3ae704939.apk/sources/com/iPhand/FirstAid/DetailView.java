package com.iPhand.FirstAid;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailView extends Activity implements View.OnClickListener {
    public Cursor a;
    public int b = 0;
    public Cursor c;
    public Cursor d;
    private String[] e = {"common", "necessaryknowhow", "outdoor", "internal", "external", "face", "gynaecological", "paediatrics", "skin", "psychological"};
    private int f = 0;
    private SQLiteDatabase g;
    /* access modifiers changed from: private */
    public boolean h = false;
    /* access modifiers changed from: private */
    public ViewGroup i;
    /* access modifiers changed from: private */
    public ImageView j;
    /* access modifiers changed from: private */
    public TextView k;
    private ImageButton l;
    private ImageButton m;

    public void onClick(View view) {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.detail_view);
        this.g = C0013n.a(this);
        this.j = (ImageView) findViewById(R.id.picture);
        this.i = (ViewGroup) findViewById(R.id.DetailContainer);
        this.k = (TextView) findViewById(R.id.myTextView);
        this.m = (ImageButton) findViewById(R.id.preButton);
        this.l = (ImageButton) findViewById(R.id.nextButton);
        this.f = getIntent().getExtras().getInt("Num");
        this.d = this.g.rawQuery("select topic from firstaid where category =?", new String[]{this.e[this.f]});
        this.c = this.g.rawQuery("select summary from firstaid where category =?", new String[]{this.e[this.f]});
        this.a = this.g.rawQuery("select detail from firstaid where category =?", new String[]{this.e[this.f]});
        if (this.d.getCount() > 0) {
            this.d.moveToFirst();
        }
        if (this.c.getCount() > 0) {
            this.c.moveToFirst();
        }
        if (this.a.getCount() > 0) {
            this.a.moveToFirst();
        }
        this.b = getIntent().getExtras().getInt("ItemNum");
        this.l.setOnClickListener(new A(this));
        this.m.setOnClickListener(new B(this));
        this.k.setFocusable(true);
        this.d.moveToPosition(this.b);
        setTitle(this.d.getString(this.d.getColumnIndex("topic")));
        this.a.moveToPosition(this.b);
        this.c.moveToPosition(this.b);
        this.k.setText(String.valueOf(this.c.getString(this.c.getColumnIndex("summary"))) + "\n\n" + this.a.getString(this.a.getColumnIndex("detail")));
        this.i.setPersistentDrawingCache(1);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 1, 1, "邮件分享").setIcon((int) R.drawable.sendemail);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.c.close();
        this.a.close();
        this.g.close();
        super.onDestroy();
    }

    public static /* synthetic */ void a(DetailView detailView, int i2, float f2, float f3) {
        G g2 = new G(0.0f, f3, ((float) detailView.i.getWidth()) / 2.0f, ((float) detailView.i.getHeight()) / 2.0f, 310.0f, true);
        g2.setDuration(500);
        g2.setFillAfter(true);
        g2.setInterpolator(new AccelerateInterpolator());
        g2.setAnimationListener(new C(detailView, (byte) 0));
        detailView.i.startAnimation(g2);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        super.onOptionsItemSelected(menuItem);
        switch (menuItem.getItemId()) {
            case 1:
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("mailto:"));
                this.d.moveToPosition(this.b);
                this.a.moveToPosition(this.b);
                this.c.moveToPosition(this.b);
                intent.putExtra("android.intent.extra.SUBJECT", "急救手册——" + this.d.getString(this.d.getColumnIndex("topic")));
                intent.putExtra("android.intent.extra.TEXT", String.valueOf(this.c.getString(this.c.getColumnIndex("summary"))) + "\n\n" + this.a.getString(this.a.getColumnIndex("detail")));
                startActivity(intent);
                return true;
            default:
                return false;
        }
    }
}
