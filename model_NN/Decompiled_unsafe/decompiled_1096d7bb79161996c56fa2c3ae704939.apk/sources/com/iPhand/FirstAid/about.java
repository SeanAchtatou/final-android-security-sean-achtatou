package com.iPhand.FirstAid;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class about extends Activity {
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) R.layout.about);
        ((TextView) findViewById(R.id.tvTitle)).setText(getString(R.string.app_name) + " " + getString(R.string.app_version));
        ((TextView) findViewById(R.id.tvContent)).setText((int) R.string.app_description);
    }
}
