package com.iPhand.FirstAid;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.ListView;

public class FirstAid extends Activity {
    public String[] a = {"常见急救", "急救须知", "户外意外急救", "内科", "外科", "五官科", "妇产科", "儿科", "皮肤科", "心理科"};
    public int[] b = {R.drawable.icon_01, R.drawable.icon_02, R.drawable.icon_03, R.drawable.icon_04, R.drawable.icon_05, R.drawable.icon_06, R.drawable.icon_07, R.drawable.icon_08, R.drawable.icon_09, R.drawable.icon_10};
    public ListView c;
    private ImageView d = null;
    private AlphaAnimation e = null;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.flashscreen);
        this.d = (ImageView) findViewById(R.id.ivStart);
        this.e = new AlphaAnimation(1.0f, 0.9f);
        this.e.setDuration(2000);
        this.e.setAnimationListener(new E(this));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 0, 1, "关于").setIcon((int) R.drawable.about);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        super.onOptionsItemSelected(menuItem);
        switch (menuItem.getItemId()) {
            case 0:
                startActivity(new Intent(this, about.class));
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.d.startAnimation(this.e);
    }
}
