package com.iPhand.FirstAid;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import java.util.ArrayList;
import java.util.HashMap;

public class ClassesListView extends Activity {
    private int[] a = {R.drawable.icon_01, R.drawable.icon_02, R.drawable.icon_03, R.drawable.icon_04, R.drawable.icon_05, R.drawable.icon_06, R.drawable.icon_07, R.drawable.icon_08, R.drawable.icon_09, R.drawable.icon_10};
    private int b = 0;
    private String c = null;
    private SQLiteDatabase d;
    private ListView e;
    private String[] f = {"common", "necessaryknowhow", "outdoor", "internal", "external", "face", "gynaecological", "paediatrics", "skin", "psychological"};

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.d.close();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        this.d = C0013n.a(this);
        super.onRestart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        this.d.close();
        super.onStop();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.dialogueclass_list);
        this.d = C0013n.a(this);
        this.e = (ListView) findViewById(R.id.DialogueClassList);
        this.b = getIntent().getExtras().getInt("Num");
        this.c = getIntent().getExtras().getString("Title");
        setTitle(this.c);
        Cursor rawQuery = this.d.rawQuery("select topic from firstaid where category =?", new String[]{this.f[this.b]});
        if (rawQuery.getCount() > 0) {
            rawQuery.moveToFirst();
        }
        try {
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < rawQuery.getCount(); i++) {
                HashMap hashMap = new HashMap();
                hashMap.put("Image", Integer.valueOf(this.a[this.b]));
                hashMap.put("ItemTitle", rawQuery.getString(rawQuery.getColumnIndex("topic")));
                arrayList.add(hashMap);
                rawQuery.moveToNext();
            }
            this.e.setAdapter((ListAdapter) new SimpleAdapter(this, arrayList, R.layout.dialogue_details_item, new String[]{"Image", "ItemTitle"}, new int[]{R.id.DialogueclassImage, R.id.DialogueclassTitle}));
            rawQuery.close();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        this.e.setOnItemClickListener(new z(this));
    }
}
