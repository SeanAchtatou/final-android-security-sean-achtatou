package com.admob.android.ads;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.widget.RelativeLayout;
import java.util.Timer;

public class AdView extends RelativeLayout {
    private static boolean c = false;
    /* access modifiers changed from: private */
    public static Handler d;
    public int a;
    public int b;
    /* access modifiers changed from: private */
    public C0006g e;
    private boolean f;
    private boolean g;
    /* access modifiers changed from: private */
    public String h;
    private u i;
    /* access modifiers changed from: private */
    public int j;
    private Timer k;
    /* access modifiers changed from: private */
    public boolean l;
    /* access modifiers changed from: private */
    public String m;

    public AdView(Context context) {
        this(context, null, 0);
    }

    public AdView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    private void a(boolean z) {
        synchronized (this) {
            if (z) {
                if (this.j > 0) {
                    if (this.k == null) {
                        this.k = new Timer();
                        this.k.schedule(new s(this), (long) this.j, (long) this.j);
                    }
                }
            }
            if ((!z || this.j == 0) && this.k != null) {
                this.k.cancel();
                this.k = null;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        this.g = true;
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        this.g = false;
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        setMeasuredDimension(getMeasuredWidth(), 48);
    }

    public void onWindowFocusChanged(boolean z) {
        a(z);
    }

    public void setBackgroundColor(int i2) {
        this.a = -16777216 | i2;
        if (this.e != null) {
            this.e.setBackgroundColor(i2);
        }
        invalidate();
    }

    public void setGoneWithoutAd(boolean z) {
        this.f = z;
    }

    public void setKeywords(String str) {
        this.h = str;
    }

    public void setListener(u uVar) {
        synchronized (this) {
            this.i = uVar;
        }
    }

    public void setRequestInterval(int i2) {
        if (i2 <= 0) {
            i2 = 0;
        } else if (i2 < 15) {
            C0010k.a("AdView.setRequestInterval(" + i2 + ") seconds must be >= 15");
        } else if (i2 > 600) {
            C0010k.a("AdView.setRequestInterval(" + i2 + ") seconds must be <= 600");
        }
        this.j = i2 * 1000;
        if (i2 == 0) {
            a(false);
            return;
        }
        Log.i("AdMob SDK", "Requesting fresh ads every " + i2 + " seconds.");
        a(true);
    }

    public void setSearchQuery(String str) {
        this.m = str;
    }

    public void setTextColor(int i2) {
        this.b = -16777216 | i2;
        if (this.e != null) {
            this.e.a(i2);
        }
        invalidate();
    }

    public void setVisibility(int i2) {
        if (super.getVisibility() != i2) {
            synchronized (this) {
                int childCount = getChildCount();
                for (int i3 = 0; i3 < childCount; i3++) {
                    getChildAt(i3).setVisibility(i2);
                }
                super.setVisibility(i2);
                if (i2 == 0) {
                    b();
                } else {
                    removeView(this.e);
                    this.e = null;
                    invalidate();
                }
            }
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AdView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        int i3 = -1;
        int i4 = -16777216;
        this.g = false;
        setFocusable(true);
        setDescendantFocusability(262144);
        setClickable(true);
        if (attributeSet != null) {
            String str = "http://schemas.android.com/apk/res/" + context.getPackageName();
            boolean attributeBooleanValue = attributeSet.getAttributeBooleanValue(str, "testing", false);
            if (attributeBooleanValue) {
                C0010k.a(attributeBooleanValue);
            }
            i3 = attributeSet.getAttributeUnsignedIntValue(str, "textColor", -1);
            i4 = attributeSet.getAttributeUnsignedIntValue(str, "backgroundColor", -16777216);
            this.h = attributeSet.getAttributeValue(str, "keywords");
            setRequestInterval(attributeSet.getAttributeIntValue(str, "refreshInterval", 0));
            setGoneWithoutAd(attributeSet.getAttributeBooleanValue(str, "isGoneWithoutAd", this.f));
        }
        setTextColor(i3);
        setBackgroundColor(i4);
        if (super.getVisibility() == 0) {
            b();
        }
    }

    public static /* synthetic */ void b(AdView adView, C0006g gVar) {
        adView.e = gVar;
        if (adView.g) {
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration(233);
            alphaAnimation.startNow();
            alphaAnimation.setFillAfter(true);
            alphaAnimation.setInterpolator(new AccelerateInterpolator());
            adView.startAnimation(alphaAnimation);
        }
    }

    public static /* synthetic */ void c(AdView adView, C0006g gVar) {
        gVar.setVisibility(8);
        x xVar = new x(0.0f, -90.0f, ((float) adView.getWidth()) / 2.0f, ((float) adView.getHeight()) / 2.0f, -0.4f * ((float) adView.getWidth()), true);
        xVar.setDuration(700);
        xVar.setFillAfter(true);
        xVar.setInterpolator(new AccelerateInterpolator());
        xVar.setAnimationListener(new t(adView, gVar));
        adView.startAnimation(xVar);
    }

    public int getVisibility() {
        if (this.f) {
            if (!(this.e != null)) {
                return 8;
            }
        }
        return super.getVisibility();
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x005b A[SYNTHETIC, Splitter:B:14:0x005b] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0064  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0091 A[SYNTHETIC, Splitter:B:29:0x0091] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0098  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void b() {
        /*
            r6 = this;
            r5 = 1
            r2 = 0
            android.content.Context r0 = r6.getContext()
            java.lang.String r1 = defpackage.C0010k.c(r0)
            if (r1 != 0) goto L_0x005e
            boolean r1 = com.admob.android.ads.AdView.c
            if (r1 != 0) goto L_0x005e
            com.admob.android.ads.AdView.c = r5
            r1 = 0
            r3 = 0
            java.lang.String r0 = defpackage.p.a(r0, r1, r3)     // Catch:{ Exception -> 0x0095, all -> 0x008d }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0095, all -> 0x008d }
            r1.<init>()     // Catch:{ Exception -> 0x0095, all -> 0x008d }
            java.lang.String r3 = "http://api.admob.com/v1/pubcode/android_sdk_emulator_notice"
            r1.append(r3)     // Catch:{ Exception -> 0x0095, all -> 0x008d }
            java.lang.String r3 = "?"
            r1.append(r3)     // Catch:{ Exception -> 0x0095, all -> 0x008d }
            r1.append(r0)     // Catch:{ Exception -> 0x0095, all -> 0x008d }
            java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x0095, all -> 0x008d }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0095, all -> 0x008d }
            r0.<init>(r1)     // Catch:{ Exception -> 0x0095, all -> 0x008d }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x0095, all -> 0x008d }
            r0.connect()     // Catch:{ Exception -> 0x0095, all -> 0x008d }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0095, all -> 0x008d }
            r3.<init>()     // Catch:{ Exception -> 0x0095, all -> 0x008d }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0095, all -> 0x008d }
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0095, all -> 0x008d }
            java.io.InputStream r0 = r0.getInputStream()     // Catch:{ Exception -> 0x0095, all -> 0x008d }
            r4.<init>(r0)     // Catch:{ Exception -> 0x0095, all -> 0x008d }
            r1.<init>(r4)     // Catch:{ Exception -> 0x0095, all -> 0x008d }
        L_0x004d:
            java.lang.String r0 = r1.readLine()     // Catch:{ Exception -> 0x0057, all -> 0x00c7 }
            if (r0 == 0) goto L_0x006c
            r3.append(r0)     // Catch:{ Exception -> 0x0057, all -> 0x00c7 }
            goto L_0x004d
        L_0x0057:
            r0 = move-exception
            r0 = r1
        L_0x0059:
            if (r0 == 0) goto L_0x005e
            r0.close()     // Catch:{ Exception -> 0x00c3 }
        L_0x005e:
            int r0 = super.getVisibility()
            if (r0 == 0) goto L_0x0098
            java.lang.String r0 = "AdMob SDK"
            java.lang.String r1 = "Cannot requestFreshAd() when the AdView is not visible.  Call AdView.setVisibility(View.VISIBLE) first."
            android.util.Log.w(r0, r1)
        L_0x006b:
            return
        L_0x006c:
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ Exception -> 0x0057, all -> 0x00c7 }
            org.json.JSONTokener r2 = new org.json.JSONTokener     // Catch:{ Exception -> 0x0057, all -> 0x00c7 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0057, all -> 0x00c7 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0057, all -> 0x00c7 }
            r0.<init>(r2)     // Catch:{ Exception -> 0x0057, all -> 0x00c7 }
            java.lang.String r2 = "data"
            java.lang.String r0 = r0.getString(r2)     // Catch:{ Exception -> 0x0057, all -> 0x00c7 }
            if (r0 == 0) goto L_0x0087
            java.lang.String r2 = "AdMob SDK"
            android.util.Log.e(r2, r0)     // Catch:{ Exception -> 0x0057, all -> 0x00c7 }
        L_0x0087:
            r1.close()     // Catch:{ Exception -> 0x008b }
            goto L_0x005e
        L_0x008b:
            r0 = move-exception
            goto L_0x005e
        L_0x008d:
            r0 = move-exception
            r1 = r2
        L_0x008f:
            if (r1 == 0) goto L_0x0094
            r1.close()     // Catch:{ Exception -> 0x00c5 }
        L_0x0094:
            throw r0
        L_0x0095:
            r0 = move-exception
            r0 = r2
            goto L_0x0059
        L_0x0098:
            boolean r0 = r6.l
            if (r0 == 0) goto L_0x00ad
            java.lang.String r0 = "AdMob SDK"
            r1 = 3
            boolean r0 = android.util.Log.isLoggable(r0, r1)
            if (r0 == 0) goto L_0x006b
            java.lang.String r0 = "AdMob SDK"
            java.lang.String r1 = "Ignoring requestFreshAd() because we are already getting a fresh ad."
            android.util.Log.d(r0, r1)
            goto L_0x006b
        L_0x00ad:
            r6.l = r5
            android.os.Handler r0 = com.admob.android.ads.AdView.d
            if (r0 != 0) goto L_0x00ba
            android.os.Handler r0 = new android.os.Handler
            r0.<init>()
            com.admob.android.ads.AdView.d = r0
        L_0x00ba:
            q r0 = new q
            r0.<init>(r6)
            r0.start()
            goto L_0x006b
        L_0x00c3:
            r0 = move-exception
            goto L_0x005e
        L_0x00c5:
            r1 = move-exception
            goto L_0x0094
        L_0x00c7:
            r0 = move-exception
            goto L_0x008f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.admob.android.ads.AdView.b():void");
    }
}
