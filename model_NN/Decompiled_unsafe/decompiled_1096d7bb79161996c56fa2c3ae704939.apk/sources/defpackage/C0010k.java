package defpackage;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Locale;

/* renamed from: k  reason: default package and case insensitive filesystem */
public final class C0010k {
    /* access modifiers changed from: private */
    public static Location a;
    /* access modifiers changed from: private */
    public static long b;
    private static String c;
    private static boolean d;
    private static String e;
    private static String f;

    static {
        Log.i("AdMob SDK", "AdMob SDK version is 20091123-ANDROID-3312276cc1406347");
    }

    static String a(Context context) {
        Location d2 = d(context);
        String str = null;
        if (d2 != null) {
            str = d2.getLatitude() + "," + d2.getLongitude();
        }
        if (Log.isLoggable("AdMob SDK", 3)) {
            Log.d("AdMob SDK", "User coordinates are " + str);
        }
        return str;
    }

    public static void a(String str) {
        Log.e("AdMob SDK", str);
        throw new IllegalArgumentException(str);
    }

    public static void a(boolean z) {
        d = z;
    }

    private static String b(String str) {
        if (str == null || str.length() <= 0) {
            return null;
        }
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes(), 0, str.length());
            return String.format("%032X", new BigInteger(1, instance.digest()));
        } catch (Exception e2) {
            Log.d("AdMob SDK", "Could not generate hash of " + str, e2);
            f = f.substring(0, 32);
            return null;
        }
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [java.util.GregorianCalendar, java.lang.String] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    static java.lang.String c() {
        /*
            r0 = 0
            r5 = 2
            r4 = 1
            if (r0 == 0) goto L_0x0030
            r1 = 3
            java.lang.Object[] r1 = new java.lang.Object[r1]
            r2 = 0
            int r3 = r0.get(r4)
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r1[r2] = r3
            int r2 = r0.get(r5)
            int r2 = r2 + 1
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1[r4] = r2
            r2 = 5
            int r0 = r0.get(r2)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r1[r5] = r0
            java.lang.String r0 = "%04d%02d%02d"
            java.lang.String r0 = java.lang.String.format(r0, r1)
        L_0x0030:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.C0010k.c():java.lang.String");
    }

    public static String c(Context context) {
        if (f == null) {
            String string = Settings.System.getString(context.getContentResolver(), "android_id");
            f = string;
            f = b(string);
            Log.i("AdMob SDK", "The user ID is " + f);
        }
        return f;
    }

    private static Location d(Context context) {
        LocationManager locationManager;
        String str;
        boolean z;
        LocationManager locationManager2;
        String str2 = null;
        boolean z2 = false;
        if (context != null && (a == null || System.currentTimeMillis() > b + 900000)) {
            synchronized (context) {
                if (a == null || System.currentTimeMillis() > b + 900000) {
                    b = System.currentTimeMillis();
                    if (context.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
                        if (Log.isLoggable("AdMob SDK", 3)) {
                            Log.d("AdMob SDK", "Trying to get locations from the network.");
                        }
                        LocationManager locationManager3 = (LocationManager) context.getSystemService("location");
                        if (locationManager3 != null) {
                            Criteria criteria = new Criteria();
                            criteria.setAccuracy(2);
                            criteria.setCostAllowed(false);
                            str2 = locationManager3.getBestProvider(criteria, true);
                            locationManager = locationManager3;
                            z2 = true;
                        } else {
                            locationManager = locationManager3;
                            z2 = true;
                        }
                    } else {
                        locationManager = null;
                    }
                    if (str2 == null && context.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0) {
                        if (Log.isLoggable("AdMob SDK", 3)) {
                            Log.d("AdMob SDK", "Trying to get locations from GPS.");
                        }
                        locationManager2 = (LocationManager) context.getSystemService("location");
                        if (locationManager2 != null) {
                            Criteria criteria2 = new Criteria();
                            criteria2.setAccuracy(1);
                            criteria2.setCostAllowed(false);
                            str = locationManager2.getBestProvider(criteria2, true);
                            z = true;
                        } else {
                            str = str2;
                            z = true;
                        }
                    } else {
                        str = str2;
                        z = z2;
                        locationManager2 = locationManager;
                    }
                    if (!z) {
                        if (Log.isLoggable("AdMob SDK", 3)) {
                            Log.d("AdMob SDK", "Cannot access user's location.  Permissions are not set.");
                        }
                    } else if (str != null) {
                        Log.i("AdMob SDK", "Location provider setup successfully.");
                        locationManager2.requestLocationUpdates(str, 0, 0.0f, new C0011l(locationManager2), context.getMainLooper());
                    } else if (Log.isLoggable("AdMob SDK", 3)) {
                        Log.d("AdMob SDK", "No location providers are available.  Ads will not be geotargeted.");
                    }
                }
            }
        }
        return a;
    }

    static String d() {
        if (e == null) {
            StringBuffer stringBuffer = new StringBuffer();
            String str = Build.VERSION.RELEASE;
            if (str.length() > 0) {
                stringBuffer.append(str);
            } else {
                stringBuffer.append("1.0");
            }
            stringBuffer.append("; ");
            Locale locale = Locale.getDefault();
            String language = locale.getLanguage();
            if (language != null) {
                stringBuffer.append(language.toLowerCase());
                String country = locale.getCountry();
                if (country != null) {
                    stringBuffer.append("-");
                    stringBuffer.append(country.toLowerCase());
                }
            } else {
                stringBuffer.append("en");
            }
            String str2 = Build.MODEL;
            if (str2.length() > 0) {
                stringBuffer.append("; ");
                stringBuffer.append(str2);
            }
            String str3 = Build.ID;
            if (str3.length() > 0) {
                stringBuffer.append(" Build/");
                stringBuffer.append(str3);
            }
            e = String.format("Mozilla/5.0 (Linux; U; Android %s) AppleWebKit/525.10+ (KHTML, like Gecko) Version/3.0.4 Mobile Safari/523.12.2 (AdMob-ANDROID-%s)", stringBuffer, "20091123");
            if (Log.isLoggable("AdMob SDK", 3)) {
                Log.d("AdMob SDK", "Phone's user-agent is:  " + e);
            }
        }
        return e;
    }

    public static boolean e() {
        return d;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0064, code lost:
        if (r0.length() != 15) goto L_0x0066;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String b(android.content.Context r4) {
        /*
            java.lang.String r0 = defpackage.C0010k.c
            if (r0 != 0) goto L_0x0059
            android.content.pm.PackageManager r0 = r4.getPackageManager()     // Catch:{ Exception -> 0x009e }
            java.lang.String r1 = r4.getPackageName()     // Catch:{ Exception -> 0x009e }
            r2 = 128(0x80, float:1.794E-43)
            android.content.pm.ApplicationInfo r0 = r0.getApplicationInfo(r1, r2)     // Catch:{ Exception -> 0x009e }
            if (r0 == 0) goto L_0x0059
            android.os.Bundle r0 = r0.metaData     // Catch:{ Exception -> 0x009e }
            java.lang.String r1 = "ADMOB_PUBLISHER_ID"
            java.lang.String r0 = r0.getString(r1)     // Catch:{ Exception -> 0x009e }
            java.lang.String r1 = "AdMob SDK"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x009e }
            java.lang.String r3 = "Publisher ID read from AndroidManifest.xml is "
            r2.<init>(r3)     // Catch:{ Exception -> 0x009e }
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ Exception -> 0x009e }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x009e }
            android.util.Log.d(r1, r2)     // Catch:{ Exception -> 0x009e }
            java.lang.String r1 = "a1496ced2842262"
            boolean r1 = r0.equals(r1)     // Catch:{ Exception -> 0x009e }
            if (r1 == 0) goto L_0x005c
            java.lang.String r1 = r4.getPackageName()     // Catch:{ Exception -> 0x009e }
            java.lang.String r2 = "com.admob.android.test"
            boolean r1 = r1.equals(r2)     // Catch:{ Exception -> 0x009e }
            if (r1 != 0) goto L_0x0050
            java.lang.String r1 = r4.getPackageName()     // Catch:{ Exception -> 0x009e }
            java.lang.String r2 = "com.example.admob.lunarlander"
            boolean r1 = r1.equals(r2)     // Catch:{ Exception -> 0x009e }
            if (r1 == 0) goto L_0x005c
        L_0x0050:
            java.lang.String r1 = "AdMob SDK"
            java.lang.String r2 = "This is a sample application so allowing sample publisher ID."
            android.util.Log.i(r1, r2)     // Catch:{ Exception -> 0x009e }
            defpackage.C0010k.c = r0     // Catch:{ Exception -> 0x009e }
        L_0x0059:
            java.lang.String r0 = defpackage.C0010k.c
            return r0
        L_0x005c:
            if (r0 == 0) goto L_0x0066
            int r1 = r0.length()     // Catch:{ Exception -> 0x009e }
            r2 = 15
            if (r1 == r2) goto L_0x007a
        L_0x0066:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x009e }
            java.lang.String r2 = "SETUP ERROR:  Incorrect AdMob publisher ID.  Should 15 [a-f,0-9] characters:  "
            r1.<init>(r2)     // Catch:{ Exception -> 0x009e }
            java.lang.String r2 = defpackage.C0010k.c     // Catch:{ Exception -> 0x009e }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x009e }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x009e }
            a(r1)     // Catch:{ Exception -> 0x009e }
        L_0x007a:
            java.lang.String r1 = "a1496ced2842262"
            boolean r1 = r0.equalsIgnoreCase(r1)     // Catch:{ Exception -> 0x009e }
            if (r1 == 0) goto L_0x0087
            java.lang.String r1 = "SETUP ERROR:  Cannot use the sample publisher ID (a1496ced2842262).  Yours is available on www.admob.com."
            a(r1)     // Catch:{ Exception -> 0x009e }
        L_0x0087:
            java.lang.String r1 = "AdMob SDK"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x009e }
            java.lang.String r3 = "Publisher ID set to "
            r2.<init>(r3)     // Catch:{ Exception -> 0x009e }
            java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ Exception -> 0x009e }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x009e }
            android.util.Log.i(r1, r2)     // Catch:{ Exception -> 0x009e }
            defpackage.C0010k.c = r0     // Catch:{ Exception -> 0x009e }
            goto L_0x0059
        L_0x009e:
            r0 = move-exception
            java.lang.String r1 = "AdMob SDK"
            java.lang.String r2 = "Could not read ADMOB_PUBLISHER_ID meta-data from AndroidManifest.xml."
            android.util.Log.e(r1, r2, r0)
            goto L_0x0059
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.C0010k.b(android.content.Context):java.lang.String");
    }
}
