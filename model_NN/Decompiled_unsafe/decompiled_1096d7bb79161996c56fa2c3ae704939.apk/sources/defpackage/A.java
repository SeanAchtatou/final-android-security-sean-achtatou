package defpackage;

import android.view.View;
import android.widget.Toast;
import com.iPhand.FirstAid.DetailView;

/* renamed from: A  reason: default package */
public final class A implements View.OnClickListener {
    private /* synthetic */ DetailView a;

    public A(DetailView detailView) {
        this.a = detailView;
    }

    public final void onClick(View view) {
        if (this.a.b == this.a.c.getCount() - 1) {
            Toast.makeText(this.a, "已经是本篇最后一条了！休息一下^_^", 1).show();
            return;
        }
        this.a.h = false;
        DetailView.a(this.a, -1, 0.0f, 180.0f);
    }
}
