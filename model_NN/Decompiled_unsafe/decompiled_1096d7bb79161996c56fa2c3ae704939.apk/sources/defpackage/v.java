package defpackage;

import android.view.animation.DecelerateInterpolator;
import com.admob.android.ads.AdView;

/* renamed from: v  reason: default package */
public final class v implements Runnable {
    final /* synthetic */ AdView a;
    /* access modifiers changed from: private */
    public C0006g b;
    /* access modifiers changed from: private */
    public C0006g c;

    public v(AdView adView, C0006g gVar) {
        this.a = adView;
        this.b = gVar;
    }

    public final void run() {
        this.c = this.a.e;
        if (this.c != null) {
            this.c.setVisibility(8);
        }
        this.b.setVisibility(0);
        x xVar = new x(90.0f, 0.0f, ((float) this.a.getWidth()) / 2.0f, ((float) this.a.getHeight()) / 2.0f, -0.4f * ((float) this.a.getWidth()), false);
        xVar.setDuration(700);
        xVar.setFillAfter(true);
        xVar.setInterpolator(new DecelerateInterpolator());
        xVar.setAnimationListener(new w(this));
        this.a.startAnimation(xVar);
    }
}
