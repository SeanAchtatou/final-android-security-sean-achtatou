package defpackage;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import java.util.Date;

/* renamed from: l  reason: default package and case insensitive filesystem */
final class C0011l implements LocationListener {
    private /* synthetic */ LocationManager a;

    C0011l(LocationManager locationManager) {
        this.a = locationManager;
    }

    public final void onLocationChanged(Location location) {
        Location unused = C0010k.a = location;
        long unused2 = C0010k.b = System.currentTimeMillis();
        this.a.removeUpdates(this);
        Log.i("AdMob SDK", "Aquired location " + C0010k.a.getLatitude() + "," + C0010k.a.getLongitude() + " at " + new Date(C0010k.b).toString() + ".");
    }

    public final void onProviderDisabled(String str) {
    }

    public final void onProviderEnabled(String str) {
    }

    public final void onStatusChanged(String str, int i, Bundle bundle) {
    }
}
