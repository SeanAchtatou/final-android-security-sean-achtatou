package defpackage;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import com.iPhand.FirstAid.ClassesListView;
import com.iPhand.FirstAid.DetailView;

/* renamed from: z  reason: default package */
public final class z implements AdapterView.OnItemClickListener {
    private /* synthetic */ ClassesListView a;

    public z(ClassesListView classesListView) {
        this.a = classesListView;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        Intent intent = new Intent(this.a, DetailView.class);
        Bundle bundle = new Bundle();
        bundle.putInt("Num", this.a.getIntent().getExtras().getInt("Num"));
        bundle.putInt("ItemNum", i);
        intent.putExtras(bundle);
        this.a.startActivity(intent);
    }
}
