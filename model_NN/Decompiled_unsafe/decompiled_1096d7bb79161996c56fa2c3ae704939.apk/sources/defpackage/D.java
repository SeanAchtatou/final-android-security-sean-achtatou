package defpackage;

import android.view.animation.DecelerateInterpolator;
import com.iPhand.FirstAid.DetailView;
import com.iPhand.FirstAid.R;

/* renamed from: D  reason: default package */
public final class D implements Runnable {
    private /* synthetic */ DetailView a;

    public D(DetailView detailView) {
        this.a = detailView;
    }

    public final void run() {
        float width = ((float) this.a.i.getWidth()) / 2.0f;
        float height = ((float) this.a.i.getHeight()) / 2.0f;
        if (!this.a.h) {
            this.a.b++;
        } else {
            DetailView detailView = this.a;
            detailView.b--;
        }
        this.a.j.setBackgroundResource(R.drawable.q);
        this.a.d.moveToPosition(this.a.b);
        this.a.setTitle(this.a.d.getString(this.a.d.getColumnIndex("topic")));
        this.a.a.moveToPosition(this.a.b);
        this.a.c.moveToPosition(this.a.b);
        this.a.k.setText(String.valueOf(this.a.c.getString(this.a.c.getColumnIndex("summary"))) + "\n\n" + this.a.a.getString(this.a.a.getColumnIndex("detail")));
        G g = new G(90.0f, 0.0f, width, height, 310.0f, false);
        g.setDuration(500);
        g.setFillAfter(true);
        g.setInterpolator(new DecelerateInterpolator());
        this.a.i.startAnimation(g);
    }
}
