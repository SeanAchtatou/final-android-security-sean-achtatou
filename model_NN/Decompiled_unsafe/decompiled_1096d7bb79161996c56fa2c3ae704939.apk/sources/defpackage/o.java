package defpackage;

import android.util.Log;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.net.URLConnection;

/* renamed from: o  reason: default package */
final class o extends Thread {
    private /* synthetic */ C0012m a;

    private o(C0012m mVar) {
        this.a = mVar;
    }

    /* synthetic */ o(C0012m mVar, byte b) {
        this(mVar);
    }

    public final void run() {
        BufferedOutputStream bufferedOutputStream;
        try {
            URLConnection openConnection = new URL(C0012m.b + "/" + this.a.h + C0012m.d).openConnection();
            openConnection.connect();
            BufferedInputStream bufferedInputStream = new BufferedInputStream(openConnection.getInputStream());
            File file = new File(this.a.g.getDir(C0012m.a, 0), "20091123");
            if (!file.exists()) {
                file.mkdir();
            }
            bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(new File(file, this.a.h + C0012m.d)));
            byte[] bArr = new byte[512];
            while (true) {
                int read = bufferedInputStream.read(bArr, 0, 512);
                if (read > 0) {
                    bufferedOutputStream.write(bArr, 0, read);
                } else {
                    bufferedOutputStream.close();
                    return;
                }
            }
        } catch (Exception e) {
            if (Log.isLoggable("AdMob SDK", 3)) {
                Log.d("AdMob SDK", "Could not get localized strings from the AdMob servers.");
            }
        } catch (Throwable th) {
            bufferedOutputStream.close();
            throw th;
        }
    }
}
