package defpackage;

import android.graphics.Matrix;

/* renamed from: h  reason: default package and case insensitive filesystem */
final class C0007h extends Thread {
    private /* synthetic */ C0006g a;

    C0007h(C0006g gVar) {
        this.a = gVar;
    }

    public final void run() {
        if (this.a.g != null) {
            this.a.g.setVisibility(4);
        }
        if (this.a.l != null) {
            this.a.l.setImageMatrix(new Matrix());
            this.a.l.setVisibility(0);
        }
        boolean unused = this.a.k = false;
    }
}
