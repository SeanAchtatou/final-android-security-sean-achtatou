package defpackage;

import android.content.Context;
import android.util.Log;
import android.widget.RelativeLayout;
import com.admob.android.ads.AdView;

/* renamed from: q  reason: default package */
public final class q extends Thread {
    final /* synthetic */ AdView a;

    public q(AdView adView) {
        this.a = adView;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.admob.android.ads.AdView.a(com.admob.android.ads.AdView, boolean):boolean
     arg types: [com.admob.android.ads.AdView, int]
     candidates:
      com.admob.android.ads.AdView.a(com.admob.android.ads.AdView, g):g
      com.admob.android.ads.AdView.a(com.admob.android.ads.AdView, boolean):boolean */
    public final void run() {
        try {
            Context context = this.a.getContext();
            C0003d b = p.b(context, this.a.h, this.a.m);
            if (b != null) {
                synchronized (this) {
                    if (this.a.e == null || !b.equals(this.a.e.a) || C0010k.e()) {
                        boolean z = this.a.e == null;
                        int d = q.super.getVisibility();
                        C0006g gVar = new C0006g(b, context);
                        gVar.setBackgroundColor(this.a.a);
                        gVar.a(this.a.b);
                        gVar.setVisibility(d);
                        gVar.setLayoutParams(new RelativeLayout.LayoutParams(-1, 48));
                        AdView.d.post(new r(this, gVar, d, z));
                    } else {
                        if (Log.isLoggable("AdMob SDK", 3)) {
                            Log.d("AdMob SDK", "Received the same ad we already had.  Discarding it.");
                        }
                        boolean unused = this.a.l = false;
                    }
                }
                return;
            }
            boolean unused2 = this.a.l = false;
        } catch (Exception e) {
            Log.e("AdMob SDK", "Unhandled exception requesting a fresh ad.", e);
            boolean unused3 = this.a.l = false;
        }
    }
}
