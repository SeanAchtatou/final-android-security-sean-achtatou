package defpackage;

/* renamed from: e  reason: default package and case insensitive filesystem */
final class C0004e extends Thread {
    private /* synthetic */ C0003d a;

    C0004e(C0003d dVar) {
        this.a = dVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00b8  */
    /* JADX WARNING: Removed duplicated region for block: B:35:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r6 = this;
            r1 = 0
            r5 = 3
            d r0 = r6.a     // Catch:{ MalformedURLException -> 0x00c2, IOException -> 0x00de }
            f r0 = r0.a     // Catch:{ MalformedURLException -> 0x00c2, IOException -> 0x00de }
            if (r0 == 0) goto L_0x0013
            d r0 = r6.a     // Catch:{ MalformedURLException -> 0x00c2, IOException -> 0x00de }
            f r0 = r0.a     // Catch:{ MalformedURLException -> 0x00c2, IOException -> 0x00de }
            r0.b()     // Catch:{ MalformedURLException -> 0x00c2, IOException -> 0x00de }
        L_0x0013:
            java.net.URL r2 = new java.net.URL     // Catch:{ MalformedURLException -> 0x00c2, IOException -> 0x00de }
            d r0 = r6.a     // Catch:{ MalformedURLException -> 0x00c2, IOException -> 0x00de }
            java.lang.String r0 = r0.d     // Catch:{ MalformedURLException -> 0x00c2, IOException -> 0x00de }
            r2.<init>(r0)     // Catch:{ MalformedURLException -> 0x00c2, IOException -> 0x00de }
            r0 = 1
            java.net.HttpURLConnection.setFollowRedirects(r0)     // Catch:{ MalformedURLException -> 0x0114, IOException -> 0x0111 }
            java.net.URLConnection r0 = r2.openConnection()     // Catch:{ MalformedURLException -> 0x0114, IOException -> 0x0111 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x0114, IOException -> 0x0111 }
            int r1 = defpackage.C0003d.c     // Catch:{ MalformedURLException -> 0x0114, IOException -> 0x0111 }
            r0.setConnectTimeout(r1)     // Catch:{ MalformedURLException -> 0x0114, IOException -> 0x0111 }
            int r1 = defpackage.C0003d.c     // Catch:{ MalformedURLException -> 0x0114, IOException -> 0x0111 }
            r0.setReadTimeout(r1)     // Catch:{ MalformedURLException -> 0x0114, IOException -> 0x0111 }
            java.lang.String r1 = "User-Agent"
            java.lang.String r3 = defpackage.C0010k.d()     // Catch:{ MalformedURLException -> 0x0114, IOException -> 0x0111 }
            r0.setRequestProperty(r1, r3)     // Catch:{ MalformedURLException -> 0x0114, IOException -> 0x0111 }
            java.lang.String r1 = "X-ADMOB-ISU"
            d r3 = r6.a     // Catch:{ MalformedURLException -> 0x0114, IOException -> 0x0111 }
            android.content.Context r3 = r3.e     // Catch:{ MalformedURLException -> 0x0114, IOException -> 0x0111 }
            java.lang.String r3 = defpackage.C0010k.c(r3)     // Catch:{ MalformedURLException -> 0x0114, IOException -> 0x0111 }
            r0.setRequestProperty(r1, r3)     // Catch:{ MalformedURLException -> 0x0114, IOException -> 0x0111 }
            r0.connect()     // Catch:{ MalformedURLException -> 0x0114, IOException -> 0x0111 }
            r0.getResponseCode()     // Catch:{ MalformedURLException -> 0x0114, IOException -> 0x0111 }
            java.net.URL r1 = r0.getURL()     // Catch:{ MalformedURLException -> 0x0114, IOException -> 0x0111 }
            java.lang.String r0 = "AdMob SDK"
            r2 = 3
            boolean r0 = android.util.Log.isLoggable(r0, r2)     // Catch:{ MalformedURLException -> 0x00c2, IOException -> 0x00de }
            if (r0 == 0) goto L_0x0075
            java.lang.String r0 = "AdMob SDK"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x00c2, IOException -> 0x00de }
            java.lang.String r3 = "Final click destination URL:  "
            r2.<init>(r3)     // Catch:{ MalformedURLException -> 0x00c2, IOException -> 0x00de }
            java.lang.StringBuilder r2 = r2.append(r1)     // Catch:{ MalformedURLException -> 0x00c2, IOException -> 0x00de }
            java.lang.String r2 = r2.toString()     // Catch:{ MalformedURLException -> 0x00c2, IOException -> 0x00de }
            android.util.Log.d(r0, r2)     // Catch:{ MalformedURLException -> 0x00c2, IOException -> 0x00de }
        L_0x0075:
            if (r1 == 0) goto L_0x00b0
            java.lang.String r0 = "AdMob SDK"
            boolean r0 = android.util.Log.isLoggable(r0, r5)
            if (r0 == 0) goto L_0x0093
            java.lang.String r0 = "AdMob SDK"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Opening "
            r2.<init>(r3)
            java.lang.StringBuilder r2 = r2.append(r1)
            java.lang.String r2 = r2.toString()
            android.util.Log.d(r0, r2)
        L_0x0093:
            android.content.Intent r0 = new android.content.Intent
            java.lang.String r2 = "android.intent.action.VIEW"
            java.lang.String r3 = r1.toString()
            android.net.Uri r3 = android.net.Uri.parse(r3)
            r0.<init>(r2, r3)
            r2 = 268435456(0x10000000, float:2.5243549E-29)
            r0.addFlags(r2)
            d r2 = r6.a     // Catch:{ Exception -> 0x00fb }
            android.content.Context r2 = r2.e     // Catch:{ Exception -> 0x00fb }
            r2.startActivity(r0)     // Catch:{ Exception -> 0x00fb }
        L_0x00b0:
            d r0 = r6.a
            f r0 = r0.a
            if (r0 == 0) goto L_0x00c1
            d r0 = r6.a
            f r0 = r0.a
            r0.a()
        L_0x00c1:
            return
        L_0x00c2:
            r0 = move-exception
        L_0x00c3:
            java.lang.String r2 = "AdMob SDK"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "Malformed click URL.  Will try to follow anyway.  "
            r3.<init>(r4)
            d r4 = r6.a
            java.lang.String r4 = r4.d
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            android.util.Log.w(r2, r3, r0)
            goto L_0x0075
        L_0x00de:
            r0 = move-exception
        L_0x00df:
            java.lang.String r2 = "AdMob SDK"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "Could not determine final click destination URL.  Will try to follow anyway.  "
            r3.<init>(r4)
            d r4 = r6.a
            java.lang.String r4 = r4.d
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            android.util.Log.w(r2, r3, r0)
            goto L_0x0075
        L_0x00fb:
            r0 = move-exception
            java.lang.String r2 = "AdMob SDK"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "Could not open browser on ad click to "
            r3.<init>(r4)
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r1 = r1.toString()
            android.util.Log.e(r2, r1, r0)
            goto L_0x00b0
        L_0x0111:
            r0 = move-exception
            r1 = r2
            goto L_0x00df
        L_0x0114:
            r0 = move-exception
            r1 = r2
            goto L_0x00c3
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.C0004e.run():void");
    }
}
