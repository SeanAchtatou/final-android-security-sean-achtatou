package defpackage;

import android.graphics.Camera;
import android.graphics.Matrix;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/* renamed from: G  reason: default package */
public final class G extends Animation {
    private Camera a;
    private final float b;
    private final float c;
    private final float d = 310.0f;
    private final float e;
    private final boolean f;
    private final float g;

    public G(float f2, float f3, float f4, float f5, float f6, boolean z) {
        this.e = f2;
        this.g = f3;
        this.b = f4;
        this.c = f5;
        this.f = z;
    }

    /* access modifiers changed from: protected */
    public final void applyTransformation(float f2, Transformation transformation) {
        float f3 = this.e;
        float f4 = f3 + ((this.g - f3) * f2);
        float f5 = this.b;
        float f6 = this.c;
        Camera camera = this.a;
        Matrix matrix = transformation.getMatrix();
        camera.save();
        if (this.f) {
            camera.translate(0.0f, 0.0f, this.d * f2);
        } else {
            camera.translate(0.0f, 0.0f, this.d * (1.0f - f2));
        }
        camera.rotateY(f4);
        camera.getMatrix(matrix);
        camera.restore();
        matrix.preTranslate(-f5, -f6);
        matrix.postTranslate(f5, f6);
    }

    public final void initialize(int i, int i2, int i3, int i4) {
        super.initialize(i, i2, i3, i4);
        this.a = new Camera();
    }
}
