package defpackage;

import android.content.Context;
import android.content.SharedPreferences;

/* renamed from: c  reason: default package and case insensitive filesystem */
public final class C0002c {
    public static String a = "endCallSum";
    private static C0002c c = null;
    SharedPreferences b = null;

    private C0002c(Context context) {
        this.b = context.getSharedPreferences("shared_pre_data", 0);
    }

    public static C0002c a(Context context) {
        if (c == null) {
            c = new C0002c(context);
        }
        return c;
    }

    public final String a(String str) {
        return this.b.getString(str, "");
    }

    public final void a(String str, int i) {
        this.b.edit().putInt(str, i).commit();
    }

    public final void a(String str, String str2) {
        this.b.edit().putString(str, str2).commit();
    }
}
