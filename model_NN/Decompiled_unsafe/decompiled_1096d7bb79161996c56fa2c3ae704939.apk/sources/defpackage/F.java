package defpackage;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import com.iPhand.FirstAid.ClassesListView;

/* renamed from: F  reason: default package */
public final class F implements AdapterView.OnItemClickListener {
    private /* synthetic */ E a;

    F(E e) {
        this.a = e;
    }

    public final void onItemClick(AdapterView adapterView, View view, int i, long j) {
        Intent intent = new Intent(this.a.a, ClassesListView.class);
        Bundle bundle = new Bundle();
        bundle.putInt("Num", i);
        bundle.putString("Title", this.a.a.a[i]);
        intent.putExtras(bundle);
        this.a.a.startActivity(intent);
    }
}
