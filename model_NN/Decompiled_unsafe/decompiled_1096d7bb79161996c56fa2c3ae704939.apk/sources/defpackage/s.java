package defpackage;

import android.util.Log;
import com.admob.android.ads.AdView;
import java.util.TimerTask;

/* renamed from: s  reason: default package */
public final class s extends TimerTask {
    private /* synthetic */ AdView a;

    public s(AdView adView) {
        this.a = adView;
    }

    public final void run() {
        if (Log.isLoggable("AdMob SDK", 3)) {
            int e = this.a.j / 1000;
            if (Log.isLoggable("AdMob SDK", 3)) {
                Log.d("AdMob SDK", "Requesting a fresh ad because a request interval passed (" + e + " seconds).");
            }
        }
        this.a.b();
    }
}
