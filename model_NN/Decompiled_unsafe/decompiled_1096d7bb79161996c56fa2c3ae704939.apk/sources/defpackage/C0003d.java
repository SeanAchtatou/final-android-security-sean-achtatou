package defpackage;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.Html;
import android.util.Log;

/* renamed from: d  reason: default package and case insensitive filesystem */
final class C0003d {
    /* access modifiers changed from: private */
    public static int c = 5000;
    /* access modifiers changed from: package-private */
    public C0005f a;
    String b;
    /* access modifiers changed from: private */
    public String d;
    /* access modifiers changed from: private */
    public Context e;
    private Bitmap f;
    private String g;
    private Bitmap h;
    private String i;

    private C0003d() {
    }

    private static int a(String str, int i2) {
        int length = str.length();
        if (i2 < 0 || i2 >= length) {
            return -1;
        }
        char charAt = str.charAt(i2);
        int i3 = i2;
        while (charAt != '>' && charAt != '<') {
            i3++;
            if (i3 >= length) {
                return -1;
            }
            charAt = str.charAt(i3);
        }
        if (charAt != '>') {
            return i3;
        }
        int i4 = i3 + 1;
        char charAt2 = str.charAt(i4);
        while (Character.isWhitespace(charAt2)) {
            i4++;
            if (i4 >= length) {
                return -1;
            }
            charAt2 = str.charAt(charAt2);
        }
        return i4;
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x004b A[SYNTHETIC, Splitter:B:22:0x004b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static android.graphics.Bitmap a(java.lang.String r6, boolean r7) {
        /*
            r0 = 0
            if (r6 == 0) goto L_0x0027
            java.net.URL r1 = new java.net.URL     // Catch:{ Throwable -> 0x0028, all -> 0x0046 }
            r1.<init>(r6)     // Catch:{ Throwable -> 0x0028, all -> 0x0046 }
            java.net.URLConnection r1 = r1.openConnection()     // Catch:{ Throwable -> 0x0028, all -> 0x0046 }
            r2 = 0
            r1.setConnectTimeout(r2)     // Catch:{ Throwable -> 0x0028, all -> 0x0046 }
            r2 = 0
            r1.setReadTimeout(r2)     // Catch:{ Throwable -> 0x0028, all -> 0x0046 }
            r1.setUseCaches(r7)     // Catch:{ Throwable -> 0x0028, all -> 0x0046 }
            r1.connect()     // Catch:{ Throwable -> 0x0028, all -> 0x0046 }
            java.io.InputStream r2 = r1.getInputStream()     // Catch:{ Throwable -> 0x0028, all -> 0x0046 }
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r2)     // Catch:{ Throwable -> 0x0055 }
            if (r2 == 0) goto L_0x0027
            r2.close()     // Catch:{ IOException -> 0x004f }
        L_0x0027:
            return r0
        L_0x0028:
            r1 = move-exception
            r2 = r0
        L_0x002a:
            java.lang.String r3 = "AdMob SDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0053 }
            java.lang.String r5 = "Problem getting image:  "
            r4.<init>(r5)     // Catch:{ all -> 0x0053 }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ all -> 0x0053 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0053 }
            android.util.Log.w(r3, r4, r1)     // Catch:{ all -> 0x0053 }
            if (r2 == 0) goto L_0x0027
            r2.close()     // Catch:{ IOException -> 0x0044 }
            goto L_0x0027
        L_0x0044:
            r1 = move-exception
            goto L_0x0027
        L_0x0046:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x0049:
            if (r2 == 0) goto L_0x004e
            r2.close()     // Catch:{ IOException -> 0x0051 }
        L_0x004e:
            throw r0
        L_0x004f:
            r1 = move-exception
            goto L_0x0027
        L_0x0051:
            r1 = move-exception
            goto L_0x004e
        L_0x0053:
            r0 = move-exception
            goto L_0x0049
        L_0x0055:
            r1 = move-exception
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.C0003d.a(java.lang.String, boolean):android.graphics.Bitmap");
    }

    public final void b() {
        Log.i("AdMob SDK", "Ad clicked.");
        if (this.d != null) {
            new C0004e(this).start();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: d.a(java.lang.String, boolean):android.graphics.Bitmap
     arg types: [java.lang.String, int]
     candidates:
      d.a(java.lang.String, int):int
      d.a(java.lang.String, boolean):android.graphics.Bitmap */
    public final Bitmap c() {
        if (this.f == null) {
            this.f = a(this.g, true);
            if (this.f == null) {
                Log.w("AdMob SDK", "Could not get icon for ad from " + this.g);
            }
        }
        return this.f;
    }

    public final boolean equals(Object obj) {
        if (obj instanceof C0003d) {
            return toString().equals(((C0003d) obj).toString());
        }
        return false;
    }

    public final int hashCode() {
        return toString().hashCode();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: d.a(java.lang.String, boolean):android.graphics.Bitmap
     arg types: [java.lang.String, int]
     candidates:
      d.a(java.lang.String, int):int
      d.a(java.lang.String, boolean):android.graphics.Bitmap */
    public static C0003d a(Context context, String str, String str2) {
        if (str == null || str.equals("")) {
            return null;
        }
        C0003d dVar = new C0003d();
        dVar.e = context;
        dVar.g = str2;
        try {
            int indexOf = str.indexOf("<a ");
            if (indexOf >= 0) {
                int indexOf2 = str.indexOf(" href=\"", indexOf) + 7;
                int indexOf3 = str.indexOf("\"", indexOf2);
                dVar.d = str.substring(indexOf2, indexOf3);
                indexOf = a(str, indexOf3 + 1);
                if (indexOf < 0) {
                    return null;
                }
            }
            if (indexOf >= 0 && str.indexOf("<img", indexOf) == indexOf) {
                int indexOf4 = str.indexOf(" src=\"", indexOf) + 6;
                dVar.i = str.substring(indexOf4, str.indexOf("\"", indexOf4));
                int indexOf5 = str.indexOf(" height=\"", indexOf) + 9;
                Integer.valueOf(str.substring(indexOf5, str.indexOf("\"", indexOf5))).intValue();
                int indexOf6 = str.indexOf(" width=\"", indexOf) + 8;
                int indexOf7 = str.indexOf("\"", indexOf6);
                Integer.valueOf(str.substring(indexOf6, indexOf7)).intValue();
                indexOf = str.indexOf("<a", indexOf7 + 1);
                if (indexOf >= 0) {
                    indexOf = a(str, indexOf + 2);
                }
            }
            if (indexOf >= 0) {
                dVar.b = str.substring(indexOf, str.indexOf("<", indexOf)).trim();
                dVar.b = Html.fromHtml(dVar.b).toString();
            }
            if (dVar.i != null) {
                if (dVar.h == null && dVar.i != null) {
                    dVar.h = a(dVar.i, false);
                }
                if (dVar.h == null) {
                    return null;
                }
            }
            if (dVar.g != null) {
                dVar.c();
            }
            return dVar;
        } catch (Exception e2) {
            Log.e("AdMob SDK", "Failed to parse ad response:  " + str, e2);
            return null;
        }
    }

    public final String toString() {
        String str = this.b;
        return str == null ? "" : str;
    }
}
