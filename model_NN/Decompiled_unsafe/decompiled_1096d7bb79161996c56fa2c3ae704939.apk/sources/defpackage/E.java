package defpackage;

import android.view.animation.Animation;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import com.iPhand.FirstAid.FirstAid;
import com.iPhand.FirstAid.R;
import java.util.ArrayList;
import java.util.HashMap;

/* renamed from: E  reason: default package */
public final class E implements Animation.AnimationListener {
    /* access modifiers changed from: private */
    public /* synthetic */ FirstAid a;

    public E(FirstAid firstAid) {
        this.a = firstAid;
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }

    public final void onAnimationEnd(Animation animation) {
        this.a.setContentView((int) R.layout.dialogueclass_list);
        this.a.c = (ListView) this.a.findViewById(R.id.DialogueClassList);
        FirstAid firstAid = this.a;
        String[] strArr = this.a.a;
        try {
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < strArr.length; i++) {
                HashMap hashMap = new HashMap();
                hashMap.put("Image", Integer.valueOf(firstAid.b[i]));
                hashMap.put("ItemTitle", strArr[i]);
                arrayList.add(hashMap);
            }
            firstAid.c.setAdapter((ListAdapter) new SimpleAdapter(firstAid, arrayList, R.layout.dialogueclass_list_itme, new String[]{"Image", "ItemTitle"}, new int[]{R.id.DialogueclassImage, R.id.DialogueclassTitle}));
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.a.c.setOnItemClickListener(new F(this));
    }
}
