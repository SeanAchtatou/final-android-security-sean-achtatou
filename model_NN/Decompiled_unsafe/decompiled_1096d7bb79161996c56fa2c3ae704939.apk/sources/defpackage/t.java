package defpackage;

import android.view.animation.Animation;
import com.admob.android.ads.AdView;

/* renamed from: t  reason: default package */
public final class t implements Animation.AnimationListener {
    private /* synthetic */ AdView a;
    private /* synthetic */ C0006g b;

    public t(AdView adView, C0006g gVar) {
        this.a = adView;
        this.b = gVar;
    }

    public final void onAnimationEnd(Animation animation) {
        this.a.post(new v(this.a, this.b));
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
