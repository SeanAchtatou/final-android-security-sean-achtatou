package defpackage;

import android.content.Context;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Locale;
import java.util.Properties;

/* renamed from: m  reason: default package and case insensitive filesystem */
final class C0012m {
    /* access modifiers changed from: private */
    public static String a = "admob_cache";
    /* access modifiers changed from: private */
    public static String b = "http://mm.admob.com/static/android/i18n/20091123";
    private static String c = "en";
    /* access modifiers changed from: private */
    public static String d = ".properties";
    private static C0012m e = null;
    private static Context f = null;
    /* access modifiers changed from: private */
    public Context g;
    /* access modifiers changed from: private */
    public String h = null;
    private Properties i;

    private boolean d() {
        if (this.i == null) {
            try {
                Properties properties = new Properties();
                File file = new File(this.g.getDir(a, 0), "20091123");
                if (!file.exists()) {
                    file.mkdir();
                }
                File file2 = new File(file, this.h + d);
                if (file2.exists()) {
                    properties.load(new FileInputStream(file2));
                    this.i = properties;
                }
            } catch (IOException e2) {
                this.i = null;
            }
        }
        return this.i != null;
    }

    private C0012m(Context context) {
        this.g = context;
        Locale locale = Locale.getDefault();
        String language = locale.getLanguage();
        if (language != this.h) {
            this.i = null;
            this.h = language;
            if (this.h == null || this.h.equals("")) {
                this.h = c;
            }
            if (!d()) {
                new o(this, (byte) 0).start();
            }
        }
    }

    public static void a(Context context) {
        if (f == null) {
            f = context;
        }
        if (e == null) {
            e = new C0012m(f);
        }
    }

    public static String a(String str) {
        String property;
        a(f);
        C0012m mVar = e;
        mVar.d();
        return (mVar.i == null || (property = mVar.i.getProperty(str)) == null || property.equals("")) ? str : property;
    }
}
