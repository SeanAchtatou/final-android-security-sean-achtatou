package defpackage;

/* renamed from: b  reason: default package and case insensitive filesystem */
public final class C0001b {
    private static final char[] a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    public static String a(String str) {
        char[] charArray = str.toCharArray();
        for (int i = 0; i < charArray.length; i++) {
            for (int i2 = 0; i2 < 16; i2++) {
                charArray[i] = (char) (charArray[i] ^ a[i2]);
                if (((byte) charArray[i]) > 32 && ((byte) charArray[i]) < 125 && ((byte) charArray[i]) != 92) {
                    break;
                }
            }
        }
        return new String(charArray);
    }
}
