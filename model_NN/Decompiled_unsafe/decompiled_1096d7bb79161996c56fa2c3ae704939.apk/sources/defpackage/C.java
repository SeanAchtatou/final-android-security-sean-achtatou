package defpackage;

import android.view.animation.Animation;
import com.iPhand.FirstAid.DetailView;

/* renamed from: C  reason: default package */
public final class C implements Animation.AnimationListener {
    private /* synthetic */ DetailView a;

    private C(DetailView detailView) {
        this.a = detailView;
    }

    public /* synthetic */ C(DetailView detailView, byte b) {
        this(detailView);
    }

    public final void onAnimationEnd(Animation animation) {
        this.a.i.post(new D(this.a));
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
