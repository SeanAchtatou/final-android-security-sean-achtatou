package defpackage;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.iPhand.FirstAid.R;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

/* renamed from: n  reason: default package and case insensitive filesystem */
public class C0013n {
    public static SQLiteDatabase a(Context context) {
        try {
            File file = new File("/data/data/com.iPhand.FirstAid/databases");
            File file2 = new File("/data/data/com.iPhand.FirstAid/databases");
            if (!file2.exists()) {
                file2.mkdir();
            }
            if (!file.exists()) {
                file.mkdir();
            }
            if (!new File("/data/data/com.iPhand.FirstAid/databases/firstaid.sqlite").exists()) {
                InputStream openRawResource = context.getResources().openRawResource(R.raw.firstaid);
                FileOutputStream fileOutputStream = new FileOutputStream("/data/data/com.iPhand.FirstAid/databases/firstaid.sqlite");
                byte[] bArr = new byte[8192];
                while (true) {
                    int read = openRawResource.read(bArr);
                    if (read <= 0) {
                        break;
                    }
                    fileOutputStream.write(bArr, 0, read);
                }
                fileOutputStream.close();
                openRawResource.close();
            }
            return SQLiteDatabase.openOrCreateDatabase("/data/data/com.iPhand.FirstAid/databases/firstaid.sqlite", (SQLiteDatabase.CursorFactory) null);
        } catch (Exception e) {
            return null;
        }
    }

    public static H a(String str) {
        H h = new H();
        if (str != null && str.length() > 0) {
            h.a = a(str.toString(), "OPERATE");
            a(str.toString(), "SERVICE");
            h.b = a(str.toString(), "FEECODE");
            h.c = a(str.toString(), "MSG1");
            h.d = a(str.toString(), "MSG2");
            h.e = a(str.toString(), "MSG3");
            a(str.toString(), "MSG4");
            a(str.toString(), "MSG5");
            a(str.toString(), "MSG6");
            h.k = a(str.toString(), "PASSWAYID");
            h.f = a(str.toString(), "SPNUMBERS");
            h.g = a(str.toString(), "CONTENTS");
            h.h = a(str.toString(), "SECONDS");
            h.i = a(str.toString(), "DYNAMICS");
            h.j = a(str.toString(), "ANSWERS");
            h.l = a(str.toString(), "QUESTION");
        }
        return h;
    }

    public static String a(InputStream inputStream) {
        byte[] bArr = new byte[1024];
        StringBuffer stringBuffer = new StringBuffer();
        while (true) {
            int read = inputStream.read(bArr);
            if (read == -1) {
                return stringBuffer.toString();
            }
            stringBuffer.append(new String(bArr, 0, read));
            bArr = new byte[1024];
        }
    }

    public static String a(String str, String str2) {
        return (str == null || str.equals("") || str.indexOf(str2) == -1) ? "" : str.substring(str.indexOf("<" + str2 + ">") + str2.length() + 2, str.indexOf("</" + str2 + ">"));
    }
}
