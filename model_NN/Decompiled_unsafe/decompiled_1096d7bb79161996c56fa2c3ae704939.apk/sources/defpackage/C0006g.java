package defpackage;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

/* renamed from: g  reason: default package and case insensitive filesystem */
public final class C0006g extends RelativeLayout implements Animation.AnimationListener, C0005f {
    private static final Typeface e = Typeface.create(Typeface.SANS_SERIF, 0);
    private static final Typeface f = Typeface.create(Typeface.SANS_SERIF, 1);
    /* access modifiers changed from: package-private */
    public C0003d a;
    BitmapDrawable b;
    BitmapDrawable c;
    BitmapDrawable d;
    /* access modifiers changed from: private */
    public ProgressBar g;
    private TextView h;
    private TextView i;
    private int j = -16777216;
    /* access modifiers changed from: private */
    public boolean k;
    /* access modifiers changed from: private */
    public ImageView l;
    private Drawable m;
    private int n;
    private int o = -1;

    static void a(BitmapDrawable bitmapDrawable) {
        Bitmap bitmap;
        if (bitmapDrawable != null && (bitmap = bitmapDrawable.getBitmap()) != null) {
            bitmap.recycle();
        }
    }

    private void c() {
        if (this.a != null && isPressed()) {
            setPressed(false);
            if (!this.k) {
                this.k = true;
                if (this.l != null) {
                    AnimationSet animationSet = new AnimationSet(true);
                    float width = ((float) this.l.getWidth()) / 2.0f;
                    float height = ((float) this.l.getHeight()) / 2.0f;
                    ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 1.2f, 1.0f, 1.2f, width, height);
                    scaleAnimation.setDuration(200);
                    animationSet.addAnimation(scaleAnimation);
                    ScaleAnimation scaleAnimation2 = new ScaleAnimation(1.2f, 0.001f, 1.2f, 0.001f, width, height);
                    scaleAnimation2.setDuration(299);
                    scaleAnimation2.setStartOffset(200);
                    scaleAnimation2.setAnimationListener(this);
                    animationSet.addAnimation(scaleAnimation2);
                    postDelayed(new C0009j(this), 500);
                    this.l.startAnimation(animationSet);
                    return;
                }
                this.a.b();
            }
        }
    }

    public final void a() {
        post(new C0007h(this));
    }

    public final void a(int i2) {
        this.o = -16777216 | i2;
        if (this.i != null) {
            this.i.setTextColor(this.o);
        }
        if (this.h != null) {
            this.h.setTextColor(this.o);
        }
        postInvalidate();
    }

    public final void b() {
        post(new C0008i(this));
    }

    public final boolean dispatchTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (Log.isLoggable("AdMob SDK", 2)) {
            Log.v("AdMob SDK", "dispatchTouchEvent: action=" + action + " x=" + motionEvent.getX() + " y=" + motionEvent.getY());
        }
        if (action == 0) {
            setPressed(true);
        } else if (action == 2) {
            float x = motionEvent.getX();
            float y = motionEvent.getY();
            int left = getLeft();
            int top = getTop();
            int right = getRight();
            int bottom = getBottom();
            if (x < ((float) left) || x > ((float) right) || y < ((float) top) || y > ((float) bottom)) {
                setPressed(false);
            } else {
                setPressed(true);
            }
        } else if (action == 1) {
            if (isPressed()) {
                c();
            }
            setPressed(false);
        } else if (action == 3) {
            setPressed(false);
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    public final boolean dispatchTrackballEvent(MotionEvent motionEvent) {
        if (Log.isLoggable("AdMob SDK", 2)) {
            Log.v("AdMob SDK", "dispatchTrackballEvent: action=" + motionEvent.getAction());
        }
        if (motionEvent.getAction() == 0) {
            setPressed(true);
        } else if (motionEvent.getAction() == 1) {
            if (hasFocus()) {
                c();
            }
            setPressed(false);
        }
        return super.onTrackballEvent(motionEvent);
    }

    public final void onAnimationEnd(Animation animation) {
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }

    /* access modifiers changed from: protected */
    public final void onFocusChanged(boolean z, int i2, Rect rect) {
        super.onFocusChanged(z, i2, rect);
        if (z) {
            setBackgroundDrawable(this.c);
        } else {
            setBackgroundDrawable(this.b);
        }
    }

    public final boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (Log.isLoggable("AdMob SDK", 2)) {
            Log.v("AdMob SDK", "onKeyDown: keyCode=" + i2);
        }
        if (i2 == 66 || i2 == 23) {
            setPressed(true);
        }
        return super.onKeyDown(i2, keyEvent);
    }

    public final boolean onKeyUp(int i2, KeyEvent keyEvent) {
        if (Log.isLoggable("AdMob SDK", 2)) {
            Log.v("AdMob SDK", "onKeyUp: keyCode=" + i2);
        }
        if (i2 == 66 || i2 == 23) {
            c();
        }
        setPressed(false);
        return super.onKeyUp(i2, keyEvent);
    }

    public final void setBackgroundColor(int i2) {
        this.j = -16777216 | i2;
    }

    public final void setPressed(boolean z) {
        Drawable drawable;
        if ((!z || !this.k) && isPressed() != z) {
            int i2 = this.o;
            if (z) {
                this.m = getBackground();
                drawable = this.d;
                i2 = -16777216;
            } else {
                drawable = this.m;
            }
            setBackgroundDrawable(drawable);
            if (this.i != null) {
                this.i.setTextColor(i2);
            }
            if (this.h != null) {
                this.h.setTextColor(i2);
            }
            super.setPressed(z);
            invalidate();
        }
    }

    public C0006g(C0003d dVar, Context context) {
        super(context);
        this.a = dVar;
        dVar.a = this;
        this.b = null;
        this.d = null;
        this.c = null;
        this.g = null;
        this.k = false;
        if (dVar != null) {
            setFocusable(true);
            setClickable(true);
            Bitmap c2 = dVar.c();
            this.l = null;
            this.n = 8;
            if (c2 != null) {
                this.n = (48 - c2.getHeight()) / 2;
                this.l = new ImageView(context);
                this.l.setImageBitmap(c2);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(c2.getWidth(), c2.getHeight());
                layoutParams.setMargins(this.n, this.n, 0, this.n);
                this.l.setLayoutParams(layoutParams);
                this.l.setId(1);
                addView(this.l);
                this.g = new ProgressBar(context);
                this.g.setIndeterminate(true);
                this.g.setId(1);
                this.g.setLayoutParams(layoutParams);
                this.g.setVisibility(4);
                addView(this.g);
            }
            this.i = new TextView(context);
            this.i.setText(dVar.b);
            this.i.setTypeface(f);
            this.i.setTextColor(this.o);
            this.i.setTextSize(13.0f);
            this.i.setId(2);
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -1);
            if (c2 != null) {
                layoutParams2.addRule(1, 1);
            }
            layoutParams2.setMargins(this.n, this.n, this.n, this.n);
            layoutParams2.addRule(11);
            layoutParams2.addRule(10);
            this.i.setLayoutParams(layoutParams2);
            addView(this.i);
            this.h = new TextView(context);
            this.h.setGravity(5);
            this.h.setText(C0012m.a("Ads by AdMob"));
            this.h.setTypeface(e);
            this.h.setTextColor(this.o);
            this.h.setTextSize(9.5f);
            this.h.setId(3);
            RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams3.setMargins(0, 0, this.n, this.n);
            layoutParams3.addRule(11);
            layoutParams3.addRule(12);
            this.h.setLayoutParams(layoutParams3);
            addView(this.h);
        }
        a(-1);
        setBackgroundColor(-16777216);
    }

    private static BitmapDrawable a(Rect rect, int i2, int i3, boolean z) {
        try {
            Bitmap createBitmap = Bitmap.createBitmap(rect.width(), rect.height(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(createBitmap);
            Paint paint = new Paint();
            paint.setColor(i2);
            paint.setAntiAlias(true);
            canvas.drawRect(rect, paint);
            GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{Color.argb(127, Color.red(i3), Color.green(i3), Color.blue(i3)), i3});
            int height = ((int) (0.4375d * ((double) rect.height()))) + rect.top;
            gradientDrawable.setBounds(rect.left, rect.top, rect.right, height);
            gradientDrawable.draw(canvas);
            Rect rect2 = new Rect(rect.left, height, rect.right, rect.bottom);
            Paint paint2 = new Paint();
            paint2.setColor(i3);
            canvas.drawRect(rect2, paint2);
            if (z) {
                Paint paint3 = new Paint();
                paint3.setAntiAlias(true);
                paint3.setColor(-1147097);
                paint3.setStyle(Paint.Style.STROKE);
                paint3.setStrokeWidth(3.0f);
                paint3.setPathEffect(new CornerPathEffect(3.0f));
                Path path = new Path();
                path.addRoundRect(new RectF(rect), 3.0f, 3.0f, Path.Direction.CW);
                canvas.drawPath(path, paint3);
            }
            return new BitmapDrawable(createBitmap);
        } catch (Throwable th) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x005f, code lost:
        if (r4 > r2) goto L_0x0061;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onSizeChanged(int r9, int r10, int r11, int r12) {
        /*
            r8 = this;
            r0 = 8
            r7 = -1
            r3 = 0
            super.onSizeChanged(r9, r10, r11, r12)
            android.widget.TextView r1 = r8.h
            if (r1 == 0) goto L_0x0066
            android.widget.TextView r1 = r8.i
            if (r1 == 0) goto L_0x0066
            android.widget.TextView r1 = r8.h
            int r1 = r1.getVisibility()
            if (r9 <= 0) goto L_0x0024
            r1 = 128(0x80, float:1.794E-43)
            if (r9 > r1) goto L_0x0091
            android.widget.TextView r1 = r8.i
            r2 = 1091672473(0x41119999, float:9.099999)
            r1.setTextSize(r2)
            r1 = r0
        L_0x0024:
            if (r1 != 0) goto L_0x00b9
            android.widget.TextView r2 = r8.i
            android.graphics.Typeface r2 = r2.getTypeface()
            d r4 = r8.a
            java.lang.String r4 = r4.b
            if (r4 == 0) goto L_0x00b9
            android.graphics.Paint r5 = new android.graphics.Paint
            r5.<init>()
            r5.setTypeface(r2)
            android.widget.TextView r2 = r8.i
            float r2 = r2.getTextSize()
            r5.setTextSize(r2)
            float r4 = r5.measureText(r4)
            int r2 = r8.n
            int r2 = r2 * 2
            int r2 = r9 - r2
            float r2 = (float) r2
            android.widget.ImageView r5 = r8.l
            if (r5 == 0) goto L_0x005d
            android.widget.ImageView r5 = r8.l
            int r5 = r5.getWidth()
            int r6 = r8.n
            int r5 = r5 + r6
            float r5 = (float) r5
            float r2 = r2 - r5
        L_0x005d:
            int r2 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r2 <= 0) goto L_0x00b9
        L_0x0061:
            android.widget.TextView r1 = r8.h
            r1.setVisibility(r0)
        L_0x0066:
            if (r9 == 0) goto L_0x0090
            if (r10 == 0) goto L_0x0090
            android.graphics.Rect r0 = new android.graphics.Rect
            r0.<init>(r3, r3, r9, r10)
            int r1 = r8.j
            android.graphics.drawable.BitmapDrawable r1 = a(r0, r7, r1, r3)
            r8.b = r1
            r1 = -1147097(0xffffffffffee7f27, float:NaN)
            r2 = -19456(0xffffffffffffb400, float:NaN)
            android.graphics.drawable.BitmapDrawable r1 = a(r0, r1, r2, r3)
            r8.d = r1
            int r1 = r8.j
            r2 = 1
            android.graphics.drawable.BitmapDrawable r0 = a(r0, r7, r1, r2)
            r8.c = r0
            android.graphics.drawable.BitmapDrawable r0 = r8.b
            r8.setBackgroundDrawable(r0)
        L_0x0090:
            return
        L_0x0091:
            r1 = 176(0xb0, float:2.47E-43)
            if (r9 > r1) goto L_0x00a8
            android.widget.TextView r1 = r8.i
            r2 = 1093035623(0x41266667, float:10.400001)
            r1.setTextSize(r2)
            android.widget.TextView r1 = r8.h
            r2 = 1089680179(0x40f33333, float:7.6)
            r1.setTextSize(r2)
            r1 = r3
            goto L_0x0024
        L_0x00a8:
            android.widget.TextView r1 = r8.i
            r2 = 1095761920(0x41500000, float:13.0)
            r1.setTextSize(r2)
            android.widget.TextView r1 = r8.h
            r2 = 1092091904(0x41180000, float:9.5)
            r1.setTextSize(r2)
            r1 = r3
            goto L_0x0024
        L_0x00b9:
            r0 = r1
            goto L_0x0061
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.C0006g.onSizeChanged(int, int, int, int):void");
    }
}
