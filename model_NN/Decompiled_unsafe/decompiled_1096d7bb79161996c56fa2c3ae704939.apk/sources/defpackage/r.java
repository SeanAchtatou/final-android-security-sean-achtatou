package defpackage;

import android.util.Log;
import com.admob.android.ads.AdView;

/* renamed from: r  reason: default package */
public final class r implements Runnable {
    private /* synthetic */ q a;
    private /* synthetic */ boolean b;
    private /* synthetic */ C0006g c;
    private /* synthetic */ int d;

    r(q qVar, C0006g gVar, int i, boolean z) {
        this.a = qVar;
        this.c = gVar;
        this.d = i;
        this.b = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.admob.android.ads.AdView.a(com.admob.android.ads.AdView, boolean):boolean
     arg types: [com.admob.android.ads.AdView, int]
     candidates:
      com.admob.android.ads.AdView.a(com.admob.android.ads.AdView, g):g
      com.admob.android.ads.AdView.a(com.admob.android.ads.AdView, boolean):boolean */
    public final void run() {
        try {
            this.a.a.addView(this.c);
            if (this.d != 0) {
                C0006g unused = this.a.a.e = this.c;
            } else if (this.b) {
                AdView.b(this.a.a, this.c);
            } else {
                AdView.c(this.a.a, this.c);
            }
        } catch (Exception e) {
            Log.e("AdMob SDK", "Unhandled exception placing AdContainer into AdView.", e);
        } finally {
            boolean unused2 = this.a.a.l = false;
        }
    }
}
