package defpackage;

import android.graphics.drawable.BitmapDrawable;
import android.view.animation.Animation;

/* renamed from: w  reason: default package */
public final class w implements Animation.AnimationListener {
    private /* synthetic */ v a;

    w(v vVar) {
        this.a = vVar;
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }

    public final void onAnimationEnd(Animation animation) {
        if (this.a.c != null) {
            this.a.a.removeView(this.a.c);
            C0006g a2 = this.a.c;
            if (a2.b != null) {
                BitmapDrawable bitmapDrawable = a2.b;
                a2.b = null;
                C0006g.a(bitmapDrawable);
            }
            if (a2.d != null) {
                BitmapDrawable bitmapDrawable2 = a2.d;
                a2.d = null;
                C0006g.a(bitmapDrawable2);
            }
            if (a2.c != null) {
                BitmapDrawable bitmapDrawable3 = a2.c;
                a2.c = null;
                C0006g.a(bitmapDrawable3);
            }
        }
        C0006g unused = this.a.a.e = this.a.b;
    }
}
