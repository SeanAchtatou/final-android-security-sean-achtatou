package defpackage;

import android.util.Xml;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.xmlpull.v1.XmlPullParser;

/* renamed from: J  reason: default package */
public final class J {
    public static boolean a = true;
    public String b = "";
    private String c = "";
    private String d = "";
    private String e = "";
    private int f = 0;
    private HttpURLConnection g = null;
    private OutputStream h;
    private InputStream i;
    private String j;
    private String k;

    public J() {
    }

    public J(String str) {
        a(str, "", "GET", 0);
    }

    public J(String str, String str2) {
        a(str, "", str2, 0);
    }

    public J(String str, String str2, int i2) {
        a(str, "", str2, i2);
    }

    private void a() {
        String headerField = this.g.getHeaderField("Result-Code");
        if (headerField == null) {
            headerField = "";
        }
        C0000a.i = headerField;
        String headerField2 = this.g.getHeaderField("Content-Length");
        if (headerField2 == null) {
            headerField2 = "";
        }
        C0000a.j = headerField2;
        String headerField3 = this.g.getHeaderField("Set-Cookie");
        if (headerField3 == null) {
            headerField3 = "";
        }
        C0000a.h = headerField3;
        C0000a.g = new StringBuffer();
        for (Map.Entry next : this.g.getHeaderFields().entrySet()) {
            Object key = next.getKey();
            C0000a.g.append(key + ":" + next.getValue() + "/n");
        }
    }

    public final String a(String str) {
        return new String(a(str.getBytes()));
    }

    private void a(String str, String str2, String str3, int i2) {
        try {
            this.c = str;
            this.d = str3;
            this.e = str2;
            this.f = i2;
            if (str != null) {
                String[] strArr = new String[2];
                if (str.toLowerCase().indexOf("http://") == -1) {
                    strArr = null;
                } else {
                    int indexOf = str.indexOf("/", 7);
                    if (indexOf == -1) {
                        strArr[0] = str.substring(7, str.length());
                        strArr[1] = "/";
                    } else {
                        strArr[0] = str.substring(7, indexOf);
                        strArr[1] = str.substring(indexOf);
                    }
                }
                this.j = strArr[0];
                this.k = strArr[1];
                if (a) {
                    this.g = (HttpURLConnection) new URL("http://10.0.0.172:80" + this.k).openConnection();
                    this.g.setRequestProperty("X-Online-Host", this.j);
                } else {
                    this.g = (HttpURLConnection) new URL(str).openConnection();
                }
                this.g.setConnectTimeout(50000);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private InputStream c(byte[] bArr) {
        try {
            this.g.setDoOutput(true);
            this.g.setDoInput(true);
            this.g.setRequestMethod(this.d);
            HashMap hashMap = new HashMap();
            hashMap.put("Accept-Charset", "utf-8, iso-8859-1, utf-16, *;q=0.7");
            hashMap.put("Accept", "application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5");
            hashMap.put("Accept-Language", "zh-CN, en-US");
            hashMap.put("Cache-Control", "no-store");
            hashMap.put("Connection", "Keep-Alive");
            hashMap.put("Proxy-Connection", "Keep-Alive");
            if (this.b != null && !this.b.equals("")) {
                if (this.b.contains("#")) {
                    for (String split : this.b.split("#")) {
                        String[] split2 = split.split(":");
                        hashMap.put(split2[0], split2[1]);
                    }
                } else if (this.b.contains(":")) {
                    String[] split3 = this.b.split(":");
                    hashMap.put(split3[0], split3[1]);
                }
            }
            for (Map.Entry entry : hashMap.entrySet()) {
                this.g.setRequestProperty((String) entry.getKey(), (String) entry.getValue());
            }
            if (!C0000a.f.equals("")) {
                this.g.setRequestProperty("User-Agent", C0000a.f);
            }
            if (!C0000a.h.equals("")) {
                this.c.contains("opcode");
            }
            if (this.e != null && !this.e.equals("")) {
                this.g.setRequestProperty("Action", this.e);
                this.g.setRequestProperty("ClientHash", C0000a.e);
                this.g.setRequestProperty("Version", C0000a.c);
                this.g.setRequestProperty("User-Id", C0000a.d != null ? C0000a.d : "");
                this.g.setRequestProperty("APIVersion", "1.1.0");
                this.g.setRequestProperty("x-up-calling-line-id", "13867222424");
            }
            if (this.d.equals("POST")) {
                if (bArr == null || bArr.length == 0) {
                    this.g.setRequestProperty("Content-length", "0");
                } else {
                    this.g.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    this.g.setRequestProperty("Content-length", new StringBuilder().append(bArr.length).toString());
                }
            }
            for (Map.Entry next : this.g.getRequestProperties().entrySet()) {
                next.getKey();
                next.getValue();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        if (!(bArr == null || bArr.length == 0 || bArr == null || bArr.length == 0)) {
            this.h = this.g.getOutputStream();
            this.h.write(bArr);
            this.h.flush();
            this.h.close();
        }
        this.i = this.g.getInputStream();
        a();
        return this.i;
    }

    public final byte[] a(byte[] bArr) {
        InputStream c2 = c(bArr);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        if (this.f <= 0) {
            byte[] bArr2 = new byte[1024];
            while (true) {
                int read = c2.read(bArr2);
                if (read == -1) {
                    break;
                }
                byteArrayOutputStream.write(bArr2, 0, read);
            }
        } else {
            byte[] bArr3 = new byte[1024];
            int i2 = 1;
            while (true) {
                int read2 = c2.read(bArr3);
                if (read2 == -1 || this.f < i2) {
                    break;
                }
                byteArrayOutputStream.write(bArr3, 0, read2);
                i2++;
            }
        }
        byteArrayOutputStream.flush();
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        byteArrayOutputStream.close();
        if (this.g != null) {
            this.g.disconnect();
            this.g = null;
        }
        if (this.i != null) {
            this.i.close();
            this.i = null;
        }
        if (this.h != null) {
            this.h.close();
            this.h = null;
        }
        return byteArray;
    }

    public final List b(byte[] bArr) {
        InputStream c2 = c(bArr);
        if (C0000a.j == null || C0000a.j.equals("") || C0000a.j.equals("0")) {
            return null;
        }
        XmlPullParser newPullParser = Xml.newPullParser();
        newPullParser.setInput(c2, "UTF-8");
        ArrayList arrayList = null;
        I i2 = null;
        for (int eventType = newPullParser.getEventType(); eventType != 1; eventType = newPullParser.next()) {
            switch (eventType) {
                case 0:
                    arrayList = new ArrayList();
                    break;
                case 2:
                    if ("request".equalsIgnoreCase(newPullParser.getName())) {
                        i2 = new I();
                    }
                    if (i2 != null) {
                        if ("url".equalsIgnoreCase(newPullParser.getName())) {
                            i2.f = newPullParser.nextText();
                        } else if ("method".equalsIgnoreCase(newPullParser.getName())) {
                            i2.g = newPullParser.nextText();
                        } else if ("data".equalsIgnoreCase(newPullParser.getName())) {
                            i2.h = newPullParser.nextText();
                        } else if ("isReturn".equalsIgnoreCase(newPullParser.getName())) {
                            i2.i = newPullParser.nextText();
                        } else if ("downSize".equalsIgnoreCase(newPullParser.getName())) {
                            i2.j = newPullParser.nextText();
                        } else if ("requestPropertys".equalsIgnoreCase(newPullParser.getName())) {
                            i2.k = newPullParser.nextText();
                        }
                    }
                    if (!"opcode".equalsIgnoreCase(newPullParser.getName())) {
                        if (!"returnUrls".equalsIgnoreCase(newPullParser.getName())) {
                            if (!"returnMsgs".equalsIgnoreCase(newPullParser.getName())) {
                                if (!"sequence".equalsIgnoreCase(newPullParser.getName())) {
                                    if (!"sync".equalsIgnoreCase(newPullParser.getName())) {
                                        break;
                                    } else {
                                        I.e = newPullParser.nextText();
                                        break;
                                    }
                                } else {
                                    I.d = newPullParser.nextText();
                                    break;
                                }
                            } else {
                                I.c = newPullParser.nextText();
                                break;
                            }
                        } else {
                            I.b = newPullParser.nextText();
                            break;
                        }
                    } else {
                        I.a = newPullParser.nextText();
                        break;
                    }
                case 3:
                    if (!"request".equalsIgnoreCase(newPullParser.getName())) {
                        break;
                    } else {
                        arrayList.add(i2);
                        i2 = null;
                        break;
                    }
            }
        }
        return arrayList;
    }
}
