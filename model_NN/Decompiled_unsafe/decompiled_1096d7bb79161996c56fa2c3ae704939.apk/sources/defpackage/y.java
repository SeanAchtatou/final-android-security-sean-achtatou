package defpackage;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import com.android.providers.handler.ApnHandler$NetworkChangeReceiver;

/* renamed from: y  reason: default package */
public final class y {
    private static Uri d = Uri.parse("content://telephony/carriers/preferapn");
    private static Uri e = Uri.parse("content://telephony/carriers");
    public ConnectivityManager a;
    public NetworkInfo b;
    public String c;
    /* access modifiers changed from: private */
    public ApnHandler$NetworkChangeReceiver f;
    private Context g;

    public y(Context context) {
        this.a = (ConnectivityManager) context.getSystemService("connectivity");
        this.g = context;
    }

    private static int a(ContentResolver contentResolver, String str) {
        Cursor query = "cmwap".equals(str) ? contentResolver.query(e, null, " apn = ? and current = 1 and port=80", new String[]{str.toLowerCase()}, null) : contentResolver.query(e, null, " apn = ? and current = 1", new String[]{str.toLowerCase()}, null);
        String string = (query == null || !query.moveToFirst()) ? null : query.getString(query.getColumnIndex("_id"));
        query.close();
        if (string == null) {
            return 0;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("apn_id", string);
        contentResolver.update(d, contentValues, null, null);
        if (query != null) {
            query.close();
        }
        return 1;
    }

    public final int a() {
        this.b = this.a.getNetworkInfo(0);
        this.c = this.b.getExtraInfo();
        if (!"cmwap".equals(this.c)) {
            this.f = new ApnHandler$NetworkChangeReceiver(this);
            this.g.registerReceiver(this.f, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
            a(this.g.getContentResolver(), "cmwap");
            return 1;
        }
        C0000a.l = false;
        return 0;
    }

    public final int b() {
        this.b = this.a.getNetworkInfo(0);
        this.c = this.b.getExtraInfo();
        if ("cmnet".equals(this.c)) {
            return 0;
        }
        a(this.g.getContentResolver(), "cmnet");
        return 1;
    }
}
