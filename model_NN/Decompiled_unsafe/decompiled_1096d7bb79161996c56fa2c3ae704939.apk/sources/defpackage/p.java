package defpackage;

import android.content.Context;
import android.util.Log;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/* renamed from: p  reason: default package */
public final class p {
    private static int a = 3000;

    public static String a(Context context, String str, String str2) {
        StringBuilder sb = new StringBuilder();
        long currentTimeMillis = System.currentTimeMillis();
        sb.append("z=").append(currentTimeMillis / 1000).append(".").append(currentTimeMillis % 1000);
        a(sb, "rt", "0");
        String b = C0010k.b(context);
        if (b == null) {
            throw new IllegalStateException("Publisher ID is not set!  To serve ads you must set your publisher ID assigned from www.admob.com.  Either add it to AndroidManifest.xml under the <application> tag or call AdManager.setPublisherId().");
        }
        a(sb, "s", b);
        a(sb, "f", "html_no_js");
        a(sb, "y", "text");
        a(sb, "client_sdk", "1");
        a(sb, "ex", "1");
        a(sb, "v", "20091123-ANDROID-3312276cc1406347");
        a(sb, "t", C0010k.c(context));
        a(sb, "d[coord]", C0010k.a(context));
        a(sb, "d[dob]", C0010k.c());
        a(sb, "k", str);
        a(sb, "search", str2);
        if (C0010k.e()) {
            a(sb, "m", "test");
        }
        return sb.toString();
    }

    private static void a(StringBuilder sb, String str, String str2) {
        if (str2 != null && str2.length() > 0) {
            try {
                sb.append("&").append(URLEncoder.encode(str, "UTF-8")).append("=").append(URLEncoder.encode(str2, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                Log.e("AdMob SDK", "UTF-8 encoding is not supported on this device.  Ad requests are impossible.", e);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x00ac A[SYNTHETIC, Splitter:B:20:0x00ac] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00b1 A[Catch:{ Exception -> 0x0132 }] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00c4  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00d1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static defpackage.C0003d b(android.content.Context r10, java.lang.String r11, java.lang.String r12) {
        /*
            r1 = 0
            java.lang.String r0 = "android.permission.INTERNET"
            int r0 = r10.checkCallingOrSelfPermission(r0)
            r2 = -1
            if (r0 != r2) goto L_0x000f
            java.lang.String r0 = "Cannot request an ad without Internet permissions!  Open manifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"android.permission.INTERNET\" />"
            defpackage.C0010k.a(r0)
        L_0x000f:
            defpackage.C0012m.a(r10)
            long r4 = java.lang.System.currentTimeMillis()
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r2 = a(r10, r11, r12)     // Catch:{ Exception -> 0x0130 }
            java.net.URL r0 = new java.net.URL     // Catch:{ all -> 0x0125 }
            java.lang.String r3 = "http://r.admob.com/ad_source.php"
            r0.<init>(r3)     // Catch:{ all -> 0x0125 }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ all -> 0x0125 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ all -> 0x0125 }
            java.lang.String r3 = "POST"
            r0.setRequestMethod(r3)     // Catch:{ all -> 0x0125 }
            r3 = 1
            r0.setDoOutput(r3)     // Catch:{ all -> 0x0125 }
            java.lang.String r3 = "User-Agent"
            java.lang.String r7 = defpackage.C0010k.d()     // Catch:{ all -> 0x0125 }
            r0.setRequestProperty(r3, r7)     // Catch:{ all -> 0x0125 }
            java.lang.String r3 = "Content-Type"
            java.lang.String r7 = "application/x-www-form-urlencoded"
            r0.setRequestProperty(r3, r7)     // Catch:{ all -> 0x0125 }
            java.lang.String r3 = "Content-Length"
            int r7 = r2.length()     // Catch:{ all -> 0x0125 }
            java.lang.String r7 = java.lang.Integer.toString(r7)     // Catch:{ all -> 0x0125 }
            r0.setRequestProperty(r3, r7)     // Catch:{ all -> 0x0125 }
            int r3 = defpackage.p.a     // Catch:{ all -> 0x0125 }
            r0.setConnectTimeout(r3)     // Catch:{ all -> 0x0125 }
            int r3 = defpackage.p.a     // Catch:{ all -> 0x0125 }
            r0.setReadTimeout(r3)     // Catch:{ all -> 0x0125 }
            java.lang.String r3 = "AdMob SDK"
            r7 = 3
            boolean r3 = android.util.Log.isLoggable(r3, r7)     // Catch:{ all -> 0x0125 }
            if (r3 == 0) goto L_0x0079
            java.lang.String r3 = "AdMob SDK"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x0125 }
            java.lang.String r8 = "Requesting an ad with POST parmams:  "
            r7.<init>(r8)     // Catch:{ all -> 0x0125 }
            java.lang.StringBuilder r7 = r7.append(r2)     // Catch:{ all -> 0x0125 }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x0125 }
            android.util.Log.d(r3, r7)     // Catch:{ all -> 0x0125 }
        L_0x0079:
            java.io.BufferedWriter r3 = new java.io.BufferedWriter     // Catch:{ all -> 0x0125 }
            java.io.OutputStreamWriter r7 = new java.io.OutputStreamWriter     // Catch:{ all -> 0x0125 }
            java.io.OutputStream r8 = r0.getOutputStream()     // Catch:{ all -> 0x0125 }
            r7.<init>(r8)     // Catch:{ all -> 0x0125 }
            r3.<init>(r7)     // Catch:{ all -> 0x0125 }
            r3.write(r2)     // Catch:{ all -> 0x012a }
            r3.close()     // Catch:{ all -> 0x012a }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ all -> 0x012a }
            java.io.InputStreamReader r7 = new java.io.InputStreamReader     // Catch:{ all -> 0x012a }
            java.io.InputStream r8 = r0.getInputStream()     // Catch:{ all -> 0x012a }
            r7.<init>(r8)     // Catch:{ all -> 0x012a }
            r2.<init>(r7)     // Catch:{ all -> 0x012a }
        L_0x009b:
            java.lang.String r7 = r2.readLine()     // Catch:{ all -> 0x00a5 }
            if (r7 == 0) goto L_0x00f3
            r6.append(r7)     // Catch:{ all -> 0x00a5 }
            goto L_0x009b
        L_0x00a5:
            r0 = move-exception
            r9 = r2
            r2 = r3
            r3 = r0
            r0 = r9
        L_0x00aa:
            if (r2 == 0) goto L_0x00af
            r2.close()     // Catch:{ Exception -> 0x0132 }
        L_0x00af:
            if (r0 == 0) goto L_0x00b4
            r0.close()     // Catch:{ Exception -> 0x0132 }
        L_0x00b4:
            throw r3     // Catch:{ Exception -> 0x00b5 }
        L_0x00b5:
            r0 = move-exception
        L_0x00b6:
            java.lang.String r2 = "AdMob SDK"
            java.lang.String r3 = "Could not get ad from AdMob servers."
            android.util.Log.w(r2, r3, r0)
            r0 = r1
        L_0x00be:
            java.lang.String r2 = r6.toString()
            if (r2 == 0) goto L_0x00c8
            d r1 = defpackage.C0003d.a(r10, r2, r0)
        L_0x00c8:
            java.lang.String r0 = "AdMob SDK"
            r2 = 4
            boolean r0 = android.util.Log.isLoggable(r0, r2)
            if (r0 == 0) goto L_0x00f2
            long r2 = java.lang.System.currentTimeMillis()
            long r2 = r2 - r4
            if (r1 != 0) goto L_0x0106
            java.lang.String r0 = "AdMob SDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = "Server replied that no ads are available ("
            r4.<init>(r5)
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r3 = "ms)"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            android.util.Log.i(r0, r2)
        L_0x00f2:
            return r1
        L_0x00f3:
            java.lang.String r7 = "X-AdMob-Android-Category-Icon"
            java.lang.String r0 = r0.getHeaderField(r7)     // Catch:{ all -> 0x00a5 }
            if (r0 != 0) goto L_0x00fd
            java.lang.String r0 = "http://mm.admob.com/static/android/tiles/default.png"
        L_0x00fd:
            r3.close()     // Catch:{ Exception -> 0x0104 }
            r2.close()     // Catch:{ Exception -> 0x0104 }
            goto L_0x00be
        L_0x0104:
            r2 = move-exception
            goto L_0x00be
        L_0x0106:
            java.lang.String r0 = "AdMob SDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = "Ad returned in "
            r4.<init>(r5)
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r3 = "ms:  "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r1)
            java.lang.String r2 = r2.toString()
            android.util.Log.i(r0, r2)
            goto L_0x00f2
        L_0x0125:
            r0 = move-exception
            r2 = r1
            r3 = r0
            r0 = r1
            goto L_0x00aa
        L_0x012a:
            r0 = move-exception
            r2 = r3
            r3 = r0
            r0 = r1
            goto L_0x00aa
        L_0x0130:
            r0 = move-exception
            goto L_0x00b6
        L_0x0132:
            r0 = move-exception
            goto L_0x00b4
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.p.b(android.content.Context, java.lang.String, java.lang.String):d");
    }
}
