package com.soft.android.appinstaller;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

public class SimpleEula {
    /* access modifiers changed from: private */
    public Activity mActivity;

    public SimpleEula(Activity context) {
        this.mActivity = context;
    }

    public void show() {
        GlobalConfig.getInstance().init(this.mActivity);
        String title = GlobalConfig.getInstance().getRulesTexts().getTitle();
        new AlertDialog.Builder(this.mActivity).setTitle(title).setMessage(GlobalConfig.getInstance().getRulesTexts().getText()).setPositiveButton(17039370, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                QuestionActivity.checkNextQuestions(SimpleEula.this.mActivity.getBaseContext(), SimpleEula.this.mActivity);
                SimpleEula.this.mActivity.finish();
            }
        }).setNegativeButton(17039360, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                SimpleEula.this.mActivity.finish();
            }
        }).create().show();
    }
}
