package com.netqin.antivirus.filemanager;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.netqin.antivirus.scan.ScanActivity;
import com.netqin.antivirus.ui.BaseListActivity;
import com.nqmobile.antivirus_ampro20.R;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashSet;

public class FileManagerActivity extends BaseListActivity {
    private static final String ACTION_VIEW_DIR = "com.netqin.mobileguard.filemanager.action.VIEW_DIR";
    private static final String DEFAULT_TOP_DIR = Environment.getExternalStorageDirectory().getAbsolutePath();
    private static final String EXT_TARGET_DIR = "/";
    public static LinkedHashSet<File> filesScanList = new LinkedHashSet<>();
    public static boolean isPackageChecked = false;
    CustomScanerManager customManager;
    /* access modifiers changed from: private */
    public File folder;
    Button mBtnScan = null;
    ImageButton mBtnUp = null;
    FileSystemNode mHighlightNode = null;
    ImageView mIvIcon = null;
    FileSystemNode mNode = new FileSystemNode(DEFAULT_TOP_DIR);
    LinearLayout mNotifyPanel = null;
    LinearLayout mOptionPanel = null;
    TextView mPathInfo = null;
    boolean mRmAfterCopy = false;
    TextView mTvMsg = null;

    public static void startFileManager(Context context) {
        Intent intent = new Intent(context, FileManagerActivity.class);
        context.startActivity(intent);
        intent.setFlags(337641472);
    }

    public static void startFileManager(Context context, String targetDir) {
        Intent intent = getLaunchIntent(context);
        intent.setAction(ACTION_VIEW_DIR);
        intent.putExtra(EXT_TARGET_DIR, targetDir);
        intent.setFlags(337641472);
        context.startActivity(intent);
    }

    public static Intent getLaunchIntent(Context context) {
        return new Intent(context, FileManagerActivity.class);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.fmanager_main);
        setRequestedOrientation(1);
        ((TextView) findViewById(R.id.activity_name)).setText((int) R.string.virus_main_scan_custom);
        this.customManager = new CustomScanerManager();
        this.mBtnUp = (ImageButton) findViewById(R.id.btn_up);
        this.mBtnUp.setEnabled(false);
        this.mBtnUp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                File localFile = FileManagerActivity.this.folder.getParentFile();
                if (localFile != null) {
                    FileManagerActivity.this.UpdateList(localFile);
                }
            }
        });
        this.mBtnScan = (Button) findViewById(R.id.btn_scan);
        this.mBtnScan.setTextColor(-7829368);
        this.mBtnScan.setEnabled(false);
        this.mBtnScan.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (FileManagerActivity.this.customManager.getScanList().size() >= 0) {
                    FileManagerActivity.filesScanList.clear();
                    FileManagerActivity.filesScanList = (LinkedHashSet) FileManagerActivity.this.customManager.getScanList().clone();
                    Intent i = new Intent(FileManagerActivity.this, ScanActivity.class);
                    i.putExtra("type", 3);
                    FileManagerActivity.this.startActivity(i);
                    FileManagerActivity.this.finish();
                }
            }
        });
        this.mPathInfo = (TextView) findViewById(R.id.et_path);
        UpdateList(new File(DEFAULT_TOP_DIR));
    }

    /* access modifiers changed from: private */
    public void UpdateList(File paramFile) {
        File localFile1 = paramFile.getParentFile();
        if (paramFile.getParentFile() == null || localFile1.getAbsolutePath().equals(EXT_TARGET_DIR)) {
            this.mBtnUp.setEnabled(false);
        } else {
            this.mBtnUp.setEnabled(true);
        }
        this.folder = paramFile;
        this.customManager.setCurentDir(paramFile);
        setListAdapter(new EfficientAdapter(this, this.folder));
        this.mPathInfo.setText(this.folder.getAbsolutePath());
    }

    private void updateNotifyPanel() {
        if (this.mNode.getChildren().size() > 0) {
            getListView().setVisibility(0);
            this.mNotifyPanel.setVisibility(8);
            return;
        }
        this.mNotifyPanel.setVisibility(0);
        this.mTvMsg = (TextView) findViewById(R.id.tv_msg);
        if (isTopNode()) {
            this.mOptionPanel.setVisibility(8);
            this.mTvMsg.setText((int) R.string.no_sdcard);
            return;
        }
        this.mOptionPanel.setVisibility(0);
        this.mTvMsg.setText((int) R.string.empty_dir);
    }

    public FileListAdapter createFileListAdapter(String path) {
        return new FileListAdapter(this, new FileSystemNode(path).getChildren());
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        File node = (File) getListView().getAdapter().getItem(position);
        if (node.isDirectory()) {
            UpdateList(node);
        }
    }

    public void onClickDirectory(FileSystemNode node) {
        selectDirNode(node);
    }

    public void onClickFile(FileSystemNode node) {
    }

    private void selectDirNode(FileSystemNode targetDir) {
        this.mNode = targetDir;
        getListView().setAdapter((ListAdapter) createFileListAdapter(targetDir.getAbsolutePath()));
        this.mPathInfo.setText(this.mNode.getAbsolutePath());
        updateNotifyPanel();
    }

    private boolean isTopNode() {
        return DEFAULT_TOP_DIR.equals(this.mNode.getAbsolutePath());
    }

    class EfficientAdapter extends BaseAdapter implements Comparator<File> {
        ArrayList<File> fileList = new ArrayList<>();
        private LayoutInflater mInflater;

        public EfficientAdapter(Context context, File file) {
            this.mInflater = LayoutInflater.from(context);
            File[] arrayOfFile = file.listFiles();
            if (arrayOfFile != null) {
                for (File f : arrayOfFile) {
                    if (f.isDirectory()) {
                        try {
                            if (f.getCanonicalPath().equals(f.getAbsolutePath())) {
                                this.fileList.add(f);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {
                        this.fileList.add(f);
                    }
                }
                Collections.sort(this.fileList, this);
            }
        }

        public int compare(File o1, File o2) {
            if (o1.isDirectory() && !o2.isDirectory()) {
                return -1;
            }
            if (o1.isDirectory() || !o2.isDirectory()) {
                return o1.getName().compareTo(o2.getName());
            }
            return 1;
        }

        public int getCount() {
            return this.fileList.size();
        }

        public Object getItem(int position) {
            return this.fileList.get(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder _H;
            if (convertView == null) {
                _H = new ViewHolder();
                convertView = this.mInflater.inflate((int) R.layout.fmanager_list_items, (ViewGroup) null);
                _H.body = (TextView) convertView.findViewById(R.id.tv_name);
                _H.icon = (ImageView) convertView.findViewById(R.id.iv_icon);
                _H.chbox = (CheckBox) convertView.findViewById(R.id.tv_checkbox);
                convertView.setTag(_H);
            } else {
                _H = (ViewHolder) convertView.getTag();
            }
            File localFile = this.fileList.get(position);
            _H.chbox.setOnCheckedChangeListener(new ItemCheckedChangeListener(localFile));
            _H.chbox.setChecked(FileManagerActivity.this.customManager.isSel(localFile));
            _H.body.setText(localFile.getName());
            if (!localFile.isDirectory()) {
                _H.icon.setBackgroundResource(R.drawable.mime_type_default);
            } else {
                _H.icon.setBackgroundResource(R.drawable.folder);
            }
            return convertView;
        }

        class ViewHolder {
            TextView body;
            CheckBox chbox;
            ImageView icon;

            ViewHolder() {
            }
        }
    }

    class ItemCheckedChangeListener implements CompoundButton.OnCheckedChangeListener {
        File file;

        ItemCheckedChangeListener(File arg2) {
            this.file = arg2;
        }

        public void onCheckedChanged(CompoundButton paramCompoundButton, boolean Boolean) {
            if (Boolean) {
                FileManagerActivity.this.customManager.add(this.file);
            } else {
                FileManagerActivity.this.customManager.remove(this.file);
            }
            Button btn_scan = (Button) FileManagerActivity.this.findViewById(R.id.btn_scan);
            if (FileManagerActivity.this.customManager.getScanList().size() <= 0) {
                btn_scan.setTextColor(-7829368);
                btn_scan.setEnabled(false);
                return;
            }
            btn_scan.setTextColor(-1);
            btn_scan.setEnabled(true);
        }
    }
}
