package com.netqin.antivirus.filemanager;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.ContextMenu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.netqin.antivirus.ui.BaseListActivity;
import com.nqmobile.antivirus_ampro20.R;

public class FileManager extends BaseListActivity {
    private static final String ACTION_VIEW_DIR = "com.netqin.mobileguard.filemanager.action.VIEW_DIR";
    private static final String DEFAULT_TOP_DIR = Environment.getExternalStorageDirectory().getAbsolutePath();
    private static final String EXT_TARGET_DIR = "/";
    Button mBtnScan = null;
    ImageButton mBtnUp = null;
    FileSystemNode mHighlightNode = null;
    ImageView mIvIcon = null;
    FileSystemNode mNode = new FileSystemNode(DEFAULT_TOP_DIR);
    LinearLayout mNotifyPanel = null;
    LinearLayout mOptionPanel = null;
    TextView mPathInfo = null;
    boolean mRmAfterCopy = false;
    TextView mTvMsg = null;

    public static void startFileManager(Context context) {
        Intent intent = new Intent(context, FileManager.class);
        context.startActivity(intent);
        intent.setFlags(337641472);
    }

    public static void startFileManager(Context context, String targetDir) {
        Intent intent = getLaunchIntent(context);
        intent.setAction(ACTION_VIEW_DIR);
        intent.putExtra(EXT_TARGET_DIR, targetDir);
        intent.setFlags(337641472);
        context.startActivity(intent);
    }

    public static Intent getLaunchIntent(Context context) {
        return new Intent(context, FileManager.class);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.fmanager_main);
        getListView().setAdapter((ListAdapter) createFileListAdapter(this.mNode.getAbsolutePath()));
        registerForContextMenu(getListView());
        this.mNotifyPanel = (LinearLayout) findViewById(R.id.notify_panel);
        this.mOptionPanel = (LinearLayout) findViewById(R.id.option_panel);
        this.mBtnScan = (Button) findViewById(R.id.btn_scan);
        this.mBtnScan.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            }
        });
        this.mBtnUp = (ImageButton) findViewById(R.id.btn_up);
        this.mBtnUp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!FileManager.this.isTopNode()) {
                    FileManager.this.selectDirNode(FileManager.this.mNode.getParent());
                }
            }
        });
        this.mPathInfo = (TextView) findViewById(R.id.et_path);
    }

    private void updateNotifyPanel() {
        if (this.mNode.getChildren().size() > 0) {
            getListView().setVisibility(0);
            this.mNotifyPanel.setVisibility(8);
            return;
        }
        this.mNotifyPanel.setVisibility(0);
        this.mTvMsg = (TextView) findViewById(R.id.tv_msg);
        if (isTopNode()) {
            this.mOptionPanel.setVisibility(8);
            this.mTvMsg.setText((int) R.string.no_sdcard);
            return;
        }
        this.mOptionPanel.setVisibility(0);
        this.mTvMsg.setText((int) R.string.empty_dir);
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
    }

    private FileListAdapter getFileListAdapter() {
        return (FileListAdapter) getListView().getAdapter();
    }

    private FileSystemNode getHighlightNode() {
        return this.mHighlightNode;
    }

    private static String normalizeFileName(String name) {
        return name.replaceAll("[\r\n\t/]", "");
    }

    public FileListAdapter createFileListAdapter(String path) {
        return new FileListAdapter(this, new FileSystemNode(path).getChildren());
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        FileSystemNode node = (FileSystemNode) getListView().getAdapter().getItem(position);
        if (node.isDirectory()) {
            onClickDirectory(node);
        } else {
            onClickFile(node);
        }
    }

    public void onClickDirectory(FileSystemNode node) {
        selectDirNode(node);
    }

    public void onClickFile(FileSystemNode node) {
    }

    /* access modifiers changed from: private */
    public void selectDirNode(FileSystemNode targetDir) {
        this.mNode = targetDir;
        getListView().setAdapter((ListAdapter) createFileListAdapter(targetDir.getAbsolutePath()));
        this.mPathInfo.setText(this.mNode.getAbsolutePath());
        updateNotifyPanel();
    }

    /* access modifiers changed from: private */
    public boolean isTopNode() {
        return DEFAULT_TOP_DIR.equals(this.mNode.getAbsolutePath());
    }
}
