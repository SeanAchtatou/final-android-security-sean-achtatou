package com.netqin.antivirus.filemanager;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import com.netqin.antivirus.util.MimeInfo;
import com.nqmobile.antivirus_ampro20.R;
import java.io.File;
import java.util.ArrayList;

public class FileListAdapter extends BaseAdapter {
    Context mContext = null;
    LayoutInflater mInflater = null;
    ArrayList<FileSystemNode> mNodes = null;

    public FileListAdapter(Context context, ArrayList<FileSystemNode> nodes) {
        this.mInflater = LayoutInflater.from(context);
        this.mNodes = nodes;
        this.mContext = context;
    }

    public boolean remove(FileSystemNode node, boolean notifyChange) {
        if (node == null || !node.delete()) {
            return false;
        }
        this.mNodes.remove(node);
        if (notifyChange) {
            notifyDataSetChanged();
        }
        return true;
    }

    public void addItem(FileSystemNode node) {
        this.mNodes.add(node);
        notifyDataSetChanged();
    }

    public int getCount() {
        return this.mNodes.size();
    }

    public Object getItem(int position) {
        return this.mNodes.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder _H;
        if (convertView == null) {
            _H = new ViewHolder();
            convertView = this.mInflater.inflate((int) R.layout.fmanager_list_items, (ViewGroup) null);
            _H.tvName = (TextView) convertView.findViewById(R.id.tv_name);
            _H.tvCheckBox = (CheckBox) convertView.findViewById(R.id.tv_checkbox);
            _H.ivIcon = (ImageView) convertView.findViewById(R.id.iv_icon);
            convertView.setTag(_H);
        } else {
            _H = (ViewHolder) convertView.getTag();
        }
        FileSystemNode node = this.mNodes.get(position);
        MimeInfo mi = node.getMimeInfo();
        _H.tvName.setText(node.getName());
        _H.tvCheckBox.setChecked(false);
        _H.tvCheckBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (_H.tvCheckBox.isChecked()) {
                    _H.tvCheckBox.setChecked(true);
                } else {
                    _H.tvCheckBox.setChecked(false);
                }
            }
        });
        _H.ivIcon.setImageDrawable(this.mContext.getResources().getDrawable(mi.getIconId()));
        return convertView;
    }

    static class ViewHolder {
        ImageView ivIcon;
        CheckBox tvCheckBox;
        TextView tvName;

        ViewHolder() {
        }
    }

    public boolean rename(FileSystemNode node, String text) {
        int i = this.mNodes.indexOf(node);
        if (i <= 0) {
            return false;
        }
        File target = new File(String.valueOf(node.getParent().getAbsolutePath()) + "/" + text);
        boolean success = node.rename(target);
        this.mNodes.set(i, new FileSystemNode(target));
        notifyDataSetChanged();
        return success;
    }
}
