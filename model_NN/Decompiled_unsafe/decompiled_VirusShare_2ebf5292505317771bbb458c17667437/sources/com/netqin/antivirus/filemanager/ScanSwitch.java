package com.netqin.antivirus.filemanager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.netqin.antivirus.scan.ScanActivity;
import com.nqmobile.antivirus_ampro20.R;
import java.io.File;
import java.util.Iterator;
import java.util.LinkedHashSet;

public class ScanSwitch extends Activity implements AdapterView.OnItemClickListener {
    public static final String DEFAULT_TOP_DIR = Environment.getExternalStorageDirectory().getAbsolutePath();
    public static LinkedHashSet<File> filesScanList;
    public static boolean isPackageChecked = false;
    /* access modifiers changed from: private */
    public Button btnScan;
    private final int[] listIcon = {R.drawable.icon, R.drawable.icon};
    /* access modifiers changed from: private */
    public final int[] listTitle = {R.string.scan_sdcard, R.string.scan_install_apk};
    private AVScanListViewAdapter mAVScanLVAdapter;

    private class ListViewHolder {
        CheckBox check;
        ImageView icon;
        TextView title;

        private ListViewHolder() {
        }

        /* synthetic */ ListViewHolder(ScanSwitch scanSwitch, ListViewHolder listViewHolder) {
            this();
        }
    }

    public static Intent getLaunchIntent(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, ScanSwitch.class);
        return intent;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.scan_switch);
        setRequestedOrientation(1);
        ((TextView) findViewById(R.id.activity_name)).setText((int) R.string.virus_main_scan_custom);
        this.btnScan = (Button) findViewById(R.id.custom_scan);
        this.btnScan.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (ScanSwitch.filesScanList != null && ScanSwitch.filesScanList.size() > 0) {
                    Iterator<File> iterator = ScanSwitch.filesScanList.iterator();
                    while (iterator.hasNext()) {
                        File next = iterator.next();
                    }
                }
                if (ScanSwitch.isPackageChecked || (ScanSwitch.filesScanList != null && ScanSwitch.filesScanList.size() > 0)) {
                    Intent i = new Intent(ScanSwitch.this, ScanActivity.class);
                    i.putExtra("type", 3);
                    ScanSwitch.this.startActivity(i);
                }
                ScanSwitch.this.finish();
            }
        });
        filesScanList = new LinkedHashSet<>();
        this.mAVScanLVAdapter = new AVScanListViewAdapter(this);
        ListView listview = (ListView) findViewById(R.id.antivirus_scan_guide_listview);
        listview.setAdapter((ListAdapter) this.mAVScanLVAdapter);
        listview.setOnItemClickListener(this);
    }

    public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
        boolean z;
        CheckBox checkBox = (CheckBox) ((RelativeLayout) v).findViewById(R.id.antivirus_scan_guide_item_check);
        switch (position) {
            case 0:
                startActivity(FileManagerActivity.getLaunchIntent(this));
                return;
            case 1:
                if (checkBox.isChecked()) {
                    z = false;
                } else {
                    z = true;
                }
                checkBox.setChecked(z);
                isPackageChecked = checkBox.isChecked();
                if (isPackageChecked) {
                    this.btnScan.setTextColor(-1);
                    this.btnScan.setEnabled(true);
                    return;
                } else if (filesScanList == null || filesScanList.size() <= 0) {
                    this.btnScan.setTextColor(-7829368);
                    this.btnScan.setEnabled(false);
                    return;
                } else {
                    this.btnScan.setTextColor(-1);
                    this.btnScan.setEnabled(true);
                    return;
                }
            default:
                return;
        }
    }

    private class AVScanListViewAdapter extends BaseAdapter {
        private Context mContext;
        private LayoutInflater mInflater = LayoutInflater.from(this.mContext);

        public AVScanListViewAdapter(Context context) {
            this.mContext = context;
        }

        public int getCount() {
            return ScanSwitch.this.listTitle.length;
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            final ListViewHolder holder;
            if (convertView == null) {
                convertView = this.mInflater.inflate((int) R.layout.scan_switch_item, (ViewGroup) null);
                holder = new ListViewHolder(ScanSwitch.this, null);
                holder.title = (TextView) convertView.findViewById(R.id.antivirus_scan_guide_item_title);
                holder.check = (CheckBox) convertView.findViewById(R.id.antivirus_scan_guide_item_check);
                convertView.setTag(holder);
            } else {
                holder = (ListViewHolder) convertView.getTag();
            }
            holder.title.setText(ScanSwitch.this.listTitle[position]);
            if (holder.check.isChecked()) {
                if (ScanSwitch.filesScanList == null) {
                    ScanSwitch.filesScanList = new LinkedHashSet<>();
                }
                ScanSwitch.filesScanList.add(new File(ScanSwitch.DEFAULT_TOP_DIR));
            }
            switch (position) {
                case 0:
                    holder.check.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            if (ScanSwitch.filesScanList == null) {
                                ScanSwitch.filesScanList = new LinkedHashSet<>();
                            }
                            if (holder.check.isChecked()) {
                                ScanSwitch.filesScanList.clear();
                                ScanSwitch.filesScanList.add(new File(ScanSwitch.DEFAULT_TOP_DIR));
                                ScanSwitch.this.btnScan.setTextColor(-1);
                                ScanSwitch.this.btnScan.setEnabled(true);
                                return;
                            }
                            ScanSwitch.filesScanList.clear();
                            if (ScanSwitch.isPackageChecked) {
                                ScanSwitch.this.btnScan.setTextColor(-1);
                                ScanSwitch.this.btnScan.setEnabled(true);
                                return;
                            }
                            ScanSwitch.this.btnScan.setTextColor(-7829368);
                            ScanSwitch.this.btnScan.setEnabled(false);
                        }
                    });
                    break;
                case 1:
                    holder.check.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            if (holder.check.isChecked()) {
                                ScanSwitch.isPackageChecked = true;
                                ScanSwitch.this.btnScan.setTextColor(-1);
                                ScanSwitch.this.btnScan.setEnabled(true);
                                return;
                            }
                            ScanSwitch.isPackageChecked = false;
                            if (ScanSwitch.filesScanList == null || ScanSwitch.filesScanList.size() <= 0) {
                                ScanSwitch.this.btnScan.setTextColor(-7829368);
                                ScanSwitch.this.btnScan.setEnabled(false);
                                return;
                            }
                            ScanSwitch.this.btnScan.setTextColor(-1);
                            ScanSwitch.this.btnScan.setEnabled(true);
                        }
                    });
                    break;
            }
            return convertView;
        }
    }
}
