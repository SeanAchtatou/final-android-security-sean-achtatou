package com.netqin.antivirus.filemanager;

import java.io.File;
import java.util.LinkedHashSet;

public class CustomScanerManager {
    LinkedHashSet filesScan = new LinkedHashSet();
    LinkedHashSet filesSelect = new LinkedHashSet();

    public void add(File mFile) {
        if (!this.filesSelect.contains(mFile)) {
            this.filesSelect.add(mFile);
            this.filesScan.add(mFile);
        }
    }

    public LinkedHashSet getScanList() {
        return this.filesScan;
    }

    public boolean isSel(File mFile) {
        return this.filesSelect.contains(mFile);
    }

    public void remove(File mFile) {
        this.filesSelect.remove(mFile);
        this.filesScan.remove(mFile);
        File[] arrayOfFile = mFile.listFiles();
        if (arrayOfFile != null) {
            for (File f : arrayOfFile) {
                if (this.filesSelect.contains(f)) {
                    remove(f);
                }
            }
        }
        removeSelParent(mFile);
    }

    /* access modifiers changed from: package-private */
    public void removeSelParent(File mFile) {
        File[] arrayOfFile;
        File parentFile = mFile.getParentFile();
        if (parentFile != null && (arrayOfFile = parentFile.listFiles()) != null) {
            int length = arrayOfFile.length;
            int i = 0;
            while (i < length) {
                if (!this.filesSelect.contains(arrayOfFile[i])) {
                    i++;
                } else {
                    return;
                }
            }
            this.filesSelect.remove(parentFile);
        }
    }

    public void setCurentDir(File mFile) {
        File[] arrayOfFile = mFile.listFiles();
        if (arrayOfFile != null && this.filesScan.contains(mFile)) {
            this.filesScan.remove(mFile);
            for (File f : arrayOfFile) {
                add(f);
            }
        }
    }
}
