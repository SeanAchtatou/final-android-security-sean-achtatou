package com.netqin.antivirus.filemanager;

import SHcMjZX.DD02zqNU;
import com.netqin.antivirus.util.MimeInfo;
import com.netqin.antivirus.util.MimeUtils;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class FileSystemNode {
    private static final Comparator<FileSystemNode> sDefaultComparator = new Comparator<FileSystemNode>() {
        public int compare(FileSystemNode o1, FileSystemNode o2) {
            if (o1.isDirectory() && !o2.isDirectory()) {
                return -1;
            }
            if (o1.isDirectory() || !o2.isDirectory()) {
                return o1.getName().compareTo(o2.getName());
            }
            return 1;
        }
    };
    File mFile = null;
    MimeInfo mMimeInfo = null;

    public File asFile() {
        return this.mFile.getAbsoluteFile();
    }

    public FileSystemNode(String path) {
        this.mFile = new File(path);
        this.mMimeInfo = MimeUtils.getMimeInfo(this.mFile);
    }

    public FileSystemNode(File file) {
        this.mFile = file;
        this.mMimeInfo = MimeUtils.getMimeInfo(this.mFile);
    }

    public MimeInfo getMimeInfo() {
        return this.mMimeInfo;
    }

    public boolean isDirectory() {
        return this.mFile.isDirectory();
    }

    public String getName() {
        return this.mFile.getName();
    }

    public long getSize() {
        return DD02zqNU.Zob2JfG1Yi11hrq(this.mFile);
    }

    public ArrayList<FileSystemNode> _getChildren() {
        ArrayList<FileSystemNode> nodes = new ArrayList<>();
        File[] files = this.mFile.listFiles();
        if (files != null) {
            for (File f : files) {
                if (onFilterNode(f)) {
                    nodes.add(new FileSystemNode(f));
                }
            }
        }
        return nodes;
    }

    private boolean onFilterNode(File file) {
        return !file.isHidden();
    }

    public boolean isHidden() {
        return this.mFile.isHidden();
    }

    public ArrayList<FileSystemNode> getChildren() {
        ArrayList<FileSystemNode> chd = _getChildren();
        Collections.sort(chd, sDefaultComparator);
        return chd;
    }

    public String getAbsolutePath() {
        return this.mFile.getAbsolutePath();
    }

    public FileSystemNode getParent() {
        return new FileSystemNode(new File(this.mFile.getParent()));
    }

    public boolean rename(File target) {
        if (!this.mFile.renameTo(target)) {
            return false;
        }
        this.mFile = target;
        return true;
    }

    public boolean delete() {
        if (isDirectory()) {
            return deleteDir(this.mFile);
        }
        return this.mFile.delete();
    }

    public boolean existed() {
        return this.mFile.exists();
    }

    public boolean clearFile() {
        if (this.mFile.isFile()) {
            return false;
        }
        File[] files = this.mFile.listFiles();
        if (files == null) {
            return true;
        }
        boolean success = true;
        for (int i = 0; i < files.length; i++) {
            if (files[i].exists() && !files[i].delete()) {
                success = false;
            }
        }
        return success;
    }

    public FileSystemNode mkdir(String name) {
        File target = new File(this.mFile.getAbsoluteFile() + "/" + name);
        if (target.exists() || !target.mkdir()) {
            return null;
        }
        return new FileSystemNode(target);
    }

    public String toString() {
        return String.format("(:PATH %s :MIME-TYPE: %s :IS-DIR %b)", this.mFile.getAbsolutePath(), getMimeInfo(), Boolean.valueOf(isDirectory()));
    }

    public static boolean clearDir(File targetDir) {
        boolean result = true;
        for (File f : targetDir.listFiles()) {
            if (f.exists() && !f.delete()) {
                result = false;
            }
        }
        return result;
    }

    public static void copyDirectory(File source, File target) throws IOException {
        if (source.isDirectory()) {
            if (!target.exists()) {
                target.mkdir();
            }
            String[] children = source.list();
            for (int i = 0; i < children.length; i++) {
                copyDirectory(new File(source, children[i]), new File(target, children[i]));
            }
            return;
        }
        copyFile(source, target);
    }

    public static int copyNode(FileSystemNode source, FileSystemNode target) throws IOException {
        File s = new File(source.getAbsolutePath());
        if (s.isDirectory()) {
            copyDirectory(s, new File(String.valueOf(target.getAbsolutePath()) + "/" + source.getName()));
            return 0;
        } else if (!s.isFile()) {
            return 0;
        } else {
            copyFile(s, new File(new File(target.getAbsolutePath()).getAbsoluteFile() + "/" + s.getName()));
            return 0;
        }
    }

    public static void copyFile(File source, File target) throws IOException {
    }

    public static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (String file : children) {
                if (!deleteDir(new File(dir, file))) {
                    return false;
                }
            }
        }
        return dir.delete();
    }
}
