package com.netqin.antivirus;

public class SHA1Util {
    private static final String b64pad = "=";
    private static final int chrsz = 8;
    private static final boolean hexcase = false;

    public static String hex_sha1(String s) {
        if (s == null) {
            s = "";
        }
        return binb2hex(core_sha1(str2binb(s), s.length() * 8));
    }

    public static String b64_hmac_sha1(String key, String data) {
        return binb2b64(core_hmac_sha1(key, data));
    }

    public static String b64_sha1(String s) {
        if (s == null) {
            s = "";
        }
        return binb2b64(core_sha1(str2binb(s), s.length() * 8));
    }

    private static String binb2b64(int[] binarray) {
        String str = "";
        int[] binarray2 = strechbinarray(binarray, binarray.length * 4);
        for (int i = 0; i < binarray2.length * 4; i += 3) {
            int triplet = (((binarray2[i >> 2] >> ((3 - (i % 4)) * 8)) & 255) << 16) | (((binarray2[(i + 1) >> 2] >> ((3 - ((i + 1) % 4)) * 8)) & 255) << 8) | ((binarray2[(i + 2) >> 2] >> ((3 - ((i + 2) % 4)) * 8)) & 255);
            for (int j = 0; j < 4; j++) {
                if ((i * 8) + (j * 6) > binarray2.length * 32) {
                    str = String.valueOf(str) + b64pad;
                } else {
                    str = String.valueOf(str) + "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz0123456789+/".charAt((triplet >> ((3 - j) * 6)) & 63);
                }
            }
        }
        return cleanb64str(str);
    }

    private static String binb2hex(int[] binarray) {
        String str = "";
        for (int i = 0; i < binarray.length * 4; i++) {
            str = String.valueOf(str) + new Character("0123456789abcdef".charAt((binarray[i >> 2] >> (((3 - (i % 4)) * 8) + 4)) & 15)).toString() + new Character("0123456789abcdef".charAt((binarray[i >> 2] >> ((3 - (i % 4)) * 8)) & 15)).toString();
        }
        return str;
    }

    private static String binb2str(int[] bin) {
        String str = "";
        for (int i = 0; i < bin.length * 32; i += 8) {
            str = String.valueOf(str) + ((char) ((bin[i >> 5] >>> (24 - (i % 32))) & 255));
        }
        return str;
    }

    private static int bit_rol(int num, int cnt) {
        return (num << cnt) | (num >>> (32 - cnt));
    }

    private static String cleanb64str(String str) {
        if (str == null) {
            str = "";
        }
        int len = str.length();
        if (len <= 1) {
            return str;
        }
        char trailchar = str.charAt(len - 1);
        String trailstr = "";
        int i = len - 1;
        while (i >= 0 && str.charAt(i) == trailchar) {
            trailstr = String.valueOf(trailstr) + str.charAt(i);
            i--;
        }
        return str.substring(0, str.indexOf(trailstr));
    }

    private static int[] complete216(int[] oldbin) {
        if (oldbin.length >= 16) {
            return oldbin;
        }
        int[] newbin = new int[(16 - oldbin.length)];
        for (int i = 0; i < newbin.length; i++) {
            newbin[i] = 0;
        }
        return concat(oldbin, newbin);
    }

    private static int[] concat(int[] oldbin, int[] newbin) {
        int[] retval = new int[(oldbin.length + newbin.length)];
        for (int i = 0; i < oldbin.length + newbin.length; i++) {
            if (i < oldbin.length) {
                retval[i] = oldbin[i];
            } else {
                retval[i] = newbin[i - oldbin.length];
            }
        }
        return retval;
    }

    private static int[] core_hmac_sha1(String key, String data) {
        if (key == null) {
            key = "";
        }
        if (data == null) {
            data = "";
        }
        int[] bkey = complete216(str2binb(key));
        if (bkey.length > 16) {
            bkey = core_sha1(bkey, key.length() * 8);
        }
        int[] ipad = new int[16];
        int[] opad = new int[16];
        for (int i = 0; i < 16; i++) {
            ipad[i] = 0;
            opad[i] = 0;
        }
        for (int i2 = 0; i2 < 16; i2++) {
            ipad[i2] = bkey[i2] ^ 909522486;
            opad[i2] = bkey[i2] ^ 1549556828;
        }
        return core_sha1(concat(opad, core_sha1(concat(ipad, str2binb(data)), (data.length() * 8) + 512)), 672);
    }

    /* JADX INFO: Multiple debug info for r14v4 int[]: [D('x' int[]), D('retval' int[])] */
    private static int[] core_sha1(int[] x, int len) {
        int[] x2 = strechbinarray(x, len >> 5);
        int size = len >> 5;
        x2[size] = x2[size] | (128 << (24 - (len % 32)));
        int[] x3 = strechbinarray(x2, (((len + 64) >> 9) << 4) + 15);
        x3[(((len + 64) >> 9) << 4) + 15] = len;
        int[] w = new int[80];
        int a = 1732584193;
        int b = -271733879;
        int c = -1732584194;
        int d = 271733878;
        int e = -1009589776;
        for (int i = 0; i < x3.length; i += 16) {
            int olda = a;
            int oldb = b;
            int oldc = c;
            int oldd = d;
            int olde = e;
            for (int j = 0; j < 80; j++) {
                if (j < 16) {
                    w[j] = x3[i + j];
                } else {
                    w[j] = rol(((w[j - 3] ^ w[j - 8]) ^ w[j - 14]) ^ w[j - 16], 1);
                }
                int t = safe_add(safe_add(rol(a, 5), sha1_ft(j, b, c, d)), safe_add(safe_add(e, w[j]), sha1_kt(j)));
                e = d;
                d = c;
                c = rol(b, 30);
                b = a;
                a = t;
            }
            a = safe_add(a, olda);
            b = safe_add(b, oldb);
            c = safe_add(c, oldc);
            d = safe_add(d, oldd);
            e = safe_add(e, olde);
        }
        return new int[]{a, b, c, d, e};
    }

    private static void dotest() {
    }

    public static String hex_hmac_sha1(String key, String data) {
        return binb2hex(core_hmac_sha1(key, data));
    }

    private static int rol(int num, int cnt) {
        return (num << cnt) | (num >>> (32 - cnt));
    }

    private static int safe_add(int x, int y) {
        int lsw = (x & 65535) + (y & 65535);
        return ((((x >> 16) + (y >> 16)) + (lsw >> 16)) << 16) | (lsw & 65535);
    }

    private static int sha1_ft(int t, int b, int c, int d) {
        if (t < 20) {
            return (b & c) | ((b ^ -1) & d);
        }
        if (t < 40) {
            return (b ^ c) ^ d;
        }
        if (t < 60) {
            return (b & c) | (b & d) | (c & d);
        }
        return (b ^ c) ^ d;
    }

    private static int sha1_kt(int t) {
        if (t < 20) {
            return 1518500249;
        }
        if (t < 40) {
            return 1859775393;
        }
        return t < 60 ? -1894007588 : -899497514;
    }

    private static boolean sha1_vm_test() {
        return hex_sha1("abc").equals("a9993e364706816aba3e25717850c26c9cd0d89d");
    }

    public static String str_hmac_sha1(String key, String data) {
        return binb2str(core_hmac_sha1(key, data));
    }

    public static String str_sha1(String s) {
        if (s == null) {
            s = "";
        }
        return binb2str(core_sha1(str2binb(s), s.length() * 8));
    }

    private static int[] str2binb(String str) {
        if (str == null) {
            str = "";
        }
        int[] tmp = new int[(str.length() * 8)];
        for (int i = 0; i < str.length() * 8; i += 8) {
            int i2 = i >> 5;
            tmp[i2] = tmp[i2] | ((str.charAt(i / 8) & 255) << (24 - (i % 32)));
        }
        int len = 0;
        int i3 = 0;
        while (i3 < tmp.length && tmp[i3] != 0) {
            i3++;
            len++;
        }
        int[] bin = new int[len];
        for (int i4 = 0; i4 < len; i4++) {
            bin[i4] = tmp[i4];
        }
        return bin;
    }

    private static int[] strechbinarray(int[] oldbin, int size) {
        int currlen = oldbin.length;
        if (currlen >= size + 1) {
            return oldbin;
        }
        int[] newbin = new int[(size + 1)];
        for (int i = 0; i < size; i++) {
            newbin[i] = 0;
        }
        for (int i2 = 0; i2 < currlen; i2++) {
            newbin[i2] = oldbin[i2];
        }
        return newbin;
    }

    public static void main(String[] args) {
    }
}
