package com.netqin.antivirus.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.data.PreferenceDataHelper;
import com.netqin.antivirus.networkmanager.HandlerContainer;
import com.netqin.antivirus.networkmanager.Log;
import com.netqin.antivirus.networkmanager.NetApplication;
import com.netqin.antivirus.networkmanager.SettingPreferences;
import com.netqin.antivirus.networkmanager.model.Counter;
import com.netqin.antivirus.networkmanager.model.Device;
import com.netqin.antivirus.networkmanager.model.Interface;
import com.netqin.antivirus.networkmanager.model.NetMeterModel;
import com.netqin.antivirus.services.NetMeterAlarm;
import com.netqin.antivirus.ui.NetTrafficActivity;
import com.nqmobile.antivirus_ampro20.R;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class NetMeterService extends WakefulService {
    private static final boolean DBG = false;
    /* access modifiers changed from: private */
    public static final String TAG = NetMeterService.class.getSimpleName();
    private NetMeterAlarm mAlarm;
    private NetApplication mApp;
    private final BroadcastReceiver mIntentReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (!TextUtils.equals(action, "android.net.wifi.WIFI_STATE_CHANGED")) {
                if (TextUtils.equals(action, "android.intent.action.SCREEN_OFF")) {
                    NetMeterService.this.mIsScreenOn = false;
                } else if (TextUtils.equals(action, "android.intent.action.SCREEN_ON")) {
                    NetMeterService.this.mIsScreenOn = true;
                }
            }
            NetMeterService.this.registerAlarm();
        }
    };
    /* access modifiers changed from: private */
    public boolean mIsScreenOn = true;
    /* access modifiers changed from: private */
    public NetMeterModel mModel;
    private int mPollingMode = -1;
    private final Runnable mUpdateRunnable = new Runnable() {
        public void run() {
            try {
                Log.d(NetMeterService.TAG, "av: mModel.isLoaded()!!! and mModel.isLoaded()??" + NetMeterService.this.mModel.isLoaded());
                if (NetMeterService.this.mModel.isLoaded()) {
                    NetMeterService.this.checkAdjustDate();
                    NetMeterService.this.updateInterfaceData();
                }
            } finally {
                NetMeterService.this.releaseLocalLock();
            }
        }
    };
    private WifiManager mWifiManager;

    public void onCreate() {
        super.onCreate();
        this.mApp = (NetApplication) getApplication();
        this.mModel = (NetMeterModel) this.mApp.getAdapter(NetMeterModel.class);
        this.mWifiManager = (WifiManager) getSystemService("wifi");
        this.mAlarm = new NetMeterAlarm(this, OnAlarmReceiver.class);
        IntentFilter f = new IntentFilter("android.net.wifi.WIFI_STATE_CHANGED");
        f.addAction("android.intent.action.SCREEN_ON");
        f.addAction("android.intent.action.SCREEN_OFF");
        registerReceiver(this.mIntentReceiver, f);
    }

    /* access modifiers changed from: protected */
    public void checkAdjustDate() {
        if (PreferenceDataHelper.getFlowAdjustValue(this) != 0) {
            String sNow = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
            String startDate = PreferenceDataHelper.getFlowStartDate(this);
            String endDate = PreferenceDataHelper.getFlowEndDate(this);
            if (!startDate.equals("") && !endDate.equals("")) {
                if (sNow.compareTo(startDate) < 0 || sNow.compareTo(endDate) > 0) {
                    PreferenceDataHelper.setFlowAdjustValue(this, 0);
                    PreferenceDataHelper.setFlowStartDate(this, "");
                    PreferenceDataHelper.setFlowEndDate(this, "");
                }
            }
        }
    }

    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        int p = NetApplication.getUpdatePolicy();
        if (this.mPollingMode != p) {
            this.mPollingMode = p;
            registerAlarm();
        }
        Handler handler = ((HandlerContainer) this.mApp.getAdapter(HandlerContainer.class)).getSlowHandler();
        handler.removeCallbacks(this.mUpdateRunnable);
        handler.post(this.mUpdateRunnable);
    }

    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.mIntentReceiver);
        ((HandlerContainer) this.mApp.getAdapter(HandlerContainer.class)).getSlowHandler().post(this.mUpdateRunnable);
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    /* access modifiers changed from: private */
    public void registerAlarm() {
        if (this.mPollingMode == 2) {
            this.mAlarm.registerAlarm(NetMeterAlarm.State.HIGH);
        } else if (this.mWifiManager.getWifiState() == 3) {
            this.mAlarm.registerAlarm(NetMeterAlarm.State.MEDIUM);
        } else if (this.mIsScreenOn) {
            this.mAlarm.registerAlarm(NetMeterAlarm.State.MEDIUM);
        } else {
            this.mAlarm.registerAlarm(NetMeterAlarm.State.LOW);
        }
    }

    /* access modifiers changed from: private */
    public void updateInterfaceData() {
        if (PreferenceManager.getDefaultSharedPreferences(this).getBoolean(SettingPreferences.KEY_METER_ONOFF, true)) {
            String[] interfaces = Device.getDevice().getInterfaces();
            Log.d(TAG, "------------------------====> Updating database <====" + interfaces.length);
            Log.d(TAG, "------------------------====> Updating database <====");
            StringBuilder sbNotify = new StringBuilder();
            for (int i = 0; i < interfaces.length; i++) {
                String inter = interfaces[i];
                Log.d(TAG, "------------------------====> Updating database <====" + inter);
                if (SysClassNet.isUp(inter)) {
                    try {
                        long t1 = System.currentTimeMillis();
                        this.mModel.getInterface(inter).updateBytes(SysClassNet.getRxBytes(inter), SysClassNet.getTxBytes(inter));
                        this.mModel.commit();
                        sbNotify.append(String.valueOf(inter) + " done in " + (System.currentTimeMillis() - t1) + " ms.\n");
                    } catch (IOException e) {
                    }
                }
            }
            Log.d(TAG, sbNotify.toString());
            long t12 = System.currentTimeMillis();
            checkAlert();
            long t2 = System.currentTimeMillis();
            StringBuilder sb = new StringBuilder();
            sb.append("Alert: ").append(t2 - t12).append(" ms");
            Log.d(TAG, sb.toString());
            Log.d("-------------------------", "Notification!!");
            updateNotificationBar();
        }
    }

    private void checkAlert() {
        String s;
        NotificationManager notify = (NotificationManager) this.mApp.getAdapter(NotificationManager.class);
        for (Interface provider : this.mModel.getInterfaces()) {
            for (Counter c : provider.getCounters()) {
                int id = (int) ((provider.getId() << 10) + c.getId());
                String a = c.getProperty(Counter.ALERT_BYTES);
                long alert = 0;
                if (a != null) {
                    alert = Long.valueOf(a).longValue();
                }
                boolean needAlert = PreferenceDataHelper.getNeedTrafficAlert(getApplicationContext());
                if (alert > 0) {
                    long[] bytes = c.getBytes();
                    long total = bytes[0] + bytes[1] + PreferenceDataHelper.getFlowAdjustValue(this);
                    Log.d("hao", "total, and alert:" + total + "---" + alert);
                    if (((double) total) <= ((double) alert) * 0.9d) {
                        PreferenceDataHelper.setNeedTrafficAlert(getApplicationContext(), true);
                        notify.cancel(id);
                    } else if (needAlert && PreferenceDataHelper.enabledAlert(getApplicationContext())) {
                        String inter = provider.getPrettyName();
                        Notification n = new Notification();
                        n.when = System.currentTimeMillis();
                        n.icon = R.drawable.icon_notemsg;
                        n.flags = 16;
                        n.tickerText = getResources().getString(R.string.meter_notify_exceed_title, inter);
                        Intent intent = new Intent("android.intent.action.MAIN");
                        intent.setFlags(335544320);
                        intent.addCategory("android.intent.category.LAUNCHER");
                        intent.setClass(this, NetTrafficActivity.class);
                        intent.setFlags(270532608);
                        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, 134217728);
                        String av = c.getProperty(Counter.ALERT_VALUE);
                        if (av != null) {
                            if (total > alert) {
                                s = getResources().getString(R.string.meter_notify_exceed, inter, av, "MB");
                            } else {
                                s = getResources().getString(R.string.meter_notify_exceed_value, inter, av, "MB");
                            }
                            n.setLatestEventInfo(this, getText(R.string.app_name), s, contentIntent);
                            notify.notify(id, n);
                            PreferenceDataHelper.setNeedTrafficAlert(getApplicationContext(), false);
                        }
                    }
                } else {
                    notify.cancel(id);
                }
            }
        }
    }

    private void updateNotificationBar() {
        long used;
        long used2;
        SharedPreferences preferences = (SharedPreferences) this.mApp.getAdapter(SharedPreferences.class);
        if (preferences.getBoolean(SettingPreferences.KEY_METER_ONOFF, true)) {
            long[] cell = updateMeterTraffic();
            try {
                Double.parseDouble(preferences.getString(SettingPreferences.KEY_METER_THRESHOLD, ""));
            } catch (NumberFormatException e) {
                preferences.edit().putString(SettingPreferences.KEY_METER_THRESHOLD, NetApplication.DEFAULT_THRESHOLD).commit();
            }
            if (cell != null) {
                used = cell[0] + cell[1];
            } else {
                used = 0;
            }
            Log.d("tang", "service used:" + used);
            long a = used + PreferenceDataHelper.getFlowAdjustValue(this);
            if (a > 0) {
                used2 = a;
            } else {
                used2 = 0;
            }
            CommonMethod.showFlowBarNotification(this, used2);
        }
    }

    private long[] updateMeterTraffic() {
        long[] wifi = new long[2];
        long[] cell = new long[2];
        Device device = Device.getDevice();
        for (Interface inter : this.mModel.getInterfaces()) {
            for (Counter counter : inter.getCounters()) {
                if (counter.getType() == 0) {
                    long[] bytes = counter.getBytes();
                    if (device.isCell(inter.getName())) {
                        cell[0] = cell[0] + bytes[0];
                        cell[1] = cell[1] + bytes[1];
                    } else if (device.isWiFi(inter.getName())) {
                        wifi[0] = wifi[0] + bytes[0];
                        wifi[1] = wifi[1] + bytes[1];
                    }
                }
            }
        }
        return cell;
    }
}
