package com.netqin.antivirus.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.os.SystemClock;
import java.util.Calendar;

public class NetMeterAlarm {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$netqin$antivirus$services$NetMeterAlarm$State = null;
    private static final String ACTION_TEST = "com.netqin.antivirus.intent.ACTION_TEST";
    private static final boolean DBG = false;
    private static final long INTER_ACTIVE = 30000;
    private static final long INTER_STANDARD = 60000;
    private static final long INTER_STANDBY = 3600000;
    private static final String TAG = NetMeterAlarm.class.getSimpleName();
    private final AlarmManager mAm;
    private final PendingIntent mAs;
    private State mCurrentState = null;

    enum State {
        LOW,
        MEDIUM,
        HIGH
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$netqin$antivirus$services$NetMeterAlarm$State() {
        int[] iArr = $SWITCH_TABLE$com$netqin$antivirus$services$NetMeterAlarm$State;
        if (iArr == null) {
            iArr = new int[State.values().length];
            try {
                iArr[State.HIGH.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[State.LOW.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[State.MEDIUM.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$netqin$antivirus$services$NetMeterAlarm$State = iArr;
        }
        return iArr;
    }

    public NetMeterAlarm(Service srv, Class<? extends BroadcastReceiver> cls) {
        this.mAm = (AlarmManager) srv.getSystemService("alarm");
        Intent i = new Intent(srv, cls);
        i.setAction(ACTION_TEST);
        this.mAs = PendingIntent.getBroadcast(srv, 0, i, 0);
    }

    public State getCurrentState() {
        return this.mCurrentState;
    }

    public boolean registerAlarm(State state) {
        if (this.mCurrentState == state) {
            return false;
        }
        switch ($SWITCH_TABLE$com$netqin$antivirus$services$NetMeterAlarm$State()[state.ordinal()]) {
            case 1:
                registerStandbyAlarm();
                break;
            case 2:
                registerStandardAlarm();
                break;
            case 3:
                registerActiveAlarm();
                break;
        }
        this.mCurrentState = state;
        return true;
    }

    private void registerActiveAlarm() {
        this.mAm.setRepeating(3, SystemClock.elapsedRealtime() + INTER_ACTIVE, INTER_ACTIVE, this.mAs);
    }

    private void registerStandardAlarm() {
        this.mAm.setRepeating(3, SystemClock.elapsedRealtime() + INTER_STANDARD, INTER_STANDARD, this.mAs);
    }

    private void registerStandbyAlarm() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(12, 59);
        calendar.set(13, 0);
        this.mAm.setRepeating(0, calendar.getTimeInMillis(), INTER_STANDBY, this.mAs);
    }
}
