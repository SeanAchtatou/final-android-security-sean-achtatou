package com.netqin.antivirus.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;

public abstract class WakefulService extends Service {
    public static final String LOCK_NAME_LOCAL = "WakeLock.Local";
    public static final String LOCK_NAME_STATIC = "WakeLock.Static";
    private static PowerManager.WakeLock mLockStatic = null;
    private PowerManager.WakeLock mLockLocal = null;

    public static void acquireStaticLock(Context context) {
        getLock(context).acquire();
    }

    private static synchronized PowerManager.WakeLock getLock(Context context) {
        PowerManager.WakeLock wakeLock;
        synchronized (WakefulService.class) {
            if (mLockStatic == null) {
                mLockStatic = ((PowerManager) context.getSystemService("power")).newWakeLock(1, LOCK_NAME_STATIC);
                mLockStatic.setReferenceCounted(true);
            }
            wakeLock = mLockStatic;
        }
        return wakeLock;
    }

    public void onCreate() {
        super.onCreate();
        this.mLockLocal = ((PowerManager) getSystemService("power")).newWakeLock(1, LOCK_NAME_LOCAL);
        this.mLockLocal.setReferenceCounted(true);
    }

    public void onStart(Intent intent, int startId) {
        this.mLockLocal.acquire();
        super.onStart(intent, startId);
        if (getLock(this).isHeld()) {
            mLockStatic.release();
        }
    }

    /* access modifiers changed from: protected */
    public void releaseLocalLock() {
        if (this.mLockLocal != null && this.mLockLocal.isHeld()) {
            this.mLockLocal.release();
        }
    }
}
