package com.netqin.antivirus.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.netqin.antivirus.common.CommonMethod;

public class OnAlarmReceiver extends BroadcastReceiver {
    private static final String TAG = "OnAlarmReceiver";

    public void onReceive(Context context, Intent intent) {
        if (!CommonMethod.isFirstRun(context) && CommonMethod.getAutoStartTag(context)) {
            WakefulService.acquireStaticLock(context);
            context.startService(new Intent(context, NetMeterService.class));
        }
    }
}
