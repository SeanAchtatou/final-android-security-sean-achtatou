package com.netqin.antivirus.services;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.text.TextUtils;
import com.netqin.antivirus.HomeActivity;
import com.netqin.antivirus.NqUtil;
import com.netqin.antivirus.Value;
import com.netqin.antivirus.appprotocol.AppRequest;
import com.netqin.antivirus.cloud.apkinfo.domain.Rate;
import com.netqin.antivirus.cloud.model.CloudHandler;
import com.netqin.antivirus.cloud.model.xml.XmlUtils;
import com.netqin.antivirus.cloud.view.CloudListener;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.net.appupdateservice.BackgroundAppUpdateService;
import com.netqin.antivirus.packagemanager.ApkInfoActivity;
import com.netqin.antivirus.scan.PeriodScanService;
import com.netqin.antivirus.scan.ScanController;
import com.nqmobile.antivirus_ampro20.R;
import java.util.Calendar;
import java.util.TimerTask;

public class ControlService extends Service implements CloudListener {
    public static boolean mRunning = false;
    /* access modifiers changed from: private */
    public BackgroundAppUpdateService backgroundAppUpdateService;
    /* access modifiers changed from: private */
    public Handler cloudTaskHandler;
    private TimerTask dialyCheckTask = new TimerTask() {
        public void run() {
            String s = NqUtil.getStringByCalendar(Calendar.getInstance());
            String n = CommonMethod.getNextDialyCheckTime(ControlService.this);
            String l = CommonMethod.getLastDialyCheckTime(ControlService.this);
            String fn = CommonMethod.getNextFreedataCheckTime(ControlService.this);
            String fl = CommonMethod.getLastFreedataCheckTime(ControlService.this);
            String pn = CommonMethod.getNextPeriodScanTime(ControlService.this);
            String pl = CommonMethod.getLastPeriodScanTime(ControlService.this);
            String softUpdateNext = CommonMethod.getSoftUpdateNextDialyCheckTime(ControlService.this);
            String softUpdateLast = CommonMethod.getSoftUpdateLastDialyCheckTime(ControlService.this);
            CommonMethod.logDebug("AVService", "CurrentTime " + s);
            CommonMethod.logDebug("AVService", "StartPeriodicalConnectNextTime " + n);
            CommonMethod.logDebug("AVService", "StartPeriodicalConnectLastTime " + l);
            CommonMethod.logDebug("AVService", "StartUpdateFreeDataNextTime " + fn);
            CommonMethod.logDebug("AVService", "StartUpdateFreeDataLastTime " + fl);
            CommonMethod.logDebug("AVService", "StartUpdateSoftwareNextTime " + softUpdateNext);
            CommonMethod.logDebug("AVService", "StartUpdateSoftwareLastTime " + softUpdateLast + "\n");
            if ((s.compareTo(n) >= 0 || s.compareTo(l) < 0) && !CommonMethod.getSoftExitState(ControlService.this)) {
                CommonMethod.logDebug("AVService", "StartPeriodicalConnect");
                AppRequest.StartPeriodicalConnect(ControlService.this, true);
                Rate rate = new Rate();
                rate.setPerformance(XmlUtils.PERFORMANCE_DAT);
                rate.setUrl_performance(XmlUtils.URL_PERFORMANCE_DAT);
                CloudHandler.rate = rate;
                CloudHandler.getInstance(ControlService.this).resetData();
                CloudHandler.getInstance(ControlService.this).start(ControlService.this, 4, new Handler(), true);
            } else if ((s.compareTo(fn) >= 0 || s.compareTo(fl) < 0) && !CommonMethod.getSoftExitState(ControlService.this)) {
                CommonMethod.logDebug("AVService", "StartUpdateFreeData");
                AppRequest.StartUpdateFreeData(ControlService.this, true);
            }
            if ((s.compareTo(softUpdateNext) >= 0 || s.compareTo(softUpdateLast) < 0) && !CommonMethod.getSoftExitState(ControlService.this)) {
                CommonMethod.logDebug("netqin_softupdate", "ControlService(TimerTaskCheck) : Do");
                ControlService.this.updateSoftware();
            }
            if (!TextUtils.isEmpty(pn) && !TextUtils.isEmpty(pl) && ((s.compareTo(pn) >= 0 || s.compareTo(pl) < 0) && !CommonMethod.getSoftExitState(ControlService.this) && !ScanController.isScaning && !CommonMethod.isFirstRun(ControlService.this))) {
                Intent i = new Intent();
                i.setClass(ControlService.this, PeriodScanService.class);
                ControlService.this.startService(i);
            }
            ControlService.this.taskHandler.postDelayed(this, 600000);
            CommonMethod.logDebug("taskHandler", "10 minute");
        }
    };
    private TimerTask firstCloudScanTask = new TimerTask() {
        public void run() {
            if (NqUtil.getStringByCalendar(Calendar.getInstance()).compareTo(CommonMethod.getFirstCloudScanTime(ControlService.this)) >= 0 && !CommonMethod.getSoftExitState(ControlService.this) && !CommonMethod.getCloudScanState(ControlService.this) && ApkInfoActivity.checkNetInfo(ControlService.this)) {
                ApkInfoActivity.isCloud = true;
                CloudHandler.getInstance(ControlService.this).start(ControlService.this, 0, new Handler(), true);
            }
            ControlService.this.cloudTaskHandler.postDelayed(this, 600000);
            CommonMethod.logDebug("cloudTaskHandler", "10 minute");
        }
    };
    private SharedPreferences mPref;
    private Notification notification;
    private NotificationManager notificationManager;
    /* access modifiers changed from: private */
    public Handler taskHandler;

    public void onCreate() {
        this.taskHandler = new Handler();
        this.cloudTaskHandler = new Handler();
        this.mPref = getSharedPreferences("netqin", 0);
        super.onCreate();
        mRunning = true;
        this.taskHandler.postDelayed(this.dialyCheckTask, 1800000);
        this.cloudTaskHandler.postDelayed(this.firstCloudScanTask, 2400000);
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        if (intent != null) {
            intent.getExtras().getBoolean("start");
        }
        if (CommonMethod.getNextPeriodScanTime(this).length() > 0) {
            CommonMethod.showFlowBarOrNot(this, new Intent(this, HomeActivity.class), CommonMethod.getNotificationTitle(this), CommonMethod.getNotificationIconState(this));
        }
    }

    /* access modifiers changed from: private */
    public boolean getIsFirstRun() {
        return this.mPref.getBoolean("IsFirstRun", true);
    }

    /* access modifiers changed from: private */
    public void setIsFirstRun(boolean b) {
        this.mPref.edit().putBoolean("IsFirstRun", b).commit();
    }

    public static void markNeedUpdate(Context cnt, ContentValues content) {
        if (content.containsKey(Value.AppUpdateNecessary)) {
            switch (content.getAsInteger(Value.AppUpdateNecessary).intValue()) {
                case 11:
                    CommonMethod.putConfigWithIntegerValue(cnt, "netqin", "softwareupdatetype", 2);
                    break;
                case 12:
                case 13:
                    break;
                default:
                    CommonMethod.putConfigWithIntegerValue(cnt, "netqin", "softwareupdatetype", 0);
                    break;
            }
        }
        if (content.containsKey(Value.AppUpdateFileLength)) {
            CommonMethod.putConfigWithIntegerValue(cnt, "netqin", "appsize", content.getAsInteger(Value.AppUpdateFileLength).intValue());
        }
        if (!content.containsKey(Value.Display)) {
            return;
        }
        if (content.getAsInteger(Value.Display).intValue() == 2) {
            CommonMethod.putConfigWithIntegerValue(cnt, "netqin", "softwaredisplaytype", 2);
        } else {
            CommonMethod.putConfigWithIntegerValue(cnt, "netqin", "softwaredisplaytype", 1);
        }
    }

    /* access modifiers changed from: private */
    public void updateSoftware() {
        new Thread(new Runnable() {
            public void run() {
                boolean mForceSoftUpdate;
                ContentValues content1 = new ContentValues();
                content1.put("IMEI", CommonMethod.getIMEI(ControlService.this));
                content1.put("IMSI", CommonMethod.getIMSI(ControlService.this));
                ControlService.this.backgroundAppUpdateService = BackgroundAppUpdateService.getInstance(ControlService.this);
                int result1 = ControlService.this.backgroundAppUpdateService.request(content1);
                if (result1 == 7890) {
                    ControlService.markNeedUpdate(ControlService.this, content1);
                }
                if (ControlService.this.getIsFirstRun()) {
                    ControlService.this.setIsFirstRun(false);
                }
                if (content1.containsKey(Value.AppUpdateNecessary)) {
                    try {
                        switch (content1.getAsInteger(Value.AppUpdateNecessary).intValue()) {
                            case 11:
                                CommonMethod.putConfigWithIntegerValue(ControlService.this, "netqin", "softwareupdatetype", 2);
                                break;
                            case 12:
                            case 13:
                                CommonMethod.putConfigWithIntegerValue(ControlService.this, "netqin", "softwareupdatetype", 1);
                                break;
                            default:
                                CommonMethod.putConfigWithIntegerValue(ControlService.this, "netqin", "softwareupdatetype", 0);
                                break;
                        }
                    } catch (Exception e) {
                    }
                }
                if (CommonMethod.getConfigWithIntegerValue(ControlService.this, "netqin", "softwareupdatetype", 0) == 2) {
                    mForceSoftUpdate = true;
                } else {
                    mForceSoftUpdate = false;
                }
                String softUpdateMassage = "";
                if (content1.containsKey("Prompt")) {
                    softUpdateMassage = content1.getAsString("Prompt");
                }
                if (mForceSoftUpdate) {
                    if (!TextUtils.isEmpty(softUpdateMassage)) {
                        CommonMethod.setSoftForceUpdateMassage(ControlService.this, softUpdateMassage);
                    }
                } else if (!TextUtils.isEmpty(softUpdateMassage)) {
                    CommonMethod.setSoftUpdateMassage(ControlService.this, softUpdateMassage);
                }
                if (content1.containsKey("NextConnectTime")) {
                    try {
                        CommonMethod.putConfigWithIntegerValue(ControlService.this, "netqin", "updatenextConnectTime", Integer.valueOf(Integer.parseInt(content1.getAsString("NextConnectTime"))).intValue());
                        int minute = CommonMethod.getConfigWithIntegerValue(ControlService.this, "netqin", "updatenextConnectTime", 4320);
                        Calendar calendar = Calendar.getInstance();
                        calendar.add(12, minute);
                        CommonMethod.setSoftUpdateNextDialyCheckTime(ControlService.this, calendar);
                        CommonMethod.setSoftUpdateLastDialyCheckTimeToCurrentTime(ControlService.this);
                    } catch (Exception e2) {
                        Calendar calendar2 = Calendar.getInstance();
                        calendar2.add(6, 7);
                        CommonMethod.setSoftUpdateNextDialyCheckTime(ControlService.this, calendar2);
                        CommonMethod.setSoftUpdateLastDialyCheckTimeToCurrentTime(ControlService.this);
                    }
                } else {
                    Calendar calendar3 = Calendar.getInstance();
                    calendar3.add(6, 1);
                    if (result1 == 8) {
                        calendar3.add(6, 6);
                    }
                    CommonMethod.setSoftUpdateNextDialyCheckTime(ControlService.this, calendar3);
                    CommonMethod.setSoftUpdateLastDialyCheckTimeToCurrentTime(ControlService.this);
                }
                ControlService.this.checkUpdateSoftwareNotify();
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public void checkUpdateSoftwareNotify() {
        String softUpdateMsg;
        Integer displayType = Integer.valueOf(CommonMethod.getConfigWithIntegerValue(this, "netqin", "softwaredisplaytype", 0));
        int type = CommonMethod.getConfigWithIntegerValue(this, "netqin", "softwareupdatetype", 0);
        if (displayType.intValue() == 1) {
            return;
        }
        if (displayType.intValue() == 2 && (type == 2 || type == 1)) {
            String softUpdateMsg2 = "";
            String softUpdateDisplay = "";
            if (type == 2) {
                if (CommonMethod.getSoftForceUpdateMassage(this).length() > 0) {
                    softUpdateMsg2 = CommonMethod.getSoftForceUpdateMassage(this);
                } else {
                    softUpdateMsg2 = getString(R.string.text_force_softupdate, new Object[]{HomeActivity.getUpdateSoftFileSize(this)});
                }
                softUpdateDisplay = getString(R.string.notification_desc_viewnow);
            }
            if (type == 1) {
                if (CommonMethod.getSoftUpdateMassage(this).length() > 0) {
                    softUpdateMsg = CommonMethod.getSoftUpdateMassage(this);
                } else {
                    softUpdateMsg = getString(R.string.text_version_may_update, new Object[]{HomeActivity.getUpdateSoftFileSize(this)});
                }
                softUpdateDisplay = getString(R.string.notification_desc_viewnow);
            }
            for (ActivityManager.RunningAppProcessInfo info : ((ActivityManager) getSystemService("activity")).getRunningAppProcesses()) {
                if (getPackageName().equals(info.processName)) {
                    if (info.importance != 100) {
                        this.notificationManager = (NotificationManager) getSystemService("notification");
                        this.notification = NotificationUtil.build(this, getString(R.string.text_sina_softupdate), softUpdateMsg2, softUpdateDisplay);
                        this.notificationManager.notify(111, this.notification);
                    } else {
                        NotificationUtil.closeNotification(this.notificationManager);
                    }
                }
            }
            return;
        }
        NotificationUtil.closeNotification(this.notificationManager);
    }

    public void complete(int resultCode) {
    }

    public void error(int errorCode) {
    }

    public void process(Bundle bundle) {
    }
}
