package com.netqin.antivirus.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import com.netqin.antivirus.AntiVirusSplash;
import com.nqmobile.antivirus_ampro20.R;

public class NotificationUtil {
    public static final int ID = 111;

    public static Notification build(Context mContext, String title, String info, String display) {
        Notification notification = new Notification(R.drawable.icon_notemsg, title, System.currentTimeMillis());
        Intent intent = new Intent(mContext, AntiVirusSplash.class);
        intent.putExtra("info", info);
        intent.setFlags(335544320);
        notification.setLatestEventInfo(mContext, title, display, PendingIntent.getActivity(mContext, 0, intent, 0));
        return notification;
    }

    public static void closeNotification(NotificationManager notificationManager) {
        if (notificationManager != null) {
            notificationManager.cancel(111);
        }
    }
}
