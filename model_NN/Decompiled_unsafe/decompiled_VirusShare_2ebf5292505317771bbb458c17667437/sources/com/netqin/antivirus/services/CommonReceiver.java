package com.netqin.antivirus.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.widget.Toast;

public class CommonReceiver extends BroadcastReceiver {
    public static final String ACTION_MOBILEDATA_MODE = "android.intent.action.MOBILEDATA_MODE";
    public static final String ACTION_SHOW_TOAST = "com.netqin.mobileguard.action_show_toast";
    private static final long DELAY = 10000;
    private static final String TAG = "CommonReceiver";

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (ACTION_SHOW_TOAST.equals(action)) {
            showToast(context, intent);
        } else if ("android.intent.action.BOOT_COMPLETED".equals(action)) {
            onBootCompleted(context, intent);
        }
    }

    private void onBootCompleted(Context context, Intent intent) {
        ((AlarmManager) context.getSystemService("alarm")).set(2, SystemClock.elapsedRealtime() + DELAY, PendingIntent.getBroadcast(context, 0, new Intent(context, OnAlarmReceiver.class), 0));
    }

    private void showToast(Context context, Intent intent) {
        Toast.makeText(context.getApplicationContext(), intent.getIntExtra("resId", 0), 0).show();
    }
}
