package com.netqin.antivirus.services;

import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import com.netqin.antivirus.data.Application;
import com.netqin.antivirus.data.PreferenceDataHelper;
import com.netqin.antivirus.util.AppQueryCondition;
import com.netqin.antivirus.util.SystemUtils;
import com.netqin.antivirus.util.TaskManagerUtils;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

public class TaskManagerService extends Service {
    private static final String TAG = "service.TaskManagerService";
    private static final String TASK_MANAGER_ACTION_SCAN = "com.netqin.av_task_scan";
    private static Hashtable<String, ResolveInfo> mInstalledApps;
    /* access modifiers changed from: private */
    public static ArrayList<Application> mRunningApps = new ArrayList<>();
    /* access modifiers changed from: private */
    public ActivityManager mActivityManager;
    /* access modifiers changed from: private */
    public PackageManager mPackageManager;
    private ServiceHandler mServiceHandler;
    private Looper mServiceLooper;

    public void onCreate() {
        this.mActivityManager = (ActivityManager) getSystemService("activity");
        this.mPackageManager = getPackageManager();
        HandlerThread handlerThread = new HandlerThread(TAG, 10);
        handlerThread.start();
        this.mServiceLooper = handlerThread.getLooper();
        this.mServiceHandler = new ServiceHandler(this.mServiceLooper);
    }

    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            Intent intent = (Intent) msg.obj;
            int startId = msg.arg1;
            if (intent != null && TaskManagerService.TASK_MANAGER_ACTION_SCAN.equals(intent.getAction())) {
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                handleTaskScan();
                TaskManagerService.this.stopSelf(startId);
            }
        }

        private void handleTaskScan() {
            List<ActivityManager.RunningAppProcessInfo> runningAppsInfo = TaskManagerService.this.mActivityManager.getRunningAppProcesses();
            TaskManagerService.removeNotRunningApps(runningAppsInfo);
            Application temp = new Application();
            for (ActivityManager.RunningAppProcessInfo runAppInfo : runningAppsInfo) {
                if (!PreferenceDataHelper.isInKeyProccessList(TaskManagerService.this.getApplicationContext(), runAppInfo.processName) && !runAppInfo.processName.endsWith(".fm")) {
                    boolean isNewProcess = true;
                    temp.uid = runAppInfo.uid;
                    int index = TaskManagerService.mRunningApps.indexOf(temp);
                    if (index >= 0) {
                        ((Application) TaskManagerService.mRunningApps.get(index)).setMemory(TaskManagerService.this.mActivityManager.getProcessMemoryInfo(new int[]{runAppInfo.pid})[0].getTotalPss());
                        isNewProcess = false;
                        TaskManagerService.this.sendBroadcast(new Intent(TaskManagerUtils.ACTION_APP_CHANGED));
                    }
                    if (isNewProcess) {
                        Application app = new Application();
                        app.importance = runAppInfo.importance;
                        app.processName = runAppInfo.processName;
                        app.uid = runAppInfo.uid;
                        try {
                            app.packageName = runAppInfo.pkgList[0];
                            app.setIcon(TaskManagerService.this.mPackageManager.getApplicationIcon(runAppInfo.pkgList[0]));
                            ApplicationInfo info = TaskManagerService.this.mPackageManager.getApplicationInfo(app.packageName, 128);
                            app.setLabelName(TaskManagerService.this.mPackageManager.getApplicationLabel(info).toString());
                            TaskManagerService.initAppFilter(TaskManagerService.this.getApplicationContext(), app, info);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        app.setMemory(TaskManagerService.this.mActivityManager.getProcessMemoryInfo(new int[]{runAppInfo.pid})[0].getTotalPss());
                        TaskManagerService.mRunningApps.add(app);
                        TaskManagerService.this.sendBroadcast(new Intent(TaskManagerUtils.ACTION_APP_CHANGED));
                    }
                }
            }
        }
    }

    public static ArrayList<Application> getRunningApps(Context context) {
        return getRunningApps(context, null, true);
    }

    public static void removeNotRunningApps(List<ActivityManager.RunningAppProcessInfo> runningAppsInfo) {
        synchronized (mRunningApps) {
            Iterator<Application> iterator = mRunningApps.iterator();
            while (iterator.hasNext()) {
                Application app = iterator.next();
                boolean isRunning = false;
                Iterator<ActivityManager.RunningAppProcessInfo> it = runningAppsInfo.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (app.uid == it.next().uid) {
                            isRunning = true;
                            break;
                        }
                    } else {
                        break;
                    }
                }
                if (!isRunning) {
                    iterator.remove();
                }
            }
        }
    }

    public static synchronized Hashtable<String, ResolveInfo> getInstalledApps(Context context) {
        Hashtable<String, ResolveInfo> hashtable;
        synchronized (TaskManagerService.class) {
            if (mInstalledApps != null) {
                hashtable = mInstalledApps;
            } else {
                Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
                intent.addCategory("android.intent.category.LAUNCHER");
                List<ResolveInfo> infos = context.getPackageManager().queryIntentActivities(intent, 0);
                mInstalledApps = new Hashtable<>();
                for (ResolveInfo resolveInfo : infos) {
                    if (!(resolveInfo.activityInfo == null || resolveInfo.activityInfo.processName == null)) {
                        mInstalledApps.put(resolveInfo.activityInfo.processName, resolveInfo);
                    }
                }
                hashtable = mInstalledApps;
            }
        }
        return hashtable;
    }

    /* access modifiers changed from: private */
    public static void initAppFilter(Context context, Application app, ApplicationInfo info) {
        if (getInstalledApps(context).containsKey(info.processName)) {
            if (SystemUtils.isSystemPackage(info)) {
                app.filter.setAppLevel(2);
            } else {
                app.filter.setAppLevel(1);
            }
        } else if (SystemUtils.isSystemPackage(info)) {
            app.filter.setAppLevel(4);
        } else {
            Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
            intent.setPackage(info.packageName);
            intent.addCategory("android.intent.category.LAUNCHER");
            List<ResolveInfo> infos = context.getPackageManager().queryIntentActivities(intent, 0);
            if (infos == null || infos.size() <= 0) {
                app.filter.setAppLevel(4);
            } else {
                app.filter.setAppLevel(1);
            }
        }
        PackageManager pm = context.getPackageManager();
        if (pm.checkPermission("android.permission.INTERNET", info.packageName) == 0) {
            app.filter.addAppPermission("android.permission.INTERNET");
        }
        if (pm.checkPermission("android.permission.RECEIVE_BOOT_COMPLETED", info.packageName) == 0) {
            app.filter.addAppPermission("android.permission.RECEIVE_BOOT_COMPLETED");
        }
    }

    public static ArrayList<Application> getRunningApps(Context context, AppQueryCondition queryCondition, boolean isBlock) {
        if (isBlock) {
            scanTask(context);
        }
        ArrayList<Application> list = new ArrayList<>();
        if (queryCondition == null) {
            return (ArrayList) mRunningApps.clone();
        }
        Application self = TaskManagerUtils.getMgSelf(context);
        boolean hasSelf = false;
        Iterator it = ((ArrayList) mRunningApps.clone()).iterator();
        while (it.hasNext()) {
            Application app = (Application) it.next();
            if (app.filter.match(queryCondition)) {
                if (app.packageName.equals(self.packageName)) {
                    self = app;
                    hasSelf = true;
                } else {
                    list.add(app);
                }
            }
        }
        if (hasSelf) {
            list.add(self);
        }
        return list;
    }

    private static void scanTask(Context context) {
        PackageManager pm = context.getPackageManager();
        List<ActivityManager.RunningAppProcessInfo> runningAppsInfo = ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses();
        removeNotRunningApps(runningAppsInfo);
        Application temp = new Application();
        for (ActivityManager.RunningAppProcessInfo runAppInfo : runningAppsInfo) {
            if (!PreferenceDataHelper.isInKeyProccessList(context, runAppInfo.processName) && !runAppInfo.processName.endsWith(".fm")) {
                temp.uid = runAppInfo.uid;
                if (!mRunningApps.contains(temp)) {
                    Application app = new Application();
                    app.importance = runAppInfo.importance;
                    app.processName = runAppInfo.processName;
                    app.uid = runAppInfo.uid;
                    try {
                        app.packageName = runAppInfo.pkgList[0];
                        initAppFilter(context, app, pm.getApplicationInfo(app.packageName, 128));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    mRunningApps.add(app);
                }
            }
        }
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        Message msg = this.mServiceHandler.obtainMessage();
        msg.obj = intent;
        msg.arg1 = startId;
        this.mServiceHandler.sendMessage(msg);
        return 2;
    }

    public void onDestroy() {
        this.mServiceLooper.quit();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public static void scanRunningTask(Context context) {
        Intent intent = new Intent();
        intent.setAction(TASK_MANAGER_ACTION_SCAN);
        context.startService(intent);
    }

    public static void remove(Context context, Application app) {
        if (app != null) {
            synchronized (mRunningApps) {
                mRunningApps.remove(app);
            }
            PreferenceDataHelper.removeWhiteApp(context, app.packageName);
        }
    }
}
