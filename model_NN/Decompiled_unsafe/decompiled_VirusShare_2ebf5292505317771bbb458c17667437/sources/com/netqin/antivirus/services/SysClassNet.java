package com.netqin.antivirus.services;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

public class SysClassNet {
    private static final String CARRIER = "/carrier";
    private static final String RX_BYTES = "/statistics/rx_bytes";
    private static final String SYS_CLASS_NET = "/sys/class/net/";
    private static final String TX_BYTES = "/statistics/tx_bytes";

    private SysClassNet() {
    }

    public static boolean isUp(String inter) {
        StringBuilder sb = new StringBuilder();
        sb.append(SYS_CLASS_NET).append(inter).append(CARRIER);
        return new File(sb.toString()).canRead();
    }

    public static long getRxBytes(String inter) throws IOException {
        return readLong(inter, RX_BYTES);
    }

    public static long getTxBytes(String inter) throws IOException {
        return readLong(inter, TX_BYTES);
    }

    private static RandomAccessFile getFile(String filename) throws IOException {
        return new RandomAccessFile(new File(filename), "r");
    }

    private static long readLong(String inter, String file) {
        StringBuilder sb = new StringBuilder();
        sb.append(SYS_CLASS_NET).append(inter).append(file);
        RandomAccessFile raf = null;
        try {
            raf = getFile(sb.toString());
            long longValue = Long.valueOf(raf.readLine()).longValue();
            if (raf == null) {
                return longValue;
            }
            try {
                raf.close();
                return longValue;
            } catch (IOException e) {
                return longValue;
            }
        } catch (Exception e2) {
            if (raf != null) {
                try {
                    raf.close();
                } catch (IOException e3) {
                }
            }
            return 0;
        } catch (Throwable th) {
            if (raf != null) {
                try {
                    raf.close();
                } catch (IOException e4) {
                }
            }
            throw th;
        }
    }
}
