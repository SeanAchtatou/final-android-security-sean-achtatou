package com.netqin.antivirus.antilost;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.common.CommonUtils;
import com.netqin.antivirus.log.LogEngine;
import com.nqmobile.antivirus_ampro20.R;

public class AntiLostSetPwd extends Activity implements TextWatcher {
    private View mBtnOk;
    private TextView mMax;
    private EditText mPwdCfm;
    private EditText mPwdNew;
    int type;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.antilost_setpwd);
        ((TextView) findViewById(R.id.activity_name)).setText((int) R.string.act_name_antilost);
        this.type = getIntent().getIntExtra("type", 0);
        this.mPwdNew = (EditText) findViewById(R.id.antilost_setpwd_pwdnew);
        this.mPwdCfm = (EditText) findViewById(R.id.antilost_setpwd_pwdcfm);
        this.mPwdNew.addTextChangedListener(this);
        this.mPwdCfm.addTextChangedListener(this);
        this.mMax = (TextView) findViewById(R.id.antilost_max_10);
        findViewById(R.id.antilost_setpwd_back).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AntiLostSetPwd.this.clickBack();
            }
        });
        this.mBtnOk = findViewById(R.id.antilost_setpwd_ok);
        this.mBtnOk.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AntiLostSetPwd.this.clickOk();
            }
        });
        if (this.type != 1) {
            findViewById(R.id.antilost_setpassword_tip).setVisibility(8);
        }
        this.mBtnOk.setEnabled(false);
    }

    /* access modifiers changed from: private */
    public void clickBack() {
        finish();
    }

    /* access modifiers changed from: private */
    public void clickOk() {
        String pwdNew = this.mPwdNew.getText().toString();
        if (pwdNew.compareTo(this.mPwdCfm.getText().toString()) == 0 || TextUtils.isEmpty(pwdNew)) {
            SharedPreferences spf = getSharedPreferences("nq_antilost", 0);
            CommonUtils.putConfigWithStringValue(this, "nq_antilost", "password", pwdNew);
            if (this.type != 1) {
                Toast toast = Toast.makeText(this, (int) R.string.text_antilost_setpwd_succ, 0);
                toast.setGravity(81, 0, 100);
                toast.show();
            } else if (TextUtils.isEmpty(CommonUtils.getConfigWithStringValue(this, "nq_antilost", "securitynum", ""))) {
                Intent i = new Intent(this, AntiLostSetSecurityNum.class);
                i.putExtra("type", 1);
                startActivity(i);
            } else {
                CommonMethod.showNotification(this, getResources().getString(R.string.text_antilost_open_done));
                spf.edit().putBoolean("start", true).commit();
                spf.edit().putBoolean("running", true).commit();
                startService(new Intent(this, AntiLostService.class));
                LogEngine.insertOperationItemLog(11, "", getFilesDir().getPath());
            }
            finish();
            return;
        }
        CommonMethod.messageDialog(this, getString(R.string.text_antilost_setpassword_diff), (int) R.string.label_netqin_antivirus);
        this.mPwdCfm.setText("");
    }

    public void afterTextChanged(Editable s) {
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {
        String pwdNew = this.mPwdNew.getText().toString();
        String pwdCfm = this.mPwdCfm.getText().toString();
        if (TextUtils.isEmpty(pwdNew) || TextUtils.isEmpty(pwdCfm)) {
            this.mBtnOk.setEnabled(false);
        } else if (pwdNew.length() < 6 || pwdNew.length() > 10 || pwdCfm.length() < 6 || pwdCfm.length() > 10) {
            this.mBtnOk.setEnabled(false);
        } else {
            this.mBtnOk.setEnabled(true);
        }
        if (pwdNew.length() > 10) {
            this.mMax.setVisibility(0);
        } else {
            this.mMax.setVisibility(8);
        }
    }
}
