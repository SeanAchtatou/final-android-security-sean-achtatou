package com.netqin.antivirus.antilost;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ActivityManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.text.TextUtils;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.common.CommonUtils;
import com.netqin.antivirus.contact.vcard.VCardConfig;
import com.nqmobile.antivirus_ampro20.R;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AntiLostService extends Service implements SharedPreferences.OnSharedPreferenceChangeListener, AntiLostGPSNotify {
    /* access modifiers changed from: private */
    public static Boolean mAntiLostLock = false;
    /* access modifiers changed from: private */
    public static Boolean mLockRunning = false;
    /* access modifiers changed from: private */
    public String deleteString = "";
    /* access modifiers changed from: private */
    public boolean isPalyAlarm = false;
    private boolean mAlarmMark = false;
    private final IBinder mBinder = new AntiLostServiceBinder();
    private AntiLostLocation[] mLocationListeners = null;
    private LocationManager mLocationManager = null;
    MediaPlayer mMediaPlayer = null;
    private String mRecvMobile;
    private String mSMSText = "";
    private String mSimNum;
    private TelephonyManager mTelephoneManager = null;
    /* access modifiers changed from: private */
    public String userMsg = null;

    public class AntiLostServiceBinder extends Binder {
        public AntiLostServiceBinder() {
        }

        /* access modifiers changed from: package-private */
        public AntiLostService getService() {
            return AntiLostService.this;
        }
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        CommonMethod.logDebug("antilost", "AntiLostService::onSharedPreferenceChanged");
        SharedPreferences spf = getSharedPreferences("nq_antilost", 0);
        if (!Boolean.valueOf(spf.getBoolean("alarm", true)).booleanValue()) {
            spf.unregisterOnSharedPreferenceChangeListener(this);
            stopAlarm();
        }
    }

    public IBinder onBind(Intent arg0) {
        return this.mBinder;
    }

    public void onCreate() {
        super.onCreate();
        CommonMethod.logDebug("antilost", "AntiLostService::onCreate");
        this.mTelephoneManager = (TelephonyManager) getSystemService("phone");
        this.mLocationManager = (LocationManager) getSystemService("location");
    }

    public void onStart(Intent intent, int startId) {
        CommonMethod.logDebug("antilost", "AntiLostService::onStart");
        super.onStart(intent, startId);
        if (intent != null) {
            SharedPreferences spf = getSharedPreferences("nq_antilost", 0);
            int smsType = intent.getIntExtra("smstype", 0);
            this.mRecvMobile = intent.getStringExtra("phonenum");
            this.deleteString = intent.getStringExtra("todeletestring");
            this.userMsg = intent.getStringExtra("usermsg");
            boolean isNeedDelSms = true;
            switch (smsType) {
                case 1:
                    spf.edit().putBoolean("lock", true).commit();
                    break;
                case 2:
                    startReceivingLocationUpdates();
                    break;
                case 3:
                    spf.edit().putBoolean("lock", true).commit();
                    spf.edit().putBoolean("alarm", true).commit();
                    break;
                case 4:
                    dealRemoteDelete();
                    break;
                case 5:
                    dealResetPassword();
                    break;
                case 6:
                    break;
                default:
                    isNeedDelSms = false;
                    break;
            }
            checkLock();
            checkPlayAlarm();
            setAntiLostRunningState(true);
            if (smsType == 1) {
                sendSmsMsg(String.valueOf(getResources().getString(R.string.text_antilost_reply_lock)) + "\n" + getResources().getString(R.string.text_antilost_reply_done));
            } else if (smsType == 3) {
                sendSmsMsg(String.valueOf(getResources().getString(R.string.text_antilost_reply_alarm)) + "\n" + getResources().getString(R.string.text_antilost_reply_done));
            }
            if (isNeedDelSms) {
                deleteCommandSms();
            }
        }
    }

    public void onDestroy() {
        CommonMethod.logDebug("antilost", "AntiLostService::onDestroy");
        setAntiLostRunningState(false);
        stopAlarm();
        stopReceivingLocationUpdates();
        super.onDestroy();
    }

    private void setAntiLostRunningState(Boolean running) {
        getSharedPreferences("nq_antilost", 0).edit().putBoolean("running", running.booleanValue()).commit();
    }

    private void showNotification() {
    }

    private void checkLock() {
        mAntiLostLock = Boolean.valueOf(getSharedPreferences("nq_antilost", 0).getBoolean("lock", false));
        if (mAntiLostLock.booleanValue() && !mLockRunning.booleanValue()) {
            Intent in = new Intent(this, AntiLostLock.class);
            in.setFlags(VCardConfig.FLAG_USE_QP_TO_PRIMARY_PROPERTIES);
            if (this.userMsg != null && this.userMsg.length() > 0) {
                in.putExtra("usermsg", this.userMsg);
            }
            startActivity(in);
            try {
                PackageManager pm = getPackageManager();
                ActivityManager am = (ActivityManager) getApplicationContext().getSystemService("activity");
                List<ActivityManager.RunningTaskInfo> rtList = am.getRunningTasks(100);
                for (int i = 0; i < rtList.size(); i++) {
                    ActivityInfo ai = pm.getActivityInfo(rtList.get(i).baseActivity, 1152);
                    if (!pm.getApplicationInfo(ai.packageName, 1152).publicSourceDir.startsWith("/system/") && !ai.packageName.toLowerCase().contains("com.nqmobile")) {
                        am.restartPackage(ai.packageName);
                    }
                }
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            Thread thread = new Thread(new Runnable() {
                public void run() {
                    while (AntiLostService.mAntiLostLock.booleanValue()) {
                        try {
                            Thread.sleep(3000);
                            AntiLostService.mLockRunning = true;
                            ActivityManager am = (ActivityManager) AntiLostService.this.getApplicationContext().getSystemService("activity");
                            List<ActivityManager.RunningTaskInfo> rtList = am.getRunningTasks(1);
                            PackageManager pm = AntiLostService.this.getPackageManager();
                            ComponentName activityName = rtList.get(0).baseActivity;
                            String activityNameStr = rtList.get(0).topActivity.getClassName();
                            ActivityInfo ai = pm.getActivityInfo(activityName, 1152);
                            if (!pm.getApplicationInfo(ai.packageName, 1152).publicSourceDir.startsWith("/system/") && !ai.packageName.toLowerCase().contains("com.nqmobile")) {
                                am.restartPackage(ai.packageName);
                            }
                            if (!activityNameStr.equalsIgnoreCase("com.netqin.antivirus.antilost.AntiLostLock")) {
                                Intent in = new Intent(AntiLostService.this, AntiLostLock.class);
                                in.setFlags(VCardConfig.FLAG_USE_QP_TO_PRIMARY_PROPERTIES);
                                if (AntiLostService.this.userMsg != null && AntiLostService.this.userMsg.length() > 0) {
                                    in.putExtra("usermsg", AntiLostService.this.userMsg);
                                }
                                AntiLostService.this.startActivity(in);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    AntiLostService.mLockRunning = false;
                }
            });
            thread.setPriority(10);
            thread.start();
        }
    }

    private void sendLocation() {
        String reply;
        CommonMethod.logDebug("antilost", "AntiLostService::sendLocation");
        double lat = 0.0d;
        double lon = 0.0d;
        Location loc = getGPSLocation();
        if (loc != null) {
            lat = loc.getLatitude();
            lon = loc.getLongitude();
        }
        TelephonyManager tm = (TelephonyManager) getSystemService("phone");
        String deviceId = tm.getDeviceId();
        String country = "";
        int cellid = 0;
        int area = 0;
        int sid = 0;
        int nid = 0;
        int bid = 0;
        String mnc = tm.getNetworkOperator();
        if (mnc != null) {
            country = mnc.substring(0, 3);
            mnc = mnc.substring(3);
        }
        if (tm.getNetworkType() == 4 || tm.getPhoneType() == 2) {
            CdmaCellLocation location = (CdmaCellLocation) tm.getCellLocation();
            bid = location.getBaseStationId();
            nid = location.getNetworkId();
            sid = location.getSystemId();
        } else {
            GsmCellLocation location2 = (GsmCellLocation) tm.getCellLocation();
            cellid = location2.getCid();
            area = location2.getLac();
        }
        String reply2 = String.valueOf(getResources().getString(R.string.text_antilost_reply_locate)) + "\n" + getResources().getString(R.string.text_antilost_reply_done) + "\n" + getResources().getString(R.string.text_antilost_reply_locate_link);
        if (!(lat == 0.0d && lon == 0.0d)) {
            lat = ((double) ((int) (10000.0d * lat))) / 10000.0d;
            lon = ((double) ((int) (10000.0d * lon))) / 10000.0d;
        }
        if (tm.getNetworkType() == 4 || tm.getPhoneType() == 2) {
            String q = String.valueOf(country) + "," + nid + "," + sid + "," + bid + "," + "-60" + "," + "5555" + "," + lat + "," + lon;
            reply = String.valueOf(reply2) + "\n" + "http://my.netqin.com/m/cdma/?q=" + q + "&ck=" + CommonMethod.encryptMD5(q);
        } else {
            String q2 = String.valueOf(country) + "," + mnc + "," + area + "," + cellid + "," + lat + "," + lon;
            reply = String.valueOf(reply2) + "\n" + "http://my.netqin.com/m/?q=" + q2 + "&ck=" + CommonMethod.encryptMD5(q2);
        }
        if (!TextUtils.isEmpty(this.mRecvMobile)) {
            sendSmsMsg(reply);
        }
    }

    private void checkPlayAlarm() {
        SharedPreferences spf = getSharedPreferences("nq_antilost", 0);
        if (Boolean.valueOf(spf.getBoolean("alarm", false)).booleanValue()) {
            playAlarm();
            spf.registerOnSharedPreferenceChangeListener(this);
        }
    }

    private void playAlarm() {
        this.isPalyAlarm = true;
        if (this.mMediaPlayer == null) {
            this.mMediaPlayer = new MediaPlayer();
        }
        if (!this.mMediaPlayer.isLooping() && !this.mMediaPlayer.isPlaying()) {
            try {
                AudioManager am = (AudioManager) getSystemService("audio");
                am.setStreamVolume(3, am.getStreamMaxVolume(3), 0);
                this.mMediaPlayer.setDataSource(getApplicationContext(), Uri.parse("android.resource://" + getResources().getResourcePackageName(R.raw.antilost) + "/" + ((int) R.raw.antilost)));
                this.mMediaPlayer.prepare();
                this.mMediaPlayer.setLooping(true);
                this.mMediaPlayer.start();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            Thread thread = new Thread(new Runnable() {
                public void run() {
                    while (AntiLostService.this.isPalyAlarm) {
                        AudioManager am = (AudioManager) AntiLostService.this.getSystemService("audio");
                        am.setStreamVolume(3, am.getStreamMaxVolume(3), 0);
                        try {
                            Thread.sleep(6000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            thread.setPriority(5);
            thread.start();
        }
    }

    private void stopAlarm() {
        this.isPalyAlarm = false;
        if (this.mMediaPlayer != null) {
            this.mMediaPlayer.stop();
            this.mMediaPlayer = null;
        }
    }

    private void startReceivingLocationUpdates() {
        CommonMethod.logDebug("antilost", "AntiLostService::startReceivingLocationUpdates begin");
        this.mLocationListeners = new AntiLostLocation[]{new AntiLostLocation(this.mTelephoneManager, this.mLocationManager, "gps", this), new AntiLostLocation(this.mTelephoneManager, this.mLocationManager, "network", this)};
        if (this.mLocationManager != null) {
            try {
                this.mLocationManager.requestLocationUpdates("gps", 1200000, 0.0f, this.mLocationListeners[0]);
            } catch (IllegalArgumentException | SecurityException e) {
            }
            try {
                this.mLocationManager.requestLocationUpdates("network", 1200000, 0.0f, this.mLocationListeners[1]);
            } catch (IllegalArgumentException | SecurityException e2) {
            }
        }
        CommonMethod.logDebug("antilost", "AntiLostService::startReceivingLocationUpdates end");
    }

    private void stopReceivingLocationUpdates() {
        CommonMethod.logDebug("antilost", "AntiLostService::stopReceivingLocationUpdates begin");
        if (!(this.mLocationManager == null || this.mLocationListeners == null)) {
            for (int i = 0; i < this.mLocationListeners.length; i++) {
                try {
                    this.mLocationManager.removeUpdates(this.mLocationListeners[i]);
                } catch (Exception e) {
                }
            }
        }
        CommonMethod.logDebug("antilost", "AntiLostService::stopReceivingLocationUpdates end");
    }

    private Location getGPSLocation() {
        CommonMethod.logDebug("antilost", "AntiLostService::getGPSLocation begin");
        for (AntiLostLocation locationCurrent : this.mLocationListeners) {
            Location loc = locationCurrent.getLocationCurrent();
            if (loc != null) {
                CommonMethod.logDebug("antilost", "AntiLostService::getGPSLocation end 1");
                return loc;
            }
        }
        CommonMethod.logDebug("antilost", "AntiLostService::getGPSLocation end 2");
        return null;
    }

    private void checkAntiLost() {
        CommonMethod.logDebug("antilost", "AntiLostService::checkAntiLost begin");
        startReceivingLocationUpdates();
        String sim = this.mTelephoneManager.getSimSerialNumber();
        int count = 0;
        while (TextUtils.isEmpty(sim) && count < 60) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            sim = this.mTelephoneManager.getSimSerialNumber();
            count++;
        }
        if (!TextUtils.isEmpty(sim)) {
            if (!sim.equalsIgnoreCase(this.mSimNum)) {
                this.mAlarmMark = true;
            }
        } else if (!TextUtils.isEmpty(this.mSimNum)) {
            this.mAlarmMark = true;
        }
        Location loc = null;
        if (loc != null) {
            constrctSMText(loc.getLongitude(), loc.getLatitude(), loc.getAltitude());
        } else {
            JSONArray jsnArray = this.mLocationListeners[0].getCellInfoCurrent();
            if (jsnArray != null) {
                constrctSMText(jsnArray);
            }
        }
        CommonMethod.logDebug("antilost", "AntiLostService::checkAntiLost end");
    }

    private void constrctSMText(double longitude, double latitude, double altitude) {
        CommonMethod.logDebug("antilost", "AntiLostService::constrctSMText begin 3");
        this.mSMSText = "your phone: imei=" + this.mTelephoneManager.getDeviceId() + "  longitude=" + longitude + "  latitude=" + latitude + "  altitude=" + altitude;
        CommonMethod.logDebug("antilost", "AntiLostService::constrctSMText end 3");
    }

    private void constrctSMText(JSONArray array) {
        CommonMethod.logDebug("antilost", "AntiLostService::constrctSMText begin");
        String imei = this.mTelephoneManager.getDeviceId();
        int len = array.length();
        int[] cellid = new int[4];
        int mcc = 0;
        int mnc = 0;
        int lac = 0;
        if (len > 2) {
            len = 2;
        }
        for (int i = 0; i < len; i++) {
            try {
                JSONObject data = array.getJSONObject(i);
                mcc = data.getInt("mobile_country_code");
                mnc = data.getInt("mobile_network_code");
                cellid[i] = data.getInt("cell_id");
                lac = data.getInt("location_area_code");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        this.mSMSText = "your phone: imei=" + imei + "  country_code=" + mcc + "  network_code=" + mnc + "  area_code=" + lac + "  cell_id1=" + cellid[0] + "  cell_id2=" + cellid[1];
        CommonMethod.logDebug("antilost", "AntiLostService::constrctSMText end");
    }

    private void deleteCommandSms() {
        final Context ct = getApplicationContext();
        Thread thread = new Thread(new Runnable() {
            public void run() {
                SmsHandler sh = SmsHandler.getInstance(ct);
                int i = 0;
                while (true) {
                    if (i >= 20) {
                        break;
                    }
                    try {
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Cursor c = sh.getLatestSms();
                    if (c.getCount() != 0) {
                        c.moveToFirst();
                        long id = c.getLong(c.getColumnIndex(SmsHandler.ROWID));
                        String body = c.getString(c.getColumnIndex(SmsHandler.BODY)).toLowerCase().trim();
                        c.close();
                        if (!TextUtils.isEmpty(body) && !TextUtils.isEmpty(AntiLostService.this.deleteString) && body.indexOf(AntiLostService.this.deleteString) > 0) {
                            sh.deleteSms(id);
                            break;
                        }
                    } else {
                        c.close();
                    }
                    i++;
                }
                Cursor cur = sh.getInboxSms(10);
                Integer num = Integer.valueOf(cur.getCount());
                if (num.intValue() == 0) {
                    cur.close();
                    return;
                }
                int j = 0;
                while (true) {
                    if (j < num.intValue()) {
                        cur.moveToPosition(j);
                        long id2 = cur.getLong(cur.getColumnIndex(SmsHandler.ROWID));
                        String body2 = cur.getString(cur.getColumnIndex(SmsHandler.BODY)).toLowerCase().trim();
                        if (!TextUtils.isEmpty(body2) && !TextUtils.isEmpty(AntiLostService.this.deleteString) && body2.indexOf(AntiLostService.this.deleteString) > 0) {
                            sh.deleteSms(id2);
                            break;
                        }
                        j++;
                    } else {
                        break;
                    }
                }
                cur.close();
            }
        });
        thread.setPriority(10);
        thread.start();
    }

    private void dealRemoteDelete() {
        final Context ct = getApplicationContext();
        Thread thread = new Thread(new Runnable() {
            public void run() {
                AntiLostService.this.sendSmsMsg(String.valueOf(AntiLostService.this.getResources().getString(R.string.text_antilost_reply_delete)) + "\n" + AntiLostService.this.getResources().getString(R.string.text_antilost_reply_begin));
                AntiLostDeletor.deleteAllFile("/sdcard/");
                AntiLostDeletor.deleteAllSMS(ct);
                AntiLostDeletor.deleteAllMMS(ct);
                AntiLostDeletor.deleteAllContacts(ct);
                AntiLostDeletor.deleteAllCallLog(ct);
                CommonMethod.cleanContactAccount(ct);
                AntiLostService.this.clearGoogleAccount();
                AntiLostService.this.sendSmsMsg(String.valueOf(AntiLostService.this.getResources().getString(R.string.text_antilost_reply_delete)) + "\n" + AntiLostService.this.getResources().getString(R.string.text_antilost_reply_delete_done));
            }
        });
        thread.setPriority(5);
        thread.start();
    }

    private void dealResetPassword() {
        String p = new StringBuilder(String.valueOf((int) (Math.random() * 1000000.0d))).toString();
        while (p.length() < 6) {
            p = String.valueOf(p) + "6";
        }
        while (p.length() > 6) {
            p = p.substring(0, 6);
        }
        SharedPreferences sharedPreferences = getSharedPreferences("nq_antilost", 0);
        CommonUtils.putConfigWithStringValue(this, "nq_antilost", "password", p);
        sendSmsMsg(String.valueOf(getResources().getString(R.string.text_antilost_reply_reset)) + "\n" + getResources().getString(R.string.text_antilost_reply_done) + "\n" + getResources().getString(R.string.text_antilost_reply_reset_newpwd) + p);
    }

    public void clearGoogleAccount() {
        AccountManager accountManager = AccountManager.get(getApplicationContext());
        Account[] acs = accountManager.getAccounts();
        for (int i = 0; i < acs.length; i++) {
            try {
                accountManager.clearPassword(acs[i]);
                accountManager.removeAccount(acs[i], null, null).getResult();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void GPSLocateFinish() {
        sendLocation();
        stopReceivingLocationUpdates();
    }

    /* access modifiers changed from: private */
    public void sendSmsMsg(String content) {
        SmsManager sms = SmsManager.getDefault();
        try {
            ArrayList<String> messageArray = sms.divideMessage(content);
            ArrayList<PendingIntent> sentIntents = new ArrayList<>();
            for (int i = 0; i < messageArray.size(); i++) {
                sentIntents.add(PendingIntent.getBroadcast(this, 0, new Intent(), 0));
            }
            ArrayList<PendingIntent> deliverIntents = new ArrayList<>();
            for (int i2 = 0; i2 < messageArray.size(); i2++) {
                deliverIntents.add(PendingIntent.getBroadcast(this, 0, new Intent(), 0));
            }
            sms.sendMultipartTextMessage(this.mRecvMobile, null, messageArray, sentIntents, deliverIntents);
        } catch (Exception e) {
        }
    }
}
