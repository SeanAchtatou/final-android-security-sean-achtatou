package com.netqin.antivirus.antilost;

import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.telephony.SmsManager;
import com.netqin.antivirus.Value;
import java.util.ArrayList;

public class SmsHandler {
    public static final String ADDRESS = "address";
    public static final String BODY = "body";
    public static final String DATE = "date";
    public static final int MESSAGE_TYPE_INBOX = 1;
    public static final int MESSAGE_TYPE_OUTBOX = 4;
    public static final int MESSAGE_TYPE_SENT = 2;
    public static final String READ = "read";
    public static final String ROWID = "_id";
    public static final String TYPE = "type";
    private static SmsHandler mHandler;
    public static Uri mMmsUri = Uri.parse("content://mms");
    public static Uri mSmsUri = Uri.parse("content://sms");
    private Context mContext;
    /* access modifiers changed from: private */
    public long mMaxDate = 0;
    private int mSmsCount = 0;

    public static synchronized SmsHandler getInstance(Context ctx) {
        SmsHandler smsHandler;
        synchronized (SmsHandler.class) {
            if (mHandler == null) {
                mHandler = new SmsHandler(ctx);
            }
            smsHandler = mHandler;
        }
        return smsHandler;
    }

    private SmsHandler(Context context) {
        this.mContext = context;
        this.mSmsCount = getSmsCount();
        this.mMaxDate = getMaxDate();
    }

    public Cursor getAllSms() {
        return this.mContext.getContentResolver().query(mSmsUri, null, null, null, "date DESC");
    }

    public Cursor getLatestSms() {
        return this.mContext.getContentResolver().query(mSmsUri, null, null, null, "_id DESC limit 1");
    }

    public int getSmsCount() {
        Cursor c = this.mContext.getContentResolver().query(mSmsUri, null, null, null, "date DESC");
        int i = c.getCount();
        c.close();
        return i;
    }

    public int getUnReadSmsCount() {
        Cursor c = this.mContext.getContentResolver().query(mSmsUri, new String[]{ROWID}, "read = 0", null, "date DESC");
        int i = c.getCount();
        c.close();
        return i;
    }

    public long getMaxDate() {
        long id = 0;
        Cursor c = this.mContext.getContentResolver().query(mSmsUri, new String[]{"date"}, null, null, "_id DESC limit 1");
        if (c.getCount() > 0) {
            c.moveToFirst();
            id = c.getLong(c.getColumnIndex("date"));
        }
        c.close();
        return id;
    }

    public int deleteAllSms() {
        return this.mContext.getContentResolver().delete(mSmsUri, null, null);
    }

    public int deleteAllMms() {
        return this.mContext.getContentResolver().delete(mMmsUri, null, null);
    }

    public boolean deleteSms(long _id) {
        if (this.mContext.getContentResolver().delete(Uri.parse("content://sms/" + _id), null, null) > 0) {
            return true;
        }
        return false;
    }

    public boolean updateSms(long _id, ContentValues values) {
        if (this.mContext.getContentResolver().update(Uri.parse("content://sms/" + _id), values, null, null) > 0) {
            return true;
        }
        return false;
    }

    public Cursor getSmsById(long _id) {
        Cursor c = this.mContext.getContentResolver().query(mSmsUri, null, "_id = " + _id, null, null);
        if (c != null) {
            c.moveToFirst();
        }
        return c;
    }

    public Cursor getInboxSms() {
        return this.mContext.getContentResolver().query(mSmsUri, null, "type = 1", null, "date DESC");
    }

    public Cursor getSentSms() {
        return this.mContext.getContentResolver().query(mSmsUri, null, "type = 2", null, "date DESC");
    }

    public Cursor getInboxSms(int num) {
        return this.mContext.getContentResolver().query(mSmsUri, null, "type = 1", null, "_id DESC limit " + num);
    }

    public static boolean sendSms(String phone, String content, PendingIntent sentIntent) {
        SmsManager sms = SmsManager.getDefault();
        try {
            ArrayList<String> messageArray = sms.divideMessage(content);
            ArrayList<PendingIntent> sentIntents = new ArrayList<>();
            for (int i = 0; i < messageArray.size(); i++) {
                sentIntents.add(sentIntent);
            }
            sms.sendMultipartTextMessage(phone, null, messageArray, sentIntents, null);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public static boolean createSentSms(Context context, String phone, String body) {
        ContentValues values = new ContentValues();
        values.put(ADDRESS, phone);
        values.put(BODY, body);
        values.put("type", (Integer) 2);
        return context.getContentResolver().insert(mSmsUri, values) != null;
    }

    public boolean createInboxSms(String phone, long time, String body, int read, int type) {
        ContentValues values = new ContentValues();
        values.put(ADDRESS, phone);
        values.put(BODY, body);
        values.put("date", Long.valueOf(time));
        values.put(READ, Integer.valueOf(read));
        values.put("type", Integer.valueOf(type));
        return this.mContext.getContentResolver().insert(mSmsUri, values) != null;
    }

    public void registerSmsObserver(ContentObserver observer) {
        this.mContext.getContentResolver().registerContentObserver(mSmsUri, true, observer);
        this.mSmsCount = getSmsCount();
        this.mMaxDate = getMaxDate();
    }

    public void unregisterSmsObserver(ContentObserver observer) {
        this.mContext.getContentResolver().unregisterContentObserver(observer);
    }

    public ContentObserver getSmsObserver(Handler handler) {
        return new SmsObserver(handler);
    }

    public class SmsObserver extends ContentObserver {
        private Handler mHandler;

        public SmsObserver(Handler handler) {
            super(handler);
            this.mHandler = handler;
        }

        public void onChange(boolean selfChange) {
            long i = SmsHandler.this.getMaxDate();
            if (i > SmsHandler.this.mMaxDate) {
                SmsHandler.this.mMaxDate = i;
                Cursor c = SmsHandler.this.getLatestSms();
                if (c.getCount() == 0) {
                    c.close();
                    return;
                }
                c.moveToFirst();
                String number = c.getString(c.getColumnIndex(SmsHandler.ADDRESS));
                long _id = c.getLong(c.getColumnIndex(SmsHandler.ROWID));
                this.mHandler.sendMessage(this.mHandler.obtainMessage(Value.SMS_CHANGE_MSG_VALUE, (int) _id, c.getInt(c.getColumnIndex("type")), number));
                c.close();
                return;
            }
            SmsHandler.this.mMaxDate = i;
        }
    }

    public static boolean SendRemoteControlSMS(int type, String phone, String password, PendingIntent sentIntent) {
        String command = null;
        if (type == 1) {
            command = "locate";
        } else if (type == 2) {
            command = "lock";
        } else if (type == 3) {
            command = "alarm";
        } else if (type == 4) {
            command = "delete";
        }
        return sendSms(phone, String.valueOf(command) + "*" + password + "*", null);
    }
}
