package com.netqin.antivirus.antilost;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import com.nqmobile.antivirus_ampro20.R;

public class AntiLostForgetPassword extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.antilost_forget);
        ((TextView) findViewById(R.id.activity_name)).setText((int) R.string.act_name_antilost);
    }
}
