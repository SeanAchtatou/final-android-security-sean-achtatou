package com.netqin.antivirus.antilost;

import android.app.ActivityManager;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import com.netqin.antivirus.contact.vcard.VCardConfig;
import com.nqmobile.antivirus_ampro20.R;
import java.io.IOException;

public class AntiLostLockExampleServise extends Service implements SharedPreferences.OnSharedPreferenceChangeListener {
    /* access modifiers changed from: private */
    public static Boolean mAntiLostLock = false;
    /* access modifiers changed from: private */
    public boolean isPalyAlarm = false;
    private final IBinder mBinder = new AntiLostServiceBinder();
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    Intent in = new Intent(AntiLostLockExampleServise.this, AntiLostLockExample.class);
                    in.setFlags(VCardConfig.FLAG_USE_QP_TO_PRIMARY_PROPERTIES);
                    AntiLostLockExampleServise.this.startActivity(in);
                    return;
                default:
                    return;
            }
        }
    };
    MediaPlayer mMediaPlayer = null;

    public class AntiLostServiceBinder extends Binder {
        public AntiLostServiceBinder() {
        }

        /* access modifiers changed from: package-private */
        public AntiLostLockExampleServise getService() {
            return AntiLostLockExampleServise.this;
        }
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        SharedPreferences spf = getSharedPreferences("nq_antilost", 0);
        mAntiLostLock = Boolean.valueOf(spf.getBoolean("lock_example", false));
        this.isPalyAlarm = spf.getBoolean("alarm_example", false);
        if (!mAntiLostLock.booleanValue() && !this.isPalyAlarm) {
            spf.unregisterOnSharedPreferenceChangeListener(this);
            stopAlarm();
            stopSelf();
        }
    }

    public IBinder onBind(Intent arg0) {
        return this.mBinder;
    }

    public void onCreate() {
        super.onCreate();
        SharedPreferences spf = getSharedPreferences("nq_antilost", 0);
        mAntiLostLock = Boolean.valueOf(spf.getBoolean("lock_example", false));
        this.isPalyAlarm = spf.getBoolean("alarm_example", false);
        spf.registerOnSharedPreferenceChangeListener(this);
    }

    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        if (mAntiLostLock.booleanValue()) {
            doLock();
        }
        if (this.isPalyAlarm) {
            playAlarm();
        }
    }

    public void onDestroy() {
        this.isPalyAlarm = false;
        mAntiLostLock = false;
        getSharedPreferences("nq_antilost", 0).unregisterOnSharedPreferenceChangeListener(this);
        stopAlarm();
        super.onDestroy();
    }

    private void doLock() {
        Thread thread = new Thread(new Runnable() {
            public void run() {
                while (AntiLostLockExampleServise.mAntiLostLock.booleanValue()) {
                    try {
                        if (!((ActivityManager) AntiLostLockExampleServise.this.getApplicationContext().getSystemService("activity")).getRunningTasks(1).get(0).topActivity.getClassName().equalsIgnoreCase("com.netqin.antivirus.antilost.AntiLostLockExample")) {
                            Message message = new Message();
                            message.what = 1;
                            AntiLostLockExampleServise.this.mHandler.sendMessage(message);
                        }
                        Thread.sleep(1000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        thread.setPriority(10);
        thread.start();
    }

    private void playAlarm() {
        this.mMediaPlayer = new MediaPlayer();
        try {
            AudioManager am = (AudioManager) getSystemService("audio");
            am.setStreamVolume(3, am.getStreamMaxVolume(3), 0);
            this.mMediaPlayer.setDataSource(getApplicationContext(), Uri.parse("android.resource://" + getResources().getResourcePackageName(R.raw.antilost) + "/" + ((int) R.raw.antilost)));
            this.mMediaPlayer.prepare();
            this.mMediaPlayer.setLooping(true);
            this.mMediaPlayer.start();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        Thread thread = new Thread(new Runnable() {
            public void run() {
                while (AntiLostLockExampleServise.this.isPalyAlarm) {
                    AudioManager am = (AudioManager) AntiLostLockExampleServise.this.getSystemService("audio");
                    am.setStreamVolume(3, am.getStreamMaxVolume(3), 0);
                    try {
                        Thread.sleep(6000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        thread.setPriority(5);
        thread.start();
    }

    private void stopAlarm() {
        this.isPalyAlarm = false;
        if (this.mMediaPlayer != null) {
            this.mMediaPlayer.stop();
        }
    }
}
