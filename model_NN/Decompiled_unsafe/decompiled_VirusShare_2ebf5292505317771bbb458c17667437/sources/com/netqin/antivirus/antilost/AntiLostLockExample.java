package com.netqin.antivirus.antilost;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.nqmobile.antivirus_ampro20.R;
import java.util.Timer;
import java.util.TimerTask;

public class AntiLostLockExample extends Activity implements TextWatcher {
    TextView countTxt;
    EditText etPassword;
    Button mBtnOk;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    AntiLostLockExample.this.updateCount();
                    return;
                default:
                    return;
            }
        }
    };
    int second;
    Timer timer;

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 84 || keyCode == 24 || keyCode == 4 || keyCode == 25) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == 84 || keyCode == 24 || keyCode == 4 || keyCode == 25) {
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences spf = getSharedPreferences("nq_antilost", 0);
        boolean lock = spf.getBoolean("lock_example", false);
        boolean alarm = spf.getBoolean("alarm_example", false);
        if (lock || alarm) {
            getWindow().setFlags(1024, 1024);
            requestWindowFeature(1);
            setContentView((int) R.layout.antilost_lock_example);
            this.second = getIntent().getIntExtra("second", -1);
            if (this.second < 0) {
                this.second = 10;
            }
            this.etPassword = (EditText) findViewById(R.id.antilost_lock_password);
            this.etPassword.addTextChangedListener(this);
            this.mBtnOk = (Button) findViewById(R.id.antilost_lock_ok);
            this.mBtnOk.setEnabled(false);
            this.mBtnOk.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    AntiLostLockExample.this.clickBtnOk();
                }
            });
            String tip = getString(R.string.text_antilost_lock_example_tip2, new Object[]{Integer.valueOf(this.second)});
            this.countTxt = (TextView) findViewById(R.id.antilost_lock_exmple_count);
            this.countTxt.setText(tip);
            this.timer = new Timer();
            this.timer.scheduleAtFixedRate(new MyTask(this, null), 1000, 1000);
            return;
        }
        finish();
    }

    private class MyTask extends TimerTask {
        private MyTask() {
        }

        /* synthetic */ MyTask(AntiLostLockExample antiLostLockExample, MyTask myTask) {
            this();
        }

        public void run() {
            Message message = new Message();
            message.what = 1;
            AntiLostLockExample.this.mHandler.sendMessage(message);
        }
    }

    /* access modifiers changed from: private */
    public void clickBtnOk() {
        String pwd = this.etPassword.getText().toString();
        SharedPreferences spf = getSharedPreferences("nq_antilost", 0);
        if (!TextUtils.isEmpty(pwd)) {
            spf.edit().putBoolean("lock_example", false).commit();
            spf.edit().putBoolean("alarm_example", false).commit();
            finish();
        }
    }

    public void afterTextChanged(Editable s) {
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (TextUtils.isEmpty(this.etPassword.getText().toString())) {
            this.mBtnOk.setEnabled(false);
        } else {
            this.mBtnOk.setEnabled(true);
        }
    }

    /* access modifiers changed from: private */
    public void updateCount() {
        if (this.second > 0) {
            this.second--;
            this.countTxt.setText(getString(R.string.text_antilost_lock_example_tip2, new Object[]{Integer.valueOf(this.second)}));
            return;
        }
        this.timer.cancel();
        SharedPreferences spf = getSharedPreferences("nq_antilost", 0);
        spf.edit().putBoolean("lock_example", false).commit();
        spf.edit().putBoolean("alarm_example", false).commit();
        finish();
    }
}
