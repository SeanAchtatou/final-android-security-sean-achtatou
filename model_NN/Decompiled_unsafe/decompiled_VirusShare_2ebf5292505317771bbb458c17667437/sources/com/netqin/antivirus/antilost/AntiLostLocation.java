package com.netqin.antivirus.antilost;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.telephony.NeighboringCellInfo;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import com.netqin.antivirus.common.CommonMethod;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AntiLostLocation implements LocationListener {
    private int[] mCid = null;
    private LocationManager mLM = null;
    private int[] mLac = null;
    private Location mLastLocation;
    private int mMCC;
    private int mMNC;
    private String mProvider;
    private AntiLostGPSNotify mService;
    private TelephonyManager mTM = null;
    private boolean mValid = false;

    public AntiLostLocation(TelephonyManager tm, LocationManager lm, String provider, AntiLostGPSNotify service) {
        this.mTM = tm;
        this.mLM = lm;
        this.mProvider = provider;
        this.mLastLocation = new Location(this.mProvider);
        this.mService = service;
    }

    public void onLocationChanged(Location newLocation) {
        CommonMethod.logDebug("antilost", "AntiLostLocation::onLocationChanged ");
        if (newLocation != null) {
            this.mLastLocation.set(newLocation);
            this.mValid = true;
        }
        this.mService.GPSLocateFinish();
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
        CommonMethod.logDebug("antilost", "AntiLostLocation::onStatusChanged " + status);
        switch (status) {
            case 0:
            case 1:
                this.mValid = false;
                return;
            default:
                return;
        }
    }

    public void onProviderEnabled(String provider) {
        CommonMethod.logDebug("antilost", "AntiLostLocation::onProviderEnabled " + provider);
    }

    public void onProviderDisabled(String provider) {
        CommonMethod.logDebug("antilost", "AntiLostLocation::onProviderDisabled " + provider);
        this.mValid = false;
        this.mService.GPSLocateFinish();
    }

    public Location getLocationCurrent() {
        CommonMethod.logDebug("antilost", "AntiLostLocation::getLocationCurrent");
        if (this.mValid) {
            return this.mLastLocation;
        }
        return this.mLM.getLastKnownLocation(this.mProvider);
    }

    public JSONArray getCellInfoCurrent() {
        CommonMethod.logDebug("antilost", "AntiLostLocation::getCellInfoCurrent");
        GsmCellLocation gsm = (GsmCellLocation) this.mTM.getCellLocation();
        List<NeighboringCellInfo> list = this.mTM.getNeighboringCellInfo();
        int count = list.size();
        if (count <= 0) {
            return null;
        }
        this.mLac = new int[count];
        this.mCid = new int[count];
        this.mLac[0] = gsm.getLac();
        this.mMCC = Integer.valueOf(this.mTM.getNetworkOperator().substring(0, 3)).intValue();
        this.mMNC = Integer.valueOf(this.mTM.getNetworkOperator().substring(3, 5)).intValue();
        JSONArray jsnArray = new JSONArray();
        for (int i = 0; i < count; i++) {
            this.mLac[i] = this.mLac[0];
            this.mCid[i] = ((NeighboringCellInfo) list.get(i)).getCid() - 415500000;
            JSONObject jsnData = new JSONObject();
            try {
                jsnData.put("cell_id", this.mCid[i]);
                jsnData.put("location_area_code", this.mLac[i]);
                jsnData.put("mobile_country_code", this.mMCC);
                jsnData.put("mobile_network_code", this.mMNC);
                jsnData.put("age", 0);
                jsnArray.put(jsnData);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jsnArray;
    }

    public JSONArray getCellInfoCurrentForTest() {
        CommonMethod.logDebug("antilost", "AntiLostLocation::getCellInfoCurrentForTest");
        GsmCellLocation gsmCellLocation = (GsmCellLocation) this.mTM.getCellLocation();
        int size = this.mTM.getNeighboringCellInfo().size();
        if (2 <= 0) {
            return null;
        }
        this.mLac = new int[2];
        this.mCid = new int[2];
        this.mLac[0] = 4257;
        this.mMCC = 460;
        this.mMNC = 0;
        this.mCid[0] = -136507493;
        this.mCid[1] = -123924659;
        JSONArray jsnArray = new JSONArray();
        for (int i = 0; i < 2; i++) {
            this.mLac[i] = this.mLac[0];
            JSONObject jsnData = new JSONObject();
            try {
                jsnData.put("cell_id", this.mCid[i]);
                jsnData.put("location_area_code", this.mLac[i]);
                jsnData.put("mobile_country_code", this.mMCC);
                jsnData.put("mobile_network_code", this.mMNC);
                jsnData.put("age", 0);
                jsnArray.put(jsnData);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jsnArray;
    }
}
