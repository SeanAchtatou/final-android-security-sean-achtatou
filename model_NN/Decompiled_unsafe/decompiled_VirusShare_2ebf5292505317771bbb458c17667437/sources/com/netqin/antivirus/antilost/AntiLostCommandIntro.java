package com.netqin.antivirus.antilost;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import com.nqmobile.antivirus_ampro20.R;

public class AntiLostCommandIntro extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.antilost_command_introl);
        setRequestedOrientation(1);
        ((TextView) findViewById(R.id.activity_name)).setText((int) R.string.help_plain);
    }
}
