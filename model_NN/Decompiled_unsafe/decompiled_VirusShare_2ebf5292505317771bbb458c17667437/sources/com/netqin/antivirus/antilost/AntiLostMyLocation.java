package com.netqin.antivirus.antilost;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.widget.Toast;
import com.netqin.antivirus.common.CommonMethod;
import com.nqmobile.antivirus_ampro20.R;

public class AntiLostMyLocation extends Activity implements AntiLostGPSNotify {
    /* access modifiers changed from: private */
    public Boolean isCurrentGPSandSIMEmpty = false;
    private AntiLostLocation[] mLocationListeners = null;
    private LocationManager mLocationManager = null;
    private TelephonyManager mTelephoneManager = null;

    public void onCreate(Bundle savedInstanceState) {
        String str;
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        boolean isForTry = false;
        ComponentName calling = getCallingActivity();
        if (calling != null && calling.getClassName().toLowerCase().indexOf("AntiLostGuide".toLowerCase()) > 0) {
            isForTry = true;
        } else if (calling != null && calling.getClassName().toLowerCase().indexOf("AntiLostRemoteIntro".toLowerCase()) > 0) {
            isForTry = true;
        }
        this.mTelephoneManager = (TelephonyManager) getSystemService("phone");
        this.mLocationManager = (LocationManager) getSystemService("location");
        this.mLocationListeners = new AntiLostLocation[]{new AntiLostLocation(this.mTelephoneManager, this.mLocationManager, "gps", this), new AntiLostLocation(this.mTelephoneManager, this.mLocationManager, "network", this)};
        startReceivingLocationUpdates();
        DialogInterface.OnClickListener listenerYes = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                AntiLostMyLocation.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(AntiLostMyLocation.this.getLocationLUrl())));
                if (AntiLostMyLocation.this.isCurrentGPSandSIMEmpty.booleanValue()) {
                    Toast toast = Toast.makeText(AntiLostMyLocation.this, (int) R.string.text_antilost_locate_gps_empty, 0);
                    toast.setGravity(81, 0, 100);
                    toast.show();
                }
                AntiLostMyLocation.this.finish();
            }
        };
        DialogInterface.OnClickListener listenerNo = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                AntiLostMyLocation.this.stopReceivingLocationUpdates();
                AntiLostMyLocation.this.finish();
            }
        };
        if (isForTry) {
            str = getResources().getString(R.string.text_antilost_guide_try_locate_query);
        } else {
            str = getResources().getString(R.string.text_antilost_myloction_query);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.text_antilost_guide_try_button_locate);
        builder.setMessage(str);
        builder.setPositiveButton((int) R.string.label_ok, listenerYes);
        builder.setNegativeButton((int) R.string.label_cancel, listenerNo);
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface arg0) {
                AntiLostMyLocation.this.stopReceivingLocationUpdates();
                AntiLostMyLocation.this.finish();
            }
        });
        builder.show();
    }

    public void onStart() {
        super.onStart();
    }

    public void onDestroy() {
        stopReceivingLocationUpdates();
        super.onDestroy();
    }

    private void startReceivingLocationUpdates() {
        if (this.mLocationManager != null) {
            try {
                this.mLocationManager.requestLocationUpdates("gps", 1200000, 0.0f, this.mLocationListeners[0]);
            } catch (IllegalArgumentException | SecurityException e) {
            }
            try {
                this.mLocationManager.requestLocationUpdates("network", 1200000, 0.0f, this.mLocationListeners[1]);
            } catch (IllegalArgumentException | SecurityException e2) {
            }
        }
    }

    /* access modifiers changed from: private */
    public void stopReceivingLocationUpdates() {
        if (this.mLocationManager != null) {
            for (int i = 0; i < this.mLocationListeners.length; i++) {
                try {
                    this.mLocationManager.removeUpdates(this.mLocationListeners[i]);
                } catch (Exception e) {
                }
            }
        }
    }

    private Location getGPSLocation() {
        for (AntiLostLocation locationCurrent : this.mLocationListeners) {
            Location loc = locationCurrent.getLocationCurrent();
            if (loc != null) {
                return loc;
            }
        }
        return null;
    }

    /* access modifiers changed from: private */
    public String getLocationLUrl() {
        String content;
        double lat = 0.0d;
        double lon = 0.0d;
        Location loc = getGPSLocation();
        if (loc != null) {
            lat = loc.getLatitude();
            lon = loc.getLongitude();
        }
        TelephonyManager tm = (TelephonyManager) getSystemService("phone");
        String country = "";
        int cellid = 0;
        int area = 0;
        int sid = 0;
        int nid = 0;
        int bid = 0;
        String mnc = tm.getNetworkOperator();
        if (mnc != null && mnc.length() > 2) {
            country = mnc.substring(0, 3);
            mnc = mnc.substring(3);
        }
        if (tm.getNetworkType() == 4 || tm.getPhoneType() == 2) {
            CdmaCellLocation location = (CdmaCellLocation) tm.getCellLocation();
            bid = location.getBaseStationId();
            nid = location.getNetworkId();
            sid = location.getSystemId();
        } else {
            GsmCellLocation location2 = (GsmCellLocation) tm.getCellLocation();
            if (location2 != null) {
                cellid = location2.getCid();
                area = location2.getLac();
            }
        }
        if (!(lat == 0.0d && lon == 0.0d)) {
            lat = ((double) ((int) (10000.0d * lat))) / 10000.0d;
            lon = ((double) ((int) (10000.0d * lon))) / 10000.0d;
        }
        if (tm.getNetworkType() == 4 || tm.getPhoneType() == 2) {
            String q = String.valueOf(country) + "," + nid + "," + sid + "," + bid + "," + "-60" + "," + "5555" + "," + lat + "," + lon;
            content = "http://my.netqin.com/m/cdma/?q=" + q + "&ck=" + CommonMethod.encryptMD5(q);
        } else {
            String q2 = String.valueOf(country) + "," + mnc + "," + area + "," + cellid + "," + lat + "," + lon;
            content = "http://my.netqin.com/m/?q=" + q2 + "&ck=" + CommonMethod.encryptMD5(q2);
        }
        if (lat == 0.0d && lon == 0.0d && area == 0 && cellid == 0) {
            this.isCurrentGPSandSIMEmpty = 1;
        } else {
            this.isCurrentGPSandSIMEmpty = null;
        }
        return content;
    }

    public void GPSLocateFinish() {
        stopReceivingLocationUpdates();
    }
}
