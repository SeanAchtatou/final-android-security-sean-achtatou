package com.netqin.antivirus.antilost;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.telephony.SmsMessage;
import android.text.TextUtils;
import com.netqin.antivirus.cloud.model.xml.XmlUtils;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.common.CommonUtils;
import com.nqmobile.antivirus_ampro20.R;

public class AntiLostReceiver extends BroadcastReceiver {
    private static final String ACT_BOOT = "android.intent.action.BOOT_COMPLETED";
    private static final String ACT_SMS_REC = "android.provider.Telephony.SMS_RECEIVED";
    public static final String StarS = "@";

    public void onReceive(Context context, Intent intent) {
        int msgIndex;
        String sms;
        SharedPreferences spf = context.getSharedPreferences("nq_antilost", 0);
        String password = CommonUtils.getConfigWithStringValue(context, "nq_antilost", "password", "");
        String action = intent.getAction();
        if (action.equals(ACT_BOOT)) {
            if (spf.getBoolean("start", false) || spf.getBoolean("lock", false)) {
                Intent intent2 = new Intent(context, AntiLostService.class);
                String user = spf.getString("usermsg", "");
                CommonMethod.logDebug("antilost", "boot up user msg: " + user);
                if (!TextUtils.isEmpty(user)) {
                    intent2.putExtra("usermsg", user);
                }
                context.startService(intent2);
            }
            if (spf.getBoolean("changesim_sendsms", false)) {
                String imsi = CommonMethod.getIMSI(context);
                String imsiPre = CommonUtils.getConfigWithStringValue(context, "nq_antilost", XmlUtils.LABEL_MOBILEINFO_IMSI, "");
                CommonMethod.logDebug("changesim_sendsms", "new imsi: " + imsi);
                CommonMethod.logDebug("changesim_sendsms", "old imsi: " + imsiPre);
                if (!imsi.matches(imsiPre) && !imsi.equalsIgnoreCase("000000000000000") && !imsiPre.equalsIgnoreCase("000000000000000")) {
                    CommonMethod.logDebug("changesim_sendsms", "start AntiLostSmsReportService");
                    context.startService(new Intent(context, AntiLostSmsReportService.class));
                }
            }
        } else if (action.equals(ACT_SMS_REC) && !TextUtils.isEmpty(password) && spf.getBoolean("start", false)) {
            try {
                Object[] pdus = (Object[]) intent.getExtras().get("pdus");
                for (int i = 0; i < pdus.length; i++) {
                    SmsMessage msg = SmsMessage.createFromPdu((byte[]) pdus[i]);
                    String text = msg.getDisplayMessageBody().toLowerCase().trim();
                    String number = msg.getDisplayOriginatingAddress();
                    if (!TextUtils.isEmpty(text)) {
                        CommonMethod.logDebug("antilost", "sms text: " + text);
                        int antilostType = 0;
                        String commandString = getCommandString(text, "locate*");
                        if (!TextUtils.isEmpty(commandString)) {
                            antilostType = 2;
                        } else {
                            commandString = getCommandString(text, "alarm*");
                            if (!TextUtils.isEmpty(commandString)) {
                                antilostType = 3;
                            } else {
                                commandString = getCommandString(text, "lock*");
                                if (!TextUtils.isEmpty(commandString)) {
                                    antilostType = 1;
                                } else {
                                    commandString = getCommandString(text, "delete*");
                                    if (!TextUtils.isEmpty(commandString)) {
                                        antilostType = 4;
                                    }
                                }
                            }
                        }
                        if (TextUtils.isEmpty(commandString) && text.indexOf("reset*") >= 0) {
                            String securityNum = CommonUtils.getConfigWithStringValue(context, "nq_antilost", "securitynum", "");
                            if (number.endsWith(securityNum) || securityNum.endsWith(number)) {
                                antilostType = 5;
                                commandString = "reset*" + password + "*";
                            }
                        }
                        CommonMethod.logDebug("antilost", "command string: " + commandString);
                        if (!TextUtils.isEmpty(commandString)) {
                            int index1 = commandString.indexOf("*");
                            int index2 = commandString.indexOf("*", index1 + 1);
                            CommonMethod.logDebug("antilost", "command 1: " + index1);
                            CommonMethod.logDebug("antilost", "command 2: " + index2);
                            if (index2 > index1) {
                                boolean isMember = CommonMethod.getIsMember(context);
                                String smsPass = commandString.substring(index1 + 1, index2);
                                CommonMethod.logDebug("antilost", "oriPass: " + password);
                                CommonMethod.logDebug("antilost", "smsPass: " + smsPass);
                                if (!smsPass.equals(password) || antilostType <= 0) {
                                    if (antilostType == 1) {
                                        sms = context.getResources().getString(R.string.text_antilost_reply_lock);
                                    } else if (antilostType == 2) {
                                        sms = context.getResources().getString(R.string.text_antilost_reply_locate);
                                    } else if (antilostType == 3) {
                                        sms = context.getResources().getString(R.string.text_antilost_reply_alarm);
                                    } else if (antilostType == 4) {
                                        sms = context.getResources().getString(R.string.text_antilost_reply_delete);
                                    } else {
                                        return;
                                    }
                                    String sms2 = String.valueOf(sms) + "\n" + context.getString(R.string.text_antilost_reply_fail);
                                    CommonMethod.logDebug("antilost", "reply error sms" + sms2);
                                    SmsHandler.sendSms(number, sms2, null);
                                    antilostType = 6;
                                } else if (!isMember && (antilostType == 1 || antilostType == 3 || antilostType == 4)) {
                                    String sms3 = context.getString(R.string.text_antilost_reply_notmember);
                                    CommonMethod.logDebug("antilost", "reply error sms" + sms3);
                                    SmsHandler.sendSms(number, sms3, null);
                                    antilostType = 6;
                                }
                                String user2 = "";
                                if ((antilostType == 1 || antilostType == 3) && (msgIndex = text.indexOf(commandString)) >= 0) {
                                    user2 = text.substring(commandString.length() + msgIndex);
                                }
                                CommonMethod.logDebug("antilost", "write to spf user msg: " + user2);
                                spf.edit().putString("usermsg", user2).commit();
                                abortBroadcast();
                                Intent intent3 = new Intent(context, AntiLostService.class);
                                intent3.putExtra("phonenum", number);
                                intent3.putExtra("smstype", antilostType);
                                intent3.putExtra("todeletestring", commandString);
                                if (!TextUtils.isEmpty(user2)) {
                                    intent3.putExtra("usermsg", user2);
                                }
                                context.startService(intent3);
                            } else {
                                continue;
                            }
                        } else {
                            continue;
                        }
                    }
                }
            } catch (Exception e) {
            }
        }
    }

    private String getCommandString(String smsText, String commmandText) {
        int commandIndex2 = -1;
        String commandString = "";
        int commandIndex1 = smsText.indexOf(commmandText);
        if (commandIndex1 >= 0) {
            commandIndex2 = smsText.indexOf("*", commmandText.length() + commandIndex1);
        }
        if (commandIndex2 > commandIndex1) {
            commandString = smsText.substring(commandIndex1, commandIndex2 + 1);
        }
        CommonMethod.logDebug("antilost", "getCommandIndex 1: " + commandIndex1);
        CommonMethod.logDebug("antilost", "getCommandIndex 2: " + commandIndex2);
        return commandString;
    }
}
