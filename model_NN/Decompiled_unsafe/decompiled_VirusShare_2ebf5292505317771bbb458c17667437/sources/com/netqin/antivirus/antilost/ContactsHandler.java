package com.netqin.antivirus.antilost;

import android.app.Activity;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;
import android.provider.ContactsContract;
import com.netqin.antivirus.NqUtil;
import java.util.ArrayList;
import java.util.Vector;

public class ContactsHandler {
    public static final String NAME = "display_name";
    public static final String PHONE_CONTACT_ID = "contact_id";
    public static final String PHONE_NUMBER = "data1";
    private static Uri mContactUri = ContactsContract.Contacts.CONTENT_URI;
    private ContentResolver mContent;

    public ContactsHandler(Context context) {
        this.mContent = context.getContentResolver();
    }

    public Cursor getContactById(long _id) {
        return this.mContent.query(ContentUris.withAppendedId(mContactUri, _id), null, null, null, null);
    }

    public long getRawContactById(long id) {
        Cursor cursor = this.mContent.query(ContentUris.withAppendedId(ContactsContract.RawContacts.CONTENT_URI, id), null, null, null, null);
        if (cursor == null) {
            return -1;
        }
        cursor.moveToFirst();
        return cursor.getLong(cursor.getColumnIndex(PHONE_CONTACT_ID));
    }

    public Cursor getPhoneByPhoneId(long _id) {
        Cursor c = this.mContent.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, new String[]{SmsHandler.ROWID, NAME, PHONE_NUMBER, PHONE_CONTACT_ID}, "_id=_id", null, null);
        if (c != null) {
            c.moveToFirst();
        }
        return c;
    }

    public String getContactsNameByPhone(String phone) {
        Cursor c = this.mContent.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, new String[]{SmsHandler.ROWID, NAME, PHONE_NUMBER, PHONE_CONTACT_ID}, "data1 LIKE '%" + NqUtil.getLastEleven(phone) + "'", null, null);
        if (c.getCount() > 0) {
            c.moveToFirst();
            String name = c.getString(c.getColumnIndex(NAME));
            c.close();
            return name;
        }
        c.close();
        return null;
    }

    public Cursor getPhoneCursorByNumber(String phone) {
        return this.mContent.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, new String[]{SmsHandler.ROWID, NAME, PHONE_NUMBER, PHONE_CONTACT_ID}, "data1=" + phone, null, null);
    }

    public Cursor getPhonesOfContact(long people_id) {
        return this.mContent.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, "contact_id=" + people_id, null, null);
    }

    /* JADX INFO: finally extract failed */
    public boolean deleteContactPhones(long people_id) {
        Cursor c = this.mContent.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, "contact_id=" + people_id, null, null);
        int i = 0;
        while (i < c.getCount()) {
            try {
                c.moveToPosition(i);
                this.mContent.delete(ContentUris.withAppendedId(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, c.getLong(c.getColumnIndex(SmsHandler.ROWID))), null, null);
                i++;
            } catch (Throwable th) {
                c.close();
                throw th;
            }
        }
        c.close();
        c.close();
        return true;
    }

    public boolean deleteContact(long _id) {
        if (this.mContent.delete(ContentUris.withAppendedId(mContactUri, _id), null, null) > 0) {
            return true;
        }
        return false;
    }

    public int deleteAllContact() {
        return this.mContent.delete(mContactUri, null, null);
    }

    public int deleteAllContact3() {
        return this.mContent.delete(Uri.parse("content://com.android.contacts/raw_contacts"), null, null);
    }

    public void deleteAllContact2() {
        Cursor people = this.mContent.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        while (people.moveToNext()) {
            deleteContact(people.getLong(people.getColumnIndex(SmsHandler.ROWID)));
        }
        people.close();
    }

    public Uri addContact(String name, String mobilePhone) {
        ArrayList<ContentProviderOperation> ops = new ArrayList<>();
        int rawContactInsertIndex = ops.size();
        ops.add(ContentProviderOperation.newInsert(ContactsContract.Contacts.CONTENT_URI).withValue(NAME, name).build());
        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI).withValueBackReference(PHONE_CONTACT_ID, rawContactInsertIndex).withValue("mimetype", "vnd.android.cursor.item/phone_v2").withValue("data2", 2).withValue(PHONE_NUMBER, mobilePhone).build());
        try {
            return this.mContent.applyBatch("com.android.contacts", ops)[0].uri;
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (OperationApplicationException e2) {
            e2.printStackTrace();
        }
        return null;
    }

    public boolean updateContactPhone(long phone_id, String sNumber) {
        ContentValues values = new ContentValues();
        values.put(PHONE_NUMBER, sNumber);
        return this.mContent.update(ContentUris.withAppendedId(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, phone_id), values, null, null) > 0;
    }

    public boolean isNumberInContact(long people_id, String sNumber) {
        return this.mContent.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, new StringBuilder("data1=").append(sNumber).append("AND").append(PHONE_CONTACT_ID).append("=").append(Long.toString(people_id)).toString(), null, null).getCount() > 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public Uri addContactPhone(long _id, long _rawContactId, String sNumber, int type, String label, int isPrimary) {
        ContentValues values = new ContentValues();
        values.put(PHONE_NUMBER, sNumber);
        values.put("mimetype", "vnd.android.cursor.item/phone_v2");
        values.put("data2", Integer.valueOf(type));
        values.put("data3", label);
        values.put("raw_contact_id", Long.toString(_rawContactId));
        if (isPrimary > 0) {
            values.put("is_primary", (Integer) 1);
        } else {
            values.put("is_primary", (Integer) 0);
        }
        return this.mContent.insert(ContactsContract.Data.CONTENT_URI, values);
    }

    public boolean updateContactName(long _id, String sName) {
        ContentValues values = new ContentValues();
        values.put(NAME, sName);
        return this.mContent.update(ContentUris.withAppendedId(mContactUri, _id), values, null, null) > 0;
    }

    public Vector<String> getPhoneNumFromCursor(Cursor c) {
        Vector<String> phoneNumber = new Vector<>(2);
        if (c.getCount() > 0) {
            for (int a = 0; a < c.getCount(); a++) {
                c.moveToPosition(a);
                phoneNumber.add(c.getString(c.getColumnIndex(PHONE_NUMBER)));
            }
        }
        return phoneNumber;
    }

    public static void pickContact(Activity act, int tag) {
        Intent intent = new Intent("android.intent.action.PICK");
        intent.setType("vnd.android.cursor.dir/contact");
        act.startActivityForResult(intent, tag);
    }

    public long getPeapleIdFromCursor(Cursor c) {
        int id = -1;
        if (c.getCount() > 0) {
            c.moveToFirst();
            id = c.getInt(c.getColumnIndex(SmsHandler.ROWID));
        }
        return (long) id;
    }
}
