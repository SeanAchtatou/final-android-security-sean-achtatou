package com.netqin.antivirus.antilost;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.netqin.antivirus.NqUtil;
import com.netqin.antivirus.appprotocol.AppConstantValue;
import com.netqin.antivirus.appprotocol.AppRequest;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.common.CommonUtils;
import com.netqin.antivirus.log.LogEngine;
import com.netqin.antivirus.softsetting.AntiLostSetting;
import com.nqmobile.antivirus_ampro20.R;

public class AntiLostMain extends Activity {
    private boolean isOpenAntiLostNew = false;
    /* access modifiers changed from: private */
    public boolean isOpenAntiLostOld = false;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.antilost_main);
        setRequestedOrientation(1);
        ((TextView) findViewById(R.id.activity_name)).setText((int) R.string.act_name_antilost);
        findViewById(R.id.antilost_main_member).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AppRequest.StartSubscribe(AntiLostMain.this, false, AppConstantValue.subscribeFromAntiLost);
            }
        });
        findViewById(R.id.antilost_main_block_line1).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AntiLostMain.this.clickRomoteIntro(5);
            }
        });
        findViewById(R.id.antilost_main_block_line2).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AntiLostMain.this.clickRomoteIntro(1);
            }
        });
        findViewById(R.id.antilost_main_block_line4).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AntiLostMain.this.clickRomoteIntro(3);
            }
        });
        findViewById(R.id.antilost_main_block_line5).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AntiLostMain.this.clickRomoteIntro(2);
            }
        });
        findViewById(R.id.antilost_main_block_line6).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AntiLostMain.this.clickRomoteIntro(4);
            }
        });
        findViewById(R.id.antilost_main_how_to).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AntiLostMain.this.startActivity(new Intent(AntiLostMain.this, AntiLostCommandIntro.class));
            }
        });
        findViewById(R.id.antilost_main_open_btn).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AntiLostMain.this.openOrCloseAntilost();
            }
        });
        this.isOpenAntiLostOld = getSharedPreferences("nq_antilost", 0).getBoolean("running", false);
        setAntiLostUiState();
    }

    public void onResume() {
        setAntiLostUiState();
        if (CommonMethod.getIsMember(this)) {
            findViewById(R.id.antilost_main_member).setVisibility(4);
            ((TextView) findViewById(R.id.antilost_main_block_line3_txt)).setText(getString(R.string.antilost_enjoy_member));
            ((ImageView) findViewById(R.id.antilost_main_block_line4_img)).setImageResource(R.drawable.antilost_main_alarm);
            ((ImageView) findViewById(R.id.antilost_main_block_line5_img)).setImageResource(R.drawable.antilost_main_lock);
            ((ImageView) findViewById(R.id.antilost_main_block_line6_img)).setImageResource(R.drawable.antilost_main_wipe);
        } else {
            ((ImageView) findViewById(R.id.antilost_main_block_line4_img)).setImageResource(R.drawable.antilost_main_alarm_notmember);
            ((ImageView) findViewById(R.id.antilost_main_block_line5_img)).setImageResource(R.drawable.antilost_main_lock_notmember);
            ((ImageView) findViewById(R.id.antilost_main_block_line6_img)).setImageResource(R.drawable.antilost_main_wipe_notmember);
        }
        super.onResume();
    }

    public void onPause() {
        super.onPause();
    }

    public void onStop() {
        super.onStop();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: private */
    public void clickRomoteIntro(int type) {
        Intent intent = new Intent(this, AntiLostRemoteIntro.class);
        intent.putExtra("type", type);
        startActivity(intent);
    }

    /* access modifiers changed from: package-private */
    public void setAntiLostUiState() {
        boolean isNowStart = getSharedPreferences("nq_antilost", 0).getBoolean("running", false);
        this.isOpenAntiLostNew = isNowStart;
        ViewGroup img = (ViewGroup) findViewById(R.id.antilost_main_state);
        TextView txt = (TextView) findViewById(R.id.antilost_main_open_btn_txt);
        if (isNowStart) {
            img.setBackgroundResource(R.drawable.antilost_main_state_on);
            txt.setText((int) R.string.antilost_main_close);
        } else {
            this.isOpenAntiLostOld = false;
            img.setBackgroundResource(R.drawable.antilost_main_state_off);
            txt.setText((int) R.string.antilost_main_open);
        }
        if (!CommonMethod.getIsMember(this) && !this.isOpenAntiLostOld && this.isOpenAntiLostNew) {
            this.isOpenAntiLostOld = true;
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(getString(R.string.antilost_guidetomember_title));
            builder.setMessage(getString(R.string.antilost_guidetomember_desc));
            builder.setPositiveButton(getString(R.string.label_upgrade), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                    AppRequest.StartSubscribe(AntiLostMain.this, false, AppConstantValue.subscribeFromAntiLost);
                }
            });
            builder.setNegativeButton(getString(R.string.label_cancel), (DialogInterface.OnClickListener) null);
            builder.show();
        }
    }

    /* access modifiers changed from: private */
    public void openOrCloseAntilost() {
        final SharedPreferences spf = getSharedPreferences("nq_antilost", 0);
        if (!spf.getBoolean("running", false)) {
            String pwd = CommonUtils.getConfigWithStringValue(this, "nq_antilost", "password", "");
            String securityNum = CommonUtils.getConfigWithStringValue(this, "nq_antilost", "securitynum", "");
            if (TextUtils.isEmpty(pwd)) {
                Intent i = new Intent(this, AntiLostSetPwd.class);
                i.putExtra("type", 1);
                startActivity(i);
            } else if (TextUtils.isEmpty(securityNum)) {
                Intent i2 = new Intent(this, AntiLostSetSecurityNum.class);
                i2.putExtra("type", 1);
                startActivity(i2);
            } else {
                CommonMethod.showNotification(this, getResources().getString(R.string.text_antilost_open_done));
                spf.edit().putBoolean("start", true).commit();
                spf.edit().putBoolean("running", true).commit();
                spf.edit().putBoolean("changesim_sendsms", true).commit();
                startService(new Intent(this, AntiLostService.class));
                setAntiLostUiState();
                LogEngine.insertOperationItemLog(11, "", getFilesDir().getPath());
            }
        } else {
            CommonMethod.yesNoBtnDialogListen(this, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    spf.edit().putBoolean("start", false).commit();
                    spf.edit().putBoolean("running", false).commit();
                    spf.edit().putBoolean("changesim_sendsms", false).commit();
                    AntiLostMain.this.stopService(new Intent(AntiLostMain.this, AntiLostService.class));
                    LogEngine.insertOperationItemLog(12, "", AntiLostMain.this.getFilesDir().getPath());
                    AntiLostMain.this.isOpenAntiLostOld = false;
                    AntiLostMain.this.setAntiLostUiState();
                }
            }, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            }, getResources().getString(R.string.text_antilost_close_tip), R.string.text_antilost_close_title);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.antilost_switch, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.antilost_swotch_menuitem_setting /*2131558848*/:
                startActivity(new Intent(this, AntiLostSetting.class));
                break;
            case R.id.antilost_swotch_menuitem_command /*2131558849*/:
                startActivity(new Intent(this, AntiLostCommandIntro.class));
                break;
            case R.id.antilost_swotch_menuitem_advice_feedback /*2131558850*/:
                NqUtil.clickAdviceFeedback(this);
                break;
            default:
                return false;
        }
        return true;
    }
}
