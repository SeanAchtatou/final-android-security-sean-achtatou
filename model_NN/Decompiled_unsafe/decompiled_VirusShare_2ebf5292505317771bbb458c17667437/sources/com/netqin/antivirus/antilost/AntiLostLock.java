package com.netqin.antivirus.antilost;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.netqin.antivirus.common.CommonUtils;
import com.nqmobile.antivirus_ampro20.R;

public class AntiLostLock extends Activity {
    boolean isPasswordOk = false;
    TextView userTxt;

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 84 || keyCode == 24 || keyCode == 4 || keyCode == 25) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == 84 || keyCode == 24 || keyCode == 4 || keyCode == 25) {
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (!this.isPasswordOk) {
            Intent intent = new Intent(this, AntiLostLock.class);
            intent.putExtra("usermsg", this.userTxt.getText());
            startActivity(intent);
            finish();
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!getSharedPreferences("nq_antilost", 0).getBoolean("lock", false)) {
            finish();
            return;
        }
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        setContentView((int) R.layout.antilost_lock);
        ((Button) findViewById(R.id.antilost_lock_ok)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AntiLostLock.this.clickBtnOk();
            }
        });
        this.userTxt = (TextView) findViewById(R.id.antilost_lock_user_msg);
        String userMsg = getIntent().getStringExtra("usermsg");
        if (userMsg != null && userMsg.length() > 0) {
            this.userTxt.setText(userMsg);
        } else if (savedInstanceState != null) {
            this.userTxt.setText(savedInstanceState.getString("UserMsg"));
        }
    }

    public void onSaveInstanceState(Bundle savedInstanceState) {
        String msg = (String) this.userTxt.getText();
        if (msg != null && msg.length() > 0) {
            savedInstanceState.putString("UserMsg", msg);
        }
        super.onSaveInstanceState(savedInstanceState);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        String msg = savedInstanceState.getString("UserMsg");
        if (msg != null && msg.length() > 0) {
            this.userTxt.setText(msg);
        }
    }

    /* access modifiers changed from: private */
    public void clickBtnOk() {
        String pwd = ((EditText) findViewById(R.id.antilost_lock_password)).getText().toString();
        SharedPreferences spf = getSharedPreferences("nq_antilost", 0);
        String org2 = CommonUtils.getConfigWithStringValue(this, "nq_antilost", "password", "");
        if (TextUtils.isEmpty(pwd) || !pwd.equals(org2)) {
            Toast toast = Toast.makeText(this, (int) R.string.text_password_error, 0);
            toast.setGravity(81, 0, 100);
            toast.show();
            return;
        }
        spf.edit().putBoolean("lock", false).commit();
        spf.edit().putBoolean("alarm", false).commit();
        this.isPasswordOk = true;
        startService(new Intent(this, AntiLostService.class));
        finish();
    }
}
