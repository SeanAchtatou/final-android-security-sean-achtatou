package com.netqin.antivirus.antilost;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.common.CommonUtils;
import com.netqin.antivirus.softsetting.AntiLostSetting;
import com.nqmobile.antivirus_ampro20.R;

public class AntiLostPassword extends Activity implements TextWatcher {
    private View mBtnOk;
    private EditText mETPassword;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.antilost_password);
        ((TextView) findViewById(R.id.activity_name)).setText((int) R.string.act_name_antilost);
        this.mETPassword = (EditText) findViewById(R.id.antilost_password_pwd);
        this.mETPassword.addTextChangedListener(this);
        this.mBtnOk = findViewById(R.id.antilost_password_ok);
        this.mBtnOk.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AntiLostPassword.this.clickOk();
            }
        });
        findViewById(R.id.antilost_password_cancel).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AntiLostPassword.this.clickCancel();
            }
        });
        findViewById(R.id.antilost_forget).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AntiLostPassword.this.clickForget();
            }
        });
    }

    /* access modifiers changed from: private */
    public void clickOk() {
        SharedPreferences spf = getSharedPreferences("nq_antilost", 0);
        if (CommonUtils.getConfigWithStringValue(this, "nq_antilost", "password", "").compareTo(this.mETPassword.getText().toString()) != 0) {
            CommonMethod.messageDialog(this, getString(R.string.text_antilost_pwd_wrong), (int) R.string.label_netqin_antivirus);
            return;
        }
        spf.edit().putBoolean("alarm", false).commit();
        if ("AntiVirusSetting".equalsIgnoreCase(getIntent().getStringExtra("tag"))) {
            startActivity(new Intent(this, AntiLostSetting.class));
        } else {
            startActivity(new Intent(this, AntiLostMain.class));
        }
        finish();
    }

    /* access modifiers changed from: private */
    public void clickCancel() {
        finish();
    }

    public void afterTextChanged(Editable s) {
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {
        String pwd = this.mETPassword.getText().toString();
        if (TextUtils.isEmpty(pwd) || pwd.length() < 6 || pwd.length() > 10) {
            this.mBtnOk.setEnabled(false);
        } else {
            this.mBtnOk.setEnabled(true);
        }
    }

    /* access modifiers changed from: private */
    public void clickForget() {
        startActivity(new Intent(this, AntiLostForgetPassword.class));
    }
}
