package com.netqin.antivirus.antilost;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.IBinder;
import android.telephony.ServiceState;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.netqin.antivirus.cloud.model.xml.XmlUtils;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.common.CommonUtils;
import com.nqmobile.antivirus_ampro20.R;

public class AntiLostSmsReportService extends Service {
    private final IBinder mBinder = new AntiLostServiceBinder();
    private ServiceState mServiceState = null;
    /* access modifiers changed from: private */
    public TelephonyManager mTelephoneManager = null;

    public class AntiLostServiceBinder extends Binder {
        public AntiLostServiceBinder() {
        }

        /* access modifiers changed from: package-private */
        public AntiLostSmsReportService getService() {
            return AntiLostSmsReportService.this;
        }
    }

    public IBinder onBind(Intent arg0) {
        return this.mBinder;
    }

    public void onCreate() {
        super.onCreate();
        this.mTelephoneManager = (TelephonyManager) getSystemService("phone");
    }

    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        if (intent != null) {
            CommonMethod.logDebug("AntiLostSmsReportService", "onStart");
            Thread thread = new Thread(new Runnable() {
                public void run() {
                    String str;
                    int i = 0;
                    boolean isInService = false;
                    while (!isInService && i < 15) {
                        try {
                            CommonMethod.logDebug("AntiLostSmsReportService", "isInService: " + isInService + " i: " + i);
                            if (AntiLostSmsReportService.this.mTelephoneManager.getSimState() == 5) {
                                CommonMethod.logDebug("AntiLostSmsReportService", "TelephonyManager.SIM_STATE_READY");
                                isInService = true;
                                SharedPreferences sharedPreferences = AntiLostSmsReportService.this.getSharedPreferences("nq_antilost", 0);
                                String imsi = CommonMethod.getIMSI(AntiLostSmsReportService.this);
                                if (!imsi.matches(CommonUtils.getConfigWithStringValue(AntiLostSmsReportService.this, "nq_antilost", XmlUtils.LABEL_MOBILEINFO_IMSI, "")) || imsi.equalsIgnoreCase("000000000000000")) {
                                    CommonMethod.logDebug("AntiLostSmsReportService", "different imsi");
                                    String securityNum = CommonUtils.getConfigWithStringValue(AntiLostSmsReportService.this, "nq_antilost", "securitynum", "");
                                    CommonMethod.logDebug("AntiLostSmsReportService", "security phone number: " + securityNum);
                                    String smsUserName = AntiLostSmsReportService.this.getSharedPreferences("netqin", 0).getString("antilost_sms_change_content_name", "");
                                    if (TextUtils.isEmpty(smsUserName)) {
                                        smsUserName = AntiLostSmsReportService.this.getString(R.string.label_netqin_antivirus);
                                    }
                                    if (CommonMethod.isLocalSimpleChinese()) {
                                        str = String.valueOf(AntiLostSmsReportService.this.getString(R.string.antilost_sms_change_content2, new Object[]{smsUserName})) + "?l=zh_cn";
                                    } else {
                                        str = String.valueOf(AntiLostSmsReportService.this.getString(R.string.antilost_sms_change_content2, new Object[]{smsUserName})) + "?l=en_en";
                                    }
                                    CommonMethod.logDebug("AntiLostSmsReportService", "before sleep");
                                    Thread.sleep(90000);
                                    CommonMethod.logDebug("AntiLostSmsReportService", "after sleep");
                                    CommonMethod.logDebug("AntiLostSmsReportService", "sendsms result: " + SmsHandler.sendSms(securityNum, str.toLowerCase(), null));
                                    CommonUtils.putConfigWithStringValue(AntiLostSmsReportService.this, "nq_antilost", XmlUtils.LABEL_MOBILEINFO_IMSI, imsi);
                                } else {
                                    AntiLostSmsReportService.this.stopSelf();
                                }
                            } else {
                                i++;
                                CommonMethod.logDebug("AntiLostSmsReportService", "sleep 20s");
                                Thread.sleep(20000);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    AntiLostSmsReportService.this.stopSelf();
                }
            });
            thread.setPriority(5);
            thread.start();
        }
    }

    public void onDestroy() {
        super.onDestroy();
    }
}
