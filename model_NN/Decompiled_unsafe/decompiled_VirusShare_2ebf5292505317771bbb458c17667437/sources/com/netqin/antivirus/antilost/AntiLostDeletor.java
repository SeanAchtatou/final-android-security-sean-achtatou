package com.netqin.antivirus.antilost;

import android.content.Context;
import java.io.File;

public class AntiLostDeletor {
    public static void deleteAllFile(String dirPath) {
        File dir = new File(dirPath);
        File[] files = dir.listFiles();
        if (files == null) {
            dir.delete();
            return;
        }
        for (int i = 0; i < files.length; i++) {
            if (files[i].isDirectory()) {
                deleteAllFile(files[i].getAbsolutePath());
                files[i].delete();
            } else {
                files[i].delete();
            }
        }
    }

    public static void deleteImgFile() {
        deleteAllFile("/sdcard/DCIM/");
    }

    public static void deleteAllContacts(Context context) {
        new ContactsHandler(context).deleteAllContact3();
    }

    public static void deleteAllSMS(Context context) {
        SmsHandler.getInstance(context).deleteAllSms();
    }

    public static void deleteAllMMS(Context context) {
        SmsHandler.getInstance(context).deleteAllMms();
    }

    public static void deleteAllCallLog(Context context) {
        new CallLogHandler(context).deleteAllCallLog();
    }
}
