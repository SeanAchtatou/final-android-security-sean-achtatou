package com.netqin.antivirus.antilost;

public class AntiLostCommon {
    public static final String DELETE_ALL = "all";
    public static final String DELETE_CALLLOG = "calllog";
    public static final String DELETE_CONTACT = "nq_contact";
    public static final String DELETE_MEDIA = "mediafile";
    public static final String DELETE_SMS = "sms";
    public static final int SMS_TYPE_ALARM = 3;
    public static final int SMS_TYPE_DELETE = 4;
    public static final int SMS_TYPE_DELETE_COMMAND_SMS = 6;
    public static final int SMS_TYPE_LOCATION = 2;
    public static final int SMS_TYPE_LOCK = 1;
    public static final int SMS_TYPE_RESET = 5;
    public static final int TYPE_OPEN_ANTILOST = 1;
}
