package com.netqin.antivirus.antilost;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.common.CommonUtils;
import com.nqmobile.antivirus_ampro20.R;

public class AntiLostChangePwd extends Activity implements TextWatcher {
    private View mBtnOk;
    private TextView mMax;
    private EditText mPwdCfm;
    private EditText mPwdNew;
    private EditText mPwdOld;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.antilost_changepwd);
        ((TextView) findViewById(R.id.activity_name)).setText((int) R.string.act_name_antilost);
        this.mPwdOld = (EditText) findViewById(R.id.antilost_changepwd_input_old);
        this.mPwdNew = (EditText) findViewById(R.id.antilost_changepwd_input_new);
        this.mPwdCfm = (EditText) findViewById(R.id.antilost_changepwd_confirm_new);
        this.mPwdOld.addTextChangedListener(this);
        this.mPwdNew.addTextChangedListener(this);
        this.mPwdCfm.addTextChangedListener(this);
        this.mMax = (TextView) findViewById(R.id.antilost_max_10);
        findViewById(R.id.antilost_changepwd_back).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AntiLostChangePwd.this.clickBack();
            }
        });
        this.mBtnOk = findViewById(R.id.antilost_changepwd_ok);
        this.mBtnOk.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AntiLostChangePwd.this.clickOk();
            }
        });
        this.mBtnOk.setEnabled(false);
    }

    /* access modifiers changed from: private */
    public void clickBack() {
        finish();
    }

    /* access modifiers changed from: private */
    public void clickOk() {
        String pwdOld = this.mPwdOld.getText().toString();
        String pwdNew = this.mPwdNew.getText().toString();
        String pwdCfm = this.mPwdCfm.getText().toString();
        if (!pwdOld.matches(CommonUtils.getConfigWithStringValue(this, "nq_antilost", "password", ""))) {
            CommonMethod.messageDialog(this, getString(R.string.text_change_pwd_old_wrong), (int) R.string.label_netqin_antivirus);
            this.mPwdOld.setText("");
        } else if (pwdNew.compareTo(pwdCfm) != 0) {
            CommonMethod.messageDialog(this, getString(R.string.text_antilost_setpassword_diff), (int) R.string.label_netqin_antivirus);
            this.mPwdCfm.setText("");
        } else {
            CommonUtils.putConfigWithStringValue(this, "nq_antilost", "password", pwdNew);
            finish();
            Toast toast = Toast.makeText(this, (int) R.string.text_antilost_setpwd_succ, 0);
            toast.setGravity(81, 0, 100);
            toast.show();
        }
    }

    public void afterTextChanged(Editable s) {
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {
        String pwdOld = this.mPwdOld.getText().toString();
        String pwdNew = this.mPwdNew.getText().toString();
        String pwdCfm = this.mPwdCfm.getText().toString();
        if (TextUtils.isEmpty(pwdOld) || TextUtils.isEmpty(pwdNew) || TextUtils.isEmpty(pwdCfm)) {
            this.mBtnOk.setEnabled(false);
        } else if (pwdOld.length() < 6 || pwdOld.length() > 10 || pwdNew.length() < 6 || pwdNew.length() > 10 || pwdCfm.length() < 6 || pwdCfm.length() > 10) {
            this.mBtnOk.setEnabled(false);
        } else {
            this.mBtnOk.setEnabled(true);
        }
        if (pwdNew.length() > 10) {
            this.mMax.setVisibility(0);
        } else {
            this.mMax.setVisibility(8);
        }
    }
}
