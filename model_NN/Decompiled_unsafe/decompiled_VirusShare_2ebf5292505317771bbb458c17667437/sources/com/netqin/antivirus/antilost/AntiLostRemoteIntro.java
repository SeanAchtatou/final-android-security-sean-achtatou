package com.netqin.antivirus.antilost;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.contact.vcard.VCardConfig;
import com.netqin.antivirus.softsetting.ChangeSMSContent;
import com.nqmobile.antivirus_ampro20.R;

public class AntiLostRemoteIntro extends Activity {
    private View mBtnTry;
    private int type = 0;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.type = getIntent().getIntExtra("type", 0);
        requestWindowFeature(1);
        setRequestedOrientation(1);
        if (this.type == 5) {
            setContentView((int) R.layout.antilost_remote_smsnotify);
        } else if (this.type == 1) {
            setContentView((int) R.layout.antilost_remote_locate);
        } else if (this.type == 4) {
            setContentView((int) R.layout.antilost_remote_erase);
        } else if (this.type == 2) {
            setContentView((int) R.layout.antilost_remote_lock);
        } else if (this.type == 3) {
            setContentView((int) R.layout.antilost_remote_alarm);
        } else {
            finish();
        }
        ((TextView) findViewById(R.id.activity_name)).setText((int) R.string.act_name_antilost);
        if (this.type != 4) {
            this.mBtnTry = findViewById(R.id.antilost_remote_btn);
            this.mBtnTry.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    AntiLostRemoteIntro.this.clickTry();
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void clickTry() {
        if (this.type == 1) {
            clickTryLocate();
        } else if (this.type == 2) {
            clickTryLock();
        } else if (this.type == 3) {
            clickTryAlarm();
        } else if (this.type == 5) {
            clickEditNotifyContent();
        }
    }

    private void clickEditNotifyContent() {
        startActivity(new Intent(this, ChangeSMSContent.class));
    }

    private void clickTryLocate() {
        if (CommonMethod.isGpsEnable(this) || CommonMethod.getIMSI(this).length() >= 2) {
            startActivity(new Intent(this, AntiLostMyLocation.class));
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.text_antilost_remote_locate_try);
        builder.setMessage((int) R.string.text_antilost_opengps_query_1);
        builder.setPositiveButton((int) R.string.text_antilost_guide_opengps_button, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent i = new Intent("android.settings.LOCATION_SOURCE_SETTINGS");
                i.addFlags(VCardConfig.FLAG_USE_QP_TO_PRIMARY_PROPERTIES);
                AntiLostRemoteIntro.this.startActivity(i);
            }
        });
        builder.setNegativeButton((int) R.string.label_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.show();
    }

    private void clickTryLock() {
        CommonMethod.yesNoBtnDialogListen(this, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                AntiLostRemoteIntro.this.getSharedPreferences("nq_antilost", 0).edit().putBoolean("lock_example", true).commit();
                Intent i = new Intent(AntiLostRemoteIntro.this, AntiLostLockExample.class);
                i.putExtra("second", 10);
                AntiLostRemoteIntro.this.startActivity(i);
                AntiLostRemoteIntro.this.startService(new Intent(AntiLostRemoteIntro.this, AntiLostLockExampleServise.class));
            }
        }, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        }, getResources().getString(R.string.text_antilost_guide_try_lock_query), R.string.text_antilost_guide_try_button_lock);
    }

    private void clickTryAlarm() {
        CommonMethod.yesNoBtnDialogListen(this, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                SharedPreferences spf = AntiLostRemoteIntro.this.getSharedPreferences("nq_antilost", 0);
                spf.edit().putBoolean("lock_example", true).commit();
                spf.edit().putBoolean("alarm_example", true).commit();
                Intent i = new Intent(AntiLostRemoteIntro.this, AntiLostLockExample.class);
                i.putExtra("second", 5);
                AntiLostRemoteIntro.this.startActivity(i);
                AntiLostRemoteIntro.this.startService(new Intent(AntiLostRemoteIntro.this, AntiLostLockExampleServise.class));
            }
        }, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        }, getResources().getString(R.string.text_antilost_guide_try_alarm_query), R.string.text_antilost_guide_try_button_alarm);
    }
}
