package com.netqin.antivirus.antilost;

import android.content.ContentValues;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.provider.CallLog;

public class CallLogHandler {
    public static final String DATE = "date";
    public static final String DURATION = "duration";
    public static final String NAME = "name";
    public static final String NEW = "new";
    public static final String NUMBER = "number";
    public static final String TYPE = "type";
    private static int mCallLogCount = 0;
    private static Uri mCallLogUri = CallLog.Calls.CONTENT_URI;
    private Context mContext;
    /* access modifiers changed from: private */
    public long mMaxDate = 0;

    public CallLogHandler(Context context) {
        this.mContext = context;
        mCallLogCount = getCallLogCount();
    }

    public Cursor getAllCallLog() {
        return this.mContext.getContentResolver().query(mCallLogUri, null, null, null, "date DESC");
    }

    public int getCallLogCount() {
        Cursor c = this.mContext.getContentResolver().query(mCallLogUri, null, null, null, "date DESC");
        int i = c.getCount();
        c.close();
        return i;
    }

    public Cursor getLatestCallLog() {
        return this.mContext.getContentResolver().query(mCallLogUri, null, null, null, "_id DESC limit 1");
    }

    public long getMaxDate() {
        long id = 0;
        Cursor c = this.mContext.getContentResolver().query(mCallLogUri, new String[]{"date"}, null, null, "_id DESC limit 1");
        if (c.getCount() > 0) {
            c.moveToFirst();
            id = c.getLong(c.getColumnIndex("date"));
        }
        c.close();
        return id;
    }

    public int deleteAllCallLog() {
        return this.mContext.getContentResolver().delete(mCallLogUri, null, null);
    }

    public boolean deleteCallLog(long id) {
        if (this.mContext.getContentResolver().delete(mCallLogUri, "_id=" + id, null) > 0) {
            return true;
        }
        return false;
    }

    public Cursor getCallLog(long id) {
        return this.mContext.getContentResolver().query(mCallLogUri, null, "_id=" + id, null, null);
    }

    public Cursor getIncomingCallLog() {
        return this.mContext.getContentResolver().query(mCallLogUri, null, "type = 1", null, "date DESC");
    }

    public Cursor getOutGoingCallLog() {
        return this.mContext.getContentResolver().query(mCallLogUri, null, "type = 2", null, "date DESC");
    }

    public Cursor getMissedCallLog() {
        return this.mContext.getContentResolver().query(mCallLogUri, null, "type = 3", null, "date DESC");
    }

    public boolean CreateCallLogItem(ContentValues values) {
        return this.mContext.getContentResolver().insert(mCallLogUri, values) != null;
    }

    public void registerCallLogObserver(ContentObserver observer) {
        this.mContext.getContentResolver().registerContentObserver(mCallLogUri, true, observer);
    }

    public void unregisterObserver(ContentObserver observer) {
        this.mContext.getContentResolver().unregisterContentObserver(observer);
    }

    public ContentObserver getCallLogObserver(Handler handler) {
        return new CallLogObserver(handler);
    }

    public class CallLogObserver extends ContentObserver {
        private Handler mHandler;

        public CallLogObserver(Handler handler) {
            super(handler);
            this.mHandler = handler;
        }

        public void onChange(boolean selfChange) {
            long i = CallLogHandler.this.getMaxDate();
            if (i > CallLogHandler.this.mMaxDate) {
                CallLogHandler.this.mMaxDate = i;
                Cursor c = CallLogHandler.this.getLatestCallLog();
                if (c.getCount() == 0) {
                    c.close();
                    return;
                }
                c.moveToFirst();
                this.mHandler.sendMessage(this.mHandler.obtainMessage(301, (int) c.getLong(c.getColumnIndex(SmsHandler.ROWID)), 0, c.getString(c.getColumnIndex("number"))));
                c.close();
                return;
            }
            CallLogHandler.this.mMaxDate = i;
        }
    }
}
