package com.netqin.antivirus.antilost;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.netqin.antivirus.appprotocol.XmlTagValue;
import com.netqin.antivirus.cloud.model.xml.XmlUtils;
import com.netqin.antivirus.common.CommonDefine;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.common.CommonUtils;
import com.netqin.antivirus.log.LogEngine;
import com.nqmobile.antivirus_ampro20.R;
import java.util.Vector;

public class AntiLostSetSecurityNum extends Activity implements TextWatcher {
    private static final int PICK_CONTACT_SUBACTIVITY = 6;
    private View mBtnNext;
    /* access modifiers changed from: private */
    public EditText mETSecurityNum;
    private int type;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.antilost_setsecuritynum);
        ((TextView) findViewById(R.id.activity_name)).setText((int) R.string.act_name_antilost);
        this.type = getIntent().getIntExtra("type", 0);
        this.mETSecurityNum = (EditText) findViewById(R.id.antilost_setsecuritynum_edit);
        SharedPreferences sharedPreferences = getSharedPreferences("nq_antilost", 0);
        String securityNumOrg = CommonUtils.getConfigWithStringValue(this, "nq_antilost", "securitynum", "");
        this.mETSecurityNum.setText(securityNumOrg);
        this.mETSecurityNum.addTextChangedListener(this);
        findViewById(R.id.antilost_setsecuritynum_pickcontact).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AntiLostSetSecurityNum.this.clickPicContact();
            }
        });
        this.mBtnNext = findViewById(R.id.antilost_set_securitynum_ok);
        if (TextUtils.isEmpty(securityNumOrg)) {
            this.mBtnNext.setEnabled(false);
        }
        this.mBtnNext.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AntiLostSetSecurityNum.this.clickNext();
            }
        });
        findViewById(R.id.antilost_set_securitynum_cancel).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AntiLostSetSecurityNum.this.clickBack();
            }
        });
    }

    /* access modifiers changed from: private */
    public void clickNext() {
        SharedPreferences spf = getSharedPreferences("nq_antilost", 0);
        String securitynumInput = this.mETSecurityNum.getText().toString();
        if (CommonMethod.isLegalSecurityNumber(securitynumInput)) {
            String imsi = CommonMethod.getIMSI(this);
            CommonUtils.putConfigWithStringValue(this, "nq_antilost", "securitynum", securitynumInput);
            CommonUtils.putConfigWithStringValue(this, "nq_antilost", XmlUtils.LABEL_MOBILEINFO_IMSI, imsi);
            spf.edit().putString(XmlTagValue.version, CommonDefine.VERSION).commit();
            if (this.type == 1) {
                CommonMethod.showNotification(this, getResources().getString(R.string.text_antilost_open_done));
                finish();
                spf.edit().putBoolean("start", true).commit();
                spf.edit().putBoolean("running", true).commit();
                spf.edit().putBoolean("changesim_sendsms", true).commit();
                startService(new Intent(this, AntiLostService.class));
                LogEngine.insertOperationItemLog(11, "", getFilesDir().getPath());
                return;
            }
            Toast toast = Toast.makeText(this, (int) R.string.text_antilost_set_securitynum_suc, 0);
            toast.setGravity(81, 0, 100);
            toast.show();
            finish();
        } else if (securitynumInput == null || securitynumInput.length() <= 0) {
            getSharedPreferences("nq_antilost", 0).edit().putBoolean("changesim_sendsms", false).commit();
            CommonUtils.putConfigWithStringValue(this, "nq_antilost", "securitynum", "");
            finish();
        } else {
            CommonMethod.messageDialog(this, (int) R.string.text_antilost_securitynum_wrong, (int) R.string.label_netqin_antivirus);
        }
    }

    /* access modifiers changed from: private */
    public void clickBack() {
        finish();
    }

    /* access modifiers changed from: private */
    public void clickPicContact() {
        ContactsHandler.pickContact(this, 6);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Uri uriRet;
        if (resultCode == -1) {
            switch (requestCode) {
                case 6:
                    if (!(data == null || (uriRet = data.getData()) == null)) {
                        try {
                            Cursor c = managedQuery(uriRet, null, null, null, null);
                            ContactsHandler contactsHandler = new ContactsHandler(this);
                            long peapleid = contactsHandler.getPeapleIdFromCursor(c);
                            c.close();
                            Cursor cc = contactsHandler.getPhonesOfContact(peapleid);
                            Vector<String> num = contactsHandler.getPhoneNumFromCursor(cc);
                            cc.close();
                            if (num.size() != 1) {
                                CharSequence[] cs = new CharSequence[num.size()];
                                for (int j = 0; j < num.size(); j++) {
                                    cs[j] = num.elementAt(j);
                                }
                                final CharSequence[] charSequenceArr = cs;
                                new AlertDialog.Builder(this).setTitle((int) R.string.label_select_tip).setItems(cs, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        AntiLostSetSecurityNum.this.mETSecurityNum.setText(charSequenceArr[which]);
                                    }
                                }).create().show();
                                break;
                            } else {
                                this.mETSecurityNum.setText(num.elementAt(0));
                                break;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            break;
                        }
                    }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void afterTextChanged(Editable arg0) {
    }

    public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
    }

    public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        String telNum = this.mETSecurityNum.getText().toString();
        if (TextUtils.isEmpty(telNum) || telNum.trim().length() < 6 || telNum.trim().length() > 20) {
            this.mBtnNext.setEnabled(false);
        } else {
            this.mBtnNext.setEnabled(true);
        }
    }
}
