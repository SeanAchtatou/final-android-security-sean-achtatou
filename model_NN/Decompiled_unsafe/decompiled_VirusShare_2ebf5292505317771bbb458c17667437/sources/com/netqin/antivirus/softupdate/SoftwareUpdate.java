package com.netqin.antivirus.softupdate;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.view.KeyEvent;
import com.netqin.antivirus.HomeActivity;
import com.netqin.antivirus.NqUtil;
import com.netqin.antivirus.Preferences;
import com.netqin.antivirus.Value;
import com.netqin.antivirus.common.CommonDefine;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.common.ProgDlgActivity;
import com.netqin.antivirus.common.ProgressTextDialog;
import com.netqin.antivirus.net.appupdateservice.AppUpdateService;
import com.netqin.antivirus.net.avservice.AVService;
import com.netqin.antivirus.services.NotificationUtil;
import com.nqmobile.antivirus_ampro20.R;
import java.util.Calendar;

public class SoftwareUpdate extends ProgDlgActivity {
    private static final String LOG_TAG = "netqin";
    public static Activity mActAntiVirusHome = null;
    public static final String mUpdateTag = "update";
    /* access modifiers changed from: private */
    public AppUpdateService appUpdateService;
    private AVService avService;
    private final DialogInterface.OnClickListener cancelListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            CommonMethod.putConfigWithIntegerValue(SoftwareUpdate.this, SoftwareUpdate.LOG_TAG, "softwareupdatetype", 0);
            SoftwareUpdate.this.mMsgCurrState = 20;
            if (SoftwareUpdate.this.appUpdateService != null) {
                SoftwareUpdate.this.appUpdateService.cancel();
            }
            SoftwareUpdate.this.clickBackToMain1();
        }
    };
    /* access modifiers changed from: private */
    public ContentValues content;
    final DialogInterface.OnClickListener exitListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            switch (SoftwareUpdate.this.softwareupdateType.intValue()) {
                case 1:
                    SoftwareUpdate.this.finish();
                    CommonMethod.putConfigWithIntegerValue(SoftwareUpdate.this, SoftwareUpdate.LOG_TAG, "softwareupdatetype", 0);
                    return;
                case 2:
                    CommonMethod.exitAntiVirus(SoftwareUpdate.this);
                    return;
                default:
                    return;
            }
        }
    };
    private boolean mDirectUpdate = false;
    private boolean mForceSoftUpdate = false;
    /* access modifiers changed from: private */
    public int mMsgCurrState = 20;
    private Message mMsgPrev = null;
    private boolean mProgSimpleMarkSucc = false;
    private boolean mProgTextMarkSucc = false;
    private int mProgressMax = 0;
    Value.SoftUpdateMsg mSoftUpdateMsg = null;
    private final DialogInterface.OnClickListener okListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            SoftwareUpdate.this.mMsgCurrState = 20;
            if (CommonMethod.getConfigWithIntegerValue(SoftwareUpdate.this, SoftwareUpdate.LOG_TAG, "softwareupdatetype", 0) == 1) {
                CommonMethod.putConfigWithIntegerValue(SoftwareUpdate.this, SoftwareUpdate.LOG_TAG, "softwareupdatetype", 0);
            }
            SoftwareUpdate.this.clickUpdate();
        }
    };
    /* access modifiers changed from: private */
    public Integer softwareupdateType;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        this.softwareupdateType = Integer.valueOf(CommonMethod.getConfigWithIntegerValue(this, LOG_TAG, "softwareupdatetype", 2));
        this.mProgSimpleMarkSucc = false;
        this.mProgTextMarkSucc = false;
        this.mProgressText = (String) getText(R.string.text_getting_software_version);
        this.mProgressDlg = CommonMethod.progressDlgCreate(this, this.mProgressText, this.mHandler);
        this.content = new ContentValues();
        this.content.put("IMEI", CommonMethod.getIMEI(this));
        this.content.put("IMSI", CommonMethod.getIMSI(this));
        this.content.put(Value.ClientVersion, new Preferences(this).getVirusDBVersion());
        this.content.put("UID", CommonMethod.getUID(this));
        this.mDirectUpdate = getIntent().getBooleanExtra("update", false);
        if (this.mDirectUpdate) {
            new Thread(new Runnable() {
                public void run() {
                    SoftwareUpdate.this.appUpdateService = AppUpdateService.getInstance(SoftwareUpdate.this);
                    SoftwareUpdate.this.appUpdateService.request(SoftwareUpdate.this.mHandler, SoftwareUpdate.this.content);
                }
            }).start();
            return;
        }
        CommonMethod.sendUserMessage(this.mHandler, 1);
        threadFuncForGetVersion();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        CommonMethod.logDebug(LOG_TAG, "SoftwareUpdate: onResume: " + this.mMsgCurrState);
        if (this.mMsgCurrState == 31 || this.mMsgCurrState == 28) {
            CommonMethod.sendUserMessage(this.mHandler, 4);
            if (this.mMsgPrev != null) {
                this.mHandler.sendMessage(this.mMsgPrev);
            }
            this.mMsgCurrState = 20;
        }
    }

    /* access modifiers changed from: protected */
    public void progressDialogExOnStop() {
        if (!this.mProgSimpleMarkSucc) {
            this.mActivityVisible = false;
            if (this.appUpdateService != null) {
                this.appUpdateService.cancel();
            }
        }
        super.progressDialogExOnStop();
        if (!this.mProgSimpleMarkSucc) {
            clickBackToMain();
        }
    }

    /* access modifiers changed from: protected */
    public void progressTextDialogOnStop() {
        if (!this.mProgTextMarkSucc) {
            this.mActivityVisible = false;
            if (this.appUpdateService != null) {
                this.appUpdateService.cancel();
            }
        }
        super.progressTextDialogOnStop();
        if (!this.mProgTextMarkSucc) {
            clickBackToMainEx();
        }
    }

    /* access modifiers changed from: protected */
    public void childFunctionCall(Message msg) {
        String tmpStr;
        int arg1 = msg.arg1;
        int arg2 = msg.arg2;
        SharedPreferences sharedPreferences = getSharedPreferences(LOG_TAG, 0);
        this.mMsgCurrState = arg1;
        AnonymousClass5 r0 = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                SoftwareUpdate.this.clickBackToMain();
            }
        };
        AnonymousClass6 r02 = new DialogInterface.OnKeyListener() {
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode != 4) {
                    return false;
                }
                SoftwareUpdate.this.clickBackToMain();
                return true;
            }
        };
        AnonymousClass7 r03 = new DialogInterface.OnKeyListener() {
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode != 4) {
                    return false;
                }
                CommonMethod.exitAntiVirus(SoftwareUpdate.this);
                return true;
            }
        };
        switch (arg1) {
            case 21:
                this.mProgressMax = arg2 / 1024;
                this.mProgTextLeft = "0%";
                this.mProgTextRight = "0KB/" + this.mProgressMax + "KB";
                CommonMethod.sendUserMessage(this.mHandler, 8, this.mProgressMax);
                return;
            case 22:
                CommonMethod.sendUserMessage(this.mHandler, 10);
                this.mProgTextMarkSucc = true;
                finish();
                installPackage(this.content);
                return;
            case CommonDefine.MSG_PROG_ARG_ERRMORE:
                CommonMethod.logDebug(LOG_TAG, "SoftwareUpdate: MSG_PROG_ARG_ERRMORE : " + this.mActivityVisible);
                saveResponseInfo();
                CommonMethod.sendUserMessage(this.mHandler, 4);
                this.mProgSimpleMarkSucc = true;
                if (this.mActivityVisible) {
                    CommonMethod.putConfigWithIntegerValue(this, LOG_TAG, "softwareupdatetype", 0);
                    CommonMethod.logDebug(LOG_TAG, "SoftwareUpdate: MSG_PROG_ARG_ERRMORE : 1");
                    try {
                        AlertDialog.Builder builder = new AlertDialog.Builder(this);
                        builder.setTitle(R.string.label_netqin_antivirus);
                        builder.setMessage(R.string.text_version_is_uptodate);
                        builder.setPositiveButton(R.string.label_ok, this.cancelListener);
                        builder.setOnKeyListener(r02);
                        CommonMethod.logDebug(LOG_TAG, "SoftwareUpdate: MSG_PROG_ARG_ERRMORE : 2");
                        builder.show();
                        CommonMethod.logDebug(LOG_TAG, "SoftwareUpdate: MSG_PROG_ARG_ERRMORE : 3");
                        return;
                    } catch (Exception e) {
                        return;
                    }
                } else {
                    this.mMsgPrev = new Message();
                    this.mMsgPrev.what = msg.what;
                    this.mMsgPrev.arg1 = msg.arg1;
                    this.mMsgPrev.arg2 = msg.arg2;
                    return;
                }
            case CommonDefine.MSG_PROG_ARG_UPDATE:
                int sizeKB = arg2 / 1024;
                this.mProgTextLeft = String.valueOf((int) (((float) (sizeKB * 100)) / ((float) this.mProgressMax))) + "%";
                this.mProgTextRight = String.valueOf(sizeKB) + "KB/" + this.mProgressMax + "KB";
                CommonMethod.sendUserMessage(this.mHandler, 9, sizeKB);
                return;
            case 31:
                CommonMethod.logDebug(LOG_TAG, "SoftwareUpdate: MSG_PROG_ARG_NEEDUPDATE : " + this.mActivityVisible);
                saveResponseInfo();
                if (CommonMethod.getConfigWithIntegerValue(this, LOG_TAG, "softwareupdatetype", 0) == 2) {
                    this.mForceSoftUpdate = true;
                } else {
                    this.mForceSoftUpdate = false;
                }
                CommonMethod.sendUserMessage(this.mHandler, 4);
                this.mProgSimpleMarkSucc = true;
                if (this.mActivityVisible) {
                    long softSize = Long.parseLong(this.content.get(Value.AppUpdateFileLength).toString());
                    if (softSize >= 1024) {
                        tmpStr = String.valueOf(softSize / 1024) + "KB";
                    } else {
                        tmpStr = String.valueOf(softSize) + "B";
                    }
                    if (this.mDirectUpdate) {
                        this.mMsgPrev = null;
                        clickUpdate();
                        return;
                    }
                    try {
                        AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
                        builder2.setTitle(R.string.label_netqin_antivirus);
                        Value.SoftUpdateMsg su = (Value.SoftUpdateMsg) msg.obj;
                        if (this.mForceSoftUpdate) {
                            if (!TextUtils.isEmpty(su.promptMsg)) {
                                builder2.setMessage(su.promptMsg);
                                CommonMethod.setSoftForceUpdateMassage(this, su.promptMsg);
                            } else {
                                builder2.setMessage(getString(R.string.text_force_softupdate, new Object[]{tmpStr}));
                            }
                            builder2.setNegativeButton(R.string.label_cancel, this.exitListener);
                            builder2.setOnKeyListener(r03);
                        } else {
                            if (!TextUtils.isEmpty(su.promptMsg)) {
                                builder2.setMessage(su.promptMsg);
                                CommonMethod.setSoftUpdateMassage(this, su.promptMsg);
                            } else {
                                builder2.setMessage(getString(R.string.text_version_may_update, new Object[]{tmpStr}));
                            }
                            builder2.setNegativeButton(R.string.label_cancel, this.cancelListener);
                            builder2.setOnKeyListener(r02);
                        }
                        builder2.setPositiveButton(R.string.label_ok, this.okListener);
                        builder2.show();
                        return;
                    } catch (Exception e2) {
                        return;
                    }
                } else if (this.mDirectUpdate) {
                    this.mMsgPrev = null;
                    return;
                } else {
                    this.mMsgPrev = new Message();
                    this.mMsgPrev.what = msg.what;
                    this.mMsgPrev.arg1 = msg.arg1;
                    this.mMsgPrev.arg2 = msg.arg2;
                    return;
                }
            case CommonDefine.MSG_PROG_ARG_CANCEL_SERVER:
                return;
            default:
                CommonMethod.logDebug(LOG_TAG, "SoftwareUpdate: DEFAULT MESSAGE");
                try {
                    AlertDialog.Builder builder3 = new AlertDialog.Builder(this);
                    builder3.setTitle(R.string.label_netqin_antivirus);
                    builder3.setMessage(getString(R.string.text_unprocess_message, new Object[]{Integer.valueOf(msg.arg1)}));
                    builder3.setPositiveButton(R.string.label_ok, r0);
                    builder3.setOnKeyListener(r02);
                    builder3.show();
                    return;
                } catch (Exception e3) {
                    return;
                }
        }
    }

    private boolean threadFuncForGetVersion() {
        new Thread(new Runnable() {
            public void run() {
                SoftwareUpdate.this.appUpdateService = AppUpdateService.getInstance(SoftwareUpdate.this);
                SoftwareUpdate.this.appUpdateService.request(SoftwareUpdate.this.mHandler, SoftwareUpdate.this.content);
            }
        }).start();
        return true;
    }

    /* access modifiers changed from: private */
    public void clickBackToMain() {
        if (this.appUpdateService != null) {
            this.appUpdateService.cancel();
        }
        saveResponseInfo();
        if (CommonMethod.getConfigWithIntegerValue(this, LOG_TAG, "softwareupdatetype", 0) == 2) {
            finish();
            CommonMethod.exitAntiVirus(this);
            return;
        }
        startActivity(new Intent(this, HomeActivity.class));
        if (mActAntiVirusHome != null) {
            mActAntiVirusHome.finish();
            mActAntiVirusHome = null;
        }
        finish();
    }

    /* access modifiers changed from: private */
    public void clickBackToMain1() {
        if (this.appUpdateService != null) {
            this.appUpdateService.cancel();
        }
        if (CommonMethod.getConfigWithIntegerValue(this, LOG_TAG, "softwareupdatetype", 0) == 2) {
            finish();
            CommonMethod.exitAntiVirus(this);
            return;
        }
        startActivity(new Intent(this, HomeActivity.class));
        if (mActAntiVirusHome != null) {
            mActAntiVirusHome.finish();
            mActAntiVirusHome = null;
        }
        finish();
    }

    private void clickBackToMainEx() {
        if (this.appUpdateService != null) {
            this.appUpdateService.cancel();
        }
        startActivity(new Intent(this, HomeActivity.class));
        if (mActAntiVirusHome != null) {
            mActAntiVirusHome.finish();
            mActAntiVirusHome = null;
        }
        finish();
    }

    /* access modifiers changed from: private */
    public void clickUpdate() {
        this.mMsgCurrState = 20;
        this.appUpdateService.next();
        CommonMethod.sendUserMessage(this.mHandler, 4);
        this.mProgSimpleMarkSucc = true;
        this.mProgTextDialog = new ProgressTextDialog(this, R.string.text_downloading, this.mHandler);
        CommonMethod.sendUserMessage(this.mHandler, 6);
    }

    private void installPackage(ContentValues content2) {
        if (content2.containsKey(Value.AppUpdateFileName)) {
            NqUtil.installPackage(this, String.valueOf(getFilesDir().getAbsolutePath()) + "/" + content2.getAsString(Value.AppUpdateFileName));
        }
        NotificationUtil.closeNotification((NotificationManager) getSystemService("notification"));
    }

    private void saveResponseInfo() {
        if (this.content.containsKey(Value.AppUpdateNecessary)) {
            try {
                switch (this.content.getAsInteger(Value.AppUpdateNecessary).intValue()) {
                    case 11:
                        CommonMethod.putConfigWithIntegerValue(this, LOG_TAG, "softwareupdatetype", 2);
                        break;
                    case 12:
                    case 13:
                        break;
                    default:
                        CommonMethod.putConfigWithIntegerValue(this, LOG_TAG, "softwareupdatetype", 0);
                        break;
                }
            } catch (Exception e) {
            }
        }
        if (this.content.containsKey(Value.AppUpdateFileLength)) {
            try {
                CommonMethod.putConfigWithIntegerValue(this, LOG_TAG, "appsize", this.content.getAsInteger(Value.AppUpdateFileLength).intValue());
            } catch (Exception e2) {
            }
        }
        if (this.content.containsKey(Value.Display)) {
            try {
                if (this.content.getAsInteger(Value.Display).intValue() == 2) {
                    CommonMethod.putConfigWithIntegerValue(this, LOG_TAG, "softwaredisplaytype", 2);
                } else {
                    CommonMethod.putConfigWithIntegerValue(this, LOG_TAG, "softwaredisplaytype", 1);
                }
            } catch (Exception e3) {
            }
        }
        if (this.content.containsKey("NextConnectTime")) {
            try {
                CommonMethod.putConfigWithIntegerValue(this, LOG_TAG, "updatenextConnectTime", this.content.getAsInteger("NextConnectTime").intValue());
                int minute = CommonMethod.getConfigWithIntegerValue(this, LOG_TAG, "updatenextConnectTime", 4320);
                Calendar calendar = Calendar.getInstance();
                calendar.add(12, minute);
                CommonMethod.setSoftUpdateNextDialyCheckTime(this, calendar);
                CommonMethod.setSoftUpdateLastDialyCheckTimeToCurrentTime(this);
            } catch (Exception e4) {
            }
        } else {
            Calendar calendar2 = Calendar.getInstance();
            calendar2.add(6, 1);
            CommonMethod.setSoftUpdateNextDialyCheckTime(this, calendar2);
            CommonMethod.setSoftUpdateLastDialyCheckTimeToCurrentTime(this);
        }
    }
}
