package com.netqin.antivirus.util;

import android.content.Context;
import android.os.Build;
import com.netqin.antivirus.data.Application;
import java.util.ArrayList;

public class TaskManagerUtils {
    public static final String ACTION_APP_CHANGED = "com.netqin_av.task_app_changed";
    public static final String ACTION_APP_KILLED = "com.netqin.task_app_killed";
    public static final String ACTION_APP_KILLED_FINISH = "com.netqin.task_app_killed_finish";
    public static final String ACTION_WIDGET_GPRS_CHANGED = "com.netqin.gprs_changed";
    public static final String ACTION_WIDGET_GPS_CHANGED = "com.netqin.gps_changed";
    public static final String EXTRA_KILLED_APP = "com.netqin.killed_app";
    private static Integer SDK = null;
    protected static final String TAG = "TaskManagerUtils";
    public static Application mMgSelf;
    private static ArrayList<String> mNgApps;

    public static double getDecimalPoint(double number, int pointNum) {
        int devid = 1;
        for (int i = 0; i < pointNum; i++) {
            devid *= 10;
        }
        return ((double) ((int) Math.round(((double) devid) * number))) / ((double) devid);
    }

    public static boolean isFroyoSDK() {
        if (SDK == null) {
            SDK = Integer.valueOf(Integer.parseInt(Build.VERSION.SDK));
        }
        if (SDK.intValue() >= 8) {
            return true;
        }
        return false;
    }

    public static boolean isNgApp(Context context, Application app) {
        if (app == null) {
            return false;
        }
        if (mNgApps == null) {
            mNgApps = new ArrayList<>();
            mNgApps.add(context.getPackageName());
        }
        return mNgApps.contains(app.packageName);
    }

    public static Application getMgSelf(Context context) {
        if (context == null) {
            return null;
        }
        if (mMgSelf == null) {
            mMgSelf = new Application(context.getPackageName());
        }
        return mMgSelf;
    }

    public static AppQueryCondition getQueryConditionForNetApp() {
        AppQueryCondition condition = new AppQueryCondition();
        condition.appLevel = 3;
        condition.appPermission = "android.permission.INTERNET";
        return condition;
    }
}
