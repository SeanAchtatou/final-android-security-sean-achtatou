package com.netqin.antivirus.util;

import android.text.TextUtils;
import java.util.ArrayList;

public class AppFilter {
    public static final int APP_LEVEL_3PART = 1;
    public static final int APP_LEVEL_DAEMON_APP = 4;
    public static final int APP_LEVEL_SYSTEM_APP = 2;
    private int mAppLevel;
    private ArrayList<String> mAppPermission = new ArrayList<>();

    public void setAppLevel(int appLevel) {
        this.mAppLevel = appLevel;
    }

    public int getAppLevel() {
        return this.mAppLevel;
    }

    public void addAppPermission(String appPermission) {
        if (!this.mAppPermission.contains(appPermission)) {
            this.mAppPermission.add(appPermission);
        }
    }

    public boolean match(AppQueryCondition queryCondition) {
        return matchAppLevel(queryCondition) && matchAppPermission(queryCondition);
    }

    private boolean matchAppPermission(AppQueryCondition queryCondition) {
        if (TextUtils.isEmpty(queryCondition.appPermission)) {
            return true;
        }
        if (this.mAppPermission.contains(queryCondition.appPermission)) {
            return true;
        }
        return false;
    }

    private boolean matchAppLevel(AppQueryCondition queryCondition) {
        if (queryCondition.appLevel <= 0) {
            return true;
        }
        if ((queryCondition.appLevel & this.mAppLevel) != 0) {
            return true;
        }
        return false;
    }
}
