package com.netqin.antivirus.util;

import SHcMjZX.DD02zqNU;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Process;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.widget.ListView;
import android.widget.TextView;
import com.netqin.antivirus.Value;
import com.netqin.antivirus.data.Application;
import com.netqin.antivirus.data.PreferenceDataHelper;
import com.netqin.antivirus.services.CommonReceiver;
import com.netqin.antivirus.services.TaskManagerService;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class SystemUtils {
    public static final String ACTION_ONEKEY = "com.netqin.mobileguard.onekey_action";
    public static final int RATE_ALERT = 1;
    public static final int RATE_OK = 0;
    public static final int RATE_WARNING = 2;
    private static String sBuildId;
    private static String sDevicesId = null;
    private static HashMap<String, ApplicationInfo> sNetApps = null;
    private static String sSubscriberId = null;
    public static long sTotalMemory = getTotalMem();

    public static long getTotalMem() {
        byte[] mBuffer = new byte[1024];
        try {
            FileInputStream is = new FileInputStream("/proc/meminfo");
            int len = is.read(mBuffer);
            is.close();
            for (int i = 0; i < len; i++) {
                if (matchText(mBuffer, i, "MemTotal")) {
                    return extractMemValue(mBuffer, i + 7);
                }
            }
        } catch (FileNotFoundException | IOException e) {
        }
        return 0;
    }

    public static boolean matchText(byte[] buffer, int index, String text) {
        int N = text.length();
        if (index + N >= buffer.length) {
            return false;
        }
        for (int i = 0; i < N; i++) {
            if (buffer[index + i] != text.charAt(i)) {
                return false;
            }
        }
        return true;
    }

    public static long extractMemValue(byte[] buffer, int index) {
        while (index < buffer.length && buffer[index] != 10) {
            if (buffer[index] < 48 || buffer[index] > 57) {
                index++;
            } else {
                int start = index;
                while (true) {
                    index++;
                    if (index >= buffer.length || buffer[index] < 48 || buffer[index] > 57) {
                    }
                }
                return ((long) Integer.parseInt(new String(buffer, 0, start, index - start))) * 1024;
            }
        }
        return 0;
    }

    public static long getAvailMem(Context context) {
        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        ((ActivityManager) context.getSystemService("activity")).getMemoryInfo(mi);
        return mi.availMem;
    }

    public static String formatBytes(Context context, double bytes) {
        if (bytes > 1000000.0d) {
            return String.format("%.2f MB", Float.valueOf(((float) ((int) (bytes / 1000.0d))) / 1000.0f));
        } else if (bytes > 1024.0d) {
            return String.format("%.2f KB", Float.valueOf(((float) ((int) (bytes / 10.0d))) / 100.0f));
        } else {
            return String.format("%d bytes", Integer.valueOf((int) bytes));
        }
    }

    public static String prettyByte(long nByte) {
        if (nByte > 1000000) {
            return String.format("%.2f MB", Float.valueOf(((float) ((int) (nByte / 1000))) / 1000.0f));
        } else if (nByte > 1024) {
            return String.format("%.2f KB", Float.valueOf(((float) ((int) (nByte / 10))) / 100.0f));
        } else {
            return String.format("%d B", Integer.valueOf((int) nByte));
        }
    }

    public static void killApp(final Context context, Application app) {
        ActivityManager am = (ActivityManager) context.getSystemService("activity");
        if (TaskManagerUtils.isFroyoSDK()) {
            am.killBackgroundProcesses(app.packageName);
        } else {
            am.restartPackage(app.packageName);
        }
        if (context.getPackageName().equalsIgnoreCase(app.packageName)) {
            new Thread(new Runnable() {
                public void run() {
                    try {
                        Thread.sleep(1500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    SystemUtils.killSelf(context);
                }
            }).start();
        }
        TaskManagerService.remove(context, app);
    }

    /* access modifiers changed from: private */
    public static void killSelf(Context context) {
        beforeKillSelf(context);
        if (TaskManagerUtils.isFroyoSDK()) {
            Process.killProcess(Process.myUid());
            System.exit(Process.myUid());
            return;
        }
        ((ActivityManager) context.getSystemService("activity")).restartPackage(context.getPackageName());
    }

    private static void beforeKillSelf(Context context) {
    }

    public static final HashMap<String, ApplicationInfo> getAllNetApps(Context context) {
        if (sNetApps != null) {
            return sNetApps;
        }
        HashMap<String, ApplicationInfo> netApps = new HashMap<>();
        PackageManager pm = context.getPackageManager();
        try {
            for (ApplicationInfo ai : pm.getInstalledApplications(0)) {
                if (pm.checkPermission("android.permission.INTERNET", ai.packageName) == 0 && (ai.flags & 8) == 0) {
                    netApps.put(ai.processName, ai);
                }
            }
        } catch (Exception e) {
            alert(context, "Error: " + e);
        }
        sNetApps = netApps;
        return netApps;
    }

    public static final ArrayList<Application> getRunningNetApps(Context context) {
        HashMap<String, ApplicationInfo> netApps = getAllNetApps(context);
        ArrayList<Application> running = new ArrayList<>();
        try {
            List<ActivityManager.RunningAppProcessInfo> infos = ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses();
            if (infos != null) {
                PackageManager pm = context.getPackageManager();
                for (ActivityManager.RunningAppProcessInfo info : infos) {
                    ApplicationInfo ai = netApps.get(info.processName);
                    if (ai != null) {
                        CharSequence name = pm.getApplicationLabel(ai);
                        Application app = new Application();
                        app.processName = name.toString();
                        app.packageName = info.pkgList[0];
                        app.setLabelName(pm.getApplicationLabel(ai).toString());
                        app.pid = info.pid;
                        running.add(app);
                    }
                }
            }
        } catch (Exception e) {
            alert(context, "Error: " + e);
        }
        return running;
    }

    public static final void alert(Context ctx, CharSequence msg) {
        if (ctx != null) {
            new AlertDialog.Builder(ctx).setNeutralButton(17039370, (DialogInterface.OnClickListener) null).setMessage(msg).show();
        }
    }

    public static final boolean isAirplaneMode(Context context) {
        return Settings.System.getInt(context.getContentResolver(), "airplane_mode_on", 0) != 0;
    }

    public static WifiManager getWifiManager(Context context) {
        return (WifiManager) context.getSystemService("wifi");
    }

    public static ConnectivityManager getConnectivityManager(Context context) {
        return (ConnectivityManager) context.getSystemService("connectivity");
    }

    public static void closeWifi(Context context) {
        WifiManager wm = getWifiManager(context);
        int status = wm.getWifiState();
        if (status == 2 || status == 3) {
            wm.setWifiEnabled(false);
        }
    }

    public static void openWifi(Context context) {
        WifiManager wm = getWifiManager(context);
        int status = wm.getWifiState();
        if (status == 0 || status == 1) {
            wm.setWifiEnabled(true);
        }
    }

    public static void closeBluetooth() {
        setBluetoothEnabled(false);
    }

    public static void openBluetooth() {
        setBluetoothEnabled(true);
    }

    public static boolean setBluetoothEnabled(boolean enabled) {
        BluetoothAdapter ba = BluetoothAdapter.getDefaultAdapter();
        if (ba == null) {
            return false;
        }
        return enabled ? ba.enable() : ba.disable();
    }

    public static ArrayList<Application> getOneKeyKillApps(Context context) {
        AppQueryCondition condition = new AppQueryCondition();
        condition.appLevel = 3;
        ArrayList<Application> apps = TaskManagerService.getRunningApps(context, condition, true);
        Application self = TaskManagerUtils.getMgSelf(context);
        ArrayList<Application> killApps = new ArrayList<>();
        Iterator<Application> it = apps.iterator();
        while (it.hasNext()) {
            Application app = it.next();
            if (!PreferenceDataHelper.isInWhiteList(context, app.packageName) && !app.packageName.endsWith(".fm")) {
                if (app.packageName.equals(self.packageName)) {
                    self = app;
                } else {
                    killApps.add(app);
                }
            }
        }
        return killApps;
    }

    public static int oneKeyKillTask(Context context) {
        List<Application> apps = getOneKeyKillApps(context);
        for (Application app : apps) {
            killApp(context, app);
        }
        return apps.size();
    }

    public static final List<ActivityManager.RunningAppProcessInfo> getRunningAppProcessInfo(Context context) {
        try {
            return ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses();
        } catch (Exception e) {
            alert(context, "Error: " + e);
            return null;
        }
    }

    public static final int getRunningAppProcessInfoNum(Context context) {
        List<ActivityManager.RunningAppProcessInfo> running = getRunningAppProcessInfo(context);
        if (running == null) {
            return 0;
        }
        return running.size();
    }

    public static String getDevicesId(Context context) {
        if (sDevicesId == null) {
            sDevicesId = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        }
        return sDevicesId;
    }

    public static boolean isBluetoothEnalbed(Context context) {
        BluetoothAdapter ba = null;
        try {
            ba = BluetoothAdapter.getDefaultAdapter();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (ba == null) {
            return false;
        }
        switch (ba.getState()) {
            case 10:
            case 11:
            case 13:
            default:
                return false;
            case 12:
                return true;
        }
    }

    public static String getSubscriberId(Context context) {
        if (sSubscriberId == null) {
            sSubscriberId = ((TelephonyManager) context.getSystemService("phone")).getSubscriberId();
        }
        return sSubscriberId;
    }

    public static String getMachineModel() {
        return Build.MODEL;
    }

    public static String getBuildId(Context context) {
        if (sBuildId == null) {
            try {
                sBuildId = new StringBuilder(String.valueOf(DD02zqNU.DLLIxskak(context.getPackageManager(), context.getPackageName(), 128).versionCode)).toString();
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }
        return sBuildId;
    }

    public static void installApk(Context context, File apkFile) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(Uri.fromFile(apkFile), MimeUtils.MIME_APPLICATION_APK);
        context.startActivity(intent);
    }

    public static Double getSummaryRating(Context context) {
        int appCount;
        int score = 100;
        long freeMemory = getAvailMem(context) / 1048576;
        if (freeMemory <= 40) {
            score = 100 - (40 - ((int) freeMemory));
        }
        AppQueryCondition appQueryCondition = new AppQueryCondition();
        appQueryCondition.appLevel = 1;
        ArrayList<Application> applications = TaskManagerService.getRunningApps(context, appQueryCondition, true);
        if (applications != null && (appCount = applications.size()) > 5) {
            score -= (appCount - 5) * 3;
        }
        if (score < 0) {
            score = 0;
        }
        return Double.valueOf(((double) Math.round(new Double(((double) score) / 10.0d).doubleValue())) / 2.0d);
    }

    public static Bitmap getRatingBitmap(Context context, int rateImgOn, int rateImgOff, int rateImgHalf, Double rateValue) {
        boolean drawHalfStar;
        int starOnNum = rateValue.intValue();
        if (rateValue.doubleValue() > ((double) rateValue.intValue())) {
            drawHalfStar = true;
        } else {
            drawHalfStar = false;
        }
        BitmapDrawable starOn = (BitmapDrawable) context.getResources().getDrawable(rateImgOn);
        BitmapDrawable starOff = (BitmapDrawable) context.getResources().getDrawable(rateImgOff);
        BitmapDrawable starHalf = (BitmapDrawable) context.getResources().getDrawable(rateImgHalf);
        Bitmap b = Bitmap.createBitmap(starOn.getIntrinsicWidth() * 5, starOn.getIntrinsicHeight() + 2, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(b);
        Paint paint = new Paint();
        int offset = starOn.getIntrinsicWidth();
        boolean drawHalfStar2 = drawHalfStar;
        for (int i = 0; i < 5; i++) {
            int left = i * offset;
            if (i < starOnNum) {
                canvas.drawBitmap(starOn.getBitmap(), (float) left, 0.0f, paint);
            } else if (drawHalfStar2) {
                canvas.drawBitmap(starHalf.getBitmap(), (float) left, 0.0f, paint);
                drawHalfStar2 = false;
            } else {
                canvas.drawBitmap(starOff.getBitmap(), (float) left, 0.0f, paint);
            }
        }
        return b;
    }

    public static boolean isPackageInstalled(Context context, String pkgName) {
        PackageManager packageManager = context.getPackageManager();
        Intent intent = new Intent();
        intent.setPackage(pkgName);
        List<ResolveInfo> list = packageManager.queryIntentActivities(intent, 0);
        return list != null && list.size() > 0;
    }

    public static void openAD(Context context, String pkgName) {
        if (isPackageInstalled(context, pkgName)) {
            context.startActivity(context.getPackageManager().getLaunchIntentForPackage(pkgName));
            return;
        }
        String lang = Locale.getDefault().toString().toLowerCase();
        Uri localUri = Uri.parse("http://www.netqin.com");
        if ("com.netqin.antivirusgm20".equalsIgnoreCase(pkgName)) {
            localUri = Uri.parse("http://m.netqin.com/p/av/?c=201361&l=" + lang);
        } else if ("com.netqin.cm".equalsIgnoreCase(pkgName)) {
            localUri = Uri.parse("http://m.netqin.com/p/cm/?c=201362&l=" + lang);
        } else if ("com.feeliu".equalsIgnoreCase(pkgName)) {
            localUri = Uri.parse("http://m.netqin.com/p/fl/?c=121&l=" + lang);
        }
        context.startActivity(new Intent("android.intent.action.VIEW", localUri));
    }

    public static void showSimpleDialog(Context context, int dialogTitle, int dialogMsg) {
        new AlertDialog.Builder(context).setIcon(17301659).setTitle(dialogTitle).setMessage(dialogMsg).setNeutralButton(17039379, (DialogInterface.OnClickListener) null).show();
    }

    public static boolean isSystemPackage(ApplicationInfo pkg) {
        return (pkg.flags & 1) == 1;
    }

    public static void showToast(Context context, int resId) {
        if (context != null) {
            Intent in = new Intent(CommonReceiver.ACTION_SHOW_TOAST);
            in.putExtra("resId", resId);
            context.sendBroadcast(in);
        }
    }

    public static String getLanguageCode(Context context) {
        if (context == null) {
            return "";
        }
        if (Locale.getDefault().getCountry().toLowerCase().equals("cn")) {
            return Value.SoftLanguage;
        }
        return "1";
    }

    public static boolean isMarketInstalled(Context context) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse("market://details?id=com.netqin.mobileguard"));
        List<ResolveInfo> list = context.getPackageManager().queryIntentActivities(intent, 0);
        if (list == null || list.size() <= 0) {
            return false;
        }
        return true;
    }

    public static void enableListBottomLine(Context context, ListView listView) {
        if (context != null && listView != null) {
            TextView emputyView = new TextView(context);
            emputyView.setHeight(3);
            emputyView.setVisibility(8);
            listView.addFooterView(emputyView);
        }
    }

    public static int getPixByDip(int dip) {
        DisplayMetrics metrics = new DisplayMetrics();
        metrics.setToDefaults();
        return (int) (((float) dip) * metrics.density);
    }

    public static int getDisplayMetrics(Context context) {
        new DisplayMetrics();
        return context.getApplicationContext().getResources().getDisplayMetrics().widthPixels;
    }
}
