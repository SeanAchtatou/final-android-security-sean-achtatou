package com.netqin.antivirus.util;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import com.netqin.antivirus.packagemanager.PackageGroup;
import com.nqmobile.antivirus_ampro20.R;
import java.util.ArrayList;
import java.util.Hashtable;

public class PackageManagerUtils {
    private static ArrayList<ResolveInfo> mPkg3rd = null;
    private static Hashtable<String, ResolveInfo> mPkgAll = new Hashtable<>();
    private static ArrayList<ResolveInfo> mPkgSys = null;

    public static ArrayList<PackageGroup> getPkgGroups(Context context) {
        mPkgAll.clear();
        Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
        intent.addCategory("android.intent.category.LAUNCHER");
        for (ResolveInfo resolveInfo : context.getPackageManager().queryIntentActivities(intent, 0)) {
            if (!(resolveInfo == null || resolveInfo.activityInfo == null || resolveInfo.activityInfo.applicationInfo == null)) {
                mPkgAll.put(resolveInfo.activityInfo.packageName, resolveInfo);
            }
        }
        mPkg3rd = new ArrayList<>();
        mPkgSys = new ArrayList<>();
        for (ResolveInfo pkg : mPkgAll.values()) {
            if (SystemUtils.isSystemPackage(pkg.activityInfo.applicationInfo)) {
                mPkgSys.add(pkg);
            } else {
                mPkg3rd.add(pkg);
            }
        }
        ArrayList<PackageGroup> groups = new ArrayList<>();
        PackageGroup group3rd = new PackageGroup(context.getString(R.string.package_group_3rd), mPkg3rd);
        PackageGroup groupsys = new PackageGroup(context.getString(R.string.package_group_sys), mPkgSys);
        groups.add(group3rd);
        groups.add(groupsys);
        return groups;
    }

    public static Hashtable<String, ResolveInfo> getPkgAll(Context context) {
        mPkgAll.clear();
        Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
        intent.addCategory("android.intent.category.LAUNCHER");
        for (ResolveInfo resolveInfo : context.getPackageManager().queryIntentActivities(intent, 0)) {
            if (!(resolveInfo == null || resolveInfo.activityInfo == null || resolveInfo.activityInfo.applicationInfo == null)) {
                mPkgAll.put(resolveInfo.activityInfo.packageName, resolveInfo);
            }
        }
        return mPkgAll;
    }
}
