package com.netqin.antivirus.util;

public final class DevConfig {
    public static boolean DEBUG = false;
    public static final boolean DEBUG_NCT = false;
    public static final String GLOBAL_SETTINGS_PREFERENCE = "global_settings";
    public static final long NCT_FOR_DEBUG = 1;
    public static boolean SPLASH_ON = true;
}
