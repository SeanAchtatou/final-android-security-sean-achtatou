package com.netqin.antivirus.util;

public class MimeInfo {
    int mIconResId;
    String mMimeType;

    public MimeInfo(String mimeType, int iconId) {
        this.mIconResId = iconId;
        this.mMimeType = mimeType;
    }

    public String getMimeType() {
        return this.mMimeType;
    }

    public int getIconId() {
        return this.mIconResId;
    }
}
