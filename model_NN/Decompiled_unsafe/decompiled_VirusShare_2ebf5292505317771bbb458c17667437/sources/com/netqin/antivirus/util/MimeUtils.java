package com.netqin.antivirus.util;

import SHcMjZX.DD02zqNU;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import com.nqmobile.antivirus_ampro20.R;
import java.io.File;
import java.util.HashMap;

public class MimeUtils {
    public static final MimeInfo DEFAULT_MIME_TYPE = new MimeInfo(MIME_APPLICATION_X_EMPTY, R.drawable.mime_type_default);
    public static final String MIME_APPLICATION_APK = "application/vnd.android.package-archive";
    public static final String MIME_APPLICATION_X_DIRECTORY = "application/x-directory";
    public static final String MIME_APPLICATION_X_EMPTY = "application/x-empty";
    public static final String MIME_TEXT_HTML = "text/html";
    public static final String MIME_TEXT_PLAIN = "text/plain";
    public static final MimeInfo X_DIR = new MimeInfo(MIME_APPLICATION_X_EMPTY, R.drawable.folder);
    static HashMap<String, Drawable> mIconSet = new HashMap<>();
    public static HashMap<String, MimeInfo> sMimeMap = new HashMap<>();

    public static Drawable getIcon(Context context, String mimeType) {
        if (TextUtils.isEmpty(mimeType)) {
            return null;
        }
        Drawable icon = mIconSet.get(mimeType);
        if (icon != null) {
            return icon;
        }
        if (mimeType.equals(MIME_APPLICATION_X_EMPTY)) {
            mIconSet.put(MIME_APPLICATION_X_EMPTY, context.getResources().getDrawable(R.drawable.file));
        } else if (mimeType.equals(MIME_APPLICATION_X_DIRECTORY)) {
            mIconSet.put(MIME_APPLICATION_X_DIRECTORY, context.getResources().getDrawable(R.drawable.folder));
        } else if (!mimeType.startsWith(MIME_TEXT_HTML) && !mimeType.startsWith("audio/") && !mimeType.startsWith("video/")) {
            mimeType.equals(MIME_APPLICATION_APK);
        }
        return context.getResources().getDrawable(R.drawable.file);
    }

    public static MimeInfo getMimeInfo(File file) {
        if (file.isDirectory()) {
            return X_DIR;
        }
        MimeInfo m = sMimeMap.get(getSurffix(file.getName()).toLowerCase());
        return m == null ? DEFAULT_MIME_TYPE : m;
    }

    public static String getSurffix(String name) {
        int x = name.lastIndexOf(46);
        if (x > 0) {
            return name.substring(x + 1);
        }
        return "";
    }

    public static Bitmap fitSizePic(File f) {
        BitmapFactory.Options opts = new BitmapFactory.Options();
        if (DD02zqNU.Zob2JfG1Yi11hrq(f) < 20480) {
            opts.inSampleSize = 1;
        } else if (DD02zqNU.Zob2JfG1Yi11hrq(f) < 51200) {
            opts.inSampleSize = 2;
        } else if (DD02zqNU.Zob2JfG1Yi11hrq(f) < 307200) {
            opts.inSampleSize = 4;
        } else if (DD02zqNU.Zob2JfG1Yi11hrq(f) < 819200) {
            opts.inSampleSize = 6;
        } else if (DD02zqNU.Zob2JfG1Yi11hrq(f) < 1048576) {
            opts.inSampleSize = 8;
        } else {
            opts.inSampleSize = 10;
        }
        return BitmapFactory.decodeFile(f.getPath(), opts);
    }

    public static String fileSizeMsg(File f) {
        if (!f.isFile()) {
            return "";
        }
        long length = DD02zqNU.Zob2JfG1Yi11hrq(f);
        if (length >= 1073741824) {
            return String.valueOf((String.valueOf(((float) length) / 1.07374182E9f) + "000").substring(0, String.valueOf(((float) length) / 1.07374182E9f).indexOf(".") + 3)) + "GB";
        } else if (length >= 1048576) {
            return String.valueOf((String.valueOf(((float) length) / 1048576.0f) + "000").substring(0, String.valueOf(((float) length) / 1048576.0f).indexOf(".") + 3)) + "MB";
        } else if (length >= 1024) {
            return String.valueOf((String.valueOf(((float) length) / 1024.0f) + "000").substring(0, String.valueOf(((float) length) / 1024.0f).indexOf(".") + 3)) + "KB";
        } else if (length < 1024) {
            return String.valueOf(String.valueOf(length)) + "B";
        } else {
            return "";
        }
    }

    public static boolean checkDirPath(String newName) {
        if (newName.indexOf("\\") == -1) {
            return true;
        }
        return false;
    }

    public static boolean checkFilePath(String newName) {
        if (newName.indexOf("\\") == -1) {
            return true;
        }
        return false;
    }
}
