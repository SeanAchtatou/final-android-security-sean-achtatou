package com.netqin.antivirus.virusdbupdate;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.netqin.antivirus.NqUtil;
import com.netqin.antivirus.Preferences;
import com.netqin.antivirus.appprotocol.AppRequest;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.log.LogEngine;
import com.netqin.antivirus.util.MimeUtils;
import com.nqmobile.antivirus_ampro20.R;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UpdateDbActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.update_virus_db);
        setRequestedOrientation(1);
        ((TextView) findViewById(R.id.activity_name)).setText((int) R.string.text_avlib_update);
        findViewById(R.id.update_db_updatedb).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                LogEngine.insertOperationItemLog(LogEngine.LOG_VIRUSDB_UPDATE_BEGIN, "", UpdateDbActivity.this.getFilesDir().getPath());
                AppRequest.StartCheckAVDB(UpdateDbActivity.this, false);
            }
        });
        findViewById(R.id.update_db_member).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AppRequest.StartSubscribe(UpdateDbActivity.this, false, 112);
            }
        });
        String update = getIntent().getStringExtra("showdialog");
        if (!TextUtils.isEmpty(update)) {
            CommonMethod.logDebug("AVService", update);
            WebView wv = new WebView(this);
            wv.getSettings().setDefaultTextEncodingName("utf-8");
            wv.setBackgroundColor(-1);
            wv.loadData("<div style=\"color: #000000\">" + update + "</div>", MimeUtils.MIME_TEXT_HTML, "utf-8");
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(getString(R.string.text_avlib_update));
            builder.setView(wv);
            builder.setPositiveButton(getString(R.string.label_yes), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                    LogEngine.insertOperationItemLog(LogEngine.LOG_VIRUSDB_UPDATE_BEGIN, "", UpdateDbActivity.this.getFilesDir().getPath());
                    AppRequest.StartCheckAVDB(UpdateDbActivity.this, false);
                }
            });
            builder.setNegativeButton(getString(R.string.label_no), (DialogInterface.OnClickListener) null);
            builder.show();
        }
    }

    public void onResume() {
        boolean lastestDB;
        String ver;
        int year;
        int month;
        int day;
        String ver2;
        String ver3;
        super.onResume();
        String latest = CommonMethod.getLatestVirusDBVersion(this);
        String current = new Preferences(this).getVirusDBVersion();
        if (latest.compareTo(current) > 0) {
            lastestDB = false;
        } else {
            lastestDB = true;
        }
        TextView tv = (TextView) findViewById(R.id.update_db_currentversion);
        if (current.length() >= 8) {
            ver = String.valueOf(getString(R.string.text_virus_libver)) + " " + (String.valueOf(current.substring(0, 4)) + "-" + current.substring(4, 6) + "-" + current.substring(6, 8)) + "<br/>";
        } else {
            ver = String.valueOf(getString(R.string.text_virus_libver)) + " " + current + "<br/>";
        }
        try {
            year = Integer.parseInt(current.substring(0, 4));
            month = Integer.parseInt(current.substring(4, 6));
            day = Integer.parseInt(current.substring(6, 8));
        } catch (NumberFormatException e) {
            year = 2011;
            month = 6;
            day = 10;
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date dd = new Date();
        long quot = 0;
        try {
            quot = ((((simpleDateFormat.parse(simpleDateFormat.format(dd)).getTime() - simpleDateFormat.parse(String.valueOf(year) + "/" + month + "/" + day).getTime()) / 1000) / 60) / 60) / 24;
        } catch (ParseException e2) {
            e2.printStackTrace();
        }
        if (quot >= 15) {
            lastestDB = false;
        }
        if (lastestDB) {
            ver3 = String.valueOf(ver) + getString(R.string.update_db_lastestvirusdb);
        } else {
            String ver4 = String.valueOf(String.valueOf(ver) + getString(R.string.update_db_notlastestvirusdb_1)) + "<font color='red'><b>";
            if (quot > 30) {
                ver2 = String.valueOf(String.valueOf(ver4) + " " + (quot / 30) + " ") + getString(R.string.update_db_notlastestvirusdb_month) + " ";
            } else {
                ver2 = String.valueOf(String.valueOf(ver4) + " " + quot + " ") + getString(R.string.update_db_notlastestvirusdb_day) + " ";
            }
            ver3 = String.valueOf(String.valueOf(ver2) + "</b></font>") + getString(R.string.update_db_notlastestvirusdb_2);
        }
        tv.setText(Html.fromHtml(ver3));
        ImageView iv = (ImageView) findViewById(R.id.update_db_statusicon);
        if (!lastestDB) {
            iv.setImageResource(R.drawable.home_icon_status_danger);
        } else {
            iv.setImageResource(R.drawable.home_icon_status_ok);
        }
        TextView tvAlias = (TextView) findViewById(R.id.update_db_virusforecast_alias_tv);
        ((TextView) findViewById(R.id.update_db_virusforecast_name_tv)).setText(String.valueOf(getString(R.string.update_db_virusforecast_name)) + CommonMethod.getVirusForecastName(this));
        tvAlias.setText(String.valueOf(getString(R.string.update_db_virusforecast_alias)) + CommonMethod.getVirusForecastAlias(this));
        ((TextView) findViewById(R.id.update_db_virusforecast_level_tv)).setText(String.valueOf(getString(R.string.update_db_virusforecast_level)) + CommonMethod.getVirusForecastLevel(this));
        ((TextView) findViewById(R.id.update_db_virusforecast_desc_tv)).setText(String.valueOf(getString(R.string.update_db_virusforecast_desc)) + CommonMethod.getVirusForecastDesc(this));
        if (TextUtils.isEmpty(CommonMethod.getVirusForecastAlias(this))) {
            tvAlias.setVisibility(8);
        }
        TextView tvDetail = (TextView) findViewById(R.id.update_db_virusforecast_detail_tv);
        tvDetail.setText(Html.fromHtml("<a href='" + CommonMethod.getVirusForecastWapurl(this) + "'>" + getString(R.string.update_db_virusforecast_detail) + "</a>"));
        tvDetail.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                String detailUrl = CommonMethod.getVirusForecastWapurl(UpdateDbActivity.this);
                if (detailUrl.indexOf("http://") < 0) {
                    detailUrl = "http://www.netqin.com";
                }
                UpdateDbActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(detailUrl)));
            }
        });
        LinearLayout llUpdate = (LinearLayout) findViewById(R.id.update_db_member);
        TextView tvUpdateDesc = (TextView) findViewById(R.id.update_db_updatetomember_desc);
        if (CommonMethod.getIsMember(this)) {
            llUpdate.setVisibility(4);
            tvUpdateDesc.setText(R.string.update_db_membertip);
            return;
        }
        llUpdate.setVisibility(0);
        tvUpdateDesc.setText(R.string.update_db_updatetomember);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.addvice_feedback_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.updatedb_advice_feedback /*2131558847*/:
                NqUtil.clickAdviceFeedback(this);
                return true;
            default:
                return true;
        }
    }

    public void onPause() {
        super.onPause();
    }

    public void onStop() {
        super.onStop();
    }

    public void onDestroy() {
        super.onDestroy();
    }
}
