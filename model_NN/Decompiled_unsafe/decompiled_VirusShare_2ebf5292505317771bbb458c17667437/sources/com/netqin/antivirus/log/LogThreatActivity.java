package com.netqin.antivirus.log;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import com.nqmobile.antivirus_ampro20.R;
import java.util.Vector;

public class LogThreatActivity extends ListActivity implements View.OnClickListener {
    private Vector<LogItem> logVector;
    private Button mClearBtn;
    private LogListAdapter mLogLVAdapter;

    public static Intent getLaunchIntent(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, LogThreatActivity.class);
        return intent;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.log_threat);
        setRequestedOrientation(1);
        initRes();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        initListView();
    }

    private void initRes() {
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
    }

    private void initListView() {
        LogEngine logEngine = new LogEngine();
        logEngine.openDB(getFilesDir().getPath());
        this.logVector = logEngine.getThreatItem();
        logEngine.closeDB();
        this.mLogLVAdapter = new LogListAdapter(this, this.logVector, 4);
        setListAdapter(this.mLogLVAdapter);
    }

    public void onClick(View v) {
    }

    private void clickCleanLog() {
        new AlertDialog.Builder(this).setTitle((int) R.string.text_clean_all_log).setMessage((int) R.string.label_tip).setPositiveButton((int) R.string.label_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                LogThreatActivity.this.doCleanLog();
            }
        }).setNegativeButton((int) R.string.label_cancel, (DialogInterface.OnClickListener) null).show();
    }

    /* access modifiers changed from: private */
    public void doCleanLog() {
        LogEngine logEngine = new LogEngine();
        logEngine.openDB(getFilesDir().getPath());
        logEngine.clearLogsByCategory(4);
        logEngine.closeDB();
        initListView();
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        if (this.logVector != null && this.logVector.size() > 0) {
            menu.add(0, 1, 0, (int) R.string.text_clean_threat_log);
        }
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                clickCleanLog();
                return true;
            default:
                return true;
        }
    }
}
