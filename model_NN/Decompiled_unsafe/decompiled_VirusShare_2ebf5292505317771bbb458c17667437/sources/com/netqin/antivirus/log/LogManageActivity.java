package com.netqin.antivirus.log;

import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;
import com.nqmobile.antivirus_ampro20.R;

public class LogManageActivity extends TabActivity {
    public static Intent getLaunchIntent(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, LogManageActivity.class);
        return intent;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.software_manage);
        setRequestedOrientation(1);
        ((TextView) findViewById(R.id.activity_name)).setText((int) R.string.label_log);
        installTabHost();
    }

    private void installTabHost() {
        installCustomTabs(getTabHost());
    }

    private void installCustomTabs(TabHost tabHost) {
        tabHost.addTab(tabHost.newTabSpec("log_guard").setIndicator(getIndicatorView(R.string.log_guard, R.drawable.log_guard)).setContent(LogGuardActivity.getLaunchIntent(this)));
        tabHost.addTab(tabHost.newTabSpec("log_threat").setIndicator(getIndicatorView(R.string.log_threat, R.drawable.log_threat)).setContent(LogThreatActivity.getLaunchIntent(this)));
        tabHost.addTab(tabHost.newTabSpec("log_operate").setIndicator(getIndicatorView(R.string.log_operate, R.drawable.log_operate)).setContent(LogOperateActivity.getLaunchIntent(this)));
    }

    private View getIndicatorView(int titleId, int iconId) {
        View indicator = ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.tab_indicator, (ViewGroup) null);
        ((ImageView) indicator.findViewById(R.id.iv_indicator)).setImageResource(iconId);
        ((TextView) indicator.findViewById(R.id.tv_indicator)).setText(titleId);
        return indicator;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        System.gc();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }
}
