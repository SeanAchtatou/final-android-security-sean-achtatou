package com.netqin.antivirus.log;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.nqmobile.antivirus_ampro20.R;
import java.util.Vector;

class LogListAdapter extends BaseAdapter {
    private Vector<LogItem> logVector;
    private int mCategory;
    private Context mContext;
    private LayoutInflater mInflater = LayoutInflater.from(this.mContext);

    public LogListAdapter(Context context) {
        this.mContext = context;
    }

    public LogListAdapter(Context context, Vector<LogItem> v, int category) {
        this.mContext = context;
        this.logVector = v;
        this.mCategory = category;
    }

    private class ListViewHolder {
        ImageView icon;
        TextView log_file_name;
        TextView log_middle_icon;
        TextView log_virus_name;
        TextView name;
        TextView time;

        private ListViewHolder() {
        }

        /* synthetic */ ListViewHolder(LogListAdapter logListAdapter, ListViewHolder listViewHolder) {
            this();
        }
    }

    public int getCount() {
        return this.logVector.size();
    }

    public Object getItem(int position) {
        return Integer.valueOf(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ListViewHolder holder;
        if (convertView == null) {
            convertView = this.mInflater.inflate((int) R.layout.log_item, (ViewGroup) null);
            holder = new ListViewHolder(this, null);
            holder.icon = (ImageView) convertView.findViewById(R.id.log_icon);
            holder.name = (TextView) convertView.findViewById(R.id.log_name);
            holder.log_middle_icon = (TextView) convertView.findViewById(R.id.log_middle_icon);
            holder.log_virus_name = (TextView) convertView.findViewById(R.id.log_virus_name);
            holder.log_file_name = (TextView) convertView.findViewById(R.id.log_file_name);
            holder.time = (TextView) convertView.findViewById(R.id.log_time);
            convertView.setTag(holder);
        } else {
            holder = (ListViewHolder) convertView.getTag();
        }
        LogItem item = this.logVector.elementAt(position);
        String title = null;
        switch (item.type) {
            case 11:
                title = this.mContext.getResources().getString(R.string.text_log_antilost_on);
                holder.icon.setImageDrawable(this.mContext.getResources().getDrawable(R.drawable.log1));
                break;
            case 12:
                title = this.mContext.getResources().getString(R.string.text_log_antilost_off);
                holder.icon.setImageDrawable(this.mContext.getResources().getDrawable(R.drawable.log1));
                break;
            case 13:
                title = this.mContext.getResources().getString(R.string.text_log_contact_backup_local);
                holder.icon.setImageDrawable(this.mContext.getResources().getDrawable(R.drawable.log1));
                break;
            case 14:
                title = this.mContext.getResources().getString(R.string.text_log_contact_backup_local_success);
                holder.icon.setImageDrawable(this.mContext.getResources().getDrawable(R.drawable.log1));
                break;
            case 15:
                title = this.mContext.getResources().getString(R.string.text_log_contact_backup_local_fail);
                holder.icon.setImageDrawable(this.mContext.getResources().getDrawable(R.drawable.log2));
                break;
            case 16:
                title = this.mContext.getResources().getString(R.string.text_log_contact_recover_local);
                holder.icon.setImageDrawable(this.mContext.getResources().getDrawable(R.drawable.log1));
                break;
            case 17:
                title = this.mContext.getResources().getString(R.string.text_log_contact_recover_local_success);
                holder.icon.setImageDrawable(this.mContext.getResources().getDrawable(R.drawable.log1));
                break;
            case 18:
                title = this.mContext.getResources().getString(R.string.text_log_contact_recover_local_fail);
                holder.icon.setImageDrawable(this.mContext.getResources().getDrawable(R.drawable.log2));
                break;
            case 19:
                title = this.mContext.getResources().getString(R.string.text_log_contact_backup_net);
                holder.icon.setImageDrawable(this.mContext.getResources().getDrawable(R.drawable.log1));
                break;
            case 20:
                title = this.mContext.getResources().getString(R.string.text_log_contact_backup_net_success);
                holder.icon.setImageDrawable(this.mContext.getResources().getDrawable(R.drawable.log1));
                break;
            case 21:
                title = this.mContext.getResources().getString(R.string.text_log_contact_backup_net_fail);
                holder.icon.setImageDrawable(this.mContext.getResources().getDrawable(R.drawable.log2));
                break;
            case 22:
                title = this.mContext.getResources().getString(R.string.text_log_contact_recover_net);
                holder.icon.setImageDrawable(this.mContext.getResources().getDrawable(R.drawable.log1));
                break;
            case 23:
                title = this.mContext.getResources().getString(R.string.text_log_contact_recover_net_success);
                holder.icon.setImageDrawable(this.mContext.getResources().getDrawable(R.drawable.log1));
                break;
            case 24:
                title = this.mContext.getResources().getString(R.string.text_log_contact_recover_net_fail);
                holder.icon.setImageDrawable(this.mContext.getResources().getDrawable(R.drawable.log2));
                break;
            case 27:
                title = this.mContext.getResources().getString(R.string.onekey_optimization_end);
                holder.icon.setImageDrawable(this.mContext.getResources().getDrawable(R.drawable.log1));
                break;
            case 30:
                title = this.mContext.getResources().getString(R.string.text_log_web_block_found);
                holder.icon.setImageDrawable(this.mContext.getResources().getDrawable(R.drawable.home_icon_status_danger));
                break;
            case 100:
                title = this.mContext.getResources().getString(R.string.text_log_scan_begin);
                holder.icon.setImageDrawable(this.mContext.getResources().getDrawable(R.drawable.log1));
                break;
            case 101:
                title = this.mContext.getResources().getString(R.string.text_log_scan_cancel);
                holder.icon.setImageDrawable(this.mContext.getResources().getDrawable(R.drawable.log1));
                break;
            case 102:
                title = this.mContext.getResources().getString(R.string.text_log_scan_end);
                holder.icon.setImageDrawable(this.mContext.getResources().getDrawable(R.drawable.log1));
                break;
            case 103:
                title = this.mContext.getResources().getString(R.string.text_log_scan_sdcard_begin);
                holder.icon.setImageDrawable(this.mContext.getResources().getDrawable(R.drawable.log1));
                break;
            case LogEngine.LOG_SCAN_SDCARD_END:
                title = this.mContext.getResources().getString(R.string.text_log_scan_sdcard_end);
                holder.icon.setImageDrawable(this.mContext.getResources().getDrawable(R.drawable.log1));
                break;
            case LogEngine.LOG_VIRUS_FOUND:
                title = this.mContext.getResources().getString(R.string.text_log_virus_found, item.descript);
                holder.icon.setImageDrawable(this.mContext.getResources().getDrawable(R.drawable.home_icon_status_danger));
                break;
            case 111:
                title = this.mContext.getResources().getString(R.string.text_log_virus_delete_success, item.descript);
                holder.icon.setImageDrawable(this.mContext.getResources().getDrawable(R.drawable.log1));
                break;
            case 112:
                title = this.mContext.getResources().getString(R.string.text_log_virus_delete_fail, item.descript);
                holder.icon.setImageDrawable(this.mContext.getResources().getDrawable(R.drawable.log2));
                break;
            case LogEngine.LOG_VIRUSDB_UPDATE_BEGIN:
                title = this.mContext.getResources().getString(R.string.text_avlib_update);
                holder.icon.setImageDrawable(this.mContext.getResources().getDrawable(R.drawable.log1));
                break;
            case LogEngine.LOG_VIRUSDB_UPDATE_SUCCESS:
                title = this.mContext.getResources().getString(R.string.text_log_virusdb_update_success);
                holder.icon.setImageDrawable(this.mContext.getResources().getDrawable(R.drawable.log1));
                break;
            case LogEngine.LOG_VIRUSDB_UPDATE_FAIL:
                title = this.mContext.getResources().getString(R.string.text_log_virusdb_update_fail);
                holder.icon.setImageDrawable(this.mContext.getResources().getDrawable(R.drawable.log2));
                break;
        }
        holder.name.setText(title);
        holder.log_file_name.setText(item.filename);
        holder.time.setText(item.time);
        if (this.mCategory == 4) {
            holder.log_middle_icon.setVisibility(0);
            holder.log_virus_name.setVisibility(0);
            holder.log_file_name.setVisibility(0);
            holder.log_virus_name.setText(item.virusname);
        } else {
            holder.log_middle_icon.setVisibility(4);
            holder.log_file_name.setVisibility(8);
        }
        return convertView;
    }
}
