package com.netqin.antivirus.log;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import com.nqmobile.antivirus_ampro20.R;
import java.util.Vector;

public class LogOperateActivity extends ListActivity implements View.OnClickListener {
    private Vector<LogItem> logVector;
    private LogListAdapter mLogLVAdapter;

    public static Intent getLaunchIntent(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, LogOperateActivity.class);
        return intent;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.log_threat);
        setRequestedOrientation(1);
        initRes();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        initListView();
    }

    private void initRes() {
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
    }

    private void initListView() {
        LogEngine logEngine = new LogEngine();
        logEngine.openDB(getFilesDir().getPath());
        this.logVector = logEngine.getOperateItem();
        logEngine.closeDB();
        this.mLogLVAdapter = new LogListAdapter(this, this.logVector, 5);
        setListAdapter(this.mLogLVAdapter);
    }

    public void onClick(View v) {
    }

    private void clickCleanLog() {
        new AlertDialog.Builder(this).setTitle((int) R.string.text_clean_all_log).setMessage((int) R.string.label_tip).setPositiveButton((int) R.string.label_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                LogOperateActivity.this.doCleanLog();
            }
        }).setNegativeButton((int) R.string.label_cancel, (DialogInterface.OnClickListener) null).show();
    }

    /* access modifiers changed from: private */
    public void doCleanLog() {
        LogEngine logEngine = new LogEngine();
        logEngine.openDB(getFilesDir().getPath());
        logEngine.clearLogsByCategory(5);
        logEngine.closeDB();
        this.logVector.clear();
        initListView();
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        if (this.logVector != null && this.logVector.size() > 0) {
            menu.add(0, 1, 0, (int) R.string.text_clean_operate_log);
        }
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                clickCleanLog();
                return true;
            default:
                return true;
        }
    }
}
