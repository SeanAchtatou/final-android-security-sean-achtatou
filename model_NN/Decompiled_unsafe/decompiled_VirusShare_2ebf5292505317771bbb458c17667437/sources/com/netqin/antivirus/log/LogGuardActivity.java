package com.netqin.antivirus.log;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import com.nqmobile.antivirus_ampro20.R;

public class LogGuardActivity extends Activity {
    private int mApkNumber = 0;
    private int mFileNumber = 0;
    private TextView mGuardApkNumberView;
    private TextView mGuardFileNumberView;
    private int mWebblockNumber = 0;
    private TextView mWebblockNumberView;

    public static Intent getLaunchIntent(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, LogGuardActivity.class);
        return intent;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.log_guard);
        setRequestedOrientation(1);
        initRes();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        initView();
    }

    private void initRes() {
        this.mGuardFileNumberView = (TextView) findViewById(R.id.guard_file_number);
        this.mGuardApkNumberView = (TextView) findViewById(R.id.guard_apk_number);
        this.mWebblockNumberView = (TextView) findViewById(R.id.webblock_number);
    }

    private void initView() {
        LogEngine logEngine = new LogEngine();
        logEngine.openDB(getFilesDir().getPath());
        Cursor c1 = logEngine.getItemByCategory(1);
        if (c1 != null) {
            try {
                if (c1.moveToNext()) {
                    this.mFileNumber = c1.getInt(c1.getColumnIndexOrThrow("t_number"));
                }
            } finally {
                c1.close();
            }
        }
        Cursor c2 = logEngine.getItemByCategory(2);
        if (c2 != null) {
            try {
                if (c2.moveToNext()) {
                    this.mApkNumber = c2.getInt(c2.getColumnIndexOrThrow("t_number"));
                }
            } finally {
                c2.close();
            }
        }
        Cursor c3 = logEngine.getItemByCategory(3);
        if (c3 != null) {
            try {
                if (c3.moveToNext()) {
                    this.mWebblockNumber = c3.getInt(c3.getColumnIndexOrThrow("t_number"));
                }
            } finally {
                c3.close();
            }
        }
        logEngine.closeDB();
        this.mGuardFileNumberView.setText(getString(R.string.guard_file_number, new Object[]{Integer.valueOf(this.mFileNumber)}));
        this.mGuardApkNumberView.setText(getString(R.string.guard_apk_number, new Object[]{Integer.valueOf(this.mApkNumber)}));
        this.mWebblockNumberView.setText(getString(R.string.webblock_number, new Object[]{Integer.valueOf(this.mWebblockNumber)}));
    }

    private void clickCleanLog() {
        new AlertDialog.Builder(this).setTitle((int) R.string.text_clean_all_log).setMessage((int) R.string.label_tip).setPositiveButton((int) R.string.label_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                LogGuardActivity.this.doCleanLog();
            }
        }).setNegativeButton((int) R.string.label_cancel, (DialogInterface.OnClickListener) null).show();
    }

    /* access modifiers changed from: private */
    public void doCleanLog() {
        LogEngine logEngine = new LogEngine();
        logEngine.openDB(getFilesDir().getPath());
        logEngine.clearLogsByCategory(2);
        logEngine.clearLogsByCategory(1);
        logEngine.clearLogsByCategory(3);
        logEngine.closeDB();
        this.mFileNumber = 0;
        this.mApkNumber = 0;
        this.mWebblockNumber = 0;
        this.mGuardFileNumberView.setText(getString(R.string.guard_file_number, new Object[]{0}));
        this.mGuardApkNumberView.setText(getString(R.string.guard_apk_number, new Object[]{0}));
        this.mWebblockNumberView.setText(getString(R.string.webblock_number, new Object[]{0}));
        onContentChanged();
        initView();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 1, 0, (int) R.string.text_clean_guard_log);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                clickCleanLog();
                return true;
            default:
                return true;
        }
    }
}
