package com.netqin.antivirus.log;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import android.util.Log;
import com.netqin.antivirus.contact.vcard.VCardConfig;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

public class LogEngine {
    private static final String DATABASE_NAME = "logs.db";
    private static final int DATABASE_VERSION = 2;
    private static boolean DBG = false;
    public static final int LOG_ALL = 1;
    public static final int LOG_ANTILOST_OFF = 12;
    public static final int LOG_ANTILOST_ON = 11;
    public static final int LOG_CATEGORY_APK_GUARD = 2;
    public static final int LOG_CATEGORY_FILE_GUARD = 1;
    public static final int LOG_CATEGORY_OPERATE = 5;
    public static final int LOG_CATEGORY_THREAT = 4;
    public static final int LOG_CATEGORY_WEB_BLOCK = 3;
    public static final int LOG_CONTACT_BACKUP_LOCAL = 13;
    public static final int LOG_CONTACT_BACKUP_LOCAL_FAIL = 15;
    public static final int LOG_CONTACT_BACKUP_LOCAL_SUCCESS = 14;
    public static final int LOG_CONTACT_BACKUP_NET = 19;
    public static final int LOG_CONTACT_BACKUP_NET_FAIL = 21;
    public static final int LOG_CONTACT_BACKUP_NET_SUCCESS = 20;
    public static final int LOG_CONTACT_RECOVER_LOCAL = 16;
    public static final int LOG_CONTACT_RECOVER_LOCAL_FAIL = 18;
    public static final int LOG_CONTACT_RECOVER_LOCAL_SUCCESS = 17;
    public static final int LOG_CONTACT_RECOVER_NET = 22;
    public static final int LOG_CONTACT_RECOVER_NET_FAIL = 24;
    public static final int LOG_CONTACT_RECOVER_NET_SUCCESS = 23;
    private static final int LOG_LIMITED_NUMBER = 100;
    public static final int LOG_ONEKEY_OPTIMIZATION = 27;
    public static final int LOG_SCAN_BEGIN = 100;
    public static final int LOG_SCAN_CANCEL = 101;
    public static final int LOG_SCAN_END = 102;
    public static final int LOG_SCAN_SDCARD_BEGIN = 103;
    public static final int LOG_SCAN_SDCARD_END = 104;
    public static final int LOG_VIRUSDB_UPDATE_BEGIN = 120;
    public static final int LOG_VIRUSDB_UPDATE_FAIL = 122;
    public static final int LOG_VIRUSDB_UPDATE_SUCCESS = 121;
    public static final int LOG_VIRUS_DELETE_FAIL = 112;
    public static final int LOG_VIRUS_DELETE_SUCCESS = 111;
    public static final int LOG_VIRUS_FOUND = 110;
    public static final int LOG_WEB_BLOCK_FOUND = 30;
    private static final String TABLE_NAME = "logs";
    private static final String TAG = "LogEngine";
    private static final String T_CATEGORY = "t_category";
    private static final String T_DESCRIPT = "t_descript";
    private static final String T_FILE_NAME = "t_file_name";
    private static final String T_ID = "_id";
    private static final String T_NUMBER = "t_number";
    private static final String T_TIME = "t_time";
    private static final String T_TYPE = "t_type";
    private static final String T_VIRUS_NAME = "t_virus_name";
    static SQLiteDatabase logdb;

    public void openDB(String packageFilesPath) {
        File dbfile = new File(String.valueOf(packageFilesPath) + "/" + DATABASE_NAME);
        boolean isExist = dbfile.exists();
        logdb = SQLiteDatabase.openDatabase(dbfile.getAbsolutePath(), null, VCardConfig.FLAG_USE_QP_TO_PRIMARY_PROPERTIES);
        if (!isExist) {
            CreateTable();
        }
    }

    public void closeDB() {
        if (logdb != null && logdb.isOpen()) {
            logdb.close();
        }
        logdb = null;
    }

    public void CreateTable() {
        try {
            logdb.execSQL("DROP TABLE IF EXISTS TABLE_NAME");
            logdb.execSQL("CREATE TABLE  [logs] ( [t_category] INTEGER NOT NULL,  [t_type] INTEGER NOT NULL DEFAULT 0,  [t_file_name] VARCHAR(128),  [t_virus_name] VARCHAR(128),  [t_descript] VARCHAR(128),  [t_number] INTEGER,  [t_time] TIMESTAMP NOT NULL);");
            logdb.execSQL("CREATE INDEX [index_type] ON [logs] ([t_type]);");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void dropTable() {
        try {
            logdb.execSQL("drop table logs");
        } catch (SQLException e) {
        }
    }

    public void insertItem(int type, String descript) {
        String sql = "insert into logs values(" + type + ", \"" + descript + "\", " + "CURRENT_TIMESTAMP" + " );";
        try {
            if (DBG) {
                Log.i(TAG, "insertItem sql= " + sql);
            }
            logdb.execSQL(sql);
        } catch (SQLException e) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void insertThreatItem(int type, String fileName, String virusName, String descript) {
        ContentValues values = new ContentValues();
        values.put(T_CATEGORY, (Integer) 4);
        values.put(T_TYPE, Integer.valueOf(type));
        values.put(T_FILE_NAME, fileName);
        values.put(T_VIRUS_NAME, virusName);
        values.put(T_DESCRIPT, descript);
        values.put(T_TIME, getDateString());
        logdb.insert(TABLE_NAME, null, values);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void insertOperationItem(int type, String descript) {
        ContentValues values = new ContentValues();
        values.put(T_CATEGORY, (Integer) 5);
        values.put(T_TYPE, Integer.valueOf(type));
        values.put(T_DESCRIPT, descript);
        values.put(T_TIME, getDateString());
        logdb.insert(TABLE_NAME, null, values);
    }

    public void insertGuardItem(int category, int num, String descript) {
        boolean needUpdate = true;
        ContentValues values = new ContentValues();
        values.put(T_CATEGORY, Integer.valueOf(category));
        values.put(T_DESCRIPT, descript);
        values.put(T_TIME, getDateString());
        Cursor c = logdb.query(TABLE_NAME, null, "T_CATEGORY =" + category, null, null, null, null);
        if (c != null) {
            if (c.getCount() > 0) {
                needUpdate = true;
                try {
                    if (c.moveToNext()) {
                        values.put(T_NUMBER, Integer.valueOf(c.getInt(c.getColumnIndexOrThrow(T_NUMBER)) + num));
                    }
                } finally {
                    c.close();
                }
            } else {
                values.put(T_NUMBER, Integer.valueOf(num));
                needUpdate = false;
            }
        }
        if (needUpdate) {
            logdb.update(TABLE_NAME, values, "T_CATEGORY =" + category, null);
        } else {
            logdb.insert(TABLE_NAME, null, values);
        }
    }

    public Cursor getItemByCategory(int category) {
        return logdb.rawQuery("select * from logs where t_category=" + category + " order by " + T_TIME + " DESC limit 100", null);
    }

    public Vector<LogItem> getThreatItem() {
        Vector<LogItem> vector = new Vector<>();
        Cursor c = getItemByCategory(4);
        if (c != null) {
            vector = new Vector<>();
            while (c.moveToNext()) {
                try {
                    int itemType = c.getInt(c.getColumnIndexOrThrow(T_TYPE));
                    String filename = c.getString(c.getColumnIndexOrThrow(T_FILE_NAME));
                    String virusname = c.getString(c.getColumnIndexOrThrow(T_VIRUS_NAME));
                    String itemDescript = c.getString(c.getColumnIndexOrThrow(T_DESCRIPT));
                    String itemTime = c.getString(c.getColumnIndexOrThrow(T_TIME));
                    LogItem item = new LogItem();
                    item.type = itemType;
                    item.virusname = virusname;
                    item.filename = filename;
                    item.descript = itemDescript;
                    item.time = itemTime;
                    vector.add(item);
                } finally {
                    c.close();
                }
            }
        }
        return vector;
    }

    public Vector<LogItem> getOperateItem() {
        Vector<LogItem> vector = new Vector<>();
        Cursor c = getItemByCategory(5);
        if (c != null) {
            vector = new Vector<>();
            while (c.moveToNext()) {
                try {
                    int itemType = c.getInt(c.getColumnIndexOrThrow(T_TYPE));
                    String itemDescript = c.getString(c.getColumnIndexOrThrow(T_DESCRIPT));
                    String itemTime = c.getString(c.getColumnIndexOrThrow(T_TIME));
                    LogItem item = new LogItem();
                    item.type = itemType;
                    item.descript = itemDescript;
                    if (itemTime == null) {
                        Date date = dateValueOf(itemTime);
                        date.setTime(1000 * ((date.getTime() / 1000) - ((long) (date.getTimezoneOffset() * 60))));
                        item.time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
                    } else {
                        item.time = itemTime;
                    }
                    vector.add(item);
                } finally {
                    c.close();
                }
            }
        }
        return vector;
    }

    public void clearLogsByCategory(int category) {
        logdb.delete(TABLE_NAME, "T_CATEGORY = " + category, null);
    }

    public String getDateString() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
    }

    public void deleteAllItem() {
        try {
            logdb.delete(TABLE_NAME, null, null);
        } catch (SQLException e) {
        }
    }

    public Vector<LogItem> getRTItem(int type) {
        Cursor cur = logdb.rawQuery("select * from logs where t_type=" + type + " order by " + T_TIME + " DESC limit 50", null);
        Vector<LogItem> vector = new Vector<>();
        Integer num = Integer.valueOf(cur.getCount());
        int typeIndex = cur.getColumnIndexOrThrow(T_TYPE);
        int descriptIndex = cur.getColumnIndexOrThrow(T_DESCRIPT);
        int timeIndex = cur.getColumnIndexOrThrow(T_TIME);
        for (int i = 0; i < num.intValue(); i++) {
            cur.moveToPosition(i);
            int itemType = cur.getInt(typeIndex);
            String itemDescript = cur.getString(descriptIndex);
            String itemTime = cur.getString(timeIndex);
            LogItem item = new LogItem();
            item.type = itemType;
            item.descript = itemDescript;
            if (itemTime != null) {
                Date date = dateValueOf(itemTime);
                date.setTime(1000 * ((date.getTime() / 1000) - ((long) (date.getTimezoneOffset() * 60))));
                item.time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
            } else {
                item.time = itemTime;
            }
            vector.add(item);
        }
        if (cur != null) {
            cur.close();
        }
        return vector;
    }

    public Vector<LogItem> getItems(int type) {
        String sql;
        if (type == 1) {
            sql = "select * from logs order by t_time DESC limit 50";
        } else {
            sql = "select * from logs where t_type=" + type + " order by " + T_TIME + " DESC limit 50";
        }
        Cursor cur = logdb.rawQuery(sql, null);
        Vector<LogItem> vector = new Vector<>();
        Integer num = Integer.valueOf(cur.getCount());
        int typeIndex = cur.getColumnIndexOrThrow(T_TYPE);
        int descriptIndex = cur.getColumnIndexOrThrow(T_DESCRIPT);
        int timeIndex = cur.getColumnIndexOrThrow(T_TIME);
        for (int i = 0; i < num.intValue(); i++) {
            cur.moveToPosition(i);
            int itemType = cur.getInt(typeIndex);
            String itemDescript = cur.getString(descriptIndex);
            String itemTime = cur.getString(timeIndex);
            LogItem item = new LogItem();
            item.type = itemType;
            item.descript = itemDescript;
            if (itemTime != null) {
                Date date = dateValueOf(itemTime);
                date.setTime(1000 * ((date.getTime() / 1000) - ((long) (date.getTimezoneOffset() * 60))));
                item.time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
            } else {
                item.time = itemTime;
            }
            vector.add(item);
        }
        if (cur != null) {
            cur.close();
        }
        return vector;
    }

    public LogItem getUptodateItem(int type) {
        Cursor cur = logdb.rawQuery("select * from logs where t_type=" + type + " order by " + T_TIME + " DESC limit 2", null);
        LogItem item = null;
        if (cur != null) {
            if (Integer.valueOf(cur.getCount()).intValue() > 0) {
                int typeIndex = cur.getColumnIndexOrThrow(T_TYPE);
                int descriptIndex = cur.getColumnIndexOrThrow(T_DESCRIPT);
                int timeIndex = cur.getColumnIndexOrThrow(T_TIME);
                cur.moveToFirst();
                int itemType = cur.getInt(typeIndex);
                String itemDescript = cur.getString(descriptIndex);
                String itemTime = cur.getString(timeIndex);
                item = new LogItem();
                item.type = itemType;
                item.descript = itemDescript;
                item.time = itemTime;
                if (!TextUtils.isEmpty(itemTime)) {
                    Date date = dateValueOf(itemTime);
                    date.setTime(1000 * ((date.getTime() / 1000) - ((long) (date.getTimezoneOffset() * 60))));
                    item.time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
                }
            }
            cur.close();
        }
        return item;
    }

    public LogItem getUptodateBackupTime() {
        Cursor cur = logdb.rawQuery("select * from logs where t_type=20 order by t_time", null);
        LogItem item = null;
        if (cur != null) {
            if (Integer.valueOf(cur.getCount()).intValue() > 0) {
                int typeIndex = cur.getColumnIndexOrThrow(T_TYPE);
                int descriptIndex = cur.getColumnIndexOrThrow(T_DESCRIPT);
                int timeIndex = cur.getColumnIndexOrThrow(T_TIME);
                cur.moveToFirst();
                int itemType = cur.getInt(typeIndex);
                String itemDescript = cur.getString(descriptIndex);
                String itemTime = cur.getString(timeIndex);
                item = new LogItem();
                item.type = itemType;
                item.descript = itemDescript;
                item.time = itemTime;
            }
            cur.close();
        }
        return item;
    }

    public Date dateValueOf(String dateString) {
        if (dateString == null) {
            throw new IllegalArgumentException();
        }
        int firstIndex = dateString.indexOf(45);
        int secondIndex = dateString.indexOf(45, firstIndex + 1);
        int space = dateString.indexOf(32);
        int firstTimeIndex = dateString.indexOf(58);
        int secondTimeIndex = dateString.indexOf(58, firstTimeIndex + 1);
        if (secondIndex == -1 || firstIndex == 0 || secondIndex + 1 == dateString.length() || space < 0 || firstTimeIndex == -1 || secondTimeIndex == -1) {
            throw new IllegalArgumentException();
        }
        int year = Integer.parseInt(dateString.substring(0, firstIndex));
        return new Date(year - 1900, Integer.parseInt(dateString.substring(firstIndex + 1, secondIndex)) - 1, Integer.parseInt(dateString.substring(secondIndex + 1, space)), Integer.parseInt(dateString.substring(space + 1, firstTimeIndex)), Integer.parseInt(dateString.substring(firstTimeIndex + 1, secondTimeIndex)), Integer.parseInt(dateString.substring(secondTimeIndex + 1, dateString.length())));
    }

    public static void insertGuardItemLog(int category, int num, String descript, String dbPath) {
        LogEngine logEngine = new LogEngine();
        logEngine.openDB(dbPath);
        logEngine.insertGuardItem(category, num, descript);
        logEngine.closeDB();
    }

    public static void insertThreatItemLog(int type, String fileName, String virusName, String descript, String dbPath) {
        LogEngine logEngine = new LogEngine();
        logEngine.openDB(dbPath);
        logEngine.insertThreatItem(type, fileName, virusName, descript);
        deleteOldItems(4);
        logEngine.closeDB();
    }

    public static void deleteOldItems(int category) {
        Cursor c = logdb.rawQuery("select * from logs where t_category=" + category + " order by " + T_TIME + " DESC limit " + 100, null);
        String limitTime = null;
        if (c != null) {
            try {
                if (c.getCount() > 99) {
                    c.moveToLast();
                    limitTime = c.getString(c.getColumnIndexOrThrow(T_TIME));
                }
            } finally {
                c.close();
            }
        }
        if (limitTime != null) {
            logdb.execSQL("delete from logs where t_category=" + category + " and " + T_TIME + " < '" + limitTime + "'");
        }
    }

    public static void insertOperationItemLog(int type, String descript, String dbPath) {
        LogEngine logEngine = new LogEngine();
        logEngine.openDB(dbPath);
        logEngine.insertOperationItem(type, descript);
        deleteOldItems(5);
        logEngine.closeDB();
    }
}
