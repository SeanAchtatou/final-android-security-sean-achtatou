package com.netqin.antivirus;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import com.netqin.antivirus.antilost.AntiLostCommon;
import com.netqin.antivirus.common.CommonDefine;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.contact.ContactCommon;
import com.netqin.antivirus.data.PreferenceDataHelper;
import com.netqin.antivirus.net.appupdateservice.BackgroundAppUpdateService;
import com.nqmobile.antivirus_ampro20.R;
import java.io.File;

public class AntiVirusSplash extends Activity {
    private static final String TAG = "AntiVirusSplash";
    private BackgroundAppUpdateService backgroundAppUpdateService;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Preferences pref = new Preferences(this);
        String chanelId = pref.getChanelIdStore();
        String temp = CommonMethod.getChannelId(this);
        if (!chanelId.equals(temp) && !temp.equals(CommonDefine.ANTIVIRUS_PID)) {
            pref.setChanelIdStore(temp);
        }
        if (CommonMethod.isLocalSimpleChinese()) {
            setContentView((int) R.layout.antivirus_splash);
        } else {
            setContentView((int) R.layout.antivirus_splash_en);
        }
        SharedPreferences spf = getSharedPreferences(AntiLostCommon.DELETE_CONTACT, 0);
        String user = spf.getString("user", "");
        boolean user_state = spf.getBoolean("user_state", false);
        if (TextUtils.isEmpty(user) || !user_state) {
            ContactCommon.mContactAccount = "";
        } else {
            ContactCommon.mContactAccount = user;
        }
        deleteFile();
        deleteOldLogDB();
        new Thread(new Runnable() {
            public void run() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                AntiVirusSplash antiVirusSplash = AntiVirusSplash.this;
                final Preferences preferences = pref;
                antiVirusSplash.runOnUiThread(new Runnable() {
                    public void run() {
                        Intent intent;
                        if (preferences.getShowFirstPage()) {
                            PreferenceDataHelper.initWhiteList(AntiVirusSplash.this);
                            intent = new Intent(AntiVirusSplash.this, Disclaimer.class);
                        } else {
                            intent = new Intent(AntiVirusSplash.this, HomeActivity.class);
                        }
                        AntiVirusSplash.this.startActivity(intent);
                        AntiVirusSplash.this.finish();
                    }
                });
            }
        }).start();
    }

    private void deleteOldLogDB() {
        Log.d("tang", "--------------------------------------------------------delete old log DB!");
        deleteDatabase("logs.db3");
    }

    private void deleteFile() {
        File file = new File("/data/data/com.nqmobile.antivirus20/shared_prefs/contact.xml");
        File file2 = new File("/data/data/com.nqmobile.antivirus20/shared_prefs/antilost.xml");
        try {
            if (file.exists() && file.isFile()) {
                file.delete();
            } else if (file2.exists() && file2.isFile()) {
                file2.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
