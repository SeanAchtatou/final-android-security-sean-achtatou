package com.netqin.antivirus.mynetqin;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.netqin.antivirus.cloud.model.xml.XmlUtils;
import com.netqin.antivirus.common.CommonMethod;
import com.nqmobile.antivirus_ampro20.R;

public class MyNetqinChina extends Activity implements AdapterView.OnItemClickListener {
    public static final String KEY_ISALLOWINSTALLOTHER = "isAllowInstallOther";
    public static final String KEY_ISROOTPOWER = "isRootPower";
    /* access modifiers changed from: private */
    public final int[] listIcon = {R.drawable.feedback, R.drawable.sina, R.drawable.tencent, R.drawable.more};
    /* access modifiers changed from: private */
    public final int[] listName = {R.string.label_advice_feedback, R.string.text_sina_microblog, R.string.text_tencent_microblog, R.string.label_recommend_more_netqin};
    private BuyListAdapter mListAdapter;

    private class ListViewHolder {
        ImageView icon;
        TextView name;

        private ListViewHolder() {
        }

        /* synthetic */ ListViewHolder(MyNetqinChina myNetqinChina, ListViewHolder listViewHolder) {
            this();
        }
    }

    private class BuyListAdapter extends BaseAdapter {
        private Context mContext;
        private LayoutInflater mInflater = LayoutInflater.from(this.mContext);

        public BuyListAdapter(Context context) {
            this.mContext = context;
        }

        public int getCount() {
            return MyNetqinChina.this.listName.length;
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ListViewHolder holder;
            if (convertView == null) {
                convertView = this.mInflater.inflate((int) R.layout.mynetqin_item, (ViewGroup) null);
                holder = new ListViewHolder(MyNetqinChina.this, null);
                holder.icon = (ImageView) convertView.findViewById(R.id.mynetqin_item_icon);
                holder.name = (TextView) convertView.findViewById(R.id.mynetqin_item_title);
                convertView.setTag(holder);
            } else {
                holder = (ListViewHolder) convertView.getTag();
            }
            holder.icon.setImageResource(MyNetqinChina.this.listIcon[position]);
            holder.name.setText(MyNetqinChina.this.listName[position]);
            convertView.setId(MyNetqinChina.this.listName[position]);
            return convertView;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.mynetqin);
        setRequestedOrientation(1);
        ((TextView) findViewById(R.id.activity_name)).setText((int) R.string.label_mynetqin);
        TextView myNetqinTitle = (TextView) findViewById(R.id.mynetqin_main_valid);
        String netqinStr = getString(R.string.text_nq_account);
        String userId = CommonMethod.getConfigWithStringValue(this, "netqin", XmlUtils.LABEL_CLIENTINFO_UID, "");
        if (!TextUtils.isEmpty(userId)) {
            myNetqinTitle.setText(String.valueOf(netqinStr) + " " + userId);
        }
        this.mListAdapter = new BuyListAdapter(this);
        ListView listview = (ListView) findViewById(R.id.mynetqin_listview);
        listview.setAdapter((ListAdapter) this.mListAdapter);
        listview.setOnItemClickListener(this);
    }

    public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
        switch (v.getId()) {
            case R.string.label_advice_feedback /*2131427596*/:
                clickAdviceFeedback();
                return;
            case R.string.label_recommend_more_netqin /*2131427902*/:
                clickMoreNetQin();
                return;
            case R.string.text_sina_microblog /*2131427905*/:
                clickSinaBlog();
                return;
            case R.string.text_tencent_microblog /*2131428274*/:
                clickTencentBlog();
                return;
            default:
                return;
        }
    }

    private void clickAdviceFeedback() {
        String uriRes = "http://www.netqin.com/feedback/report.jsp?" + CommonMethod.getUploadConfigFeedBack(this);
        CommonMethod.logDebug("netqin", "MyNetqin(Feedback): " + uriRes);
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(uriRes)));
    }

    private void clickSinaBlog() {
        CommonMethod.logDebug("netqin", "MyNetqin(Sina Micro Blog): " + "http://t.sina.com.cn/netqin");
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://t.sina.com.cn/netqin")));
    }

    private void clickTencentBlog() {
        CommonMethod.logDebug("netqin", "MyNetqin(Tencent Micro Blog): " + "http://t.qq.com/netqin/");
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://t.qq.com/netqin/")));
    }

    private void clickMoreNetQin() {
        Uri uri;
        try {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=netqin")));
        } catch (Exception e) {
            Exception e2 = e;
            if (CommonMethod.isLocalSimpleChinese()) {
                uri = Uri.parse("http://www.netqin.com/");
            } else {
                uri = Uri.parse("http://www.netqin.com/en/");
            }
            startActivity(new Intent("android.intent.action.VIEW", uri));
            e2.printStackTrace();
        }
    }
}
