package com.netqin.antivirus.mynetqin;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.netqin.antivirus.cloud.model.xml.XmlUtils;
import com.netqin.antivirus.common.CommonMethod;
import com.nqmobile.antivirus_ampro20.R;

public class MyNetqin extends Activity implements AdapterView.OnItemClickListener {
    /* access modifiers changed from: private */
    public final int[] listIcon = {R.drawable.feedback, R.drawable.facebook, R.drawable.twitter, R.drawable.more};
    /* access modifiers changed from: private */
    public final int[] listName = {R.string.label_advice_feedback, R.string.label_facebook, R.string.label_twitter, R.string.label_recommend_more_netqin};
    private BuyListAdapter mListAdapter;

    private class ListViewHolder {
        ImageView icon;
        TextView name;

        private ListViewHolder() {
        }

        /* synthetic */ ListViewHolder(MyNetqin myNetqin, ListViewHolder listViewHolder) {
            this();
        }
    }

    private class BuyListAdapter extends BaseAdapter {
        private Context mContext;
        private LayoutInflater mInflater = LayoutInflater.from(this.mContext);

        public BuyListAdapter(Context context) {
            this.mContext = context;
        }

        public int getCount() {
            return MyNetqin.this.listName.length;
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ListViewHolder holder;
            if (convertView == null) {
                convertView = this.mInflater.inflate((int) R.layout.mynetqin_item, (ViewGroup) null);
                holder = new ListViewHolder(MyNetqin.this, null);
                holder.icon = (ImageView) convertView.findViewById(R.id.mynetqin_item_icon);
                holder.name = (TextView) convertView.findViewById(R.id.mynetqin_item_title);
                convertView.setTag(holder);
            } else {
                holder = (ListViewHolder) convertView.getTag();
            }
            holder.icon.setImageResource(MyNetqin.this.listIcon[position]);
            holder.name.setText(MyNetqin.this.listName[position]);
            convertView.setId(MyNetqin.this.listName[position]);
            return convertView;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.mynetqin);
        setRequestedOrientation(1);
        ((TextView) findViewById(R.id.activity_name)).setText((int) R.string.label_mynetqin);
        TextView myNetqinTitle = (TextView) findViewById(R.id.mynetqin_main_valid);
        String netqinStr = getString(R.string.text_nq_account);
        String userId = CommonMethod.getConfigWithStringValue(this, "netqin", XmlUtils.LABEL_CLIENTINFO_UID, "");
        if (!TextUtils.isEmpty(userId)) {
            myNetqinTitle.setText(String.valueOf(netqinStr) + " " + userId);
        }
        this.mListAdapter = new BuyListAdapter(this);
        ListView listview = (ListView) findViewById(R.id.mynetqin_listview);
        listview.setAdapter((ListAdapter) this.mListAdapter);
        listview.setOnItemClickListener(this);
    }

    public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
        switch (v.getId()) {
            case R.string.label_advice_feedback /*2131427596*/:
                clickAdviceFeedback();
                return;
            case R.string.label_facebook /*2131427841*/:
                clickFacebook();
                return;
            case R.string.label_twitter /*2131427842*/:
                clickTwitter();
                return;
            case R.string.label_recommend_more_netqin /*2131427902*/:
                clickMoreNetQin();
                return;
            default:
                return;
        }
    }

    private void clickAdviceFeedback() {
        String uriRes = "http://www.netqin.com/feedback/report.jsp?" + CommonMethod.getUploadConfigFeedBack(this);
        CommonMethod.logDebug("netqin", "MyNetqin(Feedback): " + uriRes);
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(uriRes)));
    }

    private void clickFacebook() {
        CommonMethod.logDebug("netqin", "MyNetqin(Fackbook): " + "http://www.facebook.com/NetQinMobile");
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.facebook.com/NetQinMobile")));
    }

    private void clickTwitter() {
        CommonMethod.logDebug("netqin", "MyNetqin(Twitter): " + "http://twitter.com/netqin");
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://twitter.com/netqin")));
    }

    private void clickMoreNetQin() {
        Uri uri;
        try {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=netqin")));
        } catch (Exception e) {
            Exception e2 = e;
            if (CommonMethod.isLocalSimpleChinese()) {
                uri = Uri.parse("http://www.netqin.com/");
            } else {
                uri = Uri.parse("http://www.netqin.com/en/");
            }
            startActivity(new Intent("android.intent.action.VIEW", uri));
            e2.printStackTrace();
        }
    }
}
