package com.netqin.antivirus.privatesoft;

import SHcMjZX.DD02zqNU;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.netqin.antivirus.cloud.model.CloudApkInfo;
import com.netqin.antivirus.cloud.model.CloudApkInfoList;
import com.netqin.antivirus.cloud.model.CloudHandler;
import com.netqin.antivirus.data.PackageElement;
import com.nqmobile.antivirus_ampro20.R;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class VisitPrivacy extends Activity {
    private static final String ACCESS_COARSE_LOCATION = "android.permission.ACCESS_COARSE_LOCATION";
    private static final String ACCESS_FINE_LOCATION = "android.permission.ACCESS_FINE_LOCATION";
    private static final int OK = 1;
    private static final String READ_CONTACTS = "android.permission.READ_CONTACTS";
    private static final String READ_PHONE_STATE = "android.permission.READ_PHONE_STATE";
    private static final String READ_SMS = "android.permission.READ_SMS";
    private static final String TAG = "VisitDirectories";
    private static ProgressDialog progressDialog;
    /* access modifiers changed from: private */
    public VisitPrivacyAdapter adapter;
    List<PackageInfo> apkList = new ArrayList();
    /* access modifiers changed from: private */
    public Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:
                    VisitPrivacy.this.listView.setAdapter((ListAdapter) VisitPrivacy.this.adapter);
                    VisitPrivacy.this.adapter.notifyDataSetChanged();
                    VisitPrivacy.this.setRightNum();
                    VisitPrivacy.dismissLoading();
                    return;
                default:
                    return;
            }
        }
    };
    private Intent intent;
    /* access modifiers changed from: private */
    public ListView listView;
    private Runnable mInitApkInfo = new Runnable() {
        public void run() {
            VisitPrivacy.this.packageManager = VisitPrivacy.this.getPackageManager();
            VisitPrivacy.this.apkList = VisitPrivacy.this.getList();
            VisitPrivacy.this.packageElementList.clear();
            VisitPrivacy.this.visit_right_num = VisitPrivacy.this.apkList.size();
            for (PackageInfo info : VisitPrivacy.this.apkList) {
                PackageElement element = new PackageElement();
                element.setPackageInfo(info);
                VisitPrivacy.this.packageElementList.add(element);
            }
            ArrayList<PackageElement> mPkg3rd = new ArrayList<>();
            ArrayList<PackageElement> mPkgUnknown = new ArrayList<>();
            ArrayList<PackageElement> mPkgCommon = new ArrayList<>();
            ArrayList<PackageElement> mPkgRisk = new ArrayList<>();
            for (PackageElement pkg : VisitPrivacy.this.packageElementList) {
                try {
                    String packageName = pkg.getPackageInfo().packageName;
                    String sb = new StringBuilder(String.valueOf(DD02zqNU.DLLIxskak(VisitPrivacy.this.packageManager, packageName, 1).versionCode)).toString();
                    String str = DD02zqNU.DLLIxskak(VisitPrivacy.this.packageManager, packageName, 1).versionName;
                    ApplicationInfo applicationInfo = VisitPrivacy.this.packageManager.getApplicationInfo(packageName, 1);
                    CloudApkInfo apkData = null;
                    List<CloudApkInfo> list = CloudApkInfoList.getApkInfoList(VisitPrivacy.this);
                    if (list != null) {
                        int i = 0;
                        while (true) {
                            if (i >= list.size()) {
                                break;
                            } else if (packageName.equals(list.get(i).getPkgName())) {
                                apkData = list.get(i);
                                break;
                            } else {
                                i++;
                            }
                        }
                    }
                    pkg.setPackageName(packageName);
                    pkg.haveServerData = true;
                    pkg.setSscurity("40");
                    pkg.setScore("0");
                    if (apkData != null) {
                        String securityId = apkData.getSecurity();
                        pkg.setScore(apkData.getScore());
                        pkg.setSscurity(securityId);
                        pkg.setSscurityDesc(apkData.getSecurityDesc());
                        if ("10".equals(securityId)) {
                            pkg.isChecked = true;
                            mPkgRisk.add(pkg);
                        } else {
                            mPkgCommon.add(pkg);
                        }
                    } else {
                        mPkgUnknown.add(pkg);
                    }
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
            }
            Iterator<PackageElement> iterator = mPkgCommon.iterator();
            while (iterator.hasNext()) {
                mPkgRisk.add((PackageElement) iterator.next());
            }
            Iterator<PackageElement> iterator2 = mPkgUnknown.iterator();
            while (iterator2.hasNext()) {
                mPkgRisk.add(iterator2.next());
            }
            mPkg3rd.clear();
            Iterator<PackageElement> iterator3 = mPkgRisk.iterator();
            while (iterator3.hasNext()) {
                mPkg3rd.add(iterator3.next());
            }
            VisitPrivacy.this.adapter = new VisitPrivacyAdapter(VisitPrivacy.this, VisitPrivacy.this.packageManager, mPkg3rd);
            VisitPrivacy.this.adapter.notifyDataSetChanged();
            VisitPrivacy.this.visit_right_num = VisitPrivacy.this.packageElementList.size();
            Message message = new Message();
            message.what = 1;
            message.arg1 = VisitPrivacy.this.visit_right_num;
            VisitPrivacy.this.handler.sendMessage(message);
        }
    };
    private String mPackageName;
    List<PackageElement> packageElementList = new ArrayList();
    /* access modifiers changed from: private */
    public PackageManager packageManager = null;
    private TextView privacy_right;
    private TextView privacy_right_desc;
    private String right;
    /* access modifiers changed from: private */
    public int visit_right_num;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.showprivacy);
        this.mPackageName = getPackageName();
        showLoading(this, getString(R.string.label_scaning), getString(R.string.text_soft_loding));
        ((TextView) findViewById(R.id.activity_name)).setText((int) R.string.label_privacyprotection);
        this.packageManager = getPackageManager();
        this.intent = getIntent();
        this.right = this.intent.getStringExtra("right");
        this.privacy_right = (TextView) findViewById(R.id.privacy_right);
        this.privacy_right_desc = (TextView) findViewById(R.id.privacy_right_desc);
        this.visit_right_num = this.intent.getExtras().getInt("visit_right_num", 0);
        setRightNum();
        try {
            this.listView = (ListView) findViewById(R.id.tehuikuaixundata);
            List<HashMap<String, Object>> dataMap = new ArrayList<>();
            for (PackageInfo packageInfo : this.apkList) {
                if (packageInfo != null) {
                    Drawable drawable = this.packageManager.getApplicationInfo(packageInfo.packageName, 0).loadIcon(this.packageManager);
                    HashMap<String, Object> item = new HashMap<>();
                    item.put(CloudHandler.KEY_ITEM_TITLE, this.packageManager.getApplicationInfo(packageInfo.packageName, 1).loadLabel(this.packageManager));
                    item.put(CloudHandler.KEY_ITEM_ICON, drawable);
                    dataMap.add(item);
                }
            }
            this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    PackageElement item = (PackageElement) ((ListView) parent).getItemAtPosition(position);
                    try {
                        ApplicationInfo applicationInfo = VisitPrivacy.this.packageManager.getApplicationInfo(item.getPackageName(), 1);
                        Intent intent = new Intent(VisitPrivacy.this, SoftDetail.class);
                        intent.putExtra("packageName", item.getPackageName());
                        VisitPrivacy.this.startActivity(intent);
                    } catch (PackageManager.NameNotFoundException e) {
                        Toast.makeText(VisitPrivacy.this, VisitPrivacy.this.getString(R.string.apk_uninstall), 1).show();
                        e.printStackTrace();
                    }
                }
            });
        } catch (Throwable th) {
        }
    }

    /* access modifiers changed from: private */
    public void setRightNum() {
        String str = null;
        if (this.right.equals("visit_directories")) {
            str = getResources().getString(R.string.tab_title_mobile_visit_directories2, Integer.valueOf(this.visit_right_num));
            this.privacy_right_desc.setText((int) R.string.tab_title_mobile_visit_directories_desc3);
        } else if (this.right.equals("visit_sms")) {
            str = getResources().getString(R.string.tab_title_visit_sms2, Integer.valueOf(this.visit_right_num));
            this.privacy_right_desc.setText((int) R.string.tab_title_visit_sms_desc3);
        } else if (this.right.equals("visit_location")) {
            str = getResources().getString(R.string.tab_title_visit_location2, Integer.valueOf(this.visit_right_num));
            this.privacy_right_desc.setText((int) R.string.tab_title_visit_location_desc3);
        } else if (this.right.equals("visit_identity")) {
            str = getResources().getString(R.string.tab_title_visit_identity2, Integer.valueOf(this.visit_right_num));
            this.privacy_right_desc.setText((int) R.string.tab_title_visit_identity_desc3);
        }
        this.privacy_right.setText(str);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.adapter != null) {
            this.adapter.notifyDataSetChanged();
        }
        new Thread(this.mInitApkInfo).start();
    }

    public static void showLoading(Activity a, String title, String message) {
        progressDialog = ProgressDialog.show(a, title, message, true);
        progressDialog.setCancelable(true);
    }

    public static void dismissLoading() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    /* access modifiers changed from: private */
    public List<PackageInfo> getList() {
        String[] packagePermissions;
        this.apkList.clear();
        for (PackageInfo packageInfo : this.packageManager.getInstalledPackages(0)) {
            if (packageInfo.applicationInfo.publicSourceDir.indexOf("system") == -1) {
                try {
                    PackageInfo packageInfo2 = DD02zqNU.DLLIxskak(this.packageManager, packageInfo.packageName, 4096);
                    if (!packageInfo2.packageName.equals(this.mPackageName) && (packagePermissions = packageInfo2.requestedPermissions) != null) {
                        for (int j = 0; j < packagePermissions.length; j++) {
                            if (this.right.equals("visit_directories")) {
                                if (packagePermissions[j].equalsIgnoreCase(READ_CONTACTS)) {
                                    this.apkList.add(packageInfo2);
                                }
                            } else if (this.right.equals("visit_sms")) {
                                if (packagePermissions[j].equalsIgnoreCase(READ_SMS)) {
                                    this.apkList.add(packageInfo2);
                                }
                            } else if (this.right.equals("visit_location")) {
                                if ((packagePermissions[j].equalsIgnoreCase(ACCESS_COARSE_LOCATION) || packagePermissions[j].equalsIgnoreCase(ACCESS_FINE_LOCATION)) && !this.apkList.contains(packageInfo2)) {
                                    this.apkList.add(packageInfo2);
                                }
                            } else if (this.right.equals("visit_identity") && packagePermissions[j].equalsIgnoreCase(READ_PHONE_STATE)) {
                                this.apkList.add(packageInfo2);
                            }
                        }
                    }
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
        return this.apkList;
    }

    public String cert_rsa_path() {
        return getFilesDir() + CloudHandler.Cert_rsa_path;
    }
}
