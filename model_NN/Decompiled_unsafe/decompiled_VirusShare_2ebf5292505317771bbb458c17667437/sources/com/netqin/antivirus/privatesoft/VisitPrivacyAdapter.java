package com.netqin.antivirus.privatesoft;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import com.netqin.antivirus.cloud.model.CloudHandler;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.data.PackageElement;
import com.nqmobile.antivirus_ampro20.R;
import java.util.ArrayList;
import java.util.List;

public class VisitPrivacyAdapter extends BaseAdapter {
    private ApplicationInfo applicationInfo;
    private Context mContext;
    private LayoutInflater mInflater;
    List<PackageElement> packageElementList = new ArrayList();
    private PackageManager packageManager = null;

    private class ListViewHolder {
        ImageView icon;
        TextView name;
        TextView securityLevel;
        RatingBar securityStart;

        private ListViewHolder() {
        }

        /* synthetic */ ListViewHolder(VisitPrivacyAdapter visitPrivacyAdapter, ListViewHolder listViewHolder) {
            this();
        }
    }

    public VisitPrivacyAdapter(Context context, PackageManager packageManager2, List<PackageElement> packageElementsList) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(this.mContext);
        this.packageManager = packageManager2;
        this.packageElementList = packageElementsList;
    }

    public int getCount() {
        return this.packageElementList.size();
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public PackageElement getItem(int position) {
        return this.packageElementList.get(position);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ListViewHolder holder;
        if (convertView == null) {
            convertView = this.mInflater.inflate((int) R.layout.showprivacyitem, (ViewGroup) null);
            holder = new ListViewHolder(this, null);
            holder.icon = (ImageView) convertView.findViewById(R.id.directoresicon);
            holder.name = (TextView) convertView.findViewById(R.id.title);
            holder.securityLevel = (TextView) convertView.findViewById(R.id.security_level);
            holder.securityStart = (RatingBar) convertView.findViewById(R.id.virus_list_ratingbar);
            convertView.setTag(holder);
        } else {
            holder = (ListViewHolder) convertView.getTag();
        }
        PackageElement packageInfo = getItem(position);
        String packageName = packageInfo.getPackageName();
        if (CommonMethod.hasSoft(this.mContext, packageName)) {
            try {
                holder.name.setText(this.packageManager.getApplicationInfo(packageName, 1).loadLabel(this.packageManager));
                this.applicationInfo = this.packageManager.getApplicationInfo(packageName, 1);
                holder.icon.setImageDrawable(this.applicationInfo.loadIcon(this.packageManager));
                if (packageInfo.getScore() != null) {
                    holder.securityStart.setRating(Float.valueOf(packageInfo.getScore()).floatValue());
                } else {
                    holder.securityStart.setRating(0.0f);
                }
                if (packageInfo.getSecurityDesc() != null) {
                    holder.securityLevel.setText(packageInfo.getSecurityDesc());
                } else {
                    holder.securityLevel.setText((int) R.string.text_security_level_unknown);
                }
                if (packageInfo.getSecurity() == null || !packageInfo.getSecurity().equals("10")) {
                    holder.securityLevel.setTextColor(-16777216);
                } else {
                    holder.securityLevel.setTextColor(-65536);
                }
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }
        return convertView;
    }

    public String cert_rsa_path() {
        return this.mContext.getFilesDir() + CloudHandler.Cert_rsa_path;
    }
}
