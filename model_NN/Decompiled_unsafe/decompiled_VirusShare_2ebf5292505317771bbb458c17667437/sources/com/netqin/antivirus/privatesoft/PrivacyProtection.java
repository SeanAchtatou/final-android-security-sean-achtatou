package com.netqin.antivirus.privatesoft;

import SHcMjZX.DD02zqNU;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.netqin.antivirus.NqUtil;
import com.nqmobile.antivirus_ampro20.R;
import java.util.ArrayList;
import java.util.List;

public class PrivacyProtection extends Activity implements AdapterView.OnItemClickListener {
    private static final String ACCESS_COARSE_LOCATION = "android.permission.ACCESS_COARSE_LOCATION";
    private static final String ACCESS_FINE_LOCATION = "android.permission.ACCESS_FINE_LOCATION";
    private static final int OK = 1;
    private static final String READ_CONTACTS = "android.permission.READ_CONTACTS";
    private static final String READ_PHONE_STATE = "android.permission.READ_PHONE_STATE";
    private static final String READ_SMS = "android.permission.READ_SMS";
    private static Integer SDK = null;
    private static final String TAG = "AntiVirusMain";
    List<PackageInfo> directoriesList = new ArrayList();
    /* access modifiers changed from: private */
    public Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:
                    PrivacyProtection.this.mListAdapter = new PrivacyProtectionListAdapter(PrivacyProtection.this, PrivacyProtection.this.visit_directories_num, PrivacyProtection.this.visit_sms_num, PrivacyProtection.this.visit_location_num, PrivacyProtection.this.visit_identity_num);
                    PrivacyProtection.this.listview.setAdapter((ListAdapter) PrivacyProtection.this.mListAdapter);
                    PrivacyProtection.this.listview.setOnItemClickListener(PrivacyProtection.this);
                    PrivacyProtection.this.mListAdapter.notifyDataSetChanged();
                    VisitPrivacy.dismissLoading();
                    return;
                default:
                    return;
            }
        }
    };
    List<PackageInfo> identityList = new ArrayList();
    /* access modifiers changed from: private */
    public ListView listview;
    List<PackageInfo> locationList = new ArrayList();
    /* access modifiers changed from: private */
    public BaseAdapter mListAdapter = null;
    List<PackageInfo> smsList = new ArrayList();
    /* access modifiers changed from: private */
    public int visit_directories_num = 0;
    /* access modifiers changed from: private */
    public int visit_identity_num = 0;
    /* access modifiers changed from: private */
    public int visit_location_num = 0;
    /* access modifiers changed from: private */
    public int visit_sms_num = 0;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.privacyprotection);
        VisitPrivacy.showLoading(this, getString(R.string.label_scaning), getString(R.string.text_soft_loding));
        ((TextView) findViewById(R.id.activity_name)).setText((int) R.string.label_privacyprotection);
        this.listview = (ListView) findViewById(R.id.privacy_listview);
        this.mListAdapter = new PrivacyProtectionListAdapter(this, this.visit_directories_num, this.visit_sms_num, this.visit_location_num, this.visit_identity_num);
        this.listview.setAdapter((ListAdapter) this.mListAdapter);
        this.listview.setOnItemClickListener(this);
        this.mListAdapter.notifyDataSetChanged();
    }

    public void onResume() {
        super.onResume();
        new Thread(new Runnable() {
            public void run() {
                PrivacyProtection.this.handleSoft();
                Message message = new Message();
                message.what = 1;
                PrivacyProtection.this.handler.sendMessage(message);
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public void handleSoft() {
        this.visit_directories_num = 0;
        this.visit_sms_num = 0;
        this.visit_location_num = 0;
        this.visit_identity_num = 0;
        PackageManager packageManager = getPackageManager();
        List<PackageInfo> packageInfoList = packageManager.getInstalledPackages(0);
        String netQinPackageName = getPackageName();
        for (PackageInfo packageInfo : packageInfoList) {
            if (packageInfo.applicationInfo.publicSourceDir.indexOf("system") == -1 && !packageInfo.packageName.equals(netQinPackageName)) {
                try {
                    PackageInfo packageInfo2 = DD02zqNU.DLLIxskak(packageManager, packageInfo.packageName, 4096);
                    String[] packagePermissions = packageInfo2.requestedPermissions;
                    if (packagePermissions != null) {
                        for (int j = 0; j < packagePermissions.length; j++) {
                            if (packagePermissions[j].equalsIgnoreCase(READ_CONTACTS)) {
                                this.visit_directories_num++;
                            }
                            if (packagePermissions[j].equalsIgnoreCase(READ_SMS)) {
                                this.visit_sms_num++;
                            }
                            if ((packagePermissions[j].equalsIgnoreCase(ACCESS_COARSE_LOCATION) || packagePermissions[j].equalsIgnoreCase(ACCESS_FINE_LOCATION)) && !this.locationList.contains(packageInfo2)) {
                                this.visit_location_num++;
                                this.locationList.add(packageInfo2);
                            }
                            if (packagePermissions[j].equalsIgnoreCase(READ_PHONE_STATE)) {
                                this.visit_identity_num++;
                            }
                        }
                    }
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.addvice_feedback_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.updatedb_advice_feedback /*2131558847*/:
                NqUtil.clickAdviceFeedback(this);
                return true;
            default:
                return true;
        }
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        switch (view.getId()) {
            case R.string.tab_title_mobile_visit_directories /*2131427914*/:
                Intent intent = new Intent(this, VisitPrivacy.class);
                intent.putExtra("visit_right_num", this.visit_directories_num);
                intent.putExtra("right", "visit_directories");
                startActivity(intent);
                return;
            case R.string.tab_title_visit_sms /*2131427919*/:
                Intent intent2 = new Intent(this, VisitPrivacy.class);
                intent2.putExtra("visit_right_num", this.visit_sms_num);
                intent2.putExtra("right", "visit_sms");
                startActivity(intent2);
                return;
            case R.string.tab_title_visit_location /*2131427924*/:
                Intent intent3 = new Intent(this, VisitPrivacy.class);
                intent3.putExtra("visit_right_num", this.visit_location_num);
                intent3.putExtra("right", "visit_location");
                startActivity(intent3);
                return;
            case R.string.tab_title_visit_identity /*2131427929*/:
                Intent intent4 = new Intent(this, VisitPrivacy.class);
                intent4.putExtra("visit_right_num", this.visit_identity_num);
                intent4.putExtra("right", "visit_identity");
                startActivity(intent4);
                return;
            default:
                return;
        }
    }

    public static String formatNum(int num) {
        if (num < 10) {
            return "0" + num;
        }
        return new StringBuilder().append(num).toString();
    }

    public static boolean isFroyoSDK() {
        if (SDK == null) {
            SDK = Integer.valueOf(Integer.parseInt(Build.VERSION.SDK));
        }
        if (SDK.intValue() >= 8) {
            return true;
        }
        return false;
    }
}
