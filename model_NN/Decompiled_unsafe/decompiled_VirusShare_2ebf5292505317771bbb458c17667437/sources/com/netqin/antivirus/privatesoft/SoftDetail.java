package com.netqin.antivirus.privatesoft;

import SHcMjZX.DD02zqNU;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.netqin.antivirus.cloud.model.CloudHandler;
import com.netqin.antivirus.packagemanager.ApkInfoActivity;
import com.nqmobile.antivirus_ampro20.R;

public class SoftDetail extends Activity implements View.OnClickListener {
    private static final String ACCESS_COARSE_LOCATION = "android.permission.ACCESS_COARSE_LOCATION";
    private static final String ACCESS_FINE_LOCATION = "android.permission.ACCESS_FINE_LOCATION";
    private static final String READ_CONTACTS = "android.permission.READ_CONTACTS";
    private static final String READ_PHONE_STATE = "android.permission.READ_PHONE_STATE";
    private static final String READ_SMS = "android.permission.READ_SMS";
    private static final String TAG = "SoftDetail";
    private ApplicationInfo applicationInfo;
    private LinearLayout contactsLayout;
    private LinearLayout identityLayout;
    private Intent intent;
    private LinearLayout locationLayout;
    private Button lookUpDetail;
    private PackageManager packageManager;
    private String packageName;
    private LinearLayout smsLayout;
    private ImageView softIcon;
    private String softName;
    private TextView softTitle;
    private TextView softVersion;
    private Button unstallSoft;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.softdetail);
        ((TextView) findViewById(R.id.activity_name)).setText((int) R.string.text_soft_right);
        this.softIcon = (ImageView) findViewById(R.id.soft_icon);
        this.softTitle = (TextView) findViewById(R.id.soft_title);
        this.softVersion = (TextView) findViewById(R.id.soft_version);
        this.lookUpDetail = (Button) findViewById(R.id.soft_detail);
        this.unstallSoft = (Button) findViewById(R.id.soft_unstall);
        this.lookUpDetail.setOnClickListener(this);
        this.unstallSoft.setOnClickListener(this);
        this.contactsLayout = (LinearLayout) findViewById(R.id.soft_right_contacts_layout);
        this.smsLayout = (LinearLayout) findViewById(R.id.soft_right_sms_layout);
        this.locationLayout = (LinearLayout) findViewById(R.id.soft_right_location_layout);
        this.identityLayout = (LinearLayout) findViewById(R.id.soft_right_identity_layout);
        this.contactsLayout.setVisibility(8);
        this.smsLayout.setVisibility(8);
        this.locationLayout.setVisibility(8);
        this.identityLayout.setVisibility(8);
        this.intent = getIntent();
        this.packageName = this.intent.getStringExtra("packageName");
        this.packageManager = getPackageManager();
        try {
            this.applicationInfo = this.packageManager.getApplicationInfo(this.packageName, 1);
            this.softName = (String) this.packageManager.getApplicationInfo(this.packageName, 1).loadLabel(this.packageManager);
            this.softTitle.setText(this.softName);
            this.softVersion.setText(String.valueOf(getString(R.string.text_soft_test_version)) + ": " + DD02zqNU.DLLIxskak(this.packageManager, this.packageName, 1).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        this.softIcon.setImageDrawable(this.applicationInfo.loadIcon(this.packageManager));
        try {
            String[] packagePermissions = DD02zqNU.DLLIxskak(this.packageManager, this.packageName, 4096).requestedPermissions;
            if (packagePermissions != null) {
                for (int j = 0; j < packagePermissions.length; j++) {
                    if (packagePermissions[j].equalsIgnoreCase(READ_CONTACTS)) {
                        this.contactsLayout.setVisibility(0);
                    }
                    if (packagePermissions[j].equalsIgnoreCase(READ_SMS)) {
                        this.smsLayout.setVisibility(0);
                    }
                    if (packagePermissions[j].equalsIgnoreCase(ACCESS_COARSE_LOCATION) || packagePermissions[j].equalsIgnoreCase(ACCESS_FINE_LOCATION)) {
                        this.locationLayout.setVisibility(0);
                    }
                    if (packagePermissions[j].equalsIgnoreCase(READ_PHONE_STATE)) {
                        this.identityLayout.setVisibility(0);
                    }
                }
            }
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.packageManager = getPackageManager();
        try {
            this.applicationInfo = this.packageManager.getApplicationInfo(this.packageName, 1);
        } catch (PackageManager.NameNotFoundException e) {
            finish();
            e.printStackTrace();
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.soft_unstall /*2131558795*/:
                startActivity(new Intent("android.intent.action.DELETE", Uri.parse("package:" + this.packageName)));
                return;
            case R.id.soft_detail /*2131558796*/:
                Intent intent2 = new Intent(this, ApkInfoActivity.class);
                intent2.putExtra("package_name", this.packageName);
                intent2.putExtra("apk_name", this.softName);
                startActivity(intent2);
                return;
            default:
                return;
        }
    }

    public String cert_rsa_path() {
        return getFilesDir() + CloudHandler.Cert_rsa_path;
    }
}
