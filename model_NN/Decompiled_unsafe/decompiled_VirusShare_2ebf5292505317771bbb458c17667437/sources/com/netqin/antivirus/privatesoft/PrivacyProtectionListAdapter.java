package com.netqin.antivirus.privatesoft;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.nqmobile.antivirus_ampro20.R;

public class PrivacyProtectionListAdapter extends BaseAdapter {
    private final int[] listDesc = {R.string.tab_title_mobile_visit_directories_desc, R.string.tab_title_visit_sms_desc, R.string.tab_title_visit_location_desc, R.string.tab_title_visit_identity_desc};
    private final int[] listTitle = {R.string.tab_title_mobile_visit_directories, R.string.tab_title_visit_sms, R.string.tab_title_visit_location, R.string.tab_title_visit_identity};
    private Context mContext;
    private LayoutInflater mInflater;
    private int visit_directories_num = 0;
    private int visit_identity_num = 0;
    private int visit_location_num = 0;
    private int visit_sms_num = 0;

    private class ListViewHolder {
        TextView privacy_right;
        TextView privacy_right_desc;
        TextView privacy_softnum;

        private ListViewHolder() {
        }

        /* synthetic */ ListViewHolder(PrivacyProtectionListAdapter privacyProtectionListAdapter, ListViewHolder listViewHolder) {
            this();
        }
    }

    public PrivacyProtectionListAdapter(Context context, int visit_directories_num2, int visit_sms_num2, int visit_location_num2, int visit_identity_num2) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(this.mContext);
        this.visit_directories_num = visit_directories_num2;
        this.visit_sms_num = visit_sms_num2;
        this.visit_location_num = visit_location_num2;
        this.visit_identity_num = visit_identity_num2;
    }

    public int getCount() {
        return this.listTitle.length;
    }

    public Object getItem(int position) {
        return Integer.valueOf(position);
    }

    public long getItemId(int position) {
        return (long) this.listTitle[position];
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View convertView2 = this.mInflater.inflate((int) R.layout.privacyprotection_item, (ViewGroup) null);
        ListViewHolder holder = new ListViewHolder(this, null);
        holder.privacy_softnum = (TextView) convertView2.findViewById(R.id.privacy_softnum);
        holder.privacy_right = (TextView) convertView2.findViewById(R.id.privacy_right);
        holder.privacy_right_desc = (TextView) convertView2.findViewById(R.id.privacy_right_desc);
        convertView2.setTag(holder);
        if (position == 0) {
            holder.privacy_softnum.setText(PrivacyProtection.formatNum(this.visit_directories_num));
        }
        if (position == 1) {
            holder.privacy_softnum.setText(PrivacyProtection.formatNum(this.visit_sms_num));
        }
        if (position == 2) {
            holder.privacy_softnum.setText(PrivacyProtection.formatNum(this.visit_location_num));
        }
        if (position == 3) {
            holder.privacy_softnum.setText(PrivacyProtection.formatNum(this.visit_identity_num));
        }
        if (position == 0) {
            holder.privacy_softnum.setVisibility(0);
        }
        convertView2.setId(this.listTitle[position]);
        holder.privacy_right.setText(this.listTitle[position]);
        holder.privacy_right_desc.setText(this.listDesc[position]);
        return convertView2;
    }
}
