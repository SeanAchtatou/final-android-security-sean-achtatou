package com.netqin.antivirus.networkmanager.model;

public interface IOperation {
    void operationEnded();

    void operationStarted();
}
