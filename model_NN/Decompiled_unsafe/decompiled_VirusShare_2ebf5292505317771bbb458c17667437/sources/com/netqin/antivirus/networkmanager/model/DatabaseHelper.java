package com.netqin.antivirus.networkmanager.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import java.text.ParseException;
import java.util.Calendar;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "virus_net.db";
    private static final int DATABASE_VERSION = 1;
    private static MyDateFormater mDF = new MyDateFormater();

    public static final class NetCounter implements BaseColumns {
        public static final String INTERFACE = "interface";
        public static final String LAST_RESET = "last_reset";
        public static final String LAST_RX = "last_rx";
        public static final String LAST_TX = "last_tx";
        public static final String LAST_UPDATE = "last_update";
        public static final String TABLE_NAME = "counter";

        private NetCounter() {
        }
    }

    public static final class Counters implements BaseColumns {
        public static final String INTERFACE = "interface";
        public static final String POS = "position";
        public static final String TABLE_NAME = "counters";
        public static final String TYPE = "type";
        public static final String VALUE = "value";

        private Counters() {
        }
    }

    public static final class DailyCounter implements BaseColumns {
        public static final String DAY = "day";
        public static final String INTERFACE = "interface";
        public static final String LAST_UPDATE = "last_update";
        public static final String RX = "rx";
        public static final String TABLE_NAME = "daily";
        public static final String TX = "tx";

        private DailyCounter() {
        }
    }

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, 1);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE counter (_id INTEGER PRIMARY KEY,interface TEXT,last_rx LONG,last_tx LONG,last_update DATETIME,last_reset DATETIME);");
        db.execSQL("CREATE TABLE counters (_id INTEGER PRIMARY KEY,interface TEXT,type INTEGER,value TEXT,position INTEGER);");
        db.execSQL("CREATE TABLE daily (_id INTEGER PRIMARY KEY,interface TEXT,day DATE,rx LONG,tx LONG,last_update DATETIME);");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion == 1) {
            db.execSQL("DROP TABLE IF EXISTS counter");
            db.execSQL("DROP TABLE IF EXISTS counters");
            db.execSQL("DROP TABLE IF EXISTS daily");
            onCreate(db);
        }
    }

    public static String getDateTime(Calendar date) {
        return mDF.getDateTime(date);
    }

    public static String getDate(Calendar date) {
        return mDF.getDate(date);
    }

    public static String getLocaleDateTime(Calendar date) {
        return mDF.getLocaleDateTime(date);
    }

    public static String getLocaleDate(Calendar date) {
        return mDF.getLocaleDate(date);
    }

    public static Calendar parseDateTime(String date) {
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(mDF.DF_DATETIMEparse(date));
        } catch (ParseException e) {
        }
        return c;
    }

    public static Calendar parseDate(String date) {
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(mDF.DF_DATEparse(date));
        } catch (ParseException e) {
        }
        return c;
    }
}
