package com.netqin.antivirus.networkmanager.model;

import android.content.res.Resources;
import com.netqin.antivirus.networkmanager.NetApplication;
import com.nqmobile.antivirus_ampro20.R;
import java.util.ArrayList;
import java.util.List;

public abstract class Device {
    private static Device sInstance = null;
    private String[] mInterfaces = null;

    public abstract String getCell();

    public abstract String getWiFi();

    public abstract boolean isCell(String str);

    public abstract boolean isWiFi(String str);

    public static synchronized Device getDevice() {
        Device device;
        synchronized (Device.class) {
            if (sInstance == null) {
                sInstance = new DiscoverableDevice();
            }
            device = sInstance;
        }
        return device;
    }

    public synchronized String[] getInterfaces() {
        if (this.mInterfaces == null) {
            List<String> tmp = new ArrayList<>();
            if (getCell() != null) {
                tmp.add(getCell());
            }
            if (getWiFi() != null) {
                tmp.add(getWiFi());
            }
            this.mInterfaces = (String[]) tmp.toArray(new String[tmp.size()]);
        }
        return this.mInterfaces;
    }

    public String getPrettyName(String inter) {
        Resources r = NetApplication.resources();
        if (getCell() == null || !getCell().equals(inter)) {
            return (getWiFi() == null || !getWiFi().equals(inter)) ? inter : r.getString(R.string.meter_interface_type_cell);
        }
        return r.getString(R.string.meter_interface_type_cell);
    }

    public int getIcon(String inter) {
        if (getCell() == null || !getCell().equals(inter)) {
            return R.drawable.ic_wifi;
        }
        return R.drawable.ic_cell;
    }
}
