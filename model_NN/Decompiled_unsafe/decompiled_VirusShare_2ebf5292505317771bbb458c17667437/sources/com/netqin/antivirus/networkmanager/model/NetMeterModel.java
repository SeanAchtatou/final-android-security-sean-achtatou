package com.netqin.antivirus.networkmanager.model;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import com.netqin.antivirus.networkmanager.model.DatabaseHelper;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class NetMeterModel extends AbstractModel implements IModelListener {
    private static final boolean DBG = false;
    private static final String TAG = NetMeterModel.class.getSimpleName();
    private final SQLiteOpenHelper mHelper;
    private List<Interface> mInterface;
    private List<IOperation> mListeners;
    private volatile boolean mLoaded = false;
    private final List<IModel> mQueue = new ArrayList();

    public NetMeterModel(Context context) {
        this.mHelper = new DatabaseHelper(context);
    }

    public void addOperationListener(IOperation listener) {
        if (this.mListeners == null) {
            this.mListeners = new ArrayList();
        }
        this.mListeners.add(listener);
    }

    public void removeOperationListener(IOperation listener) {
        if (this.mListeners != null) {
            this.mListeners.remove(listener);
        }
    }

    private void addInterface(Interface inter) {
        if (this.mInterface == null) {
            this.mInterface = new ArrayList();
        }
        inter.setNew(true);
        this.mInterface.add(inter);
        inter.addModelListener(this);
        modelChanged(inter);
    }

    public List<Interface> getInterfaces() {
        if (this.mInterface == null) {
            return Collections.EMPTY_LIST;
        }
        return Collections.unmodifiableList(this.mInterface);
    }

    public Interface getInterface(long id) {
        for (Interface inter : getInterfaces()) {
            if (inter.getId() == id) {
                return inter;
            }
        }
        return null;
    }

    public Interface getInterface(String name) {
        for (Interface inter : getInterfaces()) {
            if (inter.getName().equals(name)) {
                return inter;
            }
        }
        Interface inter2 = new Interface(name);
        addInterface(inter2);
        Counter counter = new Counter(inter2);
        counter.setType(0);
        inter2.addCounter(counter);
        return inter2;
    }

    public void load() {
        modelChanged(this);
        commit();
        this.mLoaded = true;
        fireModelLoaded();
    }

    public boolean isLoaded() {
        return this.mLoaded;
    }

    public void modelLoaded(IModel object) {
    }

    public void modelChanged(IModel object) {
        this.mQueue.add(object);
        if (!(object instanceof Counter) && !(object instanceof Interface)) {
            boolean z = object instanceof NetMeterModel;
        }
    }

    public boolean isDirty() {
        return !this.mQueue.isEmpty();
    }

    public void commit() {
        operationStarted();
        SQLiteDatabase db = this.mHelper.getWritableDatabase();
        boolean modelChanged = !this.mQueue.isEmpty();
        do {
            List<IModel> tmp = new ArrayList<>(this.mQueue);
            this.mQueue.clear();
            for (IModel object : tmp) {
                try {
                    if (object.isNew()) {
                        object.insert(db);
                    } else if (object.isDirty()) {
                        object.update(db);
                    } else if (object.isDeleted()) {
                        object.remove(db);
                    } else {
                        object.load(db);
                    }
                } catch (Exception e) {
                }
                if (object instanceof AbstractModel) {
                    AbstractModel am = (AbstractModel) object;
                    if (am.isNew()) {
                        am.setNew(false);
                    } else if (am.isDirty()) {
                        am.setDirty(false);
                    } else if (am.isNew()) {
                        am.setNew(false);
                    }
                }
            }
        } while (isDirty());
        operationEnded();
        if (modelChanged) {
            fireModelChanged();
        }
    }

    public void rollback() {
        this.mQueue.clear();
    }

    public void load(SQLiteDatabase db) {
        SQLiteQueryBuilder query = new SQLiteQueryBuilder();
        query.setTables(DatabaseHelper.NetCounter.TABLE_NAME);
        Cursor c = query.query(db, new String[]{"interface"}, null, null, null, null, null);
        if (c != null) {
            while (c.moveToNext()) {
                try {
                    Interface inter = new Interface(c.getString(0));
                    addInterface(inter);
                    inter.setNew(false);
                } finally {
                    c.close();
                }
            }
        }
    }

    public void insert(SQLiteDatabase db) {
    }

    public void remove(SQLiteDatabase db) {
    }

    public void update(SQLiteDatabase db) {
    }

    private void operationStarted() {
        if (this.mListeners != null) {
            for (IOperation listener : this.mListeners) {
                listener.operationStarted();
            }
        }
    }

    private void operationEnded() {
        if (this.mListeners != null) {
            for (IOperation listener : this.mListeners) {
                listener.operationEnded();
            }
        }
    }
}
