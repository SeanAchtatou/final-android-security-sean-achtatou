package com.netqin.antivirus.networkmanager.model;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractModel implements IModel {
    private boolean mIsDeleted = false;
    private boolean mIsDirty = false;
    private boolean mIsNew = false;
    private List<IModelListener> mListeners;

    public void addModelListener(IModelListener listener) {
        if (this.mListeners == null) {
            this.mListeners = new ArrayList();
        }
        this.mListeners.add(listener);
    }

    public void removeModelListener(IModelListener listener) {
        if (this.mListeners != null) {
            this.mListeners.remove(listener);
        }
    }

    /* access modifiers changed from: protected */
    public void fireModelLoaded() {
        if (this.mListeners != null) {
            for (IModelListener listener : this.mListeners) {
                listener.modelLoaded(this);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void fireModelChanged() {
        if (this.mListeners != null) {
            for (IModelListener listener : this.mListeners) {
                listener.modelChanged(this);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void fireModelChanged(IModel object) {
        if (this.mListeners != null) {
            for (IModelListener listener : this.mListeners) {
                listener.modelChanged(object);
            }
        }
    }

    public boolean isDirty() {
        return this.mIsDirty;
    }

    /* access modifiers changed from: protected */
    public void setDirty(boolean isDirty) {
        this.mIsDirty = isDirty;
    }

    public boolean isNew() {
        return this.mIsNew;
    }

    /* access modifiers changed from: protected */
    public void setNew(boolean isNew) {
        this.mIsNew = isNew;
    }

    public boolean isDeleted() {
        return this.mIsDeleted;
    }

    /* access modifiers changed from: protected */
    public void setDeleted(boolean isDeleted) {
        this.mIsDeleted = isDeleted;
    }
}
