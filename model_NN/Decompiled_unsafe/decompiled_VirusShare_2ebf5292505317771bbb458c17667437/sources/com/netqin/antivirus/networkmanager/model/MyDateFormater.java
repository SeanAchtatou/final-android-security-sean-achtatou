package com.netqin.antivirus.networkmanager.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/* compiled from: DatabaseHelper */
class MyDateFormater {
    private DateFormat DF_DATE = new SimpleDateFormat("yyyy-MM-dd");
    private DateFormat DF_DATETIME = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
    private DateFormat DF_DATE_LOCALE = DateFormat.getDateInstance();
    private DateFormat DF_LOCALE = DateFormat.getDateTimeInstance();

    MyDateFormater() {
    }

    public String getDateTime(Calendar date) {
        return this.DF_DATETIME.format(date.getTime());
    }

    public String getDate(Calendar date) {
        return this.DF_DATE.format(date.getTime());
    }

    public String getLocaleDateTime(Calendar date) {
        return this.DF_LOCALE.format(date.getTime());
    }

    public String getLocaleDate(Calendar date) {
        return this.DF_DATE_LOCALE.format(date.getTime());
    }

    public Date DF_DATETIMEparse(String date) throws ParseException {
        return this.DF_DATETIME.parse(date);
    }

    public Date DF_DATEparse(String date) throws ParseException {
        return this.DF_DATE.parse(date);
    }
}
