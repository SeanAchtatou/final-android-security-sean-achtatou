package com.netqin.antivirus.networkmanager;

import android.app.Application;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Handler;
import android.os.HandlerThread;
import android.preference.PreferenceManager;
import android.widget.Toast;
import com.netqin.antivirus.networkmanager.model.NetMeterModel;
import com.netqin.antivirus.services.NetMeterService;
import com.netqin.antivirus.services.WakefulService;
import com.nqmobile.antivirus_ampro20.R;

public class NetApplication extends Application {
    private static final boolean DBG = false;
    public static final String DEFAULT_THRESHOLD = "30";
    public static final String INTENT_EXTRA_INTERFACE = "interface";
    public static final int SERVICE_HIGH = 2;
    public static final int SERVICE_LOW = 0;
    public static final String SERVICE_POLLING = "polling";
    public static final int SERVICE_STANDARD = 1;
    private static final String TAG = NetApplication.class.getSimpleName();
    public static NetworkDeviceData mNetworkData = new NetworkDeviceData();
    private static int sUpdatePolicy = 0;
    private HandlerContainer mHandlerContainer;
    /* access modifiers changed from: private */
    public NetMeterModel mModel;
    private NotificationManager mNotification;
    private SharedPreferences mPreferences;

    public static class NetworkDeviceData {
        public CharSequence[] BYTE_UNITS;
        public int[] BYTE_VALUES;
        public CharSequence[] COUNTER_TYPES;
        public int[] COUNTER_TYPES_POS;
        public Resources RESOURCES;
    }

    public static Resources resources() {
        return mNetworkData.RESOURCES;
    }

    public synchronized <T> T getAdapter(Class<T> clazz) {
        T t;
        if (NetMeterModel.class == clazz) {
            if (this.mModel == null) {
                this.mModel = new NetMeterModel(this);
                ((HandlerContainer) getAdapter(HandlerContainer.class)).getSlowHandler().post(new Runnable() {
                    public void run() {
                        NetApplication.this.mModel.load();
                    }
                });
            }
            t = clazz.cast(this.mModel);
        } else if (HandlerContainer.class == clazz) {
            if (this.mHandlerContainer == null) {
                HandlerThread looper = new HandlerThread("NetCounter Handler");
                looper.start();
                this.mHandlerContainer = new HandlerContainer(new Handler(), new Handler(looper.getLooper()));
            }
            t = clazz.cast(this.mHandlerContainer);
        } else if (SharedPreferences.class == clazz) {
            if (this.mPreferences == null) {
                this.mPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            }
            t = clazz.cast(this.mPreferences);
        } else if (NotificationManager.class == clazz) {
            if (this.mNotification == null) {
                this.mNotification = (NotificationManager) getSystemService("notification");
            }
            t = clazz.cast(this.mNotification);
        } else {
            t = null;
        }
        return t;
    }

    public void onCreate() {
        mNetworkData.BYTE_UNITS = getResources().getTextArray(R.array.byteUnits);
        mNetworkData.BYTE_VALUES = getResources().getIntArray(R.array.byteValues);
        CharSequence[] temp = getResources().getTextArray(R.array.counterTypes);
        mNetworkData.COUNTER_TYPES = new CharSequence[temp.length];
        mNetworkData.COUNTER_TYPES_POS = getResources().getIntArray(R.array.counterTypesPos);
        for (int i = 0; i < temp.length; i++) {
            mNetworkData.COUNTER_TYPES[mNetworkData.COUNTER_TYPES_POS[i]] = temp[i];
        }
        mNetworkData.RESOURCES = getResources();
        super.onCreate();
    }

    public void onTerminate() {
        synchronized (this) {
            if (this.mHandlerContainer != null) {
                this.mHandlerContainer.getSlowHandler().getLooper().quit();
            }
        }
        mNetworkData.BYTE_UNITS = null;
        mNetworkData.BYTE_VALUES = null;
        mNetworkData.COUNTER_TYPES = null;
        mNetworkData.COUNTER_TYPES_POS = null;
        mNetworkData.RESOURCES = null;
    }

    public void startService() {
        WakefulService.acquireStaticLock(this);
        startService(new Intent(this, NetMeterService.class));
    }

    public static synchronized void setUpdatePolicy(int updatePolicy) {
        synchronized (NetApplication.class) {
            sUpdatePolicy = updatePolicy;
        }
    }

    public static synchronized int getUpdatePolicy() {
        int i;
        synchronized (NetApplication.class) {
            i = sUpdatePolicy;
        }
        return i;
    }

    public void toast(int message) {
        Toast.makeText(this, message, 0).show();
    }

    public void toast(CharSequence message) {
        Toast.makeText(this, message, 0).show();
    }
}
