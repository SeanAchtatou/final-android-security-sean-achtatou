package com.netqin.antivirus.networkmanager;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.method.DigitsKeyListener;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;
import com.netqin.antivirus.HomeActivity;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.data.PreferenceDataHelper;
import com.netqin.antivirus.networkmanager.model.Counter;
import com.netqin.antivirus.networkmanager.model.Device;
import com.netqin.antivirus.networkmanager.model.Interface;
import com.netqin.antivirus.networkmanager.model.NetMeterModel;
import com.nqmobile.antivirus_ampro20.R;
import java.util.Iterator;

public class SettingPreferences extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    private static final boolean DBG = false;
    public static final String KEY_METER_ALERT = "meter_traffic_alert2";
    public static final String KEY_METER_GPRS_SWITCHER = "meter_gprs_switcher";
    public static final String KEY_METER_ONOFF = "meter_traffic_alert";
    public static final String KEY_METER_START_DATE = "meter_traffic_new_month_start_list";
    public static final String KEY_METER_THRESHOLD = "meter_traffic_threshold";
    public static final String KEY_METER_USED = "meter_traffic_used";
    private static final String TAG = NetMeterModel.class.getSimpleName();
    private ListPreference listPreference;
    private NetApplication mApp;
    /* access modifiers changed from: private */
    public HandlerContainer mContainer;
    private CheckBoxPreference mMeterAlert;
    private CheckBoxPreference mMeterOnOff;
    /* access modifiers changed from: private */
    public NetMeterModel mModel;
    private EditTextPreference mTrafficThreshold;
    private EditTextPreference mTrafficUsed;
    private boolean meterTrafficAlert;
    private SharedPreferences prefs;
    private boolean statusBar;

    public static Intent getLaunchIntent(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, SettingPreferences.class);
        return intent;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.net_setting_preferences);
        this.prefs = PreferenceManager.getDefaultSharedPreferences(this);
        getListView().setCacheColorHint(0);
        getListView().setBackgroundResource(R.drawable.main_background);
        NetApplication app = getApp();
        this.mContainer = (HandlerContainer) app.getAdapter(HandlerContainer.class);
        this.mModel = (NetMeterModel) app.getAdapter(NetMeterModel.class);
        this.mMeterOnOff = (CheckBoxPreference) findPreference(KEY_METER_ONOFF);
        this.mMeterAlert = (CheckBoxPreference) findPreference(KEY_METER_ALERT);
        this.mTrafficUsed = (EditTextPreference) findPreference(KEY_METER_USED);
        this.mTrafficThreshold = (EditTextPreference) findPreference(KEY_METER_THRESHOLD);
        this.listPreference = (ListPreference) findPreference(KEY_METER_START_DATE);
        this.mTrafficThreshold.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference pre, Object newObj) {
                try {
                    Long.parseLong((String) newObj);
                    return true;
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    Toast.makeText(SettingPreferences.this, SettingPreferences.this.getString(R.string.netsetfail), 1).show();
                    return false;
                }
            }
        });
        EditText edit = this.mTrafficUsed.getEditText();
        if (edit != null) {
            edit.setSingleLine();
            edit.setKeyListener(new DigitsKeyListener(false, true));
            edit.setFilters(new InputFilter[]{new InputFilter.LengthFilter(5)});
        }
        EditText edit2 = this.mTrafficThreshold.getEditText();
        if (edit2 != null) {
            edit2.setSingleLine();
            edit2.setKeyListener(new DigitsKeyListener(false, true));
            edit2.setFilters(new InputFilter[]{new InputFilter.LengthFilter(5)});
        }
        String day = this.prefs.getString(KEY_METER_START_DATE, "1");
        if (!day.equalsIgnoreCase("1")) {
            this.listPreference.setSummary(getString(R.string.meter_traffic_new_month_start_tip, new Object[]{day}));
        } else {
            this.listPreference.setSummary(getString(R.string.meter_traffic_new_month_start_summary));
        }
        this.statusBar = this.prefs.getBoolean("status_bar", true);
        this.meterTrafficAlert = this.prefs.getBoolean(KEY_METER_ONOFF, true);
        if (!this.meterTrafficAlert) {
            this.mMeterAlert.setEnabled(false);
            this.mTrafficUsed.setEnabled(false);
            this.mTrafficThreshold.setEnabled(false);
            this.listPreference.setEnabled(false);
            return;
        }
        this.mMeterAlert.setEnabled(true);
        this.mTrafficUsed.setEnabled(true);
        this.mTrafficThreshold.setEnabled(true);
        this.listPreference.setEnabled(true);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Preference pref = findPreference(key);
        if (pref == null) {
            return;
        }
        if (pref == this.mMeterAlert || pref == this.mTrafficThreshold) {
            updateAlertValue();
        } else if (pref == this.mTrafficUsed) {
            updateTrafficUsed();
        } else if (pref == this.mMeterOnOff) {
            this.meterTrafficAlert = this.prefs.getBoolean(KEY_METER_ONOFF, true);
            if (!this.meterTrafficAlert) {
                this.mMeterAlert.setEnabled(false);
                this.mTrafficUsed.setEnabled(false);
                this.mTrafficThreshold.setEnabled(false);
                this.listPreference.setEnabled(false);
                if (this.statusBar) {
                    CommonMethod.showFlowBarOrNot(this, new Intent(this, HomeActivity.class), CommonMethod.getNotificationTitle(this), CommonMethod.getNotificationIconState(this));
                    return;
                }
                return;
            }
            this.mMeterAlert.setEnabled(true);
            this.mTrafficUsed.setEnabled(true);
            this.mTrafficThreshold.setEnabled(true);
            this.listPreference.setEnabled(true);
            if (this.statusBar) {
                CommonMethod.showFlowBarOrNot(this, new Intent(this, HomeActivity.class), CommonMethod.getNotificationTitle(this), CommonMethod.getNotificationIconState(this));
            }
        } else if (pref == this.listPreference) {
            String day = this.prefs.getString(KEY_METER_START_DATE, "1");
            this.listPreference.setSummary(getString(R.string.meter_traffic_new_month_start_tip, new Object[]{day}));
            if (PreferenceDataHelper.getFlowAdjustValue(this) != 0) {
                String[] a = PreferenceDataHelper.getFlowStartAndEndDate(this);
                PreferenceDataHelper.setFlowStartDate(this, a[0]);
                PreferenceDataHelper.setFlowEndDate(this, a[1]);
            }
        }
    }

    public static String getFlowTotal(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(KEY_METER_THRESHOLD, NetApplication.DEFAULT_THRESHOLD);
    }

    private void updateTrafficUsed() {
        try {
            final double used = Double.valueOf(this.mTrafficUsed.getText()).doubleValue();
            this.mContainer.getSlowHandler().post(new Runnable() {
                public void run() {
                    if (SettingPreferences.this.getGprsCounter() != null) {
                        SettingPreferences.this.updateFlowAdjustValue(2 * Math.round(((used * 1024.0d) * 1024.0d) / 2.0d));
                    }
                    CommonMethod.showFlowBarNotification(SettingPreferences.this, Math.round(used * 1024.0d * 1024.0d));
                    SettingPreferences.this.mModel.commit();
                }
            });
            setAlertValue(this.mTrafficThreshold.getText());
        } catch (NumberFormatException e) {
        }
    }

    /* access modifiers changed from: private */
    public void updateFlowAdjustValue(long d) {
        long used;
        long[] cell = updateMeterTraffic();
        if (cell != null) {
            used = cell[0] + cell[1];
        } else {
            used = 0;
        }
        PreferenceDataHelper.setFlowAdjustValue(this, d - used);
        CommonMethod.showFlowBarNotification(this, d);
        String[] a = PreferenceDataHelper.getFlowStartAndEndDate(this);
        PreferenceDataHelper.setFlowStartDate(this, a[0]);
        PreferenceDataHelper.setFlowEndDate(this, a[1]);
    }

    private long[] updateMeterTraffic() {
        long[] wifi = new long[2];
        long[] cell = new long[2];
        Device device = Device.getDevice();
        for (Interface inter : this.mModel.getInterfaces()) {
            for (Counter counter : inter.getCounters()) {
                if (counter.getType() == 0) {
                    long[] bytes = counter.getBytes();
                    if (device.isCell(inter.getName())) {
                        cell[0] = cell[0] + bytes[0];
                        cell[1] = cell[1] + bytes[1];
                    } else if (device.isWiFi(inter.getName())) {
                        wifi[0] = wifi[0] + bytes[0];
                        wifi[1] = wifi[1] + bytes[1];
                    }
                }
            }
        }
        return cell;
    }

    private void updateAlertValue() {
        boolean bChecked = this.mMeterAlert.isChecked();
        String value = this.mTrafficThreshold.getText();
        if (TextUtils.isEmpty(value)) {
            value = NetApplication.DEFAULT_THRESHOLD;
            this.mTrafficThreshold.setText(value);
            this.mTrafficThreshold.getEditor().commit();
        }
        if (bChecked) {
            try {
                setAlertValue(value);
            } catch (NumberFormatException e) {
                getApp().toast((int) R.string.error_meter_set_threshold);
                this.mMeterAlert.setChecked(false);
            }
        } else {
            removeAlertValue();
        }
        CommonMethod.showFlowBarNotification(this, -1);
    }

    private void removeAlertValue() {
        this.mContainer.getSlowHandler().post(new Runnable() {
            public void run() {
                Counter counter = SettingPreferences.this.getGprsCounter();
                if (counter != null) {
                    counter.removeProperty(Counter.ALERT_UNIT);
                    counter.removeProperty(Counter.ALERT_BYTES);
                }
                SettingPreferences.this.mModel.commit();
                SettingPreferences.this.mContainer.getGuiHandler().post(new Runnable() {
                    public void run() {
                        SettingPreferences.this.getApp().startService();
                    }
                });
            }
        });
    }

    private void setAlertValue(String sValue) {
        final double value = Double.valueOf(sValue).doubleValue();
        final long bytes = Math.round(value * 1024.0d * 1024.0d);
        this.mContainer.getSlowHandler().post(new Runnable() {
            public void run() {
                Counter counter = SettingPreferences.this.getGprsCounter();
                if (counter != null) {
                    counter.setProperty(Counter.ALERT_VALUE, String.valueOf(value));
                    counter.setProperty(Counter.ALERT_BYTES, String.valueOf(bytes));
                    SettingPreferences.this.mModel.commit();
                    SettingPreferences.this.mContainer.getGuiHandler().post(new Runnable() {
                        public void run() {
                            SettingPreferences.this.getApp().startService();
                        }
                    });
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public Counter getGprsCounter() {
        Device device = Device.getDevice();
        for (Interface inter : this.mModel.getInterfaces()) {
            Iterator<Counter> it = inter.getCounters().iterator();
            while (true) {
                if (it.hasNext()) {
                    Counter counter = it.next();
                    if (counter.getType() == 0 && device.isCell(inter.getName())) {
                        return counter;
                    }
                }
            }
        }
        return null;
    }

    /* access modifiers changed from: private */
    public NetApplication getApp() {
        if (this.mApp == null) {
            this.mApp = (NetApplication) getApplication();
        }
        return this.mApp;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        item.getItemId();
        return true;
    }
}
