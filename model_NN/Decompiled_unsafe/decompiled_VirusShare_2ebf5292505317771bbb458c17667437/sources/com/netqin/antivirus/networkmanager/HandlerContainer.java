package com.netqin.antivirus.networkmanager;

import android.os.Handler;

public class HandlerContainer {
    private final Handler mGuiHandler;
    private final Handler mSlowHandler;

    public HandlerContainer(Handler gui, Handler slow) {
        this.mGuiHandler = gui;
        this.mSlowHandler = slow;
    }

    public Handler getGuiHandler() {
        return this.mGuiHandler;
    }

    public Handler getSlowHandler() {
        return this.mSlowHandler;
    }
}
