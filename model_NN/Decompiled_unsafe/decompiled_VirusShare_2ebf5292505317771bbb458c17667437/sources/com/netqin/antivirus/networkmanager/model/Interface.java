package com.netqin.antivirus.networkmanager.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import com.netqin.antivirus.antilost.SmsHandler;
import com.netqin.antivirus.networkmanager.model.DatabaseHelper;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Interface extends AbstractModel implements IModelListener {
    private static final int DAY_OF_YEAR = 6;
    private static final boolean DBG = false;
    private static final int ID_INDEX = 0;
    private static final String[] INTERFACE_SUM = {"sum(rx)", "sum(tx)"};
    private static final int LAST_RESET_INDEX = 4;
    private static final int LAST_RX_INDEX = 1;
    private static final int LAST_TX_INDEX = 2;
    private static final int LAST_UPDATE_INDEX = 3;
    private static final String TAG = Interface.class.getSimpleName();
    private static final String[] sProjectionCounter = {SmsHandler.ROWID, DatabaseHelper.NetCounter.LAST_RX, DatabaseHelper.NetCounter.LAST_TX, "last_update", DatabaseHelper.NetCounter.LAST_RESET};
    private final long[] mBytes = new long[2];
    private List<Counter> mCounters;
    private final long[] mDelta = new long[2];
    private final int mIcon;
    private long mId;
    private boolean mIsReset = false;
    private Calendar mLastReset;
    private Calendar mLastUpdate;
    private String mLastUpdateAsString;
    private final String mName;
    private final String mPrettyName;
    private final long[] mReset = new long[2];
    private boolean mUpdateOnly = false;
    private final String mWhere;
    private final String mWhereInterface;

    public Interface(String name) {
        this.mName = name;
        this.mPrettyName = Device.getDevice().getPrettyName(name);
        this.mIcon = Device.getDevice().getIcon(name);
        this.mWhere = "interface='" + name + "'";
        this.mWhereInterface = "interface='" + name + "'";
    }

    public synchronized long getId() {
        return this.mId;
    }

    public synchronized String getName() {
        return this.mName;
    }

    public synchronized String getPrettyName() {
        return this.mPrettyName;
    }

    public synchronized int getIcon() {
        return this.mIcon;
    }

    private boolean isNewDay(Calendar now) {
        if (this.mLastUpdate == null) {
            return false;
        }
        return this.mLastUpdate.get(6) != now.get(6);
    }

    public synchronized void updateBytes(long rx, long tx) {
        if (rx < 0) {
            throw new IllegalArgumentException("rx may not be smaller than 0: " + rx);
        } else if (tx < 0) {
            throw new IllegalArgumentException("tx may not be smaller than 0: " + tx);
        } else {
            Calendar now = Calendar.getInstance();
            boolean isNewDay = isNewDay(now);
            this.mLastUpdate = now;
            this.mLastUpdateAsString = DatabaseHelper.getLocaleDateTime(this.mLastUpdate);
            if (rx == this.mBytes[0] && tx == this.mBytes[1] && !isNewDay) {
                this.mUpdateOnly = true;
                setDirty(true);
                fireModelChanged();
            } else {
                if (rx < this.mBytes[0]) {
                    this.mDelta[0] = rx;
                } else {
                    this.mDelta[0] = rx - this.mBytes[0];
                }
                if (tx < this.mBytes[1]) {
                    this.mDelta[1] = tx;
                } else {
                    this.mDelta[1] = tx - this.mBytes[1];
                }
                this.mBytes[0] = rx;
                this.mBytes[1] = tx;
                setDirty(true);
                fireModelChanged();
                for (Counter counter : getCounters()) {
                    if (needsUpdate(counter)) {
                        counter.setDirty(true);
                        fireModelChanged(counter);
                    }
                }
            }
        }
    }

    private boolean needsUpdate(Counter counter) {
        return true;
    }

    public synchronized Calendar getLastUpdate() {
        return this.mLastUpdate;
    }

    public synchronized String getLastUpdateAsString() {
        return this.mLastUpdateAsString;
    }

    public synchronized Calendar getLastReset() {
        return this.mLastReset;
    }

    public synchronized void reset() {
        reset(0, 0);
    }

    public synchronized void reset(long rx, long tx) {
        this.mIsReset = true;
        this.mReset[0] = rx;
        this.mReset[1] = tx;
        setDirty(true);
        fireModelChanged();
        for (Counter counter : getCounters()) {
            counter.setDirty(true);
            fireModelChanged(counter);
        }
    }

    public synchronized void addCounter(Counter counter) {
        if (this.mCounters == null) {
            this.mCounters = new ArrayList();
        }
        counter.setNew(true);
        this.mCounters.add(counter);
        counter.addModelListener(this);
        fireModelChanged(counter);
    }

    public synchronized void removeCounter(Counter counter) {
        if (this.mCounters != null) {
            this.mCounters.remove(counter);
            counter.setDeleted(true);
            counter.removeModelListener(this);
            fireModelChanged(counter);
        }
    }

    public synchronized Counter getCounter(long id) {
        Counter counter;
        Iterator<Counter> it = this.mCounters.iterator();
        while (true) {
            if (it.hasNext()) {
                Counter counter2 = it.next();
                if (counter2.getId() == id) {
                    counter = counter2;
                    break;
                }
            } else {
                counter = null;
                break;
            }
        }
        return counter;
    }

    public synchronized List<Counter> getCounters() {
        List<Counter> unmodifiableList;
        if (this.mCounters == null) {
            unmodifiableList = Collections.emptyList();
        } else {
            unmodifiableList = Collections.unmodifiableList(this.mCounters);
        }
        return unmodifiableList;
    }

    public void modelLoaded(IModel object) {
    }

    public void modelChanged(IModel object) {
        fireModelChanged(object);
    }

    public synchronized void load(SQLiteDatabase db) {
        Cursor c;
        SQLiteQueryBuilder query = new SQLiteQueryBuilder();
        query.setTables(DatabaseHelper.NetCounter.TABLE_NAME);
        Cursor c2 = query.query(db, sProjectionCounter, this.mWhere, null, null, null, null);
        if (c2 != null) {
            try {
                if (c2.moveToNext()) {
                    this.mId = c2.getLong(0);
                    String lastUpdate = c2.getString(3);
                    String lastReset = c2.getString(4);
                    this.mBytes[0] = c2.getLong(1);
                    this.mBytes[1] = c2.getLong(2);
                    this.mLastUpdate = DatabaseHelper.parseDateTime(lastUpdate);
                    this.mLastUpdateAsString = DatabaseHelper.getLocaleDateTime(this.mLastUpdate);
                    this.mLastReset = DatabaseHelper.parseDateTime(lastReset);
                }
                c2.close();
            } catch (Throwable th) {
                c2.close();
                throw th;
            }
        }
        SQLiteQueryBuilder query2 = new SQLiteQueryBuilder();
        query2.setTables(DatabaseHelper.Counters.TABLE_NAME);
        SQLiteDatabase sQLiteDatabase = db;
        c = query2.query(sQLiteDatabase, new String[]{SmsHandler.ROWID}, this.mWhere, null, null, null, "position");
        if (c != null) {
            while (c.moveToNext()) {
                Counter counter = new Counter(c.getLong(0), this);
                addCounter(counter);
                counter.setNew(false);
            }
            c.close();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public synchronized void insert(SQLiteDatabase db) {
        Calendar c = Calendar.getInstance();
        ContentValues values = new ContentValues();
        values.put("interface", this.mName);
        values.put(DatabaseHelper.NetCounter.LAST_RX, (Integer) 0);
        values.put(DatabaseHelper.NetCounter.LAST_TX, (Integer) 0);
        String now = DatabaseHelper.getDateTime(c);
        values.put("last_update", now);
        values.put(DatabaseHelper.NetCounter.LAST_RESET, now);
        db.insert(DatabaseHelper.NetCounter.TABLE_NAME, null, values);
        this.mLastReset = c;
    }

    public void remove(SQLiteDatabase db) {
    }

    public synchronized void update(SQLiteDatabase db) {
        if (this.mUpdateOnly) {
            updateLastUpdate(db);
            this.mUpdateOnly = false;
        } else {
            updateSession(db, this.mBytes);
            updateDailySession(db, this.mDelta);
            this.mDelta[0] = 0;
            this.mDelta[1] = 0;
        }
        if (this.mIsReset) {
            Calendar c = Calendar.getInstance();
            String now = DatabaseHelper.getDateTime(c);
            ContentValues values = new ContentValues();
            values.put("interface", this.mName);
            values.put(DatabaseHelper.NetCounter.LAST_RESET, now);
            values.put("last_update", now);
            db.update(DatabaseHelper.NetCounter.TABLE_NAME, values, this.mWhere, null);
            db.delete(DatabaseHelper.DailyCounter.TABLE_NAME, this.mWhereInterface, null);
            updateDailySession(db, this.mReset);
            this.mLastReset = c;
            this.mIsReset = false;
        }
    }

    private void updateLastUpdate(SQLiteDatabase db) {
        ContentValues values = new ContentValues();
        values.put("last_update", DatabaseHelper.getDateTime(Calendar.getInstance()));
        db.update(DatabaseHelper.NetCounter.TABLE_NAME, values, this.mWhere, null);
    }

    private void updateSession(SQLiteDatabase db, long[] bytes) {
        ContentValues values = new ContentValues();
        values.put("interface", this.mName);
        values.put(DatabaseHelper.NetCounter.LAST_RX, Long.valueOf(bytes[0]));
        values.put(DatabaseHelper.NetCounter.LAST_TX, Long.valueOf(bytes[1]));
        values.put("last_update", DatabaseHelper.getDateTime(Calendar.getInstance()));
        db.update(DatabaseHelper.NetCounter.TABLE_NAME, values, this.mWhere, null);
    }

    private void updateDailySession(SQLiteDatabase db, long[] bytes) {
        ContentValues values = new ContentValues();
        values.put("interface", this.mName);
        Calendar now = Calendar.getInstance();
        Cursor c = getDailySession(db, now);
        if (c.getCount() > 0) {
            c.moveToFirst();
            long rx = c.getLong(c.getColumnIndex(DatabaseHelper.DailyCounter.RX));
            long tx = c.getLong(c.getColumnIndex(DatabaseHelper.DailyCounter.TX));
            values.put(DatabaseHelper.DailyCounter.RX, Long.valueOf(bytes[0] + rx));
            values.put(DatabaseHelper.DailyCounter.TX, Long.valueOf(bytes[1] + tx));
            db.update(DatabaseHelper.DailyCounter.TABLE_NAME, values, this.mWhereInterface + " AND " + "day" + "='" + DatabaseHelper.getDate(now) + "'", null);
        } else {
            values.put("day", DatabaseHelper.getDate(now));
            values.put(DatabaseHelper.DailyCounter.RX, Long.valueOf(bytes[0]));
            values.put(DatabaseHelper.DailyCounter.TX, Long.valueOf(bytes[1]));
            db.insert(DatabaseHelper.DailyCounter.TABLE_NAME, null, values);
        }
        c.close();
    }

    private Cursor getDailySession(SQLiteDatabase db, Calendar now) {
        SQLiteQueryBuilder query = new SQLiteQueryBuilder();
        query.setTables(DatabaseHelper.DailyCounter.TABLE_NAME);
        return query.query(db, null, this.mWhereInterface + " AND " + "day" + "='" + DatabaseHelper.getDate(now) + "'", null, null, null, null);
    }

    public final long[] getInterfaceBytes(SQLiteDatabase db, Calendar from, Calendar to) {
        long[] jArr;
        SQLiteQueryBuilder query = new SQLiteQueryBuilder();
        query.setTables(DatabaseHelper.DailyCounter.TABLE_NAME);
        StringBuilder where = new StringBuilder(this.mWhereInterface);
        if (from != null) {
            where.append(" AND ");
            where.append("day");
            where.append(">='");
            where.append(DatabaseHelper.getDate(from));
            where.append("'");
        }
        if (to != null) {
            where.append(" AND ");
            where.append("day");
            where.append("<='");
            where.append(DatabaseHelper.getDate(to));
            where.append("'");
        }
        Cursor c = query.query(db, INTERFACE_SUM, where.toString(), null, null, null, null);
        try {
            if (c.moveToNext()) {
                jArr = new long[]{c.getLong(0), c.getLong(1)};
            } else {
                jArr = new long[2];
                c.close();
            }
            return jArr;
        } finally {
            c.close();
        }
    }
}
