package com.netqin.antivirus.networkmanager.model;

import android.database.sqlite.SQLiteDatabase;

public interface IModel {
    void addModelListener(IModelListener iModelListener);

    void insert(SQLiteDatabase sQLiteDatabase);

    boolean isDeleted();

    boolean isDirty();

    boolean isNew();

    void load(SQLiteDatabase sQLiteDatabase);

    void remove(SQLiteDatabase sQLiteDatabase);

    void removeModelListener(IModelListener iModelListener);

    void update(SQLiteDatabase sQLiteDatabase);
}
