package com.netqin.antivirus.networkmanager.model;

public interface IModelListener {
    void modelChanged(IModel iModel);

    void modelLoaded(IModel iModel);
}
