package com.netqin.antivirus.networkmanager.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import com.netqin.antivirus.networkmanager.NetApplication;
import com.netqin.antivirus.networkmanager.model.DatabaseHelper;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Properties;

public class Counter extends AbstractModel {
    public static final String ALERT_BYTES = "alert-bytes";
    public static final String ALERT_UNIT = "alert-unit";
    public static final String ALERT_VALUE = "alert-value";
    public static final String DAY = "day";
    private static final boolean DBG = false;
    public static final int MONTHLY = 0;
    public static final String NUMBER = "number";
    private static final String TAG = Counter.class.getSimpleName();
    private final long[] mBytes;
    private long mId;
    private final Interface mInterface;
    private boolean mIsDirtyInternal;
    private final Properties mProperties;
    private final String[] mStringBytes;
    private final String[] mStringDate;
    private String mStringTotal;
    private String mStringType;
    private int mType;
    private String mWhere;

    public Counter(long id, Interface inter) {
        this(inter);
        this.mId = id;
        this.mWhere = "_id=" + id;
    }

    public Counter(Interface inter) {
        this.mType = 0;
        this.mBytes = new long[2];
        this.mStringBytes = new String[]{"", ""};
        this.mStringDate = new String[]{"", ""};
        this.mProperties = new Properties();
        this.mIsDirtyInternal = false;
        this.mInterface = inter;
        this.mStringType = prettyType();
    }

    public long getId() {
        return this.mId;
    }

    public Interface getInterface() {
        return this.mInterface;
    }

    public synchronized void setType(int type) {
        if (this.mType != type) {
            this.mProperties.remove(ALERT_VALUE);
            this.mProperties.remove(ALERT_UNIT);
            this.mProperties.remove(ALERT_BYTES);
            this.mProperties.remove("number");
            this.mProperties.remove("day");
            setType0(type);
            this.mIsDirtyInternal = true;
            setDirty(true);
            fireModelChanged();
        }
    }

    private synchronized void setType0(int type) {
        this.mType = type;
        this.mStringType = prettyType();
    }

    public synchronized int getType() {
        return this.mType;
    }

    public synchronized String getTypeAsString() {
        return this.mStringType;
    }

    public synchronized void setProperty(String name, String value) {
        this.mProperties.setProperty(name, value);
        if ("number".equals(name)) {
            this.mStringType = prettyType();
        }
        this.mIsDirtyInternal = true;
        setDirty(true);
        fireModelChanged();
    }

    public synchronized String getProperty(String name) {
        return this.mProperties.getProperty(name);
    }

    public synchronized String getProperty(String name, String defaultValue) {
        return this.mProperties.getProperty(name, defaultValue);
    }

    public synchronized Object removeProperty(String key) {
        this.mIsDirtyInternal = true;
        setDirty(true);
        fireModelChanged();
        return this.mProperties.remove(key);
    }

    private synchronized void setBytes(long[] bytes) {
        boolean change = false;
        for (int i = 0; i < 2; i++) {
            if (this.mBytes[i] != bytes[i]) {
                change = true;
                this.mBytes[i] = bytes[i];
                this.mStringBytes[i] = null;
            }
        }
        if (change) {
            this.mStringTotal = null;
        }
    }

    public synchronized long[] getBytes() {
        return this.mBytes;
    }

    public synchronized String[] getBytesAsString() {
        for (int i = 0; i < 2; i++) {
            if (this.mStringBytes[i] == null) {
                this.mStringBytes[i] = prettyBytes(this.mBytes[i]);
            }
        }
        return this.mStringBytes;
    }

    public synchronized String getTotalAsString() {
        if (this.mStringTotal == null) {
            this.mStringTotal = prettyBytes(this.mBytes[0] + this.mBytes[1]);
        }
        return this.mStringTotal;
    }

    public synchronized String getStartDate() {
        return this.mStringDate[0];
    }

    public synchronized String getEndDate() {
        return this.mStringDate[1];
    }

    public synchronized void load(SQLiteDatabase db) {
        SQLiteQueryBuilder query = new SQLiteQueryBuilder();
        query.setTables(DatabaseHelper.Counters.TABLE_NAME);
        Cursor c = query.query(db, null, this.mWhere, null, null, null, null);
        if (c != null) {
            try {
                if (c.moveToNext()) {
                    String v = c.getString(c.getColumnIndex(DatabaseHelper.Counters.VALUE));
                    if (v != null) {
                        try {
                            this.mProperties.load(new ByteArrayInputStream(v.getBytes()));
                        } catch (IOException e) {
                        }
                    }
                    setType0(c.getInt(c.getColumnIndex("type")));
                }
            } finally {
                c.close();
            }
        }
        loadBytes(db);
    }

    public synchronized void insert(SQLiteDatabase db) {
        ContentValues values = new ContentValues();
        values.put("interface", this.mInterface.getName());
        values.put("type", Integer.valueOf(this.mType));
        putProperties(values);
        this.mId = db.insert(DatabaseHelper.Counters.TABLE_NAME, null, values);
        this.mWhere = "_id=" + this.mId;
        setDirty(true);
        fireModelChanged();
    }

    public void remove(SQLiteDatabase db) {
        db.delete(DatabaseHelper.Counters.TABLE_NAME, this.mWhere, null);
    }

    public synchronized void update(SQLiteDatabase db) {
        if (this.mIsDirtyInternal) {
            ContentValues values = new ContentValues();
            values.put("type", Integer.valueOf(this.mType));
            putProperties(values);
            db.update(DatabaseHelper.Counters.TABLE_NAME, values, this.mWhere, null);
            this.mIsDirtyInternal = false;
        }
        loadBytes(db);
    }

    public static final String prettyBytes(long value) {
        int i;
        StringBuilder sb = new StringBuilder();
        if (value < 1024) {
            sb.append(String.valueOf(value));
            i = 0;
        } else if (value < 1048576) {
            sb.append(String.format("%.1f", Double.valueOf(((double) value) / 1024.0d)));
            i = 1;
        } else if (value < 1073741824) {
            sb.append(String.format("%.2f", Double.valueOf(((double) value) / 1048576.0d)));
            i = 2;
        } else if (value < 1099511627776L) {
            sb.append(String.format("%.3f", Double.valueOf(((double) value) / 1.073741824E9d)));
            i = 3;
        } else {
            sb.append(String.format("%.4f", Double.valueOf(((double) value) / 1.099511627776E12d)));
            i = 4;
        }
        sb.append(NetApplication.mNetworkData.BYTE_UNITS[i]);
        return sb.toString();
    }

    private String prettyType() {
        switch (this.mType) {
            case 0:
                return NetApplication.mNetworkData.COUNTER_TYPES[this.mType].toString();
            default:
                return NetApplication.mNetworkData.COUNTER_TYPES[this.mType].toString();
        }
    }

    private synchronized void loadBytes(SQLiteDatabase db) {
        Calendar c = Calendar.getInstance();
        switch (getType()) {
            case 0:
                String value = getProperty("day", "1981-01-01");
                int number = Integer.valueOf(getProperty("number", "0")).intValue();
                int v = Integer.valueOf(value.split("-")[2]).intValue();
                Calendar cc = Calendar.getInstance();
                cc.setTimeInMillis(c.getTimeInMillis());
                cc.set(5, v);
                if (c.before(cc)) {
                    c.add(2, -1);
                }
                c.add(2, -number);
                c.set(5, v);
                setBytes(this.mInterface.getInterfaceBytes(db, c, null));
                this.mStringDate[0] = DatabaseHelper.getLocaleDate(c);
                this.mStringDate[1] = DatabaseHelper.getLocaleDate(Calendar.getInstance());
                break;
            default:
                setBytes(this.mInterface.getInterfaceBytes(db, null, null));
                this.mStringDate[1] = DatabaseHelper.getLocaleDate(c);
                this.mStringDate[0] = DatabaseHelper.getLocaleDate(this.mInterface.getLastReset());
                break;
        }
    }

    private synchronized void putProperties(ContentValues values) {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        try {
            this.mProperties.store(os, "");
            values.put(DatabaseHelper.Counters.VALUE, os.toString());
            try {
                os.close();
            } catch (IOException e) {
            }
        } catch (IOException e2) {
            try {
                os.close();
            } catch (IOException e3) {
            }
        } catch (Throwable th) {
            try {
                os.close();
            } catch (IOException e4) {
            }
            throw th;
        }
        return;
    }
}
