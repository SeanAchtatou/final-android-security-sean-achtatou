package com.netqin.antivirus.networkmanager.model;

import android.content.res.Resources;
import com.netqin.antivirus.networkmanager.NetApplication;
import com.netqin.antivirus.services.SysClassNet;
import com.nqmobile.antivirus_ampro20.R;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* compiled from: Device */
class DiscoverableDevice extends Device {
    private static final String[] CELL_INTERFACES = {"rmnet0", "pdp0", "ppp0"};
    private static final String[] WIFI_INTERFACES = {"eth0", "tiwlan0", "wlan0", "athwlan0", "eth1"};
    private String mCell = null;
    private String mWiFi = null;

    DiscoverableDevice() {
    }

    public String getCell() {
        if (this.mCell == null) {
            String[] strArr = CELL_INTERFACES;
            int length = strArr.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    break;
                }
                String inter = strArr[i];
                if (SysClassNet.isUp(inter)) {
                    this.mCell = inter;
                    break;
                }
                i++;
            }
        }
        return this.mCell;
    }

    public String getWiFi() {
        if (this.mWiFi == null) {
            String[] strArr = WIFI_INTERFACES;
            int length = strArr.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    break;
                }
                String inter = strArr[i];
                if (SysClassNet.isUp(inter)) {
                    this.mWiFi = inter;
                    break;
                }
                i++;
            }
        }
        return this.mWiFi;
    }

    public synchronized String[] getInterfaces() {
        List<String> tmp;
        tmp = new ArrayList<>();
        if (getCell() != null) {
            tmp.add(getCell());
        }
        if (getWiFi() != null) {
            tmp.add(getWiFi());
        }
        return (String[]) tmp.toArray(new String[tmp.size()]);
    }

    public String getPrettyName(String inter) {
        Resources r = NetApplication.resources();
        if (getCell() != null && getCell().equals(inter)) {
            return r.getString(R.string.meter_interface_type_cell);
        }
        if (getWiFi() != null && getWiFi().equals(inter)) {
            return r.getString(R.string.meter_interface_type_wifi);
        }
        if (Arrays.asList(CELL_INTERFACES).contains(inter)) {
            return r.getString(R.string.meter_interface_type_cell);
        }
        return Arrays.asList(WIFI_INTERFACES).contains(inter) ? r.getString(R.string.meter_interface_type_wifi) : inter;
    }

    public int getIcon(String inter) {
        return (getCell() == null || !getCell().equals(inter)) ? Arrays.asList(CELL_INTERFACES).contains(inter) ? R.drawable.ic_cell : Arrays.asList(WIFI_INTERFACES).contains(inter) ? R.drawable.ic_wifi : R.drawable.ic_wifi : R.drawable.ic_cell;
    }

    public boolean isCell(String inter) {
        return Arrays.asList(CELL_INTERFACES).contains(inter);
    }

    public boolean isWiFi(String inter) {
        return Arrays.asList(WIFI_INTERFACES).contains(inter);
    }
}
