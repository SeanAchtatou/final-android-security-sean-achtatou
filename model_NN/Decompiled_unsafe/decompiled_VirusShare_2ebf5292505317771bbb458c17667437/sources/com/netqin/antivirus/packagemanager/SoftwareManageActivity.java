package com.netqin.antivirus.packagemanager;

import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;
import com.netqin.antivirus.data.PreferenceDataHelper;
import com.nqmobile.antivirus_ampro20.R;

public class SoftwareManageActivity extends TabActivity {
    public static Intent getLaunchIntent(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, SoftwareManageActivity.class);
        return intent;
    }

    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(1);
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.software_manage);
        ((TextView) findViewById(R.id.activity_name)).setText((int) R.string.package_management);
        PreferenceDataHelper.initKeyProcessList(this);
        installTabHost();
    }

    private void installTabHost() {
        installCustomTabs(getTabHost());
    }

    private void installCustomTabs(TabHost tabHost) {
        tabHost.addTab(tabHost.newTabSpec("package_installed").setIndicator(getIndicatorView(R.string.tab_package_installed, R.drawable.tab_ic_summary)).setContent(PackageInstalledActivity.getLaunchIntent(this)));
        tabHost.addTab(tabHost.newTabSpec("package_all").setIndicator(getIndicatorView(R.string.tab_package_all, R.drawable.tab_ic_summary)).setContent(PackageSDActivity.getLaunchIntent(this)));
        tabHost.addTab(tabHost.newTabSpec("package_autoboot").setIndicator(getIndicatorView(R.string.tab_package_autoboot, R.drawable.tab_ic_mynetqin)).setContent(PackageAutobootActivity.getLaunchIntent(this)));
    }

    private View getIndicatorView(int titleId, int iconId) {
        View indicator = ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.tab_indicator, (ViewGroup) null);
        ((ImageView) indicator.findViewById(R.id.iv_indicator)).setImageResource(iconId);
        ((TextView) indicator.findViewById(R.id.tv_indicator)).setText(titleId);
        return indicator;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        System.gc();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }
}
