package com.netqin.antivirus.packagemanager;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import com.netqin.antivirus.packagemanager.PackageSDActivity;
import com.nqmobile.antivirus_ampro20.R;
import java.util.ArrayList;
import java.util.Iterator;

public class PackageApkAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<PackageSDActivity.ApkElement> mData;
    private LayoutInflater mInflater;
    private View vRemove;

    public PackageApkAdapter(Context context, ArrayList<PackageSDActivity.ApkElement> data, View v) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.vRemove = v;
    }

    public synchronized ArrayList<PackageSDActivity.ApkElement> getAll() {
        return this.mData;
    }

    public synchronized ArrayList<PackageSDActivity.ApkElement> getSelected() {
        ArrayList<PackageSDActivity.ApkElement> arrayList;
        if (this.mData == null || getCount() == 0) {
            arrayList = null;
        } else {
            ArrayList<PackageSDActivity.ApkElement> selected = new ArrayList<>(getCount());
            Iterator<PackageSDActivity.ApkElement> it = this.mData.iterator();
            while (it.hasNext()) {
                PackageSDActivity.ApkElement app = it.next();
                if (app.isChecked) {
                    selected.add(app);
                }
            }
            arrayList = selected;
        }
        return arrayList;
    }

    public synchronized int getCount() {
        return this.mData.size();
    }

    public synchronized Object getItem(int position) {
        return this.mData.get(position);
    }

    public synchronized long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = this.mInflater.inflate((int) R.layout.package_apk_item, (ViewGroup) null);
            holder = new ViewHolder();
            holder.checkBox = (CheckBox) convertView.findViewById(R.id.package_apk_checkbox);
            holder.iconView = (ImageView) convertView.findViewById(R.id.package_apk_icon);
            holder.nameView = (TextView) convertView.findViewById(R.id.package_apk_name);
            holder.sizeView = (TextView) convertView.findViewById(R.id.apk_path);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final PackageSDActivity.ApkElement pkg = getApkElement(position);
        holder.nameView.setText(pkg.getApkName());
        holder.sizeView.setText(this.mContext.getResources().getString(R.string.apk_size, pkg.getSize()));
        holder.checkBox.setChecked(pkg.isChecked);
        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (holder.checkBox.isChecked()) {
                    pkg.isChecked = true;
                } else {
                    pkg.isChecked = false;
                }
                PackageApkAdapter.this.changeRemoveStatus();
            }
        });
        return convertView;
    }

    public synchronized PackageSDActivity.ApkElement getApkElement(int pos) {
        return this.mData.get(pos);
    }

    public synchronized void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        changeRemoveStatus();
    }

    static class ViewHolder {
        CheckBox checkBox;
        ImageView iconView;
        TextView nameView;
        TextView sizeView;

        ViewHolder() {
        }
    }

    public synchronized void remove(int i) {
        this.mData.remove(i);
    }

    public void remove(PackageSDActivity.ApkElement app) {
        remove(app, false);
    }

    public synchronized void remove(PackageSDActivity.ApkElement app, boolean notify) {
        this.mData.remove(app);
        if (notify) {
            notifyDataSetChanged();
        }
    }

    public synchronized void add(PackageSDActivity.ApkElement apk) {
        this.mData.add(apk);
        notifyDataSetChanged();
    }

    public synchronized void changeRemoveStatus() {
        Button ll = (Button) this.vRemove;
        if (getSelected() == null || getSelected().size() <= 0) {
            ll.setEnabled(false);
            ll.setClickable(false);
            ll.setTextColor(-7829368);
        } else {
            ll.setEnabled(true);
            ll.setClickable(true);
            ll.setTextColor(-1);
        }
    }
}
