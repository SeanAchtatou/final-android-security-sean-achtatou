package com.netqin.antivirus.packagemanager;

import SHcMjZX.DD02zqNU;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import com.netqin.antivirus.cloud.model.CloudApkInfo;
import com.netqin.antivirus.cloud.model.CloudApkInfoList;
import com.netqin.antivirus.data.PackageElement;
import com.netqin.antivirus.util.PackageManagerUtils;
import com.netqin.antivirus.util.SystemUtils;
import com.nqmobile.antivirus_ampro20.R;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

public class PackageInstalledActivity extends ListActivity implements View.OnClickListener {
    private ArrayList<PackageElement> appsNeedUninstall;
    /* access modifiers changed from: private */
    public PackageInfoAdapter mAdapter;
    private Button mBtnUninstall;
    /* access modifiers changed from: private */
    public Handler mHandler;
    private Runnable mInitApkInfo = new Runnable() {
        public void run() {
            ArrayList<PackageElement> mPkgUnknown = new ArrayList<>();
            ArrayList<PackageElement> mPkgCommon = new ArrayList<>();
            ArrayList<PackageElement> mPkgRisk = new ArrayList<>();
            Iterator<PackageElement> iterator = PackageInstalledActivity.this.mPkgPrepared.iterator();
            while (iterator.hasNext()) {
                PackageElement pkg = (PackageElement) iterator.next();
                try {
                    String packageName = pkg.getResolveInfo().activityInfo.packageName;
                    String sb = new StringBuilder(String.valueOf(DD02zqNU.DLLIxskak(PackageInstalledActivity.this.mPackageManager, packageName, 1).versionCode)).toString();
                    String str = DD02zqNU.DLLIxskak(PackageInstalledActivity.this.mPackageManager, packageName, 1).versionName;
                    ApplicationInfo applicationInfo = PackageInstalledActivity.this.mPackageManager.getApplicationInfo(packageName, 1);
                    CloudApkInfo apkData = null;
                    List<CloudApkInfo> list = CloudApkInfoList.getApkInfoList(PackageInstalledActivity.this);
                    if (list != null) {
                        int i = 0;
                        while (true) {
                            if (i >= list.size()) {
                                break;
                            } else if (packageName.equals(list.get(i).getPkgName())) {
                                apkData = list.get(i);
                                break;
                            } else {
                                i++;
                            }
                        }
                    }
                    pkg.haveServerData = true;
                    pkg.setSscurity("40");
                    pkg.setScore("0");
                    if (apkData != null) {
                        String securityId = apkData.getSecurity();
                        pkg.setScore(apkData.getScore());
                        pkg.setSscurity(securityId);
                        pkg.setSscurityDesc(apkData.getSecurityDesc());
                        if ("10".equals(securityId)) {
                            pkg.isChecked = true;
                            mPkgRisk.add(pkg);
                        } else {
                            mPkgCommon.add(pkg);
                        }
                    } else {
                        mPkgUnknown.add(pkg);
                    }
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
            }
            Iterator<PackageElement> iterator2 = mPkgCommon.iterator();
            while (iterator2.hasNext()) {
                mPkgRisk.add((PackageElement) iterator2.next());
            }
            Iterator<PackageElement> iterator3 = mPkgUnknown.iterator();
            while (iterator3.hasNext()) {
                mPkgRisk.add(iterator3.next());
            }
            PackageInstalledActivity.this.mPkg3rd.clear();
            Iterator<PackageElement> iterator4 = mPkgRisk.iterator();
            while (iterator4.hasNext()) {
                PackageInstalledActivity.this.mPkg3rd.add(iterator4.next());
            }
            PackageInstalledActivity.this.mHandler.sendEmptyMessage(0);
            PackageInstalledActivity.this.runOnUiThread(new Runnable() {
                public void run() {
                }
            });
        }
    };
    /* access modifiers changed from: private */
    public ListView mListView;
    /* access modifiers changed from: private */
    public PackageManager mPackageManager;
    /* access modifiers changed from: private */
    public ArrayList<PackageElement> mPkg3rd = null;
    private ArrayList<PackageElement> mPkgAutoBoot = null;
    /* access modifiers changed from: private */
    public ArrayList<PackageElement> mPkgPrepared = null;
    ProgressDialog mProgressDialog;
    protected String mTaskTip;
    private Runnable mUpdateApkInfo = new Runnable() {
        /* JADX WARNING: Removed duplicated region for block: B:25:0x0123  */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x012b  */
        /* JADX WARNING: Removed duplicated region for block: B:40:0x010a A[SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r24 = this;
                r0 = r24
                com.netqin.antivirus.packagemanager.PackageInstalledActivity r0 = com.netqin.antivirus.packagemanager.PackageInstalledActivity.this
                r21 = r0
                android.widget.ListView r14 = r21.mListView
                int r12 = r14.getCount()
                r5 = 0
                java.util.Vector r18 = new java.util.Vector
                r18.<init>()
                r11 = 0
                r6 = r5
            L_0x0016:
                if (r11 < r12) goto L_0x0045
                int r21 = r18.size()
                if (r21 <= 0) goto L_0x0032
                r0 = r24
                com.netqin.antivirus.packagemanager.PackageInstalledActivity r0 = com.netqin.antivirus.packagemanager.PackageInstalledActivity.this
                r21 = r0
                java.util.ArrayList r21 = r21.mPkg3rd
                java.util.Iterator r13 = r21.iterator()
            L_0x002c:
                boolean r21 = r13.hasNext()
                if (r21 != 0) goto L_0x012f
            L_0x0032:
                r0 = r24
                com.netqin.antivirus.packagemanager.PackageInstalledActivity r0 = com.netqin.antivirus.packagemanager.PackageInstalledActivity.this
                r21 = r0
                com.netqin.antivirus.packagemanager.PackageInstalledActivity$2$1 r22 = new com.netqin.antivirus.packagemanager.PackageInstalledActivity$2$1
                r0 = r22
                r1 = r24
                r0.<init>()
                r21.runOnUiThread(r22)
                return
            L_0x0045:
                java.lang.Object r16 = r14.getItemAtPosition(r11)
                com.netqin.antivirus.data.PackageElement r16 = (com.netqin.antivirus.data.PackageElement) r16
                android.content.pm.ResolveInfo r21 = r16.getResolveInfo()     // Catch:{ NameNotFoundException -> 0x010f, all -> 0x0127 }
                r0 = r21
                android.content.pm.ActivityInfo r0 = r0.activityInfo     // Catch:{ NameNotFoundException -> 0x010f, all -> 0x0127 }
                r21 = r0
                r0 = r21
                java.lang.String r0 = r0.packageName     // Catch:{ NameNotFoundException -> 0x010f, all -> 0x0127 }
                r15 = r0
                java.lang.StringBuilder r21 = new java.lang.StringBuilder     // Catch:{ NameNotFoundException -> 0x010f, all -> 0x0127 }
                r0 = r24
                com.netqin.antivirus.packagemanager.PackageInstalledActivity r0 = com.netqin.antivirus.packagemanager.PackageInstalledActivity.this     // Catch:{ NameNotFoundException -> 0x010f, all -> 0x0127 }
                r22 = r0
                android.content.pm.PackageManager r22 = r22.mPackageManager     // Catch:{ NameNotFoundException -> 0x010f, all -> 0x0127 }
                r23 = 1
                r0 = r22
                r1 = r15
                r2 = r23
                android.content.pm.PackageInfo r22 = SHcMjZX.DD02zqNU.DLLIxskak(r0, r1, r2)     // Catch:{ NameNotFoundException -> 0x010f, all -> 0x0127 }
                r0 = r22
                int r0 = r0.versionCode     // Catch:{ NameNotFoundException -> 0x010f, all -> 0x0127 }
                r22 = r0
                java.lang.String r22 = java.lang.String.valueOf(r22)     // Catch:{ NameNotFoundException -> 0x010f, all -> 0x0127 }
                r21.<init>(r22)     // Catch:{ NameNotFoundException -> 0x010f, all -> 0x0127 }
                java.lang.String r19 = r21.toString()     // Catch:{ NameNotFoundException -> 0x010f, all -> 0x0127 }
                r0 = r24
                com.netqin.antivirus.packagemanager.PackageInstalledActivity r0 = com.netqin.antivirus.packagemanager.PackageInstalledActivity.this     // Catch:{ NameNotFoundException -> 0x010f, all -> 0x0127 }
                r21 = r0
                android.content.pm.PackageManager r21 = r21.mPackageManager     // Catch:{ NameNotFoundException -> 0x010f, all -> 0x0127 }
                r22 = 1
                r0 = r21
                r1 = r15
                r2 = r22
                android.content.pm.PackageInfo r21 = SHcMjZX.DD02zqNU.DLLIxskak(r0, r1, r2)     // Catch:{ NameNotFoundException -> 0x010f, all -> 0x0127 }
                r0 = r21
                java.lang.String r0 = r0.versionName     // Catch:{ NameNotFoundException -> 0x010f, all -> 0x0127 }
                r20 = r0
                r0 = r24
                com.netqin.antivirus.packagemanager.PackageInstalledActivity r0 = com.netqin.antivirus.packagemanager.PackageInstalledActivity.this     // Catch:{ NameNotFoundException -> 0x010f, all -> 0x0127 }
                r21 = r0
                android.content.pm.PackageManager r21 = r21.mPackageManager     // Catch:{ NameNotFoundException -> 0x010f, all -> 0x0127 }
                r22 = 1
                r0 = r21
                r1 = r15
                r2 = r22
                android.content.pm.ApplicationInfo r9 = r0.getApplicationInfo(r1, r2)     // Catch:{ NameNotFoundException -> 0x010f, all -> 0x0127 }
                com.netqin.antivirus.cloud.apkinfo.db.CloudApiInfoDb r5 = new com.netqin.antivirus.cloud.apkinfo.db.CloudApiInfoDb     // Catch:{ NameNotFoundException -> 0x010f, all -> 0x0127 }
                r0 = r24
                com.netqin.antivirus.packagemanager.PackageInstalledActivity r0 = com.netqin.antivirus.packagemanager.PackageInstalledActivity.this     // Catch:{ NameNotFoundException -> 0x010f, all -> 0x0127 }
                r21 = r0
                r0 = r5
                r1 = r21
                r0.<init>(r1)     // Catch:{ NameNotFoundException -> 0x010f, all -> 0x0127 }
                r0 = r9
                java.lang.String r0 = r0.publicSourceDir     // Catch:{ NameNotFoundException -> 0x014a }
                r21 = r0
                java.lang.String r22 = ".RSA"
                r0 = r24
                com.netqin.antivirus.packagemanager.PackageInstalledActivity r0 = com.netqin.antivirus.packagemanager.PackageInstalledActivity.this     // Catch:{ NameNotFoundException -> 0x014a }
                r23 = r0
                com.netqin.antivirus.packagemanager.PackageInfoAdapter r23 = r23.mAdapter     // Catch:{ NameNotFoundException -> 0x014a }
                java.lang.String r23 = r23.cert_rsa_path()     // Catch:{ NameNotFoundException -> 0x014a }
                byte[] r21 = com.netqin.antivirus.cloud.model.CloudHandler.getSignFile(r21, r22, r23)     // Catch:{ NameNotFoundException -> 0x014a }
                r0 = r5
                r1 = r15
                r2 = r19
                r3 = r20
                r4 = r21
                com.netqin.antivirus.cloud.model.CloudApkInfo r7 = r0.getCloudApkData(r1, r2, r3, r4)     // Catch:{ NameNotFoundException -> 0x014a }
                r21 = 1
                r0 = r21
                r1 = r16
                r1.haveServerData = r0     // Catch:{ NameNotFoundException -> 0x014a }
                if (r7 == 0) goto L_0x0105
                java.lang.String r17 = r7.getSecurity()     // Catch:{ NameNotFoundException -> 0x014a }
                java.lang.String r8 = r7.getScore()     // Catch:{ NameNotFoundException -> 0x014a }
                r0 = r16
                r1 = r8
                r0.setScore(r1)     // Catch:{ NameNotFoundException -> 0x014a }
                r21 = 1
                r0 = r21
                r1 = r16
                r1.haveServerData = r0     // Catch:{ NameNotFoundException -> 0x014a }
            L_0x0105:
                if (r5 == 0) goto L_0x010a
                r5.close_virus_DB()
            L_0x010a:
                int r11 = r11 + 1
                r6 = r5
                goto L_0x0016
            L_0x010f:
                r21 = move-exception
                r10 = r21
                r5 = r6
            L_0x0113:
                r10.printStackTrace()     // Catch:{ all -> 0x0148 }
                java.lang.String r21 = r16.getPackageName()     // Catch:{ all -> 0x0148 }
                r0 = r18
                r1 = r21
                r0.add(r1)     // Catch:{ all -> 0x0148 }
                if (r5 == 0) goto L_0x010a
                r5.close_virus_DB()
                goto L_0x010a
            L_0x0127:
                r21 = move-exception
                r5 = r6
            L_0x0129:
                if (r5 == 0) goto L_0x012e
                r5.close_virus_DB()
            L_0x012e:
                throw r21
            L_0x012f:
                java.lang.Object r16 = r13.next()
                com.netqin.antivirus.data.PackageElement r16 = (com.netqin.antivirus.data.PackageElement) r16
                java.lang.String r21 = r16.getPackageName()
                r0 = r18
                r1 = r21
                boolean r21 = r0.contains(r1)
                if (r21 == 0) goto L_0x002c
                r13.remove()
                goto L_0x002c
            L_0x0148:
                r21 = move-exception
                goto L_0x0129
            L_0x014a:
                r21 = move-exception
                r10 = r21
                goto L_0x0113
            */
            throw new UnsupportedOperationException("Method not decompiled: com.netqin.antivirus.packagemanager.PackageInstalledActivity.AnonymousClass2.run():void");
        }
    };

    public static Intent getLaunchIntent(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, PackageInstalledActivity.class);
        return intent;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.package_installed);
        this.mPackageManager = getPackageManager();
        initRes();
        initListView();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        new Thread(this.mUpdateApkInfo).start();
    }

    private void initRes() {
        this.mBtnUninstall = (Button) findViewById(R.id.btn_function);
        this.mBtnUninstall.setClickable(false);
        this.mBtnUninstall.setTextColor(-7829368);
        this.mBtnUninstall.setOnClickListener(this);
        this.mListView = getListView();
        this.mProgressDialog = new ProgressDialog(this);
        this.mProgressDialog.setProgressStyle(0);
        this.mProgressDialog.setCancelable(false);
        this.mProgressDialog.setMessage(getString(R.string.netqin_loding_desc));
        this.mHandler = new Handler() {
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 0:
                        PackageInstalledActivity.this.mProgressDialog.dismiss();
                        PackageInstalledActivity.this.mAdapter.notifyDataSetChanged();
                        break;
                }
                super.handleMessage(msg);
            }
        };
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        if (this.mAdapter != null) {
            PackageElement app = this.mAdapter.getPackageElement(position);
            String packageName = app.getResolveInfo().activityInfo.packageName;
            Intent intent = new Intent();
            intent.setClass(this, ApkInfoActivity.class);
            intent.putExtra("package_name", packageName);
            intent.putExtra("apk_name", app.getResolveInfo().loadLabel(this.mPackageManager));
            startActivity(intent);
        }
    }

    private void initListView() {
        Hashtable<String, ResolveInfo> mPkgAll = PackageManagerUtils.getPkgAll(this);
        this.mPkg3rd = new ArrayList<>();
        this.mPkgPrepared = new ArrayList<>();
        this.mAdapter = new PackageInfoAdapter(this, this.mPkg3rd, this.mBtnUninstall);
        setListAdapter(this.mAdapter);
        this.mPkgPrepared.clear();
        for (ResolveInfo pkg : mPkgAll.values()) {
            if (!SystemUtils.isSystemPackage(pkg.activityInfo.applicationInfo)) {
                this.mPkgPrepared.add(new PackageElement(pkg));
            }
        }
        this.mProgressDialog.show();
        new Thread(this.mInitApkInfo).start();
    }

    public synchronized ArrayList<PackageElement> getAll() {
        return this.mPkgPrepared;
    }

    private ArrayList<PackageElement> get3rdPkg() {
        Hashtable<String, ResolveInfo> mPkgAll = PackageManagerUtils.getPkgAll(this);
        this.mPkg3rd = new ArrayList<>();
        for (ResolveInfo pkg : mPkgAll.values()) {
            if (!SystemUtils.isSystemPackage(pkg.activityInfo.applicationInfo)) {
                this.mPkg3rd.add(new PackageElement(pkg));
            }
        }
        return this.mPkg3rd;
    }

    private ArrayList<PackageElement> getAutobootPkg() {
        PackageManager pm = getPackageManager();
        Hashtable<String, ResolveInfo> mPkgAll = PackageManagerUtils.getPkgAll(this);
        this.mPkgAutoBoot = new ArrayList<>();
        for (ResolveInfo pkg : mPkgAll.values()) {
            if (!SystemUtils.isSystemPackage(pkg.activityInfo.applicationInfo) && pm.checkPermission("android.permission.RECEIVE_BOOT_COMPLETED", pkg.activityInfo.applicationInfo.packageName) == 0) {
                this.mPkgAutoBoot.add(new PackageElement(pkg));
            }
        }
        return this.mPkgAutoBoot;
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_function /*2131558618*/:
                onUninstallSelected();
                return;
            default:
                return;
        }
    }

    private void onUninstallSelected() {
        this.appsNeedUninstall = this.mAdapter.getSelected();
        if (this.appsNeedUninstall == null) {
            return;
        }
        if (this.appsNeedUninstall.size() > 0) {
            startUninstallApps();
        } else {
            Toast.makeText(this, (int) R.string.select_app_uninstall, 0).show();
        }
    }

    private void startUninstallApps() {
        Iterator<PackageElement> it = this.appsNeedUninstall.iterator();
        while (it.hasNext()) {
            startActivity(new Intent("android.intent.action.DELETE", Uri.fromParts("package", it.next().getResolveInfo().activityInfo.packageName, null)));
        }
    }
}
