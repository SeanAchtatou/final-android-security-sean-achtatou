package com.netqin.antivirus.packagemanager;

import SHcMjZX.DD02zqNU;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.netqin.antivirus.cloud.apkinfo.domain.Rate;
import com.netqin.antivirus.cloud.apkinfo.domain.Review;
import com.netqin.antivirus.cloud.model.CloudHandler;
import com.netqin.antivirus.cloud.view.CloudListener;
import com.netqin.antivirus.networkmanager.SettingPreferences;
import com.netqin.antivirus.ui.BaseListActivity;
import com.nqmobile.antivirus_ampro20.R;
import java.util.ArrayList;
import java.util.List;

public class CommentListActivity extends BaseListActivity implements View.OnClickListener, CloudListener {
    public static final int PER_PAGE = 10;
    private Drawable apkIcon;
    private String apkName;
    private String apkRateNum;
    private String apkVersionCode;
    private String apkVersionName;
    /* access modifiers changed from: private */
    public int currentPage = 0;
    View footView;
    /* access modifiers changed from: private */
    public boolean isRefreshFoot = false;
    /* access modifiers changed from: private */
    public CommentListAdapter mAdapter;
    private ImageView mApkIcon;
    private TextView mApkName;
    private TextView mApkVersion;
    private List<Review> mCommentList = new ArrayList();
    private Handler mHandler;
    /* access modifiers changed from: private */
    public ListView mListView;
    PackageManager mPackageManager;
    private String mPackageName;
    /* access modifiers changed from: private */
    public int mPageNum = 0;
    private String mServerId;
    /* access modifiers changed from: private */
    public TextView mTextEmpty;
    /* access modifiers changed from: private */
    public Handler scrollHandler;
    AbsListView.OnScrollListener scrollListener = new AbsListView.OnScrollListener() {
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            if (firstVisibleItem + visibleItemCount == totalItemCount) {
                CommentListActivity.this.isRefreshFoot = true;
            } else {
                CommentListActivity.this.isRefreshFoot = false;
            }
        }

        public void onScrollStateChanged(AbsListView view, int scrollState) {
            if (scrollState == 0 && CommentListActivity.this.isRefreshFoot) {
                if (CommentListActivity.this.currentPage + 1 > CommentListActivity.this.mPageNum) {
                    CommentListActivity.this.footView.setVisibility(8);
                    CommentListActivity.this.mListView.removeFooterView(CommentListActivity.this.footView);
                    return;
                }
                CommentListActivity.this.scrollHandler.sendEmptyMessage(1);
                CommentListActivity.this.initAndRating();
            }
        }
    };

    public static Intent getLaunchIntent(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, CommentListActivity.class);
        return intent;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.comment_list);
        ((TextView) findViewById(R.id.activity_name)).setText((int) R.string.comment_title);
        this.mHandler = new Handler() {
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 3:
                        List<Review> m = (List) msg.obj;
                        if (m.size() != 0) {
                            CommentListActivity commentListActivity = CommentListActivity.this;
                            commentListActivity.currentPage = commentListActivity.currentPage + 1;
                            for (int i = m.size() - 1; i >= 0; i--) {
                                CommentListActivity.this.mAdapter.add((Review) m.get(i));
                            }
                        } else {
                            CommentListActivity.this.mTextEmpty.setText(CommentListActivity.this.getString(R.string.package_info_comment));
                        }
                        CommentListActivity.this.footView.findViewById(R.id.list_foot_progress).setVisibility(4);
                        CommentListActivity.this.footView.findViewById(R.id.loading_msg).setVisibility(4);
                        CommentListActivity.this.footView.setVisibility(4);
                        break;
                    case 7:
                        Toast.makeText(CommentListActivity.this, (int) R.string.request_more_comment_failed, 0).show();
                        CommentListActivity.this.footView.setVisibility(8);
                        break;
                }
                super.handleMessage(msg);
            }
        };
        this.scrollHandler = new Handler() {
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 1:
                        CommentListActivity.this.footView.findViewById(R.id.list_foot_progress).setVisibility(0);
                        CommentListActivity.this.footView.findViewById(R.id.loading_msg).setVisibility(0);
                        CommentListActivity.this.footView.setVisibility(0);
                        return;
                    case 2:
                        CommentListActivity.this.footView.findViewById(R.id.list_foot_progress).setVisibility(4);
                        CommentListActivity.this.footView.findViewById(R.id.loading_msg).setVisibility(4);
                        CommentListActivity.this.footView.setVisibility(4);
                        return;
                    default:
                        return;
                }
            }
        };
        initRes();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    public String cert_rsa_path() {
        return getFilesDir() + CloudHandler.Cert_rsa_path;
    }

    private void initRes() {
        this.mListView = getListView();
        this.mApkIcon = (ImageView) findViewById(R.id.apk_icon);
        this.mApkName = (TextView) findViewById(R.id.apk_name);
        this.mApkVersion = (TextView) findViewById(R.id.apk_version);
        this.mTextEmpty = (TextView) findViewById(16908292);
        Bundle bundle = getIntent().getExtras();
        this.mPackageName = bundle.getString("package_name");
        this.apkName = bundle.getString("apk_name");
        this.apkVersionName = bundle.getString("apk_version");
        this.mServerId = bundle.getString("apk_serverId");
        this.apkRateNum = bundle.getString("apk_commentNum");
        try {
            PackageInfo info = DD02zqNU.DLLIxskak(getPackageManager(), this.mPackageName, 0);
            this.apkIcon = info.applicationInfo.loadIcon(getPackageManager());
            this.mApkIcon.setImageDrawable(this.apkIcon);
            this.apkVersionName = info.versionName;
            this.apkVersionCode = new StringBuilder(String.valueOf(info.versionCode)).toString();
        } catch (PackageManager.NameNotFoundException e) {
        }
        this.mApkName.setText(this.apkName);
        this.mApkVersion.setText(getResources().getString(R.string.version_pre, this.apkVersionName));
        initAndRating();
        this.mAdapter = new CommentListAdapter(this, this.mCommentList, true);
        this.footView = LayoutInflater.from(this).inflate((int) R.layout.list_foot, (ViewGroup) null);
        this.mListView.addFooterView(this.footView);
        this.footView.findViewById(R.id.list_foot_progress).setVisibility(0);
        this.footView.findViewById(R.id.loading_msg).setVisibility(0);
        this.footView.setVisibility(0);
        this.mListView.setAdapter((ListAdapter) this.mAdapter);
        this.mListView.setOnScrollListener(this.scrollListener);
    }

    /* access modifiers changed from: private */
    public void initAndRating() {
        if (this.currentPage > this.mPageNum) {
            this.mListView.removeFooterView(this.footView);
        } else {
            commitRating(this.currentPage + 1, 10);
        }
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    public void onClick(View v) {
    }

    private void commitRating(int offset, int length) {
        Rate rate = new Rate();
        rate.setPkgName(this.mPackageName);
        rate.setId("0");
        rate.setServerId(this.mServerId);
        rate.setName(this.apkName);
        rate.setVersionCode(this.apkVersionCode);
        rate.setVersionName(this.apkVersionName);
        rate.setOffset(String.valueOf(offset));
        rate.setLength(String.valueOf(length));
        CloudHandler.rate = rate;
        CloudHandler.getInstance(this).resetData();
        CloudHandler.getInstance(this).start(this, 3, this.mHandler, true);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        SettingPreferences.getLaunchIntent(this);
        switch (item.getItemId()) {
            case 1:
                commitRating(2, 1);
                break;
        }
        return true;
    }

    public void process(Bundle bundle) {
    }

    public void complete(int resultCode) {
    }

    public void error(int errorCode) {
    }
}
