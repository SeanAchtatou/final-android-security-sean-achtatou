package com.netqin.antivirus.packagemanager;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import com.netqin.antivirus.cloud.apkinfo.db.CloudApiInfoDb;
import com.netqin.antivirus.cloud.model.CloudHandler;
import com.netqin.antivirus.data.PackageElement;
import com.netqin.antivirus.util.AppQueryCondition;
import com.nqmobile.antivirus_ampro20.R;
import java.util.ArrayList;
import java.util.Iterator;

public class PackageInfoAdapter extends BaseAdapter {
    private CloudApiInfoDb apiInfoDb;
    private ApplicationInfo applicationInfo;
    private Context mContext;
    private ArrayList<PackageElement> mData;
    private LayoutInflater mInflater;
    private PackageManager mPackageManager;
    private AppQueryCondition mQueryCondition;
    private View vRemove;

    public PackageInfoAdapter(Context context, ArrayList<PackageElement> data, View v) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.mContext = context;
        this.mPackageManager = context.getPackageManager();
        this.vRemove = v;
    }

    public PackageInfoAdapter(Context context, ArrayList<PackageElement> data, AppQueryCondition queryCondition) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.mContext = context;
        this.mPackageManager = context.getPackageManager();
        this.mQueryCondition = queryCondition;
    }

    public synchronized ArrayList<PackageElement> getAll() {
        return this.mData;
    }

    public synchronized ArrayList<PackageElement> getSelected() {
        ArrayList<PackageElement> arrayList;
        if (this.mData == null || getCount() == 0) {
            arrayList = null;
        } else {
            ArrayList<PackageElement> selected = new ArrayList<>(getCount());
            Iterator<PackageElement> it = this.mData.iterator();
            while (it.hasNext()) {
                PackageElement app = it.next();
                if (app.isChecked) {
                    selected.add(app);
                }
            }
            arrayList = selected;
        }
        return arrayList;
    }

    public synchronized int getCount() {
        return this.mData.size();
    }

    public synchronized Object getItem(int position) {
        return this.mData.get(position);
    }

    public synchronized long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = this.mInflater.inflate((int) R.layout.package_info_item, (ViewGroup) null);
            holder = new ViewHolder();
            holder.checkBox = (CheckBox) convertView.findViewById(R.id.package_checkbox);
            holder.iconView = (ImageView) convertView.findViewById(R.id.package_icon);
            holder.nameView = (TextView) convertView.findViewById(R.id.package_name);
            holder.iconSign = (ImageView) convertView.findViewById(R.id.security_icon_sign);
            holder.descriptionView = (TextView) convertView.findViewById(R.id.package_description);
            holder.packageSecurity = (TextView) convertView.findViewById(R.id.package_security);
            holder.ratingBar = (RatingBar) convertView.findViewById(R.id.apk_list_ratingbar);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final PackageElement pkg = getPackageElement(position);
        holder.iconView.setImageDrawable(pkg.getResolveInfo().loadIcon(this.mPackageManager));
        holder.nameView.setText(pkg.getResolveInfo().loadLabel(this.mPackageManager));
        holder.checkBox.setChecked(pkg.isChecked);
        if (pkg.haveServerData) {
            if (pkg.getSecurityDesc() != null) {
                holder.iconSign.setVisibility(0);
                holder.packageSecurity.setText(pkg.getSecurityDesc());
                holder.packageSecurity.setTextColor(-16777216);
                if ("50".equals(pkg.getSecurity())) {
                    holder.iconSign.setImageResource(R.drawable.scan_safe_sign);
                } else if ("10".equals(pkg.getSecurity())) {
                    holder.iconSign.setImageResource(R.drawable.scan_danger_sign);
                    holder.packageSecurity.setTextColor(-65536);
                } else {
                    holder.iconSign.setVisibility(4);
                }
            } else {
                holder.packageSecurity.setText((int) R.string.text_security_level_unknown);
                holder.packageSecurity.setTextColor(-16777216);
                holder.iconSign.setVisibility(4);
            }
            if (pkg.getScore() != null) {
                holder.ratingBar.setRating(Float.parseFloat(pkg.getScore()));
            } else {
                holder.ratingBar.setRating(Float.parseFloat("0"));
            }
        } else {
            holder.packageSecurity.setText((int) R.string.text_security_loading);
            holder.packageSecurity.setTextColor(-16777216);
            holder.iconSign.setVisibility(4);
        }
        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (holder.checkBox.isChecked()) {
                    pkg.isChecked = true;
                } else {
                    pkg.isChecked = false;
                }
                PackageInfoAdapter.this.changeRemoveStatus();
            }
        });
        return convertView;
    }

    public synchronized PackageElement getPackageElement(int pos) {
        return this.mData.get(pos);
    }

    public synchronized void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        changeRemoveStatus();
    }

    private void initChecked() {
    }

    static class ViewHolder {
        CheckBox checkBox;
        TextView descriptionView;
        ImageView iconSign;
        ImageView iconView;
        TextView nameView;
        TextView packageSecurity;
        RatingBar ratingBar;

        ViewHolder() {
        }
    }

    public synchronized void remove(int i) {
        this.mData.remove(i);
    }

    public void remove(PackageElement app) {
        remove(app, false);
    }

    public synchronized void remove(PackageElement app, boolean notify) {
        this.mData.remove(app);
        if (notify) {
            notifyDataSetChanged();
        }
    }

    public String cert_rsa_path() {
        return this.mContext.getFilesDir() + CloudHandler.Cert_rsa_path;
    }

    /* access modifiers changed from: private */
    public void changeRemoveStatus() {
        Button ll = (Button) this.vRemove;
        if (getSelected() == null || getSelected().size() <= 0) {
            ll.setEnabled(false);
            ll.setClickable(false);
            ll.setTextColor(-7829368);
            return;
        }
        ll.setEnabled(true);
        ll.setClickable(true);
        ll.setTextColor(-1);
    }
}
