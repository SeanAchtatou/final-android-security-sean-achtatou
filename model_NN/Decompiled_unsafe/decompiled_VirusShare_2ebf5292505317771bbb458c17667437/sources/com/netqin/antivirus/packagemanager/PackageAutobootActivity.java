package com.netqin.antivirus.packagemanager;

import SHcMjZX.DD02zqNU;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.netqin.antivirus.cloud.model.CloudApkInfo;
import com.netqin.antivirus.cloud.model.CloudApkInfoList;
import com.netqin.antivirus.data.PackageElement;
import com.netqin.antivirus.util.PackageManagerUtils;
import com.netqin.antivirus.util.SystemUtils;
import com.nqmobile.antivirus_ampro20.R;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

public class PackageAutobootActivity extends ListActivity implements View.OnClickListener {
    private ArrayList<PackageElement> appsNeedUninstall;
    /* access modifiers changed from: private */
    public PackageInfoAdapter mAdapter;
    private Button mBtnUninstall;
    /* access modifiers changed from: private */
    public ListView mListView;
    /* access modifiers changed from: private */
    public PackageManager mPackageManager;
    private ArrayList<PackageElement> mPkg3rd = null;
    /* access modifiers changed from: private */
    public ArrayList<PackageElement> mPkgAutoBoot = null;
    private boolean mSelectAllStatus = false;
    protected String mTaskTip;
    private TextView mTip;
    private Runnable mUpdateApkInfo = new Runnable() {
        public void run() {
            ListView listView = PackageAutobootActivity.this.mListView;
            int itemCount = listView.getCount();
            Vector<String> v = new Vector<>();
            for (int i = 0; i < itemCount; i++) {
                PackageElement pkg = (PackageElement) listView.getItemAtPosition(i);
                try {
                    String packageName = pkg.getResolveInfo().activityInfo.packageName;
                    String sb = new StringBuilder(String.valueOf(DD02zqNU.DLLIxskak(PackageAutobootActivity.this.mPackageManager, packageName, 1).versionCode)).toString();
                    if (DD02zqNU.DLLIxskak(PackageAutobootActivity.this.mPackageManager, packageName, 1).versionName == null) {
                    }
                    ApplicationInfo applicationInfo = PackageAutobootActivity.this.mPackageManager.getApplicationInfo(packageName, 1);
                    CloudApkInfo apkData = null;
                    List<CloudApkInfo> list = CloudApkInfoList.getApkInfoList(PackageAutobootActivity.this);
                    if (list != null) {
                        int j = 0;
                        while (true) {
                            if (j >= list.size()) {
                                break;
                            } else if (packageName.equals(list.get(j).getPkgName())) {
                                apkData = list.get(j);
                                break;
                            } else {
                                j++;
                            }
                        }
                    }
                    pkg.haveServerData = true;
                    pkg.setSscurity("40");
                    pkg.setScore("0");
                    if (apkData != null) {
                        String security = apkData.getSecurity();
                        pkg.setScore(apkData.getScore());
                        pkg.setSscurityDesc(apkData.getSecurityDesc());
                        pkg.haveServerData = true;
                    }
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                    v.add(pkg.getPackageName());
                }
            }
            if (v.size() > 0) {
                Iterator<PackageElement> iterator = PackageAutobootActivity.this.mPkgAutoBoot.iterator();
                while (iterator.hasNext()) {
                    if (v.contains(iterator.next().getPackageName())) {
                        iterator.remove();
                    }
                }
            }
            PackageAutobootActivity.this.runOnUiThread(new Runnable() {
                public void run() {
                    PackageAutobootActivity.this.mAdapter.notifyDataSetChanged();
                }
            });
        }
    };

    public static Intent getLaunchIntent(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, PackageAutobootActivity.class);
        return intent;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.package_installed);
        this.mPackageManager = getPackageManager();
        initRes();
        initListView();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        new Thread(this.mUpdateApkInfo).start();
    }

    private void initRes() {
        this.mBtnUninstall = (Button) findViewById(R.id.btn_function);
        this.mBtnUninstall.setOnClickListener(this);
        this.mBtnUninstall.setClickable(false);
        this.mBtnUninstall.setTextColor(-7829368);
        this.mListView = getListView();
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        if (this.mAdapter != null) {
            PackageElement app = this.mAdapter.getPackageElement(position);
            String packageName = app.getResolveInfo().activityInfo.packageName;
            Intent intent = new Intent();
            intent.setClass(this, ApkInfoActivity.class);
            intent.putExtra("package_name", packageName);
            intent.putExtra("apk_name", app.getResolveInfo().loadLabel(this.mPackageManager));
            startActivity(intent);
        }
    }

    private void initListView() {
        this.mAdapter = new PackageInfoAdapter(this, getAutobootPkg(), this.mBtnUninstall);
        setListAdapter(this.mAdapter);
    }

    private ArrayList<PackageElement> getAutobootPkg() {
        PackageManager pm = getPackageManager();
        Hashtable<String, ResolveInfo> mPkgAll = PackageManagerUtils.getPkgAll(this);
        this.mPkgAutoBoot = new ArrayList<>();
        for (ResolveInfo pkg : mPkgAll.values()) {
            if (!SystemUtils.isSystemPackage(pkg.activityInfo.applicationInfo) && pm.checkPermission("android.permission.RECEIVE_BOOT_COMPLETED", pkg.activityInfo.applicationInfo.packageName) == 0) {
                this.mPkgAutoBoot.add(new PackageElement(pkg));
            }
        }
        return this.mPkgAutoBoot;
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_function /*2131558618*/:
                onUninstallSelected();
                return;
            default:
                return;
        }
    }

    private void onUninstallSelected() {
        this.appsNeedUninstall = this.mAdapter.getSelected();
        if (this.appsNeedUninstall == null) {
            return;
        }
        if (this.appsNeedUninstall.size() > 0) {
            startUninstallApps();
        } else {
            Toast.makeText(this, (int) R.string.select_app_uninstall, 0).show();
        }
    }

    private void startUninstallApps() {
        Iterator<PackageElement> it = this.appsNeedUninstall.iterator();
        while (it.hasNext()) {
            startActivity(new Intent("android.intent.action.DELETE", Uri.fromParts("package", it.next().getResolveInfo().activityInfo.packageName, null)));
        }
    }
}
