package com.netqin.antivirus.packagemanager;

import android.content.pm.ResolveInfo;
import java.util.List;

public class PackageGroup {
    List<ResolveInfo> mChildren = null;
    long mGroupId;
    String mGroupName;

    public PackageGroup(String name, List<ResolveInfo> groupMembers) {
        this.mGroupName = name;
        this.mChildren = groupMembers;
    }

    public int getChildrenCount() {
        if (this.mChildren == null) {
            return 0;
        }
        return this.mChildren.size();
    }

    public void addChild(ResolveInfo child) {
        this.mChildren.add(child);
    }

    public Object getChild(int position) {
        return this.mChildren.get(position);
    }

    public long getGroupId() {
        return this.mGroupId;
    }

    public Object remove(int position) {
        return this.mChildren.remove(position);
    }

    public String toString() {
        return String.format("%s (%d)", this.mGroupName, Integer.valueOf(this.mChildren.size()));
    }
}
