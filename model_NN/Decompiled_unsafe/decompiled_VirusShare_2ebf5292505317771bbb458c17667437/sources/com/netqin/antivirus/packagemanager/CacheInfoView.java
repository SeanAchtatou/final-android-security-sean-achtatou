package com.netqin.antivirus.packagemanager;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;
import java.io.Serializable;

public class CacheInfoView extends TextView implements Serializable {
    private static final long serialVersionUID = -7426522375140786579L;

    public CacheInfoView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CacheInfoView(Context context) {
        super(context);
    }

    public CacheInfoView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
}
