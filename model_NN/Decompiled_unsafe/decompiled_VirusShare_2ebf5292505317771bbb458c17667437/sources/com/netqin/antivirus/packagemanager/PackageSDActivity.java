package com.netqin.antivirus.packagemanager;

import SHcMjZX.DD02zqNU;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import com.netqin.antivirus.util.MimeUtils;
import com.netqin.antivirus.util.SystemUtils;
import com.nqmobile.antivirus_ampro20.R;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

public class PackageSDActivity extends ListActivity {
    /* access modifiers changed from: private */
    public PackageApkAdapter mAdapter;
    private ArrayList<ApkElement> mApkList = new ArrayList<>();
    private Button mBtnDelete;
    private Button mBtnSelectAll;
    CheckingSDTask mCheckUpTask = new CheckingSDTask();
    private ListView mListView;
    private boolean mSelectAllStatus = false;

    public static Intent getLaunchIntent(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, PackageSDActivity.class);
        return intent;
    }

    public ProgressDialog createCheckingUpDialog() {
        ProgressDialog dlg = new ProgressDialog(this);
        dlg.setCancelable(false);
        dlg.setTitle((int) R.string.package_scaning);
        dlg.setMessage("");
        return dlg;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.package_sd);
        this.mBtnDelete = (Button) findViewById(R.id.btn_delete_function);
        this.mBtnDelete.setClickable(false);
        this.mBtnDelete.setTextColor(-7829368);
        this.mBtnDelete.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (PackageSDActivity.this.mAdapter.getSelected() == null) {
                    Toast.makeText(PackageSDActivity.this, PackageSDActivity.this.getString(R.string.no_any_apk), 0).show();
                } else if (PackageSDActivity.this.mAdapter.getSelected().size() == 0) {
                    Toast.makeText(PackageSDActivity.this, PackageSDActivity.this.getString(R.string.choose_apk_need_deleted), 0).show();
                } else {
                    PackageSDActivity.this.createDeleteDialog(PackageSDActivity.this.mAdapter.getSelected()).show();
                }
            }
        });
        this.mBtnSelectAll = (Button) findViewById(R.id.btn_select_all);
        this.mBtnSelectAll.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PackageSDActivity.this.toggleSelectAll();
            }
        });
        this.mListView = getListView();
        this.mAdapter = new PackageApkAdapter(this, this.mApkList, this.mBtnDelete);
        this.mListView.setAdapter((ListAdapter) this.mAdapter);
        this.mCheckUpTask.execute(new Void[0]);
    }

    /* access modifiers changed from: private */
    public AlertDialog createDeleteDialog(final ArrayList<ApkElement> list) {
        return new AlertDialog.Builder(this).setTitle(getString(R.string.label_delete)).setMessage(getString(R.string.package_scaning_delete)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                Iterator it = list.iterator();
                while (it.hasNext()) {
                    ApkElement app = (ApkElement) it.next();
                    if (new File(app.getPath()).delete()) {
                        PackageSDActivity.this.mAdapter.remove(app, true);
                    }
                }
                Toast.makeText(PackageSDActivity.this, PackageSDActivity.this.getString(R.string.package_scaning_deleted), 0).show();
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
            }
        }).create();
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        intent.setDataAndType(Uri.fromFile(new File(((ApkElement) getListView().getItemAtPosition(position)).getPath())), MimeUtils.MIME_APPLICATION_APK);
        try {
            startActivity(intent);
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: private */
    public void toggleSelectAll() {
        this.mSelectAllStatus = !this.mSelectAllStatus;
        ListView listView = this.mListView;
        int itemCount = listView.getCount();
        for (int i = 0; i < itemCount; i++) {
            ((ApkElement) listView.getItemAtPosition(i)).isChecked = this.mSelectAllStatus;
        }
        int childCount = listView.getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            ((CheckBox) listView.getChildAt(i2).findViewById(R.id.package_apk_checkbox)).setChecked(this.mSelectAllStatus);
        }
        this.mAdapter.changeRemoveStatus();
    }

    class CheckingSDTask extends AsyncTask<Void, ApkElement, Void> {
        ApkElement mApkElement;
        ProgressDialog mFakeProgressDialog = null;
        ArrayList<ApkElement> mSubTasks = new ArrayList<>();
        String sdPath = Environment.getExternalStorageDirectory().getAbsolutePath();

        CheckingSDTask() {
            this.mApkElement = new ApkElement();
        }

        public void extractDir(String path) {
            try {
                File[] fs = new File(path).listFiles();
                if (fs != null) {
                    for (File file : fs) {
                        if (file.isFile() && file.getName().endsWith(".apk")) {
                            publishProgress(new ApkElement(file.getName(), file.getAbsolutePath(), SystemUtils.prettyByte(DD02zqNU.Zob2JfG1Yi11hrq(file))));
                        } else if (file.isDirectory()) {
                            extractDir(file.getAbsolutePath());
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            this.mFakeProgressDialog = PackageSDActivity.this.createCheckingUpDialog();
            this.mFakeProgressDialog.show();
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... params) {
            extractDir(this.sdPath);
            return null;
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(ApkElement... values) {
            ApkElement status = values[0];
            if (status != null) {
                this.mFakeProgressDialog.setMessage(String.format("%s   %s", String.valueOf(PackageSDActivity.this.getString(R.string.package_scaning_item)) + " ", status.getApkName()));
                PackageSDActivity.this.mAdapter.add(status);
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void result) {
            this.mFakeProgressDialog.dismiss();
        }
    }

    public class ApkElement {
        private String apkName;
        private String apkSize;
        public boolean isChecked = false;
        private String path;

        public ApkElement() {
        }

        public ApkElement(String name, String path2, String size) {
            this.apkName = name;
            this.path = path2;
            this.apkSize = size;
        }

        public String getApkName() {
            return this.apkName;
        }

        public void setApkName(String name) {
            this.apkName = name;
        }

        public String getPath() {
            return this.path;
        }

        public void setPath(String path2) {
            this.path = path2;
        }

        public String getSize() {
            return this.apkSize;
        }

        public void setApkSize(String size) {
            this.apkSize = size;
        }
    }
}
