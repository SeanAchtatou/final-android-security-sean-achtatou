package com.netqin.antivirus.packagemanager;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;
import com.netqin.antivirus.cloud.apkinfo.domain.Review;
import com.nqmobile.antivirus_ampro20.R;
import java.util.List;

public class CommentListAdapter extends BaseAdapter {
    private boolean isBig;
    private Context mContext;
    private List<Review> mData;
    private LayoutInflater mInflater;

    public CommentListAdapter(Context context, List<Review> data, boolean b) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.mContext = context;
        this.isBig = b;
    }

    public synchronized List<Review> getAll() {
        return this.mData;
    }

    public synchronized int getCount() {
        return this.mData.size();
    }

    public synchronized Object getItem(int position) {
        return this.mData.get(position);
    }

    public synchronized long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            if (this.isBig) {
                convertView = this.mInflater.inflate((int) R.layout.comment_list_item_big, (ViewGroup) null);
            } else {
                convertView = this.mInflater.inflate((int) R.layout.comment_list_item, (ViewGroup) null);
            }
            holder = new ViewHolder();
            holder.author = (TextView) convertView.findViewById(R.id.author);
            holder.time = (TextView) convertView.findViewById(R.id.time);
            holder.rating = (RatingBar) convertView.findViewById(R.id.rating);
            holder.commentBody = (TextView) convertView.findViewById(R.id.comment_body);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Review cv = getReview(position);
        holder.author.setText(cv.getUid());
        holder.time.setText(cv.getDate());
        holder.rating.setRating(Float.parseFloat(cv.getScore()));
        holder.commentBody.setText(cv.getContent());
        return convertView;
    }

    public synchronized Review getReview(int pos) {
        return this.mData.get(pos);
    }

    public synchronized void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    static class ViewHolder {
        TextView author;
        TextView commentBody;
        RatingBar rating;
        TextView time;

        ViewHolder() {
        }
    }

    public synchronized void remove(int i) {
        this.mData.remove(i);
    }

    public void remove(Review app) {
        remove(app, false);
    }

    public synchronized void remove(Review app, boolean notify) {
        this.mData.remove(app);
        if (notify) {
            notifyDataSetChanged();
        }
    }

    public synchronized void add(List<Review> add) {
        this.mData.addAll(add);
        notifyDataSetChanged();
    }

    public synchronized void add(Review add) {
        this.mData.add(add);
        notifyDataSetChanged();
    }
}
