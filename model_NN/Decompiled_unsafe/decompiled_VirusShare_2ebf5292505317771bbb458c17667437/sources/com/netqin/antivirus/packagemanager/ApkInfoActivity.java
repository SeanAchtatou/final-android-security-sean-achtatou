package com.netqin.antivirus.packagemanager;

import SHcMjZX.DD02zqNU;
import SHcMjZX.njzbYCySyhg;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.IPackageStatsObserver;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageStats;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AppSecurityPermissions;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import com.netqin.antivirus.cloud.apkinfo.db.CloudApiInfoDb;
import com.netqin.antivirus.cloud.apkinfo.domain.Rate;
import com.netqin.antivirus.cloud.apkinfo.domain.Review;
import com.netqin.antivirus.cloud.model.CloudApkInfo;
import com.netqin.antivirus.cloud.model.CloudApkInfoList;
import com.netqin.antivirus.cloud.model.CloudHandler;
import com.netqin.antivirus.cloud.view.CloudListener;
import com.netqin.antivirus.privatesoft.VisitPrivacy;
import com.netqin.antivirus.ui.ReportApkActivity;
import com.nqmobile.antivirus_ampro20.R;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ApkInfoActivity extends Activity implements CloudListener {
    private static final int APKMORE = 5;
    private static final String ATTR_PACKAGE_STATS = "PackageStats";
    private static final String ATTR_UPDATE_VIEW = "UpdateView";
    private static final int DIALOG_COMMENT = 1;
    private static final int DIALOG_COMMITTING = 3;
    private static final int DIALOG_RATING = 2;
    private static final int GET_PKG_SIZE = 1;
    public static final String IS_FIRST_RATING = "1";
    public static final int MSG_1 = 1;
    public static final int MSG_2 = 2;
    public static final int MSG_3 = 3;
    public static final int MSG_4 = 4;
    public static final String NOT_FIRST_RATING = "0";
    public static final String TOAST_COMMENT = "1";
    public static final String TOAST_RATING = "0";
    public static boolean isCloud;
    /* access modifiers changed from: private */
    public static boolean showComment;
    /* access modifiers changed from: private */
    public int OPERATE = -1;
    /* access modifiers changed from: private */
    public CloudApiInfoDb apiInfoDb;
    private String apkAdvise;
    private String apkCommentNum;
    /* access modifiers changed from: private */
    public CloudApkInfo apkData;
    private String apkDescription;
    private Drawable apkIcon;
    private String apkName;
    private String apkRateNum;
    private String apkScore;
    private String apkSecurity;
    private String apkThreatDescription;
    private String apkVersionCode;
    private String apkVersionName;
    private ApplicationInfo applicationInfo;
    private String first;
    /* access modifiers changed from: private */
    public boolean isCloudRequest;
    private Boolean isFirstRating = true;
    /* access modifiers changed from: private */
    public int isMore = 0;
    /* access modifiers changed from: private */
    public int isThreatMore = 0;
    private TextView mApkAdvise;
    /* access modifiers changed from: private */
    public TextView mApkDescription;
    private ImageView mApkIcon;
    private TextView mApkName;
    private TextView mApkRateNum;
    private RatingBar mApkRatingBar;
    private TextView mApkSecurity;
    private CacheInfoView mApkSize;
    private LinearLayout mApkThreatContainer;
    /* access modifiers changed from: private */
    public TextView mApkThreatDescription;
    private TextView mApkVersion;
    /* access modifiers changed from: private */
    public Button mBtnApkComment;
    private Button mBtnApkRate;
    private Button mBtnApkReport;
    private Button mBtnApkUninstall;
    private TextView mBtnMoreComment;
    /* access modifiers changed from: private */
    public List<Review> mCommentList = new ArrayList();
    /* access modifiers changed from: private */
    public String mDialogComment;
    /* access modifiers changed from: private */
    public String mDialogScore;
    /* access modifiers changed from: private */
    public Handler mH = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    ApkInfoActivity.this.refreshSizeInfo(msg);
                    return;
                default:
                    return;
            }
        }
    };
    Handler mHandler;
    private ListView mListView;
    PackageManager mPackageManager;
    /* access modifiers changed from: private */
    public String mPackageName;
    /* access modifiers changed from: private */
    public String mServerId;
    /* access modifiers changed from: private */
    public TextView mTxtApkMore;
    /* access modifiers changed from: private */
    public TextView mTxtThreatMore;
    private String myScore;
    /* access modifiers changed from: private */
    public String toastType;

    /* access modifiers changed from: private */
    public void commitRating() {
        Rate rate = new Rate();
        rate.setPkgName(this.mPackageName);
        rate.setId("0");
        rate.setServerId(this.mServerId);
        rate.setName(this.apkName);
        rate.setVersionCode(this.apkVersionCode);
        rate.setVersionName(this.apkVersionName);
        rate.setScore(this.mDialogScore);
        rate.setContent(this.mDialogComment);
        this.mDialogComment = null;
        if (this.isFirstRating.booleanValue()) {
            rate.setFirst("1");
        } else {
            rate.setFirst("0");
        }
        CloudHandler.rate = rate;
        CloudHandler.getInstance(this).resetData();
        CloudHandler.getInstance(this).start(this, 2, this.mHandler, true);
    }

    public static Intent getLaunchIntent(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, ApkInfoActivity.class);
        return intent;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(1);
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.apk_info);
        setRequestedOrientation(1);
        ((TextView) findViewById(R.id.activity_name)).setText((int) R.string.app_info);
        this.mPackageManager = getPackageManager();
        Bundle bundle = getIntent().getExtras();
        this.mPackageName = bundle.getString("package_name");
        this.apkName = bundle.getString("apk_name");
        bindView();
        try {
            PackageInfo info = DD02zqNU.DLLIxskak(getPackageManager(), this.mPackageName, 0);
            this.apkIcon = info.applicationInfo.loadIcon(getPackageManager());
            this.apkVersionName = info.versionName;
            this.apkVersionCode = new StringBuilder(String.valueOf(info.versionCode)).toString();
            this.mApkIcon.setImageDrawable(this.apkIcon);
            this.mApkName.setText(this.apkName);
            this.mApkVersion.setText(getResources().getString(R.string.version_pre, this.apkVersionName));
        } catch (PackageManager.NameNotFoundException e) {
        }
        LinearLayout permsView = (LinearLayout) findViewById(R.id.permissions_section);
        AppSecurityPermissions asp = new AppSecurityPermissions(this, this.mPackageName);
        if (asp.getPermissionCount() > 0) {
            permsView.setVisibility(0);
            ((LinearLayout) permsView.findViewById(R.id.security_settings_list)).addView(asp.getPermissionsView());
        } else {
            permsView.setVisibility(8);
        }
        initView();
    }

    private void bindView() {
        this.mApkIcon = (ImageView) findViewById(R.id.apk_icon);
        this.mApkName = (TextView) findViewById(R.id.apk_name);
        this.mApkVersion = (TextView) findViewById(R.id.apk_version);
        this.mBtnApkUninstall = (Button) findViewById(R.id.btn_apk_uninstall);
        this.mApkDescription = (TextView) findViewById(R.id.apk_discription);
        this.mApkAdvise = (TextView) findViewById(R.id.package_deal_suggest);
        this.mApkThreatDescription = (TextView) findViewById(R.id.threat_description);
        this.mApkSecurity = (TextView) findViewById(R.id.apk_security);
        this.mTxtApkMore = (TextView) findViewById(R.id.txt_apk_more);
        this.mTxtThreatMore = (TextView) findViewById(R.id.txt_threat_more);
        this.mBtnApkReport = (Button) findViewById(R.id.btn_apk_report);
        this.mBtnApkRate = (Button) findViewById(R.id.btn_apk_rate);
        this.mApkRatingBar = (RatingBar) findViewById(R.id.apk_ratingbar);
        this.mApkRateNum = (TextView) findViewById(R.id.apk_rate_num);
        this.mBtnApkComment = (Button) findViewById(R.id.btn_apk_comment);
        this.mBtnApkComment.setTextColor(-7829368);
        showComment = false;
        this.mBtnMoreComment = (TextView) findViewById(R.id.btn_more_comment);
        this.mApkSize = (CacheInfoView) findViewById(R.id.apk_size);
        this.mListView = (ListView) findViewById(R.id.apk_comment_list);
        this.mApkThreatContainer = (LinearLayout) findViewById(R.id.threat_container);
        this.mHandler = new Handler() {
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 0:
                        VisitPrivacy.dismissLoading();
                        Iterator it = ((List) msg.obj).iterator();
                        while (true) {
                            if (it.hasNext()) {
                                CloudApkInfo apkIn = (CloudApkInfo) it.next();
                                if (ApkInfoActivity.this.mPackageName.equalsIgnoreCase(apkIn.getPkgName())) {
                                    ApkInfoActivity.this.apkData = apkIn;
                                }
                            }
                        }
                        ApkInfoActivity.this.initView();
                        if (ApkInfoActivity.this.OPERATE == 2) {
                            ApkInfoActivity.this.showDialog(2);
                        }
                        if (ApkInfoActivity.this.OPERATE == 1) {
                            ApkInfoActivity.this.showDialog(1);
                        }
                        if (ApkInfoActivity.this.OPERATE == 5) {
                            ApkInfoActivity.this.showMoreComment();
                            break;
                        }
                        break;
                    case 2:
                        CloudApkInfo apkInfo = (CloudApkInfo) msg.obj;
                        ApkInfoActivity.this.apiInfoDb = new CloudApiInfoDb(ApkInfoActivity.this);
                        ApkInfoActivity.this.apiInfoDb.updateApkInfo(ApkInfoActivity.this.mPackageName, ApkInfoActivity.this.mServerId, apkInfo);
                        ApkInfoActivity.this.apiInfoDb.deleteReview(ApkInfoActivity.this.mPackageName, ApkInfoActivity.this.mServerId);
                        ApkInfoActivity.this.mCommentList.clear();
                        for (int i = apkInfo.getReviews().size(); i > 0; i--) {
                            Review review = apkInfo.getReviews().get(i - 1);
                            review.setServerId(ApkInfoActivity.this.mServerId);
                            review.setPkgName(ApkInfoActivity.this.mPackageName);
                            ApkInfoActivity.this.apiInfoDb.insterReview(review);
                            ApkInfoActivity.this.mCommentList.add(review);
                        }
                        ApkInfoActivity.this.apiInfoDb.updateApkMyScore(ApkInfoActivity.this.mPackageName, ApkInfoActivity.this.mServerId, "0", ApkInfoActivity.this.mDialogScore);
                        ApkInfoActivity.this.apiInfoDb.close_virus_DB();
                        if (!(CloudApkInfoList.infoList == null || apkInfo == null)) {
                            boolean exist = false;
                            int i2 = 0;
                            while (true) {
                                if (i2 < CloudApkInfoList.infoList.size()) {
                                    if (ApkInfoActivity.this.mPackageName.equals(CloudApkInfoList.infoList.get(i2).getPkgName())) {
                                        CloudApkInfoList.infoList.get(i2).setCnt(apkInfo.getCnt());
                                        CloudApkInfoList.infoList.get(i2).setCntUniq(apkInfo.getCntUniq());
                                        CloudApkInfoList.infoList.get(i2).setReviews(apkInfo.getReviews());
                                        CloudApkInfoList.infoList.get(i2).setScore(apkInfo.getScore());
                                        exist = true;
                                    } else {
                                        i2++;
                                    }
                                }
                            }
                            if (!exist) {
                                apkInfo.setPkgName(ApkInfoActivity.this.mPackageName);
                                apkInfo.setServerId(ApkInfoActivity.this.mServerId);
                                CloudApkInfoList.infoList.add(apkInfo);
                            }
                        }
                        ApkInfoActivity.this.dismissDialog(3);
                        ApkInfoActivity.this.toastSucess(2);
                        ApkInfoActivity.this.refreshView(apkInfo);
                        ApkInfoActivity.this.mBtnApkComment.setTextColor(-1);
                        ApkInfoActivity.showComment = true;
                        ApkInfoActivity.this.mBtnApkComment.setEnabled(true);
                        break;
                    case 6:
                        ApkInfoActivity.this.dismissDialog(3);
                        ApkInfoActivity.this.toastWrong(6);
                        break;
                    case 10:
                        VisitPrivacy.dismissLoading();
                        if (ApkInfoActivity.this.OPERATE == 2 && ApkInfoActivity.this.isCloudRequest) {
                            ApkInfoActivity.this.dismissDialog(3);
                        }
                        if (ApkInfoActivity.this.OPERATE == 1 && ApkInfoActivity.this.isCloudRequest) {
                            ApkInfoActivity.this.dismissDialog(3);
                        }
                        ApkInfoActivity.this.isCloudRequest = false;
                        Toast.makeText(ApkInfoActivity.this, ApkInfoActivity.this.getString(R.string.server_connect_fail), 1).show();
                        break;
                }
                super.handleMessage(msg);
            }
        };
        setBtnOnclick();
    }

    private void setBtnOnclick() {
        this.mBtnApkUninstall.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ApkInfoActivity.this.startActivity(new Intent("android.intent.action.DELETE", Uri.fromParts("package", ApkInfoActivity.this.mPackageName, null)));
            }
        });
        this.mTxtThreatMore.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (ApkInfoActivity.this.isThreatMore == 0) {
                    ApkInfoActivity.this.mTxtThreatMore.setText(ApkInfoActivity.this.getResources().getString(R.string.less));
                    ApkInfoActivity.this.mApkThreatDescription.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                    ApkInfoActivity.this.isThreatMore = 1;
                    return;
                }
                ApkInfoActivity.this.mTxtThreatMore.setText(ApkInfoActivity.this.getResources().getString(R.string.more));
                ApkInfoActivity.this.mApkThreatDescription.setLayoutParams(new LinearLayout.LayoutParams(-1, 60));
                ApkInfoActivity.this.mApkThreatDescription.setFadingEdgeLength(20);
                ApkInfoActivity.this.mApkThreatDescription.setVerticalFadingEdgeEnabled(true);
                ApkInfoActivity.this.isThreatMore = 0;
            }
        });
        this.mTxtApkMore.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (ApkInfoActivity.this.isMore == 0) {
                    ApkInfoActivity.this.mTxtApkMore.setText(ApkInfoActivity.this.getResources().getString(R.string.less));
                    ApkInfoActivity.this.mApkDescription.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                    ApkInfoActivity.this.isMore = 1;
                    return;
                }
                ApkInfoActivity.this.mTxtApkMore.setText(ApkInfoActivity.this.getResources().getString(R.string.more));
                ApkInfoActivity.this.mApkDescription.setLayoutParams(new LinearLayout.LayoutParams(-1, 60));
                ApkInfoActivity.this.mApkDescription.setFadingEdgeLength(20);
                ApkInfoActivity.this.mApkDescription.setVerticalFadingEdgeEnabled(true);
                ApkInfoActivity.this.isMore = 0;
            }
        });
        this.mBtnApkRate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (ApkInfoActivity.this.mServerId != null && ApkInfoActivity.this.mServerId.length() > 0) {
                    ApkInfoActivity.this.OPERATE = 2;
                    ApkInfoActivity.this.showDialog(2);
                } else if (ApkInfoActivity.checkNetInfo(ApkInfoActivity.this)) {
                    ApkInfoActivity.this.OPERATE = 2;
                    ApkInfoActivity.this.isCloudRequest = true;
                    ApkInfoActivity.isCloud = true;
                    CloudHandler.getInstance(ApkInfoActivity.this).start(ApkInfoActivity.this, 0, ApkInfoActivity.this.mHandler, true);
                    VisitPrivacy.showLoading(ApkInfoActivity.this, ApkInfoActivity.this.getString(R.string.netqin_loding), ApkInfoActivity.this.getString(R.string.netqin_loding_desc));
                } else {
                    Toast.makeText(ApkInfoActivity.this, ApkInfoActivity.this.getString(R.string.SEND_RECEIVE_ERROR), 1).show();
                }
            }
        });
        this.mBtnApkComment.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!ApkInfoActivity.showComment) {
                    Toast.makeText(ApkInfoActivity.this, ApkInfoActivity.this.getString(R.string.need_rating_first), 0).show();
                } else if (ApkInfoActivity.this.mServerId != null && ApkInfoActivity.this.mServerId.length() > 0) {
                    ApkInfoActivity.this.OPERATE = 1;
                    ApkInfoActivity.this.showDialog(1);
                } else if (ApkInfoActivity.checkNetInfo(ApkInfoActivity.this)) {
                    ApkInfoActivity.this.OPERATE = 1;
                    ApkInfoActivity.this.isCloudRequest = true;
                    ApkInfoActivity.isCloud = true;
                    CloudHandler.getInstance(ApkInfoActivity.this).start(ApkInfoActivity.this, 0, ApkInfoActivity.this.mHandler, true);
                    VisitPrivacy.showLoading(ApkInfoActivity.this, ApkInfoActivity.this.getString(R.string.netqin_loding), ApkInfoActivity.this.getString(R.string.netqin_loding_desc));
                } else {
                    Toast.makeText(ApkInfoActivity.this, ApkInfoActivity.this.getString(R.string.SEND_RECEIVE_ERROR), 1).show();
                }
            }
        });
        this.mBtnApkReport.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ApkInfoActivity.this.showApkReport();
            }
        });
        this.mBtnMoreComment.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ApkInfoActivity.this.OPERATE = 5;
                if (ApkInfoActivity.this.mServerId != null && ApkInfoActivity.this.mServerId.length() > 0) {
                    ApkInfoActivity.this.showMoreComment();
                } else if (ApkInfoActivity.checkNetInfo(ApkInfoActivity.this)) {
                    ApkInfoActivity.isCloud = true;
                    CloudHandler.getInstance(ApkInfoActivity.this).start(ApkInfoActivity.this, 0, ApkInfoActivity.this.mHandler, true);
                    VisitPrivacy.showLoading(ApkInfoActivity.this, ApkInfoActivity.this.getString(R.string.netqin_loding), ApkInfoActivity.this.getString(R.string.netqin_loding_desc));
                } else {
                    Toast.makeText(ApkInfoActivity.this, ApkInfoActivity.this.getString(R.string.SEND_RECEIVE_ERROR), 1).show();
                }
            }
        });
    }

    public static boolean checkNetInfo(Context context) {
        NetworkInfo info = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (info == null || !info.isAvailable()) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void initView() {
        try {
            if (this.mPackageName.equals(getPackageName())) {
                this.mBtnApkUninstall.setVisibility(4);
                this.mBtnApkReport.setVisibility(4);
            }
            this.apiInfoDb = new CloudApiInfoDb(this);
            this.applicationInfo = this.mPackageManager.getApplicationInfo(this.mPackageName, 1);
            this.apkSecurity = getString(R.string.text_security_level_unknown);
            this.mApkSecurity.setText(getString(R.string.apk_security, new Object[]{this.apkSecurity}));
            this.mApkRateNum.setText(" " + getString(R.string.ratenum_small, new Object[]{0}));
            byte[] rsaByte = CloudHandler.readFileByte(String.valueOf(getFilesDir().getAbsolutePath()) + "/" + this.mPackageName);
            if (rsaByte == null || rsaByte.length <= 0) {
                this.apkData = this.apiInfoDb.getCloudApkData(this.mPackageName, this.apkVersionCode, this.apkVersionName, CloudHandler.getSignFile(this.applicationInfo.publicSourceDir, ".RSA", cert_rsa_path()));
                if (this.apkData != null) {
                    CloudHandler.craeteFile(String.valueOf(getFilesDir().getAbsolutePath()) + "/" + this.mPackageName, this.apkData.getCertRSA());
                }
            } else {
                this.apkData = this.apiInfoDb.getCloudApkData(this.mPackageName, this.apkVersionCode, this.apkVersionName, CloudHandler.readFileByte(String.valueOf(getFilesDir().getAbsolutePath()) + "/" + this.mPackageName));
            }
            if (this.apkData != null) {
                this.myScore = this.apkData.getMyScore();
                this.first = this.apkData.getFirst();
            }
            List<CloudApkInfo> list = CloudApkInfoList.getApkInfoList(this);
            if (list != null) {
                int i = 0;
                while (true) {
                    if (i >= list.size()) {
                        break;
                    } else if (this.mPackageName.equals(list.get(i).getPkgName())) {
                        this.apkData = list.get(i);
                        this.apkData.setMyScore(this.myScore);
                        this.apkData.setFirst(this.first);
                        break;
                    } else {
                        i++;
                    }
                }
            }
            if (this.apkData != null) {
                this.apkDescription = this.apkData.getNote();
                this.apkSecurity = this.apkData.getSecurityDesc();
                if (!"50".equals(this.apkData.getSecurity())) {
                    this.apkAdvise = this.apkData.getAdvice();
                    if (this.apkAdvise == null) {
                        this.apkAdvise = getString(R.string.label_uninstall);
                    }
                    this.apkThreatDescription = this.apkData.getReason();
                    if (this.apkThreatDescription != null) {
                        this.mApkThreatDescription.setText(this.apkThreatDescription);
                        this.mApkAdvise.setText(getString(R.string.package_deal_suggest, new Object[]{this.apkAdvise}));
                        this.mApkThreatContainer.setVisibility(0);
                    }
                }
                this.apkScore = this.apkData.getScore();
                this.apkRateNum = this.apkData.getCntUniq();
                this.apkCommentNum = this.apkData.getCnt();
                this.mServerId = this.apkData.getServerId();
                if ("0".equals(this.apkData.getFirst())) {
                    this.isFirstRating = false;
                    this.mBtnApkComment.setTextColor(-1);
                    showComment = true;
                    this.mDialogScore = this.apkData.getMyScore();
                } else {
                    this.mBtnApkComment.setTextColor(-7829368);
                    showComment = false;
                    this.isFirstRating = true;
                }
                int i2 = Integer.parseInt(this.apkRateNum);
                if (i2 < 500 && i2 >= 0) {
                    this.mApkRateNum.setText(" " + getString(R.string.ratenum_small, new Object[]{Integer.valueOf(i2)}));
                } else if (i2 < 5000) {
                    this.mApkRateNum.setText(" 2131428060");
                } else {
                    this.mApkRateNum.setText(" 2131428061");
                }
                this.mApkDescription.setText(this.apkDescription);
                if (TextUtils.isEmpty(this.apkSecurity)) {
                    this.apkSecurity = getString(R.string.text_security_level_unknown);
                }
                this.mApkSecurity.setText(getString(R.string.apk_security, new Object[]{this.apkSecurity}));
                if ("10".equals(this.apkData.getSecurity())) {
                    this.mApkSecurity.setTextColor(-65536);
                }
                this.mApkRatingBar.setRating(Float.parseFloat(this.apkScore));
                this.mCommentList = this.apkData.getReviews();
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } finally {
            this.apiInfoDb.close_virus_DB();
        }
        initList();
    }

    public String cert_rsa_path() {
        return getFilesDir() + CloudHandler.Cert_rsa_path;
    }

    private void initList() {
        if (this.mCommentList != null) {
            this.mListView.setLayoutParams(new LinearLayout.LayoutParams(-2, this.mCommentList.size() * 92));
            this.mListView.setAdapter((ListAdapter) new CommentListAdapter(this, this.mCommentList, false));
            return;
        }
        this.mListView.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void refreshView(CloudApkInfo apkInfo) {
        this.apkScore = apkInfo.getScore();
        this.apkRateNum = apkInfo.getCntUniq();
        this.apkCommentNum = apkInfo.getCnt();
        int i = Integer.parseInt(this.apkRateNum);
        if (i < 500 && i >= 0) {
            this.mApkRateNum.setText(" " + getString(R.string.ratenum_small, new Object[]{Integer.valueOf(i)}));
        } else if (i < 5000) {
            this.mApkRateNum.setText(" 2131428060");
        } else {
            this.mApkRateNum.setText(" 2131428061");
        }
        this.mApkRatingBar.setRating(Float.parseFloat(this.apkScore));
        initList();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        try {
            njzbYCySyhg.Iu0rKmcDCcc(this.mPackageManager.getClass().getMethod("getPackageSizeInfo", String.class, IPackageStatsObserver.class), this.mPackageManager, new Object[]{this.mPackageName, new PkgSizeObserver(this.mApkSize)});
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.mApkIcon.requestFocus();
        this.mApkIcon.setFocusableInTouchMode(true);
        try {
            this.applicationInfo = this.mPackageManager.getApplicationInfo(this.mPackageName, 1);
        } catch (PackageManager.NameNotFoundException e2) {
            finish();
        }
    }

    class PkgSizeObserver extends IPackageStatsObserver.Stub {
        CacheInfoView mCacheView;

        public PkgSizeObserver(CacheInfoView cacheView) {
            this.mCacheView = cacheView;
        }

        public void onGetStatsCompleted(PackageStats pStats, boolean succeeded) {
            Message msg = ApkInfoActivity.this.mH.obtainMessage(1);
            Bundle data = new Bundle();
            data.putParcelable(ApkInfoActivity.ATTR_PACKAGE_STATS, pStats);
            data.putSerializable(ApkInfoActivity.ATTR_UPDATE_VIEW, this.mCacheView);
            msg.setData(data);
            ApkInfoActivity.this.mH.sendMessage(msg);
        }
    }

    /* access modifiers changed from: protected */
    public void refreshSizeInfo(Message msg) {
        PackageStats newPs = (PackageStats) msg.getData().getParcelable(ATTR_PACKAGE_STATS);
        CacheInfoView cacheView = (CacheInfoView) msg.getData().getSerializable(ATTR_UPDATE_VIEW);
        long totalSize = newPs.cacheSize + newPs.codeSize + newPs.dataSize;
        if (cacheView != null) {
            cacheView.setText(getString(R.string.size_info, new Object[]{Formatter.formatFileSize(this, totalSize)}));
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        return true;
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                View view2 = getLayoutInflater().inflate((int) R.layout.set_comment, (ViewGroup) null);
                final EditText textViewComment = (EditText) view2.findViewById(R.id.text_comment);
                textViewComment.setText((CharSequence) null);
                return new AlertDialog.Builder(this).setTitle((int) R.string.rating_comment).setView(view2).setPositiveButton((int) R.string.alert_dialog_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        ApkInfoActivity.this.mDialogComment = textViewComment.getText().toString();
                        if (ApkInfoActivity.this.mDialogComment.equals("")) {
                            Toast.makeText(ApkInfoActivity.this, (int) R.string.comment_null, 0).show();
                            return;
                        }
                        ApkInfoActivity.this.toastType = "1";
                        ApkInfoActivity.this.showDialog(3);
                        ApkInfoActivity.this.commitRating();
                    }
                }).setNegativeButton((int) R.string.alert_dialog_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                }).create();
            case 2:
                View view = getLayoutInflater().inflate((int) R.layout.set_rating, (ViewGroup) null);
                final RatingBar rb = (RatingBar) view.findViewById(R.id.rating_setter);
                final TextView ratingDescription = (TextView) view.findViewById(R.id.rating_description);
                if (!this.isFirstRating.booleanValue()) {
                    float i = Float.parseFloat(this.mDialogScore);
                    rb.setRating(i);
                    if (((double) i) <= 1.0d) {
                        ratingDescription.setText((int) R.string.rating_worst);
                    } else if (((double) i) <= 2.0d) {
                        ratingDescription.setText((int) R.string.rating_worse);
                    } else if (((double) i) <= 3.0d) {
                        ratingDescription.setText((int) R.string.rating_normal);
                    } else if (((double) i) <= 4.0d) {
                        ratingDescription.setText((int) R.string.rating_good);
                    } else if (((double) i) <= 5.0d) {
                        ratingDescription.setText((int) R.string.rating_best);
                    }
                }
                rb.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromTouch) {
                        float i = rb.getRating();
                        if (((double) i) <= 1.0d) {
                            ratingDescription.setText((int) R.string.rating_worst);
                        } else if (((double) i) <= 2.0d) {
                            ratingDescription.setText((int) R.string.rating_worse);
                        } else if (((double) i) <= 3.0d) {
                            ratingDescription.setText((int) R.string.rating_normal);
                        } else if (((double) i) <= 4.0d) {
                            ratingDescription.setText((int) R.string.rating_good);
                        } else if (((double) i) <= 5.0d) {
                            ratingDescription.setText((int) R.string.rating_best);
                        }
                    }
                });
                return new AlertDialog.Builder(this).setTitle((int) R.string.rating_title).setView(view).setPositiveButton((int) R.string.alert_dialog_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        ApkInfoActivity.this.mDialogScore = String.valueOf((int) rb.getRating());
                        ApkInfoActivity.this.commitRating();
                        ApkInfoActivity.this.toastType = "0";
                        ApkInfoActivity.this.showDialog(3);
                    }
                }).setNegativeButton((int) R.string.alert_dialog_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                }).create();
            case 3:
                ProgressDialog progressDialog = new ProgressDialog(this);
                progressDialog.setTitle(getString(R.string.label_netqin_antivirus));
                progressDialog.setMessage(getString(R.string.cloud_score_rating));
                progressDialog.setProgressStyle(0);
                progressDialog.setCancelable(true);
                progressDialog.show();
                return progressDialog;
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int id, Dialog dialog) {
        switch (id) {
            case 1:
                ((EditText) dialog.findViewById(R.id.text_comment)).setText((CharSequence) null);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void showApkReport() {
        if (!checkNetInfo(this)) {
            Toast.makeText(this, getString(R.string.SEND_RECEIVE_ERROR), 0).show();
            return;
        }
        Intent intent = new Intent();
        intent.setClass(this, ReportApkActivity.class);
        intent.putExtra("package_name", this.mPackageName);
        intent.putExtra("apk_name", this.apkName);
        intent.putExtra("apk_version", this.apkVersionName);
        intent.putExtra("apk_serverId", this.mServerId);
        startActivity(intent);
    }

    /* access modifiers changed from: private */
    public void showMoreComment() {
        Intent intent = new Intent();
        intent.setClass(this, CommentListActivity.class);
        intent.putExtra("package_name", this.mPackageName);
        intent.putExtra("apk_name", this.apkName);
        intent.putExtra("apk_version", this.apkVersionName);
        intent.putExtra("apk_serverId", this.mServerId);
        intent.putExtra("apk_commentNum", this.apkCommentNum);
        startActivity(intent);
    }

    /* access modifiers changed from: private */
    public void toastWrong(int code) {
        switch (code) {
            case 6:
                if ("0".equals(this.toastType)) {
                    Toast.makeText(this, (int) R.string.rating_failed, 0).show();
                } else if ("1".equals(this.toastType)) {
                    Toast.makeText(this, (int) R.string.comment_failed, 0).show();
                }
                Toast.makeText(this, (int) R.string.rating_failed, 0).show();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void toastSucess(int code) {
        switch (code) {
            case 2:
                if ("0".equals(this.toastType)) {
                    Toast.makeText(this, (int) R.string.rating_success, 0).show();
                    return;
                } else if ("1".equals(this.toastType)) {
                    Toast.makeText(this, (int) R.string.comment_success, 0).show();
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }

    public void process(Bundle bundle) {
    }

    public void complete(int resultCode) {
    }

    public void error(int errorCode) {
    }
}
