package com.netqin.antivirus;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.Toast;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.data.PreferenceDataHelper;
import com.netqin.antivirus.networkmanager.NetApplication;
import com.nqmobile.antivirus_ampro20.R;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;

public class Disclaimer extends Activity {
    /* access modifiers changed from: private */
    public Preferences mPref;

    private void LoadHtmFile() {
        Integer[] arrayId = {Integer.valueOf((int) R.raw.license), Integer.valueOf((int) R.raw.license_cn)};
        String[] arrayName = {"license.htm", "license_cn.htm"};
        for (int i = 0; i < arrayId.length; i++) {
            try {
                FileOutputStream fos = openFileOutput(arrayName[i], 1);
                try {
                    InputStream is = getResources().openRawResource(arrayId[i].intValue());
                    byte[] b = new byte[is.available()];
                    is.read(b);
                    fos.write(b);
                    is.close();
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (FileNotFoundException e2) {
                e2.printStackTrace();
            }
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LoadHtmFile();
        this.mPref = new Preferences(this);
        showRight();
    }

    private void showRight() {
        setTitle((int) R.string.text_license);
        setContentView((int) R.layout.disclaimer);
        WebView webView = (WebView) findViewById(R.id.rights_webview);
        if (CommonMethod.isLocalSimpleChinese()) {
            webView.loadUrl(Uri.parse("file://" + getFilesDir().getAbsolutePath() + "/license_cn.htm").toString());
        } else {
            webView.loadUrl(Uri.parse("file://" + getFilesDir().getAbsolutePath() + "/license.htm").toString());
        }
        ((Button) findViewById(R.id.button_yes)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Disclaimer.this.mPref.setShowFirstPage(false);
                Calendar calendar = Calendar.getInstance();
                CommonMethod.setLastPeriodScanTime(Disclaimer.this, calendar);
                calendar.add(11, 15);
                CommonMethod.setNextPeriodScanTime(Disclaimer.this, calendar);
                Disclaimer.this.startActivity(new Intent(Disclaimer.this, HomeActivity.class));
                if (Disclaimer.this.getSharedPreferences("netqin", 0).getBoolean("IsFirstRun", true) && !CommonMethod.IsShortCutExist(Disclaimer.this, Disclaimer.this.getString(R.string.label_netqin_antivirus_short))) {
                    Disclaimer.this.addShortcut();
                    if (CommonMethod.isLocalSimpleChinese()) {
                        PreferenceDataHelper.setTrafficThreshold(Disclaimer.this, NetApplication.DEFAULT_THRESHOLD);
                    } else {
                        PreferenceDataHelper.setTrafficThreshold(Disclaimer.this, "100");
                    }
                }
                Disclaimer.this.finish();
            }
        });
        ((Button) findViewById(R.id.button_quit)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Disclaimer.this.finish();
                CommonMethod.exitAntiVirus(Disclaimer.this);
            }
        });
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void addShortcut() {
        Intent addShortcut = new Intent("com.android.launcher.action.INSTALL_SHORTCUT");
        addShortcut.putExtra("android.intent.extra.shortcut.NAME", getString(R.string.label_netqin_antivirus_short));
        Intent i = new Intent(this, AntiVirusSplash.class);
        i.setAction("android.intent.action.MAIN");
        i.addCategory("android.intent.category.LAUNCHER");
        i.addFlags(270532608);
        addShortcut.putExtra("duplicate", false);
        addShortcut.putExtra("android.intent.extra.shortcut.INTENT", i);
        addShortcut.putExtra("android.intent.extra.shortcut.ICON_RESOURCE", Intent.ShortcutIconResource.fromContext(this, R.drawable.icon));
        sendBroadcast(addShortcut);
        Toast.makeText(this, getString(R.string.create_shortcut), 1).show();
    }
}
