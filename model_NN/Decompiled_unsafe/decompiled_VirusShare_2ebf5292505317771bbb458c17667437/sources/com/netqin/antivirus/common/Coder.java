package com.netqin.antivirus.common;

import com.netqin.antivirus.NqUtil;
import java.security.MessageDigest;
import javax.crypto.KeyGenerator;

public abstract class Coder {
    public static final String KEY_MAC = "HmacMD5";
    public static final String KEY_MD5 = "MD5";
    public static final String KEY_SHA = "SHA";

    public static byte[] decryptBASE64(String key) throws Exception {
        return NqUtil.base64Decode(key);
    }

    public static String encryptBASE64(byte[] key) throws Exception {
        return NqUtil.base64Encode(key);
    }

    public static byte[] encryptMD5(byte[] data) throws Exception {
        MessageDigest md5 = MessageDigest.getInstance(KEY_MD5);
        md5.update(data);
        return md5.digest();
    }

    public static byte[] encryptSHA(byte[] data) throws Exception {
        MessageDigest sha = MessageDigest.getInstance(KEY_SHA);
        sha.update(data);
        return sha.digest();
    }

    public static String initMacKey() throws Exception {
        return encryptBASE64(KeyGenerator.getInstance(KEY_MAC).generateKey().getEncoded());
    }
}
