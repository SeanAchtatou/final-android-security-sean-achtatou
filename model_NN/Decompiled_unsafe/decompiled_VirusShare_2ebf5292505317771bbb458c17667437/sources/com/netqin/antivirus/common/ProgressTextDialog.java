package com.netqin.antivirus.common;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ViewFlipper;
import com.nqmobile.antivirus_ampro20.R;

public class ProgressTextDialog extends Dialog {
    private ViewFlipper mAnimationFlipperFrom;
    private ViewFlipper mAnimationFlipperMiddle;
    private ViewFlipper mAnimationFlipperTo;
    private int mAnimationImageFrom = -1;
    private int mAnimationImageTo = -1;
    private LinearLayout mAnimationLayout;
    private int mAnimationLayoutState = 8;
    private Handler mHandler = null;
    private ProgressBar mProgTextBar = null;
    private TextView mTVProgTextLeft = null;
    private TextView mTVProgTextRight = null;
    private TextView mTVTitle;
    private int mTitleId;
    private int mmTVProgTextLeftState = 4;

    public ProgressTextDialog(Context context, int titleId, Handler handler) {
        super(context);
        this.mTitleId = titleId;
        this.mHandler = handler;
    }

    public void setLeftVisibleState(int state) {
        this.mmTVProgTextLeftState = state;
    }

    public void setAnimationState(int state) {
        this.mAnimationLayoutState = state;
    }

    public void setAnimationImageSourceFrom(int imgFrom) {
        this.mAnimationImageFrom = imgFrom;
    }

    public void setAnimationImageSourceTo(int imgTo) {
        this.mAnimationImageTo = imgTo;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.progress_text);
        this.mTVTitle = (TextView) findViewById(R.id.progress_text_title);
        this.mTVTitle.setText(this.mTitleId);
        this.mProgTextBar = (ProgressBar) findViewById(R.id.progress_text_bar);
        this.mTVProgTextLeft = (TextView) findViewById(R.id.progress_text_left);
        this.mTVProgTextRight = (TextView) findViewById(R.id.progress_text_right);
        this.mTVProgTextLeft.setVisibility(this.mmTVProgTextLeftState);
        this.mAnimationLayout = (LinearLayout) findViewById(R.id.progress_text_flipper_layout);
        this.mAnimationFlipperMiddle = (ViewFlipper) findViewById(R.id.progress_text_flipper_vf_middle);
        this.mAnimationFlipperFrom = (ViewFlipper) findViewById(R.id.progress_text_flipper_vf_from);
        this.mAnimationFlipperTo = (ViewFlipper) findViewById(R.id.progress_text_flipper_vf_to);
        this.mAnimationLayout.setVisibility(this.mAnimationLayoutState);
        if (this.mAnimationLayout.getVisibility() == 0) {
            ImageView imgFrom = (ImageView) findViewById(R.id.progress_text_flipper_from);
            if (this.mAnimationImageFrom > 0) {
                imgFrom.setImageResource(this.mAnimationImageFrom);
                imgFrom.setVisibility(0);
                this.mAnimationFlipperFrom.setVisibility(8);
                this.mAnimationFlipperFrom.stopFlipping();
            } else {
                this.mAnimationFlipperFrom.setVisibility(0);
                this.mAnimationFlipperFrom.startFlipping();
                imgFrom.setVisibility(8);
            }
            this.mAnimationFlipperMiddle.startFlipping();
            ImageView imgTo = (ImageView) findViewById(R.id.progress_text_flipper_to);
            if (this.mAnimationImageTo > 0) {
                imgTo.setImageResource(this.mAnimationImageTo);
                imgTo.setVisibility(0);
                this.mAnimationFlipperTo.setVisibility(8);
                this.mAnimationFlipperTo.stopFlipping();
                return;
            }
            this.mAnimationFlipperTo.setVisibility(0);
            this.mAnimationFlipperTo.startFlipping();
            imgTo.setVisibility(8);
            return;
        }
        this.mAnimationFlipperMiddle.stopFlipping();
        this.mAnimationFlipperFrom.stopFlipping();
        this.mAnimationFlipperTo.stopFlipping();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (4 == keyCode && this.mHandler != null) {
            Message msg = new Message();
            msg.what = 12;
            msg.arg1 = 37;
            this.mHandler.sendMessage(msg);
        }
        return super.onKeyDown(keyCode, event);
    }

    public TextView getTitleTextView() {
        return this.mTVTitle;
    }

    public ProgressBar getProgressBar() {
        return this.mProgTextBar;
    }

    public TextView getLeftTextView() {
        return this.mTVProgTextLeft;
    }

    public TextView getRightTextView() {
        return this.mTVProgTextRight;
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        if (this.mHandler != null) {
            CommonMethod.sendUserMessage(this.mHandler, 11);
        }
        super.onStop();
    }
}
