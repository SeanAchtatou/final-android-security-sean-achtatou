package com.netqin.antivirus.common;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.view.KeyEvent;

public class ProgressDialogEx extends ProgressDialog {
    private Handler mHandler = null;
    private boolean mMaskKeyBack = false;

    public ProgressDialogEx(Context context, Handler handler) {
        super(context);
        this.mHandler = handler;
    }

    public ProgressDialogEx(Context context, int theme, Handler handler) {
        super(context, theme);
        this.mHandler = handler;
    }

    public void setMaskKeyBack(boolean isMask) {
        this.mMaskKeyBack = isMask;
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        if (this.mHandler != null) {
            CommonMethod.sendUserMessage(this.mHandler, 5);
        }
        super.onStop();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (this.mMaskKeyBack) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
