package com.netqin.antivirus.common;

import android.content.Context;
import android.content.SharedPreferences;
import com.netqin.antivirus.cloud.model.xml.XmlUtils;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

public class MaliciousWebsite {
    private Context mContext = null;
    private SharedPreferences sp;

    public MaliciousWebsite(Context context) {
        this.mContext = context;
        this.sp = this.mContext.getSharedPreferences("MaliciousWebsiteLog", 0);
    }

    public void clearEditor() {
        this.sp.edit().clear().commit();
    }

    public void setMaliciousWebsiteStartTime(String malicious_website_start_time) {
        this.sp.edit().putString("malicious_website_start_time", malicious_website_start_time).commit();
    }

    public String getMaliciousWebsiteStartTime() {
        return this.sp.getString("malicious_website_start_time", "-1");
    }

    public void setVisitUrl(String visit_url) {
        this.sp.edit().putString("visit_url", visit_url).commit();
    }

    public String getVisitUrl() {
        return this.sp.getString("visit_url", "-1");
    }

    public void setVisitUrlUsetime(String visit_url_usetime) {
        this.sp.edit().putString("visit_url_usetime", visit_url_usetime).commit();
    }

    public String getVisitUrlUsetime() {
        return this.sp.getString("visit_url_usetime", "-1");
    }

    public void setVisitNqServerUsetime(String visit_nqserver_usetime) {
        this.sp.edit().putString("visit_nqserver_usetime", visit_nqserver_usetime).commit();
    }

    public String getVisitNqServerUsetime() {
        return this.sp.getString("visit_nqserver_usetime", "-1");
    }

    public void setClientReceiveNqServerUsetime(String client_receive_nqserver_usetime) {
        this.sp.edit().putString("client_receive_nqserver_usetime", client_receive_nqserver_usetime).commit();
    }

    public String getClientReceiveNqServerUsetime() {
        return this.sp.getString("client_receive_nqserver_usetime", "-1");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
      ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException} */
    public void writeUrlLogToFile() {
        StringBuffer performance_datBuffer = new StringBuffer();
        performance_datBuffer.append('\"');
        performance_datBuffer.append(getMaliciousWebsiteStartTime());
        performance_datBuffer.append('\"');
        performance_datBuffer.append(",");
        performance_datBuffer.append('\"');
        String userTag = CommonMethod.getUID(this.mContext);
        if (userTag != null && userTag.length() < 0) {
            userTag = CommonMethod.getIMEI(this.mContext);
        }
        performance_datBuffer.append(userTag);
        performance_datBuffer.append('\"');
        performance_datBuffer.append(",");
        performance_datBuffer.append('\"');
        performance_datBuffer.append("0");
        performance_datBuffer.append('\"');
        performance_datBuffer.append(",");
        performance_datBuffer.append('\"');
        performance_datBuffer.append(getVisitUrl());
        performance_datBuffer.append('\"');
        performance_datBuffer.append(",");
        performance_datBuffer.append('\"');
        performance_datBuffer.append(CommonMethod.getLocalIpAddress());
        performance_datBuffer.append('\"');
        performance_datBuffer.append(",");
        performance_datBuffer.append('\"');
        performance_datBuffer.append(getVisitNqServerUsetime());
        performance_datBuffer.append('\"');
        performance_datBuffer.append(",");
        performance_datBuffer.append('\"');
        performance_datBuffer.append(getClientReceiveNqServerUsetime());
        performance_datBuffer.append('\"');
        performance_datBuffer.append(10);
        String performanceStr = performance_datBuffer.toString();
        File fileInst = new File(this.mContext.getFilesDir() + XmlUtils.URL_PERFORMANCE_DAT);
        try {
            if (!fileInst.exists()) {
                fileInst.createNewFile();
            }
            FileWriter writer = new FileWriter(this.mContext.getFilesDir() + XmlUtils.URL_PERFORMANCE_DAT, true);
            writer.write(new String(performanceStr.getBytes()));
            writer.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }
}
