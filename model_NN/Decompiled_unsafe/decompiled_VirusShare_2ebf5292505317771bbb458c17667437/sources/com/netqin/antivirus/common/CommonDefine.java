package com.netqin.antivirus.common;

import com.netqin.antivirus.scan.FileUtils;

public class CommonDefine {
    public static final String ANDROID_MARKET_PACKAGE_NAME_V15 = "com.netqin.antivirusgm15";
    public static final String ANDROID_MARKET_PACKAGE_NAME_V20 = "com.netqin.antivirusgm20";
    public static final int ANTILOST_REMOTECONTROL_ALARM = 3;
    public static final int ANTILOST_REMOTECONTROL_ERASE = 4;
    public static final int ANTILOST_REMOTECONTROL_LOCATE = 1;
    public static final int ANTILOST_REMOTECONTROL_LOCK = 2;
    public static final int ANTILOST_REMOTECONTROL_SMSNOTIFY = 5;
    public static final String ANTIVIRUS_BUILDER = "5024";
    public static final String ANTIVIRUS_OSID = "351";
    public static final String ANTIVIRUS_PID = "1979";
    public static final String ANTIVIRUS_SID = "101";
    public static final String ANTIVIRUS_SID_APP = "3";
    public static final String ANTIVIRUS_VERID;
    public static final String ANTIVIRUS_VERSION;
    public static final String CONTACT_FILE_TEMP = "netqin_network_down.vcf";
    public static final String CONTACT_VCF_PATH_1 = "/sdcard/netqin_contact.vcf";
    public static final boolean DEBUG_MARK = false;
    public static final boolean DEBUG_UPDATESOFTWARE_MARK = false;
    public static final boolean EXPORT_LOG = false;
    public static final boolean LOG_DEBUG_1 = false;
    public static final boolean LOG_DEBUG_2 = false;
    public static final int MARK_UPDATESOFT_POPUP_IN = 1;
    public static final int MARK_UPDATESOFT_POPUP_OUT = 2;
    public static final int MARK_UPDATESOFT_TYPE_FORCE = 2;
    public static final int MARK_UPDATESOFT_TYPE_NONE = 0;
    public static final int MARK_UPDATESOFT_TYPE_NORMAL = 1;
    public static final int MSG_APP_MESSAGE = 13;
    public static final int MSG_CHILD_FUNC_CALL = 12;
    public static final int MSG_PROGDLG_CANCEL = 4;
    public static final int MSG_PROGDLG_INIT = 2;
    public static final int MSG_PROGDLG_ONSTOP = 5;
    public static final int MSG_PROGDLG_SHOW = 1;
    public static final int MSG_PROGDLG_UPDATE = 3;
    public static final int MSG_PROGTEXT_CANCEL = 10;
    public static final int MSG_PROGTEXT_INIT = 7;
    public static final int MSG_PROGTEXT_ONSTOP = 11;
    public static final int MSG_PROGTEXT_PROGMAX = 8;
    public static final int MSG_PROGTEXT_SHOW = 6;
    public static final int MSG_PROGTEXT_UPDATE = 9;
    public static final int MSG_PROG_ARG_BEGIN = 21;
    public static final int MSG_PROG_ARG_CANCEL = 25;
    public static final int MSG_PROG_ARG_CANCEL_SERVER = 37;
    public static final int MSG_PROG_ARG_END = 22;
    public static final int MSG_PROG_ARG_ERRMORE = 28;
    public static final int MSG_PROG_ARG_ERRMSG = 27;
    public static final int MSG_PROG_ARG_ERRMSG_CONTACT_EXPORT = 34;
    public static final int MSG_PROG_ARG_ERRMSG_CONTACT_IMPORT = 35;
    public static final int MSG_PROG_ARG_ERRMSG_NETWORK = 36;
    public static final int MSG_PROG_ARG_ERROR = 26;
    public static final int MSG_PROG_ARG_FAIL = 24;
    public static final int MSG_PROG_ARG_FINISH = 30;
    public static final int MSG_PROG_ARG_NEEDUPDATE = 31;
    public static final int MSG_PROG_ARG_NOTUPDATE = 32;
    public static final int MSG_PROG_ARG_RETRY = 33;
    public static final int MSG_PROG_ARG_SUCCC = 23;
    public static final int MSG_PROG_ARG_UNDEFINE = 20;
    public static final int MSG_PROG_ARG_UPDATE = 29;
    public static final int MSG_PROG_ARG_VAL_0 = 0;
    public static final int MSG_PROG_ARG_VAL_1 = 1;
    public static final int MSG_PROG_ARG_VAL_2 = 2;
    public static final int MSG_PROG_ARG_VAL_3 = 3;
    public static final int MSG_PROG_ARG_VAL_4 = 4;
    public static final int MSG_PROG_ARG_VAL_5 = 5;
    public static final int MSG_PROG_ARG_VAL_6 = 6;
    public static final int MSG_PROG_ARG_VAL_7 = 7;
    public static final int MSG_PROG_ARG_VAL_8 = 8;
    public static final int MSG_PROG_ARG_VAL_9 = 9;
    public static final int NOTIFICATION_ID = 16616;
    public static String NOTIFICATION_MAIN = FileUtils.NOTIFICATION_MAIN;
    public static String NOTIFICATION_START = FileUtils.NOTIFICATION_START;
    public static final String PACKAGE_NAME = "com.netqin.antivirusgm20";
    public static final int PICK_CONTACT_SUBACTIVITY = 23;
    public static final int SESSION_BACKUP_TO_NETWORK = 1;
    public static final int SESSION_RESTORE_FROM_NETWORK = 2;
    public static final String SYSTEM_BLUETOOTH_RECEIVE_PATH1 = "/sdcard/bluetooth";
    public static final String SYSTEM_BLUETOOTH_RECEIVE_PATH2 = "/sdcard/downloads/bluetooth";
    public static final String SYSTEM_BLUETOOTH_RECEIVE_PATH3 = "/mnt/emmc/downloads/bluetooth";
    public static final String SYSTEM_BROWSER_DOWNLOAD_PATH = "/sdcard/download";
    public static final String UC_BROWSER_DOWNLOAD_PATH = "/sdcard/ucdownloads";
    public static final String VERSION = "1.0";
    public static final int VERSION_STYLE_PHONEGUARD = 3;
    public static final int VERSION_STYLE_RELEASE = 1;
    public static final int VERSION_STYLE_SHOPPING = 0;
    public static final int VERSION_STYLE_TYPE = 0;
    public static final int VERSION_STYLE_VERIFY = 2;
    public static final int VERSION_VENDER_NETQIN = 0;
    public static final int VERSION_VENDER_TYPE = 0;
    public static final int VERSION_VENDER_YULONG = 1;
    public static final int VERSION_VENDER_ZHAOHANG_WUHAN = 3;
    public static final int VERSION_VENDER_ZHONGXING = 2;
    public static String VIRUSDB_FILENAME = "nqvdb.db";
    public static String VIRUSDB_UPDATE_FILENAME = FileUtils.VIRUSDB_UPDATE_FILENAME;
    public static boolean firstRunCheckFinish = false;

    static {
        switch (0) {
            case 0:
                ANTIVIRUS_VERSION = "5.0.20.02";
                ANTIVIRUS_VERID = "417";
                return;
            case 1:
                ANTIVIRUS_VERSION = "4.0.98.80";
                ANTIVIRUS_VERID = "350";
                return;
            case 2:
                ANTIVIRUS_VERSION = "4.0.98.82";
                ANTIVIRUS_VERID = "354";
                return;
            case 3:
                ANTIVIRUS_VERSION = "4.0.98.88";
                ANTIVIRUS_VERID = "351";
                return;
            default:
                ANTIVIRUS_VERSION = "5.0.00.02";
                ANTIVIRUS_VERID = "398";
                return;
        }
    }
}
