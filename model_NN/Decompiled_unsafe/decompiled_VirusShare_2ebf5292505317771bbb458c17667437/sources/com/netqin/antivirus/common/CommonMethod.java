package com.netqin.antivirus.common;

import SHcMjZX.DD02zqNU;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RemoteViews;
import android.widget.TextView;
import com.netqin.antivirus.DialogActivity;
import com.netqin.antivirus.HomeActivity;
import com.netqin.antivirus.NqUtil;
import com.netqin.antivirus.Preferences;
import com.netqin.antivirus.Value;
import com.netqin.antivirus.antilost.AntiLostCommon;
import com.netqin.antivirus.antilost.AntiLostService;
import com.netqin.antivirus.antilost.SmsHandler;
import com.netqin.antivirus.antimallink.BlockService;
import com.netqin.antivirus.appprotocol.AppService;
import com.netqin.antivirus.appprotocol.AppValue;
import com.netqin.antivirus.appprotocol.DialogBoxForSubscribe;
import com.netqin.antivirus.appprotocol.XmlTagValue;
import com.netqin.antivirus.cloud.model.CloudDataService;
import com.netqin.antivirus.cloud.model.xml.XmlUtils;
import com.netqin.antivirus.contact.ContactCommon;
import com.netqin.antivirus.net.avservice.AVService;
import com.netqin.antivirus.networkmanager.SettingPreferences;
import com.netqin.antivirus.networkmanager.model.Counter;
import com.netqin.antivirus.scan.MonitorService;
import com.netqin.antivirus.scan.PeriodScanService;
import com.netqin.antivirus.scan.SilentCloudScanService;
import com.netqin.antivirus.services.ControlService;
import com.netqin.antivirus.services.NetMeterService;
import com.netqin.antivirus.services.TaskManagerService;
import com.nqmobile.antivirus_ampro20.R;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.Proxy;
import java.net.SocketException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonMethod {
    public static ProgressDialog progressDlgCreate(Context cnt, CharSequence text) {
        ProgressDialog dlg = new ProgressDialog(cnt);
        dlg.setIndeterminate(true);
        dlg.setCancelable(true);
        dlg.setMessage(text);
        return dlg;
    }

    public static ProgressDialogEx progressDlgCreate(Context cnt, CharSequence text, Handler handler) {
        ProgressDialogEx dlg = new ProgressDialogEx(cnt, handler);
        dlg.setIndeterminate(true);
        dlg.setCancelable(true);
        dlg.setMessage(text);
        return dlg;
    }

    public static void sendUserMessage(Handler handler, int msgVal) {
        Message msg = new Message();
        msg.what = msgVal;
        handler.sendMessage(msg);
    }

    public static void sendUserMessage(Handler handler, int msgVal, int progress) {
        Message msg = new Message();
        msg.what = msgVal;
        msg.arg1 = progress;
        handler.sendMessage(msg);
    }

    public static void messageDialog(Context cnt, int msgId, int titleId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(cnt);
        builder.setTitle(titleId);
        builder.setMessage(msgId);
        builder.setPositiveButton((int) R.string.label_ok, (DialogInterface.OnClickListener) null);
        builder.show();
    }

    public static void messageDialog(Context cnt, CharSequence msgTxt, int titleId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(cnt);
        builder.setTitle(titleId);
        builder.setMessage(msgTxt);
        builder.setPositiveButton((int) R.string.label_ok, (DialogInterface.OnClickListener) null);
        builder.show();
    }

    public static void messageDialog(Context cnt, int msgId, int titleId, DialogInterface.OnClickListener okClickListener, DialogInterface.OnKeyListener onKeyListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(cnt);
        builder.setTitle(titleId);
        builder.setMessage(msgId);
        builder.setPositiveButton((int) R.string.label_ok, okClickListener);
        builder.setOnKeyListener(onKeyListener);
        builder.show();
    }

    public static void messageDialog(Context cnt, CharSequence msgTxt, int titleId, DialogInterface.OnClickListener okClickListener, DialogInterface.OnKeyListener onKeyListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(cnt);
        builder.setTitle(titleId);
        builder.setMessage(msgTxt);
        builder.setPositiveButton((int) R.string.label_ok, okClickListener);
        builder.setOnKeyListener(onKeyListener);
        builder.show();
    }

    public static void messageDialog(Context cnt, int msgId, int titleId, DialogInterface.OnClickListener okClickListener, DialogInterface.OnClickListener cancelClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(cnt);
        builder.setTitle(titleId);
        builder.setMessage(msgId);
        builder.setPositiveButton((int) R.string.label_ok, okClickListener);
        builder.setNegativeButton((int) R.string.label_cancel, cancelClickListener);
        builder.show();
    }

    public static void messageDialog(Context cnt, CharSequence msgTxt, int titleId, DialogInterface.OnClickListener okClickListener, DialogInterface.OnClickListener cancelClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(cnt);
        builder.setTitle(titleId);
        builder.setMessage(msgTxt);
        builder.setPositiveButton((int) R.string.label_ok, okClickListener);
        builder.setNegativeButton((int) R.string.label_cancel, cancelClickListener);
        builder.show();
    }

    public static void messageDialog(Context cnt, int msgId, int titleId, DialogInterface.OnClickListener okClickListener, DialogInterface.OnClickListener cancelClickListener, DialogInterface.OnKeyListener onKeyListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(cnt);
        builder.setTitle(titleId);
        builder.setMessage(msgId);
        builder.setPositiveButton((int) R.string.label_ok, okClickListener);
        builder.setNegativeButton((int) R.string.label_cancel, cancelClickListener);
        builder.setOnKeyListener(onKeyListener);
        builder.show();
    }

    public static void messageDialog(Context cnt, CharSequence msgTxt, int titleId, DialogInterface.OnClickListener okClickListener, DialogInterface.OnClickListener cancelClickListener, DialogInterface.OnKeyListener onKeyListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(cnt);
        builder.setTitle(titleId);
        builder.setMessage(msgTxt);
        builder.setPositiveButton((int) R.string.label_ok, okClickListener);
        builder.setNegativeButton((int) R.string.label_cancel, cancelClickListener);
        builder.setOnKeyListener(onKeyListener);
        builder.show();
    }

    public static void okBtnDialogListen(Context cnt, DialogInterface.OnClickListener listener, String msg, int titleId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(cnt);
        builder.setTitle(titleId);
        builder.setMessage(msg);
        builder.setPositiveButton((int) R.string.label_ok, listener);
        builder.show();
    }

    public static void yesNoBtnDialogListen(Context cnt, DialogInterface.OnClickListener listenerYes, DialogInterface.OnClickListener listenerNo, String msg, int titleId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(cnt);
        builder.setTitle(titleId);
        builder.setMessage(msg);
        builder.setPositiveButton((int) R.string.label_ok, listenerYes);
        builder.setNegativeButton((int) R.string.label_cancel, listenerNo);
        builder.show();
    }

    public static void delVirusDialogListen(Context cnt, DialogInterface.OnClickListener listenerYes, DialogInterface.OnClickListener listenerNo, String msg, int titleId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(cnt);
        builder.setTitle(titleId);
        builder.setMessage(msg);
        builder.setPositiveButton((int) R.string.text_monitor_virus_uninstall, listenerYes);
        builder.setNegativeButton((int) R.string.label_cancel, listenerNo);
        builder.show();
    }

    public static void lookVirusDialogListen(Context cnt, DialogInterface.OnClickListener listenerYes, DialogInterface.OnClickListener listenerNo, String msg, int titleId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(cnt);
        builder.setTitle(titleId);
        builder.setMessage(msg);
        builder.setPositiveButton((int) R.string.period_virus_tip_look, listenerYes);
        builder.setNegativeButton((int) R.string.label_cancel, listenerNo);
        builder.show();
    }

    public static void blockWebDialogListen(Context cnt, DialogInterface.OnClickListener listenerYes, DialogInterface.OnClickListener listenerNo, String url) {
        AlertDialog.Builder builder = new AlertDialog.Builder(cnt);
        View vTotal = LayoutInflater.from(cnt).inflate((int) R.layout.blocker_dialog, (ViewGroup) null);
        ((TextView) vTotal.findViewById(R.id.blocker_url)).setText(url);
        builder.setView(vTotal);
        builder.setTitle((int) R.string.label_alert_from_netqin);
        builder.setPositiveButton((int) R.string.label_ok, listenerYes);
        builder.setCancelable(false);
        builder.show();
    }

    public static void yesNoBtnDialogListen(Context cnt, int positiveButtonStr, int negativeButtonStr, DialogInterface.OnClickListener listenerYes, DialogInterface.OnClickListener listenerNo, String msg, int titleId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(cnt);
        builder.setTitle(titleId);
        builder.setMessage(msg);
        builder.setPositiveButton(positiveButtonStr, listenerYes);
        builder.setNegativeButton(negativeButtonStr, listenerNo);
        builder.show();
    }

    public static String getIMEI(Context context) {
        String imei = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        if (TextUtils.isEmpty(imei) || imei.equals("000000000000000")) {
            return "100100000001002";
        }
        return imei;
    }

    public static String getIMSI(Context context) {
        String imsi = ((TelephonyManager) context.getSystemService("phone")).getSubscriberId();
        if (TextUtils.isEmpty(imsi)) {
            return "000000000000000";
        }
        return imsi;
    }

    public static Proxy getApnProxy(Context context) {
        if (((ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(1).isConnected()) {
            return null;
        }
        Cursor c = NqUtil.getCurrentApn(context);
        if (c.getCount() > 0) {
            c.moveToFirst();
            long j = c.getLong(c.getColumnIndex(SmsHandler.ROWID));
            String proxy = c.getString(c.getColumnIndex("proxy"));
            String port = c.getString(c.getColumnIndex("port"));
            c.close();
            if (proxy != null && !proxy.equals("")) {
                if (port == null || port.equals("")) {
                    return null;
                }
                return new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxy, Integer.valueOf(port).intValue()));
            }
        }
        c.close();
        return null;
    }

    public static Cursor getCurrentApn(Context context) {
        return context.getContentResolver().query(Uri.parse("content://telephony/carriers/preferapn"), null, null, null, null);
    }

    public static long getCurrentApnId(Context context) {
        Cursor c = context.getContentResolver().query(Uri.parse("content://telephony/carriers/preferapn"), null, null, null, null);
        if (c == null || c.getCount() <= 0) {
            return -1;
        }
        c.moveToFirst();
        long _id = c.getLong(c.getColumnIndex(SmsHandler.ROWID));
        c.close();
        return _id;
    }

    public static String getApnNameById(Context context, long id) {
        Cursor c = context.getContentResolver().query(Uri.parse("content://telephony/carriers"), null, "_id = " + id, null, null);
        if (c == null) {
            return "";
        }
        c.moveToFirst();
        String name = c.getString(c.getColumnIndex("name"));
        c.close();
        return name;
    }

    public static String getUserOrPassword(Context cnt, String name, String item) {
        return cnt.getSharedPreferences(name, 0).getString(item, "");
    }

    public static Boolean putUserOrPassword(Context cnt, String name, String item, String value) {
        cnt.getSharedPreferences(name, 0).edit().putString(item, value).commit();
        return true;
    }

    public static boolean putConfigWithStringValue(Context cnt, String fileName, String itemName, String itemValue) {
        cnt.getSharedPreferences(fileName, 0).edit().putString(itemName, itemValue).commit();
        return true;
    }

    public static String getConfigWithStringValue(Context cnt, String fileName, String itemName, String defaultValue) {
        return cnt.getSharedPreferences(fileName, 0).getString(itemName, defaultValue);
    }

    public static boolean putConfigWithBooleanValue(Context cnt, String fileName, String itemName, boolean itemValue) {
        cnt.getSharedPreferences(fileName, 0).edit().putBoolean(itemName, itemValue).commit();
        return true;
    }

    public static boolean getConfigWithBooleanValue(Context cnt, String fileName, String itemName, boolean defaultValue) {
        return cnt.getSharedPreferences(fileName, 0).getBoolean(itemName, defaultValue);
    }

    public static boolean putConfigWithIntegerValue(Context cnt, String fileName, String itemName, int itemValue) {
        cnt.getSharedPreferences(fileName, 0).edit().putInt(itemName, itemValue).commit();
        return true;
    }

    public static int getConfigWithIntegerValue(Context cnt, String fileName, String itemName, int defaultValue) {
        return cnt.getSharedPreferences(fileName, 0).getInt(itemName, defaultValue);
    }

    public static void showRemindDialog(Context context, String message) {
        new AlertDialog.Builder(context).setTitle((int) R.string.label_netqin_antivirus).setMessage(message).setPositiveButton((int) R.string.label_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        }).setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();
            }
        }).create().show();
    }

    public static void showConnectFailDialog(Context context) {
        new AlertDialog.Builder(context).setTitle((int) R.string.label_netqin_antivirus).setMessage(context.getString(R.string.SEND_RECEIVE_ERROR)).setPositiveButton((int) R.string.label_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        }).setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();
            }
        }).create().show();
    }

    public static Boolean isDateValid(String date) {
        if (date == null) {
            return false;
        }
        String[] ymd = date.split("-");
        int yOrg = Integer.parseInt(ymd[0]);
        int mOrg = Integer.parseInt(ymd[1]);
        int dOrg = Integer.parseInt(ymd[2]);
        long curr = System.currentTimeMillis();
        int yNow = Integer.parseInt(DateFormat.format("yyyy", curr).toString());
        int mNow = Integer.parseInt(DateFormat.format("M", curr).toString());
        int dNow = Integer.parseInt(DateFormat.format("d", curr).toString());
        if (yOrg < yNow) {
            return false;
        }
        if (yOrg > yNow) {
            return true;
        }
        if (mOrg < mNow) {
            return false;
        }
        if (mOrg > mNow) {
            return true;
        }
        if (dOrg < dNow) {
            return false;
        }
        return true;
    }

    private static void saveUserInfo(String key, String tag, ContentValues content, SharedPreferences spf) {
        if (content.containsKey(key)) {
            String s = content.getAsString(key);
            if (!TextUtils.isEmpty(s)) {
                spf.edit().putString(tag, s).commit();
            }
        }
    }

    public static void saveUserAVInfo(Context context, ContentValues content) {
        SharedPreferences spf = context.getSharedPreferences("netqin", 0);
        saveUserInfo("UID", XmlUtils.LABEL_CLIENTINFO_UID, content, spf);
        saveUserInfo("UserType", "usertype", content, spf);
        saveUserInfo("LevelName", "levelname", content, spf);
        saveUserInfo(XmlTagValue.isRegistered, "isregistered", content, spf);
        saveUserInfo("Balance", "balance", content, spf);
        saveUserInfo(XmlTagValue.isMember, "ismember", content, spf);
        saveUserInfo("score", "score", content, spf);
        saveUserInfo(XmlTagValue.increaseSpeed, "increasespeed", content, spf);
        saveUserInfo("ExpiredTime", "expiredtime", content, spf);
        saveUserInfo(XmlTagValue.isSoftWareExpired, "issoftwareexpired", content, spf);
        saveUserInfo(XmlTagValue.purchasedVirusVersion, "purchasedvirusversion", content, spf);
        saveUserInfo(XmlTagValue.latestVirusVersion, "latestvirusversion", content, spf);
        saveUserInfo(XmlTagValue.virusForecastName, "virusforecastname", content, spf);
        saveUserInfo(XmlTagValue.level, "virusforecastlevel", content, spf);
        saveUserInfo(XmlTagValue.levelValue, "virusforecastlevelvalue", content, spf);
        saveUserInfo(XmlTagValue.Type, "virusforecasttype", content, spf);
        saveUserInfo(XmlTagValue.alias, "virusforecastalias", content, spf);
        saveUserInfo(XmlTagValue.desc, "virusforecastdesc", content, spf);
        saveUserInfo(XmlTagValue.wapUrl, "virusforecastwapurl", content, spf);
    }

    public static void saveNextConnectTime(Context context, ContentValues content, AppValue value) {
        SharedPreferences spf = context.getSharedPreferences("netqin", 0);
        int commandId = 0;
        if (content.containsKey("Command")) {
            commandId = content.getAsInteger("Command").intValue();
        }
        if (content.containsKey("NextConnectTime")) {
            String s = content.getAsString("NextConnectTime").trim();
            if (!TextUtils.isEmpty(s)) {
                int minutes = Integer.valueOf(s).intValue();
                Calendar calendar = Calendar.getInstance();
                calendar.add(12, minutes);
                if (commandId == 22) {
                    spf.edit().putString("next_freedatacheck_time", NqUtil.getStringByCalendar(calendar)).commit();
                } else if (value.requestCommandId == 16) {
                    spf.edit().putString("next_dialycheck_time", NqUtil.getStringByCalendar(calendar)).commit();
                }
            }
        } else if (content.containsKey("NextConnectInterval")) {
            String s2 = content.getAsString("NextConnectInterval").trim();
            if (!TextUtils.isEmpty(s2)) {
                int minutes2 = Integer.valueOf(s2).intValue();
                Calendar calendar2 = Calendar.getInstance();
                calendar2.add(12, minutes2);
                if (commandId == 22) {
                    spf.edit().putString("next_freedatacheck_time", NqUtil.getStringByCalendar(calendar2)).commit();
                } else if (value.requestCommandId == 16) {
                    spf.edit().putString("next_dialycheck_time", NqUtil.getStringByCalendar(calendar2)).commit();
                }
            }
        }
        if (commandId == 22) {
            spf.edit().putString("last_freedatacheck_time", NqUtil.getCurrentTime()).commit();
        } else if (value.requestCommandId == 16) {
            spf.edit().putString("last_dialycheck_time", NqUtil.getCurrentTime()).commit();
        }
    }

    public static void saveSubscribeDialogInfo(Context context, ContentValues content, AppValue value) {
        SharedPreferences spf = context.getSharedPreferences("subscribedialog", 0);
        int size = value.dialogBoxList.size();
        for (int i = 0; i < size; i++) {
            int postion = value.dialogBoxList.get(i).postion;
            spf.edit().putString(XmlUtils.LABEL_CONTENT + postion, value.dialogBoxList.get(i).content).commit();
            spf.edit().putInt(XmlTagValue.postion + postion, value.dialogBoxList.get(i).postion).commit();
            spf.edit().putBoolean(XmlTagValue.isShow + postion, value.dialogBoxList.get(i).isShow).commit();
        }
    }

    public static DialogBoxForSubscribe getSubscribeDialogInfo(Context context, int postion) {
        SharedPreferences spf = context.getSharedPreferences("subscribedialog", 0);
        if (spf.getInt(XmlTagValue.postion + postion, 0) != postion) {
            return null;
        }
        DialogBoxForSubscribe dbfs = new DialogBoxForSubscribe();
        dbfs.content = spf.getString(XmlUtils.LABEL_CONTENT + postion, "");
        dbfs.isShow = spf.getBoolean(XmlTagValue.isShow + postion, true);
        dbfs.postion = postion;
        return dbfs;
    }

    public static String getIsMemberString(Context context) {
        return context.getSharedPreferences("netqin", 0).getString("ismember", "N");
    }

    public static int getUserType(Context context) {
        try {
            return Integer.parseInt(context.getSharedPreferences("netqin", 0).getString("usertype", "32"));
        } catch (Exception e) {
            return 32;
        }
    }

    public static boolean getIsMember(Context context) {
        if (context.getSharedPreferences("netqin", 0).getString("ismember", "N").equalsIgnoreCase("Y")) {
            return true;
        }
        return false;
    }

    public static void setIsMember(Context context, boolean member) {
        String str = "N";
        if (member) {
            str = "Y";
        }
        context.getSharedPreferences("netqin", 0).edit().putString("ismember", str).commit();
    }

    public static String getUserBalance(Context context) {
        return context.getSharedPreferences("netqin", 0).getString("balance", "0");
    }

    public static String getUserLevelname(Context context) {
        return context.getSharedPreferences("netqin", 0).getString("levelname", "0");
    }

    public static void setUserLevelname(Context context, String name) {
        context.getSharedPreferences("netqin", 0).edit().putString("levelname", name).commit();
    }

    public static String getUID(Context context) {
        return context.getSharedPreferences("netqin", 0).getString(XmlUtils.LABEL_CLIENTINFO_UID, "0");
    }

    public static String getLatestVirusDBVersion(Context context) {
        return context.getSharedPreferences("netqin", 0).getString("latestvirusversion", "2011041001");
    }

    public static String getVirusForecastName(Context context) {
        return context.getSharedPreferences("netqin", 0).getString("virusforecastname", "");
    }

    public static void setVirusForecastName(Context context, String name) {
        context.getSharedPreferences("netqin", 0).edit().putString("virusforecastname", name).commit();
    }

    public static String getVirusForecastLevel(Context context) {
        return context.getSharedPreferences("netqin", 0).getString("virusforecastlevel", "");
    }

    public static void setVirusForecastLevel(Context context, String name) {
        context.getSharedPreferences("netqin", 0).edit().putString("virusforecastlevel", name).commit();
    }

    public static String getVirusForecastLevelvalue(Context context) {
        return context.getSharedPreferences("netqin", 0).getString("virusforecastlevelvalue", "0");
    }

    public static void setVirusForecastLevelvalue(Context context, String name) {
        context.getSharedPreferences("netqin", 0).edit().putString("virusforecastlevelvalue", name).commit();
    }

    public static String getVirusForecastType(Context context) {
        return context.getSharedPreferences("netqin", 0).getString("virusforecasttype", "");
    }

    public static void setVirusForecastType(Context context, String name) {
        context.getSharedPreferences("netqin", 0).edit().putString("virusforecasttype", name).commit();
    }

    public static String getVirusForecastAlias(Context context) {
        return context.getSharedPreferences("netqin", 0).getString("virusforecastalias", "");
    }

    public static void setVirusForecastAlias(Context context, String name) {
        context.getSharedPreferences("netqin", 0).edit().putString("virusforecastalias", name).commit();
    }

    public static String getVirusForecastDesc(Context context) {
        return context.getSharedPreferences("netqin", 0).getString("virusforecastdesc", "");
    }

    public static void setVirusForecastDesc(Context context, String name) {
        context.getSharedPreferences("netqin", 0).edit().putString("virusforecastdesc", name).commit();
    }

    public static String getVirusForecastWapurl(Context context) {
        return context.getSharedPreferences("netqin", 0).getString("virusforecastwapurl", "");
    }

    public static void setVirusForecastWapurl(Context context, String name) {
        context.getSharedPreferences("netqin", 0).edit().putString("virusforecastwapurl", name).commit();
    }

    public static boolean isLocalSimpleChinese() {
        Locale locale = Locale.getDefault();
        String countryCode = locale.getCountry();
        String languageCode = locale.getLanguage();
        if (countryCode == null || countryCode.length() <= 0) {
            if (languageCode == null || languageCode.length() <= 0 || languageCode.compareToIgnoreCase("zh") != 0) {
                return false;
            }
            return true;
        } else if (countryCode.compareToIgnoreCase("CN") == 0) {
            return true;
        } else {
            return false;
        }
    }

    public static String getPlatformLanguage() {
        String languageCode = Locale.getDefault().getLanguage();
        if (languageCode.equals("en")) {
            return "1";
        }
        if (languageCode.equals("zh")) {
            return Value.SoftLanguage;
        }
        if (languageCode.equals("ko")) {
            return "38";
        }
        return "1";
    }

    public static String getCountryLanguage() {
        Locale locale = Locale.getDefault();
        String countryCode = locale.getCountry();
        if (TextUtils.isEmpty(countryCode)) {
            countryCode = "";
        }
        String languageCode = locale.getLanguage();
        if (TextUtils.isEmpty(languageCode)) {
            languageCode = "";
        }
        return String.valueOf(languageCode) + "_" + countryCode;
    }

    public static String getCountryCode() {
        return Value.Country;
    }

    public static boolean isValidEmail(String text) {
        if (TextUtils.isEmpty(text)) {
            return false;
        }
        int index = text.indexOf(64);
        if (index < 1) {
            return false;
        }
        String left = text.substring(0, index);
        String right = text.substring(index + 1);
        if (!Pattern.compile("^[a-zA-Z0-9]+([a-zA-Z0-9_|\\.]*)").matcher(left).matches()) {
            return false;
        }
        return Pattern.compile("([a-zA-Z0-9]+[_|\\.]?)*[a-zA-Z0-9]+.[a-zA-Z]{2,3}$").matcher(right).matches();
    }

    public static boolean isValidName(String text) {
        if (TextUtils.isEmpty(text)) {
            return false;
        }
        if (Pattern.compile("^[0-9]+$").matcher(text).matches()) {
            return false;
        }
        return Pattern.compile("^[A-Za-z0-9_]+$").matcher(text).matches();
    }

    public static boolean isValidLetterAndNumber(String text) {
        if (TextUtils.isEmpty(text)) {
            return false;
        }
        return Pattern.compile("[A-Za-z0-9_]+$").matcher(text).matches();
    }

    public static void setNextDialyCheckTime(Context context, Calendar calendar) {
        context.getSharedPreferences("netqin", 0).edit().putString("next_dialycheck_time", NqUtil.getStringByCalendar(calendar)).commit();
    }

    public static String getNextDialyCheckTime(Context context) {
        return context.getSharedPreferences("netqin", 0).getString("next_dialycheck_time", "");
    }

    public static void setLastDialyCheckTimeToCurrentTime(Context context) {
        context.getSharedPreferences("netqin", 0).edit().putString("last_dialycheck_time", NqUtil.getCurrentTime()).commit();
    }

    public static String getLastDialyCheckTime(Context context) {
        return context.getSharedPreferences("netqin", 0).getString("last_dialycheck_time", "");
    }

    public static void setLastFreedataCheckTimeToCurrentTime(Context context) {
        context.getSharedPreferences("netqin", 0).edit().putString("last_freedatacheck_time", NqUtil.getCurrentTime()).commit();
    }

    public static void setNextFreedataCheckTime(Context context, Calendar calendar) {
        context.getSharedPreferences("netqin", 0).edit().putString("next_freedatacheck_time", NqUtil.getStringByCalendar(calendar)).commit();
    }

    public static String getNextFreedataCheckTime(Context context) {
        return context.getSharedPreferences("netqin", 0).getString("next_freedatacheck_time", "");
    }

    public static String getLastFreedataCheckTime(Context context) {
        return context.getSharedPreferences("netqin", 0).getString("last_freedatacheck_time", "");
    }

    public static void setSoftUpdateNextDialyCheckTime(Context context, Calendar calendar) {
        context.getSharedPreferences("netqin", 0).edit().putString("next_softupdate_dialycheck_time", NqUtil.getStringByCalendar(calendar)).commit();
    }

    public static String getSoftUpdateNextDialyCheckTime(Context context) {
        return context.getSharedPreferences("netqin", 0).getString("next_softupdate_dialycheck_time", "");
    }

    public static void setSoftUpdateLastDialyCheckTimeToCurrentTime(Context context) {
        context.getSharedPreferences("netqin", 0).edit().putString("last_softupdate_dialycheck_time", NqUtil.getCurrentTime()).commit();
    }

    public static String getSoftUpdateLastDialyCheckTime(Context context) {
        return context.getSharedPreferences("netqin", 0).getString("last_softupdate_dialycheck_time", "");
    }

    public static void setNextPeriodScanTime(Context context, Calendar calendar) {
        context.getSharedPreferences("netqin", 0).edit().putString("next_period_scan_time", NqUtil.getStringByCalendar(calendar)).commit();
    }

    public static void setLastPeriodScanTime(Context context, Calendar calendar) {
        context.getSharedPreferences("netqin", 0).edit().putString("last_period_scan_time", NqUtil.getStringByCalendar(calendar)).commit();
    }

    public static void cleanPeriodScanTime(Context context) {
        context.getSharedPreferences("netqin", 0).edit().putString("next_period_scan_time", "").commit();
    }

    public static String getNextPeriodScanTime(Context context) {
        return context.getSharedPreferences("netqin", 0).getString("next_period_scan_time", "");
    }

    public static String getLastPeriodScanTime(Context context) {
        return context.getSharedPreferences("netqin", 0).getString("last_period_scan_time", "");
    }

    public static void setFirstCloudScanTime(Context context, Calendar calendar) {
        context.getSharedPreferences("netqin", 0).edit().putString("first_cloud_scan_time", NqUtil.getStringByCalendar(calendar)).commit();
    }

    public static String getFirstCloudScanTime(Context context) {
        return context.getSharedPreferences("netqin", 0).getString("first_cloud_scan_time", "");
    }

    public static String decryAndUnzipFile(String oldPath, String newPath) {
        String decryFilePath = String.valueOf(oldPath) + "_decry";
        try {
            DesCript.decryptFile(oldPath, decryFilePath);
            Zlib.ZipDecompress(decryFilePath, newPath);
            File fileTmp = new File(decryFilePath);
            if (!fileTmp.exists()) {
                return "";
            }
            fileTmp.delete();
            return "";
        } catch (Exception e) {
            return e.getLocalizedMessage();
        }
    }

    public static String zipAndEncryFile(String oldPath, String newPath) {
        logDebug(AntiLostCommon.DELETE_CONTACT, "zipAndEncryFile: Begin");
        String msg = "";
        String zipFilePath = String.valueOf(oldPath) + "_zip";
        try {
            Zlib.ZipCompress(oldPath, zipFilePath);
            DesCript.encryptFile(zipFilePath, newPath);
            File fileTmp = new File(zipFilePath);
            if (fileTmp.exists()) {
                fileTmp.delete();
            }
        } catch (Exception e) {
            msg = e.getLocalizedMessage();
        }
        logDebug(AntiLostCommon.DELETE_CONTACT, "zipAndEncryFile: End");
        return msg;
    }

    public static void logDebug(String tag, String msg) {
    }

    public static void logDebug1(String tag, String msg) {
    }

    public static void logDebug2(String tag, String msg) {
    }

    public static void toastDebug(Context cnt, String msg) {
    }

    /* JADX INFO: Multiple debug info for r2v3 int: [D('fis' java.io.FileInputStream), D('size' int)] */
    /* JADX INFO: Multiple debug info for r0v5 byte[]: [D('file' java.io.File), D('buffer' byte[])] */
    /* JADX INFO: Multiple debug info for r8v28 short: [D('crc' short), D('crcBuffer' byte[])] */
    /* JADX INFO: Multiple debug info for r2v44 short: [D('crc' short), D('crcBuffer' byte[])] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00fd A[Catch:{ Exception -> 0x012c }] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x011b A[Catch:{ Exception -> 0x012c }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getChannelId(android.content.ContextWrapper r8) {
        /*
            java.lang.String r1 = "1979"
            java.io.File r0 = new java.io.File
            java.lang.String r2 = r8.getPackageResourcePath()
            r0.<init>(r2)
            boolean r2 = r0.exists()
            if (r2 == 0) goto L_0x0136
            r2 = 0
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0124 }
            r4.<init>(r0)     // Catch:{ Exception -> 0x0124 }
            int r2 = r4.available()     // Catch:{ Exception -> 0x012c }
            r0 = 10
            byte[] r0 = new byte[r0]     // Catch:{ Exception -> 0x012c }
            r3 = 10
            int r2 = r2 - r3
            long r2 = (long) r2     // Catch:{ Exception -> 0x012c }
            r4.skip(r2)     // Catch:{ Exception -> 0x012c }
            r4.read(r0)     // Catch:{ Exception -> 0x012c }
            r6 = 1
            r2 = 0
            byte r2 = r0[r2]     // Catch:{ Exception -> 0x012c }
            r3 = -75
            if (r2 != r3) goto L_0x0138
            r2 = 1
            byte r2 = r0[r2]     // Catch:{ Exception -> 0x012c }
            r3 = -98
            if (r2 != r3) goto L_0x0138
            r2 = 2
            byte r2 = r0[r2]     // Catch:{ Exception -> 0x012c }
            r3 = 7
            if (r2 != r3) goto L_0x0138
            r2 = 3
            byte r2 = r0[r2]     // Catch:{ Exception -> 0x012c }
            r3 = -80
            if (r2 != r3) goto L_0x0138
            r2 = 4
            byte r2 = r0[r2]     // Catch:{ Exception -> 0x012c }
            r2 = r2 & 255(0xff, float:3.57E-43)
            r3 = 5
            byte r3 = r0[r3]     // Catch:{ Exception -> 0x012c }
            int r3 = r3 << 8
            r5 = 65280(0xff00, float:9.1477E-41)
            r3 = r3 & r5
            r2 = r2 | r3
            r3 = 6
            byte r3 = r0[r3]     // Catch:{ Exception -> 0x012c }
            int r3 = r3 << 24
            int r3 = r3 >>> 8
            r2 = r2 | r3
            r3 = 7
            byte r3 = r0[r3]     // Catch:{ Exception -> 0x012c }
            int r3 = r3 << 24
            r5 = r2 | r3
            r2 = 4
            byte[] r2 = new byte[r2]     // Catch:{ Exception -> 0x012c }
            r3 = 0
        L_0x0067:
            r7 = 4
            if (r3 < r7) goto L_0x0111
            short r2 = com.netqin.antivirus.common.CRC16.toCrc(r2)     // Catch:{ Exception -> 0x012c }
            r3 = 8
            byte r3 = r0[r3]     // Catch:{ Exception -> 0x012c }
            r3 = r3 & 255(0xff, float:3.57E-43)
            r7 = 9
            byte r7 = r0[r7]     // Catch:{ Exception -> 0x012c }
            int r7 = r7 << 8
            r3 = r3 | r7
            short r3 = (short) r3     // Catch:{ Exception -> 0x012c }
            if (r3 != r2) goto L_0x0138
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x012c }
            java.lang.String r3 = java.lang.String.valueOf(r5)     // Catch:{ Exception -> 0x012c }
            r2.<init>(r3)     // Catch:{ Exception -> 0x012c }
            java.lang.String r1 = r2.toString()     // Catch:{ Exception -> 0x012c }
            r2 = 0
        L_0x008c:
            if (r2 == 0) goto L_0x0136
            java.util.zip.ZipFile r6 = new java.util.zip.ZipFile     // Catch:{ Exception -> 0x012c }
            java.lang.String r8 = r8.getPackageResourcePath()     // Catch:{ Exception -> 0x012c }
            r6.<init>(r8)     // Catch:{ Exception -> 0x012c }
            java.lang.String r8 = "res/raw/channel.dat"
            java.util.zip.ZipEntry r8 = r6.getEntry(r8)     // Catch:{ Exception -> 0x012c }
            if (r8 == 0) goto L_0x0136
            java.io.InputStream r5 = r6.getInputStream(r8)     // Catch:{ Exception -> 0x012c }
            int r8 = r5.read(r0)     // Catch:{ Exception -> 0x012c }
            if (r8 <= 0) goto L_0x0134
            r8 = 0
            byte r8 = r0[r8]     // Catch:{ Exception -> 0x012c }
            r2 = -75
            if (r8 != r2) goto L_0x0134
            r8 = 1
            byte r8 = r0[r8]     // Catch:{ Exception -> 0x012c }
            r2 = -98
            if (r8 != r2) goto L_0x0134
            r8 = 2
            byte r8 = r0[r8]     // Catch:{ Exception -> 0x012c }
            r2 = 7
            if (r8 != r2) goto L_0x0134
            r8 = 3
            byte r8 = r0[r8]     // Catch:{ Exception -> 0x012c }
            r2 = -80
            if (r8 != r2) goto L_0x0134
            r8 = 4
            byte r8 = r0[r8]     // Catch:{ Exception -> 0x012c }
            r8 = r8 & 255(0xff, float:3.57E-43)
            r2 = 5
            byte r2 = r0[r2]     // Catch:{ Exception -> 0x012c }
            int r2 = r2 << 8
            r3 = 65280(0xff00, float:9.1477E-41)
            r2 = r2 & r3
            r8 = r8 | r2
            r2 = 6
            byte r2 = r0[r2]     // Catch:{ Exception -> 0x012c }
            int r2 = r2 << 24
            int r2 = r2 >>> 8
            r8 = r8 | r2
            r2 = 7
            byte r2 = r0[r2]     // Catch:{ Exception -> 0x012c }
            int r2 = r2 << 24
            r3 = r8 | r2
            r8 = 4
            byte[] r8 = new byte[r8]     // Catch:{ Exception -> 0x012c }
            r2 = 0
        L_0x00e6:
            r7 = 4
            if (r2 < r7) goto L_0x011b
            short r8 = com.netqin.antivirus.common.CRC16.toCrc(r8)     // Catch:{ Exception -> 0x012c }
            r2 = 8
            byte r2 = r0[r2]     // Catch:{ Exception -> 0x012c }
            r2 = r2 & 255(0xff, float:3.57E-43)
            r7 = 9
            byte r0 = r0[r7]     // Catch:{ Exception -> 0x012c }
            int r0 = r0 << 8
            r0 = r0 | r2
            short r0 = (short) r0     // Catch:{ Exception -> 0x012c }
            if (r0 != r8) goto L_0x0134
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x012c }
            java.lang.String r0 = java.lang.String.valueOf(r3)     // Catch:{ Exception -> 0x012c }
            r8.<init>(r0)     // Catch:{ Exception -> 0x012c }
            java.lang.String r8 = r8.toString()     // Catch:{ Exception -> 0x012c }
        L_0x010a:
            r5.close()     // Catch:{ Exception -> 0x0131 }
            r6.close()     // Catch:{ Exception -> 0x0131 }
        L_0x0110:
            return r8
        L_0x0111:
            int r7 = r3 + 4
            byte r7 = r0[r7]     // Catch:{ Exception -> 0x012c }
            r2[r3] = r7     // Catch:{ Exception -> 0x012c }
            int r3 = r3 + 1
            goto L_0x0067
        L_0x011b:
            int r7 = r2 + 4
            byte r7 = r0[r7]     // Catch:{ Exception -> 0x012c }
            r8[r2] = r7     // Catch:{ Exception -> 0x012c }
            int r2 = r2 + 1
            goto L_0x00e6
        L_0x0124:
            r8 = move-exception
            r0 = r8
            r8 = r1
            r1 = r2
        L_0x0128:
            r0.printStackTrace()
            goto L_0x0110
        L_0x012c:
            r8 = move-exception
            r0 = r8
            r8 = r1
            r1 = r4
            goto L_0x0128
        L_0x0131:
            r0 = move-exception
            r1 = r4
            goto L_0x0128
        L_0x0134:
            r8 = r1
            goto L_0x010a
        L_0x0136:
            r8 = r1
            goto L_0x0110
        L_0x0138:
            r2 = r6
            goto L_0x008c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.netqin.antivirus.common.CommonMethod.getChannelId(android.content.ContextWrapper):java.lang.String");
    }

    public static void exitAntiVirus(Activity act) {
        if (act != null) {
            ((ActivityManager) act.getSystemService("activity")).restartPackage("com.netqin.antivirusgm20");
            if (HomeActivity.mActAntiVirusHome != null) {
                if (act != HomeActivity.mActAntiVirusHome) {
                    act.finish();
                }
                Intent i = new Intent("android.intent.action.RUN");
                i.setClass(act.getApplicationContext(), MonitorService.class);
                act.stopService(i);
                Intent i2 = new Intent("android.intent.action.RUN");
                i2.setClass(act.getApplicationContext(), SilentCloudScanService.class);
                act.stopService(i2);
                Intent i3 = new Intent("android.intent.action.RUN");
                i3.setClass(act.getApplicationContext(), BlockService.class);
                act.stopService(i3);
                Intent i4 = new Intent("android.intent.action.RUN");
                i4.setClass(act.getApplicationContext(), ControlService.class);
                act.stopService(i4);
                Intent i5 = new Intent("android.intent.action.RUN");
                i5.setClass(act.getApplicationContext(), AppService.class);
                act.stopService(i5);
                Intent i6 = new Intent("android.intent.action.RUN");
                i6.setClass(act.getApplicationContext(), CloudDataService.class);
                act.stopService(i6);
                Intent i7 = new Intent("android.intent.action.RUN");
                i7.setClass(act.getApplicationContext(), PeriodScanService.class);
                act.stopService(i7);
                Intent i8 = new Intent("android.intent.action.RUN");
                i8.setClass(act.getApplicationContext(), NetMeterService.class);
                act.stopService(i8);
                Intent i9 = new Intent("android.intent.action.RUN");
                i9.setClass(act.getApplicationContext(), TaskManagerService.class);
                act.stopService(i9);
                Intent i10 = new Intent("android.intent.action.RUN");
                i10.setClass(act.getApplicationContext(), AVService.class);
                act.stopService(i10);
                closeNotification(act, Value.NOTIFICATION_FLOW_ID);
                closeNotification(act, Value.NOTIFICATIONMANUALCLEAR_ID);
                closeNotification(act, Value.NOTIFICATION_NOCLEAR_ID);
                closeNotification(act, 111);
                setSoftExitState(act, true);
                HomeActivity.mActAntiVirusHome.finish();
                HomeActivity.mActAntiVirusHome = null;
                ((ActivityManager) act.getSystemService("activity")).restartPackage(act.getPackageName());
            }
        }
    }

    public static void copyFile(String srcPath, String dstPath) {
        String strBuff;
        File srcFile = new File(srcPath);
        File dstFile = new File(dstPath);
        if (srcFile.exists() && srcFile.canRead()) {
            if (dstFile.exists()) {
                dstFile.delete();
            }
            try {
                dstFile.createNewFile();
                FileInputStream srcFIS = new FileInputStream(srcFile);
                BufferedReader buffReader = new BufferedReader(new InputStreamReader(srcFIS));
                FileOutputStream dstFOS = new FileOutputStream(dstFile);
                do {
                    strBuff = buffReader.readLine();
                    if (!TextUtils.isEmpty(strBuff)) {
                        strBuff = String.valueOf(strBuff) + "\r\n";
                        dstFOS.write(strBuff.getBytes());
                    }
                } while (!TextUtils.isEmpty(strBuff));
                srcFIS.close();
                dstFOS.flush();
                dstFOS.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static boolean isGpsEnable(Context context) {
        return ((LocationManager) context.getSystemService("location")).isProviderEnabled("gps");
    }

    public static boolean isLegalSecurityNumber(String str) {
        if (!Pattern.compile("\\+?[0-9|\\-]{4,26}").matcher(str).matches()) {
            return false;
        }
        return true;
    }

    public static boolean isAntiLostSMSCommand(String password, String str) {
        String str2 = str.toLowerCase().trim();
        Pattern pattern1 = Pattern.compile("(locate|delete|lock|alarm)\\W" + password + "\\W");
        Pattern pattern2 = Pattern.compile("(reset)\\W");
        Pattern pattern3 = Pattern.compile("(lock|alarm)\\W" + password + "\\W.*");
        Matcher isLegal1 = pattern1.matcher(str2);
        Matcher isLegal2 = pattern2.matcher(str2);
        Matcher isLegal3 = pattern3.matcher(str2);
        if (isLegal1.matches() || isLegal2.matches() || isLegal3.matches()) {
            return true;
        }
        return false;
    }

    public static boolean isAntiLostSMSCommandWrongPassword(String str) {
        String str2 = str.toLowerCase().trim();
        Pattern pattern1 = Pattern.compile("(locate|delete|lock|alarm)\\W\\d{1,20}\\W");
        Pattern pattern3 = Pattern.compile("(lock|alarm)\\W\\d{1,20}\\W.*");
        Matcher isLegal1 = pattern1.matcher(str2);
        Matcher isLegal3 = pattern3.matcher(str2);
        if (isLegal1.matches() || isLegal3.matches()) {
            return true;
        }
        return false;
    }

    public static String getUploadConfig(Context context) {
        String userId = "uid=" + context.getSharedPreferences("netqin", 0).getString(XmlUtils.LABEL_CLIENTINFO_UID, "0");
        String model = "m=" + Build.MODEL;
        Locale locale = Locale.getDefault();
        return String.valueOf(userId) + "&" + model + "&" + ("l=" + locale.getLanguage().toLowerCase() + "_" + locale.getCountry().toLowerCase()) + "&" + "sid=101" + "&" + "osid=351" + "&" + ("verid=" + CommonDefine.ANTIVIRUS_VERID) + "&" + ("pid=" + Preferences.getPreferences(context).getChanelIdStore());
    }

    public static String getUploadConfigFeedBack(Context context) {
        String userId = "uid=" + context.getSharedPreferences("netqin", 0).getString(XmlUtils.LABEL_CLIENTINFO_UID, "0");
        String model = "m=" + Build.MODEL;
        Locale locale = Locale.getDefault();
        return String.valueOf(userId) + "&" + model + "&" + ("l=" + locale.getLanguage().toLowerCase() + "_" + locale.getCountry().toLowerCase()) + "&" + "sid=3" + "&" + "osid=351" + "&" + ("verid=" + CommonDefine.ANTIVIRUS_VERID) + "&" + ("pid=" + Preferences.getPreferences(context).getChanelIdStore());
    }

    public static String md5(String s) {
        try {
            MessageDigest digest = MessageDigest.getInstance(Coder.KEY_MD5);
            digest.update(s.getBytes());
            byte[] messageDigest = digest.digest();
            StringBuffer hexString = new StringBuffer();
            for (byte b : messageDigest) {
                String hex = Integer.toHexString(b & 255);
                if (hex.length() == 1) {
                    hex = String.valueOf(0) + hex;
                }
                hexString.append(hex);
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String encryptMD5(String str) {
        String md = md5(str);
        String a = md.substring(24, 26);
        String b = md.substring(26, 28);
        String c = md.substring(28, 30);
        String d = md.substring(30, 32);
        return String.valueOf(String.valueOf(Integer.valueOf(a, 16).intValue() % 10)) + String.valueOf(Integer.valueOf(b, 16).intValue() % 10) + String.valueOf(Integer.valueOf(c, 16).intValue() % 10) + String.valueOf(Integer.valueOf(d, 16).intValue() % 10);
    }

    public static boolean isMemberOfNetQin(Context context) {
        if (getConfigWithStringValue(context, "netqin", XmlUtils.LABEL_CLIENTINFO_UID, "0").equalsIgnoreCase("0")) {
            return false;
        }
        if (getConfigWithStringValue(context, "netqin", "isexpired", "Y").equalsIgnoreCase("Y")) {
            return false;
        }
        return "2".equalsIgnoreCase(getConfigWithStringValue(context, "netqin", "usertype", "0"));
    }

    public static void showNotification(Context ct, String str) {
        NotificationManager notificationManager = (NotificationManager) ct.getSystemService("notification");
        Notification notification = new Notification(R.drawable.icon, str, System.currentTimeMillis());
        notification.setLatestEventInfo(ct, CommonDefine.NOTIFICATION_MAIN, str, PendingIntent.getActivity(ct, 0, new Intent(ct, HomeActivity.class), 0));
        notificationManager.notify(CommonDefine.NOTIFICATION_ID, notification);
        notificationManager.cancel(CommonDefine.NOTIFICATION_ID);
    }

    public static boolean isPackageAlreadyInstalled(Activity cnt, String pkgName) {
        List<PackageInfo> installedList = cnt.getPackageManager().getInstalledPackages(8192);
        int installedListSize = installedList.size();
        for (int i = 0; i < installedListSize; i++) {
            if (pkgName.equalsIgnoreCase(installedList.get(i).packageName)) {
                return true;
            }
        }
        return false;
    }

    public static void logOffContactAccount() {
        ContactCommon.mContactAccount = "";
    }

    public static void cleanContactAccount(Context cnt) {
        putConfigWithStringValue(cnt, AntiLostCommon.DELETE_CONTACT, "user", "");
        putConfigWithStringValue(cnt, AntiLostCommon.DELETE_CONTACT, "password", "");
        putConfigWithBooleanValue(cnt, AntiLostCommon.DELETE_CONTACT, "remember", false);
        ContactCommon.mContactAccount = "";
    }

    public static String getTimeZone() {
        String c = String.valueOf(Calendar.getInstance().getTimeZone().getRawOffset() / 3600000);
        if (TextUtils.isEmpty(c)) {
            return "";
        }
        return c;
    }

    public static String getMcnc(Context context) {
        String mcnc = ((TelephonyManager) context.getSystemService("phone")).getNetworkOperator();
        if (TextUtils.isEmpty(mcnc)) {
            return "00000";
        }
        return mcnc;
    }

    public static String getNotificationTitle(Context context) {
        return context.getSharedPreferences("netqin", 0).getString("notificationTitle", context.getString(R.string.label_netqin_antivirus_full));
    }

    public static void setNotificationTitle(Context context, String notificationTitle) {
        context.getSharedPreferences("netqin", 0).edit().putString("notificationTitle", notificationTitle).commit();
    }

    public static Long getUserFlow(Context context) {
        return Long.valueOf(context.getSharedPreferences("netqin", 0).getLong("userFlow", 0));
    }

    public static void setUserFlow(Context context, Long userFlow) {
        context.getSharedPreferences("netqin", 0).edit().putLong("userFlow", userFlow.longValue()).commit();
    }

    public static String getSoftUpdateMassage(Context context) {
        return context.getSharedPreferences("netqin", 0).getString("softUpdateMassage", "");
    }

    public static void setSoftUpdateMassage(Context context, String softUpdateMassage) {
        context.getSharedPreferences("netqin", 0).edit().putString("softUpdateMassage", softUpdateMassage).commit();
    }

    public static String getSoftForceUpdateMassage(Context context) {
        return context.getSharedPreferences("netqin", 0).getString("softForceUpdateMassage", "");
    }

    public static void setSoftForceUpdateMassage(Context context, String softForceUpdateMassage) {
        context.getSharedPreferences("netqin", 0).edit().putString("softForceUpdateMassage", softForceUpdateMassage).commit();
    }

    public static boolean getNotificationIconState(Context context) {
        return context.getSharedPreferences("netqin", 0).getBoolean("NotificationIconState", false);
    }

    public static void setNotificationIconState(Context context, Boolean NotificationIconState) {
        context.getSharedPreferences("netqin", 0).edit().putBoolean("NotificationIconState", NotificationIconState.booleanValue()).commit();
    }

    public static boolean getSoftExitState(Context context) {
        return context.getSharedPreferences("netqin", 0).getBoolean("softExitState", false);
    }

    public static void setSoftExitState(Context context, Boolean softExitState) {
        context.getSharedPreferences("netqin", 0).edit().putBoolean("softExitState", softExitState.booleanValue()).commit();
    }

    public static void setCloudScanState(Context context, Boolean cloudScanState) {
        context.getSharedPreferences("netqin", 0).edit().putBoolean("cloudscanstate", cloudScanState.booleanValue()).commit();
    }

    public static boolean getCloudScanState(Context context) {
        return context.getSharedPreferences("netqin", 0).getBoolean("cloudscanstate", false);
    }

    public static void showFlowBarNotification(Context context, long userFlow) {
        if (userFlow != getUserFlow(context).longValue()) {
            String notificationTitle = getNotificationTitle(context);
            Intent intent = Value.notificationintent;
            if (intent == null) {
                intent = new Intent(context, HomeActivity.class);
            }
            if (notificationTitle == null) {
                String notificationTitle2 = context.getString(R.string.label_netqin_antivirus_full);
            }
            if (userFlow != -1) {
                setUserFlow(context, Long.valueOf(userFlow));
            }
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            boolean statusBar = prefs.getBoolean("status_bar", true);
            if (prefs.getBoolean(SettingPreferences.KEY_METER_ONOFF, true)) {
                if (statusBar) {
                    startBar(context, intent);
                }
            } else if (statusBar) {
                showNotificationNoFlowBar(context);
            }
        }
    }

    public static void showFlowBarOrNot(Context context, Intent intent, String notificationTitle, boolean isDanger) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        boolean statusBar = prefs.getBoolean("status_bar", true);
        boolean meterTrafficAlert = prefs.getBoolean(SettingPreferences.KEY_METER_ONOFF, true);
        setNotificationIconState(context, Boolean.valueOf(isDanger));
        setNotificationTitle(context, notificationTitle);
        if (meterTrafficAlert) {
            if (statusBar) {
                startBar(context, intent);
            }
        } else if (statusBar) {
            showNotificationNoFlowBar(context);
        }
    }

    /* JADX INFO: Multiple debug info for r14v1 boolean: [D('isHasDangerApk' boolean), D('intent' android.content.Intent)] */
    /* JADX INFO: Multiple debug info for r3v1 java.lang.String: [D('notificationIcon' int), D('notificationTitle' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r1v2 android.app.NotificationManager: [D('nm' android.app.NotificationManager), D('isHasDangerFile' boolean)] */
    /* JADX INFO: Multiple debug info for r14v7 android.app.PendingIntent: [D('intent' android.content.Intent), D('pIntent' android.app.PendingIntent)] */
    private static void startBar(Context context, Intent intent) {
        String notificationTitle;
        Value.notificationintent = intent;
        boolean isOutFlow = false;
        boolean isDanger = getNotificationIconState(context);
        boolean isHasDangerApk = checkDangerPackageExist(context);
        boolean isHasDangerFile = checkDangerFileExist(context);
        String notificationTitle2 = getNotificationTitle(context);
        if (notificationTitle2 == null) {
            notificationTitle = context.getString(R.string.label_netqin_antivirus_full);
        } else {
            notificationTitle = notificationTitle2;
        }
        if (((double) Integer.valueOf(new StringBuilder(String.valueOf((getUserFlow(context).longValue() / 1024) / 1024)).toString()).intValue()) / Double.parseDouble(SettingPreferences.getFlowTotal(context)) >= 0.9d) {
            isOutFlow = true;
        }
        int notificationIcon = (isDanger || isHasDangerApk || isHasDangerFile) ? R.drawable.icon_danger : R.drawable.icon_normal;
        NotificationManager nm = (NotificationManager) context.getSystemService("notification");
        Notification notification = new Notification(notificationIcon, notificationTitle, System.currentTimeMillis());
        RemoteViews view = new RemoteViews(context.getPackageName(), (int) R.layout.notification_progress);
        Intent intent2 = new Intent("android.intent.action.MAIN");
        intent2.setFlags(335544320);
        intent2.addCategory("android.intent.category.LAUNCHER");
        intent2.setClass(context, HomeActivity.class);
        intent2.setFlags(270532608);
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent2, 134217728);
        notification.icon = notificationIcon;
        notification.flags |= 2;
        notification.flags |= 32;
        view.setImageViewResource(R.id.image, notificationIcon);
        long percent = (getUserFlow(context).longValue() * 100) / ((Long.parseLong(SettingPreferences.getFlowTotal(context)) * 1024) * 1024);
        if (getUserFlow(context).longValue() <= 0 || percent >= 1) {
            view.setProgressBar(R.id.pb, 100, (int) percent, false);
        } else {
            view.setProgressBar(R.id.pb, 100, 2, false);
        }
        view.setTextViewText(R.id.textdesc, notificationTitle);
        if (isDanger) {
            view.setTextColor(R.id.textdesc, -65536);
        } else {
            view.setTextColor(R.id.textdesc, -16777216);
        }
        view.setTextViewText(R.id.flowused, context.getString(R.string.flow_used, Counter.prettyBytes(getUserFlow(context).longValue())));
        view.setTextViewText(R.id.tv, context.getString(R.string.flow_total, SettingPreferences.getFlowTotal(context)));
        if (isOutFlow) {
            view.setTextColor(R.id.flowused, -65536);
            view.setTextColor(R.id.tv, -65536);
        } else {
            view.setTextColor(R.id.flowused, -16777216);
            view.setTextColor(R.id.tv, -16777216);
        }
        notification.contentView = view;
        notification.contentIntent = pIntent;
        nm.notify(Value.NOTIFICATION_FLOW_ID, notification);
    }

    private static void showNotificationNoFlowBar(Context context) {
        int notificationIcon = R.drawable.icon_normal;
        Intent intent = Value.notificationintent;
        boolean isDanger = getNotificationIconState(context);
        boolean isHasDangerApk = checkDangerPackageExist(context);
        boolean isHasDangerFile = checkDangerFileExist(context);
        String notificationTitle = getNotificationTitle(context);
        if (intent == null) {
            intent = new Intent(context, HomeActivity.class);
        }
        if (notificationTitle == null) {
            notificationTitle = context.getString(R.string.label_netqin_antivirus_full);
        }
        if (isDanger || isHasDangerApk || isHasDangerFile) {
            notificationIcon = R.drawable.icon_danger;
        }
        Notification notification = new Notification(notificationIcon, notificationTitle, System.currentTimeMillis());
        NotificationManager nm = (NotificationManager) context.getSystemService("notification");
        RemoteViews view = new RemoteViews(context.getPackageName(), (int) R.layout.notification_no_progress);
        intent.setFlags(335544320);
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent, 134217728);
        notification.icon = notificationIcon;
        notification.flags |= 2;
        notification.flags |= 32;
        view.setImageViewResource(R.id.image, notificationIcon);
        view.setTextViewText(R.id.textdesc, notificationTitle);
        if (isDanger) {
            view.setTextColor(R.id.textdesc, -65536);
        } else {
            view.setTextColor(R.id.textdesc, -16777216);
        }
        notification.contentView = view;
        notification.contentIntent = pIntent;
        nm.notify(Value.NOTIFICATION_FLOW_ID, notification);
    }

    public static void showNotificationNoClear(Context context, Intent intent, String notificationTitle, String notificationDesc, int notificationIcon) {
        Notification notification = new Notification(notificationIcon, notificationTitle, System.currentTimeMillis());
        notification.flags |= 2;
        notification.flags |= 32;
        notification.setLatestEventInfo(context, notificationTitle, notificationDesc, PendingIntent.getActivity(context, 0, intent, 0));
        ((NotificationManager) context.getSystemService("notification")).notify(Value.NOTIFICATION_NOCLEAR_ID, notification);
    }

    public static void showNotificationManualClear(Context context, Intent intent, String notificationTitle, String notificationDesc, int notificationIcon) {
        Notification notification = new Notification(notificationIcon, notificationTitle, System.currentTimeMillis());
        notification.flags |= 16;
        notification.setLatestEventInfo(context, notificationTitle, notificationDesc, PendingIntent.getActivity(context, 0, intent, 0));
        ((NotificationManager) context.getSystemService("notification")).notify(Value.NOTIFICATIONMANUALCLEAR_ID, notification);
    }

    public static void showNotificationAutoClear(Context context, Intent intent, String notificationTitle, String notificationDesc, int notificationIcon) {
        Notification notification = new Notification(notificationIcon, notificationTitle, System.currentTimeMillis());
        NotificationManager nm = (NotificationManager) context.getSystemService("notification");
        notification.setLatestEventInfo(context, notificationTitle, notificationDesc, PendingIntent.getActivity(context, 0, intent, 0));
        nm.notify(Value.NOTIFICATIONAUTOCLEAR_ID, notification);
        nm.cancel(Value.NOTIFICATIONAUTOCLEAR_ID);
    }

    public static void showNotificationDialog(Context context, String notificationTitle, String notificationDesc, int notificationIcon, int notificationID) {
        Notification notification = new Notification(notificationIcon, notificationTitle, System.currentTimeMillis());
        notification.flags |= 16;
        Intent intent = new Intent(context, DialogActivity.class);
        intent.addFlags(536870912);
        intent.addFlags(2);
        intent.putExtra("dialogtitle", notificationTitle);
        intent.putExtra("dialogmessage", notificationDesc);
        intent.putExtra("notificationID", notificationID);
        notification.setLatestEventInfo(context, notificationTitle, context.getString(R.string.notification_desc_viewnow), PendingIntent.getActivity(context, 0, intent, 0));
        ((NotificationManager) context.getSystemService("notification")).notify(notificationID, notification);
    }

    public static void closeNotification(Context context, int notification_id) {
        ((NotificationManager) context.getSystemService("notification")).cancel(notification_id);
    }

    public static String getDate() {
        Calendar ca = Calendar.getInstance();
        int year = ca.get(1);
        int month = ca.get(2);
        int day = ca.get(5);
        int minute = ca.get(12);
        int hour = ca.get(11);
        return String.valueOf(year) + "-" + (month + 1) + "-" + day + " " + hour + ":" + minute + ":" + ca.get(13);
    }

    public static byte[] readFile(String filename) throws IOException {
        File file = new File(filename);
        if (filename == null || filename.equals("")) {
            throw new NullPointerException("无效的文件路径");
        }
        long len = DD02zqNU.Zob2JfG1Yi11hrq(file);
        byte[] bytes = new byte[((int) len)];
        BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file));
        if (((long) bufferedInputStream.read(bytes)) != len) {
            throw new IOException("读取文件不正确");
        }
        bufferedInputStream.close();
        return bytes;
    }

    public static String getLocalIpAddress() {
        try {
            Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
            while (en.hasMoreElements()) {
                Enumeration<InetAddress> enumIpAddr = en.nextElement().getInetAddresses();
                while (true) {
                    if (enumIpAddr.hasMoreElements()) {
                        InetAddress inetAddress = enumIpAddr.nextElement();
                        if (!inetAddress.isLoopbackAddress()) {
                            return inetAddress.getHostAddress().toString();
                        }
                    }
                }
            }
        } catch (SocketException e) {
        }
        return null;
    }

    public static String getVersionCode(String packageName, Context context) {
        String versionCode = null;
        try {
            versionCode = new StringBuilder(String.valueOf(DD02zqNU.DLLIxskak(context.getPackageManager(), packageName, 1).versionCode)).toString();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        if (TextUtils.isEmpty(versionCode)) {
            return "1";
        }
        return versionCode;
    }

    public static String getVersionName(String packageName, Context context) {
        String versionName = null;
        try {
            versionName = DD02zqNU.DLLIxskak(context.getPackageManager(), packageName, 1).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        if (TextUtils.isEmpty(versionName)) {
            return "nq.1";
        }
        return versionName;
    }

    public static void deleteFile(String fileName) {
        File file = new File(fileName);
        if (file.exists()) {
            file.delete();
        }
    }

    public static boolean IsShortCutExist(Context ctx, String title) {
        boolean b = false;
        try {
            Cursor c = ctx.getContentResolver().query(Uri.parse("content://com.android.launcher.settings/favorites"), null, "itemType=1 AND title='" + title + "'", null, null);
            if (c != null) {
                if (c.getCount() > 0) {
                    b = true;
                }
                c.close();
            }
        } catch (Exception e) {
        }
        return b;
    }

    public static boolean isServicesRunning(Context context, String serviceName) {
        boolean isRunning = false;
        for (ActivityManager.RunningServiceInfo si : ((ActivityManager) context.getSystemService("activity")).getRunningServices(10000)) {
            if (si.service.getPackageName().toString().compareToIgnoreCase(context.getPackageName()) == 0 && serviceName.compareToIgnoreCase(si.service.getClassName()) == 0) {
                isRunning = true;
            }
        }
        return isRunning;
    }

    public static void saveDangerPackageInfo(Context context, String packageName) {
        if (packageName != null) {
            SharedPreferences spf = context.getSharedPreferences("dangerpackage", 0);
            spf.edit().putString("packagelist", String.valueOf(spf.getString("packagelist", "")) + "#" + packageName).commit();
        }
    }

    public static String[] getDangerPackageList(Context context) {
        String[] pkgList = context.getSharedPreferences("dangerpackage", 0).getString("packagelist", "").split("#");
        for (String str : pkgList) {
        }
        return pkgList;
    }

    public static void saveDangerFileInfo(Context context, String fileName) {
        if (fileName != null) {
            SharedPreferences spf = context.getSharedPreferences("dangerfile", 0);
            spf.edit().putString("dangerfile", String.valueOf(fileName) + "#" + spf.getString("dangerfile", "")).commit();
        }
    }

    public static String[] getDangerFileList(Context context, int maxCount) {
        String[] newFileList;
        String[] fileList = context.getSharedPreferences("dangerfile", 0).getString("dangerfile", "").split("#");
        if (fileList.length < maxCount) {
            newFileList = new String[fileList.length];
            for (int i = 0; i <= fileList.length - 1; i++) {
                newFileList[i] = fileList[i];
            }
        } else {
            newFileList = new String[maxCount];
            for (int i2 = 0; i2 <= maxCount - 1; i2++) {
                newFileList[i2] = fileList[i2];
            }
        }
        return newFileList;
    }

    public static void clearEditorBySpfFileName(Context context, String spfFileName) {
        context.getSharedPreferences(spfFileName, 0).edit().clear().commit();
    }

    public static boolean isFirstRun(Context context) {
        return context.getSharedPreferences("netqin", 0).getBoolean("IsFirstRun", true);
    }

    public static boolean hasSoft(Context context, String packName) {
        try {
            int i = DD02zqNU.DLLIxskak(context.getPackageManager(), packName, 0).versionCode;
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean checkDangerPackageExist(Context context) {
        String[] pkgList = getDangerPackageList(context);
        boolean isPackageExist = false;
        if (pkgList == null || pkgList.length < 0) {
            return false;
        }
        PackageManager pm = context.getPackageManager();
        for (String packName : pkgList) {
            if (packName.trim().length() > 0) {
                try {
                    ApplicationInfo applicationInfo = pm.getApplicationInfo(packName, 1);
                    isPackageExist = true;
                } catch (PackageManager.NameNotFoundException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return isPackageExist;
    }

    public static boolean checkDangerFileExist(Context context) {
        int i = 0;
        String[] fileList = getDangerFileList(context, 20);
        boolean isPackageExist = false;
        if (fileList == null || fileList.length < 0) {
            return false;
        }
        int length = fileList.length;
        while (true) {
            if (i >= length) {
                break;
            } else if (new File(fileList[i]).exists()) {
                isPackageExist = true;
                break;
            } else {
                i++;
            }
        }
        return isPackageExist;
    }

    public static boolean getAutoStartTag(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("auto_start_protect", true);
    }

    public static void closeServiceNotification(Activity act) {
        if (act != null) {
            ((ActivityManager) act.getSystemService("activity")).restartPackage("com.netqin.antivirusgm20");
            if (HomeActivity.mActAntiVirusHome != null) {
                if (act != HomeActivity.mActAntiVirusHome) {
                    act.finish();
                }
                Intent i = new Intent("android.intent.action.RUN");
                i.setClass(act.getApplicationContext(), MonitorService.class);
                act.stopService(i);
                Intent i2 = new Intent("android.intent.action.RUN");
                i2.setClass(act.getApplicationContext(), SilentCloudScanService.class);
                act.stopService(i2);
                Intent i3 = new Intent("android.intent.action.RUN");
                i3.setClass(act.getApplicationContext(), BlockService.class);
                act.stopService(i3);
                Intent i4 = new Intent("android.intent.action.RUN");
                i4.setClass(act.getApplicationContext(), ControlService.class);
                act.stopService(i4);
                Intent i5 = new Intent("android.intent.action.RUN");
                i5.setClass(act.getApplicationContext(), AppService.class);
                act.stopService(i5);
                Intent i6 = new Intent("android.intent.action.RUN");
                i6.setClass(act.getApplicationContext(), CloudDataService.class);
                act.stopService(i6);
                Intent i7 = new Intent("android.intent.action.RUN");
                i7.setClass(act.getApplicationContext(), PeriodScanService.class);
                act.stopService(i7);
                Intent i8 = new Intent("android.intent.action.RUN");
                i8.setClass(act.getApplicationContext(), NetMeterService.class);
                act.stopService(i8);
                Intent i9 = new Intent("android.intent.action.RUN");
                i9.setClass(act.getApplicationContext(), TaskManagerService.class);
                act.stopService(i9);
                Intent i10 = new Intent("android.intent.action.RUN");
                i10.setClass(act.getApplicationContext(), AVService.class);
                act.stopService(i10);
                Intent i11 = new Intent("android.intent.action.RUN");
                i11.setClass(act.getApplicationContext(), AntiLostService.class);
                act.stopService(i11);
                closeNotification(act, Value.NOTIFICATION_FLOW_ID);
                closeNotification(act, Value.NOTIFICATIONMANUALCLEAR_ID);
                closeNotification(act, Value.NOTIFICATION_NOCLEAR_ID);
                closeNotification(act, 111);
            }
        }
    }
}
