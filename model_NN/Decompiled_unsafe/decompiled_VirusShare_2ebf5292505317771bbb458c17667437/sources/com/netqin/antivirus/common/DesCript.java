package com.netqin.antivirus.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.Key;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

public abstract class DesCript extends Coder {
    public static final String ALGORITHM = "DES";
    static final int BUFFERLEN = 256;
    static final byte[] KEYVALUE = "6^)(9-p35a%3#4S!4S0)$Yt%^&5(j.g^&o(*0)$Yv!#O@6GpG@=+3j.&6^)(0-=1".getBytes();
    static Key k = toKey(keyByte);
    static final byte[] keyByte = {-74, 93, -99, -51, 13, 4, -38, 97};
    static int keylen = KEYVALUE.length;

    private static Key toKey(byte[] key) {
        try {
            return SecretKeyFactory.getInstance(ALGORITHM).generateSecret(new DESKeySpec(key));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static byte[] decrypt(byte[] data) throws Exception {
        Cipher cipher = Cipher.getInstance("DES/ECB/NoPadding");
        cipher.init(2, k);
        return cipher.doFinal(data);
    }

    public static byte[] encrypt(byte[] data) throws Exception {
        Cipher cipher = Cipher.getInstance("DES/ECB/NoPadding");
        cipher.init(1, k);
        return cipher.doFinal(data);
    }

    public static String initKey() throws Exception {
        return initKey(null);
    }

    public static String initKey(String seed) throws Exception {
        SecureRandom secureRandom;
        if (seed != null) {
            secureRandom = new SecureRandom(decryptBASE64(seed));
        } else {
            secureRandom = new SecureRandom();
        }
        KeyGenerator kg = KeyGenerator.getInstance(ALGORITHM);
        kg.init(secureRandom);
        return encryptBASE64(kg.generateKey().getEncoded());
    }

    public static void encryptFile(String oldFile, String newFile) throws Exception {
        FileInputStream in = new FileInputStream(oldFile);
        File file = new File(newFile);
        if (!file.exists()) {
            file.createNewFile();
        }
        FileOutputStream out = new FileOutputStream(file);
        int pos = 0;
        byte[] buffer = new byte[BUFFERLEN];
        while (true) {
            int c = in.read(buffer, 0, BUFFERLEN);
            if (c == -1) {
                in.close();
                out.close();
                return;
            } else if (c == BUFFERLEN) {
                buffer = encrypt(buffer);
                out.write(buffer);
            } else {
                for (int i = 0; i < c; i++) {
                    buffer[i] = (byte) (buffer[i] ^ KEYVALUE[pos]);
                    pos++;
                    if (pos == keylen) {
                        pos = 0;
                    }
                }
                out.write(buffer, 0, c);
            }
        }
    }

    public static void decryptFile(String oldFile, String newFile) throws Exception {
        FileInputStream in = new FileInputStream(oldFile);
        File file = new File(newFile);
        if (!file.exists()) {
            file.createNewFile();
        }
        FileOutputStream out = new FileOutputStream(file);
        int pos = 0;
        byte[] buffer = new byte[BUFFERLEN];
        while (true) {
            int c = in.read(buffer, 0, BUFFERLEN);
            if (c == -1) {
                in.close();
                out.close();
                return;
            } else if (c == BUFFERLEN) {
                buffer = decrypt(buffer);
                out.write(buffer);
            } else {
                for (int i = 0; i < c; i++) {
                    buffer[i] = (byte) (buffer[i] ^ KEYVALUE[pos]);
                    pos++;
                    if (pos == keylen) {
                        pos = 0;
                    }
                }
                out.write(buffer, 0, c);
            }
        }
    }
}
