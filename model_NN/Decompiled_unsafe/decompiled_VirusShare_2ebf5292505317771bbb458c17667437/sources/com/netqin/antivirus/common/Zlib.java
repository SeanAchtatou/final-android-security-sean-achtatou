package com.netqin.antivirus.common;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

public class Zlib {
    public static void ZipCompress(String sourcePath, String destPath) throws Exception {
        FileInputStream fis = new FileInputStream(sourcePath);
        FileOutputStream fos = new FileOutputStream(destPath);
        DeflaterOutputStream zos = new DeflaterOutputStream(fos);
        byte[] buff = new byte[1024];
        while (true) {
            int num = fis.read(buff);
            if (num == -1) {
                fis.close();
                zos.close();
                fos.close();
                return;
            }
            zos.write(buff, 0, num);
        }
    }

    public static void ZipDecompress(String sourcePath, String destPath) throws Exception {
        FileInputStream fis = new FileInputStream(sourcePath);
        InflaterInputStream zis = new InflaterInputStream(fis);
        FileOutputStream fos = new FileOutputStream(destPath);
        byte[] buff = new byte[1024];
        while (true) {
            int num = zis.read(buff);
            if (num == -1) {
                zis.close();
                fis.close();
                fos.close();
                return;
            }
            fos.write(buff, 0, num);
        }
    }
}
