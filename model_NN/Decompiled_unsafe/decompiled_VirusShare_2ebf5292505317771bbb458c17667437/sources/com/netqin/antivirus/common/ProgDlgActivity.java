package com.netqin.antivirus.common;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.DisplayMetrics;

public class ProgDlgActivity extends Activity {
    protected boolean mActivityVisible = true;
    /* access modifiers changed from: protected */
    public final Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    if (ProgDlgActivity.this.mActivityVisible) {
                        ProgDlgActivity.this.setFixedScreenLayout();
                        ProgDlgActivity.this.mProgressDlg.show();
                        break;
                    }
                    break;
                case 3:
                    if (ProgDlgActivity.this.mActivityVisible && ProgDlgActivity.this.mProgressDlg != null) {
                        if (msg.obj != null) {
                            ProgDlgActivity.this.mProgressText = (String) msg.obj;
                        }
                        ProgDlgActivity.this.mProgressDlg.setMessage(ProgDlgActivity.this.mProgressText);
                        break;
                    }
                case 4:
                    if (ProgDlgActivity.this.mProgressDlg != null && ProgDlgActivity.this.mActivityVisible) {
                        ProgDlgActivity.this.mProgressDlg.cancel();
                    }
                    ProgDlgActivity.this.mProgressDlg = null;
                    if (ProgDlgActivity.this.mActivityVisible) {
                        ProgDlgActivity.this.cancelFixedScreenLayout();
                        break;
                    }
                    break;
                case 5:
                    ProgDlgActivity.this.progressDialogExOnStop();
                    break;
                case 6:
                    if (ProgDlgActivity.this.mActivityVisible && ProgDlgActivity.this.mProgTextDialog != null) {
                        ProgDlgActivity.this.setFixedScreenLayout();
                        ProgDlgActivity.this.mProgTextDialog.show();
                        break;
                    }
                case 7:
                    if (ProgDlgActivity.this.mActivityVisible && ProgDlgActivity.this.mProgTextDialog != null) {
                        ProgDlgActivity.this.mProgTextDialog.getProgressBar().setMax(100);
                        ProgDlgActivity.this.mProgTextDialog.getProgressBar().setProgress(0);
                        ProgDlgActivity.this.mProgTextLeft = "";
                        ProgDlgActivity.this.mProgTextDialog.getLeftTextView().setText(ProgDlgActivity.this.mProgTextLeft);
                        ProgDlgActivity.this.mProgTextRight = "";
                        ProgDlgActivity.this.mProgTextDialog.getRightTextView().setText(ProgDlgActivity.this.mProgTextRight);
                        break;
                    }
                case 8:
                    if (ProgDlgActivity.this.mActivityVisible && ProgDlgActivity.this.mProgTextDialog != null) {
                        ProgDlgActivity.this.mProgTextDialog.getProgressBar().setMax(msg.arg1);
                        ProgDlgActivity.this.mProgTextDialog.getProgressBar().setProgress(0);
                        if (!TextUtils.isEmpty(ProgDlgActivity.this.mProgTextLeft)) {
                            ProgDlgActivity.this.mProgTextDialog.getLeftTextView().setText(ProgDlgActivity.this.mProgTextLeft);
                        }
                        if (!TextUtils.isEmpty(ProgDlgActivity.this.mProgTextRight)) {
                            ProgDlgActivity.this.mProgTextDialog.getRightTextView().setText(ProgDlgActivity.this.mProgTextRight);
                            break;
                        }
                    }
                    break;
                case 9:
                    if (ProgDlgActivity.this.mActivityVisible && ProgDlgActivity.this.mProgTextDialog != null) {
                        ProgDlgActivity.this.mProgTextDialog.getProgressBar().setProgress(msg.arg1);
                        if (!TextUtils.isEmpty(ProgDlgActivity.this.mProgTextLeft)) {
                            ProgDlgActivity.this.mProgTextDialog.getLeftTextView().setText(ProgDlgActivity.this.mProgTextLeft);
                        }
                        if (!TextUtils.isEmpty(ProgDlgActivity.this.mProgTextRight)) {
                            ProgDlgActivity.this.mProgTextDialog.getRightTextView().setText(ProgDlgActivity.this.mProgTextRight);
                            break;
                        }
                    }
                    break;
                case 10:
                    if (ProgDlgActivity.this.mProgTextDialog != null && ProgDlgActivity.this.mActivityVisible) {
                        ProgDlgActivity.this.mProgTextDialog.cancel();
                    }
                    ProgDlgActivity.this.mProgTextDialog = null;
                    ProgDlgActivity.this.cancelFixedScreenLayout();
                    break;
                case 11:
                    ProgDlgActivity.this.progressTextDialogOnStop();
                    break;
                case 12:
                    ProgDlgActivity.this.childFunctionCall(msg);
                    break;
                case 13:
                    ProgDlgActivity.this.processAppMsg(msg);
                    break;
            }
            super.handleMessage(msg);
        }
    };
    protected int mProTextProgress = 0;
    protected ProgressTextDialog mProgTextDialog = null;
    protected String mProgTextLeft = "";
    protected String mProgTextRight = "";
    protected ProgressDialogEx mProgressDlg = null;
    protected String mProgressText = "";
    private int mScreenOrientation = -1;

    /* access modifiers changed from: protected */
    public void setFixedScreenLayout() {
        this.mScreenOrientation = getRequestedOrientation();
        if (this.mScreenOrientation == -1) {
            DisplayMetrics dm = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(dm);
            if (dm.widthPixels <= dm.heightPixels) {
                setRequestedOrientation(1);
            } else {
                setRequestedOrientation(0);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void cancelFixedScreenLayout() {
        if (this.mScreenOrientation == -1) {
            setRequestedOrientation(this.mScreenOrientation);
        }
    }

    /* access modifiers changed from: protected */
    public void childFunctionCall(Message msg) {
    }

    /* access modifiers changed from: protected */
    public void progressTextDialogOnStop() {
    }

    /* access modifiers changed from: protected */
    public void progressDialogExOnStop() {
    }

    /* access modifiers changed from: protected */
    public void processAppMsg(Message msg) {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mActivityVisible = true;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        this.mActivityVisible = true;
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.mActivityVisible = false;
        super.onDestroy();
    }
}
