package com.netqin.antivirus.common;

import android.content.Context;
import android.content.SharedPreferences;
import com.netqin.antivirus.cloud.model.xml.XmlUtils;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

public class CloudPassage {
    public static final String CHAIN_ESTABLISH_CONN_USETIME = "-1";
    public static final String CLOUD_APN = "-1";
    public static final String CLOUD_CANCEL_TAG = "-1";
    public static final String CLOUD_CANCEL_TYPE_INITIATIVE = "1";
    public static final String CLOUD_CANCEL_TYPE_NONE = "0";
    public static final String CLOUD_END_TIME = "-1";
    public static final String CLOUD_SCAN_ID = "-1";
    public static final String CLOUD_SCAN_TYPE = "-1";
    public static final String CLOUD_START_TIME = "-1";
    public static final String CLOUD_STATUS_CODE = "-1";
    public static final String CONVEY_POST_USETIME = "-1";
    public static final String LOCAL_SCANNING_USETIME = "-1";
    public static final String REPLY_XML_RECEIVE_USETIME = "-1";
    public static final int SCAN_TYPE_FULL = 2;
    public static final int SCAN_TYPE_INSTALL = 4;
    public static final int SCAN_TYPE_OTHER = 5;
    public static final int SCAN_TYPE_PERIOD = 3;
    public static final int SCAN_TYPE_QUICK = 1;
    public static final String WAIT_REPLY_USETIME = "-1";
    private boolean debug = false;
    private Context mContext = null;
    private SharedPreferences sp;

    public CloudPassage(Context context) {
        this.mContext = context;
        this.sp = this.mContext.getSharedPreferences("CloudPassageLog", 0);
    }

    public void clearEditor() {
        this.sp.edit().clear().commit();
    }

    public void setScan_id(String scan_id) {
        this.sp.edit().putString("scan_id", scan_id).commit();
    }

    public String getScan_id() {
        return this.sp.getString("scan_id", "-1");
    }

    public void setApn(String apn) {
        this.sp.edit().putString(XmlUtils.LABEL_MOBILEINFO_APN, apn).commit();
    }

    public String getApn() {
        return this.sp.getString(XmlUtils.LABEL_MOBILEINFO_APN, "-1");
    }

    public void setCloudStartTime(String cloud_start_time) {
        this.sp.edit().putString("cloud_start_time", cloud_start_time).commit();
    }

    public String getCloudStartTime() {
        return this.sp.getString("cloud_start_time", "-1");
    }

    public void setLocalScanningUsetime(String local_scanning_usetime) {
        this.sp.edit().putString("local_scanning_usetime", local_scanning_usetime).commit();
    }

    public String getLocalScanningUsetime() {
        return this.sp.getString("local_scanning_usetime", "-1");
    }

    public void setChainEstablishConnUsetime(String chain_establish_conn_usetime) {
        this.sp.edit().putString("chain_establish_conn_usetime", chain_establish_conn_usetime).commit();
    }

    public String getChainEstablishConnUsetime() {
        return this.sp.getString("chain_establish_conn_usetime", "-1");
    }

    public void setConveyPostUsetime(String convey_post_usetime) {
        this.sp.edit().putString("convey_post_usetime", convey_post_usetime).commit();
    }

    public String getConveyPostUsetime() {
        return this.sp.getString("convey_post_usetime", "-1");
    }

    public void setWaitReplyUsetime(String wait_reply_usetime) {
        this.sp.edit().putString("wait_reply_usetime", wait_reply_usetime).commit();
    }

    public String getWaitReplyUsetime() {
        return this.sp.getString("wait_reply_usetime", "-1");
    }

    public void setReplyXmlReceiveUsetime(String reply_xml_receive_usetime) {
        this.sp.edit().putString("reply_xml_receive_usetime", reply_xml_receive_usetime).commit();
    }

    public String getReplyXmlReceiveUsetime() {
        return this.sp.getString("reply_xml_receive_usetime", "-1");
    }

    public void setCloudStatus(String cloud_status_code) {
        this.sp.edit().putString("cloud_status_code", cloud_status_code).commit();
    }

    public String getCloudStatus() {
        return this.sp.getString("cloud_status_code", "0");
    }

    public void setScanType(String type) {
        this.sp.edit().putString("scan_type", type).commit();
    }

    public String getScanType() {
        return this.sp.getString("scan_type", "0");
    }

    public void setScanCancelTag(String cloud_cancel_tag) {
        this.sp.edit().putString("cloud_cancel_tag", cloud_cancel_tag).commit();
    }

    public String getScanCancelTag() {
        return this.sp.getString("cloud_cancel_tag", "0");
    }

    public void setCloudEndTime(String cloud_end_time) {
        this.sp.edit().putString("cloud_end_time", cloud_end_time).commit();
    }

    public String getCloudEndTime() {
        return this.sp.getString("cloud_end_time", "-1");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
      ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException} */
    public void writeCloudLogToFile() {
        StringBuffer performance_datBuffer = new StringBuffer();
        performance_datBuffer.append('\"');
        performance_datBuffer.append(getScan_id());
        performance_datBuffer.append('\"');
        performance_datBuffer.append(",");
        performance_datBuffer.append('\"');
        performance_datBuffer.append(getApn());
        performance_datBuffer.append('\"');
        performance_datBuffer.append(",");
        performance_datBuffer.append('\"');
        performance_datBuffer.append(getCloudStartTime());
        performance_datBuffer.append('\"');
        performance_datBuffer.append(",");
        performance_datBuffer.append('\"');
        performance_datBuffer.append(getLocalScanningUsetime());
        performance_datBuffer.append('\"');
        performance_datBuffer.append(",");
        performance_datBuffer.append('\"');
        performance_datBuffer.append(getChainEstablishConnUsetime());
        performance_datBuffer.append('\"');
        performance_datBuffer.append(",");
        performance_datBuffer.append('\"');
        performance_datBuffer.append(getConveyPostUsetime());
        performance_datBuffer.append('\"');
        performance_datBuffer.append(",");
        performance_datBuffer.append('\"');
        performance_datBuffer.append(getWaitReplyUsetime());
        performance_datBuffer.append('\"');
        performance_datBuffer.append(",");
        performance_datBuffer.append('\"');
        performance_datBuffer.append(getWaitReplyUsetime());
        performance_datBuffer.append('\"');
        performance_datBuffer.append(",");
        performance_datBuffer.append('\"');
        performance_datBuffer.append(getCloudStatus());
        performance_datBuffer.append('\"');
        performance_datBuffer.append(",");
        performance_datBuffer.append('\"');
        performance_datBuffer.append(getScanCancelTag());
        performance_datBuffer.append('\"');
        performance_datBuffer.append(",");
        performance_datBuffer.append('\"');
        performance_datBuffer.append(getScanType());
        performance_datBuffer.append('\"');
        performance_datBuffer.append(",");
        performance_datBuffer.append('\"');
        performance_datBuffer.append(getCloudEndTime());
        performance_datBuffer.append('\"');
        performance_datBuffer.append(10);
        String performanceStr = performance_datBuffer.toString();
        File fileInst = new File(this.mContext.getFilesDir() + XmlUtils.PERFORMANCE_DAT);
        try {
            if (!fileInst.exists()) {
                fileInst.createNewFile();
            }
            FileWriter writer = new FileWriter(this.mContext.getFilesDir() + XmlUtils.PERFORMANCE_DAT, true);
            writer.write(new String(performanceStr.getBytes()));
            writer.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        if (this.debug) {
            MaliciousWebsite maliciousWebsite = new MaliciousWebsite(this.mContext);
            maliciousWebsite.setMaliciousWebsiteStartTime("2011-3-30 10:30:10");
            maliciousWebsite.setVisitUrl("www.netqin.com");
            maliciousWebsite.setVisitUrlUsetime("123");
            maliciousWebsite.setVisitNqServerUsetime("123");
            maliciousWebsite.setClientReceiveNqServerUsetime("132");
            maliciousWebsite.writeUrlLogToFile();
        }
    }
}
