package com.netqin.antivirus.common;

import android.content.Context;
import com.netqin.antivirus.cloud.model.DataUtils;

public class CommonUtils {
    public static boolean putConfigWithStringValue(Context cnt, String fileName, String itemName, String itemValue) {
        cnt.getSharedPreferences(fileName, 0).edit().putString(itemName, DataUtils.encryptForXml(DataUtils.ENCRYPT_KEY, itemValue)).commit();
        return true;
    }

    public static String getConfigWithStringValue(Context cnt, String fileName, String itemName, String defaultValue) {
        return DataUtils.decryptForXml(DataUtils.ENCRYPT_KEY, cnt.getSharedPreferences(fileName, 0).getString(itemName, DataUtils.encryptForXml(DataUtils.ENCRYPT_KEY, defaultValue)));
    }
}
