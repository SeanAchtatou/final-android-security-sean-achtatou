package com.netqin.antivirus.common;

import android.app.Activity;
import android.content.Intent;

public class ClassStackManager {
    private static Activity mPrevActivity = null;

    public static void startActivity(Activity packageActivity, Class<?> cls) {
        String className = cls.getSimpleName();
        if (mPrevActivity != null) {
            String name = mPrevActivity.getLocalClassName();
            int index = name.lastIndexOf(".");
            if (index >= 0) {
                name = name.substring(index + 1);
            }
            if (name.equalsIgnoreCase(className)) {
                mPrevActivity.finish();
            }
            mPrevActivity = null;
        }
        Intent intent = new Intent(packageActivity, cls);
        mPrevActivity = packageActivity;
        packageActivity.startActivity(intent);
    }

    public static void startActivityWithBooleanExtra(Activity packageActivity, Class<?> cls, String extraName, boolean extraValue) {
        String className = cls.getSimpleName();
        if (mPrevActivity != null) {
            String name = mPrevActivity.getLocalClassName();
            int index = name.lastIndexOf(".");
            if (index >= 0) {
                name = name.substring(index + 1);
            }
            if (name.equalsIgnoreCase(className)) {
                mPrevActivity.finish();
            }
            mPrevActivity = null;
        }
        Intent intent = new Intent(packageActivity, cls);
        intent.putExtra(extraName, extraValue);
        mPrevActivity = packageActivity;
        packageActivity.startActivity(intent);
    }
}
