package com.netqin.antivirus;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.telephony.TelephonyManager;
import com.netqin.antivirus.antilost.SmsHandler;
import com.netqin.antivirus.cloud.model.xml.XmlUtils;
import com.netqin.antivirus.cloud.view.CloudListener;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.contact.vcard.VCardConfig;
import com.netqin.antivirus.util.MimeUtils;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.Calendar;
import java.util.Locale;

public class NqUtil {
    private static char[] alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".toCharArray();
    private static String base64_alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    private static byte[] codes = new byte[256];

    static {
        for (int i = 0; i < 256; i++) {
            codes[i] = -1;
        }
        for (int i2 = 65; i2 <= 90; i2++) {
            codes[i2] = (byte) (i2 - 65);
        }
        for (int i3 = 97; i3 <= 122; i3++) {
            codes[i3] = (byte) ((i3 + 26) - 97);
        }
        for (int i4 = 48; i4 <= 57; i4++) {
            codes[i4] = (byte) ((i4 + 52) - 48);
        }
        codes[43] = 62;
        codes[47] = 63;
    }

    public static String getNumber(String s) {
        String temp = "";
        if (!s.equals("")) {
            for (int i = 0; i < s.length(); i++) {
                char x = s.charAt(i);
                if (Character.isDigit(x)) {
                    temp = String.valueOf(temp) + x;
                }
            }
        }
        return temp;
    }

    public static byte[] getStringANSIBytes(String s) {
        try {
            return s.getBytes("ASCII");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static byte[] getStringUnicodeBytes(String s) {
        try {
            return s.getBytes("Unicode");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getLastEleven(String s) {
        if (s.length() <= 11) {
            return s;
        }
        return s.substring(s.length() - 11, s.length());
    }

    public static String[] getStringsFromText(String text) {
        return text.split(",");
    }

    public static Integer[] getIntsFromText(String text) {
        if (text != null && !text.equals("")) {
            String[] s = text.split(",");
            if (s.length > 0) {
                Integer[] ids = new Integer[s.length];
                for (int i = 0; i < s.length; i++) {
                    ids[i] = new Integer(s[i]);
                }
                return ids;
            }
        }
        return null;
    }

    public static String base64Encode(byte[] data) {
        int i;
        char[] out = new char[(((data.length + 2) / 3) * 4)];
        int i2 = 0;
        int index = 0;
        while (i2 < data.length) {
            boolean quad = false;
            boolean trip = false;
            int val = (data[i2] & 255) << 8;
            if (i2 + 1 < data.length) {
                val |= data[i2 + 1] & 255;
                trip = true;
            }
            int val2 = val << 8;
            if (i2 + 2 < data.length) {
                val2 |= data[i2 + 2] & 255;
                quad = true;
            }
            out[index + 3] = alphabet[quad ? val2 & 63 : 64];
            int val3 = val2 >> 6;
            int i3 = index + 2;
            char[] cArr = alphabet;
            if (trip) {
                i = val3 & 63;
            } else {
                i = 64;
            }
            out[i3] = cArr[i];
            int val4 = val3 >> 6;
            out[index + 1] = alphabet[val4 & 63];
            out[index + 0] = alphabet[(val4 >> 6) & 63];
            i2 += 3;
            index += 4;
        }
        return new String(out);
    }

    public static byte[] base64Decode(String str) {
        char[] data = str.toCharArray();
        int len = ((data.length + 3) / 4) * 3;
        if (data.length > 0 && data[data.length - 1] == '=') {
            len--;
        }
        if (data.length > 1 && data[data.length - 2] == '=') {
            len--;
        }
        byte[] out = new byte[len];
        int shift = 0;
        int accum = 0;
        int index = 0;
        for (char c : data) {
            byte b = codes[c & 255];
            if (b >= 0) {
                shift += 6;
                accum = (accum << 6) | b;
                if (shift >= 8) {
                    shift -= 8;
                    out[index] = (byte) ((accum >> shift) & 255);
                    index++;
                }
            }
        }
        if (index == out.length) {
            return out;
        }
        throw new Error("miscalculated data length!");
    }

    public static String getIMEI(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
    }

    public static String getIMSI(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getSubscriberId();
    }

    public static Cursor getApns(Context context) {
        return context.getContentResolver().query(Uri.parse("content://telephony/carriers"), null, "current = 1", null, null);
    }

    public static String getApnNameById(Context context, long id) {
        String name = "";
        Cursor c = context.getContentResolver().query(Uri.parse("content://telephony/carriers"), null, "_id = " + id, null, null);
        if (c != null) {
            c.moveToFirst();
            try {
                name = c.getString(c.getColumnIndex("name"));
            } catch (CursorIndexOutOfBoundsException e) {
                name = "noapn";
            }
            c.close();
        }
        return name;
    }

    public static Cursor getCurrentApn(Context context) {
        return context.getContentResolver().query(Uri.parse("content://telephony/carriers/preferapn"), null, null, null, null);
    }

    public static long getCurrentApnId(Context context) {
        Cursor c = context.getContentResolver().query(Uri.parse("content://telephony/carriers/preferapn"), null, null, null, null);
        if (c == null || c.getCount() <= 0) {
            return -1;
        }
        c.moveToFirst();
        long _id = c.getLong(c.getColumnIndex(SmsHandler.ROWID));
        c.close();
        return _id;
    }

    public static String getCurrentApnName(Context context) {
        Cursor c = context.getContentResolver().query(Uri.parse("content://telephony/carriers/preferapn"), null, null, null, null);
        if (c == null || c.getCount() <= 0) {
            return "";
        }
        c.moveToFirst();
        String _id = c.getString(c.getColumnIndex(XmlUtils.LABEL_MOBILEINFO_APN));
        c.close();
        return _id;
    }

    public static void setCurrentApn(Context context, long apnId) {
        ContentValues values = new ContentValues();
        values.put("apn_id", Long.valueOf(apnId));
        context.getContentResolver().update(Uri.parse("content://telephony/carriers/preferapn"), values, null, null);
    }

    public static Proxy getApnProxy(Context context) {
        if (((ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(1).isConnected()) {
            return null;
        }
        Cursor c = getCurrentApn(context);
        if (c.getCount() > 0) {
            c.moveToFirst();
            long j = c.getLong(c.getColumnIndex(SmsHandler.ROWID));
            String proxy = c.getString(c.getColumnIndex("proxy"));
            String port = c.getString(c.getColumnIndex("port"));
            c.close();
            if (proxy != null && !proxy.equals("")) {
                if (port == null || port.equals("")) {
                    return null;
                }
                return new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxy, Integer.valueOf(port).intValue()));
            }
        }
        c.close();
        return null;
    }

    public static long getMilliSecondByDay(int days) {
        return (long) (days * 24 * 60 * 60 * CloudListener.CLOUD_SCAN_SUCCEES);
    }

    public static long getMilliSecondByHourAndMins(int hour, int min) {
        return (long) ((hour * 60 * 60 * CloudListener.CLOUD_SCAN_SUCCEES) + (min * 60 * CloudListener.CLOUD_SCAN_SUCCEES));
    }

    public static void installPackage(Context ctx, String apkPath) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setFlags(VCardConfig.FLAG_USE_QP_TO_PRIMARY_PROPERTIES);
        intent.setDataAndType(Uri.fromFile(new File(apkPath)), MimeUtils.MIME_APPLICATION_APK);
        ctx.startActivity(intent);
    }

    public static String getStringByCalendar(Calendar calendar) {
        String month;
        String day;
        String hour;
        String minite;
        int m = calendar.get(2) + 1;
        if (m >= 10) {
            month = new StringBuilder().append(m).toString();
        } else {
            month = "0" + m;
        }
        int d = calendar.get(5);
        if (d >= 10) {
            day = String.valueOf("") + d;
        } else {
            day = "0" + d;
        }
        int h = calendar.get(11);
        if (h < 10) {
            hour = "0" + h;
        } else {
            hour = new StringBuilder().append(h).toString();
        }
        int mi = calendar.get(12);
        if (mi < 10) {
            minite = "0" + mi;
        } else {
            minite = new StringBuilder().append(mi).toString();
        }
        return String.valueOf(calendar.get(1)) + month + day + hour + minite;
    }

    public static String getCurrentLanguages() {
        Locale l = Locale.getDefault();
        if (l.equals(Locale.CHINA) || l.equals(Locale.CHINESE)) {
            return Value.SoftLanguage;
        }
        return "1";
    }

    public static String getCurrentTime() {
        return getStringByCalendar(Calendar.getInstance());
    }

    public static void clickAdviceFeedback(Context context) {
        context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(String.valueOf("http://www.netqin.com/feedback/report.jsp") + "?" + CommonMethod.getUploadConfigFeedBack(context))));
    }
}
