package com.netqin.antivirus.antimallink;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import com.netqin.antivirus.cloud.model.xml.XmlUtils;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MalDataBase {
    private static final String BLACK_TABLE_NAME = "blackurl";
    public static String[] testee = {"www.bocjs.tk", "www.bocw.hk", "www.cctv3w622.com", "www.zat67y.com", "www.cctv3ca.tk", "www.ccvtk.com", "www.cctv3dd.tk", "www.dstv3.com", "www.zvt42y.com", "www.zvt64y.com", "www.zvt86y.com", "www.zvt94y.com", "www.ztt78y.com", "www.zjdtt.com", "www.zat48y.com", "www.xty28t.com", "www.cntva2.com", "www.csav3.com", "www.ctv2010y.in", "www.ctv611.net", "www.fcw2010.in", "www.fcxx6.net", "volny.cz", "thedh.info", "smarttds.ws", "ttplayer.info", "61.147.115.16", "coower.com", "51edm.net", "nihao600.com", "cpadown.com", "feiyu68.com", "dnfwg.bz", "97aiwep.com", "wljjw.com", "2010player.info", "go7654321.com", "fineguardserv.in", "fineguard-serv.in", "googmei.info", "91.188.60.49", "qqmyq.info", "jprenti.com", "self-checker11.in", "fine-guard-serv.in", "fine-guardserv.in", "moedu.gov.iq", "77.78.239.173", "twzlxzjzu.in", "pferdekauf-online.de", "best-pc-defender2.com", "27dh.com", "t1sf.com", "anti-virus-protections.info", "0143.cn", "kirm-ar.ru", "qvodaac.com", "58.218.199.120", "going-to-n.com", "shildonyourpc.com", "elkxcb.in", "linksiden.no", "bookmarkocean.com", "in-scale-feed.in", "joiojojo.in", "567ad.com", "onlinecvs.info", "facabook.com", "cqww88.com", "articlesonwire.com", "superdown.co.kr", "virusmustdie.info", "newplayer.info", "govegonline.com", "18avok.com", "soundonair.net", "ttmil.com", "get-your-job.net", "daoki.com", "77.78.246.28", "foto-boom.org", "netflixmovie.net", "myfavoritesound.net", "womensbeautyonline.com", "eftpsid0363727.ru", "openwebfilm.com", "stream-video-net.com", "67bo.com", "stataskme.com", "femevogue.com", "77.78.245.62", "enong.com", "kinozavr.net", "52setu.info", "thenorthfacekorea.co.kr", "moviecriticsonline.com", "showkingon.com", "schraegschrift.de", "aijkl.net", "tradersandmarkets.com", "1cvs.info", "getfastloanonline.com", "ljxs.net", "rtystt.info", "card3fla.com", "ip527.com", "panorama.vn", "sepai365.com", "umniktds.ws", "games-all-around.com", "5fug.com", "wowedcars.com", "knowfilms.net", "pvip.info", "long-term-credits.com", "rumor-and-humor.net", "workhunt.net", "watch-to-enjoy.com", "66av.org", "thisisclick.com", "healthy-cosmetics.com", "webgentlemen.net", "webarh.com", "daedalic.org", "webmoneybag.net", "allcg.net", "okwit.com", "rutracer.org", "dhtoday.info", "64.111.196.126", "playernow.info", "meastonline.net", "kuqirt.com", "hotelmarketingstrategico.com", "niuwy.com", "baidu163so.info", "kirm-sky.ru", "bigbestmovie.com", "newsoundtracks.net", "freewz.info", "banage.ru", "bestonlineshows.net", "odontoblog.net", "gramerkagoj.com", "showstate.com", "rechkov.ru", "showsoftheworld.com", "moacbeniv.com", "funnysignage.com", "holy-varanasi.com", "193.111.244.156", "chengrenplay.info", "happyadcoopmarketing.info", "e55e.org", "77mntu.com", "unet.hk", "need4fuck.com", "hitlink.ir", "wmbshka.ru", "zxqczx.com", "14186.com", "qq12300.com", "wapda.gov.pk", "kavdy.org", "biperu.net", "getbookmark.info", "ff181.com", "49558.cn", "hqbulbul.ru", "music-star.ir", "rmmxx.com", "97bao.info", "razanw.org", "mohajersg.com", "parsaspace.ir", "adyannews.com", "youngjeezylosemymind.com", "isec.ir", "torrenbada.co.kr", "hongzhong.com", "brainkiller.it", "7shark.com", "tinypic.info", "apnihindi.com", "e2qs.com", "examfeedback.net", "zastopas.info", "top3cn.com", "playersite.info", "seocopywritingsolutions.com", "shanghaisc.cn", "209.167.111.110", "sagabox.com", "clean-protection39.in", "gooogle.it", "playeronline.info", "3x-teens.info", "jieda.in", "vixena.it", "url2vid.net", "f3jk.com", "kindle99.in", "strong-defense11.net", "197av.com", "adult-go-usenet.com", "bookmarkocean.com", "kitepoint.it", "sky-ar.ru", "a2ibba.net", "inscale-feed.in", "77.78.239.175", "traffby.com", "iisvc.com", "pivfeels.com", "best-pc-defender1.com", "3x-video.net", "sm500.com", "metalzone.biz", "33you.com", "77.78.239.174", "eftpsid0363623.com", "3zouz.com"};
    Calendar calendar = Calendar.getInstance();
    String datetime = this.format.format(this.calendar.getTime());
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    SQLiteDatabase linkDB;
    Context mContext;
    DBOpenHelper mHelper;

    public MalDataBase(Context context) {
        this.mContext = context;
    }

    public void openDB() {
        this.mHelper = new DBOpenHelper(this.mContext);
        this.linkDB = this.mHelper.getWritableDatabase();
    }

    public void closeDB() {
        if (this.linkDB.isOpen()) {
            this.linkDB.close();
        }
        this.linkDB = null;
    }

    public void insertUrl(String tempurl, String dbName) {
        try {
            this.linkDB.execSQL("INSERT INTO " + dbName + " (url,inserttime) VALUES (?,?)", new Object[]{tempurl, this.datetime});
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int getCountUrl(String dbName) {
        Cursor cursor = this.linkDB.rawQuery("SELECT COUNT(*) FROM " + dbName, null);
        if (cursor.moveToNext()) {
            return cursor.getInt(0);
        }
        return 0;
    }

    public void deleteUrl(String dbName) {
        Cursor cursor = this.linkDB.rawQuery("SELECT inserttime FROM " + dbName + " ORDER BY inserttime ASC", null);
        String time = null;
        if (cursor.moveToNext()) {
            cursor.moveToFirst();
            time = cursor.getString(cursor.getColumnIndex("inserttime"));
        }
        try {
            this.linkDB.execSQL("DELETE FROM " + dbName + " WHERE inserttime=?", new Object[]{time});
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void delURL(String url) {
        try {
            this.linkDB.execSQL("DELETE FROM blackurl WHERE url=?", new Object[]{url});
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int searchURL(String url) {
        Cursor cursor = null;
        try {
            cursor = this.linkDB.rawQuery("SELECT _id FROM blackurl WHERE url=?", new String[]{url});
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (cursor.moveToNext()) {
            return cursor.getInt(0);
        }
        return 0;
    }

    public void initUrls() {
        for (String insertUrl : testee) {
            insertUrl(insertUrl, BLACK_TABLE_NAME);
        }
    }

    public boolean isInDB(String url, String dbName) {
        boolean res = false;
        String url2 = url.toLowerCase().trim();
        if (url2.startsWith("http://")) {
            url2 = url2.substring(7);
        }
        if (url2.endsWith("/")) {
            url2 = url2.substring(0, url2.length() - 1);
        }
        Cursor cursor = this.linkDB.rawQuery("select * from " + dbName + " where url=?", new String[]{url2});
        try {
            if (cursor.moveToFirst()) {
                String string = cursor.getString(cursor.getColumnIndex(XmlUtils.LABEL_REPORT_URL));
                res = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        cursor.close();
        return res;
    }
}
