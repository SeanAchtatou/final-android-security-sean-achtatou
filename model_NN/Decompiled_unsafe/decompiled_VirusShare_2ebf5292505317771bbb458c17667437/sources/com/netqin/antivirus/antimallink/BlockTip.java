package com.netqin.antivirus.antimallink;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import com.netqin.antivirus.cloud.model.xml.XmlUtils;
import java.net.URLDecoder;

public class BlockTip extends Activity {
    /* access modifiers changed from: private */
    public KeepThread keepThread;
    /* access modifiers changed from: private */
    public String mUrl;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        this.mUrl = getIntent().getStringExtra(XmlUtils.LABEL_REPORT_URL);
        this.mUrl = URLDecoder.decode(this.mUrl);
        new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (BlockTip.this.keepThread != null) {
                    BlockTip.this.keepThread.setIsRun(false);
                }
                BlockTip.this.finish();
            }
        };
        new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (BlockTip.this.keepThread != null) {
                    BlockTip.this.keepThread.setIsRun(false);
                }
                BlockTip.this.finish();
            }
        };
    }

    public void onResume() {
        super.onResume();
    }

    public void onPause() {
        super.onPause();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void keep() {
        this.keepThread = new KeepThread(this);
        this.keepThread.setIsRun(true);
        this.keepThread.start();
    }

    public class KeepThread extends Thread {
        private boolean isRunning;
        public BlockTip mBlocker;

        public KeepThread(BlockTip blocker) {
            super("abc");
            this.mBlocker = blocker;
        }

        public synchronized void setIsRun(boolean isrun) {
            this.isRunning = isrun;
        }

        public void run() {
            while (this.isRunning) {
                try {
                    if (!((ActivityManager) BlockTip.this.getApplicationContext().getSystemService("activity")).getRunningTasks(1).get(0).topActivity.getClassName().equalsIgnoreCase("com.netqin.antivirus.antimallink.BlockTip")) {
                        Intent in = new Intent(BlockTip.this, BlockTip.class);
                        in.setFlags(8388608);
                        in.putExtra(XmlUtils.LABEL_REPORT_URL, BlockTip.this.mUrl);
                        BlockTip.this.startActivity(in);
                    }
                    Thread.sleep(1000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
