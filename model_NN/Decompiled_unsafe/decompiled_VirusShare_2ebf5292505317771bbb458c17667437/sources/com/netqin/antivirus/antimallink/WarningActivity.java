package com.netqin.antivirus.antimallink;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.netqin.antivirus.cloud.model.xml.XmlUtils;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.util.TaskManagerUtils;
import com.nqmobile.antivirus_ampro20.R;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

public class WarningActivity extends Activity {
    public static List<String> unBlockedUrlList = new ArrayList();
    /* access modifiers changed from: private */
    public ActivityManager am;
    private Button mBtnContinue;
    private Button mBtnOk;
    /* access modifiers changed from: private */
    public MalDataBase mDB;
    private TextView mTitle;
    /* access modifiers changed from: private */
    public String mUrl;
    private TextView mUrlText;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.warning);
        Intent intent = getIntent();
        this.am = (ActivityManager) getApplicationContext().getSystemService("activity");
        if (TaskManagerUtils.isFroyoSDK()) {
            this.am.killBackgroundProcesses("com.android.browser");
        } else {
            this.am.restartPackage("com.android.browser");
        }
        this.mDB = new MalDataBase(this);
        this.mDB.openDB();
        this.mUrl = intent.getStringExtra(XmlUtils.LABEL_REPORT_URL);
        this.mUrl = URLDecoder.decode(this.mUrl);
        if (!this.mUrl.startsWith("http://")) {
            this.mUrl = "http://" + this.mUrl;
        } else if (this.mUrl.endsWith("/")) {
            this.mUrl = this.mUrl.substring(0, this.mUrl.lastIndexOf("/"));
        }
        this.mUrlText = (TextView) findViewById(R.id.bad_website);
        this.mUrlText.setText(this.mUrl);
        this.mTitle = (TextView) findViewById(R.id.activity_name);
        this.mTitle.setText((int) R.string.label_netqin_antivirus);
        this.mBtnOk = (Button) findViewById(R.id.website_btn_ok);
        this.mBtnOk.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (TaskManagerUtils.isFroyoSDK()) {
                    WarningActivity.this.am.killBackgroundProcesses("com.android.browser");
                } else {
                    WarningActivity.this.am.restartPackage("com.android.browser");
                }
                WarningActivity.this.sendIntent();
                WarningActivity.this.finish();
            }
        });
        this.mBtnContinue = (Button) findViewById(R.id.website_btn_continue_to_visit);
        this.mBtnContinue.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String sUrl = "";
                if (WarningActivity.this.mUrl.startsWith("http://")) {
                    sUrl = WarningActivity.this.mUrl.substring(7);
                } else if (WarningActivity.this.mUrl.endsWith("/")) {
                    sUrl.substring(0, WarningActivity.this.mUrl.lastIndexOf("/"));
                } else {
                    sUrl = WarningActivity.this.mUrl;
                }
                if (WarningActivity.this.mDB.searchURL(sUrl) > 0) {
                    WarningActivity.this.mDB.delURL(sUrl);
                }
                WarningActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(WarningActivity.this.mUrl)));
                WarningActivity.unBlockedUrlList.add(WarningActivity.this.mUrl);
                WarningActivity.this.finish();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            if (TaskManagerUtils.isFroyoSDK()) {
                this.am.killBackgroundProcesses("com.android.browser");
            } else {
                this.am.restartPackage("com.android.browser");
            }
            sendIntent();
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    /* access modifiers changed from: private */
    public void sendIntent() {
        Uri uri;
        if (CommonMethod.isLocalSimpleChinese()) {
            uri = Uri.parse("http://m.netqin.com/");
        } else {
            uri = Uri.parse("http://m.netqin.com/en/");
        }
        startActivity(new Intent("android.intent.action.VIEW", uri));
    }
}
