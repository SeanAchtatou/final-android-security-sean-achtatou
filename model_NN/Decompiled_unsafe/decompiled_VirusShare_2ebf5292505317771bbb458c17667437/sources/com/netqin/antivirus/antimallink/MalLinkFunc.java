package com.netqin.antivirus.antimallink;

public class MalLinkFunc {
    public static native float nativeVersion();

    public native int malLinkEngineCheckLink(String str, int i);

    public native int malLinkEngineInit(String str, int i);

    public native int malLinkEngineLoad(int i);

    public native int malLinkEngineUninit(int i);

    public native int malLinkEngineUnload(int i);

    static {
        System.loadLibrary("netqinmallink");
    }
}
