package com.netqin.antivirus.antimallink;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class BlockService extends Service {
    private final IBinder mBinder = new BlockServiceBinder();
    private LogParser mLogParser;
    private FileDescriptor mTermFd;
    private FileOutputStream mTermOut;

    public class BlockServiceBinder extends Binder {
        public BlockServiceBinder() {
        }

        /* access modifiers changed from: package-private */
        public BlockService getService() {
            return BlockService.this;
        }
    }

    public IBinder onBind(Intent arg0) {
        return this.mBinder;
    }

    public void onCreate() {
        super.onCreate();
        this.mLogParser = new LogParser(this);
        clearLogCat();
        startListening();
    }

    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
    }

    public void onDestroy() {
        stopListening();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void clearLogCat() {
        new StringBuilder();
        try {
            ArrayList<String> commandLine = new ArrayList<>();
            commandLine.add("logcat");
            commandLine.add("-c");
            Process exec = Runtime.getRuntime().exec((String[]) commandLine.toArray(new String[0]));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void startListening() {
        int[] processId = new int[1];
        createSubprocess(processId);
        final int procId = processId[0];
        final Handler handler = new Handler() {
            public void handleMessage(Message msg) {
            }
        };
        new Thread(new Runnable() {
            public void run() {
                handler.sendEmptyMessage(Exec.waitFor(procId));
            }
        }).start();
        this.mTermOut = new FileOutputStream(this.mTermFd);
        this.mLogParser.init(this.mTermFd, this.mTermOut);
        this.mLogParser.startReading();
    }

    private void stopListening() {
        Exec.close(this.mTermFd);
        this.mLogParser.destroy();
    }

    private void createSubprocess(int[] processId) {
        this.mTermFd = Exec.createSubprocess("/system/bin/logcat", "-s", "-v", "raw", "ActivityManager:I", "webkit:V", "SearchDialog:I", processId);
    }
}
