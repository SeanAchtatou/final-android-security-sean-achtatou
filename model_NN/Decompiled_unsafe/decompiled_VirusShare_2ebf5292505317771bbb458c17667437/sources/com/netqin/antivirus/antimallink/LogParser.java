package com.netqin.antivirus.antimallink;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import com.netqin.antivirus.NqUtil;
import com.netqin.antivirus.cloud.model.DataUtils;
import com.netqin.antivirus.cloud.model.xml.XmlUtils;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.common.MaliciousWebsite;
import com.netqin.antivirus.contact.vcard.VCardConfig;
import com.netqin.antivirus.log.LogEngine;
import com.netqin.antivirus.scan.FileUtils;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.Proxy;
import java.net.URL;
import java.net.URLDecoder;
import org.apache.commons.codec.binary.Base64;

public class LogParser {
    private static final String BLACK_TABLE_NAME = "blackurl";
    private static final int UPDATE = 1;
    private static final String WHITE_TABLE_NAME = "whiteurl";
    final String NQBLOCKERWEB_EN = "http://m.netqin.com/en/";
    final String NQBLOCKERWEB_ZH = "http://m.netqin.com/";
    private HttpHandler httpHandler;
    private Context mContext;
    private MalDataBase mDB;
    /* access modifiers changed from: private */
    public final Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (msg.what == 1) {
                LogParser.this.update((String) msg.obj);
            }
        }
    };
    private Thread mReadingThread;
    private FileDescriptor mTermFd;
    /* access modifiers changed from: private */
    public FileInputStream mTermIn;
    private FileOutputStream mTermOut;

    public LogParser(Context context) {
        this.mContext = context;
        this.mDB = new MalDataBase(context);
        this.mDB.openDB();
    }

    public void init(FileDescriptor termFd, FileOutputStream termOut) {
        this.mTermOut = termOut;
        this.mTermFd = termFd;
        this.mTermIn = new FileInputStream(this.mTermFd);
    }

    public void startReading() {
        this.mReadingThread = new Thread(new Runnable() {
            private byte[] mBuffer = new byte[4096];

            public void run() {
                while (true) {
                    try {
                        int read = LogParser.this.mTermIn.read(this.mBuffer);
                        if (read > 2) {
                            LogParser.this.mHandler.sendMessage(LogParser.this.mHandler.obtainMessage(1, new String(this.mBuffer, 0, read)));
                        }
                    } catch (Exception e) {
                        return;
                    }
                }
            }
        });
        this.mReadingThread.setName("Input reader");
        this.mReadingThread.start();
    }

    /* access modifiers changed from: private */
    public void update(String str) {
        String url = getUrl(str);
        if (url != null) {
            int type = 0;
            try {
                type = isMalUrl(url);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (type > 0) {
                doBlockWeb(type, url);
            }
        }
    }

    public static String getUrl(String logStr) {
        String logStr2 = logStr.toLowerCase();
        if (logStr2.indexOf("action=android.intent.action.SEARCH".toLowerCase()) > 0 && logStr2.indexOf("browser".toLowerCase()) > 0 && logStr2.indexOf(";end".toLowerCase()) > 0) {
            int pos = logStr2.indexOf("S.user_query=".toLowerCase());
            int end = logStr2.lastIndexOf(";end");
            if (pos > 0 && end > pos) {
                return logStr2.substring("S.user_query=".length() + pos, end);
            }
        } else if (logStr2.indexOf("cmp=com.android.browser".toLowerCase()) > 0 && (logStr2.indexOf("action=android.intent.action.VIEW".toLowerCase()) > 0 || logStr2.indexOf("act=android.intent.action.VIEW".toLowerCase()) > 0)) {
            int pos2 = logStr2.indexOf("dat=http");
            int end2 = logStr2.lastIndexOf(" cmp=com.android.browser");
            if (pos2 > 0 && end2 > pos2) {
                String url = logStr2.substring("dat=".length() + pos2, end2);
                int spacepos = url.indexOf(" ");
                if (spacepos > 0) {
                    url = url.substring(0, spacepos);
                }
                return url;
            }
        } else if (logStr2.indexOf("guessURL before queueRequest:".toLowerCase()) >= 0) {
            String tag = "guessURL before queueRequest:".toLowerCase();
            String url2 = logStr2.substring(tag.length() + logStr2.indexOf(tag));
            if (url2.startsWith(" ")) {
                url2 = url2.substring(1);
            }
            int blankpos = url2.indexOf("\n");
            if (blankpos > 0) {
                url2 = url2.substring(0, blankpos - 1);
            }
            if (url2.indexOf(".") > 0) {
                return url2;
            }
        }
        return null;
    }

    public void doBlockWeb(int type, String url) {
        Intent intent;
        if (CommonMethod.isLocalSimpleChinese()) {
            if (!url.startsWith("http://m.netqin.com/") && !url.endsWith("http://m.netqin.com/")) {
                intent = new Intent("android.intent.action.VIEW", Uri.parse("http://m.netqin.com/"));
            } else {
                return;
            }
        } else if (!url.startsWith("http://m.netqin.com/en/") && !url.endsWith("http://m.netqin.com/en/")) {
            intent = new Intent("android.intent.action.VIEW", Uri.parse("http://m.netqin.com/en/"));
        } else {
            return;
        }
        intent.addFlags(VCardConfig.FLAG_USE_QP_TO_PRIMARY_PROPERTIES);
        intent.setClassName("com.android.browser", "com.android.browser.BrowserActivity");
        this.mContext.startActivity(intent);
        Intent intent2 = new Intent(this.mContext, WarningActivity.class);
        intent2.addFlags(VCardConfig.FLAG_USE_QP_TO_PRIMARY_PROPERTIES);
        intent2.putExtra(XmlUtils.LABEL_REPORT_URL, url);
        intent2.setFlags(276824064);
        this.mContext.startActivity(intent2);
    }

    public String compose(String para) {
        byte[] buffer = para.getBytes();
        byte[] buf = new byte[buffer.length];
        for (int i = 0; i < buf.length; i++) {
            buf[i] = (byte) (buffer[i] ^ 110);
        }
        return new String(Base64.encodeBase64(buf));
    }

    public int checkIsMalLink(String url) {
        String decodeUrl = URLDecoder.decode(url);
        MalLinkFunc ml = new MalLinkFunc();
        int err = ml.malLinkEngineInit(FileUtils.malLinkDbFilePath(this.mContext), 0);
        int err2 = ml.malLinkEngineLoad(0);
        int res = ml.malLinkEngineCheckLink(decodeUrl, 0);
        ml.malLinkEngineUnload(0);
        ml.malLinkEngineUninit(0);
        return res;
    }

    public int isMalUrl(String url) throws Exception {
        if (CommonMethod.isLocalSimpleChinese()) {
            if (url.matches("http://m.netqin.com/")) {
                return 0;
            }
        } else if (url.matches("http://m.netqin.com/en/")) {
            return 0;
        }
        if (!url.startsWith("http://") && !url.startsWith("http%3a%2f%2f")) {
            url = "http://" + url;
        } else if (url.endsWith("/")) {
            url = url.substring(0, url.lastIndexOf("/"));
        }
        for (String unBlockedUrl : WarningActivity.unBlockedUrlList) {
            if (url.equals(unBlockedUrl)) {
                WarningActivity.unBlockedUrlList.clear();
                return 0;
            }
        }
        String decodeUrl = URLDecoder.decode(url);
        if (decodeUrl.endsWith("/")) {
            decodeUrl = decodeUrl.substring(0, decodeUrl.length() - 1);
        }
        if (checkNet() && this.mDB.isInDB(decodeUrl, BLACK_TABLE_NAME)) {
            LogEngine.insertGuardItemLog(3, 0 + 1, "", this.mContext.getFilesDir().getPath());
            LogEngine.insertThreatItemLog(30, decodeUrl, "", "", this.mContext.getFilesDir().getPath());
            return 1;
        } else if (!checkNet() || !this.mDB.isInDB(decodeUrl, WHITE_TABLE_NAME)) {
            if (this.httpHandler == null) {
                this.httpHandler = new HttpHandler(this.mContext);
            }
            Proxy proxy = NqUtil.getApnProxy(this.mContext);
            if (proxy != null) {
                this.httpHandler.setProxy(proxy);
            } else {
                this.httpHandler.NoProxy();
            }
            if (decodeUrl.startsWith(" ")) {
                decodeUrl = decodeUrl.substring(1);
            }
            byte[] postdata = ("url=" + new String(DataUtils.ecrypt(decodeUrl.getBytes()))).getBytes();
            MaliciousWebsite maliciousWebsite = new MaliciousWebsite(this.mContext);
            maliciousWebsite.setMaliciousWebsiteStartTime(CommonMethod.getDate());
            maliciousWebsite.setVisitUrl(url);
            this.httpHandler.postRequest(new URL("http://www.androidsec.com/url/check"), postdata);
            byte[] response = this.httpHandler.getResponsebytes();
            if (response == null) {
                return 0;
            }
            String tempurl = decodeUrl;
            if (new String(response).toLowerCase().matches("result=1")) {
                if (tempurl.startsWith("http://")) {
                    tempurl = tempurl.substring(7);
                }
                if (tempurl.endsWith("/")) {
                    tempurl = tempurl.substring(0, tempurl.length() - 1);
                }
                if (this.mDB.getCountUrl(BLACK_TABLE_NAME) >= 200) {
                    this.mDB.deleteUrl(BLACK_TABLE_NAME);
                    this.mDB.insertUrl(tempurl, BLACK_TABLE_NAME);
                } else {
                    this.mDB.insertUrl(tempurl, BLACK_TABLE_NAME);
                }
                LogEngine.insertGuardItemLog(3, 0 + 1, "", this.mContext.getFilesDir().getPath());
                LogEngine.insertThreatItemLog(30, tempurl, "", "", this.mContext.getFilesDir().getPath());
                return 1;
            }
            if (tempurl.startsWith("http://")) {
                tempurl = tempurl.substring(7);
            }
            if (tempurl.endsWith("/")) {
                tempurl = tempurl.substring(0, tempurl.length() - 1);
            }
            if (this.mDB.getCountUrl(WHITE_TABLE_NAME) >= 500) {
                this.mDB.deleteUrl(WHITE_TABLE_NAME);
                this.mDB.insertUrl(tempurl, WHITE_TABLE_NAME);
            } else {
                this.mDB.insertUrl(tempurl, WHITE_TABLE_NAME);
            }
            LogEngine.insertGuardItemLog(3, 0 + 1, "", this.mContext.getFilesDir().getPath());
            return 0;
        } else {
            LogEngine.insertGuardItemLog(3, 0 + 1, "", this.mContext.getFilesDir().getPath());
            return 0;
        }
    }

    public void destroy() {
        this.mDB.closeDB();
    }

    private boolean checkNet() {
        NetworkInfo info = ((ConnectivityManager) this.mContext.getSystemService("connectivity")).getActiveNetworkInfo();
        if (info == null || !info.isConnected()) {
            return false;
        }
        return true;
    }
}
