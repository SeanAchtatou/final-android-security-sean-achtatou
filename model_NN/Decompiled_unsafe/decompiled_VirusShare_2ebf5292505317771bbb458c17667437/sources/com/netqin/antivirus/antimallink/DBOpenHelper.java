package com.netqin.antivirus.antimallink;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBOpenHelper extends SQLiteOpenHelper {
    public DBOpenHelper(Context context) {
        super(context, "mallink.db", (SQLiteDatabase.CursorFactory) null, 1);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS blackurl (_id INTEGER PRIMARY KEY AUTOINCREMENT,url VARCHAR(50),inserttime datetime)");
        db.execSQL("CREATE TABLE IF NOT EXISTS whiteurl (_id INTEGER PRIMARY KEY AUTOINCREMENT,url VARCHAR(50),inserttime datetime)");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}
