package com.netqin.antivirus.antimallink;

import android.content.Context;
import java.net.Proxy;
import java.net.URL;

public class HttpHandler {
    private URL appServerURL;
    private URL contactServerURL;
    private DataHandler handler = new DataHandler(this.mContext);
    private Context mContext;
    private URL updateServerURL;
    private URL userServerURL;

    public HttpHandler(Context context) {
        this.mContext = context;
    }

    public int sendAppServerRequest(byte[] requestbytes) {
        this.handler.setUrl(this.appServerURL);
        this.handler.setRequestbytes(requestbytes);
        int result = connect();
        if (result != 1) {
            return result;
        }
        return 9;
    }

    public int postRequest(URL url, byte[] requestbytes) {
        this.handler.setUrl(url);
        this.handler.setRequestbytes(requestbytes);
        int result = connect();
        if (result != 1) {
            return result;
        }
        return 9;
    }

    public int sendContactServerRequest(byte[] requestbytes) {
        this.handler.setUrl(this.contactServerURL);
        this.handler.setRequestbytes(requestbytes);
        int result = connect();
        if (result != 1) {
            return result;
        }
        return 9;
    }

    public int sendUserServerRequest(byte[] requestbytes) {
        this.handler.setUrl(this.userServerURL);
        this.handler.setRequestbytes(requestbytes);
        this.handler.setContent_type("application/cotet-stream");
        int result = connect();
        if (result != 1) {
            return result;
        }
        return 9;
    }

    public int sendRequest(byte[] requestbytes, URL url) {
        this.handler.setRequestbytes(requestbytes);
        this.handler.setUrl(url);
        int result = connect();
        if (result != 1) {
            return result;
        }
        return 9;
    }

    public int sendRequest(byte[] requestbytes, String url) {
        this.handler.setRequestbytes(requestbytes);
        this.handler.setUrl(url);
        int result = connect();
        if (result != 1) {
            return result;
        }
        return 9;
    }

    public int downloadFile(URL url) {
        this.handler.setUrl(url);
        int result = connect();
        if (result != 1) {
            return result;
        }
        return 9;
    }

    public int sendUpdateServerRequest(byte[] requestBytes) {
        this.handler.setUrl(this.updateServerURL);
        this.handler.setRequestbytes(requestBytes);
        int result = connect();
        if (result != 1) {
            return result;
        }
        return 9;
    }

    public int uploadFile(byte[] requestBytes, URL url, String range) {
        this.handler.setRange(range);
        this.handler.setUrl(url);
        this.handler.setRequestbytes(requestBytes);
        this.handler.setRequestMethod("SET");
        this.handler.setContent_type("application/octet-stream");
        int result = connect();
        if (result != 1) {
            return result;
        }
        return 9;
    }

    public int downloadPackt(URL url, String range) {
        this.handler.setRange(range);
        this.handler.setUrl(url);
        this.handler.setRequestMethod("GET");
        this.handler.setContent_type("application");
        int result = connect();
        if (result != 1) {
            return result;
        }
        return 9;
    }

    public void setCancel(boolean cancel) {
        this.handler.setCancel(cancel);
    }

    public byte[] getResponsebytes() {
        return this.handler.getResponsebytes();
    }

    private int connect() {
        this.handler.reset();
        this.handler.processData();
        return 1;
    }

    public void setProxy(Proxy proxy) {
        this.handler.setProxy(proxy);
    }

    public void NoProxy() {
        this.handler.NoProxy();
    }

    public int getCurrentResponseLength() {
        return this.handler.getCurrentResponseLength();
    }

    public boolean isFinish() {
        return this.handler.isFinish();
    }
}
