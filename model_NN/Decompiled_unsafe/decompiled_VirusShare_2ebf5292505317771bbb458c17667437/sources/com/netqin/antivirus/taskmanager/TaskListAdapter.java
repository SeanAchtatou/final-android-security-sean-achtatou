package com.netqin.antivirus.taskmanager;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import com.netqin.antivirus.data.Application;
import com.netqin.antivirus.data.PreferenceDataHelper;
import com.netqin.antivirus.services.TaskManagerService;
import com.netqin.antivirus.util.AppQueryCondition;
import com.netqin.antivirus.util.TaskManagerUtils;
import com.nqmobile.antivirus_ampro20.R;
import java.util.ArrayList;
import java.util.Iterator;

public class TaskListAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<Application> mData;
    private LayoutInflater mInflater;
    private String mPackageName;
    private AppQueryCondition mQueryCondition;
    private ArrayList<Application> mSys;
    private ArrayList<Application> mThird;

    public TaskListAdapter(Context context, ArrayList<Application> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.mContext = context;
        this.mPackageName = context.getPackageName();
    }

    public TaskListAdapter(Context context, ArrayList<Application> data, AppQueryCondition queryCondition) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.mContext = context;
        this.mQueryCondition = queryCondition;
        this.mPackageName = context.getPackageName();
    }

    public synchronized ArrayList<Application> getAll() {
        return this.mData;
    }

    public synchronized ArrayList<Application> getSelected() {
        ArrayList<Application> arrayList;
        if (this.mData == null || getCount() == 0) {
            arrayList = null;
        } else {
            ArrayList<Application> selected = new ArrayList<>(getCount());
            Iterator<Application> it = this.mData.iterator();
            while (it.hasNext()) {
                Application app = it.next();
                if (app.isChecked) {
                    selected.add(app);
                }
            }
            arrayList = selected;
        }
        return arrayList;
    }

    public synchronized int getCount() {
        return this.mData.size();
    }

    public synchronized Object getItem(int position) {
        return this.mData.get(position);
    }

    public synchronized long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = this.mInflater.inflate((int) R.layout.tasklist_item, (ViewGroup) null);
            holder = new ViewHolder();
            holder.checkBox = (CheckBox) convertView.findViewById(R.id.checkbox);
            holder.iconView = (ImageView) convertView.findViewById(R.id.icon);
            holder.nameView = (TextView) convertView.findViewById(R.id.name);
            holder.memoryView = (TextView) convertView.findViewById(R.id.memory);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final Application app = getApplication(position);
        holder.nameView.setText(app.getLabelName(this.mContext));
        holder.iconView.setImageDrawable(app.getIcon(this.mContext));
        holder.memoryView.setText(this.mContext.getString(R.string.memory_info, Double.valueOf(TaskManagerUtils.getDecimalPoint(((double) app.getMemory(this.mContext)) / 1000.0d, 1))));
        holder.checkBox.setChecked(app.isChecked);
        if (this.mPackageName == null || !app.packageName.equals(this.mPackageName)) {
            holder.checkBox.setVisibility(0);
        } else {
            holder.checkBox.setVisibility(8);
        }
        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (holder.checkBox.isChecked()) {
                    app.isChecked = true;
                } else {
                    app.isChecked = false;
                }
            }
        });
        return convertView;
    }

    public synchronized Application getApplication(int pos) {
        return this.mData.get(pos);
    }

    public synchronized void notifyDataSetChanged() {
        this.mData = TaskManagerService.getRunningApps(this.mContext, this.mQueryCondition, false);
        super.notifyDataSetChanged();
    }

    private void initChecked_bak() {
        this.mThird = new ArrayList<>();
        this.mSys = new ArrayList<>();
        Iterator<Application> iterator = this.mData.iterator();
        while (iterator.hasNext()) {
            Application app = iterator.next();
            if (app.filter.getAppLevel() == 1) {
                if (PreferenceDataHelper.isInWhiteList(this.mContext, app.packageName) || TaskManagerUtils.isNgApp(this.mContext, app)) {
                    app.isChecked = false;
                } else {
                    app.isChecked = true;
                }
                this.mThird.add(app);
            } else {
                app.isChecked = false;
                this.mSys.add(app);
            }
        }
        Iterator<Application> iterator2 = this.mSys.iterator();
        while (iterator2.hasNext()) {
            this.mThird.add(iterator2.next());
        }
        this.mData.clear();
        this.mData = this.mThird;
    }

    private void initChecked() {
        this.mThird = new ArrayList<>();
        this.mSys = new ArrayList<>();
        Iterator<Application> iterator = this.mData.iterator();
        while (iterator.hasNext()) {
            Application app = iterator.next();
            if (PreferenceDataHelper.isInWhiteList(this.mContext, app.packageName) || TaskManagerUtils.isNgApp(this.mContext, app)) {
                app.isChecked = false;
            } else {
                app.isChecked = true;
            }
        }
    }

    static class ViewHolder {
        CheckBox checkBox;
        ImageView iconView;
        TextView memoryView;
        TextView nameView;

        ViewHolder() {
        }
    }

    public synchronized void remove(int i) {
        this.mData.remove(i);
    }

    public void remove(Application app) {
        remove(app, false);
    }

    public synchronized void remove(Application app, boolean notify) {
        this.mData.remove(app);
        if (notify) {
            notifyDataSetChanged();
        }
    }
}
