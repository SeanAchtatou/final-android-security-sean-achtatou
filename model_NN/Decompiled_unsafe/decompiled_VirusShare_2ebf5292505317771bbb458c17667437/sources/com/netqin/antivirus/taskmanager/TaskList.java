package com.netqin.antivirus.taskmanager;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.netqin.antivirus.data.Application;
import com.netqin.antivirus.data.PreferenceDataHelper;
import com.netqin.antivirus.services.TaskManagerService;
import com.netqin.antivirus.ui.BaseListActivity;
import com.netqin.antivirus.ui.TriggermanTask;
import com.netqin.antivirus.util.AppQueryCondition;
import com.netqin.antivirus.util.SystemUtils;
import com.netqin.antivirus.util.TaskManagerUtils;
import com.nqmobile.antivirus_ampro20.R;
import java.util.ArrayList;
import java.util.Iterator;

public class TaskList extends BaseListActivity implements View.OnClickListener {
    public static final String EXTRA_PROMOTE = "ext_promote";
    public static final String EXTRA_QUERY_CONDITION = "com.netqin.mobileguard.query_condition";
    public static final int EXTRA_VALUE_NET_APP = 1;
    /* access modifiers changed from: private */
    public TaskListAdapter mAdapter;
    private Button mBtnShiftApps;
    /* access modifiers changed from: private */
    public int mCheckedItem = 2;
    private ArrayList<Application> mData;
    private IntentFilter mFilter;
    private ListView mListView;
    private String mPackageName;
    /* access modifiers changed from: private */
    public AppQueryCondition mQueryCondition;
    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action != null && action.equals(TaskManagerUtils.ACTION_APP_CHANGED)) {
                TaskList.this.mAdapter.notifyDataSetChanged();
            }
        }
    };
    private boolean mSelectAllStatus = false;
    protected String mTaskTip;
    private TextView mTip;
    Button selectAll;

    public static Intent getLaunchIntent(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, TaskList.class);
        return intent;
    }

    public static Intent getLaunchIntent(Context context, String promoteText) {
        Intent intent = new Intent();
        intent.setClass(context, TaskList.class);
        intent.putExtra(EXTRA_PROMOTE, promoteText);
        return intent;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.tasklist);
        setRequestedOrientation(1);
        ((TextView) findViewById(R.id.activity_name)).setText((int) R.string.home_system_opt);
        this.mPackageName = getPackageName();
        initFilter();
        registerReceiver(this.mReceiver, this.mFilter);
        initRes();
        initQueryCondition();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        initListView();
    }

    private void initFilter() {
        this.mFilter = new IntentFilter(TaskManagerUtils.ACTION_APP_CHANGED);
    }

    private void initRes() {
        this.mListView = getListView();
        this.mTip = (TextView) findViewById(R.id.task_tip);
        this.mBtnShiftApps = (Button) findViewById(R.id.shift_apps);
        this.selectAll = (Button) findViewById(R.id.select_all);
        ((Button) findViewById(R.id.kill)).setOnClickListener(this);
        ((Button) findViewById(R.id.kill_all)).setOnClickListener(this);
        this.mBtnShiftApps.setOnClickListener(this);
        this.selectAll.setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        if (this.mAdapter != null) {
            final Application app = this.mAdapter.getApplication(position);
            if (!this.mPackageName.equals(app.packageName)) {
                new AlertDialog.Builder(this).setIcon(17301659).setTitle((int) R.string.kill_title).setMessage(getString(R.string.kill_message, new Object[]{app.getLabelName(this)})).setNegativeButton(17039360, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                }).setPositiveButton(17039370, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        SystemUtils.killApp(TaskList.this, app);
                        TaskList.this.mAdapter.remove(app, true);
                    }
                }).show();
            }
        }
    }

    public boolean onContextItemSelected(MenuItem item) {
        return super.onContextItemSelected(item);
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (this.mAdapter != null) {
            final Application app = this.mAdapter.getApplication(((AdapterView.AdapterContextMenuInfo) menuInfo).position);
            new AlertDialog.Builder(this).setIcon(17301659).setTitle((int) R.string.kill_title).setMessage(getString(R.string.kill_message, new Object[]{app.getLabelName(this)})).setNegativeButton(17039360, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                }
            }).setPositiveButton(17039370, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    SystemUtils.killApp(TaskList.this, app);
                    TaskList.this.mAdapter.remove(app, true);
                }
            }).show();
        }
    }

    /* access modifiers changed from: private */
    public void initListView() {
        if (TextUtils.isEmpty(this.mTaskTip)) {
            this.mTip.setText((int) R.string.task_tip_runapp);
        } else {
            this.mTip.setText(this.mTaskTip);
        }
        String tips = getIntent().getStringExtra(EXTRA_PROMOTE);
        if (!TextUtils.isEmpty(tips)) {
            this.mTip.setText(tips);
        }
        this.mData = TaskManagerService.getRunningApps(this, this.mQueryCondition, true);
        Application self = TaskManagerUtils.getMgSelf(this);
        boolean hasSelf = false;
        Iterator<Application> it = this.mData.iterator();
        while (true) {
            if (it.hasNext()) {
                Application app = it.next();
                if (app.packageName.equals(self.packageName)) {
                    self = app;
                    hasSelf = true;
                    break;
                }
            } else {
                break;
            }
        }
        if (hasSelf) {
            this.mData.remove(self);
            this.mData.add(self);
        }
        initChecked();
        this.mAdapter = new TaskListAdapter(this, this.mData, this.mQueryCondition);
        this.mListView.setAdapter((ListAdapter) this.mAdapter);
        TaskManagerService.scanRunningTask(this);
    }

    private void initChecked() {
        Iterator<Application> iterator = this.mData.iterator();
        while (iterator.hasNext()) {
            Application app = iterator.next();
            if (PreferenceDataHelper.isInWhiteList(this, app.packageName) || TaskManagerUtils.isNgApp(this, app)) {
                app.isChecked = false;
            } else {
                app.isChecked = true;
            }
        }
    }

    private void initQueryCondition() {
        this.mQueryCondition = new AppQueryCondition();
        this.mQueryCondition.appLevel = 3;
        if (getIntent() != null) {
            switch (getIntent().getIntExtra(EXTRA_QUERY_CONDITION, 0)) {
                case 1:
                    this.mQueryCondition.appLevel = 3;
                    this.mQueryCondition.appPermission = "android.permission.INTERNET";
                    this.mCheckedItem = 1;
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.mReceiver);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.kill_all /*2131558827*/:
                onKillAll();
                return;
            case R.id.kill /*2131558828*/:
                onKillSelected();
                return;
            case R.id.select_all /*2131558829*/:
                toggleSelectAll();
                return;
            case R.id.shift_apps /*2131558830*/:
                shiftApps();
                return;
            default:
                return;
        }
    }

    private void shiftApps() {
        new AlertDialog.Builder(this).setIcon(17301659).setTitle((int) R.string.shift_apps).setSingleChoiceItems((int) R.array.shift_options, this.mCheckedItem, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                switch (whichButton) {
                    case 0:
                        TaskList.this.mCheckedItem = 0;
                        TaskList.this.mTaskTip = TaskList.this.getString(R.string.task_tip_runapp);
                        TaskList.this.mQueryCondition.appLevel = 1;
                        break;
                    case 1:
                        TaskList.this.mCheckedItem = 1;
                        TaskList.this.mTaskTip = TaskList.this.getString(R.string.task_tip_allapp);
                        TaskList.this.mQueryCondition.appLevel = 3;
                        break;
                    case 2:
                        TaskList.this.mCheckedItem = 2;
                        TaskList.this.mTaskTip = TaskList.this.getString(R.string.task_tip_allapp);
                        TaskList.this.mQueryCondition.appLevel = 7;
                        break;
                }
                dialog.dismiss();
                TaskList.this.initListView();
            }
        }).setNegativeButton(17039360, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        }).show();
    }

    private void onKillAll() {
        ArrayList<Application> apps = this.mAdapter.getAll();
        ArrayList<Application> appsForKill = new ArrayList<>();
        if (apps != null && apps.size() > 0) {
            Iterator<Application> it = apps.iterator();
            while (it.hasNext()) {
                Application application = it.next();
                if (!TaskManagerUtils.isNgApp(this, application)) {
                    appsForKill.add(application);
                }
            }
            startKillApps(appsForKill);
        }
    }

    private void onKillSelected() {
        ArrayList<Application> apps = this.mAdapter.getSelected();
        if (apps == null) {
            return;
        }
        if (apps.size() > 0) {
            startKillApps(apps);
            updateWhiteList(this);
            return;
        }
        Toast.makeText(this, (int) R.string.select_app_tokill, 0).show();
    }

    /* access modifiers changed from: protected */
    public void updateWhiteList(Context context) {
        if (this.mAdapter != null && this.mAdapter.getCount() > 0) {
            int appCount = this.mAdapter.getCount();
            for (int i = 0; i < appCount; i++) {
                Application app = this.mAdapter.getApplication(i);
                if (!app.isChecked) {
                    PreferenceDataHelper.addWhiteApp(context, app.packageName);
                } else if (PreferenceDataHelper.isInWhiteList(context, app.packageName)) {
                    PreferenceDataHelper.removeWhiteApp(context, app.packageName);
                }
            }
        }
    }

    private void startKillApps(ArrayList<Application> apps) {
        Application self = TaskManagerUtils.getMgSelf(this);
        boolean hasSelf = false;
        Iterator<Application> it = apps.iterator();
        while (true) {
            if (it.hasNext()) {
                Application app = it.next();
                if (app.packageName.equals(self.packageName)) {
                    self = app;
                    hasSelf = true;
                    break;
                }
            } else {
                break;
            }
        }
        if (hasSelf) {
            apps.remove(self);
        }
        new TriggermanTask(this, apps, createKillAppProgressDialog()) {
            /* access modifiers changed from: protected */
            public void onPostKill(Application app) {
                TaskList.this.mAdapter.remove(app, true);
            }
        }.execute(new Void[0]);
    }

    private final ProgressDialog createKillAppProgressDialog() {
        ProgressDialog pd = new ProgressDialog(this);
        pd.setProgress(0);
        pd.setMessage("");
        pd.setTitle((int) R.string.dialog_title_kill_app);
        pd.setProgressStyle(1);
        return pd;
    }

    private void toggleSelectAll() {
        this.mSelectAllStatus = !this.mSelectAllStatus;
        ListView listView = this.mListView;
        int itemCount = listView.getCount();
        for (int i = 0; i < itemCount; i++) {
            ((Application) listView.getItemAtPosition(i)).isChecked = this.mSelectAllStatus;
        }
        int childCount = listView.getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            ((CheckBox) listView.getChildAt(i2).findViewById(R.id.checkbox)).setChecked(this.mSelectAllStatus);
        }
    }
}
