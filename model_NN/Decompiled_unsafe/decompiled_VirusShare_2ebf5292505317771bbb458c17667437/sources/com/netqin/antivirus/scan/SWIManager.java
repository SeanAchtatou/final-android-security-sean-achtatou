package com.netqin.antivirus.scan;

import SHcMjZX.DD02zqNU;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import com.netqin.antivirus.HomeActivity;
import com.netqin.antivirus.antivirus.NqPackageManager;
import com.netqin.antivirus.cloud.model.CloudHandler;
import com.netqin.antivirus.cloud.virus.db.CloudVirusdb;
import com.netqin.antivirus.common.CommonDefine;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.log.LogEngine;
import com.nqmobile.antivirus_ampro20.R;
import java.util.ArrayList;

public class SWIManager implements ISWIMonitorObserver, IScanObserver {
    private ContextWrapper cntxtWrapper;
    Context context;
    ScanController controller;
    int i;
    /* access modifiers changed from: private */
    public IScanFileObserver[] mFileObservers = new IScanFileObserver[5];
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (intent.getExtras() != null && action.equals("android.intent.action.MEDIA_MOUNTED")) {
                SWIManager.this.i = 0;
                while (SWIManager.this.i < SWIManager.this.monitorPaths.length) {
                    if (SWIManager.this.mFileObservers[SWIManager.this.i] != null) {
                        SWIManager.this.mFileObservers[SWIManager.this.i].stopWatching();
                    }
                    SWIManager.this.i++;
                }
                for (int i = 0; i < SWIManager.this.monitorPaths.length; i++) {
                    SWIManager.this.mFileObservers[i] = new IScanFileObserver(SWIManager.this.monitorPaths[i], context);
                    SWIManager.this.mFileObservers[i].startWatching();
                }
            }
        }
    };
    /* access modifiers changed from: private */
    public String[] monitorPaths;
    private SWIReceiver receiver;
    VirusItem vi;

    public SWIManager(ContextWrapper contextWrapper, Context context2) {
        this.cntxtWrapper = contextWrapper;
        this.context = context2;
    }

    public void beginMonitorSWI() {
        this.receiver = new SWIReceiver(this);
        IntentFilter filter1 = new IntentFilter("android.intent.action.PACKAGE_ADDED");
        filter1.addDataScheme("package");
        IntentFilter filter2 = new IntentFilter("android.intent.action.PACKAGE_REPLACED");
        filter2.addDataScheme("package");
        IntentFilter filter3 = new IntentFilter("android.intent.action.PACKAGE_REMOVED");
        filter3.addDataScheme("package");
        IntentFilter filter4 = new IntentFilter("android.intent.action.MEDIA_MOUNTED");
        filter4.addDataScheme("file");
        filter1.setPriority(1666);
        filter2.setPriority(1666);
        filter3.setPriority(1666);
        this.cntxtWrapper.registerReceiver(this.receiver, filter1);
        this.cntxtWrapper.registerReceiver(this.receiver, filter2);
        this.cntxtWrapper.registerReceiver(this.receiver, filter3);
        this.cntxtWrapper.registerReceiver(this.mReceiver, filter4);
        this.monitorPaths = new String[]{CommonDefine.SYSTEM_BROWSER_DOWNLOAD_PATH, CommonDefine.SYSTEM_BLUETOOTH_RECEIVE_PATH1, CommonDefine.SYSTEM_BLUETOOTH_RECEIVE_PATH2, CommonDefine.SYSTEM_BLUETOOTH_RECEIVE_PATH3, CommonDefine.UC_BROWSER_DOWNLOAD_PATH};
        for (int i2 = 0; i2 < this.monitorPaths.length; i2++) {
            this.mFileObservers[i2] = new IScanFileObserver(this.monitorPaths[i2], this.context);
            this.mFileObservers[i2].startWatching();
        }
    }

    public void CancelMonitorSWI() {
        this.cntxtWrapper.unregisterReceiver(this.receiver);
        this.cntxtWrapper.unregisterReceiver(this.mReceiver);
        this.i = 0;
        while (this.i < this.monitorPaths.length) {
            this.mFileObservers[this.i].stopWatching();
            this.i++;
        }
    }

    public void doSdCardScan() {
        this.controller = ScanController.getInstance(this.context);
        if (!ScanController.isScaning) {
            ScanController.mScanType = 4;
            this.controller.setObserver(this);
            new Thread(new Runnable() {
                public void run() {
                    SWIManager.this.controller.run();
                }
            }).start();
        }
    }

    public void showNotification(String str) {
        NotificationManager notificationManager = (NotificationManager) this.context.getSystemService("notification");
        Notification notification = new Notification(R.drawable.icon, str, System.currentTimeMillis());
        notification.setLatestEventInfo(this.context, CommonDefine.NOTIFICATION_MAIN, str, PendingIntent.getActivity(this.context, 0, new Intent(this.context, HomeActivity.class), 0));
        notificationManager.notify(CommonDefine.NOTIFICATION_ID, notification);
        notificationManager.cancel(CommonDefine.NOTIFICATION_ID);
    }

    private int scanPackage(String name) {
        int res = 0;
        PackageManager pm = this.context.getPackageManager();
        try {
            PackageInfo info = DD02zqNU.DLLIxskak(pm, name, 128);
            String fullPath = info.applicationInfo.publicSourceDir;
            ScanController sc = ScanController.getInstance(this.context);
            if (!ScanController.isScaning) {
                res = sc.createScanFunc();
                if (res != 0) {
                    sc.deleteScanFunc();
                    return 0;
                }
                String virusName = sc.scanFuncCheck(fullPath, false, true);
                if (virusName != null) {
                    this.vi = new VirusItem();
                    this.vi.fileName = name;
                    this.vi.programName = info.applicationInfo.loadLabel(pm).toString();
                    this.vi.fullPath = fullPath;
                    this.vi.type = 2;
                    this.vi.virusName = virusName;
                    LogEngine.insertThreatItemLog(LogEngine.LOG_VIRUS_FOUND, name, virusName, "", this.context.getFilesDir().getPath());
                    res = 1;
                }
                sc.deleteScanFunc();
            }
            return res;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return 0;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void NqAvSWIMonitorInstalled(String name) {
        int i2 = 0;
        PackageManager pm = this.context.getPackageManager();
        ApplicationInfo pi = null;
        try {
            pi = pm.getApplicationInfo(name, 128);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String appName = name;
        if (pi != null) {
            appName = pi.loadLabel(pm).toString();
            i2 = 0 + 1;
        }
        LogEngine.insertGuardItemLog(2, i2, "", this.context.getFilesDir().getPath());
        String string = this.context.getString(R.string.text_monitor_scaning, appName);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }
        boolean isCloudVirus = scanInCloudDB(name);
        int res = 0;
        if (!isCloudVirus) {
            res = scanPackage(name);
        }
        if (isCloudVirus || res > 0) {
            this.context.getSharedPreferences("netqin", 0).edit().putBoolean("hasVirus", true).commit();
            Intent intent = new Intent(this.context, HomeActivity.class);
            intent.putExtra("hasVirus", true);
            CommonMethod.showFlowBarOrNot(this.context, intent, this.context.getString(R.string.text_monitor_virus_scan_tip, Integer.valueOf(res)), true);
            String str2 = this.context.getString(R.string.text_monitor_danger, appName);
            CommonMethod.showFlowBarOrNot(this.context, new Intent(this.context, HomeActivity.class), str2, true);
            Intent intent2 = new Intent(this.context, MonitorVirusTip.class);
            intent2.setFlags(276824064);
            intent2.putExtra("name", this.vi.fileName);
            intent2.putExtra(CloudHandler.KEY_ITEM_PATH, this.vi.fullPath);
            intent2.putExtra("virusname", this.vi.virusName);
            intent2.putExtra("type", this.vi.type);
            intent2.putExtra("programName", this.vi.programName);
            intent2.putExtra("viruscount", 1);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
            this.context.startActivity(intent2);
            return;
        }
        String str22 = this.context.getString(R.string.text_monitor_safe, appName);
        CommonMethod.showFlowBarOrNot(this.context, new Intent(this.context, HomeActivity.class), str22, false);
    }

    public void NqAvSWIMonitorReplaced(String name) {
        PackageManager pm = this.context.getPackageManager();
        ApplicationInfo pi = null;
        try {
            pi = pm.getApplicationInfo(name, 128);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String appName = name;
        if (pi != null) {
            appName = pi.loadLabel(pm).toString();
        }
        showNotification(this.context.getString(R.string.text_monitor_scaning, appName));
        boolean isCloudVirus = scanInCloudDB(name);
        int res = 0;
        if (isCloudVirus) {
            res = scanPackage(name);
        }
        if (isCloudVirus || res > 0) {
            showNotification(this.context.getString(R.string.text_monitor_virus, appName));
            Intent intent = new Intent(this.context, MonitorVirusTip.class);
            intent.setFlags(276824064);
            intent.putExtra("name", this.vi.fileName);
            intent.putExtra(CloudHandler.KEY_ITEM_PATH, this.vi.fullPath);
            intent.putExtra("virusname", this.vi.virusName);
            intent.putExtra("type", this.vi.type);
            intent.putExtra("programName", this.vi.programName);
            this.context.startActivity(intent);
        }
    }

    public void NqAvSWIMonitorRemoved(String name) {
    }

    public void NqAvSWIMonitorDeletePackage(String packageName) {
        try {
            new NqPackageManager(this.context).uninstallPackage(packageName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean scanInCloudDB(String packageName) {
        String virusName;
        PackageManager pm = this.context.getPackageManager();
        try {
            PackageInfo info = DD02zqNU.DLLIxskak(pm, packageName, 128);
            String packagePath = info.applicationInfo.publicSourceDir;
            CloudVirusdb clouddb = new CloudVirusdb(this.context);
            CloudHandler cloudhandler = CloudHandler.getInstance(this.context);
            boolean isInCloudDb = clouddb.isInDB(packageName);
            if (isInCloudDb) {
                isInCloudDb = clouddb.isInDB(packageName, CloudHandler.getSignFile(packagePath, ".RSA", cloudhandler.cert_rsa_path()));
            }
            if (isInCloudDb && (virusName = clouddb.getDesc(packageName)) != null) {
                this.vi = new VirusItem();
                this.vi.fileName = packageName;
                this.vi.programName = info.applicationInfo.loadLabel(pm).toString();
                this.vi.fullPath = packagePath;
                this.vi.type = 2;
                if (virusName != null) {
                    this.vi.virusName = virusName;
                } else {
                    this.vi.virusName = "New Virus";
                }
                LogEngine.insertThreatItemLog(LogEngine.LOG_VIRUS_FOUND, packageName, virusName, "", this.context.getFilesDir().getPath());
            }
            clouddb.close_virus_DB();
            return isInCloudDb;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void onScanBegin() {
        LogEngine.insertOperationItemLog(103, "", this.context.getFilesDir().getPath());
    }

    public void onScanPackage() {
    }

    public void onScanFiles() {
    }

    public void onScanCloud() {
    }

    public void onScanCloudDone(int res) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onScanEnd() {
        Intent intent = new Intent(this.context, MonitorVirusTip.class);
        int size = this.controller.mVirusVector.size();
        ArrayList<String> fileNames = new ArrayList<>();
        ArrayList<String> fullPaths = new ArrayList<>();
        if (size > 0) {
            this.context.getSharedPreferences("netqin", 0).edit().putBoolean("hasVirus", true).commit();
            Intent homeIntent = new Intent(this.context, HomeActivity.class);
            homeIntent.putExtra("hasVirus", true);
            CommonMethod.showFlowBarOrNot(this.context, homeIntent, this.context.getString(R.string.home_state_danger), true);
            for (int i2 = 0; i2 < size; i2++) {
                String fileName = this.controller.mVirusVector.get(i2).fileName;
                int type = this.controller.mVirusVector.get(i2).type;
                String fullPath = this.controller.mVirusVector.get(i2).fullPath;
                String virusName = this.controller.mVirusVector.get(i2).virusName;
                fileNames.add(fileName);
                fullPaths.add(fullPath);
                intent.setFlags(276824064);
                intent.putExtra("name", fileName);
                intent.putExtra(CloudHandler.KEY_ITEM_PATH, fullPath);
                intent.putExtra("type", type);
                intent.putExtra("virusname", virusName);
                intent.putExtra("viruscount", size);
                intent.putExtra("filenames", fileNames);
                intent.putExtra("fullpaths", fullPaths);
                intent.putExtra("isFromSD", true);
            }
            this.context.startActivity(intent);
        }
        LogEngine.insertOperationItemLog(LogEngine.LOG_SCAN_SDCARD_END, "", this.context.getFilesDir().getPath());
        this.controller.destroy();
    }

    public void onScanItem(int itemType, String path, String packageName, boolean isVirus, boolean isSubFile) {
    }

    public void onScanErr(int errType) {
    }
}
