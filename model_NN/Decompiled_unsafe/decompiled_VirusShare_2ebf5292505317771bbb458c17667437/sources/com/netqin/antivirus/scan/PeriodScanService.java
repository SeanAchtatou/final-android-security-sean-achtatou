package com.netqin.antivirus.scan;

import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Binder;
import android.os.IBinder;
import android.preference.PreferenceManager;
import com.netqin.antivirus.cloud.model.CloudApkInfo;
import com.netqin.antivirus.common.CloudPassage;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.contact.vcard.VCardConfig;
import java.util.ArrayList;
import java.util.Calendar;

public class PeriodScanService extends Service implements IScanObserver {
    public static boolean isCloudSuccess;
    public static ScanController mScanController;
    public static int mScanCount = 0;
    public static int mScanSecond = 0;
    public static int mVirusNum = 0;
    long localScanEnd = 0;
    long localScanStart = 0;
    private final IBinder mBinder = new PeriodScanServiceBinder();

    public class PeriodScanServiceBinder extends Binder {
        public PeriodScanServiceBinder() {
        }

        /* access modifiers changed from: package-private */
        public PeriodScanService getService() {
            return PeriodScanService.this;
        }
    }

    public IBinder onBind(Intent arg0) {
        return this.mBinder;
    }

    public void onCreate() {
        super.onCreate();
    }

    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        new CloudPassage(this).clearEditor();
        startScan();
        int dayNum = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this).getString("regular_scans", "15"));
        if (dayNum > 0) {
            Calendar calendar = Calendar.getInstance();
            CommonMethod.setLastPeriodScanTime(this, calendar);
            calendar.add(6, dayNum);
            CommonMethod.setNextPeriodScanTime(this, calendar);
        }
    }

    private void startScan() {
        CommonMethod.putConfigWithIntegerValue(this, "netqin", "neverscan", 0);
        mVirusNum = 0;
        if (mScanController != null) {
            try {
                if (ScanController.isScaning) {
                    return;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        mScanController = ScanController.getInstance(this);
        mScanController.setObserver(this);
        ScanController.mScanType = 5;
        isCloudSuccess = false;
        this.localScanStart = System.currentTimeMillis();
        try {
            mScanController.start();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void onScanBegin() {
    }

    public void onScanCloud() {
    }

    public void onScanCloudDone(int res) {
    }

    public void onScanEnd() {
        if (mScanController != null) {
            ArrayList<CloudApkInfo> cloudApkInfoList = mScanController.mCloudApkInfoList;
            PackageManager packageManager = getPackageManager();
            if (cloudApkInfoList != null) {
                for (int i = 0; i < cloudApkInfoList.size(); i++) {
                    CloudApkInfo cai = cloudApkInfoList.get(i);
                    String packageName = cai.getPkgName();
                    boolean isVirusEngineFound = false;
                    int j = 0;
                    while (true) {
                        if (j >= mScanController.mVirusVector.size()) {
                            break;
                        }
                        VirusItem vItem = mScanController.mVirusVector.get(j);
                        if (vItem.type != 2) {
                            CommonMethod.saveDangerFileInfo(this, vItem.fullPath);
                        } else if (vItem.packageName.compareToIgnoreCase(packageName) == 0) {
                            CommonMethod.saveDangerPackageInfo(this, vItem.packageName);
                            isVirusEngineFound = true;
                            break;
                        }
                        j++;
                    }
                    if (!isVirusEngineFound && cai.getVirusName() != null && cai.getVirusName().length() > 0) {
                        mVirusNum++;
                    }
                }
            }
        }
        CloudPassage cloudPassage = new CloudPassage(this);
        cloudPassage.setCloudEndTime(CommonMethod.getDate());
        cloudPassage.writeCloudLogToFile();
        cloudPassage.clearEditor();
        if (mVirusNum > 0) {
            CommonMethod.putConfigWithIntegerValue(this, "netqin", "nodeletevirusnum", mVirusNum);
            this.localScanEnd = System.currentTimeMillis();
            mScanSecond = (int) ((this.localScanEnd - this.localScanStart) / 1000);
            Intent intent = new Intent(this, PeriodScanVirusTip.class);
            intent.addFlags(VCardConfig.FLAG_USE_QP_TO_PRIMARY_PROPERTIES);
            intent.setFlags(276824064);
            startActivity(intent);
        } else if (mScanController != null) {
            mScanController.destroy();
            mScanController = null;
        }
        ScanController.isScaning = false;
        stopSelf();
    }

    public void onScanErr(int errType) {
    }

    public void onScanFiles() {
    }

    public void onScanItem(int itemType, String path, String packageName, boolean isVirus, boolean isSubFile) {
        if (isVirus) {
            mVirusNum++;
        }
        mScanCount++;
    }

    public void onScanPackage() {
    }
}
