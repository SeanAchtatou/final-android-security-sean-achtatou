package com.netqin.antivirus.scan;

public class VirusItem {
    public static final int VIRUS_FILE = 1;
    public static final int VIRUS_PACKAGE = 2;
    public String description;
    public String fileName;
    public String fullPath;
    public String nickName;
    public String packageName;
    public String programName;
    public int type;
    public String virusName;
}
