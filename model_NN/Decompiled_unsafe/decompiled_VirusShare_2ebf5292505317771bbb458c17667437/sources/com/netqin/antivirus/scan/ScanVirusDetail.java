package com.netqin.antivirus.scan;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.netqin.antivirus.Preferences;
import com.netqin.antivirus.Value;
import com.netqin.antivirus.cloud.model.CloudHandler;
import com.netqin.antivirus.common.CommonMethod;
import com.nqmobile.antivirus_ampro20.R;
import java.io.File;

public class ScanVirusDetail extends Activity {
    String mFullPath;
    String mNickName;
    String mPackageName;
    int mType;
    String mVirusClass;
    String mVirusName;
    View vRemove;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.scan_virus_detail);
        setRequestedOrientation(1);
        Bundle bundle = getIntent().getExtras();
        this.mVirusName = bundle.getString("virusname");
        this.mNickName = bundle.getString("nickname");
        this.mVirusClass = bundle.getString("vclass");
        this.mFullPath = bundle.getString("fullpath");
        this.mType = bundle.getInt("type");
        if (this.mType == 1) {
            this.mPackageName = bundle.getString(CloudHandler.KEY_ITEM_PACKAGENAME);
            ((TextView) findViewById(R.id.scan_detail_delete_tv)).setText((int) R.string.label_uninstall);
        }
        ((TextView) findViewById(R.id.activity_name)).setText((int) R.string.scan_result_detail);
        ((TextView) findViewById(R.id.scan_detail_virus_name)).setText(String.valueOf(getResources().getString(R.string.scan_virus_detail_name)) + " " + this.mVirusName);
        TextView tvNickName = (TextView) findViewById(R.id.scan_detail_virus_nickname);
        if (this.mNickName == null || this.mNickName.length() <= 1) {
            tvNickName.setVisibility(8);
        } else {
            tvNickName.setText(String.valueOf(getResources().getString(R.string.scan_virus_detail_nickname)) + " " + this.mNickName);
        }
        ((TextView) findViewById(R.id.scan_detail_virus_class)).setText(String.valueOf(getResources().getString(R.string.scan_virus_detail_dangerclass)) + " " + this.mVirusClass);
        ((TextView) findViewById(R.id.scan_detail_virus_fullpath)).setText(String.valueOf(getResources().getString(R.string.scan_virus_detail_path)) + " " + this.mFullPath);
        this.vRemove = findViewById(R.id.scan_detail_delete);
        this.vRemove.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ScanVirusDetail.this.RemoveSelectItem();
            }
        });
        TextView tvDetail = (TextView) findViewById(R.id.scan_detail_virus_viewdetail);
        tvDetail.setText(Html.fromHtml("<a href='" + Value.View_Virus_Detail_Cn + "'>" + getString(R.string.virusdetail_viewmore) + "</a>"));
        tvDetail.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                String detailUrl;
                if (CommonMethod.isLocalSimpleChinese()) {
                    detailUrl = String.format(Value.View_Virus_Detail_Cn, ScanVirusDetail.this.mVirusName, Preferences.getPreferences(ScanVirusDetail.this).getChanelIdStore());
                } else {
                    detailUrl = String.format(Value.View_Virus_Detail_En, ScanVirusDetail.this.mVirusName, Preferences.getPreferences(ScanVirusDetail.this).getChanelIdStore());
                }
                ScanVirusDetail.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(detailUrl)));
            }
        });
    }

    public void onResume() {
        super.onResume();
        if (!new File(this.mFullPath).exists()) {
            ((TextView) findViewById(R.id.scan_detail_delete_tv)).setTextColor(-7829368);
            this.vRemove.setClickable(false);
            this.vRemove.setEnabled(false);
        }
    }

    public void onPause() {
        super.onPause();
    }

    public void onStop() {
        super.onStop();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: package-private */
    public void RemoveSelectItem() {
        if (this.mType == 0) {
            ScanResult.deleteOneFile(this.mFullPath);
            if (new File(this.mFullPath).exists()) {
                Toast.makeText(this, getString(R.string.text_virus_delete_fail), 1).show();
                return;
            }
            this.vRemove.setClickable(false);
            this.vRemove.setEnabled(false);
            ((TextView) findViewById(R.id.scan_detail_delete_tv)).setTextColor(-7829368);
            Toast.makeText(this, getString(R.string.text_virus_delete_success), 1).show();
            return;
        }
        ScanResult.uninstallPackage(this.mPackageName, this);
    }
}
