package com.netqin.antivirus.scan;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import com.netqin.antivirus.HomeActivity;
import com.netqin.antivirus.common.CommonDefine;
import com.nqmobile.antivirus_ampro20.R;

public class MonitorService extends Service {
    private NotificationManager nm;
    private SWIManager swiManager;

    public IBinder onBind(Intent i) {
        return null;
    }

    public class LocalBinder extends Binder {
        public LocalBinder() {
        }

        /* access modifiers changed from: package-private */
        public MonitorService getService() {
            return MonitorService.this;
        }
    }

    public boolean onUnbind(Intent i) {
        return false;
    }

    public void onRebind(Intent i) {
    }

    public void onCreate() {
        this.nm = (NotificationManager) getSystemService("notification");
        this.swiManager = new SWIManager(this, this);
        this.swiManager.beginMonitorSWI();
    }

    public void onStart(Intent intent, int startId) {
    }

    public void onDestroy() {
        this.swiManager.CancelMonitorSWI();
        this.nm.cancel(CommonDefine.NOTIFICATION_ID);
    }

    public static void showMonitorServiceNotification(String str, Context context) {
        NotificationManager manager = (NotificationManager) context.getSystemService("notification");
        Notification notification = new Notification(R.drawable.icon, str, System.currentTimeMillis());
        notification.setLatestEventInfo(context, context.getResources().getText(R.string.label_netqin_antivirus), str, PendingIntent.getActivity(context, 0, new Intent(context, HomeActivity.class), 0));
        manager.notify(CommonDefine.NOTIFICATION_ID, notification);
        manager.cancel(CommonDefine.NOTIFICATION_ID);
    }
}
