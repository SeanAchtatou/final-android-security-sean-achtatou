package com.netqin.antivirus.scan;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.netqin.antivirus.cloud.apkinfo.db.CloudApiInfoDb;
import com.netqin.antivirus.common.CloudPassage;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.filemanager.FileManagerActivity;
import com.netqin.antivirus.log.LogEngine;
import com.nqmobile.antivirus_ampro20.R;
import java.io.File;
import java.util.LinkedHashSet;
import java.util.Timer;
import java.util.TimerTask;

public class ScanActivity extends Activity implements IScanObserver, Animation.AnimationListener {
    public static boolean isCloudSuccess;
    public static boolean isScanAllDone = false;
    public static ScanController mScanController;
    public static volatile int scanLastSecond;
    public static volatile int scanedNum;
    private final int TIMER_INTERVAL = 250;
    int cloudCounter = 0;
    int cloudCounterMax = 3;
    int cloudCurrent = 0;
    int cloudNum = 0;
    int cloudSpare = 0;
    volatile String currentScanPath;
    private boolean isCancle;
    boolean isClouding = false;
    boolean isCustomScanPackage;
    boolean isScanEnd = false;
    boolean isUIEnd = false;
    LinkedHashSet<File> mCustomScanList;
    /* access modifiers changed from: private */
    public final Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (msg.what != 1) {
                return;
            }
            if (!ScanActivity.this.isUIEnd) {
                ScanActivity.this.updateUI();
            } else if (!ScanActivity.isScanAllDone) {
                ScanActivity.isScanAllDone = true;
                LogEngine.insertOperationItemLog(102, "", ScanActivity.this.getFilesDir().getPath());
                if (ScanActivity.this.mUpdateUiTimer != null) {
                    ScanActivity.this.mUpdateUiTimer.cancel();
                }
                CloudPassage cloudPassage = new CloudPassage(ScanActivity.this);
                cloudPassage.setCloudEndTime(CommonMethod.getDate());
                cloudPassage.writeCloudLogToFile();
                cloudPassage.clearEditor();
                ScanActivity.this.startActivity(new Intent(ScanActivity.this, ScanResult.class));
                ScanActivity.this.finish();
            }
        }
    };
    Animation mLeftAnim;
    View mLeftRunView;
    private ProgressBar mProgressBar;
    Animation mRightAnim;
    View mRightRunView;
    int mScanType = 2;
    private TextView mScandeVirusNum;
    private TextView mScanedItemNum;
    private TextView mScaningItem;
    private TextView mScaningTime;
    Timer mUpdateUiTimer;
    int needScanFileNum;
    int needScanPackageNum;
    volatile int updateTimes;
    volatile int virusNum;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.scan);
        setRequestedOrientation(1);
        this.mScanType = getIntent().getIntExtra("type", 2);
        TextView tvTitle = (TextView) findViewById(R.id.activity_name);
        if (this.mScanType == 3) {
            this.mCustomScanList = new LinkedHashSet<>();
            this.mCustomScanList.addAll(FileManagerActivity.filesScanList);
            this.isCustomScanPackage = FileManagerActivity.isPackageChecked;
            FileManagerActivity.filesScanList.clear();
            FileManagerActivity.isPackageChecked = false;
            tvTitle.setText((int) R.string.scan_type_cumstom);
        } else if (this.mScanType == 1) {
            tvTitle.setText((int) R.string.scan_type_all);
        } else if (this.mScanType == 2) {
            tvTitle.setText((int) R.string.scan_type_quick);
        }
        this.mProgressBar = (ProgressBar) findViewById(R.id.scan_progress);
        this.mProgressBar.setProgress(0);
        this.mScaningItem = (TextView) findViewById(R.id.scaning_item);
        this.mScaningTime = (TextView) findViewById(R.id.scaning_time);
        this.mScanedItemNum = (TextView) findViewById(R.id.scaned_item_num);
        this.mScandeVirusNum = (TextView) findViewById(R.id.scaned_virus_num);
        findViewById(R.id.scan_stop).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ScanActivity.this.stopScan();
            }
        });
        this.mLeftAnim = AnimationUtils.loadAnimation(this, R.anim.run_left_to_right);
        this.mRightAnim = AnimationUtils.loadAnimation(this, R.anim.run_right_to_left);
        this.mLeftAnim.setInterpolator(new LinearInterpolator());
        this.mRightAnim.setInterpolator(new LinearInterpolator());
        this.mLeftRunView = findViewById(R.id.img_scan_run_left);
        this.mRightRunView = findViewById(R.id.img_scan_run_right);
        this.mLeftRunView.setAnimation(this.mLeftAnim);
        this.mRightRunView.setAnimation(this.mRightAnim);
        this.mLeftAnim.setAnimationListener(this);
        this.mRightAnim.setAnimationListener(this);
        this.mLeftAnim.start();
        this.mRightAnim.start();
        startScan();
    }

    public void onResume() {
        super.onResume();
    }

    public void onPause() {
        super.onPause();
    }

    public void onStop() {
        super.onStop();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    private class UpdateUITask extends TimerTask {
        private UpdateUITask() {
        }

        /* synthetic */ UpdateUITask(ScanActivity scanActivity, UpdateUITask updateUITask) {
            this();
        }

        public void run() {
            Message message = new Message();
            message.what = 1;
            ScanActivity.this.mHandler.sendMessage(message);
        }
    }

    public void updateUI() {
        String strMinut;
        String strSecond;
        this.updateTimes = this.updateTimes + 1;
        if (this.updateTimes == 4) {
            scanLastSecond++;
            this.updateTimes = 0;
        }
        if (!this.isScanEnd) {
            if (this.needScanPackageNum + this.needScanFileNum + this.cloudNum > 0) {
                this.mProgressBar.setProgress(((scanedNum + this.cloudCurrent) * 100) / ((this.needScanPackageNum + this.needScanFileNum) + this.cloudNum));
            } else {
                this.mProgressBar.setProgress(0);
            }
            if (scanedNum >= this.needScanPackageNum + this.needScanFileNum) {
                this.cloudCurrent = this.cloudCurrent + (this.cloudNum / LogEngine.LOG_VIRUSDB_UPDATE_BEGIN);
                if (this.cloudCurrent > this.cloudNum) {
                    this.cloudCurrent = this.cloudNum;
                }
            }
        } else {
            if (this.cloudSpare == 0) {
                this.cloudSpare = (this.cloudNum - this.cloudCurrent) / 4;
            }
            if (this.cloudCounter < this.cloudCounterMax) {
                this.cloudCurrent = this.cloudCurrent + this.cloudSpare;
                this.cloudCounter = this.cloudCounter + 1;
            } else {
                this.cloudCurrent = this.cloudNum;
                this.isUIEnd = true;
            }
            if (this.needScanPackageNum + this.needScanFileNum + this.cloudNum != 0) {
                this.mProgressBar.setProgress(((scanedNum + this.cloudCurrent) * 100) / ((this.needScanPackageNum + this.needScanFileNum) + this.cloudNum));
            } else {
                this.mProgressBar.setProgress(100);
            }
        }
        if (this.isClouding) {
            this.mScaningItem.setText((int) R.string.scan_clouding);
        } else if (this.currentScanPath != null && this.currentScanPath.length() > 1) {
            this.mScaningItem.setText(TextUtils.ellipsize(String.valueOf(getResources().getString(R.string.scaning_item)) + " " + this.currentScanPath, this.mScaningItem.getPaint(), (float) ((this.mScaningItem.getWidth() - this.mScaningItem.getPaddingLeft()) - this.mScaningItem.getPaddingRight()), TextUtils.TruncateAt.MIDDLE));
        }
        int scanTimeMinut = scanLastSecond / 60;
        int scanTimeHour = 0;
        if (scanTimeMinut > 60) {
            scanTimeMinut %= 60;
            scanTimeHour = scanTimeMinut / 60;
        }
        int scanTimeSecond = scanLastSecond % 60;
        String strHour = "0" + scanTimeHour;
        if (scanTimeMinut > 9) {
            strMinut = new StringBuilder().append(scanTimeMinut).toString();
        } else {
            strMinut = "0" + scanTimeMinut;
        }
        if (scanTimeSecond > 9) {
            strSecond = new StringBuilder().append(scanTimeSecond).toString();
        } else {
            strSecond = "0" + scanTimeSecond;
        }
        this.mScaningTime.setText(String.valueOf(getResources().getString(R.string.scaning_time)) + " " + strHour + ":" + strMinut + ":" + strSecond);
        this.mScanedItemNum.setText(String.valueOf(getResources().getString(R.string.scaning_done_count)) + " " + scanedNum);
        this.mScandeVirusNum.setText(String.valueOf(getResources().getString(R.string.scaning_virus_count)) + " " + this.virusNum);
        if (this.virusNum > 0) {
            this.mScandeVirusNum.setTextColor(-65536);
        } else {
            this.mScandeVirusNum.setTextColor(-16777216);
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        stopScan();
        return true;
    }

    private void startScan() {
        LogEngine.insertOperationItemLog(100, "", getFilesDir().getPath());
        isScanAllDone = false;
        isCloudSuccess = false;
        scanedNum = 0;
        scanLastSecond = 0;
        this.mScaningItem.setText((int) R.string.scaning_prepare);
        mScanController = ScanController.getInstance(this);
        if (mScanController != null) {
            new CloudApiInfoDb(this).close_virus_DB();
            mScanController.destroy();
            mScanController = null;
            mScanController = ScanController.getInstance(this);
        }
        ScanController.isScaning = true;
        try {
            PeriodScanService.mScanController = null;
            SilentCloudScanService.mScanController = null;
            mScanController.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mScanController.setObserver(this);
        ScanController.mScanType = this.mScanType;
        if (this.mScanType == 3) {
            mScanController.mCustomScanList = this.mCustomScanList;
            mScanController.isCustomScanPackage = this.isCustomScanPackage;
        }
        isCloudSuccess = false;
        this.needScanPackageNum = 100;
        this.needScanFileNum = 100;
        scanedNum = 0;
        this.virusNum = 0;
        this.currentScanPath = null;
        scanLastSecond = 0;
        if (this.mUpdateUiTimer != null) {
            this.mUpdateUiTimer.cancel();
            this.mUpdateUiTimer = null;
        }
        this.mUpdateUiTimer = new Timer();
        this.mUpdateUiTimer.scheduleAtFixedRate(new UpdateUITask(this, null), 1, 250);
    }

    /* access modifiers changed from: private */
    public void stopScan() {
        if (!this.isCancle) {
            this.isCancle = true;
            LogEngine.insertOperationItemLog(101, "", getFilesDir().getPath());
            if (this.mUpdateUiTimer != null) {
                this.mUpdateUiTimer.cancel();
            }
            if (mScanController != null) {
                mScanController.setCanRun(false);
            }
            startActivity(new Intent(this, ScanResult.class));
            CloudPassage cloudPassage = new CloudPassage(this);
            cloudPassage.setScanCancelTag("1");
            cloudPassage.setCloudEndTime(CommonMethod.getDate());
            cloudPassage.writeCloudLogToFile();
            cloudPassage.clearEditor();
            finish();
        }
    }

    public void onScanBegin() {
        if (!this.isCancle) {
            this.needScanFileNum = mScanController.getScanFileTotalNum();
            if (!this.isCancle) {
                if (this.mScanType != 3) {
                    this.needScanPackageNum = mScanController.getScanAppTotalNum();
                    this.cloudNum = (this.needScanPackageNum + this.needScanFileNum) / 2;
                    if (this.cloudNum < 120) {
                        this.cloudNum = LogEngine.LOG_VIRUSDB_UPDATE_BEGIN;
                    }
                } else {
                    this.cloudNum = 0;
                    this.needScanPackageNum = 0;
                }
                CommonMethod.putConfigWithIntegerValue(this, "netqin", "neverscan", 0);
            }
        }
    }

    public void onScanEnd() {
        this.isScanEnd = true;
    }

    public void onScanErr(int errType) {
        this.isScanEnd = true;
    }

    public void onScanItem(int itemType, String path, String packageName, boolean isVirus, boolean isSubFile) {
        scanedNum++;
        if (itemType == 1) {
            this.currentScanPath = path;
        } else {
            this.currentScanPath = packageName;
        }
        if (isVirus) {
            this.virusNum++;
        }
    }

    public void onScanCloud() {
        this.isClouding = true;
    }

    public void onScanFiles() {
    }

    public void onScanPackage() {
    }

    public void onScanCloudDone(int res) {
        if (res == 0) {
            isCloudSuccess = true;
        } else {
            isCloudSuccess = false;
        }
    }

    public void onAnimationEnd(Animation arg0) {
    }

    public void onAnimationRepeat(Animation arg0) {
    }

    public void onAnimationStart(Animation arg0) {
    }
}
