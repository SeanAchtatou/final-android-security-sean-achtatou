package com.netqin.antivirus.scan;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.netqin.antivirus.NqUtil;
import com.netqin.antivirus.Preferences;
import com.netqin.antivirus.antimallink.BlockService;
import com.netqin.antivirus.filemanager.FileManagerActivity;
import com.netqin.antivirus.log.LogManageActivity;
import com.nqmobile.antivirus_ampro20.R;

public class ScanMain extends Activity {
    boolean mIsVirusMonitorOn = true;
    boolean mIsWebMonitorOn = true;
    SharedPreferences pManager;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.scan_main);
        setRequestedOrientation(1);
        this.pManager = PreferenceManager.getDefaultSharedPreferences(this);
        Preferences pf = new Preferences(this);
        this.mIsVirusMonitorOn = pf.getIsRunMonitor();
        this.mIsWebMonitorOn = pf.getIsRunWebBlock();
        ((TextView) findViewById(R.id.activity_name)).setText((int) R.string.act_name_virus_sacn);
        findViewById(R.id.scan_main_block_line1).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                String state = Environment.getExternalStorageState();
                if ("mounted".equals(state) || "mounted_ro".equals(state)) {
                    ScanMain.this.startActivity(FileManagerActivity.getLaunchIntent(ScanMain.this));
                } else {
                    Toast.makeText(ScanMain.this, (int) R.string.customscan_nosdcard, 1).show();
                }
            }
        });
        findViewById(R.id.scan_main_block_line2).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Intent intent = new Intent(ScanMain.this, ScanActivity.class);
                intent.putExtra("type", 1);
                ScanMain.this.startActivity(intent);
            }
        });
        findViewById(R.id.check_log).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ScanMain.this.startActivity(LogManageActivity.getLaunchIntent(ScanMain.this));
            }
        });
        findViewById(R.id.scan_main_block_line3).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ScanMain.this.mIsVirusMonitorOn = !ScanMain.this.mIsVirusMonitorOn;
                Preferences pf = new Preferences(ScanMain.this);
                pf.setRunMonitor(ScanMain.this.mIsVirusMonitorOn);
                Intent i = new Intent("android.intent.action.RUN");
                i.putExtra("from", "homeactivity");
                i.setClass(ScanMain.this, MonitorService.class);
                if (ScanMain.this.mIsVirusMonitorOn) {
                    ScanMain.this.startService(i);
                    ScanMain.this.pManager.edit().putBoolean("time_protection", pf.getIsRunMonitor()).commit();
                } else {
                    ScanMain.this.stopService(i);
                    ScanMain.this.pManager.edit().putBoolean("time_protection", pf.getIsRunMonitor()).commit();
                }
                ScanMain.this.setVirusMonitorButtonState();
            }
        });
        findViewById(R.id.scan_main_block_line4).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ScanMain.this.mIsWebMonitorOn = !ScanMain.this.mIsWebMonitorOn;
                Preferences pf = new Preferences(ScanMain.this);
                pf.setRunWebBlock(ScanMain.this.mIsWebMonitorOn);
                Intent i = new Intent("android.intent.action.RUN");
                i.setClass(ScanMain.this, BlockService.class);
                if (ScanMain.this.mIsWebMonitorOn) {
                    ScanMain.this.startService(i);
                    ScanMain.this.pManager.edit().putBoolean("malicious_website_intercept", pf.getIsRunWebBlock()).commit();
                } else {
                    ScanMain.this.stopService(i);
                    ScanMain.this.pManager.edit().putBoolean("malicious_website_intercept", pf.getIsRunWebBlock()).commit();
                }
                ScanMain.this.setWebMonitorButtonState();
            }
        });
        setVirusMonitorButtonState();
        setWebMonitorButtonState();
    }

    public void onResume() {
        super.onResume();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.addvice_feedback_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.updatedb_advice_feedback /*2131558847*/:
                NqUtil.clickAdviceFeedback(this);
                return true;
            default:
                return true;
        }
    }

    public void onPause() {
        super.onPause();
    }

    public void onStop() {
        super.onStop();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: package-private */
    public void setVirusMonitorButtonState() {
        ImageView ali = (ImageView) findViewById(R.id.scan_main_block_line3_img);
        ImageView alonoff = (ImageView) findViewById(R.id.scan_main_block_line3_onoff);
        if (this.mIsVirusMonitorOn) {
            ali.setImageResource(R.drawable.virus_monitor_on);
            alonoff.setImageResource(R.drawable.state_on);
            return;
        }
        ali.setImageResource(R.drawable.virus_monitor_off);
        alonoff.setImageResource(R.drawable.state_off);
    }

    /* access modifiers changed from: package-private */
    public void setWebMonitorButtonState() {
        ImageView ali = (ImageView) findViewById(R.id.scan_main_block_line4_img);
        ImageView alonoff = (ImageView) findViewById(R.id.scan_main_block_line4_onoff);
        if (this.mIsWebMonitorOn) {
            ali.setImageResource(R.drawable.web_monitor_on);
            alonoff.setImageResource(R.drawable.state_on);
            return;
        }
        ali.setImageResource(R.drawable.web_monitor_off);
        alonoff.setImageResource(R.drawable.state_off);
    }
}
