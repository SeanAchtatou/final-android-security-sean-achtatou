package com.netqin.antivirus.scan;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import com.netqin.antivirus.HomeActivity;
import com.netqin.antivirus.cloud.apkinfo.db.CloudApiInfoDb;
import com.netqin.antivirus.cloud.model.CloudApkInfo;
import com.netqin.antivirus.cloud.model.CloudHandler;
import com.netqin.antivirus.cloud.view.CloudMonitorVirusTip;
import com.netqin.antivirus.common.CloudPassage;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.contact.vcard.VCardConfig;
import com.nqmobile.antivirus_ampro20.R;
import java.util.ArrayList;
import java.util.List;

public class SilentCloudScanService extends Service implements IScanObserver {
    public static ScanController mScanController;
    public static int mVirusNum = 0;
    public static List<String> newinstallapkpathArray = new ArrayList();
    private static Object obj = new Object();
    private Handler handler = null;
    boolean isCloudSuccess = false;
    boolean isFoundVirus = false;
    private String latestUpdtPackage = "";
    private Context mContext = null;
    int retryNum = 0;
    private Thread thread = null;

    public IBinder onBind(Intent arg0) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        this.mContext = this;
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.thread != null) {
            this.thread.stop();
        }
        if (this.handler != null) {
            this.handler.removeCallbacksAndMessages(obj);
        }
    }

    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        this.retryNum = 0;
        if (intent != null) {
            this.latestUpdtPackage = intent.getStringExtra(CloudHandler.KEY_ITEM_PACKAGENAME);
            boolean isRe = false;
            if (newinstallapkpathArray == null) {
                newinstallapkpathArray = new ArrayList();
            }
            if (newinstallapkpathArray.size() > 0) {
                for (String str : newinstallapkpathArray) {
                    if (str.equalsIgnoreCase(this.latestUpdtPackage)) {
                        isRe = true;
                    }
                }
                if (!isRe) {
                    newinstallapkpathArray.add(this.latestUpdtPackage);
                }
            } else if (newinstallapkpathArray.size() == 0) {
                newinstallapkpathArray.add(this.latestUpdtPackage);
            }
        }
        String str2 = this.latestUpdtPackage;
        if (this.handler == null) {
            this.handler = new Handler();
        }
        if (!ScanController.isScaning) {
            scheduleScan();
        }
    }

    /* access modifiers changed from: private */
    public void scheduleScan() {
        this.handler.removeCallbacksAndMessages(obj);
        this.retryNum++;
        if (mScanController != null) {
            mScanController.destroy();
            mScanController = null;
        }
        if (this.retryNum > 3) {
            this.retryNum = 0;
        } else {
            this.handler.postAtTime(new Runnable() {
                public void run() {
                    if (!SilentCloudScanService.this.checkNet()) {
                        SilentCloudScanService.this.scheduleScan();
                        return;
                    }
                    SilentCloudScanService.mScanController = ScanController.getInstance(SilentCloudScanService.this);
                    if (!ScanController.isScaning) {
                        SilentCloudScanService.this.startScan();
                    }
                }
            }, obj, SystemClock.uptimeMillis() + 1800000);
        }
    }

    /* access modifiers changed from: private */
    public void startScan() {
        if (this.thread != null) {
            this.thread.stop();
        }
        if (this.handler != null) {
            this.handler.removeCallbacksAndMessages(obj);
        }
        mScanController = ScanController.getInstance(this);
        if (!ScanController.isScaning && !ScanController.isScaning) {
            if (mScanController != null) {
                new CloudApiInfoDb(this).close_virus_DB();
                mScanController.destroy();
                mScanController = null;
                mScanController = ScanController.getInstance(this);
            }
            mScanController.setObserver(this);
            ScanController.mScanType = 6;
            mScanController.mSilentScanPackageNameList = newinstallapkpathArray;
            this.isCloudSuccess = false;
            mScanController.start();
        }
    }

    /* access modifiers changed from: private */
    public boolean checkNet() {
        NetworkInfo info = ((ConnectivityManager) getSystemService("connectivity")).getActiveNetworkInfo();
        if (info == null || !info.isConnected()) {
            return false;
        }
        return true;
    }

    public void onScanBegin() {
    }

    public void onScanCloud() {
    }

    public void onScanCloudDone(int res) {
        if (res == 0) {
            this.isCloudSuccess = true;
        } else {
            this.isCloudSuccess = false;
        }
    }

    public void onScanEnd() {
        mVirusNum = 0;
        try {
            if (mScanController != null) {
                ArrayList<CloudApkInfo> cloudApkInfoList = mScanController.mCloudApkInfoList;
                PackageManager pm = getPackageManager();
                for (int i = 0; i < cloudApkInfoList.size(); i++) {
                    CloudApkInfo cai = cloudApkInfoList.get(i);
                    String packageName = cai.getPkgName();
                    if ((cai.getVirusName() != null && cai.getVirusName().length() > 0) || "10".equalsIgnoreCase(cai.getSecurity())) {
                        CommonMethod.saveDangerPackageInfo(this, packageName);
                        CommonMethod.showFlowBarOrNot(this.mContext, new Intent(this.mContext, HomeActivity.class), this.mContext.getString(R.string.text_monitor_danger, pm.getApplicationInfo(packageName, 1).loadLabel(pm)), true);
                        mVirusNum++;
                    }
                }
                CloudPassage cloudPassage = new CloudPassage(this);
                cloudPassage.setCloudEndTime(CommonMethod.getDate());
                cloudPassage.writeCloudLogToFile();
                cloudPassage.clearEditor();
                if (mVirusNum <= 0) {
                    mScanController.destroy();
                    mScanController = null;
                } else {
                    CommonMethod.putConfigWithIntegerValue(this, "netqin", "nodeletevirusnum", mVirusNum);
                    Intent intent = new Intent(this, CloudMonitorVirusTip.class);
                    intent.addFlags(VCardConfig.FLAG_USE_QP_TO_PRIMARY_PROPERTIES);
                    intent.setFlags(276824064);
                    startActivity(intent);
                }
                ScanController.isScaning = false;
                stopSelf();
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void onScanErr(int errType) {
    }

    public void onScanFiles() {
    }

    public void onScanItem(int itemType, String path, String packageName, boolean isVirus, boolean isSubFile) {
        if (isVirus) {
            this.isFoundVirus = true;
        }
    }

    public void onScanPackage() {
    }
}
