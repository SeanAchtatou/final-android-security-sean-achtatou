package com.netqin.antivirus.scan;

public interface IScanObserver {
    public static final int ERROR_TYPE_SERVER = 4;
    public static final int ERROR_TYPE_XML = 5;
    public static final int ERR_CLOUD = 2;
    public static final int ERR_CLOUD_NO_NET = 3;
    public static final int ERR_TYPE_LOAD_FUNC_FAIL = 1;
    public static final int TYPE_FILE = 1;
    public static final int TYPE_PACKAGE = 2;

    void onScanBegin();

    void onScanCloud();

    void onScanCloudDone(int i);

    void onScanEnd();

    void onScanErr(int i);

    void onScanFiles();

    void onScanItem(int i, String str, String str2, boolean z, boolean z2);

    void onScanPackage();
}
