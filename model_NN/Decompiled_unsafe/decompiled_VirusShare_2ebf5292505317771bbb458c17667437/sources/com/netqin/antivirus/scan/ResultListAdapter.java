package com.netqin.antivirus.scan;

import android.content.Context;
import android.content.pm.PackageManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import com.netqin.antivirus.data.PackageElement;
import com.nqmobile.antivirus_ampro20.R;
import java.util.ArrayList;
import java.util.Iterator;

public class ResultListAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<ResultItem> mData;
    private LayoutInflater mInflater;
    private PackageManager mPackageManager;
    private View vRemove;

    public ResultListAdapter(Context context, ArrayList<ResultItem> data, View v) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.mContext = context;
        this.mPackageManager = context.getPackageManager();
        this.vRemove = v;
    }

    public synchronized ArrayList<ResultItem> getAll() {
        return this.mData;
    }

    public synchronized ArrayList<ResultItem> getSelected() {
        ArrayList<ResultItem> arrayList;
        if (this.mData == null || getCount() == 0) {
            arrayList = null;
        } else {
            ArrayList<ResultItem> selected = new ArrayList<>(getCount());
            Iterator<ResultItem> it = this.mData.iterator();
            while (it.hasNext()) {
                ResultItem item = it.next();
                if (item.isChecked) {
                    selected.add(item);
                }
            }
            arrayList = selected;
        }
        return arrayList;
    }

    public synchronized int getCount() {
        return this.mData.size();
    }

    public synchronized Object getItem(int position) {
        return this.mData.get(position);
    }

    public synchronized long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = this.mInflater.inflate((int) R.layout.scan_result_list_item, (ViewGroup) null);
            holder = new ViewHolder();
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.checkBox = (CheckBox) convertView.findViewById(R.id.result_item_checkbox);
        holder.iconView = (ImageView) convertView.findViewById(R.id.result_item_icon);
        holder.iconSign = (ImageView) convertView.findViewById(R.id.result_item_icon_sign);
        holder.titleView = (TextView) convertView.findViewById(R.id.result_item_title);
        holder.subTitleView = (TextView) convertView.findViewById(R.id.result_item_subtitle);
        holder.rankView = (RatingBar) convertView.findViewById(R.id.virus_list_ratingbar);
        holder.deletedView = (TextView) convertView.findViewById(R.id.result_item_delete_done);
        final ResultItem item = this.mData.get(position);
        if (item.type == 0) {
            holder.titleView.setText(item.fileName);
            holder.iconView.setImageResource(R.drawable.file_virus);
            holder.subTitleView.setText(item.virusName);
            holder.subTitleView.setTextColor(-65536);
            holder.rankView.setVisibility(8);
            holder.iconSign.setVisibility(8);
        } else {
            if (item.isNativeEngineVirus) {
                holder.iconView.setImageResource(R.drawable.file_virus);
                holder.iconSign.setVisibility(8);
            } else {
                holder.iconView.setImageDrawable(item.icon);
                holder.iconSign.setVisibility(0);
            }
            if (item.virusName == null || item.virusName.length() <= 0) {
                holder.titleView.setText(item.programName);
                holder.subTitleView.setText(item.securityDesc);
                holder.subTitleView.setTextColor(-16777216);
                if (item.security == null) {
                    item.isChecked = false;
                    holder.iconSign.setVisibility(4);
                } else if (item.security.intValue() >= 40) {
                    Float f = Float.valueOf(0.0f);
                    if (item.score != null) {
                        f = Float.valueOf(item.score);
                    }
                    holder.rankView.setRating(f.floatValue());
                    holder.rankView.setVisibility(0);
                    holder.iconSign.setImageResource(R.drawable.scan_safe_sign);
                } else if (item.security.intValue() >= 20) {
                    holder.iconSign.setImageResource(R.drawable.scan_unkown_sign);
                } else if (item.security.intValue() > 0) {
                    holder.iconSign.setImageResource(R.drawable.scan_danger_sign);
                }
            } else {
                holder.titleView.setText(item.programName);
                holder.subTitleView.setText(item.virusName);
                holder.subTitleView.setTextColor(-65536);
                holder.iconSign.setImageResource(R.drawable.scan_danger_sign);
                holder.rankView.setVisibility(8);
            }
        }
        if (item.isDeleted) {
            holder.deletedView.setVisibility(0);
            holder.checkBox.setVisibility(8);
        } else {
            holder.deletedView.setVisibility(8);
            holder.checkBox.setVisibility(0);
            holder.checkBox.setChecked(item.isChecked);
            holder.checkBox.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (holder.checkBox.isChecked()) {
                        item.isChecked = true;
                    } else {
                        item.isChecked = false;
                    }
                    ResultListAdapter.this.changeRemoveStatus();
                }
            });
        }
        return convertView;
    }

    public synchronized void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        changeRemoveStatus();
    }

    private void initChecked() {
    }

    static class ViewHolder {
        CheckBox checkBox;
        TextView deletedView;
        ImageView iconSign;
        ImageView iconView;
        RatingBar rankView;
        TextView subTitleView;
        TextView titleView;

        ViewHolder() {
        }
    }

    public synchronized void remove(int i) {
        this.mData.remove(i);
    }

    public void remove(PackageElement app) {
        remove(app, false);
    }

    public synchronized void remove(PackageElement app, boolean notify) {
        this.mData.remove(app);
        if (notify) {
            notifyDataSetChanged();
        }
    }

    /* access modifiers changed from: private */
    public void changeRemoveStatus() {
        TextView tv = (TextView) ((LinearLayout) this.vRemove).getChildAt(0);
        if (getSelected() == null || getSelected().size() <= 0) {
            this.vRemove.setEnabled(false);
            this.vRemove.setClickable(false);
            tv.setTextColor(-7829368);
            return;
        }
        this.vRemove.setEnabled(true);
        this.vRemove.setClickable(true);
        tv.setTextColor(-1);
    }
}
