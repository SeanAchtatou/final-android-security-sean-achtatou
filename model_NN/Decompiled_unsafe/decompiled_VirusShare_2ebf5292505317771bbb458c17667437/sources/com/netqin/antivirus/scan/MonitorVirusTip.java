package com.netqin.antivirus.scan;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;
import com.netqin.antivirus.HomeActivity;
import com.netqin.antivirus.antivirus.NqPackageManager;
import com.netqin.antivirus.cloud.model.CloudHandler;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.log.LogEngine;
import com.nqmobile.antivirus_ampro20.R;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

public class MonitorVirusTip extends Activity {
    private static final int SCAN_VIRUS_FILE_MORE = 2;
    private static final int SCAN_VIRUS_FILE_ONE = 1;
    private static final int SCAN_VIRUS_PACKAGE_INSTALLED = 0;
    ArrayList<String> fileNames;
    ArrayList<String> fullPaths;
    Intent homeIntent;
    boolean isDeletingPackage;
    boolean isFromSD;
    boolean isPaused;
    private String strTip;
    VirusItem vi;
    int virusCount;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        this.vi = new VirusItem();
        Intent intent = getIntent();
        this.vi.type = intent.getIntExtra("type", -1);
        this.vi.fileName = intent.getStringExtra("name");
        this.vi.fullPath = intent.getStringExtra(CloudHandler.KEY_ITEM_PATH);
        this.vi.virusName = intent.getStringExtra("virusname");
        this.vi.programName = intent.getStringExtra("programName");
        this.virusCount = intent.getIntExtra("viruscount", 0);
        this.fileNames = intent.getStringArrayListExtra("filenames");
        this.fullPaths = intent.getStringArrayListExtra("fullpaths");
        this.isFromSD = intent.getBooleanExtra("isFromSD", false);
        this.homeIntent = new Intent(this, HomeActivity.class);
        if (this.vi.type == 2) {
            this.strTip = getResources().getString(R.string.text_monitor_virus_delete_tip, this.vi.programName, this.vi.virusName, this.vi.fullPath);
            CommonMethod.saveDangerPackageInfo(this, this.vi.fileName);
            showDialog(0);
        } else if (this.vi.type == 1) {
            if (!this.isFromSD) {
                this.strTip = getResources().getString(R.string.text_monitor_file_delete_tip, this.vi.virusName, this.vi.fullPath);
                CommonMethod.saveDangerFileInfo(this, this.vi.fullPath);
                showDialog(1);
            } else if (this.virusCount == 1) {
                this.strTip = getResources().getString(R.string.text_monitor_file_delete_tip, this.vi.virusName, this.vi.fullPath);
                CommonMethod.saveDangerFileInfo(this, this.vi.fullPath);
                showDialog(1);
            } else {
                StringBuffer sb1 = new StringBuffer();
                StringBuffer sb2 = new StringBuffer();
                for (int i = 1; i <= this.fileNames.size(); i++) {
                    sb1.append(String.valueOf(i) + ". " + this.fileNames.get(i - 1));
                    sb1.append("\n");
                }
                sb1.deleteCharAt(sb1.length() - 1);
                Iterator<String> it = this.fullPaths.iterator();
                while (it.hasNext()) {
                    String path = it.next();
                    CommonMethod.saveDangerFileInfo(this, path);
                    sb2.append(path);
                    sb2.append(",");
                }
                sb2.deleteCharAt(sb2.length() - 1);
                this.strTip = getResources().getString(R.string.text_monitor_virus_delete_tip_2, Integer.valueOf(this.virusCount), sb1, sb2);
                showDialog(2);
            }
        }
        CommonMethod.putConfigWithIntegerValue(this, "netqin", "neverscan", 0);
        CommonMethod.putConfigWithIntegerValue(this, "netqin", "nodeletevirusnum", this.virusCount);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        DialogInterface.OnClickListener uninstalListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                MonitorVirusTip.this.DeletePackage(MonitorVirusTip.this.vi.fileName);
                new Intent().setClass(MonitorVirusTip.this, HomeActivity.class);
                CommonMethod.putConfigWithIntegerValue(MonitorVirusTip.this, "netqin", "nodeletevirusnum", CommonMethod.getConfigWithIntegerValue(MonitorVirusTip.this, "netqin", "nodeletevirusnum", 0) - 1);
                MonitorVirusTip.this.finish();
            }
        };
        DialogInterface.OnClickListener deleteListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                if (MonitorVirusTip.this.isFromSD) {
                    Iterator<String> it = MonitorVirusTip.this.fullPaths.iterator();
                    while (it.hasNext()) {
                        File f = new File(it.next());
                        if (f.exists()) {
                            f.delete();
                        }
                    }
                } else {
                    File file = new File(MonitorVirusTip.this.vi.fullPath);
                    if (file.exists()) {
                        file.delete();
                    }
                }
                Toast.makeText(MonitorVirusTip.this, MonitorVirusTip.this.getString(R.string.text_monitor_file_has_deleted), 0).show();
                CommonMethod.showFlowBarOrNot(MonitorVirusTip.this.getApplicationContext(), MonitorVirusTip.this.homeIntent, MonitorVirusTip.this.getString(R.string.netqin_mobile_security), false);
                int count = CommonMethod.getConfigWithIntegerValue(MonitorVirusTip.this, "netqin", "nodeletevirusnum", 0);
                CommonMethod.putConfigWithIntegerValue(MonitorVirusTip.this, "netqin", "nodeletevirusnum", count - MonitorVirusTip.this.virusCount < 0 ? 0 : count - MonitorVirusTip.this.virusCount);
                new Intent().setClass(MonitorVirusTip.this, HomeActivity.class);
                MonitorVirusTip.this.finish();
            }
        };
        DialogInterface.OnClickListener cancelListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                MonitorVirusTip.this.finish();
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        switch (id) {
            case 0:
                builder.setTitle((int) R.string.label_netqin_antivirus);
                builder.setMessage(this.strTip);
                builder.setPositiveButton((int) R.string.text_monitor_virus_uninstall, uninstalListener);
                builder.setNegativeButton((int) R.string.label_cancel, cancelListener);
                return builder.create();
            case 1:
                builder.setTitle((int) R.string.label_netqin_antivirus);
                builder.setMessage(this.strTip);
                builder.setPositiveButton((int) R.string.text_sdcard_scan_virus_delete, deleteListener);
                builder.setNegativeButton((int) R.string.label_cancel, cancelListener);
                return builder.create();
            case 2:
                builder.setTitle((int) R.string.label_netqin_antivirus);
                builder.setMessage(this.strTip);
                builder.setPositiveButton((int) R.string.text_dialog_deal, deleteListener);
                builder.setNegativeButton((int) R.string.text_dialog_cancel, cancelListener);
                return builder.create();
            default:
                return super.onCreateDialog(id);
        }
    }

    public void onResume() {
        super.onResume();
        if (this.isDeletingPackage) {
            LogEngine logEngine = new LogEngine();
            logEngine.openDB(getFilesDir().getPath());
            if (new File(this.vi.fullPath).exists()) {
                if (this.vi.type == 2) {
                    logEngine.insertOperationItem(112, this.vi.virusName);
                }
            } else if (this.vi.type == 2) {
                logEngine.insertOperationItem(111, this.vi.virusName);
            }
            logEngine.closeDB();
            this.isDeletingPackage = false;
            finish();
        }
    }

    public void onPause() {
        super.onPause();
        this.isPaused = true;
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void DeletePackage(String packageName) {
        try {
            new NqPackageManager(this).uninstallPackage(packageName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.isDeletingPackage = true;
    }
}
