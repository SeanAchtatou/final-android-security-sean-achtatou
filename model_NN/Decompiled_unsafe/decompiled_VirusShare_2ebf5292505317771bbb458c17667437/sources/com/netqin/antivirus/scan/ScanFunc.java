package com.netqin.antivirus.scan;

public class ScanFunc {
    IScanFuncObserver mObserver = null;

    public static native float nativeVersion();

    public native int avEngineCheckFile(String str, int i);

    public native int avEngineEnd(int i);

    public native int avEngineLoad(String str, String str2, String str3, String str4, int i);

    public native int avEngineSetTempDirInfo(int i, int i2);

    public native int avEngineUpdateDB(String str);

    public native String avEngineVirusDescription();

    public native String avEngineVirusName();

    public native String avEngineVirusNickName();

    public native int isAvEngineLoad(int i);

    static {
        System.loadLibrary("netqinantivirus");
    }

    ScanFunc(IScanFuncObserver observer) {
        this.mObserver = observer;
    }

    public void HandleScanSubFile(String fileName) {
        if (this.mObserver != null) {
            this.mObserver.onScanSubFile(fileName);
        }
    }

    public void HandleDecompressedSubFile(String fileName) {
        if (this.mObserver != null) {
            this.mObserver.onDecompressSubFile(fileName);
        }
    }
}
