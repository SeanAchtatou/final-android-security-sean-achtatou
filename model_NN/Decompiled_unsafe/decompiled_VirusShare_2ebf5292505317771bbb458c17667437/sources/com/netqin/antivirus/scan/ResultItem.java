package com.netqin.antivirus.scan;

import android.graphics.drawable.Drawable;

public class ResultItem {
    public String fileName;
    public String filePath;
    public Drawable icon;
    public int icon_res_id;
    public boolean isChecked = false;
    public boolean isDeleted = false;
    public boolean isNativeEngineVirus = false;
    public String nickName;
    public String packageName;
    public String programName;
    public String score;
    public Integer security;
    public String securityDesc;
    public String serverId;
    public int type;
    public String virusDesc;
    public String virusName;
}
