package com.netqin.antivirus.scan;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import com.netqin.antivirus.HomeActivity;
import com.netqin.antivirus.cloud.model.CloudApkInfo;
import com.netqin.antivirus.common.CommonMethod;
import com.nqmobile.antivirus_ampro20.R;
import java.util.ArrayList;
import java.util.Vector;

public class PeriodScanVirusTip extends Activity {
    public static ArrayList<CloudApkInfo> mCloudApkInfoList;
    public static int mScanSecond;
    public static int mVirusNum;
    public static Vector<VirusItem> mVirusVector;
    public static int scanedNum;

    public void onCreate(Bundle savedInstanceState) {
        mVirusVector = PeriodScanService.mScanController.mVirusVector;
        mCloudApkInfoList = PeriodScanService.mScanController.mCloudApkInfoList;
        mVirusNum = PeriodScanService.mVirusNum;
        mScanSecond = PeriodScanService.mScanSecond;
        scanedNum = PeriodScanService.mScanCount;
        PeriodScanService.mScanController.destroy();
        PeriodScanService.mScanController = null;
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        DialogInterface.OnClickListener listenerYes = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent i = new Intent(PeriodScanVirusTip.this, ScanResult.class);
                i.putExtra("from", ScanResult.FROM_PERIOD_SCAN);
                PeriodScanVirusTip.this.startActivity(i);
                PeriodScanVirusTip.this.finish();
            }
        };
        DialogInterface.OnClickListener listenerNo = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                PeriodScanVirusTip.this.finish();
            }
        };
        CommonMethod.showFlowBarOrNot(this, new Intent(this, HomeActivity.class), getString(R.string.period_virus_tip_notification, new Object[]{Integer.valueOf(mVirusNum)}), true);
        CommonMethod.putConfigWithIntegerValue(this, "netqin", "nodeletevirusnum", mVirusNum + CommonMethod.getConfigWithIntegerValue(this, "netqin", "nodeletevirusnum", 0));
        CommonMethod.lookVirusDialogListen(this, listenerYes, listenerNo, getResources().getString(R.string.period_virus_tip, Integer.valueOf(mVirusNum)), R.string.label_netqin_antivirus);
    }

    public void onResume() {
        super.onResume();
    }

    public void onPause() {
        super.onPause();
    }

    public void onDestroy() {
        super.onDestroy();
    }
}
