package com.netqin.antivirus.scan;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class SWIReceiver extends BroadcastReceiver {
    Context context;
    private ISWIMonitorObserver monitorObserver;

    public SWIReceiver(ISWIMonitorObserver observer) {
        this.monitorObserver = observer;
    }

    public void onReceive(Context context2, Intent intent) {
        this.context = context2;
        String packageName = intent.getData().getSchemeSpecificPart();
        if ("android.intent.action.PACKAGE_ADDED".equals(intent.getAction())) {
            this.monitorObserver.NqAvSWIMonitorInstalled(packageName);
        } else if (!"android.intent.action.PACKAGE_CHANGED".equals(intent.getAction()) && !"android.intent.action.PACKAGE_REPLACED".equals(intent.getAction()) && "android.intent.action.PACKAGE_REMOVED".equals(intent.getAction())) {
            this.monitorObserver.NqAvSWIMonitorRemoved(packageName);
        }
    }
}
