package com.netqin.antivirus.scan;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Environment;
import android.os.StatFs;
import android.text.TextUtils;
import com.netqin.antivirus.Preferences;
import com.netqin.antivirus.Value;
import com.netqin.antivirus.cloud.model.CloudApkInfo;
import com.netqin.antivirus.cloud.model.CloudHandler;
import com.netqin.antivirus.common.CloudPassage;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.log.LogEngine;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Vector;

public class ScanController extends Thread implements IScanFuncObserver {
    public static final int SCAN_ALL = 1;
    public static final int SCAN_CUSTOM = 3;
    public static final int SCAN_PERIOD = 5;
    public static final int SCAN_QUICK = 2;
    public static final int SCAN_SDCARD_APK = 4;
    public static final int SCAN_SILENT = 6;
    private static ScanController instance = null;
    public static boolean isScaning = false;
    public static int mScanType = 2;
    boolean isCustomScanPackage;
    LogEngine logEngine;
    volatile boolean mCanRun;
    public ArrayList<CloudApkInfo> mCloudApkInfoList;
    CloudController mCloudController;
    Context mContext;
    CloudApkInfo mCurrentApkinfo;
    LinkedHashSet<File> mCustomScanList;
    FileEnumerator mFileEnumerator;
    IScanObserver mObserver;
    ScanFunc mScanFunc;
    public List<String> mSilentScanPackageNameList = new ArrayList();
    public Vector<VirusItem> mVirusVector;

    public static synchronized ScanController getInstance(Context context) {
        ScanController scanController;
        synchronized (ScanController.class) {
            if (instance == null) {
                instance = new ScanController(context);
            }
            scanController = instance;
        }
        return scanController;
    }

    private ScanController(Context context) {
        super("nqscan");
        this.mContext = context;
        this.mCanRun = true;
    }

    public void setObserver(IScanObserver observer) {
        this.mObserver = observer;
    }

    public void run() {
        if (this.mCanRun) {
            this.mCanRun = true;
            if (mScanType != 4) {
                doScan();
            } else {
                doScanSDCardApk();
            }
        }
    }

    public void setCanRun(boolean can) {
        this.mCanRun = can;
        if (this.mCloudController != null) {
            this.mCloudController.setCanRun(can);
        }
    }

    public boolean IsCanRun() {
        return this.mCanRun;
    }

    public int init() {
        this.mFileEnumerator = new FileEnumerator();
        this.mFileEnumerator.extractDir(String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath()) + "/");
        return createScanFunc();
    }

    public void destroy() {
        this.mCanRun = false;
        deleteScanFunc();
        this.mVirusVector = null;
        this.mCloudApkInfoList = null;
        instance = null;
    }

    public void doScan() {
        if (init() == 0) {
            isScaning = true;
            Long localScanStart = Long.valueOf(System.currentTimeMillis());
            if (mScanType == 1 || mScanType == 2 || mScanType == 5 || mScanType == 6) {
                CloudPassage cloudPassage = new CloudPassage(this.mContext);
                cloudPassage.clearEditor();
                Value.backgroundType = 1;
                if (mScanType == 1) {
                    Value.backgroundType = Value.initiativeConnect;
                    CommonMethod.setCloudScanState(this.mContext, true);
                } else if (mScanType == 5) {
                    Value.backgroundType = Value.backgroundConnect;
                    CommonMethod.setCloudScanState(this.mContext, true);
                } else if (mScanType == 6) {
                    Value.backgroundType = Value.newInstallConnect;
                } else if (mScanType == 2) {
                    Value.backgroundType = Value.initiativeConnect;
                    CommonMethod.setCloudScanState(this.mContext, true);
                }
                cloudPassage.setScanType(new StringBuilder(String.valueOf(Value.backgroundType)).toString());
                cloudPassage.setCloudStartTime(CommonMethod.getDate());
            }
            if (this.mCanRun) {
                if (this.mObserver != null) {
                    this.mObserver.onScanBegin();
                }
                if (this.mVirusVector != null) {
                    this.mVirusVector.removeAllElements();
                } else {
                    this.mVirusVector = new Vector<>();
                }
                if (!isScanFuncCreated()) {
                    if (this.mObserver != null) {
                        this.mObserver.onScanErr(1);
                    }
                    isScaning = false;
                } else if (this.mCanRun) {
                    boolean needScanPackage = false;
                    if (mScanType == 2 || mScanType == 1 || mScanType == 5 || (mScanType == 3 && this.isCustomScanPackage)) {
                        needScanPackage = true;
                    }
                    if (this.mObserver != null && needScanPackage) {
                        this.mObserver.onScanPackage();
                    }
                    if (needScanPackage) {
                        this.mCloudApkInfoList = new ArrayList<>();
                        if (this.mCanRun) {
                            scanPackages();
                        } else {
                            return;
                        }
                    } else if (mScanType == 6) {
                        scanPackages();
                    }
                    if (!(mScanType == 6 || mScanType == 2 || mScanType == 5)) {
                        if (this.mObserver != null) {
                            this.mObserver.onScanFiles();
                        }
                        if (this.mCanRun) {
                            scanFiles();
                        } else {
                            return;
                        }
                    }
                    Long localScanEnd = Long.valueOf(System.currentTimeMillis());
                    if (mScanType == 1 || mScanType == 2 || mScanType == 5 || mScanType == 6) {
                        new CloudPassage(this.mContext).setLocalScanningUsetime(new StringBuilder(String.valueOf(localScanEnd.longValue() - localScanStart.longValue())).toString());
                    }
                    if (!(this.mObserver == null || mScanType == 3)) {
                        this.mObserver.onScanCloud();
                    }
                    if (this.mCloudController != null) {
                        this.mCloudController.setCanRun(false);
                        this.mCloudController = null;
                    }
                    if (mScanType != 3 || this.isCustomScanPackage) {
                        if (this.mCanRun) {
                            this.mCloudController = new CloudController(this.mContext);
                            this.mCloudController.setObserver(this.mObserver);
                            try {
                                this.mCloudController.setCanRun(true);
                                this.mCloudController.doCloudScan(this.mCloudApkInfoList);
                            } catch (Exception e) {
                                this.mObserver.onScanErr(2);
                                e.printStackTrace();
                            }
                        }
                    } else if (this.mObserver != null) {
                        this.mObserver.onScanEnd();
                    }
                }
            }
        } else if (this.mObserver != null) {
            this.mObserver.onScanErr(1);
        }
    }

    public int getScanFileTotalNum() {
        int num = 0;
        String sdcardPath = String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath()) + "/";
        if (mScanType == 5) {
            return 0 + FileEnumerator.CountFileNum(sdcardPath, true);
        }
        if (mScanType == 1) {
            return 0 + FileEnumerator.CountFileNum(sdcardPath, false);
        }
        if (mScanType != 3) {
            return 0;
        }
        Iterator<File> iterator = this.mCustomScanList.iterator();
        while (iterator.hasNext()) {
            File tmpFile = iterator.next();
            if (tmpFile.isDirectory()) {
                num += FileEnumerator.CountFileNum(tmpFile.getAbsolutePath(), false);
            } else {
                num++;
            }
        }
        return num;
    }

    public int getScanAppTotalNum() {
        return this.mContext.getPackageManager().getInstalledApplications(1152).size();
    }

    private void scanFiles() {
        if (mScanType == 3) {
            Iterator<File> iterator = this.mCustomScanList.iterator();
            while (iterator.hasNext() && this.mCanRun) {
                File tmpFile = iterator.next();
                if (tmpFile != null) {
                    if (tmpFile.isDirectory()) {
                        FileEnumerator fe = new FileEnumerator();
                        fe.extractDir(tmpFile.getAbsolutePath());
                        scanWithFileEnumerator(fe);
                    } else {
                        scanOneFile(tmpFile.getAbsolutePath());
                    }
                }
            }
        } else if (mScanType == 1 || mScanType == 2 || mScanType == 5) {
            scanWithFileEnumerator(this.mFileEnumerator);
        }
    }

    public void scanOneFile(String filePath) {
        File file = new File(filePath);
        String virusName = null;
        if (file.exists()) {
            if (mScanType == 1 || mScanType == 3) {
                virusName = scanFuncCheck(filePath, false, false);
            } else {
                virusName = scanFuncCheck(filePath, false, true);
            }
        }
        if (virusName != null) {
            VirusItem vi = new VirusItem();
            vi.fileName = file.getName();
            vi.fullPath = file.getAbsolutePath();
            vi.type = 1;
            vi.virusName = virusName;
            vi.description = this.mScanFunc.avEngineVirusDescription();
            if (this.mVirusVector != null) {
                this.mVirusVector.add(vi);
            }
            LogEngine.insertThreatItemLog(LogEngine.LOG_VIRUS_FOUND, file.getName(), virusName, "", this.mContext.getFilesDir().getPath());
        }
        if (this.mObserver != null) {
            this.mObserver.onScanItem(1, filePath, null, virusName != null, false);
        }
    }

    public void scanWithFileEnumerator(FileEnumerator fe) {
        String virusName;
        while (fe.hasMore() && this.mCanRun) {
            FileItem fi = fe.getAFileItem();
            while (fi != null && fi.isDir) {
                fi = fe.getAFileItem();
            }
            if (fi != null && !fi.isDir) {
                if ((mScanType != 2 && mScanType != 5) || fi.filePath.toLowerCase().endsWith("apk")) {
                    if (mScanType == 1 || mScanType == 3) {
                        virusName = scanFuncCheck(fi.filePath, true, false);
                    } else {
                        virusName = scanFuncCheck(fi.filePath, false, true);
                    }
                    if (virusName != null) {
                        VirusItem vi = new VirusItem();
                        File file = new File(fi.filePath);
                        vi.fileName = file.getName();
                        vi.fullPath = file.getAbsolutePath();
                        vi.type = 1;
                        vi.virusName = virusName;
                        vi.description = this.mScanFunc.avEngineVirusDescription();
                        vi.nickName = this.mScanFunc.avEngineVirusNickName();
                        this.mVirusVector.add(vi);
                        LogEngine.insertThreatItemLog(LogEngine.LOG_VIRUS_FOUND, file.getName(), virusName, "", this.mContext.getFilesDir().getPath());
                    }
                    if (this.mObserver != null) {
                        this.mObserver.onScanItem(1, fi.filePath, null, virusName != null, false);
                    }
                }
            }
        }
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 154 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void doScanSDCardApk() {
        /*
            r12 = this;
            r5 = 0
            r1 = 1
            int r9 = r12.init()
            if (r9 == 0) goto L_0x0012
            com.netqin.antivirus.scan.IScanObserver r0 = r12.mObserver
            if (r0 == 0) goto L_0x0011
            com.netqin.antivirus.scan.IScanObserver r0 = r12.mObserver
            r0.onScanErr(r1)
        L_0x0011:
            return
        L_0x0012:
            com.netqin.antivirus.scan.ScanController.isScaning = r1
            r8 = 0
            com.netqin.antivirus.scan.IScanObserver r0 = r12.mObserver
            if (r0 == 0) goto L_0x005b
            com.netqin.antivirus.scan.IScanObserver r0 = r12.mObserver
            r0.onScanBegin()
            java.util.Vector<com.netqin.antivirus.scan.VirusItem> r0 = r12.mVirusVector
            if (r0 == 0) goto L_0x0039
            java.util.Vector<com.netqin.antivirus.scan.VirusItem> r0 = r12.mVirusVector
            r0.removeAllElements()
        L_0x0027:
            boolean r0 = r12.isScanFuncCreated()
            if (r0 != 0) goto L_0x0053
            com.netqin.antivirus.scan.IScanObserver r0 = r12.mObserver
            if (r0 == 0) goto L_0x0036
            com.netqin.antivirus.scan.IScanObserver r0 = r12.mObserver
            r0.onScanErr(r1)
        L_0x0036:
            com.netqin.antivirus.scan.ScanController.isScaning = r5
            goto L_0x0011
        L_0x0039:
            java.util.Vector r0 = new java.util.Vector
            r0.<init>()
            r12.mVirusVector = r0
            goto L_0x0027
        L_0x0041:
            boolean r0 = r12.mCanRun
            if (r0 == 0) goto L_0x0011
            com.netqin.antivirus.scan.FileEnumerator r0 = r12.mFileEnumerator
            com.netqin.antivirus.scan.FileItem r6 = r0.getAFileItem()
        L_0x004b:
            if (r6 == 0) goto L_0x0051
            boolean r0 = r6.isDir
            if (r0 != 0) goto L_0x0076
        L_0x0051:
            if (r6 != 0) goto L_0x007d
        L_0x0053:
            com.netqin.antivirus.scan.FileEnumerator r0 = r12.mFileEnumerator
            boolean r0 = r0.hasMore()
            if (r0 != 0) goto L_0x0041
        L_0x005b:
            java.lang.String r0 = ""
            android.content.Context r2 = r12.mContext
            java.io.File r2 = r2.getFilesDir()
            java.lang.String r2 = r2.getPath()
            com.netqin.antivirus.log.LogEngine.insertGuardItemLog(r1, r8, r0, r2)
            com.netqin.antivirus.scan.IScanObserver r0 = r12.mObserver
            if (r0 == 0) goto L_0x0073
            com.netqin.antivirus.scan.IScanObserver r0 = r12.mObserver
            r0.onScanEnd()
        L_0x0073:
            com.netqin.antivirus.scan.ScanController.isScaning = r5
            goto L_0x0011
        L_0x0076:
            com.netqin.antivirus.scan.FileEnumerator r0 = r12.mFileEnumerator
            com.netqin.antivirus.scan.FileItem r6 = r0.getAFileItem()
            goto L_0x004b
        L_0x007d:
            boolean r0 = r6.isDir
            if (r0 != 0) goto L_0x0053
            int r8 = r8 + 1
            int r0 = com.netqin.antivirus.scan.ScanController.mScanType
            if (r0 == r1) goto L_0x008c
            int r0 = com.netqin.antivirus.scan.ScanController.mScanType
            r2 = 3
            if (r0 != r2) goto L_0x00eb
        L_0x008c:
            java.lang.String r0 = r6.filePath
            java.lang.String r11 = r12.scanFuncCheck(r0, r5, r5)
        L_0x0092:
            if (r11 == 0) goto L_0x00da
            com.netqin.antivirus.scan.VirusItem r10 = new com.netqin.antivirus.scan.VirusItem
            r10.<init>()
            java.io.File r7 = new java.io.File
            java.lang.String r0 = r6.filePath
            r7.<init>(r0)
            java.lang.String r0 = r7.getName()
            r10.fileName = r0
            java.lang.String r0 = r7.getAbsolutePath()
            r10.fullPath = r0
            r10.type = r1
            r10.virusName = r11
            com.netqin.antivirus.scan.ScanFunc r0 = r12.mScanFunc
            java.lang.String r0 = r0.avEngineVirusDescription()
            r10.description = r0
            com.netqin.antivirus.scan.ScanFunc r0 = r12.mScanFunc
            java.lang.String r0 = r0.avEngineVirusNickName()
            r10.nickName = r0
            java.util.Vector<com.netqin.antivirus.scan.VirusItem> r0 = r12.mVirusVector
            r0.add(r10)
            r0 = 110(0x6e, float:1.54E-43)
            java.lang.String r2 = r7.getName()
            java.lang.String r3 = ""
            android.content.Context r4 = r12.mContext
            java.io.File r4 = r4.getFilesDir()
            java.lang.String r4 = r4.getPath()
            com.netqin.antivirus.log.LogEngine.insertThreatItemLog(r0, r2, r11, r3, r4)
        L_0x00da:
            com.netqin.antivirus.scan.IScanObserver r0 = r12.mObserver
            if (r0 == 0) goto L_0x0053
            com.netqin.antivirus.scan.IScanObserver r0 = r12.mObserver
            java.lang.String r2 = r6.filePath
            r3 = 0
            if (r11 == 0) goto L_0x00f2
            r4 = r1
        L_0x00e6:
            r0.onScanItem(r1, r2, r3, r4, r5)
            goto L_0x0053
        L_0x00eb:
            java.lang.String r0 = r6.filePath
            java.lang.String r11 = r12.scanFuncCheck(r0, r5, r1)
            goto L_0x0092
        L_0x00f2:
            r4 = r5
            goto L_0x00e6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.netqin.antivirus.scan.ScanController.doScanSDCardApk():void");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:77:0x0063 */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r22v1, resolved type: java.util.List<android.content.pm.PackageInfo>} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r22v2, resolved type: java.util.ArrayList} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v79, resolved type: java.util.ArrayList} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r22v3, resolved type: java.util.ArrayList} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void scanPackages() {
        /*
            r30 = this;
            r0 = r30
            android.content.Context r0 = r0.mContext
            r5 = r0
            android.content.pm.PackageManager r20 = r5.getPackageManager()
            r0 = r30
            java.util.ArrayList<com.netqin.antivirus.cloud.model.CloudApkInfo> r0 = r0.mCloudApkInfoList
            r5 = r0
            if (r5 == 0) goto L_0x0043
            r0 = r30
            java.util.ArrayList<com.netqin.antivirus.cloud.model.CloudApkInfo> r0 = r0.mCloudApkInfoList
            r5 = r0
            r5.clear()
        L_0x0018:
            r22 = 0
            r0 = r30
            android.content.Context r0 = r0.mContext
            r5 = r0
            android.content.pm.PackageManager r26 = r5.getPackageManager()
            r0 = r30
            android.content.Context r0 = r0.mContext
            r5 = r0
            java.lang.String r6 = "activity"
            java.lang.Object r11 = r5.getSystemService(r6)
            android.app.ActivityManager r11 = (android.app.ActivityManager) r11
            r5 = 10000(0x2710, float:1.4013E-41)
            java.util.List r27 = r11.getRunningServices(r5)
            int r5 = com.netqin.antivirus.scan.ScanController.mScanType
            r6 = 6
            if (r5 != r6) goto L_0x0220
            r0 = r30
            java.util.List<java.lang.String> r0 = r0.mSilentScanPackageNameList
            r5 = r0
            if (r5 != 0) goto L_0x004e
        L_0x0042:
            return
        L_0x0043:
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            r0 = r5
            r1 = r30
            r1.mCloudApkInfoList = r0
            goto L_0x0018
        L_0x004e:
            java.util.ArrayList r22 = new java.util.ArrayList
            r5 = 2
            r0 = r22
            r1 = r5
            r0.<init>(r1)
            r14 = 0
        L_0x0058:
            r0 = r30
            java.util.List<java.lang.String> r0 = r0.mSilentScanPackageNameList
            r5 = r0
            int r5 = r5.size()
            if (r14 < r5) goto L_0x01f9
        L_0x0063:
            r15 = 0
            r14 = 0
        L_0x0065:
            int r5 = r22.size()
            if (r14 >= r5) goto L_0x0042
            r0 = r30
            boolean r0 = r0.mCanRun
            r5 = r0
            if (r5 == 0) goto L_0x0042
            r0 = r22
            r1 = r14
            java.lang.Object r24 = r0.get(r1)
            android.content.pm.PackageInfo r24 = (android.content.pm.PackageInfo) r24
            r0 = r24
            android.content.pm.ApplicationInfo r0 = r0.applicationInfo
            r5 = r0
            java.lang.String r7 = r5.publicSourceDir
            r0 = r24
            java.lang.String r0 = r0.packageName
            r25 = r0
            r0 = r24
            android.content.pm.ApplicationInfo r0 = r0.applicationInfo
            r5 = r0
            r0 = r5
            r1 = r26
            java.lang.CharSequence r5 = r0.loadLabel(r1)
            java.lang.String r8 = r5.toString()
            java.lang.String r5 = r7.toLowerCase()     // Catch:{ Exception -> 0x02c4 }
            java.lang.String r6 = "/system/"
            boolean r5 = r5.startsWith(r6)     // Catch:{ Exception -> 0x02c4 }
            if (r5 != 0) goto L_0x02b0
            com.netqin.antivirus.cloud.model.CloudApkInfo r17 = new com.netqin.antivirus.cloud.model.CloudApkInfo     // Catch:{ Exception -> 0x02c4 }
            r17.<init>()     // Catch:{ Exception -> 0x02c4 }
            r0 = r17
            r1 = r30
            r1.mCurrentApkinfo = r0     // Catch:{ Exception -> 0x02c4 }
            int r5 = com.netqin.antivirus.scan.ScanController.mScanType     // Catch:{ Exception -> 0x02c4 }
            r6 = 1
            if (r5 == r6) goto L_0x00b9
            int r5 = com.netqin.antivirus.scan.ScanController.mScanType     // Catch:{ Exception -> 0x02c4 }
            r6 = 3
            if (r5 != r6) goto L_0x022a
        L_0x00b9:
            r5 = 1
            r6 = 0
            r0 = r30
            r1 = r7
            r2 = r5
            r3 = r6
            java.lang.String r29 = r0.scanFuncCheck(r1, r2, r3)     // Catch:{ Exception -> 0x02c4 }
        L_0x00c4:
            if (r29 == 0) goto L_0x0129
            com.netqin.antivirus.scan.VirusItem r28 = new com.netqin.antivirus.scan.VirusItem     // Catch:{ Exception -> 0x02c4 }
            r28.<init>()     // Catch:{ Exception -> 0x02c4 }
            r0 = r25
            r1 = r28
            r1.packageName = r0     // Catch:{ Exception -> 0x02c4 }
            r0 = r7
            r1 = r28
            r1.fullPath = r0     // Catch:{ Exception -> 0x02c4 }
            r5 = 2
            r0 = r5
            r1 = r28
            r1.type = r0     // Catch:{ Exception -> 0x02c4 }
            r0 = r29
            r1 = r28
            r1.virusName = r0     // Catch:{ Exception -> 0x02c4 }
            r0 = r30
            com.netqin.antivirus.scan.ScanFunc r0 = r0.mScanFunc     // Catch:{ Exception -> 0x02c4 }
            r5 = r0
            java.lang.String r5 = r5.avEngineVirusDescription()     // Catch:{ Exception -> 0x02c4 }
            r0 = r5
            r1 = r28
            r1.description = r0     // Catch:{ Exception -> 0x02c4 }
            r0 = r8
            r1 = r28
            r1.programName = r0     // Catch:{ Exception -> 0x02c4 }
            r0 = r30
            com.netqin.antivirus.scan.ScanFunc r0 = r0.mScanFunc     // Catch:{ Exception -> 0x02c4 }
            r5 = r0
            java.lang.String r5 = r5.avEngineVirusNickName()     // Catch:{ Exception -> 0x02c4 }
            r0 = r5
            r1 = r28
            r1.nickName = r0     // Catch:{ Exception -> 0x02c4 }
            r0 = r30
            java.util.Vector<com.netqin.antivirus.scan.VirusItem> r0 = r0.mVirusVector     // Catch:{ Exception -> 0x02c4 }
            r5 = r0
            r0 = r5
            r1 = r28
            r0.add(r1)     // Catch:{ Exception -> 0x02c4 }
            r5 = 110(0x6e, float:1.54E-43)
            java.lang.String r6 = ""
            r0 = r30
            android.content.Context r0 = r0.mContext     // Catch:{ Exception -> 0x02c4 }
            r9 = r0
            java.io.File r9 = r9.getFilesDir()     // Catch:{ Exception -> 0x02c4 }
            java.lang.String r9 = r9.getPath()     // Catch:{ Exception -> 0x02c4 }
            r0 = r5
            r1 = r25
            r2 = r29
            r3 = r6
            r4 = r9
            com.netqin.antivirus.log.LogEngine.insertThreatItemLog(r0, r1, r2, r3, r4)     // Catch:{ Exception -> 0x02c4 }
        L_0x0129:
            r0 = r30
            com.netqin.antivirus.scan.IScanObserver r0 = r0.mObserver     // Catch:{ Exception -> 0x02c4 }
            r5 = r0
            if (r5 == 0) goto L_0x013d
            r0 = r30
            com.netqin.antivirus.scan.IScanObserver r0 = r0.mObserver     // Catch:{ Exception -> 0x02c4 }
            r5 = r0
            r6 = 2
            if (r29 == 0) goto L_0x0237
            r9 = 1
        L_0x0139:
            r10 = 0
            r5.onScanItem(r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x02c4 }
        L_0x013d:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02c4 }
            int r16 = r15 + 1
            java.lang.String r6 = java.lang.String.valueOf(r15)     // Catch:{ Exception -> 0x02a3 }
            r5.<init>(r6)     // Catch:{ Exception -> 0x02a3 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x02a3 }
            r0 = r17
            r1 = r5
            r0.setId(r1)     // Catch:{ Exception -> 0x02a3 }
            r0 = r17
            r1 = r8
            r0.setName(r1)     // Catch:{ Exception -> 0x02a3 }
            r0 = r17
            r1 = r25
            r0.setPkgName(r1)     // Catch:{ Exception -> 0x02a3 }
            r0 = r17
            r1 = r7
            r0.setInstallPath(r1)     // Catch:{ Exception -> 0x02a3 }
            java.lang.String r5 = com.netqin.antivirus.cloud.model.CloudHandler.getPackageInstallTime(r7)     // Catch:{ Exception -> 0x02a3 }
            r0 = r17
            r1 = r5
            r0.setIntallTime(r1)     // Catch:{ Exception -> 0x02a3 }
            r5 = 0
            r0 = r20
            r1 = r25
            r2 = r5
            android.content.pm.PackageInfo r21 = SHcMjZX.DD02zqNU.DLLIxskak(r0, r1, r2)     // Catch:{ Exception -> 0x02a3 }
            r0 = r21
            android.content.pm.ApplicationInfo r0 = r0.applicationInfo     // Catch:{ Exception -> 0x02a3 }
            r5 = r0
            r0 = r5
            r1 = r20
            android.graphics.drawable.Drawable r12 = r0.loadIcon(r1)     // Catch:{ Exception -> 0x02a3 }
            r0 = r17
            r1 = r12
            r0.setIcon(r1)     // Catch:{ Exception -> 0x02a3 }
            if (r29 == 0) goto L_0x0194
            r0 = r17
            r1 = r29
            r0.setVirusName(r1)     // Catch:{ Exception -> 0x02a3 }
        L_0x0194:
            r0 = r30
            r1 = r27
            r2 = r17
            r0.findApkRunningServices(r1, r2)     // Catch:{ Exception -> 0x02a3 }
            java.util.List<com.netqin.antivirus.cloud.model.CloudApkInfo> r19 = com.netqin.antivirus.cloud.model.CloudApkInfoList.infoList     // Catch:{ Exception -> 0x02a3 }
            if (r19 == 0) goto L_0x01ac
            r18 = 0
        L_0x01a3:
            int r5 = r19.size()     // Catch:{ Exception -> 0x02a3 }
            r0 = r18
            r1 = r5
            if (r0 < r1) goto L_0x023a
        L_0x01ac:
            r0 = r30
            java.util.ArrayList<com.netqin.antivirus.cloud.model.CloudApkInfo> r0 = r0.mCloudApkInfoList     // Catch:{ Exception -> 0x02a3 }
            r5 = r0
            r0 = r5
            r1 = r17
            r0.add(r1)     // Catch:{ Exception -> 0x02a3 }
            byte[] r5 = r17.getCertRSA()     // Catch:{ Exception -> 0x02a3 }
            int r5 = r5.length     // Catch:{ Exception -> 0x02a3 }
            if (r5 <= 0) goto L_0x01ed
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02a3 }
            r0 = r30
            android.content.Context r0 = r0.mContext     // Catch:{ Exception -> 0x02a3 }
            r6 = r0
            java.io.File r6 = r6.getFilesDir()     // Catch:{ Exception -> 0x02a3 }
            java.lang.String r6 = r6.getAbsolutePath()     // Catch:{ Exception -> 0x02a3 }
            java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ Exception -> 0x02a3 }
            r5.<init>(r6)     // Catch:{ Exception -> 0x02a3 }
            java.lang.String r6 = "/"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x02a3 }
            java.lang.String r6 = r17.getPkgName()     // Catch:{ Exception -> 0x02a3 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x02a3 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x02a3 }
            byte[] r6 = r17.getCertRSA()     // Catch:{ Exception -> 0x02a3 }
            com.netqin.antivirus.cloud.model.CloudHandler.craeteFile(r5, r6)     // Catch:{ Exception -> 0x02a3 }
        L_0x01ed:
            r5 = 0
            r0 = r5
            r1 = r30
            r1.mCurrentApkinfo = r0     // Catch:{ Exception -> 0x02a3 }
            r15 = r16
        L_0x01f5:
            int r14 = r14 + 1
            goto L_0x0065
        L_0x01f9:
            r0 = r30
            java.util.List<java.lang.String> r0 = r0.mSilentScanPackageNameList
            r5 = r0
            java.lang.Object r23 = r5.get(r14)
            java.lang.String r23 = (java.lang.String) r23
            r5 = 128(0x80, float:1.794E-43)
            r0 = r26
            r1 = r23
            r2 = r5
            android.content.pm.PackageInfo r24 = SHcMjZX.DD02zqNU.DLLIxskak(r0, r1, r2)     // Catch:{ NameNotFoundException -> 0x021a }
            r0 = r22
            r1 = r24
            r0.add(r1)     // Catch:{ NameNotFoundException -> 0x021a }
        L_0x0216:
            int r14 = r14 + 1
            goto L_0x0058
        L_0x021a:
            r5 = move-exception
            r13 = r5
            r13.printStackTrace()
            goto L_0x0216
        L_0x0220:
            r5 = 0
            r0 = r26
            r1 = r5
            java.util.List r22 = r0.getInstalledPackages(r1)
            goto L_0x0063
        L_0x022a:
            r5 = 1
            r6 = 1
            r0 = r30
            r1 = r7
            r2 = r5
            r3 = r6
            java.lang.String r29 = r0.scanFuncCheck(r1, r2, r3)     // Catch:{ Exception -> 0x02c4 }
            goto L_0x00c4
        L_0x0237:
            r9 = 0
            goto L_0x0139
        L_0x023a:
            r0 = r19
            r1 = r18
            java.lang.Object r5 = r0.get(r1)     // Catch:{ Exception -> 0x02a3 }
            com.netqin.antivirus.cloud.model.CloudApkInfo r5 = (com.netqin.antivirus.cloud.model.CloudApkInfo) r5     // Catch:{ Exception -> 0x02a3 }
            java.lang.String r5 = r5.getPkgName()     // Catch:{ Exception -> 0x02a3 }
            r0 = r25
            r1 = r5
            boolean r5 = r0.equals(r1)     // Catch:{ Exception -> 0x02a3 }
            if (r5 == 0) goto L_0x02ac
            r0 = r19
            r1 = r18
            java.lang.Object r5 = r0.get(r1)     // Catch:{ Exception -> 0x02a3 }
            com.netqin.antivirus.cloud.model.CloudApkInfo r5 = (com.netqin.antivirus.cloud.model.CloudApkInfo) r5     // Catch:{ Exception -> 0x02a3 }
            java.lang.String r5 = r5.getSecurity()     // Catch:{ Exception -> 0x02a3 }
            r0 = r17
            r1 = r5
            r0.setSecurity(r1)     // Catch:{ Exception -> 0x02a3 }
            r0 = r19
            r1 = r18
            java.lang.Object r5 = r0.get(r1)     // Catch:{ Exception -> 0x02a3 }
            com.netqin.antivirus.cloud.model.CloudApkInfo r5 = (com.netqin.antivirus.cloud.model.CloudApkInfo) r5     // Catch:{ Exception -> 0x02a3 }
            java.lang.String r5 = r5.getSecurityDesc()     // Catch:{ Exception -> 0x02a3 }
            r0 = r17
            r1 = r5
            r0.setSecurityDesc(r1)     // Catch:{ Exception -> 0x02a3 }
            r0 = r19
            r1 = r18
            java.lang.Object r5 = r0.get(r1)     // Catch:{ Exception -> 0x02a3 }
            com.netqin.antivirus.cloud.model.CloudApkInfo r5 = (com.netqin.antivirus.cloud.model.CloudApkInfo) r5     // Catch:{ Exception -> 0x02a3 }
            java.lang.String r5 = r5.getVirusName()     // Catch:{ Exception -> 0x02a3 }
            r0 = r17
            r1 = r5
            r0.setVirusName(r1)     // Catch:{ Exception -> 0x02a3 }
            r0 = r19
            r1 = r18
            java.lang.Object r5 = r0.get(r1)     // Catch:{ Exception -> 0x02a3 }
            com.netqin.antivirus.cloud.model.CloudApkInfo r5 = (com.netqin.antivirus.cloud.model.CloudApkInfo) r5     // Catch:{ Exception -> 0x02a3 }
            java.lang.String r5 = r5.getScore()     // Catch:{ Exception -> 0x02a3 }
            r0 = r17
            r1 = r5
            r0.setScore(r1)     // Catch:{ Exception -> 0x02a3 }
            goto L_0x01ac
        L_0x02a3:
            r5 = move-exception
            r13 = r5
            r15 = r16
        L_0x02a7:
            r13.printStackTrace()
            goto L_0x01f5
        L_0x02ac:
            int r18 = r18 + 1
            goto L_0x01a3
        L_0x02b0:
            r0 = r30
            com.netqin.antivirus.scan.IScanObserver r0 = r0.mObserver     // Catch:{ Exception -> 0x02c4 }
            r5 = r0
            if (r5 == 0) goto L_0x01f5
            r0 = r30
            com.netqin.antivirus.scan.IScanObserver r0 = r0.mObserver     // Catch:{ Exception -> 0x02c4 }
            r5 = r0
            r6 = 2
            r9 = 0
            r10 = 0
            r5.onScanItem(r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x02c4 }
            goto L_0x01f5
        L_0x02c4:
            r5 = move-exception
            r13 = r5
            goto L_0x02a7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.netqin.antivirus.scan.ScanController.scanPackages():void");
    }

    public synchronized int createScanFunc() {
        int loadres;
        Preferences preferences = new Preferences(this.mContext);
        String oldVer = preferences.getVirusDBVersion();
        String newVer = preferences.getNewVirusDBVersion();
        if (!oldVer.matches(newVer) && !TextUtils.isEmpty(newVer)) {
            if (updateVirusDB(this.mContext, preferences.getNewVirusDBPath(), newVer) != 0) {
                preferences.setNewVirusDBVersion(oldVer);
            }
        }
        try {
            FileUtils.copyVirusDb(this.mContext);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.mScanFunc = new ScanFunc(this);
        loadres = this.mScanFunc.avEngineLoad(FileUtils.virusDbFilePath(this.mContext), FileUtils.virusDbFilePathAndroid(this.mContext), FileUtils.unzipPath(this.mContext), CommonMethod.getIMEI(this.mContext), 0);
        if (loadres == 0) {
            StatFs statFs = new StatFs(FileUtils.unzipPath(this.mContext));
            int blockSize = statFs.getBlockSize();
            int blockCount = statFs.getBlockCount();
            this.mScanFunc.avEngineSetTempDirInfo((blockSize * statFs.getAvailableBlocks()) - 262144, blockSize * blockCount);
        }
        return loadres;
    }

    public synchronized boolean isScanFuncCreated() {
        boolean z;
        if (this.mScanFunc != null) {
            z = true;
        } else {
            z = false;
        }
        return z;
    }

    public synchronized String scanFuncCheck(String path, boolean isNeedDecompressSubFile, boolean isFastScan) {
        String str;
        String virusName = null;
        if (this.mScanFunc != null) {
            int para = 0;
            if (isNeedDecompressSubFile) {
                para = 0 ^ 1;
            }
            if (isFastScan) {
                para ^= 2;
            }
            if (this.mScanFunc.avEngineCheckFile(path, para) > 0) {
                virusName = this.mScanFunc.avEngineVirusName();
            }
            str = virusName;
        } else {
            str = null;
        }
        return str;
    }

    public synchronized void deleteScanFunc() {
        if (this.mScanFunc != null) {
            this.mScanFunc.avEngineEnd(0);
            this.mScanFunc = null;
        }
    }

    /* JADX INFO: Multiple debug info for r1v3 int: [D('availCount' int), D('loadres' int)] */
    /* JADX INFO: Multiple debug info for r7v1 int: [D('path' java.lang.String), D('loadres' int)] */
    /* JADX INFO: Multiple debug info for r1v5 int: [D('sf' android.os.StatFs), D('availCount' int)] */
    /* JADX INFO: Multiple debug info for r7v2 int: [D('path' java.lang.String), D('loadres' int)] */
    public static int updateVirusDB(Context context, String path, String newVer) {
        try {
            FileUtils.copyVirusDb(context);
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        ScanFunc func = new ScanFunc(null);
        int loadres = func.avEngineLoad(FileUtils.virusDbFilePath(context), FileUtils.virusDbFilePathAndroid(context), FileUtils.unzipPath(context), CommonMethod.getIMEI(context), 0);
        if (loadres != 0) {
            FileUtils.DeleteFile(path);
            func.avEngineEnd(0);
            return loadres;
        }
        if (loadres == 0) {
            StatFs sf = new StatFs(FileUtils.unzipPath(context));
            int blockSize = sf.getBlockSize();
            func.avEngineSetTempDirInfo((sf.getAvailableBlocks() * blockSize) - 262144, sf.getBlockCount() * blockSize);
        }
        int loadres2 = func.avEngineUpdateDB(path);
        if (loadres2 == 0) {
            new Preferences(context).setVirusDBVersion(newVer);
            LogEngine.insertOperationItemLog(LogEngine.LOG_VIRUSDB_UPDATE_SUCCESS, "", context.getFilesDir().getPath());
        } else {
            LogEngine.insertOperationItemLog(LogEngine.LOG_VIRUSDB_UPDATE_FAIL, "", context.getFilesDir().getPath());
        }
        FileUtils.DeleteFile(path);
        func.avEngineEnd(0);
        return loadres2;
    }

    public void onDecompressSubFile(String fileName) {
        if (this.mCanRun) {
            if ((fileName.toLowerCase().endsWith(".rsa") || fileName.toLowerCase().endsWith(".dsa")) && this.mCurrentApkinfo != null) {
                this.mCurrentApkinfo.setCertRSA(CloudHandler.readFileByte(fileName));
            }
        }
    }

    public void onScanSubFile(String fileName) {
    }

    private void findApkRunningServices(List<ActivityManager.RunningServiceInfo> serviceInfoList, CloudApkInfo apkInfo) {
        for (ActivityManager.RunningServiceInfo si : serviceInfoList) {
            if (si.service.getPackageName().toString().compareToIgnoreCase(apkInfo.getPkgName()) == 0) {
                apkInfo.runingServiceList.add(si.service.getClassName());
            }
        }
    }
}
