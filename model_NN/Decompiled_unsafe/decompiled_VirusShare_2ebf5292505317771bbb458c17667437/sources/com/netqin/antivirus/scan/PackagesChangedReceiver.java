package com.netqin.antivirus.scan;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.netqin.antivirus.cloud.model.CloudHandler;
import com.netqin.antivirus.common.CommonMethod;

public class PackagesChangedReceiver extends BroadcastReceiver {
    private static final boolean DBG = false;
    private static final String TAG = "PackagesChangedReceiver";
    private Context mContext = null;

    public void onReceive(Context context, Intent intent) {
        this.mContext = context;
        if ("android.intent.action.PACKAGE_ADDED".equals(intent.getAction()) || "android.intent.action.PACKAGE_CHANGED".equals(intent.getAction()) || "android.intent.action.PACKAGE_REPLACED".equals(intent.getAction())) {
            String packageName = intent.getData().getSchemeSpecificPart();
            if (packageName != null && packageName.length() != 0 && !packageName.equalsIgnoreCase(this.mContext.getPackageName()) && !ScanController.isScaning && !CommonMethod.isFirstRun(context)) {
                Intent i = new Intent(this.mContext, SilentCloudScanService.class);
                i.putExtra(CloudHandler.KEY_ITEM_PACKAGENAME, packageName);
                this.mContext.stopService(i);
                this.mContext.startService(i);
                return;
            }
            return;
        }
        "android.intent.action.PACKAGE_REMOVED".equals(intent.getAction());
    }
}
