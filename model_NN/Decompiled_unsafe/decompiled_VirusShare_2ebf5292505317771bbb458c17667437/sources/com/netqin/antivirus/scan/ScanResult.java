package com.netqin.antivirus.scan;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.netqin.antivirus.cloud.model.CloudApkInfo;
import com.netqin.antivirus.cloud.model.CloudHandler;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.log.LogEngine;
import com.netqin.antivirus.packagemanager.ApkInfoActivity;
import com.nqmobile.antivirus_ampro20.R;
import java.io.File;
import java.util.ArrayList;
import java.util.Vector;

public class ScanResult extends Activity implements AdapterView.OnItemClickListener {
    public static int FROM_MAUNUAL_SCAN = 0;
    public static int FROM_PERIOD_SCAN = 1;
    int cloudDangerNum = 0;
    private ArrayList<String> dangerPackageName;
    boolean isCloudSuccess;
    boolean isScanDone = true;
    ArrayList<CloudApkInfo> mCloudApkInfoList;
    int mFrom = FROM_MAUNUAL_SCAN;
    private ResultListAdapter mListAdapter;
    private ListView mListView;
    PackageManager mPackageManager;
    ArrayList<ResultItem> mResultItemArray;
    Vector<VirusItem> mVirusVector;
    int scanLastSecond;
    int scanedNum;
    private View vRemove;

    public void onCreate(Bundle savedInstanceState) {
        String hourStr;
        String minutStr;
        String secondStr;
        this.mFrom = getIntent().getIntExtra("from", FROM_MAUNUAL_SCAN);
        if (this.mFrom == FROM_PERIOD_SCAN) {
            this.mVirusVector = PeriodScanVirusTip.mVirusVector;
            this.mCloudApkInfoList = PeriodScanVirusTip.mCloudApkInfoList;
            this.scanLastSecond = PeriodScanVirusTip.mScanSecond;
            this.scanedNum = PeriodScanVirusTip.scanedNum;
            this.isScanDone = true;
        } else {
            this.isCloudSuccess = ScanActivity.isCloudSuccess;
            if (ScanActivity.mScanController != null) {
                this.mVirusVector = ScanActivity.mScanController.mVirusVector;
                this.mCloudApkInfoList = ScanActivity.mScanController.mCloudApkInfoList;
            }
            this.scanedNum = ScanActivity.scanedNum;
            this.scanLastSecond = ScanActivity.scanLastSecond;
            this.isScanDone = ScanActivity.isScanAllDone;
            ScanActivity.mScanController.destroy();
            ScanActivity.mScanController = null;
        }
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.scan_result);
        setRequestedOrientation(1);
        ((TextView) findViewById(R.id.activity_name)).setText((int) R.string.label_scan_result);
        ((TextView) findViewById(R.id.scan_result_scan_count)).setText(getResources().getString(R.string.scan_result_count, Integer.valueOf(this.scanedNum)));
        LogEngine.insertGuardItemLog(1, this.scanedNum, "", getFilesDir().getPath());
        TextView tvScanTime = (TextView) findViewById(R.id.scan_result_last_time);
        int minut = (this.scanLastSecond / 60) % 60;
        int second = this.scanLastSecond % 60;
        int hour = this.scanLastSecond / 3600;
        if (hour < 10) {
            hourStr = "0" + hour;
        } else {
            hourStr = new StringBuilder().append(hour).toString();
        }
        if (minut < 10) {
            minutStr = "0" + minut;
        } else {
            minutStr = new StringBuilder().append(minut).toString();
        }
        if (second < 10) {
            secondStr = "0" + second;
        } else {
            secondStr = new StringBuilder().append(second).toString();
        }
        tvScanTime.setText(getString(R.string.scan_result_time, new Object[]{String.valueOf(hourStr) + ":" + minutStr + ":" + secondStr}));
        this.vRemove = findViewById(R.id.scan_result_remove);
        this.vRemove.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ScanResult.this.RemoveSelectItem();
            }
        });
        this.mPackageManager = getPackageManager();
        makeResultList();
        ViewGroup scan_state = (ViewGroup) findViewById(R.id.scan_result_state);
        TextView tvScanReport = (TextView) findViewById(R.id.scan_result_report);
        if ((this.mVirusVector != null && this.mVirusVector.size() > 0) || this.cloudDangerNum > 0) {
            tvScanReport.setText(getResources().getString(R.string.scan_result_virus_found, Integer.valueOf(this.mVirusVector.size() + this.cloudDangerNum)));
            tvScanReport.setTextColor(-65536);
            scan_state.setBackgroundResource(R.drawable.home_icon_status_danger);
        } else if (!this.isScanDone) {
            tvScanReport.setTextColor(-16777216);
            tvScanReport.setText(R.string.scan_not_over);
            scan_state.setBackgroundResource(R.drawable.home_icon_status_warning);
        } else {
            tvScanReport.setTextColor(-16777216);
            scan_state.setBackgroundResource(R.drawable.home_icon_status_ok);
            CommonMethod.clearEditorBySpfFileName(this, "dangerpackage");
            CommonMethod.clearEditorBySpfFileName(this, "dangerfile");
        }
        ScanController.isScaning = false;
    }

    public void onResume() {
        super.onResume();
        for (int i = 0; i < this.mResultItemArray.size(); i++) {
            ResultItem item = this.mResultItemArray.get(i);
            if (new File(item.filePath).exists()) {
                item.isDeleted = false;
            } else {
                item.isDeleted = true;
                item.isChecked = false;
                if ((item.isNativeEngineVirus || ((item.virusName != null && item.virusName.length() > 0) || (item.security != null && item.security.intValue() > 0 && item.security.intValue() <= 10))) && this.mFrom != FROM_PERIOD_SCAN) {
                    CommonMethod.putConfigWithBooleanValue(this, "netqin", "guidetomember", true);
                }
            }
        }
        this.mListAdapter.notifyDataSetChanged();
    }

    public void onPause() {
        dealNoDeleteItem();
        super.onPause();
    }

    public void onStop() {
        super.onStop();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    private void dealNoDeleteItem() {
        if (this.isScanDone && this.mVirusVector != null && this.mVirusVector.size() + this.cloudDangerNum > 0) {
            int virusNum = 0;
            for (int i = 0; i < this.mResultItemArray.size(); i++) {
                ResultItem item = this.mResultItemArray.get(i);
                if (item.virusName != null && item.virusName.length() > 0 && !item.isDeleted) {
                    virusNum++;
                }
            }
            CommonMethod.putConfigWithIntegerValue(this, "netqin", "nodeletevirusnum", virusNum);
        }
    }

    private void makeResultList() {
        this.mResultItemArray = new ArrayList<>(2);
        makeResultListFromVirusAndCloud();
        if (this.mResultItemArray.size() <= 0) {
            this.vRemove.setVisibility(4);
        }
        this.mListAdapter = new ResultListAdapter(this, this.mResultItemArray, this.vRemove);
        this.mListView = (ListView) findViewById(R.id.result_list);
        this.mListView.setOnItemClickListener(this);
        this.mListView.setAdapter((ListAdapter) this.mListAdapter);
    }

    /* access modifiers changed from: package-private */
    public void makeResultListFromVirus() {
        for (int i = 0; i < this.mVirusVector.size(); i++) {
            VirusItem vItem = this.mVirusVector.get(i);
            ResultItem rItem = new ResultItem();
            rItem.isNativeEngineVirus = true;
            if (vItem.type == 1) {
                rItem.type = 0;
                rItem.fileName = vItem.fileName;
                CommonMethod.saveDangerFileInfo(this, rItem.filePath);
            } else {
                rItem.type = 1;
                rItem.programName = vItem.programName;
                rItem.packageName = vItem.packageName;
            }
            rItem.filePath = vItem.fullPath;
            rItem.virusName = vItem.virusName;
            rItem.virusDesc = vItem.description;
            rItem.nickName = vItem.nickName;
            rItem.isChecked = true;
            this.mResultItemArray.add(rItem);
        }
    }

    /* access modifiers changed from: package-private */
    public void makeResultListFromVirusAndCloud() {
        int fileVirusNum = 0;
        if (this.mVirusVector != null) {
            for (int i = 0; i < this.mVirusVector.size(); i++) {
                VirusItem vItem = this.mVirusVector.get(i);
                CommonMethod.saveDangerPackageInfo(this, vItem.packageName);
                ResultItem rItem = new ResultItem();
                rItem.isNativeEngineVirus = true;
                if (vItem.type == 1) {
                    rItem.type = 0;
                    rItem.fileName = vItem.fileName;
                    rItem.filePath = vItem.fullPath;
                    rItem.virusName = vItem.virusName;
                    rItem.virusDesc = vItem.description;
                    rItem.nickName = vItem.nickName;
                    rItem.isChecked = true;
                    this.mResultItemArray.add(rItem);
                    CommonMethod.saveDangerFileInfo(this, rItem.filePath);
                }
                fileVirusNum++;
            }
        }
        if (this.mCloudApkInfoList != null) {
            ArrayList<CloudApkInfo> cloudApkInfoList = this.mCloudApkInfoList;
            for (int i2 = 0; i2 < cloudApkInfoList.size(); i2++) {
                CloudApkInfo cai = cloudApkInfoList.get(i2);
                String packageName = cai.getPkgName();
                boolean isVirusEngineFound = false;
                if (!TextUtils.isEmpty(cai.getVirusName())) {
                    CommonMethod.saveDangerPackageInfo(this, cai.getPkgName());
                }
                if (this.mVirusVector != null) {
                    int j = 0;
                    while (true) {
                        if (j >= this.mVirusVector.size()) {
                            break;
                        }
                        VirusItem vItem2 = this.mVirusVector.get(j);
                        if (vItem2.type == 2 && vItem2.packageName.compareToIgnoreCase(packageName) == 0) {
                            ResultItem rItem2 = new ResultItem();
                            rItem2.isNativeEngineVirus = true;
                            rItem2.type = 1;
                            rItem2.programName = vItem2.programName;
                            rItem2.packageName = vItem2.packageName;
                            rItem2.filePath = vItem2.fullPath;
                            rItem2.virusName = vItem2.virusName;
                            rItem2.virusDesc = vItem2.description;
                            rItem2.nickName = vItem2.nickName;
                            rItem2.isChecked = true;
                            if (cai != null) {
                                if (cai.getSecurity() != null) {
                                    rItem2.security = Integer.valueOf(Integer.parseInt(cai.getSecurity()));
                                } else {
                                    rItem2.security = null;
                                }
                            }
                            this.mResultItemArray.add(0, rItem2);
                            isVirusEngineFound = true;
                        } else {
                            j++;
                        }
                    }
                }
                if (!isVirusEngineFound) {
                    ResultItem rItem3 = new ResultItem();
                    Integer security = null;
                    rItem3.icon = cai.getIcon();
                    rItem3.type = 1;
                    rItem3.programName = cai.getName();
                    rItem3.packageName = cai.getPkgName();
                    rItem3.filePath = cai.getInstallPath();
                    if (cai != null) {
                        if (cai.getSecurity() != null) {
                            security = Integer.valueOf(Integer.parseInt(cai.getSecurity()));
                            rItem3.securityDesc = cai.getSecurityDesc();
                            rItem3.security = security;
                        } else {
                            rItem3.security = null;
                            rItem3.securityDesc = getString(R.string.text_security_level_unknown);
                        }
                        rItem3.score = cai.getScore();
                        rItem3.serverId = cai.getServerId();
                        rItem3.virusName = cai.getVirusName();
                        if (rItem3.virusName != null && rItem3.virusName.length() > 0) {
                            this.cloudDangerNum++;
                        }
                    } else {
                        rItem3.securityDesc = getString(R.string.text_security_level_unknown);
                        rItem3.serverId = null;
                        rItem3.virusName = null;
                        rItem3.score = null;
                    }
                    if (security == null || security.intValue() >= 40) {
                        this.mResultItemArray.add(rItem3);
                    } else {
                        rItem3.isChecked = true;
                        this.mResultItemArray.add(fileVirusNum, rItem3);
                    }
                }
            }
        }
    }

    public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
        ResultItem item = this.mResultItemArray.get(position);
        if (item.type == 1 && (!item.isNativeEngineVirus || (item.security != null && item.security.intValue() == 10))) {
            Intent intent = new Intent();
            intent.setClass(this, ApkInfoActivity.class);
            intent.putExtra("package_name", item.packageName);
            intent.putExtra("apk_name", item.programName);
            intent.putExtra("server_id", item.serverId);
            startActivity(intent);
        } else if (!item.isDeleted) {
            Intent intent2 = new Intent();
            intent2.setClass(this, ScanVirusDetail.class);
            intent2.putExtra("virusname", item.virusName);
            intent2.putExtra("nickname", item.nickName);
            intent2.putExtra("vclass", item.virusDesc);
            intent2.putExtra("fullpath", item.filePath);
            intent2.putExtra("type", item.type);
            if (item.type == 1) {
                intent2.putExtra(CloudHandler.KEY_ITEM_PACKAGENAME, item.packageName);
            }
            startActivity(intent2);
        }
    }

    /* access modifiers changed from: package-private */
    public void RemoveSelectItem() {
        for (int i = 0; i < this.mResultItemArray.size(); i++) {
            ResultItem item = this.mResultItemArray.get(i);
            if (item.isChecked) {
                if (item.type == 0) {
                    deleteOneFile(item.filePath);
                    item.isDeleted = true;
                    if (this.mFrom != FROM_PERIOD_SCAN) {
                        CommonMethod.putConfigWithBooleanValue(this, "netqin", "guidetomember", true);
                    }
                } else {
                    uninstallPackage(item.packageName, this);
                }
            }
        }
        this.mListAdapter.notifyDataSetChanged();
    }

    public static void deleteOneFile(String filePath) {
        File f = new File(filePath);
        if (f.exists()) {
            f.delete();
        }
    }

    public static void uninstallPackage(String packageName, Context context) {
        try {
            context.startActivity(new Intent("android.intent.action.DELETE", Uri.fromParts("package", packageName, null)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || this.mVirusVector == null || this.mVirusVector.size() + this.cloudDangerNum <= 0) {
            return super.onKeyDown(keyCode, event);
        }
        int virusNum = 0;
        for (int i = 0; i < this.mResultItemArray.size(); i++) {
            ResultItem item = this.mResultItemArray.get(i);
            if (item.virusName != null && item.virusName.length() > 0 && !item.isDeleted) {
                virusNum++;
            }
        }
        if (virusNum > 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(getString(R.string.nodeal_virus_title));
            builder.setMessage(getString(R.string.nodeal_virus_desc));
            builder.setPositiveButton(getString(R.string.label_ok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                    ScanResult.this.finish();
                }
            });
            builder.setNegativeButton(getString(R.string.label_cancel), (DialogInterface.OnClickListener) null);
            builder.show();
            return true;
        }
        CommonMethod.clearEditorBySpfFileName(this, "dangerpackage");
        CommonMethod.clearEditorBySpfFileName(this, "dangerfile");
        finish();
        return true;
    }
}
