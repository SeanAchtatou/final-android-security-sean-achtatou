package com.netqin.antivirus.scan;

public interface IScanFuncObserver {
    void onDecompressSubFile(String str);

    void onScanSubFile(String str);
}
