package com.netqin.antivirus.scan;

import android.content.ContentValues;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.provider.Settings;
import com.netqin.antivirus.Preferences;
import com.netqin.antivirus.Value;
import com.netqin.antivirus.cloud.model.CloudApkInfo;
import com.netqin.antivirus.cloud.model.CloudHandler;
import com.netqin.antivirus.cloud.view.CloudListener;
import com.netqin.antivirus.common.CommonMethod;
import java.io.IOException;
import java.util.ArrayList;

public class CloudController implements CloudListener {
    private static final String CLOUD_SERVER_URL = Value.CLOUD_SERVER_URL;
    public static final String KEY_ISALLOWINSTALLOTHER = "isAllowInstallOther";
    public static final String KEY_ISROOTPOWER = "isRootPower";
    private boolean mCanRun = false;
    Context mContext;
    IScanObserver mScanObserver;

    public CloudController(Context context) {
        this.mContext = context;
    }

    public void setObserver(IScanObserver observer) {
        this.mScanObserver = observer;
    }

    public void setCanRun(boolean b) {
        this.mCanRun = b;
    }

    public void doCloudScan(ArrayList<CloudApkInfo> cloudApkInfoList) throws IllegalStateException, IOException, Exception {
        if (isNetAvailable(this.mContext)) {
            CloudHandler ch = CloudHandler.getInstance(this.mContext);
            ch.setApkInfoListForCloud(cloudApkInfoList);
            if (ScanController.mScanType == 5) {
                ch.start(this, 5, null, false);
            } else {
                ch.start(this, 5, null, true);
            }
        } else if (this.mScanObserver != null) {
            this.mScanObserver.onScanErr(3);
            this.mScanObserver.onScanEnd();
        }
    }

    private ContentValues getPublicData() {
        ContentValues content = new ContentValues();
        content.put("IMEI", CommonMethod.getIMEI(this.mContext));
        content.put("IMSI", CommonMethod.getIMSI(this.mContext));
        content.put(Value.ClientVersion, new Preferences(this.mContext).getVirusDBVersion());
        content.put("UID", CommonMethod.getUID(this.mContext));
        content.put("isAllowInstallOther", Boolean.valueOf(isAllowInstallOther(this.mContext)));
        content.put("isRootPower", Boolean.valueOf(CloudHandler.isRootPower()));
        return content;
    }

    public static boolean isAllowInstallOther(Context context) {
        if (Settings.Secure.getInt(context.getContentResolver(), "install_non_market_apps", 0) == 0) {
            return true;
        }
        return false;
    }

    public static boolean isNetAvailable(Context context) {
        if (((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo() != null) {
            return true;
        }
        return false;
    }

    public void complete(int resultCode) {
        if (this.mScanObserver != null) {
            if (resultCode == 1000) {
                this.mScanObserver.onScanCloudDone(0);
            } else {
                this.mScanObserver.onScanCloudDone(1);
            }
            this.mScanObserver.onScanEnd();
        }
    }

    public void error(int errorCode) {
        if (this.mCanRun && this.mScanObserver != null) {
            this.mScanObserver.onScanErr(4);
        }
    }

    public void process(Bundle bundle) {
    }
}
