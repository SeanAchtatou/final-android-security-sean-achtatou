package com.netqin.antivirus.scan;

import android.content.Context;
import com.nqmobile.antivirus_ampro20.R;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class FileUtils {
    public static final String MALLINKDB_NQ = "nqmallink.db";
    public static final String NOTIFICATION_MAIN = "NetQin_AntiVirus";
    public static final String NOTIFICATION_START = "protecting";
    public static final String UNZIP_TMPPATH = "tmpzip";
    public static final String VIRUSDB_AD = "nqAndroidVirus.db";
    public static final String VIRUSDB_CFG = "cfg.db";
    public static final String VIRUSDB_EX = "extra.db";
    public static final String VIRUSDB_NQ = "nqVirus.db";
    public static final String VIRUSDB_UPDATE_FILENAME = "update.dat";

    public static String updateVirusDbFilePath(Context context) {
        return context.getFilesDir() + "/" + VIRUSDB_UPDATE_FILENAME;
    }

    public static boolean DeleteFile(String path) {
        File file = new File(path);
        if (file.exists()) {
            return file.delete();
        }
        return true;
    }

    public static String virusDbFilePath(Context context) {
        StringBuffer sb = new StringBuffer();
        sb.append(context.getFilesDir() + "/" + VIRUSDB_NQ + "*");
        sb.append(context.getFilesDir() + "/" + VIRUSDB_EX + "*");
        sb.append(context.getFilesDir() + "/" + VIRUSDB_CFG);
        return sb.toString();
    }

    public static String virusDbFilePathAndroid(Context context) {
        StringBuffer sb = new StringBuffer();
        sb.append(context.getFilesDir() + "/" + VIRUSDB_AD + "*");
        sb.append(context.getFilesDir() + "/" + VIRUSDB_EX);
        return sb.toString();
    }

    public static String unzipPath(Context context) {
        File file = new File(context.getFilesDir() + "/" + UNZIP_TMPPATH + "/");
        if (!file.exists()) {
            file.mkdir();
        }
        return file.getAbsolutePath();
    }

    public static void copyVirusDb(Context context) throws IOException {
        File targetFile1 = new File(context.getFilesDir() + "/" + VIRUSDB_NQ);
        if (!targetFile1.exists()) {
            copyFile(context, R.raw.nqvirus, targetFile1);
        }
        File targetFile2 = new File(context.getFilesDir() + "/" + VIRUSDB_EX);
        if (!targetFile2.exists()) {
            copyFile(context, R.raw.extra, targetFile2);
        }
        File targetFile3 = new File(context.getFilesDir() + "/" + VIRUSDB_CFG);
        if (!targetFile3.exists()) {
            copyFile(context, R.raw.cfg, targetFile3);
        }
        File targetFile4 = new File(context.getFilesDir() + "/" + VIRUSDB_AD);
        if (!targetFile4.exists()) {
            copyFile(context, R.raw.nqandroidvirus, targetFile4);
        }
    }

    public static void copyFile(File sourceFile, File targetFile) throws IOException {
        FileInputStream input = new FileInputStream(sourceFile);
        BufferedInputStream inBuff = new BufferedInputStream(input);
        FileOutputStream output = new FileOutputStream(targetFile);
        BufferedOutputStream outBuff = new BufferedOutputStream(output);
        byte[] b = new byte[5120];
        while (true) {
            int len = inBuff.read(b);
            if (len == -1) {
                outBuff.flush();
                inBuff.close();
                outBuff.close();
                output.close();
                input.close();
                return;
            }
            outBuff.write(b, 0, len);
        }
    }

    public static void copyFile(Context context, int rawId, File targetFile) throws IOException {
        InputStream input = context.getResources().openRawResource(rawId);
        BufferedInputStream inBuff = new BufferedInputStream(input);
        FileOutputStream output = new FileOutputStream(targetFile);
        BufferedOutputStream outBuff = new BufferedOutputStream(output);
        byte[] b = new byte[5120];
        while (true) {
            int len = inBuff.read(b);
            if (len == -1) {
                outBuff.flush();
                inBuff.close();
                outBuff.close();
                output.close();
                input.close();
                return;
            }
            outBuff.write(b, 0, len);
        }
    }

    public static void copyDirectiory(String sourceDir, String targetDir) throws IOException {
        new File(targetDir).mkdirs();
        File[] file = new File(sourceDir).listFiles();
        for (int i = 0; i < file.length; i++) {
            if (file[i].isFile()) {
                copyFile(file[i], new File(String.valueOf(new File(targetDir).getAbsolutePath()) + File.separator + file[i].getName()));
            }
            if (file[i].isDirectory()) {
                copyDirectiory(String.valueOf(sourceDir) + "/" + file[i].getName(), String.valueOf(targetDir) + "/" + file[i].getName());
            }
        }
    }

    public static String malLinkDbFilePath(Context context) {
        StringBuffer sb = new StringBuffer();
        sb.append(context.getFilesDir() + "/" + MALLINKDB_NQ);
        return sb.toString();
    }
}
