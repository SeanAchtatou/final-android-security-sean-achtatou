package com.netqin.antivirus.scan;

import java.io.File;
import java.util.Vector;

public class FileEnumerator {
    private Vector<FileItem> fileVector = new Vector<>();

    FileEnumerator() {
    }

    public void printVector() {
    }

    public FileItem getAFileItem() {
        if (this.fileVector.size() == 0) {
            return null;
        }
        FileItem fi = this.fileVector.elementAt(0);
        this.fileVector.remove(0);
        if (fi.isDir) {
            extractDir(fi);
            printVector();
        }
        return fi;
    }

    public int fileNum() {
        return this.fileVector.size();
    }

    public boolean hasMore() {
        return !this.fileVector.isEmpty();
    }

    public void extractDir(String path) {
        this.fileVector.removeAllElements();
        try {
            File[] fs = new File(path).listFiles();
            for (int i = 0; i < fs.length; i++) {
                if (!fs[i].isDirectory()) {
                    this.fileVector.insertElementAt(new FileItem(fs[i].getAbsolutePath(), false, 0), 0);
                } else if (fs[i].isDirectory()) {
                    extractDir(new FileItem(fs[i].getAbsolutePath(), true, 0));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void extractDir(FileItem fileItem) {
        try {
            File[] fs = new File(fileItem.filePath).listFiles();
            int fileNum = 0;
            if (fs != null) {
                for (int i = 0; i < fs.length; i++) {
                    if (!fs[i].isDirectory()) {
                        this.fileVector.insertElementAt(new FileItem(fs[i].getAbsolutePath(), false, fileItem.dirLevel + 1), 0);
                        fileNum++;
                    } else if (fs[i].isDirectory()) {
                        this.fileVector.insertElementAt(new FileItem(fs[i].getAbsolutePath(), true, fileItem.dirLevel + 1), fileNum);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int CountFileNum(String strPath, boolean isOnlyApk) {
        int num = 0;
        File[] files = new File(strPath).listFiles();
        if (files == null) {
            return 0;
        }
        for (int i = 0; i < files.length; i++) {
            if (files[i].isDirectory()) {
                num += CountFileNum(files[i].getAbsolutePath(), isOnlyApk);
            } else if (!isOnlyApk) {
                num++;
            } else if (files[i].getName().toLowerCase().endsWith("apk")) {
                num++;
            }
        }
        return num;
    }
}
