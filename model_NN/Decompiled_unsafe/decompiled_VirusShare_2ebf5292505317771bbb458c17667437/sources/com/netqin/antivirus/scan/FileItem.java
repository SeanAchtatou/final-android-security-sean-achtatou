package com.netqin.antivirus.scan;

/* compiled from: FileEnumerator */
class FileItem {
    public int dirLevel;
    public String filePath;
    public boolean isDir;

    FileItem(String path, boolean isDir2, int dirLevel2) {
        this.isDir = isDir2;
        this.filePath = path;
        this.dirLevel = dirLevel2;
    }
}
