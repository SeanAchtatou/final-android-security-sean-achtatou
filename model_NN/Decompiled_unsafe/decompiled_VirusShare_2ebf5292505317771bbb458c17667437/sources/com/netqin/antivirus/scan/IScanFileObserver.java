package com.netqin.antivirus.scan;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.os.FileObserver;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import com.netqin.antivirus.HomeActivity;
import com.netqin.antivirus.cloud.model.CloudHandler;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.log.LogEngine;
import com.nqmobile.antivirus_ampro20.R;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

public class IScanFileObserver extends FileObserver {
    protected ContextWrapper contextWrapper;
    /* access modifiers changed from: private */
    public Context mContext;
    private Handler mHandler;
    int mMask;
    List<SingleFileObserver> mObservers;
    String mPath;
    private Map<String, Object> pathMap;
    /* access modifiers changed from: private */
    public VirusItem vi;
    /* access modifiers changed from: private */
    public String virusName;

    public IScanFileObserver(String path, Context context) {
        this(path, 4095, context);
        this.mContext = context;
    }

    public IScanFileObserver(String path, int mask, Context context) {
        super(path, mask);
        this.pathMap = new HashMap();
        this.virusName = null;
        this.mHandler = new Handler(Looper.myLooper());
        this.mPath = path;
        this.mMask = mask;
        this.mContext = context;
    }

    public void startWatching() {
        if (this.mObservers == null) {
            this.mObservers = new ArrayList();
            final Stack stack = new Stack();
            stack.push(this.mPath);
            new Thread(new Runnable() {
                public void run() {
                    while (!stack.isEmpty()) {
                        String parent = (String) stack.pop();
                        IScanFileObserver.this.mObservers.add(new SingleFileObserver(parent, IScanFileObserver.this.mMask));
                        File[] files = new File(parent).listFiles();
                        if (files != null) {
                            for (File f : files) {
                                if (f.isDirectory() && !f.getName().equals(".") && !f.getName().equals("..")) {
                                    stack.push(f.getPath());
                                }
                            }
                        }
                    }
                    for (SingleFileObserver sfo : IScanFileObserver.this.mObservers) {
                        sfo.startWatching();
                    }
                }
            }).start();
        }
    }

    public void stopWatching() {
        if (this.mObservers != null) {
            for (SingleFileObserver sfo : this.mObservers) {
                sfo.stopWatching();
            }
            this.mObservers.clear();
            this.mObservers = null;
        }
    }

    public void onEvent(int event, final String path) {
        Object token = this.pathMap.get(path);
        Object newToken = new Object();
        switch (event) {
            case 2:
            case 256:
                if (this.pathMap.containsKey(path) && token != null) {
                    this.mHandler.removeCallbacksAndMessages(token);
                    return;
                }
                return;
            case 8:
                if (this.pathMap.containsKey(path)) {
                    if (token != null) {
                        this.mHandler.removeCallbacksAndMessages(token);
                    }
                    this.mHandler.postAtTime(new Runnable() {
                        public void run() {
                            IScanFileObserver.this.doMonitorScan(path);
                        }
                    }, token, SystemClock.uptimeMillis() + 10000);
                    return;
                }
                if (newToken != null) {
                    this.mHandler.removeCallbacksAndMessages(newToken);
                }
                this.mHandler.postAtTime(new Runnable() {
                    public void run() {
                        IScanFileObserver.this.doMonitorScan(path);
                    }
                }, newToken, SystemClock.uptimeMillis() + 10000);
                this.pathMap.put(path, newToken);
                return;
            default:
                return;
        }
    }

    class SingleFileObserver extends FileObserver {
        String mPath;

        public SingleFileObserver(IScanFileObserver iScanFileObserver, String path) {
            this(path, 4095);
            this.mPath = path;
        }

        public SingleFileObserver(String path, int mask) {
            super(path, mask);
            this.mPath = path;
        }

        public void onEvent(int event, String path) {
            IScanFileObserver.this.onEvent(event, String.valueOf(this.mPath) + "/" + path);
        }
    }

    public void doMonitorScan(final String path) {
        new Thread(new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
            public void run() {
                String[] subPath = path.split("/");
                String appName = subPath[subPath.length - 1];
                IScanFileObserver.this.vi = new VirusItem();
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                ScanController controller = ScanController.getInstance(IScanFileObserver.this.mContext);
                if (!controller.isScanFuncCreated()) {
                    controller.createScanFunc();
                }
                IScanFileObserver.this.virusName = controller.scanFuncCheck(path, false, false);
                if (IScanFileObserver.this.virusName != null) {
                    IScanFileObserver.this.mContext.getSharedPreferences("netqin", 0).edit().putBoolean("hasVirus", true).commit();
                    Intent homeIntent = new Intent(IScanFileObserver.this.mContext, HomeActivity.class);
                    homeIntent.putExtra("hasVirus", true);
                    CommonMethod.showFlowBarOrNot(IScanFileObserver.this.mContext, homeIntent, IScanFileObserver.this.mContext.getString(R.string.text_monitor_virus_scan_tip, 1), true);
                    CommonMethod.putConfigWithIntegerValue(IScanFileObserver.this.mContext, "netqin", "nodeletevirusnum", CommonMethod.getConfigWithIntegerValue(IScanFileObserver.this.mContext, "netqin", "nodeletevirusnum", 0) + 1);
                    Intent intent = new Intent(IScanFileObserver.this.mContext, MonitorVirusTip.class);
                    intent.setFlags(276824064);
                    intent.putExtra("name", appName);
                    intent.putExtra(CloudHandler.KEY_ITEM_PATH, path);
                    intent.putExtra("virusname", IScanFileObserver.this.virusName);
                    intent.putExtra("type", 1);
                    IScanFileObserver.this.mContext.startActivity(intent);
                    LogEngine.insertThreatItemLog(4, "", IScanFileObserver.this.virusName, "", IScanFileObserver.this.mContext.getFilesDir().getPath());
                }
                LogEngine.insertGuardItemLog(1, 0 + 1, "", IScanFileObserver.this.mContext.getFilesDir().getPath());
                controller.deleteScanFunc();
            }
        }).start();
    }
}
