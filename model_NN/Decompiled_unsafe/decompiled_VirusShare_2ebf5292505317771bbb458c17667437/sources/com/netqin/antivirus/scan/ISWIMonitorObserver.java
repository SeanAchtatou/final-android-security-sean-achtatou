package com.netqin.antivirus.scan;

interface ISWIMonitorObserver {
    void NqAvSWIMonitorInstalled(String str);

    void NqAvSWIMonitorRemoved(String str);

    void NqAvSWIMonitorReplaced(String str);
}
