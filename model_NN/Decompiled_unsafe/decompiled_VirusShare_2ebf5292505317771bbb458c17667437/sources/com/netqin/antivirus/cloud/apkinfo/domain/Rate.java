package com.netqin.antivirus.cloud.apkinfo.domain;

import android.text.TextUtils;

public class Rate {
    private String content;
    private String fileName;
    private String first;
    private String id;
    private String length;
    private String name;
    private String offset;
    private String performance;
    private String performanceRef;
    private String pkgName;
    private String reason;
    private String ref;
    private String score;
    private String serverId;
    private String url_performance;
    private String url_performanceRef;
    private String versionCode;
    private String versionName;

    public String getServerId() {
        return this.serverId;
    }

    public void setServerId(String serverId2) {
        this.serverId = serverId2;
    }

    public String getFirst() {
        return this.first;
    }

    public void setFirst(String first2) {
        this.first = first2;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getReason() {
        return this.reason;
    }

    public void setReason(String reason2) {
        this.reason = reason2;
    }

    public String getFileName() {
        return this.fileName;
    }

    public void setFileName(String fileName2) {
        this.fileName = fileName2;
    }

    public String getRef() {
        return this.ref;
    }

    public void setRef(String ref2) {
        this.ref = ref2;
    }

    public String getOffset() {
        return this.offset;
    }

    public void setOffset(String offset2) {
        this.offset = offset2;
    }

    public String getLength() {
        return this.length;
    }

    public void setLength(String length2) {
        this.length = length2;
    }

    public String getPkgName() {
        return this.pkgName;
    }

    public void setPkgName(String pkgName2) {
        this.pkgName = pkgName2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getVersionCode() {
        return this.versionCode;
    }

    public void setVersionCode(String versionCode2) {
        this.versionCode = versionCode2;
    }

    public String getVersionName() {
        if (TextUtils.isEmpty(this.versionName)) {
            this.versionName = "nq.1";
        }
        return this.versionName;
    }

    public void setVersionName(String versionName2) {
        this.versionName = versionName2;
    }

    public String getScore() {
        return this.score;
    }

    public void setScore(String score2) {
        this.score = score2;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content2) {
        this.content = content2;
    }

    public String getPerformance() {
        return this.performance;
    }

    public void setPerformance(String performance2) {
        this.performance = performance2;
    }

    public String getUrl_performance() {
        return this.url_performance;
    }

    public void setUrl_performance(String url_performance2) {
        this.url_performance = url_performance2;
    }

    public String getPerformanceRef() {
        return this.performanceRef;
    }

    public void setPerformanceRef(String performanceRef2) {
        this.performanceRef = performanceRef2;
    }

    public String getUrl_performanceRef() {
        return this.url_performanceRef;
    }

    public void setUrl_performanceRef(String url_performanceRef2) {
        this.url_performanceRef = url_performanceRef2;
    }
}
