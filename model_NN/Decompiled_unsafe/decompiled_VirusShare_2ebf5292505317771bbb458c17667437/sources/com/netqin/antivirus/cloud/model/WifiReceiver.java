package com.netqin.antivirus.cloud.model;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import com.netqin.antivirus.common.CommonMethod;

public class WifiReceiver extends BroadcastReceiver {
    private static final String TAG = "WifiReceiver";

    public void onReceive(Context context, Intent intent) {
        NetworkInfo networkInfo;
        String action = intent.getAction();
        if (action != null && action.equals("android.net.wifi.STATE_CHANGE") && (networkInfo = (NetworkInfo) intent.getParcelableExtra("networkInfo")) != null && networkInfo.isConnected() && !CommonMethod.isFirstRun(context) && CommonMethod.getAutoStartTag(context)) {
            new Intent(context, CloudDataService.class);
        }
    }
}
