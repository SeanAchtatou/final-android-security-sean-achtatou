package com.netqin.antivirus.cloud.model.xml;

import android.content.Context;
import android.location.LocationManager;
import android.telephony.CellLocation;
import android.telephony.NeighboringCellInfo;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import java.util.List;

public class CellIdInfo {
    private LocationManager mLocationManager = null;
    private TelephonyManager mTelephonyManager = null;

    public CellIDData getCellId(Context aParent) {
        this.mLocationManager = (LocationManager) aParent.getSystemService("location");
        this.mTelephonyManager = (TelephonyManager) aParent.getSystemService("phone");
        int mMCC = 0;
        String mMCCStr = this.mTelephonyManager.getNetworkOperator();
        if (mMCCStr != null && mMCCStr.length() > 0) {
            mMCC = Integer.valueOf(mMCCStr.substring(0, 3)).intValue();
        }
        int mMNC = 0;
        String mMNCStr = this.mTelephonyManager.getNetworkOperator();
        if (mMNCStr != null && mMNCStr.length() > 0) {
            mMNC = Integer.valueOf(mMNCStr.substring(3, 5)).intValue();
        }
        if (this.mTelephonyManager.getPhoneType() == 2 || this.mTelephonyManager.getNetworkType() == 4) {
            CdmaCellLocation location = (CdmaCellLocation) this.mTelephonyManager.getCellLocation();
            List<NeighboringCellInfo> list = this.mTelephonyManager.getNeighboringCellInfo();
            int count = 0;
            if (list != null) {
                count = list.size();
            }
            if (count > 0) {
                return null;
            }
            CellLocation.requestLocationUpdate();
            int bid = 0;
            int nid = 0;
            int sid = 0;
            if (location != null) {
                bid = location.getBaseStationId();
                nid = location.getNetworkId();
                sid = location.getSystemId();
                int lat = location.getBaseStationLatitude();
                int lon = location.getBaseStationLongitude();
            }
            CellIDData cdata = new CellIDData();
            cdata.setCellid(new StringBuilder().append(bid).toString());
            cdata.setLac(new StringBuilder().append(nid).toString());
            cdata.setMnc(new StringBuilder().append(sid).toString());
            cdata.setMcc(new StringBuilder().append(mMCC).toString());
            return cdata;
        }
        GsmCellLocation gLct = (GsmCellLocation) this.mTelephonyManager.getCellLocation();
        int _cid = 0;
        int _lac = 0;
        if (gLct != null) {
            _cid = gLct.getCid();
            _lac = gLct.getLac();
        }
        CellIDData cdata2 = new CellIDData();
        cdata2.setCellid(new StringBuilder().append(_cid).toString());
        cdata2.setLac(new StringBuilder().append(_lac).toString());
        cdata2.setMnc(new StringBuilder().append(mMNC).toString());
        cdata2.setMcc(new StringBuilder().append(mMCC).toString());
        return cdata2;
    }
}
