package com.netqin.antivirus.cloud.apkinfo.domain;

public class Review {
    private String content;
    private String date;
    private String id;
    private String pkgCnt;
    private String pkgCntUniq;
    private String pkgName;
    private String pkgScore;
    private String score;
    private String serverId;
    private String uid;

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getServerId() {
        return this.serverId;
    }

    public void setServerId(String serverId2) {
        this.serverId = serverId2;
    }

    public String getPkgCnt() {
        return this.pkgCnt;
    }

    public void setPkgCnt(String pkgCnt2) {
        this.pkgCnt = pkgCnt2;
    }

    public String getPkgScore() {
        return this.pkgScore;
    }

    public void setPkgScore(String pkgScore2) {
        this.pkgScore = pkgScore2;
    }

    public String getPkgCntUniq() {
        return this.pkgCntUniq;
    }

    public void setPkgCntUniq(String pkgCntUniq2) {
        this.pkgCntUniq = pkgCntUniq2;
    }

    public String getUid() {
        return this.uid;
    }

    public void setUid(String uid2) {
        this.uid = uid2;
    }

    public String getDate() {
        return this.date;
    }

    public void setDate(String date2) {
        this.date = date2;
    }

    public String getScore() {
        return this.score;
    }

    public void setScore(String score2) {
        this.score = score2;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content2) {
        this.content = content2;
    }

    public String getPkgName() {
        return this.pkgName;
    }

    public void setPkgName(String pkgName2) {
        this.pkgName = pkgName2;
    }
}
