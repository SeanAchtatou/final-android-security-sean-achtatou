package com.netqin.antivirus.cloud.model;

import SHcMjZX.DD02zqNU;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import com.netqin.antivirus.NqUtil;
import com.netqin.antivirus.Preferences;
import com.netqin.antivirus.Value;
import com.netqin.antivirus.cloud.apkinfo.domain.Rate;
import com.netqin.antivirus.cloud.model.DataUtils;
import com.netqin.antivirus.cloud.model.http.HttpHandler;
import com.netqin.antivirus.cloud.model.http.HttpListener;
import com.netqin.antivirus.cloud.model.http.HttpUtil;
import com.netqin.antivirus.cloud.model.http.Request;
import com.netqin.antivirus.cloud.model.http.Response;
import com.netqin.antivirus.cloud.model.xml.CloudRequest;
import com.netqin.antivirus.cloud.model.xml.CloudResponse;
import com.netqin.antivirus.cloud.model.xml.XmlUtils;
import com.netqin.antivirus.cloud.view.CloudListener;
import com.netqin.antivirus.cloud.virus.db.CloudVirusdb;
import com.netqin.antivirus.common.CloudPassage;
import com.netqin.antivirus.common.CommonDefine;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.packagemanager.ApkInfoActivity;
import com.nqmobile.antivirus_ampro20.R;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.http.util.EncodingUtils;

public class CloudHandler implements Runnable, HttpListener {
    private static final String CACHE_AUTO_UPLOAD = "/upload_info.txt";
    public static final String CERT_DSA = ".DSA";
    public static final String CERT_RSA = ".RSA";
    public static final String CERT_SF = ".SF";
    public static final String Cert_rsa_path = "/xml/META-INF/CERT.RSA";
    private static final String Cert_sf_path = "/xml/META-INF/CERT.SF";
    private static final boolean DBG = false;
    private static final String DestPath = "/xml/";
    private static final String Dest_Meta_Inf_Path = "/xml/META-INF/";
    public static final int ERROR_TYPE_NET = 4;
    public static final int ERROR_TYPE_SERVER = 5;
    public static final int ERROR_TYPE_XML = 6;
    public static final String KEY_ISALLOWINSTALLOTHER = "isAllowInstallOther";
    public static final String KEY_ISROOTPOWER = "isRootPower";
    public static final String KEY_ITEM_CERTRSA = "certRSA";
    public static final String KEY_ITEM_CERTSF = "certSF";
    public static final String KEY_ITEM_CHECKED = "checked";
    public static final String KEY_ITEM_DESC = "desc";
    public static final String KEY_ITEM_ICON = "icon";
    public static final String KEY_ITEM_PACKAGENAME = "packagename";
    public static final String KEY_ITEM_PATH = "path";
    public static final String KEY_ITEM_TITLE = "title";
    public static final String KEY_PROGRESS_SCAN = "scan";
    public static final String KEY_PROGRESS_SCANED_NUM = "scaned_num";
    public static final String KEY_PROGRESS_TYPE = "progress_type";
    public static final int PROFRESS_TYPE_DEAL_RESULT = 2;
    public static final int PROFRESS_TYPE_DEAL_RESULT_OVER = 3;
    public static final int PROGRESS_TYPE_SCAN = 0;
    public static final int PROGRESS_TYPE_SERVER = 1;
    public static final int RESULT_CODE_DANGER = 7;
    public static final int RESULT_CODE_NULL = 8;
    public static final boolean STATISTIC_SWITCH = false;
    private static final String TAG = "CloudHandler";
    private static final String XmlPath = "/xml/AndroidManifest.xml";
    private static CloudHandler _this = null;
    public static Rate rate;
    private String CLOUD_SERVER_URL = Value.CLOUD_SERVER_URL;
    final String DestName = "AndroidManifest.xml";
    final String Dest_Meta_Inf = "META-INF";
    private String apkIsNeed = "";
    private int clearedNum = 0;
    private CloudResponse cloudResult;
    private String cloudScanId = "";
    private Thread cloudThread = null;
    private CloudVirusdb cloud_virus_db = null;
    private int command;
    private int diaLogCommand = -1;
    File file = null;
    BufferedInputStream in = null;
    private CloudApkInfo info;
    private boolean isRepeat = false;
    private boolean isRun = true;
    private CloudListener listener = null;
    ArrayList<CloudApkInfo> localScaninfos = null;
    private ArrayList<CloudApkInfo> mCloudApkInfoList;
    private Context mContext = null;
    private Handler mmHander;
    private int oldStatus = -1;
    private String reportUploderUrl = "";
    public ArrayList<HashMap<String, Object>> resultCheckedList = new ArrayList<>();
    private ArrayList<HashMap<String, Object>> resultList = null;
    private boolean showDialog = true;
    byte[] temp = null;

    private String destPath() {
        return this.mContext.getFilesDir() + DestPath;
    }

    private String xmlPath() {
        return this.mContext.getFilesDir() + XmlPath;
    }

    public String cert_sf_path() {
        return this.mContext.getFilesDir() + Cert_sf_path;
    }

    public String cert_rsa_path() {
        return this.mContext.getFilesDir() + Cert_rsa_path;
    }

    public String cache_auto_upload_path() {
        return this.mContext.getFilesDir() + CACHE_AUTO_UPLOAD;
    }

    private String dest_meta_inf_path() {
        return this.mContext.getFilesDir() + Dest_Meta_Inf_Path;
    }

    public void setApkInfoListForCloud(ArrayList<CloudApkInfo> cloudApkInfoList) {
        this.mCloudApkInfoList = cloudApkInfoList;
    }

    public static CloudHandler getInstance(Context context) {
        if (_this == null) {
            _this = new CloudHandler(context);
        }
        return _this;
    }

    private CloudHandler(Context context) {
        this.mContext = context;
    }

    public CloudHandler(CloudListener listener2, Context context, int command2) {
        this.listener = listener2;
        this.mContext = context;
        this.command = command2;
        if (this.cloudThread != null) {
            this.cloudThread.interrupt();
            this.cloudThread = null;
        }
        this.cloudThread = new Thread(this);
        this.cloudThread.start();
    }

    public void setListener(CloudListener listener2) {
        this.listener = listener2;
    }

    public int getClearedNum() {
        return this.clearedNum;
    }

    public void setClearedNum(int clearedNum2) {
        this.clearedNum = clearedNum2;
    }

    public void setShowDialog(boolean showDialog2) {
        this.showDialog = showDialog2;
        if (((this.diaLogCommand != -1) & showDialog2) && (this.listener != null)) {
            switch (this.diaLogCommand) {
                case 1:
                    sendUiMessage(R.string.text_cloud_send_server);
                    return;
                case 2:
                    sendUiMessage(R.string.text_cloud_server_response);
                    return;
                case 3:
                default:
                    return;
                case 4:
                case 5:
                case 6:
                    this.listener.error(this.diaLogCommand);
                    return;
                case 7:
                case 8:
                    this.listener.complete(this.diaLogCommand);
                    return;
            }
        }
    }

    public void resetData() {
        this.isRun = true;
        this.temp = null;
        this.in = null;
        setDiaLogCommand(-1);
        this.localScaninfos = null;
        this.clearedNum = 0;
        HttpHandler.getInstance().setCancel(false);
    }

    public static void clearData() {
        _this = null;
    }

    public static void releaseMemory() {
        _this = null;
    }

    public void start(CloudListener listener2, int command2, Handler mHander, boolean isForeground) {
        this.listener = listener2;
        this.command = command2;
        this.mmHander = mHander;
        if (this.cloudThread != null) {
            if (isForeground) {
                this.cloudThread.interrupt();
                this.cloudThread = null;
            } else if (this.cloudThread.isAlive()) {
                if (this.oldStatus == 0) {
                    this.cloudThread.interrupt();
                    this.cloudThread = null;
                } else if (this.oldStatus == 1) {
                    return;
                }
            }
        }
        if (isForeground) {
            this.oldStatus = 1;
        } else {
            this.oldStatus = 0;
        }
        this.cloudThread = new Thread(this);
        this.cloudThread.start();
    }

    public void run() {
        try {
            doClouding();
        } catch (IllegalStateException e) {
            errorNotify(6);
            this.listener.complete(1001);
            e.printStackTrace();
        } catch (IOException e2) {
            errorNotify(4);
            this.listener.complete(1001);
            e2.printStackTrace();
        } catch (Exception e3) {
            errorNotify(4);
            this.listener.complete(1001);
            e3.printStackTrace();
        }
    }

    public synchronized void cancel() {
        this.isRun = false;
        HttpHandler.getInstance().setCancel(true);
    }

    private void doClouding() throws IllegalStateException, IOException, Exception {
        switch (this.command) {
            case 0:
                cloudLocalAndServer();
                CommonMethod.setCloudScanState(this.mContext, true);
                return;
            case 1:
                reportSuspiciousAndServer(rate);
                return;
            case 2:
                scoreReviewAndServer(rate);
                return;
            case 3:
                moreScoreReviewAndServer(rate);
                return;
            case 4:
                cloudPassageAndServer(rate);
                return;
            case 5:
                cloudServer();
                CommonMethod.setCloudScanState(this.mContext, true);
                return;
            default:
                return;
        }
    }

    private void cloudServer() throws IllegalStateException, IOException, Exception {
        byte[] requestData = new CloudRequest(getPublicData(), this.mCloudApkInfoList, this.mContext).getRequestBytes();
        Request request = new Request();
        request.setWifi(HttpUtil.isWifi(this.mContext));
        request.setStream(true);
        request.setSendDatas(requestData);
        request.setUrl(this.CLOUD_SERVER_URL);
        request.setProxy(NqUtil.getApnProxy(this.mContext));
        request.setSchema(0);
        HttpHandler.getInstance().startConn(this, request, this.mContext);
    }

    private ContentValues getPublicData() {
        ContentValues content = new ContentValues();
        content.put("IMEI", CommonMethod.getIMEI(this.mContext));
        content.put("IMSI", CommonMethod.getIMSI(this.mContext));
        content.put(Value.ClientVersion, new Preferences(this.mContext).getVirusDBVersion());
        content.put("UID", CommonMethod.getUID(this.mContext));
        content.put("isAllowInstallOther", Boolean.valueOf(isAllowInstallOther(this.mContext)));
        content.put("isRootPower", Boolean.valueOf(isRootPower()));
        return content;
    }

    private void cloudLocalAndServer() throws IllegalStateException, IOException, Exception {
        this.localScaninfos = getApkInfo();
        new Preferences(this.mContext).setLastCloudScanTime(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
        if (this.isRun) {
            if (!checkNet(this.mContext)) {
                errorNotify(4);
                return;
            }
            setDiaLogCommand(1);
            sendUiMessage(R.string.text_cloud_send_server);
            System.gc();
            byte[] requestData = new CloudRequest(getPublicData(), this.localScaninfos, this.mContext).getRequestBytes();
            if (this.isRun) {
                Request request = new Request();
                request.setWifi(HttpUtil.isWifi(this.mContext));
                request.setStream(true);
                request.setSendDatas(requestData);
                request.setUrl(this.CLOUD_SERVER_URL);
                request.setProxy(NqUtil.getApnProxy(this.mContext));
                request.setSchema(0);
                if (this.isRun && this.isRun) {
                    HttpHandler.getInstance().startConn(this, request, this.mContext);
                }
            }
        }
    }

    private void reportSuspiciousAndServer(Rate rate2) throws IllegalStateException, IOException, Exception {
        new Preferences(this.mContext).setLastCloudScanTime(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
        if (this.isRun) {
            if (!checkNet(this.mContext)) {
                errorNotify(4);
                return;
            }
            setDiaLogCommand(1);
            sendUiMessage(R.string.text_cloud_send_server);
            System.gc();
            byte[] requestData = new CloudRequest(getPublicData(), this.localScaninfos, this.mContext).getReportSuspiciousRequestByte(rate2);
            if (this.isRun) {
                Request request = new Request();
                request.setWifi(HttpUtil.isWifi(this.mContext));
                request.setStream(true);
                request.setSendDatas(requestData);
                request.setUrl(this.CLOUD_SERVER_URL);
                request.setProxy(NqUtil.getApnProxy(this.mContext));
                request.setSchema(1);
                if (this.isRun && this.isRun) {
                    HttpHandler.getInstance().startConn(this, request, this.mContext);
                }
            }
        }
    }

    private void scoreReviewAndServer(Rate rate2) throws IllegalStateException, IOException, Exception {
        new Preferences(this.mContext).setLastCloudScanTime(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
        if (this.isRun) {
            if (!checkNet(this.mContext)) {
                errorNotify(4);
                return;
            }
            setDiaLogCommand(1);
            sendUiMessage(R.string.text_cloud_send_server);
            System.gc();
            byte[] requestData = new CloudRequest(getPublicData(), this.localScaninfos, this.mContext).getScoreReviewRequestByte(rate2);
            if (this.isRun) {
                Request request = new Request();
                request.setWifi(HttpUtil.isWifi(this.mContext));
                request.setStream(true);
                request.setSendDatas(requestData);
                request.setUrl(this.CLOUD_SERVER_URL);
                request.setProxy(NqUtil.getApnProxy(this.mContext));
                request.setSchema(2);
                if (this.isRun && this.isRun) {
                    HttpHandler.getInstance().startConn(this, request, this.mContext);
                }
            }
        }
    }

    private void moreScoreReviewAndServer(Rate rate2) throws IllegalStateException, IOException, Exception {
        new Preferences(this.mContext).setLastCloudScanTime(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
        if (this.isRun) {
            if (!checkNet(this.mContext)) {
                errorNotify(4);
                return;
            }
            setDiaLogCommand(1);
            sendUiMessage(R.string.text_cloud_send_server);
            System.gc();
            byte[] requestData = new CloudRequest(getPublicData(), this.localScaninfos, this.mContext).getMoreScoreReviewRequestByte(rate2);
            if (this.isRun) {
                Request request = new Request();
                request.setWifi(HttpUtil.isWifi(this.mContext));
                request.setStream(true);
                request.setSendDatas(requestData);
                request.setUrl(this.CLOUD_SERVER_URL);
                request.setProxy(NqUtil.getApnProxy(this.mContext));
                request.setSchema(3);
                if (this.isRun && this.isRun) {
                    HttpHandler.getInstance().startConn(this, request, this.mContext);
                }
            }
        }
    }

    private void cloudPassageAndServer(Rate rate2) throws IllegalStateException, IOException, Exception {
        new Preferences(this.mContext).setLastCloudScanTime(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
        if (this.isRun) {
            if (!checkNet(this.mContext)) {
                errorNotify(4);
                return;
            }
            setDiaLogCommand(1);
            sendUiMessage(R.string.text_cloud_send_server);
            System.gc();
            byte[] requestData = new CloudRequest(getPublicData(), this.localScaninfos, this.mContext).getCloudPassageRequestByte(rate2);
            if (this.isRun) {
                Request request = new Request();
                request.setWifi(HttpUtil.isWifi(this.mContext));
                request.setStream(true);
                request.setSendDatas(requestData);
                request.setUrl(this.CLOUD_SERVER_URL);
                request.setProxy(NqUtil.getApnProxy(this.mContext));
                request.setSchema(4);
                if (this.isRun && this.isRun) {
                    HttpHandler.getInstance().startConn(this, request, this.mContext);
                }
            }
        }
    }

    private void errorNotify(int errorType) {
        if (this.isRun) {
            this.listener.error(errorType);
        }
    }

    private void responseResult(byte[] responseData) throws Exception {
        String uploadUrl;
        if (responseData == null) {
            errorNotify(1);
        } else if (this.isRun) {
            setDiaLogCommand(2);
            sendUiMessage(R.string.text_cloud_server_response);
            this.cloudResult = new CloudResponse(responseData, this.mContext);
            this.info = this.cloudResult.info;
            this.reportUploderUrl = this.cloudResult.getReportUploderUrl();
            this.apkIsNeed = this.cloudResult.getApkIsNeed();
            this.cloudScanId = this.cloudResult.getReportScanId();
            this.resultList = new ArrayList<>();
            if (isHarmList(this.cloudResult)) {
                setDiaLogCommand(7);
            } else {
                setDiaLogCommand(8);
            }
            if (this.showDialog) {
                this.listener.complete(CloudListener.CLOUD_SCAN_SUCCEES);
            }
            if (isAutoUploadList(this.cloudResult) && (uploadUrl = getUploadUrl(this.cloudResult.getReportUrl())) != null && uploadUrl.trim().length() != 0) {
                this.mContext.getSharedPreferences("netqin", 0).edit().putString("upload_suspect_2_server_url", uploadUrl).commit();
                Thread.sleep(10000);
                boolean isWifiConn = false;
                NetworkInfo info2 = ((ConnectivityManager) this.mContext.getSystemService("connectivity")).getNetworkInfo(1);
                if (info2 != null) {
                    isWifiConn = info2.isConnected();
                }
                if (isWifiConn) {
                    try {
                        Uploader.getInstance(this.mContext).startUploadFile(null, uploadUrl, this.mmHander);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Uploader.getInstance(this.mContext).addFileToUpload(null);
                }
            }
        }
    }

    private void setDiaLogCommand(int command2) {
        this.diaLogCommand = command2;
    }

    public String getUploadUrl(String responseUrl) {
        StringBuffer sb = new StringBuffer();
        sb.append(responseUrl);
        sb.append("?");
        sb.append("biz_id=");
        sb.append("101");
        sb.append("&edition_id=");
        sb.append(CommonDefine.ANTIVIRUS_VERID);
        sb.append("&uid=");
        sb.append(CommonMethod.getUID(this.mContext));
        sb.append("&ext=apk");
        sb.append("&p=");
        return sb.toString();
    }

    private boolean isAutoUploadList(CloudResponse cloudResult2) {
        if (cloudResult2.getAutoUpdateInfos().size() > 0) {
            String uploadPackageName = "";
            Iterator<CloudApkInfo> it = cloudResult2.getAutoUpdateInfos().iterator();
            while (it.hasNext()) {
                CloudApkInfo info2 = it.next();
                if (this.localScaninfos != null) {
                    uploadPackageName = String.valueOf(uploadPackageName) + this.localScaninfos.get(Integer.parseInt(info2.getId())).getInstallPath() + "\n";
                }
            }
            return true;
        }
        try {
            File file2 = new File(cache_auto_upload_path());
            if (file2.exists()) {
                file2.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean isHarmList(CloudResponse cloudResult2) {
        SharedPreferences sharedPreferences = this.mContext.getSharedPreferences("neqin_virus", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove("viruslog").commit();
        if (cloudResult2.getHarmInfos().size() > 0) {
            CloudApkInfo scanInfo = null;
            Iterator<CloudApkInfo> it = cloudResult2.getHarmInfos().iterator();
            while (it.hasNext()) {
                CloudApkInfo info2 = it.next();
                HashMap<String, Object> map = new HashMap<>();
                int cnt = this.localScaninfos.size();
                int i = 0;
                while (true) {
                    if (i >= cnt) {
                        break;
                    } else if (Integer.parseInt(this.localScaninfos.get(i).getId()) == Integer.parseInt(info2.getId())) {
                        scanInfo = this.localScaninfos.get(i);
                        break;
                    } else {
                        i++;
                    }
                }
                if (scanInfo == null) {
                    return false;
                }
                byte[] rsaByte = readFileByte(String.valueOf(this.mContext.getFilesDir().getAbsolutePath()) + "/" + info2.getPkgName());
                if (rsaByte == null || rsaByte.length <= 0) {
                    scanInfo.setCertRSA(getSignFile(scanInfo.getInstallPath(), ".RSA", cert_rsa_path()));
                    craeteFile(String.valueOf(this.mContext.getFilesDir().getAbsolutePath()) + "/" + scanInfo.getPkgName(), scanInfo.getCertRSA());
                } else {
                    scanInfo.setCertRSA(readFileByte(String.valueOf(this.mContext.getFilesDir().getAbsolutePath()) + "/" + info2.getPkgName()));
                }
                map.put(KEY_ITEM_TITLE, scanInfo.getName());
                map.put(KEY_ITEM_DESC, info2.getAdvice());
                map.put(KEY_ITEM_CHECKED, true);
                map.put(KEY_ITEM_ICON, getInstalledIcon(scanInfo.getPkgName()));
                map.put(KEY_ITEM_PACKAGENAME, scanInfo.getPkgName());
                map.put(KEY_ITEM_PATH, scanInfo.getInstallPath());
                StringBuffer buffer = new StringBuffer();
                String virusData = sharedPreferences.getString("viruslog", "");
                String[] virusArray = virusData.split("\n");
                if (virusArray.length > 0) {
                    for (String trim : virusArray) {
                        if (scanInfo.getInstallPath().trim().equals(trim.trim())) {
                            this.isRepeat = true;
                        }
                    }
                }
                if (!this.isRepeat) {
                    buffer.append(String.valueOf(virusData) + "\n" + scanInfo.getInstallPath());
                    editor.putString("viruslog", buffer.toString());
                    editor.commit();
                }
                this.cloud_virus_db = new CloudVirusdb(this.mContext);
                if (!this.cloud_virus_db.isRepeat(scanInfo.getPkgName(), scanInfo.getCertRSA(), null)) {
                    this.cloud_virus_db.inster(scanInfo);
                }
                this.cloud_virus_db.close_virus_DB();
                this.resultList.add(map);
            }
            return true;
        }
        if (this.cloud_virus_db != null) {
            this.cloud_virus_db.close_virus_DB();
        }
        return false;
    }

    public ArrayList<HashMap<String, Object>> getResultList() {
        return this.resultList;
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public void changeResultList(int position, boolean checked) {
        if (this.resultList != null) {
            this.resultList.get(position).put(KEY_ITEM_CHECKED, Boolean.valueOf(checked));
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 1 */
    public boolean getResultList(int position) {
        if (this.resultList == null) {
            return false;
        }
        return ((Boolean) this.resultList.get(position).get(KEY_ITEM_CHECKED)).booleanValue();
    }

    public void changeResultListTrue() {
        if (this.resultList != null) {
            for (int i = 0; i < this.resultList.size(); i++) {
                if (new File((String) this.resultList.get(i).get(KEY_ITEM_PATH)).exists()) {
                    this.resultList.get(i).put(KEY_ITEM_CHECKED, true);
                } else {
                    this.resultList.get(i).put(KEY_ITEM_CHECKED, false);
                }
            }
        }
    }

    public int getResultListCheckedSize() {
        if (this.resultList == null) {
            return -1;
        }
        this.resultCheckedList = new ArrayList<>();
        Iterator<HashMap<String, Object>> it = this.resultList.iterator();
        while (it.hasNext()) {
            HashMap<String, Object> map = it.next();
            if (Boolean.parseBoolean(map.get(KEY_ITEM_CHECKED).toString())) {
                this.resultCheckedList.add(map);
            }
        }
        return this.resultCheckedList.size();
    }

    private void sendUiMessage(int StringId) {
        if (this.isRun && this.showDialog) {
            Bundle bundle = new Bundle();
            bundle.putInt(KEY_PROGRESS_TYPE, 1);
            bundle.putString(KEY_PROGRESS_SCAN, this.mContext.getString(StringId));
            this.listener.process(bundle);
        }
    }

    private ArrayList<CloudApkInfo> getApkInfo() {
        Exception e;
        int index;
        PackageManager pm = this.mContext.getPackageManager();
        List<PackageInfo> packs = pm.getInstalledPackages(0);
        ArrayList<CloudApkInfo> infos = new ArrayList<>();
        StringBuffer scanDoingStr = new StringBuffer();
        int appCnt = 0;
        int i = 0;
        int len = packs.size();
        int index2 = 0;
        while (i < len && this.isRun) {
            CloudApkInfo info2 = new CloudApkInfo();
            try {
                PackageInfo p = DD02zqNU.DLLIxskak(pm, packs.get(i).packageName, 64);
                if (p.applicationInfo.publicSourceDir.indexOf("system") == -1) {
                    appCnt++;
                    index = index2 + 1;
                    try {
                        info2.setId(new StringBuilder(String.valueOf(index2)).toString());
                        info2.setName(p.applicationInfo.loadLabel(pm).toString());
                        info2.setPkgName(p.packageName);
                        info2.setInstallPath(p.applicationInfo.publicSourceDir);
                        info2.setIntallTime(getInstallTime(p.applicationInfo.publicSourceDir));
                        info2.setManiFest(getManifestStr(info2.getInstallPath()));
                        byte[] rsaByte = readFileByte(String.valueOf(this.mContext.getFilesDir().getAbsolutePath()) + "/" + info2.getPkgName());
                        if (rsaByte == null || rsaByte.length <= 0) {
                            info2.setCertRSA(getSignFile(info2.getInstallPath(), ".RSA", cert_rsa_path()));
                            craeteFile(String.valueOf(this.mContext.getFilesDir().getAbsolutePath()) + "/" + info2.getPkgName(), info2.getCertRSA());
                        } else {
                            info2.setCertRSA(readFileByte(String.valueOf(this.mContext.getFilesDir().getAbsolutePath()) + "/" + info2.getPkgName()));
                        }
                        infos.add(info2);
                    } catch (Exception e2) {
                        e = e2;
                    }
                } else {
                    index = index2;
                }
                scanDoingStr.insert(0, String.valueOf(p.applicationInfo.loadLabel(pm).toString()) + "\n");
            } catch (Exception e3) {
                e = e3;
                index = index2;
                e.printStackTrace();
                Bundle bundle = new Bundle();
                bundle.putInt(KEY_PROGRESS_TYPE, 0);
                bundle.putString(KEY_PROGRESS_SCAN, scanDoingStr.toString());
                bundle.putString(KEY_PROGRESS_SCANED_NUM, String.format(this.mContext.getString(R.string.text_cloud_scaned_num), String.valueOf(i + 1) + "/" + len));
                this.listener.process(bundle);
                i++;
                index2 = index;
            }
            Bundle bundle2 = new Bundle();
            bundle2.putInt(KEY_PROGRESS_TYPE, 0);
            bundle2.putString(KEY_PROGRESS_SCAN, scanDoingStr.toString());
            bundle2.putString(KEY_PROGRESS_SCANED_NUM, String.format(this.mContext.getString(R.string.text_cloud_scaned_num), String.valueOf(i + 1) + "/" + len));
            this.listener.process(bundle2);
            i++;
            index2 = index;
        }
        return infos;
    }

    private Object getInstalledIcon(String packageName) {
        PackageManager pm = this.mContext.getPackageManager();
        try {
            return DataUtils.file.drawableToBitmap(DD02zqNU.DLLIxskak(pm, packageName, 1).applicationInfo.loadIcon(pm));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    private String getApkSha1(String filePath) throws Exception {
        this.file = new File(filePath);
        this.in = new BufferedInputStream(new FileInputStream(this.file), 4096);
        int length = (int) DD02zqNU.Zob2JfG1Yi11hrq(this.file);
        this.temp = new byte[length];
        this.in.read(this.temp, 0, length);
        String str = DataUtils.toHexString(MessageDigest.getInstance("SHA-1").digest(this.temp));
        this.file = null;
        this.in.close();
        this.in = null;
        this.temp = null;
        return str;
    }

    public void startDealResult(Handler handler, final Context context) {
        new Thread(new Runnable() {
            public void run() {
                int size = CloudHandler.this.resultCheckedList.size();
                Iterator<HashMap<String, Object>> it = CloudHandler.this.resultCheckedList.iterator();
                while (it.hasNext()) {
                    HashMap<String, Object> map = it.next();
                    if (new File(map.get(CloudHandler.KEY_ITEM_PATH).toString()).exists()) {
                        boolean unused = CloudHandler.this.clearHarmData(map.get(CloudHandler.KEY_ITEM_PACKAGENAME).toString(), context);
                    }
                }
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public boolean clearHarmData(String packageName, Context context) {
        uninstallPackage(packageName, context);
        return true;
    }

    public static void uninstallPackage(String packageName, Context context) {
        try {
            context.startActivity(new Intent("android.intent.action.DELETE", Uri.fromParts("package", packageName, null)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public byte[] getManifestStr(String apkPath) throws Exception {
        return DataUtils.Zip.extractFromZipFile(apkPath, destPath(), "AndroidManifest.xml");
    }

    public static byte[] getSignFile(String apkPath, String filterName, String dPath) {
        CommonMethod.logDebug("CloudResponse", "getSignFile 11");
        byte[] resultArray = DataUtils.Zip.extractFromZipFileMatchExtension(apkPath, dPath, filterName);
        CommonMethod.logDebug("CloudResponse", "getSignFile 12");
        if (resultArray == null && filterName == ".RSA") {
            return DataUtils.Zip.extractFromZipFileMatchExtension(apkPath, dPath, ".DSA");
        }
        return resultArray;
    }

    private String[] getCerts(PackageInfo p) {
        String[] certs = new String[p.signatures.length];
        int len = p.signatures.length;
        for (int i = 0; i < len; i++) {
            certs[i] = p.signatures[i].toCharsString();
        }
        return certs;
    }

    public static boolean isAllowInstallOther(Context context) {
        if (Settings.Secure.getInt(context.getContentResolver(), "install_non_market_apps", 0) == 0) {
            return true;
        }
        return false;
    }

    public static boolean isRootPower() {
        File system_su = new File(String.valueOf(Environment.getRootDirectory().getPath().trim()) + "/bin/su");
        File system_xsu = new File(String.valueOf(Environment.getRootDirectory().getPath().trim()) + "/xbin/su");
        File system_ssu = new File(String.valueOf(Environment.getRootDirectory().getPath().trim()) + "/sbin/su");
        if (system_su.exists() || system_xsu.exists() || system_ssu.exists()) {
            return true;
        }
        return false;
    }

    private String getInstallTime(String path) {
        this.file = new File(path);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String date = simpleDateFormat.format(new Date(DD02zqNU.ZbcazCVWIIzso(this.file)));
        try {
            simpleDateFormat.parse(date);
            return date;
        } catch (ParseException e) {
            String date2 = simpleDateFormat.format(new Date());
            e.printStackTrace();
            return date2;
        }
    }

    public static String getPackageInstallTime(String path) {
        File file2 = new File(path);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String date = simpleDateFormat.format(new Date(DD02zqNU.ZbcazCVWIIzso(file2)));
        try {
            simpleDateFormat.parse(date);
            return date;
        } catch (ParseException e) {
            String date2 = simpleDateFormat.format(new Date());
            e.printStackTrace();
            return date2;
        }
    }

    public static String getFormattedKernelVersion() {
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader("/proc/version"), 256);
            String procVersionStr = reader.readLine();
            reader.close();
            Matcher m = Pattern.compile("\\w+\\s+\\w+\\s+([^\\s]+)\\s+\\(([^\\s@]+(?:@[^\\s.]+)?)[^)]*\\)\\s+\\([^)]+\\)\\s+([^\\s]+)\\s+(?:PREEMPT\\s+)?(.+)").matcher(procVersionStr);
            if (!m.matches()) {
                return "Unavailable";
            }
            if (m.groupCount() < 4) {
                return "Unavailable";
            }
            return m.group(1) + "\n" + m.group(2) + " " + m.group(3) + "\n" + m.group(4);
        } catch (IOException e) {
            return "Unavailable";
        } catch (Throwable th) {
            reader.close();
            throw th;
        }
    }

    public String readFile(String path) {
        String str = "";
        try {
            this.file = new File(path);
            this.in = new BufferedInputStream(new FileInputStream(this.file), 4096);
            int length = (int) DD02zqNU.Zob2JfG1Yi11hrq(this.file);
            byte[] temp2 = new byte[length];
            this.in.read(temp2, 0, length);
            str = EncodingUtils.getString(temp2, "utf-8");
            this.in.close();
            byte[] temp3 = null;
            this.in = null;
            this.file = null;
            return str;
        } catch (IOException e) {
            return str;
        }
    }

    public byte[] getFileByte(String path) {
        byte[] temp2 = null;
        try {
            this.file = new File(path);
            if (!this.file.exists()) {
                return null;
            }
            this.in = new BufferedInputStream(new FileInputStream(this.file), 4096);
            int length = (int) DD02zqNU.Zob2JfG1Yi11hrq(this.file);
            temp2 = new byte[length];
            this.in.read(temp2, 0, length);
            this.in.close();
            this.in = null;
            this.file = null;
            return temp2;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static byte[] readFileByte(String path) {
        byte[] temp2 = null;
        try {
            File file2 = new File(path);
            if (!file2.exists()) {
                return null;
            }
            BufferedInputStream in2 = new BufferedInputStream(new FileInputStream(file2), 4096);
            int length = (int) DD02zqNU.Zob2JfG1Yi11hrq(file2);
            temp2 = new byte[length];
            in2.read(temp2, 0, length);
            in2.close();
            return temp2;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static boolean checkNet(Context context) {
        if (((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo() != null) {
            return true;
        }
        return false;
    }

    public static void craeteFile(String path, byte[] bytes) {
        IOException e;
        FileNotFoundException e2;
        File f = new File(path);
        if (!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException e3) {
                e3.printStackTrace();
                return;
            }
        }
        try {
            FileOutputStream fout = new FileOutputStream(f);
            try {
                fout.write(bytes);
                fout.close();
            } catch (FileNotFoundException e4) {
                e2 = e4;
                e2.printStackTrace();
            } catch (IOException e5) {
                e = e5;
                e.printStackTrace();
            }
        } catch (FileNotFoundException e6) {
            e2 = e6;
            e2.printStackTrace();
        } catch (IOException e7) {
            e = e7;
            e.printStackTrace();
        }
    }

    public void Exception(Exception e) {
        errorNotify(5);
        ApkInfoActivity.isCloud = false;
    }

    public void completed(Response data) {
        if (this.isRun) {
            if (data == null) {
                if (this.mmHander != null) {
                    Message message = new Message();
                    message.what = 10;
                    this.mmHander.sendMessage(message);
                }
                this.listener.complete(1001);
                errorNotify(5);
                return;
            }
            byte[] responseData = null;
            if (data.getResponseData() != null) {
                responseData = DataUtils.decryptDecompress(data.getResponseData());
            }
            switch (data.getScheme()) {
                case 0:
                    if (this.isRun) {
                        try {
                            new String(responseData, "utf-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        try {
                            responseResult(responseData);
                            new CloudPassage(this.mContext).setScan_id(this.cloudScanId);
                            if (this.mmHander != null) {
                                Message message2 = new Message();
                                message2.what = 0;
                                message2.obj = this.cloudResult.getAllApkInfos();
                                this.mmHander.sendMessage(message2);
                            }
                        } catch (Exception e2) {
                            errorNotify(6);
                            this.listener.complete(1001);
                            if (this.mmHander != null) {
                                Message message3 = new Message();
                                message3.what = 9;
                                this.mmHander.sendMessage(message3);
                            }
                        }
                        ApkInfoActivity.isCloud = false;
                        return;
                    }
                    return;
                case 1:
                    if (this.isRun) {
                        try {
                            new String(responseData, "utf-8");
                        } catch (UnsupportedEncodingException e3) {
                            e3.printStackTrace();
                        }
                        try {
                            responseResult(responseData);
                            Message message4 = new Message();
                            message4.what = 1;
                            Bundle bundle = new Bundle();
                            bundle.putString("apkIsNeed", this.apkIsNeed);
                            bundle.putString("reportUrl", this.reportUploderUrl);
                            message4.setData(bundle);
                            this.mmHander.sendMessage(message4);
                        } catch (Exception e4) {
                            Message message5 = new Message();
                            message5.what = 4;
                            this.mmHander.sendMessage(message5);
                            e4.printStackTrace();
                        }
                        return;
                    }
                    return;
                case 2:
                    if (this.isRun) {
                        try {
                            new String(responseData, "utf-8");
                        } catch (UnsupportedEncodingException e5) {
                            e5.printStackTrace();
                        }
                        try {
                            responseResult(responseData);
                            Message message6 = new Message();
                            message6.what = 2;
                            message6.obj = this.info;
                            this.mmHander.sendMessage(message6);
                        } catch (Exception e6) {
                            Message message7 = new Message();
                            message7.what = 6;
                            this.mmHander.sendMessage(message7);
                            e6.printStackTrace();
                        }
                        return;
                    }
                    return;
                case 3:
                    if (this.isRun) {
                        try {
                            new String(responseData, "utf-8");
                        } catch (UnsupportedEncodingException e7) {
                            e7.printStackTrace();
                        }
                        try {
                            responseResult(responseData);
                            Message message8 = new Message();
                            message8.what = 3;
                            message8.obj = this.info.getReviews();
                            this.mmHander.sendMessage(message8);
                        } catch (Exception e8) {
                            Message message9 = new Message();
                            message9.what = 7;
                            this.mmHander.sendMessage(message9);
                            e8.printStackTrace();
                        }
                        return;
                    }
                    return;
                case 4:
                    if (this.isRun) {
                        try {
                            new String(responseData, "utf-8");
                        } catch (UnsupportedEncodingException e9) {
                            e9.printStackTrace();
                        }
                        try {
                            responseResult(responseData);
                            CommonMethod.deleteFile(this.mContext.getFilesDir() + XmlUtils.PERFORMANCE_DAT);
                            CommonMethod.deleteFile(this.mContext.getFilesDir() + XmlUtils.URL_PERFORMANCE_DAT);
                        } catch (Exception e10) {
                            e10.printStackTrace();
                        }
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }
}
