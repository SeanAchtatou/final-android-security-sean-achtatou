package com.netqin.antivirus.cloud.model.http;

public class Response {
    private int responseCode;
    private byte[] responseData;
    private int scheme;

    public int getResponseCode() {
        return this.responseCode;
    }

    public void setResponseCode(int responseCode2) {
        this.responseCode = responseCode2;
    }

    public byte[] getResponseData() {
        return this.responseData;
    }

    public void setResponseData(byte[] responseData2) {
        this.responseData = responseData2;
    }

    public int getScheme() {
        return this.scheme;
    }

    public void setScheme(int scheme2) {
        this.scheme = scheme2;
    }
}
