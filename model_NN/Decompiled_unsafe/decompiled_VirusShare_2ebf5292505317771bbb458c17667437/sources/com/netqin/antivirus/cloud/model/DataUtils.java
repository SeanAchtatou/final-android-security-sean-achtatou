package com.netqin.antivirus.cloud.model;

import SHcMjZX.DD02zqNU;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import com.netqin.antivirus.log.LogEngine;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.apache.commons.codec.binary.Base64;

public class DataUtils {
    static int CRYPTKEY = LogEngine.LOG_VIRUS_FOUND;
    public static final String ENCRYPT_KEY = "0x8F*NQ18#KeYSecuRItY";
    static char[] hexChar = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    public static String decryptForXml(String key, String value) {
        return RunRC4(new String(new Base64().decode(value.getBytes())), key);
    }

    public static String encryptForXml(String key, String value) {
        return new String(new Base64().encode(RunRC4(value, key).getBytes()));
    }

    /* JADX INFO: Multiple debug info for r0v11 char[]: [D('iInputChar' char[]), D('iMask' short)] */
    /* JADX INFO: Multiple debug info for r1v1 char[]: [D('iOutputChar' char[]), D('iK' byte[])] */
    /* JADX INFO: Multiple debug info for r7v4 int: [D('i' int), D('j' int)] */
    /* JADX INFO: Multiple debug info for r7v6 int: [D('j' int), D('temp' int)] */
    /* JADX INFO: Multiple debug info for r7v10 int: [D('t' int), D('iY' int)] */
    /* JADX INFO: Multiple debug info for r7v11 char: [D('iY' int), D('iCY' char)] */
    public static String RunRC4(String aInput, String aKey) {
        int[] iS = new int[256];
        byte[] iK = new byte[256];
        for (int i = 0; i < 256; i++) {
            iS[i] = i;
        }
        for (short i2 = 0; i2 < 256; i2 = (short) (i2 + 1)) {
            iK[i2] = (byte) aKey.charAt(i2 % aKey.length());
        }
        int j = 0;
        for (int i3 = 0; i3 < 255; i3++) {
            j = ((j + iS[i3]) + iK[i3]) % 256;
            int temp = iS[i3];
            iS[i3] = iS[j];
            iS[j] = temp;
        }
        int j2 = 0;
        char[] iInputChar = aInput.toCharArray();
        char[] iOutputChar = new char[iInputChar.length];
        short x = 0;
        int i4 = 0;
        while (true) {
            int j3 = j2;
            if (x >= iInputChar.length) {
                return new String(iOutputChar);
            }
            i4 = (i4 + 1) % 256;
            j2 = (j3 + iS[i4]) % 256;
            int temp2 = iS[i4];
            iS[i4] = iS[j2];
            iS[j2] = temp2;
            iOutputChar[x] = (char) (((char) iS[(iS[i4] + (iS[j2] % 256)) % 256]) ^ iInputChar[x]);
            x = (short) (x + 1);
        }
    }

    public static byte[] decrypt(byte[] bytes) {
        int len = bytes.length;
        for (int i = 0; i < len; i++) {
            bytes[i] = (byte) (bytes[i] ^ CRYPTKEY);
        }
        return bytes;
    }

    public static byte[] ecrypt(byte[] bytes) {
        int len = bytes.length;
        for (int i = 0; i < len; i++) {
            bytes[i] = (byte) (bytes[i] ^ CRYPTKEY);
        }
        return bytes;
    }

    public static byte[] ecryptCompress(byte[] bytes) {
        byte[] ecrypt = ecrypt(zlib.compressData(bytes));
        return ecrypt(zlib.compressData(bytes));
    }

    public static byte[] decryptDecompress(byte[] bytes) {
        return zlib.decompressData(decrypt(bytes));
    }

    public static String toHexString(byte[] b) {
        StringBuffer sb = new StringBuffer(b.length * 2);
        for (int i = 0; i < b.length; i++) {
            sb.append(hexChar[(b[i] & 240) >>> 4]);
            sb.append(hexChar[b[i] & 15]);
        }
        return sb.toString();
    }

    public static class Zip {
        public static void doDecompression(String apkPathFile, String DestPath, String filter) {
            try {
                FileInputStream fins = new FileInputStream(apkPathFile);
                ZipInputStream zins = new ZipInputStream(fins);
                byte[] ch = new byte[256];
                while (true) {
                    ZipEntry ze = zins.getNextEntry();
                    if (ze == null) {
                        fins.close();
                        zins.close();
                        byte[] ch2 = null;
                        return;
                    } else if (ze.getName().equals(filter)) {
                        File zfile = new File(String.valueOf(DestPath) + ze.getName());
                        File fpath = new File(zfile.getParentFile().getPath());
                        if (ze.isDirectory()) {
                            if (!zfile.exists()) {
                                zfile.mkdirs();
                            }
                            zins.closeEntry();
                        } else {
                            if (!fpath.exists()) {
                                fpath.mkdirs();
                            }
                            FileOutputStream fouts = new FileOutputStream(zfile);
                            while (true) {
                                int i = zins.read(ch);
                                if (i == -1) {
                                    break;
                                }
                                fouts.write(ch, 0, i);
                            }
                            zins.closeEntry();
                            fouts.close();
                        }
                    }
                }
            } catch (Exception e) {
            }
        }

        public static byte[] extractFromZipFile(String apkPathFile, String DestPath, String filter) {
            try {
                FileInputStream fins = new FileInputStream(apkPathFile);
                ZipInputStream zins = new ZipInputStream(fins);
                byte[] ch = new byte[256];
                while (true) {
                    ZipEntry ze = zins.getNextEntry();
                    if (ze == null) {
                        fins.close();
                        zins.close();
                        byte[] ch2 = null;
                        break;
                    } else if (ze.getName().equals(filter)) {
                        if (ze.isDirectory()) {
                            zins.closeEntry();
                        } else {
                            ByteArrayOutputStream bos = new ByteArrayOutputStream();
                            while (true) {
                                int i = zins.read(ch);
                                if (i == -1) {
                                    zins.closeEntry();
                                    fins.close();
                                    zins.close();
                                    byte[] ch3 = null;
                                    return bos.toByteArray();
                                }
                                bos.write(ch, 0, i);
                            }
                        }
                    }
                }
            } catch (Exception e) {
            }
            return null;
        }

        public static boolean doDecompressionMatchExtension(String apkPathFile, String DestPath, String filter) {
            boolean res = false;
            try {
                FileInputStream fins = new FileInputStream(apkPathFile);
                ZipInputStream zins = new ZipInputStream(fins);
                byte[] ch = new byte[256];
                while (true) {
                    ZipEntry ze = zins.getNextEntry();
                    if (ze == null) {
                        break;
                    } else if (ze.getName().endsWith(filter)) {
                        File zfile = new File(DestPath);
                        File fpath = new File(zfile.getParentFile().getPath());
                        if (ze.isDirectory()) {
                            if (!zfile.exists()) {
                                zfile.mkdirs();
                            }
                            zins.closeEntry();
                        } else {
                            if (!fpath.exists()) {
                                fpath.mkdirs();
                            }
                            FileOutputStream fouts = new FileOutputStream(zfile);
                            while (true) {
                                int i = zins.read(ch);
                                if (i == -1) {
                                    break;
                                }
                                fouts.write(ch, 0, i);
                            }
                            zins.closeEntry();
                            fouts.close();
                        }
                        res = true;
                    }
                }
                fins.close();
                zins.close();
                byte[] ch2 = null;
            } catch (Exception e) {
            }
            return res;
        }

        public static byte[] extractFromZipFileMatchExtension(String apkPathFile, String DestPath, String filter) {
            try {
                FileInputStream fins = new FileInputStream(apkPathFile);
                ZipInputStream zins = new ZipInputStream(fins);
                byte[] ch = new byte[256];
                while (true) {
                    ZipEntry ze = zins.getNextEntry();
                    if (ze == null) {
                        fins.close();
                        zins.close();
                        byte[] ch2 = null;
                        break;
                    } else if (ze.getName().endsWith(filter)) {
                        if (ze.isDirectory()) {
                            zins.closeEntry();
                        } else {
                            ByteArrayOutputStream bos = new ByteArrayOutputStream();
                            while (true) {
                                int i = zins.read(ch);
                                if (i == -1) {
                                    zins.closeEntry();
                                    fins.close();
                                    zins.close();
                                    byte[] ch3 = null;
                                    return bos.toByteArray();
                                }
                                bos.write(ch, 0, i);
                            }
                        }
                    }
                }
            } catch (Exception e) {
            }
            return null;
        }
    }

    static class zlib {
        zlib() {
        }

        public static byte[] compressData(byte[] bytes) {
            ByteArrayOutputStream bis = new ByteArrayOutputStream();
            try {
                byte[] tempByte = new byte[100];
                int compressedDataLength = -1;
                Deflater compresser = new Deflater();
                compresser.setInput(bytes);
                compresser.finish();
                while (compressedDataLength != 0) {
                    compressedDataLength = compresser.deflate(tempByte);
                    bis.write(tempByte, 0, compressedDataLength);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bis.toByteArray();
        }

        public static byte[] decompressData(byte[] bytes) {
            ByteArrayOutputStream bis = new ByteArrayOutputStream();
            int resultLength = -1;
            try {
                Inflater decompresser = new Inflater();
                decompresser.setInput(bytes, 0, bytes.length);
                byte[] result = new byte[1024];
                while (resultLength != 0) {
                    resultLength = decompresser.inflate(result);
                    bis.write(result, 0, resultLength);
                }
                decompresser.end();
                byte[] result2 = null;
            } catch (Exception e) {
            }
            return bis.toByteArray();
        }
    }

    static class file {
        file() {
        }

        public static void craeteFile(String path, byte[] bytes) {
            IOException e;
            FileNotFoundException e2;
            File f = new File(path);
            if (!f.exists()) {
                try {
                    f.createNewFile();
                } catch (IOException e3) {
                    e3.printStackTrace();
                    return;
                }
            }
            try {
                FileOutputStream fout = new FileOutputStream(f);
                try {
                    fout.write(bytes);
                    fout.close();
                } catch (FileNotFoundException e4) {
                    e2 = e4;
                    e2.printStackTrace();
                } catch (IOException e5) {
                    e = e5;
                    e.printStackTrace();
                }
            } catch (FileNotFoundException e6) {
                e2 = e6;
                e2.printStackTrace();
            } catch (IOException e7) {
                e = e7;
                e.printStackTrace();
            }
        }

        public static Bitmap drawableToBitmap(Drawable drawable) {
            Bitmap.Config config;
            int intrinsicWidth = drawable.getIntrinsicWidth();
            int intrinsicHeight = drawable.getIntrinsicHeight();
            if (drawable.getOpacity() != -1) {
                config = Bitmap.Config.ARGB_8888;
            } else {
                config = Bitmap.Config.RGB_565;
            }
            Bitmap bitmap = maixImage(Bitmap.createBitmap(intrinsicWidth, intrinsicHeight, config), 50.0f, 50.0f);
            Canvas canvas = new Canvas(bitmap);
            drawable.setBounds(0, 0, 50, 50);
            drawable.draw(canvas);
            return bitmap;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
         arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
         candidates:
          ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
          ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
        public static Bitmap maixImage(Bitmap image, float x, float y) {
            Matrix mMatrix = new Matrix();
            mMatrix.reset();
            mMatrix.setScale(x / ((float) image.getWidth()), y / ((float) image.getHeight()));
            Bitmap.createBitmap(image, 0, 0, image.getWidth(), image.getHeight(), mMatrix, true);
            return Bitmap.createBitmap(image, 0, 0, image.getWidth(), image.getHeight(), mMatrix, true);
        }
    }

    static class cmd {
        cmd() {
        }

        public static boolean requirement4Root() {
            Exception e;
            try {
                Process p = Runtime.getRuntime().exec("su");
                DataOutputStream os = new DataOutputStream(p.getOutputStream());
                try {
                    os.writeBytes("exit\n");
                    int exitValue = p.waitFor();
                    if (os != null) {
                        p.destroy();
                    }
                    if (exitValue != 0) {
                        return false;
                    }
                    return true;
                } catch (Exception e2) {
                    e = e2;
                    e.printStackTrace();
                    return false;
                }
            } catch (Exception e3) {
                e = e3;
                e.printStackTrace();
                return false;
            }
        }

        public static boolean uninstallPackageSilent(String pkgName, PackageManager pm) {
            if (!isPackageInstalled(pkgName, pm)) {
                return true;
            }
            runCmd("pm uninstall " + pkgName);
            if (!isPackageInstalled(pkgName, pm)) {
                return true;
            }
            return false;
        }

        public static boolean isPackageInstalled(String pkgName, PackageManager pm) {
            try {
                PackageInfo DLLIxskak = DD02zqNU.DLLIxskak(pm, pkgName, 129);
                return true;
            } catch (PackageManager.NameNotFoundException e) {
                return false;
            }
        }

        public static boolean runCmd(String cmd) {
            try {
                Process p = Runtime.getRuntime().exec("su");
                DataOutputStream os = new DataOutputStream(p.getOutputStream());
                os.writeBytes(String.valueOf(cmd) + "\n");
                os.writeBytes("exit\n");
                os.flush();
                if (p.waitFor() != 0) {
                    return false;
                }
                if (os != null) {
                    p.destroy();
                }
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
    }
}
