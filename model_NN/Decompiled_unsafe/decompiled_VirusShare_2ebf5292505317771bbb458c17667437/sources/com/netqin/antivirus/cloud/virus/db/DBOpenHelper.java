package com.netqin.antivirus.cloud.virus.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBOpenHelper extends SQLiteOpenHelper {
    public DBOpenHelper(Context context) {
        super(context, "virus.db", (SQLiteDatabase.CursorFactory) null, 4);
        SQLiteDatabase database = getReadableDatabase();
        onCreate(database);
        database.close();
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS virus (virusid integer primary key autoincrement, apkname varchar(20), installpath VARCHAR(12),sf blob,rsa blob,name VARCHAR(12),desc VARCHAR(12))");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table virus");
    }
}
