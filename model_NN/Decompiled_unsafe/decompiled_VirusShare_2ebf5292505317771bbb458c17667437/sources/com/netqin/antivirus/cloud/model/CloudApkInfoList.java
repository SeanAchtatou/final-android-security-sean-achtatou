package com.netqin.antivirus.cloud.model;

import android.content.Context;
import com.netqin.antivirus.cloud.apkinfo.db.CloudApiInfoDb;
import java.util.List;

public class CloudApkInfoList {
    public static List<CloudApkInfo> infoList = null;
    public static boolean isReady = true;

    public static List<CloudApkInfo> getApkInfoList(Context context) {
        if (infoList == null) {
            CloudApiInfoDb caiDb = new CloudApiInfoDb(context);
            infoList = caiDb.getAllApkData();
            caiDb.close_virus_DB();
        }
        return infoList;
    }

    public static void clearApkInfoList() {
        if (infoList != null) {
            infoList.clear();
            infoList = null;
        }
    }
}
