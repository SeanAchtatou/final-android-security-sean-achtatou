package com.netqin.antivirus.cloud.model;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;

public class CloudDataService extends Service {
    private static final String TAG = "CloudDataService";
    private final boolean DBG = false;
    /* access modifiers changed from: private */
    public Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            if (msg != null) {
                switch (msg.what) {
                    case 1:
                        CloudDataService.this.stopSelf();
                        return;
                    default:
                        return;
                }
            }
        }
    };

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        new Thread() {
            public void run() {
                Context applicationContext = CloudDataService.this.getApplicationContext();
                CloudDataService.this.getApplicationContext();
                String reportUrl = applicationContext.getSharedPreferences("netqin", 0).getString("upload_suspect_2_server_url", "");
                if (reportUrl != null && reportUrl.trim().length() != 0) {
                    try {
                        Uploader.getInstance(CloudDataService.this).setStop(false);
                        Uploader.getInstance(CloudDataService.this).startUploadFile(null, reportUrl, CloudDataService.this.handler);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();
    }
}
