package com.netqin.antivirus.cloud.model.xml;

import SHcMjZX.DD02zqNU;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.util.Log;
import com.netqin.antivirus.cloud.apkinfo.db.CloudApiInfoDb;
import com.netqin.antivirus.cloud.apkinfo.domain.Review;
import com.netqin.antivirus.cloud.apkinfo.domain.SClasse;
import com.netqin.antivirus.cloud.model.CloudApkInfo;
import com.netqin.antivirus.cloud.model.CloudApkInfoList;
import com.netqin.antivirus.cloud.model.CloudHandler;
import com.netqin.antivirus.cloud.model.DataUtils;
import com.netqin.antivirus.cloud.model.Uploader;
import com.netqin.antivirus.common.CloudPassage;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.packagemanager.ApkInfoActivity;
import com.netqin.antivirus.scan.ScanController;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class CloudResponse {
    private static final String CACHE_AUTO_UPLOAD = "/upload_info.txt";
    public static final String CERT_DSA = ".DSA";
    public static final String CERT_RSA = ".RSA";
    public static final String CERT_SF = ".SF";
    private static final String Cert_rsa_path = "/xml/META-INF/CERT.RSA";
    private static final boolean DBG = false;
    private static final String TAG = "CloudResponse";
    public static Handler handler = null;
    public static String pakName = "";
    private String NextConnectMinutes = "";
    private String apkIsNeed = "";
    public CloudApkInfo info;
    private Context mContext = null;
    private String reportScanId = "";
    private String reportUploderUrl = "";
    private String reportUrl = "";
    private byte[] response = null;
    private ArrayList<CloudApkInfo> responseApkInfos = new ArrayList<>();
    private List<Review> reviews = new ArrayList();
    private Map<String, SClasse> sClasseMap = new HashMap();
    private ArrayList<SClasse> sClasses = new ArrayList<>();

    public String cache_auto_upload_path() {
        return this.mContext.getFilesDir() + CACHE_AUTO_UPLOAD;
    }

    public CloudResponse(byte[] response2, Context mContext2) {
        this.response = response2;
        this.mContext = mContext2;
        parser();
        if (this.responseApkInfos != null && this.responseApkInfos.size() > 0 && ScanController.mScanType != 6) {
            if (CloudApkInfoList.infoList != null) {
                CloudApkInfoList.infoList.clear();
                CloudApkInfoList.infoList = null;
            }
            if (CloudApkInfoList.infoList == null) {
                CloudApkInfoList.infoList = new ArrayList();
            }
            CloudApkInfoList.infoList.addAll(this.responseApkInfos);
        } else if (CloudApkInfoList.infoList != null) {
            if (this.info != null) {
                boolean exist = false;
                int i = 0;
                while (true) {
                    if (i >= CloudApkInfoList.infoList.size()) {
                        break;
                    } else if (this.info.getPkgName().equals(CloudApkInfoList.infoList.get(i).getPkgName())) {
                        CloudApkInfoList.infoList.get(i).setAdvice(this.info.getAdvice());
                        CloudApkInfoList.infoList.get(i).setCnt(this.info.getCnt());
                        CloudApkInfoList.infoList.get(i).setCntUniq(this.info.getCntUniq());
                        CloudApkInfoList.infoList.get(i).setNote(this.info.getNote());
                        CloudApkInfoList.infoList.get(i).setReason(this.info.getReason());
                        CloudApkInfoList.infoList.get(i).setReviews(this.info.getReviews());
                        CloudApkInfoList.infoList.get(i).setScore(this.info.getScore());
                        CloudApkInfoList.infoList.get(i).setSecurity(this.info.getSecurity());
                        CloudApkInfoList.infoList.get(i).setSecurityDesc(this.info.getSecurityDesc());
                        CloudApkInfoList.infoList.get(i).setServerId(this.info.getServerId());
                        CloudApkInfoList.infoList.get(i).setVirusName(this.info.getVirusName());
                        CloudApkInfoList.infoList.get(i).setWanted(this.info.getWanted());
                        CloudApkInfoList.infoList.get(i).setName(this.info.getName());
                        CloudApkInfoList.infoList.get(i).setPkgName(this.info.getPkgName());
                        exist = true;
                        break;
                    } else {
                        i++;
                    }
                }
                if (!exist) {
                    CloudApkInfoList.infoList.add(this.info);
                }
            }
            if (this.responseApkInfos != null && this.responseApkInfos.size() > 0) {
                for (int i2 = 0; i2 < this.responseApkInfos.size(); i2++) {
                    boolean exist2 = false;
                    int j = 0;
                    while (true) {
                        if (j >= CloudApkInfoList.infoList.size()) {
                            break;
                        } else if (this.responseApkInfos.get(i2).getPkgName().equals(CloudApkInfoList.infoList.get(j).getPkgName())) {
                            CloudApkInfoList.infoList.get(j).setAdvice(this.responseApkInfos.get(i2).getAdvice());
                            CloudApkInfoList.infoList.get(j).setCnt(this.responseApkInfos.get(i2).getCnt());
                            CloudApkInfoList.infoList.get(j).setCntUniq(this.responseApkInfos.get(i2).getCntUniq());
                            CloudApkInfoList.infoList.get(j).setNote(this.responseApkInfos.get(i2).getNote());
                            CloudApkInfoList.infoList.get(j).setReason(this.responseApkInfos.get(i2).getReason());
                            CloudApkInfoList.infoList.get(j).setReviews(this.responseApkInfos.get(i2).getReviews());
                            CloudApkInfoList.infoList.get(j).setScore(this.responseApkInfos.get(i2).getScore());
                            CloudApkInfoList.infoList.get(j).setSecurity(this.responseApkInfos.get(i2).getSecurity());
                            CloudApkInfoList.infoList.get(j).setSecurityDesc(this.responseApkInfos.get(i2).getSecurityDesc());
                            CloudApkInfoList.infoList.get(j).setServerId(this.responseApkInfos.get(i2).getServerId());
                            CloudApkInfoList.infoList.get(j).setVirusName(this.responseApkInfos.get(i2).getVirusName());
                            CloudApkInfoList.infoList.get(j).setWanted(this.responseApkInfos.get(i2).getWanted());
                            CloudApkInfoList.infoList.get(j).setName(this.responseApkInfos.get(i2).getName());
                            CloudApkInfoList.infoList.get(j).setPkgName(this.responseApkInfos.get(i2).getPkgName());
                            exist2 = true;
                            break;
                        } else {
                            j++;
                        }
                    }
                    if (!exist2) {
                        CloudApkInfoList.infoList.add(this.responseApkInfos.get(i2));
                    }
                }
            }
        }
        new Thread(new Runnable() {
            public void run() {
                CommonMethod.logDebug(CloudResponse.TAG, "indertApkData 1");
                CloudResponse.this.insertApkData();
                CommonMethod.logDebug(CloudResponse.TAG, "indertApkData 2");
            }
        }).start();
    }

    public static Element xmlToDocumentUtf(byte[] content) throws ParserConfigurationException, SAXException, IOException {
        return (Element) DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(content)).getElementsByTagName(XmlUtils.LABEL_REPLY).item(0);
    }

    private void parser() {
        try {
            Element node = xmlToDocumentUtf(this.response);
            NodeList sclasseChild = node.getElementsByTagName(XmlUtils.SClass);
            if (sclasseChild != null) {
                int len = sclasseChild.getLength();
                for (int i = 0; i < len; i++) {
                    try {
                        this.sClasses.add(parserSClass((Element) sclasseChild.item(i)));
                        this.sClasseMap.put(parserSClass((Element) sclasseChild.item(i)).getSecurityId(), parserSClass((Element) sclasseChild.item(i)));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            NodeList child = node.getElementsByTagName(XmlUtils.LABEL_APK);
            if (child != null) {
                int len2 = child.getLength();
                for (int i2 = 0; i2 < len2; i2++) {
                    try {
                        this.responseApkInfos.add(parserApk((Element) child.item(i2)));
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
            }
            NodeList reviewChild = node.getElementsByTagName(XmlUtils.LABEL_REPLY);
            if (reviewChild != null) {
                try {
                    this.info = parserMoreReview((Element) reviewChild.item(0));
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
            }
            Element reportUrlNode = (Element) node.getElementsByTagName(XmlUtils.LABEL_REPORT).item(0);
            if (reportUrlNode != null) {
                this.reportUrl = reportUrlNode.getAttribute(XmlUtils.LABEL_REPORT_URL);
            }
            Element reportScanIdNode = (Element) node.getElementsByTagName(XmlUtils.LABEL_REPORT_SCANID).item(0);
            if (reportScanIdNode != null) {
                this.reportScanId = reportScanIdNode.getChildNodes().item(0).getNodeValue();
                new CloudPassage(this.mContext).setScan_id(this.reportScanId);
            }
            Element apkWantedUrlNode = (Element) node.getElementsByTagName(XmlUtils.APKWANTED).item(0);
            if (apkWantedUrlNode != null) {
                this.apkIsNeed = apkWantedUrlNode.getAttribute(XmlUtils.ISNEED);
                this.reportUploderUrl = Uploader.getUploadUrl(this.reportUrl, this.mContext);
            }
        } catch (ParserConfigurationException e4) {
            e4.printStackTrace();
        } catch (SAXException e5) {
            e5.printStackTrace();
        } catch (IOException e6) {
            e6.printStackTrace();
        }
    }

    public ArrayList<CloudApkInfo> getAllApkInfos() {
        return this.responseApkInfos;
    }

    /* Debug info: failed to restart local var, previous not found, register: 18 */
    private CloudApkInfo parserApk(Element node) throws Exception {
        CloudApkInfo info2 = null;
        if (ApkInfoActivity.isCloud) {
            info2 = new CloudApkInfo();
        } else {
            String id = node.getAttribute("id");
            ArrayList<CloudApkInfo> mCloudApkInfoList = ScanController.getInstance(this.mContext).mCloudApkInfoList;
            if (Integer.parseInt(id) <= mCloudApkInfoList.size()) {
                info2 = mCloudApkInfoList.get(Integer.parseInt(id));
            }
        }
        info2.setId(node.getAttribute("id"));
        info2.setServerId(node.getAttribute(XmlUtils.LABEL_SERVER_Id));
        info2.setPkgName(node.getAttribute("pkgName"));
        info2.setSecurity(node.getAttribute(XmlUtils.LABEL_APK_SECURITY));
        info2.setSecurityDesc(this.sClasseMap.get(info2.getSecurity()).getText());
        info2.setWanted(node.getAttribute(XmlUtils.LABEL_APK_WANTED));
        info2.setVirusName(node.getAttribute(XmlUtils.VIRUSNAME));
        List<Review> reviews2 = new ArrayList<>();
        NodeList children = node.getChildNodes();
        int i = children.getLength();
        while (i > 0) {
            i--;
            Node n = children.item(i);
            if (n.getNodeType() == 1) {
                String name = n.getNodeName();
                if (name.equals(XmlUtils.RATING)) {
                    NamedNodeMap atts = n.getAttributes();
                    info2.setScore(atts.getNamedItem(XmlUtils.SCORE).getNodeValue());
                    info2.setCntUniq(atts.getNamedItem(XmlUtils.CNTUNIQ).getNodeValue());
                } else if (name.equals(XmlUtils.REVIEWS)) {
                    NodeList c = n.getChildNodes();
                    info2.setCnt(n.getAttributes().item(0).getNodeValue());
                    int j = c.getLength();
                    while (j > 0) {
                        j--;
                        Node n0 = c.item(j);
                        if (n0.getNodeType() == 1) {
                            NamedNodeMap atts2 = n0.getAttributes();
                            Review review = new Review();
                            review.setServerId(info2.getServerId());
                            review.setPkgName(info2.getPkgName());
                            review.setUid(atts2.getNamedItem(XmlUtils.uid).getNodeValue());
                            review.setDate(atts2.getNamedItem(XmlUtils.DATE).getNodeValue());
                            review.setScore(atts2.getNamedItem(XmlUtils.SCORE).getNodeValue());
                            NodeList l = n0.getChildNodes();
                            if (l.getLength() > 0) {
                                review.setContent(l.item(0).getNodeValue());
                            }
                            reviews2.add(review);
                        }
                    }
                } else if (name.equals(XmlUtils.NOTE)) {
                    if (n.getChildNodes().item(0) != null) {
                        info2.setNote(n.getChildNodes().item(0).getNodeValue());
                    }
                } else if (name.equals(XmlUtils.REASON)) {
                    if (n.getChildNodes().item(0) != null) {
                        info2.setReason(n.getChildNodes().item(0).getNodeValue());
                    }
                } else if (name.equals(XmlUtils.ADVICE) && n.getChildNodes().item(0) != null) {
                    info2.setAdvice(n.getChildNodes().item(0).getNodeValue());
                }
            }
        }
        info2.setReviews(reviews2);
        return info2;
    }

    private SClasse parserSClass(Element node) throws Exception {
        SClasse sclasse = new SClasse();
        sclasse.setSecurityId(node.getAttribute(XmlUtils.id));
        sclasse.setText(node.getAttribute(XmlUtils.text));
        return sclasse;
    }

    private CloudApkInfo parserMoreReview(Element node) throws Exception {
        CloudApkInfo info2 = new CloudApkInfo();
        List<Review> reviews2 = new ArrayList<>();
        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node n = children.item(i);
            if (n.getNodeType() == 1) {
                String name = n.getNodeName();
                if (name.equals(XmlUtils.RATING)) {
                    NamedNodeMap atts = n.getAttributes();
                    info2.setScore(atts.getNamedItem(XmlUtils.SCORE).getNodeValue());
                    info2.setCntUniq(atts.getNamedItem(XmlUtils.CNTUNIQ).getNodeValue());
                } else if (name.equals(XmlUtils.REVIEWS)) {
                    NodeList c = n.getChildNodes();
                    info2.setCnt(n.getAttributes().item(0).getNodeValue());
                    int j = c.getLength();
                    while (j > 0) {
                        j--;
                        Node n0 = c.item(j);
                        if (n0.getNodeType() == 1) {
                            NamedNodeMap atts2 = n0.getAttributes();
                            Review review = new Review();
                            review.setServerId(info2.getServerId());
                            review.setPkgName(info2.getPkgName());
                            review.setUid(atts2.getNamedItem(XmlUtils.uid).getNodeValue());
                            review.setDate(atts2.getNamedItem(XmlUtils.DATE).getNodeValue());
                            review.setScore(atts2.getNamedItem(XmlUtils.SCORE).getNodeValue());
                            NodeList l = n0.getChildNodes();
                            if (l.getLength() > 0) {
                                review.setContent(l.item(0).getNodeValue());
                            }
                            reviews2.add(review);
                        }
                    }
                }
            }
        }
        info2.setReviews(reviews2);
        return info2;
    }

    public ArrayList<CloudApkInfo> getHarmInfos() {
        ArrayList<CloudApkInfo> infos = new ArrayList<>();
        Iterator<CloudApkInfo> it = this.responseApkInfos.iterator();
        while (it.hasNext()) {
            CloudApkInfo info2 = it.next();
            if (info2.isHarm()) {
                infos.add(info2);
            }
        }
        return infos;
    }

    public ArrayList<CloudApkInfo> getAutoUpdateInfos() {
        ArrayList<CloudApkInfo> infos = new ArrayList<>();
        Iterator<CloudApkInfo> it = this.responseApkInfos.iterator();
        while (it.hasNext()) {
            CloudApkInfo info2 = it.next();
            if (info2.isAutoUpload()) {
                infos.add(info2);
            }
        }
        return infos;
    }

    public String getReportUrl() {
        return this.reportUrl;
    }

    public String getNextConnectMinutes() {
        return this.NextConnectMinutes;
    }

    public String getApkIsNeed() {
        return this.apkIsNeed;
    }

    public String getReportUploderUrl() {
        return this.reportUploderUrl;
    }

    public String getReportScanId() {
        return this.reportScanId;
    }

    public void setReportScanId(String reportScanId2) {
        this.reportScanId = reportScanId2;
    }

    public ArrayList<CloudApkInfo> getResponseApkInfos() {
        return this.responseApkInfos;
    }

    public void setResponseApkInfos(ArrayList<CloudApkInfo> responseApkInfos2) {
        this.responseApkInfos = responseApkInfos2;
    }

    /* access modifiers changed from: private */
    public void insertApkData() {
        Throwable e;
        if (this.responseApkInfos.size() > 0) {
            CloudApiInfoDb apiInfoDb = null;
            try {
                PackageManager packageManager = this.mContext.getPackageManager();
                CloudApiInfoDb cloudApiInfoDb = new CloudApiInfoDb(this.mContext);
                try {
                    cloudApiInfoDb.dropTable();
                    cloudApiInfoDb.close_virus_DB();
                    apiInfoDb = new CloudApiInfoDb(this.mContext);
                    int update = 0;
                    Iterator<CloudApkInfo> it = this.responseApkInfos.iterator();
                    while (it.hasNext()) {
                        CloudApkInfo apkData = it.next();
                        boolean isUpdate = false;
                        try {
                            String packageName = apkData.getPkgName();
                            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(packageName, 1);
                            String versionCode = new StringBuilder(String.valueOf(DD02zqNU.DLLIxskak(packageManager, packageName, 1).versionCode)).toString();
                            String versionName = DD02zqNU.DLLIxskak(packageManager, packageName, 1).versionName;
                            byte[] rsaByte = CloudHandler.readFileByte(String.valueOf(this.mContext.getFilesDir().getAbsolutePath()) + "/" + packageName);
                            if (rsaByte == null || rsaByte.length <= 0) {
                                apkData.setCertRSA(getSignFile(applicationInfo.publicSourceDir, ".RSA", cert_rsa_path()));
                                if (apkData != null) {
                                    CloudHandler.craeteFile(String.valueOf(this.mContext.getFilesDir().getAbsolutePath()) + "/" + packageName, apkData.getCertRSA());
                                }
                            } else {
                                apkData.setCertRSA(CloudHandler.readFileByte(String.valueOf(this.mContext.getFilesDir().getAbsolutePath()) + "/" + packageName));
                            }
                            for (int i = apkData.getReviews().size(); i > 0; i--) {
                                Review review = apkData.getReviews().get(i - 1);
                                review.setServerId(apkData.getServerId());
                                review.setPkgName(packageName);
                                apiInfoDb.insterReview(review);
                            }
                            apkData.setVersionCode(versionCode);
                            apkData.setVersionName(versionName);
                            update = apiInfoDb.updateWholeApkInfo(packageName, apkData.getServerId(), apkData);
                            if (update > 0) {
                                isUpdate = true;
                            }
                        } catch (Exception e2) {
                            apiInfoDb.insert(apkData);
                            e2.printStackTrace();
                        } catch (Throwable th) {
                        }
                        if (update <= 0 && !isUpdate) {
                            apiInfoDb.insert(apkData);
                        }
                    }
                    if (this.sClasses.size() > 0) {
                        Iterator<SClasse> it2 = this.sClasses.iterator();
                        while (it2.hasNext()) {
                            SClasse classe = it2.next();
                            Log.d("CloudApkInfoList", "save security class" + classe.getSecurityId() + classe.getText());
                            apiInfoDb.insterSClass(classe);
                        }
                    }
                    apiInfoDb.close_virus_DB();
                } catch (Throwable th2) {
                    th = th2;
                    apiInfoDb = cloudApiInfoDb;
                    apiInfoDb.close_virus_DB();
                    throw th;
                }
            } catch (Throwable th3) {
                e = th3;
            }
        }
    }

    public String cert_rsa_path() {
        return this.mContext.getFilesDir() + "/xml/META-INF/CERT.RSA";
    }

    public byte[] getSignFile(String apkPath, String filterName, String dPath) {
        CommonMethod.logDebug(TAG, "getSignFile 1");
        byte[] resultArray = DataUtils.Zip.extractFromZipFileMatchExtension(apkPath, dPath, filterName);
        CommonMethod.logDebug(TAG, "getSignFile 2");
        if (resultArray == null && filterName == ".RSA") {
            return DataUtils.Zip.extractFromZipFileMatchExtension(apkPath, dPath, ".DSA");
        }
        return resultArray;
    }
}
