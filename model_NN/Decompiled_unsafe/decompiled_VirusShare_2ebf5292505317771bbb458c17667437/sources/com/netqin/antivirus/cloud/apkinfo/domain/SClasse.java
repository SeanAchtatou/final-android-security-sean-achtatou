package com.netqin.antivirus.cloud.apkinfo.domain;

public class SClasse {
    private String id;
    private String securityId;
    private String text;

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getSecurityId() {
        return this.securityId;
    }

    public void setSecurityId(String securityId2) {
        this.securityId = securityId2;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text2) {
        this.text = text2;
    }
}
