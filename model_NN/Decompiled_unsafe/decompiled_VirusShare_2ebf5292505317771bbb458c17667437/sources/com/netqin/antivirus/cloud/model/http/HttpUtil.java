package com.netqin.antivirus.cloud.model.http;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.netqin.antivirus.cloud.model.DataUtils;
import com.netqin.antivirus.common.CloudPassage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;

public class HttpUtil {
    public static final int CLOUDPASSAGE = 4;
    public static final int CLOUDPASSAGE_FAIL = 8;
    public static final int CLOUD_SCAN = 0;
    public static final int CLOUD_SCAN_FAIL = 9;
    public static final int CONNECT_FAIL = 10;
    private static final boolean DBG = false;
    public static final int ISUPLODERSUCCEED = 9;
    public static final int MORE_SCORE_REVIEW = 3;
    public static final int MORE_SCORE_REVIEW_FAIL = 7;
    public static final int REPORT_SUSPICIOUS = 1;
    public static final int REPORT_SUSPICIOUS_FAIL1 = 4;
    public static final int REPORT_SUSPICIOUS_FAIL2 = 5;
    public static final int SCORE_REVIEW = 2;
    public static final int SCORE_REVIEW_FAIL = 6;
    public static final boolean STATISTIC_SWITCH = false;
    private static final String TAG = "HttpUtil";
    private static HttpUtil _this = null;
    private static CloudPassage cloudPassage;
    private static Context mContext;
    private static Long wait_reply_start;
    private boolean cancel = false;

    private HttpUtil() {
    }

    public static HttpUtil getInstance() {
        if (_this == null) {
            _this = new HttpUtil();
        }
        return _this;
    }

    public boolean isCancel() {
        return this.cancel;
    }

    public void setCancel(boolean cancel2) {
        this.cancel = cancel2;
    }

    /* JADX INFO: Multiple debug info for r8v13 java.lang.Long: [D('convey_post_end' java.lang.Long), D('request' com.netqin.antivirus.cloud.model.http.Request)] */
    /* JADX WARN: Type inference failed for: r2v3, types: [java.net.URLConnection] */
    /* JADX WARN: Type inference failed for: r2v6, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x007b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.net.HttpURLConnection doPost(com.netqin.antivirus.cloud.model.http.Request r8, android.content.Context r9) throws java.io.IOException, java.lang.Exception {
        /*
            com.netqin.antivirus.cloud.model.http.HttpUtil.mContext = r9
            r9 = 0
            long r1 = java.lang.System.currentTimeMillis()
            java.lang.Long r1 = java.lang.Long.valueOf(r1)
            java.net.URL r2 = new java.net.URL
            java.lang.String r3 = r8.getUrl()
            r2.<init>(r3)
            boolean r3 = r8.isProxy()     // Catch:{ Exception -> 0x011a }
            if (r3 == 0) goto L_0x00b1
            java.net.Proxy r3 = r8.getProxy()     // Catch:{ Exception -> 0x011a }
            java.net.URLConnection r2 = r2.openConnection(r3)     // Catch:{ Exception -> 0x011a }
            r0 = r2
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x011a }
            r9 = r0
            r2 = r9
        L_0x0027:
            boolean r9 = r8.isStream()     // Catch:{ Exception -> 0x00c1 }
            if (r9 != 0) goto L_0x00bc
            setHeader(r2)     // Catch:{ Exception -> 0x00c1 }
        L_0x0030:
            r9 = 15000(0x3a98, float:2.102E-41)
            r2.setConnectTimeout(r9)     // Catch:{ Exception -> 0x00c1 }
            r9 = 30000(0x7530, float:4.2039E-41)
            r2.setReadTimeout(r9)     // Catch:{ Exception -> 0x00c1 }
            r2.connect()     // Catch:{ Exception -> 0x00c1 }
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x00c1 }
            java.lang.Long r9 = java.lang.Long.valueOf(r3)     // Catch:{ Exception -> 0x00c1 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00c1 }
            long r4 = r9.longValue()     // Catch:{ Exception -> 0x00c1 }
            long r6 = r1.longValue()     // Catch:{ Exception -> 0x00c1 }
            long r4 = r4 - r6
            java.lang.String r9 = java.lang.String.valueOf(r4)     // Catch:{ Exception -> 0x00c1 }
            r3.<init>(r9)     // Catch:{ Exception -> 0x00c1 }
            java.lang.String r9 = r3.toString()     // Catch:{ Exception -> 0x00c1 }
            com.netqin.antivirus.common.CloudPassage r1 = new com.netqin.antivirus.common.CloudPassage     // Catch:{ Exception -> 0x00c1 }
            android.content.Context r3 = com.netqin.antivirus.cloud.model.http.HttpUtil.mContext     // Catch:{ Exception -> 0x00c1 }
            r1.<init>(r3)     // Catch:{ Exception -> 0x00c1 }
            com.netqin.antivirus.cloud.model.http.HttpUtil.cloudPassage = r1     // Catch:{ Exception -> 0x00c1 }
            com.netqin.antivirus.common.CloudPassage r1 = com.netqin.antivirus.cloud.model.http.HttpUtil.cloudPassage     // Catch:{ Exception -> 0x00c1 }
            r1.setChainEstablishConnUsetime(r9)     // Catch:{ Exception -> 0x00c1 }
            byte[] r9 = r8.getSendDatas()     // Catch:{ Exception -> 0x00c1 }
            if (r9 == 0) goto L_0x011f
            int r9 = r8.getSchema()     // Catch:{ Exception -> 0x00c1 }
            switch(r9) {
                case 0: goto L_0x00cd;
                case 1: goto L_0x0100;
                case 2: goto L_0x0106;
                case 3: goto L_0x010c;
                case 4: goto L_0x0112;
                default: goto L_0x0076;
            }
        L_0x0076:
            r8 = r2
        L_0x0077:
            java.lang.Long r9 = com.netqin.antivirus.cloud.model.http.HttpUtil.wait_reply_start
            if (r9 == 0) goto L_0x00a0
            long r1 = java.lang.System.currentTimeMillis()
            java.lang.Long r9 = java.lang.Long.valueOf(r1)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            long r2 = r9.longValue()
            java.lang.Long r9 = com.netqin.antivirus.cloud.model.http.HttpUtil.wait_reply_start
            long r4 = r9.longValue()
            long r2 = r2 - r4
            java.lang.String r9 = java.lang.String.valueOf(r2)
            r1.<init>(r9)
            java.lang.String r9 = r1.toString()
            com.netqin.antivirus.common.CloudPassage r1 = com.netqin.antivirus.cloud.model.http.HttpUtil.cloudPassage
            r1.setWaitReplyUsetime(r9)
        L_0x00a0:
            int r9 = r8.getResponseCode()
            r1 = 200(0xc8, float:2.8E-43)
            if (r9 == r1) goto L_0x00b0
            int r9 = r8.getResponseCode()
            r1 = 302(0x12e, float:4.23E-43)
            if (r9 != r1) goto L_0x0118
        L_0x00b0:
            return r8
        L_0x00b1:
            java.net.URLConnection r2 = r2.openConnection()     // Catch:{ Exception -> 0x011a }
            r0 = r2
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x011a }
            r9 = r0
            r2 = r9
            goto L_0x0027
        L_0x00bc:
            setHeaderContentTypeStream(r2)     // Catch:{ Exception -> 0x00c1 }
            goto L_0x0030
        L_0x00c1:
            r8 = move-exception
            r9 = r2
        L_0x00c3:
            r8.printStackTrace()
            if (r9 == 0) goto L_0x011c
            r9.disconnect()
            r8 = 0
            goto L_0x0077
        L_0x00cd:
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x00c1 }
            java.lang.Long r9 = java.lang.Long.valueOf(r3)     // Catch:{ Exception -> 0x00c1 }
            sendToServerForCloud(r2, r8)     // Catch:{ Exception -> 0x00c1 }
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x00c1 }
            java.lang.Long r8 = java.lang.Long.valueOf(r3)     // Catch:{ Exception -> 0x00c1 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00c1 }
            long r3 = r8.longValue()     // Catch:{ Exception -> 0x00c1 }
            long r5 = r9.longValue()     // Catch:{ Exception -> 0x00c1 }
            long r3 = r3 - r5
            java.lang.String r9 = java.lang.String.valueOf(r3)     // Catch:{ Exception -> 0x00c1 }
            r1.<init>(r9)     // Catch:{ Exception -> 0x00c1 }
            java.lang.String r9 = r1.toString()     // Catch:{ Exception -> 0x00c1 }
            com.netqin.antivirus.common.CloudPassage r1 = com.netqin.antivirus.cloud.model.http.HttpUtil.cloudPassage     // Catch:{ Exception -> 0x00c1 }
            r1.setConveyPostUsetime(r9)     // Catch:{ Exception -> 0x00c1 }
            com.netqin.antivirus.cloud.model.http.HttpUtil.wait_reply_start = r8     // Catch:{ Exception -> 0x00c1 }
            r8 = r2
            goto L_0x0077
        L_0x0100:
            sendToServerForReportSuspicious(r2, r8)     // Catch:{ Exception -> 0x00c1 }
            r8 = r2
            goto L_0x0077
        L_0x0106:
            sendToServerForScoreReview(r2, r8)     // Catch:{ Exception -> 0x00c1 }
            r8 = r2
            goto L_0x0077
        L_0x010c:
            sendToServerForMoreScoreReview(r2, r8)     // Catch:{ Exception -> 0x00c1 }
            r8 = r2
            goto L_0x0077
        L_0x0112:
            sendToServerForCloudPassage(r2, r8)     // Catch:{ Exception -> 0x00c1 }
            r8 = r2
            goto L_0x0077
        L_0x0118:
            r8 = 0
            goto L_0x00b0
        L_0x011a:
            r8 = move-exception
            goto L_0x00c3
        L_0x011c:
            r8 = r9
            goto L_0x0077
        L_0x011f:
            r8 = r2
            goto L_0x0077
        */
        throw new UnsupportedOperationException("Method not decompiled: com.netqin.antivirus.cloud.model.http.HttpUtil.doPost(com.netqin.antivirus.cloud.model.http.Request, android.content.Context):java.net.HttpURLConnection");
    }

    /* JADX WARN: Type inference failed for: r4v8, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.net.HttpURLConnection doGet(com.netqin.antivirus.cloud.model.http.Request r5) throws java.io.IOException {
        /*
            r2 = 0
            java.net.URL r3 = new java.net.URL
            java.lang.String r4 = r5.getUrl()
            r3.<init>(r4)
            boolean r4 = r5.isProxy()     // Catch:{ Exception -> 0x003e }
            if (r4 == 0) goto L_0x0035
            java.net.Proxy r4 = r5.getProxy()     // Catch:{ Exception -> 0x003e }
            java.net.URLConnection r4 = r3.openConnection(r4)     // Catch:{ Exception -> 0x003e }
            r0 = r4
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x003e }
            r2 = r0
        L_0x001c:
            setHeaderGet(r2)     // Catch:{ Exception -> 0x003e }
            r4 = 15000(0x3a98, float:2.102E-41)
            r2.setConnectTimeout(r4)     // Catch:{ Exception -> 0x003e }
            r2.connect()     // Catch:{ Exception -> 0x003e }
            byte[] r4 = r5.getSendDatas()     // Catch:{ Exception -> 0x003e }
            if (r4 == 0) goto L_0x0034
            byte[] r4 = r5.getSendDatas()     // Catch:{ Exception -> 0x003e }
            sendToServer(r2, r4)     // Catch:{ Exception -> 0x003e }
        L_0x0034:
            return r2
        L_0x0035:
            java.net.URLConnection r4 = r3.openConnection()     // Catch:{ Exception -> 0x003e }
            r0 = r4
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x003e }
            r2 = r0
            goto L_0x001c
        L_0x003e:
            r4 = move-exception
            r1 = r4
            r1.printStackTrace()
            if (r2 == 0) goto L_0x0034
            r2.disconnect()
            r2 = 0
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: com.netqin.antivirus.cloud.model.http.HttpUtil.doGet(com.netqin.antivirus.cloud.model.http.Request):java.net.HttpURLConnection");
    }

    public HttpURLConnection getConnectionWithPost(Request request, Context context) {
        boolean z;
        boolean z2;
        HttpURLConnection hc = null;
        int reNum = 0;
        while (true) {
            if (reNum < 1) {
                z = true;
            } else {
                z = false;
            }
            if (this.cancel) {
                z2 = false;
            } else {
                z2 = true;
            }
            if (!z || !z2) {
                break;
            }
            try {
                hc = doPost(request, context);
                if (hc != null) {
                    break;
                }
                reNum++;
            } catch (IOException e) {
                reNum++;
            } catch (Exception e2) {
                reNum++;
            }
        }
        return hc;
    }

    public HttpURLConnection getConnectionWithGet(Request request) {
        boolean z;
        boolean z2;
        HttpURLConnection hc = null;
        int reNum = 0;
        while (true) {
            if (reNum < 3) {
                z = true;
            } else {
                z = false;
            }
            if (this.cancel) {
                z2 = false;
            } else {
                z2 = true;
            }
            if (!z || !z2) {
                break;
            }
            try {
                hc = doGet(request);
                if (hc != null) {
                    break;
                }
                reNum++;
            } catch (IOException e) {
                reNum++;
            } catch (Exception e2) {
                reNum++;
            }
        }
        return hc;
    }

    public static byte[] doResponse(HttpURLConnection hc) throws IOException {
        if (hc == null) {
            return null;
        }
        InputStream input = hc.getInputStream();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        for (int inByte = input.read(); inByte != -1; inByte = input.read()) {
            bos.write(inByte);
        }
        byte[] buffer = bos.toByteArray();
        bos.close();
        release(input, hc);
        return buffer;
    }

    public static void release(InputStream input, HttpURLConnection hc) throws IOException {
        if (hc != null) {
            hc.disconnect();
        }
        if (input != null) {
            input.close();
        }
    }

    public static void sendToServer(HttpURLConnection hc, byte[] bytes) {
        OutputStream out = null;
        try {
            out = hc.getOutputStream();
            out.write(bytes);
            out.flush();
            out.close();
        } catch (IOException e) {
            IOException e2 = e;
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            e2.printStackTrace();
        }
    }

    public static void sendToServerForCloud(HttpURLConnection hc, Request aRequest) throws Exception {
        OutputStream out = null;
        try {
            out = hc.getOutputStream();
            byte[] header = new byte[16];
            header[0] = 5;
            header[4] = 1;
            header[8] = 1;
            header[12] = 11;
            out.write(header);
            out.flush();
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bos.write(aRequest.getSendDatas());
            out.write(DataUtils.ecryptCompress(bos.toByteArray()));
            out.flush();
            out.close();
        } catch (IOException e) {
            IOException e2 = e;
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            e2.printStackTrace();
        }
    }

    public static void sendToServerForReportSuspicious(HttpURLConnection hc, Request aRequest) throws Exception {
        OutputStream out = null;
        try {
            out = hc.getOutputStream();
            byte[] header = new byte[16];
            header[0] = 5;
            header[4] = 1;
            header[8] = 1;
            header[12] = 15;
            out.write(header);
            out.flush();
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bos.write(aRequest.getSendDatas());
            out.write(DataUtils.ecryptCompress(bos.toByteArray()));
            out.flush();
            out.close();
        } catch (IOException e) {
            IOException e2 = e;
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            e2.printStackTrace();
        }
    }

    public static void sendToServerForScoreReview(HttpURLConnection hc, Request aRequest) throws Exception {
        OutputStream out = null;
        try {
            out = hc.getOutputStream();
            byte[] header = new byte[16];
            header[0] = 5;
            header[4] = 1;
            header[8] = 1;
            header[12] = 12;
            out.write(header);
            out.flush();
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bos.write(aRequest.getSendDatas());
            out.write(DataUtils.ecryptCompress(bos.toByteArray()));
            out.flush();
            out.close();
        } catch (IOException e) {
            IOException e2 = e;
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            e2.printStackTrace();
        }
    }

    public static void sendToServerForMoreScoreReview(HttpURLConnection hc, Request aRequest) throws Exception {
        OutputStream out = null;
        try {
            out = hc.getOutputStream();
            byte[] header = new byte[16];
            header[0] = 5;
            header[4] = 1;
            header[8] = 1;
            header[12] = 14;
            out.write(header);
            out.flush();
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bos.write(aRequest.getSendDatas());
            out.write(DataUtils.ecryptCompress(bos.toByteArray()));
            out.flush();
            out.close();
        } catch (IOException e) {
            IOException e2 = e;
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            e2.printStackTrace();
        }
    }

    public static void sendToServerForCloudPassage(HttpURLConnection hc, Request aRequest) throws Exception {
        OutputStream out = null;
        try {
            out = hc.getOutputStream();
            byte[] header = new byte[16];
            header[0] = 5;
            header[4] = 1;
            header[8] = 1;
            header[12] = 13;
            out.write(header);
            out.flush();
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bos.write(aRequest.getSendDatas());
            out.write(DataUtils.ecryptCompress(bos.toByteArray()));
            out.flush();
            out.close();
        } catch (IOException e) {
            IOException e2 = e;
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            e2.printStackTrace();
        }
    }

    private static void setHeader(HttpURLConnection hc) throws IOException {
        hc.setUseCaches(false);
        hc.setDoOutput(true);
        hc.setDoInput(true);
        hc.setRequestMethod("POST");
    }

    private static void setHeaderContentTypeStream(HttpURLConnection hc) throws IOException {
        hc.setRequestProperty("Content-Type", "application/octet-stream;charset=utf-8");
        hc.setUseCaches(false);
        hc.setDoOutput(true);
        hc.setDoInput(true);
        hc.setRequestMethod("POST");
    }

    private static void setHeaderGet(HttpURLConnection hc) throws IOException {
        hc.setRequestProperty("Content-Type", "text/xml;charset=utf-8");
        hc.setRequestProperty("Accept", "text/javascript, text/ecmascript, application/x-javascript, */*, text/x-vcard, text/x-vcalendar, image/gif, image/vnd.wap.wbmp,textnd.wap.wml,applicationnd.wap.xhtml+xml,textml,text/css,text/vnd.wap.wml,application/vnd.wap.xhtml+xml,text/html,text/css");
        hc.setRequestProperty("Connection", "Keep-Alive");
        hc.setUseCaches(false);
        hc.setDoOutput(true);
        hc.setDoInput(true);
        hc.setRequestMethod("GET");
    }

    public static boolean isWifi(Context aContext) {
        NetworkInfo network_info = ((ConnectivityManager) aContext.getSystemService("connectivity")).getActiveNetworkInfo();
        if (network_info == null || !network_info.isConnected()) {
            return false;
        }
        if (network_info.getType() == 1) {
            return true;
        }
        return false;
    }

    public static String getAccessPoint(Context context) {
        NetworkInfo info = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (info == null) {
            return "unknown";
        }
        if (1 == info.getType()) {
            String apn = info.getTypeName();
            if (apn == null) {
                return "wifi";
            }
            return apn;
        } else if (info.getType() != 0) {
            return "unknown";
        } else {
            String apn2 = info.getExtraInfo().toLowerCase();
            if (apn2 == null) {
                return "mobile";
            }
            return apn2;
        }
    }
}
