package com.netqin.antivirus.cloud.view;

import android.os.Bundle;

public interface CloudListener {
    public static final int CLOUD_SCAN_FAIL = 1001;
    public static final int CLOUD_SCAN_SUCCEES = 1000;

    void complete(int i);

    void error(int i);

    void process(Bundle bundle);
}
