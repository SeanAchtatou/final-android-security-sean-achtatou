package com.netqin.antivirus.cloud.model.xml;

public class CellIDData {
    private String cellid;
    private String lac;
    private String mcc;
    private String mnc;

    public String getCellid() {
        return this.cellid;
    }

    public void setCellid(String cellid2) {
        this.cellid = cellid2;
    }

    public String getLac() {
        return this.lac;
    }

    public void setLac(String lac2) {
        this.lac = lac2;
    }

    public String getMnc() {
        return this.mnc;
    }

    public void setMnc(String mnc2) {
        this.mnc = mnc2;
    }

    public String getMcc() {
        return this.mcc;
    }

    public void setMcc(String mcc2) {
        this.mcc = mcc2;
    }
}
