package com.netqin.antivirus.cloud.model.http;

import java.net.Proxy;

public class Request {
    private boolean isProxy = false;
    private boolean isStream;
    private boolean isWifi;
    private int method;
    private Proxy proxy = null;
    private int schema;
    private byte[] sendDatas;
    private String url = null;

    public boolean isProxy() {
        if (this.proxy != null) {
            this.isProxy = true;
        }
        return this.isProxy;
    }

    public void setProxy(boolean isProxy2) {
        this.isProxy = isProxy2;
    }

    public Proxy getProxy() {
        return this.proxy;
    }

    public void setProxy(Proxy proxy2) {
        this.proxy = proxy2;
    }

    public boolean isStream() {
        return this.isStream;
    }

    public void setStream(boolean isStream2) {
        this.isStream = isStream2;
    }

    public boolean isWifi() {
        return this.isWifi;
    }

    public void setWifi(boolean isWifi2) {
        this.isWifi = isWifi2;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public byte[] getSendDatas() {
        return this.sendDatas;
    }

    public void setSendDatas(byte[] sendDatas2) {
        this.sendDatas = sendDatas2;
    }

    public int getMethod() {
        return this.method;
    }

    public void setMethod(int method2) {
        this.method = method2;
    }

    public int getSchema() {
        return this.schema;
    }

    public void setSchema(int schema2) {
        this.schema = schema2;
    }
}
