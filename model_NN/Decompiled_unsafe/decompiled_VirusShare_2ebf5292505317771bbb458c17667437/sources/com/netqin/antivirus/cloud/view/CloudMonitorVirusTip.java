package com.netqin.antivirus.cloud.view;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import com.netqin.antivirus.HomeActivity;
import com.netqin.antivirus.appprotocol.AppConstantValue;
import com.netqin.antivirus.appprotocol.AppRequest;
import com.netqin.antivirus.cloud.model.CloudApkInfo;
import com.netqin.antivirus.common.CommonDefine;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.contact.vcard.VCardConfig;
import com.netqin.antivirus.scan.SilentCloudScanService;
import com.nqmobile.antivirus_ampro20.R;
import java.util.ArrayList;
import java.util.Iterator;

public class CloudMonitorVirusTip extends Activity {
    public static ArrayList<CloudApkInfo> mCloudApkInfoList;
    public static int mVirusNum;
    boolean isDeletingPackage;
    boolean isPaused;
    ArrayList<String> packageNames = null;
    String programName = "";
    StringBuffer programNames;
    int virusCount;
    String virusName = "";
    String virusPath = "";

    public void onCreate(Bundle savedInstanceState) {
        String str;
        this.programNames = new StringBuffer();
        mCloudApkInfoList = SilentCloudScanService.mScanController.mCloudApkInfoList;
        mVirusNum = SilentCloudScanService.mVirusNum;
        SilentCloudScanService.mScanController.destroy();
        SilentCloudScanService.mScanController = null;
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        this.packageNames = new ArrayList<>(2);
        Intent intent = getIntent();
        ArrayList<CloudApkInfo> cloudApkInfoList = mCloudApkInfoList;
        PackageManager packageManager = getPackageManager();
        int i = 0;
        while (true) {
            if (i >= cloudApkInfoList.size()) {
                break;
            }
            CloudApkInfo cai = cloudApkInfoList.get(i);
            String packageName = cai.getPkgName();
            if ((cai.getVirusName() != null && cai.getVirusName().length() > 0) || "10".equalsIgnoreCase(cai.getSecurity())) {
                if (mVirusNum == 1) {
                    this.packageNames.add(packageName);
                    this.virusPath = cai.getInstallPath();
                    this.virusName = cai.getVirusName();
                    this.programName = cai.getName();
                    break;
                }
                this.packageNames.add(packageName);
                if (this.virusName == null) {
                    this.virusName = cai.getVirusName();
                } else {
                    this.programNames.append(String.valueOf(i + 1) + "." + cai.getName() + "\n");
                }
            }
            i++;
        }
        AnonymousClass1 r0 = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                CloudMonitorVirusTip.this.deletePackage();
                if (CommonMethod.isMemberOfNetQin(CloudMonitorVirusTip.this)) {
                    AppRequest.StartSubscribe(CloudMonitorVirusTip.this, true, AppConstantValue.subscribeFromScanBackground);
                }
                CloudMonitorVirusTip.this.finish();
            }
        };
        AnonymousClass2 r02 = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                CloudMonitorVirusTip.this.finish();
            }
        };
        if (mVirusNum == 1) {
            str = getResources().getString(R.string.text_cloud_monitor_delete_tip, this.programName, this.virusPath);
        } else {
            str = getResources().getString(R.string.text_cloud_monitor_delete_multiple_tip, Integer.valueOf(mVirusNum), this.programNames.substring(0, this.programNames.length() - 1));
        }
        CommonMethod.delVirusDialogListen(this, r0, r02, str, R.string.label_netqin_antivirus);
        SilentCloudScanService.newinstallapkpathArray.clear();
    }

    /* access modifiers changed from: package-private */
    public void finishA() {
        finish();
    }

    public void onResume() {
        super.onResume();
    }

    public void onPause() {
        super.onPause();
        this.isPaused = true;
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void deletePackage() {
        new Thread(new Runnable() {
            public void run() {
                Iterator<String> it = CloudMonitorVirusTip.this.packageNames.iterator();
                while (it.hasNext()) {
                    boolean unused = CloudMonitorVirusTip.this.clearHarmData(it.next(), CloudMonitorVirusTip.this.getBaseContext());
                }
            }
        }).start();
        this.isDeletingPackage = true;
    }

    /* access modifiers changed from: private */
    public boolean clearHarmData(String packageName, Context context) {
        uninstallPackage(packageName, context);
        return true;
    }

    private void uninstallPackage(String packageName, Context context) {
        try {
            Intent it = new Intent("android.intent.action.DELETE");
            it.setData(Uri.fromParts("package", packageName, null));
            it.setFlags(VCardConfig.FLAG_USE_QP_TO_PRIMARY_PROPERTIES);
            context.startActivity(it);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showNotification(String str) {
        NotificationManager notificationManager = (NotificationManager) getSystemService("notification");
        Notification notification = new Notification(R.drawable.icon, str, System.currentTimeMillis());
        notification.setLatestEventInfo(this, CommonDefine.NOTIFICATION_MAIN, str, PendingIntent.getActivity(this, 0, new Intent(this, HomeActivity.class), 0));
        notificationManager.notify(CommonDefine.NOTIFICATION_ID, notification);
        notificationManager.cancel(CommonDefine.NOTIFICATION_ID);
    }
}
