package com.netqin.antivirus.cloud.virus.db;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import com.netqin.antivirus.cloud.model.CloudApkInfo;
import com.netqin.antivirus.cloud.model.CloudHandler;
import java.util.ArrayList;
import java.util.List;

public class CloudVirusdb {
    private byte[] blobByte;
    private DBOpenHelper dbOpenHelper = null;
    private SQLiteDatabase sqLiteDatabase = null;
    private String tempStr;

    public CloudVirusdb(Context context) {
        this.dbOpenHelper = new DBOpenHelper(context);
        this.sqLiteDatabase = this.dbOpenHelper.getReadableDatabase();
    }

    public void inster(CloudApkInfo info) {
        SQLiteDatabase sQLiteDatabase = this.sqLiteDatabase;
        Object[] objArr = new Object[6];
        objArr[0] = info.getPkgName();
        objArr[1] = info.getInstallPath();
        objArr[3] = info.getCertRSA();
        objArr[4] = info.getName();
        objArr[5] = "";
        sQLiteDatabase.execSQL("insert into virus (apkname,installpath,sf,rsa,name,desc) values (?,?,?,?,?,?)", objArr);
    }

    public List<CloudApkInfo> getAllVirusData() {
        Cursor cusor = this.sqLiteDatabase.rawQuery("select * from virus", null);
        List<CloudApkInfo> infoList = new ArrayList<>();
        while (cusor.moveToNext()) {
            CloudApkInfo info = new CloudApkInfo();
            info.setId(new StringBuilder(String.valueOf(cusor.getInt(cusor.getColumnIndex("virusid")))).toString());
            info.setPkgName(cusor.getString(cusor.getColumnIndex("apkname")));
            info.setInstallPath(cusor.getString(cusor.getColumnIndex("installpath")));
            info.setName(cusor.getString(cusor.getColumnIndex("name")));
            info.setCertRSA(cusor.getBlob(4));
            infoList.add(info);
        }
        cusor.close();
        return infoList;
    }

    public boolean isRepeat(String packageName, byte[] rsaB, byte[] sfB) {
        boolean res = true;
        if (sfB == null) {
        }
        if (rsaB == null) {
        }
        Cursor cursor = this.sqLiteDatabase.rawQuery("select virusid as _id,installpath,sf,rsa from virus where apkname=?", new String[]{packageName});
        try {
            if (cursor.moveToFirst()) {
                String columnName = cursor.getColumnName(3);
                String columnName2 = cursor.getColumnName(2);
                byte[] tempRSA = cursor.getBlob(3);
                byte[] tempSF = cursor.getBlob(2);
                if (tempRSA.length == rsaB.length && tempSF.length == sfB.length) {
                    for (int i = 0; i < tempRSA.length - 1; i++) {
                        if (tempRSA[i] != rsaB[i]) {
                            res = true;
                        }
                    }
                    for (int i2 = 0; i2 < tempSF.length - 1; i2++) {
                        if (tempSF[i2] != sfB[i2]) {
                            res = true;
                        }
                    }
                } else {
                    res = false;
                }
            } else {
                res = false;
            }
        } catch (Exception e) {
            res = false;
            e.printStackTrace();
        } finally {
            cursor.close();
        }
        cursor.close();
        return res;
    }

    public void close_virus_DB() {
        if (this.sqLiteDatabase != null) {
            if (this.sqLiteDatabase.isOpen()) {
                this.sqLiteDatabase.close();
            }
            this.sqLiteDatabase = null;
        }
    }

    public void dropTable() {
        try {
            this.sqLiteDatabase.execSQL("drop table virus");
        } catch (SQLException e) {
        }
    }

    public boolean isInDB(String packageName) {
        boolean res = false;
        Cursor cursor = this.sqLiteDatabase.rawQuery("select virusid as _id,installpath,sf,rsa from virus where apkname=?", new String[]{packageName});
        try {
            if (cursor.moveToFirst()) {
                String string = cursor.getString(cursor.getColumnIndex("installpath"));
                res = true;
            }
        } catch (Exception e) {
        }
        cursor.close();
        return res;
    }

    public boolean isInDB(String packageName, byte[] rsa) {
        boolean res = false;
        if (isInDB(packageName)) {
            byte[] tempRSA = getRSA(packageName);
            byte[] sf = getSF(packageName);
            if (tempRSA.length == rsa.length) {
                for (int i = 0; i < tempRSA.length - 1; i++) {
                    if (tempRSA[i] != rsa[i]) {
                        return false;
                    }
                }
                res = true;
            }
        }
        return res;
    }

    public byte[] getRSA(String packageName) {
        Cursor cursor = this.sqLiteDatabase.rawQuery("select virusid as _id,name,installpath,sf,rsa from virus where apkname=?", new String[]{packageName});
        if (cursor.moveToFirst()) {
            this.blobByte = cursor.getBlob(4);
            cursor.close();
            return this.blobByte;
        }
        cursor.close();
        return null;
    }

    public byte[] getSF(String packageName) {
        Cursor cursor = this.sqLiteDatabase.rawQuery("select virusid as _id,name,installpath,sf,rsa from virus where apkname=?", new String[]{packageName});
        if (cursor.moveToFirst()) {
            this.blobByte = cursor.getBlob(3);
            cursor.close();
            return this.blobByte;
        }
        cursor.close();
        return null;
    }

    public String getName(String packageName) {
        Cursor cursor = this.sqLiteDatabase.rawQuery("select virusid as _id,installpath,sf,rsa,name,desc from virus where apkname=?", new String[]{packageName});
        if (cursor.moveToFirst()) {
            this.tempStr = cursor.getString(cursor.getColumnIndex("name"));
            cursor.close();
            return this.tempStr;
        }
        cursor.close();
        return null;
    }

    public String getDesc(String packageName) {
        Cursor cursor = this.sqLiteDatabase.rawQuery("select virusid as _id,installpath,sf,rsa,name,desc from virus where apkname=?", new String[]{packageName});
        if (cursor.moveToFirst()) {
            this.tempStr = cursor.getString(cursor.getColumnIndex(CloudHandler.KEY_ITEM_DESC));
            cursor.close();
            return this.tempStr;
        }
        cursor.close();
        return null;
    }
}
