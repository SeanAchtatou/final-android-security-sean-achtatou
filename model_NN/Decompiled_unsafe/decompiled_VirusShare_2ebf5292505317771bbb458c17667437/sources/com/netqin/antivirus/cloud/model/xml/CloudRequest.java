package com.netqin.antivirus.cloud.model.xml;

import SHcMjZX.DD02zqNU;
import android.content.ContentValues;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Xml;
import com.netqin.antivirus.Preferences;
import com.netqin.antivirus.Value;
import com.netqin.antivirus.cloud.apkinfo.domain.Rate;
import com.netqin.antivirus.cloud.model.CloudApkInfo;
import com.netqin.antivirus.cloud.model.CloudHandler;
import com.netqin.antivirus.common.CloudPassage;
import com.netqin.antivirus.common.CommonDefine;
import com.netqin.antivirus.common.CommonMethod;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import org.xmlpull.v1.XmlSerializer;

public class CloudRequest {
    private static final String TAG = "CloudRequest";
    private final boolean DBG = false;
    CellIdInfo cellIdInfo = new CellIdInfo();
    int certIndex = 0;
    ArrayList<CloudApkInfo> cloudApkInfos;
    ContentValues content;
    Context context;
    private int uploadType = 1;

    public CloudRequest(ContentValues content2, ArrayList<CloudApkInfo> cloudApkInfos2, Context context2) {
        this.content = content2;
        this.cloudApkInfos = cloudApkInfos2;
        this.context = context2;
    }

    public CloudRequest(ContentValues content2, Context context2) {
        this.content = content2;
        this.context = context2;
    }

    public String getRequestXmlStr() throws Exception, IllegalStateException, IOException {
        XmlSerializer serializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();
        serializer.setOutput(writer);
        serializer.startDocument(StringEncodings.UTF8, true);
        serializer.startTag("", XmlUtils.LABEL_REQUEST);
        XmlSerializer serializer2 = getPkgsInfo(getUploadInfo(getServerInfo(getClientInfo(getMobileInfo(serializer)))));
        serializer2.startTag("", XmlUtils.LABEL_FILE);
        serializer2.attribute("", "name", "testjson");
        int i = this.certIndex;
        this.certIndex = i + 1;
        serializer2.attribute("", XmlUtils.LABEL_REF, Integer.toString(i));
        serializer2.endTag("", XmlUtils.LABEL_FILE);
        serializer2.endTag("", XmlUtils.LABEL_REQUEST);
        serializer2.endDocument();
        System.gc();
        return writer.toString();
    }

    private XmlSerializer getServerInfo(XmlSerializer serializer) throws IllegalArgumentException, IllegalStateException, IOException {
        serializer.startTag("", "ServiceInfo");
        serializer.attribute("", XmlUtils.LABEL_SERVICEINFO_BUSINESS, "101");
        serializer.endTag("", "ServiceInfo");
        return serializer;
    }

    private XmlSerializer getUploadInfo(XmlSerializer serializer) throws IllegalArgumentException, IllegalStateException, IOException {
        serializer.startTag("", "Upload");
        serializer.attribute("", "background", Integer.toString(Value.backgroundType));
        serializer.endTag("", "Upload");
        return serializer;
    }

    public XmlSerializer getPkgsInfo(XmlSerializer serializer) throws IllegalArgumentException, IllegalStateException, IOException {
        serializer.startTag("", XmlUtils.LABEL_PKGS);
        setCertIndex(0);
        Iterator<CloudApkInfo> it = this.cloudApkInfos.iterator();
        while (it.hasNext()) {
            serializer = getApkInfos(serializer, it.next());
        }
        serializer.endTag("", XmlUtils.LABEL_PKGS);
        return serializer;
    }

    private void setCertIndex(int index) {
        this.certIndex = index;
    }

    private XmlSerializer getApkInfos(XmlSerializer serializer, CloudApkInfo apkInfo) throws IllegalArgumentException, IllegalStateException, IOException {
        serializer.startTag("", XmlUtils.LABEL_APK);
        serializer.attribute("", "id", new StringBuilder(String.valueOf(apkInfo.getId())).toString());
        serializer.attribute("", "pkgName", apkInfo.getPkgName());
        apkInfo.setVersionCode(CommonMethod.getVersionCode(apkInfo.getPkgName(), this.context));
        apkInfo.setVersionName(CommonMethod.getVersionName(apkInfo.getPkgName(), this.context));
        serializer.attribute("", XmlUtils.LABEL_VERSIONCODE, apkInfo.getVersionCode());
        serializer.attribute("", XmlUtils.LANBL_VERSIONNAME, apkInfo.getVersionName());
        serializer.attribute("", "name", apkInfo.getName());
        serializer.attribute("", XmlUtils.LABEL_APK_INSTALLPATH, apkInfo.getInstallPath());
        serializer.attribute("", XmlUtils.LABEL_APK_INSTALLTIME, apkInfo.getIntallTime());
        serializer.attribute("", XmlUtils.LABEL_APK_SYSTEMAPP, apkInfo.getSystemApp());
        serializer.attribute("", XmlUtils.LABEL_APK_ISAMDOWNLOAD, apkInfo.getIsAmDownLoad());
        serializer.attribute("", XmlUtils.LABEL_APK_LOCALSCANVIRUSNAME, apkInfo.getVirusName());
        if (apkInfo.runingServiceList != null) {
            serializer.startTag("", XmlUtils.LABEL_APK_BACKGROUNDSERVICES);
            StringBuilder builder = new StringBuilder();
            for (String text : apkInfo.runingServiceList) {
                builder.append(String.valueOf(text) + ",");
            }
            if (builder.length() > 0) {
                builder.deleteCharAt(builder.length() - 1);
                serializer.cdsect(builder.toString());
            }
            serializer.endTag("", XmlUtils.LABEL_APK_BACKGROUNDSERVICES);
        }
        if (apkInfo.getCertRSA() != null) {
            serializer.startTag("", XmlUtils.LABEL_FILE);
            serializer.attribute("", "name", "cert.rsa");
            int i = this.certIndex;
            this.certIndex = i + 1;
            serializer.attribute("", XmlUtils.LABEL_REF, Integer.toString(i));
            serializer.endTag("", XmlUtils.LABEL_FILE);
        }
        serializer.endTag("", XmlUtils.LABEL_APK);
        return serializer;
    }

    public XmlSerializer getMobileInfo(XmlSerializer serializer) throws IllegalArgumentException, RuntimeException, Exception {
        serializer.startTag("", XmlUtils.LABEL_MOBILEINFO);
        serializer.attribute("", XmlUtils.LABEL_MOBILEINGO_MODEL, Value.Model);
        serializer.attribute("", "lang", CommonMethod.getPlatformLanguage());
        serializer.attribute("", XmlUtils.LABEL_MOBILEINFO_COUNTRY, Value.Country);
        serializer.attribute("", XmlUtils.LABEL_MOBILEINFO_IMEI, this.content.getAsString("IMEI"));
        serializer.attribute("", XmlUtils.LABEL_MOBILEINFO_IMSI, this.content.getAsString("IMSI"));
        serializer.attribute("", XmlUtils.LABEL_MOBILEINFO_SMSCENTER, "1");
        String apn = getAPN(this.context);
        CellIDData _tmp = this.cellIdInfo.getCellId(this.context);
        if (_tmp != null) {
            serializer.attribute("", XmlUtils.LABEL_MOBILEINFO_LAC, _tmp.getLac());
            serializer.attribute("", XmlUtils.LABEL_MOBILEINFO_CELLID, _tmp.getCellid());
        } else {
            serializer.attribute("", XmlUtils.LABEL_MOBILEINFO_LAC, "1");
            serializer.attribute("", XmlUtils.LABEL_MOBILEINFO_CELLID, "1");
        }
        serializer.attribute("", XmlUtils.LABEL_MOBILEINFO_APN, getAPN(this.context));
        serializer.attribute("", XmlUtils.LABEL_MOBILEINFO_MCNC, CommonMethod.getMcnc(this.context));
        serializer.endTag("", XmlUtils.LABEL_MOBILEINFO);
        return serializer;
    }

    public XmlSerializer getTimeZone(XmlSerializer serializer) throws IllegalArgumentException, RuntimeException, Exception {
        serializer.startTag("", XmlUtils.LABEL_TIMEZONE);
        serializer.text(Value.TIMEZONE);
        serializer.endTag("", XmlUtils.LABEL_TIMEZONE);
        return serializer;
    }

    public static String getAPN(Context context2) {
        String apn;
        NetworkInfo info = ((ConnectivityManager) context2.getSystemService("connectivity")).getActiveNetworkInfo();
        if (info == null || 1 != info.getType()) {
            apn = info.getExtraInfo().toLowerCase();
            if (apn == null) {
                apn = "mobile";
            }
        } else {
            apn = info.getTypeName();
            if (apn == null) {
                apn = "wifi";
            }
        }
        new CloudPassage(context2).setApn(apn);
        return apn;
    }

    public XmlSerializer getClientInfo(XmlSerializer serializer) throws IllegalArgumentException, RuntimeException, IOException {
        serializer.startTag("", XmlUtils.LABEL_CLIENTINFO);
        serializer.attribute("", "lang", CommonMethod.getPlatformLanguage());
        serializer.attribute("", XmlUtils.LABEL_CLIENTINFO_PLATFORMID, "351");
        serializer.attribute("", XmlUtils.LABEL_CLIENTINFO_SUBCOOPID, Preferences.getPreferences(this.context).getChanelIdStore());
        serializer.attribute("", XmlUtils.LABEL_CLIENTINFO_EDITIONID, CommonDefine.ANTIVIRUS_VERID);
        serializer.attribute("", XmlUtils.LABEL_CLIENTINFO_UID, CommonMethod.getUID(this.context));
        serializer.attribute("", XmlUtils.LABEL_CLIENTINFO_CRACKED, Boolean.toString(this.content.getAsBoolean("isRootPower").booleanValue()));
        serializer.attribute("", XmlUtils.LABEL_CLIENTINFO_ALLOWUNKOWNSOURCE, Boolean.toString(this.content.getAsBoolean("isAllowInstallOther").booleanValue()));
        serializer.attribute("", XmlUtils.LABEL_CLIENTINFO_FIRMWAREVER, Value.FIRMWARE_VER);
        serializer.attribute("", XmlUtils.LABEL_CLIENTINFO_BASEBANDVER, "unknown");
        serializer.attribute("", XmlUtils.LABEL_CLIENTINFO_KERNELVER, "1");
        serializer.attribute("", XmlUtils.LABEL_CLIENTINFO_BUILDNUMBER, Value.BUILDNUMBER);
        serializer.endTag("", XmlUtils.LABEL_CLIENTINFO);
        return serializer;
    }

    public XmlSerializer getHeader(XmlSerializer serializer) {
        return serializer;
    }

    public byte[] getRequestBytes() throws IllegalStateException, IOException, Exception {
        return getRequestByte();
    }

    private byte[] getRequestByte() throws IllegalStateException, Exception {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        byte[] xml = getRequestXmlStr().getBytes("utf-8");
        out.write(getHeaderByte(xml.length));
        out.write(xml);
        Iterator<CloudApkInfo> it = this.cloudApkInfos.iterator();
        while (it.hasNext()) {
            CloudApkInfo info = it.next();
            if (info.getCertRSA() != null) {
                out.write(getHeaderByte(info.getCertRSA().length));
                out.write(info.getCertRSA());
            }
            if (info != null) {
                info.clearData();
            }
        }
        out.close();
        return out.toByteArray();
    }

    public String cert_rsa_path() {
        return this.context.getFilesDir() + CloudHandler.Cert_rsa_path;
    }

    public byte[] getReportSuspiciousRequestByte(Rate rate) throws IllegalStateException, Exception {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        byte[] xml = getInformRequestXmlStr(rate).getBytes("utf-8");
        out.write(getHeaderByte(xml.length));
        out.write(xml);
        CloudApkInfo info = new CloudApkInfo();
        info.setInstallPath(DD02zqNU.DLLIxskak(this.context.getPackageManager(), rate.getPkgName(), 64).applicationInfo.publicSourceDir);
        byte[] rsaByte = CloudHandler.readFileByte(String.valueOf(this.context.getFilesDir().getAbsolutePath()) + "/" + info.getPkgName());
        if (rsaByte == null || rsaByte.length <= 0) {
            info.setCertRSA(CloudHandler.getSignFile(info.getInstallPath(), ".RSA", cert_rsa_path()));
            CloudHandler.craeteFile(String.valueOf(this.context.getFilesDir().getAbsolutePath()) + "/" + rate.getPkgName(), info.getCertRSA());
        } else {
            info.setCertRSA(CloudHandler.readFileByte(String.valueOf(this.context.getFilesDir().getAbsolutePath()) + "/" + info.getPkgName()));
        }
        if (info.getCertRSA() != null) {
            if (rsaByte == null || rsaByte.length <= 0) {
                out.write(getHeaderByte(CloudHandler.getSignFile(info.getInstallPath(), ".RSA", cert_rsa_path()).length));
                CloudHandler.craeteFile(String.valueOf(this.context.getFilesDir().getAbsolutePath()) + "/" + rate.getPkgName(), info.getCertRSA());
            } else {
                out.write(getHeaderByte(CloudHandler.readFileByte(String.valueOf(this.context.getFilesDir().getAbsolutePath()) + "/" + info.getPkgName()).length));
            }
            out.write(info.getCertRSA());
        }
        if (info != null) {
            info.clearData();
        }
        out.close();
        return out.toByteArray();
    }

    public byte[] getScoreReviewRequestByte(Rate rate) throws IllegalStateException, Exception {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        out.write(getReviewRequestXmlStr(rate).getBytes("utf-8"));
        out.close();
        return out.toByteArray();
    }

    public byte[] getMoreScoreReviewRequestByte(Rate rate) throws IllegalStateException, Exception {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        out.write(getMoreReviewRequestXmlStr(rate).getBytes("utf-8"));
        out.close();
        return out.toByteArray();
    }

    public byte[] getCloudPassageRequestByte(Rate rate) throws IllegalStateException, Exception {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        byte[] xml = getCloudPassageRequestXmlStr(rate).getBytes("utf-8");
        out.write(getHeaderByte(xml.length));
        out.write(xml);
        if (new File(this.context.getFilesDir() + XmlUtils.PERFORMANCE_DAT).exists()) {
            out.write(getHeaderByte(CommonMethod.readFile(this.context.getFilesDir() + XmlUtils.PERFORMANCE_DAT).length));
            out.write(CommonMethod.readFile(this.context.getFilesDir() + XmlUtils.PERFORMANCE_DAT));
        }
        if (new File(this.context.getFilesDir() + XmlUtils.URL_PERFORMANCE_DAT).exists()) {
            out.write(getHeaderByte(CommonMethod.readFile(this.context.getFilesDir() + XmlUtils.URL_PERFORMANCE_DAT).length));
            out.write(CommonMethod.readFile(this.context.getFilesDir() + XmlUtils.URL_PERFORMANCE_DAT));
        }
        out.close();
        return out.toByteArray();
    }

    private byte[] getHeaderByte(int length) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(baos);
        dos.writeInt(length);
        dos.close();
        baos.close();
        byte[] header = new byte[4];
        for (int i = 0; i < baos.toByteArray().length; i++) {
            header[3 - i] = baos.toByteArray()[i];
        }
        return header;
    }

    public XmlSerializer getReviewInfo(XmlSerializer serializer, Rate rate) throws IllegalArgumentException, RuntimeException, IOException {
        serializer.startTag("", XmlUtils.LABEL_RATE);
        serializer.attribute("", XmlUtils.LABEL_SERVER_Id, rate.getServerId());
        serializer.attribute("", "pkgName", rate.getPkgName());
        serializer.attribute("", "name", rate.getName());
        serializer.attribute("", XmlUtils.LABEL_VERSIONCODE, rate.getVersionCode());
        serializer.attribute("", XmlUtils.LANBL_VERSIONNAME, rate.getVersionName());
        if (rate.getScore() == null) {
            serializer.attribute("", "score", "");
        } else if (rate.getScore().length() > 0) {
            serializer.attribute("", "score", rate.getScore());
        }
        serializer.attribute("", XmlUtils.FIRST, rate.getFirst());
        serializer.endTag("", XmlUtils.LABEL_RATE);
        serializer.startTag("", XmlUtils.LABEL_REVIEW);
        if (rate.getContent() == null) {
            serializer.attribute("", XmlUtils.LABEL_CONTENT, "");
        } else if (rate.getContent().length() > 0) {
            serializer.attribute("", XmlUtils.LABEL_CONTENT, rate.getContent());
        }
        serializer.endTag("", XmlUtils.LABEL_REVIEW);
        return serializer;
    }

    public String getReviewRequestXmlStr(Rate rate) throws Exception, IllegalStateException, IOException {
        XmlSerializer serializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();
        serializer.setOutput(writer);
        serializer.startDocument(StringEncodings.UTF8, true);
        serializer.startTag("", XmlUtils.LABEL_REQUEST);
        XmlSerializer serializer2 = getReviewInfo(getUploadInfo(getServerInfo(getClientInfo(getMobileInfo(serializer)))), rate);
        serializer2.endTag("", XmlUtils.LABEL_REQUEST);
        serializer2.endDocument();
        System.gc();
        return writer.toString();
    }

    public XmlSerializer getMoreReviewInfo(XmlSerializer serializer, Rate rate) throws IllegalArgumentException, RuntimeException, IOException {
        serializer.startTag("", XmlUtils.LABEL_REVIEW);
        serializer.attribute("", XmlUtils.LABEL_SERVER_Id, rate.getServerId());
        serializer.attribute("", "pkgName", rate.getPkgName());
        serializer.attribute("", "name", rate.getName());
        serializer.attribute("", XmlUtils.LABEL_VERSIONCODE, rate.getVersionCode());
        serializer.attribute("", XmlUtils.LANBL_VERSIONNAME, rate.getVersionName());
        serializer.attribute("", XmlUtils.LABEL_OFFSET, rate.getOffset());
        serializer.attribute("", XmlUtils.LABEL_LENGTH, rate.getLength());
        serializer.endTag("", XmlUtils.LABEL_REVIEW);
        return serializer;
    }

    public String getMoreReviewRequestXmlStr(Rate rate) throws Exception, IllegalStateException, IOException {
        XmlSerializer serializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();
        serializer.setOutput(writer);
        serializer.startDocument(StringEncodings.UTF8, true);
        serializer.startTag("", XmlUtils.LABEL_REQUEST);
        XmlSerializer serializer2 = getMoreReviewInfo(getUploadInfo(getServerInfo(getClientInfo(getMobileInfo(serializer)))), rate);
        serializer2.endTag("", XmlUtils.LABEL_REQUEST);
        serializer2.endDocument();
        System.gc();
        return writer.toString();
    }

    public XmlSerializer getInformInfo(XmlSerializer serializer, Rate rate) throws IllegalArgumentException, RuntimeException, IOException {
        serializer.startTag("", XmlUtils.LABEL_APK);
        serializer.attribute("", "id", rate.getId());
        serializer.attribute("", "pkgName", rate.getPkgName());
        serializer.attribute("", "name", rate.getName());
        serializer.attribute("", XmlUtils.LABEL_VERSIONCODE, rate.getVersionCode());
        serializer.attribute("", XmlUtils.LANBL_VERSIONNAME, rate.getVersionName());
        serializer.startTag("", XmlUtils.LABEL_FILE);
        serializer.attribute("", "name", rate.getFileName());
        serializer.attribute("", XmlUtils.LABEL_REF, rate.getRef());
        serializer.endTag("", XmlUtils.LABEL_FILE);
        serializer.startTag("", XmlUtils.REASON);
        serializer.cdsect(rate.getReason());
        serializer.endTag("", XmlUtils.REASON);
        serializer.endTag("", XmlUtils.LABEL_APK);
        return serializer;
    }

    public String getInformRequestXmlStr(Rate rate) throws Exception, IllegalStateException, IOException {
        XmlSerializer serializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();
        serializer.setOutput(writer);
        serializer.startDocument(StringEncodings.UTF8, true);
        serializer.startTag("", XmlUtils.LABEL_REQUEST);
        XmlSerializer serializer2 = getInformInfo(getUploadInfo(getServerInfo(getClientInfo(getMobileInfo(serializer)))), rate);
        serializer2.endTag("", XmlUtils.LABEL_REQUEST);
        serializer2.endDocument();
        System.gc();
        return writer.toString();
    }

    public XmlSerializer getCloudPassageInfo(XmlSerializer serializer, Rate rate) throws IllegalArgumentException, RuntimeException, IOException {
        serializer.startTag("", XmlUtils.FILES);
        int ref = -1;
        if (new File(this.context.getFilesDir() + XmlUtils.PERFORMANCE_DAT).exists()) {
            ref = -1 + 1;
            serializer.startTag("", XmlUtils.LABEL_FILE);
            serializer.attribute("", "name", rate.getPerformance());
            serializer.attribute("", XmlUtils.LABEL_REF, new StringBuilder(String.valueOf(ref)).toString());
            serializer.endTag("", XmlUtils.LABEL_FILE);
        }
        if (new File(this.context.getFilesDir() + XmlUtils.URL_PERFORMANCE_DAT).exists()) {
            serializer.startTag("", XmlUtils.LABEL_FILE);
            serializer.attribute("", "name", rate.getUrl_performance());
            serializer.attribute("", XmlUtils.LABEL_REF, new StringBuilder(String.valueOf(ref + 1)).toString());
            serializer.endTag("", XmlUtils.LABEL_FILE);
        }
        serializer.endTag("", XmlUtils.FILES);
        return serializer;
    }

    public String getCloudPassageRequestXmlStr(Rate rate) throws Exception, IllegalStateException, IOException {
        XmlSerializer serializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();
        serializer.setOutput(writer);
        serializer.startDocument(StringEncodings.UTF8, true);
        serializer.startTag("", XmlUtils.LABEL_REQUEST);
        XmlSerializer serializer2 = getCloudPassageInfo(getUploadInfo(getServerInfo(getClientInfo(getMobileInfo(serializer)))), rate);
        serializer2.endTag("", XmlUtils.LABEL_REQUEST);
        serializer2.endDocument();
        System.gc();
        return writer.toString();
    }
}
