package com.netqin.antivirus.cloud.model;

import android.graphics.drawable.Drawable;
import com.netqin.antivirus.cloud.apkinfo.domain.Review;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.codec.binary.Base64;

public class CloudApkInfo {
    private String Advice;
    private String Note;
    private String Reason;
    private byte[] certRSA = null;
    private String cnt;
    private String cntUniq;
    private String first;
    private Drawable icon;
    private String id = "0";
    private String installPath = "";
    private String intallTime = "";
    private String isAmDownload = "False";
    private byte[] maniFest = null;
    private String myScore;
    private String name = "";
    private String pkgName = "";
    List<Review> reviews = new ArrayList();
    public List<String> runingServiceList = new ArrayList(2);
    private String score;
    private String security;
    private String securityDesc;
    private String serverId;
    private String size;
    private String systemApp = "False";
    private String versionCode;
    private String versionName;
    private String virusName = "";
    private String wanted = "";

    public byte[] getCertRSA() {
        return this.certRSA;
    }

    public String getFirst() {
        return this.first;
    }

    public void setFirst(String first2) {
        this.first = first2;
    }

    public Drawable getIcon() {
        return this.icon;
    }

    public void setIcon(Drawable icon2) {
        this.icon = icon2;
    }

    public String getSecurityDesc() {
        return this.securityDesc;
    }

    public void setSecurityDesc(String securityDesc2) {
        this.securityDesc = securityDesc2;
    }

    public String getSecurity() {
        return this.security;
    }

    public void setSecurity(String security2) {
        this.security = security2;
    }

    public String getWanted() {
        return this.wanted;
    }

    public void setWanted(String wanted2) {
        this.wanted = wanted2;
    }

    public String getServerId() {
        return this.serverId;
    }

    public void setServerId(String serverId2) {
        this.serverId = serverId2;
    }

    public String getMyScore() {
        return this.myScore;
    }

    public void setMyScore(String myScore2) {
        this.myScore = myScore2;
    }

    public String getSize() {
        return this.size;
    }

    public void setSize(String size2) {
        this.size = size2;
    }

    public String getScore() {
        return this.score;
    }

    public void setScore(String score2) {
        this.score = score2;
    }

    public String getCntUniq() {
        return this.cntUniq;
    }

    public void setCntUniq(String cntUniq2) {
        this.cntUniq = cntUniq2;
    }

    public String getNote() {
        return this.Note;
    }

    public void setNote(String note) {
        this.Note = note;
    }

    public String getReason() {
        return this.Reason;
    }

    public void setReason(String reason) {
        this.Reason = reason;
    }

    public String getAdvice() {
        return this.Advice;
    }

    public void setAdvice(String advice) {
        this.Advice = advice;
    }

    public String getCnt() {
        return this.cnt;
    }

    public void setCnt(String cnt2) {
        this.cnt = cnt2;
    }

    public List<Review> getReviews() {
        return this.reviews;
    }

    public void setReviews(List<Review> reviews2) {
        this.reviews = reviews2;
    }

    public void setCertRSA(byte[] certRSA2) {
        this.certRSA = certRSA2;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getPkgName() {
        return this.pkgName;
    }

    public void setPkgName(String pkgName2) {
        this.pkgName = pkgName2;
    }

    public String getInstallPath() {
        return this.installPath;
    }

    public void setInstallPath(String installPath2) {
        this.installPath = installPath2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getIntallTime() {
        return this.intallTime;
    }

    public void setIntallTime(String intallTime2) {
        this.intallTime = intallTime2;
    }

    public String getSystemApp() {
        return this.systemApp;
    }

    public void setSystemApp(String systemApp2) {
        this.systemApp = systemApp2;
    }

    public String getManiFest() {
        if (this.maniFest != null) {
            return new String(Base64.encodeBase64(this.maniFest));
        }
        return "";
    }

    public void setManiFest(byte[] maniFest2) {
        this.maniFest = maniFest2;
    }

    public byte[] getManiFestByte() {
        return this.maniFest;
    }

    public void setVirusName(String name2) {
        this.virusName = name2;
    }

    public String getVirusName() {
        return this.virusName;
    }

    public String getIsAmDownLoad() {
        return this.isAmDownload;
    }

    public void clearData() {
        this.maniFest = null;
        this.certRSA = null;
    }

    public String getVersionCode() {
        return this.versionCode;
    }

    public void setVersionCode(String versionCode2) {
        this.versionCode = versionCode2;
    }

    public String getVersionName() {
        return this.versionName;
    }

    public void setVersionName(String versionName2) {
        this.versionName = versionName2;
    }

    public boolean isHarm() {
        if (this.security == null || Integer.parseInt(this.security) != 10) {
            return false;
        }
        return true;
    }

    public boolean isSecure() {
        if (this.security == null || Integer.parseInt(this.security) < 50) {
            return false;
        }
        return true;
    }

    public boolean isAutoUpload() {
        if (this.wanted.toLowerCase().equals("1")) {
            return true;
        }
        return false;
    }
}
