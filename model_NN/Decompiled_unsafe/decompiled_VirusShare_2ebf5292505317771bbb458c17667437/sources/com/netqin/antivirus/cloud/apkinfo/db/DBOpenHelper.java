package com.netqin.antivirus.cloud.apkinfo.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBOpenHelper extends SQLiteOpenHelper {
    public DBOpenHelper(Context context) {
        super(context, "cloudapkinfo.db", (SQLiteDatabase.CursorFactory) null, 4);
        SQLiteDatabase database = getReadableDatabase();
        onCreate(database);
        database.close();
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS cloudapkinfo (id integer primary key autoincrement, serverId varchar(5),apkname varchar(20), security VARCHAR(10),wanted VARCHAR(2),virusName varchar(50),score varchar(10),cntUniq varchar(10),Note varchar(50),Reason varchar(50),Advice varchar(50),versionCode varchar(20),versionName varchar(20),rsa blob,size VARCHAR(12),cnt VARCHAR(12),first VARCHAR(2),myscore VARCHAR(5))");
        db.execSQL("CREATE TABLE IF NOT EXISTS securitylevel (id integer primary key autoincrement, securityid varchar(5), text VARCHAR(10))");
        db.execSQL("CREATE TABLE IF NOT EXISTS review (id integer primary key autoincrement,serverId varchar(5),uid varchar(20), date VARCHAR(50),score varchar(5),content varchar(100),apkname varchar(20))");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table cloudapkinfo");
        db.execSQL("drop table securitylevel");
        db.execSQL("drop table review");
    }
}
