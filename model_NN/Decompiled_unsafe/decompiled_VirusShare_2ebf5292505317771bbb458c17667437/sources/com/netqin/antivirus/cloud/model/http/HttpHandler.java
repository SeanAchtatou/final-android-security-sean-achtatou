package com.netqin.antivirus.cloud.model.http;

import android.content.Context;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.zip.DataFormatException;

public class HttpHandler implements Runnable {
    private static HttpHandler _this = null;
    private boolean cancel = false;
    private HttpURLConnection hc = null;
    private HttpListener listener = null;
    private Context mContext;
    private Request request = null;

    public static HttpHandler getInstance() {
        if (_this == null) {
            _this = new HttpHandler();
        }
        return _this;
    }

    private HttpHandler() {
    }

    public void startConn(HttpListener listener2, Request request2, Context context) {
        this.request = request2;
        this.listener = listener2;
        this.mContext = context;
        setCancel(false);
        new Thread(this).start();
    }

    public void setCancel(boolean cancel2) {
        this.cancel = cancel2;
        HttpUtil.getInstance().setCancel(cancel2);
    }

    public HttpHandler(HttpListener listener2, Request request2) {
        this.request = request2;
        this.listener = listener2;
        new Thread(this).start();
    }

    private Response conn(Context context) throws IOException, DataFormatException {
        Response response = new Response();
        response.setScheme(this.request.getSchema());
        this.hc = HttpUtil.getInstance().getConnectionWithPost(this.request, this.mContext);
        if (this.hc == null) {
            return null;
        }
        response.setResponseCode(this.hc.getResponseCode());
        if (response.getResponseCode() == 302) {
            this.request.setUrl(this.hc.getHeaderField("Location"));
            return conn(this.mContext);
        } else if (this.cancel) {
            return null;
        } else {
            response.setResponseData(HttpUtil.doResponse(this.hc));
            return response;
        }
    }

    /* JADX INFO: Multiple debug info for r0v2 java.io.IOException: [D('e' java.util.zip.DataFormatException), D('e' java.io.IOException)] */
    public void run() {
        try {
            if (!this.cancel) {
                HttpUtil.getInstance().setCancel(false);
                this.listener.completed(conn(this.mContext));
            }
            try {
                HttpUtil.release(null, this.hc);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e2) {
            IOException e3 = e2;
            this.listener.Exception(e3);
            e3.printStackTrace();
            try {
                HttpUtil.release(null, this.hc);
            } catch (IOException e4) {
                e4.printStackTrace();
            }
        } catch (DataFormatException e5) {
            DataFormatException e6 = e5;
            this.listener.Exception(e6);
            e6.printStackTrace();
            try {
                HttpUtil.release(null, this.hc);
            } catch (IOException e7) {
                e7.printStackTrace();
            }
        } catch (Throwable th) {
            try {
                HttpUtil.release(null, this.hc);
            } catch (IOException e8) {
                e8.printStackTrace();
            }
            throw th;
        }
    }
}
