package com.netqin.antivirus.cloud.apkinfo.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import com.netqin.antivirus.appprotocol.XmlTagValue;
import com.netqin.antivirus.cloud.apkinfo.domain.Review;
import com.netqin.antivirus.cloud.apkinfo.domain.SClasse;
import com.netqin.antivirus.cloud.model.CloudApkInfo;
import com.netqin.antivirus.cloud.model.xml.XmlUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CloudApiInfoDb {
    public static boolean isUseDb = false;
    private DBOpenHelper dbOpenHelper = null;
    private SQLiteDatabase sqLiteDatabase = null;

    public CloudApiInfoDb(Context context) {
        this.dbOpenHelper = new DBOpenHelper(context);
        this.sqLiteDatabase = this.dbOpenHelper.getReadableDatabase();
    }

    public SQLiteDatabase getSqLiteDatabase() {
        return this.sqLiteDatabase;
    }

    public void insert(CloudApkInfo info) {
        if (TextUtils.isEmpty(info.getVersionName())) {
            info.setVersionName("nq.1");
        }
        if (TextUtils.isEmpty(info.getVersionCode())) {
            info.setVersionCode("1");
        }
        this.sqLiteDatabase.execSQL("insert into cloudapkinfo (serverId,apkname,security,wanted,virusName,score,cntUniq,Note,Reason,Advice,versionCode,versionName,rsa,size,cnt) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", new Object[]{info.getServerId(), info.getPkgName(), info.getSecurity(), info.getWanted(), info.getVirusName(), info.getScore(), info.getCntUniq(), info.getNote(), info.getReason(), info.getAdvice(), info.getVersionCode(), info.getVersionName(), info.getCertRSA(), info.getSize(), info.getCnt()});
    }

    public void insterSClass(SClasse info) {
        this.sqLiteDatabase.execSQL("insert into securitylevel (securityid,text) values (?,?)", new Object[]{info.getSecurityId(), info.getText()});
    }

    public void insterReview(Review info) {
        this.sqLiteDatabase.execSQL("insert into review (serverId,uid,date,score,content,apkname) values (?,?,?,?,?,?)", new Object[]{info.getServerId(), info.getUid(), info.getDate(), info.getScore(), info.getContent(), info.getPkgName()});
    }

    public void deleteReview(String packageName, String serverId) {
        this.sqLiteDatabase.execSQL("delete from review  where serverId=? and apkname=?", new Object[]{serverId, packageName});
    }

    public void deleteApk(String packageName, String serverId) {
        this.sqLiteDatabase.execSQL("delete from cloudapkinfo  where serverId=? and apkname=?", new Object[]{serverId, packageName});
    }

    public void updateApkInfo(String packageName, String serverId, CloudApkInfo info) {
        this.sqLiteDatabase.execSQL("update cloudapkinfo set cnt=? ,cntUniq=? ,score=? where serverId=? and apkname=?", new Object[]{info.getCnt(), info.getCntUniq(), info.getScore(), serverId, packageName});
    }

    public int updateWholeApkInfo(String packageName, String serverId, CloudApkInfo info) {
        ContentValues values = new ContentValues();
        if (!TextUtils.isEmpty(info.getSecurity())) {
            values.put(XmlUtils.LABEL_APK_SECURITY, info.getSecurity());
        }
        if (!TextUtils.isEmpty(info.getWanted())) {
            values.put(XmlUtils.LABEL_APK_WANTED, info.getWanted());
        }
        if (!TextUtils.isEmpty(info.getVirusName())) {
            values.put("virusName", info.getVirusName());
        }
        if (!TextUtils.isEmpty(info.getScore())) {
            values.put("score", info.getScore());
        }
        if (!TextUtils.isEmpty(info.getCnt())) {
            values.put("cnt", info.getCnt());
        }
        if (!TextUtils.isEmpty(info.getCntUniq())) {
            values.put("cntUniq", info.getCntUniq());
        }
        if (!TextUtils.isEmpty(info.getNote())) {
            values.put("Note", info.getNote());
        }
        if (!TextUtils.isEmpty(info.getReason())) {
            values.put("Reason", info.getReason());
        }
        if (!TextUtils.isEmpty(info.getAdvice())) {
            values.put("Advice", info.getAdvice());
        }
        if (!TextUtils.isEmpty(info.getVersionCode())) {
            values.put(XmlUtils.LABEL_VERSIONCODE, info.getVersionCode());
        }
        if (!TextUtils.isEmpty(info.getVersionName())) {
            values.put(XmlUtils.LANBL_VERSIONNAME, info.getVersionName());
        }
        if (info.getCertRSA() != null) {
            values.put("rsa", info.getCertRSA());
        }
        return this.sqLiteDatabase.update("cloudapkinfo", values, "serverId=? and apkname=?", new String[]{serverId, packageName});
    }

    public void updateApkMyScore(String packageName, String serverId, String first, String myScore) {
        this.sqLiteDatabase.execSQL("update cloudapkinfo set first=? ,myscore=?  where serverId=? and apkname=?", new Object[]{first, myScore, serverId, packageName});
    }

    public CloudApkInfo getCloudApkData(String packageName, String versionCode, String versionName, byte[] rsaB) {
        if (TextUtils.isEmpty(versionCode)) {
            versionCode = "1";
        }
        if (TextUtils.isEmpty(versionName)) {
            versionName = "nq.1";
        }
        Cursor cusor = this.sqLiteDatabase.rawQuery("select * from cloudapkinfo where apkname=? and versionCode=? and versionName=?", new String[]{packageName, versionCode, versionName});
        CloudApkInfo info = null;
        Map<String, SClasse> sclasseMap = null;
        try {
            sclasseMap = getAllSClass();
        } catch (Exception e) {
            e.printStackTrace();
        }
        while (cusor.moveToNext()) {
            info = new CloudApkInfo();
            info.setId(cusor.getString(cusor.getColumnIndex("id")));
            info.setServerId(cusor.getString(cusor.getColumnIndex(XmlUtils.LABEL_SERVER_Id)));
            info.setFirst(cusor.getString(cusor.getColumnIndex("first")));
            info.setMyScore(cusor.getString(cusor.getColumnIndex("myscore")));
            info.setPkgName(cusor.getString(cusor.getColumnIndex("apkname")));
            info.setSecurity(cusor.getString(cusor.getColumnIndex(XmlUtils.LABEL_APK_SECURITY)));
            info.setWanted(cusor.getString(cusor.getColumnIndex(XmlUtils.LABEL_APK_WANTED)));
            info.setVirusName(cusor.getString(cusor.getColumnIndex("virusName")));
            info.setScore(cusor.getString(cusor.getColumnIndex("score")));
            info.setCntUniq(cusor.getString(cusor.getColumnIndex("cntUniq")));
            info.setNote(cusor.getString(cusor.getColumnIndex("Note")));
            info.setReason(cusor.getString(cusor.getColumnIndex("Reason")));
            info.setAdvice(cusor.getString(cusor.getColumnIndex("Advice")));
            info.setVersionCode(cusor.getString(cusor.getColumnIndex(XmlUtils.LABEL_VERSIONCODE)));
            info.setVersionName(cusor.getString(cusor.getColumnIndex(XmlUtils.LANBL_VERSIONNAME)));
            info.setCertRSA(cusor.getBlob(13));
            if (!(info.getCertRSA() == null || rsaB == null)) {
                if (info.getCertRSA().length == rsaB.length) {
                    for (int i = 0; i < info.getCertRSA().length - 1; i++) {
                        if (info.getCertRSA()[i] != rsaB[i]) {
                            cusor.close();
                            return null;
                        }
                    }
                } else {
                    cusor.close();
                    return null;
                }
            }
            info.setSize(cusor.getString(cusor.getColumnIndex(XmlTagValue.size)));
            info.setCnt(cusor.getString(cusor.getColumnIndex("cnt")));
            info.setReviews(getReviews(info.getPkgName(), info.getServerId()));
            if (!(sclasseMap == null || sclasseMap.get(info.getSecurity()) == null)) {
                info.setSecurityDesc(sclasseMap.get(info.getSecurity()).getText());
            }
        }
        cusor.close();
        return info;
    }

    private List<Review> getReviews(String packageName, String serverId) {
        Cursor cusor = this.sqLiteDatabase.rawQuery("select * from review where apkname=? and serverId=? ", new String[]{packageName, serverId});
        List<Review> reviewList = new ArrayList<>();
        while (cusor.moveToNext()) {
            Review info = new Review();
            info.setId(cusor.getString(cusor.getColumnIndex("id")));
            info.setServerId(cusor.getString(cusor.getColumnIndex(XmlUtils.LABEL_SERVER_Id)));
            info.setPkgName(cusor.getString(cusor.getColumnIndex("apkname")));
            info.setUid(cusor.getString(cusor.getColumnIndex(XmlUtils.LABEL_CLIENTINFO_UID)));
            info.setDate(cusor.getString(cusor.getColumnIndex("date")));
            info.setScore(cusor.getString(cusor.getColumnIndex("score")));
            info.setContent(cusor.getString(cusor.getColumnIndex(XmlUtils.LABEL_CONTENT)));
            reviewList.add(info);
        }
        cusor.close();
        return reviewList;
    }

    private SClasse getSClasse(String securityid) {
        Cursor cusor = this.sqLiteDatabase.rawQuery("select * from securitylevel where securityid=?", new String[]{securityid});
        SClasse info = new SClasse();
        while (cusor.moveToNext()) {
            info.setId(cusor.getString(cusor.getColumnIndex("id")));
            info.setSecurityId(cusor.getString(cusor.getColumnIndex("securityid")));
            info.setText(cusor.getString(cusor.getColumnIndex("text")));
        }
        cusor.close();
        return info;
    }

    private String getSClassStr(String securityid) {
        Cursor cusor = this.sqLiteDatabase.rawQuery("select * from securitylevel where securityid=?", new String[]{securityid});
        SClasse info = new SClasse();
        while (cusor.moveToNext()) {
            info.setId(cusor.getString(cusor.getColumnIndex("id")));
            info.setSecurityId(cusor.getString(cusor.getColumnIndex("securityid")));
            info.setText(cusor.getString(cusor.getColumnIndex("text")));
        }
        cusor.close();
        return info.getText();
    }

    public void close_virus_DB() {
        try {
            if (this.sqLiteDatabase != null) {
                if (this.sqLiteDatabase.isOpen()) {
                    isUseDb = false;
                }
                this.sqLiteDatabase.close();
                this.sqLiteDatabase = null;
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void dropTable() {
        try {
            this.sqLiteDatabase.execSQL("drop table securitylevel");
            this.sqLiteDatabase.execSQL("drop table review");
        } catch (SQLException e) {
        }
    }

    public void dropClassesTable() {
        try {
            this.sqLiteDatabase.execSQL("drop table securitylevel");
        } catch (SQLException e) {
        }
    }

    private Map<String, SClasse> getAllSClass() {
        Cursor cusor = this.sqLiteDatabase.rawQuery("select * from securitylevel", null);
        Map<String, SClasse> sclasseMap = new HashMap<>();
        while (cusor.moveToNext()) {
            SClasse sClasse = new SClasse();
            sClasse.setId(cusor.getString(cusor.getColumnIndex("id")));
            sClasse.setSecurityId(cusor.getString(cusor.getColumnIndex("securityid")));
            sClasse.setText(cusor.getString(cusor.getColumnIndex("text")));
            sclasseMap.put(sClasse.getSecurityId(), sClasse);
        }
        cusor.close();
        return sclasseMap;
    }

    public List<CloudApkInfo> getAllApkData() {
        Cursor cusor = this.sqLiteDatabase.rawQuery("select * from cloudapkinfo", null);
        List<CloudApkInfo> infoList = new ArrayList<>();
        Map<String, SClasse> sclasseMap = null;
        try {
            sclasseMap = getAllSClass();
        } catch (Exception e) {
            e.printStackTrace();
        }
        while (cusor.moveToNext()) {
            CloudApkInfo info = new CloudApkInfo();
            info.setId(cusor.getString(cusor.getColumnIndex("id")));
            info.setServerId(cusor.getString(cusor.getColumnIndex(XmlUtils.LABEL_SERVER_Id)));
            info.setFirst(cusor.getString(cusor.getColumnIndex("first")));
            info.setMyScore(cusor.getString(cusor.getColumnIndex("myscore")));
            info.setPkgName(cusor.getString(cusor.getColumnIndex("apkname")));
            info.setSecurity(cusor.getString(cusor.getColumnIndex(XmlUtils.LABEL_APK_SECURITY)));
            info.setWanted(cusor.getString(cusor.getColumnIndex(XmlUtils.LABEL_APK_WANTED)));
            info.setVirusName(cusor.getString(cusor.getColumnIndex("virusName")));
            info.setScore(cusor.getString(cusor.getColumnIndex("score")));
            info.setCntUniq(cusor.getString(cusor.getColumnIndex("cntUniq")));
            info.setNote(cusor.getString(cusor.getColumnIndex("Note")));
            info.setReason(cusor.getString(cusor.getColumnIndex("Reason")));
            info.setAdvice(cusor.getString(cusor.getColumnIndex("Advice")));
            info.setVersionCode(cusor.getString(cusor.getColumnIndex(XmlUtils.LABEL_VERSIONCODE)));
            info.setVersionName(cusor.getString(cusor.getColumnIndex(XmlUtils.LANBL_VERSIONNAME)));
            info.setCertRSA(cusor.getBlob(13));
            info.setSize(cusor.getString(cusor.getColumnIndex(XmlTagValue.size)));
            info.setCnt(cusor.getString(cusor.getColumnIndex("cnt")));
            info.setReviews(getReviews(info.getPkgName(), info.getServerId()));
            if (!(sclasseMap == null || sclasseMap.get(info.getSecurity()) == null)) {
                info.setSecurityDesc(sclasseMap.get(info.getSecurity()).getText());
            }
            infoList.add(info);
        }
        cusor.close();
        return infoList;
    }
}
