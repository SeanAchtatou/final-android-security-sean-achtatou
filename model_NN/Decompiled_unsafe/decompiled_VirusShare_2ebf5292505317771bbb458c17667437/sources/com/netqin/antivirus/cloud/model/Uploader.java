package com.netqin.antivirus.cloud.model;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import com.netqin.antivirus.common.CommonMethod;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;

public class Uploader {
    private static final String CACHE_AUTO_UPLOAD = "/upload_info.txt";
    private static final String TAG = "Uploader";
    private static String UPLOAD_URL = "";
    private static Uploader _this = null;
    private final boolean DBG = false;
    private final long SPLIT_SIZE = 50000;
    private Context context = null;
    /* access modifiers changed from: private */
    public ArrayList<String> files = new ArrayList<>();
    /* access modifiers changed from: private */
    public Handler handler;
    /* access modifiers changed from: private */
    public boolean isRunning = false;
    private WifiReceiver receiver;
    /* access modifiers changed from: private */
    public boolean stop = false;

    private Uploader(Context context2) {
        this.context = context2;
    }

    public String cache_auto_upload_path() {
        return this.context.getFilesDir() + CACHE_AUTO_UPLOAD;
    }

    /* access modifiers changed from: private */
    public void runReport() {
        new Thread(new Runnable() {
            public void run() {
                Uploader.this.isRunning = true;
                if (Uploader.this.files == null || Uploader.this.files.size() == 0) {
                    Uploader.this.isRunning = false;
                    if (Uploader.this.handler != null) {
                        Uploader.this.handler.obtainMessage().what = 1;
                    }
                } else if (Uploader.this.stop) {
                    Uploader.this.isRunning = false;
                } else {
                    String filePath = (String) Uploader.this.files.iterator().next();
                    boolean success = Uploader.this.updateFile(new File(filePath));
                    Message message = new Message();
                    message.obj = Boolean.valueOf(success);
                    message.what = 9;
                    if (Uploader.this.handler != null) {
                        Uploader.this.handler.sendMessage(message);
                    }
                    if (success) {
                        Uploader.this.files.remove(filePath);
                        Iterator it = Uploader.this.files.iterator();
                        while (it.hasNext()) {
                            Uploader.reCraeteFile(Uploader.this.cache_auto_upload_path(), (String.valueOf((String) it.next()) + "\n").getBytes());
                        }
                        if (Uploader.this.files.size() > 0) {
                            Iterator it2 = Uploader.this.files.iterator();
                            while (it2.hasNext()) {
                                Uploader.reCraeteFile(Uploader.this.cache_auto_upload_path(), (String.valueOf((String) it2.next()) + "\n").getBytes());
                            }
                        } else {
                            File uploadFile = new File(Uploader.this.cache_auto_upload_path());
                            if (uploadFile.exists()) {
                                uploadFile.delete();
                            }
                        }
                    }
                    Uploader.this.isRunning = false;
                    if (!Uploader.this.stop) {
                        Uploader.this.runReport();
                    }
                }
            }
        }).start();
    }

    public static void reCraeteFile(String path, byte[] bytes) {
        IOException e;
        FileNotFoundException e2;
        File f = new File(path);
        if (!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException e3) {
                e3.printStackTrace();
                return;
            }
        }
        try {
            FileOutputStream fout = new FileOutputStream(f);
            try {
                fout.write(bytes);
                fout.close();
            } catch (FileNotFoundException e4) {
                e2 = e4;
                e2.printStackTrace();
            } catch (IOException e5) {
                e = e5;
                e.printStackTrace();
            }
        } catch (FileNotFoundException e6) {
            e2 = e6;
            e2.printStackTrace();
        } catch (IOException e7) {
            e = e7;
            e.printStackTrace();
        }
    }

    public static Uploader getInstance(Context context2) {
        if (_this == null) {
            _this = new Uploader(context2);
        }
        return _this;
    }

    public void addFileToUpload(ArrayList<String> files2) {
        if (files2 == null) {
            try {
                this.files.addAll(getCacheFile());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            this.files.addAll(files2);
        }
    }

    private ArrayList<String> getCacheFile() throws Exception {
        String[] paths = CloudHandler.getInstance(this.context).readFile(CloudHandler.getInstance(this.context).cache_auto_upload_path()).split("\n");
        ArrayList<String> files2 = new ArrayList<>();
        for (String add : paths) {
            files2.add(add);
        }
        return files2;
    }

    public void startUploadFile(ArrayList<String> files2, String reportUrl, Handler handler2) {
        this.handler = handler2;
        setStop(false);
        UPLOAD_URL = reportUrl;
        addFileToUpload(files2);
        notifyUpload();
    }

    public void notifyUpload() {
        if (!this.isRunning) {
            runReport();
        }
    }

    public void setStop(boolean stop2) {
        this.stop = stop2;
    }

    public boolean updateFile(File file) {
        int splits;
        if (this.stop) {
            return false;
        }
        try {
            long totalSize = file.length();
            if (totalSize % 50000 == 0) {
                splits = (int) (totalSize / 50000);
            } else {
                splits = ((int) ((totalSize - (totalSize % 50000)) / 50000)) + 1;
            }
            byte[] buffer = new byte[4096];
            long time = System.currentTimeMillis();
            for (int i = 0; i < splits; i++) {
                if (this.stop) {
                    return false;
                }
                StringBuffer sb = new StringBuffer();
                sb.append(String.valueOf(UPLOAD_URL) + (i + 1) + "/" + splits);
                sb.append("&time=");
                sb.append(time);
                HttpURLConnection conn = (HttpURLConnection) new URL(sb.toString()).openConnection();
                conn.setRequestProperty("Content-Type", "application/octet-stream;charset=utf-8");
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setUseCaches(false);
                conn.setConnectTimeout(10000);
                conn.setReadTimeout(10000);
                conn.connect();
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                FileInputStream fileInputStream = new FileInputStream(file);
                fileInputStream.skip(50000 * ((long) i));
                OutputStream out = conn.getOutputStream();
                int sendSize = 50000;
                while (true) {
                    int read = fileInputStream.read(buffer);
                    if (read > 0) {
                        if (!this.stop) {
                            if (read <= sendSize) {
                                bos.write(buffer, 0, read);
                                sendSize -= read;
                                continue;
                            } else {
                                bos.write(buffer, 0, sendSize);
                                sendSize = 0;
                                continue;
                            }
                            if (sendSize == 0) {
                                break;
                            }
                        } else {
                            return false;
                        }
                    } else {
                        break;
                    }
                }
                out.write(bos.toByteArray());
                out.flush();
                out.close();
                fileInputStream.close();
                if (!this.stop) {
                    conn.getResponseCode();
                }
                conn.disconnect();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (SocketTimeoutException e2) {
            e2.printStackTrace();
        } catch (IOException e3) {
            e3.printStackTrace();
        }
        if (this.stop) {
            return false;
        }
        return true;
    }

    public static String getUploadUrl(String responseUrl, Context context2) {
        StringBuffer sb = new StringBuffer();
        sb.append(responseUrl);
        sb.append("?");
        sb.append("biz_id=");
        sb.append("1");
        sb.append("&edition_id=1");
        sb.append("&uid=");
        sb.append(CommonMethod.getUID(context2));
        sb.append("&ext=apk");
        sb.append("&p=");
        return sb.toString();
    }
}
