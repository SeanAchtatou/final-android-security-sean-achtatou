package com.netqin.antivirus.net.appupdateservice;

import android.content.ContentValues;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Xml;
import com.netqin.antivirus.Log;
import com.netqin.antivirus.NqUtil;
import com.netqin.antivirus.Value;
import com.netqin.antivirus.appprotocol.XmlTagValue;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.net.HttpHandler;
import com.netqin.antivirus.net.NetReturnValue;
import com.netqin.antivirus.net.appupdateservice.request.AppUpdateReq;
import com.netqin.antivirus.net.appupdateservice.response.AppUpdateErrorHandler;
import com.netqin.antivirus.net.appupdateservice.response.AppUpdateHandler;
import com.netqin.antivirus.net.avservice.response.ResponseFilter;
import com.netqin.antivirus.net.avservice.response.SAXUserEndException;
import com.nqmobile.antivirus_ampro20.R;
import java.io.FileOutputStream;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;
import java.util.Vector;
import org.xml.sax.SAXException;

public class AppUpdateProcessor {
    /* access modifiers changed from: private */
    public boolean cancel = false;
    private Context context;
    /* access modifiers changed from: private */
    public HttpHandler httpHandler;
    private boolean next = false;
    private int zcommand;
    private boolean zwait = false;

    public AppUpdateProcessor(Context context2) {
        this.context = context2;
        this.httpHandler = new HttpHandler();
    }

    private synchronized int process(String info, int command, ContentValues content, Handler handler) {
        int sendSAXExceptionMessage;
        ResponseFilter filter = new ResponseFilter();
        try {
            CommonMethod.logDebug("netqin", "AppUpdate(Response):\n" + info);
            Xml.parse(info, filter);
        } catch (SAXException e) {
            if (!(e instanceof SAXUserEndException)) {
                sendSAXExceptionMessage = sendSAXExceptionMessage(handler, 1, info);
            }
        }
        switch (filter.getCommand()) {
            case 1:
                try {
                    Xml.parse(info, new AppUpdateErrorHandler(content));
                    sendMessage(content, handler, 1);
                    sendSAXExceptionMessage = 10;
                    break;
                } catch (SAXException e2) {
                    SAXException sAXException = e2;
                    sendSAXExceptionMessage = sendSAXExceptionMessage(handler, 1, info);
                    break;
                }
            case 6:
                if (command != 20010) {
                    try {
                        Xml.parse(processInvalidURL(info), new AppUpdateHandler(content));
                        if (!content.containsKey("Prompt")) {
                            sendSAXExceptionMessage = downloadApp(content, handler);
                            break;
                        } else {
                            sendSoftwareUpdateMessage(content, handler, command);
                            if (zwait() != 1) {
                                sendSAXExceptionMessage = 16;
                                break;
                            } else {
                                sendSAXExceptionMessage = downloadApp(content, handler);
                                break;
                            }
                        }
                    } catch (SAXException e3) {
                        SAXException sAXException2 = e3;
                        sendSAXExceptionMessage = sendSAXExceptionMessage(handler, 6, info);
                        break;
                    }
                } else {
                    try {
                        Xml.parse(processInvalidURL(info), new AppUpdateHandler(content));
                        sendSAXExceptionMessage = NetReturnValue.AppNeedUpdate;
                        break;
                    } catch (SAXException e4) {
                        sendSAXExceptionMessage = sendSAXExceptionMessage(handler, 6, info);
                        break;
                    }
                }
            default:
                sendSAXExceptionMessage = 14;
                break;
        }
        return sendSAXExceptionMessage;
    }

    private int downloadFile(ContentValues content, Handler handler) {
        int appUpdateFileLength = 0;
        int downloadedLength = 0;
        int downloadLength = 10240;
        boolean downloadFinish = false;
        boolean downloadSuccess = false;
        if (content.containsKey(Value.AppUpdateFileLength)) {
            appUpdateFileLength = Integer.valueOf(content.getAsString(Value.AppUpdateFileLength)).intValue();
            Log.d("totalLength", new StringBuilder().append(appUpdateFileLength).toString());
        }
        int downloadStart = 0;
        int downloadPacketFailTimes = 0;
        if (handler != null) {
            handler.sendMessage(handler.obtainMessage(400, 6, 2, null));
        }
        if (content.containsKey(Value.AppUpdateSrc)) {
            try {
                FileOutputStream fos = this.context.openFileOutput(content.getAsString(Value.AppUpdateFileName), 1);
                URL url = new URL(content.getAsString(Value.AppUpdateSrc));
                if (appUpdateFileLength < 10240) {
                    downloadLength = appUpdateFileLength;
                }
                while (!downloadFinish) {
                    String range = " bytes=" + downloadStart + "-" + ((downloadStart + downloadLength) - 1);
                    Log.d("Range", range);
                    byte[] res = downloadPacket(url, range);
                    Log.d("Res length ", new StringBuilder().append(res.length).toString());
                    if (res == null) {
                        downloadPacketFailTimes++;
                        if (downloadPacketFailTimes > 3) {
                            downloadFinish = true;
                            downloadSuccess = false;
                        }
                    } else if (res.length == downloadLength) {
                        downloadStart += downloadLength;
                        downloadPacketFailTimes = 0;
                        try {
                            fos.write(res);
                            fos.flush();
                            downloadedLength += downloadLength;
                            if (downloadedLength == appUpdateFileLength) {
                                Log.d("Finish", "finish");
                                fos.close();
                                downloadFinish = true;
                                downloadSuccess = true;
                            } else if (downloadedLength < appUpdateFileLength && appUpdateFileLength - downloadedLength < downloadLength) {
                                downloadLength = appUpdateFileLength - downloadedLength;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        downloadPacketFailTimes++;
                        if (downloadPacketFailTimes > 3) {
                            downloadFinish = true;
                            downloadSuccess = false;
                        }
                    }
                }
                if (downloadFinish && downloadSuccess) {
                    content.put(Value.AppUpdateSuccess, "1");
                }
            } catch (Exception e2) {
                return 8;
            }
        }
        return downloadPacketFailTimes;
    }

    private byte[] downloadPacket(URL url, String range) {
        this.httpHandler.downloadPackt(url, range);
        return this.httpHandler.getResponsebytes();
    }

    private void sendSoftwareUpdateMessage(ContentValues content, Handler handler, int command) {
        Value.SoftUpdateMsg updateMsg = new Value.SoftUpdateMsg();
        if (content.containsKey("Prompt")) {
            updateMsg.promptMsg = content.getAsString("Prompt");
        }
        if (handler != null) {
            Log.d("message", "send Message");
            Message msg = new Message();
            msg.what = 12;
            msg.arg1 = 31;
            msg.obj = updateMsg;
            handler.sendMessage(msg);
        }
    }

    private int downloadApp(ContentValues content, Handler handler) {
        final Handler hdr = handler;
        final int total = Integer.valueOf(content.getAsString(Value.AppUpdateFileLength)).intValue();
        if (handler != null) {
            Message msg = new Message();
            msg.what = 12;
            msg.arg1 = 21;
            msg.arg2 = total;
            handler.sendMessage(msg);
        }
        if (!content.containsKey(Value.AppUpdateSrc)) {
            return 8;
        }
        try {
            int downloadFile = this.httpHandler.downloadFile(new URL(content.getAsString(Value.AppUpdateSrc)));
            new Thread(new Runnable() {
                public void run() {
                    int current = 0;
                    while (!AppUpdateProcessor.this.cancel && current <= total && !AppUpdateProcessor.this.httpHandler.isFinish()) {
                        current = AppUpdateProcessor.this.httpHandler.getCurrentResponseLength();
                        if (hdr != null) {
                            Message msg = new Message();
                            msg.what = 12;
                            msg.arg1 = 29;
                            if (current < total) {
                                msg.arg2 = current;
                            } else {
                                msg.arg2 = total;
                            }
                            hdr.sendMessage(msg);
                        }
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }).start();
            byte[] resposne = this.httpHandler.getResponsebytes();
            if (resposne == null) {
                return sendSendReceiveErrorMessage(handler, 6);
            }
            try {
                FileOutputStream fos = this.context.openFileOutput(content.getAsString(Value.AppUpdateFileName), 1);
                fos.write(resposne);
                fos.flush();
                fos.close();
                content.put(Value.AppUpdateSuccess, "1");
                if (handler != null) {
                    Message msg2 = new Message();
                    msg2.what = 12;
                    msg2.arg1 = 22;
                    handler.sendMessage(msg2);
                }
                return 10;
            } catch (Exception e) {
                return sendFileStoreErrorMessage(handler, 6);
            }
        } catch (MalformedURLException e2) {
            return 8;
        }
    }

    public synchronized int process(int command, Handler handler, ContentValues content) {
        int processResult;
        Proxy proxy = NqUtil.getApnProxy(this.context);
        if (proxy != null) {
            this.httpHandler.setProxy(proxy);
        } else {
            this.httpHandler.NoProxy();
        }
        switch (command) {
            case 6:
                processResult = processResult(this.httpHandler.sendUpdateServerRequest(new AppUpdateReq(content, this.context).getRequestBytes()), 6, content, handler);
                break;
            case Value.BackgroundAppUpdateCheck:
                processResult = processResult(this.httpHandler.sendUpdateServerRequest(new AppUpdateReq(content, this.context).getRequestBytes()), Value.BackgroundAppUpdateCheck, content, handler);
                break;
            default:
                processResult = 15;
                break;
        }
        return processResult;
    }

    public int processResult(int result, int command, ContentValues content, Handler handler) {
        byte[] response = this.httpHandler.getResponsebytes();
        if (response == null) {
            return sendSendReceiveErrorMessage(handler, command);
        }
        return process(new String(response), command, content, handler);
    }

    public synchronized void next() {
        this.next = true;
        notify();
    }

    public synchronized void cancel() {
        this.next = false;
        notify();
    }

    private String processInvalidURL(String info) {
        StringBuilder sb = new StringBuilder();
        int startIndex = info.indexOf("src");
        sb.append(info.substring(0, info.indexOf("src")));
        boolean in = false;
        int i = startIndex;
        while (true) {
            if (i >= info.length()) {
                break;
            }
            if (info.charAt(i) == '\"') {
                if (!in) {
                    sb.append(info.charAt(i));
                    in = true;
                } else {
                    in = true;
                    if (1 != 0) {
                        sb.append(info.substring(i));
                        break;
                    }
                }
            } else if (info.charAt(i) != '&' || !in) {
                sb.append(info.charAt(i));
            } else {
                sb.append(String.valueOf(info.charAt(i)) + "amp;");
            }
            i++;
        }
        return sb.toString();
    }

    private int sendSAXExceptionMessage(Handler handler, int command, String parsedInfo) {
        if (handler == null) {
            return 16;
        }
        Vector<String> vec = new Vector<>();
        vec.add(this.context.getString(R.string.SEND_RECEIVE_ERROR));
        Message msg = new Message();
        msg.what = 12;
        msg.arg1 = 28;
        msg.obj = vec;
        handler.sendMessage(msg);
        return 16;
    }

    private int sendSendReceiveErrorMessage(Handler handler, int command) {
        if (handler == null || this.cancel) {
            return 8;
        }
        Vector<String> vec = new Vector<>();
        vec.add(this.context.getString(R.string.SEND_RECEIVE_ERROR));
        Message msg = new Message();
        msg.what = 12;
        msg.arg1 = 28;
        msg.obj = vec;
        handler.sendMessage(msg);
        return 8;
    }

    private int sendFileStoreErrorMessage(Handler handler, int command) {
        if (handler == null) {
            return 17;
        }
        Vector<String> vec = new Vector<>();
        vec.add(this.context.getString(R.string.FILE_STORE_ERROR));
        Message msg = new Message();
        msg.what = 12;
        msg.arg1 = 28;
        msg.obj = vec;
        handler.sendMessage(msg);
        return 17;
    }

    private void sendMessage(ContentValues content, Handler handler, int command) {
        if (content.containsKey(Value.MessageCount)) {
            int number = Integer.valueOf(content.getAsString(Value.MessageCount)).intValue();
            Vector<String> vec = new Vector<>();
            for (int i = 0; i < number; i++) {
                vec.add(content.getAsString(XmlTagValue.message + (i + 1)));
                content.remove(XmlTagValue.message + i);
            }
            if (vec.size() != 0 && handler != null) {
                Message msg = new Message();
                msg.what = 12;
                msg.arg1 = 28;
                msg.obj = vec;
                handler.sendMessage(msg);
            }
        }
    }

    public void setCancel(boolean cancel2) {
        this.cancel = cancel2;
        this.httpHandler.setCancel(cancel2);
    }

    public void setCommand(int command) {
        this.zcommand = command;
    }

    private synchronized int zwait() {
        int i;
        this.zwait = true;
        try {
            wait();
            this.zwait = false;
            i = this.next ? 1 : 0;
        } catch (InterruptedException e) {
            this.zwait = false;
            i = -1;
        }
        return i;
    }

    public boolean checkWait() {
        return this.zwait;
    }
}
