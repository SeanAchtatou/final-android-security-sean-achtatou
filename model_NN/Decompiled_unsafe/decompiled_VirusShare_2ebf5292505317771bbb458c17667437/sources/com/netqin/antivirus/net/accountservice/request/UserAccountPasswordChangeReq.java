package com.netqin.antivirus.net.accountservice.request;

import android.content.ContentValues;
import android.content.Context;
import com.netqin.antivirus.Value;
import com.netqin.antivirus.net.contactservice.request.Request;

public class UserAccountPasswordChangeReq extends Request {
    private ContentValues content;

    public UserAccountPasswordChangeReq(ContentValues content2, Context context) {
        super(content2, context);
        this.content = content2;
        super.setCommand("3");
    }

    public String getRequestXML() {
        addHeadString();
        addMobileInfo();
        addClientInfo();
        addServiceInfo();
        addProperties();
        this.requestBuffer.append(Value.XML_EndTag_Request);
        return this.requestBuffer.toString();
    }

    private void addProperties() {
        this.requestBuffer.append("<Properties>\n\t\t<Property name=\"username\">");
        if (this.content.containsKey(Value.Username)) {
            this.requestBuffer.append(this.content.getAsString(Value.Username));
        }
        this.requestBuffer.append("</Property>\n\t\t<Property name=\"password\">");
        if (this.content.containsKey(Value.Password)) {
            this.requestBuffer.append(this.content.getAsString(Value.Password));
        }
        this.requestBuffer.append("</Property>\n\t\t<Property name=\"newPassword\">");
        if (this.content.containsKey(Value.NewPassword)) {
            this.requestBuffer.append(this.content.getAsString(Value.NewPassword));
        }
        this.requestBuffer.append("</Property>\n\t");
        this.requestBuffer.append("</Properties>\n");
    }

    public byte[] getRequestBytes() {
        return getRequestXML().getBytes();
    }
}
