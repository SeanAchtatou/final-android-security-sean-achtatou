package com.netqin.antivirus.net;

import com.netqin.antivirus.Value;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;

public class DataHandler {
    private boolean cancel;
    private String content_type = "application/x-www-form-urlencoded";
    private int currentResponseLength = 0;
    private boolean finish = false;
    private Proxy proxy = null;
    private String range = "";
    private String refer = "";
    private String requestMethod = "POST";
    private byte[] requestbytes;
    private byte[] responsebytes;
    private boolean success = false;
    private URL url;
    private boolean useProxy = false;

    public void setContent_type(String contentType) {
        this.content_type = contentType;
    }

    public void setRequestMethos(String rMethod) {
        this.requestMethod = rMethod;
    }

    public synchronized byte[] getResponsebytes() {
        byte[] bArr;
        while (!this.finish) {
            try {
                wait();
            } catch (InterruptedException e) {
                bArr = null;
            }
        }
        if (this.cancel || !this.success || this.responsebytes == null || this.responsebytes.length <= 0) {
            bArr = null;
        } else {
            bArr = this.responsebytes;
        }
        return bArr;
    }

    public void setCancel(boolean cancel2) {
        this.cancel = cancel2;
    }

    public void NoProxy() {
        this.proxy = null;
        this.useProxy = false;
    }

    public void setUrl(String urlString) {
        URL url2 = null;
        try {
            url2 = new URL(urlString);
        } catch (MalformedURLException e) {
        }
        this.url = url2;
    }

    public void setProxy(Proxy proxy2) {
        this.useProxy = true;
        this.proxy = proxy2;
    }

    public void setRefer(String refer2) {
        this.refer = refer2;
    }

    public void reset() {
        this.finish = false;
        this.success = false;
        this.cancel = false;
    }

    public void setRequestbytes(byte[] requestbytes2) {
        this.requestbytes = requestbytes2;
    }

    public void setUrl(URL url2) {
        this.url = url2;
    }

    public void setRange(String range2) {
        this.range = range2;
    }

    public int getCurrentResponseLength() {
        return this.currentResponseLength;
    }

    public boolean isFinish() {
        return this.finish;
    }

    public synchronized void processData() {
        HttpURLConnection httpCon = null;
        this.currentResponseLength = 0;
        this.responsebytes = null;
        try {
            httpCon = openHttpURLConnection();
            sendDataToTheServer(httpCon);
            this.responsebytes = getDataFromTheServer(httpCon);
            this.success = true;
            this.finish = true;
            if (httpCon != null) {
                httpCon.disconnect();
            }
            notify();
        } catch (Exception e) {
            this.success = false;
            this.finish = true;
            if (httpCon != null) {
                httpCon.disconnect();
            }
            notify();
        } catch (Throwable th) {
            this.finish = true;
            if (httpCon != null) {
                httpCon.disconnect();
            }
            notify();
            throw th;
        }
    }

    private HttpURLConnection openHttpURLConnection() throws IOException {
        HttpURLConnection httpCon;
        if (this.useProxy) {
            httpCon = (HttpURLConnection) this.url.openConnection(this.proxy);
        } else {
            httpCon = (HttpURLConnection) this.url.openConnection();
        }
        initHttpRequestHeader(httpCon);
        httpCon.setRequestMethod(this.requestMethod);
        return httpCon;
    }

    private void initHttpRequestHeader(HttpURLConnection httpCon) {
        httpCon.setDoOutput(true);
        httpCon.setDoInput(true);
        httpCon.setUseCaches(false);
        httpCon.setInstanceFollowRedirects(true);
        httpCon.setRequestProperty("Accept", "*/*");
        httpCon.setRequestProperty("Server", "NetQin");
        httpCon.setConnectTimeout(Value.httpConnectionConnectTimeout);
        httpCon.setReadTimeout(Value.httpConnectionReadTimeout);
        httpCon.setRequestProperty("Content-Type", this.content_type);
        if (!this.range.equals("")) {
            httpCon.setRequestProperty("Range", this.range);
        }
        if (!this.refer.equals("")) {
            httpCon.setRequestProperty(Value.Referer, this.refer);
        }
    }

    private byte[] getDataFromTheServer(HttpURLConnection httpCon) throws IOException {
        if (this.cancel) {
            return null;
        }
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        InputStream is = new BufferedInputStream(httpCon.getInputStream());
        byte[] buffer = new byte[1024];
        while (true) {
            int bytesRead = is.read(buffer, 0, 1024);
            if (bytesRead != -1) {
                this.currentResponseLength += bytesRead;
                if (this.cancel) {
                    break;
                }
                bao.write(buffer, 0, bytesRead);
            } else {
                break;
            }
        }
        is.close();
        return bao.toByteArray();
    }

    private void sendDataToTheServer(HttpURLConnection httpCon) throws IOException {
        if (this.requestbytes != null && !this.cancel) {
            OutputStream os = httpCon.getOutputStream();
            ByteArrayInputStream bin = new ByteArrayInputStream(this.requestbytes);
            byte[] buffer = new byte[1024];
            while (true) {
                int bytesRead = bin.read(buffer, 0, 1024);
                if (bytesRead != -1 && !this.cancel) {
                    os.write(buffer, 0, bytesRead);
                    os.flush();
                }
            }
            bin.close();
            os.close();
        }
    }
}
