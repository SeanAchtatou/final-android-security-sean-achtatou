package com.netqin.antivirus.net.accountservice;

import android.content.ContentValues;
import android.content.Context;
import android.os.Handler;

public class AccountService {
    private static AccountService service = null;
    private Context context;
    private AccountProcessor processor;

    private AccountService(Context context2) {
        this.context = context2;
    }

    public static synchronized AccountService getInstance(Context context2) {
        AccountService accountService;
        synchronized (AccountService.class) {
            if (service == null) {
                service = new AccountService(context2);
            }
            accountService = service;
        }
        return accountService;
    }

    public void cancel() {
        if (this.processor.checkWait()) {
            userCancel();
        } else {
            this.processor.setCancel(true);
        }
    }

    public synchronized int request(Handler handler, ContentValues content, int command) {
        int result;
        this.processor = new AccountProcessor(this.context);
        this.processor.setCommand(command);
        this.processor.setCancel(false);
        result = this.processor.process(command, handler, content);
        this.processor = null;
        return result;
    }

    public void next() {
        this.processor.next();
    }

    public void userCancel() {
        this.processor.cancel();
    }
}
