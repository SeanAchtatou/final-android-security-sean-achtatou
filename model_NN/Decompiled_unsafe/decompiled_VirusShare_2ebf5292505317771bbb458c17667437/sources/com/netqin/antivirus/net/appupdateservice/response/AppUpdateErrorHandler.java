package com.netqin.antivirus.net.appupdateservice.response;

import android.content.ContentValues;
import com.netqin.antivirus.Value;
import com.netqin.antivirus.appprotocol.XmlTagValue;
import com.netqin.antivirus.cloud.model.xml.XmlUtils;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.ext.DefaultHandler2;

public class AppUpdateErrorHandler extends DefaultHandler2 {
    private StringBuffer buf;
    private ContentValues content;
    private int messageNumber = 0;

    public AppUpdateErrorHandler(ContentValues content2) {
        this.content = content2;
        this.buf = new StringBuffer();
    }

    public void endDocument() throws SAXException {
        this.content.put(Value.MessageCount, Integer.toString(this.messageNumber));
    }

    public void startDocument() throws SAXException {
    }

    public void endElement(String uri, String localName, String name) throws SAXException {
        if (localName.equals("Protocol")) {
            this.content.put("Protocol", this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals("Command")) {
            this.content.put("Command", this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals("SessionId")) {
            this.content.put("SessionId", this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals("Balance")) {
            this.content.put("Balance", this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals("UID")) {
            this.content.put("UID", this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals(Value.Level)) {
            this.content.put(Value.Level, this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals(Value.Status)) {
            this.content.put(Value.Status, this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals(Value.Expired)) {
            this.content.put(Value.Expired, this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals(Value.NextPayDay)) {
            this.content.put(Value.NextPayDay, this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals("LevelName")) {
            this.content.put("LevelName", this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals(Value.SecretSpaceUsable)) {
            this.content.put(Value.SecretSpaceUsable, this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals(Value.SmsFilterUsable)) {
            this.content.put(Value.SmsFilterUsable, this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals("ErrorCode")) {
            this.buf.setLength(0);
        } else if (localName.equals("AdtnUploadFreq")) {
            this.buf.setLength(0);
        } else if (localName.equals(XmlTagValue.message)) {
            this.messageNumber++;
            this.content.put(XmlTagValue.message + this.messageNumber, this.buf.toString().trim());
            this.buf.setLength(0);
        }
    }

    public void startElement(String uri, String localName, String name, Attributes attributes) throws SAXException {
        if (localName.equals(XmlUtils.LABEL_REPLY)) {
            return;
        }
        if (localName.equals("Protocol")) {
            this.buf.setLength(0);
        } else if (localName.equals("Command")) {
            this.buf.setLength(0);
        } else if (localName.equals("SessionId")) {
            this.buf.setLength(0);
        } else if (localName.equals("Balance")) {
            this.buf.setLength(0);
        } else if (localName.equals("UID")) {
            this.buf.setLength(0);
        } else if (localName.equals(Value.Level)) {
            this.buf.setLength(0);
        } else if (localName.equals(Value.Status)) {
            this.buf.setLength(0);
        } else if (localName.equals(Value.Expired)) {
            this.buf.setLength(0);
        } else if (localName.equals(Value.NextPayDay)) {
            this.buf.setLength(0);
        } else if (localName.equals("LevelName")) {
            this.buf.setLength(0);
        } else if (localName.equals(Value.SecretSpaceUsable)) {
            this.buf.setLength(0);
        } else if (localName.equals(Value.SmsFilterUsable)) {
            this.buf.setLength(0);
        } else if (localName.equals("AuthInfo")) {
            this.buf.setLength(0);
        } else if (localName.equals("ErrorInfo")) {
            this.buf.setLength(0);
        } else if (localName.equals("SystemMsg")) {
            this.buf.setLength(0);
        } else if (localName.equals("SystemMessages")) {
            this.buf.setLength(0);
        } else if (localName.equals(XmlTagValue.message)) {
            this.buf.setLength(0);
        }
    }

    public void characters(char[] ch, int start, int length) throws SAXException {
        this.buf.append(ch, start, length);
    }
}
