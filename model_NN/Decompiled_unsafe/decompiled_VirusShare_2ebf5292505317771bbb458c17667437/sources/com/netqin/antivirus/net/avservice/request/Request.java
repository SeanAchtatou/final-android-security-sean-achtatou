package com.netqin.antivirus.net.avservice.request;

import android.content.ContentValues;
import android.content.Context;
import android.text.TextUtils;
import com.netqin.antivirus.Preferences;
import com.netqin.antivirus.Security;
import com.netqin.antivirus.Value;
import com.netqin.antivirus.appprotocol.AppValue;
import com.netqin.antivirus.appprotocol.XmlTagValue;
import com.netqin.antivirus.common.CommonMethod;

public class Request {
    private AppValue appValue;
    private int commandId;
    private ContentValues content;
    private Context context;
    public StringBuffer requestBuffer = new StringBuffer();

    public Request(ContentValues content2, Context context2, AppValue value) {
        this.content = content2;
        this.context = context2;
        this.appValue = value;
    }

    public Request(Context context2) {
        this.context = context2;
    }

    public byte[] getRequestBytes(int cmdId) {
        this.commandId = cmdId;
        return Security.ecrypt(getRequestXML().getBytes());
    }

    private String getRequestXML() {
        addHeadString();
        addMobileInfo();
        addClientInfo();
        addServiceInfo();
        addChargesInfo();
        addSpecificInfo();
        addEndString();
        CommonMethod.logDebug("AVService", this.requestBuffer.toString());
        return this.requestBuffer.toString();
    }

    private void addHeadString() {
        this.requestBuffer.append(Value.XML_HEAD);
        this.requestBuffer.append("<Request>\n");
        this.requestBuffer.append("\t<Protocol>");
        this.requestBuffer.append(Value.AppProtocolVersion);
        this.requestBuffer.append("</Protocol>\n\t<Command>");
        this.requestBuffer.append(this.commandId);
        this.requestBuffer.append("</Command>\n\t<IsMandatory>");
        if (this.appValue.isMandatory) {
            this.requestBuffer.append("Y");
        } else {
            this.requestBuffer.append("N");
        }
        this.requestBuffer.append("</IsMandatory>\n\t<SessionInfo>");
        if (!TextUtils.isEmpty(this.appValue.sessionInfo)) {
            this.requestBuffer.append(this.appValue.sessionInfo);
        }
        this.requestBuffer.append("</SessionInfo>\n");
    }

    private void addEndString() {
        this.requestBuffer.append(Value.XML_EndTag_Request);
    }

    private void addMobileInfo() {
        this.requestBuffer.append("\t<MobileInfo>\n");
        this.requestBuffer.append("\t\t<Model>");
        this.requestBuffer.append(Value.Model);
        this.requestBuffer.append("</Model>\n");
        this.requestBuffer.append("\t\t<Language>");
        this.requestBuffer.append(CommonMethod.getPlatformLanguage());
        this.requestBuffer.append("</Language>\n");
        this.requestBuffer.append("\t\t<Country>");
        this.requestBuffer.append(Value.Country);
        this.requestBuffer.append("</Country>\n");
        this.requestBuffer.append("\t\t<IMEI>");
        if (this.content.getAsString("IMEI") != null) {
            this.requestBuffer.append(this.content.getAsString("IMEI"));
        }
        this.requestBuffer.append("</IMEI>\n");
        this.requestBuffer.append("\t\t<IMSI>");
        if (this.content.getAsString("IMSI") != null) {
            this.requestBuffer.append(this.content.getAsString("IMSI"));
        }
        this.requestBuffer.append("</IMSI>\n");
        this.requestBuffer.append("\t\t<SMSCenter>");
        if (!this.content.containsKey(Value.SC) || this.content.getAsString(Value.SC).equals("")) {
            this.requestBuffer.append("1");
        } else {
            this.requestBuffer.append(this.content.getAsString(Value.SC));
        }
        this.requestBuffer.append("</SMSCenter>\n");
        this.requestBuffer.append("\t\t<LAC>");
        if (!this.content.containsKey(Value.LAC) || this.content.getAsString(Value.LAC).equals("")) {
            this.requestBuffer.append("1");
        } else {
            this.requestBuffer.append(this.content.getAsString(Value.LAC));
        }
        this.requestBuffer.append("</LAC>\n");
        this.requestBuffer.append("\t\t<CellID>");
        if (!this.content.containsKey(Value.CellID) || this.content.getAsString(Value.CellID).equals("")) {
            this.requestBuffer.append("1");
        } else {
            this.requestBuffer.append(this.content.getAsString(Value.CellID));
        }
        this.requestBuffer.append("</CellID>\n");
        this.requestBuffer.append("\t\t<APN>");
        if (!this.content.containsKey(Value.APN) || this.content.getAsString(Value.APN).equals("")) {
            this.requestBuffer.append("");
        } else {
            this.requestBuffer.append(this.content.getAsString(Value.APN));
        }
        this.requestBuffer.append("</APN>\n");
        this.requestBuffer.append("\t\t<Timezone>");
        this.requestBuffer.append(Value.TIMEZONE);
        this.requestBuffer.append("</Timezone>\n");
        this.requestBuffer.append("\t</MobileInfo>\n");
    }

    private void addClientInfo() {
        this.requestBuffer.append("\t<ClientInfo>\n");
        this.requestBuffer.append("\t\t<SoftLanguage>");
        this.requestBuffer.append(CommonMethod.getPlatformLanguage());
        this.requestBuffer.append("</SoftLanguage>\n");
        this.requestBuffer.append("\t\t<PlatformID>");
        this.requestBuffer.append("351");
        this.requestBuffer.append("</PlatformID>\n");
        this.requestBuffer.append("\t\t<EditionID>");
        this.requestBuffer.append(Value.EditionID);
        this.requestBuffer.append("</EditionID>\n");
        this.requestBuffer.append("\t\t<SubCoopID>");
        this.requestBuffer.append(Preferences.getPreferences(this.context).getChanelIdStore());
        this.requestBuffer.append("</SubCoopID>\n");
        this.requestBuffer.append("\t</ClientInfo>\n");
    }

    private void addServiceInfo() {
        this.requestBuffer.append("\t<ServiceInfo>\n");
        this.requestBuffer.append("\t\t<UID>");
        if (this.content.containsKey("UID")) {
            this.requestBuffer.append(this.content.getAsString("UID"));
        }
        this.requestBuffer.append("</UID>\n");
        this.requestBuffer.append("\t\t<Business>");
        this.requestBuffer.append("101");
        this.requestBuffer.append("</Business>\n");
        this.requestBuffer.append("\t\t<LocalVirusVersion>");
        if (this.content.containsKey(XmlTagValue.localVirusVersion)) {
            this.requestBuffer.append(this.content.getAsString(XmlTagValue.localVirusVersion));
        }
        this.requestBuffer.append("</LocalVirusVersion>\n");
        this.requestBuffer.append("\t\t<ClientScene>");
        if (this.content.containsKey(XmlTagValue.clientScene)) {
            this.requestBuffer.append(this.content.getAsString(XmlTagValue.clientScene));
        } else {
            this.requestBuffer.append(0);
        }
        this.requestBuffer.append("</ClientScene>\n");
        this.requestBuffer.append("\t</ServiceInfo>\n");
    }

    private void addChargesInfo() {
        this.requestBuffer.append("\t<ChargesInfo>\n");
        this.requestBuffer.append("\t\t<SelectedCharges>");
        if (!TextUtils.isEmpty(this.appValue.selectedCharge)) {
            this.requestBuffer.append(this.appValue.selectedCharge);
        }
        this.requestBuffer.append("</SelectedCharges>\n");
        this.requestBuffer.append("\t\t<PaymentResult>");
        if (this.appValue.paymentResult > 0) {
            this.requestBuffer.append(this.appValue.paymentResult);
        }
        this.requestBuffer.append("</PaymentResult>\n");
        this.requestBuffer.append("\t\t<ChargeId>");
        if (!TextUtils.isEmpty(this.appValue.chargeId)) {
            this.requestBuffer.append(this.appValue.chargeId);
        }
        this.requestBuffer.append("</ChargeId>\n");
        this.requestBuffer.append("\t</ChargesInfo>\n");
    }

    private void addAdvertInfo() {
        this.requestBuffer.append("<AdvertInfos>");
        this.requestBuffer.append("</AdvertInfos>");
    }

    private void addSpecificInfo() {
        this.requestBuffer.append("\t<SpecificInfo>\n");
        switch (this.commandId) {
            case 2:
                addCardChargeSpecificInfo();
                break;
            case 11:
                addCheckAVDBSpecificInfo();
                break;
            case 12:
                addUpdateAVDBSpecificInfo();
                break;
            case 13:
                addDownloadAVDBSpecificInfo();
                break;
            case 16:
                addPeriodicalConnectSpecificInfo();
                break;
            case 18:
                addUninstallConnectSpecificInfo();
                break;
            case 19:
                addRegisterSpecificInfo();
                break;
            case 22:
                addUpdateFreeDataSpecificInfo();
                break;
            case 27:
                addUpdateSpecificKillerSpecificInfo();
                break;
        }
        this.requestBuffer.append("\t</SpecificInfo>\n");
    }

    private void addRegisterSpecificInfo() {
        this.requestBuffer.append("\t\t<AdditionInfo>");
        this.requestBuffer.append("</AdditionInfo>\n");
        this.requestBuffer.append("\t\t<LogInfo>");
        this.requestBuffer.append("</LogInfo>\n");
        this.requestBuffer.append("\t\t<CardNumber>");
        this.requestBuffer.append("</CardNumber>\n");
    }

    private void addCardChargeSpecificInfo() {
        this.requestBuffer.append("\t\t<CardNumber>");
        this.requestBuffer.append("</CardNumber>\n");
    }

    private void addUpdateFreeDataSpecificInfo() {
        this.requestBuffer.append("\t\t<IsBackground>");
        if (!this.content.containsKey(XmlTagValue.isBackground)) {
            this.requestBuffer.append("N");
        } else if (this.content.getAsBoolean(XmlTagValue.isBackground).booleanValue()) {
            this.requestBuffer.append("Y");
        } else {
            this.requestBuffer.append("N");
        }
        this.requestBuffer.append("</IsBackground>\n");
        this.requestBuffer.append("\t\t<MaliciousSmsVersion>");
        this.requestBuffer.append("</MaliciousSmsVersion>\n");
        this.requestBuffer.append("\t\t<FirewallVersion>");
        this.requestBuffer.append("</FirewallVersion>\n");
        this.requestBuffer.append("\t\t<ProcessVersion>");
        this.requestBuffer.append("</ProcessVersion>\n");
        this.requestBuffer.append("\t\t<MiscDataVersion>");
        this.requestBuffer.append("</MiscDataVersion>\n");
        this.requestBuffer.append("\t\t<TrojanDataVersion>");
        this.requestBuffer.append("</TrojanDataVersion>\n");
        this.requestBuffer.append("\t\t<DeductFeeDataVersion>");
        this.requestBuffer.append("</DeductFeeDataVersion>\n");
    }

    private void addCheckAVDBSpecificInfo() {
        this.requestBuffer.append("\t\t<IsBackground>");
        if (!this.content.containsKey(XmlTagValue.isBackground)) {
            this.requestBuffer.append("N");
        } else if (this.content.getAsBoolean(XmlTagValue.isBackground).booleanValue()) {
            this.requestBuffer.append("Y");
        } else {
            this.requestBuffer.append("N");
        }
        this.requestBuffer.append("</IsBackground>\n");
    }

    private void addUpdateAVDBSpecificInfo() {
        this.requestBuffer.append("\t\t<IsBackground>");
        if (!this.content.containsKey(XmlTagValue.isBackground)) {
            this.requestBuffer.append("N");
        } else if (this.content.getAsBoolean(XmlTagValue.isBackground).booleanValue()) {
            this.requestBuffer.append("Y");
        } else {
            this.requestBuffer.append("N");
        }
        this.requestBuffer.append("</IsBackground>\n");
        this.requestBuffer.append("\t\t<TargetVirusVersion>");
        if (this.content.containsKey(XmlTagValue.targetVirusVersion)) {
            this.requestBuffer.append(this.content.getAsString(XmlTagValue.targetVirusVersion));
        }
        this.requestBuffer.append("</TargetVirusVersion>\n");
    }

    private void addDownloadAVDBSpecificInfo() {
        this.requestBuffer.append("\t\t<IsBackground>");
        if (!this.content.containsKey(XmlTagValue.isBackground)) {
            this.requestBuffer.append("N");
        } else if (this.content.getAsBoolean(XmlTagValue.isBackground).booleanValue()) {
            this.requestBuffer.append("Y");
        } else {
            this.requestBuffer.append("N");
        }
        this.requestBuffer.append("</IsBackground>\n");
        this.requestBuffer.append("\t\t<VirusFileName>");
        if (!TextUtils.isEmpty(this.appValue.downloadVirusName)) {
            this.requestBuffer.append(this.appValue.downloadVirusName);
        }
        this.requestBuffer.append("</VirusFileName>\n");
        this.requestBuffer.append("\t\t<IsLastFile>");
        this.requestBuffer.append("Y");
        this.requestBuffer.append("</IsLastFile>\n");
    }

    private void addUpdateSpecificKillerSpecificInfo() {
        this.requestBuffer.append("\t\t<IsBackground>");
        if (!this.content.containsKey(XmlTagValue.isBackground)) {
            this.requestBuffer.append("N");
        } else if (this.content.getAsBoolean(XmlTagValue.isBackground).booleanValue()) {
            this.requestBuffer.append("Y");
        } else {
            this.requestBuffer.append("N");
        }
        this.requestBuffer.append("</IsBackground>\n");
        this.requestBuffer.append("\t\t<InstalledApps>");
        this.requestBuffer.append("</InstalledApps>\n");
    }

    private void addPeriodicalConnectSpecificInfo() {
        this.requestBuffer.append("\t\t<IsBackground>");
        if (!this.content.containsKey(XmlTagValue.isBackground)) {
            this.requestBuffer.append("N");
        } else if (this.content.getAsBoolean(XmlTagValue.isBackground).booleanValue()) {
            this.requestBuffer.append("Y");
        } else {
            this.requestBuffer.append("N");
        }
        this.requestBuffer.append("</IsBackground>\n");
        this.requestBuffer.append("\t\t<MaliciousSmsVersion>");
        this.requestBuffer.append("</MaliciousSmsVersion>\n");
        this.requestBuffer.append("\t\t<FirewallVersion>");
        this.requestBuffer.append("</FirewallVersion>\n");
        this.requestBuffer.append("\t\t<ProcessVersion>");
        this.requestBuffer.append("</ProcessVersion>\n");
        this.requestBuffer.append("\t\t<MiscDateVersion>");
        this.requestBuffer.append("</MiscDateVersion>\n");
        this.requestBuffer.append("\t\t<TrojanDataVersion>");
        this.requestBuffer.append("</TrojanDataVersion>\n");
        this.requestBuffer.append("\t\t<AdditionInfo>");
        this.requestBuffer.append("</AdditionInfo>\n");
        this.requestBuffer.append("\t\t<LogInfo>");
        this.requestBuffer.append("</LogInfo>\n");
        this.requestBuffer.append("\t\t<FirewallInfo>");
        this.requestBuffer.append("</FirewallInfo>\n");
    }

    private void addUninstallConnectSpecificInfo() {
        this.requestBuffer.append("\t\t<AdditionInfo>");
        this.requestBuffer.append("</AdditionInfo>\n");
        this.requestBuffer.append("\t\t<LogInfo>");
        this.requestBuffer.append("</LogInfo>\n");
    }
}
