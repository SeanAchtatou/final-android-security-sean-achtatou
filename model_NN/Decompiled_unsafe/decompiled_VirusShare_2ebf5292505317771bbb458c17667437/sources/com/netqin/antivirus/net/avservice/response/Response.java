package com.netqin.antivirus.net.avservice.response;

import android.content.ContentValues;
import android.text.TextUtils;
import com.netqin.antivirus.appprotocol.AppValue;
import com.netqin.antivirus.appprotocol.ChargeOption;
import com.netqin.antivirus.appprotocol.DialogBoxForSubscribe;
import com.netqin.antivirus.appprotocol.SysMessage;
import com.netqin.antivirus.appprotocol.XmlTagValue;
import com.netqin.antivirus.payment.ZongPricePoint;
import java.util.ArrayList;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.ext.DefaultHandler2;

public class Response extends DefaultHandler2 {
    private AppValue appValue;
    private StringBuffer buf;
    private ContentValues content;
    private boolean isNewsBox = false;
    private boolean isWapCharge = false;

    public Response(ContentValues content2, AppValue value) {
        this.content = content2;
        this.appValue = value;
        this.buf = new StringBuffer();
    }

    public void endDocument() throws SAXException {
    }

    public void startDocument() throws SAXException {
    }

    public void startElement(String uri, String localName, String name, Attributes attributes) throws SAXException {
        this.buf.setLength(0);
        if (localName.equals(XmlTagValue.memberInfo)) {
            attributes.getValue(XmlTagValue.isMember);
            attributes.getValue("score");
            attributes.getValue(XmlTagValue.increaseSpeed);
            attributes.getValue("ExpiredTime");
            String str = attributes.getValue(XmlTagValue.isMember);
            if (!TextUtils.isEmpty(str)) {
                this.content.put(XmlTagValue.isMember, str);
            }
            String str2 = attributes.getValue("score");
            if (!TextUtils.isEmpty(str2)) {
                this.content.put("score", str2);
            }
            String str3 = attributes.getValue(XmlTagValue.increaseSpeed);
            if (!TextUtils.isEmpty(str3)) {
                this.content.put(XmlTagValue.increaseSpeed, str3);
            }
            String str4 = attributes.getValue("ExpiredTime");
            if (!TextUtils.isEmpty(str4)) {
                this.content.put("ExpiredTime", str4);
            }
        } else if (localName.equals(XmlTagValue.virusForecast)) {
            String str5 = attributes.getValue("name");
            if (!TextUtils.isEmpty(str5)) {
                this.content.put(XmlTagValue.virusForecastName, str5);
            }
            String str6 = attributes.getValue(XmlTagValue.level);
            if (!TextUtils.isEmpty(str6)) {
                this.content.put(XmlTagValue.level, str6);
            }
            String str7 = attributes.getValue(XmlTagValue.levelValue);
            if (!TextUtils.isEmpty(str7)) {
                this.content.put(XmlTagValue.levelValue, str7);
            }
        } else if (localName.equals(XmlTagValue.paymentCentreUrl)) {
            String str8 = attributes.getValue(XmlTagValue.openType);
            if (!TextUtils.isEmpty(str8)) {
                this.content.put(XmlTagValue.openType, str8);
            }
        } else if (localName.equals(XmlTagValue.nextStepCmdYES)) {
            String str9 = attributes.getValue(XmlTagValue.times);
            if (!TextUtils.isEmpty(str9)) {
                this.content.put(XmlTagValue.times, str9);
            }
        } else if (localName.equals(XmlTagValue.smsCharge) || localName.equals(XmlTagValue.wapCharge) || localName.equals(XmlTagValue.zongWapCharge) || localName.equals(XmlTagValue.zongSDKCharge)) {
            if (localName.equals(XmlTagValue.wapCharge)) {
                this.isWapCharge = true;
            }
            String str10 = attributes.getValue(XmlTagValue.chargeId);
            if (!TextUtils.isEmpty(str10)) {
                this.content.put(XmlTagValue.chargeId, str10);
            }
        } else if (localName.equals(XmlTagValue.paymentUrl)) {
            String str11 = attributes.getValue(XmlTagValue.openType);
            if (!TextUtils.isEmpty(str11)) {
                this.content.put(XmlTagValue.openType, str11);
            }
        } else if (localName.equals(XmlTagValue.monitotUrls)) {
        } else {
            if (localName.equals(XmlTagValue.notification)) {
                String str12 = attributes.getValue("id");
                if (!TextUtils.isEmpty(str12)) {
                    this.content.put("id", str12);
                }
                String str13 = attributes.getValue(XmlTagValue.button);
                if (!TextUtils.isEmpty(str13)) {
                    this.content.put(XmlTagValue.button, str13);
                }
                String str14 = attributes.getValue(XmlTagValue.event);
                if (!TextUtils.isEmpty(str14)) {
                    this.content.put(XmlTagValue.event, str14);
                }
                String str15 = attributes.getValue(XmlTagValue.url);
                if (!TextUtils.isEmpty(str15)) {
                    this.content.put(XmlTagValue.url, str15);
                }
            } else if (localName.equals(XmlTagValue.regInfo)) {
                String str16 = attributes.getValue("type");
                if (!TextUtils.isEmpty(str16)) {
                    if (str16.equalsIgnoreCase("wap")) {
                        this.isWapCharge = true;
                    }
                    this.content.put("type", str16);
                }
            } else if (localName.equalsIgnoreCase(XmlTagValue.option)) {
                String str17 = attributes.getValue("id");
                if (!TextUtils.isEmpty(str17)) {
                    ChargeOption option = new ChargeOption();
                    option.id = str17;
                    if (this.appValue.multiChargeOptionList == null) {
                        this.appValue.multiChargeOptionList = new ArrayList();
                    }
                    this.appValue.multiChargeOptionList.add(option);
                }
            } else if (localName.equals(XmlTagValue.message)) {
                String value = attributes.getValue("type");
            } else if (localName.equals(XmlTagValue.localVirusFile)) {
                String str18 = attributes.getValue(XmlTagValue.version);
                if (!TextUtils.isEmpty(str18)) {
                    this.content.put(XmlTagValue.version, str18);
                }
                String str19 = attributes.getValue("name");
                if (!TextUtils.isEmpty(str19)) {
                    this.content.put(XmlTagValue.localVirusFileName, str19);
                }
                String str20 = attributes.getValue(XmlTagValue.size);
                if (!TextUtils.isEmpty(str20)) {
                    this.content.put(XmlTagValue.size, str20);
                }
            } else if (localName.equals(XmlTagValue.dialogBox)) {
                if (this.appValue.dialogBoxList == null) {
                    this.appValue.dialogBoxList = new ArrayList();
                }
                DialogBoxForSubscribe db = new DialogBoxForSubscribe();
                String str21 = attributes.getValue(XmlTagValue.postion);
                if (!TextUtils.isEmpty(str21)) {
                    db.postion = Integer.valueOf(str21).intValue();
                }
                String str22 = attributes.getValue(XmlTagValue.isShow);
                if (TextUtils.isEmpty(str22) || !str22.equalsIgnoreCase("Y")) {
                    db.isShow = false;
                } else {
                    db.isShow = true;
                }
                this.appValue.dialogBoxList.add(db);
            } else if (localName.equals(XmlTagValue.pricePoint)) {
                if (this.appValue.zongSDKChargePricePoint == null) {
                    this.appValue.zongSDKChargePricePoint = new ArrayList();
                }
            } else if (localName.equals(XmlTagValue.newsBox)) {
                String str23 = attributes.getValue(XmlTagValue.isShow);
                if (TextUtils.isEmpty(str23) || !str23.equalsIgnoreCase("Y")) {
                    this.isNewsBox = false;
                } else {
                    this.isNewsBox = true;
                }
                String str24 = attributes.getValue(XmlTagValue.postion);
                if (!TextUtils.isEmpty(str24)) {
                    this.appValue.newsBoxPosition = str24;
                }
            }
        }
    }

    public void endElement(String uri, String localName, String name) throws SAXException {
        if (localName.equals("Command") || localName.equals(XmlTagValue.sessionInfo) || localName.equals("UID") || localName.equals("UserType") || localName.equals("LevelName") || localName.equals(XmlTagValue.isRegistered) || localName.equals("Balance") || localName.equals("ExpiredTime") || localName.equals(XmlTagValue.isATSEnable) || localName.equals(XmlTagValue.isDeductEnable) || localName.equals(XmlTagValue.isCallingEnable) || localName.equals(XmlTagValue.isBankAccountEnable) || localName.equals(XmlTagValue.isAutoUpdateEnable) || localName.equals(XmlTagValue.nextPayTime) || localName.equals("NextConnectTime") || localName.equals("NextConnectInterval") || localName.equals("IsUninstallConnect") || localName.equals(XmlTagValue.isSoftWareExpired) || localName.equals(XmlTagValue.purchasedVirusVersion) || localName.equals(XmlTagValue.latestVirusVersion) || localName.equals(XmlTagValue.alias) || localName.equals(XmlTagValue.desc) || localName.equals(XmlTagValue.wapUrl) || localName.equals("OperationType") || localName.equalsIgnoreCase("Prompt") || localName.equals(XmlTagValue.reConfirmPrompt) || localName.equals(XmlTagValue.paymentCentreUrl) || localName.equals(XmlTagValue.nextStepCmdYES) || localName.equals(XmlTagValue.nextStepCmdNO) || localName.equals(XmlTagValue.isSendVisible) || localName.equals(XmlTagValue.number) || localName.equals(XmlTagValue.isNeedReConfirm) || localName.equals(XmlTagValue.isReceiveReconfirmVisible) || localName.equals(XmlTagValue.isSendReconfirmVisible) || localName.equals(XmlTagValue.reconfirmWaitTime) || localName.equals(XmlTagValue.reconfirmNumber) || localName.equals(XmlTagValue.reconfirmContent) || localName.equals(XmlTagValue.reconfirmMatch) || localName.equals(XmlTagValue.isClickVisible) || localName.equals(XmlTagValue.uRL) || localName.equals(XmlTagValue.internal) || localName.equals(XmlTagValue.rule) || localName.equals(XmlTagValue.postData) || localName.equals(XmlTagValue.isMsgVisible) || localName.equals(XmlTagValue.pageIndex) || localName.equals("ConfirmMatch") || localName.equals(XmlTagValue.successSign) || localName.equals(XmlTagValue.checkTime) || localName.equals(XmlTagValue.appName) || localName.equals(XmlTagValue.customerKey) || localName.equals(XmlTagValue.country) || localName.equals(XmlTagValue.currency) || localName.equals(XmlTagValue.transactionRef) || localName.equals("IsBalanceAdequate") || localName.equals(XmlTagValue.localVirusFile) || localName.equals(XmlTagValue.selectedCharge) || localName.equals("PaymentResult") || localName.equals(XmlTagValue.notification) || localName.equals(XmlTagValue.description) || localName.equals("ErrorCode") || localName.equals(XmlTagValue.isDataIgnore)) {
            if (!TextUtils.isEmpty(this.buf.toString().trim())) {
                this.content.put(localName, this.buf.toString().trim());
            }
            this.buf.setLength(0);
        } else if (localName.equals(XmlTagValue.Type)) {
            if (this.isWapCharge) {
                this.appValue.wapChargeType = this.buf.toString().trim();
            } else {
                this.content.put(XmlTagValue.Type, this.buf.toString().trim());
            }
            this.buf.setLength(0);
        } else if (localName.equals(XmlTagValue.count)) {
            this.content.put(XmlTagValue.count, this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals(XmlTagValue.url)) {
            this.content.put(XmlTagValue.url, this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals(XmlTagValue.paymentUrl)) {
            this.content.put(XmlTagValue.paymentUrl, this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equalsIgnoreCase(XmlTagValue.option)) {
            if (this.appValue.multiChargeOptionList == null) {
                this.appValue.multiChargeOptionList = new ArrayList();
            }
            int size = this.appValue.multiChargeOptionList.size();
            if (size > 0) {
                ChargeOption option = this.appValue.multiChargeOptionList.get(size - 1);
                option.desc = this.buf.toString().trim();
                if (option.desc.length() == 0) {
                    this.appValue.multiChargeOptionList.remove(size - 1);
                }
            }
            this.buf.setLength(0);
        } else if (localName.equals(XmlTagValue.message)) {
            if (this.appValue.tempMsgList == null) {
                this.appValue.tempMsgList = new ArrayList();
            }
            SysMessage msg = new SysMessage();
            msg.str = this.buf.toString().trim();
            this.appValue.tempMsgList.add(msg);
            this.buf.setLength(0);
        } else if (localName.equals(XmlTagValue.dialogBox)) {
            int size2 = this.appValue.dialogBoxList.size();
            this.appValue.dialogBoxList.get(size2 - 1).content = this.buf.toString().trim();
        } else if (localName.equals(XmlTagValue.purchaseKey) || localName.equals(XmlTagValue.exactPrice) || localName.equals(XmlTagValue.quantity) || localName.equals(XmlTagValue.itemDesc) || localName.equals(XmlTagValue.label)) {
            if (!TextUtils.isEmpty(this.buf.toString().trim())) {
                this.content.put(localName, this.buf.toString().trim());
            } else if (localName.equals(XmlTagValue.exactPrice) || localName.equals(XmlTagValue.quantity)) {
                this.content.put(localName, "0");
            } else {
                this.content.put(localName, "");
            }
        } else if (localName.equals(XmlTagValue.pricePoint)) {
            if (this.appValue.zongSDKChargePricePoint == null) {
                this.appValue.zongSDKChargePricePoint = new ArrayList();
            }
            if (this.content.containsKey(XmlTagValue.purchaseKey) && this.content.containsKey(XmlTagValue.exactPrice) && this.content.containsKey(XmlTagValue.quantity) && this.content.containsKey(XmlTagValue.itemDesc) && this.content.containsKey(XmlTagValue.label) && !TextUtils.isEmpty(this.content.getAsString(XmlTagValue.purchaseKey)) && !TextUtils.isEmpty(this.content.getAsString(XmlTagValue.itemDesc)) && !TextUtils.isEmpty(this.content.getAsString(XmlTagValue.label))) {
                this.appValue.zongSDKChargePricePoint.add(new ZongPricePoint(this.content.getAsString(XmlTagValue.purchaseKey), this.content.getAsFloat(XmlTagValue.exactPrice).floatValue(), this.content.getAsInteger(XmlTagValue.quantity).intValue(), this.content.getAsString(XmlTagValue.itemDesc), this.content.getAsString(XmlTagValue.label)));
            }
        } else if (localName.equals(XmlTagValue.content)) {
            if (TextUtils.isEmpty(this.buf.toString().trim())) {
                return;
            }
            if (this.isNewsBox) {
                this.appValue.newsBoxContent = this.buf.toString().trim();
                return;
            }
            this.content.put(localName, this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals(XmlTagValue.title) && !TextUtils.isEmpty(this.buf.toString().trim())) {
            if (this.isNewsBox) {
                this.appValue.newsBoxTitle = this.buf.toString().trim();
                return;
            }
            this.content.put(localName, this.buf.toString().trim());
            this.buf.setLength(0);
        }
    }

    public void characters(char[] ch, int start, int length) throws SAXException {
        this.buf.append(ch, start, length);
    }
}
