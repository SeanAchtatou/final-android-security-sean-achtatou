package com.netqin.antivirus.net.appupdateservice;

import android.content.ContentValues;
import android.content.Context;
import com.netqin.antivirus.Value;

public class BackgroundAppUpdateService {
    private static BackgroundAppUpdateService service = null;
    private Context context;
    private AppUpdateProcessor processor;

    private BackgroundAppUpdateService(Context context2) {
        this.context = context2;
    }

    public static synchronized BackgroundAppUpdateService getInstance(Context context2) {
        BackgroundAppUpdateService backgroundAppUpdateService;
        synchronized (BackgroundAppUpdateService.class) {
            if (service == null) {
                service = new BackgroundAppUpdateService(context2);
            }
            backgroundAppUpdateService = service;
        }
        return backgroundAppUpdateService;
    }

    public void cancel() {
        if (this.processor == null) {
            return;
        }
        if (this.processor.checkWait()) {
            userCancel();
        } else {
            this.processor.setCancel(true);
        }
    }

    public synchronized int request(ContentValues content) {
        int result;
        this.processor = new AppUpdateProcessor(this.context);
        this.processor.setCommand(Value.BackgroundAppUpdateCheck);
        this.processor.setCancel(false);
        result = this.processor.process(Value.BackgroundAppUpdateCheck, null, content);
        this.processor = null;
        return result;
    }

    public void next() {
        this.processor.next();
    }

    public void userCancel() {
        this.processor.cancel();
    }
}
