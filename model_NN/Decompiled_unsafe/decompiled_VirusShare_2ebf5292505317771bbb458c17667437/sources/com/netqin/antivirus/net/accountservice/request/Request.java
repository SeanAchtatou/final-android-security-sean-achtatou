package com.netqin.antivirus.net.accountservice.request;

import android.content.ContentValues;
import android.content.Context;
import com.netqin.antivirus.Preferences;
import com.netqin.antivirus.Value;
import com.netqin.antivirus.common.CommonMethod;

public class Request {
    private String command;
    private ContentValues content;
    private Context context;
    public StringBuffer requestBuffer = new StringBuffer();

    public void setCommand(String command2) {
        this.command = command2;
    }

    public Request(ContentValues content2, Context context2) {
        this.content = content2;
        this.context = context2;
    }

    public Request(Context context2) {
        this.context = context2;
    }

    public void addMobileInfo() {
        this.requestBuffer.append("<MobileInfo>\n\t\t<Model>");
        this.requestBuffer.append(Value.Model);
        this.requestBuffer.append("</Model>\n\t\t<Language>");
        this.requestBuffer.append(CommonMethod.getPlatformLanguage());
        this.requestBuffer.append("</Language>\n\t\t<Country>");
        this.requestBuffer.append(Value.Country);
        this.requestBuffer.append("</Country>\n\t\t<IMEI>");
        if (this.content.getAsString("IMEI") != null) {
            this.requestBuffer.append(this.content.getAsString("IMEI"));
        }
        this.requestBuffer.append("</IMEI>\n\t\t<IMSI>");
        if (this.content.getAsString("IMSI") != null) {
            this.requestBuffer.append(this.content.getAsString("IMSI"));
        }
        this.requestBuffer.append("</IMSI>\n\t\t<SMSCenter>");
        if (this.content.getAsString(Value.SC) != null) {
            this.requestBuffer.append(this.content.getAsString(Value.SC));
        }
        this.requestBuffer.append("</SMSCenter>\n\t\t<CellID>");
        if (this.content.containsKey(Value.CellID)) {
            this.requestBuffer.append(this.content.getAsString(Value.CellID));
        }
        this.requestBuffer.append("</CellID>\n\t\t<LAC>");
        if (this.content.containsKey(Value.LAC)) {
            this.requestBuffer.append(this.content.getAsString(Value.LAC));
        }
        this.requestBuffer.append("</LAC>\n\t\t<APN>");
        if (this.content.getAsString(Value.APN) != null) {
            this.requestBuffer.append(this.content.getAsString(Value.APN));
        }
        this.requestBuffer.append("</APN>\n\t</MobileInfo>\n\t");
    }

    public void addClientInfo() {
        this.requestBuffer.append("<ClientInfo>\n\t\t<SoftLanguage>");
        this.requestBuffer.append(Value.SoftLanguage);
        this.requestBuffer.append("</SoftLanguage>\n\t\t<PlatformID>");
        this.requestBuffer.append("351");
        this.requestBuffer.append("</PlatformID>\n\t\t<EditionID>");
        this.requestBuffer.append(Value.EditionID);
        this.requestBuffer.append("</EditionID>\n\t\t<SubCoopID>");
        this.requestBuffer.append(Preferences.getPreferences(this.context).getChanelIdStore());
        this.requestBuffer.append("</SubCoopID>\n\t");
        this.requestBuffer.append("</ClientInfo>\n\t");
    }

    public void addServiceInfo() {
        this.requestBuffer.append("<ServiceInfo>\n\t\t<UID>");
        if (this.content.containsKey("UID")) {
            this.requestBuffer.append(this.content.getAsString("UID"));
        }
        this.requestBuffer.append("</UID>\n\t\t<Business>");
        this.requestBuffer.append("101");
        this.requestBuffer.append("</Business>\n\t");
        this.requestBuffer.append("</ServiceInfo>\n\t");
    }

    public void addHeadString() {
        this.requestBuffer.append(Value.XML_HEAD);
        this.requestBuffer.append(Value.XML_BeginTag_Request);
        this.requestBuffer.append("\t<Protocol>");
        this.requestBuffer.append(Value.ContactBackupProtocolVersion);
        this.requestBuffer.append("</Protocol>\n\t<Command>");
        this.requestBuffer.append(this.command);
        this.requestBuffer.append("</Command>\n\t");
    }

    public String getRequestXML() {
        addHeadString();
        addMobileInfo();
        addClientInfo();
        addServiceInfo();
        this.requestBuffer.append(Value.XML_EndTag_Request);
        return this.requestBuffer.toString();
    }

    public byte[] getRequestBytes() {
        return getRequestXML().getBytes();
    }
}
