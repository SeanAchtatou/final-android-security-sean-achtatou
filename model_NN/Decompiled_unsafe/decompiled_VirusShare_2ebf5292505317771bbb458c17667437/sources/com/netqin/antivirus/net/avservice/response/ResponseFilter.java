package com.netqin.antivirus.net.avservice.response;

import android.text.TextUtils;
import android.util.Log;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.ext.DefaultHandler2;

public class ResponseFilter extends DefaultHandler2 {
    private StringBuffer buf = new StringBuffer();
    private String command;

    public void endDocument() throws SAXException {
    }

    public void startDocument() throws SAXException {
    }

    public void endElement(String uri, String localName, String name) throws SAXException {
        if (localName.equals("Command")) {
            Log.d("Command", this.buf.toString().trim());
            this.command = this.buf.toString().trim();
            this.buf.setLength(0);
            throw new SAXUserEndException();
        } else if (localName.equals("Protocol")) {
            this.buf.setLength(0);
        }
    }

    public int getCommand() {
        try {
            if (!TextUtils.isEmpty(this.command)) {
                return Integer.valueOf(this.command).intValue();
            }
            return 0;
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public void startElement(String uri, String localName, String name, Attributes attributes) throws SAXException {
        if (localName.equals("Command")) {
            this.buf.setLength(0);
        } else if (localName.equals("Protocol")) {
            this.buf.setLength(0);
        }
    }

    public void characters(char[] ch, int start, int length) throws SAXException {
        this.buf.append(ch, start, length);
    }
}
