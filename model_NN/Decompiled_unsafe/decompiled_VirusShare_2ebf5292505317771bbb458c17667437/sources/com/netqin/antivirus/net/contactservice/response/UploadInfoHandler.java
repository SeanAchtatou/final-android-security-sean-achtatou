package com.netqin.antivirus.net.contactservice.response;

import com.netqin.antivirus.Value;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.ext.DefaultHandler2;

public class UploadInfoHandler extends DefaultHandler2 {
    private StringBuffer buf = new StringBuffer();
    private int messageNumber = 0;
    private boolean thistimeStatus = false;
    private boolean totalStatus = false;

    public void endDocument() throws SAXException {
    }

    public void startDocument() throws SAXException {
    }

    public void endElement(String uri, String localName, String name) throws SAXException {
        if (localName.equals("Protocol")) {
            this.buf.setLength(0);
        } else if (localName.equals("Command")) {
            this.buf.setLength(0);
        } else if (localName.equals(Value.PacketStatus)) {
            if (this.buf.toString().trim().equals("0")) {
                this.thistimeStatus = true;
            } else if (this.buf.toString().trim().equals("1")) {
                this.thistimeStatus = false;
            } else {
                this.thistimeStatus = false;
            }
            this.buf.setLength(0);
        } else if (localName.equals(Value.TotalStatus)) {
            if (this.buf.toString().trim().equals("0")) {
                this.totalStatus = true;
            } else if (this.buf.toString().trim().equals("1")) {
                this.totalStatus = false;
            } else {
                this.totalStatus = false;
            }
            this.buf.setLength(0);
        }
    }

    public void startElement(String uri, String localName, String name, Attributes attributes) throws SAXException {
        if (localName.equals("Protocol")) {
            this.buf.setLength(0);
        } else if (localName.equals("Command")) {
            this.buf.setLength(0);
        } else if (localName.equals(Value.PacketStatus)) {
            this.buf.setLength(0);
        } else if (localName.equals(Value.TotalStatus)) {
            this.buf.setLength(0);
        }
    }

    public void characters(char[] ch, int start, int length) throws SAXException {
        this.buf.append(ch, start, length);
    }

    public boolean getThisTimeStatus() {
        return this.thistimeStatus;
    }

    public boolean getTotalStatus() {
        return this.totalStatus;
    }
}
