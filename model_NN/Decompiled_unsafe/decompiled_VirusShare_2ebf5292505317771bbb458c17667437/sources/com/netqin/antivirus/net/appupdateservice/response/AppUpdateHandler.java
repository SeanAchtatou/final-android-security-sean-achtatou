package com.netqin.antivirus.net.appupdateservice.response;

import android.content.ContentValues;
import com.netqin.antivirus.Value;
import com.netqin.antivirus.appprotocol.XmlTagValue;
import com.netqin.antivirus.cloud.model.xml.XmlUtils;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.ext.DefaultHandler2;

public class AppUpdateHandler extends DefaultHandler2 {
    private int ADmessageNumber = 0;
    private StringBuffer buf;
    private ContentValues content;
    private int systemMessageNumber = 0;
    private boolean type1 = false;
    private boolean type2 = false;
    private boolean type3 = false;

    public AppUpdateHandler(ContentValues contentValues) {
        this.content = contentValues;
        this.buf = new StringBuffer();
    }

    public void characters(char[] ch, int start, int length) throws SAXException {
        this.buf.append(ch, start, length);
    }

    public void endDocument() throws SAXException {
        this.content.put(Value.AppUpdateADMessageCount, Integer.toString(this.ADmessageNumber));
        this.content.put(Value.AppUpdateSystemMsgCount, Integer.toString(this.systemMessageNumber));
    }

    public void endElement(String uri, String localName, String name) throws SAXException {
        if (localName.equals("Protocol")) {
            this.content.put("Protocol", this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals("Command")) {
            this.content.put("Command", this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals("SessionId")) {
            this.content.put("SessionId", this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals("Balance")) {
            this.content.put("Balance", this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals("UID")) {
            this.content.put("UID", this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals("NextConnectTime")) {
            this.content.put("NextConnectTime", this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals(Value.Level)) {
            this.buf.setLength(0);
        } else if (localName.equals(Value.Status)) {
            this.content.put(Value.Status, this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals(Value.Expired)) {
            this.content.put(Value.Expired, this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals(Value.NextPayDay)) {
            this.content.put(Value.NextPayDay, this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals("LevelName")) {
            this.content.put("LevelName", this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals(Value.SecretSpaceUsable)) {
            this.content.put(Value.SecretSpaceUsable, this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals(Value.SmsFilterUsable)) {
            this.content.put(Value.SmsFilterUsable, this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals(XmlTagValue.message)) {
            if (!this.content.containsKey("Prompt")) {
                this.content.put("Prompt", this.buf.toString().trim());
            }
            this.buf.setLength(0);
        } else if (localName.equals("ErrorCode")) {
            this.buf.setLength(0);
        } else if (localName.equals("Module")) {
            this.buf.setLength(0);
        } else if (localName.equals("UpdateInfo")) {
            this.buf.setLength(0);
        }
    }

    public void startDocument() throws SAXException {
    }

    public void startElement(String uri, String localName, String name, Attributes attributes) throws SAXException {
        if (localName.equals("Protocol")) {
            this.buf.setLength(0);
        } else if (localName.equals("Command")) {
            this.buf.setLength(0);
        } else if (localName.equals("SessionId")) {
            this.buf.setLength(0);
        } else if (localName.equals("Balance")) {
            this.buf.setLength(0);
        } else if (localName.equals("UID")) {
            this.buf.setLength(0);
        } else if (localName.equals(Value.Level)) {
            this.buf.setLength(0);
        } else if (localName.equals("NextConnectTime")) {
            this.buf.setLength(0);
        } else if (localName.equals(Value.Status)) {
            this.buf.setLength(0);
        } else if (localName.equals(Value.Expired)) {
            this.buf.setLength(0);
        } else if (localName.equals(Value.NextPayDay)) {
            this.buf.setLength(0);
        } else if (localName.equals("LevelName")) {
            this.buf.setLength(0);
        } else if (localName.equals(Value.SecretSpaceUsable)) {
            this.buf.setLength(0);
        } else if (localName.equals(Value.SmsFilterUsable)) {
            this.buf.setLength(0);
        } else if (localName.equals(XmlUtils.LABEL_FILE)) {
            this.content.put(Value.AppUpdateFileLength, attributes.getValue(XmlUtils.LABEL_LENGTH));
            this.content.put(Value.AppUpdateFileName, attributes.getValue("name"));
            this.content.put(Value.AppUpdateSrc, attributes.getValue("src"));
            this.content.put(Value.AppUpdateAction, attributes.getValue("action"));
            this.buf.setLength(0);
        } else if (localName.equals("Module")) {
            this.content.put(Value.AppVersion, attributes.getValue(XmlTagValue.version));
            this.buf.setLength(0);
        } else if (localName.equals("ErrorCode")) {
            this.buf.setLength(0);
        } else if (localName.equals(XmlTagValue.message)) {
            String type = attributes.getValue("type");
            if (type.equals("1")) {
                this.type1 = true;
            } else if (type.equals("2")) {
                this.type2 = true;
            } else if (type.equals("3")) {
                this.type3 = true;
            }
            this.buf.setLength(0);
        } else if (localName.equals("SystemMessages")) {
            if (attributes.getValue(Value.Display) != null && attributes.getValue(Value.Display).length() > 0) {
                this.content.put(Value.Display, attributes.getValue(Value.Display));
            }
            this.buf.setLength(0);
        } else if (localName.equals("Module")) {
            this.buf.setLength(0);
        } else if (localName.equals("UpdateInfo")) {
            this.content.put(Value.AppUpdateNecessary, attributes.getValue("necessary"));
            this.buf.setLength(0);
        }
    }
}
