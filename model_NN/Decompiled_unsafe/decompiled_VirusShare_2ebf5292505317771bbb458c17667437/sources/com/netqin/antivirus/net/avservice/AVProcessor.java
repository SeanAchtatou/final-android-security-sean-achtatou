package com.netqin.antivirus.net.avservice;

import android.content.ContentValues;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Xml;
import com.netqin.antivirus.NqUtil;
import com.netqin.antivirus.Security;
import com.netqin.antivirus.Value;
import com.netqin.antivirus.appprotocol.AppValue;
import com.netqin.antivirus.appprotocol.XmlTagValue;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.net.HttpHandler;
import com.netqin.antivirus.net.avservice.request.Request;
import com.netqin.antivirus.net.avservice.response.Response;
import com.nqmobile.antivirus_ampro20.R;
import java.io.FileOutputStream;
import java.net.Proxy;
import org.xml.sax.SAXException;

public class AVProcessor {
    private int avdbsize = 0;
    private boolean cancel = false;
    private Context context;
    private HttpHandler httpHandler;

    public AVProcessor(Context context2) {
        this.context = context2;
        this.httpHandler = new HttpHandler();
    }

    public synchronized int process(int command, Handler handler, ContentValues content, AppValue value) {
        Proxy proxy = NqUtil.getApnProxy(this.context);
        if (proxy != null) {
            this.httpHandler.setProxy(proxy);
        } else {
            this.httpHandler.NoProxy();
        }
        value.requestCommandId = command;
        return processResult(this.httpHandler.sendAppServerRequest(new Request(content, this.context, value).getRequestBytes(command)), command, content, handler, value);
    }

    public int processResult(int result, int command, ContentValues content, Handler handler, AppValue value) {
        byte[] response = this.httpHandler.getResponsebytes();
        if (response == null) {
            return sendSendReceiveErrorMessage(handler, command);
        }
        if (value.requestCommandId != 13) {
            return process(new String(Security.decrypt(response)), command, content, handler, value);
        }
        CommonMethod.logDebug("AVService", response.toString());
        try {
            FileOutputStream fos = this.context.openFileOutput(value.downloadVirusName, 1);
            fos.write(response);
            fos.flush();
            fos.close();
            content.put(Value.AVDBUpdateSuccess, "1");
            return 10;
        } catch (Exception e) {
            return sendFileStoreErrorMessage(handler, 6);
        }
    }

    private synchronized int process(String info, int command, ContentValues content, Handler handler, AppValue value) {
        int sendSAXExceptionMessage;
        CommonMethod.logDebug("AVService", info);
        try {
            Xml.parse(info, new Response(content, value));
            value.sessionInfo = getContentAsString(content, XmlTagValue.sessionInfo);
            value.responseCommandId = getContentAsInteger(content, "Command");
            if (value.responseCommandId == 1) {
                value.isDataIgnore = getContentAsboolean(content, XmlTagValue.isDataIgnore);
                value.errorCode = getContentAsInteger(content, "ErrorCode");
            }
            if (value.isDataIgnore) {
                CommonMethod.logDebug("AVService", "Tag IsDataIgnore Y");
                sendSAXExceptionMessage = 10;
            } else {
                if (value.responseCommandId == 16) {
                    value.notificationDesc = getContentAsString(content, XmlTagValue.notification);
                    value.notificationEvent = getContentAsInteger(content, XmlTagValue.event);
                } else if (value.responseCommandId == 21) {
                    value.multiChargeDesc = getContentAsString(content, XmlTagValue.description);
                }
                value.purchasedVirusVersion = getContentAsString(content, XmlTagValue.purchasedVirusVersion);
                value.latestVirusVersion = getContentAsString(content, XmlTagValue.latestVirusVersion);
                value.downloadVirusName = getContentAsString(content, XmlTagValue.localVirusFileName);
                value.downloadVirusSize = getContentAsInteger(content, XmlTagValue.size);
                value.downloadVirusVersion = getContentAsString(content, XmlTagValue.version);
                value.isBalanceAdequate = getContentAsboolean(content, "IsBalanceAdequate");
                value.operationType = getContentAsInteger(content, "OperationType");
                value.chargePrompt = getContentAsString(content, "Prompt");
                if (TextUtils.isEmpty(value.chargePrompt)) {
                    value.chargePrompt = getContentAsString(content, "prompt");
                }
                value.chargeReconfirmPrompt = getContentAsString(content, XmlTagValue.reConfirmPrompt);
                if (content.containsKey(XmlTagValue.nextStepCmdYES)) {
                    value.nextStepCmdYes = getContentAsInteger(content, XmlTagValue.nextStepCmdYES);
                }
                if (content.containsKey(XmlTagValue.nextStepCmdNO)) {
                    value.nextStepCmdNo = getContentAsInteger(content, XmlTagValue.nextStepCmdNO);
                }
                value.nextStepCmdYesTryTimes = getContentAsInteger(content, XmlTagValue.times);
                value.chargeId = getContentAsString(content, XmlTagValue.chargeId);
                if (value.operationType == 81) {
                    value.zongSDKChargePaymentUrl = getContentAsString(content, XmlTagValue.paymentUrl);
                    value.zongSDKChargeAppName = getContentAsString(content, XmlTagValue.appName);
                    value.zongSDKChargeCustomerKey = getContentAsString(content, XmlTagValue.customerKey);
                    value.zongSDKChargeCountry = getContentAsString(content, XmlTagValue.country);
                    value.zongSDKChargeCurrency = getContentAsString(content, XmlTagValue.currency);
                    value.zongSDKChargeTransactionRef = getContentAsString(content, XmlTagValue.transactionRef);
                } else if (value.operationType == 11 || value.operationType == 21 || value.operationType == 31 || value.operationType == 41) {
                    value.smsChargeIsSendVisible = getContentAsboolean(content, XmlTagValue.isSendVisible);
                    value.smsChargeSendNumber = getContentAsString(content, XmlTagValue.number);
                    value.smsChargeSendContent = getContentAsString(content, XmlTagValue.content);
                    value.smsChargeSendCount = getContentAsInteger(content, XmlTagValue.count);
                    value.smsChargeIsNeedReConfirm = getContentAsboolean(content, XmlTagValue.isNeedReConfirm);
                    value.smsChargeIsReceiveReconfirmVisible = getContentAsboolean(content, XmlTagValue.isReceiveReconfirmVisible);
                    value.smsChargeIsSendReconfirmVisible = getContentAsboolean(content, XmlTagValue.isSendReconfirmVisible);
                    value.smsChargeReconfirmWaitTime = getContentAsInteger(content, XmlTagValue.reconfirmWaitTime);
                    value.smsChargeReconfirmNumber = getContentAsString(content, XmlTagValue.reconfirmNumber);
                    value.smsChargeReconfirmContent = getContentAsString(content, XmlTagValue.reconfirmContent);
                    value.smsChargeReconfirmMatch = getContentAsString(content, XmlTagValue.reconfirmMatch);
                } else if (value.operationType == 12 || value.operationType == 22 || value.operationType == 32 || value.operationType == 42 || value.operationType == 61 || value.operationType == 62 || value.operationType == 63) {
                    value.wapChargeIsClickVisible = getContentAsboolean(content, XmlTagValue.isClickVisible);
                    value.wapChargeUrl = getContentAsString(content, XmlTagValue.uRL);
                    value.wapChargeInternal = getContentAsInteger(content, XmlTagValue.internal);
                    value.wapChargeCount = getContentAsInteger(content, XmlTagValue.count);
                    value.wapChargeRule = getContentAsString(content, XmlTagValue.rule);
                    value.wapChargePostData = getContentAsString(content, XmlTagValue.postData);
                    value.wapChargeIsMsgVisible = getContentAsboolean(content, XmlTagValue.isMsgVisible);
                    value.wapChargePageIndex = getContentAsInteger(content, XmlTagValue.pageIndex);
                    value.wapChargeConfirmMatch = getContentAsString(content, "ConfirmMatch");
                    value.wapChargeSuccessSign = getContentAsString(content, XmlTagValue.successSign);
                    value.wapChargeCheckTime = getContentAsInteger(content, XmlTagValue.checkTime);
                } else if (value.operationType == 82 || value.operationType == 51) {
                    value.paymentCentreUrl = getContentAsString(content, XmlTagValue.paymentCentreUrl);
                    value.paymentCentreUrlOpenType = getContentAsInteger(content, XmlTagValue.openType);
                }
                sendSAXExceptionMessage = 10;
            }
        } catch (SAXException e) {
            sendSAXExceptionMessage = sendSAXExceptionMessage(handler, command, null);
        }
        return sendSAXExceptionMessage;
    }

    private String getContentAsString(ContentValues content, String tag) {
        if (content.containsKey(tag)) {
            return content.getAsString(tag);
        }
        return "";
    }

    private int getContentAsInteger(ContentValues content, String tag) {
        if (content.containsKey(tag)) {
            return content.getAsInteger(tag).intValue();
        }
        return 0;
    }

    private float getContentAsfloat(ContentValues content, String tag) {
        if (content.containsKey(tag)) {
            return content.getAsFloat(tag).floatValue();
        }
        return 0.0f;
    }

    private boolean getContentAsboolean(ContentValues content, String tag) {
        String str = getContentAsString(content, tag);
        if (str.equals("1") || str.equalsIgnoreCase("Y")) {
            return true;
        }
        return false;
    }

    private void sendMessage(ContentValues content, Handler handler, int command) {
        if (handler != null) {
            Message msg = new Message();
            msg.what = 13;
            handler.sendMessage(msg);
        }
    }

    private int sendSAXExceptionMessage(Handler handler, int command, String parsedInfo) {
        if (handler == null) {
            return 16;
        }
        Message msg = new Message();
        msg.what = 13;
        msg.arg1 = 27;
        msg.obj = this.context.getString(R.string.SEND_RECEIVE_ERROR);
        handler.sendMessage(msg);
        return 16;
    }

    private int sendSendReceiveErrorMessage(Handler handler, int command) {
        if (handler == null || this.cancel) {
            return 8;
        }
        Message msg = new Message();
        msg.what = 13;
        msg.arg1 = 27;
        msg.obj = this.context.getString(R.string.SEND_RECEIVE_ERROR);
        handler.sendMessage(msg);
        return 8;
    }

    private int sendFileStoreErrorMessage(Handler handler, int command) {
        if (handler == null) {
            return 17;
        }
        Message msg = new Message();
        msg.what = 13;
        msg.arg1 = 27;
        msg.obj = this.context.getString(R.string.FILE_STORE_ERROR);
        handler.sendMessage(msg);
        return 17;
    }

    public void setCancel(boolean cancel2) {
        this.cancel = cancel2;
        this.httpHandler.setCancel(cancel2);
    }
}
