package com.netqin.antivirus.net;

public interface NetReturnValue {
    public static final int AV_DB_NEED_UPDATE = 101;
    public static final int AV_DB_NOT_NEED_UPDATE = 102;
    public static final int AppNeedUpdate = 7890;
    public static final int CommandNotFound = 15;
    public static final int DB_FILE_STORE_ERROR = 13;
    public static final int DB_NOT_NEED_TO_UPDATE = 11;
    public static final int DataConnection_ERROR_AIRPLANE_MODE = 4;
    public static final int DataConnection_ERROR_NO_SIGNAL = 3;
    public static final int DataConnection_ERROR_PROF_NOT_FOUND = 2;
    public static final int DataConnection_ERROR_UNKNOWN = 5;
    public static final int DataConnection_SUCCESS = 1;
    public static final int FILE_STORE_ERROR = 17;
    public static final int PARSE_XML_ERROR = 16;
    public static final int SEND_OK = 9;
    public static final int SEND_RECEIVE_ERROR = 8;
    public static final int SEND_RECEIVE_SUCCESS = 10;
    public static final int SERVER_ERROR = 12;
    public static final int UNKNOWN_ERROR = 14;
    public static final int UserAccountCreateSuccess = 1003;
    public static final int UserCancelled = 16;
    public static final int Username_Available = 1002;
    public static final int Username_Unavailable = 1001;
}
