package com.netqin.antivirus.net.accountservice;

import android.content.ContentValues;
import android.content.Context;
import android.os.Handler;
import android.util.Xml;
import com.netqin.antivirus.Log;
import com.netqin.antivirus.NqUtil;
import com.netqin.antivirus.net.HttpHandler;
import com.netqin.antivirus.net.accountservice.request.UserAccountCheckReq;
import com.netqin.antivirus.net.accountservice.request.UserAccountCreateReq;
import com.netqin.antivirus.net.accountservice.request.UserAccountLoginReq;
import com.netqin.antivirus.net.accountservice.request.UserAccountPasswordChangeReq;
import com.netqin.antivirus.net.accountservice.response.UserAccountResponseHandler;
import com.nqmobile.antivirus_ampro20.R;
import java.net.Proxy;
import java.util.Vector;
import org.xml.sax.SAXException;

public class AccountProcessor {
    private boolean cancel = false;
    private Context context;
    private HttpHandler httpHandler;
    private boolean next = false;
    private int zcommand;
    private boolean zwait = false;

    public AccountProcessor(Context context2) {
        this.context = context2;
        this.httpHandler = new HttpHandler();
    }

    private synchronized int process(String info, int command, ContentValues content, Handler handler) {
        int sendSAXExceptionMessage;
        try {
            Xml.parse(info, new UserAccountResponseHandler(content));
            sendSAXExceptionMessage = 10;
        } catch (SAXException e) {
            sendSAXExceptionMessage = sendSAXExceptionMessage(handler, 1, info);
        }
        return sendSAXExceptionMessage;
    }

    public synchronized int process(int command, Handler handler, ContentValues content) {
        int processResult;
        Proxy proxy = NqUtil.getApnProxy(this.context);
        if (proxy != null) {
            this.httpHandler.setProxy(proxy);
        } else {
            this.httpHandler.NoProxy();
        }
        switch (command) {
            case 1:
                processResult = processResult(this.httpHandler.sendUserServerRequest(new UserAccountCreateReq(content, this.context).getRequestBytes()), 1, content, handler);
                break;
            case 2:
                processResult = processResult(this.httpHandler.sendUserServerRequest(new UserAccountLoginReq(content, this.context).getRequestBytes()), 2, content, handler);
                break;
            case 3:
                processResult = processResult(this.httpHandler.sendUserServerRequest(new UserAccountPasswordChangeReq(content, this.context).getRequestBytes()), 3, content, handler);
                break;
            case 4:
                processResult = processResult(this.httpHandler.sendUserServerRequest(new UserAccountPasswordChangeReq(content, this.context).getRequestBytes()), 4, content, handler);
                break;
            case 5:
                processResult = processResult(this.httpHandler.sendUserServerRequest(new UserAccountCheckReq(content, this.context).getRequestBytes()), 5, content, handler);
                break;
            default:
                processResult = 15;
                break;
        }
        return processResult;
    }

    public int processResult(int result, int command, ContentValues content, Handler handler) {
        byte[] response = this.httpHandler.getResponsebytes();
        if (response == null) {
            return sendSendReceiveErrorMessage(handler, command);
        }
        return process(new String(response), command, content, handler);
    }

    public synchronized void next() {
        this.next = true;
        notify();
    }

    public synchronized void cancel() {
        this.next = false;
        notify();
    }

    private int sendSAXExceptionMessage(Handler handler, int command, String parsedInfo) {
        if (handler == null) {
            return 16;
        }
        Vector<String> vec = new Vector<>();
        vec.add(this.context.getString(R.string.SEND_RECEIVE_ERROR));
        handler.sendMessage(handler.obtainMessage(400, command, 0, vec));
        return 16;
    }

    private void sendLocalNetErrorMessage(Handler handler, int command, String error) {
        if (handler != null) {
            Vector<String> vec = new Vector<>();
            vec.add(error);
            handler.sendMessage(handler.obtainMessage(400, command, 0, vec));
        }
    }

    private int processLocalNetError(int result, Handler handler, int command) {
        return result;
    }

    private int sendSendReceiveErrorMessage(Handler handler, int command) {
        if (handler == null || this.cancel) {
            return 8;
        }
        Vector<String> vec = new Vector<>();
        vec.add(this.context.getString(R.string.SEND_RECEIVE_ERROR));
        handler.sendMessage(handler.obtainMessage(400, command, 0, vec));
        return 8;
    }

    private int sendFileStoreErrorMessage(Handler handler, int command) {
        if (handler == null) {
            return 17;
        }
        handler.sendMessage(handler.obtainMessage(400, command, 0, new Vector<>()));
        return 17;
    }

    public void setCancel(boolean cancel2) {
        if (cancel2) {
            Log.d("Netprocessor", "cancel call");
        }
        this.cancel = cancel2;
        this.httpHandler.setCancel(cancel2);
    }

    public void setCommand(int command) {
        this.zcommand = command;
    }

    private synchronized int zwait() {
        int i;
        this.zwait = true;
        try {
            wait();
            this.zwait = false;
            i = this.next ? 1 : 0;
        } catch (InterruptedException e) {
            this.zwait = false;
            i = -1;
        }
        return i;
    }

    public boolean checkWait() {
        return this.zwait;
    }
}
