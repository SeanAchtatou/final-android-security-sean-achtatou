package com.netqin.antivirus.net.contactservice.response;

import android.content.ContentValues;
import android.util.Log;
import com.netqin.antivirus.Value;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.ext.DefaultHandler2;

public class ContactBackupHandler extends DefaultHandler2 {
    private StringBuffer buf;
    private ContentValues content;
    private int messageNumber = 0;

    public ContactBackupHandler(ContentValues content2) {
        this.content = content2;
        this.buf = new StringBuffer();
    }

    public void endDocument() throws SAXException {
        this.content.put(Value.MessageCount, Integer.toString(this.messageNumber));
    }

    public void startDocument() throws SAXException {
    }

    public void endElement(String uri, String localName, String name) throws SAXException {
        if (localName.equals("Protocol")) {
            this.content.put("Protocol", this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals("Command")) {
            this.content.put("Command", this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals("UID")) {
            this.content.put("UID", this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals("username")) {
            this.content.put(Value.Username, this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals("Prompt")) {
            this.content.put("Prompt", this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals(Value.UploadUrl)) {
            this.content.put(Value.UploadUrl, this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals("MaxPacketSize")) {
            this.content.put(Value.MaxPacketSize, this.buf.toString().trim());
            this.buf.setLength(0);
        }
    }

    public void startElement(String uri, String localName, String name, Attributes attributes) throws SAXException {
        Log.d("name", localName);
        if (localName.equals("Protocol")) {
            this.buf.setLength(0);
        } else if (localName.equals("Command")) {
            this.buf.setLength(0);
        } else if (localName.equals("UID")) {
            this.buf.setLength(0);
        } else if (localName.equals("username")) {
            this.buf.setLength(0);
        } else if (localName.equals("Prompt")) {
            this.buf.setLength(0);
        } else if (localName.equals(Value.UploadUrl)) {
            this.buf.setLength(0);
        } else if (localName.equals("MaxPacketSize")) {
            this.buf.setLength(0);
        }
    }

    public void characters(char[] ch, int start, int length) throws SAXException {
        this.buf.append(ch, start, length);
    }
}
