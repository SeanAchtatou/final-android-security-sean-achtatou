package com.netqin.antivirus.net.avservice;

import android.content.ContentValues;
import android.content.Context;
import android.os.Handler;
import com.netqin.antivirus.appprotocol.AppValue;

public class AVService {
    private static AVService service = null;
    private Context context;
    private AVProcessor processor;

    public AVService(Context context2) {
        this.context = context2;
    }

    public static synchronized AVService getInstance(Context context2) {
        AVService aVService;
        synchronized (AVService.class) {
            if (service == null) {
                service = new AVService(context2);
            }
            aVService = service;
        }
        return aVService;
    }

    public void cancel() {
        if (this.processor != null) {
            this.processor.setCancel(true);
        }
    }

    public synchronized int request(int command, Handler handler, ContentValues content, AppValue value) {
        int result;
        this.processor = new AVProcessor(this.context);
        this.processor.setCancel(false);
        result = this.processor.process(command, handler, content, value);
        this.processor = null;
        return result;
    }
}
