package com.netqin.antivirus.net.contactservice.response;

import android.content.ContentValues;
import com.netqin.antivirus.Value;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.ext.DefaultHandler2;

public class ContactsDownloadHandler extends DefaultHandler2 {
    private StringBuffer buf;
    private ContentValues content;
    private int messageNumber = 0;

    public ContactsDownloadHandler(ContentValues content2) {
        this.content = content2;
        this.buf = new StringBuffer();
    }

    public void endDocument() throws SAXException {
        this.content.put(Value.MessageCount, Integer.toString(this.messageNumber));
    }

    public void startDocument() throws SAXException {
    }

    public void endElement(String uri, String localName, String name) throws SAXException {
        if (localName.equals("Protocol")) {
            this.content.put("Protocol", this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals("Command")) {
            this.content.put("Command", this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals("UID")) {
            this.content.put("UID", this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals("Prompt")) {
            this.content.put("Prompt", this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals(Value.DownloadUrl)) {
            this.content.put(Value.DownloadUrl, this.buf.toString().trim());
            this.buf.setLength(0);
        }
    }

    public void startElement(String uri, String localName, String name, Attributes attributes) throws SAXException {
        if (localName.equals("Protocol")) {
            this.buf.setLength(0);
        } else if (localName.equals("Command")) {
            this.buf.setLength(0);
        } else if (localName.equals("UID")) {
            this.buf.setLength(0);
        } else if (localName.equals("Prompt")) {
            this.buf.setLength(0);
        } else if (localName.equals(Value.DownloadUrl)) {
            this.content.put(Value.VCardFileLength, attributes.getValue("fileLength"));
            this.buf.setLength(0);
        }
    }

    public void characters(char[] ch, int start, int length) throws SAXException {
        this.buf.append(ch, start, length);
    }
}
