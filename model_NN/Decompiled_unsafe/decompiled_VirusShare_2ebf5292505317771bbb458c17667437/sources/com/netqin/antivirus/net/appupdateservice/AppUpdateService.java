package com.netqin.antivirus.net.appupdateservice;

import android.content.ContentValues;
import android.content.Context;
import android.os.Handler;
import com.netqin.antivirus.Log;

public class AppUpdateService {
    private static AppUpdateService service = null;
    private Context context;
    private AppUpdateProcessor processor;

    private AppUpdateService(Context context2) {
        this.context = context2;
    }

    public static synchronized AppUpdateService getInstance(Context context2) {
        AppUpdateService appUpdateService;
        synchronized (AppUpdateService.class) {
            if (service == null) {
                service = new AppUpdateService(context2);
            }
            appUpdateService = service;
        }
        return appUpdateService;
    }

    public void cancel() {
        Log.d("cancel", "cancel");
        if (this.processor == null) {
            return;
        }
        if (this.processor.checkWait()) {
            userCancel();
        } else {
            this.processor.setCancel(true);
        }
    }

    public synchronized int request(Handler handler, ContentValues content) {
        int result;
        this.processor = new AppUpdateProcessor(this.context);
        this.processor.setCommand(6);
        this.processor.setCancel(false);
        result = this.processor.process(6, handler, content);
        this.processor = null;
        return result;
    }

    public void next() {
        this.processor.next();
    }

    public void userCancel() {
        this.processor.cancel();
    }
}
