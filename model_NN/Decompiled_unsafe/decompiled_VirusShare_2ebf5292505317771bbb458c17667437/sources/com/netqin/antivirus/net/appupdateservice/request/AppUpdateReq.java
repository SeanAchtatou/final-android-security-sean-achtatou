package com.netqin.antivirus.net.appupdateservice.request;

import android.content.ContentValues;
import android.content.Context;
import com.netqin.antivirus.Preferences;
import com.netqin.antivirus.Value;
import com.netqin.antivirus.common.CommonMethod;

public class AppUpdateReq {
    private ContentValues content;
    private Context context;
    private StringBuffer sb = new StringBuffer();

    public AppUpdateReq(ContentValues content2, Context context2) {
        this.content = content2;
        this.context = context2;
    }

    public String getRequestXML() {
        this.sb.append("<Request>\n");
        this.sb.append("<Protocol>2.2.1</Protocol>\n");
        this.sb.append("<Command>6</Command>\n");
        this.sb.append("<ClientInfo>\n\t");
        this.sb.append("<Model type=\"5\">" + Value.Model + "</Model>\n\t");
        this.sb.append("<Language>");
        this.sb.append(CommonMethod.getPlatformLanguage());
        this.sb.append("</Language>\n\t");
        this.sb.append("<Country>");
        this.sb.append(CommonMethod.getCountryCode());
        this.sb.append("</Country>\n\t");
        this.sb.append("<IMEI>");
        if (this.content.containsKey("IMEI")) {
            this.sb.append(this.content.getAsString("IMEI"));
        }
        this.sb.append("</IMEI>\n\t");
        this.sb.append("<IMSI>");
        if (this.content.containsKey("IMSI")) {
            this.sb.append(this.content.getAsString("IMSI"));
        }
        this.sb.append("</IMSI>\n\t");
        this.sb.append("<Timezone>" + CommonMethod.getTimeZone() + "</Timezone>\n\t");
        this.sb.append("<UpdateType>" + Value.UpdateType + "</UpdateType>\n");
        this.sb.append("</ClientInfo>\n");
        this.sb.append("<UserInfo>\n\t");
        this.sb.append("<UID>");
        if (!this.content.containsKey("UID") || this.content.getAsString("UID").trim().equals("")) {
            this.sb.append("");
        } else {
            this.sb.append(this.content.getAsString("UID"));
        }
        this.sb.append("</UID>\n");
        this.sb.append("</UserInfo>\n");
        this.sb.append("<ServiceInfo>\n\t");
        this.sb.append("<Service>3</Service>\n\t");
        this.sb.append("<Partner>" + Preferences.getPreferences(this.context).getChanelIdStore() + "</Partner>\n\t");
        this.sb.append("<WapMurl status=\"1\"/>\n");
        this.sb.append("</ServiceInfo>\n");
        this.sb.append("<VersionInfo os=\"351\" version=\"" + Value.EditionID + "\">\n\t");
        this.sb.append("<Module id=\"4\" version=\"31\" />\n\t");
        this.sb.append("<Module id=\"2\" version=\"200216\"/>\n");
        this.sb.append("</VersionInfo>\n");
        this.sb.append("</Request>\n");
        return this.sb.toString();
    }

    public byte[] getRequestBytes() {
        return getRequestXML().getBytes();
    }
}
