package com.netqin.antivirus.net.contactservice.request;

import android.content.ContentValues;
import android.content.Context;
import com.netqin.antivirus.Preferences;
import com.netqin.antivirus.Security;
import com.netqin.antivirus.Value;
import com.netqin.antivirus.common.CommonMethod;

public class Request {
    private String command;
    private ContentValues content;
    private Context context;
    public StringBuffer requestBuffer = new StringBuffer();

    public void setCommand(String command2) {
        this.command = command2;
    }

    public Request(ContentValues content2, Context context2) {
        this.content = content2;
        this.context = context2;
    }

    public Request(Context context2) {
        this.context = context2;
    }

    public void addMobileInfo() {
        this.requestBuffer.append("\t<MobileInfo>\n\t\t<Model>");
        this.requestBuffer.append(Value.Model);
        this.requestBuffer.append("</Model>\n\t\t<Language>");
        this.requestBuffer.append(CommonMethod.getPlatformLanguage());
        this.requestBuffer.append("</Language>\n\t\t<Country>");
        this.requestBuffer.append(Value.Country);
        this.requestBuffer.append("</Country>\n\t\t<IMEI>");
        if (this.content.getAsString("IMEI") != null) {
            this.requestBuffer.append(this.content.getAsString("IMEI"));
        }
        this.requestBuffer.append("</IMEI>\n\t\t<IMSI>");
        if (this.content.getAsString("IMSI") != null) {
            this.requestBuffer.append(this.content.getAsString("IMSI"));
        }
        this.requestBuffer.append("</IMSI>\n\t\t<SMSCenter>");
        if (this.content.getAsString(Value.SC) != null) {
            this.requestBuffer.append(this.content.getAsString(Value.SC));
        }
        this.requestBuffer.append("</SMSCenter>\n\t\t<CellID>");
        if (this.content.containsKey(Value.CellID)) {
            this.requestBuffer.append(this.content.getAsString(Value.CellID));
        }
        this.requestBuffer.append("</CellID>\n\t\t<LAC>");
        if (this.content.containsKey(Value.LAC)) {
            this.requestBuffer.append(this.content.getAsString(Value.LAC));
        }
        this.requestBuffer.append("</LAC>\n\t\t<APN>");
        if (this.content.getAsString(Value.APN) != null) {
            this.requestBuffer.append(this.content.getAsString(Value.APN));
        }
        this.requestBuffer.append("</APN>\n\t</MobileInfo>\n\t");
    }

    public void addClientInfo() {
        this.requestBuffer.append("<ClientInfo>\n\t\t<PlatformID>");
        this.requestBuffer.append("351");
        this.requestBuffer.append("</PlatformID>\n\t\t<EditionID>");
        this.requestBuffer.append(Value.EditionID);
        this.requestBuffer.append("</EditionID>\n\t\t<CorporationId>");
        this.requestBuffer.append(Value.CorporationId);
        this.requestBuffer.append("</CorporationId>\n\t\t<SubCoopID>");
        this.requestBuffer.append(Preferences.getPreferences(this.context).getChanelIdStore());
        this.requestBuffer.append("</SubCoopID>\n\t\t<SoftLanguage>");
        this.requestBuffer.append(CommonMethod.getPlatformLanguage());
        this.requestBuffer.append("</SoftLanguage>\n\t");
        this.requestBuffer.append("</ClientInfo>\n\t");
    }

    public void addServiceInfo() {
        this.requestBuffer.append("<ServiceInfo>\n\t\t<UID>");
        if (this.content.containsKey("UID")) {
            this.requestBuffer.append(this.content.getAsString("UID"));
        }
        this.requestBuffer.append("</UID>\n\t\t<Business>");
        this.requestBuffer.append("101");
        this.requestBuffer.append("</Business>\n\t\t<ContentType>");
        if (this.content.containsKey(Value.ContentType)) {
            this.requestBuffer.append(this.content.getAsString(Value.ContentType));
        }
        this.requestBuffer.append("</ContentType>\n\t</ServiceInfo>\n\t");
    }

    public void addAccountInfo() {
        this.requestBuffer.append("<NetqinAccount>\n\t\t<Username>");
        if (this.content.containsKey(Value.Username)) {
            this.requestBuffer.append(this.content.getAsString(Value.Username));
        }
        this.requestBuffer.append("</Username>\n\t\t<Password>");
        if (this.content.containsKey(Value.Password)) {
            this.requestBuffer.append("<![CDATA[" + this.content.getAsString(Value.Password) + "]]>");
        }
        this.requestBuffer.append("</Password>\n\t</NetqinAccount>\n");
    }

    public void addHeadString() {
        this.requestBuffer.append(Value.XML_HEAD);
        this.requestBuffer.append(Value.XML_BeginTag_Request);
        this.requestBuffer.append("\t<Protocol>");
        this.requestBuffer.append(Value.ContactBackupProtocolVersion);
        this.requestBuffer.append("</Protocol>\n\t<Command>");
        this.requestBuffer.append(this.command);
        this.requestBuffer.append("</Command>\n\t<FileType>");
        if (this.content.containsKey(Value.FileType)) {
            this.requestBuffer.append(this.content.getAsString(Value.FileType));
        }
        this.requestBuffer.append("</FileType>\n");
    }

    public String getRequestXML() {
        addHeadString();
        addMobileInfo();
        addClientInfo();
        addServiceInfo();
        addAccountInfo();
        this.requestBuffer.append(Value.XML_EndTag_Request);
        return this.requestBuffer.toString();
    }

    public byte[] getRequestBytes() {
        return Security.ecrypt(getRequestXML().getBytes());
    }
}
