package com.netqin.antivirus.net.contactservice.response;

import android.content.ContentValues;
import com.netqin.antivirus.Value;
import com.netqin.antivirus.appprotocol.XmlTagValue;
import com.netqin.antivirus.cloud.model.xml.XmlUtils;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.ext.DefaultHandler2;

public class ErrorHandler extends DefaultHandler2 {
    private StringBuffer buf;
    private ContentValues content;
    private int messageNumber = 0;

    public ErrorHandler(ContentValues content2) {
        this.content = content2;
        this.buf = new StringBuffer();
    }

    public void endDocument() throws SAXException {
        this.content.put(Value.MessageCount, Integer.toString(this.messageNumber));
    }

    public void startDocument() throws SAXException {
    }

    public void endElement(String uri, String localName, String name) throws SAXException {
        if (localName.equals("Protocol")) {
            this.content.put("Protocol", this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals("Command")) {
            this.content.put("Command", this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals("ErrorCode")) {
            this.content.put("ErrorCode", this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals("ErrorMsg")) {
            this.messageNumber++;
            this.content.put(XmlTagValue.message + this.messageNumber, this.buf.toString().trim());
            this.buf.setLength(0);
        }
    }

    public void startElement(String uri, String localName, String name, Attributes attributes) throws SAXException {
        if (localName.equals(XmlUtils.LABEL_REPLY)) {
            return;
        }
        if (localName.equals("Protocol")) {
            this.buf.setLength(0);
        } else if (localName.equals("Command")) {
            this.buf.setLength(0);
        } else if (localName.equals("ErrorMsg")) {
            this.buf.setLength(0);
        } else if (localName.equals("ErrorCode")) {
            this.buf.setLength(0);
        }
    }

    public void characters(char[] ch, int start, int length) throws SAXException {
        this.buf.append(ch, start, length);
    }
}
