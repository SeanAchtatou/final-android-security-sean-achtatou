package com.netqin.antivirus.net.contactservice;

import android.content.ContentValues;
import android.content.Context;
import android.os.Handler;

public class ContactService {
    private static ContactService service = null;
    private Context context;
    private ContactProcessor processor;

    private ContactService(Context context2) {
        this.context = context2;
    }

    public static synchronized ContactService getInstance(Context context2) {
        ContactService contactService;
        synchronized (ContactService.class) {
            if (service == null) {
                service = new ContactService(context2);
            }
            contactService = service;
        }
        return contactService;
    }

    public void cancel() {
        if (this.processor == null) {
            return;
        }
        if (this.processor.checkWait()) {
            userCancel();
        } else {
            this.processor.setCancel(true);
        }
    }

    public synchronized int request(Handler handler, ContentValues content, int command) {
        int result;
        this.processor = new ContactProcessor(this.context);
        this.processor.setCommand(command);
        this.processor.setCancel(false);
        result = this.processor.process(command, handler, content);
        this.processor = null;
        return result;
    }

    public void next() {
        this.processor.next();
    }

    public void userCancel() {
        this.processor.cancel();
    }
}
