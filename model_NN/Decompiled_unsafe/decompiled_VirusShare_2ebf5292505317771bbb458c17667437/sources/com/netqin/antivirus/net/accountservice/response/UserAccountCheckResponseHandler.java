package com.netqin.antivirus.net.accountservice.response;

import android.content.ContentValues;
import android.util.Log;
import com.netqin.antivirus.Value;
import com.netqin.antivirus.appprotocol.XmlTagValue;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.ext.DefaultHandler2;

public class UserAccountCheckResponseHandler extends DefaultHandler2 {
    private StringBuffer buf;
    private ContentValues content;
    private int messageNumber = 0;

    public UserAccountCheckResponseHandler(ContentValues content2) {
        this.content = content2;
        this.buf = new StringBuffer();
    }

    public void endDocument() throws SAXException {
        this.content.put(Value.MessageCount, Integer.toString(this.messageNumber));
    }

    public void startDocument() throws SAXException {
    }

    public void endElement(String uri, String localName, String name) throws SAXException {
        if (localName.equals("Protocol")) {
            this.content.put("Protocol", this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals("Command")) {
            this.content.put("Command", this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals(Value.Code)) {
            this.content.put(Value.Code, this.buf.toString().trim());
            this.buf.setLength(0);
        } else if (localName.equals(XmlTagValue.message)) {
            this.messageNumber++;
            this.content.put(XmlTagValue.message + this.messageNumber, this.buf.toString().trim());
            this.buf.setLength(0);
        }
    }

    public void startElement(String uri, String localName, String name, Attributes attributes) throws SAXException {
        Log.d("localName", localName);
        if (localName.equals("Protocol")) {
            this.buf.setLength(0);
        } else if (localName.equals("Command")) {
            this.buf.setLength(0);
        } else if (localName.equals(Value.Code)) {
            this.buf.setLength(0);
        } else if (localName.equals(XmlTagValue.message)) {
            this.buf.setLength(0);
        }
    }

    public void characters(char[] ch, int start, int length) throws SAXException {
        this.buf.append(ch, start, length);
    }
}
