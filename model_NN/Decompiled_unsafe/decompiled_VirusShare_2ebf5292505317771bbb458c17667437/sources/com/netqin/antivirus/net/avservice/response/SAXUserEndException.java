package com.netqin.antivirus.net.avservice.response;

import org.xml.sax.SAXException;

public class SAXUserEndException extends SAXException {
    private static final long serialVersionUID = 1;
}
