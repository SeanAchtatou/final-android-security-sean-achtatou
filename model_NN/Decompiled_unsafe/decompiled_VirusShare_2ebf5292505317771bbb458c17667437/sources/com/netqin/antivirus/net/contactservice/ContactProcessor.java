package com.netqin.antivirus.net.contactservice;

import android.content.ContentValues;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Xml;
import com.netqin.antivirus.Log;
import com.netqin.antivirus.NqUtil;
import com.netqin.antivirus.Security;
import com.netqin.antivirus.Value;
import com.netqin.antivirus.antilost.AntiLostCommon;
import com.netqin.antivirus.appprotocol.XmlTagValue;
import com.netqin.antivirus.common.CommonDefine;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.net.HttpHandler;
import com.netqin.antivirus.net.avservice.response.ResponseFilter;
import com.netqin.antivirus.net.avservice.response.SAXUserEndException;
import com.netqin.antivirus.net.contactservice.request.Request;
import com.netqin.antivirus.net.contactservice.response.ContactBackupHandler;
import com.netqin.antivirus.net.contactservice.response.ContactsDownloadHandler;
import com.netqin.antivirus.net.contactservice.response.ErrorHandler;
import com.netqin.antivirus.net.contactservice.response.UploadInfoHandler;
import com.nqmobile.antivirus_ampro20.R;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Proxy;
import java.net.URL;
import java.util.Vector;
import org.apache.http.util.ByteArrayBuffer;
import org.xml.sax.SAXException;

public class ContactProcessor {
    /* access modifiers changed from: private */
    public boolean cancel = false;
    private Context context;
    private int failtimes = 0;
    /* access modifiers changed from: private */
    public HttpHandler httpHandler;
    /* access modifiers changed from: private */
    public int mDownloadedLength = 0;
    /* access modifiers changed from: private */
    public boolean mDownloading = false;
    /* access modifiers changed from: private */
    public int mPacketLength = 20480;
    private int mUploadedLength = 0;
    private boolean next = false;
    private boolean uploadFileFinish = false;
    private boolean uploadFileSuccess = false;
    private int zcommand;
    private boolean zwait = false;

    private class Packet {
        ByteArrayBuffer content;
        int end;
        int length;
        int start;
        int total;

        private Packet() {
            this.start = 0;
            this.end = 0;
            this.length = 0;
            this.total = 0;
            this.content = new ByteArrayBuffer(ContactProcessor.this.mPacketLength);
        }

        /* synthetic */ Packet(ContactProcessor contactProcessor, Packet packet) {
            this();
        }

        public String getRange() {
            return "bytes " + this.start + "-" + this.end + "/" + this.total;
        }
    }

    public ContactProcessor(Context context2) {
        this.context = context2;
        this.httpHandler = new HttpHandler();
    }

    private synchronized int process(String info, int command, ContentValues content, Handler handler) {
        int sendSAXExceptionMessage;
        ResponseFilter filter = new ResponseFilter();
        try {
            CommonMethod.logDebug(AntiLostCommon.DELETE_CONTACT, "ContactProcessor : process: " + info);
            Xml.parse(info, filter);
        } catch (SAXException e) {
            if (!(e instanceof SAXUserEndException)) {
                sendSAXExceptionMessage = sendSAXExceptionMessage(handler, 24, info);
            }
        }
        switch (filter.getCommand()) {
            case Value.Command_Backup_File:
                try {
                    Xml.parse(info, new ContactBackupHandler(content));
                    if (!content.containsKey(Value.UploadUrl) || content.getAsString(Value.UploadUrl).equals("") || !content.containsKey(Value.UploadFileName) || content.getAsString(Value.UploadFileName).equals("")) {
                        sendSAXExceptionMessage = sendSendReceiveErrorMessage(handler, Value.Command_Backup_File);
                    } else {
                        try {
                            try {
                                sendSAXExceptionMessage = uploadFile(content, handler, new URL(content.getAsString(Value.UploadUrl)), new BufferedInputStream(new FileInputStream(content.getAsString(Value.UploadFileName))));
                            } catch (Exception e2) {
                                sendSAXExceptionMessage = sendSendReceiveErrorMessage(handler, Value.Command_Backup_File);
                                return sendSAXExceptionMessage;
                            }
                        } catch (Exception e3) {
                            Exception exc = e3;
                            sendSAXExceptionMessage = sendSendReceiveErrorMessage(handler, Value.Command_Backup_File);
                            return sendSAXExceptionMessage;
                        }
                    }
                } catch (SAXException e4) {
                    SAXException sAXException = e4;
                    sendSAXExceptionMessage = sendSAXExceptionMessage(handler, Value.Command_Backup_File, info);
                }
                break;
            case Value.Command_DownloadFile:
                try {
                    Xml.parse(info, new ContactsDownloadHandler(content));
                    if (!content.containsKey(Value.DownloadUrl) || content.getAsString(Value.DownloadUrl).equals("") || !content.containsKey(Value.VCardFileLength) || content.getAsString(Value.VCardFileLength).equals("")) {
                        sendSAXExceptionMessage = sendSendReceiveErrorMessage(handler, Value.Command_DownloadFile);
                    } else {
                        try {
                            try {
                                sendSAXExceptionMessage = downloadFile(content, handler, new URL(content.getAsString(Value.DownloadUrl)), new BufferedOutputStream(this.context.openFileOutput(CommonDefine.CONTACT_FILE_TEMP, 1)));
                            } catch (Exception e5) {
                                sendSAXExceptionMessage = sendSendReceiveErrorMessage(handler, Value.Command_DownloadFile);
                                return sendSAXExceptionMessage;
                            }
                        } catch (Exception e6) {
                            Exception exc2 = e6;
                            sendSAXExceptionMessage = sendSendReceiveErrorMessage(handler, Value.Command_DownloadFile);
                            return sendSAXExceptionMessage;
                        }
                    }
                } catch (SAXException e7) {
                    SAXException sAXException2 = e7;
                    sendSAXExceptionMessage = sendSAXExceptionMessage(handler, Value.Command_DownloadFile, info);
                }
                break;
            case Value.Command_ErrorInfo:
                try {
                    Xml.parse(info, new ErrorHandler(content));
                    sendMessage(content, handler, Value.Command_ErrorInfo);
                    sendSAXExceptionMessage = 10;
                    break;
                } catch (SAXException e8) {
                    SAXException sAXException3 = e8;
                    sendSAXExceptionMessage = sendSAXExceptionMessage(handler, Value.Command_ErrorInfo, info);
                    break;
                }
            default:
                sendSAXExceptionMessage = command;
                break;
        }
        return sendSAXExceptionMessage;
    }

    private int uploadFile(ContentValues content, Handler handler, URL url, InputStream in) {
        try {
            this.failtimes = 0;
            byte[] temp = new byte[this.mPacketLength];
            Packet packetInfo = new Packet(this, null);
            packetInfo.total = in.available();
            getNextPacket(in, packetInfo, temp);
            this.mUploadedLength = 0;
            sendUploadStartMessage(handler, packetInfo.total);
            while (!this.uploadFileFinish && !this.cancel) {
                uploadPacket(url, packetInfo);
                sendUploadProcessMessage(handler, this.mUploadedLength);
                processUploadResult(handler, in, content, packetInfo, temp);
            }
            if (!this.uploadFileSuccess) {
                return sendSendReceiveErrorMessage(handler, Value.Command_Backup_File);
            }
            in.close();
            deleteUploadFile(content);
            return sendUploadFinishMessage(handler);
        } catch (Exception e) {
            return sendSendReceiveErrorMessage(handler, Value.Command_Backup_File);
        }
    }

    private int processUploadResult(Handler handler, InputStream in, ContentValues content, Packet packetInfo, byte[] temp) {
        byte[] response = this.httpHandler.getResponsebytes();
        if (response == null) {
            this.failtimes++;
            if (this.failtimes > 3) {
                setUploadStatus(false);
            }
            return sendSendReceiveErrorMessage(handler, Value.Command_Backup_File);
        }
        ResponseFilter filter = new ResponseFilter();
        String info = new String(Security.decrypt(response));
        try {
            CommonMethod.logDebug(AntiLostCommon.DELETE_CONTACT, "ContactProcessor : processUploadResult : " + info);
            Xml.parse(info, filter);
        } catch (SAXException e) {
            if (!(e instanceof SAXUserEndException)) {
                return sendSAXExceptionMessage(handler, Value.Command_Backup_File, info);
            }
        }
        if (filter.getCommand() == 909) {
            setUploadStatus(false);
        } else if (filter.getCommand() == 903) {
            UploadInfoHandler uploadInfoHandler = new UploadInfoHandler();
            try {
                Xml.parse(info, uploadInfoHandler);
                if (uploadInfoHandler.getThisTimeStatus()) {
                    this.failtimes = 0;
                    if (!uploadInfoHandler.getTotalStatus()) {
                        packetInfo.start = packetInfo.end + 1;
                        getNextPacket(in, packetInfo, temp);
                    } else {
                        setUploadStatus(true);
                    }
                } else {
                    this.failtimes++;
                    if (this.failtimes > 3) {
                        setUploadStatus(false);
                    }
                }
            } catch (SAXException e2) {
                return sendSAXExceptionMessage(handler, Value.Command_Backup_File, info);
            }
        } else {
            setUploadStatus(false);
        }
        return 10;
    }

    private void setUploadStatus(boolean status) {
        this.uploadFileFinish = true;
        this.uploadFileSuccess = status;
    }

    private void uploadPacket(URL url, Packet packetInfo) {
        if (packetInfo.content.length() > 0) {
            this.mUploadedLength += packetInfo.content.length();
            this.httpHandler.uploadFile(packetInfo.content.toByteArray(), url, packetInfo.getRange());
            return;
        }
        this.uploadFileFinish = true;
    }

    private int downloadFile(ContentValues content, Handler handler, URL url, OutputStream fos) {
        this.mDownloadedLength = 0;
        int downloadStart = 0;
        int downloadPacketFailTimes = 0;
        int mVcardFileLength = Integer.valueOf(content.getAsString(Value.VCardFileLength)).intValue();
        if (mVcardFileLength < this.mPacketLength) {
            this.mPacketLength = mVcardFileLength;
        }
        sendDownloadStartMessage(handler, mVcardFileLength);
        this.mDownloading = true;
        sendDownloadProcessMessage(handler, mVcardFileLength);
        while (!this.cancel) {
            try {
                byte[] res = downloadPacket(url, " bytes=" + downloadStart + "-" + ((this.mPacketLength + downloadStart) - 1));
                if (res == null || res.length != this.mPacketLength) {
                    downloadPacketFailTimes++;
                    if (downloadPacketFailTimes > 3) {
                        return sendSendReceiveErrorMessage(handler, Value.Command_DownloadFile);
                    }
                } else {
                    downloadStart += this.mPacketLength;
                    downloadPacketFailTimes = 0;
                    fos.write(res);
                    fos.flush();
                    this.mDownloadedLength += this.mPacketLength;
                    if (this.mDownloadedLength == mVcardFileLength) {
                        fos.close();
                        sendDownloadFinishMessage(handler);
                        this.mDownloading = false;
                        return 10;
                    } else if (this.mDownloadedLength < mVcardFileLength) {
                        if (mVcardFileLength - this.mDownloadedLength < this.mPacketLength) {
                            this.mPacketLength = mVcardFileLength - this.mDownloadedLength;
                        } else {
                            this.mPacketLength = 10240;
                        }
                    }
                }
            } catch (Exception e) {
                return sendSendReceiveErrorMessage(handler, Value.Command_DownloadFile);
            } finally {
                this.mDownloading = false;
            }
        }
        fos.close();
        this.mDownloading = false;
        return 16;
    }

    private byte[] downloadPacket(URL url, String range) {
        this.httpHandler.downloadPackt(url, range);
        return this.httpHandler.getResponsebytes();
    }

    public synchronized int process(int command, Handler handler, ContentValues content) {
        int processResult;
        Proxy proxy = NqUtil.getApnProxy(this.context);
        if (proxy != null) {
            this.httpHandler.setProxy(proxy);
        } else {
            this.httpHandler.NoProxy();
        }
        switch (command) {
            case Value.Command_Backup_File:
                Request request901 = new Request(content, this.context);
                request901.setCommand("901");
                processResult = processResult(this.httpHandler.sendContactServerRequest(request901.getRequestBytes()), Value.Command_Backup_File, content, handler);
                break;
            case Value.Command_DownloadFile:
                Request request902 = new Request(content, this.context);
                request902.setCommand("902");
                processResult = processResult(this.httpHandler.sendContactServerRequest(request902.getRequestBytes()), Value.Command_DownloadFile, content, handler);
                break;
            default:
                processResult = 15;
                break;
        }
        return processResult;
    }

    public int processResult(int result, int command, ContentValues content, Handler handler) {
        byte[] response = this.httpHandler.getResponsebytes();
        if (response == null) {
            return sendSendReceiveErrorMessage(handler, command);
        }
        return process(new String(Security.decrypt(response)), command, content, handler);
    }

    public synchronized void next() {
        this.next = true;
        notify();
    }

    public synchronized void cancel() {
        this.next = false;
        notify();
    }

    private int sendSAXExceptionMessage(Handler handler, int command, String parsedInfo) {
        if (handler == null) {
            return 16;
        }
        Message msg = new Message();
        msg.what = 12;
        msg.arg1 = 27;
        msg.obj = this.context.getString(R.string.SEND_RECEIVE_ERROR);
        handler.sendMessage(msg);
        return 16;
    }

    private int sendSendReceiveErrorMessage(Handler handler, int command) {
        Log.d("Error ", "Send receive Error");
        if (handler == null || this.cancel) {
            return 8;
        }
        Message msg = new Message();
        msg.what = 12;
        msg.arg1 = 36;
        handler.sendMessage(msg);
        return 8;
    }

    private int sendFileStoreErrorMessage(Handler handler, int command) {
        if (handler == null) {
            return 17;
        }
        Message msg = new Message();
        msg.what = 12;
        msg.arg1 = 27;
        msg.obj = this.context.getString(R.string.FILE_STORE_ERROR);
        handler.sendMessage(msg);
        return 17;
    }

    private void sendMessage(ContentValues content, Handler handler, int command) {
        if (content.containsKey(Value.MessageCount)) {
            int number = Integer.valueOf(content.getAsString(Value.MessageCount)).intValue();
            Vector<String> vec = new Vector<>();
            for (int i = 0; i < number; i++) {
                vec.add(content.getAsString(XmlTagValue.message + (i + 1)));
                content.remove(XmlTagValue.message + i);
            }
            if (handler != null) {
                Message msg = new Message();
                msg.what = 12;
                msg.arg1 = 27;
                if (vec.size() > 0) {
                    msg.obj = vec.elementAt(0);
                } else {
                    msg.obj = "ErrCode = " + content.getAsString("ErrorCode");
                }
                handler.sendMessage(msg);
            }
        }
    }

    public void setCancel(boolean cancel2) {
        if (cancel2) {
            Log.d("Netprocessor", "cancel call");
        }
        this.cancel = cancel2;
        this.httpHandler.setCancel(cancel2);
    }

    public void setCommand(int command) {
        this.zcommand = command;
    }

    public boolean checkWait() {
        return this.zwait;
    }

    private void getNextPacket(InputStream in, Packet packetInfo, byte[] temp) {
        packetInfo.content.setLength(0);
        try {
            int count = in.read(temp);
            if (count > 0) {
                packetInfo.content.append(temp, 0, count);
                packetInfo.end = (packetInfo.start + count) - 1;
                return;
            }
            in.close();
        } catch (IOException e) {
            setUploadStatus(false);
        }
    }

    private void sendDownloadStartMessage(Handler handler, int length) {
        if (handler != null) {
            Message msg = new Message();
            msg.what = 12;
            msg.arg1 = 21;
            msg.arg2 = length;
            handler.sendMessage(msg);
        }
    }

    private void sendDownloadProcessMessage(final Handler handler, final int length) {
        new Thread(new Runnable() {
            public void run() {
                while (!ContactProcessor.this.cancel && ContactProcessor.this.mDownloadedLength <= length && ContactProcessor.this.mDownloading) {
                    int current = ContactProcessor.this.mDownloadedLength + ContactProcessor.this.httpHandler.getCurrentResponseLength();
                    if (handler != null) {
                        Message msg = new Message();
                        msg.what = 12;
                        msg.arg1 = 29;
                        msg.arg2 = current;
                        handler.sendMessage(msg);
                    }
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    private void sendDownloadFinishMessage(Handler handler) {
        if (handler != null) {
            Message msg = new Message();
            msg.what = 12;
            msg.arg1 = 22;
            handler.sendMessage(msg);
        }
    }

    private void deleteUploadFile(ContentValues content) {
        File file = new File(content.getAsString(Value.UploadFileName));
        if (file.exists()) {
            file.delete();
        }
    }

    private int sendUploadFinishMessage(Handler handler) {
        Message msg = new Message();
        msg.what = 12;
        msg.arg1 = 23;
        handler.sendMessage(msg);
        return 10;
    }

    private void sendUploadStartMessage(Handler handler, int length) {
        if (handler != null) {
            Message msg = new Message();
            msg.what = 12;
            msg.arg1 = 21;
            msg.arg2 = length;
            handler.sendMessage(msg);
        }
    }

    private void sendUploadProcessMessage(Handler handler, int currlen) {
        if (handler != null) {
            Message msg = new Message();
            msg.what = 12;
            msg.arg1 = 29;
            msg.arg2 = currlen;
            handler.sendMessage(msg);
        }
    }
}
