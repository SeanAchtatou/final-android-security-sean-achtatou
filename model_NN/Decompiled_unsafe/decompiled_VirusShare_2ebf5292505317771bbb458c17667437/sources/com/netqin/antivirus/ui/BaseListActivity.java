package com.netqin.antivirus.ui;

import android.app.ListActivity;

public class BaseListActivity extends ListActivity {
    public void setContentView(int layoutResID) {
        requestWindowFeature(1);
        super.setContentView(layoutResID);
    }
}
