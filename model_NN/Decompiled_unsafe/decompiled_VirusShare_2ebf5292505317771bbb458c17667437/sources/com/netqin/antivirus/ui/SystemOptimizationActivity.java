package com.netqin.antivirus.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.netqin.antivirus.NqUtil;
import com.netqin.antivirus.data.Application;
import com.netqin.antivirus.data.PreferenceDataHelper;
import com.netqin.antivirus.log.LogEngine;
import com.netqin.antivirus.packagemanager.SoftwareManageActivity;
import com.netqin.antivirus.taskmanager.TaskList;
import com.netqin.antivirus.util.SystemUtils;
import com.nqmobile.antivirus_ampro20.R;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class SystemOptimizationActivity extends Activity {
    private Button mBtnOneKey;
    /* access modifiers changed from: private */
    public Context mContext;
    private Runnable mKillApp = new Runnable() {
        public void run() {
            if (SystemOptimizationActivity.this.mProgressDialog != null) {
                long availMem1 = SystemUtils.getAvailMem(SystemOptimizationActivity.this.getApplicationContext());
                SystemOptimizationActivity.this.updateProgressMsg(SystemOptimizationActivity.this.getString(R.string.widget_close_app));
                int killCount = SystemUtils.oneKeyKillTask(SystemOptimizationActivity.this);
                try {
                    Thread.sleep(2500);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
                long freeMem = (SystemUtils.getAvailMem(SystemOptimizationActivity.this.getApplicationContext()) / 1048576) - (availMem1 / 1048576);
                if (freeMem < 0) {
                    freeMem = 0;
                }
                final String killResult = SystemOptimizationActivity.this.getString(R.string.one_kill_result, new Object[]{Integer.valueOf(killCount), Long.valueOf(freeMem)});
                SystemOptimizationActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        SystemOptimizationActivity.this.updateRatingBar();
                        final Toast t = Toast.makeText(SystemOptimizationActivity.this.getApplicationContext(), killResult, 1);
                        t.show();
                        new Timer().schedule(new TimerTask() {
                            public void run() {
                                try {
                                    Thread.sleep(2000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                t.show();
                            }
                        }, 10);
                    }
                });
                SystemOptimizationActivity.this.mProgressDialog.dismiss();
                SystemOptimizationActivity.this.playAnimation(SystemOptimizationActivity.this.usedDegreeBefore, SystemOptimizationActivity.this.usedDegreeBefore - ((float) ((((180 * freeMem) * 1024) * 1024) / SystemOptimizationActivity.this.totalMem)));
            }
        }
    };
    private LinearLayout mLayoutRunningProcess;
    private LinearLayout mLayoutSoftManager;
    private ImageView mPointer;
    /* access modifiers changed from: private */
    public ProgressDialog mProgressDialog;
    private TextView mRunningAppNums;
    private ImageView mSummaryRateView;
    private TextView mTitle;
    private TextView mTotleSummaryResult;
    private TextView mTvSummaryResult;
    /* access modifiers changed from: private */
    public long totalMem;
    /* access modifiers changed from: private */
    public float usedDegreeBefore;

    public static Intent getLaunchIntent(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, SystemOptimizationActivity.class);
        return intent;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(1);
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.system_optimization);
        this.mContext = this;
        this.mTitle = (TextView) findViewById(R.id.activity_name);
        this.mTitle.setText((int) R.string.home_system_opt);
        bindView();
        initView();
    }

    private void bindView() {
        this.mSummaryRateView = (ImageView) findViewById(R.id.summary_rate);
        this.mTvSummaryResult = (TextView) findViewById(R.id.summary_result);
        this.mTotleSummaryResult = (TextView) findViewById(R.id.totle_summary_result);
        this.mRunningAppNums = (TextView) findViewById(R.id.running_app_nums);
        this.mBtnOneKey = (Button) findViewById(R.id.btn_onekey);
        this.mBtnOneKey.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                SystemOptimizationActivity.this.onOneKeyDown();
            }
        });
        this.mLayoutRunningProcess = (LinearLayout) findViewById(R.id.running_process);
        this.mLayoutRunningProcess.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                SystemOptimizationActivity.this.startActivity(TaskList.getLaunchIntent(SystemOptimizationActivity.this));
            }
        });
        this.mLayoutSoftManager = (LinearLayout) findViewById(R.id.software_manager);
        this.mLayoutSoftManager.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                SystemOptimizationActivity.this.startActivity(SoftwareManageActivity.getLaunchIntent(SystemOptimizationActivity.this));
            }
        });
        this.mProgressDialog = new ProgressDialog(this);
        this.mProgressDialog.setProgressStyle(0);
    }

    private void initView() {
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        updateRatingBar();
        updateAnimation();
        super.onResume();
    }

    /* access modifiers changed from: private */
    public void updateAnimation() {
        long availMem = SystemUtils.getAvailMem(getApplicationContext());
        this.totalMem = SystemUtils.getTotalMem();
        this.usedDegreeBefore = (float) ((180 * (this.totalMem - availMem)) / this.totalMem);
        long j = (180 * (this.totalMem - availMem)) / this.totalMem;
        playAnimation(0.0f, this.usedDegreeBefore);
    }

    private void onCreateAnimation() {
        long availMem = SystemUtils.getAvailMem(getApplicationContext());
        this.totalMem = SystemUtils.getTotalMem();
        this.usedDegreeBefore = (float) ((180 * (this.totalMem - availMem)) / this.totalMem);
        playAnimation(0.0f, this.usedDegreeBefore);
    }

    /* access modifiers changed from: private */
    public void playAnimation(float fromDegree, float toDegree) {
        this.mPointer = (ImageView) findViewById(R.id.pointer);
        RotateAnimation rotateAnim = new RotateAnimation(fromDegree, toDegree, 2, 0.5f, 1, 0.5f);
        rotateAnim.setDuration(1500);
        rotateAnim.setInterpolator(new LinearInterpolator());
        rotateAnim.setFillAfter(true);
        rotateAnim.setRepeatCount(0);
        rotateAnim.start();
        this.mPointer.setAnimation(rotateAnim);
    }

    /* access modifiers changed from: private */
    public void onOneKeyDown() {
        if (PreferenceDataHelper.isShowOnekeyConf(this.mContext)) {
            new OnekeySettingsDialog(this.mContext) {
                public void startOneKey() {
                    SystemOptimizationActivity.this.startOnekeyTask();
                }
            }.show();
        } else {
            startOnekeyTask();
        }
    }

    /* access modifiers changed from: private */
    public void updateRatingBar() {
        long freeMemory = SystemUtils.getAvailMem(this) / 1048576;
        long totalMemory = SystemUtils.sTotalMemory / 1048576;
        this.mTotleSummaryResult.setText(String.format(getString(R.string.fmt_mem_status2), Long.valueOf(totalMemory - freeMemory), Long.valueOf(totalMemory)));
        this.mRunningAppNums.setText((int) R.string.btn_running_process);
    }

    /* access modifiers changed from: private */
    public void startOnekeyTask() {
        ArrayList<Application> oneKeyKillApps = SystemUtils.getOneKeyKillApps(this.mContext);
        this.mProgressDialog.show();
        new Thread(this.mKillApp).start();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.addvice_feedback_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.updatedb_advice_feedback /*2131558847*/:
                NqUtil.clickAdviceFeedback(this);
                return true;
            default:
                return true;
        }
    }

    /* access modifiers changed from: protected */
    public void updateProgressMsg(final String msg) {
        runOnUiThread(new Runnable() {
            public void run() {
                if (SystemOptimizationActivity.this.mProgressDialog != null) {
                    SystemOptimizationActivity.this.mProgressDialog.setMessage(msg);
                }
            }
        });
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    class OnekeyTask extends TriggermanTask {
        private static final int TASK_ID_ALL_DONE = 8111118;
        private static final int TASK_ID_KILL_APPS_DONE = 8111119;
        private AlertDialog mADialog = null;
        private long mCurrentMemeory = 0;
        private boolean mDoNothing = true;
        ListView mListView = null;
        private boolean mOptCloseBt = false;
        private boolean mOptCloseGprs = false;
        private boolean mOptCloseWifi = false;
        private boolean mOptKillApps = false;

        public OnekeyTask(Context context, ArrayList<Application> apps) {
            this.mContext = context;
            this.mTargets = apps;
            this.mADialog = createOnekeyProccessDialog();
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            this.mDoNothing = true;
            this.mOptKillApps = true;
            if (this.mOptKillApps || this.mOptCloseGprs || this.mOptCloseWifi || this.mOptCloseBt) {
                this.mADialog.setTitle((int) R.string.one_key);
                this.mADialog.setIcon((int) R.drawable.face_crying_32);
                this.mADialog.show();
                this.mCurrentMemeory = SystemUtils.getAvailMem(this.mContext);
                return;
            }
            cancel(true);
        }

        /* access modifiers changed from: protected */
        public Integer doInBackground(Void... args) {
            if (this.mOptKillApps) {
                for (int i = 0; i < this.mTargets.size(); i++) {
                    SystemUtils.killApp(this.mContext, (Application) this.mTargets.get(i));
                    this.mDoNothing = false;
                    publishProgress(new Integer[]{Integer.valueOf(i)});
                }
            }
            publishProgress(new Integer[]{Integer.valueOf((int) TASK_ID_KILL_APPS_DONE)});
            publishProgress(new Integer[]{Integer.valueOf((int) TASK_ID_ALL_DONE)});
            return Integer.valueOf(this.mTargets.size());
        }

        /* access modifiers changed from: protected */
        public final void onProgressUpdate(Integer... args) {
            if (args[0].intValue() == TASK_ID_KILL_APPS_DONE) {
                return;
            }
            if (args[0].intValue() == TASK_ID_ALL_DONE) {
                this.mADialog.getButton(-2).setText((int) R.string.alert_dialog_ok);
                if (this.mDoNothing) {
                    this.mADialog.setTitle(SystemOptimizationActivity.this.mContext.getString(R.string.onekey_result));
                    this.mADialog.setIcon((int) R.drawable.face_laugh_32);
                    OneKeyTaskStatus ts = new OneKeyTaskStatus(OneKeyTaskStatus.STATUS_KEY_REPORT);
                    ts.setDone();
                    ts.setDoneDesc(this.mContext.getString(R.string.onekey_noting_todo));
                    ts.setIconDone(R.drawable.star_on_48);
                    appendTaskStatus(ts);
                    return;
                }
                this.mADialog.setTitle(SystemOptimizationActivity.this.mContext.getString(R.string.onekey_done));
                this.mADialog.setIcon((int) R.drawable.face_laugh_32);
                long deltaMem = SystemUtils.getAvailMem(this.mContext);
                long killApps = (long) getKilledAppsNum();
                if (this.mOptKillApps) {
                    if (killApps > 0) {
                        String report = String.format(SystemOptimizationActivity.this.mContext.getString(R.string.fmt_onekey_report), Long.valueOf(killApps));
                        OneKeyTaskStatus ts2 = new OneKeyTaskStatus(OneKeyTaskStatus.STATUS_KEY_REPORT);
                        ts2.setDone();
                        ts2.setDoneDesc(report);
                        ts2.setIconDone(R.drawable.star_on_48);
                        appendTaskStatus(ts2);
                    }
                    double releaseMem = ((double) (deltaMem - this.mCurrentMemeory)) / 1048576.0d;
                    if (releaseMem > 0.0d) {
                        OneKeyTaskStatus ts3 = new OneKeyTaskStatus(OneKeyTaskStatus.STATUS_KEY_REPORT);
                        ts3.setDone();
                        ts3.setDoneDesc(String.format(SystemOptimizationActivity.this.mContext.getString(R.string.fmt_free_memeory), Integer.valueOf((int) releaseMem)));
                        ts3.setIconDone(R.drawable.star_on_48);
                        appendTaskStatus(ts3);
                        return;
                    }
                    return;
                }
                return;
            }
            onPostKill((Application) this.mTargets.get(args[0].intValue()));
        }

        private OnekeyTaskAdapter getOnekeyTaskAdapter() {
            return (OnekeyTaskAdapter) this.mListView.getAdapter();
        }

        private void appendTaskStatus(OneKeyTaskStatus ts) {
            getOnekeyTaskAdapter().addItem(ts);
            this.mListView.setSelection(getOnekeyTaskAdapter().getCount());
        }

        /* access modifiers changed from: protected */
        public void onPostKill(Application app) {
            OneKeyTaskStatus ts = new OneKeyTaskStatus(app.processName);
            ts.setDoneDesc(String.format(SystemOptimizationActivity.this.mContext.getResources().getString(R.string.close_app), app.getLabelName(this.mContext)));
            ts.setDone();
            appendTaskStatus(ts);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Integer parsedText) {
            SystemOptimizationActivity.this.updateRatingBar();
            SystemOptimizationActivity.this.updateAnimation();
            LogEngine.insertOperationItemLog(27, "", SystemOptimizationActivity.this.getFilesDir().getPath());
            SystemOptimizationActivity.this.mContext.sendBroadcast(new Intent(SystemUtils.ACTION_ONEKEY));
        }

        private final AlertDialog createMessageDialog(int title, int msg) {
            return new AlertDialog.Builder(this.mContext).setTitle(title).setMessage(msg).setPositiveButton((int) R.string.alert_dialog_ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    OnekeyTask.this.cancel(true);
                }
            }).create();
        }

        private final AlertDialog createOnekeyProccessDialog() {
            View view = LayoutInflater.from(this.mContext).inflate((int) R.layout.onekey_process_dialog, (ViewGroup) null);
            this.mListView = (ListView) view.findViewById(R.id.done_list);
            this.mListView.setAdapter((ListAdapter) new OnekeyTaskAdapter(this.mContext));
            return new AlertDialog.Builder(this.mContext).setTitle((int) R.string.one_key).setView(view).setNegativeButton((int) R.string.alert_dialog_cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                }
            }).create();
        }
    }
}
