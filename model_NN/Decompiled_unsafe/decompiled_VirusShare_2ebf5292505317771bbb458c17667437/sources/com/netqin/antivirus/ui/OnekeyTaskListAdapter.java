package com.netqin.antivirus.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import com.netqin.antivirus.data.Application;
import com.netqin.antivirus.data.PreferenceDataHelper;
import com.netqin.antivirus.services.TaskManagerService;
import com.netqin.antivirus.util.AppQueryCondition;
import com.netqin.antivirus.util.TaskManagerUtils;
import com.nqmobile.antivirus_ampro20.R;
import java.util.ArrayList;
import java.util.Iterator;

public class OnekeyTaskListAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<Application> mData;
    private LayoutInflater mInflater;
    private AppQueryCondition mQueryCondition = new AppQueryCondition();

    public OnekeyTaskListAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
        this.mContext = context;
        this.mQueryCondition.appLevel = 3;
        this.mData = TaskManagerService.getRunningApps(context, this.mQueryCondition, true);
        Application self = TaskManagerUtils.getMgSelf(context);
        boolean hasSelf = false;
        Iterator<Application> it = this.mData.iterator();
        while (true) {
            if (it.hasNext()) {
                Application app = it.next();
                if (app.packageName.equals(self.packageName)) {
                    self = app;
                    hasSelf = true;
                    break;
                }
            } else {
                break;
            }
        }
        if (hasSelf) {
            this.mData.remove(self);
        }
        initChecked();
    }

    public synchronized int getCount() {
        return this.mData.size();
    }

    public synchronized Object getItem(int position) {
        return this.mData.get(position);
    }

    public synchronized long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = this.mInflater.inflate((int) R.layout.onekey_settings_task_item, (ViewGroup) null);
            holder = new ViewHolder();
            holder.checkBox = (CheckBox) convertView.findViewById(R.id.checkbox);
            holder.iconView = (ImageView) convertView.findViewById(R.id.icon);
            holder.nameView = (TextView) convertView.findViewById(R.id.name);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final Application app = getApplication(position);
        holder.nameView.setText(app.getLabelName(this.mContext));
        holder.iconView.setImageDrawable(app.getIcon(this.mContext));
        holder.checkBox.setChecked(app.isChecked);
        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (holder.checkBox.isChecked()) {
                    app.isChecked = true;
                } else {
                    app.isChecked = false;
                }
            }
        });
        return convertView;
    }

    public synchronized Application getApplication(int pos) {
        return this.mData.get(pos);
    }

    private void initChecked() {
        Iterator<Application> iterator = this.mData.iterator();
        while (iterator.hasNext()) {
            Application app = iterator.next();
            if (PreferenceDataHelper.isInWhiteList(this.mContext, app.packageName) || TaskManagerUtils.isNgApp(this.mContext, app)) {
                app.isChecked = false;
            } else {
                app.isChecked = true;
            }
        }
    }

    static class ViewHolder {
        CheckBox checkBox;
        ImageView iconView;
        TextView nameView;

        ViewHolder() {
        }
    }
}
