package com.netqin.antivirus.ui;

import SHcMjZX.DD02zqNU;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.netqin.antivirus.cloud.apkinfo.domain.Rate;
import com.netqin.antivirus.cloud.model.CloudHandler;
import com.netqin.antivirus.cloud.model.Uploader;
import com.netqin.antivirus.cloud.model.xml.CloudResponse;
import com.netqin.antivirus.cloud.view.CloudListener;
import com.netqin.antivirus.packagemanager.ApkInfoActivity;
import com.netqin.antivirus.privatesoft.VisitPrivacy;
import com.nqmobile.antivirus_ampro20.R;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class ReportApkActivity extends Activity implements CloudListener {
    private static final String CACHE_AUTO_UPLOAD = "/upload_info.txt";
    private static final int DIALOG_NEED_UPLOAD = 3;
    private static final int DIALOG_SECOND_CONNECT = 2;
    public static final int MSG_1 = 1;
    public static final int MSG_2 = 2;
    public static final int MSG_3 = 3;
    public static final int MSG_4 = 4;
    private static final int UPLODER = 10;
    private Drawable apkIcon;
    private String apkName;
    /* access modifiers changed from: private */
    public String apkPath;
    private String apkVersionCode;
    private String apkVersionName;
    /* access modifiers changed from: private */
    public boolean isFirstReporting = false;
    /* access modifiers changed from: private */
    public boolean isReporting = false;
    private ImageView mApkIcon;
    private TextView mApkName;
    private TextView mApkVersion;
    private Button mBtnBottomLeft;
    private Button mBtnBottomRight;
    Handler mHandler;
    private String mPackageName;
    private RadioGroup mRadioGroup;
    private String mServerId;

    /* access modifiers changed from: private */
    public void commitRating(String m) {
        Rate rate = new Rate();
        rate.setPkgName(this.mPackageName);
        rate.setId("0");
        rate.setServerId(this.mServerId);
        rate.setName(this.apkName);
        rate.setVersionCode(this.apkVersionCode);
        rate.setVersionName(this.apkVersionName);
        rate.setReason(m);
        rate.setFileName("cert.rsa");
        rate.setRef("0");
        CloudResponse.pakName = rate.getPkgName();
        CloudResponse.handler = new Handler();
        CloudHandler.rate = rate;
        CloudHandler.getInstance(this).resetData();
        CloudHandler.getInstance(this).start(this, 1, this.mHandler, true);
    }

    public static Intent getLaunchIntent(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, ReportApkActivity.class);
        return intent;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(1);
        super.onCreate(savedInstanceState);
        setRequestedOrientation(1);
        setContentView((int) R.layout.report_software);
        ((TextView) findViewById(R.id.activity_name)).setText((int) R.string.report_apk);
        bindView();
        initView();
    }

    private void initView() {
        Bundle bundle = getIntent().getExtras();
        this.mPackageName = bundle.getString("package_name");
        this.apkName = bundle.getString("apk_name");
        this.apkVersionName = bundle.getString("apk_version");
        this.mServerId = bundle.getString("apk_serverId");
        try {
            PackageInfo info = DD02zqNU.DLLIxskak(getPackageManager(), this.mPackageName, 0);
            this.apkPath = info.applicationInfo.publicSourceDir;
            this.apkIcon = info.applicationInfo.loadIcon(getPackageManager());
            this.apkVersionName = info.versionName;
            this.apkVersionCode = new StringBuilder(String.valueOf(info.versionCode)).toString();
            this.mApkIcon.setImageDrawable(this.apkIcon);
            this.mApkName.setText(this.apkName);
            this.mApkVersion.setText(this.apkVersionName);
        } catch (PackageManager.NameNotFoundException e) {
        }
        this.mHandler = new Handler() {
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 1:
                        VisitPrivacy.dismissLoading();
                        Bundle b = msg.getData();
                        if (b != null && ReportApkActivity.this.isFirstReporting) {
                            String mApkIsNeed = b.getString("apkIsNeed");
                            String mReportUrl = b.getString("reportUrl");
                            if (mApkIsNeed.equalsIgnoreCase("true") && mReportUrl != null) {
                                ReportApkActivity.this.upLoadApk(mReportUrl);
                            }
                        }
                        ReportApkActivity.this.isFirstReporting = false;
                        break;
                    case 4:
                        Toast.makeText(ReportApkActivity.this, ReportApkActivity.this.getString(R.string.report_failed), 0).show();
                        break;
                    case 5:
                        Toast.makeText(ReportApkActivity.this, ReportApkActivity.this.getString(R.string.report_failed), 0).show();
                        break;
                    case 9:
                        ReportApkActivity.this.isReporting = false;
                        VisitPrivacy.dismissLoading();
                        if (!((Boolean) msg.obj).booleanValue()) {
                            Toast.makeText(ReportApkActivity.this, ReportApkActivity.this.getString(R.string.report_failed), 1).show();
                            ReportApkActivity.this.finish();
                            break;
                        } else {
                            Toast.makeText(ReportApkActivity.this, ReportApkActivity.this.getString(R.string.report_success), 1).show();
                            ReportApkActivity.this.finish();
                            break;
                        }
                    case 10:
                        if (msg.obj != null) {
                            String url = msg.obj.toString();
                            Uploader.reCraeteFile(ReportApkActivity.this.cache_auto_upload_path(), ReportApkActivity.this.apkPath.getBytes());
                            Uploader.getInstance(ReportApkActivity.this).startUploadFile(null, url, ReportApkActivity.this.mHandler);
                            break;
                        } else {
                            Toast.makeText(ReportApkActivity.this, ReportApkActivity.this.getString(R.string.report_failed), 1).show();
                            VisitPrivacy.dismissLoading();
                            ReportApkActivity.this.finish();
                            break;
                        }
                }
                super.handleMessage(msg);
            }
        };
    }

    private void bindView() {
        this.mApkIcon = (ImageView) findViewById(R.id.apk_icon);
        this.mApkName = (TextView) findViewById(R.id.apk_name);
        this.mApkVersion = (TextView) findViewById(R.id.apk_version);
        this.mBtnBottomLeft = (Button) findViewById(R.id.btn_bottomleft);
        this.mBtnBottomRight = (Button) findViewById(R.id.btn_bottomright);
        this.mBtnBottomLeft.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String s = ReportApkActivity.this.getCheckedButton();
                if (s != null) {
                    ReportApkActivity.this.commitRating(s);
                    ReportApkActivity.this.isFirstReporting = true;
                    VisitPrivacy.showLoading(ReportApkActivity.this, ReportApkActivity.this.getString(R.string.report_apk), ReportApkActivity.this.getString(R.string.connecting_server));
                }
            }
        });
        this.mBtnBottomRight.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ReportApkActivity.this.finish();
            }
        });
        this.mRadioGroup = (RadioGroup) findViewById(R.id.rg_soft_type);
        this.mRadioGroup.clearCheck();
    }

    /* access modifiers changed from: private */
    public String getCheckedButton() {
        switch (this.mRadioGroup.getCheckedRadioButtonId()) {
            case -1:
                Toast.makeText(this, getString(R.string.choice_report_reason), 0).show();
                return null;
            case R.id.rd_pay /*2131558715*/:
                return "rd_pay";
            case R.id.rd_uninstall /*2131558716*/:
                return "rd_uninstall";
            case R.id.rd_plugin /*2131558717*/:
                return "rd_plugin";
            case R.id.rd_auto_proc /*2131558718*/:
                return "rd_auto_proc";
            case R.id.rd_auto_net /*2131558719*/:
                return "rd_auto_net";
            case R.id.rd_consume_res /*2131558720*/:
                return "rd_consume_res";
            case R.id.rd_consume_power /*2131558721*/:
                return "rd_consume_power";
            case R.id.rd_good_safe /*2131558722*/:
                return "rd_good_safe";
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 2:
                return new AlertDialog.Builder(this).setTitle((int) R.string.report_apk).setMessage((int) R.string.connecting_wait).create();
            case 3:
                return new AlertDialog.Builder(this).setTitle((int) R.string.report_apk).setMessage((int) R.string.connecting_load).setPositiveButton((int) R.string.alert_dialog_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                }).setNegativeButton((int) R.string.alert_dialog_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                }).create();
            default:
                return null;
        }
    }

    public String cache_auto_upload_path() {
        return getFilesDir() + CACHE_AUTO_UPLOAD;
    }

    /* access modifiers changed from: private */
    public void upLoadApk(final String uploderUrl) {
        IOException e;
        String fileSize = null;
        try {
            try {
                fileSize = String.valueOf((String.valueOf((((double) new FileInputStream(new File(this.apkPath)).available()) / 1000.0d) / 1000.0d) + "M").substring(0, 4).replace("M", "")) + "M";
            } catch (IOException e2) {
                e = e2;
                e.printStackTrace();
                new AlertDialog.Builder(this).setTitle(getString(R.string.netqin_report_apk)).setCancelable(false).setMessage(getString(R.string.netqin_report_apk_content, new Object[]{fileSize})).setPositiveButton(getString(R.string.netqin_report_apk_uploder), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                        if (ApkInfoActivity.checkNetInfo(ReportApkActivity.this)) {
                            ReportApkActivity.this.isReporting = true;
                            VisitPrivacy.showLoading(ReportApkActivity.this, ReportApkActivity.this.getString(R.string.report_apk), ReportApkActivity.this.getString(R.string.uploading_wait));
                            Message message = new Message();
                            message.what = 10;
                            if (uploderUrl != null) {
                                message.obj = uploderUrl;
                                ReportApkActivity.this.mHandler.sendMessage(message);
                                return;
                            }
                            return;
                        }
                        Toast.makeText(ReportApkActivity.this, ReportApkActivity.this.getString(R.string.SEND_RECEIVE_ERROR), 1).show();
                    }
                }).setNegativeButton(getString(R.string.label_cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }).setCancelable(true).show();
            }
        } catch (IOException e3) {
            e = e3;
            e.printStackTrace();
            new AlertDialog.Builder(this).setTitle(getString(R.string.netqin_report_apk)).setCancelable(false).setMessage(getString(R.string.netqin_report_apk_content, new Object[]{fileSize})).setPositiveButton(getString(R.string.netqin_report_apk_uploder), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialoginterface, int i) {
                    if (ApkInfoActivity.checkNetInfo(ReportApkActivity.this)) {
                        ReportApkActivity.this.isReporting = true;
                        VisitPrivacy.showLoading(ReportApkActivity.this, ReportApkActivity.this.getString(R.string.report_apk), ReportApkActivity.this.getString(R.string.uploading_wait));
                        Message message = new Message();
                        message.what = 10;
                        if (uploderUrl != null) {
                            message.obj = uploderUrl;
                            ReportApkActivity.this.mHandler.sendMessage(message);
                            return;
                        }
                        return;
                    }
                    Toast.makeText(ReportApkActivity.this, ReportApkActivity.this.getString(R.string.SEND_RECEIVE_ERROR), 1).show();
                }
            }).setNegativeButton(getString(R.string.label_cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            }).setCancelable(true).show();
        }
        new AlertDialog.Builder(this).setTitle(getString(R.string.netqin_report_apk)).setCancelable(false).setMessage(getString(R.string.netqin_report_apk_content, new Object[]{fileSize})).setPositiveButton(getString(R.string.netqin_report_apk_uploder), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
                if (ApkInfoActivity.checkNetInfo(ReportApkActivity.this)) {
                    ReportApkActivity.this.isReporting = true;
                    VisitPrivacy.showLoading(ReportApkActivity.this, ReportApkActivity.this.getString(R.string.report_apk), ReportApkActivity.this.getString(R.string.uploading_wait));
                    Message message = new Message();
                    message.what = 10;
                    if (uploderUrl != null) {
                        message.obj = uploderUrl;
                        ReportApkActivity.this.mHandler.sendMessage(message);
                        return;
                    }
                    return;
                }
                Toast.makeText(ReportApkActivity.this, ReportApkActivity.this.getString(R.string.SEND_RECEIVE_ERROR), 1).show();
            }
        }).setNegativeButton(getString(R.string.label_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        }).setCancelable(true).show();
    }

    public void process(Bundle bundle) {
    }

    public void complete(int resultCode) {
    }

    public void error(int errorCode) {
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode != 4 || !this.isReporting) && (keyCode != 4 || !this.isFirstReporting)) {
            return super.onKeyDown(keyCode, event);
        }
        Uploader.getInstance(this).setStop(true);
        this.isFirstReporting = false;
        return true;
    }
}
