package com.netqin.antivirus.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.os.Bundle;
import android.preference.PreferenceCategory;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.netqin.antivirus.filemanager.FileManagerActivity;
import com.netqin.antivirus.networkmanager.HandlerContainer;
import com.netqin.antivirus.networkmanager.NetApplication;
import com.netqin.antivirus.networkmanager.SettingPreferences;
import com.netqin.antivirus.networkmanager.model.Counter;
import com.netqin.antivirus.networkmanager.model.DatabaseHelper;
import com.netqin.antivirus.networkmanager.model.Device;
import com.netqin.antivirus.networkmanager.model.IModel;
import com.netqin.antivirus.networkmanager.model.IModelListener;
import com.netqin.antivirus.networkmanager.model.IOperation;
import com.netqin.antivirus.networkmanager.model.Interface;
import com.netqin.antivirus.networkmanager.model.NetMeterModel;
import com.netqin.antivirus.packagemanager.ApkInfoActivity;
import com.netqin.antivirus.packagemanager.SoftwareManageActivity;
import com.nqmobile.antivirus_ampro20.R;
import java.util.Calendar;

public class NetActivity extends Activity implements IModelListener, IOperation {
    long[] cell;
    private NetApplication mApp;
    private HandlerContainer mContainer;
    private Counter mCounter;
    private TextView mGprsRemain;
    private TextView mGprsTotal;
    private SQLiteOpenHelper mHelper;
    private NetMeterModel mModel = null;
    protected long mNow = 0;
    private TextView mTrfficTodayIs;
    private TextView mTrfficTodayUse;
    private PackageManager pm;
    private ProgressBar progress;
    PreferenceCategory rrr;
    long[] wifi;

    public static Intent getLaunchIntent(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, NetActivity.class);
        return intent;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(1);
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.net);
        this.mApp = (NetApplication) getApplication();
        this.mModel = (NetMeterModel) this.mApp.getAdapter(NetMeterModel.class);
        this.mContainer = (HandlerContainer) this.mApp.getAdapter(HandlerContainer.class);
        bindView();
        initView();
    }

    private void bindView() {
        this.progress = (ProgressBar) findViewById(R.id.progress_horizontal);
        this.mGprsTotal = (TextView) findViewById(R.id.meter_gprs_total);
        this.mGprsRemain = (TextView) findViewById(R.id.meter_gprs_remain);
        this.mTrfficTodayIs = (TextView) findViewById(R.id.traffic_today_is);
        this.mTrfficTodayUse = (TextView) findViewById(R.id.traffic_today_use);
    }

    private void initView() {
        Calendar now = Calendar.getInstance();
        this.mTrfficTodayIs.setText("今天是：" + DatabaseHelper.getDate(now));
        this.mHelper = new DatabaseHelper(this);
        SQLiteDatabase db = this.mHelper.getWritableDatabase();
        SQLiteQueryBuilder query = new SQLiteQueryBuilder();
        query.setTables(DatabaseHelper.DailyCounter.TABLE_NAME);
        StringBuilder sb = new StringBuilder("interface='pdp0'");
        sb.append(" AND ");
        sb.append("day");
        sb.append("='");
        sb.append(DatabaseHelper.getDate(now));
        sb.append("'");
        Cursor c = null;
        try {
            c = query.query(db, null, sb.toString(), null, null, null, null);
            if (c != null && c.getCount() > 0) {
                c.moveToFirst();
                this.mTrfficTodayUse.setText("今日已用流量：" + Counter.prettyBytes(c.getLong(c.getColumnIndex(DatabaseHelper.DailyCounter.RX)) + c.getLong(c.getColumnIndex(DatabaseHelper.DailyCounter.TX))));
            }
        } catch (Exception e) {
        } finally {
            db.close();
            c.close();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        NetApplication.setUpdatePolicy(2);
        ((NetApplication) getApplication()).startService();
        this.mModel.addModelListener(this);
        this.mModel.addOperationListener(this);
        if (this.mModel.isLoaded()) {
            updateUI();
        }
        updateTrafficToday();
    }

    private void updateTrafficToday() {
        Calendar now = Calendar.getInstance();
        this.mTrfficTodayIs.setText("今天是：" + DatabaseHelper.getDate(now));
        this.mHelper = new DatabaseHelper(this);
        SQLiteDatabase db = this.mHelper.getWritableDatabase();
        SQLiteQueryBuilder query = new SQLiteQueryBuilder();
        query.setTables(DatabaseHelper.DailyCounter.TABLE_NAME);
        StringBuilder sb = new StringBuilder("interface='pdp0'");
        sb.append(" AND ");
        sb.append("day");
        sb.append("='");
        sb.append(DatabaseHelper.getDate(now));
        sb.append("'");
        Cursor c = null;
        try {
            c = query.query(db, null, sb.toString(), null, null, null, null);
            if (c != null && c.getCount() > 0) {
                c.moveToFirst();
                this.mTrfficTodayUse.setText("今日已用流量：" + Counter.prettyBytes(c.getLong(c.getColumnIndex(DatabaseHelper.DailyCounter.RX)) + c.getLong(c.getColumnIndex(DatabaseHelper.DailyCounter.TX))));
            }
        } catch (Exception e) {
        } finally {
            db.close();
            c.close();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 1, 0, "Setting");
        menu.add(0, 2, 0, "System optimization");
        menu.add(0, 3, 0, "Software manage");
        menu.add(0, 4, 0, "Log manage");
        menu.add(0, 5, 0, "file manage");
        menu.add(0, 6, 0, "Apk manager");
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        SettingPreferences.getLaunchIntent(this);
        switch (item.getItemId()) {
            case 1:
                startActivity(SettingPreferences.getLaunchIntent(this));
                return true;
            case 2:
                startActivity(SystemOptimizationActivity.getLaunchIntent(this));
                return true;
            case 3:
                startActivity(SoftwareManageActivity.getLaunchIntent(this));
                return true;
            case 4:
            default:
                return true;
            case 5:
                startActivity(FileManagerActivity.getLaunchIntent(this));
                return true;
            case 6:
                startActivity(ApkInfoActivity.getLaunchIntent(this));
                return true;
        }
    }

    public void modelChanged(IModel object) {
        runOnUiThread(new Runnable() {
            public void run() {
                NetActivity.this.updateUI();
            }
        });
    }

    public void modelLoaded(IModel object) {
        runOnUiThread(new Runnable() {
            public void run() {
                NetActivity.this.updateUI();
            }
        });
    }

    public void operationEnded() {
        runOnUiThread(new Runnable() {
            public void run() {
                NetActivity.this.getWindow().setFeatureInt(5, -2);
            }
        });
    }

    public void operationStarted() {
        runOnUiThread(new Runnable() {
            public void run() {
                NetActivity.this.getWindow().setFeatureInt(5, -1);
            }
        });
    }

    private void getMeterTraffic() {
        this.wifi = new long[2];
        this.cell = new long[2];
        Device device = Device.getDevice();
        for (Interface inter : this.mModel.getInterfaces()) {
            for (Counter counter : inter.getCounters()) {
                if (counter.getType() == 0) {
                    long[] bytes = counter.getBytes();
                    if (device.isCell(inter.getName())) {
                        long[] jArr = this.cell;
                        jArr[0] = jArr[0] + bytes[0];
                        long[] jArr2 = this.cell;
                        jArr2[1] = jArr2[1] + bytes[1];
                    } else if (device.isWiFi(inter.getName())) {
                        long[] jArr3 = this.wifi;
                        jArr3[0] = jArr3[0] + bytes[0];
                        long[] jArr4 = this.wifi;
                        jArr4[1] = jArr4[1] + bytes[1];
                    }
                }
            }
        }
    }

    public void updateUI() {
        long used;
        getMeterTraffic();
        SharedPreferences preferences = (SharedPreferences) this.mApp.getAdapter(SharedPreferences.class);
        String s = preferences.getString(SettingPreferences.KEY_METER_THRESHOLD, "");
        try {
            Double.parseDouble(s);
        } catch (NumberFormatException e) {
            s = NetApplication.DEFAULT_THRESHOLD;
            preferences.edit().putString(SettingPreferences.KEY_METER_THRESHOLD, s).commit();
        }
        long threshold = (long) (Double.parseDouble(s) * 1024.0d * 1024.0d);
        if (this.cell != null) {
            used = this.cell[0] + this.cell[1];
        } else {
            used = 0;
        }
        long p = threshold != 0 ? (100 * used) / threshold : 0;
        this.progress.setMax(100);
        this.progress.setProgress((int) p);
        this.mGprsTotal.setText(getString(R.string.meter_gprs_set, new Object[]{s}));
        long remain = threshold - used;
        if (remain < 0) {
            this.mGprsRemain.setText(getString(R.string.meter_gprs_set_overload, new Object[]{Counter.prettyBytes(-remain)}));
        } else {
            this.mGprsRemain.setText(getString(R.string.meter_gprs_set_remain, new Object[]{Counter.prettyBytes(remain)}));
        }
        updateTrafficToday();
    }
}
