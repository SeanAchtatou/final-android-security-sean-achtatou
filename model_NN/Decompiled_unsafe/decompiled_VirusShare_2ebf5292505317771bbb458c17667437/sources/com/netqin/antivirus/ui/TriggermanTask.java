package com.netqin.antivirus.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;
import com.netqin.antivirus.data.Application;
import com.netqin.antivirus.util.SystemUtils;
import com.nqmobile.antivirus_ampro20.R;
import java.util.ArrayList;

public class TriggermanTask extends AsyncTask<Void, Integer, Integer> {
    protected static final String TAG = "TriggermanTask";
    private long mAvailMem1;
    protected Context mContext = null;
    protected ProgressDialog mDialog = null;
    protected int mMaxProgress = 0;
    protected ArrayList<Application> mTargets = null;

    public TriggermanTask() {
    }

    public TriggermanTask(Context context, ArrayList<Application> apps, ProgressDialog dialog) {
        this.mTargets = apps;
        this.mContext = context;
        this.mMaxProgress = apps.size();
        this.mDialog = dialog;
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        this.mDialog.setProgress(0);
        this.mDialog.setMax(this.mMaxProgress);
        this.mDialog.show();
        this.mAvailMem1 = SystemUtils.getAvailMem(this.mContext);
    }

    /* access modifiers changed from: protected */
    public Integer doInBackground(Void... args) {
        for (int i = 0; i < this.mTargets.size(); i++) {
            SystemUtils.killApp(this.mContext, this.mTargets.get(i));
            publishProgress(Integer.valueOf(i));
        }
        return Integer.valueOf(this.mTargets.size());
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(Integer... args) {
        this.mDialog.setProgress(args[0].intValue());
        this.mDialog.setMessage(buildProgressMessage(args[0].intValue()));
        onPostKill(this.mTargets.get(args[0].intValue()));
    }

    /* access modifiers changed from: protected */
    public String buildProgressMessage(int i) {
        try {
            return String.format(this.mContext.getString(R.string.dialog_msg_killing), this.mTargets.get(i).labelName);
        } catch (Exception e) {
            return "";
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Integer parsedText) {
        this.mDialog.dismiss();
        int killCount = this.mTargets.size();
        long freeMem = (SystemUtils.getAvailMem(this.mContext) / 1048576) - (this.mAvailMem1 / 1048576);
        if (freeMem < 0) {
            freeMem = 0;
        }
        Toast.makeText(this.mContext, this.mContext.getString(R.string.one_kill_result, Integer.valueOf(killCount), Long.valueOf(freeMem)), 0).show();
    }

    /* access modifiers changed from: protected */
    public ProgressDialog getProgressDialog() {
        return this.mDialog;
    }

    /* access modifiers changed from: protected */
    public void onPostKill(Application app) {
    }

    /* access modifiers changed from: protected */
    public int getKilledAppsNum() {
        if (this.mTargets == null) {
            return 0;
        }
        return this.mTargets.size();
    }
}
