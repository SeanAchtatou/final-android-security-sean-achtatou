package com.netqin.antivirus.ui;

import android.app.Activity;

public class BaseActivity extends Activity {
    public void setContentView(int layoutResID) {
        requestWindowFeature(1);
        super.setContentView(layoutResID);
    }
}
