package com.netqin.antivirus.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.netqin.antivirus.data.Application;
import com.netqin.antivirus.data.PreferenceDataHelper;
import com.netqin.antivirus.ui.OnekeyTaskListAdapter;
import com.netqin.antivirus.util.DevConfig;
import com.netqin.antivirus.util.SystemUtils;
import com.nqmobile.antivirus_ampro20.R;

public abstract class OnekeySettingsDialog extends Dialog {
    public static final String KEY_ENBABLE_CTL_APPS = "enable_ctl_apps";
    protected OnekeyTaskListAdapter mAdapter;
    /* access modifiers changed from: private */
    public CheckBox mCbDontShowConf;
    /* access modifiers changed from: private */
    public CheckBox mCbMemory;
    /* access modifiers changed from: private */
    public Context mContext;
    private LinearLayout mDontShowConf;
    SharedPreferences.Editor mEditor = null;
    protected LinearLayout mListPanel;
    protected ListView mListView;
    SharedPreferences mPreferencesData = null;
    private Button mStartOnekey;
    private ImageView mToggleButton;
    private TextView mTxRunningApps;

    public abstract void startOneKey();

    public OnekeySettingsDialog(Context context) {
        super(context);
        this.mContext = context;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.onekey_settings);
        setTitle((int) R.string.onekey_dialog_title);
        this.mPreferencesData = this.mContext.getSharedPreferences(DevConfig.GLOBAL_SETTINGS_PREFERENCE, 0);
        this.mEditor = this.mPreferencesData.edit();
        this.mCbMemory = (CheckBox) findViewById(R.id.cb_memory);
        findViewById(R.id.memory_item).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                OnekeySettingsDialog.this.mCbMemory.performClick();
            }
        });
        initListPanel();
        this.mStartOnekey = (Button) findViewById(R.id.start_onekey);
        this.mStartOnekey.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                OnekeySettingsDialog.this.dismiss();
                if (OnekeySettingsDialog.this.mCbMemory.isChecked()) {
                    OnekeySettingsDialog.this.updateWhiteList(OnekeySettingsDialog.this.mContext);
                }
                OnekeySettingsDialog.this.updateSwitchPreference(OnekeySettingsDialog.this.mContext);
                OnekeySettingsDialog.this.startOneKey();
            }
        });
        this.mCbDontShowConf = (CheckBox) findViewById(R.id.cb_dont_show_onekey_conf);
        this.mCbDontShowConf.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PreferenceDataHelper.setShowOnekeyConf(OnekeySettingsDialog.this.mContext, !OnekeySettingsDialog.this.mCbDontShowConf.isChecked());
            }
        });
        this.mDontShowConf = (LinearLayout) findViewById(R.id.dont_show_onekey_conf);
        this.mDontShowConf.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                OnekeySettingsDialog.this.mCbDontShowConf.performClick();
            }
        });
        this.mCbMemory.setChecked(this.mPreferencesData.getBoolean(KEY_ENBABLE_CTL_APPS, true));
    }

    private void initListPanel() {
        this.mListPanel = (LinearLayout) findViewById(R.id.list_panel);
        this.mAdapter = new OnekeyTaskListAdapter(this.mContext);
        if (this.mListPanel.getChildCount() > 1) {
            this.mListPanel.removeViews(1, this.mListPanel.getChildCount() - 1);
        }
        this.mListPanel.setVisibility(0);
        this.mListView = new ListView(this.mContext);
        this.mListView.setLayoutParams(new ViewGroup.LayoutParams(-1, this.mAdapter.getCount() * SystemUtils.getPixByDip(50)));
        this.mListView.setDivider(this.mContext.getResources().getDrawable(17301524));
        this.mListView.setDividerHeight(1);
        this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                ((OnekeyTaskListAdapter.ViewHolder) view.getTag()).checkBox.performClick();
            }
        });
        this.mListView.setAdapter((ListAdapter) this.mAdapter);
        this.mListPanel.addView(this.mListView);
    }

    /* access modifiers changed from: protected */
    public void updateSwitchPreference(Context mContext2) {
        this.mEditor.putBoolean(KEY_ENBABLE_CTL_APPS, this.mCbMemory.isChecked());
        this.mEditor.commit();
    }

    /* access modifiers changed from: protected */
    public void updateWhiteList(Context context) {
        if (this.mAdapter != null && this.mAdapter.getCount() > 0) {
            int appCount = this.mAdapter.getCount();
            for (int i = 0; i < appCount; i++) {
                Application app = this.mAdapter.getApplication(i);
                if (!app.isChecked) {
                    PreferenceDataHelper.addWhiteApp(context, app.packageName);
                } else if (PreferenceDataHelper.isInWhiteList(context, app.packageName)) {
                    PreferenceDataHelper.removeWhiteApp(context, app.packageName);
                }
            }
        }
    }
}
