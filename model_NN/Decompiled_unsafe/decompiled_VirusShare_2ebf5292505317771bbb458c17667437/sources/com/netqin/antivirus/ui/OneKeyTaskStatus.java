package com.netqin.antivirus.ui;

public class OneKeyTaskStatus {
    public static int STATUS_DONE = 2;
    public static int STATUS_ERRO = 3;
    public static String STATUS_KEY_REPORT = ":REPORT";
    public static int STATUS_RUNNING = 1;
    public static int STATUS_START = 0;
    String mDescDone = "done";
    String mDescRunning = "...";
    int mIconDone = -1;
    String mKey = "...";
    int mStatus = STATUS_START;

    public OneKeyTaskStatus(String key) {
        this.mKey = key;
    }

    public void setIconDone(int resId) {
        this.mIconDone = resId;
    }

    public int getIconDone() {
        return this.mIconDone;
    }

    public void setRunningDesc(String runningDesc) {
        this.mDescRunning = runningDesc;
    }

    public void setDoneDesc(String doneDesc) {
        this.mDescDone = doneDesc;
    }

    public String getRunningDesc() {
        return this.mDescRunning;
    }

    public String getDoneDesc() {
        return this.mDescDone;
    }

    public void setStatus(int status) {
        this.mStatus = status;
    }

    public String getKey() {
        return this.mKey;
    }

    public boolean isRunning() {
        return this.mStatus == STATUS_RUNNING;
    }

    public void setRunning() {
        this.mStatus = STATUS_RUNNING;
    }

    public boolean isDone() {
        return this.mStatus == STATUS_DONE;
    }

    public void setDone() {
        this.mStatus = STATUS_DONE;
    }
}
