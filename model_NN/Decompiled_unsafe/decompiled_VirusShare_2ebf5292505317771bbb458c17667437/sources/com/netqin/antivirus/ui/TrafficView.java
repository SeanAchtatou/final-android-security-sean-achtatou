package com.netqin.antivirus.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.util.AttributeSet;
import com.nqmobile.antivirus_ampro20.R;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

public class TrafficView extends TrafficFactory {
    public static Bitmap mSCBitmap = null;
    double[] data = getTraffic(getContext());
    Canvas mCanvas = null;
    Paint mPaint = null;

    public TrafficView(Context context, AttributeSet attrs) {
        super(context, attrs);
        getScreenPixels();
        mSCBitmap = Bitmap.createBitmap(this.mWidth + 60, this.mHeight + 30, Bitmap.Config.ARGB_4444);
        this.mCanvas = new Canvas();
        this.mCanvas.setBitmap(mSCBitmap);
        this.mPaint = new Paint();
        if (this.screenWidth >= 480) {
            this.mCanvas.translate(30.0f, 0.0f);
        } else {
            this.mCanvas.translate(20.0f, 0.0f);
        }
        RectF rectF = new RectF(0.0f, 0.0f, (float) this.mWidth, (float) this.mHeight);
        Paint paint1 = createNewPaint(-7829368, Paint.Style.STROKE, 0.0f, 10.0f);
        Paint paint2 = createNewPaint(-3355444, Paint.Style.FILL, 15.0f, 0.0f);
        Paint paint3 = createNewPaint(-65536, Paint.Style.STROKE, 5.0f, 0.0f);
        Paint paint4 = createNewPaint(-65536, Paint.Style.STROKE, 2.0f, 0.0f);
        paint2.setStrokeJoin(Paint.Join.ROUND);
        paint2.setStrokeCap(Paint.Cap.ROUND);
        this.mCanvas.drawRect(rectF, paint1);
        for (int i = 1; i < 5; i++) {
            this.mCanvas.drawLine(1.0f, (float) (this.height * i), (float) this.mWidth, (float) (this.height * i), paint1);
        }
        for (int i2 = 1; i2 <= 33; i2++) {
            this.mCanvas.drawLine((float) (this.width * i2), 0.0f, (float) (this.width * i2), (float) this.mHeight, paint1);
        }
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        for (int i3 = 27; i3 >= -1; i3--) {
            gregorianCalendar.add(5, -1);
            this.mCanvas.drawText(new SimpleDateFormat("M.dd").format(gregorianCalendar.getTime()), (float) (DATE_X_OFFSET + (this.width * i3)), (float) (this.mHeight + DATE_Y_OFFSET), this.paint);
        }
        GregorianCalendar gregorianCalendar2 = new GregorianCalendar();
        for (int i4 = 0; i4 < 5; i4++) {
            gregorianCalendar2.add(5, 1);
            this.mCanvas.drawText(new SimpleDateFormat("M.dd").format(gregorianCalendar2.getTime()), (float) (DATE_X_OFFSET + (this.width * (i4 + 29))), (float) (this.mHeight + DATE_Y_OFFSET), this.paint);
        }
        this.mCanvas.drawText(getResources().getString(R.string.meter_traffic_today), (float) (DATE_X_OFFSET + (this.width * 28)), (float) (this.mHeight + DATE_Y_OFFSET), this.paint);
        List<Point> points = getPoints(this.data, this.mCanvas);
        Bitmap bitmap = BitmapFactory.decodeStream(getResources().openRawResource(R.drawable.traffic_point));
        Bitmap note = BitmapFactory.decodeStream(getResources().openRawResource(R.drawable.traffic_note));
        if (points.size() == 0) {
            for (int i5 = 0; i5 < 29; i5++) {
                if (this.screenHeight > 480) {
                    this.mCanvas.drawLine((float) (this.width * i5), (float) this.mHeight, (float) (this.width * (i5 + 1)), (float) this.mHeight, paint3);
                } else {
                    this.mCanvas.drawLine((float) (this.width * i5), (float) this.mHeight, (float) (this.width * (i5 + 1)), (float) this.mHeight, paint4);
                }
            }
            for (int i6 = 0; i6 <= 29; i6++) {
                if (this.screenHeight > 480) {
                    this.mCanvas.drawBitmap(bitmap, (float) ((this.width * i6) - BITMAP_X_OFFSET), (float) (this.mHeight - BITMAP_Y_OFFSET), paint2);
                } else {
                    this.mCanvas.drawBitmap(bitmap, (float) ((this.width * i6) - BITMAP_X_OFFSET), (float) (this.mHeight - BITMAP_Y_OFFSET), paint2);
                }
            }
            return;
        }
        for (int i7 = 0; i7 < points.size() - 1; i7++) {
            if (this.screenHeight > 480) {
                this.mCanvas.drawLine((float) points.get(i7).x, (float) points.get(i7).y, (float) points.get(i7 + 1).x, (float) points.get(i7 + 1).y, paint3);
            } else {
                this.mCanvas.drawLine((float) points.get(i7).x, (float) points.get(i7).y, (float) points.get(i7 + 1).x, (float) points.get(i7 + 1).y, paint4);
            }
        }
        float[] ff = new float[this.data.length];
        String[] ss = new String[this.data.length];
        for (int j = 0; j < this.data.length; j++) {
            float f = this.paint.measureText(prettyBytes((long) this.data[j]));
            String s = prettyBytes((long) this.data[j]);
            ff[j] = f;
            ss[j] = s;
        }
        for (int i8 = 0; i8 < points.size(); i8++) {
            for (int j2 = 0; j2 < this.data.length; j2++) {
                if (this.data[j2] > 0.0d) {
                    if (this.screenHeight > 480) {
                        this.mCanvas.drawBitmap(bitmap, (float) (points.get(j2).x - BITMAP_X_OFFSET), (float) (points.get(j2).y - BITMAP_Y_OFFSET), this.paint);
                        this.mCanvas.drawBitmap(note, (float) (points.get(j2).x - 30), (float) (points.get(j2).y - 48), this.paint);
                        this.mCanvas.drawText(ss[j2], (((float) points.get(j2).x) - (ff[j2] / 2.0f)) + 2.0f, (float) (points.get(j2).y - 25), this.paint);
                    } else {
                        this.mCanvas.drawBitmap(bitmap, (float) (points.get(j2).x - BITMAP_X_OFFSET), (float) (points.get(j2).y - BITMAP_Y_OFFSET), this.paint);
                        if (this.screenHeight == 320) {
                            this.mCanvas.drawBitmap(note, (float) (points.get(j2).x - 17), (float) (points.get(j2).y - 26), this.paint);
                            this.mCanvas.drawText(ss[j2], ((float) points.get(j2).x) - (ff[j2] / 2.0f), (float) (points.get(j2).y - 13), this.paint);
                        } else {
                            this.mCanvas.drawBitmap(note, (float) (points.get(j2).x - 22), (float) (points.get(j2).y - 30), this.paint);
                            this.mCanvas.drawText(ss[j2], ((float) points.get(j2).x) - (ff[j2] / 2.0f), (float) (points.get(j2).y - 16), this.paint);
                        }
                    }
                } else if (this.screenHeight > 480) {
                    this.mCanvas.drawBitmap(bitmap, (float) (points.get(j2).x - BITMAP_X_OFFSET), (float) (points.get(j2).y - BITMAP_Y_OFFSET), this.paint);
                } else {
                    this.mCanvas.drawBitmap(bitmap, (float) (points.get(j2).x - BITMAP_X_OFFSET), (float) (points.get(j2).y - BITMAP_Y_OFFSET), this.paint);
                }
            }
        }
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawBitmap(mSCBitmap, 0.0f, 0.0f, this.mPaint);
    }

    public void updateTraffic() {
        this.data = getTraffic(getContext());
    }

    private double getMaxNum(double[] data2) {
        double temp = data2[0];
        double max = 0.0d;
        for (int i = 0; i <= data2.length - 1; i++) {
            if (temp > data2[i]) {
                max = temp;
            } else {
                max = data2[i];
            }
            temp = max;
        }
        return max;
    }

    private List<Point> getPoints(double[] datas, Canvas canvas) {
        List<Point> points = new ArrayList<>();
        double x = (double) (-this.width);
        int hight = this.mHeight;
        double mY = (getMaxNum(datas) / ((double) hight)) * 1.25d;
        if (mY != 0.0d) {
            for (double i : datas) {
                x += (double) this.width;
                points.add(new Point((int) x, (int) (((double) hight) - (i / mY))));
            }
        }
        return points;
    }
}
