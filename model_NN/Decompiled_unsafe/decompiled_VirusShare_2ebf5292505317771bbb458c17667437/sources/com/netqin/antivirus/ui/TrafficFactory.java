package com.netqin.antivirus.ui;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Paint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import com.netqin.antivirus.log.LogEngine;
import com.netqin.antivirus.networkmanager.NetApplication;
import com.netqin.antivirus.networkmanager.model.DatabaseHelper;
import com.netqin.antivirus.networkmanager.model.Device;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

public class TrafficFactory extends View {
    public static int BITMAP_X_OFFSET = 0;
    public static int BITMAP_Y_OFFSET = 0;
    public static int DATE_X_OFFSET = 0;
    public static int DATE_Y_OFFSET = 0;
    public static final int SCREEN_HEIGHT_320 = 320;
    public static final int SCREEN_HEIGHT_400 = 400;
    public static final int SCREEN_HEIGHT_480 = 480;
    public static final int SCREEN_HEIGHT_800 = 800;
    public static final int SCREEN_HEIGHT_854 = 854;
    public static final int SCREEN_HEIGHT_960 = 960;
    public static final int SCREEN_WIDTH_240 = 240;
    public static final int SCREEN_WIDTH_320 = 320;
    public static final int SCREEN_WIDTH_480 = 480;
    public static final int SCREEN_WIDTH_540 = 540;
    public static final int SCREEN_WIDTH_640 = 640;
    int height;
    int mHeight;
    int mWidth;
    Paint paint;
    int screenHeight;
    int screenWidth;
    int width;

    public TrafficFactory(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /* access modifiers changed from: protected */
    public double[] getTraffic(Context context) {
        SQLiteDatabase mDatabase = new DatabaseHelper(context).getReadableDatabase();
        double[] tmpTra = new double[30];
        Cursor cursor = null;
        try {
            GregorianCalendar gregorianCalendar = new GregorianCalendar();
            String mCellType = Device.getDevice().getCell();
            for (int i = 29; i >= 0; i--) {
                String date = new SimpleDateFormat("yyyy-MM-dd").format(gregorianCalendar.getTime());
                gregorianCalendar.add(5, -1);
                if (TextUtils.isEmpty(mCellType)) {
                    cursor = mDatabase.rawQuery("SELECT rx,tx FROM daily WHERE day=? and interface=?", new String[]{date, "ppp0"});
                } else {
                    cursor = mDatabase.rawQuery("SELECT rx,tx FROM daily WHERE day=? and interface=?", new String[]{date, mCellType});
                }
                if (cursor != null && cursor.moveToNext()) {
                    cursor.moveToFirst();
                    tmpTra[i] = (double) (cursor.getLong(cursor.getColumnIndex(DatabaseHelper.DailyCounter.RX)) + cursor.getLong(cursor.getColumnIndex(DatabaseHelper.DailyCounter.TX)));
                }
            }
            cursor.close();
            mDatabase.close();
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cursor.close();
        }
        return tmpTra;
    }

    /* access modifiers changed from: protected */
    public Paint createNewPaint(int color, Paint.Style style, float strokeWidth, float textSize) {
        Paint paint2 = new Paint();
        paint2.setAntiAlias(true);
        paint2.setColor(color);
        paint2.setStyle(style);
        paint2.setStrokeWidth(strokeWidth);
        paint2.setTextSize(textSize);
        return paint2;
    }

    /* access modifiers changed from: protected */
    public final String prettyBytes(long value) {
        int i;
        StringBuilder sb = new StringBuilder();
        if (value < 1024) {
            sb.append(String.valueOf(value));
            i = 0;
        } else if (value < 1048576) {
            sb.append(String.format("%.1f", Double.valueOf(((double) value) / 1024.0d)));
            i = 1;
        } else if (value < 1073741824) {
            sb.append(String.format("%.1f", Double.valueOf(((double) value) / 1048576.0d)));
            i = 2;
        } else if (value < 1099511627776L) {
            sb.append(String.format("%.1f", Double.valueOf(((double) value) / 1.073741824E9d)));
            i = 3;
        } else {
            sb.append(String.format("%.1f", Double.valueOf(((double) value) / 1.099511627776E12d)));
            i = 4;
        }
        sb.append(NetApplication.mNetworkData.BYTE_UNITS[i]);
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public void getScreenPixels() {
        Paint paint1 = createNewPaint(-1, Paint.Style.FILL, 3.0f, 16.0f);
        Paint paint2 = createNewPaint(-1, Paint.Style.FILL, 2.0f, 10.0f);
        this.screenWidth = getResources().getDisplayMetrics().widthPixels;
        if (this.screenWidth == 640) {
            this.mWidth = (this.screenWidth * 6) - 65;
            DATE_X_OFFSET = 95;
            BITMAP_X_OFFSET = 10;
            this.paint = paint1;
        } else if (this.screenWidth == 540) {
            this.mWidth = (this.screenWidth * 6) - 75;
            DATE_X_OFFSET = 80;
            BITMAP_X_OFFSET = 10;
            this.paint = paint1;
        } else if (this.screenWidth == 480) {
            this.mWidth = (this.screenWidth * 6) - 90;
            DATE_X_OFFSET = 65;
            BITMAP_X_OFFSET = 10;
            this.paint = paint1;
        } else if (this.screenWidth == 320) {
            this.mWidth = (this.screenWidth * 6) - 80;
            DATE_X_OFFSET = 45;
            BITMAP_X_OFFSET = 5;
            this.paint = paint2;
        } else if (this.screenWidth == 240) {
            this.mWidth = (this.screenWidth * 6) - 80;
            DATE_X_OFFSET = 30;
            BITMAP_X_OFFSET = 5;
            this.paint = paint2;
        }
        this.width = this.mWidth / 34;
        this.screenHeight = getResources().getDisplayMetrics().heightPixels;
        if (this.screenHeight == 960) {
            this.mHeight = (this.screenHeight / 2) - 160;
            DATE_Y_OFFSET = 27;
            BITMAP_Y_OFFSET = 10;
        } else if (this.screenHeight == 800 || this.screenHeight == 854) {
            this.mHeight = (this.screenHeight / 2) - 160;
            DATE_Y_OFFSET = 27;
            BITMAP_Y_OFFSET = 10;
        } else if (this.screenHeight == 480) {
            this.mHeight = (this.screenHeight / 3) - 15;
            DATE_Y_OFFSET = 18;
            BITMAP_Y_OFFSET = 5;
        } else if (this.screenHeight == 320 || this.screenHeight == 400) {
            this.mHeight = (this.screenHeight / 4) + 10;
            DATE_Y_OFFSET = 12;
            BITMAP_Y_OFFSET = 4;
        }
        this.height = this.mHeight / 5;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(measureWidth(widthMeasureSpec), measureHeight(heightMeasureSpec));
    }

    private int measureWidth(int measureSpec) {
        int result;
        int specMode = View.MeasureSpec.getMode(measureSpec);
        int specSize = View.MeasureSpec.getSize(measureSpec);
        if (specMode == 1073741824) {
            return specSize;
        }
        this.screenWidth = getResources().getDisplayMetrics().widthPixels;
        if (this.screenWidth == 640) {
            result = (this.screenWidth * 6) - 10;
        } else if (this.screenWidth == 540) {
            result = (this.screenWidth * 6) - 20;
        } else if (this.screenWidth == 480) {
            result = (this.screenWidth * 6) - 30;
        } else if (this.screenWidth == 320) {
            result = (this.screenWidth * 6) - 45;
        } else if (this.screenWidth == 240) {
            result = (this.screenWidth * 6) - 40;
        } else {
            result = (this.screenWidth * 6) - 30;
        }
        if (specMode == Integer.MIN_VALUE) {
            return Math.min(result, specSize);
        }
        return result;
    }

    private int measureHeight(int measureSpec) {
        int result;
        int specMode = View.MeasureSpec.getMode(measureSpec);
        int specSize = View.MeasureSpec.getSize(measureSpec);
        if (specMode == 1073741824) {
            return specSize;
        }
        this.screenHeight = getResources().getDisplayMetrics().heightPixels;
        if (this.screenHeight == 960) {
            result = (this.screenHeight / 2) - LogEngine.LOG_VIRUSDB_UPDATE_BEGIN;
        } else if (this.screenHeight == 800 || this.screenHeight == 854) {
            result = (this.screenHeight / 2) - LogEngine.LOG_VIRUSDB_UPDATE_BEGIN;
        } else if (this.screenHeight == 480) {
            result = (this.screenHeight / 3) + 10;
        } else if (this.screenHeight == 320 || this.screenHeight == 400) {
            result = (this.screenHeight / 3) + 10;
        } else {
            result = (this.screenHeight / 2) - 160;
        }
        if (specMode == Integer.MIN_VALUE) {
            return Math.min(result, specSize);
        }
        return result;
    }
}
