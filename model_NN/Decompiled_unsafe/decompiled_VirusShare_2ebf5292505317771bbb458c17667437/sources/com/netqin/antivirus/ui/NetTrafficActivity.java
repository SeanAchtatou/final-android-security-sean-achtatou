package com.netqin.antivirus.ui;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceCategory;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.netqin.antivirus.NqUtil;
import com.netqin.antivirus.data.PreferenceDataHelper;
import com.netqin.antivirus.networkmanager.NetApplication;
import com.netqin.antivirus.networkmanager.SettingPreferences;
import com.netqin.antivirus.networkmanager.model.Counter;
import com.netqin.antivirus.networkmanager.model.DatabaseHelper;
import com.netqin.antivirus.networkmanager.model.Device;
import com.netqin.antivirus.networkmanager.model.IModel;
import com.netqin.antivirus.networkmanager.model.IModelListener;
import com.netqin.antivirus.networkmanager.model.IOperation;
import com.netqin.antivirus.networkmanager.model.Interface;
import com.netqin.antivirus.networkmanager.model.NetMeterModel;
import com.nqmobile.antivirus_ampro20.R;
import java.text.DecimalFormat;
import java.util.Calendar;

public class NetTrafficActivity extends Activity implements IModelListener, IOperation {
    public static Activity MA_ACTIVITY;
    long[] cell;
    private NetApplication mApp;
    private Counter mCounter;
    private TextView mGprsRemain;
    private TextView mGprsTotal;
    private Handler mHandler = new Handler();
    private SQLiteOpenHelper mHelper;
    private NetMeterModel mModel = null;
    protected long mNow = 0;
    private TextView mTextTrafficUsed;
    private TextView mTitle;
    private ImageView mTrafficSetting;
    private TrafficView mTrafficView;
    private TextView mTrfficTodayUse;
    private ProgressBar progress;
    PreferenceCategory rrr;
    /* access modifiers changed from: private */
    public HorizontalScrollView scrollView;
    long[] wifi;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        long[] counter;
        Interface inter;
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.net_traffic);
        MA_ACTIVITY = this;
        this.mApp = (NetApplication) getApplication();
        this.mModel = (NetMeterModel) this.mApp.getAdapter(NetMeterModel.class);
        if (!(savedInstanceState == null || (counter = savedInstanceState.getLongArray(DatabaseHelper.NetCounter.TABLE_NAME)) == null || (inter = this.mModel.getInterface(counter[0])) == null)) {
            this.mCounter = inter.getCounter(counter[1]);
        }
        this.scrollView = (HorizontalScrollView) findViewById(R.id.horizontalScrollView);
        this.mTitle = (TextView) findViewById(R.id.activity_name);
        this.mTitle.setText((int) R.string.tital_traffic_manager);
        bindView();
    }

    private void bindView() {
        this.progress = (ProgressBar) findViewById(R.id.progress_traffic_used);
        this.mGprsTotal = (TextView) findViewById(R.id.meter_gprs_total);
        this.mGprsRemain = (TextView) findViewById(R.id.meter_gprs_remain);
        this.mTrfficTodayUse = (TextView) findViewById(R.id.traffic_today_use);
        this.mTrfficTodayUse.setText(getString(R.string.meter_gprs_today_used, new Object[]{"0MB"}));
        this.mTrafficSetting = (ImageView) findViewById(R.id.traffic_setting);
        this.mTextTrafficUsed = (TextView) findViewById(R.id.text_traffic_used);
        this.mTrafficView = (TrafficView) findViewById(R.id.trafficview);
        this.mTrafficSetting.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                NetTrafficActivity.this.startActivity(new Intent(NetTrafficActivity.this, SettingPreferences.class));
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.mModel.addModelListener(this);
        this.mModel.addOperationListener(this);
        NetApplication.setUpdatePolicy(2);
        ((NetApplication) getApplication()).startService();
        if (this.mModel.isLoaded()) {
            updateUI();
        }
        updateTrafficToday();
        this.mHandler.postDelayed(new Runnable() {
            public void run() {
                NetTrafficActivity.this.scrollView.smoothScrollTo((int) (((double) NetTrafficActivity.this.getResources().getDisplayMetrics().widthPixels) * 4.6d), NetTrafficActivity.this.getResources().getDisplayMetrics().heightPixels);
            }
        }, 150);
    }

    private void updateTrafficToday() {
        Calendar now = Calendar.getInstance();
        this.mHelper = new DatabaseHelper(this);
        SQLiteDatabase db = this.mHelper.getWritableDatabase();
        SQLiteQueryBuilder query = new SQLiteQueryBuilder();
        query.setTables(DatabaseHelper.DailyCounter.TABLE_NAME);
        StringBuilder sb = new StringBuilder("interface='" + Device.getDevice().getCell() + "'");
        sb.append(" AND ");
        sb.append("day");
        sb.append("='");
        sb.append(DatabaseHelper.getDate(now));
        sb.append("'");
        Cursor c = null;
        try {
            c = query.query(db, null, sb.toString(), null, null, null, null);
            if (c != null && c.getCount() > 0) {
                c.moveToFirst();
                this.mTrfficTodayUse.setText(getString(R.string.meter_gprs_today_used, new Object[]{Counter.prettyBytes(c.getLong(c.getColumnIndex(DatabaseHelper.DailyCounter.RX)) + c.getLong(c.getColumnIndex(DatabaseHelper.DailyCounter.TX)))}));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            c.close();
            db.close();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.addvice_feedback_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.updatedb_advice_feedback /*2131558847*/:
                NqUtil.clickAdviceFeedback(this);
                return true;
            default:
                return true;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    public void modelChanged(IModel object) {
        runOnUiThread(new Runnable() {
            public void run() {
                NetTrafficActivity.this.updateUI();
            }
        });
    }

    public void modelLoaded(IModel object) {
        runOnUiThread(new Runnable() {
            public void run() {
                NetTrafficActivity.this.updateUI();
            }
        });
    }

    public void operationEnded() {
        runOnUiThread(new Runnable() {
            public void run() {
                NetTrafficActivity.this.getWindow().setFeatureInt(5, -2);
            }
        });
    }

    public void operationStarted() {
        runOnUiThread(new Runnable() {
            public void run() {
                NetTrafficActivity.this.getWindow().setFeatureInt(5, -1);
            }
        });
    }

    private void getMeterTraffic() {
        this.wifi = new long[2];
        this.cell = new long[2];
        Device device = Device.getDevice();
        for (Interface inter : this.mModel.getInterfaces()) {
            for (Counter counter : inter.getCounters()) {
                if (counter.getType() == 0) {
                    long[] bytes = counter.getBytes();
                    if (device.isCell(inter.getName())) {
                        long[] jArr = this.cell;
                        jArr[0] = jArr[0] + bytes[0];
                        long[] jArr2 = this.cell;
                        jArr2[1] = jArr2[1] + bytes[1];
                    } else if (device.isWiFi(inter.getName())) {
                        long[] jArr3 = this.wifi;
                        jArr3[0] = jArr3[0] + bytes[0];
                        long[] jArr4 = this.wifi;
                        jArr4[1] = jArr4[1] + bytes[1];
                    }
                }
            }
        }
    }

    public void updateUI() {
        long used;
        getMeterTraffic();
        SharedPreferences preferences = (SharedPreferences) this.mApp.getAdapter(SharedPreferences.class);
        String s = preferences.getString(SettingPreferences.KEY_METER_THRESHOLD, "");
        try {
            Double.parseDouble(s);
        } catch (NumberFormatException e) {
            s = NetApplication.DEFAULT_THRESHOLD;
            preferences.edit().putString(SettingPreferences.KEY_METER_THRESHOLD, s).commit();
        }
        long threshold = (long) (Double.parseDouble(s) * 1024.0d * 1024.0d);
        if (this.cell != null) {
            used = this.cell[0] + this.cell[1];
        } else {
            used = 0;
        }
        long a = used + PreferenceDataHelper.getFlowAdjustValue(this);
        long used2 = a > 0 ? a : 0;
        long p = threshold != 0 ? (100 * used2) / threshold : 0;
        this.progress.setMax(100);
        this.progress.setProgress((((int) p) >= 1 || used2 <= 0) ? (int) p : 1);
        double dUsed = threshold != 0 ? (100.0d * ((double) used2)) / ((double) threshold) : 0.0d;
        String textUsed = new DecimalFormat("##0.00").format(dUsed);
        if (dUsed == 0.0d) {
            this.mTextTrafficUsed.setVisibility(8);
        } else {
            this.mTextTrafficUsed.setText(String.valueOf(textUsed) + "%");
            this.mTextTrafficUsed.setVisibility(0);
        }
        this.mGprsTotal.setText(getString(R.string.meter_gprs_set, new Object[]{s}));
        long remain = threshold - used2;
        if (remain < 0) {
            this.mGprsRemain.setText(getString(R.string.meter_gprs_set_overload, new Object[]{Counter.prettyBytes(-remain)}));
        } else {
            this.mGprsRemain.setText(getString(R.string.meter_gprs_set_remain, new Object[]{Counter.prettyBytes(remain)}));
        }
        updateTrafficToday();
        this.mTrafficView.invalidate();
        this.mTrafficView.updateTraffic();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (this.mCounter != null) {
            outState.putLongArray(DatabaseHelper.NetCounter.TABLE_NAME, new long[]{this.mCounter.getInterface().getId(), this.mCounter.getId()});
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        NetApplication.setUpdatePolicy(1);
        ((NetApplication) getApplication()).startService();
        this.mModel.removeModelListener(this);
        this.mModel.removeOperationListener(this);
    }
}
