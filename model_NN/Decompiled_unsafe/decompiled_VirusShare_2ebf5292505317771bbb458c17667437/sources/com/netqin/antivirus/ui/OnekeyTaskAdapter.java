package com.netqin.antivirus.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.nqmobile.antivirus_ampro20.R;
import java.util.ArrayList;

public class OnekeyTaskAdapter extends BaseAdapter {
    Context mContext = null;
    LayoutInflater mInflater = null;
    ArrayList<OneKeyTaskStatus> mTasks = new ArrayList<>();

    public OnekeyTaskAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
        this.mContext = context;
    }

    public synchronized int getCount() {
        return this.mTasks.size();
    }

    public synchronized Object getItem(int position) {
        return this.mTasks.get(position);
    }

    public synchronized long getItemId(int position) {
        return (long) position;
    }

    public synchronized void addItem(OneKeyTaskStatus task) {
        this.mTasks.add(task);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder _H;
        if (convertView == null) {
            _H = new ViewHolder();
            convertView = this.mInflater.inflate((int) R.layout.onekey_task_item, (ViewGroup) null);
            _H.tvDesc = (TextView) convertView.findViewById(R.id.tv_desc);
            _H.ivIcon = (ImageView) convertView.findViewById(R.id.iv_icon);
            _H.pbStat = (ProgressBar) convertView.findViewById(R.id.pb_status);
            convertView.setTag(_H);
        } else {
            _H = (ViewHolder) convertView.getTag();
        }
        OneKeyTaskStatus ts = (OneKeyTaskStatus) getItem(position);
        if (ts != null) {
            if (ts.isDone()) {
                _H.pbStat.setVisibility(8);
                _H.tvDesc.setText(ts.getDoneDesc());
                _H.ivIcon.setVisibility(0);
                int resId = ts.getIconDone();
                _H.ivIcon.setImageDrawable(this.mContext.getResources().getDrawable(resId > 0 ? resId : R.drawable.check_on_48));
            } else {
                _H.ivIcon.setVisibility(8);
                _H.tvDesc.setText(ts.getRunningDesc());
                _H.pbStat.setVisibility(0);
                _H.ivIcon.setImageDrawable(this.mContext.getResources().getDrawable(R.drawable.check_on_48));
            }
        }
        return convertView;
    }

    public synchronized void setDone(String taskStatusKey) {
        if (this.mTasks != null) {
            int size = this.mTasks.size();
            int i = 0;
            while (true) {
                if (i >= size) {
                    break;
                }
                OneKeyTaskStatus status = this.mTasks.get(i);
                if (status.getKey().equals(taskStatusKey)) {
                    status.setDone();
                    notifyDataSetChanged();
                    break;
                }
                i++;
            }
        }
    }

    static class ViewHolder {
        ImageView ivIcon;
        ProgressBar pbStat;
        TextView tvDesc;

        ViewHolder() {
        }
    }

    public boolean areAllItemsEnabled() {
        return false;
    }

    public boolean isEnabled(int position) {
        return false;
    }
}
