package com.netqin.antivirus;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.util.MimeUtils;
import com.nqmobile.antivirus_ampro20.R;

public class DialogActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        Intent intent = getIntent();
        String title = intent.getStringExtra("dialogtitle");
        String message = intent.getStringExtra("dialogmessage");
        int notificationId = intent.getIntExtra("notificationID", 0);
        if (title == null || title.length() < 0) {
            title = getString(R.string.label_netqin_antivirus_pro);
        }
        createMessageDialog(title, message, notificationId);
    }

    private void createMessageDialog(String title, String message, final int notificationId) {
        WebView wv = new WebView(this);
        wv.getSettings().setDefaultTextEncodingName("utf-8");
        wv.setBackgroundColor(-1);
        wv.loadData("<div style=\"color: #000000\">" + message + "</div>", MimeUtils.MIME_TEXT_HTML, "utf-8");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setView(wv);
        builder.setPositiveButton((int) R.string.label_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int i) {
                CommonMethod.closeNotification(DialogActivity.this, notificationId);
                DialogActivity.this.finish();
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface arg0) {
                CommonMethod.closeNotification(DialogActivity.this, notificationId);
                DialogActivity.this.finish();
            }
        });
        builder.show();
    }
}
