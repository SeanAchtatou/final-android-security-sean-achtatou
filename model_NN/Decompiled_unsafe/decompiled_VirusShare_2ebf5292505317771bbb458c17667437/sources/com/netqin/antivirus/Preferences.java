package com.netqin.antivirus;

import android.content.Context;
import android.content.SharedPreferences;
import com.netqin.antivirus.cloud.model.xml.XmlUtils;
import com.netqin.antivirus.common.CommonDefine;

public class Preferences {
    private SharedPreferences.Editor mEditor = this.mPref.edit();
    private SharedPreferences mPref;

    public Preferences(Context context) {
        this.mPref = context.getSharedPreferences("imconfig", 0);
    }

    public static Preferences getPreferences(Context context) {
        return new Preferences(context);
    }

    public void registerChangeListener(SharedPreferences.OnSharedPreferenceChangeListener listener) {
        this.mPref.registerOnSharedPreferenceChangeListener(listener);
    }

    public void unRegisterChangeListener(SharedPreferences.OnSharedPreferenceChangeListener listener) {
        this.mPref.unregisterOnSharedPreferenceChangeListener(listener);
    }

    public void setIsFirstRun(boolean b) {
        this.mEditor.putBoolean("IsFirstRun", b);
        this.mEditor.commit();
    }

    public boolean getIsFirstRun() {
        return this.mPref.getBoolean("IsFirstRun", true);
    }

    public void setShowFirstPage(boolean b) {
        this.mEditor.putBoolean("ShowFirstPage", b);
        this.mEditor.commit();
    }

    public boolean getShowFirstPage() {
        return this.mPref.getBoolean("ShowFirstPage", true);
    }

    public void setSpamSmsDeskRemind(boolean b) {
        this.mEditor.putBoolean("SpamSmsDeskRemind", b);
        this.mEditor.commit();
    }

    public boolean getSpamSmsDeskRemind() {
        return this.mPref.getBoolean("SpamSmsDeskRemind", false);
    }

    public void setSpamSmsVibrate(boolean b) {
        this.mEditor.putBoolean("SpamSmsVibrate", b);
        this.mEditor.commit();
    }

    public boolean getSpamSmsVibrate() {
        return this.mPref.getBoolean("SpamSmsVibrate", false);
    }

    public void setAutoRun(boolean b) {
        this.mEditor.putBoolean("IsAutoRun", b);
        this.mEditor.commit();
    }

    public boolean getIsAutoRun() {
        return this.mPref.getBoolean("IsAutoRun", true);
    }

    public void setRunMonitor(boolean b) {
        this.mEditor.putBoolean("IsRunMonitor", b);
        this.mEditor.commit();
    }

    public boolean getIsRunMonitor() {
        return this.mPref.getBoolean("IsRunMonitor", true);
    }

    public void setRunWebBlock(boolean b) {
        this.mEditor.putBoolean("IsRunWebBlock", b);
        this.mEditor.commit();
    }

    public boolean getIsRunWebBlock() {
        return this.mPref.getBoolean("IsRunWebBlock", true);
    }

    public void setShowRegion(boolean b) {
        this.mEditor.putBoolean("ShowRegion", b);
        this.mEditor.commit();
    }

    public boolean getIsShowRegion() {
        return this.mPref.getBoolean("ShowRegion", true);
    }

    public void setCheatCallNotify(boolean b) {
        this.mEditor.putBoolean("CheatCallNotify", b);
        this.mEditor.commit();
    }

    public boolean getIsCheatCallNotify() {
        return this.mPref.getBoolean("CheatCallNotify", true);
    }

    public void setHangupCallNotify(boolean b) {
        this.mEditor.putBoolean("HangupCallNotify", b);
        this.mEditor.commit();
    }

    public boolean getIsHangupCallNotify() {
        return this.mPref.getBoolean("HangupCallNotify", true);
    }

    public void setSmsFilterMode(int i) {
        this.mEditor.putInt("SmsFilterMode", i);
        this.mEditor.commit();
    }

    public int getSmsFilterMode() {
        return this.mPref.getInt("SmsFilterMode", 0);
    }

    public void setFilterStrangeSms(boolean b) {
        this.mEditor.putBoolean("FilterStrangeSms", b);
        this.mEditor.commit();
    }

    public boolean getIsFilterStrangeSms() {
        return this.mPref.getBoolean("FilterStrangeSms", false);
    }

    public void setCheatSmsNotify(boolean b) {
        this.mEditor.putBoolean("IsCheatSmsNotify", b);
        this.mEditor.commit();
    }

    public boolean getCheatSmsNotify() {
        return this.mPref.getBoolean("IsCheatSmsNotify", true);
    }

    public void setReportSpamSms(boolean b) {
        this.mEditor.putBoolean("IsReportSpamSms", b);
        this.mEditor.commit();
    }

    public boolean getReportSpamSms() {
        return this.mPref.getBoolean("IsReportSpamSms", true);
    }

    public void setPrivateSpaceUsable(boolean b) {
        this.mEditor.putBoolean("IsPrivateSpaceUsable", b);
        this.mEditor.commit();
    }

    public boolean getPrivateSpaceUsable() {
        return this.mPref.getBoolean("IsPrivateSpaceUsable", false);
    }

    public void setSmsFilterUsable(boolean b) {
        this.mEditor.putBoolean("IsSmsFilterUsable", b);
        this.mEditor.commit();
    }

    public boolean getSmsFilterUsable() {
        return this.mPref.getBoolean("IsSmsFilterUsable", true);
    }

    public void setUID(String uid) {
        this.mEditor.putString(XmlUtils.LABEL_CLIENTINFO_UID, uid);
        this.mEditor.commit();
    }

    public String getUID() {
        return this.mPref.getString(XmlUtils.LABEL_CLIENTINFO_UID, "null");
    }

    public void setExpired(String date) {
        this.mEditor.putString("expired", date);
        this.mEditor.commit();
    }

    public String getExpired() {
        return this.mPref.getString("expired", "");
    }

    public void setUserLevel(int level) {
        this.mEditor.putInt("user_level", level);
        this.mEditor.commit();
    }

    public int getUserLevel() {
        return this.mPref.getInt("user_level", 0);
    }

    public void setUserLevelName(String name) {
        this.mEditor.putString("user_level_name", name);
        this.mEditor.commit();
    }

    public String getUserLevelName() {
        return this.mPref.getString("user_level_name", "Unregistered user");
    }

    public void setUserStatus(int status) {
        this.mEditor.putInt("user_status", status);
        this.mEditor.commit();
    }

    public int getUserStatus() {
        return this.mPref.getInt("user_status", 0);
    }

    public void setIMSI(String imsi) {
        this.mEditor.putString(XmlUtils.LABEL_MOBILEINFO_IMSI, imsi);
        this.mEditor.commit();
    }

    public String getIMSI() {
        return this.mPref.getString(XmlUtils.LABEL_MOBILEINFO_IMSI, "null");
    }

    public void setSpamSmsFilterSwitch(boolean open) {
        this.mEditor.putBoolean("spam_sms_filter_open", open);
        this.mEditor.commit();
    }

    public boolean getSpamSmsFilterSwitch() {
        return this.mPref.getBoolean("spam_sms_filter_open", false);
    }

    public void setPrivateSmsFilterSwitch(boolean open) {
        this.mEditor.putBoolean("private_sms_filter_open", open);
        this.mEditor.commit();
    }

    public boolean getPrivateSmsFilterSwitch() {
        return this.mPref.getBoolean("private_sms_filter_open", true);
    }

    public void setPrivateCallAcceptMode(int mode) {
        this.mEditor.putInt("private_incomingcall_mode", mode);
        this.mEditor.commit();
    }

    public int getPrivateCallAcceptMode() {
        return this.mPref.getInt("private_incomingcall_mode", 0);
    }

    public void setReplySms(String s) {
        this.mEditor.putString("reply_sms", s);
        this.mEditor.commit();
    }

    public String getReplySms() {
        return this.mPref.getString("reply_sms", "���ڲ����㣬�Ժ�jϵ");
    }

    public void setReplySmsRowId(long rowId) {
        this.mEditor.putLong("reply_sms_rowid", rowId);
        this.mEditor.commit();
    }

    public long getReplySmsRowId() {
        return this.mPref.getLong("reply_sms_rowid", 0);
    }

    public void setPrivateSmsRemind(boolean b) {
        this.mEditor.putBoolean("private_sms_remind", b);
        this.mEditor.commit();
    }

    public boolean getPriavteSmsRemind() {
        return this.mPref.getBoolean("private_sms_remind", true);
    }

    public void setPrivatePwd(String pwd) {
        this.mEditor.putString("private_password", pwd);
        this.mEditor.commit();
    }

    public String getPrivatePwd() {
        return this.mPref.getString("private_password", "123456");
    }

    public void setSpamSmsRemindTime(int t) {
        this.mEditor.putInt("spam_sms_remind_time", t);
        this.mEditor.commit();
    }

    public int getSpamSmsRemindTime() {
        return this.mPref.getInt("spam_sms_remind_time", 17);
    }

    public void setApn(long apn) {
        this.mEditor.putLong("apn_id", apn);
        this.mEditor.commit();
    }

    public long getApn() {
        return this.mPref.getLong("apn_id", 0);
    }

    public void setSceneModeId(long id) {
        this.mEditor.putLong("scene_mode_id", id);
        this.mEditor.commit();
    }

    public long getSceneModeId() {
        return this.mPref.getLong("scene_mode_id", 1);
    }

    public void setSceneModeName(String name) {
        this.mEditor.putString("scene_mode_name", name);
        this.mEditor.commit();
    }

    public String getSceneModeName() {
        return this.mPref.getString("scene_mode_name", "ֻ�ܺ���");
    }

    public void setBlackWhiteListVersion(String version) {
        this.mEditor.putString("BlackWhiteListVersion", version);
        this.mEditor.commit();
    }

    public String getBlackWhiteListVersion() {
        return this.mPref.getString("BlackWhiteListVersion", "20080911");
    }

    public void setTrashSmsVersion(String version) {
        this.mEditor.putString("TrashSmsVersion", version);
        this.mEditor.commit();
    }

    public String getTrashSmsVersion() {
        return this.mPref.getString("TrashSmsVersion", "20080911");
    }

    public void setIMEI(String imei) {
        this.mEditor.putString(XmlUtils.LABEL_MOBILEINFO_IMEI, imei);
        this.mEditor.commit();
    }

    public String getIMEI() {
        return this.mPref.getString(XmlUtils.LABEL_MOBILEINFO_IMEI, "");
    }

    public void setPwdModified() {
        this.mEditor.putBoolean("PwdModified", true);
        this.mEditor.commit();
    }

    public boolean getPwdModified() {
        return this.mPref.getBoolean("PwdModified", false);
    }

    public void setSoftNeedUpdate(boolean b) {
        this.mEditor.putBoolean("IsSoftNeedUpdate", b);
        this.mEditor.commit();
    }

    public boolean getSoftNeedUpdate() {
        return this.mPref.getBoolean("IsSoftNeedUpdate", false);
    }

    public void setSoftCouldUpdate(boolean b) {
        this.mEditor.putBoolean("IsSoftCouldUpdate", b);
        this.mEditor.commit();
    }

    public boolean getSoftCouldUpdate() {
        return this.mPref.getBoolean("IsSoftCouldUpdate", false);
    }

    public void setUpdataAppRemindTimes(int t) {
        this.mEditor.putInt("app_update_remind_times", t);
        this.mEditor.commit();
    }

    public int getUpdataAppRemindTimes() {
        return this.mPref.getInt("app_update_remind_times", 0);
    }

    public void setSoftVersion(int t) {
        this.mEditor.putInt("soft_version", t);
        this.mEditor.commit();
    }

    public int getSoftVersion() {
        return this.mPref.getInt("soft_version", 1);
    }

    public void setUpdateAppFileName(String name) {
        this.mEditor.putString("update_app_filename", name);
        this.mEditor.commit();
    }

    public String getUpdateAppFileName() {
        return this.mPref.getString("update_app_filename", "");
    }

    public void setNextDBUpdateTime(String time) {
        this.mEditor.putString("next_db_update_time", time);
        this.mEditor.commit();
    }

    public String getNextDBUpdateTime() {
        return this.mPref.getString("next_db_update_time", "");
    }

    public void setNextAppUpdateTime(String time) {
        this.mEditor.putString("next_app_update_time", time);
        this.mEditor.commit();
    }

    public String getNextAppUpdateTime() {
        return this.mPref.getString("next_app_update_time", "");
    }

    public void setSC(String sc) {
        this.mEditor.putString("sc", sc);
        this.mEditor.commit();
    }

    public String getSC() {
        return "";
    }

    public void setPromptSubscribeMsg(String msg) {
        this.mEditor.putString("PromptSubscribe", msg);
        this.mEditor.commit();
    }

    public String getPromptSubscribeMsg() {
        return this.mPref.getString("PromptSubscribe", "");
    }

    public void setVirusDBVersion(String s) {
        this.mEditor.putString("virusDBVer", s);
        this.mEditor.commit();
    }

    public String getVirusDBVersion() {
        return this.mPref.getString("virusDBVer", "2011041001");
    }

    public void setNewVirusDBVersion(String s) {
        this.mEditor.putString("newVirusDBVer", s);
        this.mEditor.commit();
    }

    public String getNewVirusDBVersion() {
        return this.mPref.getString("newVirusDBVer", "");
    }

    public void setNewVirusDBPath(String s) {
        this.mEditor.putString("newVirusDBPath", s);
        this.mEditor.commit();
    }

    public String getNewVirusDBPath() {
        return this.mPref.getString("newVirusDBPath", "");
    }

    public void setLastScanTime(String s) {
        this.mEditor.putString("lastScan", s);
        this.mEditor.commit();
    }

    public String getLastScanTime() {
        return this.mPref.getString("lastScan", "");
    }

    public void setLastCloudScanTime(String s) {
        this.mEditor.putString("lastCloudScan", s);
        this.mEditor.commit();
    }

    public String getLastCloudScanTime() {
        return this.mPref.getString("lastCloudScan", "");
    }

    public String getChanelIdStore() {
        return this.mPref.getString("chanelid", CommonDefine.ANTIVIRUS_PID);
    }

    public void setChanelIdStore(String s) {
        this.mEditor.putString("chanelid", s);
        this.mEditor.commit();
    }

    public void setCheckStatus(Boolean value) {
        this.mEditor.putBoolean("needToReCheck", value.booleanValue());
        this.mEditor.commit();
    }

    public Boolean getCheckStatus() {
        return Boolean.valueOf(this.mPref.getBoolean("needToReCheck", false));
    }

    public void setOrderNumber(String value) {
        this.mEditor.putString("orderNumber", value);
        this.mEditor.commit();
    }

    public String getOrderNumber() {
        return this.mPref.getString("orderNumber", null);
    }

    public void setTransactionID(String value) {
        this.mEditor.putString("transactionID", value);
        this.mEditor.commit();
    }

    public String getTransactionID() {
        return this.mPref.getString("transactionID", null);
    }

    public void setBalance(String balance) {
        this.mEditor.putString("balance", balance);
        this.mEditor.commit();
    }

    public String getBalance() {
        return this.mPref.getString("balance", "0");
    }

    public void setExpiredData(String date) {
        this.mEditor.putString(Value.ExpiredDate, date);
        this.mEditor.commit();
    }

    public String getExpiredData() {
        return this.mPref.getString(Value.ExpiredDate, "");
    }

    public void setInitDB(boolean b) {
        this.mEditor.putBoolean("initdb", b);
        this.mEditor.commit();
    }

    public boolean isInitDB() {
        return this.mPref.getBoolean("initdb", false);
    }
}
