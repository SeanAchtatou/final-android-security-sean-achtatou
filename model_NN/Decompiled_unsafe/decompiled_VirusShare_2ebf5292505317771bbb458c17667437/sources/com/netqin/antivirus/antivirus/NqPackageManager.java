package com.netqin.antivirus.antivirus;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.netqin.antivirus.contact.vcard.VCardConfig;

public class NqPackageManager {
    private Context act;

    public NqPackageManager(Context act2) {
        this.act = act2;
    }

    public void uninstallPackage(String packageName) {
        Intent intent1 = new Intent("android.intent.action.DELETE");
        intent1.setData(Uri.fromParts("package", packageName, null));
        intent1.setFlags(VCardConfig.FLAG_USE_QP_TO_PRIMARY_PROPERTIES);
        this.act.startActivity(intent1);
    }
}
