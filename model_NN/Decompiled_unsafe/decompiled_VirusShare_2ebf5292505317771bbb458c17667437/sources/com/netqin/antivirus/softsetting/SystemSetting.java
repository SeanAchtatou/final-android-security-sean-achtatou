package com.netqin.antivirus.softsetting;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import com.netqin.antivirus.HomeActivity;
import com.netqin.antivirus.Value;
import com.netqin.antivirus.common.CommonMethod;
import com.nqmobile.antivirus_ampro20.R;

public class SystemSetting extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener, Preference.OnPreferenceChangeListener {
    /* access modifiers changed from: private */
    public boolean autoStart;
    private CheckBoxPreference autoStartCheckBoxPreference;
    private boolean isStateBarShow;
    /* access modifiers changed from: private */
    public SharedPreferences prefs;
    private CheckBoxPreference status_barcheckBoxPreference;

    public static Intent getLaunchIntent(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, SystemSetting.class);
        return intent;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getListView().setCacheColorHint(0);
        getListView().setBackgroundResource(R.drawable.main_background);
        addPreferencesFromResource(R.xml.system_setting_preferences);
        this.prefs = PreferenceManager.getDefaultSharedPreferences(this);
        this.status_barcheckBoxPreference = (CheckBoxPreference) findPreference("status_bar");
        this.autoStartCheckBoxPreference = (CheckBoxPreference) findPreference("auto_start_protect");
        this.autoStart = this.prefs.getBoolean("auto_start_protect", true);
        this.prefs.edit().putBoolean("auto_start_protect", this.autoStart).commit();
        this.autoStartCheckBoxPreference.setOnPreferenceChangeListener(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Preference pref = findPreference(key);
        if (pref != null && pref == this.status_barcheckBoxPreference) {
            this.isStateBarShow = this.prefs.getBoolean("status_bar", true);
            if (this.isStateBarShow) {
                CommonMethod.showFlowBarOrNot(this, new Intent(this, HomeActivity.class), CommonMethod.getNotificationTitle(this), false);
            } else {
                CommonMethod.closeNotification(this, Value.NOTIFICATION_FLOW_ID);
            }
        }
    }

    public boolean onPreferenceChange(final Preference preference, Object newValue) {
        this.autoStart = ((Boolean) newValue).booleanValue();
        if (this.autoStart) {
            return true;
        }
        try {
            new AlertDialog.Builder(this).setTitle(getString(R.string.label_netqin_antivirus)).setCancelable(true).setMessage(getString(R.string.system_setting_auto_off)).setPositiveButton(getString(R.string.label_yes), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialoginterface, int i) {
                    SystemSetting.this.autoStart = false;
                    ((CheckBoxPreference) preference).setChecked(false);
                    SystemSetting.this.prefs.edit().putBoolean("auto_start_protect", false).commit();
                }
            }).setNegativeButton(getString(R.string.label_no), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    SystemSetting.this.prefs.edit().putBoolean("auto_start_protect", true).commit();
                    SystemSetting.this.autoStart = true;
                    dialog.cancel();
                }
            }).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this.autoStart;
    }
}
