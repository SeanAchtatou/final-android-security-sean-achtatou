package com.netqin.antivirus.softsetting;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import com.netqin.antivirus.Preferences;
import com.netqin.antivirus.antimallink.BlockService;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.scan.MonitorService;
import com.nqmobile.antivirus_ampro20.R;
import java.util.Calendar;

public class SoftsecuritySetting extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    private boolean isStateBarShow;
    private ListPreference mScanPeriodListPreference;
    private CheckBoxPreference maliciousWebsiteInterceptCheckBoxPreference;
    private Preferences pf;
    private SharedPreferences prefs;
    private CheckBoxPreference timeProtectionCheckBoxPreference;

    public static Intent getLaunchIntent(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, SoftsecuritySetting.class);
        return intent;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getListView().setCacheColorHint(0);
        getListView().setBackgroundResource(R.drawable.main_background);
        addPreferencesFromResource(R.xml.security_setting_preferences);
        this.prefs = PreferenceManager.getDefaultSharedPreferences(this);
        this.pf = new Preferences(this);
        this.timeProtectionCheckBoxPreference = (CheckBoxPreference) findPreference("time_protection");
        this.maliciousWebsiteInterceptCheckBoxPreference = (CheckBoxPreference) findPreference("malicious_website_intercept");
        this.mScanPeriodListPreference = (ListPreference) findPreference("regular_scans");
        this.prefs.edit().putBoolean("time_protection", this.pf.getIsRunMonitor()).commit();
        this.prefs.edit().putBoolean("malicious_website_intercept", this.pf.getIsRunWebBlock()).commit();
        String day = this.prefs.getString("regular_scans", "15");
        if (day.equalsIgnoreCase("0")) {
            this.mScanPeriodListPreference.setSummary(getString(R.string.regular_scans_neverscan));
        } else if (day.equalsIgnoreCase("15")) {
            this.mScanPeriodListPreference.setSummary(getString(R.string.regular_scans_defaultdate));
        } else {
            this.mScanPeriodListPreference.setSummary(getString(R.string.regular_scans_date, new Object[]{day}));
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Preference pref = findPreference(key);
        if (pref == null) {
            return;
        }
        if (pref == this.timeProtectionCheckBoxPreference) {
            this.isStateBarShow = this.prefs.getBoolean("time_protection", true);
            Intent i = new Intent("android.intent.action.RUN");
            i.setClass(this, MonitorService.class);
            if (this.isStateBarShow) {
                startService(i);
            } else {
                stopService(i);
            }
            this.pf.setRunMonitor(this.isStateBarShow);
        } else if (pref == this.maliciousWebsiteInterceptCheckBoxPreference) {
            this.isStateBarShow = this.prefs.getBoolean("malicious_website_intercept", true);
            Intent i2 = new Intent("android.intent.action.RUN");
            i2.setClass(this, BlockService.class);
            if (this.isStateBarShow) {
                startService(i2);
            } else {
                stopService(i2);
            }
            this.pf.setRunWebBlock(this.isStateBarShow);
        } else if (pref == this.mScanPeriodListPreference) {
            String day = this.prefs.getString("regular_scans", "15");
            if (!day.equalsIgnoreCase("0")) {
                this.mScanPeriodListPreference.setSummary(getString(R.string.regular_scans_date, new Object[]{day}));
            } else {
                this.mScanPeriodListPreference.setSummary(getString(R.string.regular_scans_neverscan));
            }
            int dayNum = Integer.parseInt(day);
            if (dayNum > 0) {
                Calendar calendar = Calendar.getInstance();
                calendar.add(6, dayNum);
                CommonMethod.setNextPeriodScanTime(this, calendar);
                return;
            }
            CommonMethod.cleanPeriodScanTime(this);
        }
    }
}
