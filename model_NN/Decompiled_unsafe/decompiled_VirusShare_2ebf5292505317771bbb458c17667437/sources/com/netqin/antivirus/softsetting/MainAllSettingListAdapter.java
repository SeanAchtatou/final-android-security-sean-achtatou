package com.netqin.antivirus.softsetting;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.nqmobile.antivirus_ampro20.R;

public class MainAllSettingListAdapter extends BaseAdapter {
    private final int[] listTitle = {R.string.soft_setting_security, R.string.meter_activity_title, R.string.soft_setting_lost, R.string.system_setting};
    private Context mContext;
    private LayoutInflater mInflater;

    private class ListViewHolder {
        TextView title;

        private ListViewHolder() {
        }

        /* synthetic */ ListViewHolder(MainAllSettingListAdapter mainAllSettingListAdapter, ListViewHolder listViewHolder) {
            this();
        }
    }

    public MainAllSettingListAdapter(Context context) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(this.mContext);
    }

    public int getCount() {
        return this.listTitle.length;
    }

    public Object getItem(int position) {
        return Integer.valueOf(position);
    }

    public long getItemId(int position) {
        return (long) this.listTitle[position];
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ListViewHolder holder;
        if (convertView == null) {
            convertView = this.mInflater.inflate((int) R.layout.antivirus_main_setting_item, (ViewGroup) null);
            holder = new ListViewHolder(this, null);
            holder.title = (TextView) convertView.findViewById(R.id.antivirus_main_item_title);
            convertView.setTag(holder);
        } else {
            holder = (ListViewHolder) convertView.getTag();
        }
        convertView.setId(this.listTitle[position]);
        holder.title.setText(this.listTitle[position]);
        return convertView;
    }
}
