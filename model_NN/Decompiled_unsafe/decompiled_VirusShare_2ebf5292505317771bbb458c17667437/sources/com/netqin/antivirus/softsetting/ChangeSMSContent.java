package com.netqin.antivirus.softsetting;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.netqin.antivirus.common.CommonMethod;
import com.nqmobile.antivirus_ampro20.R;

public class ChangeSMSContent extends Activity implements View.OnClickListener {
    private TextView changeCcontent;
    private EditText contenName;
    private Button okButton;
    private SharedPreferences spfNetQin;

    public void onCreate(Bundle savedInstanceState) {
        String str;
        String str2;
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.chengesmscontent);
        ((TextView) findViewById(R.id.activity_name)).setText((int) R.string.label_antilost_changesim_smscontent);
        this.changeCcontent = (TextView) findViewById(R.id.antilost_sms_change_content);
        this.contenName = (EditText) findViewById(R.id.antilost_sms_change_content_name);
        this.okButton = (Button) findViewById(R.id.antilost_sms_change_content_name_ok);
        this.okButton.setOnClickListener(this);
        this.spfNetQin = getSharedPreferences("netqin", 0);
        String smsUserName = this.spfNetQin.getString("antilost_sms_change_content_name", "");
        if (smsUserName == null || smsUserName.length() <= 0) {
            if (CommonMethod.isLocalSimpleChinese()) {
                str = String.valueOf(getString(R.string.antilost_sms_change_content, new Object[]{smsUserName})) + "?l=zh_cn";
            } else {
                str = String.valueOf(getString(R.string.antilost_sms_change_content, new Object[]{smsUserName})) + "?l=en_en";
            }
            this.changeCcontent.setText(str);
            return;
        }
        if (CommonMethod.isLocalSimpleChinese()) {
            str2 = String.valueOf(getString(R.string.antilost_sms_change_content2, new Object[]{smsUserName})) + "?l=zh_cn";
        } else {
            str2 = String.valueOf(getString(R.string.antilost_sms_change_content2, new Object[]{smsUserName})) + "?l=en_en";
        }
        this.changeCcontent.setText(str2);
        this.contenName.setText(smsUserName);
    }

    public void onClick(View v) {
        String str;
        switch (v.getId()) {
            case R.id.antilost_sms_change_content_name_ok /*2131558494*/:
                String contenNameStr = this.contenName.getText().toString();
                if (contenNameStr.length() > 0) {
                    if (CommonMethod.isLocalSimpleChinese()) {
                        str = String.valueOf(getString(R.string.antilost_sms_change_content2, new Object[]{contenNameStr})) + "?l=zh_cn";
                    } else {
                        str = String.valueOf(getString(R.string.antilost_sms_change_content2, new Object[]{contenNameStr})) + "?l=en_en";
                    }
                    this.changeCcontent.setText(str);
                    this.spfNetQin.edit().putString("antilost_sms_change_content_name", contenNameStr).commit();
                    return;
                }
                Toast.makeText(this, getString(R.string.antilost_sms_change_content_name_error), 1).show();
                return;
            default:
                return;
        }
    }
}
