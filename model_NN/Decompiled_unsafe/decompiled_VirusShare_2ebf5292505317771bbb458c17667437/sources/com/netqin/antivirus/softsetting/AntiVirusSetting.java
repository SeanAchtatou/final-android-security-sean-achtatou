package com.netqin.antivirus.softsetting;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.netqin.antivirus.antilost.AntiLostPassword;
import com.netqin.antivirus.common.CommonUtils;
import com.netqin.antivirus.networkmanager.SettingPreferences;
import com.nqmobile.antivirus_ampro20.R;

public class AntiVirusSetting extends Activity implements AdapterView.OnItemClickListener {
    private BaseAdapter mListAdapter = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.antivirus_main_setting);
        setRequestedOrientation(1);
        ((TextView) findViewById(R.id.activity_name)).setText((int) R.string.label_setting);
        this.mListAdapter = new MainAllSettingListAdapter(this);
        ListView listview = (ListView) findViewById(R.id.antivirus_main_listview);
        listview.setAdapter((ListAdapter) this.mListAdapter);
        listview.setOnItemClickListener(this);
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        switch (view.getId()) {
            case R.string.meter_activity_title /*2131428221*/:
                startActivity(new Intent(this, SettingPreferences.class));
                return;
            case R.string.soft_setting_security /*2131428222*/:
                startActivity(new Intent(this, SoftsecuritySetting.class));
                return;
            case R.string.soft_setting_lost /*2131428223*/:
                SharedPreferences spf = getSharedPreferences("nq_antilost", 0);
                boolean z = spf.getBoolean("first", true);
                String password = CommonUtils.getConfigWithStringValue(this, "nq_antilost", "password", "");
                boolean running = spf.getBoolean("running", false);
                spf.edit().putBoolean("first_temp", false).commit();
                if (!running || password.length() <= 5) {
                    startActivity(new Intent(this, AntiLostSetting.class));
                    return;
                }
                Intent intent = new Intent(this, AntiLostPassword.class);
                intent.putExtra("tag", "AntiVirusSetting");
                startActivity(intent);
                return;
            case R.string.system_setting /*2131428411*/:
                startActivity(new Intent(this, SystemSetting.class));
                return;
            default:
                return;
        }
    }
}
