package com.netqin.antivirus.softsetting;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.netqin.antivirus.antilost.AntiLostChangePwd;
import com.netqin.antivirus.antilost.AntiLostService;
import com.netqin.antivirus.antilost.AntiLostSetPwd;
import com.netqin.antivirus.antilost.AntiLostSetSecurityNum;
import com.netqin.antivirus.cloud.model.xml.XmlUtils;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.common.CommonUtils;
import com.netqin.antivirus.log.LogEngine;
import com.nqmobile.antivirus_ampro20.R;

public class AntiLostSetting extends Activity implements AdapterView.OnItemClickListener {
    /* access modifiers changed from: private */
    public final int[] listTitle = {R.string.text_antilost_mobile_protect, R.string.text_antilost_pwd, R.string.label_antilost_securitynum, R.string.label_antilost_changesim_sendsms, R.string.label_antilost_changesim_smscontent};
    /* access modifiers changed from: private */
    public SwitchListAdapter mListAdapter;
    private ListView mListView;

    private class ListViewHolder {
        CheckBox check;
        TextView desc;
        TextView title;

        private ListViewHolder() {
        }

        /* synthetic */ ListViewHolder(AntiLostSetting antiLostSetting, ListViewHolder listViewHolder) {
            this();
        }
    }

    private class SwitchListAdapter extends BaseAdapter {
        private Context mContext;
        private LayoutInflater mInflater = LayoutInflater.from(this.mContext);

        public SwitchListAdapter(Context context) {
            this.mContext = context;
        }

        public int getCount() {
            return AntiLostSetting.this.listTitle.length;
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ListViewHolder holder;
            if (convertView == null) {
                convertView = this.mInflater.inflate((int) R.layout.antilost_soft_setting_item, (ViewGroup) null);
                holder = new ListViewHolder(AntiLostSetting.this, null);
                holder.title = (TextView) convertView.findViewById(R.id.antilost_setting_item_title);
                holder.desc = (TextView) convertView.findViewById(R.id.antilost_setting_item_desc);
                holder.check = (CheckBox) convertView.findViewById(R.id.antilost_setting_item_check);
                convertView.setTag(holder);
            } else {
                holder = (ListViewHolder) convertView.getTag();
            }
            holder.title.setText(AntiLostSetting.this.listTitle[position]);
            SharedPreferences spf = AntiLostSetting.this.getSharedPreferences("nq_antilost", 0);
            String sn = CommonUtils.getConfigWithStringValue(AntiLostSetting.this, "nq_antilost", "securitynum", "");
            String psw = CommonUtils.getConfigWithStringValue(AntiLostSetting.this, "nq_antilost", "password", "");
            boolean send = spf.getBoolean("changesim_sendsms", false);
            switch (position) {
                case 0:
                    boolean running = spf.getBoolean("running", false);
                    holder.title.setTextColor(-1);
                    holder.check.setVisibility(0);
                    holder.check.setEnabled(true);
                    if (running) {
                        holder.desc.setText((int) R.string.label_start);
                        holder.desc.setTextColor(-1);
                    } else {
                        holder.desc.setText((int) R.string.label_close);
                        holder.desc.setTextColor(-65536);
                    }
                    holder.desc.setVisibility(0);
                    holder.check.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            AntiLostSetting.this.clickAntiLost();
                        }
                    });
                    holder.check.setChecked(running);
                    break;
                case 1:
                    if (psw == null || psw.length() <= 5) {
                        holder.desc.setText((int) R.string.label_unset);
                    } else {
                        holder.desc.setText((int) R.string.label_set);
                    }
                    if (spf.getBoolean("running", false)) {
                        holder.title.setTextColor(-1);
                        holder.desc.setTextColor(-1);
                    } else {
                        holder.title.setTextColor(-3355444);
                        holder.desc.setTextColor(-3355444);
                    }
                    holder.check.setVisibility(8);
                    holder.desc.setVisibility(0);
                    break;
                case 2:
                    if (sn == null || sn.length() <= 3) {
                        holder.desc.setText((int) R.string.label_unset);
                    } else {
                        holder.desc.setText(sn);
                    }
                    if (spf.getBoolean("running", false)) {
                        holder.title.setTextColor(-1);
                        holder.desc.setTextColor(-1);
                    } else {
                        holder.title.setTextColor(-3355444);
                        holder.desc.setTextColor(-3355444);
                    }
                    holder.check.setVisibility(8);
                    holder.desc.setVisibility(0);
                    break;
                case 3:
                    if (spf.getBoolean("running", false)) {
                        holder.title.setTextColor(-1);
                        holder.desc.setTextColor(-1);
                        holder.check.setVisibility(0);
                        holder.check.setEnabled(true);
                    } else {
                        holder.check.setVisibility(0);
                        holder.check.setEnabled(false);
                        holder.title.setTextColor(-3355444);
                        holder.desc.setTextColor(-3355444);
                    }
                    holder.desc.setText((int) R.string.label_antilost_changesim_sendsms_desc);
                    holder.desc.setVisibility(0);
                    holder.check.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            AntiLostSetting.this.clickSetChangeSIMSendSMS();
                        }
                    });
                    holder.check.setChecked(send);
                    holder.check.setFocusable(false);
                    break;
                case 4:
                    String smsUserName = AntiLostSetting.this.getSharedPreferences("netqin", 0).getString("antilost_sms_change_content_name", "");
                    if (smsUserName == null || smsUserName.length() <= 0) {
                        holder.desc.setText((int) R.string.label_unset);
                    } else {
                        holder.desc.setText((int) R.string.label_set);
                    }
                    holder.check.setVisibility(8);
                    holder.desc.setVisibility(0);
                    holder.desc.setTextColor(-1);
                    holder.title.setTextColor(-1);
                    break;
                default:
                    holder.check.setVisibility(8);
                    holder.desc.setVisibility(0);
                    break;
            }
            return convertView;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.antilost_soft_setting);
        ((TextView) findViewById(R.id.activity_name)).setText((int) R.string.soft_setting_lost);
        this.mListView = (ListView) findViewById(R.id.antilost_switch_listview);
        this.mListAdapter = new SwitchListAdapter(this);
        this.mListView.setAdapter((ListAdapter) this.mListAdapter);
        this.mListView.setOnItemClickListener(this);
    }

    public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
        boolean running = getSharedPreferences("nq_antilost", 0).getBoolean("running", false);
        switch (position) {
            case 0:
                clickAntiLost();
                return;
            case 1:
                if (running) {
                    clickPassword();
                    return;
                }
                return;
            case 2:
                if (running) {
                    clickSecurityNum();
                    return;
                }
                return;
            case 3:
                if (running) {
                    clickSetChangeSIMSendSMS();
                    return;
                }
                return;
            case 4:
                clickSetChangeSMSContent();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.mListAdapter.notifyDataSetChanged();
    }

    private void clickPassword() {
        SharedPreferences sharedPreferences = getSharedPreferences("nq_antilost", 0);
        String pwd = CommonUtils.getConfigWithStringValue(this, "nq_antilost", "password", "");
        if (pwd.length() <= 5 || pwd.length() >= 11) {
            startActivity(new Intent(this, AntiLostSetPwd.class));
        } else {
            startActivity(new Intent(this, AntiLostChangePwd.class));
        }
    }

    private void clickSecurityNum() {
        startActivity(new Intent(this, AntiLostSetSecurityNum.class));
    }

    private void clickSetChangeSMSContent() {
        startActivity(new Intent(this, ChangeSMSContent.class));
    }

    /* access modifiers changed from: private */
    public void clickSetChangeSIMSendSMS() {
        final SharedPreferences spf = getSharedPreferences("nq_antilost", 0);
        if (spf.getBoolean("changesim_sendsms", false)) {
            CommonMethod.yesNoBtnDialogListen(this, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    spf.edit().putBoolean("changesim_sendsms", false).commit();
                    AntiLostSetting.this.mListAdapter.notifyDataSetChanged();
                }
            }, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    AntiLostSetting.this.mListAdapter.notifyDataSetChanged();
                }
            }, getResources().getString(R.string.text_antilost_changesim_sendsms_close_tip), R.string.label_important_tip);
        } else if (!spf.getBoolean("start", false)) {
            CommonMethod.messageDialog(this, (int) R.string.text_antilost_open_fail_tip, (int) R.string.label_netqin_antivirus);
        } else {
            spf.edit().putBoolean("changesim_sendsms", true).commit();
            CommonUtils.putConfigWithStringValue(this, "nq_antilost", XmlUtils.LABEL_MOBILEINFO_IMSI, CommonMethod.getIMSI(this));
        }
        this.mListAdapter.notifyDataSetChanged();
    }

    private void openAntiLost() {
        SharedPreferences spf = getSharedPreferences("nq_antilost", 0);
        String pwd = CommonUtils.getConfigWithStringValue(this, "nq_antilost", "password", "");
        String securityNum = CommonUtils.getConfigWithStringValue(this, "nq_antilost", "securitynum", "");
        if (TextUtils.isEmpty(pwd)) {
            Intent i = new Intent(this, AntiLostSetPwd.class);
            i.putExtra("type", 1);
            startActivity(i);
        } else if (TextUtils.isEmpty(securityNum)) {
            Intent i2 = new Intent(this, AntiLostSetSecurityNum.class);
            i2.putExtra("type", 1);
            startActivity(i2);
        } else {
            CommonMethod.showNotification(this, getResources().getString(R.string.text_antilost_open_done));
            spf.edit().putBoolean("start", true).commit();
            spf.edit().putBoolean("running", true).commit();
            spf.edit().putBoolean("changesim_sendsms", true).commit();
            startService(new Intent(this, AntiLostService.class));
            LogEngine.insertOperationItemLog(11, "", getFilesDir().getPath());
        }
    }

    /* access modifiers changed from: private */
    public void clickAntiLost() {
        final SharedPreferences spf = getSharedPreferences("nq_antilost", 0);
        if (!spf.getBoolean("running", false)) {
            openAntiLost();
            this.mListAdapter.notifyDataSetChanged();
            return;
        }
        CommonMethod.yesNoBtnDialogListen(this, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                spf.edit().putBoolean("start", false).commit();
                spf.edit().putBoolean("running", false).commit();
                spf.edit().putBoolean("changesim_sendsms", false).commit();
                AntiLostSetting.this.stopService(new Intent(AntiLostSetting.this, AntiLostService.class));
                LogEngine.insertOperationItemLog(12, "", AntiLostSetting.this.getFilesDir().getPath());
                AntiLostSetting.this.mListAdapter.notifyDataSetChanged();
            }
        }, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                AntiLostSetting.this.mListAdapter.notifyDataSetChanged();
            }
        }, getResources().getString(R.string.text_antilost_close_tip), R.string.label_netqin_antivirus);
    }
}
