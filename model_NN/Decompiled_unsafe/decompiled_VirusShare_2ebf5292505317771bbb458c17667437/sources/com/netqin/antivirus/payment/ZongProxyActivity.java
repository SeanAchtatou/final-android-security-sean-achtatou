package com.netqin.antivirus.payment;

import android.content.Intent;
import android.os.IBinder;
import com.zong.android.engine.ZpMoConst;
import com.zong.android.engine.activities.ZongPaymentRequest;
import com.zong.android.engine.provider.PhoneState;
import com.zong.android.engine.provider.ZongPhoneManager;
import com.zong.android.engine.ui.ZongUI;
import java.util.Locale;

public class ZongProxyActivity extends PaymentActivity {
    private static final int PAYMENT_ACTIVITY_REQUEST_CODE = 1;

    /* access modifiers changed from: protected */
    public void doCustomCreate(Intent intent) {
        ZongPaymentRequest paymentRequest = new ZongPaymentRequest();
        PhoneState phoneState = ZongPhoneManager.getInstance().getPhoneState(this);
        paymentRequest.setDebugMode(false);
        paymentRequest.setSimulationMode(false);
        paymentRequest.setAppName(intent.getStringExtra("ZongAppNanme"));
        paymentRequest.setCountry(intent.getStringExtra("ZongCountry"));
        paymentRequest.setCurrency(intent.getStringExtra("ZongCurrency"));
        paymentRequest.setCustomerKey(intent.getStringExtra("ZongCustomerKey"));
        paymentRequest.setLang(Locale.getDefault().getLanguage());
        paymentRequest.setMno(phoneState.getSimOp());
        paymentRequest.setPhoneNumber(phoneState.getMsisdn(phoneState.getSimIsoCountry()));
        paymentRequest.setUrl(intent.getStringExtra("ZongUrl"));
        paymentRequest.setTransactionRef(intent.getStringExtra("ZongTransactionRef"));
        String[] keys = intent.getStringArrayExtra("ZongPurchaseKey");
        float[] amounts = intent.getFloatArrayExtra("ZongExactPrice");
        int[] quantities = intent.getIntArrayExtra("ZongQuantity");
        String[] descriptions = intent.getStringArrayExtra("ZongItemDesc");
        String[] labels = intent.getStringArrayExtra("ZongLabel");
        int i = 0;
        while (i < keys.length) {
            try {
                paymentRequest.addPricePoint(keys[i], amounts[i], quantities[i], descriptions[i], labels[i]);
                i++;
            } catch (Exception e) {
                stop(12);
                return;
            }
        }
        Intent i2 = new Intent(this, ZongUI.class);
        i2.putExtra(ZpMoConst.ZONG_MOBILE_PAYMENT_BUNDLE_KEY, paymentRequest);
        startActivityForResult(i2, 1);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (1 != requestCode) {
            return;
        }
        if (-1 == resultCode) {
            stop(11);
        } else if (resultCode == 0 || 2 == resultCode) {
            stop(PaymentHandler.STATUS_CANCALED);
        } else {
            stop(12);
        }
    }

    /* access modifiers changed from: protected */
    public void onConnected(IBinder binder) {
    }
}
