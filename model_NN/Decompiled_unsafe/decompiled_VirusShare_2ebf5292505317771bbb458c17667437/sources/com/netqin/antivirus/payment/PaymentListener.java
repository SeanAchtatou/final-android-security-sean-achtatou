package com.netqin.antivirus.payment;

import android.os.Handler;

public interface PaymentListener {
    boolean setCallback(Handler.Callback callback);
}
