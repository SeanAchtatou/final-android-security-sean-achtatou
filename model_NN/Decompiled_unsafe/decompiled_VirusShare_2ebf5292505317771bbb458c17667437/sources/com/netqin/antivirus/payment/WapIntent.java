package com.netqin.antivirus.payment;

import android.content.Context;

class WapIntent extends PaymentIntent {
    protected WapIntent(int type, Context context) {
        super(type, context);
        setClass(context, WapPayment.class);
        putExtra("com.netqin.payment.action", "com.netqin.payment.WapPayment");
    }

    /* access modifiers changed from: protected */
    public boolean checkAttributes() throws NoSuchParameterException {
        getString(this, "WAPURL");
        getString(this, "WAPSuccessSign");
        getString(this, "WAPConfirmMatch");
        getString(this, "WAPRule");
        if (getString(this, "RequestMethod").equals("POST")) {
            getString(this, "WAPPostData");
        }
        getLong(this, "WAPInternal");
        getInt(this, "WAPCount");
        getInt(this, "WAPPageIndex");
        return true;
    }
}
