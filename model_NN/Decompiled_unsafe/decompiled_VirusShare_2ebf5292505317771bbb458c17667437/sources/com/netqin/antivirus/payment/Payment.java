package com.netqin.antivirus.payment;

import android.content.Context;

public class Payment {
    private static Payment instance;
    private Context context;
    private PaymentHandler handler;
    private PaymentIntent intent;
    private String key;

    protected static synchronized PaymentHandler getHandler(String key2) {
        PaymentHandler paymentHandler;
        synchronized (Payment.class) {
            if (instance != null) {
                if (instance.equals(key2)) {
                    paymentHandler = instance.handler;
                } else {
                    instance = null;
                }
            }
            paymentHandler = null;
        }
        return paymentHandler;
    }

    protected static synchronized void finish() {
        synchronized (Payment.class) {
            instance = null;
        }
    }

    public static synchronized boolean startPayment(Context context2, PaymentHandler handler2, PaymentIntent intent2) {
        boolean z;
        synchronized (Payment.class) {
            if (instance == null) {
                try {
                    instance = new Payment(context2, handler2, intent2);
                    z = instance.startPayment();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            instance = null;
            z = false;
        }
        return z;
    }

    private Payment(Context context2, PaymentHandler handler2, PaymentIntent intent2) {
        if (handler2 == null || context2 == null) {
            throw new NullPointerException();
        }
        this.context = context2;
        this.handler = handler2;
        this.intent = intent2;
        this.key = Long.toHexString(System.currentTimeMillis());
        this.intent.putExtra("com.netqin.payment.key", this.key);
    }

    public boolean startPayment() throws NoSuchParameterException {
        if (!this.intent.checkAttributes()) {
            return false;
        }
        this.context.startService(this.intent);
        return true;
    }

    public synchronized boolean equals(Object obj) {
        boolean z;
        if (obj != null) {
            if (obj instanceof String) {
                if (this.key.equals(obj)) {
                    z = true;
                } else {
                    instance = null;
                }
            }
        }
        z = false;
        return z;
    }
}
