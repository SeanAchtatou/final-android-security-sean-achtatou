package com.netqin.antivirus.payment;

import android.content.Context;

public class ZongIntent extends PaymentIntent {
    protected ZongIntent(int type, Context context) {
        super(type, context);
        setClass(context, ZongPayment.class);
        putExtra("com.netqin.payment.action", "com.netqin.payment.ZongPayment");
    }

    /* access modifiers changed from: protected */
    public boolean checkAttributes() throws NoSuchParameterException {
        getString(this, "ZongAppNanme");
        getString(this, "ZongCountry");
        getString(this, "ZongCurrency");
        getString(this, "ZongCustomerKey");
        getString(this, "ZongUrl");
        getString(this, "ZongTransactionRef");
        return true;
    }
}
