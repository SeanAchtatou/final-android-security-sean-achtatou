package com.netqin.antivirus.payment;

import android.os.Handler;
import android.os.Message;

public abstract class PaymentHandler extends Handler {
    protected static final int STATUS_ACTIVITY_FINISH = -100;
    public static final int STATUS_CANCALED = 207;
    public static final int STATUS_CANCALED_AT_PROMPT = -1;
    protected static final int STATUS_CONFIRM_OK = 1005;
    public static final int STATUS_EWapPaymentConfirmedTimeout = 203;
    public static final int STATUS_EWapPaymentFailed = 204;
    public static final int STATUS_EWapPaymentSucceed = 201;
    public static final int STATUS_EWapPaymentTimeout = 202;
    public static final int STATUS_EwapPaymentConfirmedFailed = 205;
    public static final int STATUS_FAILED = -200;
    public static final int STATUS_MISS_PARAMETERS = -99;
    public static final int STATUS_Others = 206;
    protected static final int STATUS_PROMPT_OK = 1001;
    protected static final int STATUS_PROMPT_RE_OK = 1002;
    public static final int STATUS_RevConfirmSms_Timeout = 5;
    protected static final int STATUS_SET_CALLBACK = 1004;
    protected static final int STATUS_SHOW_CONFIRM = 1003;
    protected static final int STATUS_SHOW_SMS_CONTENT = 1006;
    public static final int STATUS_SUCCESS = 100;
    public static final int STATUS_SendConfirmSms_Failed = 4;
    public static final int STATUS_SendConfirmSms_OK = 3;
    public static final int STATUS_SendSubscribeSms_Failed = 1;
    public static final int STATUS_SendSubscribeSms_OK = 2;
    public static final int STATUS_UNSUPPORT_TYPE = -404;
    public static final int STATUS_Zong_FAILED = 12;
    public static final int STATUS_Zong_SUCCESS = 11;

    /* access modifiers changed from: protected */
    public abstract void handleMessage(int i, Message message);

    public final void handleMessage(Message msg) {
        handleMessage(msg.what, msg);
    }
}
