package com.netqin.antivirus.payment;

import android.content.Intent;
import android.os.IBinder;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.nqmobile.antivirus_ampro20.R;

public class SMSPromptActivity extends PaymentActivity {
    /* access modifiers changed from: protected */
    public void doCustomCreate(Intent intent) {
        setContentView((int) R.layout.payment_sms_activity);
        ((TextView) findViewById(R.id.payment_sms_text)).setText(intent.getStringExtra("PaymentPrompt"));
        ((Button) findViewById(R.id.payment_prompt_do)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SMSPromptActivity.this.onClickNext();
            }
        });
        ((Button) findViewById(R.id.payment_prompt_cancel)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SMSPromptActivity.this.onClickCancel();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onConnected(IBinder binder) {
    }
}
