package com.netqin.antivirus.payment;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import com.netqin.antivirus.contact.vcard.VCardConfig;
import com.nqmobile.antivirus_ampro20.R;

public class SMSPayment extends PaymentService {
    private static final int RECONFIRM = -1002;
    private static final int SEND_RECONFIRM = -1001;
    private static final int SEND_SMS = -1000;
    private boolean ReceiveReconfirmVisible;
    private boolean SendReconfirmVisible;
    private boolean SendVisible;
    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            SMSPayment.this.onMessage(msg);
        }
    };
    private boolean prograssVisible;
    private SMSTransform transform;
    protected Handler uhandler;

    /* access modifiers changed from: protected */
    public void startPayment(Intent intent) {
        boolean z;
        this.SendVisible = intent.getBooleanExtra("SMSSendVisible", false);
        this.ReceiveReconfirmVisible = intent.getBooleanExtra("SMSReceiveReconfirmVisible", false);
        this.SendReconfirmVisible = intent.getBooleanExtra("SMSSendReconfirmVisible", false);
        this.prograssVisible = this.SendReconfirmVisible | this.ReceiveReconfirmVisible | this.SendVisible;
        try {
            this.prograssVisible |= PaymentIntent.getString(intent, "PaymentPrompt").length() > 0;
        } catch (Exception e) {
        }
        try {
            boolean z2 = this.prograssVisible;
            if (PaymentIntent.getString(intent, "PaymentReConfirmPrompt").length() > 0) {
                z = true;
            } else {
                z = false;
            }
            this.prograssVisible = z2 | z;
        } catch (Exception e2) {
        }
        if (this.SendVisible) {
            StringBuffer sb = new StringBuffer();
            sb.append(getString(R.string.payment_sms_receiver)).append(intent.getStringExtra("SMSNumber"));
            sb.append("\n").append(getString(R.string.payment_sms_content)).append(intent.getStringExtra("SMSSendContent"));
            Intent i = new Intent(this, SMSPromptActivity.class);
            i.addFlags(VCardConfig.FLAG_USE_QP_TO_PRIMARY_PROPERTIES);
            i.setAction(intent.getStringExtra("com.netqin.payment.action"));
            i.putExtra("PaymentPrompt", sb.toString());
            i.putExtra("com.netqin.payment.status", (int) SEND_SMS);
            i.putExtra("com.netqin.payment.cancel", (int) PaymentHandler.STATUS_CANCALED);
            startActivity(i);
            return;
        }
        start();
    }

    private void start() {
        if (this.prograssVisible) {
            Intent i = new Intent(this, ProgressActivity.class);
            i.addFlags(VCardConfig.FLAG_USE_QP_TO_PRIMARY_PROPERTIES);
            i.setAction(this.intent.getStringExtra("com.netqin.payment.action"));
            startActivity(i);
            return;
        }
        this.transform = new SMSTransform(this, this.intent, this.handler);
        this.transform.start();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0091, code lost:
        r2 = r5.substring(1, (r9 = r5.indexOf("*", 1)));
        r8 = r5.substring(r9 + 1, r5.length() - 1);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean doNext(android.os.Message r15) {
        /*
            r14 = this;
            r13 = 1
            int r11 = r15.what
            switch(r11) {
                case -1002: goto L_0x003f;
                case -1001: goto L_0x00ef;
                case -1000: goto L_0x003b;
                case 1001: goto L_0x00f6;
                case 1002: goto L_0x0116;
                case 1004: goto L_0x001f;
                default: goto L_0x0006;
            }
        L_0x0006:
            android.os.Handler r11 = r14.uhandler
            if (r11 == 0) goto L_0x0014
            android.os.Handler r11 = r14.uhandler     // Catch:{ Exception -> 0x0126 }
            r12 = -100
            r11.sendEmptyMessage(r12)     // Catch:{ Exception -> 0x0126 }
        L_0x0011:
            r11 = 0
            r14.uhandler = r11
        L_0x0014:
            com.netqin.antivirus.payment.SMSTransform r11 = r14.transform
            if (r11 == 0) goto L_0x011f
            com.netqin.antivirus.payment.SMSTransform r11 = r14.transform
            r11.cancel()
        L_0x001d:
            r11 = 0
            return r11
        L_0x001f:
            android.os.Handler r12 = new android.os.Handler
            java.lang.Object r11 = r15.obj
            android.os.Handler$Callback r11 = (android.os.Handler.Callback) r11
            r12.<init>(r11)
            r14.uhandler = r12
            com.netqin.antivirus.payment.SMSTransform r11 = new com.netqin.antivirus.payment.SMSTransform
            android.content.Intent r12 = r14.intent
            android.os.Handler r13 = r14.handler
            r11.<init>(r14, r12, r13)
            r14.transform = r11
            com.netqin.antivirus.payment.SMSTransform r11 = r14.transform
            r11.start()
            goto L_0x001d
        L_0x003b:
            r14.start()
            goto L_0x001d
        L_0x003f:
            boolean r11 = r14.SendReconfirmVisible
            if (r11 == 0) goto L_0x00ef
            java.lang.StringBuffer r6 = new java.lang.StringBuffer
            r6.<init>()
            r11 = 2131428283(0x7f0b03bb, float:1.8478206E38)
            java.lang.String r11 = r14.getString(r11)
            java.lang.StringBuffer r11 = r6.append(r11)
            com.netqin.antivirus.payment.SMSTransform r12 = r14.transform
            java.lang.String r12 = r12.getReconfirmNumber()
            r11.append(r12)
            java.lang.String r11 = "\n"
            java.lang.StringBuffer r11 = r6.append(r11)
            r12 = 2131428284(0x7f0b03bc, float:1.8478208E38)
            java.lang.String r12 = r14.getString(r12)
            r11.append(r12)
            com.netqin.antivirus.payment.SMSTransform r11 = r14.transform
            java.lang.String r0 = r11.getReconfirmContent()
            android.content.Intent r11 = r14.intent
            java.lang.String r12 = "SMSReconfirmContent"
            java.lang.String r5 = r11.getStringExtra(r12)
            r10 = r5
            java.lang.String r11 = "*"
            boolean r11 = r5.startsWith(r11)
            if (r11 == 0) goto L_0x00b9
            java.lang.String r11 = "*"
            boolean r11 = r5.endsWith(r11)
            if (r11 == 0) goto L_0x00b9
            int r11 = r5.length()
            if (r11 <= r13) goto L_0x00b9
            java.lang.String r11 = "*"
            int r9 = r5.indexOf(r11, r13)
            java.lang.String r2 = r5.substring(r13, r9)
            int r11 = r9 + 1
            int r12 = r5.length()
            int r12 = r12 - r13
            java.lang.String r8 = r5.substring(r11, r12)
            int r1 = r0.indexOf(r2)
            int r7 = r0.indexOf(r8)
            if (r7 <= r1) goto L_0x00b9
            int r11 = r2.length()
            int r11 = r11 + r1
            java.lang.String r10 = r0.substring(r11, r7)
        L_0x00b9:
            r6.append(r10)
            android.content.Intent r3 = new android.content.Intent
            java.lang.Class<com.netqin.antivirus.payment.SMSPromptActivity> r11 = com.netqin.antivirus.payment.SMSPromptActivity.class
            r3.<init>(r14, r11)
            r11 = 268435456(0x10000000, float:2.5243549E-29)
            r3.addFlags(r11)
            android.content.Intent r11 = r14.intent
            java.lang.String r12 = "com.netqin.payment.action"
            java.lang.String r11 = r11.getStringExtra(r12)
            r3.setAction(r11)
            java.lang.String r11 = "PaymentPrompt"
            java.lang.String r12 = r6.toString()
            r3.putExtra(r11, r12)
            java.lang.String r11 = "com.netqin.payment.status"
            r12 = -1001(0xfffffffffffffc17, float:NaN)
            r3.putExtra(r11, r12)
            java.lang.String r11 = "com.netqin.payment.cancel"
            r12 = 207(0xcf, float:2.9E-43)
            r3.putExtra(r11, r12)
            r14.startActivity(r3)
            goto L_0x001d
        L_0x00ef:
            com.netqin.antivirus.payment.SMSTransform r11 = r14.transform
            r11.doNext()
            goto L_0x001d
        L_0x00f6:
            android.content.Intent r11 = r14.intent
            java.lang.String r12 = "PaymentReConfirmPrompt"
            java.lang.String r4 = r11.getStringExtra(r12)
            if (r4 == 0) goto L_0x010d
            int r11 = r4.length()
            if (r11 <= 0) goto L_0x010d
            r11 = 1002(0x3ea, float:1.404E-42)
            r14.showPrompt(r4, r11)
            goto L_0x001d
        L_0x010d:
            android.content.Intent r11 = r14.createIntent()
            r14.startPayment(r11)
            goto L_0x001d
        L_0x0116:
            android.content.Intent r11 = r14.createIntent()
            r14.startPayment(r11)
            goto L_0x001d
        L_0x011f:
            int r11 = r15.what
            r14.sendResult(r11)
            goto L_0x001d
        L_0x0126:
            r11 = move-exception
            goto L_0x0011
        */
        throw new UnsupportedOperationException("Method not decompiled: com.netqin.antivirus.payment.SMSPayment.doNext(android.os.Message):boolean");
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x00bc, code lost:
        r2 = r5.substring(1, (r9 = r5.indexOf("*", 1)));
        r8 = r5.substring(r9 + 1, r5.length() - 1);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMessage(android.os.Message r15) {
        /*
            r14 = this;
            int r11 = r15.what
            switch(r11) {
                case 1003: goto L_0x0019;
                default: goto L_0x0005;
            }
        L_0x0005:
            android.os.Handler r11 = r14.uhandler
            if (r11 == 0) goto L_0x0013
            android.os.Handler r11 = r14.uhandler     // Catch:{ Exception -> 0x0124 }
            r12 = -100
            r11.sendEmptyMessage(r12)     // Catch:{ Exception -> 0x0124 }
        L_0x0010:
            r11 = 0
            r14.uhandler = r11
        L_0x0013:
            int r11 = r15.what
            r14.sendResult(r11)
        L_0x0018:
            return
        L_0x0019:
            boolean r11 = r14.ReceiveReconfirmVisible
            if (r11 == 0) goto L_0x0069
            android.content.Intent r4 = new android.content.Intent
            java.lang.Class<com.netqin.antivirus.payment.PromptActivity> r11 = com.netqin.antivirus.payment.PromptActivity.class
            r4.<init>(r14, r11)
            r11 = 268435456(0x10000000, float:2.5243549E-29)
            r4.addFlags(r11)
            android.content.Intent r11 = r14.intent
            java.lang.String r12 = "com.netqin.payment.action"
            java.lang.String r11 = r11.getStringExtra(r12)
            r4.setAction(r11)
            java.lang.String r11 = "PaymentPrompt"
            java.lang.Object r12 = r15.obj
            java.lang.String r12 = r12.toString()
            r4.putExtra(r11, r12)
            java.lang.String r11 = "com.netqin.payment.status"
            r12 = -1002(0xfffffffffffffc16, float:NaN)
            r4.putExtra(r11, r12)
            java.lang.String r11 = "com.netqin.payment.cancel"
            r12 = 207(0xcf, float:2.9E-43)
            r4.putExtra(r11, r12)
            java.lang.String r11 = "PaymentConfirmBtn"
            r12 = 2131428353(0x7f0b0401, float:1.8478348E38)
            java.lang.String r12 = r14.getString(r12)
            r4.putExtra(r11, r12)
            java.lang.String r11 = "PaymentCancelBtn"
            r12 = 2131427377(0x7f0b0031, float:1.8476369E38)
            java.lang.String r12 = r14.getString(r12)
            r4.putExtra(r11, r12)
            r14.startActivity(r4)
            goto L_0x0018
        L_0x0069:
            boolean r11 = r14.SendReconfirmVisible
            if (r11 == 0) goto L_0x011d
            java.lang.StringBuffer r6 = new java.lang.StringBuffer
            r6.<init>()
            r11 = 2131428283(0x7f0b03bb, float:1.8478206E38)
            java.lang.String r11 = r14.getString(r11)
            java.lang.StringBuffer r11 = r6.append(r11)
            com.netqin.antivirus.payment.SMSTransform r12 = r14.transform
            java.lang.String r12 = r12.getReconfirmNumber()
            r11.append(r12)
            java.lang.String r11 = "\n"
            java.lang.StringBuffer r11 = r6.append(r11)
            r12 = 2131428284(0x7f0b03bc, float:1.8478208E38)
            java.lang.String r12 = r14.getString(r12)
            r11.append(r12)
            com.netqin.antivirus.payment.SMSTransform r11 = r14.transform
            java.lang.String r0 = r11.getReconfirmContent()
            android.content.Intent r11 = r14.intent
            java.lang.String r12 = "SMSReconfirmContent"
            java.lang.String r5 = r11.getStringExtra(r12)
            r10 = r5
            java.lang.String r11 = "*"
            boolean r11 = r5.startsWith(r11)
            if (r11 == 0) goto L_0x00e7
            java.lang.String r11 = "*"
            boolean r11 = r5.endsWith(r11)
            if (r11 == 0) goto L_0x00e7
            int r11 = r5.length()
            r12 = 1
            if (r11 <= r12) goto L_0x00e7
            java.lang.String r11 = "*"
            r12 = 1
            int r9 = r5.indexOf(r11, r12)
            r11 = 1
            java.lang.String r2 = r5.substring(r11, r9)
            int r11 = r9 + 1
            int r12 = r5.length()
            r13 = 1
            int r12 = r12 - r13
            java.lang.String r8 = r5.substring(r11, r12)
            int r1 = r0.indexOf(r2)
            int r7 = r0.indexOf(r8)
            if (r7 <= r1) goto L_0x00e7
            int r11 = r2.length()
            int r11 = r11 + r1
            java.lang.String r10 = r0.substring(r11, r7)
        L_0x00e7:
            r6.append(r10)
            android.content.Intent r3 = new android.content.Intent
            java.lang.Class<com.netqin.antivirus.payment.SMSPromptActivity> r11 = com.netqin.antivirus.payment.SMSPromptActivity.class
            r3.<init>(r14, r11)
            r11 = 268435456(0x10000000, float:2.5243549E-29)
            r3.addFlags(r11)
            android.content.Intent r11 = r14.intent
            java.lang.String r12 = "com.netqin.payment.action"
            java.lang.String r11 = r11.getStringExtra(r12)
            r3.setAction(r11)
            java.lang.String r11 = "PaymentPrompt"
            java.lang.String r12 = r6.toString()
            r3.putExtra(r11, r12)
            java.lang.String r11 = "com.netqin.payment.status"
            r12 = -1001(0xfffffffffffffc17, float:NaN)
            r3.putExtra(r11, r12)
            java.lang.String r11 = "com.netqin.payment.cancel"
            r12 = 207(0xcf, float:2.9E-43)
            r3.putExtra(r11, r12)
            r14.startActivity(r3)
            goto L_0x0018
        L_0x011d:
            com.netqin.antivirus.payment.SMSTransform r11 = r14.transform
            r11.doNext()
            goto L_0x0018
        L_0x0124:
            r11 = move-exception
            goto L_0x0010
        */
        throw new UnsupportedOperationException("Method not decompiled: com.netqin.antivirus.payment.SMSPayment.onMessage(android.os.Message):void");
    }
}
