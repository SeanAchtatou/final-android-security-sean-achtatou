package com.netqin.antivirus.payment;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.text.TextUtils;
import java.util.HashMap;

public class WapTransform extends Thread {
    static final String FEATURE_ENABLE_DUN = "enableDUN";
    static final String FEATURE_ENABLE_HIPRI = "enableHIPRI";
    static final String FEATURE_ENABLE_MMS = "enableMMS";
    static final String FEATURE_ENABLE_SUPL = "enableSUPL";
    static final String FEATURE_ENABLE_WIFI = "enableWIFI";
    private boolean MobileDataEnabled = true;
    private String content_type = "application/x-www-form-urlencoded";
    private Context context;
    private long curApnId = -1;
    private Handler handler;
    private Intent intent;
    private boolean isChangedApn = false;
    private boolean isUseWIFI = false;
    private ConnectivityManager mConnMgr;
    private int[] rule;
    private boolean stop;
    private boolean wait;

    WapTransform(Context context2, Intent intent2, Handler handler2) {
        this.context = context2;
        this.intent = intent2;
        this.handler = handler2;
        String[] s = intent2.getStringExtra("WAPRule").split("-");
        this.rule = new int[s.length];
        for (int i = 0; i < s.length; i++) {
            this.rule[i] = Integer.parseInt(s[i]);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x007c, code lost:
        r11 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x007d, code lost:
        r0 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        r15.handler.sendEmptyMessage(205);
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        recoverApn();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x008f, code lost:
        r11 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0090, code lost:
        r0 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        r15.handler.sendEmptyMessage(204);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0098, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0099, code lost:
        r11 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
        r11.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        recoverApn();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00c9, code lost:
        r11 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00ca, code lost:
        r0 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:?, code lost:
        r15.handler.sendEmptyMessage(205);
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:?, code lost:
        recoverApn();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x007c A[ExcHandler: UnsupportedEncodingException (r11v5 'e' java.io.UnsupportedEncodingException A[CUSTOM_DECLARE]), Splitter:B:1:0x0001] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00c9 A[ExcHandler: Throwable (r11v1 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:1:0x0001] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void run() {
        /*
            r15 = this;
            monitor-enter(r15)
            r15.changeWapAPN()     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            java.lang.String r8 = ""
            android.content.Intent r11 = r15.intent     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            java.lang.String r12 = "WAPCount"
            r13 = 1
            int r2 = r11.getIntExtra(r12, r13)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
        L_0x000f:
            if (r2 <= 0) goto L_0x0015
            boolean r11 = r15.stop     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            if (r11 == 0) goto L_0x0021
        L_0x0015:
            android.os.Handler r11 = r15.handler     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            r12 = 201(0xc9, float:2.82E-43)
            r11.sendEmptyMessage(r12)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            r15.recoverApn()     // Catch:{ all -> 0x008c }
        L_0x001f:
            monitor-exit(r15)
            return
        L_0x0021:
            r9 = 0
            android.content.Intent r11 = r15.intent     // Catch:{ IOException -> 0x008f, UnsupportedEncodingException -> 0x007c, Throwable -> 0x00c9 }
            java.lang.String r12 = "WAPURL"
            java.lang.String r11 = r11.getStringExtra(r12)     // Catch:{ IOException -> 0x008f, UnsupportedEncodingException -> 0x007c, Throwable -> 0x00c9 }
            android.content.Intent r12 = r15.intent     // Catch:{ IOException -> 0x008f, UnsupportedEncodingException -> 0x007c, Throwable -> 0x00c9 }
            java.lang.String r13 = "RequestMethod"
            java.lang.String r12 = r12.getStringExtra(r13)     // Catch:{ IOException -> 0x008f, UnsupportedEncodingException -> 0x007c, Throwable -> 0x00c9 }
            android.content.Intent r13 = r15.intent     // Catch:{ IOException -> 0x008f, UnsupportedEncodingException -> 0x007c, Throwable -> 0x00c9 }
            java.lang.String r14 = "WAPPostData"
            java.lang.String r13 = r13.getStringExtra(r14)     // Catch:{ IOException -> 0x008f, UnsupportedEncodingException -> 0x007c, Throwable -> 0x00c9 }
            com.netqin.antivirus.payment.WapTransform$Response r9 = r15.processData(r11, r12, r13)     // Catch:{ IOException -> 0x008f, UnsupportedEncodingException -> 0x007c, Throwable -> 0x00c9 }
            int r11 = r9.rpcode     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            r12 = 400(0x190, float:5.6E-43)
            if (r11 >= r12) goto L_0x004e
            int r11 = r9.rpcode     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            r12 = 200(0xc8, float:2.8E-43)
            if (r11 >= r12) goto L_0x00a3
        L_0x004e:
            android.os.Handler r11 = r15.handler     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            r12 = 204(0xcc, float:2.86E-43)
            r11.sendEmptyMessage(r12)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            java.io.IOException r11 = new java.io.IOException     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            java.lang.String r13 = "Bad response: "
            r12.<init>(r13)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            int r13 = r9.rpcode     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            java.lang.String r13 = " "
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            java.lang.String r13 = r9.responseMessage     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            java.lang.String r12 = r12.toString()     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            r11.<init>(r12)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            throw r11     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
        L_0x007c:
            r11 = move-exception
            r0 = r11
            android.os.Handler r11 = r15.handler     // Catch:{ all -> 0x0146 }
            r12 = 205(0xcd, float:2.87E-43)
            r11.sendEmptyMessage(r12)     // Catch:{ all -> 0x0146 }
            r0.printStackTrace()     // Catch:{ all -> 0x0146 }
            r15.recoverApn()     // Catch:{ all -> 0x008c }
            goto L_0x001f
        L_0x008c:
            r11 = move-exception
            monitor-exit(r15)
            throw r11
        L_0x008f:
            r11 = move-exception
            r0 = r11
            android.os.Handler r11 = r15.handler     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            r12 = 204(0xcc, float:2.86E-43)
            r11.sendEmptyMessage(r12)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            throw r0     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
        L_0x0099:
            r11 = move-exception
            r0 = r11
            r0.printStackTrace()     // Catch:{ all -> 0x0146 }
            r15.recoverApn()     // Catch:{ all -> 0x008c }
            goto L_0x001f
        L_0x00a3:
            r7 = 0
        L_0x00a4:
            int[] r11 = r15.rule     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            int r11 = r11.length     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            if (r7 >= r11) goto L_0x00ad
            boolean r11 = r15.stop     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            if (r11 == 0) goto L_0x00da
        L_0x00ad:
            android.content.Intent r11 = r15.intent     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            java.lang.String r12 = "WAPSuccessSign"
            java.lang.String r11 = r11.getStringExtra(r12)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            int r11 = r8.indexOf(r11)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            r12 = -1
            if (r11 != r12) goto L_0x0219
            android.os.Handler r11 = r15.handler     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            r12 = 205(0xcd, float:2.87E-43)
            r11.sendEmptyMessage(r12)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            java.io.IOException r11 = new java.io.IOException     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            r11.<init>(r8)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            throw r11     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
        L_0x00c9:
            r11 = move-exception
            r0 = r11
            android.os.Handler r11 = r15.handler     // Catch:{ all -> 0x0146 }
            r12 = 205(0xcd, float:2.87E-43)
            r11.sendEmptyMessage(r12)     // Catch:{ all -> 0x0146 }
            r0.printStackTrace()     // Catch:{ all -> 0x0146 }
            r15.recoverApn()     // Catch:{ all -> 0x008c }
            goto L_0x001f
        L_0x00da:
            java.lang.String r8 = new java.lang.String     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            byte[] r11 = r9.content     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            java.lang.String r12 = "utf-8"
            r8.<init>(r11, r12)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            java.lang.String r11 = "<wml>"
            int r11 = r8.indexOf(r11)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            int r11 = r11 + 5
            java.lang.String r12 = "</wml>"
            int r12 = r8.indexOf(r12)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            java.lang.String r11 = r8.substring(r11, r12)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            java.lang.String r8 = r11.trim()     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            java.lang.String r11 = "\r\n"
            java.lang.String r12 = ""
            java.lang.String r8 = r8.replaceAll(r11, r12)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            java.lang.String r11 = "\n"
            java.lang.String r12 = ""
            java.lang.String r8 = r8.replaceAll(r11, r12)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            java.lang.String r11 = "<a.*?(http[s]?://.*?)+?(?:\"|'|)>.+?a\\s*>"
            java.util.regex.Pattern r6 = java.util.regex.Pattern.compile(r11)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            java.util.regex.Matcher r4 = r6.matcher(r8)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            r3.<init>()     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
        L_0x011a:
            boolean r11 = r4.find()     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            if (r11 != 0) goto L_0x014b
            int[] r11 = r15.rule     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            r11 = r11[r7]     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            int r12 = r3.size()     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            if (r11 <= r12) goto L_0x015f
            android.os.Handler r11 = r15.handler     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            r12 = 204(0xcc, float:2.86E-43)
            r11.sendEmptyMessage(r12)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            java.io.IOException r11 = new java.io.IOException     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            java.lang.String r13 = "Can't get confirm url: "
            r12.<init>(r13)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            java.lang.StringBuilder r12 = r12.append(r8)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            java.lang.String r12 = r12.toString()     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            r11.<init>(r12)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            throw r11     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
        L_0x0146:
            r11 = move-exception
            r15.recoverApn()     // Catch:{ all -> 0x008c }
            throw r11     // Catch:{ all -> 0x008c }
        L_0x014b:
            com.netqin.antivirus.payment.WapTransform$Href r11 = new com.netqin.antivirus.payment.WapTransform$Href     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            r12 = 0
            java.lang.String r12 = r4.group(r12)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            r13 = 1
            java.lang.String r13 = r4.group(r13)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            r14 = 0
            r11.<init>(r12, r13, r14)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            r3.add(r11)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            goto L_0x011a
        L_0x015f:
            int[] r11 = r15.rule     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            r11 = r11[r7]     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            r12 = 1
            int r11 = r11 - r12
            java.lang.Object r0 = r3.get(r11)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            com.netqin.antivirus.payment.WapTransform$Href r0 = (com.netqin.antivirus.payment.WapTransform.Href) r0     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            java.lang.String r10 = r0.url     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            android.content.Intent r11 = r15.intent     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            java.lang.String r12 = "WAPConfirmMatch"
            java.lang.String r11 = r11.getStringExtra(r12)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            boolean r11 = com.netqin.antivirus.payment.PaymentUtil.matches(r8, r11)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            if (r11 == 0) goto L_0x01c6
            java.util.Iterator r11 = r3.iterator()     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
        L_0x0181:
            boolean r12 = r11.hasNext()     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            if (r12 != 0) goto L_0x01a9
            android.os.Handler r11 = r15.handler     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            r12 = 1003(0x3eb, float:1.406E-42)
            android.os.Message r5 = r11.obtainMessage(r12, r8)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            android.os.Handler r11 = r15.handler     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            r11.sendMessage(r5)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            r11 = 1
            r15.wait = r11     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
        L_0x0197:
            boolean r11 = r15.wait     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            if (r11 == 0) goto L_0x019f
            boolean r11 = r15.stop     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            if (r11 == 0) goto L_0x01c0
        L_0x019f:
            boolean r11 = r15.stop     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            if (r11 == 0) goto L_0x01c6
            java.io.EOFException r11 = new java.io.EOFException     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            r11.<init>()     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            throw r11     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
        L_0x01a9:
            java.lang.Object r1 = r11.next()     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            com.netqin.antivirus.payment.WapTransform$Href r1 = (com.netqin.antivirus.payment.WapTransform.Href) r1     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            boolean r12 = r1.equals(r10)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            if (r12 == 0) goto L_0x0181
            java.lang.String r12 = r1.html     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            java.lang.String r13 = ""
            java.lang.String r8 = r8.replace(r12, r13)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            goto L_0x0181
        L_0x01c0:
            r15.wait()     // Catch:{ Exception -> 0x01c4 }
            goto L_0x0197
        L_0x01c4:
            r11 = move-exception
            goto L_0x0197
        L_0x01c6:
            java.lang.String r11 = "GET"
            r12 = 0
            com.netqin.antivirus.payment.WapTransform$Response r9 = r15.processData(r10, r11, r12)     // Catch:{ IOException -> 0x020b, UnsupportedEncodingException -> 0x007c, Throwable -> 0x00c9 }
            int r11 = r9.rpcode     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            r12 = 400(0x190, float:5.6E-43)
            if (r11 >= r12) goto L_0x01dd
            int r11 = r9.rpcode     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            r12 = 200(0xc8, float:2.8E-43)
            if (r11 >= r12) goto L_0x0215
        L_0x01dd:
            android.os.Handler r11 = r15.handler     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            r12 = 205(0xcd, float:2.87E-43)
            r11.sendEmptyMessage(r12)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            java.io.IOException r11 = new java.io.IOException     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            java.lang.String r13 = "Bad response: "
            r12.<init>(r13)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            int r13 = r9.rpcode     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            java.lang.String r13 = " "
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            java.lang.String r13 = r9.responseMessage     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            java.lang.String r12 = r12.toString()     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            r11.<init>(r12)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            throw r11     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
        L_0x020b:
            r11 = move-exception
            r0 = r11
            android.os.Handler r11 = r15.handler     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            r12 = 205(0xcd, float:2.87E-43)
            r11.sendEmptyMessage(r12)     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
            throw r0     // Catch:{ UnsupportedEncodingException -> 0x007c, IOException -> 0x0099, Throwable -> 0x00c9 }
        L_0x0215:
            int r7 = r7 + 1
            goto L_0x00a4
        L_0x0219:
            int r2 = r2 + -1
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.netqin.antivirus.payment.WapTransform.run():void");
    }

    /* access modifiers changed from: protected */
    public synchronized void doNext() {
        this.wait = false;
        notify();
    }

    private void changeWapAPN() throws Exception {
        this.mConnMgr = (ConnectivityManager) this.context.getSystemService("connectivity");
        try {
            this.MobileDataEnabled = this.mConnMgr.getMobileDataEnabled();
            if (!this.MobileDataEnabled) {
                this.mConnMgr.setMobileDataEnabled(true);
            }
        } catch (Throwable th) {
            this.MobileDataEnabled = true;
            th.printStackTrace();
        }
        WifiManager wifiManager = (WifiManager) this.context.getSystemService("wifi");
        this.isUseWIFI = wifiManager.isWifiEnabled();
        HashMap<String, String> map = PaymentUtil.getCurrentApnIdAndProxy(this.context);
        String curProxy = null;
        if (!TextUtils.isEmpty(map.get("id"))) {
            this.curApnId = Long.parseLong(map.get("id"));
            curProxy = map.get("proxy");
        }
        if (curProxy == null || "".equals(curProxy)) {
            long apnId = PaymentUtil.getProxyId(this.context);
            if (apnId == 0) {
                apnId = (long) PaymentUtil.writeApn(this.context, PaymentUtil.setApnProperties(this.context));
            }
            if (apnId > 0) {
                PaymentUtil.setCurrentApn(this.context, apnId);
                this.isChangedApn = true;
            }
        }
        if (this.isUseWIFI) {
            wifiManager.setWifiEnabled(false);
        }
        if (this.isChangedApn || this.isUseWIFI || !this.MobileDataEnabled) {
            synchronized (this) {
                try {
                    wait(10000);
                } catch (InterruptedException e) {
                }
            }
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 19 */
    /* JADX WARN: Type inference failed for: r10v0, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x012a A[SYNTHETIC, Splitter:B:39:0x012a] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x012f A[SYNTHETIC, Splitter:B:42:0x012f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.netqin.antivirus.payment.WapTransform.Response processData(java.lang.String r20, java.lang.String r21, java.lang.String r22) throws java.lang.Exception {
        /*
            r19 = this;
            r9 = 0
            r0 = r19
            android.content.Context r0 = r0.context
            r16 = r0
            java.net.Proxy r14 = com.netqin.antivirus.payment.PaymentUtil.getApnProxy(r16)
            r11 = 0
            java.net.URL r5 = new java.net.URL
            r0 = r5
            r1 = r20
            r0.<init>(r1)
            if (r14 != 0) goto L_0x001c
            java.lang.NullPointerException r16 = new java.lang.NullPointerException
            r16.<init>()
            throw r16
        L_0x001c:
            r6 = 0
            java.net.URLConnection r10 = r5.openConnection(r14)     // Catch:{ all -> 0x0122 }
            r0 = r10
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ all -> 0x0122 }
            r9 = r0
            r16 = 1
            r0 = r9
            r1 = r16
            r0.setDoInput(r1)     // Catch:{ all -> 0x0122 }
            r16 = 0
            r0 = r9
            r1 = r16
            r0.setUseCaches(r1)     // Catch:{ all -> 0x0122 }
            r16 = 0
            r0 = r9
            r1 = r16
            r0.setInstanceFollowRedirects(r1)     // Catch:{ all -> 0x0122 }
            java.lang.String r16 = "Accept"
            java.lang.String r17 = "*/*"
            r0 = r9
            r1 = r16
            r2 = r17
            r0.setRequestProperty(r1, r2)     // Catch:{ all -> 0x0122 }
            java.lang.String r16 = "Connection"
            java.lang.String r17 = "Keep-Alive"
            r0 = r9
            r1 = r16
            r2 = r17
            r0.setRequestProperty(r1, r2)     // Catch:{ all -> 0x0122 }
            java.lang.String r16 = "Server"
            java.lang.String r17 = "NetQin"
            r0 = r9
            r1 = r16
            r2 = r17
            r0.setRequestProperty(r1, r2)     // Catch:{ all -> 0x0122 }
            r16 = 10000(0x2710, float:1.4013E-41)
            r0 = r9
            r1 = r16
            r0.setConnectTimeout(r1)     // Catch:{ all -> 0x0122 }
            r16 = 30000(0x7530, float:4.2039E-41)
            r0 = r9
            r1 = r16
            r0.setReadTimeout(r1)     // Catch:{ all -> 0x0122 }
            java.lang.String r16 = "Content-Type"
            r0 = r19
            java.lang.String r0 = r0.content_type     // Catch:{ all -> 0x0122 }
            r17 = r0
            r0 = r9
            r1 = r16
            r2 = r17
            r0.setRequestProperty(r1, r2)     // Catch:{ all -> 0x0122 }
            java.lang.String r16 = "POST"
            r0 = r16
            r1 = r21
            boolean r16 = r0.equals(r1)     // Catch:{ all -> 0x0122 }
            if (r16 == 0) goto L_0x0119
            r16 = 1
            r0 = r9
            r1 = r16
            r0.setDoOutput(r1)     // Catch:{ all -> 0x0122 }
            java.lang.String r16 = "POST"
            r0 = r9
            r1 = r16
            r0.setRequestMethod(r1)     // Catch:{ all -> 0x0122 }
            java.io.OutputStreamWriter r13 = new java.io.OutputStreamWriter     // Catch:{ all -> 0x0122 }
            java.io.OutputStream r16 = r9.getOutputStream()     // Catch:{ all -> 0x0122 }
            r0 = r13
            r1 = r16
            r0.<init>(r1)     // Catch:{ all -> 0x0122 }
            r0 = r13
            r1 = r22
            r0.write(r1)     // Catch:{ all -> 0x0122 }
            r13.flush()     // Catch:{ all -> 0x0122 }
            r13.close()     // Catch:{ all -> 0x0122 }
        L_0x00b5:
            com.netqin.antivirus.payment.WapTransform$Response r15 = new com.netqin.antivirus.payment.WapTransform$Response     // Catch:{ all -> 0x0122 }
            int r16 = r9.getResponseCode()     // Catch:{ all -> 0x0122 }
            java.lang.String r17 = r9.getResponseMessage()     // Catch:{ all -> 0x0122 }
            r18 = 0
            r0 = r15
            r1 = r19
            r2 = r16
            r3 = r17
            r4 = r18
            r0.<init>(r1, r2, r3, r4)     // Catch:{ all -> 0x0122 }
            java.util.Map r8 = r9.getHeaderFields()     // Catch:{ all -> 0x0122 }
            java.util.Set r16 = r8.keySet()     // Catch:{ all -> 0x0122 }
            java.util.Iterator r16 = r16.iterator()     // Catch:{ all -> 0x0122 }
        L_0x00d9:
            boolean r17 = r16.hasNext()     // Catch:{ all -> 0x0122 }
            if (r17 != 0) goto L_0x0133
            int r16 = r15.rpcode     // Catch:{ all -> 0x0122 }
            r17 = 400(0x190, float:5.6E-43)
            r0 = r16
            r1 = r17
            if (r0 <= r1) goto L_0x0150
            java.io.InputStream r11 = r9.getErrorStream()     // Catch:{ all -> 0x0122 }
        L_0x00ef:
            java.io.ByteArrayOutputStream r7 = new java.io.ByteArrayOutputStream     // Catch:{ all -> 0x0122 }
            r7.<init>()     // Catch:{ all -> 0x0122 }
            int r10 = r11.read()     // Catch:{ all -> 0x0169 }
        L_0x00f8:
            r16 = -1
            r0 = r10
            r1 = r16
            if (r0 != r1) goto L_0x0155
            byte[] r16 = r7.toByteArray()     // Catch:{ all -> 0x0169 }
            r15.setContent(r16)     // Catch:{ all -> 0x0169 }
            r7.close()     // Catch:{ all -> 0x0169 }
            if (r7 == 0) goto L_0x010e
            r7.close()     // Catch:{ Exception -> 0x015d }
        L_0x010e:
            if (r11 == 0) goto L_0x0113
            r11.close()     // Catch:{ Exception -> 0x015f }
        L_0x0113:
            if (r9 == 0) goto L_0x0118
            r9.disconnect()     // Catch:{ Exception -> 0x0161 }
        L_0x0118:
            return r15
        L_0x0119:
            java.lang.String r16 = "GET"
            r0 = r9
            r1 = r16
            r0.setRequestMethod(r1)     // Catch:{ all -> 0x0122 }
            goto L_0x00b5
        L_0x0122:
            r16 = move-exception
        L_0x0123:
            if (r6 == 0) goto L_0x0128
            r6.close()     // Catch:{ Exception -> 0x0163 }
        L_0x0128:
            if (r11 == 0) goto L_0x012d
            r11.close()     // Catch:{ Exception -> 0x0165 }
        L_0x012d:
            if (r9 == 0) goto L_0x0132
            r9.disconnect()     // Catch:{ Exception -> 0x0167 }
        L_0x0132:
            throw r16
        L_0x0133:
            java.lang.Object r12 = r16.next()     // Catch:{ all -> 0x0122 }
            java.lang.String r12 = (java.lang.String) r12     // Catch:{ all -> 0x0122 }
            java.lang.Object r19 = r8.get(r12)     // Catch:{ all -> 0x0122 }
            java.util.List r19 = (java.util.List) r19     // Catch:{ all -> 0x0122 }
            java.util.Iterator r17 = r19.iterator()     // Catch:{ all -> 0x0122 }
        L_0x0143:
            boolean r18 = r17.hasNext()     // Catch:{ all -> 0x0122 }
            if (r18 == 0) goto L_0x00d9
            java.lang.Object r19 = r17.next()     // Catch:{ all -> 0x0122 }
            java.lang.String r19 = (java.lang.String) r19     // Catch:{ all -> 0x0122 }
            goto L_0x0143
        L_0x0150:
            java.io.InputStream r11 = r9.getInputStream()     // Catch:{ all -> 0x0122 }
            goto L_0x00ef
        L_0x0155:
            r7.write(r10)     // Catch:{ all -> 0x0169 }
            int r10 = r11.read()     // Catch:{ all -> 0x0169 }
            goto L_0x00f8
        L_0x015d:
            r16 = move-exception
            goto L_0x010e
        L_0x015f:
            r16 = move-exception
            goto L_0x0113
        L_0x0161:
            r16 = move-exception
            goto L_0x0118
        L_0x0163:
            r17 = move-exception
            goto L_0x0128
        L_0x0165:
            r17 = move-exception
            goto L_0x012d
        L_0x0167:
            r17 = move-exception
            goto L_0x0132
        L_0x0169:
            r16 = move-exception
            r6 = r7
            goto L_0x0123
        */
        throw new UnsupportedOperationException("Method not decompiled: com.netqin.antivirus.payment.WapTransform.processData(java.lang.String, java.lang.String, java.lang.String):com.netqin.antivirus.payment.WapTransform$Response");
    }

    public synchronized void cancel() {
        this.handler.sendEmptyMessage(PaymentHandler.STATUS_CANCALED);
        this.stop = true;
        notify();
    }

    private void recoverApn() {
        if (this.isChangedApn) {
            try {
                PaymentUtil.setCurrentApn(this.context, this.curApnId);
                Thread.sleep(500);
            } catch (Exception e) {
            }
        }
        if (this.isUseWIFI) {
            ((WifiManager) this.context.getSystemService("wifi")).setWifiEnabled(true);
        }
        if (!this.MobileDataEnabled) {
            this.mConnMgr.setMobileDataEnabled(false);
        }
    }

    private static class Href {
        /* access modifiers changed from: private */
        public final String html;
        /* access modifiers changed from: private */
        public final String url;

        private Href(String html2, String url2) {
            this.html = html2;
            this.url = url2;
        }

        /* synthetic */ Href(String str, String str2, Href href) {
            this(str, str2);
        }

        public String toString() {
            return this.html;
        }

        public boolean equals(String s) {
            return this.url.equalsIgnoreCase(s);
        }
    }

    private class Response {
        /* access modifiers changed from: private */
        public byte[] content;
        /* access modifiers changed from: private */
        public String responseMessage;
        /* access modifiers changed from: private */
        public int rpcode;

        /* synthetic */ Response(WapTransform wapTransform, int i, String str, Response response) {
            this(i, str);
        }

        private Response(int rpcode2, String responseMessage2) {
            this.rpcode = rpcode2;
            this.responseMessage = responseMessage2;
        }

        /* access modifiers changed from: private */
        public void setContent(byte[] content2) {
            this.content = content2;
        }
    }
}
