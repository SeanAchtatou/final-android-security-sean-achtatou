package com.netqin.antivirus.payment;

import android.content.Context;
import android.content.Intent;
import java.util.List;

public abstract class PaymentIntent extends Intent {
    protected static final String ACTION = "com.netqin.payment.action";
    protected static final String ACTION_SMSPayment = "com.netqin.payment.SMSPayment";
    protected static final String ACTION_START = "com.netqin.payment.start";
    protected static final String ACTION_STATUS = "com.netqin.payment.status";
    protected static final String ACTION_WapPayment = "com.netqin.payment.WapPayment";
    protected static final String ACTION_WebPayment = "com.netqin.payment.WebPayment";
    protected static final String ACTION_ZongPayment = "com.netqin.payment.ZongPayment";
    protected static final String CANCEL_STATUS = "com.netqin.payment.cancel";
    public static final int PAYMENT_General_Wap_Recharge = 61;
    public static final int PAYMENT_General_Wap_Subscribe = 62;
    public static final int PAYMENT_General_Wap_Unsubscribe = 63;
    protected static final String PAYMENT_KEY = "com.netqin.payment.key";
    public static final int PAYMENT_PaymentCentre_Subcribe = 51;
    public static final int PAYMENT_Sms_Recharge = 21;
    public static final int PAYMENT_Sms_Subscribe = 31;
    public static final int PAYMENT_Sms_Unsubscribe = 41;
    public static final int PAYMENT_Wap_Recharge = 22;
    public static final int PAYMENT_Wap_Subscribe = 32;
    public static final int PAYMENT_Wap_Unsubscribe = 42;
    public static final int PAYMENT_ZongSDK_Subscribe = 81;
    public static final int PAYMENT_ZongWap_Subscribe = 82;
    protected static final String PaymentCancelBtn = "PaymentCancelBtn";
    protected static final String PaymentCentreUrl = "PaymentCentreUrl";
    protected static final String PaymentConfirmBtn = "PaymentConfirmBtn";
    protected static final String PaymentPrompt = "PaymentPrompt";
    protected static final String PaymentReConfirmPrompt = "PaymentReConfirmPrompt";
    protected static final String PaymentType = "PaymentType";
    protected static final String REQUESTMETHOD = "RequestMethod";
    protected static final String SMSNeedReConfirm = "SMSNeedReConfirm";
    protected static final String SMSNumber = "SMSNumber";
    protected static final String SMSReceiveReconfirmVisible = "SMSReceiveReconfirmVisible";
    protected static final String SMSReconfirmContent = "SMSReconfirmContent";
    protected static final String SMSReconfirmMatch = "SMSReconfirmMatch";
    protected static final String SMSReconfirmNumber = "SMSReconfirmNumber";
    protected static final String SMSReconfirmWaitTime = "SMSReconfirmWaitTime";
    protected static final String SMSSendContent = "SMSSendContent";
    protected static final String SMSSendCount = "SMSSendCount";
    protected static final String SMSSendReconfirmVisible = "SMSSendReconfirmVisible";
    protected static final String SMSSendVisible = "SMSSendVisible";
    public static final int SMS_Register = 11;
    public static final int Unsupported = -404;
    protected static final String WAPClickVisible = "WAPClickVisible";
    protected static final String WAPConfirmMatch = "WAPConfirmMatch";
    protected static final String WAPCount = "WAPCount";
    protected static final String WAPInternal = "WAPInternal";
    protected static final String WAPMsgVisible = "WAPMsgVisible";
    protected static final String WAPPageIndex = "WAPPageIndex";
    protected static final String WAPPostData = "WAPPostData";
    protected static final String WAPRule = "WAPRule";
    protected static final String WAPSuccessSign = "WAPSuccessSign";
    protected static final String WAPURL = "WAPURL";
    public static final int WAP_Register = 12;
    protected static final String ZongAppNanme = "ZongAppNanme";
    protected static final String ZongCountry = "ZongCountry";
    protected static final String ZongCurrency = "ZongCurrency";
    protected static final String ZongCustomerKey = "ZongCustomerKey";
    protected static final String ZongExactPrice = "ZongExactPrice";
    protected static final String ZongItemDesc = "ZongItemDesc";
    protected static final String ZongLabel = "ZongLabel";
    protected static final String ZongPurchaseKey = "ZongPurchaseKey";
    protected static final String ZongQuantity = "ZongQuantity";
    protected static final String ZongTransactionRef = "ZongTransactionRef";
    protected static final String ZongUrl = "ZongUrl";

    /* access modifiers changed from: protected */
    public abstract boolean checkAttributes() throws NoSuchParameterException;

    protected PaymentIntent(int type, Context context) {
        super(ACTION_START);
        putExtra(PaymentType, type);
    }

    public void setPaymentPrompt(String prompt) {
        putExtra(PaymentPrompt, prompt);
    }

    public void setPaymentReConfirmPrompt(String prompt) {
        putExtra(PaymentReConfirmPrompt, prompt);
    }

    public void setSMSSendVisible(boolean visible) {
        putExtra(SMSSendVisible, visible);
    }

    public void setSMSNumber(String number) {
        putExtra(SMSNumber, number);
    }

    public void setSMSSendContent(String content) {
        putExtra(SMSSendContent, content);
    }

    public void setSMSSendCount(int count) {
        putExtra(SMSSendCount, count);
    }

    public void setSMSNeedReConfirm(boolean needReConfirm) {
        putExtra(SMSNeedReConfirm, needReConfirm);
    }

    public void setSMSReceiveReconfirmVisible(boolean visible) {
        putExtra(SMSReceiveReconfirmVisible, visible);
    }

    public void setSMSSendReconfirmVisible(boolean visible) {
        putExtra(SMSSendReconfirmVisible, visible);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent} */
    public void setSMSReconfirmWaitTime(int seconds) {
        if (seconds > 0) {
            putExtra(SMSReconfirmWaitTime, ((long) seconds) * 1000);
        } else {
            putExtra(SMSReconfirmWaitTime, 90000L);
        }
    }

    public void setSMSReconfirmNumber(String number) {
        putExtra(SMSReconfirmNumber, number);
    }

    public void setSMSReconfirmContent(String content) {
        putExtra(SMSReconfirmContent, content);
    }

    public void setSMSReconfirmMatch(String match) {
        putExtra(SMSReconfirmMatch, match);
    }

    public void setWAPClickVisible(boolean visible) {
        putExtra(WAPClickVisible, visible);
    }

    public void setWAPURL(String url) {
        putExtra(WAPURL, url);
    }

    public void setWAPInternal(int seconds) {
        putExtra(WAPInternal, ((long) seconds) * 1000);
    }

    public void setWAPCount(int count) {
        putExtra(WAPCount, count);
    }

    public void setWAPRule(String Rule) {
        putExtra(WAPRule, Rule);
    }

    public void setWAPRequestMethod(String RequestMethod) {
        putExtra(REQUESTMETHOD, RequestMethod);
    }

    public void setWAPPostData(String PostData) {
        putExtra(WAPPostData, PostData);
    }

    public void setWAPMsgVisible(boolean visible) {
        putExtra(WAPMsgVisible, visible);
    }

    public void setWAPPageIndex(int index) {
        putExtra(WAPPageIndex, index);
    }

    public void setWAPConfirmMatch(String match) {
        putExtra(WAPConfirmMatch, match);
    }

    public void setWAPSuccessSign(String sign) {
        putExtra(WAPSuccessSign, sign);
    }

    public void setZongUrl(String url) {
        putExtra(ZongUrl, url);
    }

    public void setZongAppName(String name) {
        putExtra(ZongAppNanme, name);
    }

    public void setZongCustomerKey(String key) {
        putExtra(ZongCustomerKey, key);
    }

    public void setZongCountry(String country) {
        putExtra(ZongCountry, country);
    }

    public void setZongCurrency(String currency) {
        putExtra(ZongCurrency, currency);
    }

    public void setZongTransactionRef(String ref) {
        putExtra(ZongTransactionRef, ref);
    }

    public void setZongPricePoint(List<ZongPricePoint> points) throws NullPointerException {
        if (points.isEmpty()) {
            throw new NullPointerException();
        }
        int size = points.size();
        String[] keys = new String[size];
        float[] amounts = new float[size];
        int[] quantities = new int[size];
        String[] descriptions = new String[size];
        String[] labels = new String[size];
        for (int i = 0; i < size; i++) {
            ZongPricePoint point = points.get(i);
            keys[i] = point.getKey();
            amounts[i] = point.getAmount();
            quantities[i] = point.getQuantity();
            descriptions[i] = point.getDescription();
            labels[i] = point.getLabel();
        }
        putExtra(ZongPurchaseKey, keys);
        putExtra(ZongQuantity, quantities);
        putExtra(ZongExactPrice, amounts);
        putExtra(ZongItemDesc, descriptions);
        putExtra(ZongLabel, labels);
    }

    public void setPaymentCentreUrl(String url) {
        putExtra("PaymentCentreUrl", url);
    }

    public static PaymentIntent getInstance(int type, Context context) {
        switch (type) {
            case 11:
            case 21:
            case 31:
            case 41:
                return new SMSIntent(type, context);
            case 12:
            case 22:
            case 32:
            case 42:
            case 61:
            case 62:
            case 63:
                return new WapIntent(type, context);
            case 51:
            case 82:
                return new WebIntent(type, context);
            case 81:
                return new ZongIntent(type, context);
            default:
                return null;
        }
    }

    protected static int getInt(Intent intent, String name) throws NoSuchParameterException {
        int i = intent.getIntExtra(name, 286331153);
        if (i != 286331153) {
            return i;
        }
        throw new NoSuchParameterException(name);
    }

    protected static String getString(Intent intent, String name) throws NoSuchParameterException {
        String s = intent.getStringExtra(name);
        if (s != null && s.length() != 0) {
            return s;
        }
        throw new NoSuchParameterException(name);
    }

    protected static long getLong(Intent intent, String name) throws NoSuchParameterException {
        long i = intent.getLongExtra(name, 286331153);
        if (i != 286331153) {
            return i;
        }
        throw new NoSuchParameterException(name);
    }

    protected static float getFloat(Intent intent, String name) throws NoSuchParameterException {
        float i = intent.getFloatExtra(name, 2.86331168E8f);
        if (i != 2.86331168E8f) {
            return i;
        }
        throw new NoSuchParameterException(name);
    }
}
