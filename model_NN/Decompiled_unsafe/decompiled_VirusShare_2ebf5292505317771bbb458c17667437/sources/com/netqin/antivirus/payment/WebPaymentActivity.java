package com.netqin.antivirus.payment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.IBinder;
import android.os.Message;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.netqin.antivirus.appprotocol.XmlTagValue;
import com.nqmobile.antivirus_ampro20.R;

public class WebPaymentActivity extends PaymentActivity {
    private static final int MenuItem_Back = 2;
    private static final int MenuItem_Concel = 1;
    /* access modifiers changed from: private */
    public ProgressDialog pDialog;
    /* access modifiers changed from: private */
    public String url;
    private String[] urls;
    private WebView wv;

    /* access modifiers changed from: protected */
    public void doCustomCreate(Intent intent) {
        requestWindowFeature(1);
        this.url = intent.getStringExtra(XmlTagValue.paymentCentreUrl);
        this.wv = new WebView(this);
        this.wv.setClickable(true);
        this.wv.getSettings().setDefaultTextEncodingName("utf-8");
        this.wv.loadUrl(this.url);
        this.wv.setWebViewClient(new Client(this, null));
        setContentView(this.wv);
        this.pDialog = new ProgressDialog(this);
        this.pDialog.setCancelable(false);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            if (this.wv.canGoBack()) {
                this.wv.goBack();
                return true;
            }
            stop(PaymentHandler.STATUS_CANCALED);
        }
        return super.onKeyDown(keyCode, event);
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        if (this.wv.canGoBack()) {
            menu.add(0, 2, 0, (int) R.string.label_back);
        }
        menu.add(0, 1, 0, (int) R.string.label_close);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case 1:
                stop(getStatus());
                return true;
            case 2:
                if (this.wv.canGoBack()) {
                    this.wv.goBack();
                    return true;
                }
                onClickCancel();
                return true;
            default:
                return true;
        }
    }

    private class Client extends WebViewClient {
        private Client() {
        }

        /* synthetic */ Client(WebPaymentActivity webPaymentActivity, Client client) {
            this();
        }

        public void onFormResubmission(WebView view, Message dontResend, Message resend) {
        }

        public void onLoadResource(WebView view, String url) {
        }

        public void onPageFinished(WebView view, String _url) {
            if (WebPaymentActivity.this.pDialog.isShowing()) {
                WebPaymentActivity.this.pDialog.dismiss();
            }
            if (WebPaymentActivity.this.url.equals(_url)) {
                WebPaymentActivity.this.setStatus(100);
            }
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            if (!WebPaymentActivity.this.pDialog.isShowing()) {
                WebPaymentActivity.this.pDialog.show();
            }
        }

        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            if (WebPaymentActivity.this.pDialog.isShowing()) {
                WebPaymentActivity.this.pDialog.dismiss();
            }
            if (WebPaymentActivity.this.url.equals(failingUrl)) {
                WebPaymentActivity.this.setStatus(PaymentHandler.STATUS_FAILED);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onConnected(IBinder binder) {
    }
}
