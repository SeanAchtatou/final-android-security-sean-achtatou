package com.netqin.antivirus.payment;

class NoSuchParameterException extends Exception {
    private static final long serialVersionUID = 1289379132229976259L;

    protected NoSuchParameterException(String name) {
        super(name);
    }
}
