package com.netqin.antivirus.payment;

import java.net.URLDecoder;

public class ZongPricePoint {
    private float amount;
    private String description;
    private String key;
    private String label;
    private int quantity;

    public ZongPricePoint(String key2, float amount2, int quantity2, String description2, String label2) {
        this.key = URLDecoder.decode(key2);
        this.amount = amount2;
        this.quantity = quantity2;
        this.description = description2;
        this.label = label2;
    }

    /* access modifiers changed from: protected */
    public String getKey() {
        return this.key;
    }

    /* access modifiers changed from: protected */
    public float getAmount() {
        return this.amount;
    }

    /* access modifiers changed from: protected */
    public int getQuantity() {
        return this.quantity;
    }

    /* access modifiers changed from: protected */
    public String getDescription() {
        return this.description;
    }

    /* access modifiers changed from: protected */
    public String getLabel() {
        return this.label;
    }
}
