package com.netqin.antivirus.payment;

import android.content.Intent;
import com.netqin.antivirus.contact.vcard.VCardConfig;

public class WebPayment extends PaymentService {
    /* access modifiers changed from: protected */
    public void startPayment(Intent intent) {
        intent.setClass(this, WebPaymentActivity.class);
        intent.addFlags(VCardConfig.FLAG_USE_QP_TO_PRIMARY_PROPERTIES);
        startActivity(intent);
    }
}
