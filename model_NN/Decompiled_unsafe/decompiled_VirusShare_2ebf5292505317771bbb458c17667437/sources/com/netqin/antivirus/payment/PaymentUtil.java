package com.netqin.antivirus.payment;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.netqin.antivirus.antilost.SmsHandler;
import com.netqin.antivirus.cloud.model.xml.XmlUtils;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.HashMap;

public class PaymentUtil {
    public static final int OPE_FOREIGN = 5;
    public static final int OPE_MOBILE = 1;
    public static final int OPE_TELECOM = 3;
    public static final int OPE_UNICOM = 2;
    public static final int OPE_UNKNOWN = 4;

    protected static Cursor getCurrentApn(Context context) {
        return context.getContentResolver().query(Uri.parse("content://telephony/carriers/preferapn"), null, null, null, null);
    }

    public static long getCurrentApnId(Context context) {
        Cursor c = context.getContentResolver().query(Uri.parse("content://telephony/carriers/preferapn"), null, null, null, null);
        if (c == null || c.getCount() <= 0) {
            return -1;
        }
        c.moveToFirst();
        long _id = c.getLong(c.getColumnIndex(SmsHandler.ROWID));
        c.close();
        return _id;
    }

    public static void setCurrentApn(Context context, long apnId) {
        try {
            ContentValues values = new ContentValues();
            values.put("apn_id", Long.valueOf(apnId));
            context.getContentResolver().update(Uri.parse("content://telephony/carriers/preferapn"), values, null, null);
        } catch (Exception e) {
        }
    }

    protected static Proxy getApnProxy(Context context) {
        Cursor c = getCurrentApn(context);
        if (c != null && c.getCount() > 0) {
            c.moveToFirst();
            String proxy = c.getString(c.getColumnIndex("proxy"));
            String port = c.getString(c.getColumnIndex("port"));
            if (proxy != null && !proxy.equals("") && port != null && !port.equals("")) {
                return new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxy, Integer.valueOf(port).intValue()));
            }
        }
        c.close();
        return null;
    }

    protected static long getProxyId(Context context) {
        long _id = 0;
        Uri uri = Uri.parse("content://telephony/carriers");
        HashMap<String, String> map = getPhoneOperator(context);
        StringBuffer sb = new StringBuffer("current=1 and numeric='").append(map.get("numeric")).append("' and proxy='");
        switch (Integer.parseInt(map.get("mnc"))) {
            case 0:
            case 2:
            case 7:
                sb.append("10.0.0.172' and (port='80' or port='9201')  and (type is null or type = 'default')");
                break;
            case 1:
                sb.append("10.0.0.172'  and (port='80' or port='9201') and (type is null or type = 'default')");
                break;
            case 3:
                sb.append("10.0.0.200'  and port='80'");
                break;
            case 4:
                return 0;
            case 5:
                return 0;
        }
        Cursor c = context.getContentResolver().query(uri, null, sb.toString(), null, null);
        if (c != null && c.getCount() > 0) {
            c.moveToFirst();
            _id = c.getLong(c.getColumnIndex(SmsHandler.ROWID));
            c.close();
        }
        return _id;
    }

    protected static HashMap<String, String> getPhoneOperator(Context context) {
        String imsiCode = getIMSI(context);
        if ("".equals(imsiCode.trim())) {
            return null;
        }
        HashMap<String, String> map = new HashMap<>();
        String numeric = imsiCode.substring(0, 5);
        map.put("numeric", numeric);
        map.put("mcc", numeric.substring(0, 3));
        map.put("mnc", numeric.substring(3));
        return map;
    }

    protected static int writeApn(Context context, ContentValues values) {
        if (values == null) {
            return -1;
        }
        int id = -1;
        ContentResolver resolver = context.getContentResolver();
        Uri newRow = resolver.insert(Uri.parse("content://telephony/carriers"), values);
        if (newRow != null) {
            Cursor c = resolver.query(newRow, null, null, null, null);
            int idIndex = c.getColumnIndex(SmsHandler.ROWID);
            c.moveToFirst();
            id = c.getShort(idIndex);
            c.close();
        }
        values.put("apn_id", Integer.valueOf(id));
        resolver.update(Uri.parse("content://telephony/carriers/preferapn"), values, null, null);
        return id;
    }

    protected static String getIMSI(Context context) {
        String imsi = ((TelephonyManager) context.getSystemService("phone")).getSubscriberId();
        if (TextUtils.isEmpty(imsi)) {
            return "000000000000000";
        }
        return imsi;
    }

    public static ContentValues setApnProperties(Context context) {
        ContentValues values = new ContentValues();
        values.put("proxy", "10.0.0.172");
        values.put("port", "80");
        values.put("mcc", "460");
        String imsiCode = getIMSI(context);
        if (!"".equals(imsiCode.trim())) {
            String numeric = imsiCode.substring(0, 5);
            if (numeric.startsWith("460")) {
                String mnc = numeric.substring(3);
                values.put("numeric", numeric);
                values.put("mnc", mnc);
                switch (Integer.parseInt(mnc)) {
                    case 0:
                    case 2:
                    case 7:
                        values.put("name", "CMWAP");
                        values.put(XmlUtils.LABEL_MOBILEINFO_APN, "cmwap");
                        break;
                    case 1:
                        values.put("name", "UNIWAP");
                        values.put(XmlUtils.LABEL_MOBILEINFO_APN, "uniwap");
                        break;
                    case 3:
                        values.put("name", "WAP");
                        values.put(XmlUtils.LABEL_MOBILEINFO_APN, "ctwap");
                        values.put("user", "ctwap@mycdma.cn");
                        values.put("password", "vnet.mobi");
                        values.put("proxy", "10.0.0.200");
                        break;
                }
            }
        }
        return values;
    }

    public static HashMap<String, String> getCurrentApnIdAndProxy(Context context) {
        HashMap<String, String> map = new HashMap<>();
        Cursor c = context.getContentResolver().query(Uri.parse("content://telephony/carriers/preferapn"), null, null, null, null);
        if (c != null && c.getCount() > 0) {
            c.moveToFirst();
            long _id = c.getLong(c.getColumnIndex(SmsHandler.ROWID));
            String proxy = c.getString(c.getColumnIndex("proxy"));
            int current = c.getInt(c.getColumnIndex("current"));
            map.put("id", String.valueOf(_id));
            if (current == 1) {
                map.put("proxy", proxy);
            } else {
                map.put("proxy", "");
            }
            c.close();
        }
        return map;
    }

    protected static boolean matches(String match, String regex) {
        if (regex == null) {
            return false;
        }
        if (match == null) {
            return false;
        }
        if (match.length() == 0 || regex.length() == 0 || match.length() < regex.length()) {
            return false;
        }
        return match.matches(regex.replaceAll("\\*", ".*").replaceAll("\\?", "."));
    }
}
