package com.netqin.antivirus.payment;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import com.netqin.antivirus.contact.vcard.VCardConfig;
import com.netqin.antivirus.net.NetReturnValue;
import com.nqmobile.antivirus_ampro20.R;

public class WapPayment extends PaymentService {
    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            WapPayment.this.onMessage(msg);
        }
    };
    private boolean prograssVisible;
    private WapTransform transform;
    protected Handler uhandler;

    /* access modifiers changed from: protected */
    public void startPayment(Intent intent) {
        boolean z;
        this.prograssVisible = intent.getBooleanExtra("WAPMsgVisible", false);
        try {
            this.prograssVisible |= PaymentIntent.getString(intent, "PaymentPrompt").length() > 0;
        } catch (Exception e) {
        }
        try {
            boolean z2 = this.prograssVisible;
            if (PaymentIntent.getString(intent, "PaymentReConfirmPrompt").length() > 0) {
                z = true;
            } else {
                z = false;
            }
            this.prograssVisible = z2 | z;
        } catch (Exception e2) {
        }
        if (this.prograssVisible) {
            Intent i = createIntent();
            i.setClass(this, ProgressActivity.class);
            startActivity(i);
            return;
        }
        this.transform = new WapTransform(this, intent, this.handler);
        this.transform.start();
    }

    /* access modifiers changed from: protected */
    public boolean doNext(Message msg) {
        switch (msg.what) {
            case 1001:
                String prompt = this.intent.getStringExtra("PaymentReConfirmPrompt");
                if (prompt != null && prompt.length() > 0) {
                    showPrompt(prompt, NetReturnValue.Username_Available);
                    break;
                } else {
                    startPayment(createIntent());
                    break;
                }
            case NetReturnValue.Username_Available:
                startPayment(createIntent());
                break;
            case NetReturnValue.UserAccountCreateSuccess:
            default:
                if (this.uhandler != null) {
                    try {
                        this.uhandler.sendEmptyMessage(-100);
                    } catch (Exception e) {
                    }
                    this.uhandler = null;
                }
                if (this.transform == null) {
                    if (this.intent.getBooleanExtra("WAPMsgVisible", false) && msg.what > 200 && msg.what < 300) {
                        sendResult(msg.what + 100);
                        break;
                    } else {
                        sendResult(msg.what);
                        break;
                    }
                } else {
                    this.transform.cancel();
                    break;
                }
                break;
            case 1004:
                this.uhandler = new Handler((Handler.Callback) msg.obj);
                this.transform = new WapTransform(this, this.intent, this.handler);
                this.transform.start();
                break;
            case 1005:
                this.transform.doNext();
                break;
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void onMessage(Message msg) {
        switch (msg.what) {
            case NetReturnValue.UserAccountCreateSuccess:
                if (this.intent.getBooleanExtra("WAPMsgVisible", false)) {
                    Intent intent = new Intent(this, PromptActivity.class);
                    intent.addFlags(VCardConfig.FLAG_USE_QP_TO_PRIMARY_PROPERTIES);
                    intent.setAction(this.intent.getStringExtra("com.netqin.payment.action"));
                    intent.putExtra("PaymentPrompt", (String) msg.obj);
                    intent.putExtra("com.netqin.payment.status", 1005);
                    intent.putExtra("com.netqin.payment.cancel", (int) PaymentHandler.STATUS_CANCALED);
                    intent.putExtra("PaymentType", this.intent.getIntExtra("PaymentType", -404));
                    intent.putExtra("PaymentConfirmBtn", getString(R.string.payment_wap_confirm));
                    intent.putExtra("PaymentCancelBtn", getString(R.string.label_cancel));
                    startActivity(intent);
                    return;
                }
                this.transform.doNext();
                return;
            default:
                if (this.uhandler != null) {
                    try {
                        this.uhandler.sendEmptyMessage(-100);
                    } catch (Exception e) {
                    }
                    this.uhandler = null;
                }
                if (this.intent.getBooleanExtra("WAPMsgVisible", false)) {
                    sendResult(msg.what + 100);
                    return;
                } else {
                    sendResult(msg.what);
                    return;
                }
        }
    }
}
