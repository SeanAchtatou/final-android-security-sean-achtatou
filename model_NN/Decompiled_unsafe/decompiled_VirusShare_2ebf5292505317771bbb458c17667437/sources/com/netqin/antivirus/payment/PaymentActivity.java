package com.netqin.antivirus.payment;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.view.KeyEvent;

public abstract class PaymentActivity extends Activity {
    private int cancelStatus;
    protected Handler handler;
    private int nextStatus;
    private ServiceConnection serviceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName name, IBinder binder) {
            PaymentActivity.this.handler = new Handler((Handler.Callback) binder);
            PaymentActivity.this.onConnected(binder);
        }

        public void onServiceDisconnected(ComponentName name) {
            PaymentActivity.this.handler = null;
        }
    };
    private int status;

    /* access modifiers changed from: protected */
    public abstract void doCustomCreate(Intent intent);

    /* access modifiers changed from: protected */
    public abstract void onConnected(IBinder iBinder);

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        Intent i = getIntent();
        if (!bindService(new Intent(i.getAction()), this.serviceConnection, 1)) {
            finish();
            return;
        }
        this.nextStatus = i.getIntExtra("com.netqin.payment.status", -1);
        this.cancelStatus = i.getIntExtra("com.netqin.payment.cancel", -1);
        this.status = this.cancelStatus;
        doCustomCreate(i);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        stop(this.cancelStatus);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onClickNext() {
        stop(this.nextStatus);
        finish();
    }

    /* access modifiers changed from: protected */
    public void onClickCancel() {
        stop(this.cancelStatus);
    }

    /* access modifiers changed from: protected */
    public int getStatus() {
        return this.status;
    }

    /* access modifiers changed from: protected */
    public void sendStatus(int status2) {
        if (this.handler != null) {
            this.handler.sendEmptyMessage(status2);
            return;
        }
        Intent intent = new Intent("com.netqin.payment.status");
        intent.putExtra("com.netqin.payment.status", -404);
        sendBroadcast(intent);
        finish();
    }

    /* access modifiers changed from: protected */
    public void sendMessage(Message msg) {
        this.handler.sendMessage(msg);
    }

    /* access modifiers changed from: protected */
    public void setStatus(int status2) {
        this.status = status2;
    }

    /* access modifiers changed from: protected */
    public void stop(int status2) {
        this.status = status2;
        sendStatus(status2);
        finish();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        unbindService(this.serviceConnection);
        super.onDestroy();
    }
}
