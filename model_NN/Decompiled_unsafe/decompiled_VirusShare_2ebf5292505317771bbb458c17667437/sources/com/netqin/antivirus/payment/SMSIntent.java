package com.netqin.antivirus.payment;

import android.content.Context;

final class SMSIntent extends PaymentIntent {
    protected SMSIntent(int type, Context context) {
        super(type, context);
        setClass(context, SMSPayment.class);
        putExtra("com.netqin.payment.action", "com.netqin.payment.SMSPayment");
    }

    /* access modifiers changed from: protected */
    public boolean checkAttributes() throws NoSuchParameterException {
        getString(this, "SMSNumber");
        getString(this, "SMSSendContent");
        if (!getBooleanExtra("SMSNeedReConfirm", false)) {
            return true;
        }
        getString(this, "SMSReconfirmMatch");
        getString(this, "SMSReconfirmMatch");
        return true;
    }
}
