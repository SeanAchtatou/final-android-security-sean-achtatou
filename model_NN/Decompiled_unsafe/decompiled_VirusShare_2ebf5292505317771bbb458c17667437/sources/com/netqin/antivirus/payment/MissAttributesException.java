package com.netqin.antivirus.payment;

public class MissAttributesException extends Exception {
    private static final long serialVersionUID = -6586001455977589026L;

    protected MissAttributesException(String attributes) {
        super(attributes);
    }
}
