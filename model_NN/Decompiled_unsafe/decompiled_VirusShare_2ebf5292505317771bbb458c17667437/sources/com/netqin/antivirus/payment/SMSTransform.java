package com.netqin.antivirus.payment;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import com.netqin.antivirus.net.NetReturnValue;
import java.util.ArrayList;
import java.util.HashMap;

public class SMSTransform extends Thread {
    private String ReconfirmContent;
    private String ReconfirmNumber;
    private Context context;
    private Handler handler;
    private Intent intent;
    private boolean stop;
    private boolean wait;

    public SMSTransform(Context context2, Intent intent2, Handler handler2) {
        this.context = context2;
        this.intent = intent2;
        this.handler = handler2;
        this.ReconfirmNumber = intent2.getStringExtra("SMSReconfirmNumber");
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 199 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void run() {
        /*
            r21 = this;
            monitor-enter(r21)
            r10 = 0
            r0 = r21
            android.content.Intent r0 = r0.intent     // Catch:{ all -> 0x00be }
            r17 = r0
            java.lang.String r18 = "SMSNeedReConfirm"
            r19 = 0
            boolean r9 = r17.getBooleanExtra(r18, r19)     // Catch:{ all -> 0x00be }
            if (r9 == 0) goto L_0x0039
            com.netqin.antivirus.payment.SMSTransform$SMSReceiver r11 = new com.netqin.antivirus.payment.SMSTransform$SMSReceiver     // Catch:{ all -> 0x00be }
            r17 = 0
            r0 = r11
            r1 = r21
            r2 = r17
            r0.<init>(r1, r2)     // Catch:{ all -> 0x00be }
            android.content.IntentFilter r5 = new android.content.IntentFilter     // Catch:{ all -> 0x01f8 }
            r5.<init>()     // Catch:{ all -> 0x01f8 }
            java.lang.String r17 = "android.provider.Telephony.SMS_RECEIVED"
            r0 = r5
            r1 = r17
            r0.addAction(r1)     // Catch:{ all -> 0x01f8 }
            r0 = r21
            android.content.Context r0 = r0.context     // Catch:{ all -> 0x01f8 }
            r17 = r0
            r0 = r17
            r1 = r11
            r2 = r5
            r0.registerReceiver(r1, r2)     // Catch:{ all -> 0x01f8 }
            r10 = r11
        L_0x0039:
            r0 = r21
            android.content.Intent r0 = r0.intent     // Catch:{ Exception -> 0x00a9 }
            r17 = r0
            java.lang.String r18 = "SMSSendCount"
            r19 = 1
            int r8 = r17.getIntExtra(r18, r19)     // Catch:{ Exception -> 0x00a9 }
        L_0x0047:
            if (r8 <= 0) goto L_0x0051
            r0 = r21
            boolean r0 = r0.stop     // Catch:{ Exception -> 0x00a9 }
            r17 = r0
            if (r17 == 0) goto L_0x006e
        L_0x0051:
            if (r9 == 0) goto L_0x01e5
            r0 = r21
            android.os.Handler r0 = r0.handler     // Catch:{ Exception -> 0x00a9 }
            r17 = r0
            r18 = 3
            r17.sendEmptyMessage(r18)     // Catch:{ Exception -> 0x00a9 }
        L_0x005e:
            if (r10 == 0) goto L_0x006c
            r0 = r21
            android.content.Context r0 = r0.context     // Catch:{ all -> 0x00be }
            r17 = r0
            r0 = r17
            r1 = r10
            r0.unregisterReceiver(r1)     // Catch:{ all -> 0x00be }
        L_0x006c:
            monitor-exit(r21)
            return
        L_0x006e:
            r0 = r21
            android.content.Intent r0 = r0.intent     // Catch:{ Exception -> 0x00a9 }
            r17 = r0
            java.lang.String r18 = "SMSNumber"
            java.lang.String r17 = r17.getStringExtra(r18)     // Catch:{ Exception -> 0x00a9 }
            r0 = r21
            android.content.Intent r0 = r0.intent     // Catch:{ Exception -> 0x00a9 }
            r18 = r0
            java.lang.String r19 = "SMSSendContent"
            java.lang.String r18 = r18.getStringExtra(r19)     // Catch:{ Exception -> 0x00a9 }
            r19 = 0
            r0 = r21
            r1 = r17
            r2 = r18
            r3 = r19
            boolean r17 = r0.sendSms(r1, r2, r3)     // Catch:{ Exception -> 0x00a9 }
            if (r17 != 0) goto L_0x00c1
            r0 = r21
            android.os.Handler r0 = r0.handler     // Catch:{ Exception -> 0x00a9 }
            r17 = r0
            r18 = 1
            r17.sendEmptyMessage(r18)     // Catch:{ Exception -> 0x00a9 }
            java.io.IOException r17 = new java.io.IOException     // Catch:{ Exception -> 0x00a9 }
            java.lang.String r18 = "send sms failed!"
            r17.<init>(r18)     // Catch:{ Exception -> 0x00a9 }
            throw r17     // Catch:{ Exception -> 0x00a9 }
        L_0x00a9:
            r17 = move-exception
            r4 = r17
            r4.printStackTrace()     // Catch:{ all -> 0x00fc }
            if (r10 == 0) goto L_0x006c
            r0 = r21
            android.content.Context r0 = r0.context     // Catch:{ all -> 0x00be }
            r17 = r0
            r0 = r17
            r1 = r10
            r0.unregisterReceiver(r1)     // Catch:{ all -> 0x00be }
            goto L_0x006c
        L_0x00be:
            r17 = move-exception
        L_0x00bf:
            monitor-exit(r21)
            throw r17
        L_0x00c1:
            if (r9 == 0) goto L_0x01e1
            r17 = 1
            r0 = r17
            r1 = r21
            r1.stop = r0     // Catch:{ Exception -> 0x00a9 }
            r0 = r21
            android.content.Intent r0 = r0.intent     // Catch:{ Exception -> 0x01f5 }
            r17 = r0
            java.lang.String r18 = "SMSReconfirmWaitTime"
            r19 = 90000(0x15f90, double:4.4466E-319)
            long r17 = r17.getLongExtra(r18, r19)     // Catch:{ Exception -> 0x01f5 }
            r0 = r21
            r1 = r17
            r0.wait(r1)     // Catch:{ Exception -> 0x01f5 }
        L_0x00e1:
            r0 = r21
            boolean r0 = r0.stop     // Catch:{ Exception -> 0x00a9 }
            r17 = r0
            if (r17 == 0) goto L_0x010f
            r0 = r21
            android.os.Handler r0 = r0.handler     // Catch:{ Exception -> 0x00a9 }
            r17 = r0
            r18 = 5
            r17.sendEmptyMessage(r18)     // Catch:{ Exception -> 0x00a9 }
            java.lang.InterruptedException r17 = new java.lang.InterruptedException     // Catch:{ Exception -> 0x00a9 }
            java.lang.String r18 = "can't receive confirm"
            r17.<init>(r18)     // Catch:{ Exception -> 0x00a9 }
            throw r17     // Catch:{ Exception -> 0x00a9 }
        L_0x00fc:
            r17 = move-exception
            if (r10 == 0) goto L_0x010b
            r0 = r21
            android.content.Context r0 = r0.context     // Catch:{ all -> 0x00be }
            r18 = r0
            r0 = r18
            r1 = r10
            r0.unregisterReceiver(r1)     // Catch:{ all -> 0x00be }
        L_0x010b:
            throw r17     // Catch:{ all -> 0x00be }
        L_0x010c:
            r21.wait()     // Catch:{ Exception -> 0x01f2 }
        L_0x010f:
            r0 = r21
            boolean r0 = r0.wait     // Catch:{ Exception -> 0x00a9 }
            r17 = r0
            if (r17 == 0) goto L_0x011f
            r0 = r21
            boolean r0 = r0.stop     // Catch:{ Exception -> 0x00a9 }
            r17 = r0
            if (r17 == 0) goto L_0x010c
        L_0x011f:
            r0 = r21
            boolean r0 = r0.stop     // Catch:{ Exception -> 0x00a9 }
            r17 = r0
            if (r17 == 0) goto L_0x012d
            java.io.EOFException r17 = new java.io.EOFException     // Catch:{ Exception -> 0x00a9 }
            r17.<init>()     // Catch:{ Exception -> 0x00a9 }
            throw r17     // Catch:{ Exception -> 0x00a9 }
        L_0x012d:
            r0 = r21
            android.content.Intent r0 = r0.intent     // Catch:{ Exception -> 0x00a9 }
            r17 = r0
            java.lang.String r18 = "SMSReconfirmContent"
            java.lang.String r12 = r17.getStringExtra(r18)     // Catch:{ Exception -> 0x00a9 }
            r16 = r12
            java.lang.String r17 = "*"
            r0 = r12
            r1 = r17
            boolean r17 = r0.startsWith(r1)     // Catch:{ Exception -> 0x00a9 }
            if (r17 == 0) goto L_0x01b8
            java.lang.String r17 = "*"
            r0 = r12
            r1 = r17
            boolean r17 = r0.endsWith(r1)     // Catch:{ Exception -> 0x00a9 }
            if (r17 == 0) goto L_0x01b8
            int r17 = r12.length()     // Catch:{ Exception -> 0x00a9 }
            r18 = 1
            r0 = r17
            r1 = r18
            if (r0 <= r1) goto L_0x01b8
            java.lang.String r17 = "*"
            r18 = 1
            r0 = r12
            r1 = r17
            r2 = r18
            int r15 = r0.indexOf(r1, r2)     // Catch:{ Exception -> 0x00a9 }
            r17 = 1
            r0 = r12
            r1 = r17
            r2 = r15
            java.lang.String r7 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x00a9 }
            int r17 = r15 + 1
            int r18 = r12.length()     // Catch:{ Exception -> 0x00a9 }
            r19 = 1
            int r18 = r18 - r19
            r0 = r12
            r1 = r17
            r2 = r18
            java.lang.String r14 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x00a9 }
            r0 = r21
            java.lang.String r0 = r0.ReconfirmContent     // Catch:{ Exception -> 0x00a9 }
            r17 = r0
            r0 = r17
            r1 = r7
            int r6 = r0.indexOf(r1)     // Catch:{ Exception -> 0x00a9 }
            r0 = r21
            java.lang.String r0 = r0.ReconfirmContent     // Catch:{ Exception -> 0x00a9 }
            r17 = r0
            r0 = r17
            r1 = r14
            int r13 = r0.indexOf(r1)     // Catch:{ Exception -> 0x00a9 }
            if (r13 <= r6) goto L_0x01b8
            r0 = r21
            java.lang.String r0 = r0.ReconfirmContent     // Catch:{ Exception -> 0x00a9 }
            r17 = r0
            int r18 = r7.length()     // Catch:{ Exception -> 0x00a9 }
            int r18 = r18 + r6
            r0 = r17
            r1 = r18
            r2 = r13
            java.lang.String r16 = r0.substring(r1, r2)     // Catch:{ Exception -> 0x00a9 }
        L_0x01b8:
            r0 = r21
            java.lang.String r0 = r0.ReconfirmNumber     // Catch:{ Exception -> 0x00a9 }
            r17 = r0
            r18 = 0
            r0 = r21
            r1 = r17
            r2 = r16
            r3 = r18
            boolean r17 = r0.sendSms(r1, r2, r3)     // Catch:{ Exception -> 0x00a9 }
            if (r17 != 0) goto L_0x01e1
            r0 = r21
            android.os.Handler r0 = r0.handler     // Catch:{ Exception -> 0x00a9 }
            r17 = r0
            r18 = 4
            r17.sendEmptyMessage(r18)     // Catch:{ Exception -> 0x00a9 }
            java.io.IOException r17 = new java.io.IOException     // Catch:{ Exception -> 0x00a9 }
            java.lang.String r18 = "send confirm sms failed!"
            r17.<init>(r18)     // Catch:{ Exception -> 0x00a9 }
            throw r17     // Catch:{ Exception -> 0x00a9 }
        L_0x01e1:
            int r8 = r8 + -1
            goto L_0x0047
        L_0x01e5:
            r0 = r21
            android.os.Handler r0 = r0.handler     // Catch:{ Exception -> 0x00a9 }
            r17 = r0
            r18 = 2
            r17.sendEmptyMessage(r18)     // Catch:{ Exception -> 0x00a9 }
            goto L_0x005e
        L_0x01f2:
            r17 = move-exception
            goto L_0x010f
        L_0x01f5:
            r17 = move-exception
            goto L_0x00e1
        L_0x01f8:
            r17 = move-exception
            r10 = r11
            goto L_0x00bf
        */
        throw new UnsupportedOperationException("Method not decompiled: com.netqin.antivirus.payment.SMSTransform.run():void");
    }

    /* access modifiers changed from: protected */
    public synchronized void doNext() {
        this.wait = false;
        notify();
    }

    /* access modifiers changed from: protected */
    public synchronized void cancel() {
        this.handler.sendEmptyMessage(PaymentHandler.STATUS_CANCALED);
        this.stop = true;
        notify();
    }

    /* access modifiers changed from: private */
    public synchronized boolean onSMSMessage(String no, String content) {
        boolean z;
        if (no.equals(this.intent.getStringExtra("SMSReconfirmNumber")) || PaymentUtil.matches(content, this.intent.getStringExtra("SMSReconfirmMatch"))) {
            this.stop = false;
            this.ReconfirmNumber = no;
            this.ReconfirmContent = content;
            this.wait = true;
            notify();
            this.handler.sendMessage(this.handler.obtainMessage(NetReturnValue.UserAccountCreateSuccess, content));
            z = true;
        } else {
            z = false;
        }
        return z;
    }

    /* access modifiers changed from: protected */
    public String getReconfirmNumber() {
        return this.ReconfirmNumber;
    }

    /* access modifiers changed from: protected */
    public String getReconfirmContent() {
        return this.ReconfirmContent;
    }

    private boolean sendSms(String phone, String content, PendingIntent sentIntent) {
        SmsManager sms = SmsManager.getDefault();
        try {
            ArrayList<String> messageArray = sms.divideMessage(content);
            ArrayList<PendingIntent> sentIntents = new ArrayList<>();
            for (int i = 0; i < messageArray.size(); i++) {
                sentIntents.add(sentIntent);
            }
            sms.sendMultipartTextMessage(phone, null, messageArray, sentIntents, null);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private class SMSReceiver extends BroadcastReceiver {
        private SMSReceiver() {
        }

        /* synthetic */ SMSReceiver(SMSTransform sMSTransform, SMSReceiver sMSReceiver) {
            this();
        }

        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            HashMap<String, StringBuffer> msgs = new HashMap<>();
            try {
                Object[] pdus = (Object[]) bundle.get("pdus");
                for (Object obj : pdus) {
                    SmsMessage msg = SmsMessage.createFromPdu((byte[]) obj);
                    String number = msg.getDisplayOriginatingAddress();
                    String text = msg.getDisplayMessageBody().toLowerCase().trim();
                    if (msgs.containsKey(number)) {
                        ((StringBuffer) msgs.get(number)).append(text);
                    } else {
                        msgs.put(number, new StringBuffer(text));
                    }
                }
                for (String number2 : msgs.keySet()) {
                    if (SMSTransform.this.onSMSMessage(number2, ((StringBuffer) msgs.get(number2)).toString())) {
                        return;
                    }
                }
            } catch (Exception e) {
            }
        }
    }
}
