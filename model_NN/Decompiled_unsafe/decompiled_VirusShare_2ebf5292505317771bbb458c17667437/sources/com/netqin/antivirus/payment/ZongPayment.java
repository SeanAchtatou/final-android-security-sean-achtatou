package com.netqin.antivirus.payment;

import android.content.Intent;
import com.netqin.antivirus.contact.vcard.VCardConfig;

public class ZongPayment extends PaymentService {
    /* access modifiers changed from: protected */
    public void startPayment(Intent intent) {
        intent.setClass(this, ZongProxyActivity.class);
        intent.addFlags(VCardConfig.FLAG_USE_QP_TO_PRIMARY_PROPERTIES);
        startActivity(intent);
    }
}
