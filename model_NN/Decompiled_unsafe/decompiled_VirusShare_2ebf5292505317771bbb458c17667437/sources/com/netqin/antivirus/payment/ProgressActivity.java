package com.netqin.antivirus.payment;

import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.view.KeyEvent;
import com.nqmobile.antivirus_ampro20.R;

public class ProgressActivity extends PaymentActivity {
    /* access modifiers changed from: protected */
    public void doCustomCreate(Intent intent) {
        setContentView((int) R.layout.payment_progress);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /* access modifiers changed from: protected */
    public void onConnected(IBinder binder) {
        Message msg = new Message();
        msg.what = 1004;
        msg.obj = new UCallback(this, null);
        this.handler.sendMessage(msg);
    }

    private class UCallback implements Handler.Callback {
        private UCallback() {
        }

        /* synthetic */ UCallback(ProgressActivity progressActivity, UCallback uCallback) {
            this();
        }

        public boolean handleMessage(Message msg) {
            if (msg.what != -100) {
                return true;
            }
            ProgressActivity.this.finish();
            return true;
        }
    }
}
