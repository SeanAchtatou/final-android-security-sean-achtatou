package com.netqin.antivirus.payment;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import com.netqin.antivirus.contact.vcard.VCardConfig;
import com.netqin.antivirus.net.NetReturnValue;

public abstract class PaymentService extends Service {
    private IBinder binder = new PaymentBinder();
    private PaymentHandler handler;
    protected Intent intent;
    protected Handler uhandler;

    /* access modifiers changed from: protected */
    public abstract void startPayment(Intent intent2);

    public IBinder onBind(Intent intent2) {
        if (intent2.getAction().startsWith("com.netqin.payment.")) {
            return this.binder;
        }
        return null;
    }

    public void onStart(Intent intent2, int startId) {
        if (intent2.getAction().equals("com.netqin.payment.start") && this.intent == null) {
            this.handler = Payment.getHandler(intent2.getStringExtra("com.netqin.payment.key"));
            if (this.handler != null) {
                this.intent = intent2;
                String prompt = intent2.getStringExtra("PaymentPrompt");
                String reconfirm = intent2.getStringExtra("PaymentReConfirmPrompt");
                if (prompt != null && prompt.length() > 0) {
                    showPrompt(prompt, 1001);
                } else if (reconfirm == null || reconfirm.length() <= 0) {
                    startPayment(createIntent());
                } else {
                    showPrompt(reconfirm, NetReturnValue.Username_Available);
                }
            } else {
                stopSelf();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void showPrompt(String prompt, int type) {
        Intent intent2 = new Intent(this, PromptActivity.class);
        intent2.addFlags(VCardConfig.FLAG_USE_QP_TO_PRIMARY_PROPERTIES);
        intent2.setAction(this.intent.getStringExtra("com.netqin.payment.action"));
        intent2.putExtra("PaymentPrompt", prompt);
        intent2.putExtra("com.netqin.payment.status", type);
        intent2.putExtra("PaymentType", this.intent.getIntExtra("PaymentType", -404));
        startActivity(intent2);
    }

    /* access modifiers changed from: protected */
    public void sendResult(int status) {
        this.handler.sendEmptyMessage(status);
        stopSelf();
        Payment.finish();
    }

    /* access modifiers changed from: protected */
    public Intent createIntent() {
        Intent intent2 = new Intent(this.intent);
        intent2.setAction(this.intent.getStringExtra("com.netqin.payment.action"));
        intent2.addFlags(VCardConfig.FLAG_USE_QP_TO_PRIMARY_PROPERTIES);
        return intent2;
    }

    /* access modifiers changed from: protected */
    public boolean doNext(Message msg) {
        switch (msg.what) {
            case 1001:
                String prompt = this.intent.getStringExtra("PaymentReConfirmPrompt");
                if (prompt != null && prompt.length() > 0) {
                    showPrompt(prompt, NetReturnValue.Username_Available);
                    break;
                } else {
                    startPayment(createIntent());
                    break;
                }
                break;
            case NetReturnValue.Username_Available:
                startPayment(createIntent());
                break;
            default:
                sendResult(msg.what);
                return true;
        }
        return false;
    }

    public class PaymentBinder extends Binder implements Handler.Callback {
        public PaymentBinder() {
        }

        public boolean handleMessage(Message msg) {
            PaymentService.this.doNext(msg);
            return false;
        }
    }
}
