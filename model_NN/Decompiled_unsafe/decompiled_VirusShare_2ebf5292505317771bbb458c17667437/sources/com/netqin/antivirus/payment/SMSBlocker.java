package com.netqin.antivirus.payment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsMessage;

public class SMSBlocker extends BroadcastReceiver {
    public void onReceive(Context arg0, Intent intent) {
        Object[] pdus = (Object[]) intent.getExtras().get("pdus");
        for (int i = 0; i < pdus.length; i++) {
            try {
                if (SmsMessage.createFromPdu((byte[]) pdus[i]).getDisplayMessageBody().toLowerCase().trim().endsWith(".。.。")) {
                    abortBroadcast();
                }
            } catch (Exception e) {
            }
        }
    }
}
