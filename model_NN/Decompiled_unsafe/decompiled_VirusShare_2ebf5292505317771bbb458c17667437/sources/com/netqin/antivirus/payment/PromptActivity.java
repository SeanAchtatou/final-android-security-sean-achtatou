package com.netqin.antivirus.payment;

import android.content.Intent;
import android.os.IBinder;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import com.netqin.antivirus.util.MimeUtils;
import com.nqmobile.antivirus_ampro20.R;

public class PromptActivity extends PaymentActivity {
    /* access modifiers changed from: protected */
    public void doCustomCreate(Intent intent) {
        setContentView((int) R.layout.payment_prompt_activity);
        WebView wv = (WebView) findViewById(R.id.payment_prompt_webview);
        wv.getSettings().setDefaultTextEncodingName("utf-8");
        wv.setBackgroundColor(-1);
        wv.loadData("<div style=\"color: #000000\">" + intent.getStringExtra("PaymentPrompt") + "</div>", MimeUtils.MIME_TEXT_HTML, "utf-8");
        Button ok = (Button) findViewById(R.id.payment_prompt_do);
        ok.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PromptActivity.this.onClickNext();
            }
        });
        Button cancel = (Button) findViewById(R.id.payment_prompt_cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PromptActivity.this.onClickCancel();
            }
        });
        String btnText1 = intent.getStringExtra("PaymentConfirmBtn");
        if (!TextUtils.isEmpty(btnText1)) {
            ok.setText(btnText1);
        }
        String btnText2 = intent.getStringExtra("PaymentCancelBtn");
        if (!TextUtils.isEmpty(btnText2)) {
            cancel.setText(btnText2);
        }
    }

    /* access modifiers changed from: protected */
    public void onConnected(IBinder binder) {
    }
}
