package com.netqin.antivirus.payment;

import android.content.Context;
import com.netqin.antivirus.appprotocol.XmlTagValue;

class WebIntent extends PaymentIntent {
    protected WebIntent(int type, Context context) {
        super(type, context);
        setClass(context, WebPayment.class);
        putExtra("com.netqin.payment.action", "com.netqin.payment.WebPayment");
    }

    /* access modifiers changed from: protected */
    public boolean checkAttributes() throws NoSuchParameterException {
        getString(this, XmlTagValue.paymentCentreUrl);
        return true;
    }
}
