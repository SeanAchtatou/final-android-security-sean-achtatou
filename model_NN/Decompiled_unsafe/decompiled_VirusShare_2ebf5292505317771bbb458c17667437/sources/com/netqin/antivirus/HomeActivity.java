package com.netqin.antivirus;

import SHcMjZX.DD02zqNU;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.android.vending.licensing.AESObfuscator;
import com.android.vending.licensing.LicenseChecker;
import com.android.vending.licensing.LicenseCheckerCallback;
import com.android.vending.licensing.ServerManagedPolicy;
import com.netqin.antivirus.antilost.AntiLostMain;
import com.netqin.antivirus.antilost.AntiLostPassword;
import com.netqin.antivirus.antilost.AntiLostService;
import com.netqin.antivirus.antimallink.BlockService;
import com.netqin.antivirus.antivirus.NqPackageManager;
import com.netqin.antivirus.appprotocol.AppRequest;
import com.netqin.antivirus.cloud.model.CloudApkInfoList;
import com.netqin.antivirus.cloud.model.CloudHandler;
import com.netqin.antivirus.cloud.model.xml.XmlUtils;
import com.netqin.antivirus.common.CommonDefine;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.common.CommonUtils;
import com.netqin.antivirus.common.ProgDlgActivity;
import com.netqin.antivirus.contact.ContactGuide;
import com.netqin.antivirus.help.Help;
import com.netqin.antivirus.log.LogManageActivity;
import com.netqin.antivirus.mynetqin.MyNetqin;
import com.netqin.antivirus.mynetqin.MyNetqinChina;
import com.netqin.antivirus.net.appupdateservice.BackgroundAppUpdateService;
import com.netqin.antivirus.networkmanager.SettingPreferences;
import com.netqin.antivirus.privatesoft.PrivacyProtection;
import com.netqin.antivirus.scan.MonitorService;
import com.netqin.antivirus.scan.ScanActivity;
import com.netqin.antivirus.scan.ScanMain;
import com.netqin.antivirus.services.ControlService;
import com.netqin.antivirus.services.NetMeterService;
import com.netqin.antivirus.services.NotificationUtil;
import com.netqin.antivirus.services.WakefulService;
import com.netqin.antivirus.softsetting.AntiVirusSetting;
import com.netqin.antivirus.softupdate.SoftwareUpdate;
import com.netqin.antivirus.ui.NetTrafficActivity;
import com.netqin.antivirus.ui.SystemOptimizationActivity;
import com.netqin.antivirus.util.MimeUtils;
import com.netqin.antivirus.virusdbupdate.UpdateDbActivity;
import com.nqmobile.antivirus_ampro20.R;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class HomeActivity extends ProgDlgActivity {
    private static final String BASE64_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxbg51xLhUsLCxTFd84CWwpYuY1bc1A30yZ6ZiOIpgs97WczcBLkRDQC7VkTfHjrT6TtAdTQo7pJDfwWP+PiRQBo09uGOfkokba7c+5IkBncz01T2iC/49+XlerFfxsJSUUddK1gFFcjvc/SfodsWi8F4wirl2WfVkLxw47VtmiID3wrOocrSr4FZ9AsEPEM/L3UVh/kig2Vz/t2iUVAQcg2USJ6MQ67o7+MDyZBwT/HtK2o/4y8PqexcWg54P7DBL8fK8ogWqvbpf22MZuheB3ECfDEIsCaA8giXYVXSSCV5KOmkwMizltkA/PNAODkLOBoY965iiKKvnxbng2ywmwIDAQAB";
    private static final byte[] SALT = {48, -25, 10, 123, 103, -56, 77, -46, 15, 87, -59, 54, 34, 117, 33, 113, 19, 23, -40, 94};
    public static Activity mActAntiVirusHome = null;
    private static final int softForceUpdateMassage = 0;
    private static final int softUpdateMassage = 1;
    /* access modifiers changed from: private */
    public BackgroundAppUpdateService backgroundAppUpdateService;
    boolean getOUt = false;
    private LicenseChecker mChecker;
    private Handler mHandler;
    boolean mIsTrafficMonitorOn = true;
    private LicenseCheckerCallback mLicenseCheckerCallback;
    private String mMessageBoxText = "";
    Long n = 0L;
    private Notification notification;
    private NotificationManager notificationManager;
    /* access modifiers changed from: private */
    public int[] notify_list_desc = {R.string.home_notify_feedback_desc, R.string.home_notify_recommend_cm_desc, R.string.home_notify_share_desc};
    /* access modifiers changed from: private */
    public int[] notify_list_icon = {R.drawable.notify_feedback, R.drawable.notify_recommend_cm, R.drawable.onnotify_share};
    /* access modifiers changed from: private */
    public int[] notify_list_title = {R.string.label_advice_feedback, R.string.home_notify_recommend_cm_title, R.string.home_notify_share_title};
    /* access modifiers changed from: private */
    public SharedPreferences prefs;
    private String softUpdateStr = "";

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);
        mActAntiVirusHome = this;
        ((TextView) findViewById(R.id.activity_name)).setText(R.string.label_netqin_antivirus_pro);
        findViewById(R.id.home_btn_update_db).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HomeActivity.this.startActivity(new Intent(HomeActivity.this, UpdateDbActivity.class));
            }
        });
        setVirusDBExpired();
        findViewById(R.id.home_btn_privacy_protect).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HomeActivity.this.startActivity(new Intent(HomeActivity.this, PrivacyProtection.class));
            }
        });
        findViewById(R.id.home_btn_system_opt).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HomeActivity.this.startActivity(SystemOptimizationActivity.getLaunchIntent(HomeActivity.this));
            }
        });
        findViewById(R.id.home_btn_contact_backup).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HomeActivity.this.startActivity(new Intent(HomeActivity.this, ContactGuide.class));
            }
        });
        findViewById(R.id.home_btn_antilost).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SharedPreferences spf = HomeActivity.this.getSharedPreferences("nq_antilost", 0);
                boolean z = spf.getBoolean("first", true);
                String password = CommonUtils.getConfigWithStringValue(HomeActivity.this, "nq_antilost", "password", "");
                boolean running = spf.getBoolean("running", false);
                spf.edit().putBoolean("first_temp", false).commit();
                if (!running || password.length() <= 5) {
                    HomeActivity.this.startActivity(new Intent(HomeActivity.this, AntiLostMain.class));
                } else {
                    HomeActivity.this.startActivity(new Intent(HomeActivity.this, AntiLostPassword.class));
                }
            }
        });
        findViewById(R.id.home_btn_traffic_monitor).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HomeActivity.this.startActivity(new Intent(HomeActivity.this, NetTrafficActivity.class));
            }
        });
        findViewById(R.id.member).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HomeActivity.this.startActivity(new Intent(HomeActivity.this, MemberActivity.class));
            }
        });
        findViewById(R.id.quick_scan).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HomeActivity.this.startActivity(new Intent(HomeActivity.this, ScanActivity.class));
            }
        });
        findViewById(R.id.scan_type).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HomeActivity.this.startActivity(new Intent(HomeActivity.this, ScanMain.class));
            }
        });
        ImageView iv2 = (ImageView) findViewById(R.id.title_recommend_cm);
        iv2.setVisibility(0);
        iv2.setClickable(true);
        iv2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                HomeActivity.this.clickShowNotifyDialog();
            }
        });
        Intent intent = new Intent(this, ControlService.class);
        intent.putExtra("start", true);
        startService(intent);
        SharedPreferences spref = getSharedPreferences("netqin", 0);
        if (spref.getBoolean("IsFirstRun", true)) {
            Calendar cale = Calendar.getInstance();
            cale.add(11, 3);
            CommonMethod.setLastDialyCheckTimeToCurrentTime(this);
            CommonMethod.setNextDialyCheckTime(this, cale);
            Calendar calendar = Calendar.getInstance();
            calendar.add(6, 5);
            CommonMethod.setSoftUpdateNextDialyCheckTime(this, calendar);
            CommonMethod.setSoftUpdateLastDialyCheckTimeToCurrentTime(this);
            Calendar calendar2 = Calendar.getInstance();
            calendar2.add(12, 30);
            CommonMethod.setFirstCloudScanTime(this, calendar2);
            spref.edit().putBoolean("IsFirstRun", false).commit();
            CommonMethod.setVirusForecastName(this, getString(R.string.update_db_virusforecast_name_default));
            CommonMethod.setVirusForecastAlias(this, getString(R.string.update_db_virusforecast_alias_default));
            CommonMethod.setVirusForecastLevel(this, getString(R.string.update_db_virusforecast_level_default));
            CommonMethod.setVirusForecastDesc(this, getString(R.string.update_db_virusforecast_desc_default));
            CommonMethod.setVirusForecastWapurl(this, getString(R.string.update_db_virusforecast_detail_default));
        } else {
            int v = 1;
            try {
                v = DD02zqNU.DLLIxskak(getPackageManager(), getPackageName(), 0).versionCode;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            if (v > CommonMethod.getConfigWithIntegerValue(this, "netqin", "oldversionCode", 0)) {
                CommonMethod.putConfigWithIntegerValue(this, "netqin", "softwareupdatetype", 0);
                CommonMethod.putConfigWithIntegerValue(this, "netqin", "oldversionCode", v);
                CommonMethod.setSoftExitState(this, null);
                CommonMethod.setVirusForecastName(this, getString(R.string.update_db_virusforecast_name_default));
                CommonMethod.setVirusForecastAlias(this, getString(R.string.update_db_virusforecast_alias_default));
                CommonMethod.setVirusForecastLevel(this, getString(R.string.update_db_virusforecast_level_default));
                CommonMethod.setVirusForecastDesc(this, getString(R.string.update_db_virusforecast_desc_default));
                CommonMethod.setVirusForecastWapurl(this, getString(R.string.update_db_virusforecast_detail_default));
                new Thread(new Runnable() {
                    public void run() {
                        ContentValues content1 = new ContentValues();
                        content1.put("IMEI", CommonMethod.getIMEI(HomeActivity.this));
                        content1.put("IMSI", CommonMethod.getIMSI(HomeActivity.this));
                        HomeActivity.this.backgroundAppUpdateService = BackgroundAppUpdateService.getInstance(HomeActivity.this);
                        if (HomeActivity.this.backgroundAppUpdateService.request(content1) == 7890) {
                            ControlService.markNeedUpdate(HomeActivity.this, content1);
                        }
                    }
                }).start();
            }
        }
        SharedPreferences spf = getSharedPreferences("nq_antilost", 0);
        if (spf.getBoolean("start", false) || spf.getBoolean("lock", false)) {
            startService(new Intent(this, AntiLostService.class));
        }
        Preferences preferences = new Preferences(this);
        if (preferences.getIsRunMonitor() && !CommonMethod.isServicesRunning(this, MonitorService.class.getName())) {
            startService(new Intent(this, MonitorService.class));
            MonitorService.showMonitorServiceNotification((String) getResources().getText(R.string.time_protection_on), this);
        }
        if (preferences.getIsRunWebBlock()) {
            startService(new Intent(this, BlockService.class));
        }
        WakefulService.acquireStaticLock(this);
        startService(new Intent(this, NetMeterService.class));
        String uid = CommonMethod.getConfigWithStringValue(this, "netqin", XmlUtils.LABEL_CLIENTINFO_UID, "0");
        if ((TextUtils.isEmpty(uid) || uid.equalsIgnoreCase("0")) && !CommonMethod.getConfigWithBooleanValue(this, "netqin", "isactivated", false)) {
            CommonMethod.putConfigWithBooleanValue(this, "netqin", "isactivated", true);
            forceActivate();
        }
        new Thread(new Runnable() {
            public void run() {
                CloudApkInfoList.clearApkInfoList();
                CloudApkInfoList.getApkInfoList(HomeActivity.this);
            }
        }).start();
        this.mHandler = new Handler();
        boolean fromAM = CommonMethod.getConfigWithBooleanValue(this, "netqin", "fromAndroidMarket", false);
        CommonMethod.logDebug("am Test", "check value = " + fromAM);
        this.prefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean lvl = this.prefs.getBoolean("scan_have_successed", false);
        if (!fromAM && !lvl) {
            String deviceId = Settings.Secure.getString(getContentResolver(), "android_id");
            this.mLicenseCheckerCallback = new MyLicenseCheckerCallback(this, null);
            this.mChecker = new LicenseChecker(this, new ServerManagedPolicy(this, new AESObfuscator(SALT, getPackageName(), deviceId)), BASE64_PUBLIC_KEY);
            doLicenseCheck();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        checkSoftwareUpdate();
        int v = 1;
        try {
            v = DD02zqNU.DLLIxskak(getPackageManager(), getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        CommonMethod.putConfigWithIntegerValue(this, "netqin", "oldversionCode", v);
        this.getOUt = false;
        setTrafficMonitorButtonState();
        setVirusDBExpired();
        setAntilostButtonState();
        setScanState();
        TextView tv = (TextView) findViewById(R.id.member_text);
        if (CommonMethod.getIsMember(this)) {
            tv.setText((int) R.string.home_already_member);
        } else {
            tv.setText((int) R.string.home_updatetomember);
        }
        if (CommonMethod.getConfigWithBooleanValue(this, "netqin", "guidetomember", false)) {
            CommonMethod.putConfigWithBooleanValue(this, "netqin", "guidetomember", false);
            if (!CommonMethod.getIsMember(this)) {
                AppRequest.StartSubscribe(this, false, 102);
            }
        }
    }

    private void setScanState() {
        int toNotificationStringId;
        boolean isDanger;
        int neverScan = CommonMethod.getConfigWithIntegerValue(this, "netqin", "neverscan", 1);
        int configWithIntegerValue = CommonMethod.getConfigWithIntegerValue(this, "netqin", "nodeletevirusnum", 0);
        Preferences pf = new Preferences(this);
        Boolean isVirusMonitorOn = Boolean.valueOf(pf.getIsRunMonitor());
        Boolean isWebMonitorOn = Boolean.valueOf(pf.getIsRunWebBlock());
        ImageView statusImg = (ImageView) findViewById(R.id.home_status_img);
        TextView statusText = (TextView) findViewById(R.id.home_status_txt);
        if (neverScan == 1) {
            statusImg.setImageResource(R.drawable.home_icon_status_warning);
            statusText.setText((int) R.string.home_state_never_scan);
            toNotificationStringId = R.string.home_state_never_scan;
            isDanger = false;
        } else if (CommonMethod.checkDangerPackageExist(this) || CommonMethod.checkDangerFileExist(this)) {
            statusImg.setImageResource(R.drawable.home_icon_status_danger);
            statusText.setText((int) R.string.home_state_danger);
            toNotificationStringId = R.string.home_state_danger;
            isDanger = true;
        } else if (isVirusMonitorOn.booleanValue() || isWebMonitorOn.booleanValue()) {
            statusImg.setImageResource(R.drawable.home_icon_status_ok);
            statusText.setText((int) R.string.home_state_ok);
            toNotificationStringId = R.string.home_state_ok;
            isDanger = false;
        } else {
            statusImg.setImageResource(R.drawable.home_icon_status_warning);
            statusText.setText((int) R.string.home_state_monitor_off);
            toNotificationStringId = R.string.home_state_monitor_off;
            isDanger = false;
        }
        if (!isDanger) {
            if (!CommonMethod.checkDangerPackageExist(this)) {
                CommonMethod.clearEditorBySpfFileName(this, "dangerpackage");
            }
            if (!CommonMethod.checkDangerFileExist(this)) {
                CommonMethod.clearEditorBySpfFileName(this, "dangerfile");
            }
        }
        CommonMethod.showFlowBarOrNot(this, new Intent(this, HomeActivity.class), getString(toNotificationStringId), isDanger);
    }

    private void forceActivate() {
        this.mMessageBoxText = "";
        new Thread(new Runnable() {
            public void run() {
                AppRequest.StartRegister(HomeActivity.this, false);
            }
        }).start();
    }

    private void startAntivirusMonitorForce() {
        new Preferences(this).setRunMonitor(true);
        Intent i = new Intent("android.intent.action.RUN");
        i.setClass(getApplicationContext(), MonitorService.class);
        startService(i);
    }

    private void startAntivirusBlockForce() {
        new Preferences(this).setRunWebBlock(true);
        Intent i = new Intent("android.intent.action.RUN");
        i.setClass(getApplicationContext(), BlockService.class);
        startService(i);
    }

    /* access modifiers changed from: package-private */
    public void setTrafficMonitorButtonState() {
        this.prefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean isTrafficOn = this.prefs.getBoolean(SettingPreferences.KEY_METER_ONOFF, true);
        ImageView tmi = (ImageView) findViewById(R.id.home_traffic_func_img);
        if (isTrafficOn) {
            tmi.setImageResource(R.drawable.func_on);
        } else {
            tmi.setImageResource(R.drawable.func_off);
        }
    }

    /* access modifiers changed from: package-private */
    public void setAntilostButtonState() {
        boolean isNowStart = getSharedPreferences("nq_antilost", 0).getBoolean("running", false);
        ImageView ali = (ImageView) findViewById(R.id.home_antilost_func_img);
        if (isNowStart) {
            ali.setImageResource(R.drawable.func_on);
        } else {
            ali.setImageResource(R.drawable.func_off);
        }
    }

    private void setVirusDBExpired() {
        int year;
        int month;
        int day;
        TextView tv = (TextView) findViewById(R.id.virus_db_expire);
        String latest = CommonMethod.getLatestVirusDBVersion(this);
        String current = new Preferences(this).getVirusDBVersion();
        if (latest.compareTo(current) > 0) {
            tv.setVisibility(0);
        } else {
            tv.setVisibility(4);
        }
        try {
            year = Integer.parseInt(current.substring(0, 4));
            month = Integer.parseInt(current.substring(4, 6));
            day = Integer.parseInt(current.substring(6, 8));
        } catch (NumberFormatException e) {
            year = 2011;
            month = 6;
            day = 10;
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date dd = new Date();
        long quot = 0;
        try {
            Date date1 = simpleDateFormat.parse(String.valueOf(year) + "/" + month + "/" + day);
            quot = ((((simpleDateFormat.parse(simpleDateFormat.format(dd)).getTime() - date1.getTime()) / 1000) / 60) / 60) / 24;
        } catch (ParseException e2) {
            e2.printStackTrace();
        }
        if (quot >= 15) {
            tv.setVisibility(0);
        } else {
            tv.setVisibility(4);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.antivirus_main, menu);
        MenuItem mi = menu.getItem(menu.size() - 1);
        if (!CommonMethod.getIsMember(this) || CommonMethod.getUserType(this) == 16) {
            mi.setVisible(false);
            mi.setEnabled(false);
        }
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.antivirus_main_menuitem_setting /*2131558851*/:
                clickSetting();
                break;
            case R.id.antivirus_main_menuitem_log /*2131558852*/:
                clickLog();
                break;
            case R.id.antivirus_main_menuitem_recommend /*2131558853*/:
                clickRecommed();
                break;
            case R.id.antivirus_main_menuitem_mynetqin /*2131558854*/:
                clickMyNetQin();
                break;
            case R.id.antivirus_main_menuitem_feedback /*2131558855*/:
                NqUtil.clickAdviceFeedback(this);
                break;
            case R.id.antivirus_main_menuitem_update /*2131558856*/:
                if (!CloudHandler.checkNet(this)) {
                    Toast.makeText(this, getString(R.string.text_sina_nonetsoftupdatetip), 1).show();
                    break;
                } else {
                    clickUpdate();
                    break;
                }
            case R.id.antivirus_main_menuitem_help /*2131558857*/:
                clickHelp();
                break;
            case R.id.antivirus_main_menuitem_about /*2131558858*/:
                clickAbout();
                break;
            case R.id.antivirus_main_menuitem_license /*2131558859*/:
                clickLicense();
                break;
            case R.id.antivirus_main_menuitem_unsubscribe /*2131558860*/:
                AppRequest.StartUnsubscribe(this, false);
                break;
            default:
                return false;
        }
        return true;
    }

    private void clickUpdate() {
        Value.UpdateType = "1";
        startActivity(new Intent(this, SoftwareUpdate.class));
        SoftwareUpdate.mActAntiVirusHome = this;
    }

    private void clickHelp() {
        startActivity(new Intent(this, Help.class));
    }

    private void clickAbout() {
        String verMark;
        String textMsg;
        switch (0) {
            case 0:
                verMark = "";
                break;
            case 1:
                verMark = " (Release)";
                break;
            case 2:
                verMark = " (Verify)";
                break;
            default:
                verMark = "";
                break;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.text_about_software);
        View vTotal = LayoutInflater.from(this).inflate((int) R.layout.about_netqin, (ViewGroup) null);
        TextView tvVersion = (TextView) vTotal.findViewById(R.id.about_version_builder);
        if ("351".equalsIgnoreCase("350")) {
            textMsg = getString(R.string.text_about_version_build_15, new Object[]{CommonDefine.ANTIVIRUS_VERSION, ((String) CommonDefine.ANTIVIRUS_BUILDER) + verMark});
        } else {
            textMsg = getString(R.string.text_about_version_build, new Object[]{CommonDefine.ANTIVIRUS_VERSION, ((String) CommonDefine.ANTIVIRUS_BUILDER) + verMark});
        }
        tvVersion.setText(textMsg);
        builder.setView(vTotal);
        builder.setPositiveButton((int) R.string.label_ok, (DialogInterface.OnClickListener) null);
        builder.show();
    }

    private void clickLicense() {
        startActivity(new Intent(this, DeclareNote.class));
    }

    private void clickLog() {
        startActivity(new Intent(this, LogManageActivity.class));
    }

    private void clickSetting() {
        startActivity(new Intent(this, AntiVirusSetting.class));
    }

    /* access modifiers changed from: private */
    public void clickRecommed() {
        String smsContent;
        Intent intent = new Intent("android.intent.action.SEND");
        intent.setType(MimeUtils.MIME_TEXT_PLAIN);
        intent.putExtra("android.intent.extra.SUBJECT", getResources().getString(R.string.text_recommend_title));
        String smsContent2 = getResources().getString(R.string.text_recommend_sms_body);
        if (CommonMethod.isLocalSimpleChinese()) {
            smsContent = String.valueOf(smsContent2) + " " + getString(R.string.text_recommend_sms_link_cn);
        } else {
            smsContent = String.valueOf(smsContent2) + " " + getString(R.string.text_recommend_sms_link_en);
        }
        intent.putExtra("android.intent.extra.TEXT", smsContent);
        startActivity(Intent.createChooser(intent, getResources().getString(R.string.text_recommend_title_tip)));
    }

    private void clickMyNetQin() {
        if (CommonMethod.isLocalSimpleChinese()) {
            startActivity(new Intent(this, MyNetqinChina.class));
        } else {
            startActivity(new Intent(this, MyNetqin.class));
        }
    }

    /* access modifiers changed from: protected */
    public void childFunctionCall(Message msg) {
        switch (msg.arg1) {
            case 27:
                this.mMessageBoxText = (String) msg.obj;
                return;
            case CommonDefine.MSG_PROG_ARG_CANCEL_SERVER /*37*/:
                return;
            default:
                this.mMessageBoxText = getString(R.string.text_unprocess_message, new Object[]{Integer.valueOf(msg.arg1)});
                return;
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        DialogInterface.OnClickListener installListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                HomeActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://market.android.com/details?id=com.netqin.antivirusgm20")));
                CommonMethod.exitAntiVirus(HomeActivity.this);
            }
        };
        DialogInterface.OnClickListener uninstallListener15 = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                new NqPackageManager(HomeActivity.this).uninstallPackage(CommonDefine.ANDROID_MARKET_PACKAGE_NAME_V15);
            }
        };
        DialogInterface.OnClickListener exitListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                CommonMethod.exitAntiVirus(HomeActivity.this);
            }
        };
        DialogInterface.OnKeyListener keyListener = new DialogInterface.OnKeyListener() {
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode != 4) {
                    return false;
                }
                CommonMethod.exitAntiVirus(HomeActivity.this);
                return true;
            }
        };
        DialogInterface.OnClickListener updateSoftListener = new DialogInterface.OnClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
            public void onClick(DialogInterface dialog, int which) {
                if (CommonMethod.getConfigWithIntegerValue(HomeActivity.this, "netqin", "softwareupdatetype", 0) == 1) {
                    CommonMethod.putConfigWithIntegerValue(HomeActivity.this, "netqin", "softwareupdatetype", 0);
                    NotificationUtil.closeNotification((NotificationManager) HomeActivity.this.getSystemService("notification"));
                }
                Intent intent = new Intent(HomeActivity.this, SoftwareUpdate.class);
                intent.putExtra("update", true);
                SoftwareUpdate.mActAntiVirusHome = HomeActivity.this;
                HomeActivity.this.startActivity(intent);
            }
        };
        DialogInterface.OnClickListener cancelMayUpdateSoftListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                CommonMethod.putConfigWithIntegerValue(HomeActivity.this, "netqin", "softwareupdatetype", 0);
                NotificationUtil.closeNotification((NotificationManager) HomeActivity.this.getSystemService("notification"));
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        switch (id) {
            case 0:
                builder.setTitle((int) R.string.label_netqin_antivirus);
                builder.setMessage(this.softUpdateStr);
                builder.setPositiveButton((int) R.string.label_ok, updateSoftListener);
                builder.setNegativeButton((int) R.string.label_cancel, exitListener);
                builder.setOnKeyListener(keyListener);
                return builder.create();
            case 1:
                builder.setTitle((int) R.string.label_netqin_antivirus);
                builder.setMessage(this.softUpdateStr);
                builder.setPositiveButton((int) R.string.label_ok, updateSoftListener);
                builder.setNegativeButton((int) R.string.label_cancel, cancelMayUpdateSoftListener);
                return builder.create();
            case R.string.text_version_may_update /*2131427386*/:
                builder.setTitle((int) R.string.label_netqin_antivirus);
                builder.setMessage(getString(R.string.text_version_may_update, new Object[]{getUpdateSoftFileSize(this)}));
                builder.setPositiveButton((int) R.string.label_ok, updateSoftListener);
                builder.setNegativeButton((int) R.string.label_cancel, cancelMayUpdateSoftListener);
                return builder.create();
            case R.string.SEND_RECEIVE_ERROR /*2131427525*/:
                builder.setTitle((int) R.string.label_netqin_antivirus);
                builder.setMessage(this.mMessageBoxText);
                builder.setPositiveButton((int) R.string.label_ok, (DialogInterface.OnClickListener) null);
                this.mMessageBoxText = "";
                return builder.create();
            case R.string.text_force_softupdate /*2131427573*/:
                builder.setTitle((int) R.string.label_netqin_antivirus);
                builder.setMessage(getString(R.string.text_force_softupdate, new Object[]{getUpdateSoftFileSize(this)}));
                builder.setPositiveButton((int) R.string.label_ok, updateSoftListener);
                builder.setNegativeButton((int) R.string.label_cancel, exitListener);
                builder.setOnKeyListener(keyListener);
                return builder.create();
            case R.string.text_activate_succ /*2131427800*/:
                builder.setTitle((int) R.string.label_activate);
                builder.setMessage((int) R.string.text_activate_succ);
                builder.setPositiveButton((int) R.string.label_ok, (DialogInterface.OnClickListener) null);
                return builder.create();
            case R.string.text_version_not_compatible /*2131427866*/:
                builder.setTitle((int) R.string.label_netqin_antivirus);
                builder.setMessage((int) R.string.text_version_not_compatible);
                builder.setPositiveButton((int) R.string.label_install, installListener);
                builder.setNegativeButton((int) R.string.label_exit, exitListener);
                builder.setOnKeyListener(keyListener);
                return builder.create();
            case R.string.text_version_not_compatible_with_package /*2131427870*/:
                builder.setTitle((int) R.string.label_netqin_antivirus);
                builder.setMessage((int) R.string.text_version_not_compatible_with_package);
                builder.setPositiveButton((int) R.string.label_uninstall, uninstallListener15);
                builder.setNegativeButton((int) R.string.label_exit, exitListener);
                builder.setOnKeyListener(keyListener);
                return builder.create();
            case R.string.text_uninstall_other_version /*2131427871*/:
                builder.setTitle((int) R.string.label_netqin_antivirus);
                builder.setMessage((int) R.string.text_uninstall_other_version);
                builder.setPositiveButton((int) R.string.label_uninstall, uninstallListener15);
                builder.setNegativeButton((int) R.string.label_cancel, (DialogInterface.OnClickListener) null);
                builder.setOnKeyListener(keyListener);
                return builder.create();
            case R.string.unlicensed_dialog_body /*2131428421*/:
                return builder.setTitle((int) R.string.label_netqin_antivirus_short).setMessage((int) R.string.unlicensed_dialog_body).setPositiveButton((int) R.string.buy_button, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (HomeActivity.this.isAMInstalled(HomeActivity.this)) {
                            HomeActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + HomeActivity.this.getPackageName())));
                        } else {
                            HomeActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://market.android.com/details?id=" + HomeActivity.this.getPackageName())));
                        }
                        HomeActivity.this.prefs.edit().putBoolean("auto_start_protect", false).commit();
                        CommonMethod.closeServiceNotification(HomeActivity.this);
                        HomeActivity.this.finish();
                    }
                }).setNegativeButton((int) R.string.quit_button, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        HomeActivity.this.prefs.edit().putBoolean("auto_start_protect", false).commit();
                        CommonMethod.closeServiceNotification(HomeActivity.this);
                        HomeActivity.this.finish();
                    }
                }).setOnKeyListener(new DialogInterface.OnKeyListener() {
                    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                        if (keyCode == 4) {
                            HomeActivity.this.prefs.edit().putBoolean("auto_start_protect", false).commit();
                            CommonMethod.closeServiceNotification(HomeActivity.this);
                            HomeActivity.this.finish();
                            return true;
                        } else if (keyCode == 84) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }).create();
            default:
                return super.onCreateDialog(id);
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        String softUpdateMsg;
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        Integer displayType = Integer.valueOf(CommonMethod.getConfigWithIntegerValue(this, "netqin", "softwaredisplaytype", 2));
        int type = CommonMethod.getConfigWithIntegerValue(this, "netqin", "softwareupdatetype", 0);
        if (type == 2 || type == 1) {
            String softUpdateMsg2 = "";
            String softUpdateDisplay = "";
            if (type == 2) {
                if (CommonMethod.getSoftForceUpdateMassage(this).length() > 0) {
                    softUpdateMsg2 = CommonMethod.getSoftForceUpdateMassage(this);
                } else {
                    softUpdateMsg2 = getString(R.string.text_force_softupdate, new Object[]{getUpdateSoftFileSize(this)});
                }
                softUpdateDisplay = getString(R.string.text_force_softupdate, new Object[]{getUpdateSoftFileSize(this)});
            }
            if (type == 1) {
                if (CommonMethod.getSoftUpdateMassage(this).length() > 0) {
                    softUpdateMsg = CommonMethod.getSoftUpdateMassage(this);
                } else {
                    softUpdateMsg = getString(R.string.text_version_may_update, new Object[]{getUpdateSoftFileSize(this)});
                }
                softUpdateDisplay = getString(R.string.text_version_may_update, new Object[]{getUpdateSoftFileSize(this)});
            }
            if (displayType.intValue() == 2) {
                this.notificationManager = (NotificationManager) getSystemService("notification");
                this.notification = NotificationUtil.build(this, getString(R.string.text_sina_softupdate), softUpdateMsg2, softUpdateDisplay);
                this.notificationManager.notify(111, this.notification);
            }
        }
        if (this.getOUt) {
            finish();
        }
        return true;
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (4 == keyCode) {
            if (!this.getOUt) {
                this.getOUt = true;
                Toast.makeText(this, (int) R.string.exit_manager, 0).show();
                return true;
            }
            this.getOUt = false;
        }
        return super.onKeyUp(keyCode, event);
    }

    private void checkSoftwareUpdate() {
        switch (CommonMethod.getConfigWithIntegerValue(this, "netqin", "softwareupdatetype", 0)) {
            case 1:
                this.softUpdateStr = CommonMethod.getSoftUpdateMassage(this);
                if (this.softUpdateStr == null || this.softUpdateStr.length() <= 0) {
                    showDialog(R.string.text_version_may_update);
                    return;
                } else {
                    showDialog(1);
                    return;
                }
            case 2:
                this.softUpdateStr = CommonMethod.getSoftForceUpdateMassage(this);
                if (this.softUpdateStr == null || this.softUpdateStr.length() <= 0) {
                    showDialog(R.string.text_force_softupdate);
                    return;
                } else {
                    showDialog(0);
                    return;
                }
            default:
                return;
        }
    }

    public static String getUpdateSoftFileSize(Context context) {
        int softSize = CommonMethod.getConfigWithIntegerValue(context, "netqin", "appsize", 0);
        if (softSize >= 1024) {
            return String.valueOf(softSize / 1024) + "KB";
        }
        return String.valueOf(softSize) + "B";
    }

    /* access modifiers changed from: private */
    public void clickRecommendCM() {
        String uriRes;
        PackageManager packageManager = getPackageManager();
        try {
            packageManager.getApplicationInfo("com.netqin.cm", 1);
            Intent intent = new Intent("android.intent.action.MAIN");
            intent.addCategory("android.intent.category.LAUNCHER");
            intent.setComponent(new ComponentName("com.netqin.cm", "com.netqin.cm.AntiVirusSplash"));
            startActivity(intent);
        } catch (PackageManager.NameNotFoundException e) {
            try {
                packageManager.getApplicationInfo("com.netqin.mm", 1);
                Intent intent2 = new Intent("android.intent.action.MAIN");
                intent2.addCategory("android.intent.category.LAUNCHER");
                intent2.setComponent(new ComponentName("com.netqin.mm", "com.netqin.mm.AntiVirusSplash"));
                startActivity(intent2);
            } catch (PackageManager.NameNotFoundException e2) {
                if (CommonMethod.isLocalSimpleChinese()) {
                    uriRes = "http://m.netqin.com/products/cm/?c=9735";
                } else {
                    uriRes = "http://m.netqin.com/en/products/mm/?c=9735";
                }
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse(String.valueOf(uriRes) + "&" + CommonMethod.getUploadConfigFeedBack(this))));
            }
        }
    }

    /* access modifiers changed from: private */
    public void clickShowNotifyDialog() {
        NotifyListAdapter listAdapter = new NotifyListAdapter(this);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.label_netqin_antivirus));
        builder.setAdapter(listAdapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                if (arg1 == 0) {
                    NqUtil.clickAdviceFeedback(HomeActivity.this);
                } else if (arg1 == 1) {
                    HomeActivity.this.clickRecommendCM();
                } else {
                    HomeActivity.this.clickRecommed();
                }
            }
        });
        builder.setNegativeButton(getString(R.string.label_cancel), (DialogInterface.OnClickListener) null);
        builder.show();
    }

    private class NotifyListAdapter extends BaseAdapter {
        private Context mContext;
        private LayoutInflater mInflater = LayoutInflater.from(this.mContext);

        public NotifyListAdapter(Context context) {
            this.mContext = context;
        }

        public int getCount() {
            return HomeActivity.this.notify_list_title.length;
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ListViewHolder holder;
            if (convertView == null) {
                convertView = this.mInflater.inflate((int) R.layout.home_notify_item, (ViewGroup) null);
                holder = new ListViewHolder(HomeActivity.this, null);
                holder.icon = (ImageView) convertView.findViewById(R.id.home_notify_icon);
                holder.title = (TextView) convertView.findViewById(R.id.home_notify_title);
                holder.desc = (TextView) convertView.findViewById(R.id.home_notify_description);
                convertView.setTag(holder);
            } else {
                holder = (ListViewHolder) convertView.getTag();
            }
            convertView.setId(position);
            holder.icon.setImageResource(HomeActivity.this.notify_list_icon[position]);
            holder.title.setText(HomeActivity.this.notify_list_title[position]);
            holder.desc.setText(HomeActivity.this.notify_list_desc[position]);
            return convertView;
        }
    }

    private class ListViewHolder {
        TextView desc;
        ImageView icon;
        TextView title;

        private ListViewHolder() {
        }

        /* synthetic */ ListViewHolder(HomeActivity homeActivity, ListViewHolder listViewHolder) {
            this();
        }
    }

    private void doLicenseCheck() {
        setProgressBarIndeterminateVisibility(true);
        this.mChecker.checkAccess(this.mLicenseCheckerCallback);
    }

    private class MyLicenseCheckerCallback implements LicenseCheckerCallback {
        private MyLicenseCheckerCallback() {
        }

        /* synthetic */ MyLicenseCheckerCallback(HomeActivity homeActivity, MyLicenseCheckerCallback myLicenseCheckerCallback) {
            this();
        }

        public void allow() {
            if (!HomeActivity.this.isFinishing()) {
                HomeActivity.this.prefs.edit().putBoolean("scan_have_successed", true).commit();
                CommonMethod.logDebug("am Test", "allow write value = ");
                CommonMethod.putConfigWithBooleanValue(HomeActivity.this, "netqin", "fromAndroidMarket", true);
                HomeActivity.this.displayLicenseResult(HomeActivity.this.getString(R.string.license_allow));
            }
        }

        public void dontAllow() {
            if (!HomeActivity.this.isFinishing()) {
                CommonMethod.logDebug("am Test", "notallow write value = ");
                CommonMethod.putConfigWithBooleanValue(HomeActivity.this, "netqin", "fromAndroidMarket", false);
                HomeActivity.this.showDialog(R.string.unlicensed_dialog_body);
                HomeActivity.this.prefs.edit().putBoolean("auto_start_protect", false).commit();
                CommonMethod.closeServiceNotification(HomeActivity.this);
            }
        }

        public void applicationError(LicenseCheckerCallback.ApplicationErrorCode errorCode) {
            if (!HomeActivity.this.isFinishing()) {
                CommonMethod.logDebug("am Test", "err write value = ");
                CommonMethod.putConfigWithBooleanValue(HomeActivity.this, "netqin", "fromAndroidMarket", false);
                HomeActivity.this.displayLicenseResult(String.format(HomeActivity.this.getString(R.string.application_error), errorCode));
                dontAllow();
            }
        }
    }

    /* access modifiers changed from: private */
    public void displayLicenseResult(String result) {
        this.mHandler.post(new Runnable() {
            public void run() {
                HomeActivity.this.setProgressBarIndeterminateVisibility(false);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.mChecker.onDestroy();
    }

    public boolean isAMInstalled(Context context) {
        Boolean isAvilible = false;
        List<PackageInfo> pinfo = context.getPackageManager().getInstalledPackages(0);
        if (pinfo != null) {
            for (int i = 0; i < pinfo.size(); i++) {
                if (pinfo.get(i).packageName.equals("com.android.vending")) {
                    isAvilible = true;
                }
            }
        }
        return isAvilible.booleanValue();
    }
}
