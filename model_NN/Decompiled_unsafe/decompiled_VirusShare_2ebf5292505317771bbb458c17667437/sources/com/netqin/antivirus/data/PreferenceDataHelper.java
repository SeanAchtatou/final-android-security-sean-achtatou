package com.netqin.antivirus.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.format.DateFormat;
import com.netqin.antivirus.networkmanager.NetApplication;
import com.netqin.antivirus.networkmanager.SettingPreferences;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;

public class PreferenceDataHelper {
    private static final String KEY_ACCOUNT_BALANCE = "account_balance";
    private static final String KEY_ACCOUNT_NO = "account_no";
    private static final String KEY_ACCOUNT_TYPE = "account_type";
    private static final String KEY_BUSINESS_ID = "business_id";
    private static final String KEY_DEFAULT_PREFERENCE = "com.nqmobile.antivirus20_preferences";
    private static final String KEY_EDITION_ID = "edition_id";
    private static final String KEY_FIRST_RUN = "first_run";
    private static final String KEY_FLOW_ADJUST_DATE = "flow_adjust_date";
    private static final String KEY_FLOW_END_DATE = "flow_end_date";
    private static final String KEY_FLOW_START_DATE = "flow_start_date";
    private static final String KEY_FlOW_ADJUST_VALUE = "flow_adjust_value";
    private static final String KEY_IMPORTANT_PROC_LIST_PREFERENCE = "key_proc_list_preference";
    private static final String KEY_LAST_CONNECT_TIME = "LAST_CONNECT_TIME";
    private static final String KEY_METER_RECHARGE_TIP_SWITCHER = "when_recharge_complete";
    private static final String KEY_METER_TRAFFIC_THRESHOLD = "meter_traffic_threshold";
    private static final String KEY_METER_TRAFFIC_THRESHOLD2 = "meter_traffic_threshold";
    private static final String KEY_NEED_TRAFFIC_ALERT = "need_traffic_alert";
    private static final String KEY_NEW_UPDATE_SIZE = "new_update_size";
    private static final String KEY_NEW_UPDATE_URL = "new_update_url";
    private static final String KEY_NEXT_CONNECT_TIME = "next_connect_time";
    private static final String KEY_NOTIFY_SOFT_UPDATE = "notify_soft_update";
    private static final String KEY_PLATFORM_ID = "platform_id";
    private static final String KEY_SHOW_AD_PANEL = "show_ad_panel";
    private static final String KEY_SHOW_ONEKEY_CONF = "show_onekey_dialog";
    private static final String KEY_TIPS_RINGTONE = "tips_ringtone";
    private static final String KEY_WHITE_LIST_PREFERENCE = "white_list_preference";
    private static final String VAL_DEFAULT_BUSINESS_ID = "114";
    private static final String VAL_DEFAULT_EDITION_ID = "540";
    private static final String VAL_DEFAULT_PLATFORM_ID = "351";
    private static final String VAL_STRING_EMPTY = "";
    private static SharedPreferences mDefaultPref;
    private static SharedPreferences mKeyProcListPref;
    private static SharedPreferences mWhiteListPref;

    public static void addWhiteApp(Context context, String pkgName) {
        if (mWhiteListPref == null) {
            mWhiteListPref = context.getSharedPreferences(KEY_WHITE_LIST_PREFERENCE, 0);
        }
        SharedPreferences.Editor editor = mWhiteListPref.edit();
        editor.putString(pkgName, pkgName);
        editor.commit();
    }

    public static boolean isInWhiteList(Context context, String pkgName) {
        if (mWhiteListPref == null) {
            mWhiteListPref = context.getSharedPreferences(KEY_WHITE_LIST_PREFERENCE, 0);
        }
        return mWhiteListPref.contains(pkgName);
    }

    public static void initWhiteList(Context context) {
        if (mWhiteListPref == null) {
            mWhiteListPref = context.getSharedPreferences(KEY_WHITE_LIST_PREFERENCE, 0);
        }
        SharedPreferences.Editor editor = mWhiteListPref.edit();
        String appPkgName = context.getPackageName();
        editor.putString(appPkgName, appPkgName);
        editor.putString("com.netqin.cm", "com.netqin.cm");
        editor.putString("com.rechild.advancedtaskkiller", "com.rechild.advancedtaskkiller");
        editor.putString("com.netqin.mobileguard", "com.netqin.mobileguard");
        editor.commit();
    }

    public static void addKeyProcess(Context context, String procName) {
        if (mKeyProcListPref == null) {
            mKeyProcListPref = context.getSharedPreferences(KEY_IMPORTANT_PROC_LIST_PREFERENCE, 0);
        }
        SharedPreferences.Editor editor = mKeyProcListPref.edit();
        editor.putString(procName, procName);
        editor.commit();
    }

    public static void initKeyProcessList(Context context) {
        if (mKeyProcListPref == null) {
            mKeyProcListPref = context.getSharedPreferences(KEY_IMPORTANT_PROC_LIST_PREFERENCE, 0);
        }
        SharedPreferences.Editor editor = mKeyProcListPref.edit();
        editor.clear();
        editor.putString("com.android.phone", "com.android.phone");
        editor.putString("com.android.providers.telephony", "com.android.providers.telephony");
        editor.putString("android.process.acore", "android.process.acore");
        editor.putString("system", "system");
        editor.commit();
    }

    public static boolean isInKeyProccessList(Context context, String procName) {
        if (mKeyProcListPref == null) {
            mKeyProcListPref = context.getSharedPreferences(KEY_IMPORTANT_PROC_LIST_PREFERENCE, 0);
        }
        return mKeyProcListPref.contains(procName);
    }

    public static ArrayList<String> getKeyProccessList(Context context) {
        if (mKeyProcListPref == null) {
            mKeyProcListPref = context.getSharedPreferences(KEY_IMPORTANT_PROC_LIST_PREFERENCE, 0);
        }
        Collection<?> values = mKeyProcListPref.getAll().values();
        ArrayList<String> procList = new ArrayList<>();
        Iterator<?> iterator = values.iterator();
        while (iterator.hasNext()) {
            procList.add((String) iterator.next());
        }
        return procList;
    }

    public static void removeWhiteApp(Context context, String appPkgName) {
        if (mWhiteListPref == null) {
            mWhiteListPref = context.getSharedPreferences(KEY_WHITE_LIST_PREFERENCE, 0);
        }
        SharedPreferences.Editor editor = mWhiteListPref.edit();
        editor.remove(appPkgName);
        editor.commit();
    }

    public static ArrayList<String> getWhiteAppList(Context context) {
        if (mWhiteListPref == null) {
            mWhiteListPref = context.getSharedPreferences(KEY_WHITE_LIST_PREFERENCE, 0);
        }
        Collection<?> values = mWhiteListPref.getAll().values();
        ArrayList<String> whiteList = new ArrayList<>();
        Iterator<?> iterator = values.iterator();
        while (iterator.hasNext()) {
            whiteList.add((String) iterator.next());
        }
        return whiteList;
    }

    public static void setGprsDataConnectivityEnabled(Context context, boolean enabled) {
        SharedPreferences.Editor ed = PreferenceManager.getDefaultSharedPreferences(context).edit();
        ed.putBoolean(SettingPreferences.KEY_METER_GPRS_SWITCHER, enabled);
        ed.commit();
    }

    public static boolean getGprsDataConnectivityEnabled(Context context, boolean defaultValue) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(SettingPreferences.KEY_METER_GPRS_SWITCHER, defaultValue);
    }

    public static boolean isFirstRun(Context context) {
        return getDefaultPref(context).getBoolean(KEY_FIRST_RUN, true);
    }

    public static void setFirstRun(Context context, boolean run) {
        SharedPreferences.Editor editor = getDefaultPref(context).edit();
        editor.putBoolean(KEY_FIRST_RUN, run);
        editor.commit();
    }

    public static String getTrafficThreshold(Context context) {
        return getDefaultPref(context).getString(SettingPreferences.KEY_METER_THRESHOLD, NetApplication.DEFAULT_THRESHOLD);
    }

    public static void setTrafficThreshold(Context context, String trafficValue) {
        SharedPreferences.Editor editor = getDefaultPref(context).edit();
        editor.putString(SettingPreferences.KEY_METER_THRESHOLD, trafficValue);
        editor.commit();
    }

    public static String getAccountNo(Context context) {
        return getDefaultPref(context).getString(KEY_ACCOUNT_NO, VAL_STRING_EMPTY);
    }

    public static void setAccountNo(Context context, String userId) {
        SharedPreferences.Editor editor = getDefaultPref(context).edit();
        editor.putString(KEY_ACCOUNT_NO, userId);
        editor.commit();
    }

    public static void clearNewUpdateInfo(Context context) {
        SharedPreferences.Editor editor = getDefaultPref(context).edit();
        editor.putString(KEY_NEW_UPDATE_URL, VAL_STRING_EMPTY);
        editor.putString(KEY_NEW_UPDATE_SIZE, VAL_STRING_EMPTY);
        editor.commit();
    }

    public static void setEnabledNotifyUpdate(Context context, boolean enabledNotify) {
        SharedPreferences.Editor editor = getDefaultPref(context).edit();
        editor.putBoolean(KEY_NOTIFY_SOFT_UPDATE, enabledNotify);
        editor.commit();
    }

    public static boolean getEnabledNotifyUpdate(Context context) {
        return getDefaultPref(context).getBoolean(KEY_NOTIFY_SOFT_UPDATE, true);
    }

    public static void setLastConnectTime(Context context, long lastConnectTime) {
        SharedPreferences.Editor editor = getDefaultPref(context).edit();
        editor.putLong(KEY_LAST_CONNECT_TIME, lastConnectTime);
        editor.commit();
    }

    public static long getLastConnectTime(Context context) {
        return getDefaultPref(context).getLong(KEY_LAST_CONNECT_TIME, 0);
    }

    public static Long getNextConnectTime(Context context) {
        return Long.valueOf(getDefaultPref(context).getLong(KEY_NEXT_CONNECT_TIME, 0));
    }

    public static void setNextConnectTime(Context context, long nct) {
        if (nct > 0) {
            SharedPreferences.Editor editor = getDefaultPref(context).edit();
            editor.putLong(KEY_NEXT_CONNECT_TIME, nct);
            editor.commit();
        }
    }

    public static int getAccountBalance(Context context) {
        return getDefaultPref(context).getInt(KEY_ACCOUNT_BALANCE, 0);
    }

    public static String getPlatformID(Context context) {
        return getDefaultPref(context).getString(KEY_PLATFORM_ID, "351");
    }

    public static String getBusinessID(Context context) {
        return getDefaultPref(context).getString(KEY_BUSINESS_ID, VAL_DEFAULT_BUSINESS_ID);
    }

    public static boolean isShowAdPanel(Context context) {
        if (!getDefaultPref(context).contains(KEY_SHOW_AD_PANEL)) {
            SharedPreferences.Editor editor = getDefaultPref(context).edit();
            editor.putBoolean(KEY_SHOW_AD_PANEL, true);
            editor.commit();
        }
        return getDefaultPref(context).getBoolean(KEY_SHOW_AD_PANEL, false);
    }

    public static String getEditionID(Context context) {
        return getDefaultPref(context).getString(KEY_EDITION_ID, VAL_DEFAULT_EDITION_ID);
    }

    private static SharedPreferences getDefaultPref(Context context) {
        if (mDefaultPref == null) {
            mDefaultPref = context.getSharedPreferences(KEY_DEFAULT_PREFERENCE, 0);
        }
        return mDefaultPref;
    }

    public static boolean enabledAlert(Context context) {
        return getDefaultPref(context).getBoolean(SettingPreferences.KEY_METER_ALERT, true);
    }

    public static boolean isShowOnekeyConf(Context context) {
        return getDefaultPref(context).getBoolean(KEY_SHOW_ONEKEY_CONF, true);
    }

    public static void setShowOnekeyConf(Context context, boolean value) {
        getDefaultPref(context).edit().putBoolean(KEY_SHOW_ONEKEY_CONF, value).commit();
    }

    public static boolean isNotifyRechargeComplete(Context context) {
        return getDefaultPref(context).getBoolean(KEY_METER_RECHARGE_TIP_SWITCHER, true);
    }

    public static void setNotifyRechargeComplete(Context context, boolean value) {
        getDefaultPref(context).edit().putBoolean(KEY_METER_RECHARGE_TIP_SWITCHER, value).commit();
    }

    public static void setTipsRingTone(Context context, String value) {
        getDefaultPref(context).edit().putString(KEY_TIPS_RINGTONE, value).commit();
    }

    public static String getTipsRingTone(Context context) {
        return getDefaultPref(context).getString(KEY_TIPS_RINGTONE, VAL_DEFAULT_BUSINESS_ID);
    }

    public static void setFlowAdjustValue(Context context, long value) {
        getDefaultPref(context).edit().putLong(KEY_FlOW_ADJUST_VALUE, value).commit();
    }

    public static long getFlowAdjustValue(Context context) {
        return getDefaultPref(context).getLong(KEY_FlOW_ADJUST_VALUE, 0);
    }

    public static void setNeedTrafficAlert(Context context, boolean value) {
        getDefaultPref(context).edit().putBoolean(KEY_NEED_TRAFFIC_ALERT, value).commit();
    }

    public static boolean getNeedTrafficAlert(Context context) {
        return getDefaultPref(context).getBoolean(KEY_NEED_TRAFFIC_ALERT, true);
    }

    public static void setFlowAdjustDate(Context context, String value) {
        getDefaultPref(context).edit().putString(KEY_FLOW_ADJUST_DATE, value).commit();
    }

    public static String getFlowAdjustDate(Context context) {
        return getDefaultPref(context).getString(KEY_FLOW_ADJUST_DATE, VAL_STRING_EMPTY);
    }

    public static void setFlowStartDate(Context context, String value) {
        getDefaultPref(context).edit().putString(KEY_FLOW_START_DATE, value).commit();
    }

    public static String getFlowStartDate(Context context) {
        return getDefaultPref(context).getString(KEY_FLOW_START_DATE, VAL_STRING_EMPTY);
    }

    public static void setFlowEndDate(Context context, String value) {
        getDefaultPref(context).edit().putString(KEY_FLOW_END_DATE, value).commit();
    }

    public static String getFlowEndDate(Context context) {
        return getDefaultPref(context).getString(KEY_FLOW_END_DATE, VAL_STRING_EMPTY);
    }

    public static String[] getFlowStartAndEndDate(Context context) {
        String end;
        String start;
        String[] s = new String[2];
        String startDate = PreferenceManager.getDefaultSharedPreferences(context).getString(SettingPreferences.KEY_METER_START_DATE, "1");
        Calendar c = Calendar.getInstance();
        Calendar cc = Calendar.getInstance();
        cc.setTimeInMillis(c.getTimeInMillis());
        cc.set(5, Integer.valueOf(startDate).intValue());
        if (c.before(cc)) {
            c.add(2, -1);
            c.set(5, Integer.valueOf(startDate).intValue());
            cc.add(5, -1);
            start = (String) DateFormat.format("yyyy-MM-dd", c);
            end = (String) DateFormat.format("yyyy-MM-dd", cc);
        } else {
            c.add(2, 1);
            c.set(5, Integer.valueOf(startDate).intValue());
            c.add(5, -1);
            end = (String) DateFormat.format("yyyy-MM-dd", c);
            start = (String) DateFormat.format("yyyy-MM-dd", cc);
        }
        s[0] = start;
        s[1] = end;
        return s;
    }
}
