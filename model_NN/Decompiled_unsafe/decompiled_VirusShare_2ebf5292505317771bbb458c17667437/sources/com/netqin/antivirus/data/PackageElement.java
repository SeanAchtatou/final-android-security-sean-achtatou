package com.netqin.antivirus.data;

import android.content.pm.PackageInfo;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import com.netqin.antivirus.cloud.apkinfo.domain.SClasse;

public class PackageElement {
    public boolean haveServerData = false;
    private Drawable icon;
    private int importance;
    public boolean isChecked = false;
    private ResolveInfo mResolveInfo;
    private PackageInfo packageInfo;
    private String packageName;
    private int rank;
    private SClasse rankClass;
    private String score;
    private String security;
    private String securityDesc;

    public PackageElement() {
    }

    public PackageElement(ResolveInfo resolveInfo) {
        this.mResolveInfo = resolveInfo;
        setPackageName(resolveInfo.activityInfo.packageName);
    }

    public ResolveInfo getResolveInfo() {
        return this.mResolveInfo;
    }

    public PackageInfo getPackageInfo() {
        return this.packageInfo;
    }

    public void setPackageInfo(PackageInfo packageInfo2) {
        this.packageInfo = packageInfo2;
    }

    public int getImportance() {
        return this.importance;
    }

    public String getPackageName() {
        return this.packageName;
    }

    public void setPackageName(String packageName2) {
        this.packageName = packageName2;
    }

    public void setImportance(int importance2) {
        this.importance = importance2;
    }

    public int getRank() {
        return this.rank;
    }

    public void setRank(int rank2) {
        this.rank = rank2;
    }

    public String getSecurity() {
        return this.security;
    }

    public void setSscurity(String security2) {
        this.security = security2;
    }

    public String getSecurityDesc() {
        return this.securityDesc;
    }

    public void setSscurityDesc(String securityDesc2) {
        this.securityDesc = securityDesc2;
    }

    public String getScore() {
        return this.score;
    }

    public void setScore(String score2) {
        this.score = score2;
    }

    public void setIcon(Drawable icon2) {
        this.icon = icon2;
    }

    public SClasse getSClasse() {
        return this.rankClass;
    }

    public void setSClasse(SClasse rankclass) {
        this.rankClass = rankclass;
    }
}
