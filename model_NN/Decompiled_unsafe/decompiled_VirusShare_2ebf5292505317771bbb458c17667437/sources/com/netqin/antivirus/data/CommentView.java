package com.netqin.antivirus.data;

public class CommentView {
    public String author;
    public String commentBody;
    public float rating;
    public String time;
}
