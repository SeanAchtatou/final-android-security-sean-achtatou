package com.netqin.antivirus.data;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import com.netqin.antivirus.util.AppFilter;

public class Application implements Comparable<Application> {
    private static ActivityManager mActivityManager;
    private static PackageManager mPackageManager;
    public AppFilter filter = new AppFilter();
    private Drawable icon;
    public int importance;
    public boolean isChecked = false;
    public String labelName;
    private int memory;
    public String packageName;
    public int pid;
    public String processName;
    public int uid;

    public int compareTo(Application app) {
        return this.labelName.compareTo(app.labelName);
    }

    public Application() {
    }

    public Application(String packageName2) {
        this.packageName = packageName2;
    }

    public int getMemory(Context context) {
        if (this.memory > 0) {
            return this.memory;
        }
        if (context == null) {
            return 0;
        }
        if (mActivityManager == null) {
            mActivityManager = (ActivityManager) context.getSystemService("activity");
        }
        this.memory = mActivityManager.getProcessMemoryInfo(new int[]{this.pid})[0].getTotalPss();
        return this.memory;
    }

    public void setMemory(int memory2) {
        this.memory = memory2;
    }

    public Drawable getIcon(Context context) {
        if (this.icon != null) {
            return this.icon;
        }
        if (context == null) {
            return null;
        }
        if (mPackageManager == null) {
            mPackageManager = context.getPackageManager();
        }
        try {
            this.icon = mPackageManager.getApplicationIcon(this.packageName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return this.icon;
    }

    public void setIcon(Drawable icon2) {
        this.icon = icon2;
    }

    public String getLabelName(Context context) {
        if (this.labelName != null) {
            return this.labelName;
        }
        if (context == null) {
            return null;
        }
        if (mPackageManager == null) {
            mPackageManager = context.getPackageManager();
        }
        try {
            this.labelName = mPackageManager.getApplicationLabel(mPackageManager.getApplicationInfo(this.packageName, 128)).toString();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return this.labelName;
    }

    public void setLabelName(String labelName2) {
        this.labelName = labelName2;
    }

    public int hashCode() {
        int i = 1 * 31;
        return this.uid + 31;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (this.uid != ((Application) obj).uid) {
            return false;
        }
        return true;
    }

    public String toString() {
        return String.format("(:PKG_NAME '%s' :PROC_NAME '%s' :PID %d :SEL_STAT %B : APP_LEVEL '%d' : UID %d )", this.packageName, this.processName, Integer.valueOf(this.pid), Boolean.valueOf(this.isChecked), Integer.valueOf(this.filter.getAppLevel()), Integer.valueOf(this.uid));
    }
}
