package com.netqin.antivirus;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.netqin.antivirus.appprotocol.AppRequest;
import com.netqin.antivirus.common.CommonMethod;
import com.nqmobile.antivirus_ampro20.R;

public class MemberActivity extends Activity implements View.OnClickListener {
    private TextView account;
    private TextView memberTip;
    private TextView moreMemberRight;
    private ViewGroup relativeLayout;
    private LinearLayout upgradeButton;
    private TextView userType;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.member);
        setRequestedOrientation(1);
        ((TextView) findViewById(R.id.activity_name)).setText((int) R.string.lable_member_title);
        this.upgradeButton = (LinearLayout) findViewById(R.id.upgradetomember);
        this.upgradeButton.setOnClickListener(this);
        this.moreMemberRight = (TextView) findViewById(R.id.morerigth_tip);
        this.moreMemberRight.setOnClickListener(this);
    }

    private void showData() {
        this.account = (TextView) findViewById(R.id.account);
        this.userType = (TextView) findViewById(R.id.user_type);
        this.memberTip = (TextView) findViewById(R.id.member_tip);
        this.relativeLayout = (ViewGroup) findViewById(R.id.layout1);
        if (CommonMethod.getIsMember(this)) {
            this.relativeLayout.setBackgroundResource(R.drawable.member_bg);
            this.memberTip.setText(getString(R.string.member_ok_tip));
            this.upgradeButton.setVisibility(8);
        } else {
            this.relativeLayout.setBackgroundResource(R.drawable.not_member_bg);
            this.memberTip.setText(getString(R.string.member_tip));
            this.upgradeButton.setVisibility(0);
        }
        this.account.setText(getString(R.string.lable_member_account, new Object[]{CommonMethod.getUID(this)}));
        if (!CommonMethod.getIsMember(this)) {
            this.userType.setText(getString(R.string.lable_member_user_type, new Object[]{getString(R.string.netqin_domestic_consumer)}));
        } else if (CommonMethod.getUserLevelname(this).length() > 0) {
            this.userType.setText(getString(R.string.lable_member_user_type, new Object[]{CommonMethod.getUserLevelname(this)}));
        } else {
            this.userType.setText(getString(R.string.lable_member_user_type, new Object[]{getString(R.string.netqin_member)}));
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        showData();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.morerigth_tip /*2131558623*/:
                String uri = "http://my.netqin.com/vip/android/index.jsp?l=en_us";
                if (CommonMethod.isLocalSimpleChinese()) {
                    uri = "http://my.netqin.com/vip/android/index.jsp";
                }
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse(uri)));
                return;
            case R.id.upgradetomember /*2131558624*/:
                AppRequest.StartSubscribe(this, false, 111);
                return;
            default:
                return;
        }
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.clear();
        MenuInflater inflater = getMenuInflater();
        if (!CommonMethod.getIsMember(this)) {
            inflater.inflate(R.menu.not_member_menu, menu);
            return true;
        } else if (CommonMethod.getUserType(this) == 16) {
            inflater.inflate(R.menu.not_member_menu, menu);
            return true;
        } else {
            inflater.inflate(R.menu.member_menu, menu);
            return true;
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        String uri;
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.netqin_pay_center /*2131558870*/:
                if (CommonMethod.isLocalSimpleChinese()) {
                    uri = String.valueOf("http://my.netqin.com/vip/buy/buy_av5.jsp") + "?" + CommonMethod.getUploadConfigFeedBack(this);
                } else {
                    uri = String.valueOf("http://my.netqin.com/vip/buy/buy_av5.jsp?l=en_us") + "&" + CommonMethod.getUploadConfigFeedBack(this);
                }
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse(uri)));
                break;
            case R.id.netqin_homepage /*2131558871*/:
                String uri2 = "http://m.netqin.com/en/";
                if (CommonMethod.isLocalSimpleChinese()) {
                    uri2 = "http://m.netqin.com/";
                }
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse(uri2)));
                break;
            case R.id.netqin_unsubscribe /*2131558872*/:
                AppRequest.StartUnsubscribe(this, false);
                break;
            case R.id.netqin_advice_feedback /*2131558873*/:
                NqUtil.clickAdviceFeedback(this);
                break;
            default:
                return false;
        }
        return true;
    }
}
