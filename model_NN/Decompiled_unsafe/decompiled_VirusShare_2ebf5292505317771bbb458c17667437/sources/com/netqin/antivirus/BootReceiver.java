package com.netqin.antivirus;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import com.netqin.antivirus.antimallink.BlockService;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.scan.MonitorService;
import com.netqin.antivirus.services.ControlService;
import com.netqin.antivirus.services.OnAlarmReceiver;
import com.nqmobile.antivirus_ampro20.R;

public class BootReceiver extends BroadcastReceiver {
    private static final long DELAY = 10000;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED") && !CommonMethod.isFirstRun(context) && CommonMethod.getAutoStartTag(context)) {
            Intent j = new Intent(context, ControlService.class);
            j.putExtra("start", true);
            context.startService(j);
            Preferences pref = new Preferences(context);
            if (pref.getIsAutoRun()) {
                if (pref.getIsRunMonitor()) {
                    Intent i = new Intent("android.intent.action.RUN");
                    i.setClass(context, MonitorService.class);
                    MonitorService.showMonitorServiceNotification((String) context.getResources().getText(R.string.time_protection_on), context);
                    context.startService(i);
                }
                if (pref.getIsRunWebBlock()) {
                    Intent intent2 = new Intent("android.intent.action.RUN");
                    intent2.setClass(context, BlockService.class);
                    context.startService(intent2);
                }
            }
        }
        onBootCompleted(context);
    }

    private void onBootCompleted(Context context) {
        ((AlarmManager) context.getSystemService("alarm")).set(2, SystemClock.elapsedRealtime() + DELAY, PendingIntent.getBroadcast(context, 0, new Intent(context, OnAlarmReceiver.class), 0));
    }
}
