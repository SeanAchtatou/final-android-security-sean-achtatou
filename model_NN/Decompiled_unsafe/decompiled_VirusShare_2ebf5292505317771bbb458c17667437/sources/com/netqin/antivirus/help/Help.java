package com.netqin.antivirus.help;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.WebView;
import com.netqin.antivirus.common.CommonMethod;
import com.nqmobile.antivirus_ampro20.R;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

public class Help extends Activity {
    private static final String LICENSE = "help.html";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.help);
        setRequestedOrientation(1);
        WebView web = (WebView) findViewById(R.id.declare_note_content);
        File fileLicense = getFileStreamPath(LICENSE);
        if (fileLicense.exists()) {
            fileLicense.delete();
        }
        try {
            fileLicense.createNewFile();
            FileOutputStream output = new FileOutputStream(fileLicense);
            int webId = R.raw.help;
            if (CommonMethod.getCountryLanguage().equalsIgnoreCase("zh_CN")) {
                webId = R.raw.help_cn;
            }
            InputStream in = getResources().openRawResource(webId);
            byte[] buff = new byte[in.available()];
            in.read(buff);
            in.close();
            output.write(buff);
            output.close();
            web.loadUrl(Uri.parse("file://" + fileLicense.getCanonicalPath()).toString());
        } catch (Exception e) {
            CommonMethod.logDebug("netqin", "DeclareNote: " + e.getMessage());
        }
    }
}
