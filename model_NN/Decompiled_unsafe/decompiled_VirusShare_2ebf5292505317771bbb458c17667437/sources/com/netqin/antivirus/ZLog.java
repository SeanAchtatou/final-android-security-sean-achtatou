package com.netqin.antivirus;

import android.util.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class ZLog {
    public static ZLog log = null;
    public static File logfile = null;
    public static FileOutputStream output = null;
    private boolean mOutputLog;

    public static synchronized ZLog getInstance() {
        ZLog zLog;
        synchronized (ZLog.class) {
            if (log == null) {
                log = new ZLog();
            }
            zLog = log;
        }
        return zLog;
    }

    private ZLog() {
        this.mOutputLog = true;
        this.mOutputLog = false;
        if (this.mOutputLog) {
            int i = 1;
            while (true) {
                logfile = new File("/sdcard/netqin_zlog_" + i + ".txt");
                if (!logfile.exists()) {
                    try {
                        break;
                    } catch (Exception e) {
                    }
                } else {
                    i++;
                }
            }
            logfile.createNewFile();
            output = new FileOutputStream(logfile);
            if (output == null) {
                Log.d("log", "create file error   ");
            }
        }
    }

    public static synchronized boolean writeLog(String log2) {
        boolean z;
        synchronized (ZLog.class) {
            getInstance();
            try {
                if (output != null) {
                    output.write(log2.getBytes());
                    output.write("\n".getBytes());
                }
                z = true;
            } catch (IOException e) {
                z = false;
            }
        }
        return z;
    }

    public static synchronized boolean writeLog(byte[] log2) {
        boolean z;
        synchronized (ZLog.class) {
            getInstance();
            try {
                if (output != null) {
                    output.write(log2);
                    output.write("\n".getBytes());
                }
                z = true;
            } catch (IOException e) {
                z = false;
            }
        }
        return z;
    }

    public static void destory() {
        try {
            if (output != null) {
                output.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
