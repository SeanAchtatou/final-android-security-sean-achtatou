package com.netqin.antivirus.contact.vcard;

import java.util.HashMap;
import java.util.Map;

public class VCardConfig {
    public static final String DEFAULT_CHARSET = "iso-8859-1";
    private static final int FLAG_CHARSET_SHIFT_JIS = 32;
    private static final int FLAG_CHARSET_UTF8 = 0;
    private static final int FLAG_DOCOMO = 536870912;
    private static final int FLAG_USE_ANDROID_PROPERTY = Integer.MIN_VALUE;
    private static final int FLAG_USE_DEFACT_PROPERTY = 1073741824;
    public static final int FLAG_USE_QP_TO_PRIMARY_PROPERTIES = 268435456;
    private static final int FLAG_V21 = 0;
    private static final int FLAG_V30 = 1;
    public static final boolean IGNORE_CASE_EXCEPT_VALUE = true;
    static final int LOG_LEVEL = 0;
    static final int LOG_LEVEL_NONE = 0;
    static final int LOG_LEVEL_PERFORMANCE_MEASUREMENT = 1;
    static final int LOG_LEVEL_SHOW_WARNING = 2;
    static final int LOG_LEVEL_VERBOSE = 3;
    public static final int NAME_ORDER_DEFAULT = 0;
    public static final int NAME_ORDER_EUROPE = 4;
    public static final int NAME_ORDER_JAPANESE = 8;
    private static final int NAME_ORDER_MASK = 12;
    private static final Map<String, Integer> VCARD_TYPES_MAP = new HashMap();
    public static int VCARD_TYPE_DEFAULT = VCARD_TYPE_V21_GENERIC;
    public static final int VCARD_TYPE_DOCOMO = 536870952;
    private static final String VCARD_TYPE_DOCOMO_STR = "docomo";
    public static final int VCARD_TYPE_V21_EUROPE = -1073741820;
    static final String VCARD_TYPE_V21_EUROPE_STR = "v21_europe";
    public static final int VCARD_TYPE_V21_GENERIC = -805306368;
    static String VCARD_TYPE_V21_GENERIC_STR = "v21_generic";
    public static final int VCARD_TYPE_V21_JAPANESE = -1073741784;
    static final String VCARD_TYPE_V21_JAPANESE_STR = "v21_japanese";
    public static final int VCARD_TYPE_V21_JAPANESE_UTF8 = -1073741816;
    static final String VCARD_TYPE_V21_JAPANESE_UTF8_STR = "v21_japanese_utf8";
    public static final int VCARD_TYPE_V30_EUROPE = -1073741819;
    static final String VCARD_TYPE_V30_EUROPE_STR = "v30_europe";
    public static final int VCARD_TYPE_V30_GENERIC = -805306367;
    static final String VCARD_TYPE_V30_GENERIC_STR = "v30_generic";
    public static final int VCARD_TYPE_V30_JAPANESE = -1073741783;
    static final String VCARD_TYPE_V30_JAPANESE_STR = "v30_japanese";
    public static final int VCARD_TYPE_V30_JAPANESE_UTF8 = -1073741815;
    static final String VCARD_TYPE_V30_JAPANESE_UTF8_STR = "v30_japanese_utf8";

    static {
        VCARD_TYPES_MAP.put(VCARD_TYPE_V21_GENERIC_STR, Integer.valueOf((int) VCARD_TYPE_V21_GENERIC));
        VCARD_TYPES_MAP.put(VCARD_TYPE_V30_GENERIC_STR, Integer.valueOf((int) VCARD_TYPE_V30_GENERIC));
        VCARD_TYPES_MAP.put(VCARD_TYPE_V21_EUROPE_STR, Integer.valueOf((int) VCARD_TYPE_V21_EUROPE));
        VCARD_TYPES_MAP.put(VCARD_TYPE_V30_EUROPE_STR, Integer.valueOf((int) VCARD_TYPE_V30_EUROPE));
        VCARD_TYPES_MAP.put(VCARD_TYPE_V21_JAPANESE_STR, Integer.valueOf((int) VCARD_TYPE_V21_JAPANESE));
        VCARD_TYPES_MAP.put(VCARD_TYPE_V21_JAPANESE_UTF8_STR, Integer.valueOf((int) VCARD_TYPE_V21_JAPANESE_UTF8));
        VCARD_TYPES_MAP.put(VCARD_TYPE_V30_JAPANESE_STR, Integer.valueOf((int) VCARD_TYPE_V30_JAPANESE));
        VCARD_TYPES_MAP.put(VCARD_TYPE_V30_JAPANESE_UTF8_STR, Integer.valueOf((int) VCARD_TYPE_V30_JAPANESE_UTF8));
        VCARD_TYPES_MAP.put("docomo", Integer.valueOf((int) VCARD_TYPE_DOCOMO));
    }

    public static int getVCardTypeFromString(String vcardTypeString) {
        String loweredKey = vcardTypeString.toLowerCase();
        if (VCARD_TYPES_MAP.containsKey(loweredKey)) {
            return VCARD_TYPES_MAP.get(loweredKey).intValue();
        }
        return VCARD_TYPE_DEFAULT;
    }

    public static boolean isV30(int vcardType) {
        return (vcardType & 1) != 0;
    }

    public static boolean usesQuotedPrintable(int vcardType) {
        return !isV30(vcardType);
    }

    public static boolean isDoCoMo(int vcardType) {
        return (FLAG_DOCOMO & vcardType) != 0;
    }

    public static boolean isJapaneseDevice(int vcardType) {
        return vcardType == -1073741784 || vcardType == -1073741816 || vcardType == -1073741783 || vcardType == -1073741815 || vcardType == 536870952;
    }

    public static boolean usesUtf8(int vcardType) {
        return 0 != 0;
    }

    public static boolean usesShiftJis(int vcardType) {
        return (vcardType & 32) != 0;
    }

    public static boolean needsToConvertPhoneticString(int vcardType) {
        return vcardType == 536870952;
    }

    public static int getNameOrderType(int vcardType) {
        return vcardType & 12;
    }

    public static boolean usesAndroidSpecificProperty(int vcardType) {
        return (FLAG_USE_ANDROID_PROPERTY & vcardType) != 0;
    }

    public static boolean usesDefactProperty(int vcardType) {
        return (FLAG_USE_DEFACT_PROPERTY & vcardType) != 0;
    }

    public static boolean onlyOneNoteFieldIsAvailable(int vcardType) {
        return vcardType == 536870952;
    }

    public static boolean showPerformanceLog() {
        return false;
    }

    public static boolean usesQPToPrimaryProperties(int vcardType) {
        return usesQuotedPrintable(vcardType) && (268435456 & vcardType) != 0;
    }

    private VCardConfig() {
    }
}
