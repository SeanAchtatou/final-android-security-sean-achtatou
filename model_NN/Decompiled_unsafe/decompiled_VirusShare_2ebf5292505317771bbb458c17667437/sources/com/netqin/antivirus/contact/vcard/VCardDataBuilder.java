package com.netqin.antivirus.contact.vcard;

import android.accounts.Account;
import com.netqin.antivirus.contact.ContactCommon;
import com.netqin.antivirus.contact.vcard.ContactStruct;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.net.QuotedPrintableCodec;

public class VCardDataBuilder implements VCardBuilder {
    private static String LOG_TAG = "VCardDataBuilder";
    public static String TARGET_CHARSET = StringEncodings.UTF8;
    private final Account mAccount;
    private ContactStruct mCurrentContactStruct;
    private ContactStruct.Property mCurrentProperty;
    private List<EntryHandler> mEntryHandlers;
    private String mParamType;
    private String mSourceCharset;
    private boolean mStrictLineBreakParsing;
    private String mTargetCharset;
    private long mTimePushIntoContentResolver;
    private final int mVCardType;

    public VCardDataBuilder() {
        this(null, null, false, VCardConfig.VCARD_TYPE_V21_GENERIC, null);
    }

    public VCardDataBuilder(int vcardType) {
        this(null, null, false, vcardType, null);
    }

    public VCardDataBuilder(String charset, boolean strictLineBreakParsing, int vcardType, Account account) {
        this(null, charset, strictLineBreakParsing, vcardType, account);
    }

    public VCardDataBuilder(String sourceCharset, String targetCharset, boolean strictLineBreakParsing, int vcardType, Account account) {
        this.mCurrentProperty = new ContactStruct.Property();
        this.mEntryHandlers = new ArrayList();
        if (sourceCharset != null) {
            this.mSourceCharset = sourceCharset;
        } else {
            this.mSourceCharset = VCardConfig.DEFAULT_CHARSET;
        }
        if (targetCharset != null) {
            this.mTargetCharset = targetCharset;
        } else {
            this.mTargetCharset = TARGET_CHARSET;
        }
        this.mStrictLineBreakParsing = strictLineBreakParsing;
        this.mVCardType = vcardType;
        this.mAccount = account;
    }

    public VCardDataBuilder(String sourceCharset, String targetCharset, boolean strictLineBreakParsing, int vcardType) {
        this.mCurrentProperty = new ContactStruct.Property();
        this.mEntryHandlers = new ArrayList();
        if (sourceCharset != null) {
            this.mSourceCharset = sourceCharset;
        } else {
            this.mSourceCharset = VCardConfig.DEFAULT_CHARSET;
        }
        if (targetCharset != null) {
            this.mTargetCharset = targetCharset;
        } else {
            this.mTargetCharset = TARGET_CHARSET;
        }
        this.mStrictLineBreakParsing = strictLineBreakParsing;
        this.mVCardType = vcardType;
        this.mAccount = null;
    }

    public void addEntryHandler(EntryHandler entryHandler) {
        this.mEntryHandlers.add(entryHandler);
    }

    public void start() {
        for (EntryHandler entryHandler : this.mEntryHandlers) {
            entryHandler.onParsingStart();
        }
    }

    public void end() {
        for (EntryHandler entryHandler : this.mEntryHandlers) {
            entryHandler.onParsingEnd();
        }
    }

    public void startRecord(String type) {
        type.equalsIgnoreCase("VCARD");
        this.mCurrentContactStruct = new ContactStruct(this.mVCardType, this.mAccount);
    }

    public void endRecord() {
        this.mCurrentContactStruct.consolidateFields();
        for (EntryHandler entryHandler : this.mEntryHandlers) {
            entryHandler.onEntryCreated(this.mCurrentContactStruct);
        }
        this.mCurrentContactStruct = null;
    }

    public void startProperty() {
        this.mCurrentProperty.clear();
    }

    public void endProperty() {
        this.mCurrentContactStruct.addProperty(this.mCurrentProperty);
    }

    public void propertyName(String name) {
        this.mCurrentProperty.setPropertyName(name);
    }

    public void propertyGroup(String group) {
    }

    public void propertyParamType(String type) {
        this.mParamType = type;
    }

    public void propertyParamValue(String value) {
        if (this.mParamType == null) {
            this.mParamType = Constants.ATTR_TYPE;
        }
        this.mCurrentProperty.addParameter(this.mParamType, value);
        this.mParamType = null;
    }

    private String encodeString(String originalString, String targetCharset) {
        if (this.mSourceCharset.equalsIgnoreCase(targetCharset)) {
            return originalString;
        }
        ByteBuffer byteBuffer = Charset.forName(this.mSourceCharset).encode(originalString);
        byte[] bytes = new byte[byteBuffer.remaining()];
        byteBuffer.get(bytes);
        try {
            return new String(bytes, targetCharset);
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    private String handleOneValue(String value, String targetCharset, String encoding) {
        String[] lines;
        byte[] bytes;
        if (encoding != null) {
            if (encoding.equals("BASE64") || encoding.equals("B")) {
                this.mCurrentProperty.setPropertyBytes(Base64.decodeBase64(value.getBytes()));
                return value;
            } else if (encoding.equals("QUOTED-PRINTABLE")) {
                StringBuilder builder = new StringBuilder();
                int length = value.length();
                int i = 0;
                while (i < length) {
                    char ch = value.charAt(i);
                    if (ch == '=' && i < length - 1) {
                        char nextCh = value.charAt(i + 1);
                        if (nextCh == ' ' || nextCh == 9) {
                            builder.append(nextCh);
                            i++;
                            i++;
                        }
                    }
                    builder.append(ch);
                    i++;
                }
                String quotedPrintable = builder.toString();
                if (this.mStrictLineBreakParsing) {
                    lines = quotedPrintable.split("\r\n");
                } else {
                    StringBuilder builder2 = new StringBuilder();
                    int length2 = quotedPrintable.length();
                    ArrayList<String> list = new ArrayList<>();
                    int i2 = 0;
                    while (i2 < length2) {
                        char ch2 = quotedPrintable.charAt(i2);
                        if (ch2 == 10) {
                            list.add(builder2.toString());
                            builder2 = new StringBuilder();
                        } else if (ch2 == 13) {
                            list.add(builder2.toString());
                            builder2 = new StringBuilder();
                            if (i2 < length2 - 1 && quotedPrintable.charAt(i2 + 1) == 10) {
                                i2++;
                            }
                        } else {
                            builder2.append(ch2);
                        }
                        i2++;
                    }
                    String finalLine = builder2.toString();
                    if (finalLine.length() > 0) {
                        list.add(finalLine);
                    }
                    lines = (String[]) list.toArray(new String[0]);
                }
                StringBuilder builder3 = new StringBuilder();
                int length3 = lines.length;
                for (int i3 = 0; i3 < length3; i3++) {
                    String line = lines[i3];
                    if (line.endsWith("=")) {
                        line = line.substring(0, line.length() - 1);
                    }
                    builder3.append(line);
                }
                try {
                    bytes = builder3.toString().getBytes(this.mSourceCharset);
                } catch (UnsupportedEncodingException e) {
                    bytes = builder3.toString().getBytes();
                }
                try {
                    byte[] bytes2 = QuotedPrintableCodec.decodeQuotedPrintable(bytes);
                    try {
                        return ContactCommon.replaceLineMarkWithN(new String(bytes2, targetCharset));
                    } catch (UnsupportedEncodingException e2) {
                        return new String(bytes2);
                    }
                } catch (DecoderException e3) {
                    return "";
                }
            }
        }
        return encodeString(value, targetCharset);
    }

    public void propertyValues(List<String> values) {
        String charset;
        String encoding;
        if (values != null && values.size() != 0) {
            Collection<String> charsetCollection = this.mCurrentProperty.getParameters("CHARSET");
            if (charsetCollection != null) {
                charset = charsetCollection.iterator().next();
            } else {
                charset = null;
            }
            String targetCharset = CharsetUtils.nameForDefaultVendor(charset);
            Collection<String> encodingCollection = this.mCurrentProperty.getParameters("ENCODING");
            if (encodingCollection != null) {
                encoding = encodingCollection.iterator().next();
            } else {
                encoding = null;
            }
            if (targetCharset == null || targetCharset.length() == 0) {
                targetCharset = this.mTargetCharset;
            }
            for (String value : values) {
                this.mCurrentProperty.addToPropertyValueList(handleOneValue(value, targetCharset, encoding));
            }
        }
    }

    public void showPerformanceInfo() {
    }
}
