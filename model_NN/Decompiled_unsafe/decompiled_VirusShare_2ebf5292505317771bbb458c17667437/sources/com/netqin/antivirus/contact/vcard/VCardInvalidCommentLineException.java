package com.netqin.antivirus.contact.vcard;

public class VCardInvalidCommentLineException extends VCardInvalidLineException {
    public VCardInvalidCommentLineException() {
    }

    public VCardInvalidCommentLineException(String message) {
        super(message);
    }
}
