package com.netqin.antivirus.contact;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.widget.Toast;
import com.netqin.antivirus.antilost.AntiLostCommon;
import com.netqin.antivirus.common.CommonDefine;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.common.ProgDlgActivity;
import com.netqin.antivirus.common.ProgressTextDialog;
import com.netqin.antivirus.log.LogEngine;
import com.nqmobile.antivirus_ampro20.R;

public class CardBackupDoing extends ProgDlgActivity {
    public static boolean mFromContactBackupGuide = false;
    private static String mMessageBoxText = "";
    private String mBackupFilePathSucc = "";
    /* access modifiers changed from: private */
    public String mBackupFilePathTemp = "";
    /* access modifiers changed from: private */
    public VCardExportThread mVCardExportThread = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        if (mFromContactBackupGuide) {
            mFromContactBackupGuide = false;
            mMessageBoxText = "";
            showDialog(R.string.text_is_backup_to_storage_card);
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            this.mActivityVisible = false;
            if (this.mVCardExportThread != null) {
                this.mVCardExportThread.cancel();
                this.mVCardExportThread = null;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    /* access modifiers changed from: protected */
    public void progressTextDialogOnStop() {
        if (this.mVCardExportThread != null) {
            this.mVCardExportThread.cancel();
            this.mVCardExportThread = null;
        }
        super.progressTextDialogOnStop();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        DialogInterface.OnClickListener okListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                CardBackupDoing.this.clickOk();
            }
        };
        DialogInterface.OnClickListener cancelListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                CardBackupDoing.this.clickCancel();
            }
        };
        DialogInterface.OnKeyListener keyListener = new DialogInterface.OnKeyListener() {
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode != 4) {
                    return false;
                }
                CardBackupDoing.this.clickCancel();
                return true;
            }
        };
        DialogInterface.OnClickListener succListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                CardBackupDoing.this.clickSucc();
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        switch (id) {
            case 20:
                builder.setTitle((int) R.string.label_netqin_antivirus);
                builder.setMessage(mMessageBoxText);
                builder.setPositiveButton((int) R.string.label_ok, cancelListener);
                builder.setOnKeyListener(keyListener);
                return builder.create();
            case 27:
                builder.setTitle((int) R.string.label_netqin_antivirus);
                builder.setMessage(mMessageBoxText);
                builder.setPositiveButton((int) R.string.label_ok, cancelListener);
                builder.setOnKeyListener(keyListener);
                return builder.create();
            case 30:
                builder.setTitle((int) R.string.label_backup_success_tip);
                builder.setMessage(mMessageBoxText);
                builder.setPositiveButton((int) R.string.label_ok, succListener);
                builder.setOnKeyListener(keyListener);
                return builder.create();
            case CommonDefine.MSG_PROG_ARG_ERRMSG_CONTACT_EXPORT:
                builder.setTitle((int) R.string.label_netqin_antivirus);
                builder.setMessage(mMessageBoxText);
                builder.setPositiveButton((int) R.string.label_ok, cancelListener);
                builder.setOnKeyListener(keyListener);
                return builder.create();
            case R.string.text_is_backup_to_storage_card /*2131427474*/:
                builder.setTitle((int) R.string.label_netqin_antivirus);
                builder.setMessage((int) R.string.text_is_backup_to_storage_card);
                builder.setPositiveButton((int) R.string.label_ok, okListener);
                builder.setNegativeButton((int) R.string.label_cancel, cancelListener);
                builder.setOnKeyListener(keyListener);
                return builder.create();
            default:
                return super.onCreateDialog(id);
        }
    }

    /* access modifiers changed from: protected */
    public void childFunctionCall(Message msg) {
        CommonMethod.sendUserMessage(this.mHandler, 10);
        switch (msg.arg1) {
            case CommonDefine.MSG_PROG_ARG_CANCEL:
                ContactCommon.deleteFile(this.mBackupFilePathTemp);
                clickCancel();
                Toast toast = Toast.makeText(this, (int) R.string.text_backup_cantact_cancel, 0);
                toast.setGravity(81, 0, 100);
                toast.show();
                return;
            case 27:
                LogEngine.insertOperationItemLog(15, "", getFilesDir().getPath());
                mMessageBoxText = (String) msg.obj;
                showDialog(27);
                return;
            case 30:
                this.mBackupFilePathSucc = ContactCommon.renameVCardBackupFile(this, this.mBackupFilePathTemp);
                ContactCommon.deleteFile(this.mBackupFilePathTemp);
                if (!TextUtils.isEmpty(this.mBackupFilePathSucc)) {
                    getSharedPreferences(AntiLostCommon.DELETE_CONTACT, 0).edit().putString("contacts_storagecard", Integer.toString(msg.arg2)).commit();
                    saveBackupSuccInfo();
                    mMessageBoxText = getString(R.string.text_backup_contact_to_card_succ_result, new Object[]{Integer.valueOf(msg.arg2), this.mBackupFilePathSucc});
                } else {
                    mMessageBoxText = getString(R.string.text_backup_contact_fail);
                }
                showDialog(30);
                return;
            case CommonDefine.MSG_PROG_ARG_ERRMSG_CONTACT_EXPORT:
                LogEngine.insertOperationItemLog(21, "", getFilesDir().getPath());
                mMessageBoxText = (String) msg.obj;
                showDialog(34);
                return;
            case CommonDefine.MSG_PROG_ARG_CANCEL_SERVER:
                return;
            default:
                mMessageBoxText = getString(R.string.text_unprocess_message, new Object[]{Integer.valueOf(msg.arg1)});
                showDialog(20);
                return;
        }
    }

    /* access modifiers changed from: private */
    public void clickOk() {
        this.mBackupFilePathTemp = ContactCommon.getSDCardFilePathTemp(this);
        if (TextUtils.isEmpty(this.mBackupFilePathTemp)) {
            CommonMethod.messageDialog(this, (int) R.string.text_because_nosdcard, (int) R.string.label_netqin_antivirus, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    CardBackupDoing.this.clickCancel();
                }
            }, new DialogInterface.OnKeyListener() {
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    if (keyCode != 4) {
                        return false;
                    }
                    CardBackupDoing.this.clickCancel();
                    return true;
                }
            });
            return;
        }
        this.mProgTextDialog = new ProgressTextDialog(this, R.string.text_backuping_contact_to_card, this.mHandler);
        this.mProgTextDialog.setAnimationState(0);
        this.mProgTextDialog.setAnimationImageSourceFrom(R.drawable.animation_mobile);
        this.mProgTextDialog.setAnimationImageSourceTo(R.drawable.animation_card);
        this.mHandler.post(new Runnable() {
            public void run() {
                CardBackupDoing.this.mVCardExportThread = new VCardExportThread(CardBackupDoing.this.mBackupFilePathTemp, CardBackupDoing.this, CardBackupDoing.this.mHandler, CardBackupDoing.this.mProgTextDialog);
                CardBackupDoing.this.mVCardExportThread.start();
            }
        });
    }

    /* access modifiers changed from: private */
    public void clickCancel() {
        finish();
    }

    private void saveBackupSuccInfo() {
        getSharedPreferences(AntiLostCommon.DELETE_CONTACT, 0).edit().putString("bpfile_card", this.mBackupFilePathSucc).commit();
        LogEngine.insertOperationItemLog(14, "", getFilesDir().getPath());
    }

    /* access modifiers changed from: private */
    public void clickSucc() {
        finish();
    }
}
