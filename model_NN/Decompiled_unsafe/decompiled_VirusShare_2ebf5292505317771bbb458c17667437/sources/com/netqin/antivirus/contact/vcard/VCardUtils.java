package com.netqin.antivirus.contact.vcard;

import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.text.TextUtils;
import com.netqin.antivirus.antilost.ContactsHandler;
import com.netqin.antivirus.contact.vcard.ContactData;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class VCardUtils {
    private static final char[] ENCODE64 = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};
    private static char PAD = '=';
    private static final Map<Integer, String> sKnownPhoneTypesMap_ItoS = new HashMap();
    private static final Map<String, Integer> sKnownPhoneTypesMap_StoI = new HashMap();
    private static final Set<String> sPhoneTypesSetUnknownToContacts = new HashSet();

    static {
        sKnownPhoneTypesMap_ItoS.put(9, Constants.ATTR_TYPE_CAR);
        sKnownPhoneTypesMap_StoI.put(Constants.ATTR_TYPE_CAR, 9);
        sKnownPhoneTypesMap_ItoS.put(6, Constants.ATTR_TYPE_PAGER);
        sKnownPhoneTypesMap_StoI.put(Constants.ATTR_TYPE_PAGER, 6);
        sKnownPhoneTypesMap_ItoS.put(11, Constants.ATTR_TYPE_ISDN);
        sKnownPhoneTypesMap_StoI.put(Constants.ATTR_TYPE_ISDN, 11);
        sKnownPhoneTypesMap_StoI.put(Constants.ATTR_TYPE_HOME, 1);
        sKnownPhoneTypesMap_StoI.put(Constants.ATTR_TYPE_WORK, 3);
        sKnownPhoneTypesMap_StoI.put(Constants.ATTR_TYPE_CELL, 2);
        sKnownPhoneTypesMap_StoI.put(Constants.ATTR_TYPE_PHONE_EXTRA_OTHER, 7);
        sKnownPhoneTypesMap_StoI.put(Constants.ATTR_TYPE_PHONE_EXTRA_CALLBACK, 8);
        sKnownPhoneTypesMap_StoI.put(Constants.ATTR_TYPE_PHONE_EXTRA_COMPANY_MAIN, 10);
        sKnownPhoneTypesMap_StoI.put(Constants.ATTR_TYPE_PHONE_EXTRA_RADIO, 14);
        sKnownPhoneTypesMap_StoI.put(Constants.ATTR_TYPE_PHONE_EXTRA_TELEX, 15);
        sKnownPhoneTypesMap_StoI.put(Constants.ATTR_TYPE_PHONE_EXTRA_TTY_TDD, 16);
        sKnownPhoneTypesMap_StoI.put(Constants.ATTR_TYPE_PHONE_EXTRA_ASSISTANT, 19);
        sPhoneTypesSetUnknownToContacts.add(Constants.ATTR_TYPE_MODEM);
        sPhoneTypesSetUnknownToContacts.add(Constants.ATTR_TYPE_MSG);
        sPhoneTypesSetUnknownToContacts.add(Constants.ATTR_TYPE_BBS);
        sPhoneTypesSetUnknownToContacts.add(Constants.ATTR_TYPE_VIDEO);
    }

    public static String getPhoneAttributeString(Integer type) {
        return sKnownPhoneTypesMap_ItoS.get(type);
    }

    public static Object getPhoneTypeFromStrings(Collection<String> types) {
        int type = -1;
        String label = null;
        boolean isFax = false;
        boolean hasPref = false;
        if (types != null) {
            for (String typeString : types) {
                String typeString2 = typeString.toUpperCase();
                if (typeString2.equals(Constants.ATTR_TYPE_PREF)) {
                    hasPref = true;
                } else if (typeString2.equals(Constants.ATTR_TYPE_FAX)) {
                    isFax = true;
                } else {
                    if (typeString2.startsWith("X-") && type < 0) {
                        typeString2 = typeString2.substring(2);
                    }
                    Integer tmp = sKnownPhoneTypesMap_StoI.get(typeString2);
                    if (tmp != null) {
                        type = tmp.intValue();
                    } else if (type < 0) {
                        type = 0;
                        label = typeString2;
                    }
                }
            }
        }
        if (type < 0) {
            if (hasPref) {
                type = 12;
            } else {
                type = 1;
            }
        }
        if (isFax) {
            if (type == 1) {
                type = 5;
            } else if (type == 3) {
                type = 4;
            } else if (type == 7) {
                type = 13;
            }
        }
        if (type == 0) {
            return label;
        }
        return Integer.valueOf(type);
    }

    public static boolean isValidPhoneAttribute(String phoneAttribute, int vcardType) {
        return phoneAttribute.startsWith("X-") || phoneAttribute.startsWith("x-") || sPhoneTypesSetUnknownToContacts.contains(phoneAttribute);
    }

    public static String[] sortNameElements(int vcardType, String familyName, String middleName, String givenName) {
        String[] list = new String[3];
        switch (VCardConfig.getNameOrderType(vcardType)) {
            case 4:
                list[0] = middleName;
                list[1] = givenName;
                list[2] = familyName;
                break;
            case 8:
                list[0] = familyName;
                list[1] = middleName;
                list[2] = givenName;
                break;
            default:
                list[0] = givenName;
                list[1] = middleName;
                list[2] = familyName;
                break;
        }
        return list;
    }

    public static int getPhoneNumberFormat(int vcardType) {
        if (VCardConfig.isJapaneseDevice(vcardType)) {
            return 2;
        }
        return 1;
    }

    public static void insertStructuredPostalDataUsingContactsStruct(int vcardType, ContentProviderOperation.Builder builder, ContactData.PostalData postalData) {
        builder.withValueBackReference("raw_contact_id", 0);
        builder.withValue("mimetype", "vnd.android.cursor.item/postal-address_v2");
        builder.withValue("data2", Integer.valueOf(postalData.type));
        if (postalData.type == 0) {
            builder.withValue("data3", postalData.label);
        }
        builder.withValue("data5", postalData.pobox);
        builder.withValue("data4", postalData.street);
        builder.withValue("data7", postalData.localty);
        builder.withValue("data8", postalData.region);
        builder.withValue("data9", postalData.postalCode);
        builder.withValue("data10", postalData.country);
        builder.withValue(ContactsHandler.PHONE_NUMBER, postalData.getFormattedAddress(vcardType));
        if (postalData.isPrimary) {
            builder.withValue("is_primary", 1);
        }
    }

    public static void insertStructuredPostalDataUsingContactsStruct(int vcardType, ContentProviderOperation.Builder builder, ContactData.PostalData postalData, long rawContactId, boolean first) {
        builder.withValue("raw_contact_id", Long.valueOf(rawContactId));
        builder.withValue("mimetype", "vnd.android.cursor.item/postal-address_v2");
        builder.withValue("data2", Integer.valueOf(postalData.type));
        if (postalData.type == 0) {
            builder.withValue("data3", postalData.label);
        }
        builder.withValue("data5", postalData.pobox);
        builder.withValue("data6", postalData.neighborhood);
        builder.withValue("data4", postalData.street);
        builder.withValue("data7", postalData.localty);
        builder.withValue("data8", postalData.region);
        builder.withValue("data9", postalData.postalCode);
        builder.withValue("data10", postalData.country);
        builder.withValue(ContactsHandler.PHONE_NUMBER, postalData.getFormattedAddress(vcardType));
        if (postalData.isPrimary && first) {
            builder.withValue("is_primary", 1);
        }
    }

    public static String[] getVCardPostalElements(ContentValues contentValues) {
        String[] dataArray = new String[7];
        dataArray[0] = contentValues.getAsString("data5");
        if (dataArray[0] == null) {
            dataArray[0] = "";
        }
        dataArray[1] = contentValues.getAsString("data6");
        if (dataArray[1] == null) {
            dataArray[1] = "";
        }
        dataArray[2] = contentValues.getAsString("data4");
        if (dataArray[2] == null) {
            dataArray[2] = "";
        }
        dataArray[3] = contentValues.getAsString("data7");
        if (dataArray[3] == null) {
            dataArray[3] = "";
        }
        String region = contentValues.getAsString("data8");
        if (!TextUtils.isEmpty(region)) {
            dataArray[4] = region;
        } else {
            dataArray[4] = "";
        }
        dataArray[5] = contentValues.getAsString("data9");
        if (dataArray[5] == null) {
            dataArray[5] = "";
        }
        dataArray[6] = contentValues.getAsString("data10");
        if (dataArray[6] == null) {
            dataArray[6] = "";
        }
        return dataArray;
    }

    public static String constructNameFromElements(int nameOrderType, String familyName, String middleName, String givenName) {
        return constructNameFromElements(nameOrderType, familyName, middleName, givenName, null, null);
    }

    public static String constructNameFromElements(int nameOrderType, String familyName, String middleName, String givenName, String prefix, String suffix) {
        StringBuilder builder = new StringBuilder();
        String[] nameList = sortNameElements(nameOrderType, familyName, middleName, givenName);
        boolean first = true;
        if (!TextUtils.isEmpty(prefix)) {
            first = false;
            builder.append(prefix);
        }
        for (String namePart : nameList) {
            if (!TextUtils.isEmpty(namePart)) {
                if (first) {
                    first = false;
                } else {
                    builder.append(' ');
                }
                builder.append(namePart);
            }
        }
        if (!TextUtils.isEmpty(suffix)) {
            if (!first) {
                builder.append(' ');
            }
            builder.append(suffix);
        }
        return builder.toString();
    }

    public static boolean containsOnlyPrintableAscii(String str) {
        if (TextUtils.isEmpty(str)) {
            return true;
        }
        int length = str.length();
        int i = 0;
        while (i < length) {
            int c = str.codePointAt(i);
            if (c < 32 || 294 < c) {
                return false;
            }
            i = str.offsetByCodePoints(i, 1);
        }
        return true;
    }

    public static boolean containsOnlyNonCrLfPrintableAscii(String str) {
        if (TextUtils.isEmpty(str)) {
            return true;
        }
        int length = str.length();
        int i = 0;
        while (i < length) {
            int c = str.codePointAt(i);
            if (c < 32 || 294 < c || c == 10 || c == 13) {
                return false;
            }
            i = str.offsetByCodePoints(i, 1);
        }
        return true;
    }

    public static boolean containsOnlyAlphaDigitHyphen(String str) {
        if (TextUtils.isEmpty(str)) {
            return true;
        }
        int length = str.length();
        int i = 0;
        while (i < length) {
            int codepoint = str.codePointAt(i);
            if ((65 > codepoint || codepoint >= 91) && ((97 > codepoint || codepoint >= 123) && ((48 > codepoint || codepoint >= 57) && codepoint != 45))) {
                return false;
            }
            i = str.offsetByCodePoints(i, 1);
        }
        return true;
    }

    public static String encodeBase64(byte[] data) {
        if (data == null) {
            return "";
        }
        char[] charBuffer = new char[(((data.length + 2) / 3) * 4)];
        int position = 0;
        for (int i = 0; i < data.length - 2; i += 3) {
            int _3byte = ((data[i] & 255) << 16) + ((data[i + 1] & 255) << 8) + (data[i + 2] & 255);
            int position2 = position + 1;
            charBuffer[position] = ENCODE64[_3byte >> 18];
            int position3 = position2 + 1;
            charBuffer[position2] = ENCODE64[(_3byte >> 12) & 63];
            int position4 = position3 + 1;
            charBuffer[position3] = ENCODE64[(_3byte >> 6) & 63];
            position = position4 + 1;
            charBuffer[position4] = ENCODE64[_3byte & 63];
        }
        switch (data.length % 3) {
            case 1:
                int _3byte2 = (data[data.length - 1] & 255) << 16;
                int position5 = position + 1;
                charBuffer[position] = ENCODE64[_3byte2 >> 18];
                int position6 = position5 + 1;
                charBuffer[position5] = ENCODE64[(_3byte2 >> 12) & 63];
                int position7 = position6 + 1;
                charBuffer[position6] = PAD;
                int i2 = position7 + 1;
                charBuffer[position7] = PAD;
                break;
            case 2:
                int _3byte3 = ((data[data.length - 2] & 255) << 16) + ((data[data.length - 1] & 255) << 8);
                int position8 = position + 1;
                charBuffer[position] = ENCODE64[_3byte3 >> 18];
                int position9 = position8 + 1;
                charBuffer[position8] = ENCODE64[(_3byte3 >> 12) & 63];
                int position10 = position9 + 1;
                charBuffer[position9] = ENCODE64[(_3byte3 >> 6) & 63];
                int i3 = position10 + 1;
                charBuffer[position10] = PAD;
                break;
        }
        return new String(charBuffer);
    }

    public static String toHalfWidthString(String orgString) {
        if (TextUtils.isEmpty(orgString)) {
            return null;
        }
        StringBuilder builder = new StringBuilder();
        int length = orgString.length();
        for (int i = 0; i < length; i++) {
            char ch = orgString.charAt(i);
            CharSequence halfWidthText = JapaneseUtils.tryGetHalfWidthText(ch);
            if (halfWidthText != null) {
                builder.append(halfWidthText);
            } else {
                builder.append(ch);
            }
        }
        return builder.toString();
    }

    private VCardUtils() {
    }
}
