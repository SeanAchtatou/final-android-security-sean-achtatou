package com.netqin.antivirus.contact.vcard;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.text.TextUtils;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.contact.vcard.ContactData;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VCardContact {
    private static final String LOG_TAG_1 = "vcardcontact";
    public static final int PHONE_VALID_LENG = 8;
    private final ContentResolver mContentResolver;

    public VCardContact(ContentResolver cr) {
        this.mContentResolver = cr;
    }

    public int queryContactItemsString(Map<String, List<ContentValues>> contValuesListMap, String key, Uri uri, String mimeType, String[] projection, String contId) {
        long currentTimeMillis = System.currentTimeMillis();
        int sum = 0;
        try {
            Cursor cursor = this.mContentResolver.query(uri, projection, "contact_id=? AND mimetype='" + mimeType + "'", new String[]{contId}, null);
            if (cursor != null) {
                int count = cursor.getCount();
                if (count > 0) {
                    List<ContentValues> contentValuesList = contValuesListMap.get(key);
                    if (contentValuesList == null) {
                        contentValuesList = new ArrayList<>();
                        contValuesListMap.put(key, contentValuesList);
                    } else {
                        contentValuesList.clear();
                    }
                    cursor.moveToFirst();
                    for (int j = 0; j < count; j++) {
                        ContentValues contentValues = new ContentValues();
                        for (int i = 0; i < projection.length; i++) {
                            String columnName = cursor.getColumnName(i);
                            contentValues.put(projection[i], cursor.getString(i));
                        }
                        contentValuesList.add(contentValues);
                        cursor.moveToNext();
                        sum++;
                    }
                }
                cursor.close();
            }
        } catch (OutOfMemoryError e) {
        } finally {
            System.currentTimeMillis();
        }
        return sum;
    }

    public int queryContactItems(Map<String, List<ContentValues>> contValuesListMap, String contId) {
        long currentTimeMillis = System.currentTimeMillis();
        String[] projection = ContactData.PROJECTION_EXPORT;
        int sum = 0;
        int projleng = projection.length;
        String[] selectionArgs = {contId};
        String mimeType = "";
        try {
            List<ContentValues> conValListStructName = contValuesListMap.get("vnd.android.cursor.item/name");
            if (conValListStructName == null) {
                conValListStructName = new ArrayList<>();
                contValuesListMap.put("vnd.android.cursor.item/name", conValListStructName);
            } else {
                conValListStructName.clear();
            }
            List<ContentValues> conValListNickname = contValuesListMap.get("vnd.android.cursor.item/nickname");
            if (conValListNickname == null) {
                conValListNickname = new ArrayList<>();
                contValuesListMap.put("vnd.android.cursor.item/nickname", conValListNickname);
            } else {
                conValListNickname.clear();
            }
            List<ContentValues> conValListPhone = contValuesListMap.get("vnd.android.cursor.item/phone_v2");
            if (conValListPhone == null) {
                conValListPhone = new ArrayList<>();
                contValuesListMap.put("vnd.android.cursor.item/phone_v2", conValListPhone);
            } else {
                conValListPhone.clear();
            }
            List<ContentValues> conValListStructPostal = contValuesListMap.get("vnd.android.cursor.item/postal-address_v2");
            if (conValListStructPostal == null) {
                conValListStructPostal = new ArrayList<>();
                contValuesListMap.put("vnd.android.cursor.item/postal-address_v2", conValListStructPostal);
            } else {
                conValListStructPostal.clear();
            }
            List<ContentValues> conValListEmail = contValuesListMap.get("vnd.android.cursor.item/email_v2");
            if (conValListEmail == null) {
                conValListEmail = new ArrayList<>();
                contValuesListMap.put("vnd.android.cursor.item/email_v2", conValListEmail);
            } else {
                conValListEmail.clear();
            }
            List<ContentValues> conValListIm = contValuesListMap.get("vnd.android.cursor.item/im");
            if (conValListIm == null) {
                conValListIm = new ArrayList<>();
                contValuesListMap.put("vnd.android.cursor.item/im", conValListIm);
            } else {
                conValListIm.clear();
            }
            List<ContentValues> conValListWebsite = contValuesListMap.get("vnd.android.cursor.item/website");
            if (conValListWebsite == null) {
                conValListWebsite = new ArrayList<>();
                contValuesListMap.put("vnd.android.cursor.item/website", conValListWebsite);
            } else {
                conValListWebsite.clear();
            }
            List<ContentValues> conValListEvent = contValuesListMap.get("vnd.android.cursor.item/contact_event");
            if (conValListEvent == null) {
                conValListEvent = new ArrayList<>();
                contValuesListMap.put("vnd.android.cursor.item/contact_event", conValListEvent);
            } else {
                conValListEvent.clear();
            }
            List<ContentValues> conValListOrgan = contValuesListMap.get("vnd.android.cursor.item/organization");
            if (conValListOrgan == null) {
                conValListOrgan = new ArrayList<>();
                contValuesListMap.put("vnd.android.cursor.item/organization", conValListOrgan);
            } else {
                conValListOrgan.clear();
            }
            List<ContentValues> conValListPhoto = contValuesListMap.get("vnd.android.cursor.item/photo");
            if (conValListPhoto == null) {
                conValListPhoto = new ArrayList<>();
                contValuesListMap.put("vnd.android.cursor.item/photo", conValListPhoto);
            } else {
                conValListPhoto.clear();
            }
            List<ContentValues> conValListNote = contValuesListMap.get("vnd.android.cursor.item/note");
            if (conValListNote == null) {
                conValListNote = new ArrayList<>();
                contValuesListMap.put("vnd.android.cursor.item/note", conValListNote);
            } else {
                conValListNote.clear();
            }
            Cursor cursor = this.mContentResolver.query(ContactsContract.Data.CONTENT_URI, projection, "contact_id=?", selectionArgs, null);
            if (cursor != null) {
                int count = cursor.getCount();
                if (count > 0) {
                    cursor.moveToFirst();
                    for (int j = 0; j < count; j++) {
                        ContentValues contentValues = new ContentValues();
                        for (int i = 0; i < projleng; i++) {
                            if (i == 0) {
                                mimeType = cursor.getString(i);
                                contentValues.put(projection[i], mimeType);
                            } else if (TextUtils.isEmpty(mimeType) || !mimeType.equalsIgnoreCase("vnd.android.cursor.item/photo") || i != 20) {
                                contentValues.put(projection[i], cursor.getString(i));
                            } else {
                                contentValues.put(projection[i], cursor.getBlob(i));
                            }
                        }
                        if (!TextUtils.isEmpty(mimeType)) {
                            if (mimeType.equalsIgnoreCase("vnd.android.cursor.item/name")) {
                                conValListStructName.add(contentValues);
                            } else if (mimeType.equalsIgnoreCase("vnd.android.cursor.item/nickname")) {
                                conValListNickname.add(contentValues);
                            } else if (mimeType.equalsIgnoreCase("vnd.android.cursor.item/phone_v2")) {
                                conValListPhone.add(contentValues);
                            } else if (mimeType.equalsIgnoreCase("vnd.android.cursor.item/postal-address_v2")) {
                                conValListStructPostal.add(contentValues);
                            } else if (mimeType.equalsIgnoreCase("vnd.android.cursor.item/email_v2")) {
                                conValListEmail.add(contentValues);
                            } else if (mimeType.equalsIgnoreCase("vnd.android.cursor.item/im")) {
                                conValListIm.add(contentValues);
                            } else if (mimeType.equalsIgnoreCase("vnd.android.cursor.item/website")) {
                                conValListWebsite.add(contentValues);
                            } else if (mimeType.equalsIgnoreCase("vnd.android.cursor.item/contact_event")) {
                                conValListEvent.add(contentValues);
                            } else if (mimeType.equalsIgnoreCase("vnd.android.cursor.item/organization")) {
                                conValListOrgan.add(contentValues);
                            } else if (mimeType.equalsIgnoreCase("vnd.android.cursor.item/photo")) {
                                conValListPhoto.add(contentValues);
                            } else if (mimeType.equalsIgnoreCase("vnd.android.cursor.item/note")) {
                                conValListNote.add(contentValues);
                            }
                        }
                        cursor.moveToNext();
                        sum++;
                    }
                }
                cursor.close();
            }
        } catch (OutOfMemoryError e) {
        } finally {
            System.currentTimeMillis();
        }
        return sum;
    }

    public int queryContactItemsPhoto(Map<String, List<ContentValues>> contValuesListMap, String key, String contId) {
        long currentTimeMillis = System.currentTimeMillis();
        int sum = 0;
        try {
            Cursor cursor = this.mContentResolver.query(ContactsContract.Data.CONTENT_URI, ContactData.PROJECTION_PHOTO, "contact_id=? AND mimetype='vnd.android.cursor.item/photo'", new String[]{contId}, null);
            int count = cursor.getCount();
            if (count > 0) {
                List<ContentValues> contentValuesList = contValuesListMap.get(key);
                if (contentValuesList == null) {
                    contentValuesList = new ArrayList<>();
                    contValuesListMap.put(key, contentValuesList);
                } else {
                    contentValuesList.clear();
                }
                cursor.moveToFirst();
                for (int j = 0; j < count; j++) {
                    ContentValues contentValues = new ContentValues();
                    for (int i = 0; i < ContactData.PROJECTION_PHOTO.length; i++) {
                        String columnName = cursor.getColumnName(i);
                        if (i == 4) {
                            contentValues.put(ContactData.PROJECTION_PHOTO[i], cursor.getBlob(i));
                        } else {
                            contentValues.put(ContactData.PROJECTION_PHOTO[i], cursor.getString(i));
                        }
                    }
                    contentValuesList.add(contentValues);
                    cursor.moveToNext();
                    sum++;
                }
            }
            cursor.close();
        } catch (OutOfMemoryError e) {
        } finally {
            System.currentTimeMillis();
        }
        return sum;
    }

    public int queryContactDisplayName(Map<String, ContactData.IdData> idMap, String selection, String[] selectionArgs) {
        long currentTimeMillis = System.currentTimeMillis();
        int sum = 0;
        try {
            idMap.clear();
            Cursor cursor = this.mContentResolver.query(ContactsContract.Data.CONTENT_URI, ContactData.PROJECTION_DIPLAYNAME, selection, selectionArgs, ContactData.PROJECTION_DIPLAYNAME[0]);
            if (cursor != null) {
                int count = cursor.getCount();
                if (count > 0) {
                    cursor.moveToFirst();
                    for (int j = 0; j < count; j++) {
                        ContactData.IdData idData = new ContactData.IdData(cursor.getInt(0), cursor.getString(4), cursor.getString(2), cursor.getString(1));
                        idMap.put(idData.mRawId, idData);
                        cursor.moveToNext();
                        sum++;
                    }
                }
                cursor.close();
            }
        } catch (OutOfMemoryError e) {
        } finally {
            System.currentTimeMillis();
        }
        return sum;
    }

    public int queryContactStructureName(Map<String, Map<String, ContactData.StructureNameData>> idNameMap, String mapKey, String selection, String[] selectionArgs) {
        long currentTimeMillis = System.currentTimeMillis();
        int sum = 0;
        try {
            Cursor cursor = this.mContentResolver.query(ContactsContract.Data.CONTENT_URI, ContactData.PROJECTION_STRUCTUREDNAME, selection, selectionArgs, ContactData.PROJECTION_STRUCTUREDNAME[0]);
            if (cursor != null) {
                int count = cursor.getCount();
                if (count > 0) {
                    cursor.moveToFirst();
                    Map<String, ContactData.StructureNameData> structNameMap = idNameMap.get(mapKey);
                    if (structNameMap != null) {
                        structNameMap.clear();
                    } else {
                        structNameMap = new HashMap<>();
                        idNameMap.put(mapKey, structNameMap);
                    }
                    for (int j = 0; j < count; j++) {
                        structNameMap.put(cursor.getString(2), new ContactData.StructureNameData(cursor.getInt(0), cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(7), cursor.getString(8), cursor.getString(1)));
                        cursor.moveToNext();
                        sum++;
                    }
                }
                cursor.close();
            }
        } catch (OutOfMemoryError e) {
        } finally {
            System.currentTimeMillis();
        }
        return sum;
    }

    public int queryContactNickName(Map<String, Map<String, ContactData.NickNameData>> idNickMap, String mapKey, String selection, String[] selectionArgs) {
        long currentTimeMillis = System.currentTimeMillis();
        int sum = 0;
        try {
            Cursor cursor = this.mContentResolver.query(ContactsContract.Data.CONTENT_URI, ContactData.PROJECTION_NICKNAME, selection, selectionArgs, ContactData.PROJECTION_NICKNAME[0]);
            if (cursor != null) {
                int count = cursor.getCount();
                if (count > 0) {
                    cursor.moveToFirst();
                    Map<String, ContactData.NickNameData> nickMap = idNickMap.get(mapKey);
                    if (nickMap != null) {
                        nickMap.clear();
                    } else {
                        nickMap = new HashMap<>();
                        idNickMap.put(mapKey, nickMap);
                    }
                    for (int j = 0; j < count; j++) {
                        ContactData.NickNameData nickData = new ContactData.NickNameData(cursor.getInt(0), cursor.getInt(4), cursor.getString(5), cursor.getString(6));
                        nickMap.put(nickData.mTypeLabelNick, nickData);
                        cursor.moveToNext();
                        sum++;
                    }
                }
                cursor.close();
            }
        } catch (OutOfMemoryError e) {
        } finally {
            System.currentTimeMillis();
        }
        return sum;
    }

    public int queryContactPhone(Map<String, Map<String, ContactData.PhoneData>> phoneIdCutMap, String mapKey, Map<String, ContactData.PhoneData> phoneCutMap, String selection, String[] selectionArgs) {
        String phoneNUMBER;
        long currentTimeMillis = System.currentTimeMillis();
        int sum = 0;
        try {
            Cursor cursor = this.mContentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, ContactData.PROJECTION_PHONE, selection, selectionArgs, ContactData.PROJECTION_PHONE[0]);
            if (cursor != null) {
                int count = cursor.getCount();
                if (count > 0) {
                    cursor.moveToFirst();
                    Map<String, ContactData.PhoneData> phoneMap = phoneIdCutMap.get(mapKey);
                    if (phoneMap != null) {
                        phoneMap.clear();
                    } else {
                        phoneMap = new HashMap<>();
                        phoneIdCutMap.put(mapKey, phoneMap);
                    }
                    for (int j = 0; j < count; j++) {
                        String phoneNUMBER2 = cursor.getString(6);
                        if (!TextUtils.isEmpty(phoneNUMBER2)) {
                            int id = cursor.getInt(0);
                            int phoneTYPE = cursor.getInt(4);
                            String phoneLABEL = cursor.getString(5);
                            String string = cursor.getString(2);
                            ContactData.PhoneData phoneData = new ContactData.PhoneData(id, phoneTYPE, phoneLABEL, phoneNUMBER2);
                            int length = phoneData.data.length();
                            if (length > 8) {
                                phoneNUMBER = phoneData.data.substring(length - 8);
                            } else {
                                phoneNUMBER = phoneData.data;
                            }
                            ContactData.PhoneData phoneData2 = new ContactData.PhoneData(id, phoneTYPE, phoneLABEL, phoneNUMBER);
                            ContactData.PhoneData phoneData3 = new ContactData.PhoneData(id, phoneTYPE, phoneLABEL, phoneNUMBER);
                            phoneCutMap.put(phoneData2.data, phoneData2);
                            phoneMap.put(phoneData3.data, phoneData3);
                        }
                        cursor.moveToNext();
                        sum++;
                    }
                }
                cursor.close();
            }
        } catch (OutOfMemoryError e) {
        } finally {
            System.currentTimeMillis();
        }
        return sum;
    }

    public int queryContactStructuredPostal(Map<String, Map<String, ContactData.PostalData>> idPostalMap, String mapKey, String selection, String[] selectionArgs) {
        long currentTimeMillis = System.currentTimeMillis();
        int sum = 0;
        try {
            Cursor cursor = this.mContentResolver.query(ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_URI, ContactData.PROJECTION_POSTAL, selection, selectionArgs, ContactData.PROJECTION_POSTAL[0]);
            if (cursor != null) {
                int count = cursor.getCount();
                if (count > 0) {
                    cursor.moveToFirst();
                    Map<String, ContactData.PostalData> postalMap = idPostalMap.get(mapKey);
                    if (postalMap != null) {
                        postalMap.clear();
                    } else {
                        postalMap = new HashMap<>();
                        idPostalMap.put(mapKey, postalMap);
                    }
                    for (int j = 0; j < count; j++) {
                        ContactData.PostalData postalData = new ContactData.PostalData(cursor.getInt(0), cursor.getInt(4), cursor.getString(5), cursor.getString(6), cursor.getString(7), cursor.getString(8), cursor.getString(9), cursor.getString(10), cursor.getString(11), cursor.getString(12), cursor.getString(13));
                        postalMap.put(postalData.mTypeLabelContent, postalData);
                        cursor.moveToNext();
                        sum++;
                    }
                }
                cursor.close();
            }
        } catch (OutOfMemoryError e) {
        } finally {
            System.currentTimeMillis();
        }
        return sum;
    }

    public int queryContactEmail(Map<String, Map<String, ContactData.EmailData>> idEmailMap, String mapKey, String selection, String[] selectionArgs) {
        long currentTimeMillis = System.currentTimeMillis();
        int sum = 0;
        try {
            Cursor cursor = this.mContentResolver.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, ContactData.PROJECTION_EMAIL, selection, selectionArgs, ContactData.PROJECTION_EMAIL[0]);
            if (cursor != null) {
                int count = cursor.getCount();
                if (count > 0) {
                    cursor.moveToFirst();
                    Map<String, ContactData.EmailData> emailMap = idEmailMap.get(mapKey);
                    if (emailMap != null) {
                        emailMap.clear();
                    } else {
                        emailMap = new HashMap<>();
                        idEmailMap.put(mapKey, emailMap);
                    }
                    for (int j = 0; j < count; j++) {
                        ContactData.EmailData emailData = new ContactData.EmailData(cursor.getInt(0), cursor.getInt(4), cursor.getString(5), cursor.getString(6));
                        emailMap.put(emailData.mTypeLabelEmail, emailData);
                        cursor.moveToNext();
                        sum++;
                    }
                }
                cursor.close();
            }
        } catch (OutOfMemoryError e) {
        } finally {
            System.currentTimeMillis();
        }
        return sum;
    }

    public int queryContactIm(Map<String, Map<String, ContactData.ImData>> idImMap, String mapKey, String selection, String[] selectionArgs) {
        long currentTimeMillis = System.currentTimeMillis();
        int sum = 0;
        try {
            Cursor cursor = this.mContentResolver.query(ContactsContract.Data.CONTENT_URI, ContactData.PROJECTION_IM, selection, selectionArgs, ContactData.PROJECTION_IM[0]);
            if (cursor != null) {
                int count = cursor.getCount();
                if (count > 0) {
                    cursor.moveToFirst();
                    Map<String, ContactData.ImData> imMap = idImMap.get(mapKey);
                    if (imMap != null) {
                        imMap.clear();
                    } else {
                        imMap = new HashMap<>();
                        idImMap.put(mapKey, imMap);
                    }
                    for (int j = 0; j < count; j++) {
                        ContactData.ImData imData = new ContactData.ImData(cursor.getInt(0), cursor.getInt(4), cursor.getString(5), cursor.getString(6), cursor.getInt(7), cursor.getString(8));
                        imMap.put(imData.mTypeLabelContent, imData);
                        cursor.moveToNext();
                        sum++;
                    }
                }
                cursor.close();
            }
        } catch (OutOfMemoryError e) {
        } finally {
            System.currentTimeMillis();
        }
        return sum;
    }

    public int queryContactWebsite(Map<String, Map<String, ContactData.WebsiteData>> idWebMap, String mapKey, String selection, String[] selectionArgs) {
        long currentTimeMillis = System.currentTimeMillis();
        int sum = 0;
        try {
            Cursor cursor = this.mContentResolver.query(ContactsContract.Data.CONTENT_URI, ContactData.PROJECTION_WEBSITE, selection, selectionArgs, ContactData.PROJECTION_WEBSITE[0]);
            if (cursor != null) {
                int count = cursor.getCount();
                if (count > 0) {
                    cursor.moveToFirst();
                    Map<String, ContactData.WebsiteData> webMap = idWebMap.get(mapKey);
                    if (webMap != null) {
                        webMap.clear();
                    } else {
                        webMap = new HashMap<>();
                        idWebMap.put(mapKey, webMap);
                    }
                    for (int j = 0; j < count; j++) {
                        ContactData.WebsiteData webData = new ContactData.WebsiteData(cursor.getInt(0), cursor.getInt(4), cursor.getString(5), cursor.getString(6));
                        webMap.put(webData.mTypeLabelContent, webData);
                        cursor.moveToNext();
                        sum++;
                    }
                }
                cursor.close();
            }
        } catch (OutOfMemoryError e) {
        } finally {
            System.currentTimeMillis();
        }
        return sum;
    }

    public int queryContactEvent(Map<String, ContactData.EventData> eventMap, String selection, String[] selectionArgs) {
        long currentTimeMillis = System.currentTimeMillis();
        int sum = 0;
        try {
            Cursor cursor = this.mContentResolver.query(ContactsContract.Data.CONTENT_URI, ContactData.PROJECTION_EVENT, selection, selectionArgs, ContactData.PROJECTION_EVENT[0]);
            if (cursor != null) {
                int count = cursor.getCount();
                if (count > 0) {
                    cursor.moveToFirst();
                    eventMap.clear();
                    for (int j = 0; j < count; j++) {
                        ContactData.EventData eventData = new ContactData.EventData(cursor.getInt(0), cursor.getInt(4), cursor.getString(5), cursor.getString(6));
                        eventMap.put(eventData.mTypeLabel, eventData);
                        cursor.moveToNext();
                        sum++;
                    }
                }
                cursor.close();
            }
        } catch (OutOfMemoryError e) {
        } finally {
            System.currentTimeMillis();
        }
        return sum;
    }

    public int queryContactOrganization(Map<String, Map<String, ContactData.OrganizationData>> idOrganMap, String mapKey, String selection, String[] selectionArgs) {
        long currentTimeMillis = System.currentTimeMillis();
        int sum = 0;
        try {
            Cursor cursor = this.mContentResolver.query(ContactsContract.Data.CONTENT_URI, ContactData.PROJECTION_ORGANIZATION, selection, selectionArgs, ContactData.PROJECTION_ORGANIZATION[0]);
            if (cursor != null) {
                int count = cursor.getCount();
                if (count > 0) {
                    cursor.moveToFirst();
                    Map<String, ContactData.OrganizationData> organMap = idOrganMap.get(mapKey);
                    if (organMap != null) {
                        organMap.clear();
                    } else {
                        organMap = new HashMap<>();
                        idOrganMap.put(mapKey, organMap);
                    }
                    for (int j = 0; j < count; j++) {
                        ContactData.OrganizationData organData = new ContactData.OrganizationData(cursor.getInt(0), cursor.getInt(4), cursor.getString(5), cursor.getString(6), cursor.getString(7), cursor.getString(8), cursor.getString(9), cursor.getString(10), cursor.getString(11), cursor.getString(12));
                        organMap.put(organData.mTypeLabelContent, organData);
                        cursor.moveToNext();
                        sum++;
                    }
                }
                cursor.close();
            }
        } catch (OutOfMemoryError e) {
        } finally {
            System.currentTimeMillis();
        }
        return sum;
    }

    public int queryContactPhotoId(String selection, String[] selectionArgs) {
        long currentTimeMillis = System.currentTimeMillis();
        int id = -1;
        try {
            Cursor cursor = this.mContentResolver.query(ContactsContract.Data.CONTENT_URI, ContactData.PROJECTION_PHOTO, selection, selectionArgs, ContactData.PROJECTION_PHOTO[0]);
            if (cursor != null) {
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    id = cursor.getInt(0);
                }
                cursor.close();
            }
        } catch (OutOfMemoryError e) {
        } finally {
            System.currentTimeMillis();
        }
        return id;
    }

    public int queryContactNote(Map<String, Map<String, ContactData.NoteData>> idNoteMap, String mapKey, String selection, String[] selectionArgs) {
        long currentTimeMillis = System.currentTimeMillis();
        int sum = 0;
        try {
            Cursor cursor = this.mContentResolver.query(ContactsContract.Data.CONTENT_URI, ContactData.PROJECTION_NOTE, selection, selectionArgs, ContactData.PROJECTION_NOTE[0]);
            if (cursor != null) {
                int count = cursor.getCount();
                if (count > 0) {
                    cursor.moveToFirst();
                    Map<String, ContactData.NoteData> noteMap = idNoteMap.get(mapKey);
                    if (noteMap != null) {
                        noteMap.clear();
                    } else {
                        noteMap = new HashMap<>();
                        idNoteMap.put(mapKey, noteMap);
                    }
                    for (int j = 0; j < count; j++) {
                        ContactData.NoteData noteData = new ContactData.NoteData(cursor.getInt(0), cursor.getString(4), cursor.getString(1));
                        if (!TextUtils.isEmpty(noteData.mTypeContent)) {
                            noteMap.put(noteData.mTypeContent, noteData);
                        }
                        cursor.moveToNext();
                        sum++;
                    }
                }
                cursor.close();
            }
        } catch (OutOfMemoryError e) {
        } finally {
            System.currentTimeMillis();
        }
        return sum;
    }

    public void checkSameStructureName(String rawId, List<String> diffRawIdList, ContactData.StructureNameData name, Map<String, ContactData.StructureNameData> map) {
        long currentTimeMillis = System.currentTimeMillis();
        int sum = 0;
        String nameGivenFamily = "";
        if (!TextUtils.isEmpty(name.mGivenName)) {
            nameGivenFamily = String.valueOf(nameGivenFamily) + name.mGivenName;
            nameGivenFamily.trim();
        }
        if (!TextUtils.isEmpty(name.mFamilyName)) {
            nameGivenFamily = String.valueOf(nameGivenFamily) + name.mFamilyName;
            nameGivenFamily.trim();
        }
        for (Map.Entry<String, ContactData.StructureNameData> me : map.entrySet()) {
            sum++;
            ContactData.StructureNameData sd = (ContactData.StructureNameData) me.getValue();
            String sdGivenFamily = "";
            if (!TextUtils.isEmpty(sd.mGivenName)) {
                sdGivenFamily = String.valueOf(sdGivenFamily) + sd.mGivenName;
                sdGivenFamily.trim();
            }
            if (!TextUtils.isEmpty(sd.mFamilyName)) {
                sdGivenFamily = String.valueOf(sdGivenFamily) + sd.mFamilyName;
                sdGivenFamily.trim();
            }
            if (TextUtils.isEmpty(nameGivenFamily) || TextUtils.isEmpty(sdGivenFamily) || nameGivenFamily.equalsIgnoreCase(sdGivenFamily)) {
                try {
                    if (!TextUtils.isEmpty(name.mPrefix) && !TextUtils.isEmpty(sd.mPrefix) && !name.mPrefix.equals(sd.mPrefix)) {
                        diffRawIdList.add(rawId);
                    } else if (!TextUtils.isEmpty(name.mMiddleName) && !TextUtils.isEmpty(sd.mMiddleName) && !name.mMiddleName.equalsIgnoreCase(sd.mMiddleName)) {
                        diffRawIdList.add(rawId);
                    } else if (!TextUtils.isEmpty(name.mSuffix) && !TextUtils.isEmpty(sd.mSuffix) && !name.mSuffix.equalsIgnoreCase(sd.mSuffix)) {
                        diffRawIdList.add(rawId);
                    }
                } catch (Throwable th) {
                    System.currentTimeMillis();
                    throw th;
                }
            } else {
                diffRawIdList.add(rawId);
            }
            System.currentTimeMillis();
            return;
        }
        System.currentTimeMillis();
    }

    public void checkSameNickName(String rawId, List<String> diffRawIdList, Map<String, ContactData.NickNameData> minMap, Map<String, ContactData.NickNameData> maxMap) {
        long currentTimeMillis = System.currentTimeMillis();
        int sum = 0;
        try {
            if (minMap.size() > maxMap.size()) {
                diffRawIdList.add(rawId);
                return;
            }
            for (Map.Entry<String, ContactData.NickNameData> me : minMap.entrySet()) {
                sum++;
                if (!maxMap.containsKey((String) me.getKey())) {
                    diffRawIdList.add(rawId);
                    System.currentTimeMillis();
                    return;
                }
            }
            System.currentTimeMillis();
        } finally {
            System.currentTimeMillis();
        }
    }

    public void checkSamePhone(String rawId, List<String> diffRawIdList, Map<String, ContactData.PhoneData> minMap, Map<String, ContactData.PhoneData> maxMap) {
        long currentTimeMillis = System.currentTimeMillis();
        CommonMethod.logDebug1(LOG_TAG_1, "checkSamePhone: rawId=" + rawId);
        int sum = 0;
        try {
            if (minMap.size() > maxMap.size()) {
                diffRawIdList.add(rawId);
                return;
            }
            for (Map.Entry<String, ContactData.PhoneData> me : minMap.entrySet()) {
                sum++;
                if (!maxMap.containsKey((String) me.getKey())) {
                    diffRawIdList.add(rawId);
                    System.currentTimeMillis();
                    return;
                }
            }
            System.currentTimeMillis();
        } finally {
            System.currentTimeMillis();
        }
    }

    public void checkSameStructuredPostal(String rawId, List<String> diffRawIdList, Map<String, ContactData.PostalData> minMap, Map<String, ContactData.PostalData> maxMap) {
        long currentTimeMillis = System.currentTimeMillis();
        CommonMethod.logDebug1(LOG_TAG_1, "checkSamePostal: rawId=" + rawId);
        int sum = 0;
        try {
            if (minMap.size() > maxMap.size()) {
                diffRawIdList.add(rawId);
                return;
            }
            for (Map.Entry<String, ContactData.PostalData> me : minMap.entrySet()) {
                sum++;
                if (!maxMap.containsKey((String) me.getKey())) {
                    diffRawIdList.add(rawId);
                    System.currentTimeMillis();
                    return;
                }
            }
            System.currentTimeMillis();
        } finally {
            System.currentTimeMillis();
        }
    }

    public void checkSameEmail(String rawId, List<String> diffRawIdList, Map<String, ContactData.EmailData> minMap, Map<String, ContactData.EmailData> maxMap) {
        long currentTimeMillis = System.currentTimeMillis();
        int sum = 0;
        try {
            if (minMap.size() > maxMap.size()) {
                diffRawIdList.add(rawId);
                return;
            }
            for (Map.Entry<String, ContactData.EmailData> me : minMap.entrySet()) {
                sum++;
                if (!maxMap.containsKey((String) me.getKey())) {
                    diffRawIdList.add(rawId);
                    System.currentTimeMillis();
                    return;
                }
            }
            System.currentTimeMillis();
        } finally {
            System.currentTimeMillis();
        }
    }

    public void checkSameIm(String rawId, List<String> diffRawIdList, Map<String, ContactData.ImData> minMap, Map<String, ContactData.ImData> maxMap) {
        long currentTimeMillis = System.currentTimeMillis();
        int sum = 0;
        try {
            if (minMap.size() > maxMap.size()) {
                diffRawIdList.add(rawId);
                return;
            }
            for (Map.Entry<String, ContactData.ImData> me : minMap.entrySet()) {
                sum++;
                if (!maxMap.containsKey((String) me.getKey())) {
                    diffRawIdList.add(rawId);
                    System.currentTimeMillis();
                    return;
                }
            }
            System.currentTimeMillis();
        } finally {
            System.currentTimeMillis();
        }
    }

    public void checkSameWebsite(String rawId, List<String> diffRawIdList, Map<String, ContactData.WebsiteData> minMap, Map<String, ContactData.WebsiteData> maxMap) {
        long currentTimeMillis = System.currentTimeMillis();
        int sum = 0;
        try {
            if (minMap.size() > maxMap.size()) {
                diffRawIdList.add(rawId);
                return;
            }
            for (Map.Entry<String, ContactData.WebsiteData> me : minMap.entrySet()) {
                sum++;
                if (!maxMap.containsKey((String) me.getKey())) {
                    diffRawIdList.add(rawId);
                    System.currentTimeMillis();
                    return;
                }
            }
            System.currentTimeMillis();
        } finally {
            System.currentTimeMillis();
        }
    }

    public void checkSameOrganization(String rawId, List<String> diffRawIdList, Map<String, ContactData.OrganizationData> minMap, Map<String, ContactData.OrganizationData> maxMap) {
        long currentTimeMillis = System.currentTimeMillis();
        int sum = 0;
        try {
            if (minMap.size() > maxMap.size()) {
                diffRawIdList.add(rawId);
                return;
            }
            for (Map.Entry<String, ContactData.OrganizationData> me : minMap.entrySet()) {
                sum++;
                if (!maxMap.containsKey((String) me.getKey())) {
                    diffRawIdList.add(rawId);
                    System.currentTimeMillis();
                    return;
                }
            }
            System.currentTimeMillis();
        } finally {
            System.currentTimeMillis();
        }
    }

    public void checkSameNote(String rawId, List<String> diffRawIdList, Map<String, ContactData.NoteData> minMap, Map<String, ContactData.NoteData> maxMap) {
        long currentTimeMillis = System.currentTimeMillis();
        int sum = 0;
        try {
            if (minMap.size() > maxMap.size()) {
                diffRawIdList.add(rawId);
                return;
            }
            for (Map.Entry<String, ContactData.NoteData> me : minMap.entrySet()) {
                sum++;
                if (!maxMap.containsKey((String) me.getKey())) {
                    diffRawIdList.add(rawId);
                    System.currentTimeMillis();
                    return;
                }
            }
            System.currentTimeMillis();
        } finally {
            System.currentTimeMillis();
        }
    }
}
