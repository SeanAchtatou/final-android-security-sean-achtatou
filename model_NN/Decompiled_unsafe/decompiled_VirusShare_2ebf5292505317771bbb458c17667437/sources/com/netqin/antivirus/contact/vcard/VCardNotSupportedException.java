package com.netqin.antivirus.contact.vcard;

public class VCardNotSupportedException extends VCardException {
    public VCardNotSupportedException() {
    }

    public VCardNotSupportedException(String message) {
        super(message);
    }
}
