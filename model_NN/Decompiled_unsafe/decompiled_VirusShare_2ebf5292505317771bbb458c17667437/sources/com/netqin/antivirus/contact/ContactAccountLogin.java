package com.netqin.antivirus.contact;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.netqin.antivirus.SHA1Util;
import com.netqin.antivirus.Value;
import com.netqin.antivirus.antilost.AntiLostCommon;
import com.netqin.antivirus.appprotocol.XmlTagValue;
import com.netqin.antivirus.common.CommonDefine;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.common.CommonUtils;
import com.netqin.antivirus.common.ProgDlgActivity;
import com.netqin.antivirus.net.accountservice.AccountService;
import com.nqmobile.antivirus_ampro20.R;

public class ContactAccountLogin extends ProgDlgActivity implements TextWatcher {
    public static Activity mContactAccountLogin = null;
    /* access modifiers changed from: private */
    public AccountService accountService;
    /* access modifiers changed from: private */
    public ContentValues content;
    private TextView createAccount;
    private TextView forgetPsw;
    /* access modifiers changed from: private */
    public boolean isFromAAG;
    private String mAccount;
    private Button mBtnCancel;
    private Button mBtnOk;
    private EditText mETAccount;
    private EditText mETPassword;
    /* access modifiers changed from: private */
    public int mSession = 0;

    public void onCreate(Bundle savedInstanceState) {
        this.accountService = AccountService.getInstance(this);
        this.content = new ContentValues();
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.contacts_account_login);
        mContactAccountLogin = this;
        this.mActivityVisible = true;
        Intent intent = getIntent();
        this.mSession = intent.getIntExtra("session", 0);
        this.isFromAAG = intent.getBooleanExtra("isFromAAG", false);
        this.mETAccount = (EditText) findViewById(R.id.contacts_account_login_user);
        this.mETPassword = (EditText) findViewById(R.id.contacts_account_login_pwd);
        this.mBtnOk = (Button) findViewById(R.id.contact_account_login_ok);
        this.mBtnCancel = (Button) findViewById(R.id.contact_account_login_cancel);
        this.forgetPsw = (TextView) findViewById(R.id.contact_account_login_forget);
        this.createAccount = (TextView) findViewById(R.id.contact_account_login_create);
        this.mAccount = getSharedPreferences(AntiLostCommon.DELETE_CONTACT, 0).getString("user", "");
        if (!TextUtils.isEmpty(this.mAccount)) {
            this.mETAccount.setText(this.mAccount);
            this.mETPassword.requestFocus();
        } else {
            this.mETAccount.setText("");
        }
        this.mETAccount.addTextChangedListener(this);
        this.mETPassword.addTextChangedListener(this);
        this.mBtnOk.addTextChangedListener(this);
        this.mBtnOk.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ContactAccountLogin.this.clickBtnOk();
            }
        });
        this.mBtnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ContactAccountLogin.this.clickBtnCancel();
            }
        });
        this.forgetPsw.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ContactAccountLogin.this.clickForgetBackupAccount();
            }
        });
        this.createAccount.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ContactAccountLogin.this.clickCreateBackupAccount();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.mActivityVisible = false;
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        this.mActivityVisible = true;
        super.onResume();
    }

    public void afterTextChanged(Editable s) {
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {
        String account = this.mETAccount.getText().toString().trim();
        String password = this.mETPassword.getText().toString().trim();
        if (TextUtils.isEmpty(account) || TextUtils.isEmpty(password)) {
            this.mBtnOk.setEnabled(false);
        } else {
            this.mBtnOk.setEnabled(true);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void clickCreateBackupAccount() {
        if (ContactGuide.isBackupFromContactGuide) {
            Intent intent = new Intent(this, ContactAccountCreate.class);
            intent.putExtra("createAccountBackup", true);
            startActivity(intent);
            finish();
        } else if (ContactGuide.isRestoreFromContactGuide) {
            Intent intent2 = new Intent(this, ContactAccountCreate.class);
            intent2.putExtra("createAccountRestore", true);
            startActivity(intent2);
            finish();
        } else {
            startActivity(new Intent(this, ContactAccountCreate.class));
            finish();
        }
        finish();
    }

    /* access modifiers changed from: private */
    public void clickBtnOk() {
        final String account = this.mETAccount.getText().toString().trim();
        final String password = this.mETPassword.getText().toString().trim();
        this.mProgressText = (String) getText(R.string.text_account_logining);
        this.mProgressDlg = CommonMethod.progressDlgCreate(this, this.mProgressText, this.mHandler);
        CommonMethod.sendUserMessage(this.mHandler, 1);
        this.content.put(Value.Username, account);
        this.content.put(Value.Password, SHA1Util.hex_sha1(password));
        this.content.put("UID", CommonMethod.getUID(this));
        new Thread(new Runnable() {
            public void run() {
                int result = ContactAccountLogin.this.accountService.request(null, ContactAccountLogin.this.content, 2);
                final String accountInfo = ContactCommon.getAccountInfo(ContactAccountLogin.this, account);
                CommonMethod.sendUserMessage(ContactAccountLogin.this.mHandler, 4);
                if (!ContactAccountLogin.this.mActivityVisible) {
                    return;
                }
                if (result == 8 || result == 16) {
                    ContactAccountLogin.this.handleMSG(R.string.SEND_RECEIVE_ERROR);
                } else if (ContactAccountLogin.this.content.containsKey(Value.Code)) {
                    String code = ContactAccountLogin.this.content.getAsString(Value.Code);
                    if (code.equals("0")) {
                        Handler access$6 = ContactAccountLogin.this.mHandler;
                        final String str = password;
                        final String str2 = account;
                        access$6.post(new Runnable() {
                            public void run() {
                                Toast toast = Toast.makeText(ContactAccountLogin.this, (int) R.string.text_succ_login, 0);
                                toast.setGravity(81, 0, 100);
                                toast.show();
                                if (ContactAccountLogin.this.isFromAAG) {
                                    AccountAdminGuide.mAccountAdminGuide.finish();
                                }
                                ContactAccountLogin.this.finish();
                                if (ContactAccountLogin.mContactAccountLogin != null) {
                                    ContactAccountLogin.mContactAccountLogin.finish();
                                    ContactAccountLogin.mContactAccountLogin = null;
                                }
                                switch (ContactAccountLogin.this.mSession) {
                                    case 1:
                                        ContactAccountLogin.this.startActivity(new Intent(ContactAccountLogin.this, ServerBackupDoing.class));
                                        break;
                                    case 2:
                                        ContactAccountLogin.this.startActivity(new Intent(ContactAccountLogin.this, ServerRestoreDoing.class));
                                        break;
                                }
                                CommonUtils.putConfigWithStringValue(ContactAccountLogin.this, AntiLostCommon.DELETE_CONTACT, "password", str);
                                CommonMethod.putConfigWithStringValue(ContactAccountLogin.this, AntiLostCommon.DELETE_CONTACT, "user", str2);
                                CommonMethod.putConfigWithBooleanValue(ContactAccountLogin.this, AntiLostCommon.DELETE_CONTACT, "user_state", true);
                                CommonMethod.putConfigWithStringValue(ContactAccountLogin.this, AntiLostCommon.DELETE_CONTACT, XmlTagValue.version, CommonDefine.VERSION);
                                ContactCommon.mContactAccount = str2;
                                if (accountInfo.equals("-1") || accountInfo.equals("-2") || accountInfo.equals("-3") || TextUtils.isEmpty(accountInfo)) {
                                    CommonMethod.putConfigWithStringValue(ContactAccountLogin.this, AntiLostCommon.DELETE_CONTACT, "contacts_network", "0");
                                    CommonMethod.putConfigWithStringValue(ContactAccountLogin.this, AntiLostCommon.DELETE_CONTACT, "contacts_backup_time", "0");
                                    return;
                                }
                                String backupPeople = accountInfo.split("#")[1];
                                String backupTime = accountInfo.split("T")[0];
                                CommonMethod.putConfigWithStringValue(ContactAccountLogin.this, AntiLostCommon.DELETE_CONTACT, "contacts_network", backupPeople);
                                CommonMethod.putConfigWithStringValue(ContactAccountLogin.this, AntiLostCommon.DELETE_CONTACT, "contacts_backup_time", backupTime);
                            }
                        });
                    } else if (code.equals("-1")) {
                        ContactAccountLogin.this.handleMSG(R.string.SYSTEM_ERROR);
                    } else if (code.equals("-2")) {
                        ContactAccountLogin.this.handleMSG(R.string.USERNAME_NOT_EXIST);
                    } else if (code.equals("-3")) {
                        ContactAccountLogin.this.handleMSG(R.string.PASSWORD_ERROR);
                    }
                }
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public void handleMSG(final int msgID) {
        this.mHandler.post(new Runnable() {
            public void run() {
                CommonMethod.messageDialog(ContactAccountLogin.this, msgID, (int) R.string.label_netqin_antivirus);
            }
        });
    }

    /* access modifiers changed from: private */
    public void clickBtnCancel() {
        finish();
    }

    /* access modifiers changed from: private */
    public void clickForgetBackupAccount() {
        finish();
        String uri = "http://o.netqin.com";
        if (!CommonMethod.isLocalSimpleChinese()) {
            switch (0) {
                case 0:
                    uri = "http://www.netqin.com/uc/goResetPassAndroid.html?l=en_us";
                    break;
                case 1:
                    uri = "http://vm.netqin.cn:4080/uc/goResetPassAndroid.html?l=en_us";
                    break;
                case 2:
                    uri = "http://www.netqin.com/uc/goResetPassAndroid.html?l=en_us";
                    break;
            }
        } else {
            switch (0) {
                case 0:
                    uri = "http://www.netqin.com/uc/goResetPassAndroid.html?l=zh_cn";
                    break;
                case 1:
                    uri = "http://vm.netqin.cn:4080/uc/goResetPassAndroid.html?l=zh_cn";
                    break;
                case 2:
                    uri = "http://www.netqin.com/uc/goResetPassAndroid.html?l=zh_cn";
                    break;
            }
        }
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(uri)));
    }
}
