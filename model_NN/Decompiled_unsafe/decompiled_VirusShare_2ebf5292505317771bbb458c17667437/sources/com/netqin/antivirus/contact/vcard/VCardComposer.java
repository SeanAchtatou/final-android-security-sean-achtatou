package com.netqin.antivirus.contact.vcard;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.text.format.Time;
import com.netqin.antivirus.antilost.ContactsHandler;
import com.netqin.antivirus.antilost.SmsHandler;
import com.netqin.antivirus.common.CommonMethod;
import com.nqmobile.antivirus_ampro20.R;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class VCardComposer {
    private static final int CALLER_NAME_COLUMN_INDEX = 3;
    private static final int CALLER_NUMBERLABEL_COLUMN_INDEX = 5;
    private static final int CALLER_NUMBERTYPE_COLUMN_INDEX = 4;
    private static final int CALL_TYPE_COLUMN_INDEX = 2;
    private static final int DATE_COLUMN_INDEX = 1;
    private static final String DEFAULT_EMAIL_TYPE = "INTERNET";
    public static final String FAILURE_REASON_FAILED_TO_GET_DATABASE_INFO = "Failed to get database information";
    public static final String FAILURE_REASON_NOT_INITIALIZED = "The vCard composer object is not correctly initialized";
    private static final String FLAG_TIMEZONE_UTC = "Z";
    public static final String FOR_EXPORT_ONLY = "for_export_only";
    private static final String LOG_TAG = "vcard.VCardComposer";
    private static final String LOG_TAG_2 = "vcardcomposer";
    public static final String NO_ERROR = "No error";
    private static final int NUMBER_COLUMN_INDEX = 0;
    private static final String SHIFT_JIS = "SHIFT_JIS";
    private static final String VCARD_ATTR_ENCODING_BASE64_V21 = "ENCODING=BASE64";
    private static final String VCARD_ATTR_ENCODING_BASE64_V30 = "ENCODING=b";
    private static final String VCARD_ATTR_ENCODING_QP = "ENCODING=QUOTED-PRINTABLE";
    private static final String VCARD_ATTR_EQUAL = "=";
    private static final String VCARD_ATTR_SEPARATOR = ";";
    private static final String VCARD_COL_SEPARATOR = "\r\n";
    private static final String VCARD_DATA_PUBLIC = "PUBLIC";
    private static final String VCARD_DATA_SEPARATOR = ":";
    private static final String VCARD_DATA_VCARD = "VCARD";
    private static final String VCARD_ITEM_SEPARATOR = ";";
    private static final String VCARD_PROPERTY_ADR = "ADR";
    private static final String VCARD_PROPERTY_BEGIN = "BEGIN";
    private static final String VCARD_PROPERTY_BIRTHDAY = "BDAY";
    private static final String VCARD_PROPERTY_CALLTYPE_INCOMING = "INCOMING";
    private static final String VCARD_PROPERTY_CALLTYPE_MISSED = "MISSED";
    private static final String VCARD_PROPERTY_CALLTYPE_OUTGOING = "OUTGOING";
    private static final String VCARD_PROPERTY_EMAIL = "EMAIL";
    private static final String VCARD_PROPERTY_END = "END";
    private static final String VCARD_PROPERTY_FULL_NAME = "FN";
    private static final String VCARD_PROPERTY_NAME = "N";
    private static final String VCARD_PROPERTY_NICKNAME = "NICKNAME";
    private static final String VCARD_PROPERTY_NOTE = "NOTE";
    private static final String VCARD_PROPERTY_ORG = "ORG";
    private static final String VCARD_PROPERTY_PHOTO = "PHOTO";
    private static final String VCARD_PROPERTY_SORT_STRING = "SORT-STRING";
    private static final String VCARD_PROPERTY_SOUND = "SOUND";
    private static final String VCARD_PROPERTY_TEL = "TEL";
    private static final String VCARD_PROPERTY_TITLE = "TITLE";
    private static final String VCARD_PROPERTY_URL = "URL";
    private static final String VCARD_PROPERTY_VERSION = "VERSION";
    private static final String VCARD_PROPERTY_X_CLASS = "X-CLASS";
    private static final String VCARD_PROPERTY_X_DCM_HMN_MODE = "X-DCM-HMN-MODE";
    private static final String VCARD_PROPERTY_X_NICKNAME = "X-NICKNAME";
    private static final String VCARD_PROPERTY_X_NO = "X-NO";
    private static final String VCARD_PROPERTY_X_PHONETIC_FIRST_NAME = "X-PHONETIC-FIRST-NAME";
    private static final String VCARD_PROPERTY_X_PHONETIC_LAST_NAME = "X-PHONETIC-LAST-NAME";
    private static final String VCARD_PROPERTY_X_PHONETIC_MIDDLE_NAME = "X-PHONETIC-MIDDLE-NAME";
    private static final String VCARD_PROPERTY_X_REDUCTION = "X-REDUCTION";
    private static final String VCARD_PROPERTY_X_TIMESTAMP = "X-IRMC-CALL-DATETIME";
    public static final String VCARD_TYPE_STRING_DOCOMO = "docomo";
    private static final String VCARD_WS = " ";
    private static String mCurrentDisplayName = "";
    private static final String[] sCallLogProjection = {"number", "date", "type", "name", "numbertype", "numberlabel"};
    private static final String[] sContactsProjection = {SmsHandler.ROWID};
    private static final Uri sDataRequestUri;
    private static final Map<Integer, String> sImMap = new HashMap();
    private final boolean mCareHandlerErrors;
    /* access modifiers changed from: private */
    public final String mCharsetString;
    private final ContentResolver mContentResolver;
    private final Context mContext;
    private Cursor mCursor;
    /* access modifiers changed from: private */
    public String mErrorReason;
    private final List<OneEntryHandler> mHandlerList;
    private int mIdColumn;
    private boolean mIsCallLogComposer;
    /* access modifiers changed from: private */
    public final boolean mIsDoCoMo;
    private final boolean mIsJapaneseMobilePhone;
    private final boolean mIsV30;
    private boolean mNeedPhotoForVCard;
    private final boolean mOnlyOneNoteFieldIsAvailable;
    private boolean mTerminateIsCalled;
    private final boolean mUsesAndroidProperty;
    private final boolean mUsesDefactProperty;
    private final boolean mUsesQPToPrimaryProperties;
    private final boolean mUsesQuotedPrintable;
    private final boolean mUsesShiftJis;
    private final boolean mUsesUtf8;
    private final String mVCardAttributeCharset;
    private final int mVCardType;

    public interface OneEntryHandler {
        boolean onEntryCreated(String str);

        boolean onInit(Context context);

        void onTerminate();
    }

    static {
        Uri.Builder builder = ContactsContract.RawContacts.CONTENT_URI.buildUpon();
        builder.appendQueryParameter(FOR_EXPORT_ONLY, "1");
        sDataRequestUri = builder.build();
        sImMap.put(0, Constants.PROPERTY_X_AIM);
        sImMap.put(1, Constants.PROPERTY_X_MSN);
        sImMap.put(2, Constants.PROPERTY_X_YAHOO);
        sImMap.put(6, Constants.PROPERTY_X_ICQ);
        sImMap.put(7, Constants.PROPERTY_X_JABBER);
        sImMap.put(3, Constants.PROPERTY_X_SKYPE_USERNAME);
    }

    public class HandlerForOutputStream implements OneEntryHandler {
        private static final String LOG_TAG = "vcard.VCardComposer.HandlerForOutputStream";
        private boolean mOnTerminateIsCalled = false;
        private final OutputStream mOutputStream;
        private Writer mWriter;

        public HandlerForOutputStream(OutputStream outputStream) {
            this.mOutputStream = outputStream;
        }

        public boolean onInit(Context context) {
            try {
                this.mWriter = new BufferedWriter(new OutputStreamWriter(this.mOutputStream, VCardComposer.this.mCharsetString));
                if (VCardComposer.this.mIsDoCoMo) {
                    try {
                        this.mWriter.write(VCardComposer.this.createOneEntryInternal("-1"));
                    } catch (IOException e) {
                        VCardComposer.this.mErrorReason = "IOException occurred: " + e.getMessage();
                        return false;
                    }
                }
                return true;
            } catch (UnsupportedEncodingException e2) {
                VCardComposer.this.mErrorReason = "Encoding is not supported (usually this does not happen!): " + VCardComposer.this.mCharsetString;
                return false;
            }
        }

        public boolean onEntryCreated(String vcard) {
            try {
                this.mWriter.write(vcard);
                this.mWriter.flush();
                return true;
            } catch (IOException e) {
                VCardComposer.this.mErrorReason = "IOException occurred: " + e.getMessage();
                return false;
            }
        }

        public void onTerminate() {
            this.mOnTerminateIsCalled = true;
            if (this.mWriter != null) {
                try {
                    this.mWriter.flush();
                    if (this.mOutputStream != null && (this.mOutputStream instanceof FileOutputStream)) {
                        ((FileOutputStream) this.mOutputStream).getFD().sync();
                    }
                    try {
                        this.mWriter.close();
                    } catch (IOException e) {
                    }
                } catch (IOException e2) {
                    CommonMethod.logDebug1(LOG_TAG, "IOException during closing the output stream: " + e2.getMessage());
                    try {
                        this.mWriter.close();
                    } catch (IOException e3) {
                    }
                } catch (Throwable th) {
                    try {
                        this.mWriter.close();
                    } catch (IOException e4) {
                    }
                    throw th;
                }
            }
        }

        public void finalize() {
            if (!this.mOnTerminateIsCalled) {
                onTerminate();
            }
        }
    }

    public VCardComposer(Context context) {
        this(context, VCardConfig.VCARD_TYPE_DEFAULT, true, false, true);
    }

    public VCardComposer(Context context, String vcardTypeStr, boolean careHandlerErrors) {
        this(context, VCardConfig.getVCardTypeFromString(vcardTypeStr), careHandlerErrors, false, true);
    }

    public VCardComposer(Context context, int vcardType, boolean careHandlerErrors) {
        this(context, vcardType, careHandlerErrors, false, true);
    }

    public VCardComposer(Context context, int vcardType, boolean careHandlerErrors, boolean isCallLogComposer, boolean needPhotoInVCard) {
        this.mErrorReason = NO_ERROR;
        this.mIsCallLogComposer = false;
        this.mNeedPhotoForVCard = true;
        this.mContext = context;
        this.mVCardType = vcardType;
        this.mCareHandlerErrors = careHandlerErrors;
        this.mIsCallLogComposer = isCallLogComposer;
        this.mNeedPhotoForVCard = needPhotoInVCard;
        this.mContentResolver = context.getContentResolver();
        this.mIsV30 = VCardConfig.isV30(vcardType);
        this.mUsesQuotedPrintable = VCardConfig.usesQuotedPrintable(vcardType);
        this.mIsDoCoMo = VCardConfig.isDoCoMo(vcardType);
        this.mIsJapaneseMobilePhone = VCardConfig.needsToConvertPhoneticString(vcardType);
        this.mOnlyOneNoteFieldIsAvailable = VCardConfig.onlyOneNoteFieldIsAvailable(vcardType);
        this.mUsesAndroidProperty = VCardConfig.usesAndroidSpecificProperty(vcardType);
        this.mUsesDefactProperty = VCardConfig.usesDefactProperty(vcardType);
        this.mUsesUtf8 = VCardConfig.usesUtf8(vcardType);
        this.mUsesShiftJis = VCardConfig.usesShiftJis(vcardType);
        this.mUsesQPToPrimaryProperties = VCardConfig.usesQPToPrimaryProperties(vcardType);
        this.mHandlerList = new ArrayList();
        if (this.mIsDoCoMo) {
            this.mCharsetString = CharsetUtils.charsetForVendor(SHIFT_JIS, VCARD_TYPE_STRING_DOCOMO).name();
            this.mVCardAttributeCharset = "CHARSET=SHIFT_JIS";
        } else if (this.mUsesShiftJis) {
            this.mCharsetString = CharsetUtils.charsetForVendor(SHIFT_JIS).name();
            this.mVCardAttributeCharset = "CHARSET=SHIFT_JIS";
        } else {
            this.mCharsetString = StringEncodings.UTF8;
            this.mVCardAttributeCharset = "CHARSET=UTF-8";
        }
    }

    public String composeVCardForPhoneOwnNumber(int phonetype, String phoneName, String phoneNumber, boolean vcardVer21) {
        StringBuilder builder = new StringBuilder();
        appendVCardLine(builder, VCARD_PROPERTY_BEGIN, VCARD_DATA_VCARD);
        if (!vcardVer21) {
            appendVCardLine(builder, VCARD_PROPERTY_VERSION, Constants.VERSION_V30);
        } else {
            appendVCardLine(builder, VCARD_PROPERTY_VERSION, Constants.VERSION_V21);
        }
        boolean needCharset = false;
        if (!VCardUtils.containsOnlyPrintableAscii(phoneName)) {
            needCharset = true;
        }
        appendVCardLine(builder, VCARD_PROPERTY_FULL_NAME, phoneName, needCharset, false);
        appendVCardLine(builder, VCARD_PROPERTY_NAME, phoneName, needCharset, false);
        if (!TextUtils.isEmpty(phoneNumber)) {
            StringBuilder sb = builder;
            appendVCardTelephoneLine(sb, Integer.valueOf(phonetype), Integer.toString(phonetype), phoneNumber, false);
        }
        appendVCardLine(builder, VCARD_PROPERTY_END, VCARD_DATA_VCARD);
        return builder.toString();
    }

    public void addHandler(OneEntryHandler handler) {
        this.mHandlerList.add(handler);
    }

    public boolean init() {
        return init(null, null);
    }

    public boolean init(String selection, String[] selectionArgs) {
        if (this.mCareHandlerErrors) {
            List<OneEntryHandler> finishedList = new ArrayList<>(this.mHandlerList.size());
            for (OneEntryHandler handler : this.mHandlerList) {
                if (!handler.onInit(this.mContext)) {
                    for (OneEntryHandler finished : finishedList) {
                        finished.onTerminate();
                    }
                    return false;
                }
            }
        } else {
            for (OneEntryHandler handler2 : this.mHandlerList) {
                handler2.onInit(this.mContext);
            }
        }
        if (this.mIsCallLogComposer) {
            this.mCursor = this.mContentResolver.query(CallLog.Calls.CONTENT_URI, sCallLogProjection, selection, selectionArgs, null);
        } else {
            this.mCursor = this.mContentResolver.query(ContactsContract.Contacts.CONTENT_URI, sContactsProjection, selection, selectionArgs, null);
        }
        if (this.mCursor == null) {
            this.mErrorReason = FAILURE_REASON_FAILED_TO_GET_DATABASE_INFO;
            return false;
        } else if (getCount() == 0 || !this.mCursor.moveToFirst()) {
            try {
                this.mCursor.close();
            } catch (SQLiteException e) {
            } finally {
                this.mCursor = null;
                this.mErrorReason = this.mContext.getString(R.string.text_error_no_exportable_contact);
            }
            return false;
        } else {
            if (this.mIsCallLogComposer) {
                this.mIdColumn = -1;
            } else {
                this.mIdColumn = this.mCursor.getColumnIndex(SmsHandler.ROWID);
            }
            return true;
        }
    }

    public boolean createOneEntry() {
        String vcard;
        if (this.mCursor == null || this.mCursor.isAfterLast()) {
            this.mErrorReason = FAILURE_REASON_NOT_INITIALIZED;
            return false;
        }
        try {
            if (this.mIsCallLogComposer) {
                vcard = createOneCallLogEntryInternal();
            } else if (this.mIdColumn >= 0) {
                vcard = createOneEntryInternal(this.mCursor.getString(this.mIdColumn));
            } else {
                this.mCursor.moveToNext();
                return true;
            }
            this.mCursor.moveToNext();
            if (this.mCareHandlerErrors) {
                new ArrayList(this.mHandlerList.size());
                for (OneEntryHandler handler : this.mHandlerList) {
                    if (!handler.onEntryCreated(vcard)) {
                        return false;
                    }
                }
            } else {
                for (OneEntryHandler handler2 : this.mHandlerList) {
                    handler2.onEntryCreated(vcard);
                }
            }
            return true;
        } catch (OutOfMemoryError e) {
            System.gc();
            this.mCursor.moveToNext();
            return true;
        } catch (Throwable th) {
            this.mCursor.moveToNext();
            throw th;
        }
    }

    private final String toRfc2455Format(long millSecs) {
        Time startDate = new Time();
        startDate.set(millSecs);
        return String.valueOf(startDate.format2445()) + FLAG_TIMEZONE_UTC;
    }

    private void tryAppendCallHistoryTimeStampField(StringBuilder builder) {
        String callLogTypeStr;
        switch (this.mCursor.getInt(2)) {
            case 1:
                callLogTypeStr = VCARD_PROPERTY_CALLTYPE_INCOMING;
                break;
            case 2:
                callLogTypeStr = VCARD_PROPERTY_CALLTYPE_OUTGOING;
                break;
            case 3:
                callLogTypeStr = VCARD_PROPERTY_CALLTYPE_MISSED;
                break;
            default:
                return;
        }
        long dateAsLong = this.mCursor.getLong(1);
        builder.append(VCARD_PROPERTY_X_TIMESTAMP);
        builder.append(";");
        appendTypeAttribute(builder, callLogTypeStr);
        builder.append(VCARD_DATA_SEPARATOR);
        builder.append(toRfc2455Format(dateAsLong));
        builder.append(VCARD_COL_SEPARATOR);
    }

    private String createOneCallLogEntryInternal() {
        StringBuilder builder = new StringBuilder();
        appendVCardLine(builder, VCARD_PROPERTY_BEGIN, VCARD_DATA_VCARD);
        if (this.mIsV30) {
            appendVCardLine(builder, VCARD_PROPERTY_VERSION, Constants.VERSION_V30);
        } else {
            appendVCardLine(builder, VCARD_PROPERTY_VERSION, Constants.VERSION_V21);
        }
        String name = this.mCursor.getString(3);
        if (TextUtils.isEmpty(name)) {
            name = this.mCursor.getString(0);
        }
        boolean needCharset = !VCardUtils.containsOnlyPrintableAscii(name);
        appendVCardLine(builder, VCARD_PROPERTY_FULL_NAME, name, needCharset, false);
        appendVCardLine(builder, VCARD_PROPERTY_NAME, name, needCharset, false);
        String number = this.mCursor.getString(0);
        int type = this.mCursor.getInt(4);
        String label = this.mCursor.getString(5);
        if (TextUtils.isEmpty(label)) {
            label = Integer.toString(type);
        }
        appendVCardTelephoneLine(builder, Integer.valueOf(type), label, number, false);
        tryAppendCallHistoryTimeStampField(builder);
        appendVCardLine(builder, VCARD_PROPERTY_END, VCARD_DATA_VCARD);
        return builder.toString();
    }

    /* access modifiers changed from: private */
    public String createOneEntryInternal(String contactId) {
        Map<String, List<ContentValues>> contentValuesListMap = new HashMap<>();
        long currentTimeMillis = System.currentTimeMillis();
        new VCardContact(this.mContentResolver).queryContactItems(contentValuesListMap, contactId);
        StringBuilder builder = new StringBuilder();
        appendVCardLine(builder, VCARD_PROPERTY_BEGIN, VCARD_DATA_VCARD);
        if (this.mIsV30) {
            appendVCardLine(builder, VCARD_PROPERTY_VERSION, Constants.VERSION_V30);
        } else {
            appendVCardLine(builder, VCARD_PROPERTY_VERSION, Constants.VERSION_V21);
        }
        appendStructuredNames(builder, contentValuesListMap);
        appendNickNames(builder, contentValuesListMap);
        appendPhones(builder, contentValuesListMap);
        appendEmails(builder, contentValuesListMap);
        appendPostals(builder, contentValuesListMap);
        appendIms(builder, contentValuesListMap);
        appendWebsites(builder, contentValuesListMap);
        appendBirthday(builder, contentValuesListMap);
        appendOrganizations(builder, contentValuesListMap);
        if (this.mNeedPhotoForVCard) {
            appendPhotos(builder, contentValuesListMap);
        }
        appendNotes(builder, contentValuesListMap);
        if (this.mIsDoCoMo) {
            appendVCardLine(builder, VCARD_PROPERTY_X_CLASS, VCARD_DATA_PUBLIC);
            appendVCardLine(builder, VCARD_PROPERTY_X_REDUCTION, "");
            appendVCardLine(builder, VCARD_PROPERTY_X_NO, "");
            appendVCardLine(builder, VCARD_PROPERTY_X_DCM_HMN_MODE, "");
        }
        appendVCardLine(builder, VCARD_PROPERTY_END, VCARD_DATA_VCARD);
        return builder.toString();
    }

    public void terminate() {
        for (OneEntryHandler handler : this.mHandlerList) {
            handler.onTerminate();
        }
        if (this.mCursor != null) {
            try {
                this.mCursor.close();
            } catch (SQLiteException e) {
            }
            this.mCursor = null;
        }
        this.mTerminateIsCalled = true;
    }

    public void finalize() {
        if (!this.mTerminateIsCalled) {
            terminate();
        }
    }

    public int getCount() {
        if (this.mCursor == null) {
            return 0;
        }
        return this.mCursor.getCount();
    }

    public boolean isAfterLast() {
        if (this.mCursor == null) {
            return false;
        }
        return this.mCursor.isAfterLast();
    }

    public String getErrorReason() {
        return this.mErrorReason;
    }

    private void appendStructuredNames(StringBuilder builder, Map<String, List<ContentValues>> contentValuesListMap) {
        List<ContentValues> contentValuesList = contentValuesListMap.get("vnd.android.cursor.item/name");
        if (contentValuesList != null && contentValuesList.size() > 0) {
            appendStructuredNamesInternal(builder, contentValuesList);
        } else if (this.mIsDoCoMo) {
            appendVCardLine(builder, VCARD_PROPERTY_NAME, "");
        } else if (this.mIsV30) {
            appendVCardLine(builder, VCARD_PROPERTY_NAME, "");
            appendVCardLine(builder, VCARD_PROPERTY_FULL_NAME, "");
        }
    }

    private boolean containsNonEmptyName(ContentValues contentValues) {
        return !TextUtils.isEmpty(contentValues.getAsString("data3")) || !TextUtils.isEmpty(contentValues.getAsString("data5")) || !TextUtils.isEmpty(contentValues.getAsString("data2")) || !TextUtils.isEmpty(contentValues.getAsString("data4")) || !TextUtils.isEmpty(contentValues.getAsString("data6")) || !TextUtils.isEmpty(contentValues.getAsString(ContactsHandler.PHONE_NUMBER));
    }

    private void appendStructuredNamesInternal(StringBuilder builder, List<ContentValues> contentValuesList) {
        String encodedPhoneticFamilyName;
        String encodedPhoneticMiddleName;
        String encodedPhoneticGivenName;
        String encodedPhoneticFamilyName2;
        String encodedPhoneticMiddleName2;
        String encodedPhoneticGivenName2;
        String encodedFamily;
        String encodedGiven;
        String encodedMiddle;
        String encodedPrefix;
        String encodedSuffix;
        String encodedFullname;
        String encodedDisplayName;
        ContentValues primaryContentValues = null;
        ContentValues subprimaryContentValues = null;
        Iterator<ContentValues> it = contentValuesList.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            ContentValues contentValues = it.next();
            if (contentValues != null) {
                Integer isSuperPrimary = contentValues.getAsInteger("is_super_primary");
                if (isSuperPrimary != null && isSuperPrimary.intValue() > 0) {
                    primaryContentValues = contentValues;
                    break;
                } else if (primaryContentValues == null) {
                    Integer isPrimary = contentValues.getAsInteger("is_primary");
                    if (isPrimary != null && isPrimary.intValue() > 0 && containsNonEmptyName(contentValues)) {
                        primaryContentValues = contentValues;
                    } else if (subprimaryContentValues == null && containsNonEmptyName(contentValues)) {
                        subprimaryContentValues = contentValues;
                    }
                }
            }
        }
        if (primaryContentValues == null) {
            if (subprimaryContentValues != null) {
                primaryContentValues = subprimaryContentValues;
            } else {
                primaryContentValues = new ContentValues();
            }
        }
        String familyName = primaryContentValues.getAsString("data3");
        String middleName = primaryContentValues.getAsString("data5");
        String givenName = primaryContentValues.getAsString("data2");
        String prefix = primaryContentValues.getAsString("data4");
        String suffix = primaryContentValues.getAsString("data6");
        String displayName = primaryContentValues.getAsString(ContactsHandler.PHONE_NUMBER);
        mCurrentDisplayName = displayName;
        if (!TextUtils.isEmpty(familyName) || !TextUtils.isEmpty(givenName)) {
            boolean reallyUseQuotedPrintableToName = this.mUsesQPToPrimaryProperties && (!VCardUtils.containsOnlyNonCrLfPrintableAscii(familyName) || !VCardUtils.containsOnlyNonCrLfPrintableAscii(givenName) || !VCardUtils.containsOnlyNonCrLfPrintableAscii(middleName) || !VCardUtils.containsOnlyNonCrLfPrintableAscii(prefix) || !VCardUtils.containsOnlyNonCrLfPrintableAscii(suffix));
            if (reallyUseQuotedPrintableToName) {
                encodedFamily = encodeQuotedPrintable(familyName);
                encodedGiven = encodeQuotedPrintable(givenName);
                encodedMiddle = encodeQuotedPrintable(middleName);
                encodedPrefix = encodeQuotedPrintable(prefix);
                encodedSuffix = encodeQuotedPrintable(suffix);
            } else {
                encodedFamily = escapeCharacters(familyName);
                encodedGiven = escapeCharacters(givenName);
                encodedMiddle = escapeCharacters(middleName);
                encodedPrefix = escapeCharacters(prefix);
                encodedSuffix = escapeCharacters(suffix);
            }
            builder.append(VCARD_PROPERTY_NAME);
            if (shouldAppendCharsetAttribute(Arrays.asList(familyName, givenName, middleName, prefix, suffix))) {
                builder.append(";");
                builder.append(this.mVCardAttributeCharset);
            }
            if (reallyUseQuotedPrintableToName) {
                builder.append(";");
                builder.append(VCARD_ATTR_ENCODING_QP);
            }
            builder.append(VCARD_DATA_SEPARATOR);
            builder.append(encodedFamily);
            builder.append(";");
            builder.append(encodedGiven);
            builder.append(";");
            builder.append(encodedMiddle);
            builder.append(";");
            builder.append(encodedPrefix);
            builder.append(";");
            builder.append(encodedSuffix);
            builder.append(VCARD_COL_SEPARATOR);
            if (this.mIsV30) {
                String fullname = VCardUtils.constructNameFromElements(VCardConfig.getNameOrderType(this.mVCardType), familyName, middleName, givenName, prefix, suffix);
                boolean reallyUseQuotedPrintableToFullname = this.mUsesQPToPrimaryProperties && !VCardUtils.containsOnlyNonCrLfPrintableAscii(fullname);
                if (reallyUseQuotedPrintableToFullname) {
                    encodedFullname = encodeQuotedPrintable(fullname);
                } else {
                    encodedFullname = escapeCharacters(fullname);
                }
                builder.append(VCARD_PROPERTY_FULL_NAME);
                if (shouldAppendCharsetAttribute(encodedFullname)) {
                    builder.append(";");
                    builder.append(this.mVCardAttributeCharset);
                }
                if (reallyUseQuotedPrintableToFullname) {
                    builder.append(";");
                    builder.append(VCARD_ATTR_ENCODING_QP);
                }
                builder.append(VCARD_DATA_SEPARATOR);
                builder.append(encodedFullname);
                builder.append(VCARD_COL_SEPARATOR);
            }
        } else if (!TextUtils.isEmpty(displayName)) {
            boolean reallyUseQuotedPrintableToDisplayName = this.mUsesQPToPrimaryProperties && !VCardUtils.containsOnlyNonCrLfPrintableAscii(displayName);
            if (reallyUseQuotedPrintableToDisplayName) {
                encodedDisplayName = encodeQuotedPrintable(displayName);
            } else {
                encodedDisplayName = escapeCharacters(displayName);
            }
            builder.append(VCARD_PROPERTY_NAME);
            if (shouldAppendCharsetAttribute(encodedDisplayName)) {
                builder.append(";");
                builder.append(this.mVCardAttributeCharset);
            }
            if (reallyUseQuotedPrintableToDisplayName) {
                builder.append(";");
                builder.append(VCARD_ATTR_ENCODING_QP);
            }
            builder.append(VCARD_DATA_SEPARATOR);
            builder.append(encodedDisplayName);
            builder.append(";");
            builder.append(";");
            builder.append(";");
            builder.append(";");
            builder.append(VCARD_COL_SEPARATOR);
        } else if (this.mIsDoCoMo) {
            appendVCardLine(builder, VCARD_PROPERTY_NAME, "");
        } else if (this.mIsV30) {
            appendVCardLine(builder, VCARD_PROPERTY_NAME, "");
            appendVCardLine(builder, VCARD_PROPERTY_FULL_NAME, "");
        }
        String phoneticFamilyName = primaryContentValues.getAsString("data9");
        String phoneticMiddleName = primaryContentValues.getAsString("data8");
        String phoneticGivenName = primaryContentValues.getAsString("data7");
        if (!TextUtils.isEmpty(phoneticFamilyName) || !TextUtils.isEmpty(phoneticMiddleName) || !TextUtils.isEmpty(phoneticGivenName)) {
            if (this.mIsJapaneseMobilePhone) {
                phoneticFamilyName = VCardUtils.toHalfWidthString(phoneticFamilyName);
                phoneticMiddleName = VCardUtils.toHalfWidthString(phoneticMiddleName);
                phoneticGivenName = VCardUtils.toHalfWidthString(phoneticGivenName);
            }
            if (this.mIsV30) {
                String sortString = VCardUtils.constructNameFromElements(this.mVCardType, phoneticFamilyName, phoneticMiddleName, phoneticGivenName);
                builder.append(VCARD_PROPERTY_SORT_STRING);
                String encodedSortString = escapeCharacters(sortString);
                if (shouldAppendCharsetAttribute(encodedSortString)) {
                    builder.append(";");
                    builder.append(this.mVCardAttributeCharset);
                }
                builder.append(VCARD_DATA_SEPARATOR);
                builder.append(encodedSortString);
                builder.append(VCARD_COL_SEPARATOR);
            } else {
                builder.append(VCARD_PROPERTY_SOUND);
                builder.append(";");
                builder.append(Constants.ATTR_TYPE_X_IRMC_N);
                if (this.mUsesQPToPrimaryProperties && (!VCardUtils.containsOnlyNonCrLfPrintableAscii(phoneticFamilyName) || !VCardUtils.containsOnlyNonCrLfPrintableAscii(phoneticMiddleName) || !VCardUtils.containsOnlyNonCrLfPrintableAscii(phoneticGivenName))) {
                    encodedPhoneticFamilyName2 = encodeQuotedPrintable(phoneticFamilyName);
                    encodedPhoneticMiddleName2 = encodeQuotedPrintable(phoneticMiddleName);
                    encodedPhoneticGivenName2 = encodeQuotedPrintable(phoneticGivenName);
                } else {
                    encodedPhoneticFamilyName2 = escapeCharacters(phoneticFamilyName);
                    encodedPhoneticMiddleName2 = escapeCharacters(phoneticMiddleName);
                    encodedPhoneticGivenName2 = escapeCharacters(phoneticGivenName);
                }
                if (shouldAppendCharsetAttribute(Arrays.asList(encodedPhoneticFamilyName2, encodedPhoneticMiddleName2, encodedPhoneticGivenName2))) {
                    builder.append(";");
                    builder.append(this.mVCardAttributeCharset);
                }
                builder.append(VCARD_DATA_SEPARATOR);
                builder.append(encodedPhoneticFamilyName2);
                builder.append(";");
                builder.append(encodedPhoneticGivenName2);
                builder.append(";");
                builder.append(encodedPhoneticMiddleName2);
                builder.append(";");
                builder.append(";");
                builder.append(VCARD_COL_SEPARATOR);
            }
        } else if (this.mIsDoCoMo) {
            builder.append(VCARD_PROPERTY_SOUND);
            builder.append(";");
            builder.append(Constants.ATTR_TYPE_X_IRMC_N);
            builder.append(VCARD_DATA_SEPARATOR);
            builder.append(";");
            builder.append(";");
            builder.append(";");
            builder.append(";");
            builder.append(VCARD_COL_SEPARATOR);
        }
        if (this.mUsesDefactProperty) {
            if (!TextUtils.isEmpty(phoneticGivenName)) {
                boolean reallyUseQuotedPrintable = this.mUsesQPToPrimaryProperties && !VCardUtils.containsOnlyNonCrLfPrintableAscii(phoneticGivenName);
                if (reallyUseQuotedPrintable) {
                    encodedPhoneticGivenName = encodeQuotedPrintable(phoneticGivenName);
                } else {
                    encodedPhoneticGivenName = escapeCharacters(phoneticGivenName);
                }
                builder.append(VCARD_PROPERTY_X_PHONETIC_FIRST_NAME);
                if (shouldAppendCharsetAttribute(encodedPhoneticGivenName)) {
                    builder.append(";");
                    builder.append(this.mVCardAttributeCharset);
                }
                if (reallyUseQuotedPrintable) {
                    builder.append(";");
                    builder.append(VCARD_ATTR_ENCODING_QP);
                }
                builder.append(VCARD_DATA_SEPARATOR);
                builder.append(encodedPhoneticGivenName);
                builder.append(VCARD_COL_SEPARATOR);
            }
            if (!TextUtils.isEmpty(phoneticMiddleName)) {
                boolean reallyUseQuotedPrintable2 = this.mUsesQPToPrimaryProperties && !VCardUtils.containsOnlyNonCrLfPrintableAscii(phoneticMiddleName);
                if (reallyUseQuotedPrintable2) {
                    encodedPhoneticMiddleName = encodeQuotedPrintable(phoneticMiddleName);
                } else {
                    encodedPhoneticMiddleName = escapeCharacters(phoneticMiddleName);
                }
                builder.append(VCARD_PROPERTY_X_PHONETIC_MIDDLE_NAME);
                if (shouldAppendCharsetAttribute(encodedPhoneticMiddleName)) {
                    builder.append(";");
                    builder.append(this.mVCardAttributeCharset);
                }
                if (reallyUseQuotedPrintable2) {
                    builder.append(";");
                    builder.append(VCARD_ATTR_ENCODING_QP);
                }
                builder.append(VCARD_DATA_SEPARATOR);
                builder.append(encodedPhoneticMiddleName);
                builder.append(VCARD_COL_SEPARATOR);
            }
            if (!TextUtils.isEmpty(phoneticFamilyName)) {
                boolean reallyUseQuotedPrintable3 = this.mUsesQPToPrimaryProperties && !VCardUtils.containsOnlyNonCrLfPrintableAscii(phoneticFamilyName);
                if (reallyUseQuotedPrintable3) {
                    encodedPhoneticFamilyName = encodeQuotedPrintable(phoneticFamilyName);
                } else {
                    encodedPhoneticFamilyName = escapeCharacters(phoneticFamilyName);
                }
                builder.append(VCARD_PROPERTY_X_PHONETIC_LAST_NAME);
                if (shouldAppendCharsetAttribute(encodedPhoneticFamilyName)) {
                    builder.append(";");
                    builder.append(this.mVCardAttributeCharset);
                }
                if (reallyUseQuotedPrintable3) {
                    builder.append(";");
                    builder.append(VCARD_ATTR_ENCODING_QP);
                }
                builder.append(VCARD_DATA_SEPARATOR);
                builder.append(encodedPhoneticFamilyName);
                builder.append(VCARD_COL_SEPARATOR);
            }
        }
    }

    private void appendNickNames(StringBuilder builder, Map<String, List<ContentValues>> contentValuesListMap) {
        String propertyNickname;
        String encodedNickname;
        List<ContentValues> contentValuesList = contentValuesListMap.get("vnd.android.cursor.item/nickname");
        if (contentValuesList != null) {
            if (this.mIsV30) {
                propertyNickname = VCARD_PROPERTY_NICKNAME;
            } else if (this.mUsesAndroidProperty) {
                propertyNickname = VCARD_PROPERTY_X_NICKNAME;
            } else {
                return;
            }
            for (ContentValues contentValues : contentValuesList) {
                String nickname = contentValues.getAsString(ContactsHandler.PHONE_NUMBER);
                if (!TextUtils.isEmpty(nickname)) {
                    boolean reallyUseQuotedPrintable = this.mUsesQuotedPrintable && !VCardUtils.containsOnlyNonCrLfPrintableAscii(nickname);
                    if (reallyUseQuotedPrintable) {
                        encodedNickname = encodeQuotedPrintable(nickname);
                    } else {
                        encodedNickname = escapeCharacters(nickname);
                    }
                    builder.append(propertyNickname);
                    if (shouldAppendCharsetAttribute(propertyNickname)) {
                        builder.append(";");
                        builder.append(this.mVCardAttributeCharset);
                    }
                    if (reallyUseQuotedPrintable) {
                        builder.append(";");
                        builder.append(VCARD_ATTR_ENCODING_QP);
                    }
                    builder.append(VCARD_DATA_SEPARATOR);
                    builder.append(encodedNickname);
                    builder.append(VCARD_COL_SEPARATOR);
                }
            }
        }
    }

    private void appendPhones(StringBuilder builder, Map<String, List<ContentValues>> contentValuesListMap) {
        List<ContentValues> contentValuesList = contentValuesListMap.get("vnd.android.cursor.item/phone_v2");
        boolean phoneLineExists = false;
        boolean isPrimaryMark = false;
        if (contentValuesList != null) {
            HashSet hashSet = new HashSet();
            for (ContentValues contentValues : contentValuesList) {
                Integer typeAsObject = contentValues.getAsInteger("data2");
                String label = contentValues.getAsString("data3");
                String phoneNumber = contentValues.getAsString(ContactsHandler.PHONE_NUMBER);
                if (phoneNumber != null) {
                    phoneNumber = phoneNumber.trim();
                }
                if (!TextUtils.isEmpty(phoneNumber)) {
                    int type = typeAsObject != null ? typeAsObject.intValue() : 1;
                    boolean primary = !TextUtils.isEmpty(contentValues.getAsString("is_primary"));
                    if (isPrimaryMark) {
                        primary = false;
                    } else {
                        isPrimaryMark = primary;
                    }
                    phoneLineExists = true;
                    if (type == 6) {
                        phoneLineExists = true;
                        if (!hashSet.contains(phoneNumber)) {
                            hashSet.add(phoneNumber);
                            appendVCardTelephoneLine(builder, Integer.valueOf(type), label, phoneNumber, primary);
                        }
                    } else {
                        List<String> phoneNumberList = splitIfSeveralPhoneNumbersExist(phoneNumber);
                        if (!phoneNumberList.isEmpty()) {
                            phoneLineExists = true;
                            for (String actualPhoneNumber : phoneNumberList) {
                                if (!hashSet.contains(actualPhoneNumber)) {
                                    hashSet.add(actualPhoneNumber);
                                    appendVCardTelephoneLine(builder, Integer.valueOf(type), label, actualPhoneNumber, primary);
                                    primary = false;
                                }
                            }
                        }
                    }
                }
            }
        }
        if (!phoneLineExists && this.mIsDoCoMo) {
            appendVCardTelephoneLine(builder, 1, "", "", false);
        }
    }

    private List<String> splitIfSeveralPhoneNumbersExist(String phoneNumber) {
        List<String> phoneList = new ArrayList<>();
        StringBuilder builder = new StringBuilder();
        int length = phoneNumber.length();
        for (int i = 0; i < length; i++) {
            char ch = phoneNumber.charAt(i);
            if ((ch == ';' || ch == 10) && builder.length() > 0) {
                phoneList.add(builder.toString());
                builder = new StringBuilder();
            } else if (ch != '-') {
                builder.append(ch);
            }
        }
        if (builder.length() > 0) {
            phoneList.add(builder.toString());
        }
        return phoneList;
    }

    private void appendEmails(StringBuilder builder, Map<String, List<ContentValues>> contentValuesListMap) {
        List<ContentValues> contentValuesList = contentValuesListMap.get("vnd.android.cursor.item/email_v2");
        boolean emailAddressExists = false;
        if (contentValuesList != null) {
            Set<String> addressSet = new HashSet<>();
            for (ContentValues contentValues : contentValuesList) {
                Integer typeAsObject = contentValues.getAsInteger("data2");
                int type = typeAsObject != null ? typeAsObject.intValue() : 3;
                String label = contentValues.getAsString("data3");
                String emailAddress = contentValues.getAsString(ContactsHandler.PHONE_NUMBER);
                if (emailAddress != null) {
                    emailAddress = emailAddress.trim();
                }
                if (!TextUtils.isEmpty(emailAddress)) {
                    emailAddressExists = true;
                    if (!addressSet.contains(emailAddress)) {
                        addressSet.add(emailAddress);
                        appendVCardEmailLine(builder, Integer.valueOf(type), label, emailAddress);
                    }
                }
            }
        }
        if (!emailAddressExists && this.mIsDoCoMo) {
            appendVCardEmailLine(builder, 1, "", "");
        }
    }

    private void appendPostals(StringBuilder builder, Map<String, List<ContentValues>> contentValuesListMap) {
        List<ContentValues> contentValuesList = contentValuesListMap.get("vnd.android.cursor.item/postal-address_v2");
        if (contentValuesList != null) {
            if (this.mIsDoCoMo) {
                appendPostalsForDoCoMo(builder, contentValuesList);
            } else {
                appendPostalsForGeneric(builder, contentValuesList);
            }
        } else if (this.mIsDoCoMo) {
            builder.append(VCARD_PROPERTY_ADR);
            builder.append(";");
            builder.append(Constants.ATTR_TYPE_HOME);
            builder.append(VCARD_DATA_SEPARATOR);
            builder.append(VCARD_COL_SEPARATOR);
        }
    }

    private void appendPostalsForDoCoMo(StringBuilder builder, List<ContentValues> contentValuesList) {
        if (!appendPostalsForDoCoMoInternal(builder, contentValuesList, 1) && !appendPostalsForDoCoMoInternal(builder, contentValuesList, 2) && !appendPostalsForDoCoMoInternal(builder, contentValuesList, 3) && appendPostalsForDoCoMoInternal(builder, contentValuesList, 0)) {
        }
    }

    private boolean appendPostalsForDoCoMoInternal(StringBuilder builder, List<ContentValues> contentValuesList, Integer preferedType) {
        for (ContentValues contentValues : contentValuesList) {
            Integer type = contentValues.getAsInteger("data2");
            String label = contentValues.getAsString("data3");
            if (type == preferedType) {
                appendVCardPostalLine(builder, type, label, contentValues);
                return true;
            }
        }
        return false;
    }

    private void appendPostalsForGeneric(StringBuilder builder, List<ContentValues> contentValuesList) {
        for (ContentValues contentValues : contentValuesList) {
            Integer type = contentValues.getAsInteger("data2");
            String label = contentValues.getAsString("data3");
            if (type != null) {
                appendVCardPostalLine(builder, type, label, contentValues);
            }
        }
    }

    private void appendIms(StringBuilder builder, Map<String, List<ContentValues>> contentValuesListMap) {
        List<ContentValues> contentValuesList = contentValuesListMap.get("vnd.android.cursor.item/im");
        if (contentValuesList != null) {
            for (ContentValues contentValues : contentValuesList) {
                Integer protocol = contentValues.getAsInteger("data5");
                String data = contentValues.getAsString(ContactsHandler.PHONE_NUMBER);
                String custom = contentValues.getAsString("data6");
                if (data != null) {
                    data = data.trim();
                }
                if (!TextUtils.isEmpty(data) && protocol != null && VCardConfig.usesAndroidSpecificProperty(this.mVCardType)) {
                    switch (protocol.intValue()) {
                        case -1:
                            if (this.mUsesAndroidProperty && !TextUtils.isEmpty(custom)) {
                                if (!TextUtils.isGraphic(custom)) {
                                    appendVCardLine(builder, Constants.PROPERTY_X_IM_MARK, data);
                                    break;
                                } else {
                                    appendVCardLine(builder, Constants.PROPERTY_X_IM_MARK + custom, data);
                                    break;
                                }
                            }
                        case 0:
                            appendVCardLine(builder, Constants.PROPERTY_X_AIM, data);
                            continue;
                        case 1:
                            appendVCardLine(builder, Constants.PROPERTY_X_MSN, data);
                            continue;
                        case 2:
                            appendVCardLine(builder, Constants.PROPERTY_X_YAHOO, data);
                            continue;
                        case 3:
                            appendVCardLine(builder, Constants.PROPERTY_X_SKYPE_USERNAME, data);
                            continue;
                        case 4:
                            appendVCardLine(builder, Constants.PROPERTY_X_QQ, data);
                            continue;
                        case 5:
                            appendVCardLine(builder, Constants.PROPERTY_X_GOOGLE_TALK, data);
                            continue;
                        case 6:
                            appendVCardLine(builder, Constants.PROPERTY_X_ICQ, data);
                            continue;
                        case 7:
                            appendVCardLine(builder, Constants.PROPERTY_X_JABBER, data);
                            continue;
                        case 8:
                            appendVCardLine(builder, Constants.PROPERTY_X_NETMEETING, data);
                            continue;
                        default:
                            CommonMethod.logDebug1("netqin", "appendIms:" + protocol + " : " + data);
                            continue;
                    }
                }
            }
        }
    }

    private void appendWebsites(StringBuilder builder, Map<String, List<ContentValues>> contentValuesListMap) {
        List<ContentValues> contentValuesList = contentValuesListMap.get("vnd.android.cursor.item/website");
        if (contentValuesList != null) {
            for (ContentValues contentValues : contentValuesList) {
                String website = contentValues.getAsString(ContactsHandler.PHONE_NUMBER);
                if (website != null) {
                    website = website.trim();
                }
                if (!TextUtils.isEmpty(website)) {
                    appendVCardLine(builder, "URL", website);
                }
            }
        }
    }

    private void appendBirthday(StringBuilder builder, Map<String, List<ContentValues>> contentValuesListMap) {
        Integer eventType;
        List<ContentValues> contentValuesList = contentValuesListMap.get("vnd.android.cursor.item/contact_event");
        if (contentValuesList != null && contentValuesList.size() > 0 && (eventType = ((ContentValues) contentValuesList.get(0)).getAsInteger("data2")) != null && eventType.equals(3)) {
            String birthday = ((ContentValues) contentValuesList.get(0)).getAsString(ContactsHandler.PHONE_NUMBER);
            if (birthday != null) {
                birthday = birthday.trim();
            }
            if (!TextUtils.isEmpty(birthday)) {
                appendVCardLine(builder, VCARD_PROPERTY_BIRTHDAY, birthday);
            }
        }
    }

    private void appendOrganizations(StringBuilder builder, Map<String, List<ContentValues>> contentValuesListMap) {
        List<ContentValues> contentValuesList = contentValuesListMap.get("vnd.android.cursor.item/organization");
        if (contentValuesList != null) {
            for (ContentValues contentValues : contentValuesList) {
                String company = contentValues.getAsString(ContactsHandler.PHONE_NUMBER);
                if (company != null) {
                    company = company.trim();
                }
                String title = contentValues.getAsString("data4");
                if (title != null) {
                    title = title.trim();
                }
                if (!TextUtils.isEmpty(company)) {
                    appendVCardLine(builder, VCARD_PROPERTY_ORG, company, !VCardUtils.containsOnlyPrintableAscii(company), this.mUsesQuotedPrintable && !VCardUtils.containsOnlyNonCrLfPrintableAscii(company));
                }
                if (!TextUtils.isEmpty(title)) {
                    appendVCardLine(builder, VCARD_PROPERTY_TITLE, title, !VCardUtils.containsOnlyPrintableAscii(title), this.mUsesQuotedPrintable && !VCardUtils.containsOnlyNonCrLfPrintableAscii(title));
                }
            }
        }
    }

    private void appendPhotos(StringBuilder builder, Map<String, List<ContentValues>> contentValuesListMap) {
        String photoType;
        List<ContentValues> contentValuesList = contentValuesListMap.get("vnd.android.cursor.item/photo");
        if (contentValuesList != null) {
            for (ContentValues contentValues : contentValuesList) {
                byte[] data = contentValues.getAsByteArray("data15");
                if (data != null) {
                    if (data.length >= 3 && data[0] == 71 && data[1] == 73 && data[2] == 70) {
                        photoType = "GIF";
                    } else if (data.length >= 4 && data[0] == -119 && data[1] == 80 && data[2] == 78 && data[3] == 71) {
                        photoType = "PNG";
                    } else if (data.length >= 2 && data[0] == -1 && data[1] == -40) {
                        photoType = "JPEG";
                    } else {
                        CommonMethod.logDebug1(LOG_TAG, "Unknown photo type. Ignore.");
                    }
                    String photoString = VCardUtils.encodeBase64(data);
                    if (photoString.length() > 0) {
                        appendVCardPhotoLine(builder, photoString, photoType);
                    }
                }
            }
        }
    }

    private void appendNotes(StringBuilder builder, Map<String, List<ContentValues>> contentValuesListMap) {
        List<ContentValues> contentValuesList = contentValuesListMap.get("vnd.android.cursor.item/note");
        if (contentValuesList == null) {
            return;
        }
        if (this.mOnlyOneNoteFieldIsAvailable) {
            StringBuilder noteBuilder = new StringBuilder();
            boolean first = true;
            for (ContentValues contentValues : contentValuesList) {
                String note = contentValues.getAsString(ContactsHandler.PHONE_NUMBER);
                if (note == null) {
                    note = "";
                }
                if (note.length() > 0) {
                    if (first) {
                        first = false;
                    } else {
                        noteBuilder.append(10);
                    }
                    noteBuilder.append(note);
                }
            }
            String noteStr = noteBuilder.toString();
            appendVCardLine(builder, VCARD_PROPERTY_NOTE, noteStr, !VCardUtils.containsOnlyPrintableAscii(noteStr), this.mUsesQuotedPrintable && !VCardUtils.containsOnlyNonCrLfPrintableAscii(noteStr));
            return;
        }
        for (ContentValues contentValues2 : contentValuesList) {
            String noteStr2 = contentValues2.getAsString(ContactsHandler.PHONE_NUMBER);
            if (!TextUtils.isEmpty(noteStr2)) {
                appendVCardLine(builder, VCARD_PROPERTY_NOTE, noteStr2, !VCardUtils.containsOnlyPrintableAscii(noteStr2), this.mUsesQuotedPrintable && !VCardUtils.containsOnlyNonCrLfPrintableAscii(noteStr2));
            }
        }
    }

    private String escapeCharacters(String unescaped) {
        if (TextUtils.isEmpty(unescaped)) {
            return "";
        }
        StringBuilder tmpBuilder = new StringBuilder();
        int length = unescaped.length();
        for (int i = 0; i < length; i++) {
            char ch = unescaped.charAt(i);
            switch (ch) {
                case 13:
                    if (i + 1 < length && unescaped.charAt(i) == 10) {
                        break;
                    }
                case 10:
                    tmpBuilder.append("\\n");
                    break;
                case ',':
                    if (!this.mIsV30) {
                        tmpBuilder.append(ch);
                        break;
                    } else {
                        tmpBuilder.append("\\,");
                        break;
                    }
                case ';':
                    tmpBuilder.append('\\');
                    tmpBuilder.append(';');
                    break;
                case '\\':
                    if (this.mIsV30) {
                        tmpBuilder.append("\\\\");
                        break;
                    }
                case '<':
                case '>':
                    if (!this.mIsDoCoMo) {
                        tmpBuilder.append(ch);
                        break;
                    } else {
                        tmpBuilder.append('\\');
                        tmpBuilder.append(ch);
                        break;
                    }
                default:
                    tmpBuilder.append(ch);
                    break;
            }
        }
        return tmpBuilder.toString();
    }

    private void appendVCardPhotoLine(StringBuilder builder, String encodedData, String photoType) {
        StringBuilder tmpBuilder = new StringBuilder();
        tmpBuilder.append(VCARD_PROPERTY_PHOTO);
        tmpBuilder.append(";");
        if (this.mIsV30) {
            tmpBuilder.append(VCARD_ATTR_ENCODING_BASE64_V30);
        } else {
            tmpBuilder.append(VCARD_ATTR_ENCODING_BASE64_V21);
        }
        tmpBuilder.append(";");
        appendTypeAttribute(tmpBuilder, photoType);
        tmpBuilder.append(VCARD_DATA_SEPARATOR);
        tmpBuilder.append(encodedData);
        String tmpStr = tmpBuilder.toString();
        StringBuilder tmpBuilder2 = new StringBuilder();
        int lineCount = 0;
        int length = tmpStr.length();
        for (int i = 0; i < length; i++) {
            tmpBuilder2.append(tmpStr.charAt(i));
            lineCount++;
            if (lineCount > 72) {
                tmpBuilder2.append(VCARD_COL_SEPARATOR);
                tmpBuilder2.append(VCARD_WS);
                lineCount = 0;
            }
        }
        builder.append(tmpBuilder2.toString());
        builder.append(VCARD_COL_SEPARATOR);
        builder.append(VCARD_COL_SEPARATOR);
    }

    private void appendVCardPostalLine(StringBuilder builder, Integer typeAsObject, String label, ContentValues contentValues) {
        int typeAsPrimitive;
        builder.append(VCARD_PROPERTY_ADR);
        builder.append(";");
        boolean dataExists = false;
        String[] dataArray = VCardUtils.getVCardPostalElements(contentValues);
        boolean actuallyUseQuotedPrintable = false;
        boolean shouldAppendCharset = false;
        int length = dataArray.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                break;
            }
            String data = dataArray[i];
            if (!TextUtils.isEmpty(data)) {
                dataExists = true;
                if (!shouldAppendCharset && !VCardUtils.containsOnlyPrintableAscii(data)) {
                    shouldAppendCharset = true;
                }
                if (this.mUsesQuotedPrintable && !VCardUtils.containsOnlyNonCrLfPrintableAscii(data)) {
                    actuallyUseQuotedPrintable = true;
                    break;
                }
            }
            i++;
        }
        int length2 = dataArray.length;
        for (int i2 = 0; i2 < length2; i2++) {
            String data2 = dataArray[i2];
            if (!TextUtils.isEmpty(data2)) {
                if (actuallyUseQuotedPrintable) {
                    dataArray[i2] = encodeQuotedPrintable(data2);
                } else {
                    dataArray[i2] = escapeCharacters(data2);
                }
            }
        }
        if (typeAsObject == null) {
            typeAsPrimitive = 3;
        } else {
            typeAsPrimitive = typeAsObject.intValue();
        }
        String typeAsString = null;
        switch (typeAsPrimitive) {
            case 0:
                if (this.mUsesAndroidProperty && !TextUtils.isEmpty(label) && VCardUtils.containsOnlyAlphaDigitHyphen(label)) {
                    builder.append("X-");
                    builder.append(label);
                    builder.append(";");
                    break;
                }
            case 1:
                typeAsString = Constants.ATTR_TYPE_HOME;
                break;
            case 2:
                typeAsString = Constants.ATTR_TYPE_WORK;
                break;
        }
        boolean shouldAppendAttrSeparator = false;
        if (typeAsString != null) {
            appendTypeAttribute(builder, typeAsString);
            shouldAppendAttrSeparator = true;
        }
        if (dataExists) {
            if (shouldAppendCharset) {
                if (shouldAppendAttrSeparator) {
                    builder.append(";");
                }
                builder.append(this.mVCardAttributeCharset);
                shouldAppendAttrSeparator = true;
            }
            if (actuallyUseQuotedPrintable) {
                if (shouldAppendAttrSeparator) {
                    builder.append(";");
                }
                builder.append(VCARD_ATTR_ENCODING_QP);
            }
        }
        builder.append(VCARD_DATA_SEPARATOR);
        if (dataExists) {
            builder.append(dataArray[0]);
            builder.append(";");
            builder.append(dataArray[1]);
            builder.append(";");
            builder.append(dataArray[2]);
            builder.append(";");
            builder.append(dataArray[3]);
            builder.append(";");
            builder.append(dataArray[4]);
            builder.append(";");
            builder.append(dataArray[5]);
            builder.append(";");
            builder.append(dataArray[6]);
        }
        builder.append(VCARD_COL_SEPARATOR);
    }

    private void appendVCardEmailLine(StringBuilder builder, Integer typeAsObject, String label, String data) {
        int typeAsPrimitive;
        String typeAsString;
        builder.append(VCARD_PROPERTY_EMAIL);
        if (typeAsObject == null) {
            typeAsPrimitive = 3;
        } else {
            typeAsPrimitive = typeAsObject.intValue();
        }
        switch (typeAsPrimitive) {
            case 0:
                if (this.mUsesAndroidProperty && !TextUtils.isEmpty(label) && VCardUtils.containsOnlyAlphaDigitHyphen(label)) {
                    typeAsString = "X-" + label;
                    break;
                } else {
                    typeAsString = "INTERNET";
                    break;
                }
            case 1:
                typeAsString = Constants.ATTR_TYPE_HOME;
                break;
            case 2:
                typeAsString = Constants.ATTR_TYPE_WORK;
                break;
            case 3:
                typeAsString = "INTERNET";
                break;
            case 4:
                typeAsString = Constants.ATTR_TYPE_CELL;
                break;
            default:
                typeAsString = "INTERNET";
                break;
        }
        builder.append(";");
        appendTypeAttribute(builder, typeAsString);
        builder.append(VCARD_DATA_SEPARATOR);
        builder.append(data);
        builder.append(VCARD_COL_SEPARATOR);
    }

    private void appendVCardTelephoneLine(StringBuilder builder, Integer typeAsObject, String label, String encodedData, boolean is_primary) {
        int typeAsPrimitive;
        builder.append(VCARD_PROPERTY_TEL);
        builder.append(";");
        if (typeAsObject == null) {
            typeAsPrimitive = 7;
        } else {
            typeAsPrimitive = typeAsObject.intValue();
        }
        if (is_primary) {
            builder.append(Constants.ATTR_TYPE_PREF);
            builder.append(";");
        }
        switch (typeAsPrimitive) {
            case 0:
                if (this.mUsesAndroidProperty && !TextUtils.isEmpty(label) && VCardUtils.containsOnlyAlphaDigitHyphen(label)) {
                    appendTypeAttribute(builder, "X-" + label);
                    break;
                } else {
                    appendTypeAttribute(builder, Constants.ATTR_TYPE_VOICE);
                    break;
                }
            case 1:
                appendTypeAttributes(builder, Arrays.asList(Constants.ATTR_TYPE_HOME, Constants.ATTR_TYPE_VOICE));
                break;
            case 2:
                builder.append(Constants.ATTR_TYPE_CELL);
                break;
            case 3:
                appendTypeAttributes(builder, Arrays.asList(Constants.ATTR_TYPE_WORK, Constants.ATTR_TYPE_VOICE));
                break;
            case 4:
                appendTypeAttributes(builder, Arrays.asList(Constants.ATTR_TYPE_WORK, Constants.ATTR_TYPE_FAX));
                break;
            case 5:
                appendTypeAttributes(builder, Arrays.asList(Constants.ATTR_TYPE_HOME, Constants.ATTR_TYPE_FAX));
                break;
            case 6:
                if (!this.mIsDoCoMo) {
                    appendTypeAttribute(builder, Constants.ATTR_TYPE_PAGER);
                    break;
                } else {
                    builder.append(Constants.ATTR_TYPE_VOICE);
                    break;
                }
            case 7:
                appendTypeAttribute(builder, Constants.ATTR_TYPE_VOICE);
                break;
            default:
                appendUncommonPhoneType(builder, Integer.valueOf(typeAsPrimitive));
                break;
        }
        builder.append(VCARD_DATA_SEPARATOR);
        builder.append(encodedData);
        builder.append(VCARD_COL_SEPARATOR);
    }

    private void appendUncommonPhoneType(StringBuilder builder, Integer type) {
        if (this.mIsDoCoMo) {
            builder.append(Constants.ATTR_TYPE_VOICE);
            return;
        }
        String phoneAttribute = VCardUtils.getPhoneAttributeString(type);
        if (phoneAttribute != null) {
            appendTypeAttribute(builder, phoneAttribute);
        }
    }

    private void appendVCardLine(StringBuilder builder, String propertyName, String rawData) {
        appendVCardLine(builder, propertyName, rawData, false, false);
    }

    private void appendVCardLine(StringBuilder builder, String field, String rawData, boolean needCharset, boolean needQuotedPrintable) {
        String encodedData;
        builder.append(field);
        if (needCharset) {
            builder.append(";");
            builder.append(this.mVCardAttributeCharset);
        }
        if (needQuotedPrintable) {
            builder.append(";");
            builder.append(VCARD_ATTR_ENCODING_QP);
            encodedData = encodeQuotedPrintable(rawData);
        } else {
            encodedData = escapeCharacters(rawData);
        }
        builder.append(VCARD_DATA_SEPARATOR);
        builder.append(encodedData);
        builder.append(VCARD_COL_SEPARATOR);
    }

    private void appendTypeAttributes(StringBuilder builder, List<String> types) {
        boolean first = true;
        for (String type : types) {
            if (first) {
                first = false;
            } else {
                builder.append(";");
            }
            appendTypeAttribute(builder, type);
        }
    }

    private void appendTypeAttribute(StringBuilder builder, String type) {
        if (this.mIsV30) {
            builder.append(Constants.ATTR_TYPE).append(VCARD_ATTR_EQUAL);
        }
        builder.append(type);
    }

    private boolean shouldAppendCharsetAttribute(String propertyValue) {
        return !VCardUtils.containsOnlyPrintableAscii(propertyValue) && (!this.mIsV30 || !this.mUsesUtf8);
    }

    private boolean shouldAppendCharsetAttribute(List<String> propertyValueList) {
        boolean shouldAppendBasically = false;
        Iterator<String> it = propertyValueList.iterator();
        while (true) {
            if (it.hasNext()) {
                if (!VCardUtils.containsOnlyPrintableAscii(it.next())) {
                    shouldAppendBasically = true;
                    break;
                }
            } else {
                break;
            }
        }
        if (!shouldAppendBasically || (this.mIsV30 && this.mUsesUtf8)) {
            return false;
        }
        return true;
    }

    private String encodeQuotedPrintable(String str) {
        byte[] strArray;
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        StringBuilder tmpBuilder = new StringBuilder();
        int length = str.length();
        int i = 0;
        while (i < length) {
            char ch = str.charAt(i);
            if (ch == 13) {
                if (i + 1 < length && str.charAt(i + 1) == 10) {
                    i++;
                }
                tmpBuilder.append(VCARD_COL_SEPARATOR);
            } else if (ch == 10) {
                tmpBuilder.append(VCARD_COL_SEPARATOR);
            } else {
                tmpBuilder.append(ch);
            }
            i++;
        }
        String str2 = tmpBuilder.toString();
        StringBuilder tmpBuilder2 = new StringBuilder();
        int index = 0;
        int lineCount = 0;
        try {
            strArray = str2.getBytes(this.mCharsetString);
        } catch (UnsupportedEncodingException e) {
            strArray = str2.getBytes();
        }
        while (index < strArray.length) {
            tmpBuilder2.append(String.format("=%02X", Byte.valueOf(strArray[index])));
            index++;
            lineCount += 3;
            if (lineCount >= 67) {
                tmpBuilder2.append("=\r\n");
                lineCount = 0;
            }
        }
        return tmpBuilder2.toString();
    }

    public static void initCurrentDisplayName() {
        mCurrentDisplayName = "";
    }

    public static String getCurrentDisplayName() {
        return mCurrentDisplayName;
    }
}
