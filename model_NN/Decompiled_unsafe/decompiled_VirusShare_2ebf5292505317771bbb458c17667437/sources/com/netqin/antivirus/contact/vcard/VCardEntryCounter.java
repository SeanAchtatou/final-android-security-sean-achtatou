package com.netqin.antivirus.contact.vcard;

import java.util.List;

public class VCardEntryCounter implements VCardBuilder {
    private int mCount;

    public int getCount() {
        return this.mCount;
    }

    public void start() {
    }

    public void end() {
    }

    public void startRecord(String type) {
    }

    public void endRecord() {
        this.mCount++;
    }

    public void startProperty() {
    }

    public void endProperty() {
    }

    public void propertyGroup(String group) {
    }

    public void propertyName(String name) {
    }

    public void propertyParamType(String type) {
    }

    public void propertyParamValue(String value) {
    }

    public void propertyValues(List<String> list) {
    }
}
