package com.netqin.antivirus.contact.vcard;

public class VCardNestedException extends VCardNotSupportedException {
    public VCardNestedException() {
    }

    public VCardNestedException(String message) {
        super(message);
    }
}
