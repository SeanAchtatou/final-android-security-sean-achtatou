package com.netqin.antivirus.contact;

import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.text.TextUtils;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.common.ProgressTextDialog;
import com.netqin.antivirus.contact.vcard.ContactStruct;
import com.netqin.antivirus.contact.vcard.EntryCommitter;
import com.netqin.antivirus.contact.vcard.VCardBuilderCollection;
import com.netqin.antivirus.contact.vcard.VCardConfig;
import com.netqin.antivirus.contact.vcard.VCardDataBuilder;
import com.netqin.antivirus.contact.vcard.VCardEntryCounter;
import com.netqin.antivirus.contact.vcard.VCardNestedException;
import com.netqin.antivirus.contact.vcard.VCardParser_V21;
import com.netqin.antivirus.contact.vcard.VCardSourceDetector;
import java.util.Arrays;

public class VCardReadThread extends Thread implements DialogInterface.OnCancelListener {
    private static final String LOG_TAG = "netqin";
    private static final int RUN_STATE_BEGIN = 0;
    private static final int RUN_STATE_CANCEL = 1;
    private static final int RUN_STATE_END = 2;
    private static final int RUN_STATE_ERROR = 3;
    private VCardDataBuilder mBuilder = null;
    private boolean mCanceled;
    private String mCanonicalPath;
    /* access modifiers changed from: private */
    public Context mContext = null;
    private Handler mHandler = null;
    private ProgressTextDialog mProgressTextDialog = null;
    private ContentResolver mResolver;
    private int mRunState = 0;
    private VCardParser_V21 mVCardParser;
    private PowerManager.WakeLock mWakeLock;

    public VCardReadThread(String canonicalPath, Context cnt, Handler handler, ProgressTextDialog prog) {
        this.mCanonicalPath = canonicalPath;
        this.mContext = cnt;
        this.mHandler = handler;
        this.mProgressTextDialog = prog;
        init();
    }

    private void init() {
        this.mResolver = this.mContext.getContentResolver();
        this.mWakeLock = ((PowerManager) this.mContext.getSystemService("power")).newWakeLock(536870918, LOG_TAG);
    }

    public void finalize() {
        if (this.mWakeLock != null && this.mWakeLock.isHeld()) {
            this.mWakeLock.release();
        }
    }

    public void run() {
        boolean result;
        this.mRunState = 0;
        this.mWakeLock.acquire();
        String errorReason = "";
        try {
            ContactStruct.initCompletedItems();
            ContactStruct.initCurrentDisplayName();
            ContactStruct.setRunMark(true);
            if (!TextUtils.isEmpty(this.mCanonicalPath)) {
                CommonMethod.sendUserMessage(this.mHandler, 6);
                VCardEntryCounter counter = new VCardEntryCounter();
                VCardSourceDetector detector = new VCardSourceDetector();
                try {
                    result = readOneVCardFile(this.mCanonicalPath, VCardConfig.DEFAULT_CHARSET, new VCardBuilderCollection(Arrays.asList(counter, detector)), null, true);
                } catch (VCardNestedException e) {
                    result = readOneVCardFile(this.mCanonicalPath, VCardConfig.DEFAULT_CHARSET, counter, detector, false);
                }
                if (!result) {
                    this.mWakeLock.release();
                    int itemCount = ContactStruct.getCompletedItems();
                    Message msg = new Message();
                    msg.what = 12;
                    msg.arg2 = itemCount;
                    switch (this.mRunState) {
                        case 0:
                            msg.arg1 = 21;
                            break;
                        case 1:
                            msg.arg1 = 25;
                            break;
                        case 2:
                            msg.arg1 = 30;
                            break;
                        case 3:
                            msg.arg1 = 35;
                            msg.obj = errorReason;
                            break;
                        default:
                            msg.arg1 = 21;
                            break;
                    }
                    this.mHandler.sendMessage(msg);
                    return;
                }
                CommonMethod.sendUserMessage(this.mHandler, 8, counter.getCount());
                String charset = detector.getEstimatedCharset();
                if (TextUtils.isEmpty(charset)) {
                    charset = StringEncodings.UTF8;
                }
                doActuallyReadOneVCard(this.mCanonicalPath, charset, detector);
            }
        } catch (VCardNestedException e2) {
            result = false;
            this.mRunState = 3;
            errorReason = "Fail: readOneVCardFile";
        } catch (Throwable th) {
            this.mWakeLock.release();
            int itemCount2 = ContactStruct.getCompletedItems();
            Message msg2 = new Message();
            msg2.what = 12;
            msg2.arg2 = itemCount2;
            switch (this.mRunState) {
                case 0:
                    msg2.arg1 = 21;
                    break;
                case 1:
                    msg2.arg1 = 25;
                    break;
                case 2:
                    msg2.arg1 = 30;
                    break;
                case 3:
                    msg2.arg1 = 35;
                    msg2.obj = errorReason;
                    break;
                default:
                    msg2.arg1 = 21;
                    break;
            }
            this.mHandler.sendMessage(msg2);
            throw th;
        }
        if (this.mRunState != 1) {
            this.mRunState = 2;
        }
        this.mWakeLock.release();
        int itemCount3 = ContactStruct.getCompletedItems();
        Message msg3 = new Message();
        msg3.what = 12;
        msg3.arg2 = itemCount3;
        switch (this.mRunState) {
            case 0:
                msg3.arg1 = 21;
                break;
            case 1:
                msg3.arg1 = 25;
                break;
            case 2:
                msg3.arg1 = 30;
                break;
            case 3:
                msg3.arg1 = 35;
                msg3.obj = errorReason;
                break;
            default:
                msg3.arg1 = 21;
                break;
        }
        this.mHandler.sendMessage(msg3);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.netqin.antivirus.contact.vcard.VCardDataBuilder.<init>(java.lang.String, java.lang.String, boolean, int):void
     arg types: [java.lang.String, java.lang.String, int, int]
     candidates:
      com.netqin.antivirus.contact.vcard.VCardDataBuilder.<init>(java.lang.String, boolean, int, android.accounts.Account):void
      com.netqin.antivirus.contact.vcard.VCardDataBuilder.<init>(java.lang.String, java.lang.String, boolean, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.netqin.antivirus.contact.vcard.VCardDataBuilder.<init>(java.lang.String, java.lang.String, boolean, int):void
     arg types: [?[OBJECT, ARRAY], ?[OBJECT, ARRAY], int, int]
     candidates:
      com.netqin.antivirus.contact.vcard.VCardDataBuilder.<init>(java.lang.String, boolean, int, android.accounts.Account):void
      com.netqin.antivirus.contact.vcard.VCardDataBuilder.<init>(java.lang.String, java.lang.String, boolean, int):void */
    private boolean doActuallyReadOneVCard(String canonicalPath, String charset, VCardSourceDetector detector) {
        int vcardType = VCardConfig.getVCardTypeFromString("default");
        if (charset != null) {
            this.mBuilder = new VCardDataBuilder(charset, charset, false, vcardType);
        } else {
            charset = VCardConfig.DEFAULT_CHARSET;
            this.mBuilder = new VCardDataBuilder((String) null, (String) null, false, vcardType);
        }
        this.mBuilder.addEntryHandler(new EntryCommitter(this.mResolver));
        if (this.mProgressTextDialog != null) {
            this.mBuilder.addEntryHandler(new ProgressShower(this.mProgressTextDialog, this.mContext, this.mHandler));
        }
        try {
            if (!readOneVCardFile(canonicalPath, charset, this.mBuilder, detector, false)) {
                return false;
            }
        } catch (VCardNestedException e) {
        }
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:33:0x004c, code lost:
        r5 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x004d, code lost:
        r8.mHandler.post(new com.netqin.antivirus.contact.VCardReadThread.AnonymousClass1(r8));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x005d, code lost:
        r5 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x005e, code lost:
        r0 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0061, code lost:
        if ((r0 instanceof com.netqin.antivirus.contact.vcard.VCardNestedException) == false) goto L_0x0068;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0067, code lost:
        throw ((com.netqin.antivirus.contact.vcard.VCardNestedException) r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0068, code lost:
        r8.mHandler.post(new com.netqin.antivirus.contact.VCardReadThread.AnonymousClass2(r8));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0074, code lost:
        r5 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0075, code lost:
        r8.mHandler.post(new com.netqin.antivirus.contact.VCardReadThread.AnonymousClass3(r8));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:?, code lost:
        return false;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0048 A[SYNTHETIC, Splitter:B:29:0x0048] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x005d A[ExcHandler: VCardNotSupportedException (r5v3 'e' com.netqin.antivirus.contact.vcard.VCardNotSupportedException A[CUSTOM_DECLARE]), Splitter:B:1:0x0001] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0074 A[ExcHandler: VCardException (r5v0 'e' com.netqin.antivirus.contact.vcard.VCardException A[CUSTOM_DECLARE]), Splitter:B:1:0x0001] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean readOneVCardFile(java.lang.String r9, java.lang.String r10, com.netqin.antivirus.contact.vcard.VCardBuilder r11, com.netqin.antivirus.contact.vcard.VCardSourceDetector r12, boolean r13) throws com.netqin.antivirus.contact.vcard.VCardNestedException {
        /*
            r8 = this;
            r7 = 0
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ IOException -> 0x004c, VCardNotSupportedException -> 0x005d, VCardException -> 0x0074 }
            r3.<init>(r9)     // Catch:{ IOException -> 0x004c, VCardNotSupportedException -> 0x005d, VCardException -> 0x0074 }
            com.netqin.antivirus.contact.vcard.VCardParser_V21_Ex r5 = new com.netqin.antivirus.contact.vcard.VCardParser_V21_Ex     // Catch:{ IOException -> 0x004c, VCardNotSupportedException -> 0x005d, VCardException -> 0x0074 }
            r5.<init>(r12)     // Catch:{ IOException -> 0x004c, VCardNotSupportedException -> 0x005d, VCardException -> 0x0074 }
            r8.mVCardParser = r5     // Catch:{ IOException -> 0x004c, VCardNotSupportedException -> 0x005d, VCardException -> 0x0074 }
            com.netqin.antivirus.contact.vcard.VCardParser_V21 r5 = r8.mVCardParser     // Catch:{ VCardVersionException -> 0x001b }
            boolean r6 = r8.mCanceled     // Catch:{ VCardVersionException -> 0x001b }
            r5.parse(r3, r10, r11, r6)     // Catch:{ VCardVersionException -> 0x001b }
            if (r3 == 0) goto L_0x0019
            r3.close()     // Catch:{ IOException -> 0x0086, VCardNotSupportedException -> 0x005d, VCardException -> 0x0074 }
        L_0x0019:
            r5 = 1
        L_0x001a:
            return r5
        L_0x001b:
            r5 = move-exception
            r1 = r5
            r3.close()     // Catch:{ IOException -> 0x0082 }
        L_0x0020:
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ all -> 0x0088 }
            r4.<init>(r9)     // Catch:{ all -> 0x0088 }
            com.netqin.antivirus.contact.vcard.VCardParser_V30 r5 = new com.netqin.antivirus.contact.vcard.VCardParser_V30     // Catch:{ VCardVersionException -> 0x003a }
            r5.<init>()     // Catch:{ VCardVersionException -> 0x003a }
            r8.mVCardParser = r5     // Catch:{ VCardVersionException -> 0x003a }
            com.netqin.antivirus.contact.vcard.VCardParser_V21 r5 = r8.mVCardParser     // Catch:{ VCardVersionException -> 0x003a }
            boolean r6 = r8.mCanceled     // Catch:{ VCardVersionException -> 0x003a }
            r5.parse(r4, r10, r11, r6)     // Catch:{ VCardVersionException -> 0x003a }
            if (r4 == 0) goto L_0x008a
            r4.close()     // Catch:{ IOException -> 0x005a, VCardNotSupportedException -> 0x005d, VCardException -> 0x0074 }
            r3 = r4
            goto L_0x0019
        L_0x003a:
            r5 = move-exception
            r2 = r5
            com.netqin.antivirus.contact.vcard.VCardException r5 = new com.netqin.antivirus.contact.vcard.VCardException     // Catch:{ all -> 0x0044 }
            java.lang.String r6 = "vCard with unspported version."
            r5.<init>(r6)     // Catch:{ all -> 0x0044 }
            throw r5     // Catch:{ all -> 0x0044 }
        L_0x0044:
            r5 = move-exception
            r3 = r4
        L_0x0046:
            if (r3 == 0) goto L_0x004b
            r3.close()     // Catch:{ IOException -> 0x0084, VCardNotSupportedException -> 0x005d, VCardException -> 0x0074 }
        L_0x004b:
            throw r5     // Catch:{ IOException -> 0x004c, VCardNotSupportedException -> 0x005d, VCardException -> 0x0074 }
        L_0x004c:
            r5 = move-exception
            r0 = r5
            android.os.Handler r5 = r8.mHandler
            com.netqin.antivirus.contact.VCardReadThread$1 r6 = new com.netqin.antivirus.contact.VCardReadThread$1
            r6.<init>()
            r5.post(r6)
            r5 = r7
            goto L_0x001a
        L_0x005a:
            r5 = move-exception
            r3 = r4
            goto L_0x0019
        L_0x005d:
            r5 = move-exception
            r0 = r5
            boolean r5 = r0 instanceof com.netqin.antivirus.contact.vcard.VCardNestedException
            if (r5 == 0) goto L_0x0068
            if (r13 == 0) goto L_0x0068
            com.netqin.antivirus.contact.vcard.VCardNestedException r0 = (com.netqin.antivirus.contact.vcard.VCardNestedException) r0
            throw r0
        L_0x0068:
            android.os.Handler r5 = r8.mHandler
            com.netqin.antivirus.contact.VCardReadThread$2 r6 = new com.netqin.antivirus.contact.VCardReadThread$2
            r6.<init>()
            r5.post(r6)
            r5 = r7
            goto L_0x001a
        L_0x0074:
            r5 = move-exception
            r0 = r5
            android.os.Handler r5 = r8.mHandler
            com.netqin.antivirus.contact.VCardReadThread$3 r6 = new com.netqin.antivirus.contact.VCardReadThread$3
            r6.<init>()
            r5.post(r6)
            r5 = r7
            goto L_0x001a
        L_0x0082:
            r5 = move-exception
            goto L_0x0020
        L_0x0084:
            r6 = move-exception
            goto L_0x004b
        L_0x0086:
            r5 = move-exception
            goto L_0x0019
        L_0x0088:
            r5 = move-exception
            goto L_0x0046
        L_0x008a:
            r3 = r4
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: com.netqin.antivirus.contact.VCardReadThread.readOneVCardFile(java.lang.String, java.lang.String, com.netqin.antivirus.contact.vcard.VCardBuilder, com.netqin.antivirus.contact.vcard.VCardSourceDetector, boolean):boolean");
    }

    public void cancel() {
        this.mCanceled = true;
        this.mRunState = 1;
        ContactStruct.setRunMark(false);
        if (this.mVCardParser != null) {
            this.mVCardParser.cancel();
        }
    }

    public void onCancel(DialogInterface dialog) {
        cancel();
    }
}
