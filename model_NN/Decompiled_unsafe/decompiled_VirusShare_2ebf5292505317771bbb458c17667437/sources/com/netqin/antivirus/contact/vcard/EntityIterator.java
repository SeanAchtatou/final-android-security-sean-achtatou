package com.netqin.antivirus.contact.vcard;

import android.os.RemoteException;

public interface EntityIterator {
    void close();

    boolean hasNext() throws RemoteException;

    Entity next() throws RemoteException;

    void reset() throws RemoteException;
}
