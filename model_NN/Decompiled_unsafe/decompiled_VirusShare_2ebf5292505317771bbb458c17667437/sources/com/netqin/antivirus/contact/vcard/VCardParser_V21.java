package com.netqin.antivirus.contact.vcard;

import android.text.TextUtils;
import com.netqin.antivirus.appprotocol.XmlTagValue;
import com.netqin.antivirus.common.CommonMethod;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

public class VCardParser_V21 extends VCardParser {
    private static final String LOG_TAG = "vcard.VCardParser_V21";
    private static final int STATE_GROUP_OR_PROPNAME = 0;
    private static final int STATE_PARAMS = 1;
    private static final int STATE_PARAMS_IN_DQUOTE = 2;
    private static final HashSet<String> sAvailableEncodingV21 = new HashSet<>(Arrays.asList("7BIT", "8BIT", "QUOTED-PRINTABLE", "BASE64", "B"));
    private static final HashSet<String> sAvailablePropertyNameSetV21 = new HashSet<>(Arrays.asList("BEGIN", "LOGO", "PHOTO", "LABEL", "FN", "TITLE", "SOUND", "VERSION", "TEL", "EMAIL", "TZ", "GEO", "NOTE", XmlTagValue.uRL, "BDAY", "ROLE", "REV", "UID", "KEY", "MAILER"));
    private static final HashSet<String> sKnownTypeSet = new HashSet<>(Arrays.asList("DOM", "INTL", "POSTAL", "PARCEL", Constants.ATTR_TYPE_HOME, Constants.ATTR_TYPE_WORK, Constants.ATTR_TYPE_PREF, Constants.ATTR_TYPE_VOICE, Constants.ATTR_TYPE_FAX, Constants.ATTR_TYPE_MSG, Constants.ATTR_TYPE_CELL, Constants.ATTR_TYPE_PAGER, Constants.ATTR_TYPE_BBS, Constants.ATTR_TYPE_MODEM, Constants.ATTR_TYPE_CAR, Constants.ATTR_TYPE_ISDN, Constants.ATTR_TYPE_VIDEO, "AOL", "APPLELINK", "ATTMAIL", "CIS", "EWORLD", Constants.ATTR_TYPE_INTERNET, "IBMMAIL", "MCIMAIL", "POWERSHARE", "PRODIGY", "TLX", "X400", "GIF", "CGM", "WMF", "BMP", "MET", "PMB", "DIB", "PICT", "TIFF", "PDF", "PS", "JPEG", "QTIME", "MPEG", "MPEG2", "AVI", "WAVE", "AIFF", "PCM", "X509", "PGP"));
    private static final HashSet<String> sKnownValueSet = new HashSet<>(Arrays.asList("INLINE", XmlTagValue.uRL, "CONTENT-ID", "CID"));
    protected VCardBuilder mBuilder;
    protected String mEncoding;
    protected String mExPreviousLine;
    protected int mIndexForDebug;
    private int mNestCount;
    private String mPreviousLine;
    protected BufferedReader mReader;
    private long mTimeEndProperty;
    private long mTimeHandleBase64;
    private long mTimeHandleMiscPropertyValue;
    private long mTimeHandleQuotedPrintable;
    private long mTimeParseAdrOrgN;
    private long mTimeParseItems;
    private long mTimeParseLineAndHandleGroup;
    private long mTimeParsePropertyValues;
    private long mTimeReadEndRecord;
    private long mTimeReadStartRecord;
    private long mTimeStartProperty;
    private long mTimeTotal;
    protected HashSet<String> mWarningValueMap;
    protected final String sDefaultEncoding;

    public VCardParser_V21() {
        this.mIndexForDebug = 0;
        this.mBuilder = null;
        this.mEncoding = null;
        this.sDefaultEncoding = "8BIT";
        this.mWarningValueMap = new HashSet<>();
        this.mIndexForDebug = 0;
    }

    public VCardParser_V21(VCardSourceDetector detector) {
        this.mIndexForDebug = 0;
        this.mBuilder = null;
        this.mEncoding = null;
        this.sDefaultEncoding = "8BIT";
        this.mWarningValueMap = new HashSet<>();
        if (detector != null && detector.getType() == 3) {
            this.mNestCount = 1;
        }
        this.mIndexForDebug = 0;
    }

    /* access modifiers changed from: protected */
    public void parseVCardFile() throws IOException, VCardException {
        boolean firstReading = true;
        while (!this.mCanceled && parseOneVCard(firstReading)) {
            firstReading = false;
        }
        if (this.mNestCount > 0) {
            boolean useCache = true;
            for (int i = 0; i < this.mNestCount; i++) {
                readEndVCard(useCache, true);
                useCache = false;
            }
        }
    }

    /* access modifiers changed from: protected */
    public String getVersion() {
        return Constants.VERSION_V21;
    }

    /* access modifiers changed from: protected */
    public boolean isValidPropertyName(String propertyName) {
        if (sAvailablePropertyNameSetV21.contains(propertyName.toUpperCase()) || propertyName.startsWith("X-") || this.mWarningValueMap.contains(propertyName)) {
            return true;
        }
        this.mWarningValueMap.add(propertyName);
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean isValidEncoding(String encoding) {
        return sAvailableEncodingV21.contains(encoding.toUpperCase());
    }

    /* access modifiers changed from: protected */
    public String getLine() throws IOException {
        return this.mReader.readLine();
    }

    /* access modifiers changed from: protected */
    public String getNonEmptyLine() throws IOException, VCardException {
        String line;
        do {
            line = getLine();
            this.mIndexForDebug++;
            if (line == null) {
                break;
            }
        } while (line.trim().length() <= 0);
        return line;
    }

    private boolean parseOneVCard(boolean firstReading) throws IOException, VCardException {
        boolean allowGarbage = true;
        if (firstReading && this.mNestCount > 0) {
            for (int i = 0; i < this.mNestCount; i++) {
                if (!readBeginVCard(allowGarbage)) {
                    return false;
                }
                allowGarbage = true;
            }
        }
        if (!readBeginVCard(allowGarbage)) {
            return false;
        }
        if (this.mBuilder != null) {
            long start = System.currentTimeMillis();
            this.mBuilder.startRecord("VCARD");
            this.mTimeReadStartRecord += System.currentTimeMillis() - start;
        }
        long start2 = System.currentTimeMillis();
        parseItems();
        this.mTimeParseItems += System.currentTimeMillis() - start2;
        readEndVCard(true, false);
        if (this.mBuilder != null) {
            long start3 = System.currentTimeMillis();
            this.mBuilder.endRecord();
            this.mTimeReadEndRecord += System.currentTimeMillis() - start3;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean readBeginVCard(boolean allowGarbage) throws IOException, VCardException {
        while (true) {
            String line = getLine();
            if (line == null) {
                return false;
            }
            if (line.trim().length() > 0) {
                String[] strArray = line.split(":", 2);
                if (strArray.length == 2 && strArray[0].trim().equalsIgnoreCase("BEGIN") && strArray[1].trim().equalsIgnoreCase("VCARD")) {
                    return true;
                }
                if (!allowGarbage) {
                    if (this.mNestCount > 0) {
                        this.mPreviousLine = line;
                        return false;
                    }
                    throw new VCardException("Expected String \"BEGIN:VCARD\" did not come (Instead, \"" + line + "\" came)");
                } else if (!allowGarbage) {
                    throw new VCardException("Reached where must not be reached.");
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void readEndVCard(boolean useCache, boolean allowGarbage) throws IOException, VCardException {
        String line;
        do {
            if (useCache) {
                line = this.mPreviousLine;
            } else {
                do {
                    line = getLine();
                    if (line == null) {
                        throw new VCardException("Expected END:VCARD was not found.");
                    }
                } while (line.trim().length() <= 0);
            }
            String[] strArray = line.split(":", 2);
            if (strArray.length == 2 && strArray[0].trim().equalsIgnoreCase("END") && strArray[1].trim().equalsIgnoreCase("VCARD")) {
                return;
            }
            if (!allowGarbage) {
                throw new VCardException("END:VCARD != \"" + this.mPreviousLine + "\"");
            }
            useCache = false;
        } while (allowGarbage);
    }

    /* access modifiers changed from: protected */
    public void parseItems() throws IOException, VCardException {
        if (this.mBuilder != null) {
            long start = System.currentTimeMillis();
            this.mBuilder.startProperty();
            this.mTimeStartProperty += System.currentTimeMillis() - start;
        }
        boolean ended = parseItem();
        if (this.mBuilder != null && !ended) {
            long start2 = System.currentTimeMillis();
            this.mBuilder.endProperty();
            this.mTimeEndProperty += System.currentTimeMillis() - start2;
        }
        while (!ended) {
            if (this.mBuilder != null) {
                long start3 = System.currentTimeMillis();
                this.mBuilder.startProperty();
                this.mTimeStartProperty += System.currentTimeMillis() - start3;
            }
            try {
                ended = parseItem();
            } catch (VCardInvalidCommentLineException e) {
                ended = false;
            }
            if (this.mBuilder != null && !ended) {
                long start4 = System.currentTimeMillis();
                this.mBuilder.endProperty();
                this.mTimeEndProperty += System.currentTimeMillis() - start4;
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean parseItem() throws IOException, VCardException {
        this.mEncoding = "8BIT";
        String line = getNonEmptyLine();
        if (TextUtils.isEmpty(line)) {
            return true;
        }
        long start = System.currentTimeMillis();
        String[] propertyNameAndValue = separateLineAndHandleGroup(line);
        if (propertyNameAndValue == null) {
            return true;
        }
        if (TextUtils.isEmpty(propertyNameAndValue[0]) || TextUtils.isEmpty(propertyNameAndValue[1])) {
            return false;
        }
        if (propertyNameAndValue.length != 2) {
            return false;
        }
        String propertyName = propertyNameAndValue[0].toUpperCase();
        String propertyValue = propertyNameAndValue[1];
        this.mTimeParseLineAndHandleGroup += System.currentTimeMillis() - start;
        if (propertyName.equals("ADR") || propertyName.equals("ORG") || propertyName.equals("N")) {
            long start2 = System.currentTimeMillis();
            handleMultiplePropertyValue(propertyName, propertyValue);
            this.mTimeParseAdrOrgN += System.currentTimeMillis() - start2;
            return false;
        } else if (propertyName.equals("AGENT")) {
            handleAgent(propertyValue);
            return false;
        } else if (!isValidPropertyName(propertyName)) {
            return false;
        } else {
            if (propertyName.equals("BEGIN")) {
                return false;
            }
            if (!propertyName.equals("VERSION") || propertyValue.equals(getVersion())) {
                long start3 = System.currentTimeMillis();
                handlePropertyValue(propertyName, propertyValue);
                this.mTimeParsePropertyValues += System.currentTimeMillis() - start3;
                return false;
            }
            throw new VCardVersionException("Incompatible version: " + propertyValue + " != " + getVersion());
        }
    }

    /* access modifiers changed from: protected */
    public String[] separateLineAndHandleGroup(String line) {
        int length = line.length();
        int state = 0;
        int nameIndex = 0;
        String[] propertyNameAndValue = new String[2];
        if (length > 0 && line.charAt(0) == '#') {
            return propertyNameAndValue;
        }
        int i = 0;
        while (i < length) {
            try {
                char ch = line.charAt(i);
                switch (state) {
                    case 0:
                        if (ch == ':') {
                            String propertyName = line.substring(nameIndex, i);
                            if (propertyName.equalsIgnoreCase("END")) {
                                this.mPreviousLine = line;
                                return null;
                            }
                            if (this.mBuilder != null) {
                                this.mBuilder.propertyName(propertyName);
                            }
                            propertyNameAndValue[0] = propertyName;
                            if (i < length - 1) {
                                propertyNameAndValue[1] = line.substring(i + 1);
                            } else {
                                propertyNameAndValue[1] = "";
                            }
                            return propertyNameAndValue;
                        } else if (ch == '.') {
                            String groupName = line.substring(nameIndex, i);
                            if (this.mBuilder != null) {
                                this.mBuilder.propertyGroup(groupName);
                            }
                            nameIndex = i + 1;
                            break;
                        } else if (ch == ';') {
                            String propertyName2 = line.substring(nameIndex, i);
                            if (!propertyName2.equalsIgnoreCase("END")) {
                                if (this.mBuilder != null) {
                                    this.mBuilder.propertyName(propertyName2);
                                }
                                propertyNameAndValue[0] = propertyName2;
                                nameIndex = i + 1;
                                state = 1;
                                break;
                            } else {
                                this.mPreviousLine = line;
                                return null;
                            }
                        } else {
                            continue;
                        }
                    case 1:
                        if (ch != '\"') {
                            if (ch != ';') {
                                if (ch != ':') {
                                    break;
                                } else {
                                    handleParams(line.substring(nameIndex, i));
                                    if (i < length - 1) {
                                        propertyNameAndValue[1] = line.substring(i + 1);
                                    } else {
                                        propertyNameAndValue[1] = "";
                                    }
                                    return propertyNameAndValue;
                                }
                            } else {
                                handleParams(line.substring(nameIndex, i));
                                nameIndex = i + 1;
                                break;
                            }
                        } else {
                            state = 2;
                            break;
                        }
                    case 2:
                        if (ch != '\"') {
                            break;
                        } else {
                            state = 1;
                            break;
                        }
                }
                i++;
            } catch (VCardException e) {
                VCardException vCardException = e;
                return propertyNameAndValue;
            }
        }
        return propertyNameAndValue;
    }

    /* access modifiers changed from: protected */
    public void handleParams(String params) throws VCardException {
        String[] strArray = params.split("=", 2);
        if (strArray.length == 2) {
            String paramName = strArray[0].trim();
            String paramValue = strArray[1].trim();
            if (paramName.equals(Constants.ATTR_TYPE)) {
                handleType(paramValue);
            } else if (paramName.equals("VALUE")) {
                handleValue(paramValue);
            } else if (paramName.equals("ENCODING")) {
                handleEncoding(paramValue);
            } else if (paramName.equals("CHARSET")) {
                handleCharset(paramValue);
            } else if (paramName.equals("LANGUAGE")) {
                handleLanguage(paramValue);
            } else if (paramName.startsWith("X-")) {
                handleAnyParam(paramName, paramValue);
            } else {
                throw new VCardException("Unknown type \"" + paramName + "\"");
            }
        } else {
            handleType(strArray[0]);
        }
    }

    /* access modifiers changed from: protected */
    public void handleType(String ptypeval) {
        String upperTypeValue = ptypeval;
        if (!sKnownTypeSet.contains(upperTypeValue) && !upperTypeValue.startsWith("X-") && !this.mWarningValueMap.contains(ptypeval)) {
            this.mWarningValueMap.add(ptypeval);
        }
        if (this.mBuilder != null) {
            this.mBuilder.propertyParamType(Constants.ATTR_TYPE);
            this.mBuilder.propertyParamValue(upperTypeValue);
        }
    }

    /* access modifiers changed from: protected */
    public void handleValue(String pvalueval) throws VCardException {
        if (!sKnownValueSet.contains(pvalueval.toUpperCase()) && !pvalueval.startsWith("X-")) {
            throw new VCardException("Unknown value \"" + pvalueval + "\"");
        } else if (this.mBuilder != null) {
            this.mBuilder.propertyParamType("VALUE");
            this.mBuilder.propertyParamValue(pvalueval);
        }
    }

    /* access modifiers changed from: protected */
    public void handleEncoding(String pencodingval) throws VCardException {
        if (isValidEncoding(pencodingval) || pencodingval.startsWith("X-")) {
            if (this.mBuilder != null) {
                this.mBuilder.propertyParamType("ENCODING");
                this.mBuilder.propertyParamValue(pencodingval);
            }
            this.mEncoding = pencodingval;
            return;
        }
        throw new VCardException("Unknown encoding \"" + pencodingval + "\"");
    }

    /* access modifiers changed from: protected */
    public void handleCharset(String charsetval) {
        if (this.mBuilder != null) {
            this.mBuilder.propertyParamType("CHARSET");
            this.mBuilder.propertyParamValue(charsetval);
        }
    }

    /* access modifiers changed from: protected */
    public void handleLanguage(String langval) throws VCardException {
        String[] strArray = langval.split("-");
        if (strArray.length != 2) {
            throw new VCardException("Invalid Language: \"" + langval + "\"");
        }
        String tmp = strArray[0];
        int length = tmp.length();
        for (int i = 0; i < length; i++) {
            if (!isLetter(tmp.charAt(i))) {
                throw new VCardException("Invalid Language: \"" + langval + "\"");
            }
        }
        String tmp2 = strArray[1];
        int length2 = tmp2.length();
        for (int i2 = 0; i2 < length2; i2++) {
            if (!isLetter(tmp2.charAt(i2))) {
                throw new VCardException("Invalid Language: \"" + langval + "\"");
            }
        }
        if (this.mBuilder != null) {
            this.mBuilder.propertyParamType("LANGUAGE");
            this.mBuilder.propertyParamValue(langval);
        }
    }

    /* access modifiers changed from: protected */
    public void handleAnyParam(String paramName, String paramValue) {
        if (this.mBuilder != null) {
            this.mBuilder.propertyParamType(paramName);
            this.mBuilder.propertyParamValue(paramValue);
        }
    }

    /* access modifiers changed from: protected */
    public void handlePropertyValue(String propertyName, String propertyValue) throws IOException, VCardException {
        if (this.mEncoding.equalsIgnoreCase("QUOTED-PRINTABLE")) {
            long start = System.currentTimeMillis();
            String result = getQuotedPrintable(propertyValue);
            if (this.mBuilder != null) {
                ArrayList<String> v = new ArrayList<>();
                v.add(result);
                this.mBuilder.propertyValues(v);
            }
            this.mTimeHandleQuotedPrintable += System.currentTimeMillis() - start;
        } else if (this.mEncoding.equalsIgnoreCase("BASE64") || this.mEncoding.equalsIgnoreCase("B")) {
            long start2 = System.currentTimeMillis();
            try {
                String result2 = getBase64(propertyValue);
                if (this.mBuilder != null) {
                    ArrayList<String> v2 = new ArrayList<>();
                    v2.add(result2);
                    this.mBuilder.propertyValues(v2);
                }
            } catch (OutOfMemoryError e) {
                if (this.mBuilder != null) {
                    this.mBuilder.propertyValues(null);
                }
            }
            this.mTimeHandleBase64 += System.currentTimeMillis() - start2;
        } else {
            if (this.mEncoding != null && !this.mEncoding.equalsIgnoreCase("7BIT") && !this.mEncoding.equalsIgnoreCase("8BIT")) {
                this.mEncoding.toUpperCase().startsWith("X-");
            }
            long start3 = System.currentTimeMillis();
            if (this.mBuilder != null) {
                ArrayList<String> v3 = new ArrayList<>();
                v3.add(maybeUnescapeText(propertyValue));
                this.mBuilder.propertyValues(v3);
            }
            this.mTimeHandleMiscPropertyValue += System.currentTimeMillis() - start3;
        }
    }

    /* access modifiers changed from: protected */
    public String getQuotedPrintable(String firstString) throws IOException, VCardException {
        String line;
        if (!firstString.trim().endsWith("=")) {
            return firstString;
        }
        int pos = firstString.length() - 1;
        do {
        } while (firstString.charAt(pos) != '=');
        StringBuilder builder = new StringBuilder();
        builder.append(firstString.substring(0, pos + 1));
        builder.append("\r\n");
        while (true) {
            if (!TextUtils.isEmpty(this.mExPreviousLine) && (line = getLine()) != null) {
                if (!line.trim().endsWith("=")) {
                    builder.append(line);
                    break;
                }
                int pos2 = line.length() - 1;
                do {
                } while (line.charAt(pos2) != '=');
                builder.append(line.substring(0, pos2 + 1));
                builder.append("\r\n");
            } else {
                break;
            }
        }
        return builder.toString();
    }

    /* access modifiers changed from: protected */
    public String getBase64(String firstString) throws IOException, VCardException {
        StringBuilder builder = new StringBuilder();
        builder.append(firstString);
        while (true) {
            String line = getLine();
            if (line != null) {
                if (line.length() == 0 || line.equalsIgnoreCase("END:VCARD")) {
                    break;
                }
                builder.append(line);
            } else {
                CommonMethod.logDebug1("v2130", "VCardParser_V21: getBase64: VCardException");
                break;
            }
        }
        return builder.toString();
    }

    /* access modifiers changed from: protected */
    public void handleMultiplePropertyValue(String propertyName, String propertyValue) throws IOException, VCardException {
        if (this.mEncoding.equalsIgnoreCase("QUOTED-PRINTABLE")) {
            propertyValue = getQuotedPrintable(propertyValue);
        }
        if (this.mBuilder != null) {
            StringBuilder builder = new StringBuilder();
            ArrayList<String> list = new ArrayList<>();
            int length = propertyValue.length();
            int i = 0;
            while (i < length) {
                char ch = propertyValue.charAt(i);
                if (ch == '\\' && i < length - 1) {
                    String unescapedString = maybeUnescapeCharacter(propertyValue.charAt(i + 1));
                    if (unescapedString != null) {
                        builder.append(unescapedString);
                        i++;
                    } else {
                        builder.append(ch);
                    }
                } else if (ch == ';') {
                    list.add(builder.toString());
                    builder = new StringBuilder();
                } else {
                    builder.append(ch);
                }
                i++;
            }
            list.add(builder.toString());
            this.mBuilder.propertyValues(list);
        }
    }

    /* access modifiers changed from: protected */
    public void handleAgent(String propertyValue) throws VCardException {
        throw new VCardNotSupportedException("AGENT Property is not supported now.");
    }

    /* access modifiers changed from: protected */
    public String maybeUnescapeText(String text) {
        return text;
    }

    /* access modifiers changed from: protected */
    public String maybeUnescapeCharacter(char ch) {
        if (ch == '\\' || ch == ';' || ch == ':' || ch == ',') {
            return String.valueOf(ch);
        }
        return null;
    }

    public boolean parse(InputStream is, VCardBuilder builder) throws IOException, VCardException {
        return parse(is, VCardConfig.DEFAULT_CHARSET, builder);
    }

    public boolean parse(InputStream is, String charset, VCardBuilder builder) throws IOException, VCardException {
        InputStreamReader tmpReader = new InputStreamReader(is, charset);
        if (VCardConfig.showPerformanceLog()) {
            this.mReader = new CustomBufferedReader(tmpReader);
        } else {
            this.mReader = new BufferedReader(tmpReader);
        }
        this.mBuilder = builder;
        long start = System.currentTimeMillis();
        if (this.mBuilder != null) {
            this.mBuilder.start();
        }
        parseVCardFile();
        if (this.mBuilder != null) {
            this.mBuilder.end();
        }
        this.mTimeTotal += System.currentTimeMillis() - start;
        if (!VCardConfig.showPerformanceLog()) {
            return true;
        }
        showPerformanceInfo();
        return true;
    }

    public void parse(InputStream is, String charset, VCardBuilder builder, boolean canceled) throws IOException, VCardException {
        this.mCanceled = canceled;
        parse(is, charset, builder);
    }

    private void showPerformanceInfo() {
    }

    private boolean isLetter(char ch) {
        if ((ch < 'a' || ch > 'z') && (ch < 'A' || ch > 'Z')) {
            return false;
        }
        return true;
    }
}
