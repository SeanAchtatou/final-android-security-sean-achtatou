package com.netqin.antivirus.contact.vcard;

import android.os.Build;
import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.nio.charset.UnsupportedCharsetException;

public final class CharsetUtils {
    private static final String VENDOR_DOCOMO = "docomo";

    private CharsetUtils() {
    }

    public static String nameForVendor(String charsetName, String vendor) {
        if (!vendor.equalsIgnoreCase("docomo") || !isShiftJis(charsetName)) {
            return charsetName;
        }
        return "docomo-shift_jis-2007";
    }

    public static String nameForDefaultVendor(String charsetName) {
        return nameForVendor(charsetName, getDefaultVendor());
    }

    public static Charset charsetForVendor(String charsetName, String vendor) throws UnsupportedCharsetException, IllegalCharsetNameException {
        return Charset.forName(nameForVendor(charsetName, vendor));
    }

    public static Charset charsetForVendor(String charsetName) throws UnsupportedCharsetException, IllegalCharsetNameException {
        return charsetForVendor(charsetName, getDefaultVendor());
    }

    private static boolean isShiftJis(String charsetName) {
        if (charsetName == null) {
            return false;
        }
        int length = charsetName.length();
        if (length == 4 || length == 9) {
            return charsetName.equalsIgnoreCase("shift_jis") || charsetName.equalsIgnoreCase("shift-jis") || charsetName.equalsIgnoreCase("sjis");
        }
        return false;
    }

    private static String getDefaultVendor() {
        return Build.BRAND;
    }
}
