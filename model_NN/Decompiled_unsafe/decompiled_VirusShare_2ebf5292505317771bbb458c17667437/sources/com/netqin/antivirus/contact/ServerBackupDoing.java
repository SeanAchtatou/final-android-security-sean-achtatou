package com.netqin.antivirus.contact;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.widget.Toast;
import com.netqin.antivirus.Value;
import com.netqin.antivirus.antilost.AntiLostCommon;
import com.netqin.antivirus.common.CommonDefine;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.common.CommonUtils;
import com.netqin.antivirus.common.ProgDlgActivity;
import com.netqin.antivirus.common.ProgressTextDialog;
import com.netqin.antivirus.log.LogEngine;
import com.netqin.antivirus.net.contactservice.ContactService;
import com.nqmobile.antivirus_ampro20.R;

public class ServerBackupDoing extends ProgDlgActivity {
    public static boolean mFromContactGuide = false;
    /* access modifiers changed from: private */
    public static String mMessageBoxText = "";
    public static Activity mServerBackDoing = null;
    /* access modifiers changed from: private */
    public ContactService contactService = null;
    /* access modifiers changed from: private */
    public ContentValues content;
    /* access modifiers changed from: private */
    public int mContactCount = 0;
    /* access modifiers changed from: private */
    public String mContactFilePath = "";
    /* access modifiers changed from: private */
    public String mContactUploadFilePath = "";
    /* access modifiers changed from: private */
    public int mProgressCurr = 0;
    private int mProgressMax = 0;
    public float mUnitKInt = 1.0f;
    public String mUnitString = "B";
    /* access modifiers changed from: private */
    public VCardExportThread mVCardExportThread = null;
    /* access modifiers changed from: private */
    public int result;

    public void onCreate(Bundle savedInstanceState) {
        this.content = new ContentValues();
        this.contactService = ContactService.getInstance(this);
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        if (dm.widthPixels <= dm.heightPixels) {
            setRequestedOrientation(1);
        } else {
            setRequestedOrientation(0);
        }
        if (ContactGuide.isBackupFromContactGuide) {
            ContactGuide.isBackupFromContactGuide = false;
            clickOk();
        }
        if (mFromContactGuide) {
            mFromContactGuide = false;
            mMessageBoxText = "";
            showDialog(R.string.text_is_backup_to_network_atonce);
        }
        mServerBackDoing = this;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            this.mActivityVisible = false;
            if (this.contactService != null) {
                this.contactService.cancel();
            }
            if (this.mVCardExportThread != null) {
                this.mVCardExportThread.cancel();
                this.mVCardExportThread = null;
            }
            Message msg = new Message();
            msg.what = 12;
            msg.arg1 = 37;
            this.mHandler.sendMessage(msg);
        }
        return super.onKeyDown(keyCode, event);
    }

    /* access modifiers changed from: protected */
    public void progressTextDialogOnStop() {
        if (this.mVCardExportThread != null) {
            this.mVCardExportThread.cancel();
            this.mVCardExportThread = null;
        }
        super.progressTextDialogOnStop();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        DialogInterface.OnClickListener okListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                ServerBackupDoing.this.clickOk();
            }
        };
        DialogInterface.OnClickListener cancelListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                ServerBackupDoing.this.clickCancel();
            }
        };
        DialogInterface.OnKeyListener keyListener = new DialogInterface.OnKeyListener() {
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode != 4) {
                    return false;
                }
                ServerBackupDoing.this.clickCancel();
                return true;
            }
        };
        DialogInterface.OnClickListener uploadListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                ServerBackupDoing.this.retryUpload();
            }
        };
        DialogInterface.OnClickListener succListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                ServerBackupDoing.this.clickSucc();
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        switch (id) {
            case 20:
                builder.setTitle((int) R.string.label_netqin_antivirus);
                builder.setMessage(mMessageBoxText);
                builder.setPositiveButton((int) R.string.label_ok, cancelListener);
                builder.setOnKeyListener(keyListener);
                return builder.create();
            case 27:
                builder.setTitle((int) R.string.label_netqin_antivirus);
                builder.setMessage(mMessageBoxText);
                builder.setPositiveButton((int) R.string.label_ok, cancelListener);
                builder.setOnKeyListener(keyListener);
                return builder.create();
            case 30:
                builder.setTitle((int) R.string.label_backup_success_tip);
                builder.setMessage(mMessageBoxText);
                builder.setPositiveButton((int) R.string.label_ok, succListener);
                builder.setOnKeyListener(keyListener);
                return builder.create();
            case CommonDefine.MSG_PROG_ARG_ERRMSG_CONTACT_EXPORT:
                builder.setTitle((int) R.string.label_netqin_antivirus);
                builder.setMessage(mMessageBoxText);
                builder.setPositiveButton((int) R.string.label_ok, cancelListener);
                builder.setOnKeyListener(keyListener);
                return builder.create();
            case CommonDefine.MSG_PROG_ARG_ERRMSG_NETWORK:
                builder.setTitle((int) R.string.label_backup_break_off_tip);
                builder.setMessage(mMessageBoxText);
                builder.setPositiveButton((int) R.string.label_ok, uploadListener);
                builder.setNegativeButton((int) R.string.label_cancel, cancelListener);
                builder.setOnKeyListener(keyListener);
                return builder.create();
            case R.string.text_is_backup_to_network_atonce /*2131428142*/:
                builder.setTitle((int) R.string.label_netqin_antivirus);
                builder.setMessage((int) R.string.text_is_backup_to_network_atonce);
                builder.setPositiveButton((int) R.string.label_ok, okListener);
                builder.setNegativeButton((int) R.string.label_cancel, cancelListener);
                builder.setOnKeyListener(keyListener);
                return builder.create();
            default:
                return super.onCreateDialog(id);
        }
    }

    /* access modifiers changed from: protected */
    public void childFunctionCall(Message msg) {
        int arg1 = msg.arg1;
        int arg2 = msg.arg2;
        switch (arg1) {
            case 21:
                this.mProgTextDialog.getTitleTextView().setText((int) R.string.text_uploading_contact2);
                if (arg2 > 1024) {
                    this.mUnitKInt = 1024.0f;
                    this.mUnitString = "KB";
                } else {
                    this.mUnitKInt = 1.0f;
                    this.mUnitString = "B";
                }
                this.mProgressMax = arg2;
                this.mProgTextRight = "0%";
                CommonMethod.sendUserMessage(this.mHandler, 8, this.mProgressMax);
                return;
            case 22:
            case 24:
            case CommonDefine.MSG_PROG_ARG_CANCEL:
            case CommonDefine.MSG_PROG_ARG_ERROR:
            case CommonDefine.MSG_PROG_ARG_ERRMORE:
            case 31:
            case 32:
            case CommonDefine.MSG_PROG_ARG_ERRMSG_CONTACT_IMPORT:
            default:
                if (this.contactService != null) {
                    this.contactService.cancel();
                    this.contactService = null;
                }
                CommonMethod.sendUserMessage(this.mHandler, 4);
                CommonMethod.sendUserMessage(this.mHandler, 10);
                mMessageBoxText = getString(R.string.text_unprocess_message, new Object[]{Integer.valueOf(msg.arg1)});
                showDialog(20);
                return;
            case 23:
                LogEngine.insertOperationItemLog(20, "", getFilesDir().getPath());
                if (this.mActivityVisible && this.mProgressCurr < this.mProgressMax) {
                    this.mProgressCurr = this.mProgressMax;
                    if (this.mUnitString.equalsIgnoreCase("KB")) {
                        this.mProgTextLeft = String.valueOf(ContactCommon.floatEndWithTwo(((float) this.mProgressCurr) / this.mUnitKInt)) + this.mUnitString;
                    } else {
                        this.mProgTextLeft = String.valueOf(this.mProgressCurr) + this.mUnitString;
                    }
                    this.mProgTextRight = "100%";
                    CommonMethod.sendUserMessage(this.mHandler, 9, this.mProgressCurr);
                }
                CommonMethod.sendUserMessage(this.mHandler, 10);
                if (this.result == 10) {
                    SharedPreferences spf = getSharedPreferences(AntiLostCommon.DELETE_CONTACT, 0);
                    spf.edit().putString("contacts_network", Integer.toString(this.mContactCount)).commit();
                    spf.edit().putString("contacts_backup_time", ContactCommon.getPresentTime()).commit();
                    this.mHandler.post(new Runnable() {
                        public void run() {
                            String tmpStr;
                            if (ServerBackupDoing.this.mUnitString.equalsIgnoreCase("KB")) {
                                tmpStr = String.valueOf(ContactCommon.floatEndWithTwo(((float) ServerBackupDoing.this.mProgressCurr) / ServerBackupDoing.this.mUnitKInt)) + ServerBackupDoing.this.mUnitString;
                            } else {
                                tmpStr = String.valueOf(ServerBackupDoing.this.mProgressCurr) + ServerBackupDoing.this.mUnitString;
                            }
                            ServerBackupDoing.mMessageBoxText = ServerBackupDoing.this.getString(R.string.text_backup_contact_to_network_succ_result, new Object[]{Integer.valueOf(ServerBackupDoing.this.mContactCount), tmpStr});
                            ServerBackupDoing.this.showDialog(30);
                        }
                    });
                    return;
                }
                return;
            case 27:
                if (this.contactService != null) {
                    this.contactService.cancel();
                    this.contactService = null;
                }
                ContactCommon.writeOperationLog(21, getFilesDir().getPath());
                CommonMethod.sendUserMessage(this.mHandler, 10);
                mMessageBoxText = (String) msg.obj;
                showDialog(27);
                return;
            case CommonDefine.MSG_PROG_ARG_UPDATE:
                if (arg2 >= this.mProgressCurr) {
                    if (arg2 > this.mProgressMax) {
                        arg2 = this.mProgressMax;
                    }
                    this.mProgressCurr = arg2;
                    this.mProgTextRight = String.valueOf((int) (((float) (this.mProgressCurr * 100)) / ((float) this.mProgressMax))) + "%";
                    CommonMethod.sendUserMessage(this.mHandler, 9, this.mProgressCurr);
                    return;
                }
                return;
            case 30:
                this.mContactCount = msg.arg2;
                this.mContactUploadFilePath = String.valueOf(this.mContactFilePath) + "_final";
                new Thread(new Runnable() {
                    public void run() {
                        String res = CommonMethod.zipAndEncryFile(ServerBackupDoing.this.mContactFilePath, ServerBackupDoing.this.mContactUploadFilePath);
                        if (!TextUtils.isEmpty(res)) {
                            Message msg = new Message();
                            msg.what = 12;
                            msg.arg1 = 27;
                            msg.obj = res;
                            ServerBackupDoing.this.mHandler.sendMessage(msg);
                            return;
                        }
                        ServerBackupDoing.this.content.put("IMEI", CommonMethod.getIMEI(ServerBackupDoing.this));
                        ServerBackupDoing.this.content.put("IMSI", CommonMethod.getIMSI(ServerBackupDoing.this));
                        ServerBackupDoing.this.content.put(Value.ContentType, "AV_VCARD");
                        ServerBackupDoing.this.content.put(Value.Username, CommonMethod.getUserOrPassword(ServerBackupDoing.this, AntiLostCommon.DELETE_CONTACT, "user"));
                        ServerBackupDoing.this.content.put(Value.Password, CommonUtils.getConfigWithStringValue(ServerBackupDoing.this, AntiLostCommon.DELETE_CONTACT, "password", ""));
                        ServerBackupDoing.this.content.put(Value.UploadFileName, ServerBackupDoing.this.mContactUploadFilePath);
                        ServerBackupDoing.this.content.put("UID", CommonMethod.getUID(ServerBackupDoing.this));
                        ServerBackupDoing.this.result = 0;
                        if (ServerBackupDoing.this.contactService != null) {
                            ServerBackupDoing.this.result = ServerBackupDoing.this.contactService.request(ServerBackupDoing.this.mHandler, ServerBackupDoing.this.content, Value.Command_Backup_File);
                        }
                    }
                }).start();
                return;
            case 33:
                this.mContactCount = msg.arg2;
                this.mContactUploadFilePath = String.valueOf(this.mContactFilePath) + "_final";
                if (this.contactService == null) {
                    this.contactService = ContactService.getInstance(this);
                }
                new Thread(new Runnable() {
                    public void run() {
                        ServerBackupDoing.this.content.put("IMEI", CommonMethod.getIMEI(ServerBackupDoing.this));
                        ServerBackupDoing.this.content.put("IMSI", CommonMethod.getIMSI(ServerBackupDoing.this));
                        ServerBackupDoing.this.content.put(Value.ContentType, "AV_VCARD");
                        ServerBackupDoing.this.content.put(Value.Username, CommonMethod.getUserOrPassword(ServerBackupDoing.this, AntiLostCommon.DELETE_CONTACT, "user"));
                        ServerBackupDoing.this.content.put(Value.Password, CommonUtils.getConfigWithStringValue(ServerBackupDoing.this, AntiLostCommon.DELETE_CONTACT, "password", ""));
                        ServerBackupDoing.this.content.put(Value.UploadFileName, ServerBackupDoing.this.mContactUploadFilePath);
                        ServerBackupDoing.this.content.put("UID", CommonMethod.getUID(ServerBackupDoing.this));
                        ServerBackupDoing.this.result = 0;
                        ServerBackupDoing.this.result = ServerBackupDoing.this.contactService.request(ServerBackupDoing.this.mHandler, ServerBackupDoing.this.content, Value.Command_Backup_File);
                    }
                }).start();
                return;
            case CommonDefine.MSG_PROG_ARG_ERRMSG_CONTACT_EXPORT:
                ContactCommon.writeOperationLog(21, getFilesDir().getPath());
                CommonMethod.sendUserMessage(this.mHandler, 10);
                mMessageBoxText = (String) msg.obj;
                showDialog(34);
                return;
            case CommonDefine.MSG_PROG_ARG_ERRMSG_NETWORK:
                if (this.contactService != null) {
                    this.contactService.cancel();
                    this.contactService = null;
                }
                ContactCommon.writeOperationLog(21, getFilesDir().getPath());
                CommonMethod.sendUserMessage(this.mHandler, 10);
                mMessageBoxText = getString(R.string.text_uploading_fail);
                showDialog(36);
                return;
            case CommonDefine.MSG_PROG_ARG_CANCEL_SERVER:
                if (this.contactService != null) {
                    this.contactService.cancel();
                    this.contactService = null;
                }
                clickCancel();
                Toast toast = Toast.makeText(this, getResources().getString(R.string.text_backup_cantact_cancel, Integer.valueOf(msg.arg2)), 0);
                toast.setGravity(81, 0, 100);
                toast.show();
                return;
        }
    }

    /* access modifiers changed from: private */
    public void clickOk() {
        this.mContactFilePath = ContactCommon.getVCardNetworkFilePath(this);
        if (TextUtils.isEmpty(this.mContactFilePath)) {
            CommonMethod.messageDialog(this, getString(R.string.text_fail_create_file, new Object[]{this.mContactFilePath}), (int) R.string.label_netqin_antivirus);
            return;
        }
        this.mProgTextDialog = new ProgressTextDialog(this, R.string.text_exporting_contact1, this.mHandler);
        this.mProgTextDialog.setAnimationState(0);
        this.mProgTextDialog.setAnimationImageSourceFrom(R.drawable.animation_mobile);
        this.mProgTextDialog.setAnimationImageSourceTo(-1);
        this.mProgTextDialog.setLeftVisibleState(0);
        this.mHandler.post(new Runnable() {
            public void run() {
                ServerBackupDoing.this.mVCardExportThread = new VCardExportThread(ServerBackupDoing.this.mContactFilePath, ServerBackupDoing.this, ServerBackupDoing.this.mHandler, ServerBackupDoing.this.mProgTextDialog);
                ServerBackupDoing.this.mVCardExportThread.start();
            }
        });
    }

    /* access modifiers changed from: private */
    public void clickCancel() {
        finish();
        if (mServerBackDoing != null) {
            mServerBackDoing.finish();
            mServerBackDoing = null;
        }
        if (this.mProgTextDialog != null) {
            this.mProgTextDialog.dismiss();
        }
        if (this.mProgressDlg != null) {
            this.mProgressDlg.dismiss();
        }
    }

    /* access modifiers changed from: private */
    public void retryUpload() {
        this.mProgTextDialog = new ProgressTextDialog(this, R.string.text_uploading_contact2, this.mHandler);
        this.mProgTextDialog.setAnimationState(0);
        this.mProgTextDialog.setAnimationImageSourceFrom(R.drawable.animation_mobile);
        this.mProgTextDialog.setAnimationImageSourceTo(-1);
        this.mProgTextDialog.setLeftVisibleState(0);
        CommonMethod.sendUserMessage(this.mHandler, 6);
        Message msg = new Message();
        msg.what = 12;
        msg.arg1 = 33;
        msg.arg2 = this.mContactCount;
        this.mHandler.sendMessage(msg);
    }

    /* access modifiers changed from: private */
    public void clickSucc() {
        finish();
        if (mServerBackDoing != null) {
            mServerBackDoing.finish();
            mServerBackDoing = null;
        }
    }
}
