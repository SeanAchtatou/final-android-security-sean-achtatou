package com.netqin.antivirus.contact;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import com.netqin.antivirus.antilost.SmsHandler;
import com.netqin.antivirus.common.CommonDefine;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.contact.vcard.ContactData;
import com.netqin.antivirus.log.LogEngine;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ContactCommon {
    public static final String mCardContactFileName = "netqin_contact.vcf";
    public static final String mCardContactFileNameTmp = "netqin_contact.vcf.tmp";
    public static String mContactAccount = "";
    public static String mEmailAccount = "";
    public static boolean mIsMailAccount = false;
    public static final String mNetworkContactFileName = "netqin_network_up.vcf";

    public static String getSDCardFilePath(Context cnt) {
        File file = new File(CommonDefine.CONTACT_VCF_PATH_1);
        if (!file.exists() || !file.canRead()) {
            return "";
        }
        try {
            return file.getCanonicalPath();
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getSDCardFilePathTemp(Context cnt) {
        File fileTmp;
        String path;
        File fileTmp2 = new File("/sdcard");
        if (!fileTmp2.exists() || !fileTmp2.isDirectory() || !fileTmp2.canWrite()) {
            fileTmp = null;
        } else {
            fileTmp = new File("/sdcard/netqin_contact.vcf.tmp");
            if (fileTmp.exists()) {
                fileTmp.delete();
            }
            try {
                fileTmp.createNewFile();
                fileTmp.delete();
            } catch (IOException e) {
                fileTmp = null;
            }
        }
        if (fileTmp == null) {
            return null;
        }
        try {
            path = fileTmp.getCanonicalPath();
        } catch (IOException e2) {
            path = "";
            e2.printStackTrace();
        }
        return path;
    }

    public static String getVCardNetworkFilePath(Context cnt) {
        String path;
        File fileTmp = cnt.getFileStreamPath(mNetworkContactFileName);
        if (fileTmp.exists()) {
            fileTmp.delete();
        }
        try {
            fileTmp.createNewFile();
            fileTmp.delete();
        } catch (IOException e) {
            fileTmp = null;
        }
        if (fileTmp == null) {
            return null;
        }
        try {
            path = fileTmp.getCanonicalPath();
        } catch (IOException e2) {
            path = "";
            e2.printStackTrace();
        }
        return path;
    }

    public static String renameVCardBackupFile(Context cnt, String path) {
        boolean result;
        String pathSucc = path.substring(0, path.length() - 4);
        File fileTemp = new File(path);
        File fileSucc = new File(pathSucc);
        if (!fileTemp.exists()) {
            return null;
        }
        if (fileSucc.exists()) {
            fileSucc.delete();
        }
        try {
            result = fileTemp.renameTo(fileSucc);
        } catch (SecurityException se) {
            result = false;
            se.printStackTrace();
        }
        if (result) {
            return pathSucc;
        }
        return null;
    }

    public static void deleteFile(String path) {
        File file = new File(path);
        if (file.exists() && file.isFile()) {
            file.delete();
        }
    }

    public static void writeOperationLog(int type, String dbPath) {
        LogEngine logEngine = new LogEngine();
        logEngine.openDB(dbPath);
        logEngine.insertItem(type, "");
        logEngine.closeDB();
    }

    public static String floatEndWithTwo(float val) {
        return new DecimalFormat("0.00").format((double) val);
    }

    public static String replaceLineMarkWithN(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        StringBuilder tmpBuilder = new StringBuilder();
        int length = str.length();
        int i = 0;
        while (i < length) {
            char ch = str.charAt(i);
            if (ch == 13) {
                if (i + 1 < length && str.charAt(i + 1) == 10) {
                    i++;
                }
                tmpBuilder.append("\n");
            } else {
                tmpBuilder.append(ch);
            }
            i++;
        }
        return tmpBuilder.toString();
    }

    public static boolean contactViewIsExist(Intent intent, PackageManager pm) {
        if (intent == null || pm == null || intent.resolveActivity(pm) == null) {
            return false;
        }
        return true;
    }

    public static void informationProcess(Context cnt) {
        File fileMark = new File("/sdcard/nqdbg");
        if (fileMark.exists() && fileMark.canRead()) {
            try {
                FileReader fr = new FileReader(fileMark);
                BufferedReader br = new BufferedReader(fr);
                String mark = br.readLine();
                br.close();
                fr.close();
                if (!TextUtils.isEmpty(mark) && mark.compareTo("netqin") == 0) {
                    copyContactFileToCard(cnt);
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
    }

    private static void copyContactFileToCard(Context cnt) {
        try {
            CommonMethod.copyFile(cnt.getFileStreamPath(mNetworkContactFileName).getCanonicalPath(), "/sdcard/cp_netqin_network_up.vcf");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            CommonMethod.copyFile(cnt.getFileStreamPath("netqin_network_down.vcf_final").getCanonicalPath(), "/sdcard/cp_netqin_network_dw.vcf");
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    /* JADX WARN: Type inference failed for: r11v12, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getAccountInfo(android.content.Context r13, java.lang.String r14) {
        /*
            r4 = 0
            r7 = 0
            r9 = 0
            r2 = 0
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            r1.<init>()
            java.lang.String r5 = ""
            r6 = 0
            java.net.URL r10 = new java.net.URL     // Catch:{ IOException -> 0x0071 }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0071 }
            java.lang.String r12 = com.netqin.antivirus.Value.ContactAccountInfoURL     // Catch:{ IOException -> 0x0071 }
            java.lang.String r12 = java.lang.String.valueOf(r12)     // Catch:{ IOException -> 0x0071 }
            r11.<init>(r12)     // Catch:{ IOException -> 0x0071 }
            java.lang.StringBuilder r11 = r11.append(r14)     // Catch:{ IOException -> 0x0071 }
            java.lang.String r11 = r11.toString()     // Catch:{ IOException -> 0x0071 }
            r10.<init>(r11)     // Catch:{ IOException -> 0x0071 }
            java.net.Proxy r6 = com.netqin.antivirus.common.CommonMethod.getApnProxy(r13)     // Catch:{ IOException -> 0x0074 }
            if (r6 == 0) goto L_0x005c
            java.net.URLConnection r11 = r10.openConnection(r6)     // Catch:{ IOException -> 0x0074 }
            r0 = r11
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x0074 }
            r2 = r0
        L_0x0032:
            r11 = 10000(0x2710, float:1.4013E-41)
            r2.setConnectTimeout(r11)     // Catch:{ IOException -> 0x0074 }
            r11 = 10000(0x2710, float:1.4013E-41)
            r2.setReadTimeout(r11)     // Catch:{ IOException -> 0x0074 }
            java.lang.String r11 = "GET"
            r2.setRequestMethod(r11)     // Catch:{ IOException -> 0x0074 }
            java.io.InputStream r4 = r2.getInputStream()     // Catch:{ IOException -> 0x0074 }
            java.io.BufferedReader r8 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0074 }
            java.io.InputStreamReader r11 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0074 }
            r11.<init>(r4)     // Catch:{ IOException -> 0x0074 }
            r8.<init>(r11)     // Catch:{ IOException -> 0x0074 }
        L_0x004f:
            java.lang.String r5 = r8.readLine()     // Catch:{ IOException -> 0x0069 }
            if (r5 != 0) goto L_0x0065
            r9 = r10
            r7 = r8
        L_0x0057:
            java.lang.String r11 = r1.toString()
            return r11
        L_0x005c:
            java.net.URLConnection r11 = r10.openConnection()     // Catch:{ IOException -> 0x0074 }
            r0 = r11
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x0074 }
            r2 = r0
            goto L_0x0032
        L_0x0065:
            r1.append(r5)     // Catch:{ IOException -> 0x0069 }
            goto L_0x004f
        L_0x0069:
            r11 = move-exception
            r3 = r11
            r9 = r10
            r7 = r8
        L_0x006d:
            r3.printStackTrace()
            goto L_0x0057
        L_0x0071:
            r11 = move-exception
            r3 = r11
            goto L_0x006d
        L_0x0074:
            r11 = move-exception
            r3 = r11
            r9 = r10
            goto L_0x006d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.netqin.antivirus.contact.ContactCommon.getAccountInfo(android.content.Context, java.lang.String):java.lang.String");
    }

    public static String getPresentTime() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
    }

    public static int getContactCount(Context context) {
        String[] sContactsProjection = {SmsHandler.ROWID};
        Cursor cursor = context.getContentResolver().query(ContactData.CONTENT_URI, sContactsProjection, null, null, null);
        if (cursor == null) {
            cursor = context.getContentResolver().query(Uri.parse("content://contacts/people"), sContactsProjection, null, null, null);
        }
        if (cursor == null) {
            return 0;
        }
        cursor.moveToFirst();
        return cursor.getCount();
    }
}
