package com.netqin.antivirus.contact.vcard;

import java.io.IOException;
import java.io.InputStream;

public abstract class VCardParser {
    protected boolean mCanceled;

    public abstract void parse(InputStream inputStream, String str, VCardBuilder vCardBuilder, boolean z) throws IOException, VCardException;

    public abstract boolean parse(InputStream inputStream, VCardBuilder vCardBuilder) throws IOException, VCardException;

    public abstract boolean parse(InputStream inputStream, String str, VCardBuilder vCardBuilder) throws IOException, VCardException;

    public void cancel() {
        this.mCanceled = true;
    }
}
