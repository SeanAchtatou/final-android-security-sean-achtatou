package com.netqin.antivirus.contact;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.netqin.antivirus.DeclareNote;
import com.netqin.antivirus.Value;
import com.netqin.antivirus.antilost.AntiLostCommon;
import com.netqin.antivirus.appprotocol.XmlTagValue;
import com.netqin.antivirus.common.CommonDefine;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.common.CommonUtils;
import com.netqin.antivirus.common.ProgDlgActivity;
import com.netqin.antivirus.net.accountservice.AccountService;
import com.nqmobile.antivirus_ampro20.R;

public class ContactAccountCreate extends ProgDlgActivity implements TextWatcher {
    /* access modifiers changed from: private */
    public AccountService accountService;
    /* access modifiers changed from: private */
    public ContentValues content;
    /* access modifiers changed from: private */
    public Handler handler;
    /* access modifiers changed from: private */
    public EditText mAccount;
    private Button mBtnCancel;
    /* access modifiers changed from: private */
    public Button mBtnCreate;
    /* access modifiers changed from: private */
    public CheckBox mCheckBox;
    /* access modifiers changed from: private */
    public EditText mConfirmPsw;
    private TextView mNQLicense;
    private TextView mNameWarning;
    /* access modifiers changed from: private */
    public EditText mPassword;
    private TextView mPswCfmWarning;
    private TextView mPswNewWarning;
    private TextView mTitle;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.contacts_account_create);
        this.accountService = AccountService.getInstance(this);
        this.mAccount = (EditText) findViewById(R.id.contacts_account_create_user);
        this.mPassword = (EditText) findViewById(R.id.contacts_account_create_pwd);
        this.mConfirmPsw = (EditText) findViewById(R.id.contacts_account_create_pwd_confirm);
        this.mBtnCreate = (Button) findViewById(R.id.contacts_account_create_ok);
        this.mBtnCancel = (Button) findViewById(R.id.contacts_account_create_cancel);
        this.mCheckBox = (CheckBox) findViewById(R.id.account_create_agree);
        this.mNQLicense = (TextView) findViewById(R.id.netqin_license);
        this.mTitle = (TextView) findViewById(R.id.activity_name);
        this.mTitle.setText((int) R.string.label_create_account_tip);
        this.mNameWarning = (TextView) findViewById(R.id.contacts_account_create_name_warning);
        this.mPswNewWarning = (TextView) findViewById(R.id.contacts_account_create_psw_warning);
        this.mPswCfmWarning = (TextView) findViewById(R.id.contacts_account_create_psw_cfm_warning);
        this.mAccount.addTextChangedListener(this);
        this.mPassword.addTextChangedListener(this);
        this.mConfirmPsw.addTextChangedListener(this);
        Account[] googleAccount = AccountManager.get(this).getAccountsByType("com.google");
        if (googleAccount.length > 0) {
            this.mAccount.setText(googleAccount[0].name);
            this.mPassword.requestFocus();
        } else {
            this.mAccount.setText("");
        }
        this.mAccount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    String account = ContactAccountCreate.this.mAccount.getText().toString().trim();
                    if (!CommonMethod.isValidEmail(account) && !TextUtils.isEmpty(account)) {
                        CommonMethod.messageDialog(ContactAccountCreate.this, (int) R.string.text_account_invalid, (int) R.string.label_netqin_antivirus);
                        ContactAccountCreate.this.mAccount.setText("");
                    }
                }
            }
        });
        this.mBtnCreate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ContactAccountCreate.this.clickCreate();
            }
        });
        this.mBtnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ContactAccountCreate.this.clickCancel();
            }
        });
        this.mNQLicense.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ContactAccountCreate.this.startActivity(new Intent(ContactAccountCreate.this, DeclareNote.class));
            }
        });
    }

    /* access modifiers changed from: private */
    public void clickCancel() {
        Intent intent = getIntent();
        boolean isBackupFromContactGuide = intent.getBooleanExtra("createAccountBackup", false);
        boolean isRestoreFromContactGuide = intent.getBooleanExtra("createAccountRestore", false);
        if (isBackupFromContactGuide) {
            finish();
            ContactGuide.isBackupFromContactGuide = false;
        }
        if (isRestoreFromContactGuide) {
            finish();
            ContactGuide.isRestoreFromContactGuide = false;
        }
        finish();
    }

    /* access modifiers changed from: protected */
    public void clickCreate() {
        final String pwdNew = this.mPassword.getText().toString().trim();
        String pwdCfm = this.mConfirmPsw.getText().toString().trim();
        this.accountService = AccountService.getInstance(this);
        this.content = new ContentValues();
        if (!CommonMethod.isValidEmail(this.mAccount.getText().toString().trim())) {
            CommonMethod.messageDialog(this, (int) R.string.text_account_invalid, (int) R.string.label_netqin_antivirus);
        } else if (!CommonMethod.isValidLetterAndNumber(pwdNew)) {
            CommonMethod.messageDialog(this, (int) R.string.text_password_must_letter_number, (int) R.string.label_netqin_antivirus);
        } else if (pwdNew.compareTo(pwdCfm) != 0) {
            CommonMethod.messageDialog(this, (int) R.string.text_confirmpsw_wrong, (int) R.string.label_netqin_antivirus);
            this.mConfirmPsw.setText("");
        } else {
            this.mProgressText = (String) getText(R.string.text_creating_backup_account);
            this.mProgressDlg = CommonMethod.progressDlgCreate(this, this.mProgressText, this.mHandler);
            CommonMethod.sendUserMessage(this.mHandler, 1);
            new Thread(new Runnable() {
                public void run() {
                    ContactAccountCreate.this.content.put("IMEI", CommonMethod.getIMEI(ContactAccountCreate.this));
                    ContactAccountCreate.this.content.put("IMSI", CommonMethod.getIMSI(ContactAccountCreate.this));
                    ContactAccountCreate.this.content.put(Value.Username, ContactAccountCreate.this.mAccount.getText().toString().trim());
                    ContactAccountCreate.this.content.put(Value.Password, pwdNew);
                    ContactAccountCreate.this.content.put(Value.Email, ContactAccountCreate.this.mAccount.getText().toString().trim());
                    ContactAccountCreate.this.content.put("UID", CommonMethod.getUID(ContactAccountCreate.this));
                    int result = ContactAccountCreate.this.accountService.request(ContactAccountCreate.this.handler, ContactAccountCreate.this.content, 1);
                    if (result == 8 || result == 16) {
                        ContactAccountCreate.this.runOnUiThread(new Runnable() {
                            public void run() {
                                CommonMethod.messageDialog(ContactAccountCreate.this, (int) R.string.SEND_RECEIVE_ERROR, (int) R.string.label_netqin_antivirus);
                                CommonMethod.sendUserMessage(ContactAccountCreate.this.mHandler, 4);
                            }
                        });
                    } else if (ContactAccountCreate.this.content.containsKey(Value.Code)) {
                        String code = ContactAccountCreate.this.content.getAsString(Value.Code);
                        if (code.equals("0")) {
                            ContactAccountCreate.this.runOnUiThread(new Runnable() {
                                public void run() {
                                    Toast toast = Toast.makeText(ContactAccountCreate.this, (int) R.string.text_succ_create_account, 0);
                                    toast.setGravity(81, 0, 100);
                                    toast.show();
                                }
                            });
                            CommonMethod.putConfigWithStringValue(ContactAccountCreate.this, AntiLostCommon.DELETE_CONTACT, "contacts_network", "0");
                            CommonMethod.putConfigWithStringValue(ContactAccountCreate.this, AntiLostCommon.DELETE_CONTACT, "contacts_backup_time", "0");
                            Intent intent = ContactAccountCreate.this.getIntent();
                            boolean isBackupFromContactGuide = intent.getBooleanExtra("createAccountBackup", false);
                            boolean isRestoreFromContactGuide = intent.getBooleanExtra("createAccountRestore", false);
                            if (isBackupFromContactGuide) {
                                ContactGuide.isBackupFromContactGuide = false;
                                ServerBackupDoing.mFromContactGuide = true;
                                ContactAccountCreate.this.startActivity(new Intent(ContactAccountCreate.this, ServerBackupDoing.class));
                                ContactAccountCreate.this.finish();
                            }
                            if (isRestoreFromContactGuide) {
                                ContactGuide.isRestoreFromContactGuide = false;
                                ServerRestoreDoing.mFromContactGuide = true;
                                ContactAccountCreate.this.startActivity(new Intent(ContactAccountCreate.this, ServerRestoreDoing.class));
                                if (ServerBackupDoing.mServerBackDoing != null) {
                                    ServerBackupDoing.mServerBackDoing.finish();
                                }
                                ContactAccountCreate.this.finish();
                            }
                            ContactAccountCreate.this.finish();
                            if (AccountAdminGuide.mAccountAdminGuide != null) {
                                AccountAdminGuide.mAccountAdminGuide.finish();
                            }
                            CommonMethod.sendUserMessage(ContactAccountCreate.this.mHandler, 4);
                            CommonMethod.putConfigWithStringValue(ContactAccountCreate.this, AntiLostCommon.DELETE_CONTACT, "user", ContactAccountCreate.this.mAccount.getText().toString().trim());
                            CommonUtils.putConfigWithStringValue(ContactAccountCreate.this, AntiLostCommon.DELETE_CONTACT, "password", pwdNew);
                            CommonMethod.putConfigWithBooleanValue(ContactAccountCreate.this, AntiLostCommon.DELETE_CONTACT, "user_state", true);
                            CommonMethod.putConfigWithStringValue(ContactAccountCreate.this, AntiLostCommon.DELETE_CONTACT, XmlTagValue.version, CommonDefine.VERSION);
                            ContactCommon.mContactAccount = ContactAccountCreate.this.mAccount.getText().toString().trim();
                        } else if (code.equals("-1")) {
                            ContactAccountCreate.this.runOnUiThread(new Runnable() {
                                public void run() {
                                    CommonMethod.messageDialog(ContactAccountCreate.this, (int) R.string.SYSTEM_ERROR, (int) R.string.label_create_account_tip);
                                    CommonMethod.sendUserMessage(ContactAccountCreate.this.mHandler, 4);
                                }
                            });
                        } else if (code.equals("-2")) {
                            ContactAccountCreate.this.runOnUiThread(new Runnable() {
                                public void run() {
                                    CommonMethod.messageDialog(ContactAccountCreate.this, (int) R.string.USERNAME_REPEAT, (int) R.string.label_create_account_tip);
                                    CommonMethod.sendUserMessage(ContactAccountCreate.this.mHandler, 4);
                                    ContactAccountCreate.this.mAccount.setText("");
                                    ContactAccountCreate.this.mAccount.requestFocus();
                                }
                            });
                        }
                    }
                }
            }).start();
        }
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {
        String textAccount = this.mAccount.getText().toString().trim();
        String textPassword = this.mPassword.getText().toString().trim();
        String textConfirmPsw = this.mConfirmPsw.getText().toString().trim();
        if (TextUtils.isEmpty(textConfirmPsw) || TextUtils.isEmpty(textPassword) || TextUtils.isEmpty(textAccount) || !this.mCheckBox.isChecked()) {
            this.mBtnCreate.setEnabled(false);
        } else if (textPassword.length() <= 20 && textPassword.length() >= 6 && textConfirmPsw.length() <= 20 && textConfirmPsw.length() >= 6) {
            this.mBtnCreate.setEnabled(true);
        }
        if (textAccount.length() > 100) {
            this.mNameWarning.setVisibility(0);
        } else {
            this.mNameWarning.setVisibility(8);
        }
        if (textPassword.length() <= 0) {
            this.mPswNewWarning.setVisibility(8);
        } else if (textPassword.length() < 6) {
            this.mPswNewWarning.setText((int) R.string.text_less_than_6);
            this.mPswNewWarning.setVisibility(0);
            this.mBtnCreate.setEnabled(false);
        } else if (textPassword.length() > 20) {
            this.mPswNewWarning.setText((int) R.string.text_more_than_20);
            this.mPswNewWarning.setVisibility(0);
            this.mBtnCreate.setEnabled(false);
        } else {
            this.mPswNewWarning.setVisibility(8);
        }
        if (textConfirmPsw.length() <= 0) {
            this.mPswCfmWarning.setVisibility(8);
        } else if (textConfirmPsw.length() < 6) {
            this.mPswCfmWarning.setText((int) R.string.text_less_than_6);
            this.mPswCfmWarning.setVisibility(0);
            this.mBtnCreate.setEnabled(false);
        } else if (textConfirmPsw.length() > 20) {
            this.mPswCfmWarning.setText((int) R.string.text_more_than_20);
            this.mPswCfmWarning.setVisibility(0);
            this.mBtnCreate.setEnabled(false);
        } else {
            this.mPswCfmWarning.setVisibility(8);
        }
    }

    public void afterTextChanged(Editable s) {
        this.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            final String textAccount;
            final String textConfirmPsw;
            final String textPassword;

            {
                this.textAccount = ContactAccountCreate.this.mAccount.getText().toString().trim();
                this.textPassword = ContactAccountCreate.this.mPassword.getText().toString().trim();
                this.textConfirmPsw = ContactAccountCreate.this.mConfirmPsw.getText().toString().trim();
            }

            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked && !ContactAccountCreate.this.mCheckBox.isChecked()) {
                    ContactAccountCreate.this.mBtnCreate.setEnabled(false);
                } else if (TextUtils.isEmpty(this.textAccount) || TextUtils.isEmpty(this.textPassword) || TextUtils.isEmpty(this.textConfirmPsw)) {
                    ContactAccountCreate.this.mBtnCreate.setEnabled(false);
                } else if (this.textPassword.length() < 6 || this.textPassword.length() > 20 || this.textConfirmPsw.length() < 6 || this.textConfirmPsw.length() > 20) {
                    ContactAccountCreate.this.mBtnCreate.setEnabled(false);
                } else if (!isChecked) {
                    ContactAccountCreate.this.mBtnCreate.setEnabled(false);
                } else {
                    ContactAccountCreate.this.mBtnCreate.setEnabled(true);
                }
            }
        });
    }
}
