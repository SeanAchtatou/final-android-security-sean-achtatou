package com.netqin.antivirus.contact.vcard;

public class VCardVersionException extends VCardException {
    public VCardVersionException() {
    }

    public VCardVersionException(String message) {
        super(message);
    }
}
