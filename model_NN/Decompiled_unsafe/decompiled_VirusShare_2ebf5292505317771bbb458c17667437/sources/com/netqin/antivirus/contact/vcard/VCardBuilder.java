package com.netqin.antivirus.contact.vcard;

import java.util.List;

public interface VCardBuilder {
    void end();

    void endProperty();

    void endRecord();

    void propertyGroup(String str);

    void propertyName(String str);

    void propertyParamType(String str);

    void propertyParamValue(String str);

    void propertyValues(List<String> list);

    void start();

    void startProperty();

    void startRecord(String str);
}
