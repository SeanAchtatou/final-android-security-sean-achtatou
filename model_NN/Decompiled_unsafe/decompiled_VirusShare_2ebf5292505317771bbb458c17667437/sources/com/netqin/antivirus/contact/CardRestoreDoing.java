package com.netqin.antivirus.contact;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.widget.Toast;
import com.netqin.antivirus.antilost.AntiLostCommon;
import com.netqin.antivirus.common.CommonDefine;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.common.ProgDlgActivity;
import com.netqin.antivirus.common.ProgressTextDialog;
import com.netqin.antivirus.contact.vcard.ContactData;
import com.nqmobile.antivirus_ampro20.R;
import java.io.File;
import java.io.IOException;

public class CardRestoreDoing extends ProgDlgActivity {
    public static boolean mFromContactRestoreGuide = false;
    private static String mMessageBoxText = "";
    private String mContactFilePath = "";
    /* access modifiers changed from: private */
    public VCardReadThread mVCardReadThread = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        if (mFromContactRestoreGuide) {
            mFromContactRestoreGuide = false;
            mMessageBoxText = "";
            this.mContactFilePath = getSharedPreferences(AntiLostCommon.DELETE_CONTACT, 0).getString("bpfile_card", "");
            if (TextUtils.isEmpty(this.mContactFilePath)) {
                this.mContactFilePath = ContactCommon.getSDCardFilePath(this);
                if (TextUtils.isEmpty(this.mContactFilePath)) {
                    showDialog(R.string.text_not_backup_to_card_tip);
                } else {
                    showDialog(R.string.text_is_restore_from_storage_card);
                }
            } else {
                showDialog(R.string.text_is_restore_from_storage_card);
            }
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            this.mActivityVisible = false;
            if (this.mVCardReadThread != null) {
                this.mVCardReadThread.cancel();
                this.mVCardReadThread = null;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    /* access modifiers changed from: protected */
    public void progressTextDialogOnStop() {
        if (this.mVCardReadThread != null) {
            this.mVCardReadThread.cancel();
            this.mVCardReadThread = null;
        }
        super.progressTextDialogOnStop();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        DialogInterface.OnClickListener okListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                CardRestoreDoing.this.clickOk();
            }
        };
        DialogInterface.OnClickListener cancelListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                CardRestoreDoing.this.clickCancel();
            }
        };
        DialogInterface.OnKeyListener keyListener = new DialogInterface.OnKeyListener() {
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode != 4) {
                    return false;
                }
                CardRestoreDoing.this.clickCancel();
                return true;
            }
        };
        DialogInterface.OnClickListener contactListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                CardRestoreDoing.this.viewContact();
            }
        };
        DialogInterface.OnClickListener succListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                CardRestoreDoing.this.clickSucc();
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        switch (id) {
            case 20:
                builder.setTitle((int) R.string.label_netqin_antivirus);
                builder.setMessage(mMessageBoxText);
                builder.setPositiveButton((int) R.string.label_ok, cancelListener);
                builder.setOnKeyListener(keyListener);
                return builder.create();
            case 27:
                builder.setTitle((int) R.string.label_netqin_antivirus);
                builder.setMessage(mMessageBoxText);
                builder.setPositiveButton((int) R.string.label_ok, cancelListener);
                builder.setOnKeyListener(keyListener);
                return builder.create();
            case 30:
                builder.setTitle((int) R.string.label_restore_success_tip);
                builder.setMessage(mMessageBoxText);
                if (ContactCommon.contactViewIsExist(new Intent("android.intent.action.VIEW", ContactData.CONTENT_URI), getPackageManager())) {
                    builder.setPositiveButton((int) R.string.label_ok, contactListener);
                    builder.setNegativeButton((int) R.string.label_cancel, succListener);
                } else {
                    builder.setPositiveButton((int) R.string.label_ok, succListener);
                }
                builder.setOnKeyListener(keyListener);
                return builder.create();
            case CommonDefine.MSG_PROG_ARG_ERRMSG_CONTACT_IMPORT:
                builder.setTitle((int) R.string.label_netqin_antivirus);
                builder.setMessage(mMessageBoxText);
                builder.setPositiveButton((int) R.string.label_ok, cancelListener);
                builder.setOnKeyListener(keyListener);
                return builder.create();
            case R.string.text_not_backup_to_card_tip /*2131427489*/:
                builder.setTitle((int) R.string.label_netqin_antivirus);
                builder.setMessage((int) R.string.text_not_backup_to_card_tip);
                builder.setPositiveButton((int) R.string.label_ok, cancelListener);
                builder.setOnKeyListener(keyListener);
                return builder.create();
            case R.string.text_is_restore_from_storage_card /*2131428140*/:
                builder.setTitle((int) R.string.label_netqin_antivirus);
                builder.setMessage((int) R.string.text_is_restore_from_storage_card);
                builder.setPositiveButton((int) R.string.label_ok, okListener);
                builder.setNegativeButton((int) R.string.label_cancel, cancelListener);
                builder.setOnKeyListener(keyListener);
                return builder.create();
            default:
                return super.onCreateDialog(id);
        }
    }

    /* access modifiers changed from: protected */
    public void childFunctionCall(Message msg) {
        CommonMethod.sendUserMessage(this.mHandler, 10);
        switch (msg.arg1) {
            case CommonDefine.MSG_PROG_ARG_CANCEL:
                if (this.mVCardReadThread != null) {
                    this.mVCardReadThread.cancel();
                    this.mVCardReadThread = null;
                }
                clickCancel();
                Toast toast = Toast.makeText(this, getString(R.string.text_restore_cantact_cancel, new Object[]{Integer.valueOf(msg.arg2)}), 0);
                toast.setGravity(81, 0, 100);
                toast.show();
                return;
            case 27:
                ContactCommon.writeOperationLog(18, getFilesDir().getPath());
                mMessageBoxText = (String) msg.obj;
                showDialog(27);
                return;
            case 30:
                getSharedPreferences(AntiLostCommon.DELETE_CONTACT, 0).edit().putString("contacts_storagecard", Integer.toString(msg.arg2)).commit();
                int count = ContactCommon.getContactCount(this);
                if (ContactCommon.contactViewIsExist(new Intent("android.intent.action.VIEW", ContactData.CONTENT_URI), getPackageManager())) {
                    mMessageBoxText = getString(R.string.text_resotre_contact_succ_result, new Object[]{Integer.valueOf(msg.arg2), Integer.valueOf(count)});
                } else {
                    mMessageBoxText = getString(R.string.text_resotre_contact_succ_result_noview, new Object[]{Integer.valueOf(msg.arg2), Integer.valueOf(count)});
                }
                ContactCommon.writeOperationLog(17, getFilesDir().getPath());
                showDialog(30);
                return;
            case CommonDefine.MSG_PROG_ARG_ERRMSG_CONTACT_IMPORT:
                ContactCommon.writeOperationLog(24, getFilesDir().getPath());
                mMessageBoxText = (String) msg.obj;
                showDialog(35);
                return;
            case CommonDefine.MSG_PROG_ARG_CANCEL_SERVER:
                return;
            default:
                mMessageBoxText = getString(R.string.text_unprocess_message, new Object[]{Integer.valueOf(msg.arg1)});
                showDialog(20);
                return;
        }
    }

    /* access modifiers changed from: private */
    public void clickOk() {
        final File file = new File(this.mContactFilePath);
        if (!file.exists() || !file.canRead()) {
            CommonMethod.messageDialog(this, getString(R.string.text_contact_not_exist, new Object[]{this.mContactFilePath}), (int) R.string.label_netqin_antivirus, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    CardRestoreDoing.this.clickCancel();
                }
            }, new DialogInterface.OnKeyListener() {
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    if (keyCode != 4) {
                        return false;
                    }
                    CardRestoreDoing.this.clickCancel();
                    return true;
                }
            });
            return;
        }
        this.mProgTextDialog = new ProgressTextDialog(this, R.string.text_restoring_contacts_from_sdcard, this.mHandler);
        this.mProgTextDialog.setAnimationState(0);
        this.mProgTextDialog.setAnimationImageSourceFrom(R.drawable.animation_card);
        this.mProgTextDialog.setAnimationImageSourceTo(R.drawable.animation_mobile);
        this.mHandler.post(new Runnable() {
            public void run() {
                try {
                    CardRestoreDoing.this.mVCardReadThread = new VCardReadThread(file.getCanonicalPath(), CardRestoreDoing.this, CardRestoreDoing.this.mHandler, CardRestoreDoing.this.mProgTextDialog);
                    CardRestoreDoing.this.mVCardReadThread.start();
                } catch (IOException e) {
                    IOException e2 = e;
                    CommonMethod.messageDialog(CardRestoreDoing.this, e2.getLocalizedMessage(), (int) R.string.label_netqin_antivirus);
                    e2.printStackTrace();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void clickCancel() {
        if (this.mProgTextDialog != null) {
            this.mProgTextDialog.dismiss();
        }
        if (this.mProgressDlg != null) {
            this.mProgressDlg.dismiss();
        }
        finish();
    }

    /* access modifiers changed from: private */
    public void viewContact() {
        finish();
        try {
            startActivity(new Intent("android.intent.action.VIEW", ContactData.CONTENT_URI));
        } catch (ActivityNotFoundException e) {
            Toast toast = Toast.makeText(this, e.getLocalizedMessage(), 0);
            toast.setGravity(81, 0, 100);
            toast.show();
        }
    }

    /* access modifiers changed from: private */
    public void clickSucc() {
        finish();
    }
}
