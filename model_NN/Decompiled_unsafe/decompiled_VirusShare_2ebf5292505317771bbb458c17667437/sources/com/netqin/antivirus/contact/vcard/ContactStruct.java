package com.netqin.antivirus.contact.vcard;

import android.accounts.Account;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import com.netqin.antivirus.antilost.ContactsHandler;
import com.netqin.antivirus.appprotocol.XmlTagValue;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.contact.vcard.ContactData;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ContactStruct {
    private static final String ACCOUNT_TYPE_GOOGLE = "com.google";
    private static final String GOOGLE_MY_CONTACTS_GROUP = "System Group: My Contacts";
    private static final String LOG_TAG = "vcard.ContactStruct";
    private static final String LOG_TAG_1 = "contactstruct";
    private static final String LOG_TAG_2 = "contactstruct_getsame";
    private static final String LOG_TAG_3 = "contactstruct_update";
    private static final String LOG_TAG_4 = "contactstruct_insert";
    private static final int MARK_ELEMENTS_EQU = 3;
    private static final int MARK_ELEMENTS_MAX = 2;
    private static final int MARK_ELEMENTS_MIN = 1;
    private static final int MARK_ELEMENTS_UNDEFINE = 0;
    private static int mCompletedItems = 0;
    private static String mCurrentDisplayName = "";
    private static final List<String> mIdKeyRemoveList = new ArrayList();
    private static final Map<String, Integer> mRawIdMarkElements = new HashMap();
    private static boolean mRunLoop = true;
    private static final Map<String, Integer> sImMap = new HashMap();
    private final Account mAccount;
    private String mBirthday;
    private String mDisplayName;
    private List<ContactData.EmailData> mEmailList;
    private String mFamilyName;
    private String mFullName;
    private String mGivenName;
    private List<ContactData.ImData> mImList;
    private String mMiddleName;
    private List<String> mNickNameList;
    private List<String> mNoteList;
    private List<ContactData.OrganizationData> mOrganizationList;
    private List<ContactData.PhoneData> mPhoneList;
    private String mPhoneticFamilyName;
    private String mPhoneticFullName;
    private String mPhoneticGivenName;
    private String mPhoneticMiddleName;
    private List<ContactData.PhotoData> mPhotoList;
    private List<ContactData.PostalData> mPostalList;
    private boolean mPrefIsSet_Address;
    private boolean mPrefIsSet_Email;
    private boolean mPrefIsSet_Organization;
    private boolean mPrefIsSet_Phone;
    private String mPrefix;
    private String mSuffix;
    private final int mVCardType;
    private List<String> mWebsiteList;

    static {
        sImMap.put(Constants.PROPERTY_X_AIM, 0);
        sImMap.put(Constants.PROPERTY_X_MSN, 1);
        sImMap.put(Constants.PROPERTY_X_YAHOO, 2);
        sImMap.put(Constants.PROPERTY_X_ICQ, 6);
        sImMap.put(Constants.PROPERTY_X_JABBER, 7);
        sImMap.put(Constants.PROPERTY_X_SKYPE_USERNAME, 3);
        sImMap.put(Constants.PROPERTY_X_GOOGLE_TALK, 5);
        sImMap.put(Constants.PROPERTY_X_GOOGLE_TALK_WITH_SPACE, 5);
        sImMap.put(Constants.PROPERTY_X_QQ, 4);
    }

    static class Property {
        /* access modifiers changed from: private */
        public Map<String, Collection<String>> mParameterMap = new HashMap();
        /* access modifiers changed from: private */
        public byte[] mPropertyBytes;
        /* access modifiers changed from: private */
        public String mPropertyName;
        /* access modifiers changed from: private */
        public List<String> mPropertyValueList = new ArrayList();

        public Property() {
            clear();
        }

        public void setPropertyName(String propertyName) {
            this.mPropertyName = propertyName;
        }

        public void addParameter(String paramName, String paramValue) {
            Collection<String> values;
            if (!this.mParameterMap.containsKey(paramName)) {
                if (paramName.equals(Constants.ATTR_TYPE)) {
                    values = new HashSet<>();
                } else {
                    values = new ArrayList<>();
                }
                this.mParameterMap.put(paramName, values);
            } else {
                values = this.mParameterMap.get(paramName);
            }
            values.add(paramValue);
        }

        public void addToPropertyValueList(String propertyValue) {
            this.mPropertyValueList.add(propertyValue);
        }

        public void setPropertyBytes(byte[] propertyBytes) {
            this.mPropertyBytes = propertyBytes;
        }

        public final Collection<String> getParameters(String type) {
            return this.mParameterMap.get(type);
        }

        public final List<String> getPropertyValueList() {
            return this.mPropertyValueList;
        }

        public void clear() {
            this.mPropertyName = null;
            this.mParameterMap.clear();
            this.mPropertyValueList.clear();
        }
    }

    public ContactStruct() {
        this(VCardConfig.VCARD_TYPE_V21_GENERIC);
    }

    public ContactStruct(int vcardType) {
        this(vcardType, null);
    }

    public ContactStruct(int vcardType, Account account) {
        this.mFamilyName = "";
        this.mGivenName = "";
        this.mMiddleName = "";
        this.mPrefix = "";
        this.mSuffix = "";
        this.mFullName = "";
        this.mPhoneticFamilyName = "";
        this.mPhoneticGivenName = "";
        this.mPhoneticMiddleName = "";
        this.mPhoneticFullName = "";
        this.mVCardType = vcardType;
        this.mAccount = account;
    }

    public ContactStruct(String givenName, String familyName, String middleName, String prefix, String suffix, String phoneticGivenName, String pheneticFamilyName, String phoneticMiddleName, List<byte[]> list, List<String> list2, List<ContactData.PhoneData> list3, List<ContactData.EmailData> emailList, List<ContactData.PostalData> postalList, List<ContactData.OrganizationData> organizationList, List<ContactData.PhotoData> photoList, List<String> websiteList) {
        this(VCardConfig.VCARD_TYPE_DEFAULT);
        this.mGivenName = givenName;
        this.mFamilyName = familyName;
        this.mPrefix = prefix;
        this.mSuffix = suffix;
        this.mPhoneticGivenName = givenName;
        this.mPhoneticFamilyName = familyName;
        this.mPhoneticMiddleName = middleName;
        this.mEmailList = emailList;
        this.mPostalList = postalList;
        this.mOrganizationList = organizationList;
        this.mPhotoList = photoList;
        this.mWebsiteList = websiteList;
    }

    public String getFamilyName() {
        return this.mFamilyName;
    }

    public String getGivenName() {
        return this.mGivenName;
    }

    public String getMiddleName() {
        return this.mMiddleName;
    }

    public String getPrefix() {
        return this.mPrefix;
    }

    public String getSuffix() {
        return this.mSuffix;
    }

    public String getFullName() {
        return this.mFullName;
    }

    public String getPhoneticFamilyName() {
        return this.mPhoneticFamilyName;
    }

    public String getPhoneticGivenName() {
        return this.mPhoneticGivenName;
    }

    public String getPhoneticMiddleName() {
        return this.mPhoneticMiddleName;
    }

    public String getPhoneticFullName() {
        return this.mPhoneticFullName;
    }

    public final List<String> getNickNameList() {
        return this.mNickNameList;
    }

    public String getDisplayName() {
        if (TextUtils.isEmpty(this.mDisplayName)) {
            constructDisplayName();
        }
        return this.mDisplayName;
    }

    public String getBirthday() {
        return this.mBirthday;
    }

    public final List<ContactData.PhotoData> getPhotoList() {
        return this.mPhotoList;
    }

    public final List<String> getNotes() {
        return this.mNoteList;
    }

    public final List<ContactData.PhoneData> getPhoneList() {
        return this.mPhoneList;
    }

    public final List<ContactData.EmailData> getEmailList() {
        return this.mEmailList;
    }

    public final List<ContactData.PostalData> getPostalList() {
        return this.mPostalList;
    }

    public final List<ContactData.OrganizationData> getOrganizationList() {
        return this.mOrganizationList;
    }

    private void addPhone(int type, String data, String label, boolean isPrimary) {
        if (this.mPhoneList == null) {
            this.mPhoneList = new ArrayList();
        }
        StringBuilder builder = new StringBuilder();
        String trimed = data.trim();
        int length = trimed.length();
        for (int i = 0; i < length; i++) {
            char ch = trimed.charAt(i);
            if (('0' <= ch && ch <= '9') || (i == 0 && ch == '+')) {
                builder.append(ch);
            }
        }
        this.mPhoneList.add(new ContactData.PhoneData(type, PhoneNumberUtils.formatNumber(builder.toString()), label, isPrimary));
    }

    private void addNickName(String nickName) {
        if (this.mNickNameList == null) {
            this.mNickNameList = new ArrayList();
        }
        this.mNickNameList.add(nickName);
    }

    private void addEmail(int type, String data, String label, boolean isPrimary) {
        if (this.mEmailList == null) {
            this.mEmailList = new ArrayList();
        }
        this.mEmailList.add(new ContactData.EmailData(type, data, label, isPrimary));
    }

    private void addPostal(int type, List<String> propValueList, String label, boolean isPrimary) {
        if (this.mPostalList == null) {
            this.mPostalList = new ArrayList();
        }
        this.mPostalList.add(new ContactData.PostalData(type, propValueList, label, isPrimary));
    }

    private void addOrganization(int type, String companyName, String positionName, boolean isPrimary) {
        if (this.mOrganizationList == null) {
            this.mOrganizationList = new ArrayList();
        }
        this.mOrganizationList.add(new ContactData.OrganizationData(type, companyName, positionName, isPrimary));
    }

    private void addIm(int type, String data, String label, boolean isPrimary) {
        if (this.mImList == null) {
            this.mImList = new ArrayList();
        }
        this.mImList.add(new ContactData.ImData(type, data, label, isPrimary));
    }

    private void addImEx(int type, String label, String data, boolean isPrimary, int protocalName, String protocalCustom) {
        if (this.mImList == null) {
            this.mImList = new ArrayList();
        }
        this.mImList.add(new ContactData.ImData(-1, type, label, data, protocalName, protocalCustom));
    }

    private void addNote(String note) {
        if (this.mNoteList == null) {
            this.mNoteList = new ArrayList(1);
        }
        this.mNoteList.add(note);
    }

    private void addPhotoBytes(String formatName, byte[] photoBytes) {
        if (this.mPhotoList == null) {
            this.mPhotoList = new ArrayList(1);
        }
        this.mPhotoList.add(new ContactData.PhotoData(0, null, photoBytes));
    }

    private void setPosition(String positionValue) {
        if (this.mOrganizationList == null) {
            this.mOrganizationList = new ArrayList();
        }
        int size = this.mOrganizationList.size();
        if (size == 0) {
            addOrganization(2, "", null, false);
            size = 1;
        }
        this.mOrganizationList.get(size - 1).positionName = positionValue;
    }

    private void handleNProperty(List<String> elems) {
        int size;
        if (elems != null && (size = elems.size()) >= 1) {
            if (size > 5) {
                size = 5;
            }
            switch (size) {
                case 5:
                    this.mSuffix = elems.get(4);
                case 4:
                    this.mPrefix = elems.get(3);
                case 3:
                    this.mMiddleName = elems.get(2);
                case 2:
                    this.mGivenName = elems.get(1);
                    break;
            }
            this.mFamilyName = elems.get(0);
        }
    }

    private void handlePhoneticNameFromSound(List<String> elems) {
        int size;
        if (elems != null && (size = elems.size()) >= 1) {
            if (size > 3) {
                size = 3;
            }
            switch (size) {
                case 3:
                    this.mPhoneticMiddleName = elems.get(2);
                case 2:
                    this.mPhoneticGivenName = elems.get(1);
                    break;
            }
            this.mPhoneticFamilyName = elems.get(0);
        }
    }

    public void addProperty(Property property) {
        boolean isPrimary;
        int type;
        String label;
        boolean isPrimary2;
        String typeString;
        String propName = property.mPropertyName;
        Map<String, Collection<String>> paramMap = property.mParameterMap;
        List<String> propValueList = property.mPropertyValueList;
        byte[] propBytes = property.mPropertyBytes;
        if (propValueList.size() != 0) {
            String propValue = listToString(propValueList).trim();
            if (propName.equals("VERSION")) {
                return;
            }
            if (propName.equals("FN")) {
                this.mFullName = propValue;
            } else if (propName.equals("NAME") && this.mFullName == null) {
                this.mFullName = propValue;
            } else if (propName.equals("N")) {
                handleNProperty(propValueList);
            } else if (propName.equals("SORT-STRING")) {
                this.mPhoneticFullName = propValue;
            } else if (propName.equals("NICKNAME") || propName.equals("X-NICKNAME")) {
                addNickName(propValue);
            } else if (propName.equals("SOUND")) {
                Collection<String> typeCollection = paramMap.get(Constants.ATTR_TYPE);
                if (typeCollection != null && typeCollection.contains(Constants.ATTR_TYPE_X_IRMC_N)) {
                    handlePhoneticNameFromSound(propValueList);
                }
            } else if (propName.equals("ADR")) {
                boolean valuesAreAllEmpty = true;
                Iterator it = propValueList.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (((String) it.next()).length() > 0) {
                            valuesAreAllEmpty = false;
                            break;
                        }
                    } else {
                        break;
                    }
                }
                if (!valuesAreAllEmpty) {
                    int type2 = -1;
                    String label2 = "";
                    boolean isPrimary3 = false;
                    Collection<String> typeCollection2 = paramMap.get(Constants.ATTR_TYPE);
                    if (typeCollection2 != null) {
                        for (String typeString2 : typeCollection2) {
                            String typeString3 = typeString2.toUpperCase();
                            if (typeString3.equals(Constants.ATTR_TYPE_PREF) && !this.mPrefIsSet_Address) {
                                this.mPrefIsSet_Address = true;
                                isPrimary3 = true;
                            } else if (typeString3.equals(Constants.ATTR_TYPE_HOME)) {
                                type2 = 1;
                                label2 = "";
                            } else if (typeString3.equals(Constants.ATTR_TYPE_WORK) || typeString3.equalsIgnoreCase("COMPANY")) {
                                type2 = 2;
                                label2 = "";
                            } else if (!typeString3.equals("PARCEL") && !typeString3.equals("DOM") && !typeString3.equals("INTL")) {
                                if (typeString3.startsWith("X-") && type2 < 0) {
                                    typeString3 = typeString3.substring(2);
                                }
                                type2 = 0;
                                label2 = typeString3;
                            }
                        }
                    }
                    if (type2 < 0) {
                        type2 = 1;
                    }
                    addPostal(type2, propValueList, label2, isPrimary3);
                }
            } else if (propName.equals("EMAIL")) {
                int type3 = -1;
                String label3 = null;
                boolean isPrimary4 = false;
                Collection<String> typeCollection3 = paramMap.get(Constants.ATTR_TYPE);
                if (typeCollection3 != null) {
                    for (String typeString4 : typeCollection3) {
                        String typeStringOrg = typeString4;
                        String typeString5 = typeString4.toUpperCase();
                        if (typeString5.equals(Constants.ATTR_TYPE_PREF) && !this.mPrefIsSet_Email) {
                            this.mPrefIsSet_Email = true;
                            isPrimary4 = true;
                        } else if (typeString5.equals(Constants.ATTR_TYPE_HOME)) {
                            type3 = 1;
                        } else if (typeString5.equals(Constants.ATTR_TYPE_WORK)) {
                            type3 = 2;
                        } else if (typeString5.equals(Constants.ATTR_TYPE_CELL)) {
                            type3 = 4;
                        } else if (typeString5.equals(Constants.ATTR_TYPE_INTERNET)) {
                            type3 = 3;
                        } else {
                            if (!typeString5.startsWith("X-") || type3 >= 0) {
                                typeString = typeStringOrg;
                            } else {
                                typeString = typeStringOrg.substring(2);
                            }
                            type3 = 0;
                            label3 = typeString;
                        }
                    }
                }
                if (type3 < 0) {
                    type3 = 3;
                }
                addEmail(type3, propValue, label3, isPrimary4);
            } else if (propName.equals("ORG")) {
                boolean isPrimary5 = false;
                Collection<String> typeCollection4 = paramMap.get(Constants.ATTR_TYPE);
                if (typeCollection4 != null) {
                    for (String typeString6 : typeCollection4) {
                        if (typeString6.equals(Constants.ATTR_TYPE_PREF) && !this.mPrefIsSet_Organization) {
                            this.mPrefIsSet_Organization = true;
                            isPrimary5 = true;
                        }
                    }
                }
                StringBuilder builder = new StringBuilder();
                Iterator<String> iter = propValueList.iterator();
                while (iter.hasNext()) {
                    builder.append((String) iter.next());
                    if (iter.hasNext()) {
                        builder.append(' ');
                    }
                }
                addOrganization(1, builder.toString(), "", isPrimary5);
            } else if (propName.equals("TITLE")) {
                setPosition(propValue);
            } else if (propName.equals("ROLE")) {
                setPosition(propValue);
            } else if (propName.equals("PHOTO") || propName.equals("LOGO")) {
                String formatName = null;
                Collection<String> typeCollection5 = paramMap.get(Constants.ATTR_TYPE);
                if (typeCollection5 != null) {
                    formatName = (String) typeCollection5.iterator().next();
                }
                Collection<String> paramMapValue = paramMap.get("VALUE");
                if (paramMapValue == null || !paramMapValue.contains(XmlTagValue.uRL)) {
                    addPhotoBytes(formatName, propBytes);
                }
            } else if (propName.equals("TEL")) {
                Collection<String> typeCollection6 = paramMap.get(Constants.ATTR_TYPE);
                Object typeObject = VCardUtils.getPhoneTypeFromStrings(typeCollection6);
                if (typeObject instanceof Integer) {
                    type = ((Integer) typeObject).intValue();
                    label = null;
                } else {
                    type = 0;
                    label = typeObject.toString();
                }
                if (this.mPrefIsSet_Phone || typeCollection6 == null || !typeCollection6.contains(Constants.ATTR_TYPE_PREF)) {
                    isPrimary2 = false;
                } else {
                    this.mPrefIsSet_Phone = true;
                    isPrimary2 = true;
                }
                addPhone(type, propValue, label, isPrimary2);
            } else if (propName.equals(Constants.PROPERTY_X_SKYPE_PSTNNUMBER)) {
                Collection<String> typeCollection7 = paramMap.get(Constants.ATTR_TYPE);
                if (this.mPrefIsSet_Phone || typeCollection7 == null || !typeCollection7.contains(Constants.ATTR_TYPE_PREF)) {
                    isPrimary = false;
                } else {
                    this.mPrefIsSet_Phone = true;
                    isPrimary = true;
                }
                addPhone(7, propValue, null, isPrimary);
            } else if (sImMap.containsKey(propName)) {
                int protocalName = sImMap.get(propName).intValue();
                boolean isPrimary6 = false;
                Collection<String> typeCollection8 = paramMap.get(Constants.ATTR_TYPE);
                if (typeCollection8 != null) {
                    for (String typeString7 : typeCollection8) {
                        if (typeString7.equals(Constants.ATTR_TYPE_PREF)) {
                            isPrimary6 = true;
                        }
                    }
                }
                addImEx(3, "", propValue, isPrimary6, protocalName, "");
            } else if (propName.startsWith(Constants.PROPERTY_X_IM_MARK)) {
                boolean isPrimary7 = false;
                Collection<String> typeCollection9 = paramMap.get(Constants.ATTR_TYPE);
                if (typeCollection9 != null) {
                    for (String typeString8 : typeCollection9) {
                        if (typeString8.equals(Constants.ATTR_TYPE_PREF)) {
                            isPrimary7 = true;
                        }
                    }
                }
                addImEx(0, "", propValue, isPrimary7, -1, propName.substring(Constants.PROPERTY_X_IM_MARK.length()));
            } else if (propName.equals("NOTE")) {
                addNote(propValue);
            } else if (propName.equals(XmlTagValue.uRL)) {
                if (this.mWebsiteList == null) {
                    this.mWebsiteList = new ArrayList(1);
                }
                this.mWebsiteList.add(propValue);
            } else if (propName.equals("X-PHONETIC-FIRST-NAME")) {
                this.mPhoneticGivenName = propValue;
            } else if (propName.equals("X-PHONETIC-MIDDLE-NAME")) {
                this.mPhoneticMiddleName = propValue;
            } else if (propName.equals("X-PHONETIC-LAST-NAME")) {
                this.mPhoneticFamilyName = propValue;
            } else if (propName.equals("BDAY")) {
                this.mBirthday = propValue;
            }
        }
    }

    private void constructDisplayName() {
        List<String> nameList;
        if (!TextUtils.isEmpty(this.mFamilyName) || !TextUtils.isEmpty(this.mGivenName)) {
            StringBuilder builder = new StringBuilder();
            switch (VCardConfig.getNameOrderType(this.mVCardType)) {
                case 4:
                    nameList = Arrays.asList(this.mPrefix, this.mMiddleName, this.mGivenName, this.mFamilyName, this.mSuffix);
                    break;
                case 8:
                    if (VCardUtils.containsOnlyPrintableAscii(this.mFamilyName) && VCardUtils.containsOnlyPrintableAscii(this.mGivenName)) {
                        nameList = Arrays.asList(this.mPrefix, this.mGivenName, this.mMiddleName, this.mFamilyName, this.mSuffix);
                        break;
                    } else {
                        nameList = Arrays.asList(this.mPrefix, this.mFamilyName, this.mMiddleName, this.mGivenName, this.mSuffix);
                        break;
                    }
                default:
                    nameList = Arrays.asList(this.mPrefix, this.mGivenName, this.mMiddleName, this.mFamilyName, this.mSuffix);
                    break;
            }
            boolean first = true;
            for (String namePart : nameList) {
                if (!TextUtils.isEmpty(namePart)) {
                    if (first) {
                        first = false;
                    } else {
                        builder.append(' ');
                    }
                    builder.append(namePart);
                }
            }
            this.mDisplayName = builder.toString();
        } else if (!TextUtils.isEmpty(this.mFullName)) {
            this.mDisplayName = this.mFullName;
        } else if (!TextUtils.isEmpty(this.mPhoneticFamilyName) || !TextUtils.isEmpty(this.mPhoneticGivenName)) {
            this.mDisplayName = VCardUtils.constructNameFromElements(this.mVCardType, this.mPhoneticFamilyName, this.mPhoneticMiddleName, this.mPhoneticGivenName);
        } else if (this.mEmailList != null && this.mEmailList.size() > 0) {
            this.mDisplayName = this.mEmailList.get(0).data;
        } else if (this.mPhoneList != null && this.mPhoneList.size() > 0) {
            this.mDisplayName = this.mPhoneList.get(0).data;
        } else if (this.mPostalList != null && this.mPostalList.size() > 0) {
            this.mDisplayName = this.mPostalList.get(0).getFormattedAddress(this.mVCardType);
        }
        if (this.mDisplayName == null) {
            this.mDisplayName = "";
        }
    }

    public void consolidateFields() {
        constructDisplayName();
        if (this.mPhoneticFullName != null) {
            this.mPhoneticFullName = this.mPhoneticFullName.trim();
        }
        if (!this.mPrefIsSet_Phone && this.mPhoneList != null && this.mPhoneList.size() > 0) {
            this.mPhoneList.get(0).isPrimary = true;
        }
        if (!this.mPrefIsSet_Address && this.mPostalList != null && this.mPostalList.size() > 0) {
            this.mPostalList.get(0).isPrimary = true;
        }
        if (!this.mPrefIsSet_Email && this.mEmailList != null && this.mEmailList.size() > 0) {
            this.mEmailList.get(0).isPrimary = true;
        }
        if (!this.mPrefIsSet_Organization && this.mOrganizationList != null && this.mOrganizationList.size() > 0) {
            this.mOrganizationList.get(0).isPrimary = true;
        }
    }

    private void checkIdKeyRemoveList() {
        if (mIdKeyRemoveList != null) {
            for (String rmKey : mIdKeyRemoveList) {
                ContactData.mRawIDContactMap.remove(rmKey);
                ContactData.mRawIDContactMap.remove(rmKey);
                ContactData.mIDStructureNameMap.remove(rmKey);
                ContactData.mIDNickNameMap.remove(rmKey);
                ContactData.mIDPhoneMap.remove(rmKey);
                ContactData.mIDPostalMap.remove(rmKey);
                ContactData.mIDEmailMap.remove(rmKey);
                ContactData.mIDImMap.remove(rmKey);
                ContactData.mIDEventMap.remove(rmKey);
                ContactData.mIDWebsiteMap.remove(rmKey);
                ContactData.mIDOrganizationMap.remove(rmKey);
                ContactData.mIDNoteMap.remove(rmKey);
            }
        }
    }

    private void setElementsMark(int fileMapSize, int sysMapSize, String rawId) {
        int markValue;
        Integer value;
        int markOrg = 0;
        if (mRawIdMarkElements.size() > 0 && (value = mRawIdMarkElements.get(rawId)) != null) {
            markOrg = value.intValue();
        }
        if (fileMapSize > sysMapSize) {
            markValue = 2;
        } else if (fileMapSize < sysMapSize) {
            markValue = 1;
        } else {
            markValue = 3;
        }
        if (markOrg != 1 && markOrg != 2) {
            mRawIdMarkElements.put(rawId, Integer.valueOf(markValue));
        } else if (markOrg == markValue) {
        } else {
            if (markValue == 1 || markValue == 2) {
                mIdKeyRemoveList.add(rawId);
            }
        }
    }

    private void setElementsMarkEx(int markValue, String rawId) {
        Integer value;
        int markOrg = 0;
        if (mRawIdMarkElements.size() > 0 && (value = mRawIdMarkElements.get(rawId)) != null) {
            markOrg = value.intValue();
        }
        if (markOrg != 1 && markOrg != 2) {
            mRawIdMarkElements.put(rawId, Integer.valueOf(markValue));
        } else if (markOrg == markValue) {
        } else {
            if (markValue == 1 || markValue == 2) {
                mIdKeyRemoveList.add(rawId);
            }
        }
    }

    private void setElementsMarkMin(String rawId) {
        Integer elementType = mRawIdMarkElements.get(rawId);
        if (elementType == null || elementType.intValue() == 0 || elementType.intValue() == 3) {
            setElementsMarkEx(1, rawId);
        } else if (elementType.intValue() == 2) {
            mIdKeyRemoveList.add(rawId);
        }
    }

    private ContactData.IdData getSameContact(ContentResolver resolver) {
        String displayName;
        Map<String, ContactData.NoteData> minNoteMap;
        Map<String, ContactData.NoteData> maxNoteMap;
        Map<String, ContactData.OrganizationData> minOrganizationMap;
        Map<String, ContactData.OrganizationData> maxOrganizationMap;
        Map<String, ContactData.WebsiteData> minWebMap;
        Map<String, ContactData.WebsiteData> maxWebMap;
        Map<String, ContactData.ImData> minImMap;
        Map<String, ContactData.ImData> maxImMap;
        Map<String, ContactData.EmailData> minEmailMap;
        Map<String, ContactData.EmailData> maxEmailMap;
        Map<String, ContactData.PostalData> minPostalMap;
        Map<String, ContactData.PostalData> maxPostalMap;
        Map<String, ContactData.PhoneData> minPhoneMap;
        Map<String, ContactData.PhoneData> maxPhoneMap;
        String tmpStr;
        Map<String, ContactData.NickNameData> minNickMap;
        Map<String, ContactData.NickNameData> maxNickMap;
        long currentTimeMillis = System.currentTimeMillis();
        VCardContact vCardContact = new VCardContact(resolver);
        mRawIdMarkElements.clear();
        boolean isNewContactItem = false;
        long currentTimeMillis2 = System.currentTimeMillis();
        if (!TextUtils.isEmpty(this.mGivenName) || !TextUtils.isEmpty(this.mFamilyName) || !TextUtils.isEmpty(this.mMiddleName) || !TextUtils.isEmpty(this.mPrefix) || !TextUtils.isEmpty(this.mSuffix) || !TextUtils.isEmpty(this.mPhoneticGivenName) || !TextUtils.isEmpty(this.mPhoneticFamilyName) || !TextUtils.isEmpty(this.mPhoneticMiddleName)) {
            if (!TextUtils.isEmpty(this.mGivenName)) {
                displayName = this.mGivenName;
            } else if (!TextUtils.isEmpty(this.mFamilyName)) {
                displayName = this.mFamilyName;
            } else if (!TextUtils.isEmpty(this.mMiddleName)) {
                displayName = this.mMiddleName;
            } else {
                displayName = getDisplayName();
            }
            if (displayName.indexOf("'") >= 0) {
                displayName = displayName.replace("'", "''");
            }
            if (vCardContact.queryContactDisplayName(ContactData.mRawIDContactMap, "display_name LIKE '%" + displayName.trim() + "%'", null) <= 0) {
                isNewContactItem = true;
            }
        } else {
            isNewContactItem = true;
        }
        long currentTimeMillis3 = System.currentTimeMillis();
        if (isNewContactItem) {
            return null;
        }
        mIdKeyRemoveList.clear();
        long prevTime = System.currentTimeMillis();
        if (mRunLoop && ContactData.mRawIDContactMap.size() > 0) {
            for (Map.Entry me1 : ContactData.mRawIDContactMap.entrySet()) {
                String rawId = (String) me1.getKey();
                if (vCardContact.queryContactStructureName(ContactData.mIDStructureNameMap, rawId, "contact_id=? AND mimetype='vnd.android.cursor.item/name'", new String[]{((ContactData.IdData) me1.getValue()).mContactId}) > 0) {
                    Map<String, ContactData.StructureNameData> structNameMap = ContactData.mIDStructureNameMap.get(rawId);
                    if (structNameMap != null || structNameMap.size() > 0) {
                        vCardContact.checkSameStructureName(rawId, mIdKeyRemoveList, new ContactData.StructureNameData(-1, this.mGivenName, this.mFamilyName, this.mPrefix, this.mMiddleName, this.mSuffix, this.mDisplayName), structNameMap);
                    } else {
                        setElementsMarkMin(rawId);
                    }
                } else {
                    setElementsMarkEx(2, rawId);
                }
            }
        }
        checkIdKeyRemoveList();
        long currTime = System.currentTimeMillis();
        mIdKeyRemoveList.clear();
        long prevTime2 = System.currentTimeMillis();
        if (mRunLoop && ContactData.mRawIDContactMap.size() > 0) {
            Map<String, ContactData.NickNameData> nickFileMap = new HashMap<>();
            if (this.mNickNameList != null) {
                for (String nickData : this.mNickNameList) {
                    ContactData.NickNameData nickNameData = new ContactData.NickNameData(-1, 1, "", nickData);
                    nickFileMap.put(nickNameData.mTypeLabelNick, nickNameData);
                }
            }
            for (Map.Entry me12 : ContactData.mRawIDContactMap.entrySet()) {
                String rawId2 = (String) me12.getKey();
                if (vCardContact.queryContactNickName(ContactData.mIDNickNameMap, rawId2, "contact_id=? AND mimetype='vnd.android.cursor.item/nickname'", new String[]{((ContactData.IdData) me12.getValue()).mContactId}) > 0) {
                    Map<String, ContactData.NickNameData> nickSysMap = ContactData.mIDNickNameMap.get(rawId2);
                    if (nickFileMap.size() > 0) {
                        setElementsMark(nickFileMap.size(), nickSysMap.size(), rawId2);
                        switch (mRawIdMarkElements.get(rawId2).intValue()) {
                            case 1:
                                minNickMap = nickFileMap;
                                maxNickMap = nickSysMap;
                                break;
                            case 2:
                                minNickMap = nickSysMap;
                                maxNickMap = nickFileMap;
                                break;
                            default:
                                minNickMap = nickFileMap;
                                maxNickMap = nickSysMap;
                                break;
                        }
                        vCardContact.checkSameNickName(rawId2, mIdKeyRemoveList, minNickMap, maxNickMap);
                    } else {
                        setElementsMarkMin(rawId2);
                    }
                } else if (nickFileMap.size() > 0) {
                    setElementsMarkEx(2, rawId2);
                }
            }
        }
        checkIdKeyRemoveList();
        long currTime2 = System.currentTimeMillis();
        mIdKeyRemoveList.clear();
        long prevTime3 = System.currentTimeMillis();
        if (mRunLoop && ContactData.mRawIDContactMap.size() > 0) {
            Map<String, ContactData.PhoneData> phoneFileMap = new HashMap<>();
            if (this.mPhoneList != null) {
                for (ContactData.PhoneData phoneData : this.mPhoneList) {
                    int length = phoneData.data.length();
                    if (length > 8) {
                        tmpStr = phoneData.data.substring(length - 8);
                    } else {
                        tmpStr = phoneData.data;
                    }
                    phoneFileMap.put(tmpStr, phoneData);
                }
            }
            Map<String, ContactData.PhoneData> phoneSysCutMap = new HashMap<>();
            for (Map.Entry me13 : ContactData.mRawIDContactMap.entrySet()) {
                String rawId3 = (String) me13.getKey();
                if (vCardContact.queryContactPhone(ContactData.mIDPhoneCutMap, rawId3, phoneSysCutMap, "contact_id=? AND mimetype='vnd.android.cursor.item/phone_v2'", new String[]{((ContactData.IdData) me13.getValue()).mContactId}) > 0) {
                    if (phoneFileMap.size() > 0) {
                        setElementsMark(phoneFileMap.size(), phoneSysCutMap.size(), rawId3);
                        switch (mRawIdMarkElements.get(rawId3).intValue()) {
                            case 1:
                                minPhoneMap = phoneFileMap;
                                maxPhoneMap = phoneSysCutMap;
                                break;
                            case 2:
                                minPhoneMap = phoneSysCutMap;
                                maxPhoneMap = phoneFileMap;
                                break;
                            default:
                                minPhoneMap = phoneFileMap;
                                maxPhoneMap = phoneSysCutMap;
                                break;
                        }
                        vCardContact.checkSamePhone(rawId3, mIdKeyRemoveList, minPhoneMap, maxPhoneMap);
                    } else {
                        setElementsMarkMin(rawId3);
                    }
                } else if (phoneFileMap.size() > 0) {
                    setElementsMarkEx(2, rawId3);
                }
            }
        }
        checkIdKeyRemoveList();
        long currTime3 = System.currentTimeMillis();
        mIdKeyRemoveList.clear();
        long prevTime4 = System.currentTimeMillis();
        if (mRunLoop && ContactData.mRawIDContactMap.size() > 0) {
            Map<String, ContactData.PostalData> postalFileMap = new HashMap<>();
            if (this.mPostalList != null) {
                for (ContactData.PostalData postalData : this.mPostalList) {
                    ContactData.PostalData pd = new ContactData.PostalData(-1, postalData.type, postalData.label, "", postalData.street, postalData.pobox, postalData.neighborhood, postalData.localty, postalData.region, postalData.postalCode, postalData.country);
                    postalFileMap.put(pd.mTypeLabelContent, pd);
                }
            }
            for (Map.Entry me14 : ContactData.mRawIDContactMap.entrySet()) {
                String rawId4 = (String) me14.getKey();
                if (vCardContact.queryContactStructuredPostal(ContactData.mIDPostalMap, rawId4, "contact_id=? AND mimetype='vnd.android.cursor.item/postal-address_v2'", new String[]{((ContactData.IdData) me14.getValue()).mContactId}) > 0) {
                    Map<String, ContactData.PostalData> postalSysMap = ContactData.mIDPostalMap.get(rawId4);
                    if (postalFileMap.size() > 0) {
                        setElementsMark(postalFileMap.size(), postalSysMap.size(), rawId4);
                        switch (mRawIdMarkElements.get(rawId4).intValue()) {
                            case 1:
                                minPostalMap = postalFileMap;
                                maxPostalMap = postalSysMap;
                                break;
                            case 2:
                                minPostalMap = postalSysMap;
                                maxPostalMap = postalFileMap;
                                break;
                            default:
                                minPostalMap = postalFileMap;
                                maxPostalMap = postalSysMap;
                                break;
                        }
                        vCardContact.checkSameStructuredPostal(rawId4, mIdKeyRemoveList, minPostalMap, maxPostalMap);
                    } else {
                        setElementsMarkMin(rawId4);
                    }
                } else if (postalFileMap.size() > 0) {
                    setElementsMarkEx(2, rawId4);
                }
            }
        }
        checkIdKeyRemoveList();
        long currTime4 = System.currentTimeMillis();
        mIdKeyRemoveList.clear();
        long prevTime5 = System.currentTimeMillis();
        if (mRunLoop && ContactData.mRawIDContactMap.size() > 0) {
            Map<String, ContactData.EmailData> emailFileMap = new HashMap<>();
            if (this.mEmailList != null) {
                for (ContactData.EmailData emailData : this.mEmailList) {
                    ContactData.EmailData emailData2 = new ContactData.EmailData(-1, emailData.type, emailData.label, emailData.data);
                    emailFileMap.put(emailData2.mTypeLabelEmail, emailData2);
                }
            }
            for (Map.Entry me15 : ContactData.mRawIDContactMap.entrySet()) {
                String rawId5 = (String) me15.getKey();
                if (vCardContact.queryContactEmail(ContactData.mIDEmailMap, rawId5, "contact_id=? AND mimetype='vnd.android.cursor.item/email_v2'", new String[]{((ContactData.IdData) me15.getValue()).mContactId}) > 0) {
                    Map<String, ContactData.EmailData> emailSysMap = ContactData.mIDEmailMap.get(rawId5);
                    if (emailFileMap.size() > 0) {
                        setElementsMark(emailFileMap.size(), emailSysMap.size(), rawId5);
                        switch (mRawIdMarkElements.get(rawId5).intValue()) {
                            case 1:
                                minEmailMap = emailFileMap;
                                maxEmailMap = emailSysMap;
                                break;
                            case 2:
                                minEmailMap = emailSysMap;
                                maxEmailMap = emailFileMap;
                                break;
                            default:
                                minEmailMap = emailFileMap;
                                maxEmailMap = emailSysMap;
                                break;
                        }
                        vCardContact.checkSameEmail(rawId5, mIdKeyRemoveList, minEmailMap, maxEmailMap);
                    } else {
                        setElementsMarkMin(rawId5);
                    }
                } else if (emailFileMap.size() > 0) {
                    setElementsMarkEx(2, rawId5);
                }
            }
        }
        checkIdKeyRemoveList();
        long currTime5 = System.currentTimeMillis();
        mIdKeyRemoveList.clear();
        long prevTime6 = System.currentTimeMillis();
        if (mRunLoop && ContactData.mRawIDContactMap.size() > 0) {
            Map<String, ContactData.ImData> imFileMap = new HashMap<>();
            if (this.mImList != null) {
                for (ContactData.ImData imData : this.mImList) {
                    imFileMap.put(imData.mTypeLabelContent, imData);
                }
            }
            for (Map.Entry me16 : ContactData.mRawIDContactMap.entrySet()) {
                String rawId6 = (String) me16.getKey();
                if (vCardContact.queryContactIm(ContactData.mIDImMap, rawId6, "contact_id=? AND mimetype='vnd.android.cursor.item/im'", new String[]{((ContactData.IdData) me16.getValue()).mContactId}) > 0) {
                    Map<String, ContactData.ImData> imSysMap = ContactData.mIDImMap.get(rawId6);
                    if (imFileMap.size() > 0) {
                        setElementsMark(imFileMap.size(), imSysMap.size(), rawId6);
                        switch (mRawIdMarkElements.get(rawId6).intValue()) {
                            case 1:
                                minImMap = imFileMap;
                                maxImMap = imSysMap;
                                break;
                            case 2:
                                minImMap = imSysMap;
                                maxImMap = imFileMap;
                                break;
                            default:
                                minImMap = imFileMap;
                                maxImMap = imSysMap;
                                break;
                        }
                        vCardContact.checkSameIm(rawId6, mIdKeyRemoveList, minImMap, maxImMap);
                    } else {
                        setElementsMarkMin(rawId6);
                    }
                } else if (imFileMap.size() > 0) {
                    setElementsMarkEx(2, rawId6);
                }
            }
        }
        checkIdKeyRemoveList();
        long currTime6 = System.currentTimeMillis();
        mIdKeyRemoveList.clear();
        long prevTime7 = System.currentTimeMillis();
        if (mRunLoop && ContactData.mRawIDContactMap.size() > 0) {
            Map<String, ContactData.WebsiteData> webFileMap = new HashMap<>();
            if (this.mWebsiteList != null) {
                for (String web : this.mWebsiteList) {
                    ContactData.WebsiteData websiteData = new ContactData.WebsiteData(-1, 7, "", web);
                    webFileMap.put(websiteData.mTypeLabelContent, websiteData);
                }
            }
            for (Map.Entry me17 : ContactData.mRawIDContactMap.entrySet()) {
                String rawId7 = (String) me17.getKey();
                if (vCardContact.queryContactWebsite(ContactData.mIDWebsiteMap, rawId7, "contact_id=? AND mimetype='vnd.android.cursor.item/website'", new String[]{((ContactData.IdData) me17.getValue()).mContactId}) > 0) {
                    Map<String, ContactData.WebsiteData> webSysMap = ContactData.mIDWebsiteMap.get(rawId7);
                    if (webFileMap.size() > 0) {
                        setElementsMark(webFileMap.size(), webSysMap.size(), rawId7);
                        switch (mRawIdMarkElements.get(rawId7).intValue()) {
                            case 1:
                                minWebMap = webFileMap;
                                maxWebMap = webSysMap;
                                break;
                            case 2:
                                minWebMap = webSysMap;
                                maxWebMap = webFileMap;
                                break;
                            default:
                                minWebMap = webFileMap;
                                maxWebMap = webSysMap;
                                break;
                        }
                        vCardContact.checkSameWebsite(rawId7, mIdKeyRemoveList, minWebMap, maxWebMap);
                    } else {
                        setElementsMarkMin(rawId7);
                    }
                } else if (webFileMap.size() > 0) {
                    setElementsMarkEx(2, rawId7);
                }
            }
        }
        checkIdKeyRemoveList();
        long currTime7 = System.currentTimeMillis();
        mIdKeyRemoveList.clear();
        long prevTime8 = System.currentTimeMillis();
        if (mRunLoop && ContactData.mRawIDContactMap.size() > 0) {
            Map<String, ContactData.OrganizationData> organFileMap = new HashMap<>();
            if (this.mOrganizationList != null) {
                for (ContactData.OrganizationData organData : this.mOrganizationList) {
                    ContactData.OrganizationData organ = new ContactData.OrganizationData(-1, organData.type, organData.label, organData.companyName, organData.positionName, "", "", "", "", "");
                    organFileMap.put(organ.mTypeLabelContent, organ);
                }
            }
            for (Map.Entry me18 : ContactData.mRawIDContactMap.entrySet()) {
                String rawId8 = (String) me18.getKey();
                if (vCardContact.queryContactOrganization(ContactData.mIDOrganizationMap, rawId8, "contact_id=? AND mimetype='vnd.android.cursor.item/organization'", new String[]{((ContactData.IdData) me18.getValue()).mContactId}) > 0) {
                    Map<String, ContactData.OrganizationData> organSysMap = ContactData.mIDOrganizationMap.get(rawId8);
                    if (organFileMap.size() > 0) {
                        setElementsMark(organFileMap.size(), organSysMap.size(), rawId8);
                        switch (mRawIdMarkElements.get(rawId8).intValue()) {
                            case 1:
                                minOrganizationMap = organFileMap;
                                maxOrganizationMap = organSysMap;
                                break;
                            case 2:
                                minOrganizationMap = organSysMap;
                                maxOrganizationMap = organFileMap;
                                break;
                            default:
                                minOrganizationMap = organFileMap;
                                maxOrganizationMap = organSysMap;
                                break;
                        }
                        vCardContact.checkSameOrganization(rawId8, mIdKeyRemoveList, minOrganizationMap, maxOrganizationMap);
                    } else {
                        setElementsMarkMin(rawId8);
                    }
                } else if (organFileMap.size() > 0) {
                    setElementsMarkEx(2, rawId8);
                }
            }
        }
        checkIdKeyRemoveList();
        long currTime8 = System.currentTimeMillis();
        mIdKeyRemoveList.clear();
        long prevTime9 = System.currentTimeMillis();
        if (mRunLoop && ContactData.mRawIDContactMap.size() > 0) {
            Map<String, ContactData.NoteData> noteFileMap = new HashMap<>();
            if (this.mNoteList != null) {
                for (String noteData : this.mNoteList) {
                    ContactData.NoteData noteData2 = new ContactData.NoteData(-1, noteData, "");
                    noteFileMap.put(noteData2.mTypeContent, noteData2);
                }
            }
            for (Map.Entry me19 : ContactData.mRawIDContactMap.entrySet()) {
                String rawId9 = (String) me19.getKey();
                if (vCardContact.queryContactNote(ContactData.mIDNoteMap, rawId9, "contact_id=? AND mimetype='vnd.android.cursor.item/note'", new String[]{((ContactData.IdData) me19.getValue()).mContactId}) > 0) {
                    Map<String, ContactData.NoteData> noteSysMap = ContactData.mIDNoteMap.get(rawId9);
                    if (noteFileMap.size() > 0) {
                        setElementsMark(noteFileMap.size(), noteSysMap.size(), rawId9);
                        switch (mRawIdMarkElements.get(rawId9).intValue()) {
                            case 1:
                                minNoteMap = noteFileMap;
                                maxNoteMap = noteSysMap;
                                break;
                            case 2:
                                minNoteMap = noteSysMap;
                                maxNoteMap = noteFileMap;
                                break;
                            default:
                                minNoteMap = noteFileMap;
                                maxNoteMap = noteSysMap;
                                break;
                        }
                        vCardContact.checkSameNote(rawId9, mIdKeyRemoveList, minNoteMap, maxNoteMap);
                    } else {
                        setElementsMarkMin(rawId9);
                    }
                } else if (noteFileMap.size() > 0) {
                    setElementsMarkEx(2, rawId9);
                }
            }
        }
        checkIdKeyRemoveList();
        long currTime9 = System.currentTimeMillis();
        if (ContactData.mRawIDContactMap.size() > 0) {
            return (ContactData.IdData) ContactData.mRawIDContactMap.entrySet().iterator().next().getValue();
        }
        return null;
    }

    public void pushIntoContentResolver(ContentResolver resolver) {
        ContentProviderOperation.Builder builder;
        ContentProviderOperation.Builder builder2;
        String tmpStr;
        Integer value;
        boolean isNewContactItem = false;
        long currentTimeMillis = System.currentTimeMillis();
        ArrayList<ContentProviderOperation> operationList = new ArrayList<>();
        mIdKeyRemoveList.clear();
        ContactData.mRawIDContactMap.clear();
        ContactData.mIDStructureNameMap.clear();
        ContactData.mIDNickNameMap.clear();
        ContactData.mIDPhoneMap.clear();
        ContactData.mIDPostalMap.clear();
        ContactData.mIDEmailMap.clear();
        ContactData.mIDImMap.clear();
        ContactData.mIDEventMap.clear();
        ContactData.mIDWebsiteMap.clear();
        ContactData.mIDNoteMap.clear();
        mCurrentDisplayName = getDisplayName();
        ContactData.IdData idData = getSameContact(resolver);
        if (mRunLoop) {
            long currentTimeMillis2 = System.currentTimeMillis();
            if (idData == null) {
                pushIntoContentResolverForInsert(resolver);
                return;
            }
            long currTime = System.currentTimeMillis();
            int markElements = 0;
            if (mRawIdMarkElements.size() > 0 && (value = mRawIdMarkElements.get(idData.mRawId)) != null) {
                markElements = value.intValue();
            }
            if (markElements == 1 || markElements == 3) {
                mCompletedItems++;
                return;
            }
            new String[1][0] = String.valueOf(idData.mRawId);
            VCardContact vCardContact = new VCardContact(resolver);
            long currentTimeMillis3 = System.currentTimeMillis();
            Map<String, ContactData.StructureNameData> nameMap = ContactData.mIDStructureNameMap.get(idData.mRawId);
            if (nameMap != null) {
                ContactData.StructureNameData nameData = (ContactData.StructureNameData) nameMap.get(idData.mContactId);
                if (nameData != null) {
                    ContentProviderOperation.Builder builder3 = ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI);
                    builder3.withSelection("_id=?", new String[]{String.valueOf(nameData.id)});
                    builder3.withValue("mimetype", "vnd.android.cursor.item/name");
                    if (!TextUtils.isEmpty(this.mGivenName) && !this.mGivenName.equalsIgnoreCase(nameData.mGivenName)) {
                        builder3.withValue("data2", this.mGivenName);
                        isNewContactItem = true;
                    }
                    if (!TextUtils.isEmpty(this.mFamilyName) && !this.mFamilyName.equalsIgnoreCase(nameData.mFamilyName)) {
                        builder3.withValue("data3", this.mFamilyName);
                        isNewContactItem = true;
                    }
                    if (!TextUtils.isEmpty(this.mMiddleName) && !this.mMiddleName.equalsIgnoreCase(nameData.mMiddleName)) {
                        builder3.withValue("data5", this.mMiddleName);
                        isNewContactItem = true;
                    }
                    if (!TextUtils.isEmpty(this.mPrefix) && !this.mPrefix.equalsIgnoreCase(nameData.mPrefix)) {
                        builder3.withValue("data4", this.mPrefix);
                        isNewContactItem = true;
                    }
                    if (!TextUtils.isEmpty(this.mSuffix) && !this.mSuffix.equalsIgnoreCase(nameData.mSuffix)) {
                        builder3.withValue("data6", this.mSuffix);
                        isNewContactItem = true;
                    }
                    operationList.add(builder3.build());
                }
            }
            long currTime2 = System.currentTimeMillis();
            long prevTime = System.currentTimeMillis();
            Map<String, ContactData.NickNameData> nickSysMap = ContactData.mIDNickNameMap.get(idData.mRawId);
            if (this.mNickNameList != null && this.mNickNameList.size() > 0) {
                for (String nickName : this.mNickNameList) {
                    ContactData.NickNameData nickNameData = new ContactData.NickNameData(-1, 1, "", nickName);
                    boolean isNewData = true;
                    if (nickSysMap != null && nickSysMap.size() > 0) {
                        if (nickSysMap.containsKey(nickNameData.mTypeLabelNick)) {
                            isNewData = false;
                        }
                    }
                    if (isNewData) {
                        ContentProviderOperation.Builder builder4 = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
                        builder4.withValue("raw_contact_id", idData.mRawId);
                        builder4.withValue("mimetype", "vnd.android.cursor.item/nickname");
                        builder4.withValue("data2", Integer.valueOf(nickNameData.type));
                        builder4.withValue("data3", nickNameData.label);
                        builder4.withValue(ContactsHandler.PHONE_NUMBER, nickNameData.data);
                        operationList.add(builder4.build());
                        isNewContactItem = true;
                        long currTime3 = System.currentTimeMillis();
                    }
                }
            }
            long prevTime2 = System.currentTimeMillis();
            Map<String, ContactData.PhoneData> phoneSysCutMap = ContactData.mIDPhoneCutMap.get(idData.mRawId);
            if (this.mPhoneList != null && this.mPhoneList.size() > 0) {
                for (ContactData.PhoneData phoneData : this.mPhoneList) {
                    boolean isNewData2 = true;
                    if (phoneSysCutMap != null && phoneSysCutMap.size() > 0) {
                        int length = phoneData.data.length();
                        if (length > 8) {
                            tmpStr = phoneData.data.substring(length - 8);
                        } else {
                            tmpStr = phoneData.data;
                        }
                        if (phoneSysCutMap.containsKey(tmpStr)) {
                            isNewData2 = false;
                        }
                    }
                    if (isNewData2) {
                        ContentProviderOperation.Builder builder5 = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
                        builder5.withValue("raw_contact_id", idData.mRawId);
                        builder5.withValue("mimetype", "vnd.android.cursor.item/phone_v2");
                        builder5.withValue("data2", Integer.valueOf(phoneData.type));
                        builder5.withValue("data3", phoneData.label);
                        builder5.withValue(ContactsHandler.PHONE_NUMBER, phoneData.data);
                        operationList.add(builder5.build());
                        isNewContactItem = true;
                        long currTime4 = System.currentTimeMillis();
                    }
                }
            }
            long prevTime3 = System.currentTimeMillis();
            Map<String, ContactData.PostalData> postalSysMap = ContactData.mIDPostalMap.get(idData.mRawId);
            if (this.mPostalList != null && this.mPostalList.size() > 0) {
                for (ContactData.PostalData postalData : this.mPostalList) {
                    ContactData.PostalData pd = new ContactData.PostalData(-1, postalData.type, postalData.label, "", postalData.street, postalData.pobox, postalData.neighborhood, postalData.localty, postalData.region, postalData.postalCode, postalData.country);
                    boolean isNewData3 = true;
                    if (postalSysMap != null && postalSysMap.size() > 0 && postalSysMap.containsKey(pd.mTypeLabelContent)) {
                        isNewData3 = false;
                    }
                    if (isNewData3) {
                        ContentProviderOperation.Builder builder6 = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
                        builder6.withValue("raw_contact_id", idData.mRawId);
                        builder6.withValue("mimetype", "vnd.android.cursor.item/postal-address_v2");
                        builder6.withValue("data2", Integer.valueOf(pd.type));
                        builder6.withValue("data3", pd.label);
                        builder6.withValue("data4", pd.street);
                        builder6.withValue("data5", pd.pobox);
                        builder6.withValue("data6", pd.neighborhood);
                        builder6.withValue("data7", pd.localty);
                        builder6.withValue("data8", pd.region);
                        builder6.withValue("data9", pd.postalCode);
                        builder6.withValue("data10", pd.country);
                        builder6.withValue(ContactsHandler.PHONE_NUMBER, pd.formatAddress);
                        operationList.add(builder6.build());
                        isNewContactItem = true;
                        long currTime5 = System.currentTimeMillis();
                    }
                }
            }
            long prevTime4 = System.currentTimeMillis();
            Map<String, ContactData.EmailData> emailSysMap = ContactData.mIDEmailMap.get(idData.mRawId);
            if (this.mEmailList != null && this.mEmailList.size() > 0) {
                for (ContactData.EmailData emailData : this.mEmailList) {
                    ContactData.EmailData emailData2 = new ContactData.EmailData(-1, emailData.type, emailData.label, emailData.data);
                    boolean isNewData4 = true;
                    if (emailSysMap != null && emailSysMap.size() > 0) {
                        if (emailSysMap.containsKey(emailData2.mTypeLabelEmail)) {
                            isNewData4 = false;
                        }
                    }
                    if (isNewData4) {
                        ContentProviderOperation.Builder builder7 = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
                        builder7.withValue("raw_contact_id", idData.mRawId);
                        builder7.withValue("mimetype", "vnd.android.cursor.item/email_v2");
                        builder7.withValue("data2", Integer.valueOf(emailData2.type));
                        builder7.withValue("data3", emailData2.label);
                        builder7.withValue(ContactsHandler.PHONE_NUMBER, emailData2.data);
                        operationList.add(builder7.build());
                        isNewContactItem = true;
                        long currTime6 = System.currentTimeMillis();
                    }
                }
            }
            long prevTime5 = System.currentTimeMillis();
            Map<String, ContactData.ImData> imSysMap = ContactData.mIDImMap.get(idData.mRawId);
            if (this.mImList != null && this.mImList.size() > 0) {
                for (ContactData.ImData imData : this.mImList) {
                    ContactData.ImData im = new ContactData.ImData(-1, imData.type, imData.label, imData.data, 0, "");
                    boolean isNewData5 = true;
                    if (imSysMap != null && imSysMap.size() > 0 && imSysMap.containsKey(im.mTypeLabelContent)) {
                        isNewData5 = false;
                    }
                    if (isNewData5) {
                        ContentProviderOperation.Builder builder8 = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
                        builder8.withValue("raw_contact_id", idData.mRawId);
                        builder8.withValue("mimetype", "vnd.android.cursor.item/im");
                        builder8.withValue("data2", Integer.valueOf(im.type));
                        builder8.withValue("data3", im.label);
                        builder8.withValue(ContactsHandler.PHONE_NUMBER, im.data);
                        builder8.withValue("data5", Integer.valueOf(im.mProtocolName));
                        builder8.withValue("data6", im.mCustomProtocol);
                        operationList.add(builder8.build());
                        isNewContactItem = true;
                        long currTime7 = System.currentTimeMillis();
                    }
                }
            }
            long prevTime6 = System.currentTimeMillis();
            Map<String, ContactData.WebsiteData> webSysMap = ContactData.mIDWebsiteMap.get(idData.mRawId);
            if (this.mWebsiteList != null && this.mWebsiteList.size() > 0) {
                for (String web : this.mWebsiteList) {
                    ContactData.WebsiteData websiteData = new ContactData.WebsiteData(-1, 7, "", web);
                    boolean isNewData6 = true;
                    if (webSysMap != null && webSysMap.size() > 0) {
                        if (webSysMap.containsKey(websiteData.mTypeLabelContent)) {
                            isNewData6 = false;
                        }
                    }
                    if (isNewData6) {
                        ContentProviderOperation.Builder builder9 = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
                        builder9.withValue("raw_contact_id", idData.mRawId);
                        builder9.withValue("mimetype", "vnd.android.cursor.item/website");
                        builder9.withValue("data2", Integer.valueOf(websiteData.type));
                        builder9.withValue("data3", websiteData.label);
                        builder9.withValue(ContactsHandler.PHONE_NUMBER, websiteData.data);
                        operationList.add(builder9.build());
                        isNewContactItem = true;
                        long currTime8 = System.currentTimeMillis();
                    }
                }
            }
            long prevTime7 = System.currentTimeMillis();
            Map<String, ContactData.OrganizationData> organSysMap = ContactData.mIDOrganizationMap.get(idData.mRawId);
            if (this.mOrganizationList != null && this.mOrganizationList.size() > 0) {
                for (ContactData.OrganizationData organData : this.mOrganizationList) {
                    ContactData.OrganizationData organ = new ContactData.OrganizationData(-1, organData.type, organData.label, organData.companyName, organData.positionName, "", "", "", "", "");
                    boolean isNewData7 = true;
                    if (organSysMap != null && organSysMap.size() > 0 && organSysMap.containsKey(organ.mTypeLabelContent)) {
                        isNewData7 = false;
                    }
                    if (isNewData7) {
                        ContentProviderOperation.Builder builder10 = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
                        builder10.withValue("raw_contact_id", idData.mRawId);
                        builder10.withValue("mimetype", "vnd.android.cursor.item/organization");
                        builder10.withValue("data2", Integer.valueOf(organ.type));
                        builder10.withValue("data3", organ.label);
                        builder10.withValue(ContactsHandler.PHONE_NUMBER, organ.companyName);
                        builder10.withValue("data4", organ.positionName);
                        builder10.withValue("data5", organ.mDepartment);
                        builder10.withValue("data6", organ.mJobDescrition);
                        builder10.withValue("data7", organ.mSymbol);
                        builder10.withValue("data8", organ.mPhoneticName);
                        builder10.withValue("data9", organ.mOfficeLocation);
                        operationList.add(builder10.build());
                        isNewContactItem = true;
                        long currTime9 = System.currentTimeMillis();
                    }
                }
            }
            long prevTime8 = System.currentTimeMillis();
            Map<String, ContactData.NoteData> noteSysMap = ContactData.mIDNoteMap.get(idData.mRawId);
            if (this.mNoteList != null && this.mNoteList.size() > 0) {
                for (String noteData : this.mNoteList) {
                    ContactData.NoteData noteData2 = new ContactData.NoteData(-1, noteData, "");
                    boolean isNewData8 = true;
                    if (noteSysMap != null && noteSysMap.size() > 0) {
                        if (noteSysMap.containsKey(noteData2.mNoteContent)) {
                            isNewData8 = false;
                        }
                    }
                    if (isNewData8) {
                        ContentProviderOperation.Builder builder11 = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
                        builder11.withValue("raw_contact_id", idData.mRawId);
                        builder11.withValue("mimetype", "vnd.android.cursor.item/note");
                        builder11.withValue(ContactsHandler.PHONE_NUMBER, noteData2.mNoteContent);
                        operationList.add(builder11.build());
                        isNewContactItem = true;
                        long currTime10 = System.currentTimeMillis();
                    }
                }
            }
            long prevTime9 = System.currentTimeMillis();
            if (isNewContactItem && !TextUtils.isEmpty(this.mBirthday)) {
                HashMap hashMap = new HashMap();
                if (vCardContact.queryContactEvent(hashMap, "contact_id=? AND mimetype='vnd.android.cursor.item/contact_event'", new String[]{idData.mContactId}) <= 0 || hashMap.size() <= 0) {
                    builder2 = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
                    builder2.withValue("raw_contact_id", idData.mRawId);
                    builder2.withValue("data2", 3);
                } else {
                    builder2 = ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI);
                    ContactData.EventData eventData = (ContactData.EventData) ((Map.Entry) hashMap.entrySet().iterator().next()).getValue();
                    builder2.withSelection("_id=?", new String[]{String.valueOf(eventData.id)});
                    builder2.withValue("data2", Integer.valueOf(eventData.type));
                    builder2.withValue("data3", eventData.label);
                }
                builder2.withValue("mimetype", "vnd.android.cursor.item/contact_event");
                builder2.withValue(ContactsHandler.PHONE_NUMBER, this.mBirthday);
                operationList.add(builder2.build());
                long currTime11 = System.currentTimeMillis();
            }
            long prevTime10 = System.currentTimeMillis();
            if (isNewContactItem && this.mPhotoList != null && this.mPhotoList.size() > 0) {
                for (ContactData.PhotoData photoData : this.mPhotoList) {
                    int photoId = vCardContact.queryContactPhotoId("contact_id=? AND mimetype='vnd.android.cursor.item/photo'", new String[]{idData.mContactId});
                    if (photoId >= 0) {
                        builder = ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI);
                        builder.withSelection("_id=?", new String[]{String.valueOf(photoId)});
                    } else {
                        CommonMethod.logDebug1(LOG_TAG_3, "pushIntoContentResolver: Update(new Photo)");
                        builder = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
                        builder.withValue("raw_contact_id", idData.mRawId);
                    }
                    builder.withValue("mimetype", "vnd.android.cursor.item/photo");
                    builder.withValue("data15", photoData.photoBytes);
                    operationList.add(builder.build());
                    long currTime12 = System.currentTimeMillis();
                }
            }
            long prevTime11 = System.currentTimeMillis();
            if (isNewContactItem) {
                try {
                    resolver.applyBatch("com.android.contacts", operationList);
                } catch (OperationApplicationException | RemoteException e) {
                }
            }
            mCompletedItems++;
            long currTime13 = System.currentTimeMillis();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void pushIntoContentResolverForInsert(ContentResolver resolver) {
        long currentTimeMillis = System.currentTimeMillis();
        ArrayList<ContentProviderOperation> operationList = new ArrayList<>();
        ContentValues contentValues = new ContentValues();
        long currentTimeMillis2 = System.currentTimeMillis();
        String myGroupsId = null;
        if (this.mAccount != null) {
            contentValues.put("account_name", this.mAccount.name);
            contentValues.put("account_type", this.mAccount.type);
            if (ACCOUNT_TYPE_GOOGLE.equals(this.mAccount.type)) {
                Cursor cursor = resolver.query(ContactsContract.Groups.CONTENT_URI, new String[]{"sourceid"}, "title=?", new String[]{GOOGLE_MY_CONTACTS_GROUP}, null);
                if (cursor != null) {
                    try {
                        if (cursor.moveToFirst()) {
                            myGroupsId = cursor.getString(0);
                        }
                    } catch (Throwable th) {
                        if (cursor != null) {
                            cursor.close();
                        }
                        throw th;
                    }
                }
                if (cursor != null) {
                    cursor.close();
                }
            }
        } else {
            contentValues.putNull("account_name");
            contentValues.putNull("account_type");
        }
        contentValues.put("aggregation_mode", (Integer) 3);
        long rawContactId = ContentUris.parseId(resolver.insert(ContactsContract.RawContacts.CONTENT_URI, contentValues));
        long currentTimeMillis3 = System.currentTimeMillis();
        long prevTime = System.currentTimeMillis();
        ContentProviderOperation.Builder builder = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
        builder.withValue("raw_contact_id", Long.valueOf(rawContactId));
        builder.withValue("mimetype", "vnd.android.cursor.item/name");
        builder.withValue("data2", this.mGivenName);
        builder.withValue("data3", this.mFamilyName);
        builder.withValue("data5", this.mMiddleName);
        builder.withValue("data4", this.mPrefix);
        builder.withValue("data6", this.mSuffix);
        builder.withValue("data7", this.mPhoneticGivenName);
        builder.withValue("data9", this.mPhoneticFamilyName);
        builder.withValue("data8", this.mPhoneticMiddleName);
        builder.withValue(ContactsHandler.PHONE_NUMBER, getDisplayName());
        operationList.add(builder.build());
        long currTime = System.currentTimeMillis();
        long prevTime2 = System.currentTimeMillis();
        if (this.mNickNameList != null && this.mNickNameList.size() > 0) {
            boolean first = true;
            for (String nickName : this.mNickNameList) {
                ContentProviderOperation.Builder builder2 = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
                builder2.withValue("raw_contact_id", Long.valueOf(rawContactId));
                builder2.withValue("mimetype", "vnd.android.cursor.item/nickname");
                builder2.withValue("data2", 1);
                builder2.withValue(ContactsHandler.PHONE_NUMBER, nickName);
                if (first) {
                    builder2.withValue("is_primary", 1);
                    first = false;
                }
                operationList.add(builder2.build());
            }
            long currTime2 = System.currentTimeMillis();
        }
        long prevTime3 = System.currentTimeMillis();
        if (this.mPhoneList != null) {
            boolean first2 = true;
            for (ContactData.PhoneData phoneData : this.mPhoneList) {
                ContentProviderOperation.Builder builder3 = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
                builder3.withValue("raw_contact_id", Long.valueOf(rawContactId));
                builder3.withValue("mimetype", "vnd.android.cursor.item/phone_v2");
                builder3.withValue("data2", Integer.valueOf(phoneData.type));
                if (phoneData.type == 0) {
                    builder3.withValue("data3", phoneData.label);
                }
                builder3.withValue(ContactsHandler.PHONE_NUMBER, phoneData.data);
                if (phoneData.isPrimary && first2) {
                    builder3.withValue("is_primary", 1);
                    first2 = false;
                }
                operationList.add(builder3.build());
            }
            long currTime3 = System.currentTimeMillis();
        }
        long prevTime4 = System.currentTimeMillis();
        if (this.mOrganizationList != null) {
            boolean first3 = true;
            for (ContactData.OrganizationData organizationData : this.mOrganizationList) {
                ContentProviderOperation.Builder builder4 = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
                builder4.withValue("raw_contact_id", Long.valueOf(rawContactId));
                builder4.withValue("mimetype", "vnd.android.cursor.item/organization");
                builder4.withValue("data2", Integer.valueOf(organizationData.type));
                builder4.withValue(ContactsHandler.PHONE_NUMBER, organizationData.companyName);
                builder4.withValue("data4", organizationData.positionName);
                if (first3) {
                    builder4.withValue("is_primary", 1);
                    first3 = false;
                }
                operationList.add(builder4.build());
            }
            long currTime4 = System.currentTimeMillis();
        }
        long prevTime5 = System.currentTimeMillis();
        if (this.mEmailList != null) {
            boolean first4 = true;
            for (ContactData.EmailData emailData : this.mEmailList) {
                ContentProviderOperation.Builder builder5 = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
                builder5.withValue("raw_contact_id", Long.valueOf(rawContactId));
                builder5.withValue("mimetype", "vnd.android.cursor.item/email_v2");
                builder5.withValue("data2", Integer.valueOf(emailData.type));
                if (emailData.type == 0) {
                    builder5.withValue("data3", emailData.label);
                }
                builder5.withValue(ContactsHandler.PHONE_NUMBER, emailData.data);
                if (emailData.isPrimary && first4) {
                    builder5.withValue("is_primary", 1);
                    first4 = false;
                }
                operationList.add(builder5.build());
            }
            long currTime5 = System.currentTimeMillis();
        }
        long prevTime6 = System.currentTimeMillis();
        if (this.mPostalList != null) {
            boolean first5 = true;
            for (ContactData.PostalData postalData : this.mPostalList) {
                ContentProviderOperation.Builder builder6 = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
                VCardUtils.insertStructuredPostalDataUsingContactsStruct(this.mVCardType, builder6, postalData, rawContactId, first5);
                operationList.add(builder6.build());
                if (postalData.isPrimary && first5) {
                    first5 = false;
                }
            }
            long currTime6 = System.currentTimeMillis();
        }
        long prevTime7 = System.currentTimeMillis();
        if (this.mImList != null) {
            boolean first6 = true;
            for (ContactData.ImData imData : this.mImList) {
                ContentProviderOperation.Builder builder7 = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
                builder7.withValue("raw_contact_id", Long.valueOf(rawContactId));
                builder7.withValue("mimetype", "vnd.android.cursor.item/im");
                builder7.withValue("data2", 3);
                builder7.withValue("data5", Integer.valueOf(imData.mProtocolName));
                if (imData.mProtocolName == -1) {
                    builder7.withValue("data6", imData.mCustomProtocol);
                }
                builder7.withValue(ContactsHandler.PHONE_NUMBER, imData.data);
                if (imData.isPrimary && first6) {
                    builder7.withValue("is_primary", 1);
                    first6 = false;
                }
                operationList.add(builder7.build());
            }
            long currTime7 = System.currentTimeMillis();
        }
        long prevTime8 = System.currentTimeMillis();
        if (this.mNoteList != null) {
            for (String note : this.mNoteList) {
                ContentProviderOperation.Builder builder8 = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
                builder8.withValue("raw_contact_id", Long.valueOf(rawContactId));
                builder8.withValue("mimetype", "vnd.android.cursor.item/note");
                builder8.withValue(ContactsHandler.PHONE_NUMBER, note);
                operationList.add(builder8.build());
            }
            long currTime8 = System.currentTimeMillis();
        }
        long prevTime9 = System.currentTimeMillis();
        if (this.mPhotoList != null) {
            boolean first7 = true;
            for (ContactData.PhotoData photoData : this.mPhotoList) {
                ContentProviderOperation.Builder builder9 = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
                builder9.withValue("raw_contact_id", Long.valueOf(rawContactId));
                builder9.withValue("mimetype", "vnd.android.cursor.item/photo");
                builder9.withValue("data15", photoData.photoBytes);
                if (first7) {
                    builder9.withValue("is_primary", 1);
                    first7 = false;
                }
                operationList.add(builder9.build());
            }
            long currTime9 = System.currentTimeMillis();
        }
        long prevTime10 = System.currentTimeMillis();
        if (this.mWebsiteList != null) {
            for (String website : this.mWebsiteList) {
                ContentProviderOperation.Builder builder10 = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
                builder10.withValue("raw_contact_id", Long.valueOf(rawContactId));
                builder10.withValue("mimetype", "vnd.android.cursor.item/website");
                builder10.withValue(ContactsHandler.PHONE_NUMBER, website);
                builder10.withValue("data2", 7);
                operationList.add(builder10.build());
            }
            long currTime10 = System.currentTimeMillis();
        }
        long prevTime11 = System.currentTimeMillis();
        if (!TextUtils.isEmpty(this.mBirthday)) {
            ContentProviderOperation.Builder builder11 = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
            builder11.withValue("raw_contact_id", Long.valueOf(rawContactId));
            builder11.withValue("mimetype", "vnd.android.cursor.item/contact_event");
            builder11.withValue(ContactsHandler.PHONE_NUMBER, this.mBirthday);
            builder11.withValue("data2", 3);
            operationList.add(builder11.build());
            long currTime11 = System.currentTimeMillis();
        }
        long prevTime12 = System.currentTimeMillis();
        if (myGroupsId != null) {
            ContentProviderOperation.Builder builder12 = ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI);
            builder12.withValue("raw_contact_id", Long.valueOf(rawContactId));
            builder12.withValue("mimetype", "vnd.android.cursor.item/group_membership");
            builder12.withValue("group_sourceid", myGroupsId);
            operationList.add(builder12.build());
            long currTime12 = System.currentTimeMillis();
        }
        long prevTime13 = System.currentTimeMillis();
        try {
            resolver.applyBatch("com.android.contacts", operationList);
            mCompletedItems++;
        } catch (OperationApplicationException | RemoteException e) {
        }
        long currTime13 = System.currentTimeMillis();
    }

    public boolean isIgnorable() {
        return getDisplayName().length() == 0;
    }

    /* Debug info: failed to restart local var, previous not found, register: 7 */
    private String listToString(List<String> list) {
        int size = list.size();
        if (size > 1) {
            StringBuilder builder = new StringBuilder();
            for (String type : list) {
                builder.append(type);
                if (0 < size - 1) {
                    builder.append(";");
                }
            }
            return builder.toString();
        } else if (size == 1) {
            return list.get(0);
        } else {
            return "";
        }
    }

    public static void setRunMark(boolean run) {
        mRunLoop = run;
        CommonMethod.logDebug1("netqin", "setRunMark : " + mRunLoop);
    }

    public static void initCompletedItems() {
        mCompletedItems = 0;
    }

    public static int getCompletedItems() {
        return mCompletedItems;
    }

    public static void initCurrentDisplayName() {
        mCurrentDisplayName = "";
    }

    public static String getCurrentDisplayName() {
        return mCurrentDisplayName;
    }
}
