package com.netqin.antivirus.contact.vcard;

public class VCardFile {
    private String mCanonicalPath;
    private long mLastModified;
    private String mName;

    public VCardFile(String name, String canonicalPath, long lastModified) {
        this.mName = name;
        this.mCanonicalPath = canonicalPath;
        this.mLastModified = lastModified;
    }

    public String getName() {
        return this.mName;
    }

    public String getCanonicalPath() {
        return this.mCanonicalPath;
    }

    public long getLastModified() {
        return this.mLastModified;
    }
}
