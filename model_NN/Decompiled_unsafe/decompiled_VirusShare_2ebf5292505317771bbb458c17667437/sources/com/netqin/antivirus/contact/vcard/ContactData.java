package com.netqin.antivirus.contact.vcard;

import android.net.Uri;
import android.provider.ContactsContract;
import android.text.TextUtils;
import com.netqin.antivirus.antilost.ContactsHandler;
import com.netqin.antivirus.antilost.SmsHandler;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ContactData {
    public static final Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
    public static final int DATA_CONTACT_ID = 2;
    public static final int DATA_DISPLAY_NAME = 1;
    public static final int DATA_ID = 0;
    public static final int DATA_IS_PRIMARY = 3;
    public static final int DATA_RAW_COLUMN_ID = 4;
    public static final int EMAIL_COLUMN_CONTACT_ID = 2;
    public static final int EMAIL_COLUMN_DATA = 6;
    public static final int EMAIL_COLUMN_DISPLAY_NAME = 1;
    public static final int EMAIL_COLUMN_ID = 0;
    public static final int EMAIL_COLUMN_IS_PRIMARY = 3;
    public static final int EMAIL_COLUMN_LABEL = 5;
    public static final int EMAIL_COLUMN_TYPE = 4;
    public static final int EVENT_COLUMN_CONTACT_ID = 2;
    public static final int EVENT_COLUMN_DISPLAY_NAME = 1;
    public static final int EVENT_COLUMN_ID = 0;
    public static final int EVENT_COLUMN_IS_PRIMARY = 3;
    public static final int EVENT_COLUMN_LABEL = 5;
    public static final int EVENT_COLUMN_START_DATE = 6;
    public static final int EVENT_COLUMN_TYPE = 4;
    public static final int EXPORT_COLUMN_DISPLAY_NAME = 1;
    public static final int EXPORT_COLUMN_MIMETYPE = 0;
    public static final int EXPORT_COLUMN_PHOTO = 20;
    public static final int IM_COLUMN_CONTACT_ID = 2;
    public static final int IM_COLUMN_CUSTOM_PROTOCOL = 8;
    public static final int IM_COLUMN_DATA = 6;
    public static final int IM_COLUMN_DISPLAY_NAME = 1;
    public static final int IM_COLUMN_ID = 0;
    public static final int IM_COLUMN_IS_PRIMARY = 3;
    public static final int IM_COLUMN_LABEL = 5;
    public static final int IM_COLUMN_PROTOCOL = 7;
    public static final int IM_COLUMN_TYPE = 4;
    public static final int NICKNAME_COLUMN_CONTACT_ID = 2;
    public static final int NICKNAME_COLUMN_DISPLAY_NAME = 1;
    public static final int NICKNAME_COLUMN_ID = 0;
    public static final int NICKNAME_COLUMN_IS_PRIMARY = 3;
    public static final int NICKNAME_COLUMN_LABEL = 5;
    public static final int NICKNAME_COLUMN_NAME = 6;
    public static final int NICKNAME_COLUMN_TYPE = 4;
    public static final int NOTE_COLUMN_CONTACT_ID = 2;
    public static final int NOTE_COLUMN_DISPLAY_NAME = 1;
    public static final int NOTE_COLUMN_ID = 0;
    public static final int NOTE_COLUMN_IS_PRIMARY = 3;
    public static final int NOTE_COLUMN_NOTE = 4;
    public static final String NOTE_SELECTION = "raw_contact_id=? AND mimetype='vnd.android.cursor.item/note'";
    public static final int ORGANIZATION_COLUMN_COMPANY = 6;
    public static final int ORGANIZATION_COLUMN_CONTACT_ID = 2;
    public static final int ORGANIZATION_COLUMN_DEPARTMENT = 8;
    public static final int ORGANIZATION_COLUMN_DISPLAY_NAME = 1;
    public static final int ORGANIZATION_COLUMN_ID = 0;
    public static final int ORGANIZATION_COLUMN_IS_PRIMARY = 3;
    public static final int ORGANIZATION_COLUMN_JOB_DESCRIPTION = 9;
    public static final int ORGANIZATION_COLUMN_LABEL = 5;
    public static final int ORGANIZATION_COLUMN_OFFICE_LOCATION = 12;
    public static final int ORGANIZATION_COLUMN_PHONETIC_NAME = 11;
    public static final int ORGANIZATION_COLUMN_SYMBOL = 10;
    public static final int ORGANIZATION_COLUMN_TITLE = 7;
    public static final int ORGANIZATION_COLUMN_TYPE = 4;
    public static final int PEOPLE_CONTACT_COLUMN_ID = 0;
    public static final int PEOPLE_CONTACT_COLUMN_PEOPLE = 1;
    public static final int PEOPLE_CONTACT_COLUMN_PHONE = 2;
    public static final int PHONE_COLUMN_CONTACT_ID = 2;
    public static final int PHONE_COLUMN_DISPLAY_NAME = 1;
    public static final int PHONE_COLUMN_ID = 0;
    public static final int PHONE_COLUMN_IS_PRIMARY = 3;
    public static final int PHONE_COLUMN_LABEL = 5;
    public static final int PHONE_COLUMN_NUMBER = 6;
    public static final int PHONE_COLUMN_TYPE = 4;
    public static final Uri PHONE_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
    public static final int PHOTO_COLUMN_CONTACT_ID = 2;
    public static final int PHOTO_COLUMN_DISPLAY_NAME = 1;
    public static final int PHOTO_COLUMN_ID = 0;
    public static final int PHOTO_COLUMN_IS_PRIMARY = 3;
    public static final int PHOTO_COLUMN_PHOTO = 4;
    public static final int POSTAL_COLUMN_CITY = 10;
    public static final int POSTAL_COLUMN_CONTACT_ID = 2;
    public static final int POSTAL_COLUMN_COUNTRY = 13;
    public static final int POSTAL_COLUMN_DISPLAY_NAME = 1;
    public static final int POSTAL_COLUMN_FORMATTED_ADDRESS = 6;
    public static final int POSTAL_COLUMN_ID = 0;
    public static final int POSTAL_COLUMN_IS_PRIMARY = 3;
    public static final int POSTAL_COLUMN_LABEL = 5;
    public static final int POSTAL_COLUMN_NEIGHBORHOOD = 9;
    public static final int POSTAL_COLUMN_POBOX = 8;
    public static final int POSTAL_COLUMN_POSTCODE = 12;
    public static final int POSTAL_COLUMN_REGION = 11;
    public static final int POSTAL_COLUMN_STREET = 7;
    public static final int POSTAL_COLUMN_TYPE = 4;
    public static final String[] PROJECTION_DIPLAYNAME = {SmsHandler.ROWID, ContactsHandler.NAME, ContactsHandler.PHONE_CONTACT_ID, "is_primary", "raw_contact_id"};
    public static final String[] PROJECTION_EMAIL = {SmsHandler.ROWID, "data4", ContactsHandler.PHONE_CONTACT_ID, "is_primary", "data2", "data3", ContactsHandler.PHONE_NUMBER};
    public static final String[] PROJECTION_EVENT = {SmsHandler.ROWID, ContactsHandler.NAME, ContactsHandler.PHONE_CONTACT_ID, "is_primary", "data2", "data3", ContactsHandler.PHONE_NUMBER};
    public static final String[] PROJECTION_EXPORT = {"mimetype", ContactsHandler.NAME, SmsHandler.ROWID, ContactsHandler.PHONE_CONTACT_ID, "raw_contact_id", "is_primary", ContactsHandler.PHONE_NUMBER, "data2", "data3", "data4", "data5", "data6", "data7", "data8", "data9", "data10", "data11", "data12", "data13", "data14", "data15"};
    public static final String[] PROJECTION_IM = {SmsHandler.ROWID, ContactsHandler.NAME, ContactsHandler.PHONE_CONTACT_ID, "is_primary", "data2", "data3", ContactsHandler.PHONE_NUMBER, "data5", "data6"};
    public static final String[] PROJECTION_NICKNAME = {SmsHandler.ROWID, ContactsHandler.NAME, ContactsHandler.PHONE_CONTACT_ID, "is_primary", "data2", "data3", ContactsHandler.PHONE_NUMBER};
    public static final String[] PROJECTION_NOTE = {SmsHandler.ROWID, ContactsHandler.NAME, ContactsHandler.PHONE_CONTACT_ID, "is_primary", ContactsHandler.PHONE_NUMBER};
    public static final String[] PROJECTION_ORGANIZATION = {SmsHandler.ROWID, ContactsHandler.NAME, ContactsHandler.PHONE_CONTACT_ID, "is_primary", "data2", "data3", ContactsHandler.PHONE_NUMBER, "data4", "data5", "data6", "data7", "data8", "data9"};
    public static final String[] PROJECTION_PEOPLE_CONTACT = {SmsHandler.ROWID, ContactsHandler.NAME, ContactsHandler.PHONE_NUMBER};
    public static final String[] PROJECTION_PHONE = {SmsHandler.ROWID, ContactsHandler.NAME, ContactsHandler.PHONE_CONTACT_ID, "is_primary", "data2", "data3", ContactsHandler.PHONE_NUMBER};
    public static final String[] PROJECTION_PHOTO = {SmsHandler.ROWID, ContactsHandler.NAME, ContactsHandler.PHONE_CONTACT_ID, "is_primary", "data15"};
    public static final String[] PROJECTION_POSTAL = {SmsHandler.ROWID, ContactsHandler.NAME, ContactsHandler.PHONE_CONTACT_ID, "is_primary", "data2", "data3", ContactsHandler.PHONE_NUMBER, "data4", "data5", "data6", "data7", "data8", "data9", "data10"};
    public static final String[] PROJECTION_STRUCTUREDNAME = {SmsHandler.ROWID, ContactsHandler.PHONE_NUMBER, ContactsHandler.PHONE_CONTACT_ID, "is_primary", "data2", "data3", "data4", "data5", "data6"};
    public static final String[] PROJECTION_WEBSITE = {SmsHandler.ROWID, ContactsHandler.NAME, ContactsHandler.PHONE_CONTACT_ID, "is_primary", "data2", "data3", ContactsHandler.PHONE_NUMBER};
    public static final int STRUCTUREDNAME_COLUMN_CONTACT_ID = 2;
    public static final int STRUCTUREDNAME_COLUMN_DISPLAY_NAME = 1;
    public static final int STRUCTUREDNAME_COLUMN_FAMILY_NAME = 5;
    public static final int STRUCTUREDNAME_COLUMN_GIVEN_NAME = 4;
    public static final int STRUCTUREDNAME_COLUMN_ID = 0;
    public static final int STRUCTUREDNAME_COLUMN_IS_PRIMARY = 3;
    public static final int STRUCTUREDNAME_COLUMN_MIDDLE_NAME = 7;
    public static final int STRUCTUREDNAME_COLUMN_PREFIX = 6;
    public static final int STRUCTUREDNAME_COLUMN_SUFFIX = 8;
    public static final int WEBSITE_COLUMN_CONTACT_ID = 2;
    public static final int WEBSITE_COLUMN_DISPLAY_NAME = 1;
    public static final int WEBSITE_COLUMN_ID = 0;
    public static final int WEBSITE_COLUMN_IS_PRIMARY = 3;
    public static final int WEBSITE_COLUMN_LABEL = 5;
    public static final int WEBSITE_COLUMN_TYPE = 4;
    public static final int WEBSITE_COLUMN_URL = 6;
    public static final Map<String, Map<String, EmailData>> mIDEmailMap = new HashMap();
    public static final Map<String, Map<String, EventData>> mIDEventMap = new HashMap();
    public static final Map<String, Map<String, ImData>> mIDImMap = new HashMap();
    public static final Map<String, Map<String, NickNameData>> mIDNickNameMap = new HashMap();
    public static final Map<String, Map<String, NoteData>> mIDNoteMap = new HashMap();
    public static final Map<String, Map<String, OrganizationData>> mIDOrganizationMap = new HashMap();
    public static final Map<String, Map<String, PhoneData>> mIDPhoneCutMap = new HashMap();
    public static final Map<String, Map<String, PhoneData>> mIDPhoneMap = new HashMap();
    public static final Map<String, Map<String, PostalData>> mIDPostalMap = new HashMap();
    public static final Map<String, Map<String, StructureNameData>> mIDStructureNameMap = new HashMap();
    public static final Map<String, Map<String, WebsiteData>> mIDWebsiteMap = new HashMap();
    public static final Map<String, IdData> mRawIDContactMap = new HashMap();

    public static class IdData {
        public final String mContactId;
        public final String mDisplayName;
        public final int mId;
        public final String mRawId;

        public IdData(int id, String rawId, String contactId, String display) {
            this.mId = id;
            this.mRawId = rawId;
            this.mContactId = contactId;
            this.mDisplayName = display;
        }
    }

    public static class StructureNameData {
        public final int id;
        public String mDisplayName = "";
        public String mFamilyName = "";
        public String mGivenName = "";
        public String mMiddleName = "";
        public String mPrefix = "";
        public String mSuffix = "";

        public StructureNameData(int id2, String given, String family, String prefix, String middle, String suffix, String dislpay) {
            this.id = id2;
            if (!TextUtils.isEmpty(given)) {
                this.mGivenName = given.trim();
            }
            if (!TextUtils.isEmpty(family)) {
                this.mFamilyName = family.trim();
            }
            if (!TextUtils.isEmpty(prefix)) {
                this.mPrefix = prefix.trim();
            }
            if (!TextUtils.isEmpty(middle)) {
                this.mMiddleName = middle.trim();
            }
            if (!TextUtils.isEmpty(suffix)) {
                this.mSuffix = suffix.trim();
            }
            if (!TextUtils.isEmpty(dislpay)) {
                this.mDisplayName = dislpay.trim();
            }
            if (TextUtils.isEmpty(this.mPrefix) && TextUtils.isEmpty(this.mSuffix) && TextUtils.isEmpty(this.mMiddleName) && !TextUtils.isEmpty(this.mGivenName) && !TextUtils.isEmpty(this.mDisplayName) && this.mGivenName.length() == 1 && !this.mDisplayName.contains(" ")) {
                this.mGivenName = this.mDisplayName;
            }
        }
    }

    public static class NickNameData {
        public final String data;
        public final int id;
        public final String label;
        public final String mTypeLabel;
        public final String mTypeLabelNick;
        public final int type;

        public NickNameData(int id2, int type2, String label2, String data2) {
            this.id = id2;
            this.type = type2;
            if (TextUtils.isEmpty(label2) || this.type != 0) {
                this.label = "";
            } else {
                this.label = label2.trim();
            }
            if (TextUtils.isEmpty(data2)) {
                this.data = "";
            } else {
                this.data = data2.trim();
            }
            this.mTypeLabel = (String.valueOf(this.type) + "_" + this.label).toUpperCase();
            this.mTypeLabelNick = (String.valueOf(this.mTypeLabel) + "_" + this.data).toUpperCase();
        }
    }

    public static class PhoneData {
        public String data;
        public int id;
        public boolean isPrimary;
        public String label;
        public int type;

        public PhoneData(int type2, String data2, String label2, boolean isPrimary2) {
            this.type = type2;
            if (TextUtils.isEmpty(label2) || this.type != 0) {
                this.label = "";
            } else {
                this.label = label2.trim();
            }
            if (TextUtils.isEmpty(data2)) {
                this.data = "";
            } else {
                this.data = data2.trim();
            }
            removeFormatSymbol();
            this.isPrimary = isPrimary2;
        }

        public PhoneData(int id2, int type2, String label2, String data2) {
            this.id = id2;
            this.type = type2;
            if (TextUtils.isEmpty(label2) || this.type != 0) {
                this.label = "";
            } else {
                this.label = label2.trim();
            }
            if (TextUtils.isEmpty(data2)) {
                this.data = "";
            } else {
                this.data = data2.trim();
            }
            removeFormatSymbol();
        }

        public boolean equals(Object obj) {
            if (obj instanceof PhoneData) {
                return false;
            }
            PhoneData phoneData = (PhoneData) obj;
            return this.type == phoneData.type && this.data.equals(phoneData.data) && this.label.equals(phoneData.label) && this.isPrimary == phoneData.isPrimary;
        }

        public String toString() {
            return String.format("type: %d, data: %s, label: %s, isPrimary: %s", Integer.valueOf(this.type), this.data, this.label, Boolean.valueOf(this.isPrimary));
        }

        private void removeFormatSymbol() {
            if (!TextUtils.isEmpty(this.data)) {
                this.data = this.data.replace("-", "");
            }
        }
    }

    public static class PostalData {
        public static final int ADDR_MAX_DATA_SIZE = 7;
        public String country;
        private String[] dataArray;
        public String formatAddress;
        public int id;
        public boolean isPrimary;
        public final String label;
        public String localty;
        public String mTypeLabel;
        public String mTypeLabelContent;
        public String neighborhood;
        public String pobox;
        public String postalCode;
        public String region;
        public String street;
        public final int type;

        public PostalData(int type2, List<String> propValueList, String label2, boolean isPrimary2) {
            int i;
            this.type = type2;
            this.dataArray = new String[7];
            int size = propValueList.size();
            size = size > 7 ? 7 : size;
            int i2 = 0;
            Iterator<String> it = propValueList.iterator();
            while (true) {
                if (it.hasNext()) {
                    String addressElement = it.next();
                    if (TextUtils.isEmpty(addressElement)) {
                        this.dataArray[i2] = addressElement;
                    } else {
                        this.dataArray[i2] = addressElement.trim();
                    }
                    i2++;
                    if (i2 >= size) {
                        i = i2;
                        break;
                    }
                } else {
                    i = i2;
                    break;
                }
            }
            while (i < 7) {
                this.dataArray[i] = null;
                i++;
            }
            this.pobox = this.dataArray[0];
            this.neighborhood = this.dataArray[1];
            this.street = this.dataArray[2];
            this.localty = this.dataArray[3];
            this.region = this.dataArray[4];
            this.postalCode = this.dataArray[5];
            this.country = this.dataArray[6];
            if (TextUtils.isEmpty(label2) || this.type != 0) {
                this.label = "";
            } else {
                this.label = label2;
            }
            this.isPrimary = isPrimary2;
        }

        public PostalData(int id2, int type2, String label2, String formatAddress2, String street2, String pobox2, String neighborhood2, String city, String region2, String postcode, String country2) {
            this.id = id2;
            this.type = type2;
            if (TextUtils.isEmpty(label2) || this.type != 0) {
                this.label = "";
            } else {
                this.label = label2.trim();
            }
            if (TextUtils.isEmpty(formatAddress2)) {
                this.formatAddress = "";
            } else {
                this.formatAddress = formatAddress2.trim();
            }
            if (TextUtils.isEmpty(street2)) {
                this.street = "";
            } else {
                this.street = street2.trim();
            }
            if (TextUtils.isEmpty(pobox2)) {
                this.pobox = "";
            } else {
                this.pobox = pobox2.trim();
            }
            if (TextUtils.isEmpty(neighborhood2)) {
                this.neighborhood = "";
            } else {
                this.neighborhood = neighborhood2.trim();
            }
            if (TextUtils.isEmpty(city)) {
                this.localty = "";
            } else {
                this.localty = city.trim();
            }
            if (TextUtils.isEmpty(region2)) {
                this.region = "";
            } else {
                this.region = region2.trim();
            }
            if (TextUtils.isEmpty(postcode)) {
                this.postalCode = "";
            } else {
                this.postalCode = postcode.trim();
            }
            if (TextUtils.isEmpty(country2)) {
                this.country = "";
            } else {
                this.country = country2.trim();
            }
            this.mTypeLabel = (String.valueOf(this.type) + "_" + this.label).toUpperCase();
            this.mTypeLabelContent = (String.valueOf(this.mTypeLabel) + "_" + this.street + "_" + this.pobox + "_" + this.neighborhood + "_" + this.localty + "_" + this.region + "_" + this.postalCode + "_" + this.country).toUpperCase();
        }

        public boolean equals(Object obj) {
            if (obj instanceof PostalData) {
                return false;
            }
            PostalData postalData = (PostalData) obj;
            return Arrays.equals(this.dataArray, postalData.dataArray) && this.type == postalData.type && (this.type != 0 || this.label == postalData.label) && this.isPrimary == postalData.isPrimary;
        }

        public String getFormattedAddress(int vcardType) {
            StringBuilder builder = new StringBuilder();
            boolean empty = true;
            if (VCardConfig.isJapaneseDevice(vcardType)) {
                for (int i = 6; i >= 0; i--) {
                    String addressPart = this.dataArray[i];
                    if (!TextUtils.isEmpty(addressPart)) {
                        if (!empty) {
                            builder.append(' ');
                        }
                        builder.append(addressPart);
                        empty = false;
                    }
                }
            } else {
                for (int i2 = 0; i2 < 7; i2++) {
                    String addressPart2 = this.dataArray[i2];
                    if (!TextUtils.isEmpty(addressPart2)) {
                        if (!empty) {
                            builder.append(' ');
                        }
                        builder.append(addressPart2);
                        empty = false;
                    }
                }
            }
            return builder.toString().trim();
        }

        public String toString() {
            return String.format("type: %d, label: %s, isPrimary: %s", Integer.valueOf(this.type), this.label, Boolean.valueOf(this.isPrimary));
        }
    }

    public static class EmailData {
        public final String data;
        public int id;
        public boolean isPrimary;
        public String label;
        public String mTypeLabel;
        public String mTypeLabelEmail;
        public final int type;

        public EmailData(int type2, String data2, String label2, boolean isPrimary2) {
            this.type = type2;
            if (TextUtils.isEmpty(label2) || this.type != 0) {
                this.label = "";
            } else {
                this.label = label2.trim();
            }
            if (TextUtils.isEmpty(data2)) {
                this.data = "";
            } else {
                this.data = data2.trim();
            }
            this.isPrimary = isPrimary2;
        }

        public EmailData(int id2, int type2, String label2, String data2) {
            this.id = id2;
            this.type = type2;
            if (TextUtils.isEmpty(label2) || this.type != 0) {
                this.label = "";
            } else {
                this.label = label2.trim();
            }
            if (this.type == 0 && TextUtils.isEmpty(this.label)) {
                this.label = Constants.ATTR_TYPE_INTERNET;
            }
            if (TextUtils.isEmpty(data2)) {
                this.data = "";
            } else {
                this.data = data2.trim();
            }
            this.mTypeLabel = (String.valueOf(this.type) + "_" + this.label).toUpperCase();
            this.mTypeLabelEmail = this.data.toUpperCase();
        }

        public boolean equals(Object obj) {
            if (obj instanceof EmailData) {
                return false;
            }
            EmailData emailData = (EmailData) obj;
            return this.type == emailData.type && this.data.equals(emailData.data) && this.label.equals(emailData.label) && this.isPrimary == emailData.isPrimary;
        }

        public String toString() {
            return String.format("type: %d, data: %s, label: %s, isPrimary: %s", Integer.valueOf(this.type), this.data, this.label, Boolean.valueOf(this.isPrimary));
        }
    }

    public static class ImData {
        public final String data;
        public int id;
        public boolean isPrimary = false;
        public final String label;
        public String mCustomProtocol;
        public int mProtocolName;
        public final String mTypeLabel;
        public final String mTypeLabelContent;
        public final int type;

        public ImData(int type2, String data2, String label2, boolean isPrimary2) {
            this.type = type2;
            if (TextUtils.isEmpty(label2) || this.type != 0) {
                this.label = "";
            } else {
                this.label = label2.trim();
            }
            if (TextUtils.isEmpty(data2)) {
                this.data = "";
            } else {
                this.data = data2.trim();
            }
            this.isPrimary = isPrimary2;
            this.mTypeLabel = (String.valueOf(this.type) + "_" + this.label).toUpperCase();
            this.mTypeLabelContent = (String.valueOf(this.mTypeLabel) + "_" + this.data).toUpperCase();
        }

        public ImData(int id2, int type2, String label2, String data2, int protocalName, String protocalCustom) {
            this.id = id2;
            this.type = type2;
            if (TextUtils.isEmpty(label2) || this.type != 0) {
                this.label = "";
            } else {
                this.label = label2.trim();
            }
            if (TextUtils.isEmpty(data2)) {
                this.data = "";
            } else {
                this.data = data2.trim();
            }
            this.mProtocolName = protocalName;
            if (this.mProtocolName == -1) {
                this.mCustomProtocol = protocalCustom.trim();
            } else {
                this.mCustomProtocol = "";
            }
            this.mTypeLabel = (String.valueOf(this.type) + "_" + this.label).toUpperCase();
            this.mTypeLabelContent = (String.valueOf(this.mTypeLabel) + "_" + this.data + "_" + this.mProtocolName + "_" + this.mCustomProtocol).toUpperCase();
        }

        public boolean equals(Object obj) {
            if (obj instanceof ImData) {
                return false;
            }
            ImData imData = (ImData) obj;
            return this.type == imData.type && this.data.equals(imData.data) && this.label.equals(imData.label) && this.isPrimary == imData.isPrimary;
        }

        public String toString() {
            return String.format("type: %d, data: %s, label: %s, isPrimary: %s", Integer.valueOf(this.type), this.data, this.label, Boolean.valueOf(this.isPrimary));
        }
    }

    public static class WebsiteData {
        public final String data;
        public final int id;
        public final String label;
        public final String mTypeLabel;
        public final String mTypeLabelContent;
        public final int type;

        public WebsiteData(int id2, int type2, String label2, String data2) {
            this.id = id2;
            this.type = type2;
            if (TextUtils.isEmpty(label2) || this.type != 0) {
                this.label = "";
            } else {
                this.label = label2.trim();
            }
            if (TextUtils.isEmpty(data2)) {
                this.data = "";
            } else {
                this.data = data2.trim();
            }
            this.mTypeLabel = (String.valueOf(this.type) + "_" + this.label).toUpperCase();
            this.mTypeLabelContent = (String.valueOf(this.mTypeLabel) + "_" + this.data).toUpperCase();
        }
    }

    public static class EventData {
        public final String data;
        public final int id;
        public final String label;
        public final String mTypeLabel;
        public final String mTypeLabelContent;
        public final int type;

        public EventData(int id2, int type2, String label2, String data2) {
            this.id = id2;
            this.type = type2;
            if (TextUtils.isEmpty(label2) || this.type != 0) {
                this.label = "";
            } else {
                this.label = label2.trim();
            }
            if (TextUtils.isEmpty(data2)) {
                this.data = "";
            } else {
                this.data = data2.trim();
            }
            this.mTypeLabel = (String.valueOf(this.type) + "_" + this.label).toUpperCase();
            this.mTypeLabelContent = (String.valueOf(this.mTypeLabel) + "_" + this.data).toUpperCase();
        }
    }

    public static class OrganizationData {
        public final String companyName;
        public int id;
        public boolean isPrimary;
        public String label = "";
        public String mDepartment;
        public String mJobDescrition;
        public String mOfficeLocation;
        public String mPhoneticName;
        public String mSymbol;
        public final String mTypeLabel;
        public final String mTypeLabelContent;
        public String positionName;
        public final int type;

        public OrganizationData(int type2, String companyName2, String positionName2, boolean isPrimary2) {
            this.type = type2;
            if (TextUtils.isEmpty(companyName2)) {
                this.companyName = "";
            } else {
                this.companyName = companyName2.trim();
            }
            if (TextUtils.isEmpty(positionName2)) {
                this.positionName = "";
            } else {
                this.positionName = positionName2.trim();
            }
            this.isPrimary = isPrimary2;
            this.mTypeLabel = "1_".toUpperCase();
            this.mTypeLabelContent = (String.valueOf(this.mTypeLabel) + "_" + companyName2 + "_" + positionName2).toUpperCase();
        }

        public OrganizationData(int id2, int type2, String label2, String company, String title, String department, String job, String symbol, String phonetic, String officLoc) {
            this.id = id2;
            this.type = type2;
            if (TextUtils.isEmpty(label2) || this.type != 0) {
                this.label = "";
            } else {
                this.label = label2.trim();
            }
            if (TextUtils.isEmpty(company)) {
                this.companyName = "";
            } else {
                this.companyName = company.trim();
            }
            if (TextUtils.isEmpty(title)) {
                this.positionName = "";
            } else {
                this.positionName = title.trim();
            }
            if (TextUtils.isEmpty(department)) {
                this.mDepartment = "";
            } else {
                this.mDepartment = department.trim();
            }
            if (TextUtils.isEmpty(job)) {
                this.mJobDescrition = "";
            } else {
                this.mJobDescrition = job.trim();
            }
            if (TextUtils.isEmpty(symbol)) {
                this.mSymbol = "";
            } else {
                this.mSymbol = symbol.trim();
            }
            if (TextUtils.isEmpty(phonetic)) {
                this.mPhoneticName = "";
            } else {
                this.mPhoneticName = phonetic.trim();
            }
            if (TextUtils.isEmpty(officLoc)) {
                this.mOfficeLocation = "";
            } else {
                this.mOfficeLocation = officLoc.trim();
            }
            this.mTypeLabel = "1_".toUpperCase();
            this.mTypeLabelContent = (String.valueOf(this.mTypeLabel) + "_" + this.companyName + "_" + this.positionName + "_" + this.mDepartment + "_" + this.mJobDescrition + "_" + this.mSymbol + "_" + this.mPhoneticName + "_" + this.mOfficeLocation).toUpperCase();
        }

        public boolean equals(Object obj) {
            if (obj instanceof OrganizationData) {
                return false;
            }
            OrganizationData organization = (OrganizationData) obj;
            return this.type == organization.type && this.companyName.equals(organization.companyName) && this.positionName.equals(organization.positionName) && this.isPrimary == organization.isPrimary;
        }

        public String toString() {
            return String.format("type: %d, company: %s, position: %s, isPrimary: %s", Integer.valueOf(this.type), this.companyName, this.positionName, Boolean.valueOf(this.isPrimary));
        }
    }

    public static class PhotoData {
        public static final String FORMAT_FLASH = "SWF";
        public final String formatName;
        public final byte[] photoBytes;
        public final int type;

        public PhotoData(int type2, String formatName2, byte[] photoBytes2) {
            this.type = type2;
            if (TextUtils.isEmpty(formatName2)) {
                this.formatName = "";
            } else {
                this.formatName = formatName2.trim();
            }
            this.photoBytes = photoBytes2;
        }
    }

    public static class NoteData {
        public final int id;
        public final String mDisplayName;
        public final String mNoteContent;
        public final String mTypeContent;

        public NoteData(int id2, String note, String display) {
            this.id = id2;
            if (TextUtils.isEmpty(display)) {
                this.mDisplayName = "";
            } else {
                this.mDisplayName = display.trim();
            }
            if (TextUtils.isEmpty(note)) {
                this.mNoteContent = "";
            } else {
                this.mNoteContent = note.trim();
            }
            this.mTypeContent = this.mNoteContent.toUpperCase();
        }
    }
}
