package com.netqin.antivirus.contact.vcard;

public class VCardInvalidLineException extends VCardException {
    public VCardInvalidLineException() {
    }

    public VCardInvalidLineException(String message) {
        super(message);
    }
}
