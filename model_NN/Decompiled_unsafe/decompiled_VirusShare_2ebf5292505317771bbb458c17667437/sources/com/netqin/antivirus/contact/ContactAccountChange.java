package com.netqin.antivirus.contact;

import android.content.ContentValues;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.netqin.antivirus.SHA1Util;
import com.netqin.antivirus.Value;
import com.netqin.antivirus.antilost.AntiLostCommon;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.common.CommonUtils;
import com.netqin.antivirus.common.ProgDlgActivity;
import com.netqin.antivirus.net.accountservice.AccountService;
import com.nqmobile.antivirus_ampro20.R;

public class ContactAccountChange extends ProgDlgActivity implements TextWatcher {
    /* access modifiers changed from: private */
    public AccountService accountService;
    /* access modifiers changed from: private */
    public ContentValues content;
    /* access modifiers changed from: private */
    public Handler handler;
    private String mAccount;
    /* access modifiers changed from: private */
    public boolean mActivityVisible = false;
    private Button mBtnOk;
    private EditText mETPwdCfm;
    private EditText mETPwdNew;
    /* access modifiers changed from: private */
    public EditText mETPwdOrg;
    private EditText mETUser;
    private TextView mTVWarning1;
    private TextView mTVWarning2;
    private TextView mTitle;

    public void onCreate(Bundle savedInstanceState) {
        this.accountService = AccountService.getInstance(this);
        this.content = new ContentValues();
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.contact_account_change);
        this.mActivityVisible = true;
        this.mETUser = (EditText) findViewById(R.id.contact_account_change_user);
        this.mETPwdOrg = (EditText) findViewById(R.id.contact_account_change_pwdorg);
        this.mETPwdNew = (EditText) findViewById(R.id.contact_account_change_pwdnew);
        this.mETPwdCfm = (EditText) findViewById(R.id.contact_account_change_pwdcfm);
        this.mBtnOk = (Button) findViewById(R.id.contact_account_change_ok);
        this.mTVWarning1 = (TextView) findViewById(R.id.contact_account_change_warning1);
        this.mTVWarning2 = (TextView) findViewById(R.id.contact_account_change_warning2);
        this.mTitle = (TextView) findViewById(R.id.activity_name);
        this.mTitle.setText((int) R.string.label_change_password);
        this.mETUser.addTextChangedListener(this);
        this.mETPwdOrg.addTextChangedListener(this);
        this.mETPwdNew.addTextChangedListener(this);
        this.mETPwdCfm.addTextChangedListener(this);
        this.mAccount = CommonMethod.getUserOrPassword(this, AntiLostCommon.DELETE_CONTACT, "user");
        this.mETUser.setText(this.mAccount);
        this.mBtnOk.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ContactAccountChange.this.clickOk();
            }
        });
        ((Button) findViewById(R.id.contact_account_change_cancel)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ContactAccountChange.this.clickCancel();
            }
        });
    }

    public void afterTextChanged(Editable s) {
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {
        String account = this.mETUser.getText().toString().trim();
        String pwdOrg = this.mETPwdOrg.getText().toString().trim();
        String pwdNew = this.mETPwdNew.getText().toString().trim();
        String pwdCfm = this.mETPwdCfm.getText().toString().trim();
        if (TextUtils.isEmpty(account) || TextUtils.isEmpty(pwdOrg) || TextUtils.isEmpty(pwdNew) || TextUtils.isEmpty(pwdCfm)) {
            this.mBtnOk.setEnabled(false);
        } else if (pwdOrg.length() < 6 || pwdOrg.length() > 20 || pwdNew.length() < 6 || pwdNew.length() > 20 || pwdCfm.length() < 6 || pwdCfm.length() > 20) {
            this.mBtnOk.setEnabled(false);
        } else {
            this.mBtnOk.setEnabled(true);
        }
        if (pwdNew.length() <= 0) {
            this.mTVWarning1.setVisibility(8);
        } else if (pwdNew.length() < 6) {
            this.mTVWarning1.setText((int) R.string.text_less_than_6);
            this.mTVWarning1.setVisibility(0);
        } else if (pwdNew.length() > 20) {
            this.mTVWarning1.setText((int) R.string.text_more_than_20);
            this.mTVWarning1.setVisibility(0);
        } else {
            this.mTVWarning1.setVisibility(8);
        }
        if (pwdCfm.length() <= 0) {
            this.mTVWarning2.setVisibility(8);
        } else if (pwdCfm.length() < 6) {
            this.mTVWarning2.setText((int) R.string.text_less_than_6);
            this.mTVWarning2.setVisibility(0);
        } else if (pwdCfm.length() > 20) {
            this.mTVWarning2.setText((int) R.string.text_more_than_20);
            this.mTVWarning2.setVisibility(0);
        } else {
            this.mTVWarning2.setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public void clickOk() {
        String account = this.mETUser.getText().toString();
        String pwdOrg = this.mETPwdOrg.getText().toString();
        final String pwdNew = this.mETPwdNew.getText().toString();
        String pwdCfm = this.mETPwdCfm.getText().toString();
        if (!CommonMethod.isValidLetterAndNumber(pwdNew)) {
            CommonMethod.messageDialog(this, (int) R.string.text_password_must_letter_number, (int) R.string.label_netqin_antivirus);
        } else if (pwdNew.compareTo(pwdCfm) != 0) {
            CommonMethod.messageDialog(this, getString(R.string.text_antilost_setpassword_diff), (int) R.string.label_netqin_antivirus);
            this.mETPwdCfm.setText("");
            this.mETPwdCfm.requestFocus();
        } else {
            this.content.put(Value.Username, account);
            this.content.put(Value.Password, SHA1Util.hex_sha1(pwdOrg));
            this.content.put(Value.NewPassword, SHA1Util.hex_sha1(pwdNew));
            this.content.put("UID", CommonMethod.getUID(this));
            this.mProgressText = (String) getText(R.string.text_connecting);
            this.mProgressDlg = CommonMethod.progressDlgCreate(this, this.mProgressText, this.mHandler);
            CommonMethod.sendUserMessage(this.mHandler, 1);
            new Thread(new Runnable() {
                public void run() {
                    final int result = ContactAccountChange.this.accountService.request(ContactAccountChange.this.handler, ContactAccountChange.this.content, 3);
                    CommonMethod.sendUserMessage(ContactAccountChange.this.mHandler, 4);
                    Handler access$5 = ContactAccountChange.this.mHandler;
                    final String str = pwdNew;
                    access$5.post(new Runnable() {
                        public void run() {
                            if (!ContactAccountChange.this.mActivityVisible) {
                                return;
                            }
                            if (result == 8 || result == 16) {
                                CommonMethod.messageDialog(ContactAccountChange.this, (int) R.string.SEND_RECEIVE_ERROR, (int) R.string.label_netqin_antivirus);
                            } else if (ContactAccountChange.this.content.containsKey(Value.Code)) {
                                String code = ContactAccountChange.this.content.getAsString(Value.Code);
                                if (code.equals("0")) {
                                    CommonUtils.putConfigWithStringValue(ContactAccountChange.this, AntiLostCommon.DELETE_CONTACT, "password", str);
                                    ContactAccountChange.this.finish();
                                    if (AccountAdminGuide.mAccountAdminGuide != null) {
                                        AccountAdminGuide.mAccountAdminGuide.finish();
                                    }
                                    Toast toast = Toast.makeText(ContactAccountChange.this, (int) R.string.text_modify_password_succ, 1);
                                    toast.setGravity(81, 0, 100);
                                    toast.show();
                                } else if (code.equals("-1")) {
                                    CommonMethod.messageDialog(ContactAccountChange.this, (int) R.string.SYSTEM_ERROR, (int) R.string.label_netqin_antivirus);
                                } else if (code.equals("-2")) {
                                    CommonMethod.messageDialog(ContactAccountChange.this, (int) R.string.USERNAME_NOT_EXIST, (int) R.string.label_netqin_antivirus);
                                } else if (code.equals("-3")) {
                                    CommonMethod.messageDialog(ContactAccountChange.this, (int) R.string.ORIGINAL_PASSWORD_ERROR, (int) R.string.label_netqin_antivirus);
                                    ContactAccountChange.this.mETPwdOrg.setText("");
                                    ContactAccountChange.this.mETPwdOrg.requestFocus();
                                }
                            }
                        }
                    });
                }
            }).start();
        }
    }

    /* access modifiers changed from: private */
    public void clickCancel() {
        finish();
    }
}
