package com.netqin.antivirus.contact.vcard;

public interface EntryHandler {
    void onEntryCreated(ContactStruct contactStruct);

    void onParsingEnd();

    void onParsingStart();
}
