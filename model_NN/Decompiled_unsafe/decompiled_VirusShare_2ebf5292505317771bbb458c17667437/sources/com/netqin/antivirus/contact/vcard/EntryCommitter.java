package com.netqin.antivirus.contact.vcard;

import android.content.ContentResolver;

public class EntryCommitter implements EntryHandler {
    public static String LOG_TAG = "vcard.EntryComitter";
    private ContentResolver mContentResolver;
    private long mTimeToCommit;

    public EntryCommitter(ContentResolver resolver) {
        this.mContentResolver = resolver;
    }

    public void onParsingStart() {
    }

    public void onParsingEnd() {
        VCardConfig.showPerformanceLog();
    }

    public void onEntryCreated(ContactStruct contactStruct) {
        long start = System.currentTimeMillis();
        contactStruct.pushIntoContentResolver(this.mContentResolver);
        this.mTimeToCommit += System.currentTimeMillis() - start;
    }
}
