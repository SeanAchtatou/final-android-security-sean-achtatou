package com.netqin.antivirus.contact.vcard;

import java.util.Collection;
import java.util.List;

public class VCardBuilderCollection implements VCardBuilder {
    private final Collection<VCardBuilder> mVCardBuilderCollection;

    public VCardBuilderCollection(Collection<VCardBuilder> vBuilderCollection) {
        this.mVCardBuilderCollection = vBuilderCollection;
    }

    public Collection<VCardBuilder> getVCardBuilderBaseCollection() {
        return this.mVCardBuilderCollection;
    }

    public void start() {
        for (VCardBuilder builder : this.mVCardBuilderCollection) {
            builder.start();
        }
    }

    public void end() {
        for (VCardBuilder builder : this.mVCardBuilderCollection) {
            builder.end();
        }
    }

    public void startRecord(String type) {
        for (VCardBuilder builder : this.mVCardBuilderCollection) {
            builder.startRecord(type);
        }
    }

    public void endRecord() {
        for (VCardBuilder builder : this.mVCardBuilderCollection) {
            builder.endRecord();
        }
    }

    public void startProperty() {
        for (VCardBuilder builder : this.mVCardBuilderCollection) {
            builder.startProperty();
        }
    }

    public void endProperty() {
        for (VCardBuilder builder : this.mVCardBuilderCollection) {
            builder.endProperty();
        }
    }

    public void propertyGroup(String group) {
        for (VCardBuilder builder : this.mVCardBuilderCollection) {
            builder.propertyGroup(group);
        }
    }

    public void propertyName(String name) {
        for (VCardBuilder builder : this.mVCardBuilderCollection) {
            builder.propertyName(name);
        }
    }

    public void propertyParamType(String type) {
        for (VCardBuilder builder : this.mVCardBuilderCollection) {
            builder.propertyParamType(type);
        }
    }

    public void propertyParamValue(String value) {
        for (VCardBuilder builder : this.mVCardBuilderCollection) {
            builder.propertyParamValue(value);
        }
    }

    public void propertyValues(List<String> values) {
        for (VCardBuilder builder : this.mVCardBuilderCollection) {
            builder.propertyValues(values);
        }
    }
}
