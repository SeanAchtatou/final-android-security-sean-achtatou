package com.netqin.antivirus.contact.vcard;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.Iterator;

public final class Entity implements Parcelable {
    public static final Parcelable.Creator<Entity> CREATOR = new Parcelable.Creator<Entity>() {
        public Entity createFromParcel(Parcel source) {
            return new Entity(source, null);
        }

        public Entity[] newArray(int size) {
            return new Entity[size];
        }
    };
    private final ArrayList<NamedContentValues> mSubValues;
    private final ContentValues mValues;

    public Entity(ContentValues values) {
        this.mValues = values;
        this.mSubValues = new ArrayList<>();
    }

    public ContentValues getEntityValues() {
        return this.mValues;
    }

    public ArrayList<NamedContentValues> getSubValues() {
        return this.mSubValues;
    }

    public void addSubValue(Uri uri, ContentValues values) {
        this.mSubValues.add(new NamedContentValues(uri, values));
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        this.mValues.writeToParcel(dest, 0);
        dest.writeInt(this.mSubValues.size());
        Iterator<NamedContentValues> it = this.mSubValues.iterator();
        while (it.hasNext()) {
            NamedContentValues value = it.next();
            value.uri.writeToParcel(dest, 0);
            value.values.writeToParcel(dest, 0);
        }
    }

    private Entity(Parcel source) {
        this.mValues = ContentValues.CREATOR.createFromParcel(source);
        int numValues = source.readInt();
        this.mSubValues = new ArrayList<>(numValues);
        for (int i = 0; i < numValues; i++) {
            this.mSubValues.add(new NamedContentValues((Uri) Uri.CREATOR.createFromParcel(source), ContentValues.CREATOR.createFromParcel(source)));
        }
    }

    /* synthetic */ Entity(Parcel parcel, Entity entity) {
        this(parcel);
    }

    public static class NamedContentValues {
        public final Uri uri;
        public final ContentValues values;

        public NamedContentValues(Uri uri2, ContentValues values2) {
            this.uri = uri2;
            this.values = values2;
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Entity: ").append(getEntityValues());
        Iterator<NamedContentValues> it = getSubValues().iterator();
        while (it.hasNext()) {
            NamedContentValues namedValue = it.next();
            sb.append("\n  ").append(namedValue.uri);
            sb.append("\n  -> ").append(namedValue.values);
        }
        return sb.toString();
    }
}
