package com.netqin.antivirus.contact.vcard;

public class VCardException extends Exception {
    public VCardException() {
    }

    public VCardException(String message) {
        super(message);
    }
}
