package com.netqin.antivirus.contact;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.widget.Toast;
import com.netqin.antivirus.Value;
import com.netqin.antivirus.antilost.AntiLostCommon;
import com.netqin.antivirus.common.CommonDefine;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.common.CommonUtils;
import com.netqin.antivirus.common.ProgDlgActivity;
import com.netqin.antivirus.common.ProgressTextDialog;
import com.netqin.antivirus.contact.vcard.ContactData;
import com.netqin.antivirus.net.contactservice.ContactService;
import com.nqmobile.antivirus_ampro20.R;
import java.io.File;
import java.io.IOException;

public class ServerRestoreDoing extends ProgDlgActivity {
    public static boolean mFromContactGuide = false;
    private static String mMessageBoxText = "";
    public static Activity mServerRestoreDoing = null;
    /* access modifiers changed from: private */
    public ContactService contactService;
    /* access modifiers changed from: private */
    public ContentValues content;
    private boolean mProgTextMarkSucc = false;
    /* access modifiers changed from: private */
    public int mProgressCurr = 0;
    private int mProgressMax = 0;
    public float mUnitKInt = 1.0f;
    public String mUnitString = "B";
    /* access modifiers changed from: private */
    public VCardReadThread mVCardReadThread = null;

    public void onCreate(Bundle savedInstanceState) {
        this.content = new ContentValues();
        this.contactService = ContactService.getInstance(this);
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        if (ContactGuide.isRestoreFromContactGuide) {
            ContactGuide.isRestoreFromContactGuide = false;
            clickOk();
        }
        if (mFromContactGuide) {
            mFromContactGuide = false;
            mMessageBoxText = "";
            showDialog(R.string.text_is_restore_from_network);
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            this.mActivityVisible = false;
            if (this.contactService != null) {
                this.contactService.cancel();
            }
            if (this.mVCardReadThread != null) {
                this.mVCardReadThread.cancel();
                this.mVCardReadThread = null;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    /* access modifiers changed from: protected */
    public void progressTextDialogOnStop() {
        if (!this.mProgTextMarkSucc) {
            if (this.contactService != null) {
                this.contactService.cancel();
            }
            if (this.mVCardReadThread != null) {
                this.mVCardReadThread.cancel();
                this.mVCardReadThread = null;
            } else {
                Message msg = new Message();
                msg.what = 12;
                msg.arg1 = 25;
                msg.arg2 = 0;
                this.mHandler.sendMessage(msg);
            }
        }
        super.progressTextDialogOnStop();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        DialogInterface.OnClickListener cancelListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                ServerRestoreDoing.this.clickCancel();
            }
        };
        DialogInterface.OnKeyListener keyListener = new DialogInterface.OnKeyListener() {
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode != 4) {
                    return false;
                }
                ServerRestoreDoing.this.clickCancel();
                return true;
            }
        };
        DialogInterface.OnClickListener contactListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                ServerRestoreDoing.this.viewContact();
            }
        };
        DialogInterface.OnClickListener downloadListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                ServerRestoreDoing.this.clickOk();
            }
        };
        DialogInterface.OnClickListener succListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                ServerRestoreDoing.this.clickSucc();
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        switch (id) {
            case 20:
                builder.setTitle((int) R.string.label_netqin_antivirus);
                builder.setMessage(mMessageBoxText);
                builder.setPositiveButton((int) R.string.label_ok, cancelListener);
                builder.setOnKeyListener(keyListener);
                return builder.create();
            case 27:
                builder.setTitle((int) R.string.label_netqin_antivirus);
                builder.setMessage(mMessageBoxText);
                builder.setPositiveButton((int) R.string.label_ok, cancelListener);
                builder.setOnKeyListener(keyListener);
                return builder.create();
            case 30:
                builder.setTitle((int) R.string.label_restore_success_tip);
                builder.setMessage(mMessageBoxText);
                if (ContactCommon.contactViewIsExist(new Intent("android.intent.action.VIEW", ContactData.CONTENT_URI), getPackageManager())) {
                    builder.setPositiveButton((int) R.string.label_ok, contactListener);
                    builder.setNegativeButton((int) R.string.label_cancel, succListener);
                } else {
                    builder.setPositiveButton((int) R.string.label_ok, succListener);
                }
                builder.setOnKeyListener(keyListener);
                return builder.create();
            case CommonDefine.MSG_PROG_ARG_ERRMSG_CONTACT_IMPORT:
                builder.setTitle((int) R.string.label_netqin_antivirus);
                builder.setMessage(mMessageBoxText);
                builder.setPositiveButton((int) R.string.label_ok, cancelListener);
                builder.setOnKeyListener(keyListener);
                return builder.create();
            case CommonDefine.MSG_PROG_ARG_ERRMSG_NETWORK:
                builder.setTitle((int) R.string.label_netqin_antivirus);
                builder.setMessage(mMessageBoxText);
                builder.setPositiveButton((int) R.string.label_ok, downloadListener);
                builder.setNegativeButton((int) R.string.label_cancel, cancelListener);
                builder.setOnKeyListener(keyListener);
                return builder.create();
            case R.string.text_is_restore_from_network /*2131427477*/:
                builder.setTitle((int) R.string.label_netqin_antivirus);
                builder.setMessage((int) R.string.text_is_restore_from_network);
                builder.setPositiveButton((int) R.string.label_ok, downloadListener);
                builder.setNegativeButton((int) R.string.label_cancel, cancelListener);
                builder.setOnKeyListener(keyListener);
                return builder.create();
            default:
                return super.onCreateDialog(id);
        }
    }

    /* access modifiers changed from: protected */
    public void childFunctionCall(Message msg) {
        String filePath;
        int arg1 = msg.arg1;
        int arg2 = msg.arg2;
        switch (arg1) {
            case 21:
                this.mProgressMax = arg2;
                this.mProgTextRight = "0%";
                CommonMethod.sendUserMessage(this.mHandler, 8, this.mProgressMax);
                return;
            case 22:
                if (this.mProgressCurr < this.mProgressMax) {
                    this.mProgressCurr = this.mProgressMax;
                    this.mProgTextRight = "100%";
                    runOnUiThread(new Runnable() {
                        public void run() {
                            CommonMethod.sendUserMessage(ServerRestoreDoing.this.mHandler, 9, ServerRestoreDoing.this.mProgressCurr);
                        }
                    });
                }
                try {
                    filePath = getFileStreamPath(CommonDefine.CONTACT_FILE_TEMP).getCanonicalPath();
                } catch (IOException e) {
                    filePath = "";
                    e.printStackTrace();
                }
                importContact(filePath);
                return;
            case 23:
            case 24:
            case CommonDefine.MSG_PROG_ARG_ERROR:
            case CommonDefine.MSG_PROG_ARG_ERRMORE:
            case 31:
            case 32:
            case 33:
            case CommonDefine.MSG_PROG_ARG_ERRMSG_CONTACT_EXPORT:
            default:
                mMessageBoxText = getString(R.string.text_unprocess_message, new Object[]{Integer.valueOf(msg.arg1)});
                showDialog(20);
                return;
            case CommonDefine.MSG_PROG_ARG_CANCEL:
                clickCancel();
                Toast toast = Toast.makeText(this, getString(R.string.text_restore_cantact_cancel, new Object[]{Integer.valueOf(msg.arg2)}), 0);
                toast.setGravity(81, 0, 100);
                toast.show();
                return;
            case 27:
                this.mProgTextMarkSucc = true;
                ContactCommon.writeOperationLog(24, getFilesDir().getPath());
                CommonMethod.sendUserMessage(this.mHandler, 10);
                mMessageBoxText = (String) msg.obj;
                showDialog(27);
                return;
            case CommonDefine.MSG_PROG_ARG_UPDATE:
                if (arg2 >= this.mProgressCurr) {
                    if (arg2 > this.mProgressMax) {
                        arg2 = this.mProgressMax;
                    }
                    this.mProgressCurr = arg2;
                    this.mProgTextRight = String.valueOf((int) (((float) (this.mProgressCurr * 100)) / ((float) this.mProgressMax))) + "%";
                    CommonMethod.sendUserMessage(this.mHandler, 9, this.mProgressCurr);
                    return;
                }
                return;
            case 30:
                this.mProgTextMarkSucc = true;
                ContactCommon.writeOperationLog(23, getFilesDir().getPath());
                CommonMethod.sendUserMessage(this.mHandler, 10);
                if (this.mUnitString.equalsIgnoreCase("KB")) {
                    String str = String.valueOf(ContactCommon.floatEndWithTwo(((float) this.mProgressCurr) / this.mUnitKInt)) + this.mUnitString;
                } else {
                    String str2 = String.valueOf(this.mProgressCurr) + this.mUnitString;
                }
                CommonMethod.putConfigWithStringValue(this, AntiLostCommon.DELETE_CONTACT, "contacts_network", Integer.toString(msg.arg2));
                int count = ContactCommon.getContactCount(this);
                if (ContactCommon.contactViewIsExist(new Intent("android.intent.action.VIEW", ContactData.CONTENT_URI), getPackageManager())) {
                    mMessageBoxText = getString(R.string.text_resotre_network_contact_succ_result, new Object[]{Integer.valueOf(msg.arg2), Integer.valueOf(count)});
                } else {
                    mMessageBoxText = getString(R.string.text_resotre_network_contact_succ_result_noview, new Object[]{Integer.valueOf(msg.arg2), Integer.valueOf(count)});
                }
                showDialog(30);
                return;
            case CommonDefine.MSG_PROG_ARG_ERRMSG_CONTACT_IMPORT:
                this.mProgTextMarkSucc = true;
                ContactCommon.writeOperationLog(24, getFilesDir().getPath());
                CommonMethod.sendUserMessage(this.mHandler, 10);
                mMessageBoxText = (String) msg.obj;
                showDialog(35);
                return;
            case CommonDefine.MSG_PROG_ARG_ERRMSG_NETWORK:
                this.mProgTextMarkSucc = true;
                ContactCommon.writeOperationLog(24, getFilesDir().getPath());
                CommonMethod.sendUserMessage(this.mHandler, 10);
                mMessageBoxText = getString(R.string.text_downloading_fail);
                showDialog(36);
                return;
            case CommonDefine.MSG_PROG_ARG_CANCEL_SERVER:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void clickOk() {
        this.mProgTextDialog = new ProgressTextDialog(this, R.string.text_downloading_contact1, this.mHandler);
        this.mProgTextDialog.setLeftVisibleState(0);
        this.mProgTextDialog.setAnimationState(0);
        this.mProgTextDialog.setAnimationImageSourceFrom(-1);
        this.mProgTextDialog.setAnimationImageSourceTo(R.drawable.animation_mobile);
        CommonMethod.sendUserMessage(this.mHandler, 6);
        this.content.put("IMEI", CommonMethod.getIMEI(this));
        this.content.put("IMSI", CommonMethod.getIMSI(this));
        this.content.put(Value.ContentType, "AV_VCARD");
        this.content.put(Value.Username, CommonMethod.getUserOrPassword(this, AntiLostCommon.DELETE_CONTACT, "user"));
        this.content.put(Value.Password, CommonUtils.getConfigWithStringValue(this, AntiLostCommon.DELETE_CONTACT, "password", ""));
        this.content.put("UID", CommonMethod.getUID(this));
        new Thread(new Runnable() {
            public void run() {
                ServerRestoreDoing.this.contactService.request(ServerRestoreDoing.this.mHandler, ServerRestoreDoing.this.content, Value.Command_DownloadFile);
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public void clickCancel() {
        if (mServerRestoreDoing != null) {
            mServerRestoreDoing.finish();
            mServerRestoreDoing = null;
        }
        if (this.mProgTextDialog != null) {
            this.mProgTextDialog.dismiss();
        }
        if (this.mProgressDlg != null) {
            this.mProgressDlg.dismiss();
        }
        finish();
    }

    private void importContact(String contactPath) {
        Message msg = new Message();
        msg.what = 12;
        msg.arg1 = 27;
        this.mProgTextDialog.getTitleTextView().setText((int) R.string.text_importing_contact2);
        File file = new File(contactPath);
        if (!file.exists() || !file.canRead()) {
            msg.obj = getString(R.string.text_contact_not_exist, new Object[]{contactPath});
            this.mHandler.sendMessage(msg);
            return;
        }
        final String networkFilePath = String.valueOf(contactPath) + "_final";
        String result = CommonMethod.decryAndUnzipFile(contactPath, networkFilePath);
        if (!TextUtils.isEmpty(result)) {
            msg.obj = result;
            this.mHandler.sendMessage(msg);
            return;
        }
        runOnUiThread(new Runnable() {
            public void run() {
                CommonMethod.sendUserMessage(ServerRestoreDoing.this.mHandler, 7);
                CommonMethod.sendUserMessage(ServerRestoreDoing.this.mHandler, 9, 0);
            }
        });
        this.mHandler.post(new Runnable() {
            public void run() {
                ServerRestoreDoing.this.mVCardReadThread = new VCardReadThread(networkFilePath, ServerRestoreDoing.this, ServerRestoreDoing.this.mHandler, ServerRestoreDoing.this.mProgTextDialog);
                ServerRestoreDoing.this.mVCardReadThread.start();
            }
        });
    }

    /* access modifiers changed from: private */
    public void viewContact() {
        finish();
        try {
            startActivity(new Intent("android.intent.action.VIEW", ContactData.CONTENT_URI));
        } catch (ActivityNotFoundException e) {
            Toast toast = Toast.makeText(this, e.getLocalizedMessage(), 0);
            toast.setGravity(81, 0, 100);
            toast.show();
        }
    }

    /* access modifiers changed from: private */
    public void clickSucc() {
        finish();
        if (mServerRestoreDoing != null) {
            mServerRestoreDoing.finish();
            mServerRestoreDoing = null;
        }
    }
}
