package com.netqin.antivirus.contact;

import android.content.Context;
import android.os.Handler;
import android.widget.ProgressBar;
import com.netqin.antivirus.common.ProgressTextDialog;
import com.netqin.antivirus.contact.vcard.ContactStruct;
import com.netqin.antivirus.contact.vcard.EntryHandler;

public class ProgressShower implements EntryHandler {
    private Context mContext = null;
    private Handler mHandler = null;
    /* access modifiers changed from: private */
    public ProgressTextDialog mProgressDialog = null;

    private class ShowProgressRunnable implements Runnable {
        private ContactStruct mContact;

        public ShowProgressRunnable(ContactStruct contact) {
            this.mContact = contact;
        }

        public void run() {
            if (ProgressShower.this.mProgressDialog != null) {
                ProgressBar progBar = ProgressShower.this.mProgressDialog.getProgressBar();
                progBar.incrementProgressBy(1);
                ProgressShower.this.mProgressDialog.getRightTextView().setText(String.valueOf(progBar.getProgress()) + "/" + progBar.getMax());
            }
        }
    }

    public ProgressShower(ProgressTextDialog progDlg, Context context, Handler handler) {
        this.mContext = context;
        this.mHandler = handler;
        this.mProgressDialog = progDlg;
    }

    public void onParsingStart() {
    }

    public void onEntryCreated(ContactStruct contactStruct) {
        if (this.mProgressDialog != null && this.mHandler != null) {
            this.mHandler.post(new ShowProgressRunnable(contactStruct));
        }
    }

    public void onParsingEnd() {
    }
}
