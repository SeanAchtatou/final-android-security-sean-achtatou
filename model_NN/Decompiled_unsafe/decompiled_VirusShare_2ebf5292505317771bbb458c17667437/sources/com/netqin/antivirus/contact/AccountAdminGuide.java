package com.netqin.antivirus.contact;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.netqin.antivirus.antilost.AntiLostCommon;
import com.netqin.antivirus.common.CommonMethod;
import com.nqmobile.antivirus_ampro20.R;

public class AccountAdminGuide extends Activity implements AdapterView.OnItemClickListener {
    public static Activity mAccountAdminGuide;
    private boolean isFromAAG = false;
    /* access modifiers changed from: private */
    public int[] listIcon = null;
    private final int[] listIcon1 = {R.drawable.contacts_login_passport, R.drawable.contacts_create_passport};
    private final int[] listIcon2 = {R.drawable.contacts_modifi_password, R.drawable.contacts_exit_passport, R.drawable.contacts_switch_passport, R.drawable.contacts_create_passport};
    /* access modifiers changed from: private */
    public int[] listTitle = null;
    private final int[] listTitle1 = {R.string.label_login_backup_account, R.string.label_create_backup_account};
    private final int[] listTitle2 = {R.string.label_change_password, R.string.text_logoff, R.string.text_swtich_backup_account, R.string.label_create_backup_account};
    private GuideListAdapter mListAdapter;
    private TextView mTitle;
    private String mUserName = "";

    private class ListViewHolder {
        ImageView icon;
        TextView title;

        private ListViewHolder() {
        }

        /* synthetic */ ListViewHolder(AccountAdminGuide accountAdminGuide, ListViewHolder listViewHolder) {
            this();
        }
    }

    private class GuideListAdapter extends BaseAdapter {
        private Context mContext;
        private LayoutInflater mInflater = LayoutInflater.from(this.mContext);

        public GuideListAdapter(Context context) {
            this.mContext = context;
        }

        public int getCount() {
            return AccountAdminGuide.this.listTitle.length;
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ListViewHolder holder;
            if (convertView == null) {
                convertView = this.mInflater.inflate((int) R.layout.account_admin_guide_item, (ViewGroup) null);
                holder = new ListViewHolder(AccountAdminGuide.this, null);
                holder.icon = (ImageView) convertView.findViewById(R.id.account_admin_guide_item_icon);
                holder.title = (TextView) convertView.findViewById(R.id.account_admin_guide_item_title);
                convertView.setTag(holder);
            } else {
                holder = (ListViewHolder) convertView.getTag();
            }
            holder.icon.setImageResource(AccountAdminGuide.this.listIcon[position]);
            holder.title.setText(AccountAdminGuide.this.listTitle[position]);
            convertView.setId(AccountAdminGuide.this.listTitle[position]);
            return convertView;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.account_admin_guide);
        this.mTitle = (TextView) findViewById(R.id.activity_name);
        this.mTitle.setText((int) R.string.label_account_admin);
        mAccountAdminGuide = this;
        this.listTitle = this.listTitle1;
        this.listIcon = this.listIcon1;
        this.mListAdapter = new GuideListAdapter(this);
        ListView listview = (ListView) findViewById(R.id.account_admin_guide_listview);
        listview.setAdapter((ListAdapter) this.mListAdapter);
        listview.setOnItemClickListener(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (TextUtils.isEmpty(ContactCommon.mContactAccount)) {
            this.listIcon = this.listIcon1;
            this.listTitle = this.listTitle1;
        } else {
            this.listIcon = this.listIcon2;
            this.listTitle = this.listTitle2;
        }
        this.mUserName = CommonMethod.getConfigWithStringValue(this, AntiLostCommon.DELETE_CONTACT, "user", "");
        if (this.mListAdapter != null) {
            this.mListAdapter.notifyDataSetChanged();
        }
    }

    public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
        switch (v.getId()) {
            case R.string.label_create_backup_account /*2131428109*/:
                clickCreateAccount();
                return;
            case R.string.label_login_backup_account /*2131428110*/:
                clickLoginAccount();
                return;
            case R.string.label_change_password /*2131428111*/:
                clickChangePassword();
                return;
            case R.string.text_swtich_backup_account /*2131428121*/:
                clickSwitchAccount();
                return;
            case R.string.text_logoff /*2131428122*/:
                clickLogoffAccount();
                return;
            default:
                return;
        }
    }

    private void clickLogoffAccount() {
        ContactCommon.mContactAccount = "";
        CommonMethod.putConfigWithBooleanValue(this, AntiLostCommon.DELETE_CONTACT, "user_state", false);
        Toast toast = Toast.makeText(this, (int) R.string.text_succ_logoff, 0);
        toast.setGravity(81, 0, 100);
        toast.show();
        finish();
    }

    private void clickLoginAccount() {
        this.isFromAAG = true;
        Intent intent = new Intent(this, ContactAccountLogin.class);
        intent.putExtra("isFromAAG", this.isFromAAG);
        startActivity(intent);
    }

    private void clickSwitchAccount() {
        startActivity(new Intent(this, ContactAccountSwitch.class));
    }

    private void clickCreateAccount() {
        startActivity(new Intent(this, ContactAccountCreate.class));
    }

    private void clickChangePassword() {
        if (!TextUtils.isEmpty(this.mUserName)) {
            startActivity(new Intent(this, ContactAccountChange.class));
        }
    }
}
