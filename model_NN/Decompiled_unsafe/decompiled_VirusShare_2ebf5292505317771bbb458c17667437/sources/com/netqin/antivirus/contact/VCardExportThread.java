package com.netqin.antivirus.contact;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.PowerManager;
import com.netqin.antivirus.common.ProgressTextDialog;

public class VCardExportThread extends Thread implements DialogInterface.OnCancelListener {
    private static final String LOG_TAG = "netqin";
    private static final String LOG_TAG_1 = "vcardexportthread_run";
    private static final int RUN_STATE_BEGIN = 0;
    private static final int RUN_STATE_CANCEL = 1;
    private static final int RUN_STATE_END = 2;
    private static final int RUN_STATE_ERROR = 3;
    private boolean mCanceled = false;
    private Context mContext = null;
    private String mExportFilePath;
    private Handler mHandler = null;
    /* access modifiers changed from: private */
    public int mProgressCurr = 0;
    /* access modifiers changed from: private */
    public ProgressTextDialog mProgressTextDialog = null;
    private int mRunState = 0;
    private PowerManager.WakeLock mWakeLock;

    public VCardExportThread(String filePath, Context cnt, Handler handler, ProgressTextDialog prog) {
        this.mExportFilePath = filePath;
        this.mContext = cnt;
        this.mHandler = handler;
        this.mProgressTextDialog = prog;
        this.mProgressCurr = 0;
        init();
    }

    private void init() {
        this.mWakeLock = ((PowerManager) this.mContext.getSystemService("power")).newWakeLock(536870918, LOG_TAG);
    }

    public void finalize() {
        if (this.mWakeLock != null && this.mWakeLock.isHeld()) {
            this.mWakeLock.release();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.netqin.antivirus.contact.vcard.VCardComposer.<init>(android.content.Context, java.lang.String, boolean):void
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.netqin.antivirus.contact.vcard.VCardComposer.<init>(android.content.Context, int, boolean):void
      com.netqin.antivirus.contact.vcard.VCardComposer.<init>(android.content.Context, java.lang.String, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x03cf  */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x03d7  */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x03df  */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x03e7  */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x038f  */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x03bb  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r23 = this;
            long r16 = java.lang.System.currentTimeMillis()
            r0 = r23
            android.os.PowerManager$WakeLock r0 = r0.mWakeLock
            r18 = r0
            r18.acquire()
            r18 = 0
            r0 = r18
            r1 = r23
            r1.mRunState = r0
            r4 = 0
            r18 = 0
            r0 = r18
            r1 = r23
            r1.mProgressCurr = r0
            java.lang.String r9 = ""
            com.netqin.antivirus.contact.vcard.VCardComposer.initCurrentDisplayName()
            r0 = r23
            android.os.Handler r0 = r0.mHandler
            r18 = r0
            r19 = 6
            com.netqin.antivirus.common.CommonMethod.sendUserMessage(r18, r19)
            r11 = 0
            java.io.FileOutputStream r12 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x00bb }
            r0 = r23
            java.lang.String r0 = r0.mExportFilePath     // Catch:{ FileNotFoundException -> 0x00bb }
            r18 = r0
            r0 = r12
            r1 = r18
            r0.<init>(r1)     // Catch:{ FileNotFoundException -> 0x00bb }
            com.netqin.antivirus.contact.vcard.VCardComposer r5 = new com.netqin.antivirus.contact.vcard.VCardComposer     // Catch:{ all -> 0x0417 }
            r0 = r23
            android.content.Context r0 = r0.mContext     // Catch:{ all -> 0x0417 }
            r18 = r0
            java.lang.String r19 = "default"
            r20 = 1
            r0 = r5
            r1 = r18
            r2 = r19
            r3 = r20
            r0.<init>(r1, r2, r3)     // Catch:{ all -> 0x0417 }
            com.netqin.antivirus.contact.vcard.VCardComposer$HandlerForOutputStream r18 = new com.netqin.antivirus.contact.vcard.VCardComposer$HandlerForOutputStream     // Catch:{ all -> 0x041b }
            r5.getClass()     // Catch:{ all -> 0x041b }
            r0 = r18
            r1 = r5
            r2 = r12
            r0.<init>(r2)     // Catch:{ all -> 0x041b }
            r0 = r5
            r1 = r18
            r0.addHandler(r1)     // Catch:{ all -> 0x041b }
            boolean r18 = r5.init()     // Catch:{ all -> 0x041b }
            if (r18 != 0) goto L_0x016f
            java.lang.String r9 = r5.getErrorReason()     // Catch:{ all -> 0x041b }
            r18 = 3
            r0 = r18
            r1 = r23
            r1.mRunState = r0     // Catch:{ all -> 0x041b }
            if (r5 == 0) goto L_0x007c
            r5.terminate()
        L_0x007c:
            r0 = r23
            android.os.PowerManager$WakeLock r0 = r0.mWakeLock
            r18 = r0
            r18.release()
            android.os.Message r10 = new android.os.Message
            r10.<init>()
            r18 = 12
            r0 = r18
            r1 = r10
            r1.what = r0
            r0 = r23
            int r0 = r0.mProgressCurr
            r18 = r0
            r0 = r18
            r1 = r10
            r1.arg2 = r0
            r0 = r23
            int r0 = r0.mRunState
            r18 = r0
            switch(r18) {
                case 0: goto L_0x0149;
                case 1: goto L_0x0152;
                case 2: goto L_0x015b;
                case 3: goto L_0x0164;
                default: goto L_0x00a5;
            }
        L_0x00a5:
            r18 = 21
            r0 = r18
            r1 = r10
            r1.arg1 = r0
        L_0x00ac:
            r0 = r23
            android.os.Handler r0 = r0.mHandler
            r18 = r0
            r0 = r18
            r1 = r10
            r0.sendMessage(r1)
            r11 = r12
            r4 = r5
        L_0x00ba:
            return
        L_0x00bb:
            r18 = move-exception
            r8 = r18
            r0 = r23
            android.content.Context r0 = r0.mContext     // Catch:{ all -> 0x038c }
            r18 = r0
            r19 = 2131427534(0x7f0b00ce, float:1.8476687E38)
            r20 = 1
            r0 = r20
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ all -> 0x038c }
            r20 = r0
            r21 = 0
            r0 = r23
            java.lang.String r0 = r0.mExportFilePath     // Catch:{ all -> 0x038c }
            r22 = r0
            r20[r21] = r22     // Catch:{ all -> 0x038c }
            java.lang.String r9 = r18.getString(r19, r20)     // Catch:{ all -> 0x038c }
            r18 = 3
            r0 = r18
            r1 = r23
            r1.mRunState = r0     // Catch:{ all -> 0x038c }
            if (r4 == 0) goto L_0x00ea
            r4.terminate()
        L_0x00ea:
            r0 = r23
            android.os.PowerManager$WakeLock r0 = r0.mWakeLock
            r18 = r0
            r18.release()
            android.os.Message r10 = new android.os.Message
            r10.<init>()
            r18 = 12
            r0 = r18
            r1 = r10
            r1.what = r0
            r0 = r23
            int r0 = r0.mProgressCurr
            r18 = r0
            r0 = r18
            r1 = r10
            r1.arg2 = r0
            r0 = r23
            int r0 = r0.mRunState
            r18 = r0
            switch(r18) {
                case 0: goto L_0x0127;
                case 1: goto L_0x012f;
                case 2: goto L_0x0137;
                case 3: goto L_0x013f;
                default: goto L_0x0113;
            }
        L_0x0113:
            r18 = 21
            r0 = r18
            r1 = r10
            r1.arg1 = r0
        L_0x011a:
            r0 = r23
            android.os.Handler r0 = r0.mHandler
            r18 = r0
            r0 = r18
            r1 = r10
            r0.sendMessage(r1)
            goto L_0x00ba
        L_0x0127:
            r18 = 21
            r0 = r18
            r1 = r10
            r1.arg1 = r0
            goto L_0x011a
        L_0x012f:
            r18 = 25
            r0 = r18
            r1 = r10
            r1.arg1 = r0
            goto L_0x011a
        L_0x0137:
            r18 = 30
            r0 = r18
            r1 = r10
            r1.arg1 = r0
            goto L_0x011a
        L_0x013f:
            r18 = 34
            r0 = r18
            r1 = r10
            r1.arg1 = r0
            r10.obj = r9
            goto L_0x011a
        L_0x0149:
            r18 = 21
            r0 = r18
            r1 = r10
            r1.arg1 = r0
            goto L_0x00ac
        L_0x0152:
            r18 = 25
            r0 = r18
            r1 = r10
            r1.arg1 = r0
            goto L_0x00ac
        L_0x015b:
            r18 = 30
            r0 = r18
            r1 = r10
            r1.arg1 = r0
            goto L_0x00ac
        L_0x0164:
            r18 = 34
            r0 = r18
            r1 = r10
            r1.arg1 = r0
            r10.obj = r9
            goto L_0x00ac
        L_0x016f:
            int r15 = r5.getCount()     // Catch:{ all -> 0x041b }
            if (r15 > 0) goto L_0x01f1
            r0 = r23
            android.content.Context r0 = r0.mContext     // Catch:{ all -> 0x041b }
            r18 = r0
            r19 = 2131427535(0x7f0b00cf, float:1.847669E38)
            java.lang.String r9 = r18.getString(r19)     // Catch:{ all -> 0x041b }
            r18 = 3
            r0 = r18
            r1 = r23
            r1.mRunState = r0     // Catch:{ all -> 0x041b }
            if (r5 == 0) goto L_0x018f
            r5.terminate()
        L_0x018f:
            r0 = r23
            android.os.PowerManager$WakeLock r0 = r0.mWakeLock
            r18 = r0
            r18.release()
            android.os.Message r10 = new android.os.Message
            r10.<init>()
            r18 = 12
            r0 = r18
            r1 = r10
            r1.what = r0
            r0 = r23
            int r0 = r0.mProgressCurr
            r18 = r0
            r0 = r18
            r1 = r10
            r1.arg2 = r0
            r0 = r23
            int r0 = r0.mRunState
            r18 = r0
            switch(r18) {
                case 0: goto L_0x01cf;
                case 1: goto L_0x01d7;
                case 2: goto L_0x01df;
                case 3: goto L_0x01e7;
                default: goto L_0x01b8;
            }
        L_0x01b8:
            r18 = 21
            r0 = r18
            r1 = r10
            r1.arg1 = r0
        L_0x01bf:
            r0 = r23
            android.os.Handler r0 = r0.mHandler
            r18 = r0
            r0 = r18
            r1 = r10
            r0.sendMessage(r1)
            r11 = r12
            r4 = r5
            goto L_0x00ba
        L_0x01cf:
            r18 = 21
            r0 = r18
            r1 = r10
            r1.arg1 = r0
            goto L_0x01bf
        L_0x01d7:
            r18 = 25
            r0 = r18
            r1 = r10
            r1.arg1 = r0
            goto L_0x01bf
        L_0x01df:
            r18 = 30
            r0 = r18
            r1 = r10
            r1.arg1 = r0
            goto L_0x01bf
        L_0x01e7:
            r18 = 34
            r0 = r18
            r1 = r10
            r1.arg1 = r0
            r10.obj = r9
            goto L_0x01bf
        L_0x01f1:
            r0 = r23
            android.os.Handler r0 = r0.mHandler     // Catch:{ all -> 0x041b }
            r18 = r0
            r19 = 8
            r0 = r18
            r1 = r19
            r2 = r15
            com.netqin.antivirus.common.CommonMethod.sendUserMessage(r0, r1, r2)     // Catch:{ all -> 0x041b }
            long r13 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x041b }
        L_0x0205:
            boolean r18 = r5.isAfterLast()     // Catch:{ all -> 0x041b }
            if (r18 == 0) goto L_0x0258
            r18 = 2
            r0 = r18
            r1 = r23
            r1.mRunState = r0     // Catch:{ all -> 0x041b }
            if (r5 == 0) goto L_0x0218
            r5.terminate()
        L_0x0218:
            r0 = r23
            android.os.PowerManager$WakeLock r0 = r0.mWakeLock
            r18 = r0
            r18.release()
            android.os.Message r10 = new android.os.Message
            r10.<init>()
            r18 = 12
            r0 = r18
            r1 = r10
            r1.what = r0
            r0 = r23
            int r0 = r0.mProgressCurr
            r18 = r0
            r0 = r18
            r1 = r10
            r1.arg2 = r0
            r0 = r23
            int r0 = r0.mRunState
            r18 = r0
            switch(r18) {
                case 0: goto L_0x03f1;
                case 1: goto L_0x03fa;
                case 2: goto L_0x0403;
                case 3: goto L_0x040c;
                default: goto L_0x0241;
            }
        L_0x0241:
            r18 = 21
            r0 = r18
            r1 = r10
            r1.arg1 = r0
        L_0x0248:
            r0 = r23
            android.os.Handler r0 = r0.mHandler
            r18 = r0
            r0 = r18
            r1 = r10
            r0.sendMessage(r1)
            r11 = r12
            r4 = r5
            goto L_0x00ba
        L_0x0258:
            r0 = r23
            boolean r0 = r0.mCanceled     // Catch:{ all -> 0x041b }
            r18 = r0
            if (r18 == 0) goto L_0x02cf
            r18 = 1
            r0 = r18
            r1 = r23
            r1.mRunState = r0     // Catch:{ all -> 0x041b }
            if (r5 == 0) goto L_0x026d
            r5.terminate()
        L_0x026d:
            r0 = r23
            android.os.PowerManager$WakeLock r0 = r0.mWakeLock
            r18 = r0
            r18.release()
            android.os.Message r10 = new android.os.Message
            r10.<init>()
            r18 = 12
            r0 = r18
            r1 = r10
            r1.what = r0
            r0 = r23
            int r0 = r0.mProgressCurr
            r18 = r0
            r0 = r18
            r1 = r10
            r1.arg2 = r0
            r0 = r23
            int r0 = r0.mRunState
            r18 = r0
            switch(r18) {
                case 0: goto L_0x02ad;
                case 1: goto L_0x02b5;
                case 2: goto L_0x02bd;
                case 3: goto L_0x02c5;
                default: goto L_0x0296;
            }
        L_0x0296:
            r18 = 21
            r0 = r18
            r1 = r10
            r1.arg1 = r0
        L_0x029d:
            r0 = r23
            android.os.Handler r0 = r0.mHandler
            r18 = r0
            r0 = r18
            r1 = r10
            r0.sendMessage(r1)
            r11 = r12
            r4 = r5
            goto L_0x00ba
        L_0x02ad:
            r18 = 21
            r0 = r18
            r1 = r10
            r1.arg1 = r0
            goto L_0x029d
        L_0x02b5:
            r18 = 25
            r0 = r18
            r1 = r10
            r1.arg1 = r0
            goto L_0x029d
        L_0x02bd:
            r18 = 30
            r0 = r18
            r1 = r10
            r1.arg1 = r0
            goto L_0x029d
        L_0x02c5:
            r18 = 34
            r0 = r18
            r1 = r10
            r1.arg1 = r0
            r10.obj = r9
            goto L_0x029d
        L_0x02cf:
            boolean r18 = r5.createOneEntry()     // Catch:{ all -> 0x041b }
            if (r18 != 0) goto L_0x0348
            java.lang.String r9 = r5.getErrorReason()     // Catch:{ all -> 0x041b }
            r18 = 3
            r0 = r18
            r1 = r23
            r1.mRunState = r0     // Catch:{ all -> 0x041b }
            if (r5 == 0) goto L_0x02e6
            r5.terminate()
        L_0x02e6:
            r0 = r23
            android.os.PowerManager$WakeLock r0 = r0.mWakeLock
            r18 = r0
            r18.release()
            android.os.Message r10 = new android.os.Message
            r10.<init>()
            r18 = 12
            r0 = r18
            r1 = r10
            r1.what = r0
            r0 = r23
            int r0 = r0.mProgressCurr
            r18 = r0
            r0 = r18
            r1 = r10
            r1.arg2 = r0
            r0 = r23
            int r0 = r0.mRunState
            r18 = r0
            switch(r18) {
                case 0: goto L_0x0326;
                case 1: goto L_0x032e;
                case 2: goto L_0x0336;
                case 3: goto L_0x033e;
                default: goto L_0x030f;
            }
        L_0x030f:
            r18 = 21
            r0 = r18
            r1 = r10
            r1.arg1 = r0
        L_0x0316:
            r0 = r23
            android.os.Handler r0 = r0.mHandler
            r18 = r0
            r0 = r18
            r1 = r10
            r0.sendMessage(r1)
            r11 = r12
            r4 = r5
            goto L_0x00ba
        L_0x0326:
            r18 = 21
            r0 = r18
            r1 = r10
            r1.arg1 = r0
            goto L_0x0316
        L_0x032e:
            r18 = 25
            r0 = r18
            r1 = r10
            r1.arg1 = r0
            goto L_0x0316
        L_0x0336:
            r18 = 30
            r0 = r18
            r1 = r10
            r1.arg1 = r0
            goto L_0x0316
        L_0x033e:
            r18 = 34
            r0 = r18
            r1 = r10
            r1.arg1 = r0
            r10.obj = r9
            goto L_0x0316
        L_0x0348:
            r0 = r23
            int r0 = r0.mProgressCurr     // Catch:{ all -> 0x041b }
            r18 = r0
            int r18 = r18 + 1
            r0 = r18
            r1 = r23
            r1.mProgressCurr = r0     // Catch:{ all -> 0x041b }
            r0 = r23
            android.os.Handler r0 = r0.mHandler     // Catch:{ all -> 0x041b }
            r18 = r0
            r19 = 9
            r0 = r23
            int r0 = r0.mProgressCurr     // Catch:{ all -> 0x041b }
            r20 = r0
            com.netqin.antivirus.common.CommonMethod.sendUserMessage(r18, r19, r20)     // Catch:{ all -> 0x041b }
            r0 = r23
            com.netqin.antivirus.common.ProgressTextDialog r0 = r0.mProgressTextDialog     // Catch:{ all -> 0x041b }
            r18 = r0
            if (r18 == 0) goto L_0x0382
            r0 = r23
            android.os.Handler r0 = r0.mHandler     // Catch:{ all -> 0x041b }
            r18 = r0
            com.netqin.antivirus.contact.VCardExportThread$1 r19 = new com.netqin.antivirus.contact.VCardExportThread$1     // Catch:{ all -> 0x041b }
            r0 = r19
            r1 = r23
            r2 = r15
            r0.<init>(r2)     // Catch:{ all -> 0x041b }
            r18.post(r19)     // Catch:{ all -> 0x041b }
        L_0x0382:
            long r6 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x041b }
            long r13 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x041b }
            goto L_0x0205
        L_0x038c:
            r18 = move-exception
        L_0x038d:
            if (r4 == 0) goto L_0x0392
            r4.terminate()
        L_0x0392:
            r0 = r23
            android.os.PowerManager$WakeLock r0 = r0.mWakeLock
            r19 = r0
            r19.release()
            android.os.Message r10 = new android.os.Message
            r10.<init>()
            r19 = 12
            r0 = r19
            r1 = r10
            r1.what = r0
            r0 = r23
            int r0 = r0.mProgressCurr
            r19 = r0
            r0 = r19
            r1 = r10
            r1.arg2 = r0
            r0 = r23
            int r0 = r0.mRunState
            r19 = r0
            switch(r19) {
                case 0: goto L_0x03cf;
                case 1: goto L_0x03d7;
                case 2: goto L_0x03df;
                case 3: goto L_0x03e7;
                default: goto L_0x03bb;
            }
        L_0x03bb:
            r19 = 21
            r0 = r19
            r1 = r10
            r1.arg1 = r0
        L_0x03c2:
            r0 = r23
            android.os.Handler r0 = r0.mHandler
            r19 = r0
            r0 = r19
            r1 = r10
            r0.sendMessage(r1)
            throw r18
        L_0x03cf:
            r19 = 21
            r0 = r19
            r1 = r10
            r1.arg1 = r0
            goto L_0x03c2
        L_0x03d7:
            r19 = 25
            r0 = r19
            r1 = r10
            r1.arg1 = r0
            goto L_0x03c2
        L_0x03df:
            r19 = 30
            r0 = r19
            r1 = r10
            r1.arg1 = r0
            goto L_0x03c2
        L_0x03e7:
            r19 = 34
            r0 = r19
            r1 = r10
            r1.arg1 = r0
            r10.obj = r9
            goto L_0x03c2
        L_0x03f1:
            r18 = 21
            r0 = r18
            r1 = r10
            r1.arg1 = r0
            goto L_0x0248
        L_0x03fa:
            r18 = 25
            r0 = r18
            r1 = r10
            r1.arg1 = r0
            goto L_0x0248
        L_0x0403:
            r18 = 30
            r0 = r18
            r1 = r10
            r1.arg1 = r0
            goto L_0x0248
        L_0x040c:
            r18 = 34
            r0 = r18
            r1 = r10
            r1.arg1 = r0
            r10.obj = r9
            goto L_0x0248
        L_0x0417:
            r18 = move-exception
            r11 = r12
            goto L_0x038d
        L_0x041b:
            r18 = move-exception
            r11 = r12
            r4 = r5
            goto L_0x038d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.netqin.antivirus.contact.VCardExportThread.run():void");
    }

    public void cancel() {
        if (this.mRunState != 2) {
            this.mCanceled = true;
            this.mRunState = 1;
        }
    }

    public void onCancel(DialogInterface dialog) {
        cancel();
    }
}
