package com.netqin.antivirus.contact;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.netqin.antivirus.NqUtil;
import com.netqin.antivirus.antilost.AntiLostCommon;
import com.netqin.antivirus.antilost.SmsHandler;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.contact.vcard.ContactData;
import com.nqmobile.antivirus_ampro20.R;
import java.io.File;

public class ContactGuide extends Activity implements AdapterView.OnItemClickListener {
    public static boolean isBackupFromContactGuide = false;
    public static boolean isRestoreFromContactGuide = false;
    private TextView contactsBackupNum;
    private TextView contactsMobileNum;
    /* access modifiers changed from: private */
    public int[] contacts_guide_icon = {R.drawable.backup_contact, R.drawable.restore_contact, R.drawable.account_admin};
    /* access modifiers changed from: private */
    public int[] contacts_guide_title = {R.string.contacts_guide_backup, R.string.contacts_guide_restore, R.string.contacts_guide_passport};
    private TextView lastTimeBackup;
    private LinearLayout layout4;
    private ListView listView;
    private GestureDetector mGDINFO = null;
    private GuideListAdapter mListAdapter;
    private TextView mTitle;
    private TextView passportState;

    private class ListViewHolder {
        ImageView icon;
        TextView title;

        private ListViewHolder() {
        }

        /* synthetic */ ListViewHolder(ContactGuide contactGuide, ListViewHolder listViewHolder) {
            this();
        }
    }

    private class GuideListAdapter extends BaseAdapter {
        private Context mContext;
        private LayoutInflater mInflater = LayoutInflater.from(this.mContext);

        public GuideListAdapter(Context context) {
            this.mContext = context;
        }

        public int getCount() {
            return ContactGuide.this.contacts_guide_title.length;
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) ContactGuide.this.contacts_guide_title[position];
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ListViewHolder holder;
            if (convertView == null) {
                convertView = this.mInflater.inflate((int) R.layout.contacts_guide_item, (ViewGroup) null);
                holder = new ListViewHolder(ContactGuide.this, null);
                holder.icon = (ImageView) convertView.findViewById(R.id.contacts_guide_list_icon);
                holder.title = (TextView) convertView.findViewById(R.id.contacts_guide_list_title);
                convertView.setTag(holder);
            } else {
                holder = (ListViewHolder) convertView.getTag();
            }
            convertView.setId(ContactGuide.this.contacts_guide_title[position]);
            holder.icon.setImageResource(ContactGuide.this.contacts_guide_icon[position]);
            holder.title.setText(ContactGuide.this.contacts_guide_title[position]);
            return convertView;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.contacts_guide);
        this.mTitle = (TextView) findViewById(R.id.activity_name);
        this.mTitle.setText((int) R.string.text_contact_backuprestore);
        this.listView = (ListView) findViewById(R.id.contacts_guide_listview);
        this.mListAdapter = new GuideListAdapter(this);
        this.listView.setAdapter((ListAdapter) this.mListAdapter);
        this.listView.setOnItemClickListener(this);
        this.passportState = (TextView) findViewById(R.id.contacts_passport_state);
        this.contactsMobileNum = (TextView) findViewById(R.id.contacts_number);
        this.lastTimeBackup = (TextView) findViewById(R.id.contacts_last_time);
        this.contactsBackupNum = (TextView) findViewById(R.id.contacts_number_backup);
        this.layout4 = (LinearLayout) findViewById(R.id.contacts_guide_layout4);
        this.mGDINFO = new GestureDetector(new GestureDetectorEx(this, null));
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        SharedPreferences spf = getSharedPreferences(AntiLostCommon.DELETE_CONTACT, 0);
        String contactsNetwork = spf.getString("contacts_network", "0");
        String contactBackupTime = spf.getString("contacts_backup_time", "0");
        if (TextUtils.isEmpty(ContactCommon.mContactAccount)) {
            this.passportState.setText((int) R.string.contacts_guide_passport_log_off);
        } else {
            this.passportState.setText(ContactCommon.mContactAccount);
        }
        String[] sContactsProjection = {SmsHandler.ROWID};
        Cursor cursor = getContentResolver().query(ContactData.CONTENT_URI, sContactsProjection, null, null, null);
        if (cursor == null) {
            cursor = getContentResolver().query(Uri.parse("content://contacts/people"), sContactsProjection, null, null, null);
        }
        this.contactsMobileNum.setText(" 0");
        if (cursor != null) {
            cursor.moveToFirst();
            Integer count = Integer.valueOf(cursor.getCount());
            if (count.intValue() > 0) {
                this.contactsMobileNum.setText(" " + count.toString());
            }
        }
        if (contactsNetwork.equals("0")) {
            this.contactsBackupNum.setText(" 0");
        } else {
            this.contactsBackupNum.setText(" " + contactsNetwork);
        }
        if (contactsNetwork.equals("0")) {
            this.layout4.setVisibility(8);
        } else {
            this.lastTimeBackup.setText(" " + contactBackupTime.split(" ")[0]);
            this.layout4.setVisibility(0);
        }
        if (this.mListAdapter != null) {
            this.mListAdapter.notifyDataSetChanged();
        }
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        switch (view.getId()) {
            case R.string.contacts_guide_backup /*2131428087*/:
                clickBackupToServer();
                return;
            case R.string.contacts_guide_restore /*2131428088*/:
                clickRestoreFromServer();
                return;
            case R.string.contacts_guide_passport /*2131428089*/:
                clickAccountAdmin();
                return;
            default:
                return;
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.backup_to_sdcard /*2131558865*/:
                clickBackupToCard();
                break;
            case R.id.restore_from_sdcard /*2131558866*/:
                cilckRestoreFromCard();
                break;
            case R.id.privacy_promise /*2131558867*/:
                clickPrivacyPromise();
                break;
            case R.id.advice_feedback /*2131558868*/:
                NqUtil.clickAdviceFeedback(this);
                break;
            default:
                return false;
        }
        return true;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.contacts_menu, menu);
        return true;
    }

    public boolean onTouchEvent(MotionEvent event) {
        this.mGDINFO.onTouchEvent(event);
        return false;
    }

    private void clickBackupToServer() {
        if (TextUtils.isEmpty(ContactCommon.mContactAccount)) {
            isBackupFromContactGuide = true;
            Intent intent = new Intent(this, ContactAccountLogin.class);
            intent.putExtra("session", 1);
            startActivity(intent);
            return;
        }
        ServerBackupDoing.mFromContactGuide = true;
        startActivity(new Intent(this, ServerBackupDoing.class));
    }

    private void clickRestoreFromServer() {
        if (TextUtils.isEmpty(ContactCommon.mContactAccount)) {
            isRestoreFromContactGuide = true;
            Intent intent = new Intent(this, ContactAccountLogin.class);
            intent.putExtra("session", 2);
            startActivity(intent);
            return;
        }
        ServerRestoreDoing.mFromContactGuide = true;
        startActivity(new Intent(this, ServerRestoreDoing.class));
    }

    private void clickPrivacyPromise() {
        Uri uri;
        if (CommonMethod.isLocalSimpleChinese()) {
            uri = Uri.parse(getString(R.string.text_privacy_promise_website));
        } else {
            uri = Uri.parse(String.valueOf(getString(R.string.text_privacy_promise_website)) + "?l=en_us");
        }
        startActivity(new Intent("android.intent.action.VIEW", uri));
    }

    private void clickAccountAdmin() {
        startActivity(new Intent(this, AccountAdminGuide.class));
    }

    private void cilckRestoreFromCard() {
        File fileSdcard = new File("/sdcard");
        if (!fileSdcard.exists() || !fileSdcard.isDirectory() || !fileSdcard.canRead()) {
            CommonMethod.messageDialog(this, (int) R.string.text_because_nosdcard, (int) R.string.label_netqin_antivirus);
        } else if (!TextUtils.isEmpty(getSharedPreferences(AntiLostCommon.DELETE_CONTACT, 0).getString("bpfile_card", "")) || !TextUtils.isEmpty(ContactCommon.getSDCardFilePath(this))) {
            CardRestoreDoing.mFromContactRestoreGuide = true;
            startActivity(new Intent(this, CardRestoreDoing.class));
        } else {
            CommonMethod.messageDialog(this, (int) R.string.text_not_backup_to_card_tip, (int) R.string.label_netqin_antivirus);
        }
    }

    private void clickBackupToCard() {
        File fileSdcard = new File("/sdcard");
        if (!fileSdcard.exists() || !fileSdcard.isDirectory() || !fileSdcard.canRead()) {
            CommonMethod.messageDialog(this, (int) R.string.text_because_nosdcard, (int) R.string.label_netqin_antivirus);
            return;
        }
        CardBackupDoing.mFromContactBackupGuide = true;
        startActivity(new Intent(this, CardBackupDoing.class));
    }

    private class GestureDetectorEx extends GestureDetector.SimpleOnGestureListener {
        private static final int SWIPE_MAX_OFF_PATH = 250;
        private static final int SWIPE_MIN_DISTANCE = 120;
        private static final int SWIPE_THRESHOLD_VELOCITY = 200;

        private GestureDetectorEx() {
        }

        /* synthetic */ GestureDetectorEx(ContactGuide contactGuide, GestureDetectorEx gestureDetectorEx) {
            this();
        }

        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            try {
                if (Math.abs(e1.getY() - e2.getY()) > 250.0f) {
                    return false;
                }
                if (e2.getX() - e1.getX() > 120.0f && Math.abs(velocityX) > 200.0f) {
                    ContactCommon.informationProcess(ContactGuide.this);
                }
                return false;
            } catch (Exception e) {
            }
        }
    }
}
