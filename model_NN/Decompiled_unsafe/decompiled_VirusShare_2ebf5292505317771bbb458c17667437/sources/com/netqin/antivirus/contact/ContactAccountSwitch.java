package com.netqin.antivirus.contact;

import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.netqin.antivirus.SHA1Util;
import com.netqin.antivirus.Value;
import com.netqin.antivirus.antilost.AntiLostCommon;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.common.CommonUtils;
import com.netqin.antivirus.common.ProgDlgActivity;
import com.netqin.antivirus.net.accountservice.AccountService;
import com.nqmobile.antivirus_ampro20.R;

public class ContactAccountSwitch extends ProgDlgActivity implements TextWatcher {
    /* access modifiers changed from: private */
    public AccountService accountService;
    /* access modifiers changed from: private */
    public ContentValues content;
    private Button mBtnOk;
    private EditText mETAccount;
    private EditText mETPassword;
    private TextView mTitle;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.contact_account_switch);
        this.mActivityVisible = true;
        this.accountService = AccountService.getInstance(this);
        this.content = new ContentValues();
        this.mTitle = (TextView) findViewById(R.id.activity_name);
        this.mTitle.setText((int) R.string.text_swtich_backup_account);
        this.mETAccount = (EditText) findViewById(R.id.contacts_account_switch_user);
        this.mETPassword = (EditText) findViewById(R.id.contacts_account_switch_pwd);
        this.mETAccount.addTextChangedListener(this);
        this.mETPassword.addTextChangedListener(this);
        this.mBtnOk = (Button) findViewById(R.id.contact_account_switch_ok);
        this.mBtnOk.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ContactAccountSwitch.this.clickBtnOk();
            }
        });
        ((Button) findViewById(R.id.contact_account_switch_cancel)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ContactAccountSwitch.this.clickBtnCancel();
            }
        });
        findViewById(R.id.contact_account_switch_create).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ContactAccountSwitch.this.clickCreateBackupAccount();
            }
        });
        findViewById(R.id.contact_account_switch_forget).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ContactAccountSwitch.this.clickFindBackupAccount();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.mActivityVisible = false;
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        this.mActivityVisible = true;
        super.onResume();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.contact_account_switch, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.contact_account_switch_menuitem_create /*2131558862*/:
                clickCreateBackupAccount();
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: private */
    public void clickCreateBackupAccount() {
        startActivity(new Intent(this, ContactAccountCreate.class));
    }

    /* access modifiers changed from: private */
    public void clickBtnOk() {
        final String account = this.mETAccount.getText().toString().trim();
        final String password = this.mETPassword.getText().toString().trim();
        this.mProgressText = (String) getText(R.string.text_account_switching);
        this.mProgressDlg = CommonMethod.progressDlgCreate(this, this.mProgressText, this.mHandler);
        CommonMethod.sendUserMessage(this.mHandler, 1);
        this.content.put(Value.Username, account);
        this.content.put(Value.Password, SHA1Util.hex_sha1(password));
        this.content.put("UID", CommonMethod.getUID(this));
        new Thread(new Runnable() {
            public void run() {
                int result = ContactAccountSwitch.this.accountService.request(null, ContactAccountSwitch.this.content, 2);
                final String accountInfo = ContactCommon.getAccountInfo(ContactAccountSwitch.this, account);
                CommonMethod.sendUserMessage(ContactAccountSwitch.this.mHandler, 4);
                if (!ContactAccountSwitch.this.mActivityVisible) {
                    return;
                }
                if (result == 8 || result == 16) {
                    ContactAccountSwitch.this.handleMSG(R.string.SEND_RECEIVE_ERROR);
                } else if (ContactAccountSwitch.this.content.containsKey(Value.Code)) {
                    String code = ContactAccountSwitch.this.content.getAsString(Value.Code);
                    if (code.equals("0")) {
                        Handler access$6 = ContactAccountSwitch.this.mHandler;
                        final String str = account;
                        final String str2 = password;
                        access$6.post(new Runnable() {
                            public void run() {
                                Toast toast = Toast.makeText(ContactAccountSwitch.this, (int) R.string.text_succ_login, 0);
                                toast.setGravity(81, 0, 100);
                                toast.show();
                                CommonMethod.putConfigWithStringValue(ContactAccountSwitch.this, AntiLostCommon.DELETE_CONTACT, "user", str);
                                CommonUtils.putConfigWithStringValue(ContactAccountSwitch.this, AntiLostCommon.DELETE_CONTACT, "password", str2);
                                ContactCommon.mContactAccount = str;
                                ContactAccountSwitch.this.finish();
                                if (AccountAdminGuide.mAccountAdminGuide != null) {
                                    AccountAdminGuide.mAccountAdminGuide.finish();
                                    AccountAdminGuide.mAccountAdminGuide = null;
                                }
                                if (accountInfo.equals("-1") || accountInfo.equals("-2") || accountInfo.equals("-3") || TextUtils.isEmpty(accountInfo)) {
                                    CommonMethod.putConfigWithStringValue(ContactAccountSwitch.this, AntiLostCommon.DELETE_CONTACT, "contacts_network", "0");
                                    CommonMethod.putConfigWithStringValue(ContactAccountSwitch.this, AntiLostCommon.DELETE_CONTACT, "contacts_backup_time", "0");
                                    return;
                                }
                                String backupPeople = accountInfo.split("#")[1];
                                String backupTime = accountInfo.split("T")[0];
                                CommonMethod.putConfigWithStringValue(ContactAccountSwitch.this, AntiLostCommon.DELETE_CONTACT, "contacts_network", backupPeople);
                                CommonMethod.putConfigWithStringValue(ContactAccountSwitch.this, AntiLostCommon.DELETE_CONTACT, "contacts_backup_time", backupTime);
                            }
                        });
                    } else if (code.equals("-1")) {
                        ContactAccountSwitch.this.handleMSG(R.string.SYSTEM_ERROR);
                    } else if (code.equals("-2")) {
                        ContactAccountSwitch.this.handleMSG(R.string.USERNAME_NOT_EXIST);
                    } else if (code.equals("-3")) {
                        ContactAccountSwitch.this.handleMSG(R.string.PASSWORD_ERROR);
                    }
                }
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public void handleMSG(final int msgID) {
        this.mHandler.post(new Runnable() {
            public void run() {
                CommonMethod.messageDialog(ContactAccountSwitch.this, msgID, (int) R.string.label_netqin_antivirus);
            }
        });
    }

    /* access modifiers changed from: private */
    public void clickBtnCancel() {
        finish();
    }

    /* access modifiers changed from: private */
    public void clickFindBackupAccount() {
        String uri = "http://o.netqin.com";
        if (!CommonMethod.isLocalSimpleChinese()) {
            switch (0) {
                case 0:
                    uri = "http://www.netqin.com/uc/goResetPassAndroid.html?l=en_us";
                    break;
                case 1:
                    uri = "http://vm.netqin.cn:4080/uc/goResetPassAndroid.html?l=en_us";
                    break;
                case 2:
                    uri = "http://www.netqin.com/uc/goResetPassAndroid.html?l=en_us";
                    break;
            }
        } else {
            switch (0) {
                case 0:
                    uri = "http://www.netqin.com/uc/goResetPassAndroid.html?l=zh_cn";
                    break;
                case 1:
                    uri = "http://vm.netqin.cn:4080/uc/goResetPassAndroid.html?l=zh_cn";
                    break;
                case 2:
                    uri = "http://www.netqin.com/uc/goResetPassAndroid.html?l=zh_cn";
                    break;
            }
        }
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(uri)));
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {
        String account = this.mETAccount.getText().toString().trim();
        String password = this.mETPassword.getText().toString().trim();
        if (TextUtils.isEmpty(account) || TextUtils.isEmpty(password)) {
            this.mBtnOk.setEnabled(false);
        } else {
            this.mBtnOk.setEnabled(true);
        }
    }

    public void afterTextChanged(Editable s) {
    }
}
