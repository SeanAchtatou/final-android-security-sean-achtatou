package com.netqin.antivirus.appprotocol;

import android.content.Context;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.payment.Payment;
import com.netqin.antivirus.payment.PaymentHandler;
import com.netqin.antivirus.payment.PaymentIntent;

public class AppStartPayment {
    private AppValue appValue;
    private Context context;
    private PaymentHandler paymentHandler;

    public AppStartPayment(Context context2, PaymentHandler handler, AppValue value) {
        this.context = context2;
        this.paymentHandler = handler;
        this.appValue = value;
    }

    public boolean startChargeRequest() {
        switch (this.appValue.operationType) {
            case 11:
            case 21:
            case 31:
            case 41:
                return initSmsChargePara();
            case 12:
            case 22:
            case 32:
            case 42:
            case 61:
            case 62:
            case 63:
                return initWapChargePara();
            case 51:
            case 82:
                return initWebChargePara();
            case 81:
                return initZongSdkChargePara();
            default:
                return false;
        }
    }

    private boolean initSmsChargePara() {
        CommonMethod.logDebug("AVService", "initSmsChargePara()");
        PaymentIntent paymentIntent = PaymentIntent.getInstance(this.appValue.operationType, this.context);
        paymentIntent.setPaymentPrompt(this.appValue.chargePrompt);
        paymentIntent.setPaymentReConfirmPrompt(this.appValue.chargeReconfirmPrompt);
        paymentIntent.setSMSNeedReConfirm(this.appValue.smsChargeIsNeedReConfirm);
        paymentIntent.setSMSNumber(this.appValue.smsChargeSendNumber);
        paymentIntent.setSMSReceiveReconfirmVisible(this.appValue.smsChargeIsReceiveReconfirmVisible);
        paymentIntent.setSMSReconfirmContent(this.appValue.smsChargeReconfirmContent);
        paymentIntent.setSMSReconfirmMatch(this.appValue.smsChargeReconfirmMatch);
        paymentIntent.setSMSReconfirmNumber(this.appValue.smsChargeReconfirmNumber);
        if (this.appValue.smsChargeReconfirmWaitTime > 0) {
            paymentIntent.setSMSReconfirmWaitTime(this.appValue.smsChargeReconfirmWaitTime);
        } else {
            paymentIntent.setSMSReconfirmWaitTime(90);
        }
        paymentIntent.setSMSSendContent(this.appValue.smsChargeSendContent);
        paymentIntent.setSMSSendCount(this.appValue.smsChargeSendCount);
        paymentIntent.setSMSSendReconfirmVisible(this.appValue.smsChargeIsSendReconfirmVisible);
        paymentIntent.setSMSSendVisible(this.appValue.smsChargeIsSendVisible);
        return Payment.startPayment(this.context, this.paymentHandler, paymentIntent);
    }

    private boolean initWapChargePara() {
        CommonMethod.logDebug("AVService", "initWapChargePara()");
        PaymentIntent paymentIntent = PaymentIntent.getInstance(this.appValue.operationType, this.context);
        paymentIntent.setPaymentPrompt(this.appValue.chargePrompt);
        paymentIntent.setPaymentReConfirmPrompt(this.appValue.chargeReconfirmPrompt);
        paymentIntent.setWAPClickVisible(this.appValue.wapChargeIsClickVisible);
        paymentIntent.setWAPConfirmMatch(this.appValue.wapChargeConfirmMatch);
        paymentIntent.setWAPCount(this.appValue.wapChargeCount);
        paymentIntent.setWAPInternal(this.appValue.wapChargeInternal);
        paymentIntent.setWAPMsgVisible(this.appValue.wapChargeIsMsgVisible);
        paymentIntent.setWAPPageIndex(this.appValue.wapChargePageIndex);
        paymentIntent.setWAPPostData(this.appValue.wapChargePostData);
        paymentIntent.setWAPRequestMethod(this.appValue.wapChargeType);
        paymentIntent.setWAPRule(this.appValue.wapChargeRule);
        paymentIntent.setWAPSuccessSign(this.appValue.wapChargeSuccessSign);
        paymentIntent.setWAPURL(this.appValue.wapChargeUrl);
        return Payment.startPayment(this.context, this.paymentHandler, paymentIntent);
    }

    private boolean initZongSdkChargePara() {
        CommonMethod.logDebug("AVService", "initZongSdkChargePara()");
        if (this.appValue.zongSDKChargePricePoint == null || this.appValue.zongSDKChargePricePoint.size() <= 0) {
            return false;
        }
        PaymentIntent paymentIntent = PaymentIntent.getInstance(this.appValue.operationType, this.context);
        paymentIntent.setPaymentPrompt(this.appValue.chargePrompt);
        paymentIntent.setPaymentReConfirmPrompt(this.appValue.chargeReconfirmPrompt);
        paymentIntent.setZongAppName(this.appValue.zongSDKChargeAppName);
        paymentIntent.setZongCountry(this.appValue.zongSDKChargeCountry);
        paymentIntent.setZongCurrency(this.appValue.zongSDKChargeCurrency);
        paymentIntent.setZongCustomerKey(this.appValue.zongSDKChargeCustomerKey);
        paymentIntent.setZongPricePoint(this.appValue.zongSDKChargePricePoint);
        paymentIntent.setZongTransactionRef(this.appValue.zongSDKChargeTransactionRef);
        paymentIntent.setZongUrl(this.appValue.zongSDKChargePaymentUrl);
        return Payment.startPayment(this.context, this.paymentHandler, paymentIntent);
    }

    private boolean initWebChargePara() {
        CommonMethod.logDebug("AVService", "initWebChargePara()");
        PaymentIntent paymentIntent = PaymentIntent.getInstance(this.appValue.operationType, this.context);
        paymentIntent.setPaymentPrompt(this.appValue.chargePrompt);
        paymentIntent.setPaymentReConfirmPrompt(this.appValue.chargeReconfirmPrompt);
        paymentIntent.setPaymentCentreUrl(this.appValue.paymentCentreUrl);
        return Payment.startPayment(this.context, this.paymentHandler, paymentIntent);
    }
}
