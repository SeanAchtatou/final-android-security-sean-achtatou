package com.netqin.antivirus.appprotocol;

import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.os.Message;
import android.text.TextUtils;
import com.netqin.antivirus.NqUtil;
import com.netqin.antivirus.Preferences;
import com.netqin.antivirus.Value;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.common.Zlib;
import com.netqin.antivirus.net.avservice.AVService;
import com.netqin.antivirus.payment.PaymentHandler;
import com.netqin.antivirus.scan.FileUtils;
import com.netqin.antivirus.scan.ScanActivity;
import com.netqin.antivirus.scan.ScanController;
import com.netqin.antivirus.virusdbupdate.UpdateDbActivity;
import com.nqmobile.antivirus_ampro20.R;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

public class AppService extends Service {
    private static final String TAG = "AVService";
    /* access modifiers changed from: private */
    public AppValue appValue = null;
    private AVService avService;
    /* access modifiers changed from: private */
    public int commandId;
    private int connectResult;
    private boolean isBackground;
    PaymentHandler paymenthandler = new PaymentHandler() {
        /* access modifiers changed from: protected */
        public void handleMessage(int status, Message msg) {
            CommonMethod.logDebug(AppService.TAG, "AppService recharge result " + status);
            if (status == 207) {
                AppService.this.appValue.paymentResult = 0;
                AppService.this.commandId = AppService.this.appValue.nextStepCmdNo;
            } else {
                AppService.this.appValue.paymentResult = status;
                AppService.this.commandId = AppService.this.appValue.nextStepCmdYes;
            }
            if (AppService.this.commandId > 0) {
                AppService.this.startTimer();
            }
        }
    };
    private int retryTimes = 3;
    private String targetAVDBVersion;
    private Timer timer = null;

    public IBinder onBind(Intent arg0) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            return 1;
        }
        if (intent.getBooleanExtra("cancel", false)) {
            cancelRequest();
        } else {
            this.timer = new Timer();
            this.appValue = new AppValue();
            this.commandId = intent.getIntExtra("commandid", 0);
            this.isBackground = intent.getBooleanExtra("isBackground", false);
            if (this.commandId == 12) {
                this.targetAVDBVersion = intent.getStringExtra("targetAVDBVersion");
            }
            this.avService = new AVService(this);
            new Thread(new Runnable() {
                public void run() {
                    AppService.this.threadFuncForRequest();
                }
            }).start();
        }
        return 1;
    }

    /* access modifiers changed from: private */
    public void threadFuncForRequest() {
        ContentValues content = new ContentValues();
        this.appValue.requestCommandId = 0;
        this.appValue.responseCommandId = 0;
        this.appValue.operationType = 0;
        NetworkInfo ni = ((ConnectivityManager) getSystemService("connectivity")).getActiveNetworkInfo();
        String apnStr = "";
        if (ni != null) {
            String apnStr2 = ni.getTypeName();
            switch (ni.getType()) {
                case 1:
                    apnStr = "wifi";
                    break;
                default:
                    apnStr = NqUtil.getCurrentApnName(this);
                    break;
            }
        }
        content.put(Value.APN, apnStr);
        content.put("IMEI", CommonMethod.getIMEI(this));
        content.put("UID", CommonMethod.getUID(this));
        content.put("IMSI", CommonMethod.getIMSI(this));
        content.put(XmlTagValue.isBackground, Boolean.valueOf(this.isBackground));
        content.put(XmlTagValue.localVirusVersion, new Preferences(this).getVirusDBVersion());
        if (this.commandId == 12 && !TextUtils.isEmpty(this.targetAVDBVersion)) {
            content.put(XmlTagValue.targetVirusVersion, this.targetAVDBVersion);
        }
        this.connectResult = this.avService.request(this.commandId, null, content, this.appValue);
        CommonMethod.logDebug(TAG, new StringBuilder(String.valueOf(this.connectResult)).toString());
        if (this.connectResult == 10) {
            if (!this.appValue.isDataIgnore) {
                CommonMethod.saveUserAVInfo(this, content);
            }
            CommonMethod.saveNextConnectTime(this, content, this.appValue);
            if ((this.appValue.responseCommandId == 16 || this.appValue.responseCommandId == 1) && content.containsKey(XmlTagValue.notification)) {
                String s = content.getAsString(XmlTagValue.notification);
                String e = content.getAsString(XmlTagValue.event);
                boolean isMember = CommonMethod.getIsMember(this);
                if (!TextUtils.isEmpty(s) && e.equals("1") && !isMember) {
                    CommonMethod.logDebug(TAG, s);
                    Intent intent = new Intent();
                    intent.setClass(this, UpdateDbActivity.class);
                    intent.putExtra("showdialog", s);
                    CommonMethod.showNotificationManualClear(this, intent, getString(R.string.home_db_expire), getString(R.string.notification_desc_updatedbnow), R.drawable.icon_notemsg);
                }
            }
            if (this.appValue.responseCommandId == 22) {
                if (this.appValue.dialogBoxList != null) {
                    CommonMethod.saveSubscribeDialogInfo(this, content, this.appValue);
                }
                if (!TextUtils.isEmpty(this.appValue.newsBoxContent) && !TextUtils.isEmpty(this.appValue.newsBoxTitle) && !TextUtils.isEmpty(this.appValue.newsBoxPosition) && !this.appValue.newsBoxPosition.equals("40")) {
                    CommonMethod.showNotificationDialog(this, this.appValue.newsBoxTitle, this.appValue.newsBoxContent, R.drawable.icon_notemsg, Value.NOTIFICATION_DIALOG_ID0);
                }
            }
            if (content.containsKey(Value.AVDBUpdateSuccess) && content.getAsString(Value.AVDBUpdateSuccess).equals("1")) {
                String depressPath = FileUtils.updateVirusDbFilePath(this);
                String path = getFilesDir() + "/" + this.appValue.downloadVirusName;
                try {
                    Zlib.ZipDecompress(path, depressPath);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                FileUtils.DeleteFile(path);
                String newVer = this.appValue.downloadVirusVersion;
                Preferences preferences = new Preferences(this);
                if (!newVer.matches(preferences.getVirusDBVersion())) {
                    if (ScanActivity.mScanController == null) {
                        ScanController.updateVirusDB(this, depressPath, newVer);
                    } else if (!ScanController.isScaning) {
                        ScanController.updateVirusDB(this, depressPath, newVer);
                    } else {
                        preferences.setNewVirusDBPath(depressPath);
                        preferences.setNewVirusDBVersion(newVer);
                    }
                }
                CommonMethod.closeNotification(this, Value.NOTIFICATIONMANUALCLEAR_ID);
            }
            if (this.appValue.responseCommandId == 19 && !TextUtils.isEmpty(this.appValue.purchasedVirusVersion) && this.appValue.purchasedVirusVersion.compareTo(new Preferences(this).getVirusDBVersion()) > 0) {
                this.targetAVDBVersion = this.appValue.purchasedVirusVersion;
                this.commandId = 12;
                threadFuncForRequest();
            }
        } else {
            if (this.retryTimes > 0) {
                this.retryTimes = this.retryTimes - 1;
                startTimer();
            }
            Calendar calendar = Calendar.getInstance();
            calendar.add(6, 1);
            if (this.appValue.requestCommandId == 16) {
                CommonMethod.setLastDialyCheckTimeToCurrentTime(this);
                CommonMethod.setNextDialyCheckTime(this, calendar);
            } else if (this.appValue.requestCommandId == 22) {
                CommonMethod.setLastFreedataCheckTimeToCurrentTime(this);
                CommonMethod.setNextFreedataCheckTime(this, calendar);
            }
        }
        continueDoRequest(this.appValue.responseCommandId);
        stopSelf();
    }

    private void cancelRequest() {
        if (this.avService != null) {
            this.avService.cancel();
        }
    }

    private void continueDoRequest(int command) {
        if (this.appValue.operationType <= 0) {
            switch (command) {
                case 11:
                    if (CommonMethod.getIsMember(this)) {
                        this.targetAVDBVersion = this.appValue.latestVirusVersion;
                        this.commandId = 12;
                        threadFuncForRequest();
                        return;
                    }
                    return;
                case 12:
                    this.commandId = 13;
                    threadFuncForRequest();
                    return;
                case 13:
                case 14:
                case 15:
                default:
                    return;
                case 16:
                    boolean isMember = CommonMethod.getIsMember(this);
                    CommonMethod.logDebug(TAG, "AppService::handleMessage isMember: " + isMember);
                    if (isMember && this.appValue.latestVirusVersion.compareTo(new Preferences(this).getVirusDBVersion()) > 0) {
                        this.commandId = 11;
                        threadFuncForRequest();
                        return;
                    }
                    return;
            }
        } else if (!new AppStartPayment(this, this.paymenthandler, this.appValue).startChargeRequest()) {
            CommonMethod.logDebug(TAG, "startChargeRequest failed" + this.appValue.operationType);
        }
    }

    /* access modifiers changed from: private */
    public void startTimer() {
        this.timer.schedule(new TimerTask() {
            public void run() {
                CommonMethod.logDebug(AppService.TAG, "AppService retry timeout");
                if (AppService.this.commandId > 0) {
                    AppService.this.threadFuncForRequest();
                } else {
                    AppService.this.stopSelf();
                }
            }
        }, 60000);
    }
}
