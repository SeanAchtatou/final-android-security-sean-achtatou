package com.netqin.antivirus.appprotocol;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Toast;
import com.netqin.antivirus.NqUtil;
import com.netqin.antivirus.Preferences;
import com.netqin.antivirus.Value;
import com.netqin.antivirus.cloud.model.CloudHandler;
import com.netqin.antivirus.common.CommonMethod;
import com.netqin.antivirus.common.ProgDlgActivity;
import com.netqin.antivirus.common.Zlib;
import com.netqin.antivirus.net.avservice.AVService;
import com.netqin.antivirus.payment.PaymentHandler;
import com.netqin.antivirus.scan.FileUtils;
import com.netqin.antivirus.scan.ScanActivity;
import com.netqin.antivirus.scan.ScanController;
import com.netqin.antivirus.util.MimeUtils;
import com.nqmobile.antivirus_ampro20.R;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class AppActivity extends ProgDlgActivity {
    private static final String TAG = "AppActivity";
    /* access modifiers changed from: private */
    public boolean activityActive = false;
    private int appCommandId;
    /* access modifiers changed from: private */
    public AppValue appValue = null;
    private AVService avService = null;
    private boolean balanceChanged = false;
    private int balanceChangedNum = Value.PREF_CHANGE_MSG_VALUE;
    private int clientScene = 0;
    private int currentIndex = 0;
    private boolean isBackground = false;
    private boolean isSubscribe = false;
    private boolean isUpdateDB = false;
    private boolean memberChanged = false;
    private int messageSize = 0;
    DialogInterface.OnClickListener negativeCommonListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface arg0, int arg1) {
            AppActivity.this.finish();
        }
    };
    DialogInterface.OnClickListener negativeUpdateAVDBListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface arg0, int arg1) {
            if (TextUtils.isEmpty(AppActivity.this.appValue.purchasedVirusVersion) || TextUtils.isEmpty(AppActivity.this.appValue.latestVirusVersion)) {
                AppActivity.this.finish();
            } else if (AppActivity.this.appValue.purchasedVirusVersion.compareTo(new Preferences(AppActivity.this).getVirusDBVersion()) <= 0 || AppActivity.this.appValue.purchasedVirusVersion.compareTo(AppActivity.this.appValue.latestVirusVersion) >= 0) {
                AppActivity.this.finish();
            } else {
                AppActivity.this.targetAVDBVersion = AppActivity.this.appValue.purchasedVirusVersion;
                AppActivity.this.startRequest(12);
            }
        }
    };
    /* access modifiers changed from: private */
    public int nextCmdYes = 0;
    /* access modifiers changed from: private */
    public int nextStepCmdTryTimes = 3;
    PaymentHandler paymenthandler = new PaymentHandler() {
        /* access modifiers changed from: protected */
        public void handleMessage(int status, Message msg) {
            CommonMethod.logDebug("AVService", "AppActivity recharge result " + status);
            if (AppActivity.this.activityActive) {
                if (status == -1) {
                    AppActivity.this.appValue.paymentResult = 0;
                    AppActivity.this.appValue.isMandatory = true;
                    if (AppActivity.this.appValue.nextStepCmdNo > 0) {
                        AppActivity.this.startRequest(AppActivity.this.appValue.nextStepCmdNo);
                    } else if (TextUtils.isEmpty(AppActivity.this.appValue.purchasedVirusVersion) || AppActivity.this.appValue.purchasedVirusVersion.compareTo(new Preferences(AppActivity.this).getVirusDBVersion()) <= 0 || AppActivity.this.appValue.responseCommandId != 11) {
                        AppActivity.this.showToast(R.string.general_operation_cancel);
                        AppActivity.this.finish();
                    } else {
                        AppActivity.this.targetAVDBVersion = AppActivity.this.appValue.purchasedVirusVersion;
                        AppActivity.this.startRequest(12);
                    }
                } else if (status == 207) {
                    AppActivity.this.showToast(R.string.general_operation_cancel);
                    AppActivity.this.finish();
                } else {
                    AppActivity.this.appValue.paymentResult = status;
                    if (AppActivity.this.appValue.nextStepCmdYes > 0) {
                        AppActivity.this.nextCmdYes = AppActivity.this.appValue.nextStepCmdYes;
                        AppActivity.this.nextStepCmdTryTimes = AppActivity.this.appValue.nextStepCmdYesTryTimes - 1;
                        AppActivity.this.createWaitingDialog(AppActivity.this.getString(R.string.connect_waiting_charge_effect), AppActivity.this.getString(R.string.label_netqin_antivirus));
                        AppActivity.this.startTimer();
                        return;
                    }
                    AppActivity.this.finish();
                }
            }
        }
    };
    DialogInterface.OnClickListener positiveSubscribeListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface arg0, int arg1) {
            AppActivity.this.startRequest(9);
        }
    };
    DialogInterface.OnClickListener positiveUnsubscribeListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface arg0, int arg1) {
            AppActivity.this.startRequest(10);
        }
    };
    DialogInterface.OnClickListener positiveUpdateAVDBListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface arg0, int arg1) {
            AppActivity.this.targetAVDBVersion = AppActivity.this.appValue.latestVirusVersion;
            AppActivity.this.startRequest(12);
        }
    };
    private ProgressDialog progressDialog = null;
    /* access modifiers changed from: private */
    public RadioGroup radioGroup = null;
    /* access modifiers changed from: private */
    public String targetAVDBVersion = "";
    /* access modifiers changed from: private */
    public Timer timer = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.appactivity);
        setRequestedOrientation(1);
        this.activityActive = true;
        this.timer = new Timer();
        this.avService = new AVService(this);
        Intent intent = getIntent();
        int commandId = intent.getIntExtra("commandid", 0);
        this.isBackground = intent.getBooleanExtra("isBackground", false);
        this.clientScene = intent.getIntExtra("clientScene", 0);
        if (commandId == 9) {
            this.isSubscribe = true;
        } else if (commandId == 11) {
            this.isUpdateDB = true;
        }
        this.appValue = new AppValue();
        if (commandId > 1 && !showPromptBeforeRequest(commandId)) {
            startRequest(commandId);
        }
    }

    /* access modifiers changed from: private */
    public void startRequest(int commandId) {
        String str;
        if (!CloudHandler.checkNet(this)) {
            showToast((int) R.string.SEND_RECEIVE_ERROR);
            finish();
            return;
        }
        this.appCommandId = commandId;
        String title = getString(R.string.label_netqin_antivirus);
        if (commandId == 15) {
            str = getString(R.string.connect_waiting_charge_effect);
        } else if (commandId == 19) {
            str = getString(R.string.text_activating_software);
            title = getString(R.string.label_activate);
        } else {
            str = getString(R.string.text_process_wait);
        }
        createWaitingDialog(str, title);
        new Thread(new Runnable() {
            public void run() {
                AppActivity.this.threadFuncForRequest();
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public void threadFuncForRequest() {
        int newValue;
        int oldValue;
        ContentValues content = new ContentValues();
        this.appValue.requestCommandId = 0;
        this.appValue.responseCommandId = 0;
        this.appValue.operationType = 0;
        this.appValue.chargePrompt = "";
        this.appValue.chargeReconfirmPrompt = "";
        this.appValue.nextStepCmdYes = 0;
        this.appValue.nextStepCmdNo = 0;
        this.appValue.multiChargeDesc = "";
        if (this.appValue.multiChargeOptionList != null) {
            this.appValue.multiChargeOptionList.clear();
            this.appValue.multiChargeOptionList = null;
        }
        if (this.appValue.tempMsgList != null) {
            this.appValue.tempMsgList.clear();
            this.appValue.tempMsgList = null;
        }
        NetworkInfo ni = ((ConnectivityManager) getSystemService("connectivity")).getActiveNetworkInfo();
        String apnStr = "";
        if (ni != null) {
            String apnStr2 = ni.getTypeName();
            switch (ni.getType()) {
                case 1:
                    apnStr = "wifi";
                    break;
                default:
                    apnStr = NqUtil.getCurrentApnName(this);
                    break;
            }
        }
        CommonMethod.logDebug("AVService", "Current apn name: " + apnStr);
        content.put(Value.APN, apnStr);
        content.put("IMEI", CommonMethod.getIMEI(this));
        content.put("UID", CommonMethod.getUID(this));
        content.put("IMSI", CommonMethod.getIMSI(this));
        content.put(XmlTagValue.isBackground, Boolean.valueOf(this.isBackground));
        content.put(XmlTagValue.localVirusVersion, new Preferences(this).getVirusDBVersion());
        content.put(XmlTagValue.clientScene, Integer.valueOf(this.clientScene));
        if (this.appCommandId == 12 && !TextUtils.isEmpty(this.targetAVDBVersion)) {
            content.put(XmlTagValue.targetVirusVersion, this.targetAVDBVersion);
        }
        int connectResult = this.avService.request(this.appCommandId, this.mHandler, content, this.appValue);
        CommonMethod.logDebug("AVService", new StringBuilder(String.valueOf(connectResult)).toString());
        this.appValue.selectedCharge = "";
        if (connectResult == 10) {
            if (this.appValue.requestCommandId == 15) {
                if (content.containsKey(XmlTagValue.isMember)) {
                    if (content.getAsString(XmlTagValue.isMember).equalsIgnoreCase(CommonMethod.getIsMemberString(this))) {
                        this.memberChanged = false;
                    } else {
                        this.memberChanged = true;
                    }
                }
                if (content.containsKey("Balance")) {
                    String newBalance = content.getAsString("Balance");
                    String oldBalance = CommonMethod.getUserBalance(this);
                    if (newBalance.equals(oldBalance)) {
                        this.balanceChanged = false;
                    } else {
                        this.balanceChanged = true;
                        try {
                            newValue = Integer.parseInt(newBalance);
                            oldValue = Integer.parseInt(oldBalance);
                        } catch (Exception e) {
                            newValue = Value.PREF_CHANGE_MSG_VALUE;
                            oldValue = 0;
                        }
                        this.balanceChangedNum = newValue - oldValue;
                        if (this.balanceChangedNum < 0) {
                            this.balanceChangedNum = Value.PREF_CHANGE_MSG_VALUE;
                        }
                    }
                }
                CommonMethod.logDebug("AVService", "user info update finish");
            }
            if (!this.appValue.isDataIgnore) {
                CommonMethod.logDebug("AVService", "Need save user info");
                CommonMethod.saveUserAVInfo(this, content);
            }
            CommonMethod.saveNextConnectTime(this, content, this.appValue);
            if (content.containsKey(Value.AVDBUpdateSuccess) && content.getAsString(Value.AVDBUpdateSuccess).equals("1")) {
                CommonMethod.logDebug("AVService", "Update local virus db");
                String depressPath = FileUtils.updateVirusDbFilePath(this);
                String path = getFilesDir() + "/" + this.appValue.downloadVirusName;
                try {
                    Zlib.ZipDecompress(path, depressPath);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                FileUtils.DeleteFile(path);
                String newVer = this.appValue.downloadVirusVersion;
                Preferences preferences = new Preferences(this);
                if (!newVer.matches(preferences.getVirusDBVersion())) {
                    if (ScanActivity.mScanController == null) {
                        ScanController.updateVirusDB(this, depressPath, newVer);
                    } else if (!ScanController.isScaning) {
                        ScanController.updateVirusDB(this, depressPath, newVer);
                    } else {
                        preferences.setNewVirusDBPath(depressPath);
                        preferences.setNewVirusDBVersion(newVer);
                    }
                }
                CommonMethod.closeNotification(this, Value.NOTIFICATIONMANUALCLEAR_ID);
            }
            Message msg = new Message();
            msg.what = 13;
            this.mHandler.sendMessage(msg);
        } else if (this.appCommandId != 15) {
        } else {
            if (this.nextStepCmdTryTimes > 0) {
                this.nextStepCmdTryTimes = this.nextStepCmdTryTimes - 1;
                startTimer();
            } else if (this.isSubscribe) {
                CommonMethod.setIsMember(this, true);
                CommonMethod.setUserLevelname(this, getString(R.string.lable_member_user_type_member));
            }
        }
    }

    /* access modifiers changed from: private */
    public void cancelRequest() {
        if (this.avService != null) {
            this.avService.cancel();
        }
        finish();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.activityActive = false;
        this.timer.cancel();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return super.onKeyDown(keyCode, event);
    }

    /* access modifiers changed from: private */
    public void createWaitingDialog(String message, String title) {
        if (this.progressDialog == null) {
            this.progressDialog = new ProgressDialog(this);
            this.progressDialog.setTitle(title);
            this.progressDialog.setMessage(message);
            this.progressDialog.setProgressStyle(0);
            this.progressDialog.setButton(-2, getString(R.string.label_cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                    AppActivity.this.timer.cancel();
                    AppActivity.this.cancelRequest();
                    AppActivity.this.showToast(R.string.general_operation_cancel);
                    AppActivity.this.finish();
                }
            });
            this.progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface arg0) {
                    AppActivity.this.timer.cancel();
                    AppActivity.this.cancelRequest();
                    AppActivity.this.showToast(R.string.general_operation_cancel);
                    AppActivity.this.finish();
                }
            });
            this.progressDialog.setCancelable(true);
            this.progressDialog.show();
        }
    }

    private void createMessageDialog(String message) {
        WebView wv = new WebView(this);
        wv.getSettings().setDefaultTextEncodingName("utf-8");
        wv.setBackgroundColor(-1);
        wv.loadData("<div style=\"color: #000000\">" + message + "</div>", MimeUtils.MIME_TEXT_HTML, "utf-8");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.label_netqin_antivirus);
        builder.setView(wv);
        builder.setPositiveButton((int) R.string.label_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int i) {
                if (!AppActivity.this.displayAppMessage()) {
                    AppActivity.this.finish();
                }
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface arg0) {
                AppActivity.this.finish();
            }
        });
        builder.show();
    }

    private void createPromptDialog(String title, String message, String positiveStr, DialogInterface.OnClickListener positiveBtn, String negativeStr, DialogInterface.OnClickListener negativeBtn) {
        WebView wv = new WebView(this);
        wv.getSettings().setDefaultTextEncodingName("utf-8");
        wv.setBackgroundColor(-1);
        wv.loadData("<div style=\"color: #000000\">" + message + "</div>", MimeUtils.MIME_TEXT_HTML, "utf-8");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setView(wv);
        builder.setPositiveButton(positiveStr, positiveBtn);
        builder.setNegativeButton(negativeStr, negativeBtn);
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface arg0) {
                AppActivity.this.finish();
            }
        });
        builder.show();
    }

    /* access modifiers changed from: private */
    public boolean displayAppMessage() {
        if (this.appValue.messageList == null) {
            return false;
        }
        this.messageSize = this.appValue.messageList.size();
        if (this.currentIndex >= 0 && this.currentIndex < this.messageSize) {
            createMessageDialog(this.appValue.messageList.get(this.currentIndex).str);
        }
        if (this.currentIndex < 0 || this.currentIndex >= this.messageSize) {
            return false;
        }
        this.currentIndex++;
        return true;
    }

    private void displayMultiChargeSelectList() {
        new StringBuffer("0");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        String titleStr = "";
        if (!TextUtils.isEmpty(this.appValue.chargePrompt)) {
            titleStr = String.valueOf(titleStr) + this.appValue.chargePrompt;
        }
        if (!TextUtils.isEmpty(this.appValue.multiChargeDesc)) {
            titleStr = String.valueOf(titleStr) + this.appValue.multiChargeDesc;
        }
        if (TextUtils.isEmpty(titleStr)) {
            titleStr = getString(R.string.label_netqin_antivirus);
        }
        builder.setTitle(titleStr);
        builder.setIcon(17301659);
        if (this.appValue.multiChargeOptionList != null) {
            ScrollView sv = new ScrollView(this);
            this.radioGroup = new RadioGroup(this);
            for (int i = 0; i < this.appValue.multiChargeOptionList.size(); i++) {
                RadioButton newRadioButton = new RadioButton(this);
                newRadioButton.setText(this.appValue.multiChargeOptionList.get(i).desc);
                newRadioButton.setId(i);
                if (i % 2 != 0) {
                    newRadioButton.setBackgroundColor(-12303292);
                }
                if (i == 0) {
                    newRadioButton.setChecked(true);
                }
                LinearLayout.LayoutParams layoutParams = new RadioGroup.LayoutParams(-1, -2);
                layoutParams.bottomMargin = 2;
                layoutParams.topMargin = 2;
                this.radioGroup.addView(newRadioButton, this.radioGroup.getChildCount(), layoutParams);
            }
            sv.addView(this.radioGroup);
            builder.setView(sv);
        }
        builder.setPositiveButton((int) R.string.label_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                int index = -1;
                if (AppActivity.this.radioGroup != null) {
                    index = AppActivity.this.radioGroup.getCheckedRadioButtonId();
                    CommonMethod.logDebug("AVService", "radio index: " + index);
                }
                if (AppActivity.this.appValue.nextStepCmdYes <= 1 || index < 0 || index >= AppActivity.this.appValue.multiChargeOptionList.size()) {
                    AppActivity.this.showToast(R.string.general_operation_cancel);
                    AppActivity.this.finish();
                    return;
                }
                AppActivity.this.nextCmdYes = AppActivity.this.appValue.nextStepCmdYes;
                AppActivity.this.appValue.selectedCharge = AppActivity.this.appValue.multiChargeOptionList.get(index).id;
                AppActivity.this.startRequest(AppActivity.this.appValue.nextStepCmdYes);
            }
        });
        builder.setNegativeButton((int) R.string.label_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (AppActivity.this.appValue.nextStepCmdNo > 1) {
                    AppActivity.this.appValue.selectedCharge = "0";
                    AppActivity.this.startRequest(AppActivity.this.appValue.nextStepCmdNo);
                    return;
                }
                AppActivity.this.finish();
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface arg0) {
                AppActivity.this.finish();
            }
        });
        builder.show();
    }

    /* access modifiers changed from: private */
    public void showToast(int id) {
        showToast(getString(id));
    }

    private void showToast(String str) {
        Toast.makeText(this, str, 1).show();
    }

    /* access modifiers changed from: protected */
    public void processAppMsg(Message msg) {
        if (this.activityActive) {
            if (msg.arg1 == 27) {
                if (this.progressDialog != null) {
                    this.progressDialog.dismiss();
                    this.progressDialog = null;
                }
                showToast((String) msg.obj);
                finish();
                return;
            }
            doNext();
        }
    }

    private void doNext() {
        CommonMethod.logDebug("AVService", "doNext");
        if (this.activityActive) {
            if (this.appValue.requestCommandId == 15) {
                if (this.memberChanged || this.balanceChanged) {
                    if (!this.memberChanged && this.balanceChanged) {
                        if (this.isSubscribe) {
                            if (this.appValue.messageList != null) {
                                this.appValue.messageList.clear();
                                this.appValue.messageList = null;
                            }
                            if (this.appValue.tempMsgList == null || this.appValue.tempMsgList.size() <= 0) {
                                this.appValue.messageList = new ArrayList();
                                SysMessage sysMsg = new SysMessage();
                                sysMsg.str = getString(R.string.payment_service_unavailable);
                                this.appValue.messageList.add(sysMsg);
                            } else {
                                this.appValue.messageList = new ArrayList();
                                this.appValue.messageList.clear();
                                for (int i = 0; i < this.appValue.tempMsgList.size(); i++) {
                                    this.appValue.messageList.add(this.appValue.tempMsgList.get(i));
                                }
                                this.appValue.tempMsgList.clear();
                                this.appValue.tempMsgList = null;
                            }
                        } else if (this.isUpdateDB) {
                            startRequest(11);
                            return;
                        } else {
                            if (this.appValue.messageList != null) {
                                this.appValue.messageList.clear();
                                this.appValue.messageList = null;
                            }
                            if (this.appValue.tempMsgList != null) {
                                this.appValue.tempMsgList.clear();
                                this.appValue.tempMsgList = null;
                            }
                            this.appValue.messageList = new ArrayList();
                            SysMessage sysMsg2 = new SysMessage();
                            sysMsg2.str = getString(R.string.payment_service_chargeok, new Object[]{Integer.valueOf(this.balanceChangedNum)});
                            this.appValue.messageList.add(sysMsg2);
                        }
                    }
                } else if (this.nextStepCmdTryTimes > 0) {
                    this.nextStepCmdTryTimes--;
                    startTimer();
                    return;
                } else {
                    if (this.appValue.messageList != null) {
                        this.appValue.messageList.clear();
                        this.appValue.messageList = null;
                    }
                    if (this.appValue.tempMsgList == null) {
                        this.appValue.messageList = new ArrayList();
                        SysMessage sysMsg3 = new SysMessage();
                        sysMsg3.str = getString(R.string.payment_service_unavailable);
                        this.appValue.messageList.add(sysMsg3);
                    }
                }
            }
            if (this.appValue.tempMsgList != null) {
                if (this.appValue.messageList == null) {
                    this.appValue.messageList = new ArrayList();
                    for (int i2 = 0; i2 < this.appValue.tempMsgList.size(); i2++) {
                        this.appValue.messageList.add(this.appValue.tempMsgList.get(i2));
                    }
                } else {
                    this.appValue.messageList.clear();
                    this.appValue.messageList.addAll(this.appValue.tempMsgList);
                }
                this.appValue.tempMsgList.clear();
                this.appValue.tempMsgList = null;
            }
            if ((this.appValue.operationType != 12 || this.appValue.wapChargeIsMsgVisible) && ((this.appValue.operationType != 11 || this.appValue.smsChargeIsSendVisible || this.appValue.smsChargeIsReceiveReconfirmVisible || this.appValue.smsChargeIsSendReconfirmVisible) && this.progressDialog != null)) {
                this.progressDialog.dismiss();
                this.progressDialog = null;
            }
            boolean display = true;
            if (this.appValue.responseCommandId == 21 && this.appValue.operationType != 81) {
                displayMultiChargeSelectList();
            } else if (this.appValue.operationType > 0) {
                if (this.appValue.operationType == 31 || this.appValue.operationType == 32 || this.appValue.operationType == 62) {
                    this.isSubscribe = true;
                } else {
                    this.isSubscribe = false;
                }
                if (!new AppStartPayment(this, this.paymenthandler, this.appValue).startChargeRequest()) {
                    showToast((int) R.string.general_operation_cancel);
                    display = false;
                }
            } else if (this.appValue.responseCommandId == 11) {
                if (!TextUtils.isEmpty(this.appValue.chargePrompt)) {
                    createPromptDialog(getString(R.string.label_netqin_antivirus), this.appValue.chargePrompt, getString(R.string.label_yes), this.positiveUpdateAVDBListener, getString(R.string.label_no), this.negativeUpdateAVDBListener);
                } else if (this.appValue.isBalanceAdequate) {
                    this.targetAVDBVersion = this.appValue.latestVirusVersion;
                    startRequest(12);
                } else {
                    showToast((int) R.string.user_balance_insufficient);
                    display = false;
                }
            } else if (this.appValue.responseCommandId == 12) {
                startRequest(13);
            } else if (this.appValue.responseCommandId != 19) {
                this.currentIndex = 0;
                display = displayAppMessage();
            } else if (TextUtils.isEmpty(this.appValue.purchasedVirusVersion) || this.appValue.purchasedVirusVersion.compareTo(new Preferences(this).getVirusDBVersion()) <= 0) {
                this.currentIndex = 0;
                display = displayAppMessage();
            } else {
                this.targetAVDBVersion = this.appValue.purchasedVirusVersion;
                startRequest(12);
            }
            if (!display) {
                finish();
            }
        }
    }

    private boolean showPromptBeforeRequest(int command) {
        String str;
        switch (command) {
            case 9:
                if (this.clientScene == 0 || this.clientScene == 106 || this.clientScene == 111 || this.clientScene == 112) {
                    return false;
                }
                DialogBoxForSubscribe dbfs = CommonMethod.getSubscribeDialogInfo(this, 20);
                if (dbfs == null || this.clientScene != 102) {
                    createPromptDialog(getString(R.string.user_subscribe_title), getString(R.string.user_subscribe_desc), getString(R.string.user_subscribe_title), this.positiveSubscribeListener, getString(R.string.user_subscribe_later), this.negativeCommonListener);
                    return true;
                } else if (!dbfs.isShow) {
                    return false;
                } else {
                    if (!TextUtils.isEmpty(dbfs.content)) {
                        str = dbfs.content;
                    } else {
                        str = getString(R.string.user_subscribe_desc);
                    }
                    createPromptDialog(getString(R.string.user_subscribe_title), str, getString(R.string.user_subscribe_title), this.positiveSubscribeListener, getString(R.string.user_subscribe_later), this.negativeCommonListener);
                    return true;
                }
            case 10:
                createPromptDialog(getString(R.string.user_unsubscribe_title), getString(R.string.user_unsubscribe_desc), getString(R.string.label_ok), this.positiveUnsubscribeListener, getString(R.string.label_cancel), this.negativeCommonListener);
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: private */
    public void startTimer() {
        if (this.activityActive) {
            this.timer.schedule(new TimerTask() {
                public void run() {
                    CommonMethod.logDebug("AVService", "AppActivity retry timeout");
                    if (AppActivity.this.nextCmdYes > 0) {
                        AppActivity.this.startRequest(AppActivity.this.nextCmdYes);
                    } else {
                        AppActivity.this.finish();
                    }
                }
            }, 30000);
        }
    }
}
