package com.netqin.antivirus.appprotocol;

import android.content.Context;
import android.content.Intent;
import com.netqin.antivirus.common.CommonMethod;

public class AppRequest {
    private static final String TAG = "AppRequest";
    private static int clientScene = 0;
    private static boolean isBackground;

    public static void StartRegister(Context context, boolean background) {
        isBackground = background;
        doStart(context, 19);
    }

    public static void StartCheckAVDB(Context context, boolean background) {
        isBackground = background;
        doStart(context, 11);
    }

    public static void StartUninstallConnect(Context context, boolean background) {
        isBackground = background;
        doStart(context, 18);
    }

    public static void StartSubscribe(Context context, boolean background, int cs) {
        isBackground = background;
        clientScene = cs;
        doStart(context, 9);
    }

    public static void StartUnsubscribe(Context context, boolean background) {
        isBackground = background;
        doStart(context, 10);
    }

    public static void StartCharge(Context context, boolean background) {
        isBackground = background;
        doStart(context, 5);
    }

    public static void StartUpdateFreeData(Context context, boolean background) {
        isBackground = background;
        doStart(context, 22);
    }

    public static void StartPeriodicalConnect(Context context, boolean background) {
        isBackground = background;
        doStart(context, 16);
    }

    public static void StartUpdateUserInfo(Context context, boolean background) {
        isBackground = background;
        doStart(context, 15);
    }

    private static void doStart(Context context, int commandId) {
        Intent intent = new Intent();
        intent.putExtra("commandid", commandId);
        intent.putExtra("isBackground", isBackground);
        intent.putExtra("clientScene", clientScene);
        if (CommonMethod.isFirstRun(context)) {
            return;
        }
        if (isBackground) {
            intent.setClass(context, AppService.class);
            context.startService(intent);
            return;
        }
        intent.setClass(context, AppActivity.class);
        context.startActivity(intent);
    }
}
