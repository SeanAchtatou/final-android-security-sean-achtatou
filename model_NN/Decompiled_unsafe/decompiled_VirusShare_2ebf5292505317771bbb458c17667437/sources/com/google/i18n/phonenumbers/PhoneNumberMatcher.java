package com.google.i18n.phonenumbers;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

final class PhoneNumberMatcher implements Iterator<PhoneNumberMatch> {
    private static final Pattern INNER;
    private static final Pattern PATTERN;
    private static final Pattern PUB_PAGES = Pattern.compile("\\d{1,5}-+\\d{1,5}\\s{0,4}\\(\\d{1,4}");
    private static final Pattern SLASH_SEPARATED_DATES = Pattern.compile("(?:(?:[0-3]?\\d/[01]?\\d)|(?:[01]?\\d/[0-3]?\\d))/(?:[12]\\d)?\\d{2}");
    private PhoneNumberMatch lastMatch = null;
    private final PhoneNumberUtil.Leniency leniency;
    private long maxTries;
    private final String preferredRegion;
    private int searchIndex = 0;
    private State state = State.NOT_READY;
    private final CharSequence text;
    private final PhoneNumberUtil util;

    private enum State {
        NOT_READY,
        READY,
        DONE
    }

    static {
        String limit = limit(0, 2);
        String limit2 = limit(0, 4);
        String limit3 = limit(0, 18);
        String str = "[" + removeSpace("-x‐-―−ー－-／  ​⁠　()（）［］.\\[\\]/~⁓∼～") + "]" + limit2;
        String str2 = "[-x‐-―−ー－-／  ​⁠　()（）［］.\\[\\]/~⁓∼～]" + limit2;
        String str3 = "\\p{Nd}" + limit(1, 18);
        PATTERN = Pattern.compile("(?:" + "[(\\[+＋]" + str2 + ")" + limit + str3 + "(?:" + str2 + str3 + ")" + limit3 + "(?:" + PhoneNumberUtil.KNOWN_EXTN_PATTERNS + ")?", 66);
        INNER = Pattern.compile("[(\\[+＋]" + limit + str3 + "(?:" + str + str3 + ")" + limit3, 66);
    }

    PhoneNumberMatcher(PhoneNumberUtil phoneNumberUtil, CharSequence charSequence, String str, PhoneNumberUtil.Leniency leniency2, long j) {
        if (phoneNumberUtil == null || leniency2 == null) {
            throw new NullPointerException();
        } else if (j < 0) {
            throw new IllegalArgumentException();
        } else {
            this.util = phoneNumberUtil;
            this.text = charSequence != null ? charSequence : "";
            this.preferredRegion = str;
            this.leniency = leniency2;
            this.maxTries = j;
        }
    }

    private PhoneNumberMatch extractInnerMatch(String str, int i) {
        int i2 = 0;
        Matcher matcher = INNER.matcher(str);
        while (this.maxTries > 0 && matcher.find(i2)) {
            PhoneNumberMatch parseAndVerify = parseAndVerify(str.substring(matcher.start(), matcher.end()), matcher.start() + i);
            if (parseAndVerify != null) {
                return parseAndVerify;
            }
            this.maxTries--;
            i2 = matcher.end();
        }
        return null;
    }

    private PhoneNumberMatch extractMatch(CharSequence charSequence, int i) {
        if (PUB_PAGES.matcher(charSequence).find() || SLASH_SEPARATED_DATES.matcher(charSequence).find()) {
            return null;
        }
        String obj = charSequence.toString();
        PhoneNumberMatch parseAndVerify = parseAndVerify(obj, i);
        return parseAndVerify != null ? parseAndVerify : extractInnerMatch(obj, i);
    }

    private PhoneNumberMatch find(int i) {
        Matcher matcher = PATTERN.matcher(this.text);
        int i2 = i;
        while (this.maxTries > 0 && matcher.find(i2)) {
            int start = matcher.start();
            CharSequence trimAfterFirstMatch = trimAfterFirstMatch(PhoneNumberUtil.SECOND_NUMBER_START_PATTERN, this.text.subSequence(start, matcher.end()));
            PhoneNumberMatch extractMatch = extractMatch(trimAfterFirstMatch, start);
            if (extractMatch != null) {
                return extractMatch;
            }
            i2 = start + trimAfterFirstMatch.length();
            this.maxTries--;
        }
        return null;
    }

    private static String limit(int i, int i2) {
        if (i >= 0 && i2 > 0 && i2 >= i) {
            return "{" + i + "," + i2 + "}";
        }
        throw new IllegalArgumentException();
    }

    private PhoneNumberMatch parseAndVerify(String str, int i) {
        try {
            Phonenumber.PhoneNumber parse = this.util.parse(str, this.preferredRegion);
            if (this.leniency.verify(parse, this.util)) {
                return new PhoneNumberMatch(i, str, parse);
            }
        } catch (NumberParseException e) {
        }
        return null;
    }

    private static String removeSpace(String str) {
        StringBuilder sb = new StringBuilder(str.length());
        int i = 0;
        while (i < str.length()) {
            int codePointAt = str.codePointAt(i);
            if (!Character.isSpaceChar(codePointAt)) {
                sb.appendCodePoint(codePointAt);
            }
            i += Character.charCount(codePointAt);
        }
        return sb.toString();
    }

    private static CharSequence trimAfterFirstMatch(Pattern pattern, CharSequence charSequence) {
        Matcher matcher = pattern.matcher(charSequence);
        return matcher.find() ? charSequence.subSequence(0, matcher.start()) : charSequence;
    }

    public boolean hasNext() {
        if (this.state == State.NOT_READY) {
            this.lastMatch = find(this.searchIndex);
            if (this.lastMatch == null) {
                this.state = State.DONE;
            } else {
                this.searchIndex = this.lastMatch.end();
                this.state = State.READY;
            }
        }
        return this.state == State.READY;
    }

    public PhoneNumberMatch next() {
        if (!hasNext()) {
            throw new NoSuchElementException();
        }
        PhoneNumberMatch phoneNumberMatch = this.lastMatch;
        this.lastMatch = null;
        this.state = State.NOT_READY;
        return phoneNumberMatch;
    }

    public void remove() {
        throw new UnsupportedOperationException();
    }
}
