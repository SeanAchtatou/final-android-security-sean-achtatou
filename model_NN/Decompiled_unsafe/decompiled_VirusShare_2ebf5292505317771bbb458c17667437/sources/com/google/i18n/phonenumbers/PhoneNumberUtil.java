package com.google.i18n.phonenumbers;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.Phonemetadata;
import com.google.i18n.phonenumbers.Phonenumber;
import com.netqin.antivirus.Value;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneNumberUtil {
    private static final Map<Character, Character> ALL_NORMALIZATION_MAPPINGS;
    private static final Map<Character, Character> ALL_PLUS_NUMBER_GROUPING_SYMBOLS;
    private static final Map<Character, Character> ALPHA_MAPPINGS;
    private static final Pattern CAPTURING_DIGIT_PATTERN = Pattern.compile("([" + VALID_DIGITS + "])");
    private static final String CAPTURING_EXTN_DIGITS = ("([" + VALID_DIGITS + "]{1,7})");
    private static final Pattern CC_PATTERN = Pattern.compile("\\$CC");
    private static final String DEFAULT_EXTN_PREFIX = " ext. ";
    static final Map<Character, Character> DIGIT_MAPPINGS;
    private static final Pattern EXTN_PATTERN = Pattern.compile("(?:" + KNOWN_EXTN_PATTERNS + ")$", REGEX_FLAGS);
    private static final Pattern FG_PATTERN = Pattern.compile("\\$FG");
    private static final Pattern FIRST_GROUP_PATTERN = Pattern.compile("(\\$1)");
    static final String KNOWN_EXTN_PATTERNS = (RFC3966_EXTN_PREFIX + CAPTURING_EXTN_DIGITS + "|" + "[  \\t,]*(?:ext(?:ensi(?:ó?|ó))?n?|" + "ｅｘｔｎ?|[,xｘ#＃~～]|int|anexo|ｉｎｔ)" + "[:\\.．]?[  \\t,-]*" + CAPTURING_EXTN_DIGITS + "#?|" + "[- ]+([" + VALID_DIGITS + "]{1,5})#");
    private static final Logger LOGGER = Logger.getLogger(PhoneNumberUtil.class.getName());
    static final int MAX_LENGTH_COUNTRY_CODE = 3;
    static final int MAX_LENGTH_FOR_NSN = 15;
    static final String META_DATA_FILE_PREFIX = "/com/google/i18n/phonenumbers/data/PhoneNumberMetadataProto";
    private static final int MIN_LENGTH_FOR_NSN = 3;
    private static final int NANPA_COUNTRY_CODE = 1;
    private static final Pattern NON_DIGITS_PATTERN = Pattern.compile("(\\D+)");
    private static final Pattern NP_PATTERN = Pattern.compile("\\$NP");
    static final String PLUS_CHARS = "+＋";
    private static final Pattern PLUS_CHARS_PATTERN = Pattern.compile("[+＋]+");
    static final char PLUS_SIGN = '+';
    static final int REGEX_FLAGS = 66;
    private static final String RFC3966_EXTN_PREFIX = ";ext=";
    private static final String SECOND_NUMBER_START = "[\\\\/] *x";
    static final Pattern SECOND_NUMBER_START_PATTERN = Pattern.compile(SECOND_NUMBER_START);
    private static final Pattern SEPARATOR_PATTERN = Pattern.compile("[-x‐-―−ー－-／  ​⁠　()（）［］.\\[\\]/~⁓∼～]+");
    private static final Pattern UNIQUE_INTERNATIONAL_PREFIX = Pattern.compile("[\\d]+(?:[~⁓∼～][\\d]+)?");
    private static final String UNKNOWN_REGION = "ZZ";
    private static final String UNWANTED_END_CHARS = "[[\\P{N}&&\\P{L}]&&[^#]]+$";
    private static final Pattern UNWANTED_END_CHAR_PATTERN = Pattern.compile(UNWANTED_END_CHARS);
    private static final String VALID_ALPHA = (Arrays.toString(ALPHA_MAPPINGS.keySet().toArray()).replaceAll("[, \\[\\]]", "") + Arrays.toString(ALPHA_MAPPINGS.keySet().toArray()).toLowerCase().replaceAll("[, \\[\\]]", ""));
    private static final Pattern VALID_ALPHA_PHONE_PATTERN = Pattern.compile("(?:.*?[A-Za-z]){3}.*");
    private static final String VALID_DIGITS = Arrays.toString(DIGIT_MAPPINGS.keySet().toArray()).replaceAll("[, \\[\\]]", "");
    private static final String VALID_PHONE_NUMBER = ("[+＋]*(?:[-x‐-―−ー－-／  ​⁠　()（）［］.\\[\\]/~⁓∼～]*[" + VALID_DIGITS + "]){3,}[" + VALID_PUNCTUATION + VALID_ALPHA + VALID_DIGITS + "]*");
    private static final Pattern VALID_PHONE_NUMBER_PATTERN = Pattern.compile(VALID_PHONE_NUMBER + "(?:" + KNOWN_EXTN_PATTERNS + ")?", REGEX_FLAGS);
    static final String VALID_PUNCTUATION = "-x‐-―−ー－-／  ​⁠　()（）［］.\\[\\]/~⁓∼～";
    private static final String VALID_START_CHAR = ("[+＋" + VALID_DIGITS + "]");
    static final Pattern VALID_START_CHAR_PATTERN = Pattern.compile(VALID_START_CHAR);
    private static PhoneNumberUtil instance = null;
    private Map<Integer, List<String>> countryCallingCodeToRegionCodeMap = null;
    private String currentFilePrefix = META_DATA_FILE_PREFIX;
    private final Set<String> nanpaRegions = new HashSet(35);
    private RegexCache regexCache = new RegexCache(100);
    private Map<String, Phonemetadata.PhoneMetadata> regionToMetadataMap = new HashMap();
    private final Set<String> supportedRegions = new HashSet((int) Value.SMS_CHANGE_MSG_VALUE);

    public enum Leniency {
        POSSIBLE {
            /* access modifiers changed from: package-private */
            public boolean verify(Phonenumber.PhoneNumber phoneNumber, PhoneNumberUtil phoneNumberUtil) {
                return phoneNumberUtil.isPossibleNumber(phoneNumber);
            }
        },
        VALID {
            /* access modifiers changed from: package-private */
            public boolean verify(Phonenumber.PhoneNumber phoneNumber, PhoneNumberUtil phoneNumberUtil) {
                return phoneNumberUtil.isValidNumber(phoneNumber);
            }
        };

        /* access modifiers changed from: package-private */
        public abstract boolean verify(Phonenumber.PhoneNumber phoneNumber, PhoneNumberUtil phoneNumberUtil);
    }

    public enum MatchType {
        NOT_A_NUMBER,
        NO_MATCH,
        SHORT_NSN_MATCH,
        NSN_MATCH,
        EXACT_MATCH
    }

    public enum PhoneNumberFormat {
        E164,
        INTERNATIONAL,
        NATIONAL,
        RFC3966
    }

    public enum PhoneNumberType {
        FIXED_LINE,
        MOBILE,
        FIXED_LINE_OR_MOBILE,
        TOLL_FREE,
        PREMIUM_RATE,
        SHARED_COST,
        VOIP,
        PERSONAL_NUMBER,
        PAGER,
        UAN,
        UNKNOWN
    }

    public enum ValidationResult {
        IS_POSSIBLE,
        INVALID_COUNTRY_CODE,
        TOO_SHORT,
        TOO_LONG
    }

    static {
        HashMap hashMap = new HashMap();
        hashMap.put('0', '0');
        hashMap.put('1', '1');
        hashMap.put('2', '2');
        hashMap.put('3', '3');
        hashMap.put('4', '4');
        hashMap.put('5', '5');
        hashMap.put('6', '6');
        hashMap.put('7', '7');
        hashMap.put('8', '8');
        hashMap.put('9', '9');
        HashMap hashMap2 = new HashMap(50);
        hashMap2.putAll(hashMap);
        hashMap2.put(65296, '0');
        hashMap2.put(1632, '0');
        hashMap2.put(1776, '0');
        hashMap2.put(65297, '1');
        hashMap2.put(1633, '1');
        hashMap2.put(1777, '1');
        hashMap2.put(65298, '2');
        hashMap2.put(1634, '2');
        hashMap2.put(1778, '2');
        hashMap2.put(65299, '3');
        hashMap2.put(1635, '3');
        hashMap2.put(1779, '3');
        hashMap2.put(65300, '4');
        hashMap2.put(1636, '4');
        hashMap2.put(1780, '4');
        hashMap2.put(65301, '5');
        hashMap2.put(1637, '5');
        hashMap2.put(1781, '5');
        hashMap2.put(65302, '6');
        hashMap2.put(1638, '6');
        hashMap2.put(1782, '6');
        hashMap2.put(65303, '7');
        hashMap2.put(1639, '7');
        hashMap2.put(1783, '7');
        hashMap2.put(65304, '8');
        hashMap2.put(1640, '8');
        hashMap2.put(1784, '8');
        hashMap2.put(65305, '9');
        hashMap2.put(1641, '9');
        hashMap2.put(1785, '9');
        DIGIT_MAPPINGS = Collections.unmodifiableMap(hashMap2);
        HashMap hashMap3 = new HashMap(40);
        hashMap3.put('A', '2');
        hashMap3.put('B', '2');
        hashMap3.put('C', '2');
        hashMap3.put('D', '3');
        hashMap3.put('E', '3');
        hashMap3.put('F', '3');
        hashMap3.put('G', '4');
        hashMap3.put('H', '4');
        hashMap3.put('I', '4');
        hashMap3.put('J', '5');
        hashMap3.put('K', '5');
        hashMap3.put('L', '5');
        hashMap3.put('M', '6');
        hashMap3.put('N', '6');
        hashMap3.put('O', '6');
        hashMap3.put('P', '7');
        hashMap3.put('Q', '7');
        hashMap3.put('R', '7');
        hashMap3.put('S', '7');
        hashMap3.put('T', '8');
        hashMap3.put('U', '8');
        hashMap3.put('V', '8');
        hashMap3.put('W', '9');
        hashMap3.put('X', '9');
        hashMap3.put('Y', '9');
        hashMap3.put('Z', '9');
        ALPHA_MAPPINGS = Collections.unmodifiableMap(hashMap3);
        HashMap hashMap4 = new HashMap(100);
        hashMap4.putAll(hashMap3);
        hashMap4.putAll(hashMap2);
        ALL_NORMALIZATION_MAPPINGS = Collections.unmodifiableMap(hashMap4);
        HashMap hashMap5 = new HashMap();
        for (Character charValue : ALPHA_MAPPINGS.keySet()) {
            char charValue2 = charValue.charValue();
            hashMap5.put(Character.valueOf(Character.toLowerCase(charValue2)), Character.valueOf(charValue2));
            hashMap5.put(Character.valueOf(charValue2), Character.valueOf(charValue2));
        }
        hashMap5.putAll(hashMap);
        hashMap5.put('-', '-');
        hashMap5.put(65293, '-');
        hashMap5.put(8208, '-');
        hashMap5.put(8209, '-');
        hashMap5.put(8210, '-');
        hashMap5.put(8211, '-');
        hashMap5.put(8212, '-');
        hashMap5.put(8213, '-');
        hashMap5.put(8722, '-');
        hashMap5.put('/', '/');
        hashMap5.put(65295, '/');
        hashMap5.put(' ', ' ');
        hashMap5.put(12288, ' ');
        hashMap5.put(8288, ' ');
        hashMap5.put('.', '.');
        hashMap5.put(65294, '.');
        ALL_PLUS_NUMBER_GROUPING_SYMBOLS = Collections.unmodifiableMap(hashMap5);
    }

    private PhoneNumberUtil() {
    }

    private boolean checkRegionForParsing(String str, String str2) {
        return isValidRegionCode(str2) || !(str == null || str.length() == 0 || !PLUS_CHARS_PATTERN.matcher(str).lookingAt());
    }

    public static String convertAlphaCharactersInNumber(String str) {
        return normalizeHelper(str, ALL_NORMALIZATION_MAPPINGS, false);
    }

    static String extractPossibleNumber(String str) {
        Matcher matcher = VALID_START_CHAR_PATTERN.matcher(str);
        if (!matcher.find()) {
            return "";
        }
        String substring = str.substring(matcher.start());
        Matcher matcher2 = UNWANTED_END_CHAR_PATTERN.matcher(substring);
        if (matcher2.find()) {
            substring = substring.substring(0, matcher2.start());
            LOGGER.log(Level.FINER, "Stripped trailing characters: " + substring);
        }
        Matcher matcher3 = SECOND_NUMBER_START_PATTERN.matcher(substring);
        return matcher3.find() ? substring.substring(0, matcher3.start()) : substring;
    }

    private String formatAccordingToFormats(String str, List<Phonemetadata.NumberFormat> list, PhoneNumberFormat phoneNumberFormat) {
        return formatAccordingToFormats(str, list, phoneNumberFormat, null);
    }

    private String formatAccordingToFormats(String str, List<Phonemetadata.NumberFormat> list, PhoneNumberFormat phoneNumberFormat, String str2) {
        for (Phonemetadata.NumberFormat next : list) {
            int leadingDigitsPatternSize = next.leadingDigitsPatternSize();
            if (leadingDigitsPatternSize == 0 || this.regexCache.getPatternForRegex(next.getLeadingDigitsPattern(leadingDigitsPatternSize - 1)).matcher(str).lookingAt()) {
                Matcher matcher = this.regexCache.getPatternForRegex(next.getPattern()).matcher(str);
                if (matcher.matches()) {
                    String format = next.getFormat();
                    if (phoneNumberFormat != PhoneNumberFormat.NATIONAL || str2 == null || str2.length() <= 0 || next.getDomesticCarrierCodeFormattingRule().length() <= 0) {
                        String nationalPrefixFormattingRule = next.getNationalPrefixFormattingRule();
                        return (phoneNumberFormat != PhoneNumberFormat.NATIONAL || nationalPrefixFormattingRule == null || nationalPrefixFormattingRule.length() <= 0) ? matcher.replaceAll(format) : matcher.replaceAll(FIRST_GROUP_PATTERN.matcher(format).replaceFirst(nationalPrefixFormattingRule));
                    }
                    return matcher.replaceAll(FIRST_GROUP_PATTERN.matcher(format).replaceFirst(CC_PATTERN.matcher(next.getDomesticCarrierCodeFormattingRule()).replaceFirst(str2)));
                }
            }
        }
        return str;
    }

    private void formatExtension(String str, String str2, StringBuffer stringBuffer) {
        Phonemetadata.PhoneMetadata metadataForRegion = getMetadataForRegion(str2);
        if (metadataForRegion.hasPreferredExtnPrefix()) {
            stringBuffer.append(metadataForRegion.getPreferredExtnPrefix()).append(str);
        } else {
            stringBuffer.append(DEFAULT_EXTN_PREFIX).append(str);
        }
    }

    private String formatNationalNumber(String str, String str2, PhoneNumberFormat phoneNumberFormat) {
        return formatNationalNumber(str, str2, phoneNumberFormat, null);
    }

    private String formatNationalNumber(String str, String str2, PhoneNumberFormat phoneNumberFormat, String str3) {
        Phonemetadata.PhoneMetadata metadataForRegion = getMetadataForRegion(str2);
        String formatAccordingToFormats = formatAccordingToFormats(str, (metadataForRegion.intlNumberFormats().size() == 0 || phoneNumberFormat == PhoneNumberFormat.NATIONAL) ? metadataForRegion.numberFormats() : metadataForRegion.intlNumberFormats(), phoneNumberFormat, str3);
        return phoneNumberFormat == PhoneNumberFormat.RFC3966 ? SEPARATOR_PATTERN.matcher(formatAccordingToFormats).replaceAll("-") : formatAccordingToFormats;
    }

    private void formatNumberByFormat(int i, PhoneNumberFormat phoneNumberFormat, StringBuffer stringBuffer) {
        switch (phoneNumberFormat) {
            case E164:
                stringBuffer.insert(0, i).insert(0, (char) PLUS_SIGN);
                return;
            case INTERNATIONAL:
                stringBuffer.insert(0, " ").insert(0, i).insert(0, (char) PLUS_SIGN);
                return;
            case RFC3966:
                stringBuffer.insert(0, "-").insert(0, i).insert(0, (char) PLUS_SIGN);
                return;
            default:
                return;
        }
    }

    public static synchronized PhoneNumberUtil getInstance() {
        PhoneNumberUtil instance2;
        synchronized (PhoneNumberUtil.class) {
            instance2 = instance == null ? getInstance(META_DATA_FILE_PREFIX, CountryCodeToRegionCodeMap.getCountryCodeToRegionCodeMap()) : instance;
        }
        return instance2;
    }

    static synchronized PhoneNumberUtil getInstance(String str, Map<Integer, List<String>> map) {
        PhoneNumberUtil phoneNumberUtil;
        synchronized (PhoneNumberUtil.class) {
            if (instance == null) {
                instance = new PhoneNumberUtil();
                instance.countryCallingCodeToRegionCodeMap = map;
                instance.init(str);
            }
            phoneNumberUtil = instance;
        }
        return phoneNumberUtil;
    }

    private PhoneNumberType getNumberTypeHelper(String str, Phonemetadata.PhoneMetadata phoneMetadata) {
        Phonemetadata.PhoneNumberDesc generalDesc = phoneMetadata.getGeneralDesc();
        return (!generalDesc.hasNationalNumberPattern() || !isNumberMatchingDesc(str, generalDesc)) ? PhoneNumberType.UNKNOWN : isNumberMatchingDesc(str, phoneMetadata.getPremiumRate()) ? PhoneNumberType.PREMIUM_RATE : isNumberMatchingDesc(str, phoneMetadata.getTollFree()) ? PhoneNumberType.TOLL_FREE : isNumberMatchingDesc(str, phoneMetadata.getSharedCost()) ? PhoneNumberType.SHARED_COST : isNumberMatchingDesc(str, phoneMetadata.getVoip()) ? PhoneNumberType.VOIP : isNumberMatchingDesc(str, phoneMetadata.getPersonalNumber()) ? PhoneNumberType.PERSONAL_NUMBER : isNumberMatchingDesc(str, phoneMetadata.getPager()) ? PhoneNumberType.PAGER : isNumberMatchingDesc(str, phoneMetadata.getUan()) ? PhoneNumberType.UAN : isNumberMatchingDesc(str, phoneMetadata.getFixedLine()) ? phoneMetadata.getSameMobileAndFixedLinePattern() ? PhoneNumberType.FIXED_LINE_OR_MOBILE : isNumberMatchingDesc(str, phoneMetadata.getMobile()) ? PhoneNumberType.FIXED_LINE_OR_MOBILE : PhoneNumberType.FIXED_LINE : (phoneMetadata.getSameMobileAndFixedLinePattern() || !isNumberMatchingDesc(str, phoneMetadata.getMobile())) ? PhoneNumberType.UNKNOWN : PhoneNumberType.MOBILE;
    }

    private String getRegionCodeForNumberFromRegionList(Phonenumber.PhoneNumber phoneNumber, List<String> list) {
        String nationalSignificantNumber = getNationalSignificantNumber(phoneNumber);
        for (String next : list) {
            Phonemetadata.PhoneMetadata metadataForRegion = getMetadataForRegion(next);
            if (metadataForRegion.hasLeadingDigits()) {
                if (this.regexCache.getPatternForRegex(metadataForRegion.getLeadingDigits()).matcher(nationalSignificantNumber).lookingAt()) {
                    return next;
                }
            } else if (getNumberTypeHelper(nationalSignificantNumber, metadataForRegion) != PhoneNumberType.UNKNOWN) {
                return next;
            }
        }
        return null;
    }

    private boolean hasValidRegionCode(String str, int i, String str2) {
        if (isValidRegionCode(str)) {
            return true;
        }
        LOGGER.log(Level.WARNING, "Number " + str2 + " has invalid or missing country calling code (" + i + ")");
        return false;
    }

    private void init(String str) {
        this.currentFilePrefix = str;
        for (List<String> addAll : this.countryCallingCodeToRegionCodeMap.values()) {
            this.supportedRegions.addAll(addAll);
        }
        this.nanpaRegions.addAll(this.countryCallingCodeToRegionCodeMap.get(1));
    }

    private boolean isNationalNumberSuffixOfTheOther(Phonenumber.PhoneNumber phoneNumber, Phonenumber.PhoneNumber phoneNumber2) {
        String valueOf = String.valueOf(phoneNumber.getNationalNumber());
        String valueOf2 = String.valueOf(phoneNumber2.getNationalNumber());
        return valueOf.endsWith(valueOf2) || valueOf2.endsWith(valueOf);
    }

    private boolean isNumberMatchingDesc(String str, Phonemetadata.PhoneNumberDesc phoneNumberDesc) {
        return this.regexCache.getPatternForRegex(phoneNumberDesc.getPossibleNumberPattern()).matcher(str).matches() && this.regexCache.getPatternForRegex(phoneNumberDesc.getNationalNumberPattern()).matcher(str).matches();
    }

    private boolean isValidRegionCode(String str) {
        return str != null && this.supportedRegions.contains(str.toUpperCase());
    }

    static boolean isViablePhoneNumber(String str) {
        if (str.length() < 3) {
            return false;
        }
        return VALID_PHONE_NUMBER_PATTERN.matcher(str).matches();
    }

    private void loadMetadataForRegionFromFile(String str, String str2) {
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(PhoneNumberUtil.class.getResourceAsStream(str + "_" + str2));
            Phonemetadata.PhoneMetadataCollection phoneMetadataCollection = new Phonemetadata.PhoneMetadataCollection();
            phoneMetadataCollection.readExternal(objectInputStream);
            for (Phonemetadata.PhoneMetadata put : phoneMetadataCollection.getMetadataList()) {
                this.regionToMetadataMap.put(str2, put);
            }
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, e.toString());
        }
    }

    private void maybeGetFormattedExtension(Phonenumber.PhoneNumber phoneNumber, String str, PhoneNumberFormat phoneNumberFormat, StringBuffer stringBuffer) {
        if (phoneNumber.hasExtension() && phoneNumber.getExtension().length() > 0) {
            if (phoneNumberFormat == PhoneNumberFormat.RFC3966) {
                stringBuffer.append(RFC3966_EXTN_PREFIX).append(phoneNumber.getExtension());
            } else {
                formatExtension(phoneNumber.getExtension(), str, stringBuffer);
            }
        }
    }

    static String normalize(String str) {
        return VALID_ALPHA_PHONE_PATTERN.matcher(str).matches() ? normalizeHelper(str, ALL_NORMALIZATION_MAPPINGS, true) : normalizeHelper(str, DIGIT_MAPPINGS, true);
    }

    static void normalize(StringBuffer stringBuffer) {
        stringBuffer.replace(0, stringBuffer.length(), normalize(stringBuffer.toString()));
    }

    public static String normalizeDigitsOnly(String str) {
        return normalizeHelper(str, DIGIT_MAPPINGS, true);
    }

    private static String normalizeHelper(String str, Map<Character, Character> map, boolean z) {
        StringBuffer stringBuffer = new StringBuffer(str.length());
        for (char c : str.toCharArray()) {
            Character ch = map.get(Character.valueOf(Character.toUpperCase(c)));
            if (ch != null) {
                stringBuffer.append(ch);
            } else if (!z) {
                stringBuffer.append(c);
            }
        }
        return stringBuffer.toString();
    }

    private void parseHelper(String str, String str2, boolean z, boolean z2, Phonenumber.PhoneNumber phoneNumber) throws NumberParseException {
        Phonemetadata.PhoneMetadata phoneMetadata;
        if (str == null) {
            throw new NumberParseException(NumberParseException.ErrorType.NOT_A_NUMBER, "The phone number supplied was null.");
        }
        String extractPossibleNumber = extractPossibleNumber(str);
        if (!isViablePhoneNumber(extractPossibleNumber)) {
            throw new NumberParseException(NumberParseException.ErrorType.NOT_A_NUMBER, "The string supplied did not seem to be a phone number.");
        } else if (!z2 || checkRegionForParsing(extractPossibleNumber, str2)) {
            if (z) {
                phoneNumber.setRawInput(str);
            }
            StringBuffer stringBuffer = new StringBuffer(extractPossibleNumber);
            String maybeStripExtension = maybeStripExtension(stringBuffer);
            if (maybeStripExtension.length() > 0) {
                phoneNumber.setExtension(maybeStripExtension);
            }
            Phonemetadata.PhoneMetadata metadataForRegion = getMetadataForRegion(str2);
            StringBuffer stringBuffer2 = new StringBuffer();
            int maybeExtractCountryCode = maybeExtractCountryCode(stringBuffer.toString(), metadataForRegion, stringBuffer2, z, phoneNumber);
            if (maybeExtractCountryCode != 0) {
                String regionCodeForCountryCode = getRegionCodeForCountryCode(maybeExtractCountryCode);
                if (!regionCodeForCountryCode.equals(str2)) {
                    phoneMetadata = getMetadataForRegion(regionCodeForCountryCode);
                }
                phoneMetadata = metadataForRegion;
            } else {
                normalize(stringBuffer);
                stringBuffer2.append(stringBuffer);
                if (str2 != null) {
                    phoneNumber.setCountryCode(metadataForRegion.getCountryCode());
                    phoneMetadata = metadataForRegion;
                } else {
                    if (z) {
                        phoneNumber.clearCountryCodeSource();
                    }
                    phoneMetadata = metadataForRegion;
                }
            }
            if (stringBuffer2.length() < 3) {
                throw new NumberParseException(NumberParseException.ErrorType.TOO_SHORT_NSN, "The string supplied is too short to be a phone number.");
            }
            if (phoneMetadata != null) {
                String maybeStripNationalPrefixAndCarrierCode = maybeStripNationalPrefixAndCarrierCode(stringBuffer2, phoneMetadata);
                if (z) {
                    phoneNumber.setPreferredDomesticCarrierCode(maybeStripNationalPrefixAndCarrierCode);
                }
            }
            int length = stringBuffer2.length();
            if (length < 3) {
                throw new NumberParseException(NumberParseException.ErrorType.TOO_SHORT_NSN, "The string supplied is too short to be a phone number.");
            } else if (length > 15) {
                throw new NumberParseException(NumberParseException.ErrorType.TOO_LONG, "The string supplied is too long to be a phone number.");
            } else {
                if (stringBuffer2.charAt(0) == '0' && phoneMetadata != null && phoneMetadata.isLeadingZeroPossible()) {
                    phoneNumber.setItalianLeadingZero(true);
                }
                phoneNumber.setNationalNumber(Long.parseLong(stringBuffer2.toString()));
            }
        } else {
            throw new NumberParseException(NumberParseException.ErrorType.INVALID_COUNTRY_CODE, "Missing or invalid default region.");
        }
    }

    private boolean parsePrefixAsIdd(Pattern pattern, StringBuffer stringBuffer) {
        Matcher matcher = pattern.matcher(stringBuffer);
        if (!matcher.lookingAt()) {
            return false;
        }
        int end = matcher.end();
        Matcher matcher2 = CAPTURING_DIGIT_PATTERN.matcher(stringBuffer.substring(end));
        if (matcher2.find() && normalizeHelper(matcher2.group(1), DIGIT_MAPPINGS, true).equals("0")) {
            return false;
        }
        stringBuffer.delete(0, end);
        return true;
    }

    static synchronized void resetInstance() {
        synchronized (PhoneNumberUtil.class) {
            instance = null;
        }
    }

    private ValidationResult testNumberLengthAgainstPattern(Pattern pattern, String str) {
        Matcher matcher = pattern.matcher(str);
        return matcher.matches() ? ValidationResult.IS_POSSIBLE : matcher.lookingAt() ? ValidationResult.TOO_LONG : ValidationResult.TOO_SHORT;
    }

    /* access modifiers changed from: package-private */
    public boolean canBeInternationallyDialled(Phonenumber.PhoneNumber phoneNumber) {
        String regionCodeForNumber = getRegionCodeForNumber(phoneNumber);
        String nationalSignificantNumber = getNationalSignificantNumber(phoneNumber);
        if (!isValidRegionCode(regionCodeForNumber)) {
            return true;
        }
        return !isNumberMatchingDesc(nationalSignificantNumber, getMetadataForRegion(regionCodeForNumber).getNoInternationalDialling());
    }

    /* access modifiers changed from: package-private */
    public int extractCountryCode(StringBuffer stringBuffer, StringBuffer stringBuffer2) {
        int length = stringBuffer.length();
        int i = 1;
        while (i <= 3 && i <= length) {
            int parseInt = Integer.parseInt(stringBuffer.substring(0, i));
            if (this.countryCallingCodeToRegionCodeMap.containsKey(Integer.valueOf(parseInt))) {
                stringBuffer2.append(stringBuffer.substring(i));
                return parseInt;
            }
            i++;
        }
        return 0;
    }

    public Iterable<PhoneNumberMatch> findNumbers(CharSequence charSequence, String str) {
        return findNumbers(charSequence, str, Leniency.VALID, Long.MAX_VALUE);
    }

    public Iterable<PhoneNumberMatch> findNumbers(CharSequence charSequence, String str, Leniency leniency, long j) {
        final CharSequence charSequence2 = charSequence;
        final String str2 = str;
        final Leniency leniency2 = leniency;
        final long j2 = j;
        return new Iterable<PhoneNumberMatch>() {
            public Iterator<PhoneNumberMatch> iterator() {
                return new PhoneNumberMatcher(PhoneNumberUtil.this, charSequence2, str2, leniency2, j2);
            }
        };
    }

    public String format(Phonenumber.PhoneNumber phoneNumber, PhoneNumberFormat phoneNumberFormat) {
        StringBuffer stringBuffer = new StringBuffer(20);
        format(phoneNumber, phoneNumberFormat, stringBuffer);
        return stringBuffer.toString();
    }

    public void format(Phonenumber.PhoneNumber phoneNumber, PhoneNumberFormat phoneNumberFormat, StringBuffer stringBuffer) {
        stringBuffer.setLength(0);
        int countryCode = phoneNumber.getCountryCode();
        String nationalSignificantNumber = getNationalSignificantNumber(phoneNumber);
        if (phoneNumberFormat == PhoneNumberFormat.E164) {
            stringBuffer.append(nationalSignificantNumber);
            formatNumberByFormat(countryCode, PhoneNumberFormat.E164, stringBuffer);
            return;
        }
        String regionCodeForCountryCode = getRegionCodeForCountryCode(countryCode);
        if (!isValidRegionCode(regionCodeForCountryCode)) {
            stringBuffer.append(nationalSignificantNumber);
            return;
        }
        stringBuffer.append(formatNationalNumber(nationalSignificantNumber, regionCodeForCountryCode, phoneNumberFormat));
        maybeGetFormattedExtension(phoneNumber, regionCodeForCountryCode, phoneNumberFormat, stringBuffer);
        formatNumberByFormat(countryCode, phoneNumberFormat, stringBuffer);
    }

    public String formatByPattern(Phonenumber.PhoneNumber phoneNumber, PhoneNumberFormat phoneNumberFormat, List<Phonemetadata.NumberFormat> list) {
        int countryCode = phoneNumber.getCountryCode();
        String nationalSignificantNumber = getNationalSignificantNumber(phoneNumber);
        String regionCodeForCountryCode = getRegionCodeForCountryCode(countryCode);
        if (!hasValidRegionCode(regionCodeForCountryCode, countryCode, nationalSignificantNumber)) {
            return nationalSignificantNumber;
        }
        ArrayList arrayList = new ArrayList(list.size());
        for (Phonemetadata.NumberFormat next : list) {
            String nationalPrefixFormattingRule = next.getNationalPrefixFormattingRule();
            if (nationalPrefixFormattingRule.length() > 0) {
                Phonemetadata.NumberFormat numberFormat = new Phonemetadata.NumberFormat();
                numberFormat.mergeFrom(next);
                String nationalPrefix = getMetadataForRegion(regionCodeForCountryCode).getNationalPrefix();
                if (nationalPrefix.length() > 0) {
                    numberFormat.setNationalPrefixFormattingRule(FG_PATTERN.matcher(NP_PATTERN.matcher(nationalPrefixFormattingRule).replaceFirst(nationalPrefix)).replaceFirst("\\$1"));
                } else {
                    numberFormat.clearNationalPrefixFormattingRule();
                }
                arrayList.add(numberFormat);
            } else {
                arrayList.add(next);
            }
        }
        StringBuffer stringBuffer = new StringBuffer(formatAccordingToFormats(nationalSignificantNumber, arrayList, phoneNumberFormat));
        maybeGetFormattedExtension(phoneNumber, regionCodeForCountryCode, phoneNumberFormat, stringBuffer);
        formatNumberByFormat(countryCode, phoneNumberFormat, stringBuffer);
        return stringBuffer.toString();
    }

    public String formatInOriginalFormat(Phonenumber.PhoneNumber phoneNumber, String str) {
        if (!phoneNumber.hasCountryCodeSource()) {
            return format(phoneNumber, PhoneNumberFormat.NATIONAL);
        }
        switch (phoneNumber.getCountryCodeSource()) {
            case FROM_NUMBER_WITH_PLUS_SIGN:
                return format(phoneNumber, PhoneNumberFormat.INTERNATIONAL);
            case FROM_NUMBER_WITH_IDD:
                return formatOutOfCountryCallingNumber(phoneNumber, str);
            case FROM_NUMBER_WITHOUT_PLUS_SIGN:
                return format(phoneNumber, PhoneNumberFormat.INTERNATIONAL).substring(1);
            default:
                return format(phoneNumber, PhoneNumberFormat.NATIONAL);
        }
    }

    public String formatNationalNumberWithCarrierCode(Phonenumber.PhoneNumber phoneNumber, String str) {
        int countryCode = phoneNumber.getCountryCode();
        String nationalSignificantNumber = getNationalSignificantNumber(phoneNumber);
        String regionCodeForCountryCode = getRegionCodeForCountryCode(countryCode);
        if (!hasValidRegionCode(regionCodeForCountryCode, countryCode, nationalSignificantNumber)) {
            return nationalSignificantNumber;
        }
        StringBuffer stringBuffer = new StringBuffer(20);
        stringBuffer.append(formatNationalNumber(nationalSignificantNumber, regionCodeForCountryCode, PhoneNumberFormat.NATIONAL, str));
        maybeGetFormattedExtension(phoneNumber, regionCodeForCountryCode, PhoneNumberFormat.NATIONAL, stringBuffer);
        formatNumberByFormat(countryCode, PhoneNumberFormat.NATIONAL, stringBuffer);
        return stringBuffer.toString();
    }

    public String formatNationalNumberWithPreferredCarrierCode(Phonenumber.PhoneNumber phoneNumber, String str) {
        return formatNationalNumberWithCarrierCode(phoneNumber, phoneNumber.hasPreferredDomesticCarrierCode() ? phoneNumber.getPreferredDomesticCarrierCode() : str);
    }

    public String formatOutOfCountryCallingNumber(Phonenumber.PhoneNumber phoneNumber, String str) {
        if (!isValidRegionCode(str)) {
            return format(phoneNumber, PhoneNumberFormat.INTERNATIONAL);
        }
        int countryCode = phoneNumber.getCountryCode();
        String regionCodeForCountryCode = getRegionCodeForCountryCode(countryCode);
        String nationalSignificantNumber = getNationalSignificantNumber(phoneNumber);
        if (!isValidRegionCode(regionCodeForCountryCode)) {
            return nationalSignificantNumber;
        }
        if (countryCode == 1) {
            if (isNANPACountry(str)) {
                return countryCode + " " + format(phoneNumber, PhoneNumberFormat.NATIONAL);
            }
        } else if (countryCode == getCountryCodeForRegion(str)) {
            return format(phoneNumber, PhoneNumberFormat.NATIONAL);
        }
        String formatNationalNumber = formatNationalNumber(nationalSignificantNumber, regionCodeForCountryCode, PhoneNumberFormat.INTERNATIONAL);
        Phonemetadata.PhoneMetadata metadataForRegion = getMetadataForRegion(str);
        String internationalPrefix = metadataForRegion.getInternationalPrefix();
        String preferredInternationalPrefix = UNIQUE_INTERNATIONAL_PREFIX.matcher(internationalPrefix).matches() ? internationalPrefix : metadataForRegion.hasPreferredInternationalPrefix() ? metadataForRegion.getPreferredInternationalPrefix() : "";
        StringBuffer stringBuffer = new StringBuffer(formatNationalNumber);
        maybeGetFormattedExtension(phoneNumber, regionCodeForCountryCode, PhoneNumberFormat.INTERNATIONAL, stringBuffer);
        if (preferredInternationalPrefix.length() > 0) {
            stringBuffer.insert(0, " ").insert(0, countryCode).insert(0, " ").insert(0, preferredInternationalPrefix);
        } else {
            formatNumberByFormat(countryCode, PhoneNumberFormat.INTERNATIONAL, stringBuffer);
        }
        return stringBuffer.toString();
    }

    public String formatOutOfCountryKeepingAlphaChars(Phonenumber.PhoneNumber phoneNumber, String str) {
        int indexOf;
        String rawInput = phoneNumber.getRawInput();
        if (rawInput.length() == 0) {
            return formatOutOfCountryCallingNumber(phoneNumber, str);
        }
        int countryCode = phoneNumber.getCountryCode();
        String regionCodeForCountryCode = getRegionCodeForCountryCode(countryCode);
        if (!hasValidRegionCode(regionCodeForCountryCode, countryCode, rawInput)) {
            return rawInput;
        }
        String normalizeHelper = normalizeHelper(rawInput, ALL_PLUS_NUMBER_GROUPING_SYMBOLS, true);
        String nationalSignificantNumber = getNationalSignificantNumber(phoneNumber);
        String substring = (nationalSignificantNumber.length() <= 3 || (indexOf = normalizeHelper.indexOf(nationalSignificantNumber.substring(0, 3))) == -1) ? normalizeHelper : normalizeHelper.substring(indexOf);
        Phonemetadata.PhoneMetadata metadataForRegion = getMetadataForRegion(str);
        if (countryCode == 1) {
            if (isNANPACountry(str)) {
                return countryCode + " " + substring;
            }
        } else if (countryCode == getCountryCodeForRegion(str)) {
            ArrayList arrayList = new ArrayList(metadataForRegion.numberFormatSize());
            for (Phonemetadata.NumberFormat mergeFrom : metadataForRegion.numberFormats()) {
                Phonemetadata.NumberFormat numberFormat = new Phonemetadata.NumberFormat();
                numberFormat.mergeFrom(mergeFrom);
                numberFormat.setPattern("(\\d+)(.*)");
                numberFormat.setFormat("$1$2");
                arrayList.add(numberFormat);
            }
            return formatAccordingToFormats(substring, arrayList, PhoneNumberFormat.NATIONAL);
        }
        String internationalPrefix = metadataForRegion.getInternationalPrefix();
        String preferredInternationalPrefix = UNIQUE_INTERNATIONAL_PREFIX.matcher(internationalPrefix).matches() ? internationalPrefix : metadataForRegion.getPreferredInternationalPrefix();
        StringBuffer stringBuffer = new StringBuffer(substring);
        maybeGetFormattedExtension(phoneNumber, regionCodeForCountryCode, PhoneNumberFormat.INTERNATIONAL, stringBuffer);
        if (preferredInternationalPrefix.length() > 0) {
            stringBuffer.insert(0, " ").insert(0, countryCode).insert(0, " ").insert(0, preferredInternationalPrefix);
        } else {
            formatNumberByFormat(countryCode, PhoneNumberFormat.INTERNATIONAL, stringBuffer);
        }
        return stringBuffer.toString();
    }

    public AsYouTypeFormatter getAsYouTypeFormatter(String str) {
        return new AsYouTypeFormatter(str);
    }

    public int getCountryCodeForRegion(String str) {
        if (!isValidRegionCode(str)) {
            return 0;
        }
        Phonemetadata.PhoneMetadata metadataForRegion = getMetadataForRegion(str);
        if (metadataForRegion == null) {
            return 0;
        }
        return metadataForRegion.getCountryCode();
    }

    public Phonenumber.PhoneNumber getExampleNumber(String str) {
        return getExampleNumberForType(str, PhoneNumberType.FIXED_LINE);
    }

    public Phonenumber.PhoneNumber getExampleNumberForType(String str, PhoneNumberType phoneNumberType) {
        Phonemetadata.PhoneNumberDesc numberDescByType = getNumberDescByType(getMetadataForRegion(str), phoneNumberType);
        try {
            if (numberDescByType.hasExampleNumber()) {
                return parse(numberDescByType.getExampleNumber(), str);
            }
        } catch (NumberParseException e) {
            LOGGER.log(Level.SEVERE, e.toString());
        }
        return null;
    }

    public int getLengthOfGeographicalAreaCode(Phonenumber.PhoneNumber phoneNumber) {
        String regionCodeForNumber = getRegionCodeForNumber(phoneNumber);
        if (!isValidRegionCode(regionCodeForNumber)) {
            return 0;
        }
        Phonemetadata.PhoneMetadata metadataForRegion = getMetadataForRegion(regionCodeForNumber);
        if (!metadataForRegion.hasNationalPrefix()) {
            return 0;
        }
        PhoneNumberType numberTypeHelper = getNumberTypeHelper(getNationalSignificantNumber(phoneNumber), metadataForRegion);
        if (numberTypeHelper == PhoneNumberType.FIXED_LINE || numberTypeHelper == PhoneNumberType.FIXED_LINE_OR_MOBILE) {
            return getLengthOfNationalDestinationCode(phoneNumber);
        }
        return 0;
    }

    public int getLengthOfNationalDestinationCode(Phonenumber.PhoneNumber phoneNumber) {
        Phonenumber.PhoneNumber phoneNumber2;
        if (phoneNumber.hasExtension()) {
            phoneNumber2 = new Phonenumber.PhoneNumber();
            phoneNumber2.mergeFrom(phoneNumber);
            phoneNumber2.clearExtension();
        } else {
            phoneNumber2 = phoneNumber;
        }
        String[] split = NON_DIGITS_PATTERN.split(format(phoneNumber2, PhoneNumberFormat.INTERNATIONAL));
        if (split.length <= 3) {
            return 0;
        }
        return (!getRegionCodeForNumber(phoneNumber).equals("AR") || getNumberType(phoneNumber) != PhoneNumberType.MOBILE) ? split[2].length() : split[3].length() + 1;
    }

    /* access modifiers changed from: package-private */
    public Phonemetadata.PhoneMetadata getMetadataForRegion(String str) {
        if (!isValidRegionCode(str)) {
            return null;
        }
        String upperCase = str.toUpperCase();
        if (!this.regionToMetadataMap.containsKey(upperCase)) {
            loadMetadataForRegionFromFile(this.currentFilePrefix, upperCase);
        }
        return this.regionToMetadataMap.get(upperCase);
    }

    public String getNationalSignificantNumber(Phonenumber.PhoneNumber phoneNumber) {
        StringBuffer stringBuffer = new StringBuffer((!phoneNumber.hasItalianLeadingZero() || !phoneNumber.getItalianLeadingZero() || !isLeadingZeroPossible(phoneNumber.getCountryCode())) ? "" : "0");
        stringBuffer.append(phoneNumber.getNationalNumber());
        return stringBuffer.toString();
    }

    public String getNddPrefixForRegion(String str, boolean z) {
        if (!isValidRegionCode(str)) {
            LOGGER.log(Level.SEVERE, "Invalid or missing region code provided.");
            return null;
        }
        Phonemetadata.PhoneMetadata metadataForRegion = getMetadataForRegion(str);
        if (metadataForRegion == null) {
            LOGGER.log(Level.SEVERE, "Unsupported region code provided.");
            return null;
        }
        String nationalPrefix = metadataForRegion.getNationalPrefix();
        if (nationalPrefix.length() == 0) {
            return null;
        }
        return z ? nationalPrefix.replace("~", "") : nationalPrefix;
    }

    /* access modifiers changed from: package-private */
    public Phonemetadata.PhoneNumberDesc getNumberDescByType(Phonemetadata.PhoneMetadata phoneMetadata, PhoneNumberType phoneNumberType) {
        switch (phoneNumberType) {
            case PREMIUM_RATE:
                return phoneMetadata.getPremiumRate();
            case TOLL_FREE:
                return phoneMetadata.getTollFree();
            case MOBILE:
                return phoneMetadata.getMobile();
            case FIXED_LINE:
            case FIXED_LINE_OR_MOBILE:
                return phoneMetadata.getFixedLine();
            case SHARED_COST:
                return phoneMetadata.getSharedCost();
            case VOIP:
                return phoneMetadata.getVoip();
            case PERSONAL_NUMBER:
                return phoneMetadata.getPersonalNumber();
            case PAGER:
                return phoneMetadata.getPager();
            case UAN:
                return phoneMetadata.getUan();
            default:
                return phoneMetadata.getGeneralDesc();
        }
    }

    public PhoneNumberType getNumberType(Phonenumber.PhoneNumber phoneNumber) {
        String regionCodeForNumber = getRegionCodeForNumber(phoneNumber);
        return !isValidRegionCode(regionCodeForNumber) ? PhoneNumberType.UNKNOWN : getNumberTypeHelper(getNationalSignificantNumber(phoneNumber), getMetadataForRegion(regionCodeForNumber));
    }

    public String getRegionCodeForCountryCode(int i) {
        List list = this.countryCallingCodeToRegionCodeMap.get(Integer.valueOf(i));
        return list == null ? UNKNOWN_REGION : (String) list.get(0);
    }

    public String getRegionCodeForNumber(Phonenumber.PhoneNumber phoneNumber) {
        List list = this.countryCallingCodeToRegionCodeMap.get(Integer.valueOf(phoneNumber.getCountryCode()));
        if (list == null) {
            return null;
        }
        return list.size() == 1 ? (String) list.get(0) : getRegionCodeForNumberFromRegionList(phoneNumber, list);
    }

    public Set<String> getSupportedRegions() {
        return this.supportedRegions;
    }

    public boolean isAlphaNumber(String str) {
        if (!isViablePhoneNumber(str)) {
            return false;
        }
        StringBuffer stringBuffer = new StringBuffer(str);
        maybeStripExtension(stringBuffer);
        return VALID_ALPHA_PHONE_PATTERN.matcher(stringBuffer).matches();
    }

    /* access modifiers changed from: package-private */
    public boolean isLeadingZeroPossible(int i) {
        Phonemetadata.PhoneMetadata metadataForRegion = getMetadataForRegion(getRegionCodeForCountryCode(i));
        if (metadataForRegion == null) {
            return false;
        }
        return metadataForRegion.isLeadingZeroPossible();
    }

    public boolean isNANPACountry(String str) {
        return str != null && this.nanpaRegions.contains(str.toUpperCase());
    }

    public MatchType isNumberMatch(Phonenumber.PhoneNumber phoneNumber, Phonenumber.PhoneNumber phoneNumber2) {
        Phonenumber.PhoneNumber phoneNumber3 = new Phonenumber.PhoneNumber();
        phoneNumber3.mergeFrom(phoneNumber);
        Phonenumber.PhoneNumber phoneNumber4 = new Phonenumber.PhoneNumber();
        phoneNumber4.mergeFrom(phoneNumber2);
        phoneNumber3.clearRawInput();
        phoneNumber3.clearCountryCodeSource();
        phoneNumber3.clearPreferredDomesticCarrierCode();
        phoneNumber4.clearRawInput();
        phoneNumber4.clearCountryCodeSource();
        phoneNumber4.clearPreferredDomesticCarrierCode();
        if (phoneNumber3.hasExtension() && phoneNumber3.getExtension().length() == 0) {
            phoneNumber3.clearExtension();
        }
        if (phoneNumber4.hasExtension() && phoneNumber4.getExtension().length() == 0) {
            phoneNumber4.clearExtension();
        }
        if (phoneNumber3.hasExtension() && phoneNumber4.hasExtension() && !phoneNumber3.getExtension().equals(phoneNumber4.getExtension())) {
            return MatchType.NO_MATCH;
        }
        int countryCode = phoneNumber3.getCountryCode();
        int countryCode2 = phoneNumber4.getCountryCode();
        if (countryCode != 0 && countryCode2 != 0) {
            return phoneNumber3.exactlySameAs(phoneNumber4) ? MatchType.EXACT_MATCH : (countryCode != countryCode2 || !isNationalNumberSuffixOfTheOther(phoneNumber3, phoneNumber4)) ? MatchType.NO_MATCH : MatchType.SHORT_NSN_MATCH;
        }
        phoneNumber3.setCountryCode(countryCode2);
        return phoneNumber3.exactlySameAs(phoneNumber4) ? MatchType.NSN_MATCH : isNationalNumberSuffixOfTheOther(phoneNumber3, phoneNumber4) ? MatchType.SHORT_NSN_MATCH : MatchType.NO_MATCH;
    }

    public MatchType isNumberMatch(Phonenumber.PhoneNumber phoneNumber, String str) {
        try {
            return isNumberMatch(phoneNumber, parse(str, UNKNOWN_REGION));
        } catch (NumberParseException e) {
            if (e.getErrorType() == NumberParseException.ErrorType.INVALID_COUNTRY_CODE) {
                String regionCodeForCountryCode = getRegionCodeForCountryCode(phoneNumber.getCountryCode());
                try {
                    if (!regionCodeForCountryCode.equals(UNKNOWN_REGION)) {
                        MatchType isNumberMatch = isNumberMatch(phoneNumber, parse(str, regionCodeForCountryCode));
                        return isNumberMatch == MatchType.EXACT_MATCH ? MatchType.NSN_MATCH : isNumberMatch;
                    }
                    Phonenumber.PhoneNumber phoneNumber2 = new Phonenumber.PhoneNumber();
                    parseHelper(str, null, false, false, phoneNumber2);
                    return isNumberMatch(phoneNumber, phoneNumber2);
                } catch (NumberParseException e2) {
                    return MatchType.NOT_A_NUMBER;
                }
            }
            return MatchType.NOT_A_NUMBER;
        }
    }

    public MatchType isNumberMatch(String str, String str2) {
        try {
            return isNumberMatch(parse(str, UNKNOWN_REGION), str2);
        } catch (NumberParseException e) {
            if (e.getErrorType() == NumberParseException.ErrorType.INVALID_COUNTRY_CODE) {
                try {
                    return isNumberMatch(parse(str2, UNKNOWN_REGION), str);
                } catch (NumberParseException e2) {
                    if (e2.getErrorType() == NumberParseException.ErrorType.INVALID_COUNTRY_CODE) {
                        try {
                            Phonenumber.PhoneNumber phoneNumber = new Phonenumber.PhoneNumber();
                            Phonenumber.PhoneNumber phoneNumber2 = new Phonenumber.PhoneNumber();
                            parseHelper(str, null, false, false, phoneNumber);
                            parseHelper(str2, null, false, false, phoneNumber2);
                            return isNumberMatch(phoneNumber, phoneNumber2);
                        } catch (NumberParseException e3) {
                        }
                    }
                }
            }
            return MatchType.NOT_A_NUMBER;
        }
    }

    public boolean isPossibleNumber(Phonenumber.PhoneNumber phoneNumber) {
        return isPossibleNumberWithReason(phoneNumber) == ValidationResult.IS_POSSIBLE;
    }

    public boolean isPossibleNumber(String str, String str2) {
        try {
            return isPossibleNumber(parse(str, str2));
        } catch (NumberParseException e) {
            return false;
        }
    }

    public ValidationResult isPossibleNumberWithReason(Phonenumber.PhoneNumber phoneNumber) {
        String nationalSignificantNumber = getNationalSignificantNumber(phoneNumber);
        String regionCodeForCountryCode = getRegionCodeForCountryCode(phoneNumber.getCountryCode());
        if (!isValidRegionCode(regionCodeForCountryCode)) {
            return ValidationResult.INVALID_COUNTRY_CODE;
        }
        Phonemetadata.PhoneNumberDesc generalDesc = getMetadataForRegion(regionCodeForCountryCode).getGeneralDesc();
        if (generalDesc.hasNationalNumberPattern()) {
            return testNumberLengthAgainstPattern(this.regexCache.getPatternForRegex(generalDesc.getPossibleNumberPattern()), nationalSignificantNumber);
        }
        LOGGER.log(Level.FINER, "Checking if number is possible with incomplete metadata.");
        int length = nationalSignificantNumber.length();
        return length < 3 ? ValidationResult.TOO_SHORT : length > 15 ? ValidationResult.TOO_LONG : ValidationResult.IS_POSSIBLE;
    }

    public boolean isValidNumber(Phonenumber.PhoneNumber phoneNumber) {
        String regionCodeForNumber = getRegionCodeForNumber(phoneNumber);
        return isValidRegionCode(regionCodeForNumber) && isValidNumberForRegion(phoneNumber, regionCodeForNumber);
    }

    public boolean isValidNumberForRegion(Phonenumber.PhoneNumber phoneNumber, String str) {
        if (phoneNumber.getCountryCode() != getCountryCodeForRegion(str)) {
            return false;
        }
        Phonemetadata.PhoneMetadata metadataForRegion = getMetadataForRegion(str);
        Phonemetadata.PhoneNumberDesc generalDesc = metadataForRegion.getGeneralDesc();
        String nationalSignificantNumber = getNationalSignificantNumber(phoneNumber);
        if (generalDesc.hasNationalNumberPattern()) {
            return getNumberTypeHelper(nationalSignificantNumber, metadataForRegion) != PhoneNumberType.UNKNOWN;
        }
        int length = nationalSignificantNumber.length();
        return length > 3 && length <= 15;
    }

    /* access modifiers changed from: package-private */
    public int maybeExtractCountryCode(String str, Phonemetadata.PhoneMetadata phoneMetadata, StringBuffer stringBuffer, boolean z, Phonenumber.PhoneNumber phoneNumber) throws NumberParseException {
        if (str.length() == 0) {
            return 0;
        }
        StringBuffer stringBuffer2 = new StringBuffer(str);
        String str2 = "NonMatch";
        if (phoneMetadata != null) {
            str2 = phoneMetadata.getInternationalPrefix();
        }
        Phonenumber.PhoneNumber.CountryCodeSource maybeStripInternationalPrefixAndNormalize = maybeStripInternationalPrefixAndNormalize(stringBuffer2, str2);
        if (z) {
            phoneNumber.setCountryCodeSource(maybeStripInternationalPrefixAndNormalize);
        }
        if (maybeStripInternationalPrefixAndNormalize == Phonenumber.PhoneNumber.CountryCodeSource.FROM_DEFAULT_COUNTRY) {
            if (phoneMetadata != null) {
                int countryCode = phoneMetadata.getCountryCode();
                String valueOf = String.valueOf(countryCode);
                String stringBuffer3 = stringBuffer2.toString();
                if (stringBuffer3.startsWith(valueOf)) {
                    StringBuffer stringBuffer4 = new StringBuffer(stringBuffer3.substring(valueOf.length()));
                    Phonemetadata.PhoneNumberDesc generalDesc = phoneMetadata.getGeneralDesc();
                    Pattern patternForRegex = this.regexCache.getPatternForRegex(generalDesc.getNationalNumberPattern());
                    maybeStripNationalPrefixAndCarrierCode(stringBuffer4, phoneMetadata);
                    Pattern patternForRegex2 = this.regexCache.getPatternForRegex(generalDesc.getPossibleNumberPattern());
                    if ((!patternForRegex.matcher(stringBuffer2).matches() && patternForRegex.matcher(stringBuffer4).matches()) || testNumberLengthAgainstPattern(patternForRegex2, stringBuffer2.toString()) == ValidationResult.TOO_LONG) {
                        stringBuffer.append(stringBuffer4);
                        if (z) {
                            phoneNumber.setCountryCodeSource(Phonenumber.PhoneNumber.CountryCodeSource.FROM_NUMBER_WITHOUT_PLUS_SIGN);
                        }
                        phoneNumber.setCountryCode(countryCode);
                        return countryCode;
                    }
                }
            }
            phoneNumber.setCountryCode(0);
            return 0;
        } else if (stringBuffer2.length() < 3) {
            throw new NumberParseException(NumberParseException.ErrorType.TOO_SHORT_AFTER_IDD, "Phone number had an IDD, but after this was not long enough to be a viable phone number.");
        } else {
            int extractCountryCode = extractCountryCode(stringBuffer2, stringBuffer);
            if (extractCountryCode != 0) {
                phoneNumber.setCountryCode(extractCountryCode);
                return extractCountryCode;
            }
            throw new NumberParseException(NumberParseException.ErrorType.INVALID_COUNTRY_CODE, "Country calling code supplied was not recognised.");
        }
    }

    /* access modifiers changed from: package-private */
    public String maybeStripExtension(StringBuffer stringBuffer) {
        Matcher matcher = EXTN_PATTERN.matcher(stringBuffer);
        if (matcher.find() && isViablePhoneNumber(stringBuffer.substring(0, matcher.start()))) {
            int groupCount = matcher.groupCount();
            for (int i = 1; i <= groupCount; i++) {
                if (matcher.group(i) != null) {
                    String group = matcher.group(i);
                    stringBuffer.delete(matcher.start(), stringBuffer.length());
                    return group;
                }
            }
        }
        return "";
    }

    /* access modifiers changed from: package-private */
    public Phonenumber.PhoneNumber.CountryCodeSource maybeStripInternationalPrefixAndNormalize(StringBuffer stringBuffer, String str) {
        if (stringBuffer.length() == 0) {
            return Phonenumber.PhoneNumber.CountryCodeSource.FROM_DEFAULT_COUNTRY;
        }
        Matcher matcher = PLUS_CHARS_PATTERN.matcher(stringBuffer);
        if (matcher.lookingAt()) {
            stringBuffer.delete(0, matcher.end());
            normalize(stringBuffer);
            return Phonenumber.PhoneNumber.CountryCodeSource.FROM_NUMBER_WITH_PLUS_SIGN;
        }
        Pattern patternForRegex = this.regexCache.getPatternForRegex(str);
        if (parsePrefixAsIdd(patternForRegex, stringBuffer)) {
            normalize(stringBuffer);
            return Phonenumber.PhoneNumber.CountryCodeSource.FROM_NUMBER_WITH_IDD;
        }
        normalize(stringBuffer);
        return parsePrefixAsIdd(patternForRegex, stringBuffer) ? Phonenumber.PhoneNumber.CountryCodeSource.FROM_NUMBER_WITH_IDD : Phonenumber.PhoneNumber.CountryCodeSource.FROM_DEFAULT_COUNTRY;
    }

    /* access modifiers changed from: package-private */
    public String maybeStripNationalPrefixAndCarrierCode(StringBuffer stringBuffer, Phonemetadata.PhoneMetadata phoneMetadata) {
        String str = "";
        int length = stringBuffer.length();
        String nationalPrefixForParsing = phoneMetadata.getNationalPrefixForParsing();
        if (!(length == 0 || nationalPrefixForParsing.length() == 0)) {
            Matcher matcher = this.regexCache.getPatternForRegex(nationalPrefixForParsing).matcher(stringBuffer);
            if (matcher.lookingAt()) {
                Pattern patternForRegex = this.regexCache.getPatternForRegex(phoneMetadata.getGeneralDesc().getNationalNumberPattern());
                int groupCount = matcher.groupCount();
                String nationalPrefixTransformRule = phoneMetadata.getNationalPrefixTransformRule();
                if (nationalPrefixTransformRule != null && nationalPrefixTransformRule.length() != 0 && matcher.group(groupCount) != null) {
                    StringBuffer stringBuffer2 = new StringBuffer(stringBuffer);
                    stringBuffer2.replace(0, length, matcher.replaceFirst(nationalPrefixTransformRule));
                    if (patternForRegex.matcher(stringBuffer2.toString()).matches()) {
                        if (groupCount > 1) {
                            str = matcher.group(1);
                        }
                        stringBuffer.replace(0, stringBuffer.length(), stringBuffer2.toString());
                    }
                } else if (patternForRegex.matcher(stringBuffer.substring(matcher.end())).matches()) {
                    if (groupCount > 0 && matcher.group(groupCount) != null) {
                        str = matcher.group(1);
                    }
                    stringBuffer.delete(0, matcher.end());
                }
            }
        }
        return str;
    }

    public Phonenumber.PhoneNumber parse(String str, String str2) throws NumberParseException {
        Phonenumber.PhoneNumber phoneNumber = new Phonenumber.PhoneNumber();
        parse(str, str2, phoneNumber);
        return phoneNumber;
    }

    public void parse(String str, String str2, Phonenumber.PhoneNumber phoneNumber) throws NumberParseException {
        parseHelper(str, str2, false, true, phoneNumber);
    }

    public Phonenumber.PhoneNumber parseAndKeepRawInput(String str, String str2) throws NumberParseException {
        Phonenumber.PhoneNumber phoneNumber = new Phonenumber.PhoneNumber();
        parseAndKeepRawInput(str, str2, phoneNumber);
        return phoneNumber;
    }

    public void parseAndKeepRawInput(String str, String str2, Phonenumber.PhoneNumber phoneNumber) throws NumberParseException {
        parseHelper(str, str2, true, true, phoneNumber);
    }

    public boolean truncateTooLongNumber(Phonenumber.PhoneNumber phoneNumber) {
        if (isValidNumber(phoneNumber)) {
            return true;
        }
        Phonenumber.PhoneNumber phoneNumber2 = new Phonenumber.PhoneNumber();
        phoneNumber2.mergeFrom(phoneNumber);
        long nationalNumber = phoneNumber.getNationalNumber();
        do {
            nationalNumber /= 10;
            phoneNumber2.setNationalNumber(nationalNumber);
            if (isPossibleNumberWithReason(phoneNumber2) == ValidationResult.TOO_SHORT || nationalNumber == 0) {
                return false;
            }
        } while (!isValidNumber(phoneNumber2));
        phoneNumber.setNationalNumber(nationalNumber);
        return true;
    }
}
