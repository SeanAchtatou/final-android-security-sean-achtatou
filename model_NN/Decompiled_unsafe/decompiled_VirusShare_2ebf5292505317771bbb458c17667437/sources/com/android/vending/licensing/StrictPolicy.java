package com.android.vending.licensing;

import com.android.vending.licensing.Policy;

public class StrictPolicy implements Policy {
    private static final String TAG = "StrictPolicy";
    private Policy.LicenseResponse mLastResponse = Policy.LicenseResponse.RETRY;
    private PreferenceObfuscator mPreferences;

    public void processServerResponse(Policy.LicenseResponse response, ResponseData rawData) {
        this.mLastResponse = response;
    }

    public boolean allowAccess() {
        return this.mLastResponse == Policy.LicenseResponse.LICENSED;
    }
}
