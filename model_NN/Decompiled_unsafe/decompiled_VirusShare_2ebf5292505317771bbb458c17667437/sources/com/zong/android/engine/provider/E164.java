package com.zong.android.engine.provider;

import com.netqin.antivirus.Value;
import com.netqin.antivirus.appprotocol.AppConstantValue;
import com.netqin.antivirus.log.LogEngine;
import com.netqin.antivirus.networkmanager.NetApplication;
import com.netqin.antivirus.payment.PaymentHandler;
import com.netqin.antivirus.ui.TrafficFactory;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class E164 extends Enum<E164> {
    public static final E164 AD = new E164("AD", 0, 376);
    public static final E164 AE = new E164("AE", 1, 971);
    public static final E164 AF = new E164("AF", 2, 93);
    public static final E164 AG = new E164("AG", 3, 1);
    public static final E164 AL = new E164("AL", 4, 355);
    public static final E164 AM = new E164("AM", 5, 374);
    public static final E164 AN = new E164("AN", 6, 599);
    public static final E164 AO = new E164("AO", 7, 244);
    public static final E164 AQ = new E164("AQ", 8, 672);
    public static final E164 AR = new E164("AR", 9, 54, "0", new String[]{"11", "22", "23", "24", "26", "29", "33", "34", "35", "37", "38"}, 11, 14);
    public static final E164 AS = new E164("AS", 10, 684);
    public static final E164 AT = new E164("AT", 11, 43, "0", new String[]{"6"}, 11, 15);
    public static final E164 AU = new E164("AU", 12, 61, "0", new String[]{"4"}, 11, 11);
    public static final E164 AW = new E164("AW", 13, 297);
    public static final E164 AZ = new E164("AZ", 14, 994);
    public static final E164 BA = new E164("BA", 15, 387);
    public static final E164 BB = new E164("BB", 16, 1246);
    public static final E164 BD = new E164("BD", 17, 880);
    public static final E164 BE = new E164("BE", 18, 32, "0", new String[]{"4"}, 11, 11);
    public static final E164 BF = new E164("BF", 19, 226);
    public static final E164 BG = new E164("BG", 20, 359);
    public static final E164 BH = new E164("BH", 21, 973);
    public static final E164 BI = new E164("BI", 22, 257);
    public static final E164 BJ = new E164("BJ", 23, 229);
    public static final E164 BM = new E164("BM", 24, 1441);
    public static final E164 BN = new E164("BN", 25, 673);
    public static final E164 BO = new E164("BO", 26, 591);
    public static final E164 BR = new E164("BR", 27, 55, "0", null, 12, 12);
    public static final E164 BS = new E164("BS", 28, 1242);
    public static final E164 BT = new E164("BT", 29, 975);
    public static final E164 BW = new E164("BW", 30, 267);
    public static final E164 BY = new E164("BY", 31, 375);
    public static final E164 BZ = new E164("BZ", 32, 501);
    public static final E164 CA = new E164("CA", (byte) 0);
    public static final E164 CD = new E164("CD", 34, 243);
    public static final E164 CF = new E164("CF", 35, 236);
    public static final E164 CG = new E164("CG", 36, 242);
    public static final E164 CH = new E164("CH", 37, 41, "0", new String[]{"76", "77", "78", "79"}, 11, 11);
    public static final E164 CI = new E164("CI", 38, 225);
    public static final E164 CK = new E164("CK", 39, 682);
    public static final E164 CL = new E164("CL", 40, 56, null, new String[]{"92", "93", "94", "95", "96", "97", "98", "99"}, 8, 11);
    public static final E164 CM = new E164("CM", 41, 237);
    public static final E164 CN = new E164("CN", 42, 86);
    public static final E164 CO = new E164("CO", 43, 57, null, new String[]{NetApplication.DEFAULT_THRESHOLD, Value.SoftLanguage, "32"}, 12, 12, true);
    public static final E164 CR = new E164("CR", 44, 506);
    public static final E164 CU = new E164("CU", 45, 53);
    public static final E164 CV = new E164("CV", 46, 238);
    public static final E164 CY = new E164("CY", 47, 357);
    public static final E164 CZ = new E164("CZ", 48, 420, null, new String[]{"6", "7"}, 12, 12);
    public static final E164 DE = new E164("DE", 49, 49, "0", new String[]{"1"}, 12, 13);
    public static final E164 DJ = new E164("DJ", 50, 253);
    public static final E164 DK = new E164("DK", 51, 45, null, new String[]{"2", "3", "4", "5", "6", "7"}, 9, 10);
    public static final E164 DO = new E164("DO", 52, 1);
    public static final E164 DZ = new E164("DZ", 53, 213);
    public static final E164 EC = new E164("EC", 54, 593);
    public static final E164 EE = new E164("EE", 55, 372);
    public static final E164 EG = new E164("EG", 56, 20);
    public static final E164 ER = new E164("ER", 57, 291);
    public static final E164 ES = new E164("ES", 58, 34, null, new String[]{"6"}, 11, 11);
    public static final E164 ET = new E164("ET", 59, 251);
    public static final E164 FI = new E164("FI", 60, 358, "0", new String[]{"4", "5"}, 9, 14);
    public static final E164 FJ = new E164("FJ", 61, 679);
    public static final E164 FM = new E164("FM", 62, 691);
    public static final E164 FO = new E164("FO", 63, 298);
    public static final E164 FR = new E164("FR", 64, 33, "0", new String[]{"6", "7"}, 11, 11);
    public static final E164 GA = new E164("GA", 65, 241);
    public static final E164 GB = new E164("GB", 66, 44, "0", new String[]{"7"}, 12, 12);
    public static final E164 GE = new E164("GE", 67, 995);
    public static final E164 GF = new E164("GF", 68, 594);
    public static final E164 GH = new E164("GH", 69, 233);
    public static final E164 GI = new E164("GI", 70, 350);
    public static final E164 GL = new E164("GL", 71, 299);
    public static final E164 GM = new E164("GM", 72, 220);
    public static final E164 GN = new E164("GN", 73, 224);
    public static final E164 GP = new E164("GP", 74, 590);
    public static final E164 GQ = new E164("GQ", 75, TrafficFactory.SCREEN_WIDTH_240);
    public static final E164 GR = new E164("GR", 76, 30, null, new String[]{"693", "694", "695", "697", "698", "690", "699"}, 12, 12);
    public static final E164 GT = new E164("GT", 77, 502);
    public static final E164 GW = new E164("GW", 78, 245);
    public static final E164 GY = new E164("GY", 79, 592);
    public static final E164 HK = new E164("HK", 80, 852, null, null, 11, 11);
    public static final E164 HN = new E164("HN", 81, 504);
    public static final E164 HR = new E164("HR", 82, 385);
    public static final E164 HT = new E164("HT", 83, 509);
    public static final E164 HU = new E164("HU", 84, 36, "06", new String[]{"20", NetApplication.DEFAULT_THRESHOLD, "60", "70"}, 10, 11);
    public static final E164 ID = new E164("ID", 85, 62, null, new String[]{"2", "8"}, 11, 14);
    public static final E164 IE = new E164("IE", 86, 353, "0", new String[]{"8"}, 12, 13);
    public static final E164 IL = new E164("IL", 87, 972);
    public static final E164 IN = new E164("IN", 88, 91, "0", new String[]{"7", "8", "9"}, 12, 12);
    public static final E164 IQ = new E164("IQ", 89, 964);
    public static final E164 IR = new E164("IR", 90, 98);
    public static final E164 IS = new E164("IS", 91, 354);
    public static final E164 IT = new E164("IT", 92, 39, null, new String[]{"3"}, 11, 12);
    public static final E164 JM = new E164("JM", 93, 1);
    public static final E164 JO = new E164("JO", 94, 962);
    public static final E164 JP = new E164("JP", 95, 81);
    public static final E164 KE = new E164("KE", 96, 254);
    public static final E164 KG = new E164("KG", 97, 996);
    public static final E164 KH = new E164("KH", 98, 855);
    public static final E164 KI = new E164("KI", 99, 686);
    public static final E164 KM = new E164("KM", 100, 269);
    public static final E164 KP = new E164("KP", 101, 850);
    public static final E164 KR = new E164("KR", 102, 82);
    public static final E164 KW = new E164("KW", 103, 965);
    public static final E164 KY = new E164("KY", LogEngine.LOG_SCAN_SDCARD_END, 1345);
    public static final E164 KZ = new E164("KZ", 105, 7);
    public static final E164 LA = new E164("LA", AppConstantValue.subscribeFromAntiLost, 856);
    public static final E164 LB = new E164("LB", 107, 961);
    public static final E164 LI = new E164("LI", AppConstantValue.subscribeFromScanForeground, 423);
    public static final E164 LK = new E164("LK", AppConstantValue.subscribeFromScanBackground, 94);
    public static final E164 LR = new E164("LR", LogEngine.LOG_VIRUS_FOUND, 231);
    public static final E164 LS = new E164("LS", 111, 266);
    public static final E164 LT = new E164("LT", 112, 370);
    public static final E164 LU = new E164("LU", 113, 352);
    public static final E164 LV = new E164("LV", 114, 371);
    public static final E164 LY = new E164("LY", 115, 218);
    public static final E164 MA = new E164("MA", 116, 212);
    public static final E164 MC = new E164("MC", 117, 377);
    public static final E164 MD = new E164("MD", 118, 373);
    public static final E164 MG = new E164("MG", 119, 261);
    public static final E164 MK = new E164("MK", LogEngine.LOG_VIRUSDB_UPDATE_BEGIN, 389);
    public static final E164 ML = new E164("ML", LogEngine.LOG_VIRUSDB_UPDATE_SUCCESS, 223);
    public static final E164 MM = new E164("MM", LogEngine.LOG_VIRUSDB_UPDATE_FAIL, 95);
    public static final E164 MN = new E164("MN", 123, 976);
    public static final E164 MO = new E164("MO", 124, 853);
    public static final E164 MR = new E164("MR", 125, 222);
    public static final E164 MS = new E164("MS", 126, 1664);
    public static final E164 MT = new E164("MT", 127, 356);
    public static final E164 MU = new E164("MU", 128, 230);
    public static final E164 MV = new E164("MV", 129, TrafficFactory.SCREEN_HEIGHT_960);
    public static final E164 MW = new E164("MW", 130, 265);
    public static final E164 MX = new E164("MX", 131, 52, null, new String[]{"2", "3", "4", "5", "6", "7", "8", "9"}, 12, 12, true);
    public static final E164 MY = new E164("MY", 132, 60, "0", new String[]{"10", "11", "12", "13", "14", "15", "16", "17", "18", "19"}, 11, 12);
    public static final E164 MZ = new E164("MZ", 133, 258);
    public static final E164 NA = new E164("NA", 134, 264);
    public static final E164 NC = new E164("NC", 135, 687);
    public static final E164 NE = new E164("NE", 136, 227);
    public static final E164 NF = new E164("NF", 137, 672);
    public static final E164 NG = new E164("NG", 138, 234);
    public static final E164 NI = new E164("NI", 139, 505);
    public static final E164 NL = new E164("NL", 140, 31, "0", new String[]{"6"}, 11, 11);
    public static final E164 NO = new E164("NO", 141, 47, null, new String[]{"4", "9"}, 10, 12);
    public static final E164 NP = new E164("NP", 142, 977);
    public static final E164 NR = new E164("NR", 143, 674);
    public static final E164 NZ = new E164("NZ", 144, 64, "0", new String[]{"21", "22", "25", "26", "27", "28", "29"}, 10, 12);
    public static final E164 OM = new E164("OM", 145, 968);
    public static final E164 PA = new E164("PA", 146, 507);
    public static final E164 PE = new E164("PE", 147, 51, "0", new String[]{"9"}, 11, 11);
    public static final E164 PF = new E164("PF", 148, 689);
    public static final E164 PG = new E164("PG", 149, 675);
    public static final E164 PH = new E164("PH", 150, 63, "0", new String[]{"9"}, 11, 12);
    public static final E164 PK = new E164("PK", 151, 92);
    public static final E164 PL = new E164("PL", 152, 48, "0", new String[]{"5", "6", "7", "8"}, 11, 11);
    public static final E164 PM = new E164("PM", 153, 508);
    public static final E164 PS = new E164("PS", 154, 970);
    public static final E164 PT = new E164("PT", 155, 351, null, new String[]{"9"}, 12, 12);
    public static final E164 PY = new E164("PY", 156, 595);
    public static final E164 QA = new E164("QA", 157, 974);
    public static final E164 RE = new E164("RE", 158, 262);
    public static final E164 RO = new E164("RO", 159, 40);
    public static final E164 RU = new E164("RU", 160, 7, null, new String[]{"9"}, 11, 11);
    public static final E164 RW = new E164("RW", 161, 250);
    public static final E164 SA = new E164("SA", 162, 966);
    public static final E164 SB = new E164("SB", 163, 677);
    public static final E164 SC = new E164(Value.SC, 164, 248);
    public static final E164 SD = new E164("SD", 165, 249);
    public static final E164 SE = new E164("SE", 166, 46, "0", new String[]{"7"}, 11, 11);
    public static final E164 SG = new E164("SG", 167, 65, null, null, 9, 10);
    public static final E164 SI = new E164("SI", 168, 386);
    public static final E164 SK = new E164("SK", 169, 421);
    public static final E164 SL = new E164("SL", 170, 232);
    public static final E164 SM = new E164("SM", 171, 378);
    public static final E164 SN = new E164("SN", 172, 221);
    public static final E164 SO = new E164("SO", 173, 252);
    public static final E164 SR = new E164("SR", 174, 597);
    public static final E164 ST = new E164("ST", 175, 239);
    public static final E164 SV = new E164("SV", 176, 503);
    public static final E164 SY = new E164("SY", 177, 963);
    public static final E164 SZ = new E164("SZ", 178, 268);
    public static final E164 TC = new E164("TC", 179, 1649);
    public static final E164 TD = new E164("TD", 180, 235);
    public static final E164 TG = new E164("TG", 181, 228);
    public static final E164 TH = new E164("TH", 182, 66);
    public static final E164 TJ = new E164("TJ", 183, 992);
    public static final E164 TL = new E164("TL", 184, 670);
    public static final E164 TM = new E164("TM", 185, 993);
    public static final E164 TN = new E164("TN", 186, 216);
    public static final E164 TO = new E164("TO", 187, 676);
    public static final E164 TR = new E164("TR", 188, 90, null, new String[]{"50", "53", "54", "55"}, 12, 12);
    public static final E164 TT = new E164("TT", 189, 1868);
    public static final E164 TW = new E164("TW", 190, 886, "0", new String[]{"91", "92", "93", "95", "96", "97", "98"}, 11, 13);
    public static final E164 TZ = new E164("TZ", 191, 255);
    public static final E164 UA = new E164("UA", 192, 380);
    public static final E164 UG = new E164("UG", 193, 256);
    public static final E164 US = new E164("US");
    public static final E164 UY = new E164("UY", 195, 598);
    public static final E164 UZ = new E164("UZ", 196, 998);
    public static final E164 VA = new E164("VA", 197, 379);
    public static final E164 VE = new E164("VE", 198, 58, "0", new String[]{"412", "414", "424", "415", "416", "426", "417", "418"}, 12, 12);
    public static final E164 VN = new E164("VN", 199, 84);
    public static final E164 VU = new E164("VU", Value.PREF_CHANGE_MSG_VALUE, 678);
    public static final E164 WF = new E164("WF", 201, 681);
    public static final E164 WS = new E164("WS", 202, 685);
    public static final E164 YE = new E164("YE", 203, 967);
    public static final E164 ZA = new E164("ZA", 204, 27, "0", new String[]{"7", "8"}, 7, 14, true);
    public static final E164 ZM = new E164("ZM", 205, 260);
    public static final E164 ZW = new E164("ZW", 206, 263);
    private static final /* synthetic */ E164[] a;
    private final int countryPrefix;
    private final String localPrefix;
    private final int maxLength;
    private final int minLength;
    private final boolean needMno;
    private final String[] prefixes;

    static {
        E164[] e164Arr = new E164[PaymentHandler.STATUS_CANCALED];
        e164Arr[0] = AD;
        e164Arr[1] = AE;
        e164Arr[2] = AF;
        e164Arr[3] = AG;
        e164Arr[4] = AL;
        e164Arr[5] = AM;
        e164Arr[6] = AN;
        e164Arr[7] = AO;
        e164Arr[8] = AQ;
        e164Arr[9] = AR;
        e164Arr[10] = AS;
        e164Arr[11] = AT;
        e164Arr[12] = AU;
        e164Arr[13] = AW;
        e164Arr[14] = AZ;
        e164Arr[15] = BA;
        e164Arr[16] = BB;
        e164Arr[17] = BD;
        e164Arr[18] = BE;
        e164Arr[19] = BF;
        e164Arr[20] = BG;
        e164Arr[21] = BH;
        e164Arr[22] = BI;
        e164Arr[23] = BJ;
        e164Arr[24] = BM;
        e164Arr[25] = BN;
        e164Arr[26] = BO;
        e164Arr[27] = BR;
        e164Arr[28] = BS;
        e164Arr[29] = BT;
        e164Arr[30] = BW;
        e164Arr[31] = BY;
        e164Arr[32] = BZ;
        e164Arr[33] = CA;
        e164Arr[34] = CD;
        e164Arr[35] = CF;
        e164Arr[36] = CG;
        e164Arr[37] = CH;
        e164Arr[38] = CI;
        e164Arr[39] = CK;
        e164Arr[40] = CL;
        e164Arr[41] = CM;
        e164Arr[42] = CN;
        e164Arr[43] = CO;
        e164Arr[44] = CR;
        e164Arr[45] = CU;
        e164Arr[46] = CV;
        e164Arr[47] = CY;
        e164Arr[48] = CZ;
        e164Arr[49] = DE;
        e164Arr[50] = DJ;
        e164Arr[51] = DK;
        e164Arr[52] = DO;
        e164Arr[53] = DZ;
        e164Arr[54] = EC;
        e164Arr[55] = EE;
        e164Arr[56] = EG;
        e164Arr[57] = ER;
        e164Arr[58] = ES;
        e164Arr[59] = ET;
        e164Arr[60] = FI;
        e164Arr[61] = FJ;
        e164Arr[62] = FM;
        e164Arr[63] = FO;
        e164Arr[64] = FR;
        e164Arr[65] = GA;
        e164Arr[66] = GB;
        e164Arr[67] = GE;
        e164Arr[68] = GF;
        e164Arr[69] = GH;
        e164Arr[70] = GI;
        e164Arr[71] = GL;
        e164Arr[72] = GM;
        e164Arr[73] = GN;
        e164Arr[74] = GP;
        e164Arr[75] = GQ;
        e164Arr[76] = GR;
        e164Arr[77] = GT;
        e164Arr[78] = GW;
        e164Arr[79] = GY;
        e164Arr[80] = HK;
        e164Arr[81] = HN;
        e164Arr[82] = HR;
        e164Arr[83] = HT;
        e164Arr[84] = HU;
        e164Arr[85] = ID;
        e164Arr[86] = IE;
        e164Arr[87] = IL;
        e164Arr[88] = IN;
        e164Arr[89] = IQ;
        e164Arr[90] = IR;
        e164Arr[91] = IS;
        e164Arr[92] = IT;
        e164Arr[93] = JM;
        e164Arr[94] = JO;
        e164Arr[95] = JP;
        e164Arr[96] = KE;
        e164Arr[97] = KG;
        e164Arr[98] = KH;
        e164Arr[99] = KI;
        e164Arr[100] = KM;
        e164Arr[101] = KP;
        e164Arr[102] = KR;
        e164Arr[103] = KW;
        e164Arr[104] = KY;
        e164Arr[105] = KZ;
        e164Arr[106] = LA;
        e164Arr[107] = LB;
        e164Arr[108] = LI;
        e164Arr[109] = LK;
        e164Arr[110] = LR;
        e164Arr[111] = LS;
        e164Arr[112] = LT;
        e164Arr[113] = LU;
        e164Arr[114] = LV;
        e164Arr[115] = LY;
        e164Arr[116] = MA;
        e164Arr[117] = MC;
        e164Arr[118] = MD;
        e164Arr[119] = MG;
        e164Arr[120] = MK;
        e164Arr[121] = ML;
        e164Arr[122] = MM;
        e164Arr[123] = MN;
        e164Arr[124] = MO;
        e164Arr[125] = MR;
        e164Arr[126] = MS;
        e164Arr[127] = MT;
        e164Arr[128] = MU;
        e164Arr[129] = MV;
        e164Arr[130] = MW;
        e164Arr[131] = MX;
        e164Arr[132] = MY;
        e164Arr[133] = MZ;
        e164Arr[134] = NA;
        e164Arr[135] = NC;
        e164Arr[136] = NE;
        e164Arr[137] = NF;
        e164Arr[138] = NG;
        e164Arr[139] = NI;
        e164Arr[140] = NL;
        e164Arr[141] = NO;
        e164Arr[142] = NP;
        e164Arr[143] = NR;
        e164Arr[144] = NZ;
        e164Arr[145] = OM;
        e164Arr[146] = PA;
        e164Arr[147] = PE;
        e164Arr[148] = PF;
        e164Arr[149] = PG;
        e164Arr[150] = PH;
        e164Arr[151] = PK;
        e164Arr[152] = PL;
        e164Arr[153] = PM;
        e164Arr[154] = PS;
        e164Arr[155] = PT;
        e164Arr[156] = PY;
        e164Arr[157] = QA;
        e164Arr[158] = RE;
        e164Arr[159] = RO;
        e164Arr[160] = RU;
        e164Arr[161] = RW;
        e164Arr[162] = SA;
        e164Arr[163] = SB;
        e164Arr[164] = SC;
        e164Arr[165] = SD;
        e164Arr[166] = SE;
        e164Arr[167] = SG;
        e164Arr[168] = SI;
        e164Arr[169] = SK;
        e164Arr[170] = SL;
        e164Arr[171] = SM;
        e164Arr[172] = SN;
        e164Arr[173] = SO;
        e164Arr[174] = SR;
        e164Arr[175] = ST;
        e164Arr[176] = SV;
        e164Arr[177] = SY;
        e164Arr[178] = SZ;
        e164Arr[179] = TC;
        e164Arr[180] = TD;
        e164Arr[181] = TG;
        e164Arr[182] = TH;
        e164Arr[183] = TJ;
        e164Arr[184] = TL;
        e164Arr[185] = TM;
        e164Arr[186] = TN;
        e164Arr[187] = TO;
        e164Arr[188] = TR;
        e164Arr[189] = TT;
        e164Arr[190] = TW;
        e164Arr[191] = TZ;
        e164Arr[192] = UA;
        e164Arr[193] = UG;
        e164Arr[194] = US;
        e164Arr[195] = UY;
        e164Arr[196] = UZ;
        e164Arr[197] = VA;
        e164Arr[198] = VE;
        e164Arr[199] = VN;
        e164Arr[200] = VU;
        e164Arr[201] = WF;
        e164Arr[202] = WS;
        e164Arr[203] = YE;
        e164Arr[204] = ZA;
        e164Arr[205] = ZM;
        e164Arr[206] = ZW;
        a = e164Arr;
    }

    private E164(String str) {
        this(str, 194, 1, null, null, 11, 11, false);
    }

    private E164(String str, byte b) {
        this(str, 33, 1, null, null, 11, 11, true);
    }

    private E164(String str, int i, int i2) {
        this(str, i, i2, null, null, 0, 20, false);
    }

    private E164(String str, int i, int i2, String str2, String[] strArr, int i3, int i4) {
        this(str, i, i2, str2, strArr, i3, i4, false);
    }

    private E164(String str, int i, int i2, String str2, String[] strArr, int i3, int i4, boolean z) {
        this.countryPrefix = i2;
        this.localPrefix = str2;
        this.prefixes = strArr;
        this.minLength = i3;
        this.maxLength = i4;
        this.needMno = z;
    }

    public static E164 fromIsoCountryCode(String str) {
        return valueOf(str);
    }

    public static String getCountryPrefix(String str) {
        E164 e164 = getE164(str);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append('+');
        stringBuffer.append(e164.getCountryPrefix());
        return stringBuffer.toString();
    }

    public static E164 getE164(String str) {
        E164 valueOf = valueOf(str);
        if (valueOf != null) {
            return valueOf;
        }
        throw new IllegalArgumentException("country: " + str);
    }

    public static String getLocalPrefix(String str) {
        return getE164(str).getLocalPrefix();
    }

    public static E164 valueOf(String str) {
        return (E164) Enum.valueOf(E164.class, str);
    }

    public static E164[] values() {
        E164[] e164Arr = a;
        int length = e164Arr.length;
        E164[] e164Arr2 = new E164[length];
        System.arraycopy(e164Arr, 0, e164Arr2, 0, length);
        return e164Arr2;
    }

    public final int getCountryPrefix() {
        return this.countryPrefix;
    }

    public final String getLocalPrefix() {
        if (this.localPrefix == null || this.localPrefix.equals("")) {
            return null;
        }
        return this.localPrefix.trim();
    }

    public final int getMaxLength() {
        return this.maxLength;
    }

    public final int getMinLength() {
        return this.minLength;
    }

    public final int getPrefixLength() {
        if (getPrefixes() == null) {
            return 0;
        }
        return getPrefixes()[0].length();
    }

    public final String[] getPrefixes() {
        return this.prefixes;
    }

    public final boolean isNeedMno() {
        return this.needMno;
    }
}
