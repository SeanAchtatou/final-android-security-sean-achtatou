package com.zong.android.engine.process;

import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import com.netqin.antivirus.cloud.model.xml.XmlUtils;
import com.zong.android.engine.activities.ZongPaymentRequest;
import com.zong.android.engine.activities.ZongPricePointsElement;
import com.zong.android.engine.provider.PhoneState;
import com.zong.android.engine.provider.ZongPhoneManager;
import com.zong.android.engine.sms.ZongSmsReceiver;
import com.zong.android.engine.sms.a;
import com.zong.android.engine.task.PaymentProcessorTask;
import com.zong.android.engine.utils.d;
import com.zong.android.engine.utils.g;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.StringTokenizer;
import zongfuscated.C0003d;
import zongfuscated.I;
import zongfuscated.L;
import zongfuscated.n;
import zongfuscated.o;

public class ZongServiceProcess extends Service {
    /* access modifiers changed from: private */
    public static final String a = ZongServiceProcess.class.getSimpleName();
    private boolean b;
    /* access modifiers changed from: private */
    public boolean c;
    /* access modifiers changed from: private */
    public ContentValues d;
    private ZongSmsReceiver e;
    /* access modifiers changed from: private */
    public C0003d f;
    /* access modifiers changed from: private */
    public PaymentProcessorTask g;
    /* access modifiers changed from: private */
    public ZongPaymentRequest h = null;
    private StringBuilder i;
    private StringBuilder j;
    /* access modifiers changed from: private */
    public boolean k;
    /* access modifiers changed from: private */
    public Message l;
    /* access modifiers changed from: private */
    public Messenger m;
    /* access modifiers changed from: private */
    public int n;
    private Handler o = new Handler(new c(this));
    private Messenger p = new Messenger(this.o);
    /* access modifiers changed from: private */
    public Handler q = new Handler(new a(this));

    static /* synthetic */ void a(ContentValues contentValues, String str) {
        if (!d.a(str)) {
            StringTokenizer stringTokenizer = new StringTokenizer(str, "|");
            while (stringTokenizer.hasMoreElements()) {
                StringTokenizer stringTokenizer2 = new StringTokenizer((String) stringTokenizer.nextElement(), "=");
                contentValues.put((String) stringTokenizer2.nextElement(), (String) stringTokenizer2.nextElement());
            }
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        g.a(a, "Starting Application");
        if (!this.b) {
            this.b = true;
            this.f = new C0003d(this.h.getProcessingUrl());
            this.g = new PaymentProcessorTask(this, this.q, this.f);
            this.e = new ZongSmsReceiver(this.g.a());
            ZongSmsReceiver zongSmsReceiver = this.e;
            g.a(a, "Registring Handler");
            Context applicationContext = getApplicationContext();
            if (applicationContext != null) {
                try {
                    IntentFilter intentFilter = new IntentFilter();
                    intentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");
                    intentFilter.setPriority(999);
                    applicationContext.registerReceiver(zongSmsReceiver, intentFilter);
                } catch (Exception e2) {
                    g.a(a, "Failed to register SMS Receiver", e2);
                }
            } else {
                g.c(a, "Register failed due to missing context");
            }
            if (g.a()) {
                a.a().a(getApplicationContext());
            }
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        if (this.b) {
            g.a(a, "Stop Application");
            this.b = false;
            this.g.b();
            ZongSmsReceiver zongSmsReceiver = this.e;
            g.a(a, "Un-Registring Handler");
            if (zongSmsReceiver != null) {
                Context applicationContext = getApplicationContext();
                if (applicationContext != null) {
                    try {
                        applicationContext.unregisterReceiver(zongSmsReceiver);
                    } catch (Exception e2) {
                        g.a(a, "Failed to un-register SMS Receiver", e2);
                    }
                } else {
                    g.c(a, "Un-Register failed due to missing context");
                }
            } else {
                g.a(a, "Un-Register failed no receiver registered");
            }
            if (g.a()) {
                a.a().b(getApplicationContext());
            }
            this.g = null;
            this.e = null;
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        final L l2 = new L();
        l2.a(this.f);
        Thread thread = new Thread(new Runnable() {
            public final void run() {
                String str;
                HashMap hashMap = new HashMap();
                hashMap.put("lang", d.b(ZongServiceProcess.this.h.getLang()));
                hashMap.put(XmlUtils.LABEL_MOBILEINFO_COUNTRY, d.b(ZongServiceProcess.this.h.getCountry()));
                hashMap.put("msisdn", d.b(ZongServiceProcess.this.h.getPhoneNumber()));
                hashMap.put("operator", d.b(ZongServiceProcess.this.h.getMno()));
                hashMap.put("customerKey", d.b(ZongServiceProcess.this.h.getCustomerKey()));
                try {
                    o a2 = new n(ZongServiceProcess.this.f.a("/zongpay/actions/androgyny"), hashMap).a();
                    if (a2 != null) {
                        ArrayList<I> e = a2.a().e();
                        String str2 = null;
                        if (e != null) {
                            ZongServiceProcess.this.d = new ContentValues();
                            Iterator<I> it = e.iterator();
                            while (it.hasNext()) {
                                I next = it.next();
                                if ("state".equalsIgnoreCase(next.a())) {
                                    str2 = d.b(next.b()).trim();
                                } else {
                                    ZongServiceProcess.this.d.put(next.a(), d.b(next.b()).trim());
                                }
                            }
                            str = str2;
                        } else {
                            str = null;
                        }
                        l2.a(Boolean.valueOf("validate".equals(str)));
                        l2.a(ZongServiceProcess.this.d);
                        ZongServiceProcess.this.q.sendMessage(ZongServiceProcess.this.q.obtainMessage(4, l2));
                    }
                } catch (Exception e2) {
                    g.a(ZongServiceProcess.a, "ZongNativeView.applicationStart()", e2);
                }
            }
        });
        thread.setName("XML Page Loader");
        thread.setDaemon(true);
        thread.start();
    }

    /* access modifiers changed from: private */
    public void f() {
        final L l2 = new L();
        l2.a(this.f);
        this.i = null;
        this.j = null;
        ArrayList<ZongPricePointsElement> pricepointsList = this.h.getPricepointsList();
        int size = pricepointsList.size();
        if (size > 0) {
            NumberFormat currencyFormatter = this.h.getCurrencyFormatter();
            this.j = new StringBuilder(pricepointsList.get(0).c());
            this.i = new StringBuilder(currencyFormatter.format((double) pricepointsList.get(0).getAmount()));
            for (int i2 = 1; i2 < size; i2++) {
                this.j.append("|").append(pricepointsList.get(i2).c());
                this.i.append("|").append(currencyFormatter.format((double) pricepointsList.get(i2).getAmount()));
            }
        }
        l2.a(this.j.toString());
        l2.b(this.i.toString());
        Thread thread = new Thread(new Runnable() {
            public final void run() {
                HashMap hashMap = new HashMap();
                hashMap.put("lang", d.b(ZongServiceProcess.this.h.getLang()));
                hashMap.put(XmlUtils.LABEL_MOBILEINFO_COUNTRY, d.b(ZongServiceProcess.this.h.getCountry()));
                hashMap.put("msisdn", d.b(ZongServiceProcess.this.h.getPhoneNumber()));
                hashMap.put("operator", d.b(ZongServiceProcess.this.h.getMno()));
                hashMap.put("customerKey", d.b(ZongServiceProcess.this.h.getCustomerKey()));
                PhoneState phoneState = ZongPhoneManager.getInstance().getPhoneState(ZongServiceProcess.this);
                hashMap.put("lineNumber", d.b(phoneState.getLineNumber()));
                hashMap.put("imeaId", d.b(phoneState.getImeaId()));
                hashMap.put("netIsoCountry", d.b(phoneState.getNetIsoCountry()));
                hashMap.put("netOp", d.b(phoneState.getNetOp()));
                hashMap.put("netOpName", d.b(phoneState.getNetOpName()));
                hashMap.put("netType", d.b(phoneState.getNetType().toString()));
                hashMap.put("simIsoCountry", d.b(phoneState.getSimIsoCountry()));
                hashMap.put("simOp", d.b(phoneState.getSimOp()));
                hashMap.put("simOpName", d.b(phoneState.getSimOpName()));
                hashMap.put("subscribeId", d.b(phoneState.getSubsciberId()));
                hashMap.put("simSerial", d.b(phoneState.getSimSerial()));
                try {
                    o a2 = new n(ZongServiceProcess.this.f.a("/zongpay/actions/androgyne"), hashMap).a();
                    if (a2 != null) {
                        ArrayList<I> e = a2.a().e();
                        if (e != null) {
                            ZongServiceProcess.this.d = new ContentValues();
                            Iterator<I> it = e.iterator();
                            while (it.hasNext()) {
                                I next = it.next();
                                if ("msisdnValid".equalsIgnoreCase(next.a())) {
                                    ZongServiceProcess.this.c = Boolean.parseBoolean(next.b());
                                } else if ("errorMessages".equalsIgnoreCase(next.a())) {
                                    ZongServiceProcess.a(ZongServiceProcess.this.d, d.b(next.b()).trim());
                                } else {
                                    ZongServiceProcess.this.d.put(next.a(), d.b(next.b()).trim());
                                }
                            }
                            if (!ZongServiceProcess.this.c) {
                                ZongServiceProcess.this.h.setPhoneNumber(com.zong.android.engine.task.a.b(ZongServiceProcess.this.getApplicationContext(), ZongServiceProcess.this.h.getPhoneNumber()));
                            }
                        }
                        l2.a(Boolean.valueOf(ZongServiceProcess.this.c));
                        l2.a(ZongServiceProcess.this.d);
                        l2.c(a2.b());
                        ZongServiceProcess.this.q.sendMessage(ZongServiceProcess.this.q.obtainMessage(4, l2));
                    }
                } catch (Exception e2) {
                    g.a(ZongServiceProcess.a, "ZongWebView.applicationStart()", e2);
                }
            }
        });
        thread.setName("HTML Page Loader");
        thread.setDaemon(true);
        thread.start();
    }

    static /* synthetic */ void j(ZongServiceProcess zongServiceProcess) {
        g.a(a, "Stop And Go Application");
        zongServiceProcess.d();
        zongServiceProcess.c();
        if (zongServiceProcess.n == 1) {
            zongServiceProcess.f();
        } else {
            zongServiceProcess.e();
        }
    }

    public final void a() {
        if (this.e != null) {
            this.e.abortBroadcast();
            g.a(a, "Aborting SMS Broadcast");
        }
    }

    public IBinder onBind(Intent intent) {
        g.a(a, "LifeCycle Event: onBind");
        return this.p.getBinder();
    }

    public void onCreate() {
        super.onCreate();
        this.b = false;
        g.a(a, "LifeCycle Event: onCreate");
    }

    public void onDestroy() {
        super.onDestroy();
        g.a(a, "LifeCycle Event: onDestroy");
        d();
    }

    public void onRebind(Intent intent) {
        super.onRebind(intent);
        g.a(a, "LifeCycle Event: onRebind");
    }

    public boolean onUnbind(Intent intent) {
        g.a(a, "LifeCycle Event: onUnbind");
        return super.onUnbind(intent);
    }
}
