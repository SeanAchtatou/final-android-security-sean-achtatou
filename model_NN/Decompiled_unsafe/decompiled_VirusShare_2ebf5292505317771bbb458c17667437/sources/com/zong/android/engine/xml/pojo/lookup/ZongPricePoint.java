package com.zong.android.engine.xml.pojo.lookup;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class ZongPricePoint implements Serializable {
    private static final long serialVersionUID = 2370090782716445022L;
    String countryCode;
    float exchangeRate;
    ArrayList<ZongPricePointItem> items = new ArrayList<>();
    String localCurrency;
    HashMap<String, String> providers = new HashMap<>();

    public String getCountryCode() {
        return this.countryCode;
    }

    public float getExchangeRate() {
        return this.exchangeRate;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0044, code lost:
        r1.add(r4);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.ArrayList<com.zong.android.engine.xml.pojo.lookup.ZongPricePointItem> getFilteredItems(java.lang.String r5, boolean r6) {
        /*
            r4 = this;
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            java.util.ArrayList<com.zong.android.engine.xml.pojo.lookup.ZongPricePointItem> r0 = r4.items
            java.util.Iterator r2 = r0.iterator()
        L_0x000b:
            boolean r0 = r2.hasNext()
            if (r0 != 0) goto L_0x0012
            return r1
        L_0x0012:
            java.lang.Object r4 = r2.next()
            com.zong.android.engine.xml.pojo.lookup.ZongPricePointItem r4 = (com.zong.android.engine.xml.pojo.lookup.ZongPricePointItem) r4
            boolean r0 = r4.isZongPlusOnly()
            if (r0 != 0) goto L_0x000b
            boolean r0 = r4.isSupportedAllProviders()
            if (r0 != 0) goto L_0x0048
            java.util.HashMap r0 = r4.getProviders()
            java.util.Set r0 = r0.keySet()
            java.util.Iterator r3 = r0.iterator()
        L_0x0030:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x000b
            java.lang.Object r0 = r3.next()
            java.lang.String r0 = (java.lang.String) r0
            boolean r0 = r0.equals(r5)
            if (r0 != 0) goto L_0x0044
            if (r6 == 0) goto L_0x0030
        L_0x0044:
            r1.add(r4)
            goto L_0x000b
        L_0x0048:
            r1.add(r4)
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.zong.android.engine.xml.pojo.lookup.ZongPricePoint.getFilteredItems(java.lang.String, boolean):java.util.ArrayList");
    }

    public ArrayList<ZongPricePointItem> getItems() {
        return this.items;
    }

    public String getLocalCurrency() {
        return this.localCurrency;
    }

    public HashMap<String, String> getProviders() {
        return this.providers;
    }

    public void putProvider(String str, String str2) {
        this.providers.put(str, str2);
    }

    public void setCountryCode(String str) {
        this.countryCode = str;
    }

    public void setExchangeRate(float f) {
        this.exchangeRate = f;
    }

    public void setItems(ArrayList<ZongPricePointItem> arrayList) {
        this.items = arrayList;
    }

    public void setLocalCurrency(String str) {
        this.localCurrency = str;
    }
}
