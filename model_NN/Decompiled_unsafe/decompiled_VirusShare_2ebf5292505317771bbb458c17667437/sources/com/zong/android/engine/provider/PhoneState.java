package com.zong.android.engine.provider;

import android.telephony.TelephonyManager;
import com.google.i18n.phonenumbers.AsYouTypeFormatter;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.zong.android.engine.utils.d;
import com.zong.android.engine.utils.g;
import java.util.regex.Pattern;

public class PhoneState {
    private static final String a = PhoneState.class.getSimpleName();
    private static boolean b;
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;
    private Integer h;
    private Integer i;
    private String j;
    private String k;
    private String l;
    private String m;
    private String n;

    static {
        try {
            Class.forName("com.google.i18n.phonenumbers.PhoneNumberUtil");
            b = true;
        } catch (Exception e2) {
            b = false;
        }
    }

    public PhoneState(TelephonyManager telephonyManager) {
        this.c = telephonyManager.getDeviceId();
        this.d = telephonyManager.getLine1Number();
        this.e = telephonyManager.getNetworkCountryIso();
        this.f = telephonyManager.getNetworkOperator();
        this.g = telephonyManager.getNetworkOperatorName();
        this.h = Integer.valueOf(telephonyManager.getNetworkType());
        this.i = Integer.valueOf(telephonyManager.getPhoneType());
        this.j = telephonyManager.getSimCountryIso();
        this.k = telephonyManager.getSimOperator();
        this.l = telephonyManager.getSimOperatorName();
        this.m = telephonyManager.getSimSerialNumber();
        this.n = telephonyManager.getSubscriberId();
    }

    public String formatMobileNumber(String str, String str2) {
        if (!b) {
            return str2;
        }
        if (d.a(str2)) {
            return str2;
        }
        AsYouTypeFormatter asYouTypeFormatter = PhoneNumberUtil.getInstance().getAsYouTypeFormatter(str);
        asYouTypeFormatter.clear();
        String str3 = "";
        for (int i2 = 0; i2 < str2.length(); i2++) {
            str3 = asYouTypeFormatter.inputDigit(str2.charAt(i2));
        }
        return str3;
    }

    public String getImeaId() {
        return this.c;
    }

    public String getLineNumber() {
        return this.d;
    }

    @Deprecated
    public String getMsisdn() {
        String lineNumber = getLineNumber();
        return lineNumber != null ? !lineNumber.startsWith("+") ? "+" + lineNumber : lineNumber : "null";
    }

    public String getMsisdn(String str) {
        return b ? getMsisdnUseGoogle(getLineNumber(), str) : getMsisdnUseZong(getLineNumber(), str);
    }

    public String getMsisdn(String str, String str2) {
        return b ? getMsisdnUseGoogle(str, str2) : getMsisdnUseZong(str, str2);
    }

    /* access modifiers changed from: protected */
    public String getMsisdnUseGoogle(String str, String str2) {
        if (str == null) {
            return str;
        }
        try {
            PhoneNumberUtil instance = PhoneNumberUtil.getInstance();
            g.a(a, "(G) Starts parsing phone number");
            Phonenumber.PhoneNumber parse = instance.parse(str, str2);
            if (instance.isValidNumber(parse)) {
                String format = instance.format(parse, PhoneNumberUtil.PhoneNumberFormat.E164);
                g.a(a, "(G) Phone number matching ACCEPTED", format);
                return format;
            }
            g.a(a, "(G) Phone number matching REJECTED", str);
            g.a(a, "(Z) Using Zong Parser");
            return getMsisdnUseZong(str, str2);
        } catch (Exception e2) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public String getMsisdnUseZong(String str, String str2) {
        if (str == null) {
            return str;
        }
        try {
            g.a(a, "Phone Number has value", str);
            String replace = str.trim().replace(".", "").replace("-", "").replace("(", "").replace(")", "").replace("[", "").replace("]", "");
            g.a(a, "Phone Number after filtering", replace);
            if (!replace.startsWith("+")) {
                g.a(a, "Doesn't have a (+)");
                E164 fromIsoCountryCode = E164.fromIsoCountryCode(str2);
                if (replace.startsWith(Integer.toString(fromIsoCountryCode.getCountryPrefix()))) {
                    g.a(a, "Starts with country code");
                    replace = "+" + replace;
                } else {
                    g.a(a, "Doen't starts with country code");
                    String localPrefix = fromIsoCountryCode.getLocalPrefix();
                    if (localPrefix != null) {
                        g.a(a, "Local prefix for that country");
                        if (replace.startsWith(localPrefix)) {
                            g.a(a, "Starts with local prefix");
                            replace = String.valueOf(E164.getCountryPrefix(str2)) + replace.substring(localPrefix.length());
                        } else {
                            g.a(a, "Doesn't start with local prefix");
                            replace = String.valueOf(E164.getCountryPrefix(str2)) + replace;
                        }
                    } else {
                        g.a(a, "No local prefix for that country");
                        replace = String.valueOf(E164.getCountryPrefix(str2)) + replace;
                    }
                }
            }
            E164 e164 = E164.getE164(str2);
            String str3 = "(\\+[0-9]{" + e164.getMinLength() + "," + e164.getMaxLength() + "})";
            g.a(a, "Country and Matching PATTERN being validated", str2, str3);
            if (!Pattern.compile(str3).matcher(replace).find()) {
                g.a(a, "Final matching reject");
                return null;
            }
            g.a(a, "Final matching accept", replace);
            return replace;
        } catch (Exception e2) {
            return null;
        }
    }

    public String getNetIsoCountry() {
        return this.e;
    }

    public String getNetOp() {
        return this.f;
    }

    public String getNetOpName() {
        return this.g;
    }

    public Integer getNetType() {
        return this.h;
    }

    public Integer getPhoneType() {
        return this.i;
    }

    public String getSimIsoCountry() {
        return this.j;
    }

    public String getSimOp() {
        return this.k;
    }

    public String getSimOpName() {
        return this.l;
    }

    public String getSimSerial() {
        return this.m;
    }

    public String getSubsciberId() {
        return this.n;
    }

    public boolean isCDMA() {
        return 2 == this.i.intValue();
    }

    public boolean isGSM() {
        return 1 == this.i.intValue();
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("PHONE STATE:\n");
        sb.append(" imeaId(").append(this.c).append(")\n");
        sb.append(" lineNumber(").append(this.d).append(")\n");
        sb.append(" netIsoCountry(").append(this.e).append(")\n");
        sb.append(" netOp(").append(this.f).append(")\n");
        sb.append(" netOpName(").append(this.g).append(")\n");
        sb.append(" netType(").append(this.h).append(")\n");
        sb.append(" phoneType(").append(this.i).append(")\n");
        sb.append(" simIsoCountry(").append(this.j).append(")\n");
        sb.append(" simOp(").append(this.k).append(")\n");
        sb.append(" simOpName(").append(this.l).append(")\n");
        sb.append(" simSerial(").append(this.m).append(")\n");
        sb.append(" subsciberId(").append(this.n).append(")\n");
        return sb.toString();
    }
}
