package com.zong.android.engine.xml.pojo.lookup;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;

public class ZongPricePointItem implements Serializable {
    private static final long serialVersionUID = 7690478759703994783L;
    String entrypointUrl;
    float exactPrice;
    String itemRef;
    float outPayment;
    boolean priceMatched;
    HashMap<String, String> providers = null;
    String purchaseKey;
    float requestCurrencyOutpayment;
    boolean supportedAllProviders = true;
    String url;
    float workingPrice;
    boolean zongPlusOnly;

    public String getEntrypointUrl() {
        return this.entrypointUrl;
    }

    public float getExactPrice() {
        return this.exactPrice;
    }

    public String getItemRef() {
        return this.itemRef;
    }

    public float getOutPayment() {
        return this.outPayment;
    }

    public HashMap<String, String> getProviders() {
        return this.providers;
    }

    public String getPurchaseKey() {
        return this.purchaseKey;
    }

    public float getRequestCurrencyOutpayment() {
        return this.requestCurrencyOutpayment;
    }

    public String getUrl() {
        return this.url;
    }

    public float getWorkingPrice() {
        return this.workingPrice;
    }

    public boolean isPriceMatched() {
        return this.priceMatched;
    }

    public boolean isSupportedAllProviders() {
        return this.supportedAllProviders;
    }

    public boolean isZongPlusOnly() {
        return this.zongPlusOnly;
    }

    public void putProvider(String str, String str2) {
        if (this.providers == null) {
            this.supportedAllProviders = false;
            this.providers = new HashMap<>();
        }
        this.providers.put(str, str2);
    }

    public void setEntrypointUrl(String str) {
        this.entrypointUrl = str;
        int indexOf = this.entrypointUrl.indexOf("?purchaseKey=");
        int length = "?purchaseKey=".length() + indexOf;
        try {
            this.purchaseKey = URLDecoder.decode(this.entrypointUrl.substring(length), StringEncodings.UTF8);
        } catch (UnsupportedEncodingException e) {
            this.purchaseKey = URLDecoder.decode(this.entrypointUrl.substring(length));
        }
        this.url = this.entrypointUrl.substring(0, indexOf);
    }

    public void setExactPrice(float f) {
        this.exactPrice = f;
    }

    public void setItemRef(String str) {
        this.itemRef = str;
    }

    public void setOutPayment(float f) {
        this.outPayment = f;
    }

    public void setPriceMatched(boolean z) {
        this.priceMatched = z;
    }

    public void setPurchaseKey(String str) {
        this.purchaseKey = str;
    }

    public void setRequestCurrencyOutpayment(float f) {
        this.requestCurrencyOutpayment = f;
    }

    public void setUrl(String str) {
        this.url = str;
    }

    public void setWorkingPrice(float f) {
        this.workingPrice = f;
    }

    public void setZongPlusOnly(boolean z) {
        this.zongPlusOnly = z;
    }
}
