package com.zong.android.engine.utils;

import android.util.Log;

public final class g {
    private static boolean a = true;

    /* synthetic */ g() {
        this((byte) 0);
    }

    private g(byte b) {
    }

    public static void a(String str, String str2) {
        if (a) {
            Log.d(new StringBuilder(str.length() + 6).append("[[ ").append(str).append(" ]]").toString(), new StringBuilder(str2.length() + 6).append("<- ").append(str2).append(" ->").toString());
        }
    }

    public static void a(String str, String str2, Throwable th) {
        Log.e(str, str2, th);
    }

    public static void a(String str, String str2, String... strArr) {
        if (a) {
            StringBuilder append = new StringBuilder(str2.length() + 6).append("<- ").append(str2).append(" ->");
            StringBuilder append2 = new StringBuilder(str.length() + 6).append("[[ ").append(str).append(" ]]");
            StringBuilder sb = null;
            if (strArr.length > 0) {
                sb = new StringBuilder(strArr.length * 32);
                for (String append3 : strArr) {
                    sb.append(" | ").append(append3);
                }
            }
            Log.d(append2.toString(), sb != null ? append.toString().concat(sb.toString()) : append.toString());
        }
    }

    public static void a(boolean z) {
        a = z;
    }

    public static boolean a() {
        return a;
    }

    public static void b(String str, String str2) {
        Log.i(str, str2);
    }

    public static void c(String str, String str2) {
        Log.e(str, str2);
    }
}
