package com.zong.android.engine.task;

import android.content.Context;
import android.content.SharedPreferences;
import com.zong.android.engine.provider.PhoneState;
import com.zong.android.engine.provider.ZongPhoneManager;
import com.zong.android.engine.utils.g;

public class a {
    private static final String a = a.class.getSimpleName();

    /* synthetic */ a() {
        this((byte) 0);
    }

    private a(byte b) {
    }

    public static void a(Context context, String str) {
        g.a(a, "SAVING MSISDN " + str);
        SharedPreferences.Editor edit = context.getSharedPreferences("PhoneContext", 0).edit();
        edit.putString("msisdn", str);
        PhoneState phoneState = ZongPhoneManager.getInstance().getPhoneState(context);
        edit.putString("simcountry", phoneState.getSimIsoCountry());
        edit.putString("simserial", phoneState.getSimSerial());
        edit.putString("subscrid", phoneState.getSubsciberId());
        edit.commit();
    }

    public static String b(Context context, String str) {
        g.a(a, "GETTING MSISDN " + str);
        SharedPreferences sharedPreferences = context.getSharedPreferences("PhoneContext", 0);
        if (sharedPreferences == null) {
            return null;
        }
        PhoneState phoneState = ZongPhoneManager.getInstance().getPhoneState(context);
        String simIsoCountry = phoneState.getSimIsoCountry();
        String simSerial = phoneState.getSimSerial();
        String subsciberId = phoneState.getSubsciberId();
        String string = sharedPreferences.getString("simcountry", "");
        String string2 = sharedPreferences.getString("simserial", "");
        String string3 = sharedPreferences.getString("subscrid", "");
        if (!simIsoCountry.equals(string) || !simSerial.equals(string2) || !subsciberId.equals(string3)) {
            return null;
        }
        return sharedPreferences.getString("msisdn", str);
    }
}
