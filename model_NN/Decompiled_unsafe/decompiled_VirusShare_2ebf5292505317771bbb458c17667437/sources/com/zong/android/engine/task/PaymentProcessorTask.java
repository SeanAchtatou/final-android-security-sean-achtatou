package com.zong.android.engine.task;

import android.content.ContentValues;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.netqin.antivirus.antilost.SmsHandler;
import com.netqin.antivirus.networkmanager.NetApplication;
import com.zong.android.engine.ZpMoConst;
import com.zong.android.engine.activities.ZongPaymentRequest;
import com.zong.android.engine.process.ZongServiceProcess;
import com.zong.android.engine.utils.d;
import com.zong.android.engine.utils.e;
import com.zong.android.engine.utils.g;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import zongfuscated.C0001a;
import zongfuscated.C0002b;
import zongfuscated.C0003d;
import zongfuscated.C0006g;
import zongfuscated.C0011l;
import zongfuscated.F;
import zongfuscated.H;
import zongfuscated.I;
import zongfuscated.o;
import zongfuscated.q;
import zongfuscated.s;
import zongfuscated.z;

public class PaymentProcessorTask extends Thread {
    /* access modifiers changed from: private */
    public static final String b = PaymentProcessorTask.class.getSimpleName();
    ZongServiceProcess a;
    /* access modifiers changed from: private */
    public boolean c;
    private String d;
    /* access modifiers changed from: private */
    public String e;
    private String f;
    private e g;
    private boolean h;
    private boolean i;
    private HashSet<String> j;
    /* access modifiers changed from: private */
    public F k;
    private C0003d l;
    private ExecutorService m;
    private Future<?> n = null;
    /* access modifiers changed from: private */
    public LinkedBlockingQueue<C0006g> o;
    private final CountDownLatch p;
    private Handler q = null;
    private Handler r = null;
    private Context s;
    private String t = "";
    private int u = 0;
    private int v = 0;
    private long w = System.currentTimeMillis();

    enum ActionType {
        WAIT("wait"),
        SEND_KEYWORD("send-phone-info"),
        POLLING(NetApplication.SERVICE_POLLING),
        ENTER_PINCODE("enter-pincode-fr"),
        PROGRESS("progress"),
        SENDFLOW("sendflow"),
        MULTIMO("multi-mo"),
        SEND_PINCODE("send-pincode"),
        COMPLETED_TX("complete-transaction"),
        REDIRECT("redirect_client"),
        ERROR("show-error"),
        DEBUG("debug-view"),
        FB_PROGRESS("fb-progress"),
        FB_COMPLETED_TX("fb-complete-transaction"),
        FB_ERROR("fb-show-error"),
        FB_TIMEOUT("fb-timeout-transaction");
        
        private final String tag;

        private ActionType(String str) {
            this.tag = str;
        }

        public final boolean match(String str) {
            return this.tag.equals(str);
        }
    }

    enum Task {
        IMMEDIAT(0),
        TIME_OUT(300000),
        DELAY(4000),
        SMSDELAY(1000);
        
        private final long time;

        private Task(long j) {
            this.time = j;
        }

        public final long getTime() {
            return this.time;
        }
    }

    private static final class a implements ThreadFactory {
        /* synthetic */ a() {
            this((byte) 0);
        }

        private a(byte b) {
        }

        public final Thread newThread(Runnable runnable) {
            Thread thread = new Thread(runnable);
            thread.setDaemon(true);
            return thread;
        }
    }

    public PaymentProcessorTask(ZongServiceProcess zongServiceProcess, Handler handler, C0003d dVar) {
        super("PaymentProcessorTask");
        this.a = zongServiceProcess;
        this.s = zongServiceProcess.getApplicationContext();
        this.g = new e();
        this.f = this.s.getClass().getCanonicalName();
        this.g.a(this.f, this.s);
        this.q = handler;
        this.l = dVar;
        this.p = new CountDownLatch(1);
        start();
    }

    private static ContentValues a(String[]... strArr) {
        ContentValues contentValues = new ContentValues();
        for (String[] strArr2 : strArr) {
            contentValues.put(strArr2[0], strArr2[1]);
        }
        return contentValues;
    }

    /* access modifiers changed from: private */
    public void a(int i2, ContentValues contentValues) {
        boolean z = true;
        this.h = true;
        C0002b bVar = new C0002b(contentValues);
        bVar.a(i2);
        if (i2 != 0) {
            z = false;
        }
        bVar.a(Boolean.valueOf(z));
        a(3, bVar);
    }

    private void a(int i2, Object obj) {
        this.q.sendMessage(this.q.obtainMessage(i2, obj));
    }

    private void a(Message message, long j2) {
        if (this.h) {
            return;
        }
        if (j2 > 0) {
            this.r.sendMessageDelayed(message, j2);
        } else {
            this.r.sendMessage(message);
        }
    }

    /* access modifiers changed from: private */
    public void a(String str, String str2) {
        a(2, a(new String[]{"com.zong.view.result.source", "com.zong.result.source.halt"}, new String[]{"com.zong.view.result.error.param.code", str}, new String[]{"com.zong.view.result.error.param.label", str2}));
    }

    private void a(o oVar) {
        H a2 = oVar.a();
        g.a(b, "processPolling Action", a2.b(), a2.c());
        g.a(b, "processPolling Message", a2.d());
        a(2, new q(a2.d()));
        a(this.r.obtainMessage(3, 0, 0, oVar), Task.DELAY.getTime());
    }

    private void a(final z zVar, final String str, final o oVar) {
        this.n = this.m.submit(new Runnable() {
            public final void run() {
                try {
                    g.a(PaymentProcessorTask.b, "Waiting for SMS");
                    int i = 0;
                    boolean z = false;
                    while (!z && i < 3) {
                        int i2 = i + 1;
                        zVar.a((C0006g) PaymentProcessorTask.this.o.take());
                        g.a(PaymentProcessorTask.b, "Got a SMS in the Queue");
                        String a2 = zVar.a(PaymentProcessorTask.this.c, PaymentProcessorTask.this.e);
                        if (a2 != null) {
                            if (!PaymentProcessorTask.this.c) {
                                PaymentProcessorTask.this.a.a();
                            }
                            HashMap hashMap = new HashMap();
                            hashMap.put(str, a2);
                            g.a(PaymentProcessorTask.b, "Parse pincode", a2);
                            PaymentProcessorTask.this.k.a(oVar, hashMap);
                            g.a(PaymentProcessorTask.b, "Sent Exchange Message - DONE");
                            i = i2;
                            z = true;
                        } else {
                            i = i2;
                        }
                    }
                    if (!z) {
                        g.c(PaymentProcessorTask.b, "PANIC ERROR NO VALID SMS(3) ARRIVED");
                        PaymentProcessorTask.this.a("android.xml.payment.fatal.error", "sendSmsSyncPincode 3 Invalid SMS");
                    }
                    g.a(PaymentProcessorTask.b, "Found Valid SMS");
                } catch (InterruptedException e) {
                    g.a(PaymentProcessorTask.b, "queue.take", e);
                }
            }
        });
    }

    static /* synthetic */ boolean a(PaymentProcessorTask paymentProcessorTask, Message message) {
        if (paymentProcessorTask.h) {
            return true;
        }
        if (message.what == 0) {
            Bundle data = message.getData();
            String string = data.getString("originatingAddr");
            String string2 = data.getString(SmsHandler.BODY);
            paymentProcessorTask.o.add(new C0006g(string, string2));
            g.a(b, "processPremiumSMS (shortcode, body)", string, string2);
            return true;
        } else if (message.what == 2) {
            g.c(b, "processTimeout Action");
            paymentProcessorTask.a("android.xml.payment.process.timeout", "processTimeout Action");
            return true;
        } else if (message.what == 3) {
            o oVar = (o) message.obj;
            H a2 = oVar.a();
            g.a(b, "Executing Scheduled Action", a2.b(), a2.c());
            paymentProcessorTask.k.a(oVar, (HashMap<String, String>) null);
            return true;
        } else if (message.what != 4) {
            return false;
        } else {
            o oVar2 = (o) message.obj;
            if (oVar2 == null) {
                paymentProcessorTask.a("android.xml.payment.fatal.error", "process Empty Message");
            }
            String a3 = oVar2.a().a();
            if (a3.equals(paymentProcessorTask.t)) {
                paymentProcessorTask.v++;
                if (System.currentTimeMillis() - paymentProcessorTask.w < Task.access$2(Task.DELAY)) {
                    paymentProcessorTask.u++;
                    g.a(b, "Thresold broken for action", a3, Integer.toString(paymentProcessorTask.u));
                    if (paymentProcessorTask.u > 20) {
                        g.c(b, "Multi Action Threshold exhausted");
                        paymentProcessorTask.a("android.xml.payment.threshold.error", "Multi Action Threshold exhausted");
                    }
                }
            } else {
                paymentProcessorTask.t = a3;
                paymentProcessorTask.u = 0;
                paymentProcessorTask.v = 0;
            }
            if (paymentProcessorTask.v > 20) {
                g.c(b, "Multi Action DETECTED exhausted");
            }
            paymentProcessorTask.w = System.currentTimeMillis();
            if (ActionType.WAIT.match(a3)) {
                paymentProcessorTask.a(oVar2);
                return true;
            } else if (ActionType.SEND_KEYWORD.match(a3)) {
                H a4 = oVar2.a();
                g.a(b, "processPhoneInfo Action", a4.b(), a4.c());
                HashMap hashMap = new HashMap();
                hashMap.put("mno", paymentProcessorTask.d);
                g.a(b, "Sendig MNO to server", paymentProcessorTask.d);
                paymentProcessorTask.k.a(oVar2, hashMap);
                return true;
            } else if (ActionType.POLLING.match(a3)) {
                paymentProcessorTask.a(oVar2);
                return true;
            } else if (ActionType.ENTER_PINCODE.match(a3)) {
                paymentProcessorTask.b(oVar2);
                return true;
            } else if (ActionType.PROGRESS.match(a3)) {
                paymentProcessorTask.e(oVar2);
                return true;
            } else if (ActionType.SENDFLOW.match(a3)) {
                paymentProcessorTask.c(oVar2);
                return true;
            } else if (ActionType.MULTIMO.match(a3)) {
                paymentProcessorTask.d(oVar2);
                return true;
            } else if (ActionType.SEND_PINCODE.match(a3)) {
                paymentProcessorTask.f(oVar2);
                return true;
            } else if (ActionType.COMPLETED_TX.match(a3)) {
                paymentProcessorTask.g(oVar2);
                return true;
            } else if (ActionType.REDIRECT.match(a3)) {
                paymentProcessorTask.h(oVar2);
                return true;
            } else if (ActionType.ERROR.match(a3)) {
                paymentProcessorTask.j(oVar2);
                return true;
            } else if (ActionType.DEBUG.match(a3)) {
                paymentProcessorTask.i(oVar2);
                return true;
            } else if (ActionType.FB_PROGRESS.match(a3)) {
                H a5 = oVar2.a();
                g.a(b, "processFacebookInProgress Action", a5.b(), a5.c());
                g.a(b, "processFacebookInProgress Message", a5.d());
                paymentProcessorTask.a(2, new q(a5.d()));
                paymentProcessorTask.a(paymentProcessorTask.r.obtainMessage(3, 0, 0, oVar2), Task.DELAY.getTime());
                return true;
            } else if (ActionType.FB_COMPLETED_TX.match(a3)) {
                paymentProcessorTask.k(oVar2);
                return true;
            } else if (ActionType.FB_ERROR.match(a3)) {
                paymentProcessorTask.l(oVar2);
                return true;
            } else {
                if (ActionType.FB_TIMEOUT.match(a3)) {
                    paymentProcessorTask.m(oVar2);
                }
                return true;
            }
        }
    }

    /* access modifiers changed from: private */
    public void b(String str, String str2) {
        if (!this.c) {
            com.zong.android.engine.sms.a.a().a(str, str2);
            g.a(b, "Sent shortcode / keyword to server", str, str2);
        }
    }

    private void b(o oVar) {
        Integer num;
        String str;
        String str2;
        String str3;
        String str4 = null;
        H a2 = oVar.a();
        g.a(b, "processSendKeywordMessage Action", a2.b(), a2.c());
        g.a(b, "processSendKeywordMessage Message", a2.d());
        a(2, new q(a2.d()));
        ArrayList<I> e2 = a2.e();
        if (e2 != null) {
            Iterator<I> it = e2.iterator();
            Integer num2 = null;
            String str5 = null;
            String str6 = null;
            while (it.hasNext()) {
                I next = it.next();
                if ("shortcode".equalsIgnoreCase(next.a())) {
                    str4 = d.b(next.b()).trim();
                } else if ("keyword".equalsIgnoreCase(next.a())) {
                    str6 = d.b(next.b()).trim();
                } else if ("messageExpression".equalsIgnoreCase(next.a())) {
                    str5 = d.b(next.b()).trim();
                } else if ("groupId".equalsIgnoreCase(next.a())) {
                    num2 = Integer.valueOf(d.b(next.b()).trim());
                }
            }
            g.a(b, "processSendKeywordMessage Params ", str4, str6, str5, num2.toString());
            num = num2;
            str = str5;
            str2 = str6;
            str3 = str4;
        } else {
            num = null;
            str = null;
            str2 = null;
            str3 = null;
        }
        d();
        if (str3 == null || str2 == null || str == null || num == null) {
            a("android.xml.payment.data.error", "processSendKeywordMessage Missing params info");
            return;
        }
        g.a(b, "processSendKeywordMessage", "Sending Keyword SMS");
        b(str3, str2);
        a(new C0001a(num, str), "code", oVar);
        g.a(b, "Synchronizing on SMS for pincode code");
    }

    private void c(final String str, final String str2) {
        this.n = this.m.submit(new Runnable() {
            public final void run() {
                PaymentProcessorTask.this.b(str, str2);
                g.a(PaymentProcessorTask.b, "Keyword Reply Sent");
            }
        });
    }

    private void c(o oVar) {
        String str;
        String str2;
        H a2 = oVar.a();
        g.a(b, "processSendFlow Action", a2.b(), a2.c());
        g.a(b, "processSendFlow Message", a2.d());
        a(2, new q(a2.d()));
        if (this.i) {
            this.i = false;
            ArrayList<I> e2 = a2.e();
            if (e2 != null) {
                Iterator<I> it = e2.iterator();
                String str3 = null;
                String str4 = null;
                while (it.hasNext()) {
                    I next = it.next();
                    if ("shortcode".equalsIgnoreCase(next.a())) {
                        str4 = d.b(next.b()).trim();
                    } else if ("keyword".equalsIgnoreCase(next.a())) {
                        str3 = d.b(next.b()).trim();
                    }
                }
                g.a(b, "processSendFlow Params ", str4, str3);
                str = str3;
                str2 = str4;
            } else {
                str = null;
                str2 = null;
            }
            if (str2 == null || str == null) {
                a("android.xml.payment.data.error", "processSendFlow Missing params info");
            } else {
                c(str2, str);
            }
        }
        a(this.r.obtainMessage(3, 0, 0, oVar), Task.DELAY.getTime());
    }

    private void d() {
        if (this.c) {
            new Thread() {
                public final void run() {
                    try {
                        g.a(PaymentProcessorTask.b, "checkSimulationMode()", "Sleep on SMS Message", Long.valueOf(Task.SMSDELAY.getTime()).toString());
                        sleep(Task.SMSDELAY.getTime());
                        PaymentProcessorTask.this.o.put(new C0006g());
                        g.a(PaymentProcessorTask.b, "checkSimulationMode()", "Sending empty SMS Message");
                    } catch (InterruptedException e) {
                        g.a(PaymentProcessorTask.b, "Cannot insert into the Queue empty SMS Message", e);
                    }
                }
            }.start();
        }
    }

    private void d(o oVar) {
        String str;
        String str2;
        String str3;
        H a2 = oVar.a();
        g.a(b, "processMultiMoFlow Action", a2.b(), a2.c());
        g.a(b, "processMultiMoFlow Message", a2.d());
        a(2, new q(a2.d()));
        ArrayList<I> e2 = a2.e();
        if (e2 != null) {
            Iterator<I> it = e2.iterator();
            String str4 = null;
            String str5 = null;
            String str6 = null;
            while (it.hasNext()) {
                I next = it.next();
                if ("shortcode".equalsIgnoreCase(next.a())) {
                    str6 = d.b(next.b()).trim();
                } else if ("keyword".equalsIgnoreCase(next.a())) {
                    str5 = d.b(next.b()).trim();
                } else if ("currentMoIndex".equalsIgnoreCase(next.a())) {
                    str4 = d.b(next.b()).trim();
                }
            }
            g.a(b, "processMultiMoFlow Params ", str6, str5);
            str = str4;
            str2 = str5;
            str3 = str6;
        } else {
            str = null;
            str2 = null;
            str3 = null;
        }
        if (!this.j.contains(str)) {
            this.j.add(str);
            if (str3 == null || str2 == null) {
                a("android.xml.payment.data.error", "processSendFlow Missing params info");
            } else {
                c(str3, str2);
            }
        }
        a(this.r.obtainMessage(3, 0, 0, oVar), Task.DELAY.getTime());
    }

    private void e(o oVar) {
        final String str;
        final String str2;
        H a2 = oVar.a();
        g.a(b, "processCtaFlow Action", a2.b(), a2.c());
        g.a(b, "processCtaFlow Message", a2.d());
        a(2, new q(a2.d()));
        if (this.i) {
            this.i = false;
            ArrayList<I> e2 = a2.e();
            if (e2 != null) {
                Iterator<I> it = e2.iterator();
                String str3 = null;
                String str4 = null;
                while (it.hasNext()) {
                    I next = it.next();
                    if ("shortcode".equalsIgnoreCase(next.a())) {
                        str4 = d.b(next.b()).trim();
                    } else if ("keyword".equalsIgnoreCase(next.a())) {
                        str3 = d.b(next.b()).trim();
                    }
                }
                g.a(b, "processCtaFlow Params ", str4, str3);
                str = str3;
                str2 = str4;
            } else {
                str = null;
                str2 = null;
            }
            d();
            if (str2 == null || str == null) {
                a("android.xml.payment.data.error", "processCtaFlow Missing params info");
            } else {
                this.n = this.m.submit(new Runnable() {
                    public final void run() {
                        int i = 0;
                        try {
                            if (!PaymentProcessorTask.this.c) {
                                while (true) {
                                    int i2 = i;
                                    if (i2 < 6) {
                                        i = i2 + 1;
                                        g.a(PaymentProcessorTask.b, "Waiting for SMS");
                                        C0006g gVar = (C0006g) PaymentProcessorTask.this.o.take();
                                        if (gVar.a().equals(str2) && gVar.b().contains(str)) {
                                            PaymentProcessorTask.this.a.a();
                                            break;
                                        }
                                        g.a(PaymentProcessorTask.b, "No Match for SMS .... skipping", String.valueOf(i));
                                    } else {
                                        break;
                                    }
                                }
                                PaymentProcessorTask.this.b(str2, str);
                                g.a(PaymentProcessorTask.b, "Keyword Reply Sent");
                            }
                        } catch (InterruptedException e) {
                            g.a(PaymentProcessorTask.b, "queue.take", e);
                        }
                    }
                });
                g.a(b, "Synchronizing on SMS arrival for Reply");
            }
        }
        a(this.r.obtainMessage(3, 0, 0, oVar), Task.DELAY.getTime());
    }

    private void f(o oVar) {
        Integer num;
        String str;
        H a2 = oVar.a();
        g.a(b, "processSendPincode Action", a2.b(), a2.c());
        ArrayList<I> e2 = a2.e();
        if (e2 != null) {
            Iterator<I> it = e2.iterator();
            Integer num2 = null;
            String str2 = null;
            String str3 = null;
            while (it.hasNext()) {
                I next = it.next();
                if ("shortcode".equalsIgnoreCase(next.a())) {
                    str3 = d.b(next.b()).trim();
                } else if ("messageExpression".equalsIgnoreCase(next.a())) {
                    str2 = d.b(next.b()).trim();
                } else if ("groupId".equalsIgnoreCase(next.a())) {
                    num2 = Integer.valueOf(d.b(next.b()).trim());
                }
            }
            g.a(b, "processSendPincode Params ", str3, str2, num2.toString());
            num = num2;
            str = str2;
        } else {
            num = null;
            str = null;
        }
        d();
        if (str == null || num == null) {
            a("android.xml.payment.data.error", "processSendPincode Missing params info");
            return;
        }
        a(new s(num, str), "pincode", oVar);
        g.a(b, "Synchronizing on SMS for pincode");
    }

    private void g(o oVar) {
        H a2 = oVar.a();
        g.a(b, "processCompleteTransaction Action", a2.b(), a2.c());
        final ContentValues a3 = a(new String[]{"com.zong.view.result.source", "com.zong.result.source.success"}, new String[]{"com.zong.view.result.html", d.b(oVar.b()).trim()});
        ArrayList<I> e2 = a2.e();
        if (e2 != null) {
            Iterator<I> it = e2.iterator();
            while (it.hasNext()) {
                I next = it.next();
                a3.put("com.zong.view.result.success.param." + next.a(), d.b(next.b()).trim());
            }
        }
        g.a(b, "Completion exit enabled");
        this.k.a(oVar, new Runnable() {
            public final void run() {
                PaymentProcessorTask.this.a(0, a3);
            }
        });
    }

    private void h(o oVar) {
        H a2 = oVar.a();
        ContentValues a3 = a(new String[]{"com.zong.view.result.source", "com.zong.result.source.redirect"});
        ArrayList<I> e2 = a2.e();
        if (e2 != null) {
            Iterator<I> it = e2.iterator();
            while (it.hasNext()) {
                I next = it.next();
                g.a(b, "processRedirect Params ", next.a(), next.b());
            }
        }
        g.a(b, "Redirect exit enabled");
        a(0, a3);
    }

    private void i(o oVar) {
        H a2 = oVar.a();
        g.a(b, "processDebug Action", a2.b(), a2.c());
        ArrayList<I> e2 = a2.e();
        if (e2 != null) {
            Iterator<I> it = e2.iterator();
            while (it.hasNext()) {
                I next = it.next();
                g.a(b, "processDebug Params ", next.a(), next.b());
            }
        }
        a(this.r.obtainMessage(3, 0, 0, oVar), 0);
    }

    private void j(o oVar) {
        H a2 = oVar.a();
        g.a(b, "processError Action");
        ContentValues a3 = a(new String[]{"com.zong.view.result.source", "com.zong.result.source.error"}, new String[]{"com.zong.view.result.html", d.b(oVar.b()).trim()});
        ArrayList<I> e2 = a2.e();
        if (e2 != null) {
            Iterator<I> it = e2.iterator();
            while (it.hasNext()) {
                I next = it.next();
                a3.put("com.zong.view.result.error.param." + next.a(), d.b(next.b()).trim());
            }
        }
        a(1, a3);
    }

    private void k(o oVar) {
        H a2 = oVar.a();
        g.a(b, "processFacebookCompleteTransaction Action", a2.b(), a2.c());
        ContentValues a3 = a(new String[]{"com.zong.view.result.source", "com.zong.result.source.fb.success"});
        ArrayList<I> e2 = a2.e();
        if (e2 != null) {
            Iterator<I> it = e2.iterator();
            while (it.hasNext()) {
                I next = it.next();
                a3.put("com.zong.view.result.success.param." + next.a(), d.b(next.b()).trim());
            }
        }
        a(0, a3);
    }

    private void l(o oVar) {
        H a2 = oVar.a();
        g.a(b, "processFacebookError Action", a2.b(), a2.c());
        ContentValues a3 = a(new String[]{"com.zong.view.result.source", "com.zong.result.source.fb.error"});
        ArrayList<I> e2 = a2.e();
        if (e2 != null) {
            Iterator<I> it = e2.iterator();
            while (it.hasNext()) {
                I next = it.next();
                a3.put("com.zong.view.result.error.param." + next.a(), d.b(next.b()).trim());
            }
        }
        a(1, a3);
    }

    private void m(o oVar) {
        H a2 = oVar.a();
        g.a(b, "processFacebookTimeOut Action", a2.b(), a2.c());
        ContentValues a3 = a(new String[]{"com.zong.view.result.source", "com.zong.result.source.fb.error"});
        ArrayList<I> e2 = a2.e();
        if (e2 != null) {
            Iterator<I> it = e2.iterator();
            while (it.hasNext()) {
                I next = it.next();
                a3.put("com.zong.view.result.error.param." + next.a(), d.b(next.b()).trim());
            }
        }
        a(1, a3);
    }

    public final Handler a() {
        try {
            this.p.await();
        } catch (InterruptedException e2) {
        }
        return this.r;
    }

    public final void a(C0011l lVar) {
        g.b(b, "Starting Pay Service");
        if (this.g.a(this.f) != null) {
            this.q.sendEmptyMessage(1);
            this.h = false;
            this.i = true;
            this.j = new HashSet<>();
            ZongPaymentRequest a2 = lVar.a();
            this.c = a2.getSimulationMode().booleanValue();
            this.d = a2.getMno();
            this.e = a2.getPhoneNumber();
            HashMap hashMap = new HashMap();
            hashMap.put("purchaseKey", lVar.b());
            hashMap.put("transactionRef", a2.getTransactionRef());
            hashMap.put("itemDesc", lVar.c());
            hashMap.put("msisdn", a2.getPhoneNumber());
            hashMap.put("lang", a2.getLang());
            hashMap.put("userId", d.b(a2.getUserId()));
            hashMap.put("username", d.b(a2.getUserName()));
            hashMap.put("zongVersion", ZpMoConst.VERSION_ID);
            hashMap.put("androidVersion", Build.VERSION.RELEASE);
            hashMap.put("androidCode", String.valueOf(Build.VERSION.SDK_INT));
            this.k.a(hashMap);
            a(this.r.obtainMessage(2), Task.TIME_OUT.getTime());
            return;
        }
        g.c(b, "processPayment failed due to missing context");
        a("android.xml.payment.fatal.error", "processPayment failed due to missing context");
    }

    public final void b() {
        g.a(b, "Stopping Payment Processor");
        this.j = null;
        this.h = true;
        this.r.removeMessages(0);
        this.r.removeMessages(1);
        this.r.removeMessages(2);
        this.r.removeMessages(3);
        this.r.removeMessages(4);
        this.r.getLooper().quit();
        this.r = null;
        this.k.a();
        if (this.n != null) {
            this.n.cancel(true);
        }
        this.n = null;
        this.g.b(this.f);
        this.g = null;
        ExecutorService executorService = this.m;
        g.a(b, "Pool Shutdown");
        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(2, TimeUnit.SECONDS)) {
                executorService.shutdownNow();
                if (!executorService.awaitTermination(2, TimeUnit.SECONDS)) {
                    System.err.println("Pool did not terminate");
                }
            }
        } catch (InterruptedException e2) {
            executorService.shutdownNow();
            Thread.currentThread().interrupt();
        }
    }

    public void run() {
        Looper.prepare();
        this.r = new Handler() {
            public final void handleMessage(Message message) {
                if (!PaymentProcessorTask.a(PaymentProcessorTask.this, message)) {
                    super.handleMessage(message);
                }
            }
        };
        this.p.countDown();
        this.m = Executors.newCachedThreadPool(new a());
        this.k = new F(this.m, this.l, a());
        this.o = new LinkedBlockingQueue<>(8);
        Looper.loop();
    }
}
