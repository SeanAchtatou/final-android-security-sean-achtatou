package com.zong.android.engine.process;

import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import com.zong.android.engine.utils.g;

class a implements Handler.Callback {
    private /* synthetic */ ZongServiceProcess a;

    a(ZongServiceProcess zongServiceProcess) {
        this.a = zongServiceProcess;
    }

    public final boolean handleMessage(Message message) {
        Message obtain;
        switch (message.what) {
            case 1:
                obtain = Message.obtain(null, message.what, null);
                break;
            case 2:
                obtain = Message.obtain(null, message.what, message.obj);
                break;
            case 3:
                obtain = Message.obtain(null, message.what, message.obj);
                break;
            case 4:
                obtain = Message.obtain(null, message.what, message.obj);
                break;
            default:
                obtain = null;
                break;
        }
        this.a.l = obtain;
        if (!(this.a.k || this.a.m == null || obtain == null)) {
            g.a(ZongServiceProcess.a, "ReplyingTo Client", Integer.toString(message.what));
            try {
                this.a.m.send(obtain);
                this.a.l = (Message) null;
            } catch (RemoteException e) {
            }
        }
        return false;
    }
}
