package com.zong.android.engine.activities;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import com.zong.android.engine.ZpMoConst;
import com.zong.android.engine.provider.PhoneState;
import com.zong.android.engine.provider.ZongPhoneManager;
import com.zong.android.engine.utils.g;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;

public class ZongPaymentRequest implements Parcelable {
    public static final Parcelable.Creator<ZongPaymentRequest> CREATOR = new b();
    private static final String a = ZongPaymentRequest.class.getSimpleName();
    private Integer b;
    private Float c;
    private Boolean d;
    private Boolean e;
    private String f;
    private String g;
    private String h;
    private String i;
    private String j;
    private String k;
    private String l;
    private String m;
    private String n;
    private String o;
    private String p;
    private ArrayList<ZongPricePointsElement> q;

    public ZongPaymentRequest() {
        this.b = 0;
        this.c = Float.valueOf(0.0f);
        this.d = false;
        this.e = false;
        this.f = "ZongApp";
        this.h = "https://pay01.zong.com";
        this.i = "en";
        this.j = "US";
        this.k = "USD";
        this.q = null;
        this.q = new ArrayList<>();
    }

    /* synthetic */ ZongPaymentRequest(Parcel parcel) {
        this(parcel, (byte) 0);
    }

    private ZongPaymentRequest(Parcel parcel, byte b2) {
        this.b = 0;
        this.c = Float.valueOf(0.0f);
        this.d = false;
        this.e = false;
        this.f = "ZongApp";
        this.h = "https://pay01.zong.com";
        this.i = "en";
        this.j = "US";
        this.k = "USD";
        this.q = null;
        this.b = Integer.valueOf(parcel.readInt());
        this.c = Float.valueOf(parcel.readFloat());
        this.d = Boolean.valueOf(Boolean.parseBoolean(parcel.readString()));
        this.e = Boolean.valueOf(Boolean.parseBoolean(parcel.readString()));
        this.f = parcel.readString();
        this.g = parcel.readString();
        this.h = parcel.readString();
        this.i = parcel.readString();
        this.j = parcel.readString();
        this.k = parcel.readString();
        this.l = parcel.readString();
        this.m = parcel.readString();
        this.n = parcel.readString();
        this.o = parcel.readString();
        this.p = parcel.readString();
        ZongPricePointsElement[] zongPricePointsElementArr = (ZongPricePointsElement[]) ZongPricePointsElement.CREATOR.newArray(parcel.readInt());
        parcel.readTypedArray(zongPricePointsElementArr, ZongPricePointsElement.CREATOR);
        this.q = new ArrayList<>();
        for (ZongPricePointsElement add : zongPricePointsElementArr) {
            this.q.add(add);
        }
    }

    public static ZongPaymentRequest createPaymentRequest(Context context) {
        String str;
        ZongPaymentRequest zongPaymentRequest = new ZongPaymentRequest();
        PhoneState phoneState = ZongPhoneManager.getInstance().getPhoneState(context);
        if (phoneState.getSimIsoCountry() != null) {
            zongPaymentRequest.setCountry(phoneState.getSimIsoCountry().toUpperCase());
        }
        zongPaymentRequest.setMno(phoneState.getSimOp());
        try {
            str = phoneState.getMsisdn(zongPaymentRequest.getCountry());
        } catch (Exception e2) {
            str = null;
        }
        zongPaymentRequest.setPhoneNumber(str);
        return zongPaymentRequest;
    }

    public void addPricePoint(String str, float f2, int i2, String str2, String str3) {
        this.q.add(new ZongPricePointsElement(str, f2, i2, str2, str3));
    }

    public int describeContents() {
        return 0;
    }

    public void flushPricepointsList() {
        if (this.q != null) {
            this.q.clear();
        }
    }

    public String getAppName() {
        return this.f;
    }

    public String getCountry() {
        return this.j;
    }

    public String getCurrency() {
        return this.k;
    }

    public NumberFormat getCurrencyFormatter() {
        NumberFormat currencyInstance = NumberFormat.getCurrencyInstance(new Locale(getLang(), getCountry()));
        currencyInstance.setMinimumFractionDigits(2);
        currencyInstance.setMaximumFractionDigits(2);
        return currencyInstance;
    }

    public ZongPricePointsElement getCurrentPricePoint() {
        return this.q.get(this.b.intValue());
    }

    public String getCustomerKey() {
        return this.g;
    }

    public Boolean getDebugMode() {
        return this.e;
    }

    public String getHost() {
        return this.h;
    }

    public String getLang() {
        return this.i;
    }

    public String getLookupUrl() {
        return String.valueOf(this.h) + ZpMoConst.LOOKUP_URL;
    }

    public String getMno() {
        if (this.m != null) {
            return this.m;
        }
        if (getCountry().equalsIgnoreCase("CA")) {
            g.a(a, "Using MNO", "CA - Bell Mobility - 302610");
            return "302610";
        } else if (getCountry().equalsIgnoreCase("IT")) {
            g.a(a, "Using MNO", "IT - TIM - 22201");
            return "22201";
        } else if (getCountry().equalsIgnoreCase("ZA")) {
            g.a(a, "Using MNO", "ZA - Vodacom - 65501");
            return "65501";
        } else if (getCountry().equalsIgnoreCase("NZ")) {
            g.a(a, "Using MNO", "NZ - Telecom NZ - 53003");
            return "53003";
        } else if (getCountry().equalsIgnoreCase("SE")) {
            g.a(a, "Using MNO", "SE - Hi3G Access AB - 24002");
            return "24002";
        } else if (getCountry().equalsIgnoreCase("DK")) {
            g.a(a, "Using MNO", "DK - TDC Mobil - 23801");
            return "23801";
        } else {
            g.a(a, "Dummy MNO", "0123456789");
            return "0123456789";
        }
    }

    public String getPhoneNumber() {
        return this.l;
    }

    public Integer getPricePointsSelection() {
        return this.b;
    }

    public ArrayList<ZongPricePointsElement> getPricepointsList() {
        return this.q;
    }

    public String getProcessingUrl() {
        return String.valueOf(this.h) + ZpMoConst.PROCESSING_URL;
    }

    public Boolean getSimulationMode() {
        return this.d;
    }

    public String getTransactionRef() {
        return this.n;
    }

    public String getUserId() {
        return this.o;
    }

    public String getUserName() {
        return this.p;
    }

    public Float getVirtualCurrencyConv() {
        return this.c;
    }

    public void setAppName(String str) {
        this.f = str;
    }

    public void setCountry(String str) {
        this.j = str;
    }

    public void setCurrency(String str) {
        this.k = str;
    }

    public void setCustomerKey(String str) {
        this.g = str;
    }

    public void setDebugMode(Boolean bool) {
        this.e = bool;
    }

    public void setHost(String str) {
        this.h = str;
    }

    public void setLang(String str) {
        this.i = str;
    }

    public void setMno(String str) {
        this.m = str;
    }

    public void setPhoneNumber(String str) {
        this.l = str;
    }

    public void setPricePointsSelection(Integer num) {
        this.b = num;
    }

    public void setPricepointsList(ArrayList<ZongPricePointsElement> arrayList) {
        this.q = arrayList;
    }

    public void setSimulationMode(Boolean bool) {
        this.d = bool;
    }

    public void setTransactionRef(String str) {
        this.n = str;
    }

    public void setUrl(String str) {
        this.h = str.substring(0, str.indexOf(ZpMoConst.PROCESSING_URL));
    }

    public void setUserId(String str) {
        this.o = str;
    }

    public void setUserName(String str) {
        this.p = str;
    }

    public void setVirtualCurrencyConv(Float f2) {
        this.c = f2;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder("\n");
        sb.append("simulationMode: ").append(this.d).append("\n");
        sb.append("debugMode: ").append(this.e).append("\n");
        sb.append("appName: ").append(this.f).append("\n");
        sb.append("customerKey: ").append(this.g).append("\n");
        sb.append("host: ").append(this.h).append("\n");
        sb.append("lang: ").append(this.i).append("\n");
        sb.append("country: ").append(this.j).append("\n");
        sb.append("currency: ").append(this.k).append("\n");
        sb.append("phoneNumber: ").append(this.l).append("\n");
        sb.append("mno: ").append(this.m).append("\n");
        sb.append("transactionRef: ").append(this.n).append("\n");
        sb.append("userId: ").append(this.o).append("\n");
        sb.append("userName: ").append(this.p).append("\n");
        sb.append("Price Point List: ").append("\n");
        Iterator<ZongPricePointsElement> it = this.q.iterator();
        while (it.hasNext()) {
            sb.append("---------").append("\n");
            sb.append(it.next().toString()).append("\n");
        }
        return sb.toString();
    }

    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeInt(this.b.intValue());
        parcel.writeFloat(this.c.floatValue());
        parcel.writeString(this.d.toString());
        parcel.writeString(this.e.toString());
        parcel.writeString(this.f);
        parcel.writeString(this.g);
        parcel.writeString(this.h);
        parcel.writeString(this.i);
        parcel.writeString(this.j);
        parcel.writeString(this.k);
        parcel.writeString(this.l);
        parcel.writeString(this.m);
        parcel.writeString(this.n);
        parcel.writeString(this.o);
        parcel.writeString(this.p);
        int size = this.q.size();
        parcel.writeInt(size);
        parcel.writeTypedArray((ZongPricePointsElement[]) this.q.toArray(new ZongPricePointsElement[size]), i2);
    }
}
