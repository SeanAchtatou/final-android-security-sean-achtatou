package com.zong.android.engine.task;

import android.content.Context;
import android.content.SharedPreferences;
import com.netqin.antivirus.appprotocol.XmlTagValue;
import com.netqin.antivirus.cloud.model.xml.XmlUtils;
import com.zong.android.engine.ZpMoConst;
import com.zong.android.engine.activities.ZongPaymentRequest;
import com.zong.android.engine.utils.g;
import com.zong.android.engine.xml.pojo.lookup.ZongPricePoint;
import com.zong.android.engine.xml.pojo.lookup.ZongPricePointItem;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import zongfuscated.D;

public class PricePointTask {
    private static final String a = PricePointTask.class.getSimpleName();

    private static class a {
        /* access modifiers changed from: private */
        public static final PricePointTask a = new PricePointTask();

        private a() {
        }
    }

    /* synthetic */ PricePointTask() {
        this((byte) 0);
    }

    private PricePointTask(byte b) {
    }

    private static ZongPricePoint a(String str) {
        ZongPricePoint zongPricePoint;
        Exception e;
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(com.zong.android.engine.utils.a.a(str)));
            ZongPricePoint zongPricePoint2 = (ZongPricePoint) objectInputStream.readObject();
            try {
                objectInputStream.close();
                return zongPricePoint2;
            } catch (Exception e2) {
                e = e2;
                zongPricePoint = zongPricePoint2;
                g.a(a, "Failed extractFromBlob", e);
                return zongPricePoint;
            }
        } catch (Exception e3) {
            Exception exc = e3;
            zongPricePoint = null;
            e = exc;
            g.a(a, "Failed extractFromBlob", e);
            return zongPricePoint;
        }
    }

    private static String a(ZongPricePoint zongPricePoint) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
            objectOutputStream.writeObject(zongPricePoint);
            objectOutputStream.flush();
            objectOutputStream.close();
            return com.zong.android.engine.utils.a.a(byteArrayOutputStream.toByteArray());
        } catch (IOException e) {
            g.a(a, "Failed makeBlob", e);
            return null;
        }
    }

    public static PricePointTask getInstance() {
        return a.a;
    }

    public void flushCache(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("PricePointsCache", 0);
        if (sharedPreferences != null) {
            sharedPreferences.edit().clear().commit();
        }
    }

    public ArrayList<ZongPricePointItem> getFilteredPricePointsItems(Context context, ZongPaymentRequest zongPaymentRequest) {
        ZongPricePoint pricePoints = getPricePoints(context, true, zongPaymentRequest.getLookupUrl(), zongPaymentRequest.getCustomerKey(), zongPaymentRequest.getCountry(), zongPaymentRequest.getCurrency());
        return pricePoints != null ? pricePoints.getFilteredItems(zongPaymentRequest.getMno(), zongPaymentRequest.getSimulationMode().booleanValue()) : new ArrayList<>();
    }

    public ZongPricePoint getPricePoints(Context context, String str, String str2, String str3) {
        return getPricePoints(context, true, ZpMoConst.LOOKUP_URL, str, str2, str3);
    }

    public ZongPricePoint getPricePoints(Context context, String str, String str2, String str3, String str4) {
        return getPricePoints(context, true, str, str2, str3, str4);
    }

    public ZongPricePoint getPricePoints(Context context, boolean z, String str, String str2, String str3) {
        return getPricePoints(context, z, ZpMoConst.LOOKUP_URL, str, str2, str3);
    }

    public ZongPricePoint getPricePoints(Context context, boolean z, String str, String str2, String str3, String str4) {
        ZongPricePoint zongPricePoint = null;
        if (z) {
            g.a(a, "Enable PricePoints CACHE");
            SharedPreferences sharedPreferences = context.getSharedPreferences("PricePointsCache", 0);
            if (sharedPreferences != null) {
                if (System.currentTimeMillis() - Long.valueOf(sharedPreferences.getLong("time", 0)).longValue() < 21600000) {
                    String string = sharedPreferences.getString(XmlUtils.LABEL_MOBILEINFO_COUNTRY, "");
                    String string2 = sharedPreferences.getString("currencyCode", "");
                    String string3 = sharedPreferences.getString("clientRef", "");
                    String string4 = sharedPreferences.getString(XmlUtils.LABEL_REPORT_URL, "");
                    if (!string.equalsIgnoreCase(str3) || !string2.equalsIgnoreCase(str4) || !string3.equalsIgnoreCase(str2) || !string4.equalsIgnoreCase(str)) {
                        g.a(a, "Owner change:: Flushing PricePoints CACHE");
                    } else {
                        g.a(a, "Using PricePoints CACHE");
                        zongPricePoint = a(sharedPreferences.getString("pricePoints", null));
                    }
                } else {
                    g.a(a, "Time-to-Live expired:: Flushing PricePoints CACHE");
                }
            }
        } else {
            g.a(a, "Disable PricePoints CACHE");
        }
        if (zongPricePoint != null) {
            return zongPricePoint;
        }
        D d = new D(str, str2, str3, str4);
        ZongPricePoint zongPricePoint2 = zongPricePoint;
        int i = 0;
        while (zongPricePoint2 == null && i < 3) {
            int i2 = i + 1;
            ZongPricePoint a2 = d.a();
            if (a2 != null) {
                g.a(a, "Ctry", a2.getCountryCode());
                g.a(a, "Cur", a2.getLocalCurrency());
                for (String next : a2.getProviders().keySet()) {
                    g.a(a, "Root Prov Key", next, a2.getProviders().get(next));
                }
                Iterator<ZongPricePointItem> it = a2.getItems().iterator();
                while (it.hasNext()) {
                    ZongPricePointItem next2 = it.next();
                    g.a(a, "Ref", next2.getItemRef());
                    g.a(a, XmlTagValue.uRL, next2.getEntrypointUrl());
                    if (next2.getProviders() != null) {
                        for (String next3 : next2.getProviders().keySet()) {
                            g.a(a, "Item Key", next3, a2.getProviders().get(next3));
                        }
                    }
                }
                if (z) {
                    g.a(a, "Created PricePoints CACHE");
                    flushCache(context);
                    SharedPreferences.Editor edit = context.getSharedPreferences("PricePointsCache", 0).edit();
                    edit.putLong("time", System.currentTimeMillis());
                    edit.putString("clientRef", str2);
                    edit.putString(XmlUtils.LABEL_MOBILEINFO_COUNTRY, str3);
                    edit.putString("currencyCode", str4);
                    edit.putString(XmlUtils.LABEL_REPORT_URL, str);
                    edit.putString("pricePoints", a(a2));
                    edit.commit();
                }
            }
            i = i2;
            zongPricePoint2 = a2;
        }
        return zongPricePoint2;
    }
}
