package com.zong.android.engine.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import com.zong.android.engine.ZpMoConst;
import com.zong.android.engine.activities.ZongPaymentRequest;
import com.zong.android.engine.activities.ZongPricePointsElement;
import com.zong.android.engine.process.ZongActivityProcess;
import com.zong.android.engine.provider.PhoneState;
import com.zong.android.engine.provider.ZongPhoneManager;
import com.zong.android.engine.utils.g;
import java.text.NumberFormat;
import java.util.ArrayList;
import zongfuscated.C0002b;
import zongfuscated.L;
import zongfuscated.q;
import zongfuscated.y;

public class ZongUI extends ZongActivityProcess {
    private static final String d = ZongUI.class.getSimpleName();
    private static final Typeface e = Typeface.DEFAULT;
    private static /* synthetic */ boolean s = (!ZongUI.class.desiredAssertionStatus());
    /* access modifiers changed from: private */
    public final Handler f = new Handler();
    /* access modifiers changed from: private */
    public Bitmap[] g = null;
    private String h;
    /* access modifiers changed from: private */
    public TextView i = null;
    private ProgressDialog j = null;
    /* access modifiers changed from: private */
    public RadioButton[] k;
    /* access modifiers changed from: private */
    public TextView[] l;
    private c m = new c(this);
    private b n = new b(this);
    private a o = new a(this);
    private f p = new f(this);
    private d q = new d(this);
    private e r = new e(this);

    private class a implements View.OnClickListener {
        /* synthetic */ a(ZongUI zongUI) {
            this((byte) 0);
        }

        private a(byte b) {
        }

        public final void onClick(View view) {
            ZongUI.this.k();
        }
    }

    private class b implements View.OnClickListener {
        /* synthetic */ b(ZongUI zongUI) {
            this((byte) 0);
        }

        private b(byte b) {
        }

        public final void onClick(View view) {
            ZongUI.this.d();
        }
    }

    private class c implements View.OnClickListener {
        /* synthetic */ c(ZongUI zongUI) {
            this((byte) 0);
        }

        private c(byte b) {
        }

        public final void onClick(View view) {
            ZongUI.a(ZongUI.this);
        }
    }

    private class d implements View.OnClickListener {
        /* synthetic */ d(ZongUI zongUI) {
            this((byte) 0);
        }

        private d(byte b) {
        }

        public final void onClick(View view) {
            int i = 0;
            for (RadioButton radioButton : ZongUI.this.k) {
                if (radioButton != view) {
                    radioButton.setChecked(false);
                } else {
                    ZongUI.this.g().setPricePointsSelection(Integer.valueOf(i));
                }
                i++;
            }
        }
    }

    private class e implements View.OnClickListener {
        /* synthetic */ e(ZongUI zongUI) {
            this((byte) 0);
        }

        private e(byte b) {
        }

        public final void onClick(View view) {
            int i = 0;
            for (TextView textView : ZongUI.this.l) {
                if (textView != view) {
                    ZongUI.this.k[i].setChecked(false);
                } else {
                    ZongUI.this.k[i].setChecked(true);
                    ZongUI.this.g().setPricePointsSelection(Integer.valueOf(i));
                }
                i++;
            }
        }
    }

    private class f implements View.OnClickListener {
        /* synthetic */ f(ZongUI zongUI) {
            this((byte) 0);
        }

        private f(byte b) {
        }

        public final void onClick(View view) {
            ZongUI.this.c();
        }
    }

    static /* synthetic */ void a(ZongUI zongUI) {
        if (zongUI.g().getPhoneNumber() == null) {
            zongUI.k();
            return;
        }
        LinearLayout linearLayout = new LinearLayout(zongUI);
        linearLayout.setOrientation(1);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        linearLayout.setVerticalScrollBarEnabled(false);
        TableLayout tableLayout = new TableLayout(zongUI);
        tableLayout.setLayoutParams(new TableRow.LayoutParams(-2, -2));
        tableLayout.setColumnStretchable(1, true);
        TableRow tableRow = new TableRow(zongUI);
        tableRow.setLayoutParams(new TableRow.LayoutParams(-1, -1));
        tableRow.setWeightSum(1.0f);
        tableRow.setBaselineAligned(true);
        TextView textView = new TextView(zongUI);
        textView.setText(zongUI.a("ui.amount.title"));
        textView.setLayoutParams(new TableRow.LayoutParams(-2, -1));
        textView.setPadding(4, 0, 8, 0);
        textView.setGravity(16);
        textView.setTextSize(1, 18.0f);
        textView.setTypeface(Typeface.DEFAULT, 1);
        NumberFormat currencyFormatter = zongUI.g().getCurrencyFormatter();
        TextView textView2 = new TextView(zongUI);
        textView2.setText(currencyFormatter.format((double) zongUI.g().getCurrentPricePoint().getAmount()));
        textView2.setLayoutParams(new TableRow.LayoutParams(-2, -1));
        textView2.setPadding(4, 0, 8, 0);
        textView2.setGravity(16);
        textView2.setTextSize(1, 18.0f);
        textView2.setTypeface(Typeface.DEFAULT, 1);
        tableRow.addView(textView);
        tableRow.addView(textView2);
        tableLayout.addView(tableRow);
        linearLayout.addView(tableLayout);
        TextView textView3 = new TextView(zongUI);
        textView3.setText(zongUI.a("ui.tc"));
        textView3.setLayoutParams(new TableRow.LayoutParams(-2, -1));
        textView3.setPadding(4, 0, 8, 0);
        textView3.setGravity(16);
        textView3.setTextSize(1, 18.0f);
        textView3.setTypeface(Typeface.DEFAULT, 1);
        linearLayout.addView(textView3);
        TextView textView4 = new TextView(zongUI);
        textView4.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        textView4.setText(String.valueOf(zongUI.a("ui.tc.charges")) + zongUI.a("ui.tc.buying") + zongUI.a("ui.tc.support"));
        textView4.setPadding(12, 1, 10, 1);
        linearLayout.addView(textView4);
        AlertDialog.Builder builder = new AlertDialog.Builder(zongUI);
        builder.setView(linearLayout);
        builder.setTitle(zongUI.a("ui.confirm.title"));
        builder.setPositiveButton(zongUI.a("ui.confirm.accept"), new DialogInterface.OnClickListener() {
            public final void onClick(DialogInterface dialogInterface, int i) {
                ZongUI.this.b();
            }
        });
        builder.setNegativeButton(zongUI.a("ui.confirm.refuse"), new DialogInterface.OnClickListener() {
            public final void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        builder.show();
    }

    private Rect i() {
        Display defaultDisplay = ((WindowManager) getSystemService("window")).getDefaultDisplay();
        return new Rect(0, 0, defaultDisplay.getWidth(), defaultDisplay.getHeight());
    }

    /* access modifiers changed from: private */
    public void j() {
        final String[] strArr = {a("ui.logo.zong.url")};
        new Thread(new Runnable() {
            public final void run() {
                int length = strArr.length;
                ZongUI.this.g = new Bitmap[length];
                for (int i = 0; i < length; i++) {
                    try {
                        ZongUI.this.g[i] = com.zong.android.engine.utils.f.a(strArr[i]);
                    } catch (Exception e) {
                        ZongUI.this.g[i] = null;
                    }
                }
                ZongUI.this.f.post(new Runnable() {
                    public final void run() {
                        ZongUI.k(ZongUI.this);
                    }
                });
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public void k() {
        final ZongPaymentRequest g2 = g();
        final EditText editText = new EditText(this);
        editText.setInputType(3);
        editText.setText(g2.getPhoneNumber());
        editText.setHint(a("ui.dialog.edit.mobile.hint"));
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(editText);
        builder.setTitle(a("ui.dialog.edit.mobile.title"));
        builder.setMessage(a("ui.dialog.edit.mobile.message"));
        builder.setCancelable(false);
        builder.setPositiveButton(a("ui.ok"), new DialogInterface.OnClickListener() {
            public final void onClick(DialogInterface dialogInterface, int i) {
                String trim = com.zong.android.engine.utils.d.b(editText.getText().toString()).trim();
                if ("".equals(trim)) {
                    trim = null;
                }
                PhoneState phoneState = ZongPhoneManager.getInstance().getPhoneState(ZongUI.this);
                if (!g2.getSimulationMode().booleanValue()) {
                    String msisdn = phoneState.getMsisdn(trim, g2.getCountry());
                    if (msisdn != null) {
                        com.zong.android.engine.task.a.a(ZongUI.this, msisdn);
                        g2.setPhoneNumber(msisdn);
                    }
                    ZongUI.this.i.setText(phoneState.formatMobileNumber(ZongUI.this.g().getCountry(), g2.getPhoneNumber()));
                    return;
                }
                g2.setPhoneNumber(trim);
                com.zong.android.engine.task.a.a(ZongUI.this, trim);
                ZongUI.this.i.setText(phoneState.formatMobileNumber(ZongUI.this.g().getCountry(), trim));
            }
        });
        builder.setNegativeButton(a("ui.cancel"), new DialogInterface.OnClickListener() {
            public final void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    static /* synthetic */ void k(ZongUI zongUI) {
        String b2;
        Rect i2 = zongUI.i();
        float f2 = (float) (i2.right - i2.left);
        int i3 = (int) (((f2 - (0.95f * f2)) / 2.0f) + 0.5f);
        ScrollView scrollView = new ScrollView(zongUI);
        scrollView.setVerticalScrollBarEnabled(false);
        scrollView.setBackgroundColor(-1513240);
        LinearLayout linearLayout = new LinearLayout(zongUI);
        linearLayout.setOrientation(1);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        linearLayout.setVerticalScrollBarEnabled(false);
        linearLayout.setBackgroundColor(-1513240);
        TableLayout tableLayout = new TableLayout(zongUI);
        tableLayout.setLayoutParams(new TableRow.LayoutParams(-1, -2));
        tableLayout.setColumnStretchable(0, false);
        TableRow tableRow = new TableRow(zongUI);
        tableRow.setLayoutParams(new TableRow.LayoutParams(-1, -1));
        tableRow.setWeightSum(1.0f);
        tableRow.setBackgroundColor(-2490350);
        tableRow.setGravity(3);
        tableRow.setPadding(2, 2, 2, 2);
        if (zongUI.g[0] != null) {
            ImageView imageView = new ImageView(zongUI);
            imageView.setImageBitmap(zongUI.g[0]);
            imageView.setLayoutParams(new TableRow.LayoutParams(-2, -2));
            imageView.setPadding(i3, 2, 0, 2);
            tableRow.addView(imageView);
        } else {
            TextView textView = new TextView(zongUI);
            textView.setText(zongUI.a("ui.home.title"));
            textView.setPadding(10, 10, 10, 10);
            textView.setTypeface(e, 1);
            textView.setTextSize(1, 18.0f);
            textView.setTextColor(-1);
            textView.setBackgroundColor(-2490350);
            tableRow.addView(textView);
        }
        tableLayout.addView(tableRow);
        linearLayout.addView(tableLayout);
        TableLayout tableLayout2 = new TableLayout(zongUI);
        tableLayout2.setLayoutParams(new TableRow.LayoutParams(-2, -2));
        tableLayout2.setColumnStretchable(1, true);
        tableLayout2.setPadding(i3, 20, i3, 0);
        TableRow tableRow2 = new TableRow(zongUI);
        tableRow2.setLayoutParams(new TableRow.LayoutParams(-1, -1));
        tableRow2.setWeightSum(1.0f);
        tableRow2.setBackgroundColor(-3355444);
        tableRow2.setPadding(2, 2, 2, 2);
        tableRow2.setBaselineAligned(true);
        TextView textView2 = new TextView(zongUI);
        textView2.setText(zongUI.a("ui.home.billto"));
        textView2.setLayoutParams(new TableRow.LayoutParams(-2, -1));
        textView2.setTextColor(-16777216);
        textView2.setBackgroundColor(-1);
        textView2.setPadding(4, 0, 8, 0);
        textView2.setGravity(16);
        textView2.setTextSize(1, 18.0f);
        textView2.setTypeface(Typeface.DEFAULT, 1);
        if (zongUI.g().getPhoneNumber() != null) {
            b2 = zongUI.g().getPhoneNumber();
        } else {
            b2 = com.zong.android.engine.task.a.b(zongUI, null);
            if (b2 != null) {
                zongUI.g().setPhoneNumber(b2);
            }
        }
        String formatMobileNumber = ZongPhoneManager.getInstance().getPhoneState(zongUI).formatMobileNumber(zongUI.g().getCountry(), b2);
        zongUI.i = new TextView(zongUI);
        zongUI.i.setText(formatMobileNumber);
        zongUI.i.setLayoutParams(new TableRow.LayoutParams(-2, -1));
        zongUI.i.setTextColor(-16777216);
        zongUI.i.setTextSize(1, 18.0f);
        zongUI.i.setBackgroundColor(-1);
        zongUI.i.setPadding(2, 0, 2, 0);
        zongUI.i.setGravity(16);
        Button button = new Button(zongUI);
        button.setLayoutParams(new TableRow.LayoutParams(-2, -1));
        button.setGravity(17);
        button.setText(zongUI.a("ui.confirm.edit.mobile"));
        button.setOnClickListener(zongUI.o);
        tableRow2.addView(textView2);
        tableRow2.addView(zongUI.i);
        tableRow2.addView(button);
        tableLayout2.addView(tableRow2);
        linearLayout.addView(tableLayout2);
        TextView textView3 = new TextView(zongUI);
        textView3.setText(zongUI.a("ui.home.package"));
        textView3.setTextColor(-16777216);
        textView3.setPadding(i3, 10, 0, 10);
        textView3.setTextSize(1, 18.0f);
        textView3.setTypeface(Typeface.DEFAULT, 1);
        linearLayout.addView(textView3);
        TableLayout tableLayout3 = new TableLayout(zongUI);
        tableLayout3.setLayoutParams(new TableRow.LayoutParams(-2, -2));
        tableLayout3.setColumnStretchable(0, true);
        tableLayout3.setPadding(i3, 0, i3, 0);
        ArrayList<ZongPricePointsElement> pricepointsList = zongUI.g().getPricepointsList();
        int size = pricepointsList.size();
        int i4 = size - 1;
        zongUI.l = new TextView[size];
        zongUI.k = new RadioButton[size];
        for (int i5 = 0; i5 < size; i5++) {
            ZongPricePointsElement zongPricePointsElement = pricepointsList.get(i5);
            TableRow tableRow3 = new TableRow(zongUI);
            tableRow3.setLayoutParams(new TableRow.LayoutParams(-1, -1));
            tableRow3.setWeightSum(1.0f);
            tableRow3.setBackgroundColor(-3355444);
            if (size <= 1) {
                tableRow3.setPadding(2, 2, 2, 2);
            } else if (i5 == 0) {
                tableRow3.setPadding(2, 2, 2, 1);
            } else if (i5 == i4) {
                tableRow3.setPadding(2, 1, 2, 2);
            } else {
                tableRow3.setPadding(2, 1, 2, 1);
            }
            String c2 = zongPricePointsElement.c().length() > 30 ? String.valueOf(zongPricePointsElement.c().substring(0, 25)) + " ... " : zongPricePointsElement.c();
            TextView textView4 = new TextView(zongUI);
            textView4.setText(c2);
            textView4.setTextColor(-16777216);
            textView4.setTextSize(1, 16.0f);
            textView4.setLayoutParams(new TableRow.LayoutParams(-2, -1));
            textView4.setBackgroundColor(-1);
            textView4.setPadding(4, 0, 0, 0);
            textView4.setGravity(16);
            textView4.setOnClickListener(zongUI.r);
            zongUI.l[i5] = textView4;
            RadioButton radioButton = new RadioButton(zongUI);
            radioButton.setText("   ");
            radioButton.setLayoutParams(new TableRow.LayoutParams(-2, -1));
            radioButton.setBackgroundColor(-1);
            radioButton.setOnClickListener(zongUI.q);
            zongUI.k[i5] = radioButton;
            if (i5 == 0) {
                radioButton.setChecked(true);
            }
            tableRow3.addView(textView4);
            tableRow3.addView(radioButton);
            tableLayout3.addView(tableRow3);
        }
        linearLayout.addView(tableLayout3);
        TableLayout tableLayout4 = new TableLayout(zongUI);
        tableLayout4.setLayoutParams(new TableRow.LayoutParams(-1, -1));
        tableLayout4.setPadding(i3, 10, i3, 0);
        tableLayout4.setColumnStretchable(0, true);
        tableLayout4.setColumnStretchable(1, true);
        TableRow tableRow4 = new TableRow(zongUI);
        tableRow4.setLayoutParams(new TableRow.LayoutParams(-1, -1));
        tableRow4.setWeightSum(1.0f);
        Button button2 = new Button(zongUI);
        button2.setText(zongUI.a("ui.home.buy"));
        button2.setOnClickListener(zongUI.m);
        button2.setLayoutParams(new TableRow.LayoutParams(-1, -1, 0.5f));
        button2.setTextColor(-1);
        button2.setGravity(17);
        button2.setTypeface(Typeface.DEFAULT, 1);
        button2.setTextSize(1, 18.0f);
        button2.getBackground().setColorFilter(-1376236, PorterDuff.Mode.MULTIPLY);
        tableRow4.addView(button2);
        Button button3 = new Button(zongUI);
        button3.setText(zongUI.a("ui.cancel"));
        button3.setOnClickListener(zongUI.n);
        button3.setLayoutParams(new TableRow.LayoutParams(-1, -1, 0.5f));
        button3.setTextColor(-13421773);
        button3.setGravity(17);
        button3.setTextSize(1, 18.0f);
        button3.setTypeface(Typeface.DEFAULT, 1);
        tableRow4.addView(button3);
        tableLayout4.addView(tableRow4);
        linearLayout.addView(tableLayout4);
        scrollView.addView(linearLayout);
        zongUI.setContentView(scrollView);
    }

    public final void a(L l2) {
        super.a(l2);
        if (l2.a().booleanValue()) {
            j();
            return;
        }
        String a2 = a("ui.error.carrier.down.title");
        new AlertDialog.Builder(this).setTitle(a2).setMessage(a("ui.error.carrier.down.msg")).setPositiveButton(a("ui.close"), new DialogInterface.OnClickListener() {
            public final void onClick(DialogInterface dialogInterface, int i) {
                ZongUI.this.d();
            }
        }).setNegativeButton(a("ui.continue"), new DialogInterface.OnClickListener() {
            public final void onClick(DialogInterface dialogInterface, int i) {
                ZongUI.this.j();
            }
        }).show();
    }

    public final void a(C0002b bVar) {
        super.a(bVar);
        if (this.j != null) {
            this.j.dismiss();
            this.j = null;
        }
        if (bVar.a()) {
            g.a(d, "Payment Status: OK");
            y e2 = bVar.e();
            Rect i2 = i();
            float f2 = (float) (i2.right - i2.left);
            int i3 = (int) (((f2 - (0.95f * f2)) / 2.0f) + 0.5f);
            ScrollView scrollView = new ScrollView(this);
            scrollView.setVerticalScrollBarEnabled(false);
            scrollView.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
            scrollView.setBackgroundColor(-1513240);
            LinearLayout linearLayout = new LinearLayout(this);
            linearLayout.setOrientation(1);
            linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
            linearLayout.setVerticalScrollBarEnabled(false);
            linearLayout.setBackgroundColor(-1513240);
            TableLayout tableLayout = new TableLayout(this);
            tableLayout.setLayoutParams(new TableRow.LayoutParams(-1, -1));
            tableLayout.setColumnStretchable(0, false);
            TableRow tableRow = new TableRow(this);
            tableRow.setLayoutParams(new TableRow.LayoutParams(-1, -1));
            tableRow.setWeightSum(1.0f);
            tableRow.setBackgroundColor(-2490350);
            tableRow.setGravity(3);
            tableRow.setPadding(2, 2, 2, 2);
            if (this.g[0] != null) {
                ImageView imageView = new ImageView(this);
                imageView.setImageBitmap(this.g[0]);
                imageView.setLayoutParams(new TableRow.LayoutParams(-2, -2));
                imageView.setPadding(i3, 2, 0, 2);
                tableRow.addView(imageView);
            } else {
                TextView textView = new TextView(this);
                textView.setText(a("ui.home.title"));
                textView.setPadding(10, 10, 10, 10);
                textView.setTypeface(e, 1);
                textView.setTextSize(1, 18.0f);
                textView.setTextColor(-1);
                textView.setBackgroundColor(-2490350);
                tableRow.addView(textView);
            }
            tableLayout.addView(tableRow);
            linearLayout.addView(tableLayout);
            TableLayout tableLayout2 = new TableLayout(this);
            tableLayout2.setLayoutParams(new TableRow.LayoutParams(-2, -2));
            tableLayout2.setColumnStretchable(0, true);
            TableRow tableRow2 = new TableRow(this);
            tableRow2.setLayoutParams(new TableRow.LayoutParams(-1, -1));
            tableRow2.setWeightSum(1.0f);
            tableRow2.setPadding(2, 2, 2, 2);
            TextView textView2 = new TextView(this);
            textView2.setText(e2.c());
            textView2.setLayoutParams(new TableRow.LayoutParams(-2, -1));
            textView2.setTextColor(-16777216);
            textView2.setTypeface(Typeface.DEFAULT, 1);
            textView2.setTextSize(1, 18.0f);
            textView2.setPadding(10, 10, 0, 10);
            textView2.setGravity(3);
            tableRow2.addView(textView2);
            tableLayout2.addView(tableRow2);
            linearLayout.addView(tableLayout2);
            TableLayout tableLayout3 = new TableLayout(this);
            tableLayout3.setLayoutParams(new TableRow.LayoutParams(-2, -2));
            tableLayout3.setColumnStretchable(0, true);
            TableRow tableRow3 = new TableRow(this);
            tableRow3.setLayoutParams(new TableRow.LayoutParams(-1, -1));
            tableRow3.setWeightSum(1.0f);
            tableRow3.setPadding(2, 2, 2, 2);
            TextView textView3 = new TextView(this);
            textView3.setText(e2.f());
            textView3.setLayoutParams(new TableRow.LayoutParams(-2, -1));
            textView3.setTextColor(-16777216);
            textView3.setTextSize(1, 16.0f);
            textView3.setPadding(10, 10, 5, 10);
            textView3.setGravity(3);
            tableRow3.addView(textView3);
            tableLayout3.addView(tableRow3);
            linearLayout.addView(tableLayout3);
            TableLayout tableLayout4 = new TableLayout(this);
            tableLayout4.setLayoutParams(new TableRow.LayoutParams(-1, -1));
            tableLayout4.setColumnStretchable(1, true);
            tableLayout4.setPadding(10, 10, 10, 10);
            TableRow tableRow4 = new TableRow(this);
            tableRow4.setLayoutParams(new TableRow.LayoutParams(-1, -1));
            tableRow4.setWeightSum(1.0f);
            tableRow4.setBackgroundColor(-3355444);
            tableRow4.setPadding(2, 2, 2, 1);
            CheckBox checkBox = new CheckBox(this);
            checkBox.setChecked(true);
            checkBox.setClickable(false);
            checkBox.setText("          ");
            checkBox.setLayoutParams(new TableRow.LayoutParams(-1, -1, 0.0f));
            checkBox.setTextColor(-16777216);
            checkBox.setBackgroundColor(-1);
            checkBox.setPadding(2, 0, 2, 0);
            checkBox.setGravity(16);
            tableRow4.addView(checkBox);
            TextView textView4 = new TextView(this);
            textView4.setText(e2.g());
            textView4.setLayoutParams(new TableRow.LayoutParams(-2, -1, 1.0f));
            textView4.setTextColor(-16777216);
            textView4.setBackgroundColor(-1);
            textView4.setPadding(2, 0, 2, 0);
            textView4.setGravity(16);
            tableRow4.addView(textView4);
            tableLayout4.addView(tableRow4);
            TableRow tableRow5 = new TableRow(this);
            tableRow5.setLayoutParams(new TableRow.LayoutParams(-1, -1));
            tableRow5.setWeightSum(1.0f);
            tableRow5.setBackgroundColor(-3355444);
            tableRow5.setPadding(2, 1, 2, 1);
            CheckBox checkBox2 = new CheckBox(this);
            checkBox2.setChecked(true);
            checkBox2.setClickable(false);
            checkBox2.setText("          ");
            checkBox2.setLayoutParams(new TableRow.LayoutParams(-2, -1, 0.0f));
            checkBox2.setTextColor(-16777216);
            checkBox2.setBackgroundColor(-1);
            checkBox2.setPadding(2, 0, 2, 0);
            checkBox2.setGravity(16);
            tableRow5.addView(checkBox2);
            TextView textView5 = new TextView(this);
            textView5.setText(e2.d());
            textView5.setLayoutParams(new TableRow.LayoutParams(-2, -1, 1.0f));
            textView5.setTextColor(-16777216);
            textView5.setBackgroundColor(-1);
            textView5.setPadding(2, 0, 2, 0);
            textView5.setGravity(16);
            tableRow5.addView(textView5);
            tableLayout4.addView(tableRow5);
            TableRow tableRow6 = new TableRow(this);
            tableRow6.setLayoutParams(new TableRow.LayoutParams(-1, -1));
            tableRow6.setWeightSum(1.0f);
            tableRow6.setBackgroundColor(-3355444);
            tableRow6.setPadding(2, 1, 2, 2);
            CheckBox checkBox3 = new CheckBox(this);
            checkBox3.setChecked(true);
            checkBox3.setClickable(false);
            checkBox3.setText("          ");
            checkBox3.setLayoutParams(new TableRow.LayoutParams(-2, -1, 0.0f));
            checkBox3.setTextColor(-16777216);
            checkBox3.setBackgroundColor(-1);
            checkBox3.setPadding(2, 0, 2, 0);
            checkBox3.setGravity(16);
            tableRow6.addView(checkBox3);
            TextView textView6 = new TextView(this);
            textView6.setText(e2.e());
            textView6.setLayoutParams(new TableRow.LayoutParams(-2, -1, 1.0f));
            textView6.setTextColor(-16777216);
            textView6.setBackgroundColor(-1);
            textView6.setPadding(2, 0, 2, 0);
            textView6.setGravity(16);
            tableRow6.addView(textView6);
            tableLayout4.addView(tableRow6);
            linearLayout.addView(tableLayout4);
            TableLayout tableLayout5 = new TableLayout(this);
            tableLayout5.setLayoutParams(new TableRow.LayoutParams(-1, -1));
            tableLayout5.setPadding(i3, 10, i3, 0);
            tableLayout5.setColumnStretchable(0, true);
            TableRow tableRow7 = new TableRow(this);
            tableRow7.setLayoutParams(new TableRow.LayoutParams(-1, -1));
            tableRow7.setWeightSum(1.0f);
            Button button = new Button(this);
            button.setText(a("ui.close"));
            button.setOnClickListener(this.p);
            button.setLayoutParams(new TableRow.LayoutParams(-1, -1));
            button.setTextColor(-13421773);
            button.setGravity(17);
            tableRow7.addView(button);
            tableLayout5.addView(tableRow7);
            linearLayout.addView(tableLayout5);
            scrollView.addView(linearLayout);
            setContentView(scrollView);
            return;
        }
        g.a(d, "Payment Status: FAILED");
        new AlertDialog.Builder(this).setTitle(a("ui.dialog.error.title")).setMessage(bVar.f().b()).setPositiveButton(a("ui.ok"), (DialogInterface.OnClickListener) null).show();
    }

    public final void a(q qVar) {
        super.a(qVar);
        if (this.j != null) {
            this.h = qVar.a();
            this.j.setMessage(this.h);
        }
    }

    public final void e() {
        super.e();
        g.a(d, "Starting Payment Processing");
        this.j = new ProgressDialog(this);
        this.j.setProgressStyle(0);
        this.j.setTitle(a("ui.progress.title"));
        this.j.setCancelable(true);
        this.j.setIndeterminate(true);
        this.j.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public final void onCancel(DialogInterface dialogInterface) {
                ZongUI.this.d();
            }
        });
        if (this.j != null) {
            this.h = a("ui.progress.start");
            this.j.setMessage(this.h);
            this.j.show();
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        g.a(d, "LifeCycle Event: onCreate");
        this.a = 2;
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            a((ZongPaymentRequest) extras.getParcelable(ZpMoConst.ZONG_MOBILE_PAYMENT_BUNDLE_KEY));
            if (s || g() != null) {
                g.a(g().getDebugMode().booleanValue());
            } else {
                throw new AssertionError();
            }
        }
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setGravity(17);
        linearLayout.setBackgroundColor(-2490350);
        ProgressBar progressBar = new ProgressBar(this);
        progressBar.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
        progressBar.setIndeterminate(true);
        linearLayout.addView(progressBar);
        setContentView(linearLayout);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4 || keyEvent.getRepeatCount() != 0) {
            return super.onKeyDown(i2, keyEvent);
        }
        finish();
        return true;
    }
}
