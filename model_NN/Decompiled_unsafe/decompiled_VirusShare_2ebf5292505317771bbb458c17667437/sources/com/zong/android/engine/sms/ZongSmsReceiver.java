package com.zong.android.engine.sms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.SmsMessage;
import com.netqin.antivirus.antilost.SmsHandler;
import com.zong.android.engine.utils.g;
import java.lang.ref.WeakReference;

public class ZongSmsReceiver extends BroadcastReceiver {
    private static final String a = ZongSmsReceiver.class.getSimpleName();
    private WeakReference<Handler> b;

    public ZongSmsReceiver() {
    }

    public ZongSmsReceiver(Handler handler) {
        this.b = new WeakReference<>(handler);
    }

    public void onReceive(Context context, Intent intent) {
        g.a(a, "ZongSmsReceiver.onReceive");
        Bundle extras = intent.getExtras();
        if (extras != null) {
            Object[] objArr = (Object[]) extras.get("pdus");
            SmsMessage[] smsMessageArr = new SmsMessage[objArr.length];
            for (int i = 0; i < smsMessageArr.length; i++) {
                smsMessageArr[i] = SmsMessage.createFromPdu((byte[]) objArr[i]);
                String originatingAddress = smsMessageArr[i].getOriginatingAddress();
                String str = smsMessageArr[i].getMessageBody().toString();
                g.a(a, "Premium SMS arrived", originatingAddress, str);
                Handler handler = this.b.get();
                if (handler != null) {
                    Message obtainMessage = handler.obtainMessage(0);
                    Bundle bundle = new Bundle();
                    bundle.putString(SmsHandler.BODY, str);
                    bundle.putString("originatingAddr", originatingAddress);
                    obtainMessage.setData(bundle);
                    handler.sendMessage(obtainMessage);
                } else {
                    g.a(a, "PINCODE send failed on a dead SMS Handler in payment flow");
                }
            }
        }
    }
}
