package com.zong.android.engine.process;

import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import com.zong.android.engine.activities.ZongPaymentRequest;
import com.zong.android.engine.utils.g;
import zongfuscated.C0011l;

class c implements Handler.Callback {
    private /* synthetic */ ZongServiceProcess a;

    c(ZongServiceProcess zongServiceProcess) {
        this.a = zongServiceProcess;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zong.android.engine.process.ZongServiceProcess.a(com.zong.android.engine.process.ZongServiceProcess, boolean):void
     arg types: [com.zong.android.engine.process.ZongServiceProcess, int]
     candidates:
      com.zong.android.engine.process.ZongServiceProcess.a(android.content.ContentValues, java.lang.String):void
      com.zong.android.engine.process.ZongServiceProcess.a(com.zong.android.engine.process.ZongServiceProcess, int):void
      com.zong.android.engine.process.ZongServiceProcess.a(com.zong.android.engine.process.ZongServiceProcess, android.content.ContentValues):void
      com.zong.android.engine.process.ZongServiceProcess.a(com.zong.android.engine.process.ZongServiceProcess, android.os.Message):void
      com.zong.android.engine.process.ZongServiceProcess.a(com.zong.android.engine.process.ZongServiceProcess, android.os.Messenger):void
      com.zong.android.engine.process.ZongServiceProcess.a(com.zong.android.engine.process.ZongServiceProcess, com.zong.android.engine.activities.ZongPaymentRequest):void
      com.zong.android.engine.process.ZongServiceProcess.a(com.zong.android.engine.process.ZongServiceProcess, boolean):void */
    private void a() {
        this.a.k = false;
        if (this.a.m != null && this.a.l != null) {
            g.a(ZongServiceProcess.a, "Restoring Messaging to Client", Integer.toString(this.a.l.what));
            try {
                this.a.m.send(this.a.l);
            } catch (RemoteException e) {
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.zong.android.engine.process.ZongServiceProcess.a(com.zong.android.engine.process.ZongServiceProcess, boolean):void
     arg types: [com.zong.android.engine.process.ZongServiceProcess, int]
     candidates:
      com.zong.android.engine.process.ZongServiceProcess.a(android.content.ContentValues, java.lang.String):void
      com.zong.android.engine.process.ZongServiceProcess.a(com.zong.android.engine.process.ZongServiceProcess, int):void
      com.zong.android.engine.process.ZongServiceProcess.a(com.zong.android.engine.process.ZongServiceProcess, android.content.ContentValues):void
      com.zong.android.engine.process.ZongServiceProcess.a(com.zong.android.engine.process.ZongServiceProcess, android.os.Message):void
      com.zong.android.engine.process.ZongServiceProcess.a(com.zong.android.engine.process.ZongServiceProcess, android.os.Messenger):void
      com.zong.android.engine.process.ZongServiceProcess.a(com.zong.android.engine.process.ZongServiceProcess, com.zong.android.engine.activities.ZongPaymentRequest):void
      com.zong.android.engine.process.ZongServiceProcess.a(com.zong.android.engine.process.ZongServiceProcess, boolean):void */
    public final boolean handleMessage(Message message) {
        switch (message.what) {
            case 1:
            case 2:
            case 3:
                this.a.n = message.what;
                this.a.k = false;
                this.a.m = message.replyTo;
                this.a.h = (ZongPaymentRequest) message.obj;
                g.a(this.a.h.getDebugMode().booleanValue());
                this.a.c();
                if (message.what != 1) {
                    if (message.what != 2) {
                        if (message.what == 3) {
                            this.a.e();
                            break;
                        }
                    } else {
                        this.a.e();
                        break;
                    }
                } else {
                    this.a.f();
                    break;
                }
                break;
            case 4:
                this.a.m = (Messenger) null;
                this.a.d();
                break;
            case 5:
                this.a.k = true;
                break;
            case 6:
                if (this.a.k) {
                    a();
                    break;
                }
                break;
            case 7:
                a();
                break;
            case 8:
                this.a.g.a((C0011l) message.obj);
                break;
            case 9:
                ZongServiceProcess.j(this.a);
                break;
        }
        return false;
    }
}
