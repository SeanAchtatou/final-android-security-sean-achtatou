package com.zong.android.engine.process;

import android.app.Activity;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import com.zong.android.engine.ZpMoConst;
import com.zong.android.engine.activities.ZongPaymentRequest;
import com.zong.android.engine.activities.ZongPricePointsElement;
import com.zong.android.engine.utils.b;
import com.zong.android.engine.utils.g;
import zongfuscated.C0002b;
import zongfuscated.C0011l;
import zongfuscated.L;
import zongfuscated.q;

public abstract class ZongActivityProcess extends Activity {
    /* access modifiers changed from: private */
    public static final String d = ZongActivityProcess.class.getSimpleName();
    protected int a = 2;
    Messenger b = null;
    boolean c;
    private ZongPaymentRequest e;
    private L f = null;
    private C0002b g = null;
    private ContentValues h;
    private final Handler i = new Handler(new b(this));
    private Messenger j = new Messenger(this.i);
    private ServiceConnection k = null;

    private final class a implements ServiceConnection {
        private int a;

        public a(int i) {
            this.a = i;
        }

        public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            ZongActivityProcess.this.b = new Messenger(iBinder);
            g.a(ZongActivityProcess.d, "Service LifeCycle Event: onServiceConnected");
            ZongActivityProcess.this.a(this.a, ZongActivityProcess.this.g());
        }

        public final void onServiceDisconnected(ComponentName componentName) {
            ZongActivityProcess.this.b = null;
            ZongActivityProcess.this.c = false;
            g.a(ZongActivityProcess.d, "Service LifeCycle Event: onServiceDisconnected");
        }
    }

    private void a(int i2) {
        g.a(d, "LifeCycle Event: onStart");
        if (!this.c) {
            this.k = new a(i2);
            this.c = bindService(new Intent(this, ZongServiceProcess.class), this.k, 1);
        }
    }

    private void b(C0002b bVar) {
        if (bVar != null) {
            Intent c2 = bVar.c();
            if (bVar.a()) {
                c2.putExtra(ZpMoConst.ZONG_MOBILE_RESPONSE_REQUEST_OBJECT, this.e);
            }
            setResult(bVar.a() ? -1 : bVar.b(), c2);
        }
        finish();
    }

    /* access modifiers changed from: protected */
    public final String a(String str) {
        return this.h.containsKey(str) ? this.h.getAsString(str) : str;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        a(9, null);
    }

    /* access modifiers changed from: protected */
    public final void a(int i2, Object obj) {
        Message obtain;
        if (this.c && this.b != null) {
            if (obj == null) {
                try {
                    obtain = Message.obtain((Handler) null, i2);
                } catch (RemoteException e2) {
                    return;
                }
            } else {
                obtain = Message.obtain(null, i2, obj);
            }
            obtain.replyTo = this.j;
            this.b.send(obtain);
        }
    }

    public final void a(ZongPaymentRequest zongPaymentRequest) {
        this.e = zongPaymentRequest;
    }

    public void a(L l) {
        this.f = l;
        this.h = l.e();
        if (this.h == null) {
            this.h = new ContentValues();
        }
    }

    public void a(C0002b bVar) {
        this.g = bVar;
    }

    public void a(q qVar) {
    }

    /* access modifiers changed from: protected */
    public void b() {
        C0011l lVar = new C0011l();
        lVar.a(this.e);
        ZongPricePointsElement currentPricePoint = this.e.getCurrentPricePoint();
        String a2 = currentPricePoint.a();
        String b2 = currentPricePoint.b();
        lVar.a(a2);
        lVar.b(b2);
        g.a(d, this.e.toString());
        a(8, lVar);
        g.a(d, "Excute Payment");
    }

    /* access modifiers changed from: protected */
    public final void c() {
        b(this.g);
    }

    /* access modifiers changed from: protected */
    public final void d() {
        C0002b bVar = new C0002b();
        bVar.a((Boolean) false);
        bVar.a(2);
        bVar.a(a("ui.error.stop.user.code"), a("ui.error.stop.user.label"));
        b(bVar);
    }

    public void e() {
    }

    public final L f() {
        return this.f;
    }

    public final ZongPaymentRequest g() {
        return this.e;
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        g.a(d, "LifeCycle Event: onConfigurationChanged");
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        g.a(d, "LifeCycle Event: onCreate");
        this.c = false;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        g.a(d, "LifeCycle Event: onDestroy");
        if (this.c) {
            a(4, null);
            unbindService(this.k);
            this.c = false;
        }
        b.a().c();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        g.a(d, "LifeCycle Event: onPause");
        a(5, null);
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        g.a(d, "LifeCycle Event: onRestart");
        a(7, null);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        g.a(d, "LifeCycle Event: onResume");
        a(6, null);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        g.a(d, "LifeCycle Event: onStart");
        switch (this.a) {
            case 0:
                a(1);
                return;
            case 1:
            default:
                return;
            case 2:
                a(3);
                return;
        }
    }
}
