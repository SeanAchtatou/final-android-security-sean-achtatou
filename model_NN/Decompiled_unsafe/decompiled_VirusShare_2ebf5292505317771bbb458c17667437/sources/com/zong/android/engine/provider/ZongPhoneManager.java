package com.zong.android.engine.provider;

import android.content.Context;
import android.telephony.TelephonyManager;
import com.zong.android.engine.utils.g;

public class ZongPhoneManager {
    private static final String a = ZongPhoneManager.class.getSimpleName();

    private static class a {
        /* access modifiers changed from: private */
        public static final ZongPhoneManager a = new ZongPhoneManager();

        private a() {
        }
    }

    /* synthetic */ ZongPhoneManager() {
        this((byte) 0);
    }

    private ZongPhoneManager(byte b) {
    }

    public static ZongPhoneManager getInstance() {
        return a.a;
    }

    public PhoneState getPhoneState(Context context) {
        PhoneState phoneState = new PhoneState((TelephonyManager) context.getSystemService("phone"));
        g.a(a, "Phone State", phoneState.toString());
        return phoneState;
    }
}
