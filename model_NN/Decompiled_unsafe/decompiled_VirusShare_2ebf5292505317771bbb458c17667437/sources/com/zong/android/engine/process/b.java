package com.zong.android.engine.process;

import android.os.Handler;
import android.os.Message;
import zongfuscated.C0002b;
import zongfuscated.L;
import zongfuscated.q;

class b implements Handler.Callback {
    private /* synthetic */ ZongActivityProcess a;

    b(ZongActivityProcess zongActivityProcess) {
        this.a = zongActivityProcess;
    }

    public final boolean handleMessage(Message message) {
        switch (message.what) {
            case 1:
                this.a.e();
                return false;
            case 2:
                this.a.a((q) message.obj);
                return false;
            case 3:
                this.a.a((C0002b) message.obj);
                return false;
            case 4:
                this.a.a((L) message.obj);
                return false;
            default:
                return false;
        }
    }
}
