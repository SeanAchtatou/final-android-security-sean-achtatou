package com.zong.android.engine.activities;

import android.os.Parcel;
import android.os.Parcelable;

public class ZongPricePointsElement implements Parcelable {
    public static final Parcelable.Creator<ZongPricePointsElement> CREATOR = new a();
    private String a;
    private float b;
    private int c;
    private String d;
    private String e;

    /* synthetic */ ZongPricePointsElement(Parcel parcel) {
        this(parcel, (byte) 0);
    }

    private ZongPricePointsElement(Parcel parcel, byte b2) {
        this.a = parcel.readString();
        this.b = parcel.readFloat();
        this.c = parcel.readInt();
        this.d = parcel.readString();
        this.e = parcel.readString();
    }

    public ZongPricePointsElement(String str, float f, int i, String str2, String str3) {
        this.a = str;
        this.b = f;
        this.c = i;
        this.d = str2;
        this.e = str3;
    }

    public final String a() {
        return this.a;
    }

    public final String b() {
        return this.d;
    }

    public final String c() {
        return this.e;
    }

    public int describeContents() {
        return 0;
    }

    public float getAmount() {
        return this.b;
    }

    public int getQuantity() {
        return this.c;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("key: ").append(this.a).append("\n");
        sb.append("amount: ").append(this.b).append("\n");
        sb.append("quantity: ").append(this.c).append("\n");
        sb.append("itemDesc: ").append(this.d).append("\n");
        sb.append("label: ").append(this.e).append("\n");
        return sb.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.a);
        parcel.writeFloat(this.b);
        parcel.writeInt(this.c);
        parcel.writeString(this.d);
        parcel.writeString(this.e);
    }
}
