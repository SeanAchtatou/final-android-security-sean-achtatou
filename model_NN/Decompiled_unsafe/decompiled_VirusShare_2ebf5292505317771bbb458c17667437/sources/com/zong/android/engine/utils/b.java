package com.zong.android.engine.utils;

import com.zong.android.engine.ZpMoConst;
import java.io.IOException;
import java.security.KeyStore;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

public final class b implements HttpClient {
    private static final String a = b.class.getSimpleName();
    private static boolean b;
    private final HttpParams c;
    private final SchemeRegistry d;
    private HttpClient e;
    private ClientConnectionManager f;

    private static class a {
        /* access modifiers changed from: private */
        public static final b a = new b();

        private a() {
        }
    }

    static {
        b = false;
        try {
            Class.forName("com.zong.android.engine.utils.c");
            b = true;
        } catch (Exception e2) {
            b = false;
        }
    }

    /* synthetic */ b() {
        this((byte) 0);
    }

    private b(byte b2) {
        SSLSocketFactory socketFactory;
        this.c = new BasicHttpParams();
        HttpConnectionParams.setStaleCheckingEnabled(this.c, false);
        HttpConnectionParams.setConnectionTimeout(this.c, 60000);
        HttpConnectionParams.setSoTimeout(this.c, 60000);
        HttpConnectionParams.setSocketBufferSize(this.c, 32768);
        this.c.setBooleanParameter("http.protocol.expect-continue", false);
        ConnManagerParams.setMaxTotalConnections(this.c, 5);
        ConnManagerParams.setMaxConnectionsPerRoute(this.c, new ConnPerRouteBean(5));
        HttpClientParams.setRedirecting(this.c, false);
        HttpClientParams.setCookiePolicy(this.c, "rfc2109");
        HttpProtocolParams.setUserAgent(this.c, ZpMoConst.ANDROID_USER_AGENT);
        HttpProtocolParams.setVersion(this.c, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setContentCharset(this.c, StringEncodings.UTF8);
        this.d = new SchemeRegistry();
        this.d.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        if (b) {
            try {
                KeyStore instance = KeyStore.getInstance(KeyStore.getDefaultType());
                instance.load(null, null);
                SSLSocketFactory cVar = new c(instance);
                cVar.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
                socketFactory = cVar;
            } catch (Exception e2) {
                SSLSocketFactory socketFactory2 = SSLSocketFactory.getSocketFactory();
                g.a(a, "", e2);
                socketFactory = socketFactory2;
            }
        } else {
            socketFactory = SSLSocketFactory.getSocketFactory();
        }
        this.d.register(new Scheme("https", socketFactory, 443));
    }

    public static b a() {
        return a.a;
    }

    public static HttpClient b() {
        if (a.a.e == null) {
            g.a(a, "Allocating new HTTP client");
            b a2 = a.a;
            b a3 = a.a;
            a3.f = new ThreadSafeClientConnManager(a3.c, a3.d);
            a2.e = new DefaultHttpClient(a3.f, a3.c) {
                /* access modifiers changed from: protected */
                public final HttpContext createHttpContext() {
                    BasicHttpContext basicHttpContext = new BasicHttpContext();
                    basicHttpContext.setAttribute("http.authscheme-registry", getAuthSchemes());
                    basicHttpContext.setAttribute("http.cookiespec-registry", getCookieSpecs());
                    basicHttpContext.setAttribute("http.auth.credentials-provider", getCredentialsProvider());
                    return basicHttpContext;
                }
            };
        }
        return a.a.e;
    }

    public final void c() {
        g.a(a, "Closing HTTP client connection manager");
        if (this.e != null) {
            getConnectionManager().closeExpiredConnections();
            getConnectionManager().shutdown();
        }
        this.f = null;
        this.e = null;
    }

    public final <T> T execute(HttpHost httpHost, HttpRequest httpRequest, ResponseHandler<? extends T> responseHandler) throws IOException {
        return this.e.execute(httpHost, httpRequest, responseHandler);
    }

    public final <T> T execute(HttpHost httpHost, HttpRequest httpRequest, ResponseHandler<? extends T> responseHandler, HttpContext httpContext) throws IOException {
        return this.e.execute(httpHost, httpRequest, responseHandler, httpContext);
    }

    public final <T> T execute(HttpUriRequest httpUriRequest, ResponseHandler<? extends T> responseHandler) throws IOException {
        return this.e.execute(httpUriRequest, responseHandler);
    }

    public final <T> T execute(HttpUriRequest httpUriRequest, ResponseHandler<? extends T> responseHandler, HttpContext httpContext) throws IOException {
        return this.e.execute(httpUriRequest, responseHandler, httpContext);
    }

    public final HttpResponse execute(HttpHost httpHost, HttpRequest httpRequest) throws IOException {
        return this.e.execute(httpHost, httpRequest);
    }

    public final HttpResponse execute(HttpHost httpHost, HttpRequest httpRequest, HttpContext httpContext) throws IOException {
        return this.e.execute(httpHost, httpRequest, httpContext);
    }

    public final HttpResponse execute(HttpUriRequest httpUriRequest) throws IOException {
        return this.e.execute(httpUriRequest);
    }

    public final HttpResponse execute(HttpUriRequest httpUriRequest, HttpContext httpContext) throws IOException {
        return this.e.execute(httpUriRequest, httpContext);
    }

    public final ClientConnectionManager getConnectionManager() {
        return this.e.getConnectionManager();
    }

    public final HttpParams getParams() {
        return this.e.getParams();
    }
}
