package com.zong.android.engine;

public class ZpMoConst {
    public static final String ANDROID_USER_AGENT = "Android";
    public static final int ERROR = 1;
    public static final int EXCHANGE = 4;
    public static final String LOOKUP_URL = "/zongpay/actions/default";
    public static final int MT = 1;
    public static final int OK = 0;
    public static final String PROCESSING_URL = "/zongpay/actions/processing";
    public static final int PSMS = 0;
    public static final int SCHEDULED = 3;
    public static final int STOP = 2;
    public static final int TIMEOUT = 2;
    public static final String VERSION_ID = "2.3";
    public static final String ZONG_MOBILE_PAYMENT_BUNDLE_KEY = "com.zong.intent.Request";
    public static final String ZONG_MOBILE_RESPONSE_ERROR_CODE = "com.zong.intent.error.Code";
    public static final String ZONG_MOBILE_RESPONSE_ERROR_LABEL = "com.zong.intent.error.label";
    public static final String ZONG_MOBILE_RESPONSE_PRICEPOINT_INDEX = "com.zong.intent.SelectPricePointIndex";
    public static final String ZONG_MOBILE_RESPONSE_REQUEST_OBJECT = "com.zong.intent.RequestObject";
    public static final String ZONG_PAY_ROOT_CONTEXT = "/zongpay";
    public static final String ZONG_SERVICE_INTENT_ACTION_NAME = "com.zong.pay.SERVICE";
    public static final String ZONG_SERVICE_INTENT_PAYLOAD = "com.zong.pay.SERVICE.payload";
}
