package com.zong.android.engine.utils;

import android.content.Context;
import java.lang.ref.WeakReference;
import java.util.HashMap;

public class e {
    private static final String a = e.class.getSimpleName();
    private HashMap<String, WeakReference<Context>> b = new HashMap<>();

    public final synchronized Context a(String str) {
        Context context;
        WeakReference weakReference = this.b.get(str);
        if (weakReference == null) {
            g.a(a, "Invalid Context", str);
            context = null;
        } else {
            g.a(a, "Getting Context", str);
            context = (Context) weakReference.get();
        }
        return context;
    }

    public final synchronized void a(String str, Context context) {
        this.b.put(str, new WeakReference(context));
        g.a(a, "Setting Context", str);
    }

    public final synchronized void b(String str) {
        this.b.remove(str);
        g.a(a, "Reseting Context", str);
    }
}
