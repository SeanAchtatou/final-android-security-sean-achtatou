package com.zong.android.engine.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class f {
    static {
        f.class.getSimpleName();
    }

    public static Bitmap a(String str) {
        Bitmap bitmap = null;
        for (int i = 0; i < 3 && (bitmap = b(str)) == null; i++) {
        }
        return bitmap;
    }

    private static Bitmap b(String str) {
        Bitmap bitmap;
        HttpURLConnection httpURLConnection;
        InputStream inputStream;
        try {
            HttpURLConnection httpURLConnection2 = (HttpURLConnection) new URL(str).openConnection();
            try {
                httpURLConnection2.setDoInput(true);
                httpURLConnection2.connect();
                InputStream inputStream2 = httpURLConnection2.getInputStream();
                try {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 1;
                    Bitmap decodeStream = BitmapFactory.decodeStream(inputStream2, null, options);
                    try {
                        inputStream2.close();
                        httpURLConnection2.disconnect();
                        bitmap = decodeStream;
                        inputStream = null;
                        httpURLConnection = null;
                    } catch (Exception e) {
                        inputStream = inputStream2;
                        bitmap = decodeStream;
                        httpURLConnection = httpURLConnection2;
                    }
                } catch (Exception e2) {
                    httpURLConnection = httpURLConnection2;
                    inputStream = inputStream2;
                    bitmap = null;
                }
            } catch (Exception e3) {
                bitmap = null;
                httpURLConnection = httpURLConnection2;
                inputStream = null;
            }
        } catch (Exception e4) {
            bitmap = null;
            httpURLConnection = null;
            inputStream = null;
        }
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (Exception e5) {
            }
        }
        if (httpURLConnection != null) {
            httpURLConnection.disconnect();
        }
        return bitmap;
    }
}
