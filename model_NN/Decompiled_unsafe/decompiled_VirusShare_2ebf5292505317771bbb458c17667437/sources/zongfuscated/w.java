package zongfuscated;

class w implements C0008i {
    private /* synthetic */ C0002b a;

    w(C0002b bVar) {
        this.a = bVar;
    }

    public final String a() {
        return this.a.a("com.zong.view.result.success.param.orderId");
    }

    public final String b() {
        return this.a.a("com.zong.view.result.success.param.credits");
    }

    public final String c() {
        return this.a.a("com.zong.view.result.success.param.title");
    }

    public final String d() {
        return this.a.a("com.zong.view.result.success.param.sms");
    }

    public final String e() {
        return this.a.a("com.zong.view.result.success.param.help");
    }

    public final String f() {
        return this.a.a("com.zong.view.result.success.param.description");
    }

    public final String g() {
        return this.a.a("com.zong.view.result.success.param.charge");
    }

    public final boolean h() {
        return C0002b.b(this.a, "com.zong.view.result.success.param.isMMT").booleanValue();
    }

    public final boolean i() {
        return C0002b.b(this.a, "com.zong.view.result.success.param.isMMO").booleanValue();
    }

    public final boolean j() {
        return C0002b.b(this.a, "com.zong.view.result.success.param.isOneClick").booleanValue();
    }

    public final boolean k() {
        return C0002b.b(this.a, "com.zong.view.result.success.param.isPartial").booleanValue();
    }

    public final int l() {
        return C0002b.c(this.a, "com.zong.view.result.success.param.nbMMT").intValue();
    }

    public final float m() {
        return C0002b.d(this.a, "com.zong.view.result.success.param.billedAmount").floatValue();
    }

    public final float n() {
        return C0002b.d(this.a, "com.zong.view.result.success.param.priceAmount").floatValue();
    }

    public final float o() {
        return C0002b.d(this.a, "com.zong.view.result.success.param.outPayPref").floatValue();
    }

    public final float p() {
        return C0002b.d(this.a, "com.zong.view.result.success.param.outPayUSD").floatValue();
    }
}
