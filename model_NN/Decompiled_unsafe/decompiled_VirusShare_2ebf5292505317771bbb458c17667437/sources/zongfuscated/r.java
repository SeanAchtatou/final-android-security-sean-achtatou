package zongfuscated;

import com.zong.android.engine.utils.g;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class r implements z {
    private static final String a = r.class.getSimpleName();
    private Integer b;
    private String c;
    private C0006g d;

    public r(Integer num, String str) {
        this.b = num;
        this.c = str;
    }

    public final String a() {
        g.a(a, "parseSMS", this.b.toString(), this.c, this.d.a);
        Matcher matcher = Pattern.compile(this.c).matcher(this.d.a);
        if (matcher.find()) {
            String group = matcher.group(this.b.intValue());
            g.a(a, "SMS pincode was validated", group);
            return group;
        }
        g.a(a, "Pincode not found");
        return null;
    }

    public final void a(C0006g gVar) {
        this.d = gVar;
    }
}
