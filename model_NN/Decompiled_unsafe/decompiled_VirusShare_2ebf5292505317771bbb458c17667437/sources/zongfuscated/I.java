package zongfuscated;

import android.os.Parcel;
import android.os.Parcelable;

public final class I implements Parcelable {
    public static final Parcelable.Creator<I> CREATOR = new C();
    private String a;
    private String b;
    private String c;

    public I() {
    }

    public I(Parcel parcel) {
        this.a = parcel.readString();
        this.b = parcel.readString();
        this.c = parcel.readString();
    }

    public final String a() {
        return this.a;
    }

    public final void a(String str) {
        this.a = str;
    }

    public final String b() {
        return this.b;
    }

    public final void b(String str) {
        this.b = str;
    }

    public final void c(String str) {
        this.c = str;
    }

    public final boolean c() {
        return Boolean.valueOf(this.c).booleanValue();
    }

    public final int describeContents() {
        return 0;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("name:").append(this.a).append(" | ");
        sb.append("value:").append(this.b).append(" | ");
        sb.append("isAndroidParam:").append(this.c);
        return sb.toString();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.a);
        parcel.writeString(this.b);
        parcel.writeString(this.c);
    }
}
