package zongfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

public final class H implements Parcelable {
    public static final Parcelable.Creator<H> CREATOR = new m();
    private String a;
    private String b;
    private String c;
    private String d;
    private ArrayList<I> e;

    public H() {
        this.e = new ArrayList<>();
    }

    public H(Parcel parcel) {
        this.a = parcel.readString();
        this.b = parcel.readString();
        this.c = parcel.readString();
        this.d = parcel.readString();
        int readInt = parcel.readInt();
        if (readInt > 0) {
            I[] iArr = (I[]) I.CREATOR.newArray(readInt);
            parcel.readTypedArray(iArr, I.CREATOR);
            this.e = new ArrayList<>();
            for (I add : iArr) {
                this.e.add(add);
            }
        }
    }

    public final String a() {
        return this.a;
    }

    public final void a(String str) {
        this.a = str;
    }

    public final void a(ArrayList<I> arrayList) {
        this.e = arrayList;
    }

    public final String b() {
        return this.b;
    }

    public final void b(String str) {
        this.b = str;
    }

    public final String c() {
        return this.c;
    }

    public final void c(String str) {
        this.c = str;
    }

    public final String d() {
        return this.d;
    }

    public final void d(String str) {
        this.d = str;
    }

    public final int describeContents() {
        return 0;
    }

    public final ArrayList<I> e() {
        return this.e;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.a);
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        int size = this.e.size();
        parcel.writeInt(size);
        if (size > 0) {
            parcel.writeTypedArray((I[]) this.e.toArray(new I[size]), i);
        }
    }
}
