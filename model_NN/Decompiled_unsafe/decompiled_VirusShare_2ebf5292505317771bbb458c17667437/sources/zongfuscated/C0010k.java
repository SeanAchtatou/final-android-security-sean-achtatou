package zongfuscated;

import android.sax.Element;
import android.sax.EndTextElementListener;
import android.sax.RootElement;
import android.sax.StartElementListener;
import android.util.Xml;
import com.netqin.antivirus.cloud.model.xml.XmlUtils;
import com.netqin.antivirus.networkmanager.model.DatabaseHelper;
import com.zong.android.engine.utils.g;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

/* renamed from: zongfuscated.k  reason: case insensitive filesystem */
public class C0010k {
    private static final String a = C0010k.class.getSimpleName();

    public static G a(InputStream inputStream) throws IOException, SAXException {
        final G g = new G();
        g.a(new ArrayList());
        RootElement rootElement = new RootElement("ZgRespond");
        Element child = rootElement.getChild("actions");
        Element child2 = rootElement.getChild("view");
        Element child3 = child.getChild("action");
        Element child4 = child3.getChild("message");
        Element child5 = child3.getChild("params");
        child3.setStartElementListener(new StartElementListener() {
            public final void start(Attributes attributes) {
                H h = new H();
                String value = attributes.getValue("type");
                String value2 = attributes.getValue(XmlUtils.LABEL_REPORT_URL);
                String value3 = attributes.getValue("eventId");
                h.a(value);
                h.b(value2);
                h.c(value3);
                h.a(new ArrayList());
                G.this.a().add(h);
            }
        });
        child4.setEndTextElementListener(new EndTextElementListener() {
            public final void end(String str) {
                G.this.a().get(G.this.a().size() - 1).d(str.trim());
            }
        });
        child5.getChild("param").setStartElementListener(new StartElementListener() {
            public final void start(Attributes attributes) {
                I i = new I();
                String value = attributes.getValue("name");
                String value2 = attributes.getValue(DatabaseHelper.Counters.VALUE);
                String value3 = attributes.getValue("isAndroidParameter");
                i.a(value);
                i.b(value2);
                i.c(value3);
                G.this.a().get(G.this.a().size() - 1).e().add(i);
            }
        });
        child2.setEndTextElementListener(new EndTextElementListener() {
            public final void end(String str) {
                G.this.a(str);
            }
        });
        try {
            Xml.parse(inputStream, Xml.Encoding.UTF_8, rootElement.getContentHandler());
            return g;
        } catch (IOException e) {
            g.c(a, "Parse IOException");
            throw e;
        } catch (SAXException e2) {
            g.c(a, "Parse SAXException");
            throw e2;
        }
    }
}
