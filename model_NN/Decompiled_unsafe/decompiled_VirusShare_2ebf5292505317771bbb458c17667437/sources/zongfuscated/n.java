package zongfuscated;

import android.os.Handler;
import android.os.Message;
import com.zong.android.engine.ZpMoConst;
import com.zong.android.engine.utils.b;
import com.zong.android.engine.utils.g;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;

public class n implements Runnable {
    private static final String a = n.class.getSimpleName();
    private boolean b;
    private HttpGet c;
    private Handler d;
    private Runnable e;

    public n(Handler handler, String str, HashMap<String, String> hashMap, Runnable runnable) {
        this.b = false;
        this.c = null;
        this.d = null;
        this.e = null;
        this.d = handler;
        this.e = runnable;
        this.b = runnable != null;
        String str2 = String.valueOf(str) + a(str, hashMap);
        g.a(a, "Request URI", str2);
        this.c = new HttpGet(str2);
        this.c.addHeader("User-Agent", ZpMoConst.ANDROID_USER_AGENT);
    }

    public n(String str, HashMap<String, String> hashMap) {
        this(null, str, hashMap, null);
    }

    private static String a(String str, HashMap<String, String> hashMap) {
        String encode;
        if (hashMap == null || hashMap.size() <= 0) {
            return "";
        }
        StringBuilder sb = str.contains("?") ? new StringBuilder("&") : new StringBuilder("?");
        int i = 0;
        for (String next : hashMap.keySet()) {
            if (i > 0) {
                sb.append("&");
            }
            String str2 = hashMap.get(next) != null ? hashMap.get(next) : "";
            try {
                encode = URLEncoder.encode(str2, StringEncodings.UTF8);
            } catch (UnsupportedEncodingException e2) {
                g.a(a, "Default Encoding Scheme used", e2);
                encode = URLEncoder.encode(str2);
            }
            sb.append(next).append("=").append(encode);
            i++;
        }
        String sb2 = sb.toString();
        g.a(a, "Params List partURL", sb2);
        return sb2;
    }

    public final o a() throws Exception {
        InputStream content;
        int i = 0;
        o oVar = null;
        HttpClient b2 = b.b();
        while (oVar == null && i < 3) {
            i++;
            try {
                HttpEntity entity = b2.execute(this.c).getEntity();
                if (entity != null) {
                    if (g.a()) {
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        entity.writeTo(byteArrayOutputStream);
                        content = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
                        g.a(a, "Call() Output Stream", byteArrayOutputStream.toString());
                    } else {
                        content = entity.getContent();
                    }
                    oVar = !this.b ? new o(C0010k.a(content)) : new o();
                }
            } catch (Exception e2) {
                g.a(a, "call()", e2);
                Thread.sleep(3000);
            }
        }
        if (oVar != null) {
            return oVar;
        }
        throw new Exception("Failed to ZongService.call() ... HTTP Connection Failure ");
    }

    public void run() {
        try {
            o a2 = a();
            if (this.b) {
                this.d.postAtFrontOfQueue(this.e);
                g.a(a, "Message Call was FLUSH - Posting Flush Action");
            } else if (a2 != null) {
                g.a(a, "Message Call was Sucessful");
                Message obtainMessage = this.d.obtainMessage(4, a2);
                if (obtainMessage != null) {
                    this.d.sendMessage(obtainMessage);
                    g.a(a, "Message Send was Sucessful");
                    return;
                }
                g.a(a, "Message Send Failure");
            } else {
                g.a(a, "Message Call Failure");
            }
        } catch (Exception e2) {
            g.a(a, "RUN()", e2);
        }
    }
}
