package zongfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

public final class G implements Parcelable {
    public static final Parcelable.Creator<G> CREATOR = new J();
    private ArrayList<H> a;
    private String b;

    public G() {
        this.a = new ArrayList<>();
    }

    public G(Parcel parcel) {
        this.b = parcel.readString();
        int readInt = parcel.readInt();
        if (readInt > 0) {
            H[] hArr = (H[]) H.CREATOR.newArray(readInt);
            parcel.readTypedArray(hArr, H.CREATOR);
            this.a = new ArrayList<>();
            for (H add : hArr) {
                this.a.add(add);
            }
        }
    }

    public final ArrayList<H> a() {
        return this.a;
    }

    public final void a(String str) {
        this.b = str;
    }

    public final void a(ArrayList<H> arrayList) {
        this.a = arrayList;
    }

    public final String b() {
        return this.b;
    }

    public final int describeContents() {
        return 0;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.b);
        int size = this.a.size();
        parcel.writeInt(size);
        if (size > 0) {
            parcel.writeTypedArray((H[]) this.a.toArray(new H[size]), i);
        }
    }
}
