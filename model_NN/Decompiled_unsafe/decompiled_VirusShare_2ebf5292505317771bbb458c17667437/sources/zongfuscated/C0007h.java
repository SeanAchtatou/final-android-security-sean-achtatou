package zongfuscated;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: zongfuscated.h  reason: case insensitive filesystem */
class C0007h implements Parcelable.Creator<L> {
    C0007h() {
    }

    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new L(parcel);
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new L[i];
    }
}
