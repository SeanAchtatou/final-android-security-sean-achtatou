package zongfuscated;

import android.os.Handler;
import com.zong.android.engine.utils.g;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

public class F {
    private static final String a = F.class.getSimpleName();
    private C0003d b;
    private Handler c;
    private final ExecutorService d;
    private Future<?> e = null;

    public F(ExecutorService executorService, C0003d dVar, Handler handler) {
        this.d = executorService;
        this.b = dVar;
        this.c = handler;
    }

    private void a(String str, HashMap<String, String> hashMap, Runnable runnable) {
        n nVar = new n(this.c, str, hashMap, runnable);
        g.a(a, "Service Loader Created for URL", str);
        try {
            if (!this.d.isShutdown()) {
                g.a(a, "Pool Running");
                this.e = this.d.submit(nVar);
                g.a(a, "Loader Executed");
                return;
            }
            g.a(a, "Pool Shutdown");
        } catch (Exception e2) {
            g.a(a, "ZongExchange.process", e2);
        }
    }

    private void a(o oVar, HashMap<String, String> hashMap, Runnable runnable) {
        H h = oVar.a;
        String a2 = this.b.a(h.b().trim());
        HashMap hashMap2 = new HashMap();
        if (h.c() != null) {
            hashMap2.put("_eventId", h.c());
        }
        ArrayList<I> e2 = h.e();
        if (!(e2 == null || hashMap == null)) {
            Iterator<I> it = e2.iterator();
            while (it.hasNext()) {
                I next = it.next();
                String a3 = next.a();
                if (next.c() && hashMap.get(a3) != null) {
                    g.a(a, "Request Params", a3, hashMap.get(a3));
                    hashMap2.put(a3, hashMap.get(a3));
                }
            }
        }
        g.a(a, "Request Params", hashMap2.toString());
        a(a2, hashMap2, runnable);
    }

    public final void a() {
        if (this.e != null) {
            this.e.cancel(true);
        }
        this.e = null;
    }

    public final void a(HashMap<String, String> hashMap) {
        a(this.b.a(), hashMap, (Runnable) null);
    }

    public final void a(o oVar, Runnable runnable) {
        a(oVar, (HashMap<String, String>) null, runnable);
    }

    public final void a(o oVar, HashMap<String, String> hashMap) {
        a(oVar, hashMap, (Runnable) null);
    }
}
