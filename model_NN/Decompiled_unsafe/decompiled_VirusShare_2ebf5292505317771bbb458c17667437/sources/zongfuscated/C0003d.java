package zongfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.zong.android.engine.utils.g;
import java.net.URI;
import java.net.URISyntaxException;

/* renamed from: zongfuscated.d  reason: case insensitive filesystem */
public class C0003d implements Parcelable {
    public static final Parcelable.Creator<C0003d> CREATOR = new C0005f();
    private static final String a = C0003d.class.getSimpleName();
    private String b;
    private String c;
    private int d;
    private String e;

    static {
        String[] strArr = {"192.168.1.20", "192.168.1.59", "192.168.1.84", "10.6.0.41", "10.3.0.167"};
    }

    /* synthetic */ C0003d(Parcel parcel) {
        this(parcel, (byte) 0);
    }

    private C0003d(Parcel parcel, byte b2) {
        this.b = parcel.readString();
        this.c = parcel.readString();
        this.d = parcel.readInt();
        this.e = parcel.readString();
        g.a(a, "URI Dump", this.b, this.c, Integer.toString(this.d), this.e);
    }

    public C0003d(String str) {
        try {
            URI uri = new URI(str);
            this.b = uri.getScheme();
            this.c = uri.getHost();
            this.d = uri.getPort();
            this.e = uri.getPath();
            g.a(a, "URI Dump", this.b, this.c, Integer.toString(this.d), this.e);
        } catch (URISyntaxException e2) {
            g.a(a, "URI Syntax Exception", e2);
        }
    }

    private String b() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.b).append("://").append(this.c);
        if (this.d != -1) {
            sb.append(":").append(this.d);
        }
        return sb.toString();
    }

    public final String a() {
        return b() + this.e;
    }

    public final String a(String str) {
        return b() + str;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeInt(this.d);
        parcel.writeString(this.e);
    }
}
