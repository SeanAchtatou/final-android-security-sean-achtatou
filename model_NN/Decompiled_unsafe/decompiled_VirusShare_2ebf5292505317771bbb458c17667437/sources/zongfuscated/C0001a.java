package zongfuscated;

import com.zong.android.engine.utils.g;

/* renamed from: zongfuscated.a  reason: case insensitive filesystem */
public class C0001a extends r {
    private static final String a = C0001a.class.getSimpleName();

    public C0001a(Integer num, String str) {
        super(num, str);
    }

    public final String a(boolean z, String str) {
        if (z) {
            int length = str.length() - 3;
            String substring = str.substring(length);
            String str2 = substring.contains("00") ? "9999" + str.substring(length + 1) : "999" + substring;
            g.a(a, "FlowFR::Simulation mode pinCode generator");
            return str2;
        }
        String a2 = a();
        g.a(a, "FlowFR::Live mode pinCode parsing");
        return a2;
    }
}
