package zongfuscated;

import android.os.Parcel;
import android.os.Parcelable;

public final class q implements Parcelable {
    public static final Parcelable.Creator<q> CREATOR = new E();
    private String a;

    /* synthetic */ q(Parcel parcel) {
        this(parcel, (byte) 0);
    }

    private q(Parcel parcel, byte b) {
        this.a = parcel.readString();
    }

    public q(String str) {
        this.a = str;
    }

    public final String a() {
        return this.a;
    }

    public final int describeContents() {
        return 0;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.a);
    }
}
