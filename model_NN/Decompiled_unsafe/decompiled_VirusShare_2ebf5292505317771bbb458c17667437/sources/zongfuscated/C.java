package zongfuscated;

import android.os.Parcel;
import android.os.Parcelable;

class C implements Parcelable.Creator<I> {
    C() {
    }

    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new I(parcel);
    }

    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new I[i];
    }
}
