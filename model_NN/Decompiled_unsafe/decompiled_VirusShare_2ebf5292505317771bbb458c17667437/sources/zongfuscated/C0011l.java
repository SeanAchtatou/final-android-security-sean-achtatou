package zongfuscated;

import android.os.Parcel;
import android.os.Parcelable;
import com.zong.android.engine.activities.ZongPaymentRequest;

/* renamed from: zongfuscated.l  reason: case insensitive filesystem */
public final class C0011l implements Parcelable {
    public static final Parcelable.Creator<C0011l> CREATOR = new A();
    private ZongPaymentRequest a;
    private String b;
    private String c;

    public C0011l() {
    }

    /* synthetic */ C0011l(Parcel parcel) {
        this(parcel, (byte) 0);
    }

    private C0011l(Parcel parcel, byte b2) {
        this.a = (ZongPaymentRequest) parcel.readParcelable(getClass().getClassLoader());
        this.b = parcel.readString();
        this.c = parcel.readString();
    }

    public final ZongPaymentRequest a() {
        return this.a;
    }

    public final void a(ZongPaymentRequest zongPaymentRequest) {
        this.a = zongPaymentRequest;
    }

    public final void a(String str) {
        this.b = str;
    }

    public final String b() {
        return this.b;
    }

    public final void b(String str) {
        this.c = str;
    }

    public final String c() {
        return this.c;
    }

    public final int describeContents() {
        return 0;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.a, i);
        parcel.writeString(this.b);
        parcel.writeString(this.c);
    }
}
