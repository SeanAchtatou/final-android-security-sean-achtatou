package zongfuscated;

import java.util.ArrayList;

public final class o {
    H a = null;
    private G b = null;

    public o() {
    }

    public o(G g) {
        this.b = g;
        ArrayList<H> a2 = g.a();
        this.a = (a2 == null || a2.size() <= 0) ? new H() : a2.get(a2.size() - 1);
    }

    public final H a() {
        return this.a;
    }

    public final String b() {
        return this.b.b();
    }
}
