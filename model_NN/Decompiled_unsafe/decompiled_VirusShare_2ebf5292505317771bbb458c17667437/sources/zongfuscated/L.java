package zongfuscated;

import android.content.ContentValues;
import android.os.Parcel;
import android.os.Parcelable;

public final class L implements Parcelable {
    public static final Parcelable.Creator<L> CREATOR = new C0007h();
    private Boolean a;
    private String b;
    private String c;
    private String d;
    private ContentValues e;
    private C0003d f;

    public L() {
    }

    /* synthetic */ L(Parcel parcel) {
        this(parcel, (byte) 0);
    }

    private L(Parcel parcel, byte b2) {
        this.a = Boolean.valueOf(Boolean.parseBoolean(parcel.readString()));
        this.b = parcel.readString();
        this.c = parcel.readString();
        this.d = parcel.readString();
        this.e = (ContentValues) parcel.readParcelable(getClass().getClassLoader());
        this.f = (C0003d) parcel.readParcelable(getClass().getClassLoader());
    }

    public final Boolean a() {
        return this.a;
    }

    public final void a(ContentValues contentValues) {
        this.e = contentValues;
    }

    public final void a(Boolean bool) {
        this.a = bool;
    }

    public final void a(String str) {
        this.b = str;
    }

    public final void a(C0003d dVar) {
        this.f = dVar;
    }

    public final String b() {
        return this.b;
    }

    public final void b(String str) {
        this.c = str;
    }

    public final String c() {
        return this.c;
    }

    public final void c(String str) {
        this.d = str;
    }

    public final String d() {
        return this.d;
    }

    public final int describeContents() {
        return 0;
    }

    public final ContentValues e() {
        return this.e;
    }

    public final C0003d f() {
        return this.f;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.a.toString());
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        parcel.writeParcelable(this.e, i);
        parcel.writeParcelable(this.f, i);
    }
}
