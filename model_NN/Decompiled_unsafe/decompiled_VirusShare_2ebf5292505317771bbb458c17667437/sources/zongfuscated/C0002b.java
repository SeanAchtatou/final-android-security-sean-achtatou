package zongfuscated;

import android.content.ContentValues;
import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: zongfuscated.b  reason: case insensitive filesystem */
public final class C0002b implements Parcelable {
    public static final Parcelable.Creator<C0002b> CREATOR = new t();
    private Boolean a;
    private int b;
    private ContentValues c;
    private final transient C0009j d;
    private final transient K e;
    private final transient y f;
    private final transient C0008i g;
    private final transient C0004e h;

    public C0002b() {
        this(new ContentValues());
    }

    public C0002b(ContentValues contentValues) {
        this.d = new v(this);
        this.e = new u(this);
        this.f = new x(this);
        this.g = new w(this);
        this.h = new p(this);
        this.c = contentValues;
    }

    /* synthetic */ C0002b(Parcel parcel) {
        this(parcel, (byte) 0);
    }

    private C0002b(Parcel parcel, byte b2) {
        this.d = new v(this);
        this.e = new u(this);
        this.f = new x(this);
        this.g = new w(this);
        this.h = new p(this);
        this.a = Boolean.valueOf(Boolean.parseBoolean(parcel.readString()));
        this.b = parcel.readInt();
        this.c = (ContentValues) parcel.readParcelable(getClass().getClassLoader());
    }

    /* access modifiers changed from: private */
    public String a(String str) {
        return this.c.containsKey(str) ? this.c.getAsString(str) : "";
    }

    static /* synthetic */ Boolean b(C0002b bVar, String str) {
        if (bVar.c.containsKey(str)) {
            return bVar.c.getAsBoolean(str);
        }
        return false;
    }

    static /* synthetic */ Integer c(C0002b bVar, String str) {
        if (bVar.c.containsKey(str)) {
            return bVar.c.getAsInteger(str);
        }
        return 0;
    }

    static /* synthetic */ Float d(C0002b bVar, String str) {
        return bVar.c.containsKey(str) ? bVar.c.getAsFloat(str) : Float.valueOf(0.0f);
    }

    public final void a(int i) {
        this.b = i;
    }

    public final void a(Boolean bool) {
        this.a = bool;
    }

    public final void a(String str, String str2) {
        this.c.put("com.zong.view.result.source", "com.zong.result.source.cancel");
        this.c.put("com.zong.view.result.error.param.code", str);
        this.c.put("com.zong.view.result.error.param.label", str2);
    }

    public final boolean a() {
        return this.a.booleanValue();
    }

    public final int b() {
        return this.b;
    }

    public final Intent c() {
        Intent intent = new Intent();
        if (this.a.booleanValue()) {
            if ("com.zong.result.source.fb.success".equals(a("com.zong.view.result.source"))) {
                intent.putExtra("credits", this.g.b());
                intent.putExtra("orderId", this.g.a());
            }
            intent.putExtra("isPartial", this.f.k());
            intent.putExtra("isMMT", this.f.h());
            intent.putExtra("isMMO", this.f.i());
            intent.putExtra("isOneClick", this.f.j());
            intent.putExtra("nbMT", this.f.l());
            intent.putExtra("billedAmount", this.f.m());
            intent.putExtra("priceAmount", this.f.n());
            intent.putExtra("outPayPref", this.f.o());
            intent.putExtra("outPayUSD", this.f.p());
        } else if ("com.zong.result.source.fb.error".equals(a("com.zong.view.result.source"))) {
            intent.putExtra("orderId", this.h.c());
            intent.putExtra("errorCode", this.h.a());
            intent.putExtra("errorLabel", this.h.b());
            if (this.h.e()) {
                intent.putExtra("errorType", 100);
            } else if (this.h.d()) {
                intent.putExtra("errorType", 101);
            }
        } else {
            intent.putExtra("errorCode", this.d.a());
            intent.putExtra("errorLabel", this.d.b());
            if ("com.zong.result.source.error".equals(a("com.zong.view.result.source"))) {
                intent.putExtra("errorType", 0);
            } else if ("com.zong.result.source.halt".equals(a("com.zong.view.result.source"))) {
                intent.putExtra("errorType", 1);
            } else if ("com.zong.result.source.cancel".equals(a("com.zong.view.result.source"))) {
                intent.putExtra("errorType", 2);
            }
        }
        return intent;
    }

    public final K d() {
        return this.e;
    }

    public final int describeContents() {
        return 0;
    }

    public final y e() {
        return this.f;
    }

    public final C0009j f() {
        return this.d;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.a.toString());
        parcel.writeInt(this.b);
        parcel.writeParcelable(this.c, i);
    }
}
