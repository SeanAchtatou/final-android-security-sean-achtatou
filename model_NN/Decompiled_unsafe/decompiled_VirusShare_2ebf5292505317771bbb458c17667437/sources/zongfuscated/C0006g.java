package zongfuscated;

/* renamed from: zongfuscated.g  reason: case insensitive filesystem */
public final class C0006g {
    String a;
    private String b;

    public C0006g() {
        this(null, null);
    }

    public C0006g(String str, String str2) {
        this.b = str;
        this.a = str2;
    }

    public final String a() {
        return this.b;
    }

    public final String b() {
        return this.a;
    }
}
