package android.support.v4.media;

import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

final class ttmhx7 implements Parcelable.Creator {
    ttmhx7() {
    }

    /* renamed from: ttmhx7 */
    public MediaDescriptionCompat createFromParcel(Parcel parcel) {
        return Build.VERSION.SDK_INT < 21 ? new MediaDescriptionCompat(parcel, null) : MediaDescriptionCompat.ttmhx7(cehyzt7dw.ttmhx7(parcel));
    }

    /* renamed from: ttmhx7 */
    public MediaDescriptionCompat[] newArray(int i) {
        return new MediaDescriptionCompat[i];
    }
}
