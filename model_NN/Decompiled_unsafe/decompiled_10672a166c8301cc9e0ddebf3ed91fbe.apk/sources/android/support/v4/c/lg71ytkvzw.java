package android.support.v4.c;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

final class lg71ytkvzw implements Set {
    final /* synthetic */ fxug2rdnfo ttmhx7;

    lg71ytkvzw(fxug2rdnfo fxug2rdnfo) {
        this.ttmhx7 = fxug2rdnfo;
    }

    public boolean addAll(Collection collection) {
        int ttmhx72 = this.ttmhx7.ttmhx7();
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            this.ttmhx7.ttmhx7(entry.getKey(), entry.getValue());
        }
        return ttmhx72 != this.ttmhx7.ttmhx7();
    }

    public void clear() {
        this.ttmhx7.cehyzt7dw();
    }

    public boolean contains(Object obj) {
        if (!(obj instanceof Map.Entry)) {
            return false;
        }
        Map.Entry entry = (Map.Entry) obj;
        int ttmhx72 = this.ttmhx7.ttmhx7(entry.getKey());
        if (ttmhx72 >= 0) {
            return cehyzt7dw.ttmhx7(this.ttmhx7.ttmhx7(ttmhx72, 1), entry.getValue());
        }
        return false;
    }

    public boolean containsAll(Collection collection) {
        for (Object contains : collection) {
            if (!contains(contains)) {
                return false;
            }
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.c.fxug2rdnfo.ttmhx7(java.util.Set, java.lang.Object):boolean
     arg types: [android.support.v4.c.lg71ytkvzw, java.lang.Object]
     candidates:
      android.support.v4.c.fxug2rdnfo.ttmhx7(java.util.Map, java.util.Collection):boolean
      android.support.v4.c.fxug2rdnfo.ttmhx7(int, int):java.lang.Object
      android.support.v4.c.fxug2rdnfo.ttmhx7(int, java.lang.Object):java.lang.Object
      android.support.v4.c.fxug2rdnfo.ttmhx7(java.lang.Object, java.lang.Object):void
      android.support.v4.c.fxug2rdnfo.ttmhx7(java.lang.Object[], int):java.lang.Object[]
      android.support.v4.c.fxug2rdnfo.ttmhx7(java.util.Set, java.lang.Object):boolean */
    public boolean equals(Object obj) {
        return fxug2rdnfo.ttmhx7((Set) this, obj);
    }

    public int hashCode() {
        int ttmhx72 = this.ttmhx7.ttmhx7() - 1;
        int i = 0;
        while (ttmhx72 >= 0) {
            Object ttmhx73 = this.ttmhx7.ttmhx7(ttmhx72, 0);
            Object ttmhx74 = this.ttmhx7.ttmhx7(ttmhx72, 1);
            ttmhx72--;
            i += (ttmhx74 == null ? 0 : ttmhx74.hashCode()) ^ (ttmhx73 == null ? 0 : ttmhx73.hashCode());
        }
        return i;
    }

    public boolean isEmpty() {
        return this.ttmhx7.ttmhx7() == 0;
    }

    public Iterator iterator() {
        return new b5zlaptmyxarl(this.ttmhx7);
    }

    public boolean remove(Object obj) {
        throw new UnsupportedOperationException();
    }

    public boolean removeAll(Collection collection) {
        throw new UnsupportedOperationException();
    }

    public boolean retainAll(Collection collection) {
        throw new UnsupportedOperationException();
    }

    public int size() {
        return this.ttmhx7.ttmhx7();
    }

    public Object[] toArray() {
        throw new UnsupportedOperationException();
    }

    public Object[] toArray(Object[] objArr) {
        throw new UnsupportedOperationException();
    }

    /* renamed from: ttmhx7 */
    public boolean add(Map.Entry entry) {
        throw new UnsupportedOperationException();
    }
}
