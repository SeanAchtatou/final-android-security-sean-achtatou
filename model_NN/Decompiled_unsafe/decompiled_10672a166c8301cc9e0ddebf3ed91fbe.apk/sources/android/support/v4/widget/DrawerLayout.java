package android.support.v4.widget;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.a.ttmhx7;
import android.support.v4.view.ay6ebym1yp0qgk;
import android.support.v4.view.e8kxjqktk9t;
import android.support.v4.view.ijavw7l1x;
import android.support.v4.view.rulrdod1midre;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

public class DrawerLayout extends ViewGroup {
    private static final boolean cehyzt7dw;
    /* access modifiers changed from: private */
    public static final int[] ozpoxuz523b2 = {16842931};
    static final ozpoxuz523b2 ttmhx7;
    private Object aecbla89ntoa8;
    private final lg71ytkvzw ay6ebym1yp0qgk;
    private final aecbla89ntoa8 b5zlaptmyxarl;
    private boolean bpogj6;
    private int ca2ssr26fefu;
    private usuayu88rw4 cpgyvt8o4r3;
    private float e8kxjqktk9t;
    private final aecbla89ntoa8 ef5tn1cvshg414;
    private Drawable eyli1ymagd3o;
    private int flawb66z00q;
    private int fxug2rdnfo;
    private final lg71ytkvzw iux03f6yieb;
    private boolean k3jokks5k5;
    private Paint lg71ytkvzw;
    private float lqwegpi5;
    private int mhtc4dliin7r;
    private float mqnmk83l0o;
    private boolean oc9mgl157cp;
    private Drawable ol99ycz2wbkd;
    private boolean rulrdod1midre;
    private Drawable sgnd7s4;
    private final ttmhx7 uin6g3d5rqgcbs;
    private int usuayu88rw4;
    private boolean zs1ge47fq1dgv5;

    public class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator CREATOR = new e8kxjqktk9t();
        int cehyzt7dw = 0;
        int ozpoxuz523b2 = 0;
        int ttmhx7 = 0;

        public SavedState(Parcel parcel) {
            super(parcel);
            this.ttmhx7 = parcel.readInt();
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.ttmhx7);
        }
    }

    static {
        boolean z = true;
        if (Build.VERSION.SDK_INT < 19) {
            z = false;
        }
        cehyzt7dw = z;
        if (Build.VERSION.SDK_INT >= 21) {
            ttmhx7 = new cehyzt7dw();
        } else {
            ttmhx7 = new uin6g3d5rqgcbs();
        }
    }

    private View fxug2rdnfo() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (e8kxjqktk9t(childAt) && b5zlaptmyxarl(childAt)) {
                return childAt;
            }
        }
        return null;
    }

    private static boolean iux03f6yieb(View view) {
        Drawable background = view.getBackground();
        return background != null && background.getOpacity() == -1;
    }

    static String ozpoxuz523b2(int i) {
        return (i & 3) == 3 ? "LEFT" : (i & 5) == 5 ? "RIGHT" : Integer.toHexString(i);
    }

    private void ttmhx7(View view, boolean z) {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if ((z || e8kxjqktk9t(childAt)) && (!z || childAt != view)) {
                ijavw7l1x.ozpoxuz523b2(childAt, 4);
            } else {
                ijavw7l1x.ozpoxuz523b2(childAt, 1);
            }
        }
    }

    private boolean uin6g3d5rqgcbs() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            if (((fxug2rdnfo) getChildAt(i).getLayoutParams()).cehyzt7dw) {
                return true;
            }
        }
        return false;
    }

    private boolean usuayu88rw4() {
        return fxug2rdnfo() != null;
    }

    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        super.addView(view, i, layoutParams);
        if (ttmhx7() != null || e8kxjqktk9t(view)) {
            ijavw7l1x.ozpoxuz523b2(view, 4);
        } else {
            ijavw7l1x.ozpoxuz523b2(view, 1);
        }
        if (!cehyzt7dw) {
            ijavw7l1x.ttmhx7(view, this.uin6g3d5rqgcbs);
        }
    }

    public boolean b5zlaptmyxarl(View view) {
        if (e8kxjqktk9t(view)) {
            return ((fxug2rdnfo) view.getLayoutParams()).ozpoxuz523b2 > 0.0f;
        }
        throw new IllegalArgumentException("View " + view + " is not a drawer");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.DrawerLayout.ttmhx7(android.view.View, boolean):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.widget.DrawerLayout.ttmhx7(int, int):void
      android.support.v4.widget.DrawerLayout.ttmhx7(android.view.View, float):void
      android.support.v4.widget.DrawerLayout.ttmhx7(android.view.View, int):boolean
      android.support.v4.widget.DrawerLayout.ttmhx7(android.view.View, boolean):void */
    /* access modifiers changed from: package-private */
    public void cehyzt7dw(View view) {
        fxug2rdnfo fxug2rdnfo2 = (fxug2rdnfo) view.getLayoutParams();
        if (!fxug2rdnfo2.uin6g3d5rqgcbs) {
            fxug2rdnfo2.uin6g3d5rqgcbs = true;
            if (this.cpgyvt8o4r3 != null) {
                this.cpgyvt8o4r3.ttmhx7(view);
            }
            ttmhx7(view, true);
            if (hasWindowFocus()) {
                sendAccessibilityEvent(32);
            }
            view.requestFocus();
        }
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return (layoutParams instanceof fxug2rdnfo) && super.checkLayoutParams(layoutParams);
    }

    public void computeScroll() {
        int childCount = getChildCount();
        float f = 0.0f;
        for (int i = 0; i < childCount; i++) {
            f = Math.max(f, ((fxug2rdnfo) getChildAt(i).getLayoutParams()).ozpoxuz523b2);
        }
        this.e8kxjqktk9t = f;
        if (this.ef5tn1cvshg414.ttmhx7(true) || this.b5zlaptmyxarl.ttmhx7(true)) {
            ijavw7l1x.ozpoxuz523b2(this);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* access modifiers changed from: protected */
    public boolean drawChild(Canvas canvas, View view, long j) {
        int i;
        int height = getHeight();
        boolean fxug2rdnfo2 = fxug2rdnfo(view);
        int i2 = 0;
        int width = getWidth();
        int save = canvas.save();
        if (fxug2rdnfo2) {
            int childCount = getChildCount();
            int i3 = 0;
            while (i3 < childCount) {
                View childAt = getChildAt(i3);
                if (childAt != view && childAt.getVisibility() == 0 && iux03f6yieb(childAt) && e8kxjqktk9t(childAt)) {
                    if (childAt.getHeight() < height) {
                        i = width;
                    } else if (ttmhx7(childAt, 3)) {
                        int right = childAt.getRight();
                        if (right <= i2) {
                            right = i2;
                        }
                        i2 = right;
                        i = width;
                    } else {
                        i = childAt.getLeft();
                        if (i < width) {
                        }
                    }
                    i3++;
                    width = i;
                }
                i = width;
                i3++;
                width = i;
            }
            canvas.clipRect(i2, 0, width, getHeight());
        }
        int i4 = width;
        boolean drawChild = super.drawChild(canvas, view, j);
        canvas.restoreToCount(save);
        if (this.e8kxjqktk9t > 0.0f && fxug2rdnfo2) {
            this.lg71ytkvzw.setColor((((int) (((float) ((this.fxug2rdnfo & -16777216) >>> 24)) * this.e8kxjqktk9t)) << 24) | (this.fxug2rdnfo & 16777215));
            canvas.drawRect((float) i2, 0.0f, (float) i4, (float) getHeight(), this.lg71ytkvzw);
        } else if (this.ol99ycz2wbkd != null && ttmhx7(view, 3)) {
            int intrinsicWidth = this.ol99ycz2wbkd.getIntrinsicWidth();
            int right2 = view.getRight();
            float max = Math.max(0.0f, Math.min(((float) right2) / ((float) this.ef5tn1cvshg414.ozpoxuz523b2()), 1.0f));
            this.ol99ycz2wbkd.setBounds(right2, view.getTop(), intrinsicWidth + right2, view.getBottom());
            this.ol99ycz2wbkd.setAlpha((int) (255.0f * max));
            this.ol99ycz2wbkd.draw(canvas);
        } else if (this.eyli1ymagd3o != null && ttmhx7(view, 5)) {
            int intrinsicWidth2 = this.eyli1ymagd3o.getIntrinsicWidth();
            int left = view.getLeft();
            float max2 = Math.max(0.0f, Math.min(((float) (getWidth() - left)) / ((float) this.b5zlaptmyxarl.ozpoxuz523b2()), 1.0f));
            this.eyli1ymagd3o.setBounds(left - intrinsicWidth2, view.getTop(), left, view.getBottom());
            this.eyli1ymagd3o.setAlpha((int) (255.0f * max2));
            this.eyli1ymagd3o.draw(canvas);
        }
        return drawChild;
    }

    /* access modifiers changed from: package-private */
    public boolean e8kxjqktk9t(View view) {
        return (e8kxjqktk9t.ttmhx7(((fxug2rdnfo) view.getLayoutParams()).ttmhx7, ijavw7l1x.uin6g3d5rqgcbs(view)) & 7) != 0;
    }

    public void ef5tn1cvshg414(View view) {
        if (!e8kxjqktk9t(view)) {
            throw new IllegalArgumentException("View " + view + " is not a sliding drawer");
        }
        if (this.bpogj6) {
            fxug2rdnfo fxug2rdnfo2 = (fxug2rdnfo) view.getLayoutParams();
            fxug2rdnfo2.ozpoxuz523b2 = 0.0f;
            fxug2rdnfo2.uin6g3d5rqgcbs = false;
        } else if (ttmhx7(view, 3)) {
            this.ef5tn1cvshg414.ttmhx7(view, -view.getWidth(), view.getTop());
        } else {
            this.b5zlaptmyxarl.ttmhx7(view, getWidth(), view.getTop());
        }
        invalidate();
    }

    /* access modifiers changed from: package-private */
    public boolean fxug2rdnfo(View view) {
        return ((fxug2rdnfo) view.getLayoutParams()).ttmhx7 == 0;
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new fxug2rdnfo(-1, -1);
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new fxug2rdnfo(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof fxug2rdnfo ? new fxug2rdnfo((fxug2rdnfo) layoutParams) : layoutParams instanceof ViewGroup.MarginLayoutParams ? new fxug2rdnfo((ViewGroup.MarginLayoutParams) layoutParams) : new fxug2rdnfo(layoutParams);
    }

    public Drawable getStatusBarBackgroundDrawable() {
        return this.sgnd7s4;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.DrawerLayout.ttmhx7(android.view.View, boolean):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.widget.DrawerLayout.ttmhx7(int, int):void
      android.support.v4.widget.DrawerLayout.ttmhx7(android.view.View, float):void
      android.support.v4.widget.DrawerLayout.ttmhx7(android.view.View, int):boolean
      android.support.v4.widget.DrawerLayout.ttmhx7(android.view.View, boolean):void */
    public void lg71ytkvzw(View view) {
        if (!e8kxjqktk9t(view)) {
            throw new IllegalArgumentException("View " + view + " is not a sliding drawer");
        }
        if (this.bpogj6) {
            fxug2rdnfo fxug2rdnfo2 = (fxug2rdnfo) view.getLayoutParams();
            fxug2rdnfo2.ozpoxuz523b2 = 1.0f;
            fxug2rdnfo2.uin6g3d5rqgcbs = true;
            ttmhx7(view, true);
        } else if (ttmhx7(view, 3)) {
            this.ef5tn1cvshg414.ttmhx7(view, 0, view.getTop());
        } else {
            this.b5zlaptmyxarl.ttmhx7(view, getWidth() - view.getWidth(), view.getTop());
        }
        invalidate();
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.bpogj6 = true;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.bpogj6 = true;
    }

    public void onDraw(Canvas canvas) {
        int ttmhx72;
        super.onDraw(canvas);
        if (this.zs1ge47fq1dgv5 && this.sgnd7s4 != null && (ttmhx72 = ttmhx7.ttmhx7(this.aecbla89ntoa8)) > 0) {
            this.sgnd7s4.setBounds(0, 0, getWidth(), ttmhx72);
            this.sgnd7s4.draw(canvas);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        boolean z;
        View uin6g3d5rqgcbs2;
        int ttmhx72 = rulrdod1midre.ttmhx7(motionEvent);
        boolean ttmhx73 = this.ef5tn1cvshg414.ttmhx7(motionEvent) | this.b5zlaptmyxarl.ttmhx7(motionEvent);
        switch (ttmhx72) {
            case 0:
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                this.mqnmk83l0o = x;
                this.lqwegpi5 = y;
                z = this.e8kxjqktk9t > 0.0f && (uin6g3d5rqgcbs2 = this.ef5tn1cvshg414.uin6g3d5rqgcbs((int) x, (int) y)) != null && fxug2rdnfo(uin6g3d5rqgcbs2);
                this.k3jokks5k5 = false;
                this.rulrdod1midre = false;
                break;
            case 1:
            case 3:
                ttmhx7(true);
                this.k3jokks5k5 = false;
                this.rulrdod1midre = false;
                z = false;
                break;
            case 2:
                if (this.ef5tn1cvshg414.uin6g3d5rqgcbs(3)) {
                    this.iux03f6yieb.ttmhx7();
                    this.ay6ebym1yp0qgk.ttmhx7();
                    z = false;
                    break;
                }
                z = false;
                break;
            default:
                z = false;
                break;
        }
        return ttmhx73 || z || uin6g3d5rqgcbs() || this.rulrdod1midre;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4 || !usuayu88rw4()) {
            return super.onKeyDown(i, keyEvent);
        }
        ay6ebym1yp0qgk.ozpoxuz523b2(keyEvent);
        return true;
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return super.onKeyUp(i, keyEvent);
        }
        View fxug2rdnfo2 = fxug2rdnfo();
        if (fxug2rdnfo2 != null && ttmhx7(fxug2rdnfo2) == 0) {
            ozpoxuz523b2();
        }
        return fxug2rdnfo2 != null;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5;
        float f;
        this.oc9mgl157cp = true;
        int i6 = i3 - i;
        int childCount = getChildCount();
        for (int i7 = 0; i7 < childCount; i7++) {
            View childAt = getChildAt(i7);
            if (childAt.getVisibility() != 8) {
                fxug2rdnfo fxug2rdnfo2 = (fxug2rdnfo) childAt.getLayoutParams();
                if (fxug2rdnfo(childAt)) {
                    childAt.layout(fxug2rdnfo2.leftMargin, fxug2rdnfo2.topMargin, fxug2rdnfo2.leftMargin + childAt.getMeasuredWidth(), fxug2rdnfo2.topMargin + childAt.getMeasuredHeight());
                } else {
                    int measuredWidth = childAt.getMeasuredWidth();
                    int measuredHeight = childAt.getMeasuredHeight();
                    if (ttmhx7(childAt, 3)) {
                        i5 = ((int) (((float) measuredWidth) * fxug2rdnfo2.ozpoxuz523b2)) + (-measuredWidth);
                        f = ((float) (measuredWidth + i5)) / ((float) measuredWidth);
                    } else {
                        i5 = i6 - ((int) (((float) measuredWidth) * fxug2rdnfo2.ozpoxuz523b2));
                        f = ((float) (i6 - i5)) / ((float) measuredWidth);
                    }
                    boolean z2 = f != fxug2rdnfo2.ozpoxuz523b2;
                    switch (fxug2rdnfo2.ttmhx7 & 112) {
                        case 16:
                            int i8 = i4 - i2;
                            int i9 = (i8 - measuredHeight) / 2;
                            if (i9 < fxug2rdnfo2.topMargin) {
                                i9 = fxug2rdnfo2.topMargin;
                            } else if (i9 + measuredHeight > i8 - fxug2rdnfo2.bottomMargin) {
                                i9 = (i8 - fxug2rdnfo2.bottomMargin) - measuredHeight;
                            }
                            childAt.layout(i5, i9, measuredWidth + i5, measuredHeight + i9);
                            break;
                        case 80:
                            int i10 = i4 - i2;
                            childAt.layout(i5, (i10 - fxug2rdnfo2.bottomMargin) - childAt.getMeasuredHeight(), measuredWidth + i5, i10 - fxug2rdnfo2.bottomMargin);
                            break;
                        default:
                            childAt.layout(i5, fxug2rdnfo2.topMargin, measuredWidth + i5, measuredHeight + fxug2rdnfo2.topMargin);
                            break;
                    }
                    if (z2) {
                        ozpoxuz523b2(childAt, f);
                    }
                    int i11 = fxug2rdnfo2.ozpoxuz523b2 > 0.0f ? 0 : 4;
                    if (childAt.getVisibility() != i11) {
                        childAt.setVisibility(i11);
                    }
                }
            }
        }
        this.oc9mgl157cp = false;
        this.bpogj6 = false;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0054, code lost:
        if (r5 != 0) goto L_0x0056;
     */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0040  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r14, int r15) {
        /*
            r13 = this;
            r1 = 300(0x12c, float:4.2E-43)
            r7 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            r12 = 1073741824(0x40000000, float:2.0)
            int r3 = android.view.View.MeasureSpec.getMode(r14)
            int r5 = android.view.View.MeasureSpec.getMode(r15)
            int r2 = android.view.View.MeasureSpec.getSize(r14)
            int r0 = android.view.View.MeasureSpec.getSize(r15)
            if (r3 != r12) goto L_0x001b
            if (r5 == r12) goto L_0x0056
        L_0x001b:
            boolean r6 = r13.isInEditMode()
            if (r6 == 0) goto L_0x0058
            if (r3 != r7) goto L_0x0050
        L_0x0023:
            if (r5 != r7) goto L_0x0054
            r1 = r0
        L_0x0026:
            r13.setMeasuredDimension(r2, r1)
            java.lang.Object r0 = r13.aecbla89ntoa8
            if (r0 == 0) goto L_0x0060
            boolean r0 = android.support.v4.view.ijavw7l1x.fxug2rdnfo(r13)
            if (r0 == 0) goto L_0x0060
            r0 = 1
            r3 = r0
        L_0x0035:
            int r6 = android.support.v4.view.ijavw7l1x.uin6g3d5rqgcbs(r13)
            int r7 = r13.getChildCount()
            r5 = r4
        L_0x003e:
            if (r5 >= r7) goto L_0x0138
            android.view.View r8 = r13.getChildAt(r5)
            int r0 = r8.getVisibility()
            r9 = 8
            if (r0 != r9) goto L_0x0062
        L_0x004c:
            int r0 = r5 + 1
            r5 = r0
            goto L_0x003e
        L_0x0050:
            if (r3 != 0) goto L_0x0023
            r2 = r1
            goto L_0x0023
        L_0x0054:
            if (r5 == 0) goto L_0x0026
        L_0x0056:
            r1 = r0
            goto L_0x0026
        L_0x0058:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "DrawerLayout must be measured with MeasureSpec.EXACTLY."
            r0.<init>(r1)
            throw r0
        L_0x0060:
            r3 = r4
            goto L_0x0035
        L_0x0062:
            android.view.ViewGroup$LayoutParams r0 = r8.getLayoutParams()
            android.support.v4.widget.fxug2rdnfo r0 = (android.support.v4.widget.fxug2rdnfo) r0
            if (r3 == 0) goto L_0x007d
            int r9 = r0.ttmhx7
            int r9 = android.support.v4.view.e8kxjqktk9t.ttmhx7(r9, r6)
            boolean r10 = android.support.v4.view.ijavw7l1x.fxug2rdnfo(r8)
            if (r10 == 0) goto L_0x009e
            android.support.v4.widget.ozpoxuz523b2 r10 = android.support.v4.widget.DrawerLayout.ttmhx7
            java.lang.Object r11 = r13.aecbla89ntoa8
            r10.ttmhx7(r8, r11, r9)
        L_0x007d:
            boolean r9 = r13.fxug2rdnfo(r8)
            if (r9 == 0) goto L_0x00a6
            int r9 = r0.leftMargin
            int r9 = r2 - r9
            int r10 = r0.rightMargin
            int r9 = r9 - r10
            int r9 = android.view.View.MeasureSpec.makeMeasureSpec(r9, r12)
            int r10 = r0.topMargin
            int r10 = r1 - r10
            int r0 = r0.bottomMargin
            int r0 = r10 - r0
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r12)
            r8.measure(r9, r0)
            goto L_0x004c
        L_0x009e:
            android.support.v4.widget.ozpoxuz523b2 r10 = android.support.v4.widget.DrawerLayout.ttmhx7
            java.lang.Object r11 = r13.aecbla89ntoa8
            r10.ttmhx7(r0, r11, r9)
            goto L_0x007d
        L_0x00a6:
            boolean r9 = r13.e8kxjqktk9t(r8)
            if (r9 == 0) goto L_0x0109
            int r9 = r13.usuayu88rw4(r8)
            r9 = r9 & 7
            r10 = r4 & r9
            if (r10 == 0) goto L_0x00eb
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Child drawer has absolute gravity "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = ozpoxuz523b2(r9)
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = " but this "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "DrawerLayout"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = " already has a "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "drawer view along that edge"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x00eb:
            int r9 = r13.usuayu88rw4
            int r10 = r0.leftMargin
            int r9 = r9 + r10
            int r10 = r0.rightMargin
            int r9 = r9 + r10
            int r10 = r0.width
            int r9 = getChildMeasureSpec(r14, r9, r10)
            int r10 = r0.topMargin
            int r11 = r0.bottomMargin
            int r10 = r10 + r11
            int r0 = r0.height
            int r0 = getChildMeasureSpec(r15, r10, r0)
            r8.measure(r9, r0)
            goto L_0x004c
        L_0x0109:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Child "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r8)
            java.lang.String r2 = " at index "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.String r2 = " does not have a valid layout_gravity - must be Gravity.LEFT, "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "Gravity.RIGHT or Gravity.NO_GRAVITY"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0138:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.widget.DrawerLayout.onMeasure(int, int):void");
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        View ttmhx72;
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (!(savedState.ttmhx7 == 0 || (ttmhx72 = ttmhx7(savedState.ttmhx7)) == null)) {
            lg71ytkvzw(ttmhx72);
        }
        ttmhx7(savedState.ozpoxuz523b2, 3);
        ttmhx7(savedState.cehyzt7dw, 5);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        View ttmhx72 = ttmhx7();
        if (ttmhx72 != null) {
            savedState.ttmhx7 = ((fxug2rdnfo) ttmhx72.getLayoutParams()).ttmhx7;
        }
        savedState.ozpoxuz523b2 = this.ca2ssr26fefu;
        savedState.cehyzt7dw = this.flawb66z00q;
        return savedState;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean z;
        View ttmhx72;
        this.ef5tn1cvshg414.ozpoxuz523b2(motionEvent);
        this.b5zlaptmyxarl.ozpoxuz523b2(motionEvent);
        switch (motionEvent.getAction() & 255) {
            case 0:
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                this.mqnmk83l0o = x;
                this.lqwegpi5 = y;
                this.k3jokks5k5 = false;
                this.rulrdod1midre = false;
                break;
            case 1:
                float x2 = motionEvent.getX();
                float y2 = motionEvent.getY();
                View uin6g3d5rqgcbs2 = this.ef5tn1cvshg414.uin6g3d5rqgcbs((int) x2, (int) y2);
                if (uin6g3d5rqgcbs2 != null && fxug2rdnfo(uin6g3d5rqgcbs2)) {
                    float f = x2 - this.mqnmk83l0o;
                    float f2 = y2 - this.lqwegpi5;
                    int uin6g3d5rqgcbs3 = this.ef5tn1cvshg414.uin6g3d5rqgcbs();
                    if ((f * f) + (f2 * f2) < ((float) (uin6g3d5rqgcbs3 * uin6g3d5rqgcbs3)) && (ttmhx72 = ttmhx7()) != null) {
                        z = ttmhx7(ttmhx72) == 2;
                        ttmhx7(z);
                        this.k3jokks5k5 = false;
                        break;
                    }
                }
                z = true;
                ttmhx7(z);
                this.k3jokks5k5 = false;
            case 3:
                ttmhx7(true);
                this.k3jokks5k5 = false;
                this.rulrdod1midre = false;
                break;
        }
        return true;
    }

    public void ozpoxuz523b2() {
        ttmhx7(false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.widget.DrawerLayout.ttmhx7(android.view.View, boolean):void
     arg types: [android.view.View, int]
     candidates:
      android.support.v4.widget.DrawerLayout.ttmhx7(int, int):void
      android.support.v4.widget.DrawerLayout.ttmhx7(android.view.View, float):void
      android.support.v4.widget.DrawerLayout.ttmhx7(android.view.View, int):boolean
      android.support.v4.widget.DrawerLayout.ttmhx7(android.view.View, boolean):void */
    /* access modifiers changed from: package-private */
    public void ozpoxuz523b2(View view) {
        View rootView;
        fxug2rdnfo fxug2rdnfo2 = (fxug2rdnfo) view.getLayoutParams();
        if (fxug2rdnfo2.uin6g3d5rqgcbs) {
            fxug2rdnfo2.uin6g3d5rqgcbs = false;
            if (this.cpgyvt8o4r3 != null) {
                this.cpgyvt8o4r3.ozpoxuz523b2(view);
            }
            ttmhx7(view, false);
            if (hasWindowFocus() && (rootView = getRootView()) != null) {
                rootView.sendAccessibilityEvent(32);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void ozpoxuz523b2(View view, float f) {
        fxug2rdnfo fxug2rdnfo2 = (fxug2rdnfo) view.getLayoutParams();
        if (f != fxug2rdnfo2.ozpoxuz523b2) {
            fxug2rdnfo2.ozpoxuz523b2 = f;
            ttmhx7(view, f);
        }
    }

    public void requestDisallowInterceptTouchEvent(boolean z) {
        super.requestDisallowInterceptTouchEvent(z);
        this.k3jokks5k5 = z;
        if (z) {
            ttmhx7(true);
        }
    }

    public void requestLayout() {
        if (!this.oc9mgl157cp) {
            super.requestLayout();
        }
    }

    public void setDrawerListener(usuayu88rw4 usuayu88rw42) {
        this.cpgyvt8o4r3 = usuayu88rw42;
    }

    public void setDrawerLockMode(int i) {
        ttmhx7(i, 3);
        ttmhx7(i, 5);
    }

    public void setScrimColor(int i) {
        this.fxug2rdnfo = i;
        invalidate();
    }

    public void setStatusBarBackground(int i) {
        this.sgnd7s4 = i != 0 ? ttmhx7.ttmhx7(getContext(), i) : null;
    }

    public void setStatusBarBackground(Drawable drawable) {
        this.sgnd7s4 = drawable;
    }

    public void setStatusBarBackgroundColor(int i) {
        this.sgnd7s4 = new ColorDrawable(i);
    }

    public int ttmhx7(View view) {
        int usuayu88rw42 = usuayu88rw4(view);
        if (usuayu88rw42 == 3) {
            return this.ca2ssr26fefu;
        }
        if (usuayu88rw42 == 5) {
            return this.flawb66z00q;
        }
        return 0;
    }

    /* access modifiers changed from: package-private */
    public View ttmhx7() {
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (((fxug2rdnfo) childAt.getLayoutParams()).uin6g3d5rqgcbs) {
                return childAt;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public View ttmhx7(int i) {
        int ttmhx72 = e8kxjqktk9t.ttmhx7(i, ijavw7l1x.uin6g3d5rqgcbs(this)) & 7;
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if ((usuayu88rw4(childAt) & 7) == ttmhx72) {
                return childAt;
            }
        }
        return null;
    }

    public void ttmhx7(int i, int i2) {
        int ttmhx72 = e8kxjqktk9t.ttmhx7(i2, ijavw7l1x.uin6g3d5rqgcbs(this));
        if (ttmhx72 == 3) {
            this.ca2ssr26fefu = i;
        } else if (ttmhx72 == 5) {
            this.flawb66z00q = i;
        }
        if (i != 0) {
            (ttmhx72 == 3 ? this.ef5tn1cvshg414 : this.b5zlaptmyxarl).usuayu88rw4();
        }
        switch (i) {
            case 1:
                View ttmhx73 = ttmhx7(ttmhx72);
                if (ttmhx73 != null) {
                    ef5tn1cvshg414(ttmhx73);
                    return;
                }
                return;
            case 2:
                View ttmhx74 = ttmhx7(ttmhx72);
                if (ttmhx74 != null) {
                    lg71ytkvzw(ttmhx74);
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: package-private */
    public void ttmhx7(int i, int i2, View view) {
        int i3 = 1;
        int ttmhx72 = this.ef5tn1cvshg414.ttmhx7();
        int ttmhx73 = this.b5zlaptmyxarl.ttmhx7();
        if (!(ttmhx72 == 1 || ttmhx73 == 1)) {
            i3 = (ttmhx72 == 2 || ttmhx73 == 2) ? 2 : 0;
        }
        if (view != null && i2 == 0) {
            fxug2rdnfo fxug2rdnfo2 = (fxug2rdnfo) view.getLayoutParams();
            if (fxug2rdnfo2.ozpoxuz523b2 == 0.0f) {
                ozpoxuz523b2(view);
            } else if (fxug2rdnfo2.ozpoxuz523b2 == 1.0f) {
                cehyzt7dw(view);
            }
        }
        if (i3 != this.mhtc4dliin7r) {
            this.mhtc4dliin7r = i3;
            if (this.cpgyvt8o4r3 != null) {
                this.cpgyvt8o4r3.ttmhx7(i3);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void ttmhx7(View view, float f) {
        if (this.cpgyvt8o4r3 != null) {
            this.cpgyvt8o4r3.ttmhx7(view, f);
        }
    }

    /* access modifiers changed from: package-private */
    public void ttmhx7(boolean z) {
        int childCount = getChildCount();
        boolean z2 = false;
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            fxug2rdnfo fxug2rdnfo2 = (fxug2rdnfo) childAt.getLayoutParams();
            if (e8kxjqktk9t(childAt) && (!z || fxug2rdnfo2.cehyzt7dw)) {
                z2 = ttmhx7(childAt, 3) ? z2 | this.ef5tn1cvshg414.ttmhx7(childAt, -childAt.getWidth(), childAt.getTop()) : z2 | this.b5zlaptmyxarl.ttmhx7(childAt, getWidth(), childAt.getTop());
                fxug2rdnfo2.cehyzt7dw = false;
            }
        }
        this.iux03f6yieb.ttmhx7();
        this.ay6ebym1yp0qgk.ttmhx7();
        if (z2) {
            invalidate();
        }
    }

    /* access modifiers changed from: package-private */
    public boolean ttmhx7(View view, int i) {
        return (usuayu88rw4(view) & i) == i;
    }

    /* access modifiers changed from: package-private */
    public float uin6g3d5rqgcbs(View view) {
        return ((fxug2rdnfo) view.getLayoutParams()).ozpoxuz523b2;
    }

    /* access modifiers changed from: package-private */
    public int usuayu88rw4(View view) {
        return e8kxjqktk9t.ttmhx7(((fxug2rdnfo) view.getLayoutParams()).ttmhx7, ijavw7l1x.uin6g3d5rqgcbs(this));
    }
}
