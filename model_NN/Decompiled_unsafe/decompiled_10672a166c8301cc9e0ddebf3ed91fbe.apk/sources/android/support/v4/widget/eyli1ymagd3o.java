package android.support.v4.widget;

import android.util.Log;
import android.view.View;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

class eyli1ymagd3o extends ol99ycz2wbkd {
    private Field ozpoxuz523b2;
    private Method ttmhx7;

    eyli1ymagd3o() {
        try {
            this.ttmhx7 = View.class.getDeclaredMethod("getDisplayList", null);
        } catch (NoSuchMethodException e) {
            Log.e("SlidingPaneLayout", "Couldn't fetch getDisplayList method; dimming won't work right.", e);
        }
        try {
            this.ozpoxuz523b2 = View.class.getDeclaredField("mRecreateDisplayList");
            this.ozpoxuz523b2.setAccessible(true);
        } catch (NoSuchFieldException e2) {
            Log.e("SlidingPaneLayout", "Couldn't fetch mRecreateDisplayList field; dimming will be slow.", e2);
        }
    }

    public void ttmhx7(SlidingPaneLayout slidingPaneLayout, View view) {
        if (this.ttmhx7 == null || this.ozpoxuz523b2 == null) {
            view.invalidate();
            return;
        }
        try {
            this.ozpoxuz523b2.setBoolean(view, true);
            this.ttmhx7.invoke(view, null);
        } catch (Exception e) {
            Log.e("SlidingPaneLayout", "Error refreshing display list state", e);
        }
        super.ttmhx7(slidingPaneLayout, view);
    }
}
