package com.soft.install;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Offert extends Activity {
    private static final int RESULT_OK = 1;
    private Button yesButton;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.offert);
        setListeners();
    }

    private void setListeners() {
        this.yesButton = (Button) findViewById(R.id.yes_button_offert);
        this.yesButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Offert.this.setResult(Offert.RESULT_OK);
                Offert.this.finish();
            }
        });
    }
}
