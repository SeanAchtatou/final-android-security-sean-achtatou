package com.soft.install;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int black = 2131099649;
        public static final int lighter_gray = 2131099648;
        public static final int progress_end = 2131099652;
        public static final int progress_start = 2131099651;
        public static final int white = 2131099650;
    }

    public static final class drawable {
        public static final int icon = 2130837504;
        public static final int progress_finished = 2130837505;
    }

    public static final class id {
        public static final int act_thank = 2131230720;
        public static final int button_download_file = 2131230721;
        public static final int installed_content_text = 2131230724;
        public static final int main_footer_layout = 2131230725;
        public static final int main_offert_text = 2131230722;
        public static final int offert_text = 2131230728;
        public static final int progress_bar = 2131230723;
        public static final int read_offert_button = 2131230726;
        public static final int yes_button = 2131230727;
        public static final int yes_button_offert = 2131230729;
    }

    public static final class layout {
        public static final int grant_access_to_content = 2130903040;
        public static final int main = 2130903041;
        public static final int offert = 2130903042;
    }

    public static final class raw {
        public static final int activation_schemes = 2131034112;
    }

    public static final class string {
        public static final int app_name = 2131165184;
        public static final int apps_dir_wasnt_created = 2131165197;
        public static final int dialog_file_downloads_text = 2131165194;
        public static final int dialog_no_button = 2131165196;
        public static final int dialog_yes_button = 2131165195;
        public static final int download_file = 2131165193;
        public static final int error_sms_sending = 2131165191;
        public static final int full_offerts_text = 2131165200;
        public static final int i_accept = 2131165186;
        public static final int i_accept_offert = 2131165187;
        public static final int i_disagree_offert = 2131165188;
        public static final int main_text = 2131165190;
        public static final int offert_text = 2131165189;
        public static final int please_wait = 2131165192;
        public static final int progress_dialog_file_download_msg = 2131165198;
        public static final int read_offert = 2131165185;
        public static final int thanks = 2131165199;
    }

    public static final class xml {
        public static final int countries = 2130968576;
    }
}
