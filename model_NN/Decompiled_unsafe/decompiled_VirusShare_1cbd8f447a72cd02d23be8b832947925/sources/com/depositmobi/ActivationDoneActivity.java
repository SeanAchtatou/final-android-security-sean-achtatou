package com.depositmobi;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ActivationDoneActivity extends Activity implements View.OnClickListener {
    private Button mButton;
    private TextView mTextView;
    private String url;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activation_done);
        this.mTextView = (TextView) findViewById(R.id.thanks_for_activation_text);
        this.mButton = (Button) findViewById(R.id.button_download_file);
        this.mButton.setOnClickListener(this);
        this.url = getIntent().getStringExtra("URL");
        this.mTextView.setText(String.valueOf(getResources().getString(R.string.thanks_for_activation)) + this.url);
    }

    public void onClick(View v) {
        if (v.getId() == R.id.button_download_file) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse(this.url)));
        }
    }
}
