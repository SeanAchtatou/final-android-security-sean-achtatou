package com.google.gson.reflect;

import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.HashMap;
import java.util.Map;

public abstract class TypeToken<T> {
    final Class<? super T> a;
    final Type b;

    private static class a<T> extends TypeToken<T> {
        public a(Type type) {
            super(type);
        }
    }

    protected TypeToken() {
        Type genericSuperclass = getClass().getGenericSuperclass();
        if (genericSuperclass instanceof Class) {
            throw new RuntimeException("Missing type parameter.");
        }
        this.b = ((ParameterizedType) genericSuperclass).getActualTypeArguments()[0];
        this.a = a(this.b);
    }

    /* synthetic */ TypeToken(Type x0) {
        this(x0, (byte) 0);
    }

    private TypeToken(Type type, byte b2) {
        if (type == null) {
            throw new NullPointerException("type");
        }
        this.a = a(type);
        this.b = type;
    }

    private static AssertionError a(Type type, Class<?>... clsArr) {
        StringBuilder sb = new StringBuilder("Unexpected type. Expected one of: ");
        for (Class<?> name : clsArr) {
            sb.append(name.getName()).append(", ");
        }
        sb.append("but got: ").append(type.getClass().getName()).append(", for type token: ").append(type.toString()).append('.');
        return new AssertionError(sb.toString());
    }

    private static Class<?> a(Type type) {
        if (type instanceof Class) {
            return (Class) type;
        }
        if (type instanceof ParameterizedType) {
            Type rawType = ((ParameterizedType) type).getRawType();
            if (rawType instanceof Class) {
                return (Class) rawType;
            }
            throw a(rawType, Class.class);
        } else if (type instanceof GenericArrayType) {
            return Array.newInstance(a(((GenericArrayType) type).getGenericComponentType()), 0).getClass();
        } else {
            throw a(type, ParameterizedType.class, GenericArrayType.class);
        }
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:14:0x0012 */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r2v3 */
    /* JADX WARN: Type inference failed for: r2v7, types: [java.lang.reflect.Type] */
    /* JADX WARN: Type inference failed for: r2v10 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean a(java.lang.reflect.Type r4, java.lang.reflect.GenericArrayType r5) {
        /*
            java.lang.reflect.Type r1 = r5.getGenericComponentType()
            boolean r2 = r1 instanceof java.lang.reflect.ParameterizedType
            if (r2 == 0) goto L_0x0032
            boolean r2 = r4 instanceof java.lang.reflect.GenericArrayType
            if (r2 == 0) goto L_0x0020
            java.lang.reflect.GenericArrayType r4 = (java.lang.reflect.GenericArrayType) r4
            java.lang.reflect.Type r2 = r4.getGenericComponentType()
        L_0x0012:
            r0 = r1
            java.lang.reflect.ParameterizedType r0 = (java.lang.reflect.ParameterizedType) r0
            r4 = r0
            java.util.HashMap r1 = new java.util.HashMap
            r1.<init>()
            boolean r1 = a(r2, r4, r1)
        L_0x001f:
            return r1
        L_0x0020:
            boolean r2 = r4 instanceof java.lang.Class
            if (r2 == 0) goto L_0x0034
            java.lang.Class r4 = (java.lang.Class) r4
            r2 = r4
        L_0x0027:
            boolean r3 = r2.isArray()
            if (r3 == 0) goto L_0x0012
            java.lang.Class r2 = r2.getComponentType()
            goto L_0x0027
        L_0x0032:
            r1 = 1
            goto L_0x001f
        L_0x0034:
            r2 = r4
            goto L_0x0012
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.gson.reflect.TypeToken.a(java.lang.reflect.Type, java.lang.reflect.GenericArrayType):boolean");
    }

    private static boolean a(Type type, ParameterizedType parameterizedType, Map<String, Type> map) {
        boolean z;
        if (type == null) {
            return false;
        }
        if (parameterizedType.equals(type)) {
            return true;
        }
        Class<?> a2 = a(type);
        ParameterizedType parameterizedType2 = null;
        if (type instanceof ParameterizedType) {
            parameterizedType2 = (ParameterizedType) type;
        }
        if (parameterizedType2 != null) {
            Type[] actualTypeArguments = parameterizedType2.getActualTypeArguments();
            TypeVariable[] typeParameters = a2.getTypeParameters();
            for (int i = 0; i < actualTypeArguments.length; i++) {
                Type type2 = actualTypeArguments[i];
                TypeVariable typeVariable = typeParameters[i];
                Type type3 = type2;
                while (type3 instanceof TypeVariable) {
                    type3 = map.get(((TypeVariable) type3).getName());
                }
                map.put(typeVariable.getName(), type3);
            }
            if (parameterizedType2.getRawType().equals(parameterizedType.getRawType())) {
                Type[] actualTypeArguments2 = parameterizedType2.getActualTypeArguments();
                Type[] actualTypeArguments3 = parameterizedType.getActualTypeArguments();
                int i2 = 0;
                while (true) {
                    if (i2 >= actualTypeArguments2.length) {
                        z = true;
                        break;
                    }
                    Type type4 = actualTypeArguments2[i2];
                    Type type5 = actualTypeArguments3[i2];
                    if (!(type5.equals(type4) ? true : type4 instanceof TypeVariable ? type5.equals(map.get(((TypeVariable) type4).getName())) : false)) {
                        break;
                    }
                    i2++;
                }
            }
            z = false;
            if (z) {
                return true;
            }
        }
        for (Type a3 : a2.getGenericInterfaces()) {
            if (a(a3, parameterizedType, new HashMap(map))) {
                return true;
            }
        }
        return a(a2.getGenericSuperclass(), parameterizedType, new HashMap(map));
    }

    public static <T> TypeToken<T> get(Class<T> type) {
        return new a(type);
    }

    public static TypeToken<?> get(Type type) {
        return new a(type);
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof TypeToken)) {
            return false;
        }
        return this.b.equals(((TypeToken) o).b);
    }

    public Class<? super T> getRawType() {
        return this.a;
    }

    public Type getType() {
        return this.b;
    }

    public int hashCode() {
        return this.b.hashCode();
    }

    public boolean isAssignableFrom(TypeToken<?> token) {
        return isAssignableFrom(token.getType());
    }

    public boolean isAssignableFrom(Class<?> cls) {
        return isAssignableFrom((Type) cls);
    }

    public boolean isAssignableFrom(Type from) {
        if (from == null) {
            return false;
        }
        if (this.b.equals(from)) {
            return true;
        }
        if (this.b instanceof Class) {
            return this.a.isAssignableFrom(a(from));
        }
        if (this.b instanceof ParameterizedType) {
            return a(from, (ParameterizedType) this.b, new HashMap());
        }
        if (this.b instanceof GenericArrayType) {
            return this.a.isAssignableFrom(a(from)) && a(from, (GenericArrayType) this.b);
        }
        throw a(this.b, Class.class, ParameterizedType.class, GenericArrayType.class);
    }

    public String toString() {
        return this.b instanceof Class ? ((Class) this.b).getName() : this.b.toString();
    }
}
