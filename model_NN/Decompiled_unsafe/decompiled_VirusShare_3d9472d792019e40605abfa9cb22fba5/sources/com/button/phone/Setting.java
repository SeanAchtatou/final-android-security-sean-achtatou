package com.button.phone;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;
import android.widget.RemoteViews;

public class Setting extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    private static final int NOTIFICATION_ID = 2131099649;
    public static final String PREF_KEY_BOOTSTART = "pref_key_bootstart";
    public static final String PREF_KEY_NOTIFY = "pref_key_notify";
    private CheckBoxPreference mBootstartPref;
    private ListPreference mNotifyPref;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setPreferenceScreen(createPreferenceHierarchy());
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    private PreferenceScreen createPreferenceHierarchy() {
        PreferenceScreen root = getPreferenceManager().createPreferenceScreen(this);
        this.mNotifyPref = new ListPreference(this);
        this.mNotifyPref.setKey(PREF_KEY_NOTIFY);
        this.mNotifyPref.setDialogTitle((int) R.string.setting_title_show_notification);
        this.mNotifyPref.setEntries((int) R.array.status_bar_variant_names);
        this.mNotifyPref.setEntryValues((int) R.array.status_bar_variant_names_values);
        this.mNotifyPref.setDefaultValue("0");
        this.mNotifyPref.setTitle((int) R.string.setting_title_show_notification);
        this.mNotifyPref.setSummary((int) R.string.setting_summary_show_notification);
        root.addPreference(this.mNotifyPref);
        this.mBootstartPref = new CheckBoxPreference(this);
        this.mBootstartPref.setKey(PREF_KEY_BOOTSTART);
        this.mBootstartPref.setTitle((int) R.string.setting_title_auto_startup);
        this.mBootstartPref.setDefaultValue(true);
        this.mBootstartPref.setSummary((int) R.string.setting_summary_auto_startup);
        root.addPreference(this.mBootstartPref);
        return root;
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(PREF_KEY_NOTIFY)) {
            String b = sharedPreferences.getString(key, "0");
            if (b.equals("0")) {
                cancelNotification(this);
                this.mBootstartPref.setChecked(false);
                return;
            }
            showNotification(this, b);
        }
    }

    public static void showNotification(Context ctx, String mode) {
        cancelNotification(ctx);
        if (!mode.equals("0")) {
            NotificationManager notificationManager = (NotificationManager) ctx.getSystemService("notification");
            Notification notification = new Notification();
            notification.when = -1;
            if (mode.equals("1")) {
                notification.icon = R.drawable.icon;
            } else {
                notification.icon = R.drawable.ic_placeholder;
            }
            PendingIntent contentIntent = PendingIntent.getActivity(ctx, 0, new Intent(ctx, Switcher.class), 0);
            notification.flags = 2;
            notification.contentView = new RemoteViews(ctx.getPackageName(), (int) R.layout.notification1);
            notification.contentIntent = contentIntent;
            notificationManager.notify(R.string.app_name, notification);
        }
    }

    public static void cancelNotification(Context ctx) {
        ((NotificationManager) ctx.getSystemService("notification")).cancel(R.string.app_name);
    }
}
