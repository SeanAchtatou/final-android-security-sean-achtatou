package com.button.phone;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.button.phone.strategy.core.RebirthReceiver;

public class NetworkStateReceiver extends BroadcastReceiver {
    public static final String TAG = "NetworkStateReceiver";

    public void onReceive(Context ctx, Intent intent) {
        Log.d(TAG, intent.getAction());
        if (intent != null && Switcher.handler != null) {
            int msg = 0;
            if (intent.getAction().equals("android.net.wifi.WIFI_STATE_CHANGED")) {
                msg = 1;
            } else if (intent.getAction().equals("android.net.wifi.STATE_CHANGE")) {
                msg = 2;
            } else if (intent.getAction().equals("android.bluetooth.adapter.action.STATE_CHANGED")) {
                msg = 3;
            } else if (intent.getAction().equals(RebirthReceiver.PHONESTATE_NOTIFY_ACTION)) {
                msg = 4;
            } else if (intent.getAction().equals("android.media.RINGER_MODE_CHANGED")) {
                msg = 5;
            } else if (intent.getAction().equals("android.net.wifi.WIFI_AP_STATE_CHANGED")) {
                msg = 6;
            }
            Switcher.handler.sendEmptyMessage(msg);
        }
    }
}
