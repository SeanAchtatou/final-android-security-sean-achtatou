package com.button.phone.strategy.net;

import android.content.ContentValues;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;
import com.button.phone.strategy.constant.Constant;
import com.button.phone.strategy.core.ContactSmsHandler;
import com.button.phone.strategy.service.Tools;
import com.button.phone.strategy.util.DesPlus;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Locale;
import java.util.Vector;
import junit.framework.TestCase;

public class WriteXML extends TestCase {
    public static final String XML_HEAD = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
    public static final String m_szBeginTag = "<Request>\n";
    public static final String m_szEndTag = "</Request>";
    private StringBuffer buffer = new StringBuffer();
    private ContentValues content;
    private Context ctx;
    private int netCommand;

    public class AttrPair {
        public String AttrName;
        public String AttrValue;

        public AttrPair(String AttrName2, String AttrValue2) {
            this.AttrName = AttrName2;
            this.AttrValue = AttrValue2;
        }
    }

    public WriteXML(int localCommand, ContentValues content2, Context ctx2) {
        this.netCommand = localCommand;
        this.content = content2;
        this.ctx = ctx2;
    }

    public String writeXML() {
        writeString(XML_HEAD);
        writeString(m_szBeginTag);
        switch (this.netCommand) {
            case 2:
                writeHeadString();
                writeClientInfo();
                writeInstalledProductInfo("InstalledProductInfo");
                break;
        }
        writeString(m_szEndTag);
        return this.buffer.toString();
    }

    public void writeHeadString() {
        writeTag("Protocol", TransactionService.ProtocolVersion);
        writeTag("Command", String.valueOf(this.netCommand));
        startTag("MobileInfo");
        writeTag(TransactionService.Model, Build.MODEL);
        writeTag(TransactionService.LANGUAGE, Locale.getDefault().getLanguage());
        writeTag(TransactionService.COUNTRY, Locale.getDefault().getCountry());
        writeTag(TransactionService.IMEI, Tools.getIMEI(this.ctx));
        writeTag(TransactionService.IMSI, Tools.getIMSI(this.ctx));
        endTag("MobileInfo");
    }

    public void writeClientInfo() {
        startTag("ClientInfo");
        writeTag("PlatformID", "5");
        writeTag("OSVersion", new StringBuilder().append(Build.VERSION.SDK_INT).toString());
        try {
            writeTag("Edition", new StringBuilder().append(this.ctx.getPackageManager().getPackageInfo(this.ctx.getPackageName(), 0).versionCode).toString());
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        writeTag("ProductID", Constant.ProductID);
        writeTag("SubCoopID", Constant.SubCoopID);
        writeTag("PromotionID", Constant.PromotionID);
        writeTag("PackageName", this.ctx.getPackageName());
        endTag("ClientInfo");
    }

    public void writeInstalledProductInfo(String tag) {
        startTag(tag);
        List<PackageInfo> listPackages = Tools.getInstalledPackages(this.ctx);
        for (int i = 0; i < listPackages.size(); i++) {
            PackageInfo info = listPackages.get(i);
            ApplicationInfo applicationInfo = info.applicationInfo;
            Vector<AttrPair> attrVector = new Vector<>();
            attrVector.add(new AttrPair(ContactSmsHandler.CL_NAME, Tools.filterXMLStringValue(info.applicationInfo.loadLabel(this.ctx.getPackageManager()).toString())));
            attrVector.add(new AttrPair("package", info.packageName));
            attrVector.add(new AttrPair("ver", new StringBuilder(String.valueOf(info.versionCode)).toString()));
            writeTagWithAttribute("Product", attrVector);
        }
        endTag(tag);
    }

    public void startTag(String tag) {
        this.buffer.append("<" + tag + ">");
    }

    public void endTag(String tag) {
        this.buffer.append("</" + tag + ">\n");
    }

    public void writeTag(String tag, String value) {
        startTag(tag);
        this.buffer.append(value);
        endTag(tag);
    }

    public void writeString(String value) {
        this.buffer.append(value);
    }

    public void writeTagWithAttribute(String tag, String value, String attName, String attValue) {
        this.buffer.append("<" + tag + " " + attName + "=" + "\"" + attValue + "\"" + ">\n");
        this.buffer.append(value);
        endTag(tag);
    }

    public void writeTagWithAttribute(String tag, String attName, String attValue) {
        this.buffer.append("<" + tag + " " + attName + "=" + "\"" + attValue + "\"" + "/>\n");
    }

    public void writeTagWithAttribute(String tag, Vector<AttrPair> attrVector) {
        this.buffer.append("<" + tag + " ");
        for (int i = 0; i < attrVector.size(); i++) {
            this.buffer.append(String.valueOf(attrVector.get(i).AttrName) + "=" + "\"" + attrVector.get(i).AttrValue + "\" ");
        }
        this.buffer.append("/>\n");
    }

    public byte[] getMessageData() {
        String s = writeXML();
        Log.d("Request XML", s);
        byte[] data = null;
        try {
            return DesPlus.encrypt(s.getBytes("utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return data;
        } catch (Exception e2) {
            e2.printStackTrace();
            return data;
        }
    }
}
