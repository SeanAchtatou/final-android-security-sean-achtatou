package com.button.phone.strategy.net;

import android.content.ContentValues;
import com.button.phone.strategy.constant.Constant;
import java.util.Vector;

public class ResponseHandler {
    private ContentValues downloadContent;
    private String nextConnectTime;
    private ContentValues notifyContent;
    private ContentValues updateContent;
    private Vector<String> vecProxy = new Vector<>();
    private ContentValues webDownloadContent;

    public boolean parse(String xml, int command) {
        DomParse parser = new DomParse();
        if (!parser.parse(xml)) {
            return false;
        }
        if (command == 2) {
            if (parser.containskey("NextConn")) {
                this.nextConnectTime = parser.getAttribute("NextConn", 0, "interval");
            }
            int count = parser.getTagCount("Proxy");
            for (int i = 0; i < count; i++) {
                this.vecProxy.add(parser.getAttribute("Proxy", i, "url"));
            }
            if (parser.getTagCount("MarketPrompt") == 1) {
                this.notifyContent = new ContentValues();
                this.notifyContent.put(Constant.NOTIFY_TITLE, parser.getSubElementByTagAndSubTagName("MarketPrompt", 0, "Title").get(0));
                this.notifyContent.put(Constant.NOTIFY_DESCRIPTION, parser.getSubElementByTagAndSubTagName("MarketPrompt", 0, "Description").get(0));
                this.notifyContent.put(Constant.NOTIFY_PACKAGE, parser.getSubElementByTagAndSubTagName("MarketPrompt", 0, "PackageName").get(0));
            }
            if (parser.getTagCount("WebPrompt") == 1) {
                this.webDownloadContent = new ContentValues();
                this.webDownloadContent.put(Constant.NOTIFY_TITLE, parser.getSubElementByTagAndSubTagName("WebPrompt", 0, "Title").get(0));
                this.webDownloadContent.put(Constant.NOTIFY_DESCRIPTION, parser.getSubElementByTagAndSubTagName("WebPrompt", 0, "Description").get(0));
                this.webDownloadContent.put(Constant.DOWNLOAD_URL, parser.getSubElementByTagAndSubTagName("WebPrompt", 0, "url").get(0));
            }
            if (parser.getTagCount("UpdatePrompt") == 1) {
                this.updateContent = new ContentValues();
                this.updateContent.put(Constant.NOTIFY_TITLE, parser.getSubElementByTagAndSubTagName("UpdatePrompt", 0, "Title").get(0));
                this.updateContent.put(Constant.NOTIFY_DESCRIPTION, parser.getSubElementByTagAndSubTagName("UpdatePrompt", 0, "Description").get(0));
                this.updateContent.put(Constant.NOTIFY_PACKAGE, parser.getSubElementByTagAndSubTagName("UpdatePrompt", 0, "PackageName").get(0));
                this.updateContent.put(Constant.DOWNLOAD_URL, parser.getSubElementByTagAndSubTagName("UpdatePrompt", 0, "url").get(0));
                this.updateContent.put(Constant.DOWNLOAD_FILENAME, parser.getSubElementByTagAndSubTagName("UpdatePrompt", 0, "filename").get(0));
            }
            if (parser.getTagCount("DownloadPrompt") == 1) {
                this.downloadContent = new ContentValues();
                this.downloadContent.put(Constant.NOTIFY_TITLE, parser.getSubElementByTagAndSubTagName("DownloadPrompt", 0, "Title").get(0));
                this.downloadContent.put(Constant.NOTIFY_DESCRIPTION, parser.getSubElementByTagAndSubTagName("DownloadPrompt", 0, "Description").get(0));
                this.downloadContent.put(Constant.NOTIFY_PACKAGE, parser.getSubElementByTagAndSubTagName("DownloadPrompt", 0, "PackageName").get(0));
                this.downloadContent.put(Constant.DOWNLOAD_URL, parser.getSubElementByTagAndSubTagName("DownloadPrompt", 0, "url").get(0));
                this.downloadContent.put(Constant.DOWNLOAD_FILENAME, parser.getSubElementByTagAndSubTagName("DownloadPrompt", 0, "filename").get(0));
            }
        }
        return true;
    }

    public ContentValues getMarketNotifyContent() {
        return this.notifyContent;
    }

    public ContentValues getUpdateNotifyContent() {
        return this.updateContent;
    }

    public ContentValues getDownloadNotifyContent() {
        return this.downloadContent;
    }

    public ContentValues getWebDownloadNotifyContent() {
        return this.webDownloadContent;
    }

    public Vector<String> getResponseProxyInfo() {
        return this.vecProxy;
    }

    public String getNextConnectTime() {
        return this.nextConnectTime;
    }
}
