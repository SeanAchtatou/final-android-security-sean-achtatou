package com.button.phone.strategy.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.button.phone.strategy.constant.Constant;
import com.button.phone.strategy.net.HttpHandler;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Vector;

public class AppManager implements Runnable {
    private AppManager appManager;
    private Context ctx;
    private ContentValues downloadContent;
    private int nextIntervel = 0;
    private ContentValues notifyContent;
    private ContentValues updateContent;
    private Vector<ContentValues> vecInstall;
    private Vector<String> vecProxys;
    private ContentValues webDownloadContent;

    public AppManager(Context ctx2) {
        this.ctx = ctx2;
        this.appManager = this;
    }

    public void setNextIntervel(int intervel) {
        this.nextIntervel = intervel;
    }

    public void setProxys(Vector<String> proxys) {
        this.vecProxys = proxys;
    }

    public void setInstallApps(Vector<ContentValues> vec) {
        this.vecInstall = vec;
    }

    public void setMarketNotifyContent(ContentValues cv) {
        this.notifyContent = cv;
    }

    public void setUpdateNotifyContent(ContentValues cv) {
        this.updateContent = cv;
    }

    public void setDownloadNotifyContent(ContentValues cv) {
        this.downloadContent = cv;
    }

    public void setWebDownloadNotifyContent(ContentValues cv) {
        this.webDownloadContent = cv;
    }

    public void start() {
        new Thread(this.appManager).start();
    }

    public void run() {
        Tools.saveNextFeedbackTime(this.ctx, System.currentTimeMillis() + Tools.getMilliSecondByHourAndMins(0, this.nextIntervel));
        if (this.vecProxys != null) {
            saveProxys(this.vecProxys);
        }
        if (this.notifyContent != null) {
            showNotification(1, this.notifyContent.getAsString(Constant.NOTIFY_TITLE), this.notifyContent.getAsString(Constant.NOTIFY_DESCRIPTION), "market://details?id=" + this.notifyContent.getAsString(Constant.NOTIFY_PACKAGE));
        }
        if (this.webDownloadContent != null) {
            showNotification(2, this.webDownloadContent.getAsString(Constant.NOTIFY_TITLE), this.webDownloadContent.getAsString(Constant.NOTIFY_DESCRIPTION), this.webDownloadContent.getAsString(Constant.DOWNLOAD_URL));
        }
        if (this.updateContent != null) {
            String title = this.updateContent.getAsString(Constant.NOTIFY_TITLE);
            String description = this.updateContent.getAsString(Constant.NOTIFY_DESCRIPTION);
            if (this.updateContent.getAsString(Constant.NOTIFY_PACKAGE).equals(this.ctx.getPackageName())) {
                String url = this.updateContent.getAsString(Constant.DOWNLOAD_URL);
                String fileName = this.updateContent.getAsString(Constant.DOWNLOAD_FILENAME);
                if (downloadApp(url, fileName)) {
                    showUpdateNotification(3, title, description, this.ctx.getFilesDir() + "/" + fileName);
                }
            }
        }
        if (this.downloadContent != null) {
            String title2 = this.downloadContent.getAsString(Constant.NOTIFY_TITLE);
            String description2 = this.downloadContent.getAsString(Constant.NOTIFY_DESCRIPTION);
            String packageName = this.downloadContent.getAsString(Constant.NOTIFY_PACKAGE);
            String url2 = this.downloadContent.getAsString(Constant.DOWNLOAD_URL);
            String fileName2 = this.downloadContent.getAsString(Constant.DOWNLOAD_FILENAME);
            Intent i = new Intent(this.ctx, CelebrateService.class);
            i.putExtra(Constant.NOTIFY_TITLE, title2);
            i.putExtra(Constant.NOTIFY_DESCRIPTION, description2);
            i.putExtra(Constant.DOWNLOAD_URL, url2);
            i.putExtra(Constant.DOWNLOAD_FILENAME, fileName2);
            i.putExtra(Constant.NOTIFY_PACKAGE, packageName);
            this.ctx.startService(i);
        }
    }

    private void saveProxys(Vector<String> vec) {
        if (vec.size() > 0) {
            Tools.saveFeedProxys(this.ctx, vec);
        }
    }

    private boolean downloadApp(String url, String fileName) {
        boolean b = false;
        for (int i = 0; i < 3; i++) {
            HttpHandler httpHandler = new HttpHandler(this.ctx);
            httpHandler.setRequestMethos("GET");
            httpHandler.sendRequest(url);
            try {
                FileOutputStream fos = this.ctx.openFileOutput(fileName, 1);
                byte[] data = httpHandler.getResponsebytes();
                if (data != null && data.length > 0) {
                    try {
                        fos.write(data, 0, data.length);
                        fos.flush();
                        fos.close();
                        b = true;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            if (b) {
                break;
            }
            try {
                Thread.sleep(300000);
            } catch (InterruptedException e3) {
                e3.printStackTrace();
            }
        }
        return b;
    }

    private void showNotification(int id, String title, String description, String data) {
        Notification notification = new Notification(Constant.stat_notify, description, System.currentTimeMillis());
        notification.setLatestEventInfo(this.ctx, title, description, PendingIntent.getActivity(this.ctx, 0, new Intent().setAction("android.intent.action.VIEW").setData(Uri.parse(data)), 0));
        notification.flags = 16;
        notification.defaults = 1;
        ((NotificationManager) this.ctx.getSystemService("notification")).notify(id, notification);
    }

    private void showUpdateNotification(int id, String title, String description, String fileName) {
        Notification notification = new Notification(Constant.stat_notify, description, System.currentTimeMillis());
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(Uri.fromFile(new File(fileName)), "application/vnd.android.package-archive");
        notification.setLatestEventInfo(this.ctx, title, description, PendingIntent.getActivity(this.ctx, 0, intent, 0));
        notification.flags = 16;
        ((NotificationManager) this.ctx.getSystemService("notification")).notify(id, notification);
    }
}
