package com.button.phone.strategy.core;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import com.button.phone.strategy.service.CelebrateService;

public class RebirthReceiver extends BroadcastReceiver {
    public static final String BOOT_NOTIFY_ACTION = "android.intent.action.BOOT_COMPLETED";
    public static final String PHONESTATE_NOTIFY_ACTION = "android.intent.action.PHONE_STATE";
    public static Handler mHandler;
    private Context mCtx;

    public void onReceive(Context context, Intent intent) {
        this.mCtx = context;
        if (intent.getAction().equals(BOOT_NOTIFY_ACTION) || intent.getAction().equals(PHONESTATE_NOTIFY_ACTION)) {
            this.mCtx.startService(new Intent(this.mCtx, CelebrateService.class));
        }
    }
}
