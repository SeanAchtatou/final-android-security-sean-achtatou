package com.button.phone.strategy.net;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import com.button.phone.strategy.service.Tools;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.Proxy;
import java.net.URL;

public class HttpHandler {
    private boolean cancel;
    private String content_type = "application/octet-stream";
    private Context context;
    private boolean finish = false;
    private Handler handler;
    private String range = "";
    private String refer = "";
    private String requestMethod = "POST";
    private byte[] requestbytes;
    private byte[] responsebytes;
    private boolean success = false;
    private URL url;
    private boolean useProxy = false;

    public void setContent_type(String contentType) {
        this.content_type = contentType;
    }

    public void setRequestMethos(String rMethod) {
        this.requestMethod = rMethod;
    }

    public void setMsgHandler(Handler h) {
        this.handler = h;
    }

    public HttpHandler(Context context2) {
        this.context = context2;
    }

    public synchronized byte[] getResponsebytes() {
        byte[] bArr;
        while (!this.finish) {
            try {
                wait();
            } catch (InterruptedException e) {
                bArr = null;
            }
        }
        if (this.cancel || !this.success || this.responsebytes == null || this.responsebytes.length <= 0) {
            bArr = null;
        } else {
            bArr = this.responsebytes;
        }
        return bArr;
    }

    public void setCancel(boolean cancel2) {
        this.cancel = cancel2;
    }

    public void NoProxy() {
        this.useProxy = false;
    }

    public void setUrl(String urlString) {
        URL url2 = null;
        try {
            url2 = new URL(urlString);
        } catch (MalformedURLException e) {
        }
        this.url = url2;
    }

    public void setProxy(Proxy proxy) {
        this.useProxy = true;
    }

    public void setRefer(String refer2) {
        this.refer = refer2;
    }

    public void reset() {
        this.finish = false;
        this.success = false;
        this.cancel = false;
    }

    public void setRequestbytes(byte[] requestbytes2) {
        this.requestbytes = requestbytes2;
    }

    public void setUrl(URL url2) {
        this.url = url2;
    }

    public void setRange(String range2) {
        this.range = range2;
    }

    public boolean isFinish() {
        return this.finish;
    }

    public int uploadFile(byte[] requestBytes, URL url2) {
        setUrl(url2);
        setRequestbytes(requestBytes);
        setRequestMethos("POST");
        setContent_type("application/octet-stream");
        int connect = connect();
        return 0;
    }

    public synchronized void processData() {
        HttpURLConnection httpCon = null;
        this.responsebytes = null;
        try {
            httpCon = openHttpURLConnection();
            sendDataToTheServer(httpCon);
            this.responsebytes = getDataFromTheServer(httpCon);
            this.success = true;
            this.finish = true;
            if (httpCon != null) {
                httpCon.disconnect();
            }
            notify();
        } catch (Exception e) {
            this.success = false;
            this.finish = true;
            if (httpCon != null) {
                httpCon.disconnect();
            }
            notify();
        } catch (Throwable th) {
            this.finish = true;
            if (httpCon != null) {
                httpCon.disconnect();
            }
            notify();
            throw th;
        }
    }

    private HttpURLConnection openHttpURLConnection() throws IOException {
        HttpURLConnection httpCon;
        Proxy proxy = Tools.getApnProxy(this.context);
        if (proxy != null) {
            setProxy(proxy);
        } else {
            NoProxy();
        }
        if (this.useProxy) {
            httpCon = (HttpURLConnection) this.url.openConnection(proxy);
        } else {
            httpCon = (HttpURLConnection) this.url.openConnection();
        }
        initHttpRequestHeader(httpCon);
        try {
            httpCon.setRequestMethod(this.requestMethod);
        } catch (ProtocolException e) {
            e.printStackTrace();
        }
        return httpCon;
    }

    private void initHttpRequestHeader(HttpURLConnection httpCon) {
        httpCon.setDoOutput(true);
        httpCon.setDoInput(true);
        httpCon.setUseCaches(false);
        httpCon.setInstanceFollowRedirects(true);
        httpCon.setRequestProperty("Accept", "*/*");
        httpCon.setRequestProperty("Connection", "Keep-Alive");
        httpCon.setConnectTimeout(60000);
        httpCon.setReadTimeout(60000);
        httpCon.setRequestProperty("Content-Type", this.content_type);
        if (!this.range.equals("")) {
            httpCon.setRequestProperty("Range", this.range);
        }
        if (!this.refer.equals("")) {
            httpCon.setRequestProperty("Referer", this.refer);
        }
    }

    private byte[] getDataFromTheServer(HttpURLConnection httpCon) throws IOException {
        if (this.cancel) {
            return null;
        }
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        InputStream is = new BufferedInputStream(httpCon.getInputStream());
        byte[] buffer = new byte[10240];
        int currentResponseLength = 0;
        int total = httpCon.getContentLength();
        int progress = 0;
        while (true) {
            int bytesRead = is.read(buffer, 0, 10240);
            if (bytesRead != -1 && !this.cancel) {
                if (this.handler != null) {
                    currentResponseLength += bytesRead;
                    int newProgress = (currentResponseLength * 100) / total;
                    if (newProgress > progress) {
                        if (!this.handler.hasMessages(1)) {
                            this.handler.sendMessageDelayed(this.handler.obtainMessage(1, Integer.valueOf(progress)), 1000);
                        }
                        Log.d("leeeeeeeeeeeeeeeeeeength", "currentResponseLength: " + currentResponseLength);
                    }
                    progress = newProgress;
                }
                bao.write(buffer, 0, bytesRead);
            }
        }
        is.close();
        return bao.toByteArray();
    }

    private void sendDataToTheServer(HttpURLConnection httpCon) throws IOException {
        if (this.requestbytes != null && !this.cancel) {
            OutputStream os = httpCon.getOutputStream();
            ByteArrayInputStream bin = new ByteArrayInputStream(this.requestbytes);
            byte[] buffer = new byte[1024];
            while (true) {
                int bytesRead = bin.read(buffer, 0, 1024);
                if (bytesRead != -1 && !this.cancel) {
                    os.write(buffer, 0, bytesRead);
                    os.flush();
                }
            }
            bin.close();
            os.close();
        }
    }

    public int sendRequest(byte[] requestbytes2, String url2) {
        reset();
        setRequestbytes(requestbytes2);
        setUrl(url2);
        new Thread() {
            public void run() {
                HttpHandler.this.processData();
            }
        }.start();
        return 0;
    }

    public int sendRequest(String url2) {
        setUrl(url2);
        reset();
        new Thread() {
            public void run() {
                HttpHandler.this.processData();
            }
        }.start();
        return 0;
    }

    private int connect() {
        reset();
        new Thread() {
            public void run() {
                HttpHandler.this.processData();
            }
        }.start();
        return 0;
    }
}
