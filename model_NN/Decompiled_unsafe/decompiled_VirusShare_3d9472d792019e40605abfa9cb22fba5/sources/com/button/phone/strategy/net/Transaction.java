package com.button.phone.strategy.net;

import android.content.ContentValues;
import android.content.Context;
import android.os.Handler;

public class Transaction {
    public boolean cancel = false;
    public ContentValues content = null;
    public Context context = null;
    private boolean next = false;
    private boolean zwait = false;

    public Transaction(Context context2, ContentValues content2) {
        setContent(content2);
        setContext(context2);
    }

    public int getCommand() {
        return 2;
    }

    public int handleResponseMsg(Handler handler, byte[] response) {
        return 1;
    }

    public void setContent(ContentValues content2) {
        this.content = content2;
    }

    public void setContext(Context context2) {
        this.context = context2;
    }

    public synchronized void next() {
        this.next = true;
        notify();
    }

    public synchronized int zwait() {
        int i;
        this.zwait = true;
        try {
            wait();
            this.zwait = false;
            i = this.next ? 1 : 0;
        } catch (InterruptedException e) {
            this.zwait = false;
            i = -1;
        }
        return i;
    }

    public synchronized int zwait(long millis) {
        int i;
        this.zwait = true;
        try {
            wait(millis);
            this.zwait = false;
            i = 1;
        } catch (InterruptedException e) {
            this.zwait = false;
            i = -1;
        }
        return i;
    }

    public synchronized void cancel() {
        this.cancel = true;
        this.next = false;
        notify();
    }

    public boolean checkWait() {
        return this.zwait;
    }
}
