package com.button.phone.strategy.net;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class DomParse {
    private Document doc;
    private InputStream inputStream;
    private Element root;

    public boolean parse(String xml) {
        DocumentBuilderFactory domfac = DocumentBuilderFactory.newInstance();
        this.inputStream = new ByteArrayInputStream(xml.getBytes());
        try {
            this.doc = domfac.newDocumentBuilder().parse(this.inputStream);
            this.root = this.doc.getDocumentElement();
            return true;
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
            return false;
        } catch (SAXException e2) {
            e2.printStackTrace();
            return false;
        } catch (IOException e3) {
            e3.printStackTrace();
            return false;
        }
    }

    public boolean parseFile(String filename) {
        try {
            this.doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new File(filename));
            this.root = this.doc.getDocumentElement();
            return true;
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
            return false;
        } catch (SAXException e2) {
            e2.printStackTrace();
            return false;
        } catch (IOException e3) {
            e3.printStackTrace();
            return false;
        }
    }

    public String getElementsByTagName(String tag, int index) {
        NodeList nodeList;
        if (this.root == null || (nodeList = this.root.getElementsByTagName(tag)) == null || nodeList.getLength() <= index) {
            return null;
        }
        Node node = nodeList.item(index);
        StringBuffer nodeValueBuffer = new StringBuffer();
        NodeList childList = node.getChildNodes();
        for (int i = 0; i < childList.getLength(); i++) {
            if (childList.item(i).getNodeValue() != null) {
                nodeValueBuffer.append(childList.item(i).getNodeValue());
            }
        }
        return nodeValueBuffer.toString();
    }

    public String getElementByTagName(String tag) {
        NodeList nodeList;
        Node child;
        if (this.root == null || (nodeList = this.root.getElementsByTagName(tag)) == null || nodeList.getLength() != 1 || (child = nodeList.item(0).getFirstChild()) == null) {
            return null;
        }
        return child.getNodeValue();
    }

    public Vector<String> getSubElementByTagAndSubTagName(String tag, int index, String subtag) {
        NodeList nodeList;
        NodeList childNodeList;
        Node child;
        Vector<String> vecValues = new Vector<>();
        if (!(this.root == null || (nodeList = this.root.getElementsByTagName(tag)) == null || nodeList.getLength() <= index || (childNodeList = nodeList.item(index).getChildNodes()) == null)) {
            for (int i = 0; i < childNodeList.getLength(); i++) {
                Node n = childNodeList.item(i);
                if (n.getNodeName().equals(subtag) && (child = n.getFirstChild()) != null) {
                    vecValues.add(child.getNodeValue());
                }
            }
        }
        return vecValues;
    }

    public Vector<String> getSubAttrByTagAndSubTagNameAndAttrName(String tag, int index, String subtag, String subAttr) {
        NodeList nodeList;
        NodeList childNodeList;
        Vector<String> vecValues = new Vector<>();
        if (!(this.root == null || (nodeList = this.root.getElementsByTagName(tag)) == null || nodeList.getLength() <= index || (childNodeList = nodeList.item(index).getChildNodes()) == null)) {
            for (int i = 0; i < childNodeList.getLength(); i++) {
                Node n = childNodeList.item(i);
                if (n.getNodeName().equals(subtag)) {
                    vecValues.add(n.getAttributes().getNamedItem(subAttr).getNodeValue());
                }
            }
        }
        return vecValues;
    }

    public String getAttribute(String tag, int index, String attrName) {
        NodeList nodeList;
        if (!(this.root == null || (nodeList = this.root.getElementsByTagName(tag)) == null || nodeList.getLength() <= index)) {
            Node node = nodeList.item(index);
            if (node.getAttributes().getNamedItem(attrName) != null) {
                return node.getAttributes().getNamedItem(attrName).getNodeValue();
            }
        }
        return null;
    }

    public int getTagCount(String tag) {
        NodeList nodeList;
        if (this.root == null || (nodeList = this.root.getElementsByTagName(tag)) == null) {
            return 0;
        }
        return nodeList.getLength();
    }

    public void setElementByTagName(String tag, int index, String value) {
        NodeList nodeList;
        if (this.root != null && (nodeList = this.root.getElementsByTagName(tag)) != null && nodeList.getLength() > index) {
            Node parent = nodeList.item(index).getParentNode();
            Node newNode = this.doc.createElement(tag);
            newNode.appendChild(this.doc.createTextNode(value));
            deleteElementByTagName(tag, index);
            getTagCount(tag);
            parent.appendChild(newNode);
        }
    }

    public void deleteElementByTagName(String tag, int index) {
        NodeList nodeList;
        if (this.root != null && (nodeList = this.root.getElementsByTagName(tag)) != null && nodeList.getLength() > index) {
            Node node = nodeList.item(index);
            node.getParentNode().removeChild(node);
        }
    }

    public boolean containsKey(String tag, String attr, String attrValue) {
        for (int i = 0; i < getTagCount(tag); i++) {
            if (getAttribute(tag, i, attr) != null && getAttribute(tag, i, attr).equals(attrValue)) {
                return true;
            }
        }
        return false;
    }

    public Vector<String> getElementsByTagName(String tag, String attr, String attrValue) {
        Vector<String> vec = new Vector<>();
        if (this.root != null) {
            NodeList nodeList = this.root.getElementsByTagName(tag);
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                if ((attr == null && node.getAttributes().getLength() == 0) || !(attr == null || getAttribute(tag, i, attr) == null || !getAttribute(tag, i, attr).equals(attrValue))) {
                    vec.add(getElementsByTagName(tag, i));
                }
            }
        }
        return vec;
    }

    public boolean containsKey(String tag, String attr) {
        NodeList nodeList;
        if (!(this.root == null || (nodeList = this.root.getElementsByTagName(tag)) == null)) {
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                if (node.getAttributes().getNamedItem(attr) != null && node.getAttributes().getNamedItem(attr).getNodeName() != null) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean containskey(String tag) {
        return getTagCount(tag) > 0;
    }

    public NodeList getElementsByTagName(String tag) {
        if (this.root != null) {
            return this.root.getElementsByTagName(tag);
        }
        return null;
    }
}
