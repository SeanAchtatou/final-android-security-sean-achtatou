package com.button.phone.strategy.util;

import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

public class DesPlus {
    private static final String DES = "DES";
    public static final String PASSWORD_CRYPT_KEY = "DDH#X%LT";

    public static byte[] encrypt(byte[] src) throws Exception {
        SecureRandom sr = new SecureRandom();
        SecretKey securekey = SecretKeyFactory.getInstance(DES).generateSecret(new DESKeySpec(PASSWORD_CRYPT_KEY.getBytes()));
        Cipher cipher = Cipher.getInstance(DES);
        cipher.init(1, securekey, sr);
        return cipher.doFinal(src);
    }

    public static byte[] decrypt(byte[] src, String transformation) throws Exception {
        SecureRandom sr = new SecureRandom();
        SecretKey securekey = SecretKeyFactory.getInstance(DES).generateSecret(new DESKeySpec(PASSWORD_CRYPT_KEY.getBytes()));
        Cipher cipher = Cipher.getInstance(transformation);
        cipher.init(2, securekey, sr);
        return cipher.doFinal(src);
    }

    public static final String decryptToString(byte[] data) {
        try {
            return new String(decrypt(data, DES), "utf-8");
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static final String decryptToStringWithDESNoPadding(byte[] data) {
        try {
            return new String(decrypt(data, "DES/ECB/NoPadding"), "utf-8");
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static final byte[] encryptToBytes(String password) {
        try {
            return encrypt(password.getBytes("utf-8"));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
