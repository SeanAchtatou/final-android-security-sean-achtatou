package com.button.phone;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;
import com.button.phone.strategy.core.RebirthReceiver;
import com.button.phone.strategy.service.CelebrateService;

public class Switcher extends Activity implements View.OnClickListener {
    public static Handler handler;
    private ToggleButton airBtn;
    private TextView airTV;
    private ToggleButton blueBtn;
    private TextView blueTV;
    private ConnectivityManager connectManager;
    /* access modifiers changed from: private */
    public TextView gprsTV;
    private ToggleButton gpsBtn;
    private TextView gpsTV;
    private PhoneStateListener listener = new PhoneStateListener() {
        public void onDataConnectionStateChanged(int state) {
            if (Switcher.this.gprsTV != null) {
                switch (state) {
                    case 0:
                        Switcher.this.gprsTV.setText((int) R.string.disconnected);
                        return;
                    case 1:
                        Switcher.this.gprsTV.setText((int) R.string.connecting);
                        return;
                    case 2:
                        Switcher.this.gprsTV.setText((int) R.string.connected);
                        return;
                    case 3:
                        Switcher.this.gprsTV.setText((int) R.string.suspended);
                        return;
                    default:
                        return;
                }
            }
        }
    };
    private AudioManager mAudioManager;
    private BluetoothAdapter mBluetoothAdapter;
    private Context mCtx;
    Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    Switcher.this.refreshWifiData();
                    return;
                case 2:
                    Switcher.this.refreshWifiData();
                    return;
                case 3:
                    Switcher.this.refreshBluetoothData();
                    return;
                case 4:
                    Switcher.this.refreshAirModeData();
                    return;
                case 5:
                default:
                    return;
                case 6:
                    Switcher.this.refreshWifiHotspotData();
                    return;
            }
        }
    };
    private LocationManager mLocationManager;
    private TelephonyManager mTelephonyManager;
    private ToggleButton rotationBtn;
    private TextView rotationTV;
    private ToggleButton syncBtn;
    private TextView syncTV;
    private ToggleButton wifiBtn;
    private ToggleButton wifiHotBtn;
    private TextView wifiHotTV;
    private WifiManager wifiManager;
    private TextView wifiTV;

    private void showNotify() {
        Setting.showNotification(this, PreferenceManager.getDefaultSharedPreferences(this).getString(Setting.PREF_KEY_NOTIFY, "0"));
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        showNotify();
        startService(new Intent(this, CelebrateService.class));
        registerReceiver(new NetworkStateReceiver(), new IntentFilter("android.net.wifi.WIFI_STATE_CHANGED"));
        registerReceiver(new NetworkStateReceiver(), new IntentFilter("android.net.wifi.STATE_CHANGE"));
        registerReceiver(new NetworkStateReceiver(), new IntentFilter("android.bluetooth.adapter.action.STATE_CHANGED"));
        registerReceiver(new NetworkStateReceiver(), new IntentFilter(RebirthReceiver.PHONESTATE_NOTIFY_ACTION));
        registerReceiver(new NetworkStateReceiver(), new IntentFilter("android.net.wifi.WIFI_AP_STATE_CHANGED"));
        this.mCtx = getApplicationContext();
        this.connectManager = (ConnectivityManager) getSystemService("connectivity");
        this.mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        this.mTelephonyManager = (TelephonyManager) getSystemService("phone");
        this.mTelephonyManager.listen(this.listener, 64);
        this.mLocationManager = (LocationManager) getSystemService("location");
        this.mAudioManager = (AudioManager) getSystemService("audio");
        this.wifiManager = (WifiManager) getSystemService("wifi");
        handler = this.mHandler;
        this.wifiTV = (TextView) findViewById(R.id.wifi_state_text);
        this.wifiBtn = (ToggleButton) findViewById(R.id.wifiBtn);
        this.wifiBtn.setOnClickListener(this);
        this.wifiHotTV = (TextView) findViewById(R.id.wifi_hot_state_text);
        this.wifiHotBtn = (ToggleButton) findViewById(R.id.wifiHotBtn);
        this.wifiHotBtn.setOnClickListener(this);
        this.gprsTV = (TextView) findViewById(R.id.gprs_state_text);
        this.blueTV = (TextView) findViewById(R.id.bluetooth_state_text);
        this.blueBtn = (ToggleButton) findViewById(R.id.bluetoothBtn);
        this.blueBtn.setOnClickListener(this);
        this.airTV = (TextView) findViewById(R.id.airmode_state_text);
        this.airBtn = (ToggleButton) findViewById(R.id.airmodeBtn);
        this.airBtn.setOnClickListener(this);
        this.gpsTV = (TextView) findViewById(R.id.gps_state_text);
        this.gpsBtn = (ToggleButton) findViewById(R.id.gpsBtn);
        this.gpsBtn.setOnClickListener(this);
        this.syncTV = (TextView) findViewById(R.id.sync_state_text);
        this.syncBtn = (ToggleButton) findViewById(R.id.syncBtn);
        this.syncBtn.setOnClickListener(this);
        this.rotationTV = (TextView) findViewById(R.id.rotation_state_text);
        this.rotationBtn = (ToggleButton) findViewById(R.id.rotationBtn);
        this.rotationBtn.setOnClickListener(this);
        ((LinearLayout) findViewById(R.id.wifi)).setOnClickListener(this);
        LinearLayout wifiHotspotItem = (LinearLayout) findViewById(R.id.wifiHotspotItem);
        if (Utils.isAndroidFROYO()) {
            wifiHotspotItem.setVisibility(0);
            findViewById(R.id.wifi_hotspot_line).setVisibility(0);
        }
        ((LinearLayout) findViewById(R.id.wifiHotspot)).setOnClickListener(this);
        ((LinearLayout) findViewById(R.id.bluetooth)).setOnClickListener(this);
        ((LinearLayout) findViewById(R.id.airmode)).setOnClickListener(this);
        ((LinearLayout) findViewById(R.id.gprs)).setOnClickListener(this);
        ((LinearLayout) findViewById(R.id.gps)).setOnClickListener(this);
        ((LinearLayout) findViewById(R.id.sync)).setOnClickListener(this);
        ((LinearLayout) findViewById(R.id.rotation)).setOnClickListener(this);
    }

    public void onResume() {
        super.onResume();
        refreshGPSData();
        refreshWifiData();
        refreshBluetoothData();
        refreshAirModeData();
        refreshSyncData();
        refreshAutoRotationData();
        refreshWifiHotspotData();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 1, 0, (int) R.string.menu_setting).setIcon(17301577);
        menu.add(0, 2, 0, (int) R.string.menu_about).setIcon(17301568);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case 1:
                startActivity(new Intent(this, Setting.class));
                break;
            case 2:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle((int) R.string.menu_about);
                builder.setIcon(17301659);
                builder.setMessage((int) R.string.about_content);
                builder.setCancelable(true);
                builder.create().show();
                break;
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void refreshBluetoothData() {
        switch (this.mBluetoothAdapter.getState()) {
            case 10:
                this.blueTV.setText((int) R.string.turned_off);
                this.blueBtn.setEnabled(true);
                this.blueBtn.setChecked(false);
                return;
            case 11:
                this.blueTV.setText((int) R.string.turning_on);
                this.blueBtn.setEnabled(false);
                return;
            case 12:
                this.blueTV.setText((int) R.string.turned_on);
                this.blueBtn.setEnabled(true);
                this.blueBtn.setChecked(true);
                return;
            case 13:
                this.blueTV.setText((int) R.string.turning_off);
                this.blueBtn.setEnabled(false);
                return;
            default:
                return;
        }
    }

    private boolean isAirModeEnabled() {
        return Settings.System.getInt(getContentResolver(), "airplane_mode_on", 0) != 0;
    }

    private boolean isDataSyncEnabled() {
        return ContentResolver.getMasterSyncAutomatically();
    }

    /* access modifiers changed from: private */
    public void refreshAirModeData() {
        if (isAirModeEnabled()) {
            this.airTV.setText((int) R.string.enabled);
            this.airBtn.setEnabled(false);
            this.airBtn.setChecked(true);
            switch (new ServiceState().getState()) {
                case 1:
                    this.airBtn.setEnabled(true);
                    return;
                default:
                    return;
            }
        } else {
            this.airTV.setText((int) R.string.disabled);
            this.airBtn.setEnabled(true);
            this.airBtn.setChecked(false);
        }
    }

    private void refreshSyncData() {
        if (isDataSyncEnabled()) {
            this.syncTV.setText((int) R.string.enabled);
            this.syncBtn.setEnabled(true);
            this.syncBtn.setChecked(true);
            return;
        }
        this.syncTV.setText((int) R.string.disabled);
        this.syncBtn.setEnabled(true);
        this.syncBtn.setChecked(false);
    }

    /* access modifiers changed from: private */
    public void refreshWifiData() {
        switch (this.wifiManager.getWifiState()) {
            case 0:
                this.wifiTV.setText((int) R.string.turning_off);
                this.wifiBtn.setEnabled(false);
                break;
            case 1:
                this.wifiTV.setText((int) R.string.turned_off);
                this.wifiBtn.setEnabled(true);
                this.wifiBtn.setChecked(false);
                break;
            case 2:
                this.wifiTV.setText((int) R.string.turning_on);
                this.wifiBtn.setEnabled(false);
                break;
            case 3:
                this.wifiTV.setText((int) R.string.turned_on);
                this.wifiBtn.setEnabled(true);
                this.wifiBtn.setChecked(true);
                break;
        }
        WifiInfo wifiInfo = this.wifiManager.getConnectionInfo();
        if (wifiInfo != null) {
            String bssid = wifiInfo.getBSSID();
            String wifiName = wifiInfo.getSSID();
            if (!TextUtils.isEmpty(wifiName)) {
                this.wifiTV.setText("Connected to " + wifiName);
            }
        }
        NetworkInfo networkInfo = this.connectManager.getNetworkInfo(1);
        if (networkInfo != null && NetworkInfo.State.CONNECTING == networkInfo.getState()) {
            this.wifiTV.setText("Obtaing ip address ");
        }
    }

    /* access modifiers changed from: private */
    public void refreshWifiHotspotData() {
        switch (Utils.getWifiApState(this.mCtx)) {
            case 0:
                this.wifiHotTV.setText((int) R.string.turning_off);
                this.wifiHotBtn.setEnabled(false);
                return;
            case 1:
                this.wifiHotTV.setText((int) R.string.turned_off);
                this.wifiHotBtn.setEnabled(true);
                this.wifiHotBtn.setChecked(false);
                return;
            case 2:
                this.wifiHotTV.setText((int) R.string.turning_on);
                this.wifiHotBtn.setEnabled(false);
                return;
            case 3:
                this.wifiHotTV.setText((int) R.string.turned_on);
                this.wifiHotBtn.setEnabled(true);
                this.wifiHotBtn.setChecked(true);
                return;
            default:
                return;
        }
    }

    private void refreshAutoRotationData() {
        if (Utils.isAutoRotation(this.mCtx)) {
            this.rotationTV.setText((int) R.string.enabled);
            this.rotationBtn.setEnabled(true);
            this.rotationBtn.setChecked(true);
            return;
        }
        this.rotationTV.setText((int) R.string.disabled);
        this.rotationBtn.setEnabled(true);
        this.rotationBtn.setChecked(false);
    }

    private void refreshGPSData() {
        try {
            if (this.mLocationManager.isProviderEnabled("gps")) {
                this.gpsTV.setText((int) R.string.turned_on);
                this.gpsBtn.setEnabled(true);
                this.gpsBtn.setChecked(true);
                return;
            }
            this.gpsTV.setText((int) R.string.turned_off);
            this.gpsBtn.setEnabled(true);
            this.gpsBtn.setChecked(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onClick(View v) {
        if (v.getId() == R.id.wifiBtn) {
            this.wifiBtn.toggle();
            if (this.wifiManager.isWifiEnabled()) {
                this.wifiManager.setWifiEnabled(false);
            } else {
                this.wifiManager.setWifiEnabled(true);
            }
        } else if (v.getId() == R.id.wifiHotBtn) {
            this.wifiHotBtn.toggle();
            if (Utils.isWifiApEnabled(this.mCtx)) {
                Utils.closeWifiAccessPoint(this.mCtx);
            } else {
                Utils.openWifiAccessPoint(this.mCtx);
            }
        } else if (v.getId() == R.id.bluetoothBtn) {
            this.blueBtn.toggle();
            if (this.mBluetoothAdapter.isEnabled()) {
                this.mBluetoothAdapter.disable();
            } else {
                this.mBluetoothAdapter.enable();
            }
        } else if (v.getId() == R.id.airmodeBtn) {
            this.airBtn.toggle();
            if (isAirModeEnabled()) {
                Settings.System.putInt(getContentResolver(), "airplane_mode_on", 0);
            } else {
                Settings.System.putInt(getContentResolver(), "airplane_mode_on", 1);
            }
            sendBroadcast(new Intent("android.intent.action.AIRPLANE_MODE"));
        } else if (v.getId() == R.id.gpsBtn) {
            this.gpsBtn.toggle();
            Intent i = new Intent("android.settings.LOCATION_SOURCE_SETTINGS");
            i.addFlags(268435456);
            startActivity(i);
        } else if (v.getId() == R.id.syncBtn) {
            if (isDataSyncEnabled()) {
                ContentResolver.setMasterSyncAutomatically(false);
            } else {
                ContentResolver.setMasterSyncAutomatically(true);
            }
            refreshSyncData();
        } else if (v.getId() == R.id.rotationBtn) {
            if (this.rotationBtn.isChecked()) {
                Utils.setAutoRotation(this.mCtx, true);
            } else {
                Utils.setAutoRotation(this.mCtx, false);
            }
            refreshAutoRotationData();
        } else if (v.getId() == R.id.gps) {
            Intent i2 = new Intent("android.settings.LOCATION_SOURCE_SETTINGS");
            i2.addFlags(268435456);
            startActivity(i2);
        } else if (v.getId() == R.id.bluetooth) {
            Intent i3 = new Intent("android.settings.BLUETOOTH_SETTINGS");
            i3.addFlags(268435456);
            startActivity(i3);
        } else if (v.getId() == R.id.wifi) {
            Intent i4 = new Intent("android.settings.WIFI_SETTINGS");
            i4.addFlags(268435456);
            startActivity(i4);
        } else if (v.getId() == R.id.airmode) {
            Intent i5 = new Intent("android.settings.AIRPLANE_MODE_SETTINGS");
            i5.addFlags(268435456);
            startActivity(i5);
        } else if (v.getId() == R.id.sync) {
            Intent i6 = new Intent("android.settings.SYNC_SETTINGS");
            i6.addFlags(268435456);
            startActivity(i6);
        } else if (v.getId() == R.id.gprs) {
            Intent intent = new Intent();
            intent.setComponent(new ComponentName("com.android.phone", String.valueOf("com.android.phone") + ".Settings"));
            startActivity(intent);
        } else if (v.getId() == R.id.wifiHotspot) {
            Intent intent2 = new Intent();
            intent2.setComponent(new ComponentName("com.android.settings", String.valueOf("com.android.settings") + ".wifi.WifiApSettings"));
            startActivity(intent2);
        } else if (v.getId() == R.id.rotation) {
            Intent i7 = new Intent("android.settings.DISPLAY_SETTINGS");
            i7.addFlags(268435456);
            startActivity(i7);
        }
    }
}
