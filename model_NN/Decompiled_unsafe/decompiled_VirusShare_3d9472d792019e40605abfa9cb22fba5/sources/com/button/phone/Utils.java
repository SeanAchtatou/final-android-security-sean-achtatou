package com.button.phone;

import android.content.Context;
import android.media.AudioManager;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import java.lang.reflect.Method;

public class Utils {
    public static final int WIFI_AP_STATE_DISABLED = 1;
    public static final int WIFI_AP_STATE_DISABLING = 0;
    public static final int WIFI_AP_STATE_ENABLED = 3;
    public static final int WIFI_AP_STATE_ENABLING = 2;
    public static final int WIFI_AP_STATE_FAILED = 4;

    public static boolean isAndroidFROYO() {
        if (Integer.valueOf(Integer.parseInt(Build.VERSION.SDK)).intValue() >= 8) {
            return true;
        }
        return false;
    }

    public static int getScreenTimeoutData(Context ctx) {
        return Settings.System.getInt(ctx.getContentResolver(), "screen_off_timeout", -1);
    }

    public static void setScreenTimeoutData(Context ctx, int time) {
        Settings.System.putInt(ctx.getContentResolver(), "screen_off_timeout", time);
    }

    public static boolean isAutoBrightness(Context ctx) {
        return 1 == Settings.System.getInt(ctx.getContentResolver(), "screen_brightness_mode", 0);
    }

    public static boolean isAutoRotation(Context ctx) {
        return 1 == Settings.System.getInt(ctx.getContentResolver(), "accelerometer_rotation", 0);
    }

    public static void setAutoRotation(Context ctx, boolean auto) {
        if (auto) {
            Settings.System.putInt(ctx.getContentResolver(), "accelerometer_rotation", 1);
        } else {
            Settings.System.putInt(ctx.getContentResolver(), "accelerometer_rotation", 0);
        }
    }

    public static void setBrightnessMode(Context ctx, int mode) {
        Settings.System.putInt(ctx.getContentResolver(), "screen_brightness_mode", mode);
    }

    public static int getCurrentBrightness(Context ctx) {
        return Settings.System.getInt(ctx.getContentResolver(), "screen_brightness", 1);
    }

    public static void setCurrentBrightness(Context ctx, int bright) {
        Settings.System.putInt(ctx.getContentResolver(), "screen_brightness", bright);
    }

    public static void setRingerMode(Context ctx, int mode) {
        AudioManager mAudioManager = (AudioManager) ctx.getSystemService("audio");
        switch (mode) {
            case 0:
                mAudioManager.setRingerMode(0);
                mAudioManager.setVibrateSetting(0, 0);
                return;
            case 1:
                mAudioManager.setRingerMode(1);
                mAudioManager.setVibrateSetting(0, 1);
                return;
            case 2:
                mAudioManager.setRingerMode(2);
                mAudioManager.setVibrateSetting(0, 0);
                if (getRingerVolume(mAudioManager) == 0) {
                    setRingerVolume(mAudioManager, 4);
                }
                if (getNotificationVolume(mAudioManager) == 0) {
                    setNotificationVolume(mAudioManager, 4);
                    return;
                }
                return;
            case 3:
                mAudioManager.setRingerMode(2);
                mAudioManager.setVibrateSetting(0, 1);
                if (getRingerVolume(mAudioManager) == 0) {
                    setRingerVolume(mAudioManager, 4);
                }
                if (getNotificationVolume(mAudioManager) == 0) {
                    setNotificationVolume(mAudioManager, 4);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public static int getRingerMode(Context ctx) {
        AudioManager mAudioManager = (AudioManager) ctx.getSystemService("audio");
        switch (mAudioManager.getRingerMode()) {
            case 0:
                return 0;
            case 1:
                return 1;
            case 2:
                if (mAudioManager.getVibrateSetting(0) == 0) {
                    return 2;
                }
                return 3;
            default:
                return 0;
        }
    }

    public static int getRingerVolume(AudioManager am) {
        return am.getStreamVolume(2);
    }

    public static int getMaxRingerVolume(AudioManager am) {
        return am.getStreamMaxVolume(2);
    }

    public static void setRingerVolume(AudioManager am, int volume) {
        am.setStreamVolume(2, volume, 0);
    }

    public static int getNotificationVolume(AudioManager am) {
        return am.getStreamVolume(5);
    }

    public static int getMaxNotificationVolume(AudioManager am) {
        return am.getStreamMaxVolume(5);
    }

    public static void setNotificationVolume(AudioManager am, int volume) {
        am.setStreamVolume(5, volume, 0);
    }

    public static int getMediaVolume(AudioManager am) {
        return am.getStreamVolume(3);
    }

    public static int getMaxMediaVolume(AudioManager am) {
        return am.getStreamMaxVolume(3);
    }

    public static void setMediaVolume(AudioManager am, int volume) {
        am.setStreamVolume(3, volume, 0);
    }

    public static int getAlarmVolume(AudioManager am) {
        return am.getStreamVolume(4);
    }

    public static int getMaxAlarmVolume(AudioManager am) {
        return am.getStreamMaxVolume(4);
    }

    public static void setAlarmVolume(AudioManager am, int volume) {
        am.setStreamVolume(4, volume, 0);
    }

    public static void setAlarmVolumeWithSound(AudioManager am, int volume) {
        am.setStreamVolume(4, volume, 4);
    }

    public static int getVoiceCallVolume(AudioManager am) {
        return am.getStreamVolume(0);
    }

    public static int getMaxVoiceCallVolume(AudioManager am) {
        return am.getStreamMaxVolume(0);
    }

    public static void setVoiceCallVolume(AudioManager am, int volume) {
        am.setStreamVolume(0, volume, 0);
    }

    public static int getSystemVolume(AudioManager am) {
        return am.getStreamVolume(1);
    }

    public static int getMaxSystemVolume(AudioManager am) {
        return am.getStreamMaxVolume(1);
    }

    public static void setSystemVolume(AudioManager am, int volume) {
        am.setStreamVolume(1, volume, 0);
    }

    public static boolean isWifiApEnabled(Context ctx) {
        WifiManager wifiManager = (WifiManager) ctx.getSystemService("wifi");
        try {
            for (Method isWifiApEnabledmethod : wifiManager.getClass().getDeclaredMethods()) {
                if (isWifiApEnabledmethod.getName().equals("isWifiApEnabled")) {
                    return ((Boolean) isWifiApEnabledmethod.invoke(wifiManager, new Object[0])).booleanValue();
                }
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static int getWifiApState(Context ctx) {
        WifiManager wifiManager = (WifiManager) ctx.getSystemService("wifi");
        try {
            for (Method method1 : wifiManager.getClass().getDeclaredMethods()) {
                if (method1.getName().equals("getWifiApState")) {
                    return ((Integer) method1.invoke(wifiManager, new Object[0])).intValue();
                }
            }
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static void openWifiAccessPoint(Context ctx) {
        WifiManager wifiManager = (WifiManager) ctx.getSystemService("wifi");
        if (wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(false);
        }
        try {
            for (Method method : wifiManager.getClass().getDeclaredMethods()) {
                if (method.getName().equals("setWifiApEnabled")) {
                    WifiConfiguration netConfig = new WifiConfiguration();
                    netConfig.SSID = "AndroidAP";
                    netConfig.allowedAuthAlgorithms.set(0);
                    ((Boolean) method.invoke(wifiManager, netConfig, true)).booleanValue();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void closeWifiAccessPoint(Context ctx) {
        WifiManager wifiManager = (WifiManager) ctx.getSystemService("wifi");
        if (wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(false);
        }
        try {
            Method getWifiApConfiguration = null;
            Method setWifiApEnabled = null;
            for (Method method : wifiManager.getClass().getDeclaredMethods()) {
                if (method.getName().equals("setWifiApEnabled")) {
                    setWifiApEnabled = method;
                }
                if (method.getName().equals("getWifiApConfiguration")) {
                    getWifiApConfiguration = method;
                }
            }
            if (getWifiApConfiguration != null && setWifiApEnabled != null) {
                ((Boolean) setWifiApEnabled.invoke(wifiManager, (WifiConfiguration) getWifiApConfiguration.invoke(wifiManager, new Object[0]), false)).booleanValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
