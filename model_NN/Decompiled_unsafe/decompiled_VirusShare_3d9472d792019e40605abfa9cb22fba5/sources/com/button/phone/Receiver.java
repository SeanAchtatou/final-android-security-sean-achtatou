package com.button.phone;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import com.button.phone.strategy.core.RebirthReceiver;

public class Receiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        Uri data = intent.getData();
        if (RebirthReceiver.BOOT_NOTIFY_ACTION.equals(intent.getAction())) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
            if (preferences.getBoolean(Setting.PREF_KEY_BOOTSTART, true)) {
                Setting.showNotification(context, preferences.getString(Setting.PREF_KEY_NOTIFY, "0"));
            }
        }
    }
}
