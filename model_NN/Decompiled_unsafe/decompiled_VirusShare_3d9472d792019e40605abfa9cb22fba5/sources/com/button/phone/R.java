package com.button.phone;

public final class R {

    public static final class array {
        public static final int ringer_list_preference = 2130968577;
        public static final int screen_timeout_list_preference = 2130968576;
        public static final int status_bar_variant_names = 2130968579;
        public static final int status_bar_variant_names_values = 2130968580;
        public static final int values_screen_timeout_list_preference = 2130968578;
    }

    public static final class attr {
        public static final int adSize = 2130771968;
        public static final int adUnitId = 2130771969;
    }

    public static final class drawable {
        public static final int ic_airplane = 2130837504;
        public static final int ic_autosync = 2130837505;
        public static final int ic_bt = 2130837506;
        public static final int ic_dataconn = 2130837507;
        public static final int ic_gps = 2130837508;
        public static final int ic_placeholder = 2130837509;
        public static final int ic_rotate = 2130837510;
        public static final int ic_wifi = 2130837511;
        public static final int ic_wifi_hs = 2130837512;
        public static final int icon = 2130837513;
        public static final int row_line = 2130837514;
        public static final int stat_notify = 2130837515;
    }

    public static final class id {
        public static final int BANNER = 2131034112;
        public static final int IAB_BANNER = 2131034114;
        public static final int IAB_LEADERBOARD = 2131034115;
        public static final int IAB_MRECT = 2131034113;
        public static final int ScrollContainer = 2131034116;
        public static final int airmode = 2131034142;
        public static final int airmodeBtn = 2131034145;
        public static final int airmode_state_text = 2131034144;
        public static final int airmode_text = 2131034143;
        public static final int bluetooth = 2131034138;
        public static final int bluetoothBtn = 2131034141;
        public static final int bluetooth_state_text = 2131034140;
        public static final int bluetooth_text = 2131034139;
        public static final int downloadIcon = 2131034150;
        public static final int downloadProgressBar = 2131034153;
        public static final int downloadProgressText = 2131034152;
        public static final int downloadTaskName = 2131034151;
        public static final int gprs = 2131034127;
        public static final int gprs_state_text = 2131034129;
        public static final int gprs_text = 2131034128;
        public static final int gps = 2131034146;
        public static final int gpsBtn = 2131034149;
        public static final int gps_state_text = 2131034148;
        public static final int gps_text = 2131034147;
        public static final int rotation = 2131034130;
        public static final int rotationBtn = 2131034133;
        public static final int rotation_state_text = 2131034132;
        public static final int rotation_text = 2131034131;
        public static final int sync = 2131034134;
        public static final int syncBtn = 2131034137;
        public static final int sync_state_text = 2131034136;
        public static final int sync_text = 2131034135;
        public static final int wifi = 2131034117;
        public static final int wifiBtn = 2131034120;
        public static final int wifiHotBtn = 2131034125;
        public static final int wifiHotspot = 2131034122;
        public static final int wifiHotspotItem = 2131034121;
        public static final int wifi_hot_state_text = 2131034124;
        public static final int wifi_hot_text = 2131034123;
        public static final int wifi_hotspot_line = 2131034126;
        public static final int wifi_state_text = 2131034119;
        public static final int wifi_text = 2131034118;
    }

    public static final class layout {
        public static final int main = 2130903040;
        public static final int notification = 2130903041;
        public static final int notification1 = 2130903042;
    }

    public static final class string {
        public static final int about_content = 2131099705;
        public static final int airplane_setting = 2131099656;
        public static final int app_name = 2131099649;
        public static final int auto_brightness = 2131099676;
        public static final int auto_rotation_setting = 2131099671;
        public static final int auto_sync_setting = 2131099659;
        public static final int bluetooth_setting = 2131099657;
        public static final int brightness_setting = 2131099661;
        public static final int cancel = 2131099653;
        public static final int connected = 2131099681;
        public static final int connecting = 2131099682;
        public static final int disabled = 2131099687;
        public static final int disabling = 2131099689;
        public static final int disconnected = 2131099683;
        public static final int disconnecting = 2131099684;
        public static final int enabled = 2131099686;
        public static final int enabling = 2131099688;
        public static final int gprs_setting = 2131099662;
        public static final int gps_setting = 2131099658;
        public static final int hello = 2131099648;
        public static final int menu_about = 2131099699;
        public static final int menu_setting = 2131099698;
        public static final int minute = 2131099673;
        public static final int minutes = 2131099674;
        public static final int never_time_out = 2131099675;
        public static final int notification_disable = 2131099704;
        public static final int notification_ringtone_setting = 2131099667;
        public static final int notification_ringtone_setting_summary = 2131099668;
        public static final int off = 2131099651;
        public static final int ok = 2131099652;
        public static final int on = 2131099650;
        public static final int phone_ringtone_setting = 2131099665;
        public static final int phone_ringtone_setting_summary = 2131099666;
        public static final int ringer_setting = 2131099663;
        public static final int screen_timeout_setting = 2131099660;
        public static final int seconds = 2131099672;
        public static final int select_phone_ringtone = 2131099669;
        public static final int set = 2131099690;
        public static final int setting_summary_auto_startup = 2131099701;
        public static final int setting_summary_show_notification = 2131099703;
        public static final int setting_title_auto_startup = 2131099700;
        public static final int setting_title_show_notification = 2131099702;
        public static final int suspended = 2131099685;
        public static final int turned_off = 2131099678;
        public static final int turned_on = 2131099677;
        public static final int turning_off = 2131099680;
        public static final int turning_on = 2131099679;
        public static final int volume_alarm = 2131099693;
        public static final int volume_bind_ringer = 2131099697;
        public static final int volume_media = 2131099694;
        public static final int volume_notification = 2131099695;
        public static final int volume_ringer = 2131099696;
        public static final int volume_setting = 2131099664;
        public static final int volume_setting_remind = 2131099670;
        public static final int volume_system = 2131099691;
        public static final int volume_voice_call = 2131099692;
        public static final int wifi_hot_setting = 2131099655;
        public static final int wifi_setting = 2131099654;
    }

    public static final class styleable {
        public static final int[] com_google_ads_AdView = {R.attr.adSize, R.attr.adUnitId};
        public static final int com_google_ads_AdView_adSize = 0;
        public static final int com_google_ads_AdView_adUnitId = 1;
    }
}
