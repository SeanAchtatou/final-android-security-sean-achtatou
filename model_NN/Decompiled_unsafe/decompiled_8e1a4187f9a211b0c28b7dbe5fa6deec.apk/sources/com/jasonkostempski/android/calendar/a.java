package com.jasonkostempski.android.calendar;

import android.text.format.DateFormat;
import android.text.format.DateUtils;
import java.util.Calendar;
import java.util.Date;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private Calendar f301a;
    private Date b;
    private Calendar c = Calendar.getInstance();
    private String[] d;
    private String[] e;
    private m f;
    private Calendar g;
    private Calendar h;

    public a() {
        this.c.set(11, 12);
        this.c.set(12, 0);
        this.f301a = Calendar.getInstance();
        this.f301a.set(11, 23);
        this.f301a.set(12, 59);
        this.d = new String[this.c.getActualMaximum(7)];
        this.e = new String[(this.c.getActualMaximum(2) + 1)];
        for (int i = 0; i < this.d.length; i++) {
            this.d[i] = DateUtils.getDayOfWeekString(i + 1, 30);
        }
        for (int i2 = 0; i2 < this.e.length; i2++) {
            this.e[i2] = DateUtils.getMonthString(i2, 30);
        }
    }

    private void h() {
        xh();
    }

    private final int xa() {
        return this.c.get(1);
    }

    private final String xa(CharSequence charSequence) {
        return DateFormat.format(charSequence, this.c).toString();
    }

    private final void xa(int i) {
        if (i != 0) {
            this.c.add(2, i);
            h();
        }
    }

    private final void xa(int i, int i2) {
        this.c.set(1, i);
        this.c.set(2, i2);
        h();
    }

    private final void xa(m mVar) {
        this.f = mVar;
    }

    private final void xa(Calendar calendar) {
        this.c = calendar;
        h();
    }

    private final void xa(Date date) {
        System.out.println(this + " / setFirstValidDate: " + date);
        this.b = date;
    }

    private final int xb() {
        return this.c.get(2);
    }

    private final void xb(int i) {
        if (i != 0) {
            this.c.add(5, i);
            h();
        }
    }

    private final void xb(int i, int i2) {
        this.c.add(2, i);
        this.c.set(5, i2);
        h();
    }

    private final int xc() {
        return this.c.get(5);
    }

    private final boolean xc(int i, int i2) {
        Calendar calendar = (Calendar) this.c.clone();
        calendar.add(2, i);
        calendar.set(5, i2);
        boolean z = calendar.before(this.f301a) && (this.b == null || calendar.getTimeInMillis() >= this.b.getTime());
        System.out.println("_firstValidDate:" + this.b);
        return z;
    }

    private final String[] xd() {
        return this.d;
    }

    private final String[] xe() {
        return this.e;
    }

    private final int[] xf() {
        int i;
        this.g = null;
        this.h = null;
        int[] iArr = new int[42];
        Calendar calendar = (Calendar) this.c.clone();
        calendar.set(5, 1);
        int i2 = calendar.get(7);
        int actualMaximum = calendar.getActualMaximum(5);
        int i3 = i2 - 1;
        if (i3 > 0) {
            calendar.set(5, -1);
            int actualMaximum2 = calendar.getActualMaximum(5);
            int i4 = i3;
            int i5 = 0;
            while (i4 > 0) {
                int i6 = (actualMaximum2 - i4) + 1;
                if (i5 == i3) {
                    this.g = (Calendar) calendar.clone();
                    this.g.set(5, i6);
                }
                iArr[i5] = i6;
                i4--;
                i5++;
            }
            i = i5;
        } else {
            i = 0;
        }
        int i7 = 0;
        int i8 = i;
        while (i7 < actualMaximum) {
            if (i7 == 0 && this.g == null) {
                this.g = (Calendar) calendar.clone();
            }
            iArr[i8] = i7 + 1;
            i7++;
            i8++;
        }
        int i9 = 1;
        int i10 = i8;
        for (int i11 = i8; i11 < iArr.length; i11++) {
            if (i11 == i10) {
                iArr[i10] = i9;
            }
            i9++;
            i10++;
        }
        this.h = (Calendar) this.c.clone();
        this.h.add(2, 1);
        this.h.set(5, iArr[41]);
        return iArr;
    }

    private final Calendar xg() {
        return (Calendar) this.c.clone();
    }

    private void xh() {
        if (this.f != null) {
            this.f.a(this);
        }
    }

    public final int a() {
        return xa();
    }

    public final String a(CharSequence charSequence) {
        return xa(charSequence);
    }

    public final void a(int i) {
        xa(i);
    }

    public final void a(int i, int i2) {
        xa(i, i2);
    }

    public final void a(m mVar) {
        xa(mVar);
    }

    public final void a(Calendar calendar) {
        xa(calendar);
    }

    public final void a(Date date) {
        xa(date);
    }

    public final int b() {
        return xb();
    }

    public final void b(int i) {
        xb(i);
    }

    public final void b(int i, int i2) {
        xb(i, i2);
    }

    public final int c() {
        return xc();
    }

    public final boolean c(int i, int i2) {
        return xc(i, i2);
    }

    public final String[] d() {
        return xd();
    }

    public final String[] e() {
        return xe();
    }

    public final int[] f() {
        return xf();
    }

    public final Calendar g() {
        return xg();
    }
}
