package com.jasonkostempski.android.calendar;

import android.view.View;

final class j implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ CalendarView f309a;

    j(CalendarView calendarView) {
        this.f309a = calendarView;
    }

    private final void xonClick(View view) {
        int i = view == this.f309a.s ? 1 : -1;
        if (this.f309a.v == 2) {
            this.f309a.m.a(i);
        } else if (this.f309a.v == 1) {
            this.f309a.m.b(i);
            CalendarView.xi(this.f309a);
        } else if (this.f309a.v == 3) {
            int unused = CalendarView.xa(this.f309a, i);
            this.f309a.c();
        }
    }

    public final void onClick(View view) {
        xonClick(view);
    }
}
