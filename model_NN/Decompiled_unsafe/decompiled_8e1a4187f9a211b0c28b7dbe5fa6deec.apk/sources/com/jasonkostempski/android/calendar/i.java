package com.jasonkostempski.android.calendar;

import android.view.View;

final class i implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ CalendarView f308a;

    i(CalendarView calendarView) {
        this.f308a = calendarView;
    }

    private final void xonClick(View view) {
        int[] iArr = (int[]) view.getTag();
        this.f308a.m.b(iArr[0], iArr[1]);
        CalendarView.xi(this.f308a);
        this.f308a.a(1);
    }

    public final void onClick(View view) {
        xonClick(view);
    }
}
