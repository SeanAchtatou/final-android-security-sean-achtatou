package com.agilebinary.a.a.a.c.b.a;

public final class f {

    /* renamed from: a  reason: collision with root package name */
    private k f38a;
    private boolean b;

    private final void xa() {
        this.b = true;
        if (this.f38a != null) {
            this.f38a.b();
        }
    }

    private final void xa(k kVar) {
        this.f38a = kVar;
        if (this.b) {
            kVar.b();
        }
    }

    public final void a() {
        xa();
    }

    public final void a(k kVar) {
        xa(kVar);
    }
}
