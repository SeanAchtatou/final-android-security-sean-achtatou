package com.agilebinary.a.a.a.c.b.a;

import com.agilebinary.a.a.a.c.b.a;
import com.agilebinary.a.a.a.h.c.c;
import com.agilebinary.a.a.a.h.m;
import java.util.concurrent.TimeUnit;

public final class g extends a {
    private final long d;
    private long e;
    private long f;
    private long g;

    public g(m mVar, c cVar, long j, TimeUnit timeUnit) {
        super(mVar, cVar);
        if (cVar == null) {
            throw new IllegalArgumentException("HTTP route may not be null");
        }
        this.d = System.currentTimeMillis();
        if (j > 0) {
            this.f = this.d + timeUnit.toMillis(j);
        } else {
            this.f = Long.MAX_VALUE;
        }
        this.g = this.f;
    }

    private final void xa(long j, TimeUnit timeUnit) {
        this.e = System.currentTimeMillis();
        this.g = Math.min(this.f, j > 0 ? this.e + timeUnit.toMillis(j) : Long.MAX_VALUE);
    }

    private final boolean xa(long j) {
        return j >= this.g;
    }

    private final void xb() {
        super.b();
    }

    private final com.agilebinary.a.a.a.h.g xc() {
        return this.f35a;
    }

    private final c xd() {
        return this.b;
    }

    private final long xe() {
        return this.e;
    }

    public final void a(long j, TimeUnit timeUnit) {
        xa(j, timeUnit);
    }

    public final boolean a(long j) {
        return xa(j);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        xb();
    }

    /* access modifiers changed from: protected */
    public final com.agilebinary.a.a.a.h.g c() {
        return xc();
    }

    /* access modifiers changed from: protected */
    public final c d() {
        return xd();
    }

    public final long e() {
        return xe();
    }
}
