package com.agilebinary.a.a.a.h.d;

import com.agilebinary.a.a.a.h.e.a;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;

public abstract class d implements b {

    /* renamed from: a  reason: collision with root package name */
    private static final String[] f110a;

    static {
        String[] strArr = {"ac", "co", "com", "ed", "edu", "go", "gouv", "gov", "info", "lg", "ne", "net", "or", "org"};
        f110a = strArr;
        Arrays.sort(strArr);
    }

    private static int a(String str) {
        return xa(str);
    }

    private void a(String str, X509Certificate x509Certificate) {
        xa(str, x509Certificate);
    }

    public static void a(String str, String[] strArr, String[] strArr2, boolean z) {
        xa(str, strArr, strArr2, z);
    }

    private static String[] a(X509Certificate x509Certificate, String str) {
        return xa(x509Certificate, str);
    }

    private static boolean b(String str) {
        return xb(str);
    }

    private static int xa(String str) {
        int i = 0;
        for (int i2 = 0; i2 < str.length(); i2++) {
            if (str.charAt(i2) == '.') {
                i++;
            }
        }
        return i;
    }

    private void xa(String str, X509Certificate x509Certificate) {
        String[] strArr;
        LinkedList linkedList = new LinkedList();
        StringTokenizer stringTokenizer = new StringTokenizer(x509Certificate.getSubjectX500Principal().toString(), ",");
        while (stringTokenizer.hasMoreTokens()) {
            String nextToken = stringTokenizer.nextToken();
            int indexOf = nextToken.indexOf("CN=");
            if (indexOf >= 0) {
                linkedList.add(nextToken.substring(indexOf + 3));
            }
        }
        if (!linkedList.isEmpty()) {
            String[] strArr2 = new String[linkedList.size()];
            linkedList.toArray(strArr2);
            strArr = strArr2;
        } else {
            strArr = null;
        }
        a(str, strArr, a(x509Certificate, str));
    }

    private final void xa(String str, SSLSocket sSLSocket) {
        if (str == null) {
            throw new NullPointerException("host to verify is null");
        }
        SSLSession session = sSLSocket.getSession();
        if (session == null) {
            sSLSocket.getInputStream().available();
            session = sSLSocket.getSession();
            if (session == null) {
                sSLSocket.startHandshake();
                session = sSLSocket.getSession();
            }
        }
        a(str, (X509Certificate) session.getPeerCertificates()[0]);
    }

    /* JADX WARNING: Removed duplicated region for block: B:42:0x00c6  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x010a  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x00df A[EDGE_INSN: B:61:0x00df->B:49:0x00df ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void xa(java.lang.String r10, java.lang.String[] r11, java.lang.String[] r12, boolean r13) {
        /*
            r9 = 46
            r8 = 3
            r7 = 1
            r6 = 0
            java.util.LinkedList r0 = new java.util.LinkedList
            r0.<init>()
            if (r11 == 0) goto L_0x0018
            int r1 = r11.length
            if (r1 <= 0) goto L_0x0018
            r1 = r11[r6]
            if (r1 == 0) goto L_0x0018
            r1 = r11[r6]
            r0.add(r1)
        L_0x0018:
            if (r12 == 0) goto L_0x0028
            int r1 = r12.length
            r2 = r6
        L_0x001c:
            if (r2 >= r1) goto L_0x0028
            r3 = r12[r2]
            if (r3 == 0) goto L_0x0025
            r0.add(r3)
        L_0x0025:
            int r2 = r2 + 1
            goto L_0x001c
        L_0x0028:
            boolean r1 = r0.isEmpty()
            if (r1 == 0) goto L_0x004d
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Certificate for <"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r10)
            java.lang.String r1 = "> doesn't contain CN or DNS subjectAlt"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            javax.net.ssl.SSLException r1 = new javax.net.ssl.SSLException
            r1.<init>(r0)
            throw r1
        L_0x004d:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = r10.trim()
            java.util.Locale r3 = java.util.Locale.ENGLISH
            java.lang.String r2 = r2.toLowerCase(r3)
            java.util.Iterator r3 = r0.iterator()
            r0 = r6
        L_0x0061:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x00df
            java.lang.Object r0 = r3.next()
            java.lang.String r0 = (java.lang.String) r0
            java.util.Locale r4 = java.util.Locale.ENGLISH
            java.lang.String r0 = r0.toLowerCase(r4)
            java.lang.String r4 = " <"
            r1.append(r4)
            r1.append(r0)
            r4 = 62
            r1.append(r4)
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x008b
            java.lang.String r4 = " OR"
            r1.append(r4)
        L_0x008b:
            java.lang.String r4 = "*."
            boolean r4 = r0.startsWith(r4)
            if (r4 == 0) goto L_0x0106
            int r4 = r0.lastIndexOf(r9)
            if (r4 < 0) goto L_0x0106
            int r4 = r0.length()
            r5 = 7
            if (r4 < r5) goto L_0x0104
            r5 = 9
            if (r4 > r5) goto L_0x0104
            int r5 = r4 - r8
            char r5 = r0.charAt(r5)
            if (r5 != r9) goto L_0x0104
            r5 = 2
            int r4 = r4 - r8
            java.lang.String r4 = r0.substring(r5, r4)
            java.lang.String[] r5 = com.agilebinary.a.a.a.h.d.d.f110a
            int r4 = java.util.Arrays.binarySearch(r5, r4)
            if (r4 < 0) goto L_0x0104
            r4 = r6
        L_0x00bb:
            if (r4 == 0) goto L_0x0106
            boolean r4 = b(r10)
            if (r4 != 0) goto L_0x0106
            r4 = r7
        L_0x00c4:
            if (r4 == 0) goto L_0x010a
            java.lang.String r4 = r0.substring(r7)
            boolean r4 = r2.endsWith(r4)
            if (r4 == 0) goto L_0x0110
            if (r13 == 0) goto L_0x0110
            int r4 = a(r2)
            int r0 = a(r0)
            if (r4 != r0) goto L_0x0108
            r0 = r7
        L_0x00dd:
            if (r0 == 0) goto L_0x0061
        L_0x00df:
            if (r0 != 0) goto L_0x010f
            javax.net.ssl.SSLException r0 = new javax.net.ssl.SSLException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "hostname in certificate didn't match: <"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r10)
            java.lang.String r3 = "> !="
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0104:
            r4 = r7
            goto L_0x00bb
        L_0x0106:
            r4 = r6
            goto L_0x00c4
        L_0x0108:
            r0 = r6
            goto L_0x00dd
        L_0x010a:
            boolean r0 = r2.equals(r0)
            goto L_0x00dd
        L_0x010f:
            return
        L_0x0110:
            r0 = r4
            goto L_0x00dd
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.h.d.d.xa(java.lang.String, java.lang.String[], java.lang.String[], boolean):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.security.cert.CertificateParsingException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    private static String[] xa(X509Certificate x509Certificate, String str) {
        Collection<List<?>> collection;
        int i = b(str) ? 7 : 2;
        LinkedList linkedList = new LinkedList();
        try {
            collection = x509Certificate.getSubjectAlternativeNames();
        } catch (CertificateParsingException e) {
            Logger.getLogger(d.class.getName()).log(Level.FINE, "Error parsing certificate.", (Throwable) e);
            collection = null;
        }
        if (collection != null) {
            for (List next : collection) {
                if (((Integer) next.get(0)).intValue() == i) {
                    linkedList.add((String) next.get(1));
                }
            }
        }
        if (linkedList.isEmpty()) {
            return null;
        }
        String[] strArr = new String[linkedList.size()];
        linkedList.toArray(strArr);
        return strArr;
    }

    private static boolean xb(String str) {
        return str != null && (a.a(str) || a.b(str));
    }

    private final boolean xverify(String str, SSLSession sSLSession) {
        try {
            a(str, (X509Certificate) sSLSession.getPeerCertificates()[0]);
            return true;
        } catch (SSLException e) {
            return false;
        }
    }

    public final void a(String str, SSLSocket sSLSocket) {
        xa(str, sSLSocket);
    }

    public final boolean verify(String str, SSLSession sSLSession) {
        return xverify(str, sSLSession);
    }
}
