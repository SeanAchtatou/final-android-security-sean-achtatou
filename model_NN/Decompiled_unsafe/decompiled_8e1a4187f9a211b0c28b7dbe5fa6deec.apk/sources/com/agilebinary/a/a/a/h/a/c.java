package com.agilebinary.a.a.a.h.a;

import java.util.concurrent.ConcurrentHashMap;

public final class c implements b {

    /* renamed from: a  reason: collision with root package name */
    private final ConcurrentHashMap f99a;
    private volatile int b;

    public c() {
        this((byte) 0);
    }

    private c(byte b2) {
        this.f99a = new ConcurrentHashMap();
        a(2);
    }

    private final int xa(com.agilebinary.a.a.a.h.c.c cVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("HTTP route may not be null.");
        }
        Integer num = (Integer) this.f99a.get(cVar);
        return num != null ? num.intValue() : this.b;
    }

    private final void xa(int i) {
        if (i <= 0) {
            throw new IllegalArgumentException("The maximum must be greater than 0.");
        }
        this.b = i;
    }

    private final String xtoString() {
        return this.f99a.toString();
    }

    public final int a(com.agilebinary.a.a.a.h.c.c cVar) {
        return xa(cVar);
    }

    public final void a(int i) {
        xa(i);
    }

    public final String toString() {
        return xtoString();
    }
}
