package com.agilebinary.a.a.a.g;

import com.agilebinary.a.a.a.c;
import com.agilebinary.a.a.a.i.b;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;

public final class e extends b {
    private final byte[] b;

    public e(c cVar) {
        super(cVar);
        if (!cVar.a() || cVar.c() < 0) {
            this.b = b.b(cVar);
        } else {
            this.b = null;
        }
    }

    private final void xa(OutputStream outputStream) {
        if (outputStream == null) {
            throw new IllegalArgumentException("Output stream may not be null");
        } else if (this.b != null) {
            outputStream.write(this.b);
        } else {
            this.f94a.a(outputStream);
        }
    }

    private final boolean xa() {
        return true;
    }

    private final long xc() {
        return this.b != null ? (long) this.b.length : this.f94a.c();
    }

    private final InputStream xf() {
        return this.b != null ? new ByteArrayInputStream(this.b) : this.f94a.f();
    }

    private final boolean xg() {
        return this.b == null && this.f94a.g();
    }

    private final boolean xk_() {
        return this.b == null && this.f94a.k_();
    }

    public final void a(OutputStream outputStream) {
        xa(outputStream);
    }

    public final boolean a() {
        return xa();
    }

    public final long c() {
        return xc();
    }

    public final InputStream f() {
        return xf();
    }

    public final boolean g() {
        return xg();
    }

    public final boolean k_() {
        return xk_();
    }
}
