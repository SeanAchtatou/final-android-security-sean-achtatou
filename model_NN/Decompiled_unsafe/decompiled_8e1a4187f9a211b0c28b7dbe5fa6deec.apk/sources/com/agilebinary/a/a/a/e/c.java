package com.agilebinary.a.a.a.e;

public final class c {
    private c() {
    }

    public static int a(e eVar) {
        return xa(eVar);
    }

    public static boolean b(e eVar) {
        return xb(eVar);
    }

    public static int c(e eVar) {
        return xc(eVar);
    }

    private static int xa(e eVar) {
        if (eVar != null) {
            return eVar.a("http.socket.timeout", 0);
        }
        throw new IllegalArgumentException("HTTP parameters may not be null");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.a.e.e.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.a.a.a.e.e.a(java.lang.String, int):int
      com.agilebinary.a.a.a.e.e.a(java.lang.String, java.lang.Object):com.agilebinary.a.a.a.e.e
      com.agilebinary.a.a.a.e.e.a(java.lang.String, boolean):boolean */
    private static boolean xb(e eVar) {
        if (eVar != null) {
            return eVar.a("http.socket.reuseaddr", false);
        }
        throw new IllegalArgumentException("HTTP parameters may not be null");
    }

    private static int xc(e eVar) {
        if (eVar != null) {
            return eVar.a("http.connection.timeout", 0);
        }
        throw new IllegalArgumentException("HTTP parameters may not be null");
    }
}
