package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.m;
import com.agilebinary.a.a.a.r;
import com.agilebinary.a.a.a.u;
import java.io.Serializable;

public final class e implements r, Serializable, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final String f10a;
    private final c b;
    private final int c;

    public e(c cVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Char array buffer may not be null");
        }
        int c2 = cVar.c(58);
        if (c2 == -1) {
            throw new u("Invalid header: " + cVar.toString());
        }
        String b2 = cVar.b(0, c2);
        if (b2.length() == 0) {
            throw new u("Invalid header: " + cVar.toString());
        }
        this.b = cVar;
        this.f10a = b2;
        this.c = c2 + 1;
    }

    private final String xa() {
        return this.f10a;
    }

    private final String xb() {
        return this.b.b(this.c, this.b.c());
    }

    private final m[] xc() {
        i iVar = new i(0, this.b.c());
        iVar.a(this.c);
        return f.f11a.a(this.b, iVar);
    }

    private final Object xclone() {
        return super.clone();
    }

    private final int xd() {
        return this.c;
    }

    private final c xe() {
        return this.b;
    }

    private final String xtoString() {
        return this.b.toString();
    }

    public final String a() {
        return xa();
    }

    public final String b() {
        return xb();
    }

    public final m[] c() {
        return xc();
    }

    public final Object clone() {
        return xclone();
    }

    public final int d() {
        return xd();
    }

    public final c e() {
        return xe();
    }

    public final String toString() {
        return xtoString();
    }
}
