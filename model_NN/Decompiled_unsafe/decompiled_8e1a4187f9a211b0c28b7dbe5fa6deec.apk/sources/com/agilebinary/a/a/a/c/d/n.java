package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.a.c;
import com.agilebinary.a.a.a.a.g;
import com.agilebinary.a.a.a.d.e;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public final class n implements e {

    /* renamed from: a  reason: collision with root package name */
    private final ConcurrentHashMap f73a = new ConcurrentHashMap();

    private static g a(Map map, c cVar) {
        return xa(map, cVar);
    }

    private final g xa(c cVar) {
        if (cVar != null) {
            return a(this.f73a, cVar);
        }
        throw new IllegalArgumentException("Authentication scope may not be null");
    }

    private static g xa(Map map, c cVar) {
        int i;
        g gVar = (g) map.get(cVar);
        if (gVar != null) {
            return gVar;
        }
        c cVar2 = null;
        int i2 = -1;
        for (c cVar3 : map.keySet()) {
            int a2 = cVar.a(cVar3);
            if (a2 > i2) {
                i = a2;
            } else {
                cVar3 = cVar2;
                i = i2;
            }
            i2 = i;
            cVar2 = cVar3;
        }
        return cVar2 != null ? (g) map.get(cVar2) : gVar;
    }

    private final String xtoString() {
        return this.f73a.toString();
    }

    public final g a(c cVar) {
        return xa(cVar);
    }

    public final String toString() {
        return xtoString();
    }
}
