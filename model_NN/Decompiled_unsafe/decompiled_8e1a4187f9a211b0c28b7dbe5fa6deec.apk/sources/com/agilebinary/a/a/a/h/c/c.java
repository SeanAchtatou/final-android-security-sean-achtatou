package com.agilebinary.a.a.a.h.c;

import com.agilebinary.a.a.a.b;
import java.net.InetAddress;

public final class c implements b, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private static final b[] f106a = new b[0];
    private final b b;
    private final InetAddress c;
    private final b[] d;
    private final g e;
    private final e f;
    private final boolean g;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.a.h.c.c.<init>(java.net.InetAddress, com.agilebinary.a.a.a.b, com.agilebinary.a.a.a.b[], boolean, com.agilebinary.a.a.a.h.c.g, com.agilebinary.a.a.a.h.c.e):void
     arg types: [?[OBJECT, ARRAY], com.agilebinary.a.a.a.b, com.agilebinary.a.a.a.b[], int, com.agilebinary.a.a.a.h.c.g, com.agilebinary.a.a.a.h.c.e]
     candidates:
      com.agilebinary.a.a.a.h.c.c.<init>(com.agilebinary.a.a.a.b, java.net.InetAddress, com.agilebinary.a.a.a.b[], boolean, com.agilebinary.a.a.a.h.c.g, com.agilebinary.a.a.a.h.c.e):void
      com.agilebinary.a.a.a.h.c.c.<init>(java.net.InetAddress, com.agilebinary.a.a.a.b, com.agilebinary.a.a.a.b[], boolean, com.agilebinary.a.a.a.h.c.g, com.agilebinary.a.a.a.h.c.e):void */
    public c(b bVar) {
        this((InetAddress) null, bVar, f106a, false, g.PLAIN, e.PLAIN);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public c(b bVar, InetAddress inetAddress, b bVar2, boolean z) {
        this(inetAddress, bVar, bVar2 == null ? f106a : new b[]{bVar2}, z, z ? g.TUNNELLED : g.PLAIN, z ? e.LAYERED : e.PLAIN);
        if (bVar2 == null) {
            throw new IllegalArgumentException("Proxy host may not be null.");
        }
    }

    public c(b bVar, InetAddress inetAddress, boolean z) {
        this(inetAddress, bVar, f106a, z, g.PLAIN, e.PLAIN);
    }

    public c(b bVar, InetAddress inetAddress, b[] bVarArr, boolean z, g gVar, e eVar) {
        this(inetAddress, bVar, a(bVarArr), z, gVar, eVar);
    }

    private c(InetAddress inetAddress, b bVar, b[] bVarArr, boolean z, g gVar, e eVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Target host may not be null.");
        } else if (bVarArr == null) {
            throw new IllegalArgumentException("Proxies may not be null.");
        } else if (gVar == g.TUNNELLED && bVarArr.length == 0) {
            throw new IllegalArgumentException("Proxy required if tunnelled.");
        } else {
            g gVar2 = gVar == null ? g.PLAIN : gVar;
            e eVar2 = eVar == null ? e.PLAIN : eVar;
            this.b = bVar;
            this.c = inetAddress;
            this.d = bVarArr;
            this.g = z;
            this.e = gVar2;
            this.f = eVar2;
        }
    }

    private static b[] a(b[] bVarArr) {
        return xa(bVarArr);
    }

    private final b xa() {
        return this.b;
    }

    private final b xa(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("Hop index must not be negative: " + i);
        }
        int length = this.d.length + 1;
        if (i < length) {
            return i < length - 1 ? this.d[i] : this.b;
        }
        throw new IllegalArgumentException("Hop index " + i + " exceeds route length " + length);
    }

    private static b[] xa(b[] bVarArr) {
        if (bVarArr == null || bVarArr.length <= 0) {
            return f106a;
        }
        for (b bVar : bVarArr) {
            if (bVar == null) {
                throw new IllegalArgumentException("Proxy chain may not contain null elements.");
            }
        }
        b[] bVarArr2 = new b[bVarArr.length];
        System.arraycopy(bVarArr, 0, bVarArr2, 0, bVarArr.length);
        return bVarArr2;
    }

    private final InetAddress xb() {
        return this.c;
    }

    private final int xc() {
        return this.d.length + 1;
    }

    private final Object xclone() {
        return super.clone();
    }

    private final boolean xd() {
        return this.e == g.TUNNELLED;
    }

    private final boolean xe() {
        return this.f == e.LAYERED;
    }

    private final boolean xequals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof c)) {
            return false;
        }
        c cVar = (c) obj;
        boolean equals = this.b.equals(cVar.b) & (this.c == cVar.c || (this.c != null && this.c.equals(cVar.c))) & (this.d == cVar.d || this.d.length == cVar.d.length) & (this.g == cVar.g && this.e == cVar.e && this.f == cVar.f);
        if (!equals || this.d == null) {
            return equals;
        }
        boolean z = equals;
        int i = 0;
        while (z && i < this.d.length) {
            z = this.d[i].equals(cVar.d[i]);
            i++;
        }
        return z;
    }

    private final boolean xf() {
        return this.g;
    }

    private final b xg() {
        if (this.d.length == 0) {
            return null;
        }
        return this.d[0];
    }

    private final int xhashCode() {
        int hashCode = this.b.hashCode();
        if (this.c != null) {
            hashCode ^= this.c.hashCode();
        }
        int length = hashCode ^ this.d.length;
        int i = length;
        for (b hashCode2 : this.d) {
            i ^= hashCode2.hashCode();
        }
        return ((this.g ? 286331153 ^ i : i) ^ this.e.hashCode()) ^ this.f.hashCode();
    }

    private final String xtoString() {
        StringBuilder sb = new StringBuilder(((this.d.length + 1) * 30) + 50);
        sb.append("HttpRoute[");
        if (this.c != null) {
            sb.append(this.c);
            sb.append("->");
        }
        sb.append('{');
        if (this.e == g.TUNNELLED) {
            sb.append('t');
        }
        if (this.f == e.LAYERED) {
            sb.append('l');
        }
        if (this.g) {
            sb.append('s');
        }
        sb.append("}->");
        for (b append : this.d) {
            sb.append(append);
            sb.append("->");
        }
        sb.append(this.b);
        sb.append(']');
        return sb.toString();
    }

    public final b a() {
        return xa();
    }

    public final b a(int i) {
        return xa(i);
    }

    public final InetAddress b() {
        return xb();
    }

    public final int c() {
        return xc();
    }

    public final Object clone() {
        return xclone();
    }

    public final boolean d() {
        return xd();
    }

    public final boolean e() {
        return xe();
    }

    public final boolean equals(Object obj) {
        return xequals(obj);
    }

    public final boolean f() {
        return xf();
    }

    public final b g() {
        return xg();
    }

    public final int hashCode() {
        return xhashCode();
    }

    public final String toString() {
        return xtoString();
    }
}
