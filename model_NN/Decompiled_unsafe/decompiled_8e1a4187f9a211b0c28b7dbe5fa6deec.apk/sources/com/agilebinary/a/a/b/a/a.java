package com.agilebinary.a.a.b.a;

import org.apache.commons.logging.Log;

public final class a {
    public static Log a(Class cls) {
        return xa(cls);
    }

    public static Log a(String str) {
        return xa(str);
    }

    private static Log xa(Class cls) {
        return new b(cls.getName());
    }

    private static Log xa(String str) {
        return new b(str);
    }
}
