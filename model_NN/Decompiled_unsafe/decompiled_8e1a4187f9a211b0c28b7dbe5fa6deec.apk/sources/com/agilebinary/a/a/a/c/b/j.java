package com.agilebinary.a.a.a.c.b;

import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.j.e;
import com.agilebinary.a.a.a.j.f;

public final class j implements e {

    /* renamed from: a  reason: collision with root package name */
    private final e f49a;
    private final q b;
    private final String c;

    public j(e eVar, q qVar, String str) {
        this.f49a = eVar;
        this.b = qVar;
        this.c = str != null ? str : "ASCII";
    }

    private final void xa(int i) {
        this.f49a.a(i);
        if (this.b.a()) {
            this.b.a(new byte[]{(byte) i});
        }
    }

    private final void xa(c cVar) {
        this.f49a.a(cVar);
        if (this.b.a()) {
            this.b.a((new String(cVar.b(), 0, cVar.c()) + "\r\n").getBytes(this.c));
        }
    }

    private final void xa(String str) {
        this.f49a.a(str);
        if (this.b.a()) {
            this.b.a((str + "\r\n").getBytes(this.c));
        }
    }

    private final void xa(byte[] bArr, int i, int i2) {
        this.f49a.a(bArr, i, i2);
        if (this.b.a()) {
            this.b.a(bArr, i, i2);
        }
    }

    private final void xb() {
        this.f49a.b();
    }

    private final f xc() {
        return this.f49a.c();
    }

    public final void a(int i) {
        xa(i);
    }

    public final void a(c cVar) {
        xa(cVar);
    }

    public final void a(String str) {
        xa(str);
    }

    public final void a(byte[] bArr, int i, int i2) {
        xa(bArr, i, i2);
    }

    public final void b() {
        xb();
    }

    public final f c() {
        return xc();
    }
}
