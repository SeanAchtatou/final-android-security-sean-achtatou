package com.agilebinary.a.a.a.i;

import java.lang.reflect.Method;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static final Method f114a = a();

    private a() {
    }

    private static Method a() {
        return xa();
    }

    public static void a(Throwable th, Throwable th2) {
        xa(th, th2);
    }

    private static Method xa() {
        try {
            return Throwable.class.getMethod("initCause", Throwable.class);
        } catch (NoSuchMethodException e) {
            return null;
        }
    }

    private static void xa(Throwable th, Throwable th2) {
        if (f114a != null) {
            try {
                f114a.invoke(th, th2);
            } catch (Exception e) {
            }
        }
    }
}
