package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.c;
import com.agilebinary.a.a.a.p;
import com.agilebinary.a.a.a.t;

public final class k extends l implements p {
    private c c;
    /* access modifiers changed from: private */
    public boolean d;

    public k(p pVar) {
        super(pVar);
        c h = pVar.h();
        this.c = h != null ? new h(this, h) : null;
        this.d = false;
    }

    private final boolean xg_() {
        t c2 = c("Expect");
        return c2 != null && "100-continue".equalsIgnoreCase(c2.b());
    }

    private final c xh() {
        return this.c;
    }

    private final boolean xi() {
        return this.c == null || this.c.a() || !this.d;
    }

    public final boolean g_() {
        return xg_();
    }

    public final c h() {
        return xh();
    }

    public final boolean i() {
        return xi();
    }
}
