package com.agilebinary.a.a.a.i;

import java.io.Serializable;

public final class e implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private byte[] f117a;
    private int b;

    public e(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("Buffer capacity may not be negative");
        }
        this.f117a = new byte[i];
    }

    private void d(int i) {
        xd(i);
    }

    private final void xa() {
        this.b = 0;
    }

    private final void xa(int i) {
        int i2 = this.b + 1;
        if (i2 > this.f117a.length) {
            d(i2);
        }
        this.f117a[this.b] = (byte) i;
        this.b = i2;
    }

    private final void xa(byte[] bArr, int i, int i2) {
        if (bArr != null) {
            if (i < 0 || i > bArr.length || i2 < 0 || i + i2 < 0 || i + i2 > bArr.length) {
                throw new IndexOutOfBoundsException("off: " + i + " len: " + i2 + " b.length: " + bArr.length);
            } else if (i2 != 0) {
                int i3 = this.b + i2;
                if (i3 > this.f117a.length) {
                    d(i3);
                }
                System.arraycopy(bArr, i, this.f117a, this.b, i2);
                this.b = i3;
            }
        }
    }

    private final void xa(char[] cArr, int i, int i2) {
        if (cArr != null) {
            if (i < 0 || i > cArr.length || i2 < 0 || i + i2 < 0 || i + i2 > cArr.length) {
                throw new IndexOutOfBoundsException("off: " + i + " len: " + i2 + " b.length: " + cArr.length);
            } else if (i2 != 0) {
                int i3 = this.b;
                int i4 = i3 + i2;
                if (i4 > this.f117a.length) {
                    d(i4);
                }
                int i5 = i;
                while (i3 < i4) {
                    this.f117a[i3] = (byte) cArr[i5];
                    i5++;
                    i3++;
                }
                this.b = i4;
            }
        }
    }

    private final int xb(int i) {
        return this.f117a[i];
    }

    private final byte[] xb() {
        byte[] bArr = new byte[this.b];
        if (this.b > 0) {
            System.arraycopy(this.f117a, 0, bArr, 0, this.b);
        }
        return bArr;
    }

    private final int xc() {
        return this.f117a.length;
    }

    private final void xc(int i) {
        if (i < 0 || i > this.f117a.length) {
            throw new IndexOutOfBoundsException("len: " + i + " < 0 or > buffer len: " + this.f117a.length);
        }
        this.b = i;
    }

    private final int xd() {
        return this.b;
    }

    private void xd(int i) {
        byte[] bArr = new byte[Math.max(this.f117a.length << 1, i)];
        System.arraycopy(this.f117a, 0, bArr, 0, this.b);
        this.f117a = bArr;
    }

    private final byte[] xe() {
        return this.f117a;
    }

    private final boolean xf() {
        return this.b == 0;
    }

    private final boolean xg() {
        return this.b == this.f117a.length;
    }

    public final void a() {
        xa();
    }

    public final void a(int i) {
        xa(i);
    }

    public final void a(byte[] bArr, int i, int i2) {
        xa(bArr, i, i2);
    }

    public final void a(char[] cArr, int i, int i2) {
        xa(cArr, i, i2);
    }

    public final int b(int i) {
        return xb(i);
    }

    public final byte[] b() {
        return xb();
    }

    public final int c() {
        return xc();
    }

    public final void c(int i) {
        xc(i);
    }

    public final int d() {
        return xd();
    }

    public final byte[] e() {
        return xe();
    }

    public final boolean f() {
        return xf();
    }

    public final boolean g() {
        return xg();
    }
}
