package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.i.c;

public final class i {

    /* renamed from: a  reason: collision with root package name */
    private final int f14a;
    private final int b;
    private int c;

    public i(int i, int i2) {
        if (i < 0) {
            throw new IndexOutOfBoundsException("Lower bound cannot be negative");
        } else if (i > i2) {
            throw new IndexOutOfBoundsException("Lower bound cannot be greater then upper bound");
        } else {
            this.f14a = i;
            this.b = i2;
            this.c = i;
        }
    }

    private final int xa() {
        return this.b;
    }

    private final void xa(int i) {
        if (i < this.f14a) {
            throw new IndexOutOfBoundsException("pos: " + i + " < lowerBound: " + this.f14a);
        } else if (i > this.b) {
            throw new IndexOutOfBoundsException("pos: " + i + " > upperBound: " + this.b);
        } else {
            this.c = i;
        }
    }

    private final int xb() {
        return this.c;
    }

    private final boolean xc() {
        return this.c >= this.b;
    }

    private final String xtoString() {
        c cVar = new c(16);
        cVar.a('[');
        cVar.a(Integer.toString(this.f14a));
        cVar.a('>');
        cVar.a(Integer.toString(this.c));
        cVar.a('>');
        cVar.a(Integer.toString(this.b));
        cVar.a(']');
        return cVar.toString();
    }

    public final int a() {
        return xa();
    }

    public final void a(int i) {
        xa(i);
    }

    public final int b() {
        return xb();
    }

    public final boolean c() {
        return xc();
    }

    public final String toString() {
        return xtoString();
    }
}
