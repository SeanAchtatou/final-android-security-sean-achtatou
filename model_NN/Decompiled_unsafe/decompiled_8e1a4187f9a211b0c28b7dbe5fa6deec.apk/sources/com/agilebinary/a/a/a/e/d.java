package com.agilebinary.a.a.a.e;

public abstract class d implements e {
    protected d() {
    }

    private final int xa(String str, int i) {
        Object a2 = a(str);
        return a2 == null ? i : ((Integer) a2).intValue();
    }

    private final boolean xa(String str, boolean z) {
        Object a2 = a(str);
        return a2 == null ? z : ((Boolean) a2).booleanValue();
    }

    private final e xb(String str) {
        a(str, Boolean.TRUE);
        return this;
    }

    private final e xb(String str, int i) {
        a(str, new Integer(i));
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.a.e.d.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.a.a.a.e.d.a(java.lang.String, int):int
      com.agilebinary.a.a.a.e.e.a(java.lang.String, int):int
      com.agilebinary.a.a.a.e.e.a(java.lang.String, java.lang.Object):com.agilebinary.a.a.a.e.e
      com.agilebinary.a.a.a.e.d.a(java.lang.String, boolean):boolean */
    private final boolean xc(String str) {
        return a(str, false);
    }

    private final boolean xd(String str) {
        return !a(str, false);
    }

    public final int a(String str, int i) {
        return xa(str, i);
    }

    public final boolean a(String str, boolean z) {
        return xa(str, z);
    }

    public final e b(String str) {
        return xb(str);
    }

    public final e b(String str, int i) {
        return xb(str, i);
    }

    public final boolean c(String str) {
        return xc(str);
    }

    public final boolean d(String str) {
        return xd(str);
    }
}
