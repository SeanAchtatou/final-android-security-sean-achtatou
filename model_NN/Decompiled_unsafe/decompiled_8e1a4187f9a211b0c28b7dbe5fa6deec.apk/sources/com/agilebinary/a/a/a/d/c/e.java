package com.agilebinary.a.a.a.d.c;

import com.agilebinary.a.a.a.c;
import com.agilebinary.a.a.a.d.a.a;
import com.agilebinary.a.a.a.p;
import com.agilebinary.a.a.a.t;

public abstract class e extends f implements p {
    private c c;

    private final void xa(c cVar) {
        this.c = cVar;
    }

    private Object xclone() {
        e eVar = (e) super.clone();
        if (this.c != null) {
            eVar.c = (c) a.a(this.c);
        }
        return eVar;
    }

    private final boolean xg_() {
        t c2 = c("Expect");
        return c2 != null && "100-continue".equalsIgnoreCase(c2.b());
    }

    private final c xh() {
        return this.c;
    }

    public final void a(c cVar) {
        xa(cVar);
    }

    public Object clone() {
        return xclone();
    }

    public final boolean g_() {
        return xg_();
    }

    public final c h() {
        return xh();
    }
}
