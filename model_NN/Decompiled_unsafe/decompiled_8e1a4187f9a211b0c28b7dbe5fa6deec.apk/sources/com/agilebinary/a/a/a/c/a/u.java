package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.k.j;
import com.agilebinary.a.a.a.k.k;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public abstract class u implements j {

    /* renamed from: a  reason: collision with root package name */
    private final Map f32a = new HashMap(10);

    private final k xa(String str) {
        return (k) this.f32a.get(str);
    }

    private final void xa(String str, k kVar) {
        if (str == null) {
            throw new IllegalArgumentException("Attribute name may not be null");
        } else if (kVar == null) {
            throw new IllegalArgumentException("Attribute handler may not be null");
        } else {
            this.f32a.put(str, kVar);
        }
    }

    private final Collection xc() {
        return this.f32a.values();
    }

    /* access modifiers changed from: protected */
    public final k a(String str) {
        return xa(str);
    }

    public final void a(String str, k kVar) {
        xa(str, kVar);
    }

    /* access modifiers changed from: protected */
    public final Collection c() {
        return xc();
    }
}
