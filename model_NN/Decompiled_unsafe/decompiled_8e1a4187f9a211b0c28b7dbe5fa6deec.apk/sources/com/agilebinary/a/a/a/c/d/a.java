package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.d.k;
import com.agilebinary.a.a.a.f;
import com.agilebinary.a.a.a.p;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ConnectException;
import java.net.UnknownHostException;
import javax.net.ssl.SSLException;

public final class a implements k {

    /* renamed from: a  reason: collision with root package name */
    private final int f68a;
    private final boolean b;

    public a() {
        this((byte) 0);
    }

    private a(byte b2) {
        this.f68a = 3;
        this.b = false;
    }

    private final boolean xa(IOException iOException, int i, com.agilebinary.a.a.a.f.k kVar) {
        if (iOException == null) {
            throw new IllegalArgumentException("Exception parameter may not be null");
        } else if (kVar == null) {
            throw new IllegalArgumentException("HTTP context may not be null");
        } else if (i > this.f68a) {
            return false;
        } else {
            if (iOException instanceof InterruptedIOException) {
                return false;
            }
            if (iOException instanceof UnknownHostException) {
                return false;
            }
            if (iOException instanceof ConnectException) {
                return false;
            }
            if (iOException instanceof SSLException) {
                return false;
            }
            if (!(((f) kVar.a("http.request")) instanceof p)) {
                return true;
            }
            Boolean bool = (Boolean) kVar.a("http.request_sent");
            return !(bool != null && bool.booleanValue()) || this.b;
        }
    }

    public final boolean a(IOException iOException, int i, com.agilebinary.a.a.a.f.k kVar) {
        return xa(iOException, i, kVar);
    }
}
