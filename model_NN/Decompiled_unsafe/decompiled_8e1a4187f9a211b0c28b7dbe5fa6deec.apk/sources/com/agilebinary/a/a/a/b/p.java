package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.m;
import com.agilebinary.a.a.a.v;
import com.agilebinary.a.a.a.w;
import java.util.NoSuchElementException;

public final class p implements v {

    /* renamed from: a  reason: collision with root package name */
    private final w f18a;
    private final j b;
    private m c;
    private c d;
    private i e;

    public p(w wVar) {
        this(wVar, f.f11a);
    }

    private p(w wVar, j jVar) {
        this.c = null;
        this.d = null;
        this.e = null;
        if (wVar == null) {
            throw new IllegalArgumentException("Header iterator may not be null");
        } else if (jVar == null) {
            throw new IllegalArgumentException("Parser may not be null");
        } else {
            this.f18a = wVar;
            this.b = jVar;
        }
    }

    private void b() {
        xb();
    }

    private final m xa() {
        if (this.c == null) {
            b();
        }
        if (this.c == null) {
            throw new NoSuchElementException("No more header elements available");
        }
        m mVar = this.c;
        this.c = null;
        return mVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x005e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void xb() {
        /*
            r6 = this;
            r5 = 0
            r4 = 0
        L_0x0002:
            com.agilebinary.a.a.a.w r1 = r6.f18a
            boolean r1 = r1.hasNext()
            if (r1 != 0) goto L_0x000e
            com.agilebinary.a.a.a.b.i r1 = r6.e
            if (r1 == 0) goto L_0x007a
        L_0x000e:
            com.agilebinary.a.a.a.b.i r1 = r6.e
            if (r1 == 0) goto L_0x001a
            com.agilebinary.a.a.a.b.i r1 = r6.e
            boolean r1 = r1.c()
            if (r1 == 0) goto L_0x0052
        L_0x001a:
            r6.e = r4
            r6.d = r4
        L_0x001e:
            com.agilebinary.a.a.a.w r1 = r6.f18a
            boolean r1 = r1.hasNext()
            if (r1 == 0) goto L_0x0052
            com.agilebinary.a.a.a.w r1 = r6.f18a
            com.agilebinary.a.a.a.t r2 = r1.a()
            boolean r1 = r2 instanceof com.agilebinary.a.a.a.r
            if (r1 == 0) goto L_0x007b
            r0 = r2
            com.agilebinary.a.a.a.r r0 = (com.agilebinary.a.a.a.r) r0
            r1 = r0
            com.agilebinary.a.a.a.i.c r1 = r1.e()
            r6.d = r1
            com.agilebinary.a.a.a.b.i r1 = new com.agilebinary.a.a.a.b.i
            com.agilebinary.a.a.a.i.c r3 = r6.d
            int r3 = r3.c()
            r1.<init>(r5, r3)
            r6.e = r1
            com.agilebinary.a.a.a.b.i r1 = r6.e
            com.agilebinary.a.a.a.r r2 = (com.agilebinary.a.a.a.r) r2
            int r2 = r2.d()
            r1.a(r2)
        L_0x0052:
            com.agilebinary.a.a.a.b.i r1 = r6.e
            if (r1 == 0) goto L_0x0002
        L_0x0056:
            com.agilebinary.a.a.a.b.i r1 = r6.e
            boolean r1 = r1.c()
            if (r1 != 0) goto L_0x009f
            com.agilebinary.a.a.a.b.j r1 = r6.b
            com.agilebinary.a.a.a.i.c r2 = r6.d
            com.agilebinary.a.a.a.b.i r3 = r6.e
            com.agilebinary.a.a.a.m r1 = r1.b(r2, r3)
            java.lang.String r2 = r1.a()
            int r2 = r2.length()
            if (r2 != 0) goto L_0x0078
            java.lang.String r2 = r1.b()
            if (r2 == 0) goto L_0x0056
        L_0x0078:
            r6.c = r1
        L_0x007a:
            return
        L_0x007b:
            java.lang.String r1 = r2.b()
            if (r1 == 0) goto L_0x001e
            com.agilebinary.a.a.a.i.c r2 = new com.agilebinary.a.a.a.i.c
            int r3 = r1.length()
            r2.<init>(r3)
            r6.d = r2
            com.agilebinary.a.a.a.i.c r2 = r6.d
            r2.a(r1)
            com.agilebinary.a.a.a.b.i r1 = new com.agilebinary.a.a.a.b.i
            com.agilebinary.a.a.a.i.c r2 = r6.d
            int r2 = r2.c()
            r1.<init>(r5, r2)
            r6.e = r1
            goto L_0x0052
        L_0x009f:
            com.agilebinary.a.a.a.b.i r1 = r6.e
            boolean r1 = r1.c()
            if (r1 == 0) goto L_0x0002
            r6.e = r4
            r6.d = r4
            goto L_0x0002
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.b.p.xb():void");
    }

    private final boolean xhasNext() {
        if (this.c == null) {
            b();
        }
        return this.c != null;
    }

    private final Object xnext() {
        return a();
    }

    private final void xremove() {
        throw new UnsupportedOperationException("Remove not supported");
    }

    public final m a() {
        return xa();
    }

    public final boolean hasNext() {
        return xhasNext();
    }

    public final Object next() {
        return xnext();
    }

    public final void remove() {
        xremove();
    }
}
