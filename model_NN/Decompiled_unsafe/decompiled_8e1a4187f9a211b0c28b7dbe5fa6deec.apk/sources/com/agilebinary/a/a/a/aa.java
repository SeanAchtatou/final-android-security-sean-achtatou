package com.agilebinary.a.a.a;

import java.io.Serializable;

public final class aa extends a implements Serializable {
    public static final aa c = new aa(1, 0);
    public static final aa d = new aa(1, 1);
    private static aa e = new aa(0, 9);

    private aa(int i, int i2) {
        super("HTTP", i, i2);
    }

    private final a xa(int i, int i2) {
        if (i == this.f2a && i2 == this.b) {
            return this;
        }
        if (i == 1) {
            if (i2 == 0) {
                return c;
            }
            if (i2 == 1) {
                return d;
            }
        }
        return (i == 0 && i2 == 9) ? e : new aa(i, i2);
    }

    public final a a(int i, int i2) {
        return xa(i, i2);
    }
}
