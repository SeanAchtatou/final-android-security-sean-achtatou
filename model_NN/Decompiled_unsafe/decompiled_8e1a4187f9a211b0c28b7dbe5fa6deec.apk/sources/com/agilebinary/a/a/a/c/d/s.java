package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.d.a;
import com.agilebinary.a.a.a.d.c.g;
import com.agilebinary.a.a.a.d.f;
import com.agilebinary.a.a.a.d.l;
import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.f.b;
import com.agilebinary.a.a.a.f.c;
import com.agilebinary.a.a.a.h.c.h;
import com.agilebinary.a.a.a.h.k;
import com.agilebinary.a.a.a.h.n;
import com.agilebinary.a.a.a.j;
import com.agilebinary.a.a.a.p;
import com.agilebinary.a.a.a.t;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;

public final class s implements a {

    /* renamed from: a  reason: collision with root package name */
    private final Log f78a;
    private k b;
    private h c;
    private com.agilebinary.a.a.a.s d;
    private n e;
    private b f;
    private c g;
    private com.agilebinary.a.a.a.d.k h;
    private com.agilebinary.a.a.a.d.h i = null;
    private com.agilebinary.a.a.a.d.c j;
    private com.agilebinary.a.a.a.d.b k;
    private com.agilebinary.a.a.a.d.b l;
    private f m;
    private e n;
    private com.agilebinary.a.a.a.h.a o;
    private com.agilebinary.a.a.a.a.e p;
    private com.agilebinary.a.a.a.a.e q;
    private int r;
    private int s;
    private int t;
    private com.agilebinary.a.a.a.b u;

    public s(Log log, b bVar, k kVar, com.agilebinary.a.a.a.s sVar, n nVar, h hVar, c cVar, com.agilebinary.a.a.a.d.k kVar2, com.agilebinary.a.a.a.d.c cVar2, com.agilebinary.a.a.a.d.b bVar2, com.agilebinary.a.a.a.d.b bVar3, f fVar, e eVar) {
        if (log == null) {
            throw new IllegalArgumentException("Log may not be null.");
        } else if (bVar == null) {
            throw new IllegalArgumentException("Request executor may not be null.");
        } else if (kVar == null) {
            throw new IllegalArgumentException("Client connection manager may not be null.");
        } else if (sVar == null) {
            throw new IllegalArgumentException("Connection reuse strategy may not be null.");
        } else if (nVar == null) {
            throw new IllegalArgumentException("Connection keep alive strategy may not be null.");
        } else if (hVar == null) {
            throw new IllegalArgumentException("Route planner may not be null.");
        } else if (cVar == null) {
            throw new IllegalArgumentException("HTTP protocol processor may not be null.");
        } else if (kVar2 == null) {
            throw new IllegalArgumentException("HTTP request retry handler may not be null.");
        } else if (cVar2 == null) {
            throw new IllegalArgumentException("Redirect strategy may not be null.");
        } else if (bVar2 == null) {
            throw new IllegalArgumentException("Target authentication handler may not be null.");
        } else if (bVar3 == null) {
            throw new IllegalArgumentException("Proxy authentication handler may not be null.");
        } else if (fVar == null) {
            throw new IllegalArgumentException("User token handler may not be null.");
        } else if (eVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        } else {
            this.f78a = log;
            this.f = bVar;
            this.b = kVar;
            this.d = sVar;
            this.e = nVar;
            this.c = hVar;
            this.g = cVar;
            this.h = kVar2;
            this.j = cVar2;
            this.k = bVar2;
            this.l = bVar3;
            this.m = fVar;
            this.n = eVar;
            this.o = null;
            this.r = 0;
            this.s = 0;
            this.t = this.n.a("http.protocol.max-redirects", 100);
            this.p = new com.agilebinary.a.a.a.a.e();
            this.q = new com.agilebinary.a.a.a.a.e();
        }
    }

    private static l a(com.agilebinary.a.a.a.f fVar) {
        return xa(fVar);
    }

    private m a(m mVar, j jVar, com.agilebinary.a.a.a.f.k kVar) {
        return xa(mVar, jVar, kVar);
    }

    private com.agilebinary.a.a.a.h.c.c a(com.agilebinary.a.a.a.b bVar, com.agilebinary.a.a.a.f fVar) {
        return xa(bVar, fVar);
    }

    private void a() {
        xa();
    }

    private void a(com.agilebinary.a.a.a.a.e eVar, com.agilebinary.a.a.a.b bVar, com.agilebinary.a.a.a.d.e eVar2) {
        xa(eVar, bVar, eVar2);
    }

    private void a(m mVar, com.agilebinary.a.a.a.f.k kVar) {
        xa(mVar, kVar);
    }

    private void a(Map map, com.agilebinary.a.a.a.a.e eVar, com.agilebinary.a.a.a.d.b bVar, j jVar, com.agilebinary.a.a.a.f.k kVar) {
        xa(map, eVar, bVar, jVar, kVar);
    }

    private boolean a(com.agilebinary.a.a.a.h.c.c cVar, com.agilebinary.a.a.a.f.k kVar) {
        return xa(cVar, kVar);
    }

    private j b(m mVar, com.agilebinary.a.a.a.f.k kVar) {
        return xb(mVar, kVar);
    }

    private void b() {
        xb();
    }

    private static l xa(com.agilebinary.a.a.a.f fVar) {
        return fVar instanceof p ? new k((p) fVar) : new l(fVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.a.e.e.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.a.a.a.e.e.a(java.lang.String, int):int
      com.agilebinary.a.a.a.e.e.a(java.lang.String, java.lang.Object):com.agilebinary.a.a.a.e.e
      com.agilebinary.a.a.a.e.e.a(java.lang.String, boolean):boolean */
    private m xa(m mVar, j jVar, com.agilebinary.a.a.a.f.k kVar) {
        com.agilebinary.a.a.a.h.c.c b2 = mVar.b();
        l a2 = mVar.a();
        e g2 = a2.g();
        if (g2 == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        } else if (!g2.a("http.protocol.handle-redirects", true) || !this.j.a(a2, jVar)) {
            com.agilebinary.a.a.a.d.e eVar = (com.agilebinary.a.a.a.d.e) kVar.a("http.auth.credentials-provider");
            if (eVar != null && com.agilebinary.a.a.a.d.d.a.a(g2)) {
                if (this.k.a(jVar)) {
                    com.agilebinary.a.a.a.b bVar = (com.agilebinary.a.a.a.b) kVar.a("http.target_host");
                    com.agilebinary.a.a.a.b a3 = bVar == null ? b2.a() : bVar;
                    this.f78a.debug("Target requested authentication");
                    try {
                        a(this.k.b(jVar), this.p, this.k, jVar, kVar);
                    } catch (com.agilebinary.a.a.a.a.b e2) {
                        if (this.f78a.isWarnEnabled()) {
                            this.f78a.warn("Authentication error: " + e2.getMessage());
                            return null;
                        }
                    }
                    a(this.p, a3, eVar);
                    if (this.p.d() != null) {
                        return mVar;
                    }
                    return null;
                }
                this.p.a((com.agilebinary.a.a.a.a.c) null);
                if (this.l.a(jVar)) {
                    com.agilebinary.a.a.a.b g3 = b2.g();
                    this.f78a.debug("Proxy requested authentication");
                    try {
                        a(this.l.b(jVar), this.q, this.l, jVar, kVar);
                    } catch (com.agilebinary.a.a.a.a.b e3) {
                        if (this.f78a.isWarnEnabled()) {
                            this.f78a.warn("Authentication error: " + e3.getMessage());
                            return null;
                        }
                    }
                    a(this.q, g3, eVar);
                    if (this.q.d() != null) {
                        return mVar;
                    }
                    return null;
                }
                this.q.a((com.agilebinary.a.a.a.a.c) null);
            }
            return null;
        } else if (this.s >= this.t) {
            throw new l("Maximum redirects (" + this.t + ") exceeded");
        } else {
            this.s++;
            this.u = null;
            com.agilebinary.a.a.a.d.c.b a4 = this.j.a(a2, jVar, kVar);
            a4.a(a2.k().e());
            URI e_ = a4.e_();
            if (e_.getHost() == null) {
                throw new com.agilebinary.a.a.a.l("Redirect URI does not specify a valid host name: " + e_);
            }
            com.agilebinary.a.a.a.b bVar2 = new com.agilebinary.a.a.a.b(e_.getHost(), e_.getPort(), e_.getScheme());
            this.p.a((com.agilebinary.a.a.a.a.c) null);
            this.q.a((com.agilebinary.a.a.a.a.c) null);
            if (!b2.a().equals(bVar2)) {
                this.p.a();
                com.agilebinary.a.a.a.a.h c2 = this.q.c();
                if (c2 != null && c2.d()) {
                    this.q.a();
                }
            }
            l a5 = a(a4);
            a5.a(g2);
            com.agilebinary.a.a.a.h.c.c a6 = a(bVar2, a5);
            m mVar2 = new m(a5, a6);
            if (this.f78a.isDebugEnabled()) {
                this.f78a.debug("Redirecting to '" + e_ + "' via " + a6);
            }
            return mVar2;
        }
    }

    private com.agilebinary.a.a.a.h.c.c xa(com.agilebinary.a.a.a.b bVar, com.agilebinary.a.a.a.f fVar) {
        com.agilebinary.a.a.a.b bVar2 = bVar == null ? (com.agilebinary.a.a.a.b) fVar.g().a("http.default-host") : bVar;
        if (bVar2 != null) {
            return this.c.a(bVar2, fVar);
        }
        throw new IllegalStateException("Target host must not be null, or set in parameters.");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.a.e.e.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.a.a.a.e.e.a(java.lang.String, int):int
      com.agilebinary.a.a.a.e.e.a(java.lang.String, java.lang.Object):com.agilebinary.a.a.a.e.e
      com.agilebinary.a.a.a.e.e.a(java.lang.String, boolean):boolean */
    private final j xa(com.agilebinary.a.a.a.b bVar, com.agilebinary.a.a.a.f fVar, com.agilebinary.a.a.a.f.k kVar) {
        l a2;
        l a3 = a(fVar);
        a3.a(this.n);
        com.agilebinary.a.a.a.h.c.c a4 = a(bVar, a3);
        this.u = (com.agilebinary.a.a.a.b) fVar.g().a("http.virtual-host");
        m mVar = new m(a3, a4);
        long c2 = (long) com.agilebinary.a.a.a.e.c.c(this.n);
        boolean z = false;
        j jVar = null;
        m mVar2 = mVar;
        boolean z2 = false;
        while (!z) {
            try {
                a2 = mVar2.a();
                com.agilebinary.a.a.a.h.c.c b2 = mVar2.b();
                Object a5 = kVar.a("http.user-token");
                if (this.o == null) {
                    com.agilebinary.a.a.a.h.b a6 = this.b.a(b2, a5);
                    if (fVar instanceof g) {
                        ((g) fVar).a(a6);
                    }
                    this.o = a6.a(c2, TimeUnit.MILLISECONDS);
                    e eVar = this.n;
                    if (eVar == null) {
                        throw new IllegalArgumentException("HTTP parameters may not be null");
                    } else if (eVar.a("http.connection.stalecheck", true) && this.o.l()) {
                        this.f78a.debug("Stale connection check");
                        if (this.o.e()) {
                            this.f78a.debug("Stale connection detected");
                            this.o.k();
                        }
                    }
                }
                if (fVar instanceof g) {
                    ((g) fVar).a(this.o);
                }
                try {
                    a(mVar2, kVar);
                    a2.j();
                    URI e_ = a2.e_();
                    if (b2.g() == null || b2.d()) {
                        if (e_.isAbsolute()) {
                            a2.a(com.agilebinary.a.a.a.d.a.b.a(e_, (com.agilebinary.a.a.a.b) null));
                        }
                    } else if (!e_.isAbsolute()) {
                        a2.a(com.agilebinary.a.a.a.d.a.b.a(e_, b2.a()));
                    }
                    com.agilebinary.a.a.a.b bVar2 = this.u;
                    if (bVar2 == null) {
                        bVar2 = b2.a();
                    }
                    com.agilebinary.a.a.a.b g2 = b2.g();
                    kVar.a("http.target_host", bVar2);
                    kVar.a("http.proxy_host", g2);
                    kVar.a("http.connection", this.o);
                    kVar.a("http.auth.target-scope", this.p);
                    kVar.a("http.auth.proxy-scope", this.q);
                    b.a(a2, this.g, kVar);
                    jVar = b(mVar2, kVar);
                    if (jVar != null) {
                        jVar.a(this.n);
                        b.a(jVar, this.g, kVar);
                        z2 = this.d.a(jVar, kVar);
                        if (z2) {
                            long a7 = this.e.a(jVar);
                            if (this.f78a.isDebugEnabled()) {
                                this.f78a.debug("Connection can be kept alive for " + (a7 >= 0 ? a7 + " " + TimeUnit.MILLISECONDS : "ever"));
                            }
                            this.o.a(a7, TimeUnit.MILLISECONDS);
                        }
                        m a8 = a(mVar2, jVar, kVar);
                        if (a8 == null) {
                            z = true;
                        } else {
                            if (z2) {
                                com.agilebinary.a.a.a.i.b.a(jVar.b());
                                this.o.g();
                            } else {
                                this.o.k();
                            }
                            if (!a8.b().equals(mVar2.b())) {
                                a();
                            }
                            mVar2 = a8;
                        }
                        if (this.o != null && a5 == null) {
                            Object a9 = this.m.a(kVar);
                            kVar.a("http.user-token", a9);
                            if (a9 != null) {
                                this.o.a(a9);
                            }
                        }
                    }
                } catch (g e2) {
                    if (this.f78a.isDebugEnabled()) {
                        this.f78a.debug(e2.getMessage());
                    }
                    jVar = e2.a();
                }
            } catch (URISyntaxException e3) {
                throw new com.agilebinary.a.a.a.l("Invalid URI: " + a2.a().c(), e3);
            } catch (InterruptedException e4) {
                InterruptedIOException interruptedIOException = new InterruptedIOException();
                interruptedIOException.initCause(e4);
                throw interruptedIOException;
            } catch (com.agilebinary.a.a.a.c.b.n e5) {
                InterruptedIOException interruptedIOException2 = new InterruptedIOException("Connection has been shut down");
                interruptedIOException2.initCause(e5);
                throw interruptedIOException2;
            } catch (com.agilebinary.a.a.a.k e6) {
                b();
                throw e6;
            } catch (IOException e7) {
                b();
                throw e7;
            } catch (RuntimeException e8) {
                b();
                throw e8;
            }
        }
        if (jVar == null || jVar.b() == null || !jVar.b().g()) {
            if (z2) {
                this.o.g();
            }
            a();
        } else {
            jVar.a(new com.agilebinary.a.a.a.h.j(jVar.b(), this.o, z2));
        }
        return jVar;
    }

    private void xa() {
        try {
            this.o.d_();
        } catch (IOException e2) {
            this.f78a.debug("IOException releasing connection", e2);
        }
        this.o = null;
    }

    private void xa(com.agilebinary.a.a.a.a.e eVar, com.agilebinary.a.a.a.b bVar, com.agilebinary.a.a.a.d.e eVar2) {
        if (eVar.b()) {
            String a2 = bVar.a();
            int b2 = bVar.b();
            if (b2 < 0) {
                b2 = this.b.a().a(bVar).a();
            }
            com.agilebinary.a.a.a.a.h c2 = eVar.c();
            com.agilebinary.a.a.a.a.c cVar = new com.agilebinary.a.a.a.a.c(a2, b2, c2.c(), c2.b());
            if (this.f78a.isDebugEnabled()) {
                this.f78a.debug("Authentication scope: " + cVar);
            }
            com.agilebinary.a.a.a.a.g d2 = eVar.d();
            if (d2 == null) {
                d2 = eVar2.a(cVar);
                if (this.f78a.isDebugEnabled()) {
                    if (d2 != null) {
                        this.f78a.debug("Found credentials");
                    } else {
                        this.f78a.debug("Credentials not found");
                    }
                }
            } else if (c2.e()) {
                this.f78a.debug("Authentication failed");
                d2 = null;
            }
            eVar.a(cVar);
            eVar.a(d2);
        }
    }

    private void xa(m mVar, com.agilebinary.a.a.a.f.k kVar) {
        int a2;
        com.agilebinary.a.a.a.h.c.c b2 = mVar.b();
        boolean z = true;
        int i2 = 0;
        while (z) {
            i2++;
            try {
                if (!this.o.l()) {
                    this.o.a(b2, kVar, this.n);
                } else {
                    this.o.b(com.agilebinary.a.a.a.e.c.a(this.n));
                }
                com.agilebinary.a.a.a.h.c.a aVar = new com.agilebinary.a.a.a.h.c.a();
                do {
                    com.agilebinary.a.a.a.h.c.c c_ = this.o.c_();
                    a2 = aVar.a(b2, c_);
                    switch (a2) {
                        case -1:
                            throw new IllegalStateException("Unable to establish route.\nplanned = " + b2 + "\ncurrent = " + c_);
                        case 0:
                            break;
                        case 1:
                        case 2:
                            this.o.a(b2, kVar, this.n);
                            continue;
                        case 3:
                            a(b2, kVar);
                            this.f78a.debug("Tunnel to target created.");
                            this.o.a(this.n);
                            continue;
                        case 4:
                            throw new UnsupportedOperationException("Proxy chains are not supported.");
                        case 5:
                            this.o.a(kVar, this.n);
                            continue;
                        default:
                            throw new IllegalStateException("Unknown step indicator " + a2 + " from RouteDirector.");
                    }
                } while (a2 > 0);
                z = false;
            } catch (IOException e2) {
                try {
                    this.o.k();
                } catch (IOException e3) {
                }
                if (this.h.a(e2, i2, kVar)) {
                    if (this.f78a.isInfoEnabled()) {
                        this.f78a.info("I/O exception (" + e2.getClass().getName() + ") caught when connecting to the target host: " + e2.getMessage());
                    }
                    if (this.f78a.isDebugEnabled()) {
                        this.f78a.debug(e2.getMessage(), e2);
                    }
                    this.f78a.info("Retrying connect");
                } else {
                    throw e2;
                }
            }
        }
    }

    private void xa(Map map, com.agilebinary.a.a.a.a.e eVar, com.agilebinary.a.a.a.d.b bVar, j jVar, com.agilebinary.a.a.a.f.k kVar) {
        com.agilebinary.a.a.a.a.h c2 = eVar.c();
        if (c2 == null) {
            c2 = bVar.a(map, jVar, kVar);
            eVar.a(c2);
        }
        String b2 = c2.b();
        if (((t) map.get(b2.toLowerCase(Locale.ENGLISH))) == null) {
            throw new com.agilebinary.a.a.a.a.b(b2 + " authorization challenge expected, but not found");
        }
        this.f78a.debug("Authorization challenge processed");
    }

    private boolean xa(com.agilebinary.a.a.a.h.c.c cVar, com.agilebinary.a.a.a.f.k kVar) {
        j jVar;
        com.agilebinary.a.a.a.b g2 = cVar.g();
        com.agilebinary.a.a.a.b a2 = cVar.a();
        boolean z = false;
        j jVar2 = null;
        while (true) {
            if (z) {
                jVar = jVar2;
                break;
            }
            if (!this.o.l()) {
                this.o.a(cVar, kVar, this.n);
            }
            com.agilebinary.a.a.a.b a3 = cVar.a();
            String a4 = a3.a();
            int b2 = a3.b();
            int a5 = b2 < 0 ? this.b.a().a(a3.c()).a() : b2;
            StringBuilder sb = new StringBuilder(a4.length() + 6);
            sb.append(a4);
            sb.append(':');
            sb.append(Integer.toString(a5));
            com.agilebinary.a.a.a.b.n nVar = new com.agilebinary.a.a.a.b.n("CONNECT", sb.toString(), com.agilebinary.a.a.a.e.b.b(this.n));
            nVar.a(this.n);
            kVar.a("http.target_host", a2);
            kVar.a("http.proxy_host", g2);
            kVar.a("http.connection", this.o);
            kVar.a("http.auth.target-scope", this.p);
            kVar.a("http.auth.proxy-scope", this.q);
            kVar.a("http.request", nVar);
            b.a(nVar, this.g, kVar);
            j a6 = b.a(nVar, this.o, kVar);
            a6.a(this.n);
            b.a(a6, this.g, kVar);
            if (a6.a().b() < 200) {
                throw new com.agilebinary.a.a.a.k("Unexpected response to CONNECT request: " + a6.a());
            }
            com.agilebinary.a.a.a.d.e eVar = (com.agilebinary.a.a.a.d.e) kVar.a("http.auth.credentials-provider");
            if (eVar != null && com.agilebinary.a.a.a.d.d.a.a(this.n)) {
                if (this.l.a(a6)) {
                    this.f78a.debug("Proxy requested authentication");
                    try {
                        a(this.l.b(a6), this.q, this.l, a6, kVar);
                    } catch (com.agilebinary.a.a.a.a.b e2) {
                        if (this.f78a.isWarnEnabled()) {
                            this.f78a.warn("Authentication error: " + e2.getMessage());
                            jVar = a6;
                            break;
                        }
                    }
                    a(this.q, g2, eVar);
                    if (this.q.d() == null) {
                        z = true;
                    } else if (this.d.a(a6, kVar)) {
                        this.f78a.debug("Connection kept alive");
                        com.agilebinary.a.a.a.i.b.a(a6.b());
                        z = false;
                        jVar2 = a6;
                    } else {
                        this.o.k();
                        z = false;
                    }
                    jVar2 = a6;
                } else {
                    this.q.a((com.agilebinary.a.a.a.a.c) null);
                }
            }
            z = true;
            jVar2 = a6;
        }
        if (jVar.a().b() > 299) {
            com.agilebinary.a.a.a.c b3 = jVar.b();
            if (b3 != null) {
                jVar.a(new com.agilebinary.a.a.a.g.e(b3));
            }
            this.o.k();
            throw new g("CONNECT refused by proxy: " + jVar.a(), jVar);
        }
        this.o.g();
        return false;
    }

    private j xb(m mVar, com.agilebinary.a.a.a.f.k kVar) {
        l a2 = mVar.a();
        com.agilebinary.a.a.a.h.c.c b2 = mVar.b();
        j jVar = null;
        IOException e2 = null;
        boolean z = true;
        while (z) {
            this.r++;
            a2.m();
            if (!a2.i()) {
                this.f78a.debug("Cannot retry non-repeatable request");
                if (e2 != null) {
                    throw new com.agilebinary.a.a.a.d.j("Cannot retry request with a non-repeatable request entity.  The cause lists the reason the original request failed.", e2);
                }
                throw new com.agilebinary.a.a.a.d.j("Cannot retry request with a non-repeatable request entity.");
            }
            try {
                if (this.f78a.isDebugEnabled()) {
                    this.f78a.debug("Attempt " + this.r + " to execute request");
                }
                jVar = b.a(a2, this.o, kVar);
                z = false;
            } catch (IOException e3) {
                e2 = e3;
                this.f78a.debug("Closing the connection.");
                try {
                    this.o.k();
                } catch (IOException e4) {
                }
                if (this.h.a(e2, a2.l(), kVar)) {
                    if (this.f78a.isInfoEnabled()) {
                        this.f78a.info("I/O exception (" + e2.getClass().getName() + ") caught when processing request: " + e2.getMessage());
                    }
                    if (this.f78a.isDebugEnabled()) {
                        this.f78a.debug(e2.getMessage(), e2);
                    }
                    this.f78a.info("Retrying request");
                    if (!b2.d()) {
                        this.f78a.debug("Reopening the direct connection.");
                        this.o.a(b2, kVar, this.n);
                    } else {
                        this.f78a.debug("Proxied connection. Need to start over.");
                        z = false;
                    }
                } else {
                    throw e2;
                }
            }
        }
        return jVar;
    }

    private void xb() {
        com.agilebinary.a.a.a.h.a aVar = this.o;
        if (aVar != null) {
            this.o = null;
            try {
                aVar.b();
            } catch (IOException e2) {
                if (this.f78a.isDebugEnabled()) {
                    this.f78a.debug(e2.getMessage(), e2);
                }
            }
            try {
                aVar.d_();
            } catch (IOException e3) {
                this.f78a.debug("Error releasing connection", e3);
            }
        }
    }

    public final j a(com.agilebinary.a.a.a.b bVar, com.agilebinary.a.a.a.f fVar, com.agilebinary.a.a.a.f.k kVar) {
        return xa(bVar, fVar, kVar);
    }
}
