package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.b.e;
import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.k.f;
import com.agilebinary.a.a.a.t;
import java.util.ArrayList;
import java.util.List;

public final class w extends ah {

    /* renamed from: a  reason: collision with root package name */
    private static String[] f33a = {"EEE, dd MMM yyyy HH:mm:ss zzz", "EEEE, dd-MMM-yy HH:mm:ss zzz", "EEE MMM d HH:mm:ss yyyy", "EEE, dd-MMM-yyyy HH:mm:ss z", "EEE, dd-MMM-yyyy HH-mm-ss z", "EEE, dd MMM yy HH:mm:ss z", "EEE dd-MMM-yyyy HH:mm:ss z", "EEE dd MMM yyyy HH:mm:ss z", "EEE dd-MMM-yyyy HH-mm-ss z", "EEE dd-MMM-yy HH:mm:ss z", "EEE dd MMM yy HH:mm:ss z", "EEE,dd-MMM-yy HH:mm:ss z", "EEE,dd-MMM-yyyy HH:mm:ss z", "EEE, dd-MM-yyyy HH:mm:ss z"};
    private static final String[] b = {"EEE, dd MMM yyyy HH:mm:ss zzz", "EEEE, dd-MMM-yy HH:mm:ss zzz", "EEE MMM d HH:mm:ss yyyy", "EEE, dd-MMM-yyyy HH:mm:ss z", "EEE, dd-MMM-yyyy HH-mm-ss z", "EEE, dd MMM yy HH:mm:ss z", "EEE dd-MMM-yyyy HH:mm:ss z", "EEE dd MMM yyyy HH:mm:ss z", "EEE dd-MMM-yyyy HH-mm-ss z", "EEE dd-MMM-yy HH:mm:ss z", "EEE dd MMM yy HH:mm:ss z", "EEE,dd-MMM-yy HH:mm:ss z", "EEE,dd-MMM-yyyy HH:mm:ss z", "EEE, dd-MM-yyyy HH:mm:ss z"};
    private final String[] c;

    public w() {
        this(null);
    }

    public w(String[] strArr) {
        if (strArr != null) {
            this.c = (String[]) strArr.clone();
        } else {
            this.c = b;
        }
        a("path", new m());
        a("domain", new r());
        a("max-age", new ac());
        a("secure", new h());
        a("comment", new o());
        a("expires", new j(this.c));
    }

    private final int xa() {
        return 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00ca  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final java.util.List xa(com.agilebinary.a.a.a.t r9, com.agilebinary.a.a.a.k.f r10) {
        /*
            r8 = this;
            r6 = 1
            r4 = -1
            r5 = 0
            if (r9 != 0) goto L_0x000d
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "Header may not be null"
            r1.<init>(r2)
            throw r1
        L_0x000d:
            if (r10 != 0) goto L_0x0017
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "Cookie origin may not be null"
            r1.<init>(r2)
            throw r1
        L_0x0017:
            java.lang.String r1 = r9.a()
            java.lang.String r2 = r9.b()
            java.lang.String r3 = "Set-Cookie"
            boolean r1 = r1.equalsIgnoreCase(r3)
            if (r1 != 0) goto L_0x004a
            com.agilebinary.a.a.a.k.e r1 = new com.agilebinary.a.a.a.k.e
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Unrecognized cookie header '"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = r9.toString()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "'"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2)
            throw r1
        L_0x004a:
            java.util.Locale r1 = java.util.Locale.ENGLISH
            java.lang.String r1 = r2.toLowerCase(r1)
            java.lang.String r3 = "expires="
            int r1 = r1.indexOf(r3)
            if (r1 == r4) goto L_0x00a1
            java.lang.String r3 = "expires="
            int r3 = r3.length()
            int r1 = r1 + r3
            r3 = 59
            int r3 = r2.indexOf(r3, r1)
            if (r3 != r4) goto L_0x006b
            int r3 = r2.length()
        L_0x006b:
            java.lang.String r1 = r2.substring(r1, r3)     // Catch:{ k -> 0x00a0 }
            java.lang.String[] r2 = r8.c     // Catch:{ k -> 0x00a0 }
            com.agilebinary.a.a.a.c.a.l.a(r1, r2)     // Catch:{ k -> 0x00a0 }
            r1 = r6
        L_0x0075:
            if (r1 == 0) goto L_0x00ca
            boolean r1 = r9 instanceof com.agilebinary.a.a.a.r
            if (r1 == 0) goto L_0x00a3
            r0 = r9
            com.agilebinary.a.a.a.r r0 = (com.agilebinary.a.a.a.r) r0
            r1 = r0
            com.agilebinary.a.a.a.i.c r1 = r1.e()
            com.agilebinary.a.a.a.b.i r2 = new com.agilebinary.a.a.a.b.i
            com.agilebinary.a.a.a.r r9 = (com.agilebinary.a.a.a.r) r9
            int r3 = r9.d()
            int r4 = r1.c()
            r2.<init>(r3, r4)
        L_0x0092:
            com.agilebinary.a.a.a.m[] r3 = new com.agilebinary.a.a.a.m[r6]
            com.agilebinary.a.a.a.m r1 = com.agilebinary.a.a.a.c.a.aa.a(r1, r2)
            r3[r5] = r1
            r1 = r3
        L_0x009b:
            java.util.List r1 = r8.a(r1, r10)
            return r1
        L_0x00a0:
            r1 = move-exception
        L_0x00a1:
            r1 = r5
            goto L_0x0075
        L_0x00a3:
            java.lang.String r1 = r9.b()
            if (r1 != 0) goto L_0x00b1
            com.agilebinary.a.a.a.k.e r1 = new com.agilebinary.a.a.a.k.e
            java.lang.String r2 = "Header value is null"
            r1.<init>(r2)
            throw r1
        L_0x00b1:
            com.agilebinary.a.a.a.i.c r2 = new com.agilebinary.a.a.a.i.c
            int r3 = r1.length()
            r2.<init>(r3)
            r2.a(r1)
            com.agilebinary.a.a.a.b.i r1 = new com.agilebinary.a.a.a.b.i
            int r3 = r2.c()
            r1.<init>(r5, r3)
            r7 = r2
            r2 = r1
            r1 = r7
            goto L_0x0092
        L_0x00ca:
            com.agilebinary.a.a.a.m[] r1 = r9.c()
            goto L_0x009b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.c.a.w.xa(com.agilebinary.a.a.a.t, com.agilebinary.a.a.a.k.f):java.util.List");
    }

    private final List xa(List list) {
        if (list == null) {
            throw new IllegalArgumentException("List of cookies may not be null");
        } else if (list.isEmpty()) {
            throw new IllegalArgumentException("List of cookies may not be empty");
        } else {
            c cVar = new c(list.size() * 20);
            cVar.a("Cookie");
            cVar.a(": ");
            for (int i = 0; i < list.size(); i++) {
                com.agilebinary.a.a.a.k.c cVar2 = (com.agilebinary.a.a.a.k.c) list.get(i);
                if (i > 0) {
                    cVar.a("; ");
                }
                cVar.a(cVar2.a());
                cVar.a("=");
                String b2 = cVar2.b();
                if (b2 != null) {
                    cVar.a(b2);
                }
            }
            ArrayList arrayList = new ArrayList(1);
            arrayList.add(new e(cVar));
            return arrayList;
        }
    }

    private final t xb() {
        return null;
    }

    private final String xtoString() {
        return "compatibility";
    }

    public final int a() {
        return xa();
    }

    public final List a(t tVar, f fVar) {
        return xa(tVar, fVar);
    }

    public final List a(List list) {
        return xa(list);
    }

    public final t b() {
        return xb();
    }

    public final String toString() {
        return xtoString();
    }
}
