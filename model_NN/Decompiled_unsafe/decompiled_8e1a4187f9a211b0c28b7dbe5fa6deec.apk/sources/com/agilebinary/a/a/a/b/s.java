package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.a;
import com.agilebinary.a.a.a.aa;
import com.agilebinary.a.a.a.f.d;
import com.agilebinary.a.a.a.h;
import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.t;
import com.agilebinary.a.a.a.u;

public final class s implements t {

    /* renamed from: a  reason: collision with root package name */
    public static final s f21a = new s();
    private a b;

    public s() {
        this((byte) 0);
    }

    private s(byte b2) {
        this.b = aa.d;
    }

    private a c(c cVar, i iVar) {
        return xc(cVar, iVar);
    }

    private static void d(c cVar, i iVar) {
        xd(cVar, iVar);
    }

    private final t xa(c cVar) {
        return new e(cVar);
    }

    private final boolean xa(c cVar, i iVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Char array buffer may not be null");
        } else if (iVar == null) {
            throw new IllegalArgumentException("Parser cursor may not be null");
        } else {
            int b2 = iVar.b();
            String a2 = this.b.a();
            int length = a2.length();
            if (cVar.c() < length + 4) {
                return false;
            }
            if (b2 < 0) {
                b2 = (cVar.c() - 4) - length;
            } else if (b2 == 0) {
                while (b2 < cVar.c() && d.a(cVar.a(b2))) {
                    b2++;
                }
            }
            if (b2 + length + 4 > cVar.c()) {
                return false;
            }
            int i = 0;
            boolean z = true;
            while (z && i < length) {
                z = cVar.a(b2 + i) == a2.charAt(i);
                i++;
            }
            return z ? cVar.a(b2 + length) == '/' : z;
        }
    }

    private final h xb(c cVar, i iVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Char array buffer may not be null");
        } else if (iVar == null) {
            throw new IllegalArgumentException("Parser cursor may not be null");
        } else {
            int b2 = iVar.b();
            int a2 = iVar.a();
            try {
                a c = c(cVar, iVar);
                d(cVar, iVar);
                int b3 = iVar.b();
                int a3 = cVar.a(32, b3, a2);
                if (a3 < 0) {
                    a3 = a2;
                }
                String b4 = cVar.b(b3, a3);
                for (int i = 0; i < b4.length(); i++) {
                    if (!Character.isDigit(b4.charAt(i))) {
                        throw new u("Status line contains invalid status code: " + cVar.a(b2, a2));
                    }
                }
                return new m(c, Integer.parseInt(b4), a3 < a2 ? cVar.b(a3, a2) : "");
            } catch (NumberFormatException e) {
                throw new u("Status line contains invalid status code: " + cVar.a(b2, a2));
            } catch (IndexOutOfBoundsException e2) {
                throw new u("Invalid status line: " + cVar.a(b2, a2));
            }
        }
    }

    private a xc(c cVar, i iVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Char array buffer may not be null");
        } else if (iVar == null) {
            throw new IllegalArgumentException("Parser cursor may not be null");
        } else {
            String a2 = this.b.a();
            int length = a2.length();
            int b2 = iVar.b();
            int a3 = iVar.a();
            d(cVar, iVar);
            int b3 = iVar.b();
            if (b3 + length + 4 > a3) {
                throw new u("Not a valid protocol version: " + cVar.a(b2, a3));
            }
            int i = 0;
            boolean z = true;
            while (z && i < length) {
                z = cVar.a(b3 + i) == a2.charAt(i);
                i++;
            }
            if (!(z ? cVar.a(b3 + length) == '/' : z)) {
                throw new u("Not a valid protocol version: " + cVar.a(b2, a3));
            }
            int i2 = length + 1 + b3;
            int a4 = cVar.a(46, i2, a3);
            if (a4 == -1) {
                throw new u("Invalid protocol version number: " + cVar.a(b2, a3));
            }
            try {
                int parseInt = Integer.parseInt(cVar.b(i2, a4));
                int i3 = a4 + 1;
                int a5 = cVar.a(32, i3, a3);
                if (a5 == -1) {
                    a5 = a3;
                }
                try {
                    int parseInt2 = Integer.parseInt(cVar.b(i3, a5));
                    iVar.a(a5);
                    return this.b.a(parseInt, parseInt2);
                } catch (NumberFormatException e) {
                    throw new u("Invalid protocol minor version number: " + cVar.a(b2, a3));
                }
            } catch (NumberFormatException e2) {
                throw new u("Invalid protocol major version number: " + cVar.a(b2, a3));
            }
        }
    }

    private static void xd(c cVar, i iVar) {
        int b2 = iVar.b();
        int a2 = iVar.a();
        while (b2 < a2 && d.a(cVar.a(b2))) {
            b2++;
        }
        iVar.a(b2);
    }

    public final t a(c cVar) {
        return xa(cVar);
    }

    public final boolean a(c cVar, i iVar) {
        return xa(cVar, iVar);
    }

    public final h b(c cVar, i iVar) {
        return xb(cVar, iVar);
    }
}
