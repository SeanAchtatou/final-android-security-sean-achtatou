package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.d.c;
import com.agilebinary.a.a.a.d.c.b;
import com.agilebinary.a.a.a.d.c.d;
import com.agilebinary.a.a.a.d.m;
import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.f;
import com.agilebinary.a.a.a.f.k;
import com.agilebinary.a.a.a.j;
import com.agilebinary.a.a.a.l;
import com.agilebinary.a.a.a.t;
import com.agilebinary.a.a.b.a.a;
import java.net.URI;
import java.net.URISyntaxException;
import org.apache.commons.logging.Log;

public final class p implements c {

    /* renamed from: a  reason: collision with root package name */
    private final Log f75a = a.a(getClass());

    private static URI a(String str) {
        return xa(str);
    }

    private URI b(f fVar, j jVar, k kVar) {
        return xb(fVar, jVar, kVar);
    }

    private final b xa(f fVar, j jVar, k kVar) {
        URI b = b(fVar, jVar, kVar);
        return fVar.a().a().equalsIgnoreCase("HEAD") ? new d(b) : new com.agilebinary.a.a.a.d.c.a(b);
    }

    private static URI xa(String str) {
        try {
            return new URI(str);
        } catch (URISyntaxException e) {
            throw new l("Invalid redirect URI: " + str, e);
        }
    }

    private final boolean xa(f fVar, j jVar) {
        if (jVar == null) {
            throw new IllegalArgumentException("HTTP response may not be null");
        }
        int b = jVar.a().b();
        String a2 = fVar.a().a();
        t c = jVar.c("location");
        switch (b) {
            case 301:
            case 307:
                return a2.equalsIgnoreCase("GET") || a2.equalsIgnoreCase("HEAD");
            case 302:
                return (a2.equalsIgnoreCase("GET") || a2.equalsIgnoreCase("HEAD")) && c != null;
            case 303:
                return true;
            case 304:
            case 305:
            case 306:
            default:
                return false;
        }
    }

    private URI xb(f fVar, j jVar, k kVar) {
        r rVar;
        URI uri;
        if (jVar == null) {
            throw new IllegalArgumentException("HTTP response may not be null");
        }
        t c = jVar.c("location");
        if (c == null) {
            throw new l("Received redirect response " + jVar.a() + " but no location header");
        }
        String b = c.b();
        if (this.f75a.isDebugEnabled()) {
            this.f75a.debug("Redirect requested to location '" + b + "'");
        }
        URI a2 = a(b);
        e g = jVar.g();
        if (!a2.isAbsolute()) {
            if (g.c("http.protocol.reject-relative-redirect")) {
                throw new l("Relative redirect location '" + a2 + "' not allowed");
            }
            com.agilebinary.a.a.a.b bVar = (com.agilebinary.a.a.a.b) kVar.a("http.target_host");
            if (bVar == null) {
                throw new IllegalStateException("Target host not available in the HTTP context");
            }
            try {
                a2 = com.agilebinary.a.a.a.d.a.b.a(com.agilebinary.a.a.a.d.a.b.a(new URI(fVar.a().c()), bVar, true), a2);
            } catch (URISyntaxException e) {
                throw new l(e.getMessage(), e);
            }
        }
        if (g.d("http.protocol.allow-circular-redirects")) {
            r rVar2 = (r) kVar.a("http.protocol.redirect-locations");
            if (rVar2 == null) {
                rVar = new r();
                kVar.a("http.protocol.redirect-locations", rVar);
            } else {
                rVar = rVar2;
            }
            if (a2.getFragment() != null) {
                try {
                    uri = com.agilebinary.a.a.a.d.a.b.a(a2, new com.agilebinary.a.a.a.b(a2.getHost(), a2.getPort(), a2.getScheme()), true);
                } catch (URISyntaxException e2) {
                    throw new l(e2.getMessage(), e2);
                }
            } else {
                uri = a2;
            }
            if (rVar.a(uri)) {
                throw new m("Circular redirect to '" + uri + "'");
            }
            rVar.b(uri);
        }
        return a2;
    }

    public final b a(f fVar, j jVar, k kVar) {
        return xa(fVar, jVar, kVar);
    }

    public final boolean a(f fVar, j jVar) {
        return xa(fVar, jVar);
    }
}
