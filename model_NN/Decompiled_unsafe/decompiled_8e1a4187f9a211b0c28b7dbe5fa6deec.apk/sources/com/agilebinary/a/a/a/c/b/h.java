package com.agilebinary.a.a.a.c.b;

import com.agilebinary.a.a.a.b;
import com.agilebinary.a.a.a.c.f;
import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.f.k;
import com.agilebinary.a.a.a.h.g;
import com.agilebinary.a.a.a.i;
import com.agilebinary.a.a.a.j;
import com.agilebinary.a.a.a.j.d;
import com.agilebinary.a.a.a.t;
import com.agilebinary.a.a.b.a.a;
import java.io.IOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.logging.Log;

public final class h extends f implements k, g {

    /* renamed from: a  reason: collision with root package name */
    private final Log f47a = a.a(getClass());
    private final Log b = a.a("org.apache.http.headers");
    private final Log c = a.a("org.apache.http.wire");
    private volatile Socket d;
    private b e;
    private boolean f;
    private volatile boolean g;
    private final Map h = new HashMap();

    private final com.agilebinary.a.a.a.j.a xa(Socket socket, int i, e eVar) {
        com.agilebinary.a.a.a.j.a a2 = super.a(socket, i == -1 ? 8192 : i, eVar);
        return this.c.isDebugEnabled() ? new k(a2, new q(this.c), com.agilebinary.a.a.a.e.b.a(eVar)) : a2;
    }

    private final d xa(com.agilebinary.a.a.a.j.a aVar, i iVar, e eVar) {
        return new p(aVar, iVar, eVar);
    }

    private final Object xa(String str) {
        return this.h.get(str);
    }

    private final void xa(com.agilebinary.a.a.a.f fVar) {
        if (this.f47a.isDebugEnabled()) {
            this.f47a.debug("Sending request: " + fVar.a());
        }
        super.a(fVar);
        if (this.b.isDebugEnabled()) {
            this.b.debug(">> " + fVar.a().toString());
            t[] e2 = fVar.e();
            int length = e2.length;
            for (int i = 0; i < length; i++) {
                this.b.debug(">> " + e2[i].toString());
            }
        }
    }

    private final void xa(String str, Object obj) {
        this.h.put(str, obj);
    }

    private final void xa(Socket socket, b bVar) {
        f();
        this.d = socket;
        this.e = bVar;
        if (this.g) {
            socket.close();
            throw new IOException("Connection already shutdown");
        }
    }

    private final void xa(Socket socket, b bVar, boolean z, e eVar) {
        a();
        if (bVar == null) {
            throw new IllegalArgumentException("Target host must not be null.");
        } else if (eVar == null) {
            throw new IllegalArgumentException("Parameters must not be null.");
        } else {
            if (socket != null) {
                this.d = socket;
                a(socket, eVar);
            }
            this.e = bVar;
            this.f = z;
        }
    }

    private final void xa(boolean z, e eVar) {
        f();
        if (eVar == null) {
            throw new IllegalArgumentException("Parameters must not be null.");
        }
        this.f = z;
        a(this.d, eVar);
    }

    private final com.agilebinary.a.a.a.j.e xb(Socket socket, int i, e eVar) {
        com.agilebinary.a.a.a.j.e b2 = super.b(socket, i == -1 ? 8192 : i, eVar);
        return this.c.isDebugEnabled() ? new j(b2, new q(this.c), com.agilebinary.a.a.a.e.b.a(eVar)) : b2;
    }

    private final j xd() {
        j d2 = super.d();
        if (this.f47a.isDebugEnabled()) {
            this.f47a.debug("Receiving response: " + d2.a());
        }
        if (this.b.isDebugEnabled()) {
            this.b.debug("<< " + d2.a().toString());
            t[] e2 = d2.e();
            int length = e2.length;
            for (int i = 0; i < length; i++) {
                this.b.debug("<< " + e2[i].toString());
            }
        }
        return d2;
    }

    private final boolean xh_() {
        return this.f;
    }

    private final Socket xi_() {
        return this.d;
    }

    private final void xk() {
        try {
            super.k();
            this.f47a.debug("Connection closed");
        } catch (IOException e2) {
            this.f47a.debug("I/O error closing connection", e2);
        }
    }

    private final void xm() {
        this.g = true;
        try {
            super.m();
            this.f47a.debug("Connection shut down");
            Socket socket = this.d;
            if (socket != null) {
                socket.close();
            }
        } catch (IOException e2) {
            this.f47a.debug("I/O error shutting down connection", e2);
        }
    }

    /* access modifiers changed from: protected */
    public final com.agilebinary.a.a.a.j.a a(Socket socket, int i, e eVar) {
        return xa(socket, i, eVar);
    }

    /* access modifiers changed from: protected */
    public final d a(com.agilebinary.a.a.a.j.a aVar, i iVar, e eVar) {
        return xa(aVar, iVar, eVar);
    }

    public final Object a(String str) {
        return xa(str);
    }

    public final void a(com.agilebinary.a.a.a.f fVar) {
        xa(fVar);
    }

    public final void a(String str, Object obj) {
        xa(str, obj);
    }

    public final void a(Socket socket, b bVar) {
        xa(socket, bVar);
    }

    public final void a(Socket socket, b bVar, boolean z, e eVar) {
        xa(socket, bVar, z, eVar);
    }

    public final void a(boolean z, e eVar) {
        xa(z, eVar);
    }

    /* access modifiers changed from: protected */
    public final com.agilebinary.a.a.a.j.e b(Socket socket, int i, e eVar) {
        return xb(socket, i, eVar);
    }

    public final j d() {
        return xd();
    }

    public final boolean h_() {
        return xh_();
    }

    public final Socket i_() {
        return xi_();
    }

    public final void k() {
        xk();
    }

    public final void m() {
        xm();
    }
}
