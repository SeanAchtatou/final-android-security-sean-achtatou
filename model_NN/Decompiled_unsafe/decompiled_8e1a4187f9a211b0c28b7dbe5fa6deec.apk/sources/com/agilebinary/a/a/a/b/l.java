package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.e.a;
import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.t;
import com.agilebinary.a.a.a.w;
import com.agilebinary.a.a.a.x;

public abstract class l implements x {

    /* renamed from: a  reason: collision with root package name */
    protected b f15a;
    protected e b;

    protected l() {
        this((byte) 0);
    }

    private l(byte b2) {
        this.f15a = new b();
        this.b = null;
    }

    private final void xa(e eVar) {
        if (eVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        }
        this.b = eVar;
    }

    private final void xa(t tVar) {
        this.f15a.a(tVar);
    }

    private final void xa(String str, String str2) {
        if (str == null) {
            throw new IllegalArgumentException("Header name may not be null");
        }
        this.f15a.a(new h(str, str2));
    }

    private final void xa(t[] tVarArr) {
        this.f15a.a(tVarArr);
    }

    private final boolean xa(String str) {
        return this.f15a.c(str);
    }

    private final void xb(String str, String str2) {
        if (str == null) {
            throw new IllegalArgumentException("Header name may not be null");
        }
        this.f15a.b(new h(str, str2));
    }

    private final t[] xb(String str) {
        return this.f15a.a(str);
    }

    private final t xc(String str) {
        return this.f15a.b(str);
    }

    private final w xd(String str) {
        return this.f15a.d(str);
    }

    private final t[] xe() {
        return this.f15a.b();
    }

    private final w xf() {
        return this.f15a.c();
    }

    private final e xg() {
        if (this.b == null) {
            this.b = new a();
        }
        return this.b;
    }

    public final void a(e eVar) {
        xa(eVar);
    }

    public final void a(t tVar) {
        xa(tVar);
    }

    public final void a(String str, String str2) {
        xa(str, str2);
    }

    public final void a(t[] tVarArr) {
        xa(tVarArr);
    }

    public final boolean a(String str) {
        return xa(str);
    }

    public final void b(String str, String str2) {
        xb(str, str2);
    }

    public final t[] b(String str) {
        return xb(str);
    }

    public final t c(String str) {
        return xc(str);
    }

    public final w d(String str) {
        return xd(str);
    }

    public final t[] e() {
        return xe();
    }

    public final w f() {
        return xf();
    }

    public final e g() {
        return xg();
    }
}
