package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.t;
import com.agilebinary.a.a.a.w;
import java.util.List;
import java.util.NoSuchElementException;

public final class g implements w {

    /* renamed from: a  reason: collision with root package name */
    private List f12a;
    private int b;
    private int c;
    private String d;

    public g(List list, String str) {
        if (list == null) {
            throw new IllegalArgumentException("Header list must not be null.");
        }
        this.f12a = list;
        this.d = str;
        this.b = a(-1);
        this.c = -1;
    }

    private int a(int i) {
        return xa(i);
    }

    private int xa(int i) {
        if (i < -1) {
            return -1;
        }
        int size = this.f12a.size() - 1;
        boolean z = false;
        int i2 = i;
        while (!z && i2 < size) {
            i2++;
            if (this.d == null) {
                z = true;
            } else {
                z = this.d.equalsIgnoreCase(((t) this.f12a.get(i2)).a());
            }
        }
        if (z) {
            return i2;
        }
        return -1;
    }

    private final t xa() {
        int i = this.b;
        if (i < 0) {
            throw new NoSuchElementException("Iteration already finished.");
        }
        this.c = i;
        this.b = a(i);
        return (t) this.f12a.get(i);
    }

    private final boolean xhasNext() {
        return this.b >= 0;
    }

    private final Object xnext() {
        return a();
    }

    private final void xremove() {
        if (this.c < 0) {
            throw new IllegalStateException("No header to remove.");
        }
        this.f12a.remove(this.c);
        this.c = -1;
        this.b--;
    }

    public final t a() {
        return xa();
    }

    public final boolean hasNext() {
        return xhasNext();
    }

    public final Object next() {
        return xnext();
    }

    public final void remove() {
        xremove();
    }
}
