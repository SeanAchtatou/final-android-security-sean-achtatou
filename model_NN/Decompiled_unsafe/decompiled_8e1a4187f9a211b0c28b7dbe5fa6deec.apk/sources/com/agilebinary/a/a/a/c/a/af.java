package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.k.h;
import java.io.Serializable;
import java.util.Date;

public final class af extends d implements h, Serializable {

    /* renamed from: a  reason: collision with root package name */
    private String f25a;
    private int[] b;
    private boolean c;

    public af(String str, String str2) {
        super(str, str2);
    }

    private final void xa(int[] iArr) {
        this.b = iArr;
    }

    private final void xa_(String str) {
        this.f25a = str;
    }

    private final boolean xb(Date date) {
        return this.c || super.b(date);
    }

    private final Object xclone() {
        af afVar = (af) super.clone();
        afVar.b = (int[]) this.b.clone();
        return afVar;
    }

    private final int[] xg() {
        return this.b;
    }

    private final void xi() {
        this.c = true;
    }

    public final void a(int[] iArr) {
        xa(iArr);
    }

    public final void a_(String str) {
        xa_(str);
    }

    public final boolean b(Date date) {
        return xb(date);
    }

    public final Object clone() {
        return xclone();
    }

    public final int[] g() {
        return xg();
    }

    public final void i() {
        xi();
    }
}
