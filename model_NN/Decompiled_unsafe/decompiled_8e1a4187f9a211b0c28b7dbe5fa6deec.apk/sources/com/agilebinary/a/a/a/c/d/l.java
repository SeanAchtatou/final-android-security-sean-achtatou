package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.a;
import com.agilebinary.a.a.a.b.q;
import com.agilebinary.a.a.a.d;
import com.agilebinary.a.a.a.d.c.b;
import com.agilebinary.a.a.a.f;
import java.net.URI;
import java.net.URISyntaxException;

public class l extends com.agilebinary.a.a.a.b.l implements b {
    private final f c;
    private URI d;
    private String e;
    private a f;
    private int g;

    public l(f fVar) {
        if (fVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        }
        this.c = fVar;
        a(fVar.g());
        a(fVar.e());
        if (fVar instanceof b) {
            this.d = ((b) fVar).e_();
            this.e = ((b) fVar).b();
            this.f = null;
        } else {
            d a2 = fVar.a();
            try {
                this.d = new URI(a2.c());
                this.e = a2.a();
                this.f = fVar.c();
            } catch (URISyntaxException e2) {
                throw new com.agilebinary.a.a.a.l("Invalid request URI: " + a2.c(), e2);
            }
        }
        this.g = 0;
    }

    private final d xa() {
        String str = this.e;
        a c2 = c();
        String str2 = null;
        if (this.d != null) {
            str2 = this.d.toASCIIString();
        }
        if (str2 == null || str2.length() == 0) {
            str2 = "/";
        }
        return new q(str, str2, c2);
    }

    private final void xa(URI uri) {
        this.d = uri;
    }

    private final String xb() {
        return this.e;
    }

    private final a xc() {
        if (this.f == null) {
            this.f = com.agilebinary.a.a.a.e.b.b(g());
        }
        return this.f;
    }

    private final void xd() {
        throw new UnsupportedOperationException();
    }

    private final URI xe_() {
        return this.d;
    }

    private boolean xi() {
        return true;
    }

    private final void xj() {
        this.f15a.a();
        a(this.c.e());
    }

    private final f xk() {
        return this.c;
    }

    private final int xl() {
        return this.g;
    }

    private final void xm() {
        this.g++;
    }

    public final d a() {
        return xa();
    }

    public final void a(URI uri) {
        xa(uri);
    }

    public final String b() {
        return xb();
    }

    public final a c() {
        return xc();
    }

    public final void d() {
        xd();
    }

    public final URI e_() {
        return xe_();
    }

    public boolean i() {
        return xi();
    }

    public final void j() {
        xj();
    }

    public final f k() {
        return xk();
    }

    public final int l() {
        return xl();
    }

    public final void m() {
        xm();
    }
}
