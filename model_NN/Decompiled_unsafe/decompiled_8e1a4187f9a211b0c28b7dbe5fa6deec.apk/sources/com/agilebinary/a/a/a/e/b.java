package com.agilebinary.a.a.a.e;

import com.agilebinary.a.a.a.a;
import com.agilebinary.a.a.a.aa;

public final class b {
    private b() {
    }

    public static String a(e eVar) {
        return xa(eVar);
    }

    public static a b(e eVar) {
        return xb(eVar);
    }

    private static String xa(e eVar) {
        if (eVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        }
        String str = (String) eVar.a("http.protocol.element-charset");
        return str == null ? "US-ASCII" : str;
    }

    private static a xb(e eVar) {
        if (eVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        }
        Object a2 = eVar.a("http.protocol.version");
        return a2 == null ? aa.d : (a) a2;
    }
}
