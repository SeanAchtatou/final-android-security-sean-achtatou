package com.agilebinary.a.a.a.a;

import com.agilebinary.a.a.a.i.f;
import java.util.Locale;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private static String f4a = null;
    private static String b = null;
    private static String c = null;
    private static c d = new c(f4a, -1, b, c);
    private final String e;
    private final String f;
    private final String g;
    private final int h;

    public c(String str, int i, String str2) {
        this(str, i, str2, c);
    }

    public c(String str, int i, String str2, String str3) {
        this.g = str == null ? f4a : str.toLowerCase(Locale.ENGLISH);
        this.h = i < 0 ? -1 : i;
        this.f = str2 == null ? b : str2;
        this.e = str3 == null ? c : str3.toUpperCase(Locale.ENGLISH);
    }

    private final int xa(c cVar) {
        int i = 0;
        if (f.a(this.e, cVar.e)) {
            i = 0 + 1;
        } else if (!(this.e == c || cVar.e == c)) {
            return -1;
        }
        if (f.a(this.f, cVar.f)) {
            i += 2;
        } else if (!(this.f == b || cVar.f == b)) {
            return -1;
        }
        if (this.h == cVar.h) {
            i += 4;
        } else if (!(this.h == -1 || cVar.h == -1)) {
            return -1;
        }
        if (f.a(this.g, cVar.g)) {
            return i + 8;
        }
        if (this.g == f4a || cVar.g == f4a) {
            return i;
        }
        return -1;
    }

    private final boolean xequals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof c)) {
            return super.equals(obj);
        }
        c cVar = (c) obj;
        return f.a(this.g, cVar.g) && this.h == cVar.h && f.a(this.f, cVar.f) && f.a(this.e, cVar.e);
    }

    private final int xhashCode() {
        return f.a(f.a((f.a(17, this.g) * 37) + this.h, this.f), this.e);
    }

    private final String xtoString() {
        StringBuilder sb = new StringBuilder();
        if (this.e != null) {
            sb.append(this.e.toUpperCase(Locale.ENGLISH));
            sb.append(' ');
        }
        if (this.f != null) {
            sb.append('\'');
            sb.append(this.f);
            sb.append('\'');
        } else {
            sb.append("<any realm>");
        }
        if (this.g != null) {
            sb.append('@');
            sb.append(this.g);
            if (this.h >= 0) {
                sb.append(':');
                sb.append(this.h);
            }
        }
        return sb.toString();
    }

    public final int a(c cVar) {
        return xa(cVar);
    }

    public final boolean equals(Object obj) {
        return xequals(obj);
    }

    public final int hashCode() {
        return xhashCode();
    }

    public final String toString() {
        return xtoString();
    }
}
