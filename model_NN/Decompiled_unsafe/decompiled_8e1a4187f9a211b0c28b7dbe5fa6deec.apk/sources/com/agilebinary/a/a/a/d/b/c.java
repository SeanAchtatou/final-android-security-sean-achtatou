package com.agilebinary.a.a.a.d.b;

import com.agilebinary.a.a.a.ad;
import com.agilebinary.a.a.a.d.d;
import com.agilebinary.a.a.a.f.k;
import com.agilebinary.a.a.a.k.e;
import com.agilebinary.a.a.a.k.f;
import com.agilebinary.a.a.a.k.j;
import com.agilebinary.a.a.a.t;
import com.agilebinary.a.a.a.w;
import com.agilebinary.a.a.b.a.a;
import org.apache.commons.logging.Log;

public final class c implements ad {

    /* renamed from: a  reason: collision with root package name */
    private final Log f86a = a.a(getClass());

    private void a(w wVar, j jVar, f fVar, d dVar) {
        xa(wVar, jVar, fVar, dVar);
    }

    private final void xa(com.agilebinary.a.a.a.j jVar, k kVar) {
        if (jVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        } else if (kVar == null) {
            throw new IllegalArgumentException("HTTP context may not be null");
        } else {
            j jVar2 = (j) kVar.a("http.cookie-spec");
            if (jVar2 != null) {
                d dVar = (d) kVar.a("http.cookie-store");
                if (dVar == null) {
                    this.f86a.info("CookieStore not available in HTTP context");
                    return;
                }
                f fVar = (f) kVar.a("http.cookie-origin");
                if (fVar == null) {
                    this.f86a.info("CookieOrigin not available in HTTP context");
                    return;
                }
                a(jVar.d("Set-Cookie"), jVar2, fVar, dVar);
                if (jVar2.a() > 0) {
                    a(jVar.d("Set-Cookie2"), jVar2, fVar, dVar);
                }
            }
        }
    }

    private void xa(w wVar, j jVar, f fVar, d dVar) {
        while (wVar.hasNext()) {
            t a2 = wVar.a();
            try {
                for (com.agilebinary.a.a.a.k.c cVar : jVar.a(a2, fVar)) {
                    try {
                        jVar.a(cVar, fVar);
                        dVar.a(cVar);
                        if (this.f86a.isDebugEnabled()) {
                            this.f86a.debug("Cookie accepted: \"" + cVar + "\". ");
                        }
                    } catch (e e) {
                        if (this.f86a.isWarnEnabled()) {
                            this.f86a.warn("Cookie rejected: \"" + cVar + "\". " + e.getMessage());
                        }
                    }
                }
            } catch (e e2) {
                if (this.f86a.isWarnEnabled()) {
                    this.f86a.warn("Invalid cookie header: \"" + a2 + "\". " + e2.getMessage());
                }
            }
        }
    }

    public final void a(com.agilebinary.a.a.a.j jVar, k kVar) {
        xa(jVar, kVar);
    }
}
