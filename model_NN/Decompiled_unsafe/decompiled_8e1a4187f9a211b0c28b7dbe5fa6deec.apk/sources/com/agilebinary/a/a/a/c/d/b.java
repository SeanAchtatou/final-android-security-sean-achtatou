package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.f.k;
import com.agilebinary.a.a.a.j;
import java.util.List;
import java.util.Map;

public final class b extends t {
    private final List xa(j jVar, k kVar) {
        List list = (List) jVar.g().a("http.auth.proxy-scheme-pref");
        return list != null ? list : super.a(jVar, kVar);
    }

    private final boolean xa(j jVar) {
        if (jVar != null) {
            return jVar.a().b() == 407;
        }
        throw new IllegalArgumentException("HTTP response may not be null");
    }

    private final Map xb(j jVar) {
        if (jVar != null) {
            return a(jVar.b("Proxy-Authenticate"));
        }
        throw new IllegalArgumentException("HTTP response may not be null");
    }

    /* access modifiers changed from: protected */
    public final List a(j jVar, k kVar) {
        return xa(jVar, kVar);
    }

    public final boolean a(j jVar) {
        return xa(jVar);
    }

    public final Map b(j jVar) {
        return xb(jVar);
    }
}
