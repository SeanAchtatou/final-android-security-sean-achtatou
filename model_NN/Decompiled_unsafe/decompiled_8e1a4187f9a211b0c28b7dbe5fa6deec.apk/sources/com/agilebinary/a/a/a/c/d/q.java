package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.e.d;
import com.agilebinary.a.a.a.e.e;

public final class q extends d {

    /* renamed from: a  reason: collision with root package name */
    private e f76a = null;
    private e b;
    private e c;
    private e d;

    public q(e eVar, e eVar2) {
        this.b = eVar;
        this.c = eVar2;
        this.d = null;
    }

    private final e xa(String str, Object obj) {
        throw new UnsupportedOperationException("Setting parameters in a stack is not supported.");
    }

    private final Object xa(String str) {
        if (str == null) {
            throw new IllegalArgumentException("Parameter name must not be null.");
        }
        Object obj = null;
        if (this.d != null) {
            obj = this.d.a(str);
        }
        if (obj == null && this.c != null) {
            obj = this.c.a(str);
        }
        if (obj == null && this.b != null) {
            obj = this.b.a(str);
        }
        return (obj != null || this.f76a == null) ? obj : this.f76a.a(str);
    }

    public final e a(String str, Object obj) {
        return xa(str, obj);
    }

    public final Object a(String str) {
        return xa(str);
    }
}
