package com.agilebinary.a.a.a.h;

import com.agilebinary.a.a.a.c;
import com.agilebinary.a.a.a.g.b;
import java.io.InputStream;
import java.io.OutputStream;

public final class j extends b implements d, e {
    private a b;
    private boolean c;

    public j(c cVar, a aVar, boolean z) {
        super(cVar);
        if (aVar == null) {
            throw new IllegalArgumentException("Connection may not be null.");
        }
        this.b = aVar;
        this.c = z;
    }

    private void h() {
        xh();
    }

    private void i() {
        xi();
    }

    private final void xa(OutputStream outputStream) {
        super.a(outputStream);
        h();
    }

    private final boolean xa() {
        return false;
    }

    /* JADX INFO: finally extract failed */
    private final boolean xa(InputStream inputStream) {
        try {
            if (this.c && this.b != null) {
                inputStream.close();
                this.b.g();
            }
            i();
            return false;
        } catch (Throwable th) {
            i();
            throw th;
        }
    }

    private final void xb() {
        if (this.b != null) {
            try {
                this.b.b();
            } finally {
                this.b = null;
            }
        }
    }

    /* JADX INFO: finally extract failed */
    private final boolean xb(InputStream inputStream) {
        try {
            if (this.c && this.b != null) {
                inputStream.close();
                this.b.g();
            }
            i();
            return false;
        } catch (Throwable th) {
            i();
            throw th;
        }
    }

    private final void xd_() {
        h();
    }

    private final InputStream xf() {
        return new c(this.f94a.f(), this);
    }

    private void xh() {
        if (this.b != null) {
            try {
                if (this.c) {
                    com.agilebinary.a.a.a.i.b.a(this.f94a);
                    this.b.g();
                }
            } finally {
                i();
            }
        }
    }

    private void xi() {
        if (this.b != null) {
            try {
                this.b.d_();
            } finally {
                this.b = null;
            }
        }
    }

    private final boolean xj_() {
        if (this.b == null) {
            return false;
        }
        this.b.b();
        return false;
    }

    public final void a(OutputStream outputStream) {
        xa(outputStream);
    }

    public final boolean a() {
        return xa();
    }

    public final boolean a(InputStream inputStream) {
        return xa(inputStream);
    }

    public final void b() {
        xb();
    }

    public final boolean b(InputStream inputStream) {
        return xb(inputStream);
    }

    public final void d_() {
        xd_();
    }

    public final InputStream f() {
        return xf();
    }

    public final boolean j_() {
        return xj_();
    }
}
