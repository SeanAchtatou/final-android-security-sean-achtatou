package com.agilebinary.a.a.a.k;

import java.io.Serializable;
import java.util.Comparator;

public final class a implements Serializable, Comparator {
    /* access modifiers changed from: private */
    /* renamed from: xcompare */
    public final /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
        c cVar = (c) obj;
        c cVar2 = (c) obj2;
        int compareTo = cVar.a().compareTo(cVar2.a());
        if (compareTo == 0) {
            String c = cVar.c();
            if (c == null) {
                c = "";
            } else if (c.indexOf(46) == -1) {
                c = c + ".local";
            }
            String c2 = cVar2.c();
            if (c2 == null) {
                c2 = "";
            } else if (c2.indexOf(46) == -1) {
                c2 = c2 + ".local";
            }
            compareTo = c.compareToIgnoreCase(c2);
        }
        if (compareTo != 0) {
            return compareTo;
        }
        String d = cVar.d();
        if (d == null) {
            d = "/";
        }
        String d2 = cVar2.d();
        if (d2 == null) {
            d2 = "/";
        }
        return d.compareTo(d2);
    }
}
