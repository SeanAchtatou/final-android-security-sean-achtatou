package com.agilebinary.a.a.a.c.c;

import com.agilebinary.a.a.a.e.b;
import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.j.e;
import com.agilebinary.a.a.a.j.f;
import com.agilebinary.a.a.a.j.g;
import java.io.OutputStream;

public abstract class d implements e, g {

    /* renamed from: a  reason: collision with root package name */
    private static final byte[] f59a = {13, 10};
    private OutputStream b;
    private com.agilebinary.a.a.a.i.e c;
    private String d = "US-ASCII";
    private boolean e = true;
    private int f = 512;
    private i g;

    private void a(byte[] bArr) {
        xa(bArr);
    }

    private void d() {
        xd();
    }

    private final int xa() {
        return this.c.d();
    }

    private final void xa(int i) {
        if (this.c.g()) {
            d();
        }
        this.c.a(i);
    }

    private final void xa(c cVar) {
        if (cVar != null) {
            if (this.e) {
                int i = 0;
                int c2 = cVar.c();
                while (c2 > 0) {
                    int min = Math.min(this.c.c() - this.c.d(), c2);
                    if (min > 0) {
                        com.agilebinary.a.a.a.i.e eVar = this.c;
                        if (cVar != null) {
                            eVar.a(cVar.b(), i, min);
                        }
                    }
                    if (this.c.g()) {
                        d();
                    }
                    i += min;
                    c2 -= min;
                }
            } else {
                a(cVar.toString().getBytes(this.d));
            }
            a(f59a);
        }
    }

    private final void xa(OutputStream outputStream, int i, com.agilebinary.a.a.a.e.e eVar) {
        if (outputStream == null) {
            throw new IllegalArgumentException("Input stream may not be null");
        } else if (i <= 0) {
            throw new IllegalArgumentException("Buffer size may not be negative or zero");
        } else if (eVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        } else {
            this.b = outputStream;
            this.c = new com.agilebinary.a.a.a.i.e(i);
            this.d = b.a(eVar);
            this.e = this.d.equalsIgnoreCase("US-ASCII") || this.d.equalsIgnoreCase("ASCII");
            this.f = eVar.a("http.connection.min-chunk-limit", 512);
            this.g = new i();
        }
    }

    private final void xa(String str) {
        if (str != null) {
            if (str.length() > 0) {
                a(str.getBytes(this.d));
            }
            a(f59a);
        }
    }

    private void xa(byte[] bArr) {
        if (bArr != null) {
            a(bArr, 0, bArr.length);
        }
    }

    private final void xa(byte[] bArr, int i, int i2) {
        if (bArr != null) {
            if (i2 > this.f || i2 > this.c.c()) {
                d();
                this.b.write(bArr, i, i2);
                this.g.a((long) i2);
                return;
            }
            if (i2 > this.c.c() - this.c.d()) {
                d();
            }
            this.c.a(bArr, i, i2);
        }
    }

    private final void xb() {
        d();
        this.b.flush();
    }

    private final f xc() {
        return this.g;
    }

    private void xd() {
        int d2 = this.c.d();
        if (d2 > 0) {
            this.b.write(this.c.e(), 0, d2);
            this.c.a();
            this.g.a((long) d2);
        }
    }

    public final int a() {
        return xa();
    }

    public final void a(int i) {
        xa(i);
    }

    public final void a(c cVar) {
        xa(cVar);
    }

    /* access modifiers changed from: protected */
    public final void a(OutputStream outputStream, int i, com.agilebinary.a.a.a.e.e eVar) {
        xa(outputStream, i, eVar);
    }

    public final void a(String str) {
        xa(str);
    }

    public final void a(byte[] bArr, int i, int i2) {
        xa(bArr, i, i2);
    }

    public final void b() {
        xb();
    }

    public final f c() {
        return xc();
    }
}
