package com.agilebinary.a.a.a.c.b;

import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.h.c.c;
import com.agilebinary.a.a.a.h.g;
import com.agilebinary.a.a.a.h.k;

public abstract class m extends l {

    /* renamed from: a  reason: collision with root package name */
    protected volatile a f52a;

    protected m(k kVar, a aVar) {
        super(kVar, aVar.f35a);
        this.f52a = aVar;
    }

    private void a(a aVar) {
        xa(aVar);
    }

    private void xa(a aVar) {
        if (o() || aVar == null) {
            throw new n();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.a.c.b.a.a(boolean, com.agilebinary.a.a.a.e.e):void
     arg types: [int, com.agilebinary.a.a.a.e.e]
     candidates:
      com.agilebinary.a.a.a.c.b.a.a(com.agilebinary.a.a.a.h.c.c, com.agilebinary.a.a.a.e.e):void
      com.agilebinary.a.a.a.c.b.a.a(boolean, com.agilebinary.a.a.a.e.e):void */
    private final void xa(e eVar) {
        a i = i();
        a(i);
        i.a(false, eVar);
    }

    private final void xa(com.agilebinary.a.a.a.f.k kVar, e eVar) {
        a i = i();
        a(i);
        i.a(eVar);
    }

    private final void xa(c cVar, com.agilebinary.a.a.a.f.k kVar, e eVar) {
        a i = i();
        a(i);
        i.a(cVar, eVar);
    }

    private final void xa(Object obj) {
        a i = i();
        a(i);
        i.a(obj);
    }

    private final c xc_() {
        a i = i();
        a(i);
        if (i.c == null) {
            return null;
        }
        return i.c.h();
    }

    private a xi() {
        return this.f52a;
    }

    private synchronized void xj() {
        this.f52a = null;
        super.j();
    }

    private final void xk() {
        a i = i();
        if (i != null) {
            i.b();
        }
        g n = n();
        if (n != null) {
            n.k();
        }
    }

    private final void xm() {
        a i = i();
        if (i != null) {
            i.b();
        }
        g n = n();
        if (n != null) {
            n.m();
        }
    }

    public final void a(e eVar) {
        xa(eVar);
    }

    public final void a(com.agilebinary.a.a.a.f.k kVar, e eVar) {
        xa(kVar, eVar);
    }

    public final void a(c cVar, com.agilebinary.a.a.a.f.k kVar, e eVar) {
        xa(cVar, kVar, eVar);
    }

    public final void a(Object obj) {
        xa(obj);
    }

    public final c c_() {
        return xc_();
    }

    /* access modifiers changed from: protected */
    public a i() {
        return xi();
    }

    /* access modifiers changed from: protected */
    public synchronized void j() {
        xj();
    }

    public final void k() {
        xk();
    }

    public final void m() {
        xm();
    }
}
