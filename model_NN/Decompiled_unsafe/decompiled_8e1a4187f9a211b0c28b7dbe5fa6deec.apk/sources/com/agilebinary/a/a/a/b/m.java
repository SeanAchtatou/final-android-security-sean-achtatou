package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.a;
import com.agilebinary.a.a.a.h;
import java.io.Serializable;

public final class m implements h, Serializable, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final a f16a;
    private final int b;
    private final String c;

    public m(a aVar, int i, String str) {
        if (aVar == null) {
            throw new IllegalArgumentException("Protocol version may not be null.");
        } else if (i < 0) {
            throw new IllegalArgumentException("Status code may not be negative.");
        } else {
            this.f16a = aVar;
            this.b = i;
            this.c = str;
        }
    }

    private final a xa() {
        return this.f16a;
    }

    private final int xb() {
        return this.b;
    }

    private final String xc() {
        return this.c;
    }

    private final Object xclone() {
        return super.clone();
    }

    private final String xtoString() {
        return r.a(this).toString();
    }

    public final a a() {
        return xa();
    }

    public final int b() {
        return xb();
    }

    public final String c() {
        return xc();
    }

    public final Object clone() {
        return xclone();
    }

    public final String toString() {
        return xtoString();
    }
}
