package com.agilebinary.a.a.a.d.a;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.Stack;

public final class b {
    private b() {
    }

    private static String a(String str) {
        return xa(str);
    }

    private static URI a(String str, String str2, int i, String str3, String str4, String str5) {
        return xa(str, str2, i, str3, str4, str5);
    }

    private static URI a(URI uri) {
        return xa(uri);
    }

    public static URI a(URI uri, com.agilebinary.a.a.a.b bVar) {
        return xa(uri, bVar);
    }

    public static URI a(URI uri, com.agilebinary.a.a.a.b bVar, boolean z) {
        return xa(uri, bVar, z);
    }

    public static URI a(URI uri, URI uri2) {
        return xa(uri, uri2);
    }

    private static String xa(String str) {
        if (str == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder(str.length());
        boolean z = false;
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (charAt != '/') {
                sb.append(charAt);
                z = false;
            } else if (!z) {
                sb.append(charAt);
                z = true;
            }
        }
        return sb.toString();
    }

    private static URI xa(String str, String str2, int i, String str3, String str4, String str5) {
        StringBuilder sb = new StringBuilder();
        if (str2 != null) {
            if (str != null) {
                sb.append(str);
                sb.append("://");
            }
            sb.append(str2);
            if (i > 0) {
                sb.append(':');
                sb.append(i);
            }
        }
        if (str3 == null || !str3.startsWith("/")) {
            sb.append('/');
        }
        if (str3 != null) {
            sb.append(str3);
        }
        if (str4 != null) {
            sb.append('?');
            sb.append(str4);
        }
        if (str5 != null) {
            sb.append('#');
            sb.append(str5);
        }
        return new URI(sb.toString());
    }

    private static URI xa(URI uri) {
        String path = uri.getPath();
        if (path == null || path.indexOf("/.") == -1) {
            return uri;
        }
        String[] split = path.split("/");
        Stack stack = new Stack();
        for (int i = 0; i < split.length; i++) {
            if (split[i].length() != 0 && !".".equals(split[i])) {
                if (!"..".equals(split[i])) {
                    stack.push(split[i]);
                } else if (!stack.isEmpty()) {
                    stack.pop();
                }
            }
        }
        StringBuilder sb = new StringBuilder();
        Iterator it = stack.iterator();
        while (it.hasNext()) {
            sb.append('/').append((String) it.next());
        }
        try {
            return new URI(uri.getScheme(), uri.getAuthority(), sb.toString(), uri.getQuery(), uri.getFragment());
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private static URI xa(URI uri, com.agilebinary.a.a.a.b bVar) {
        return a(uri, bVar, false);
    }

    private static URI xa(URI uri, com.agilebinary.a.a.a.b bVar, boolean z) {
        if (uri == null) {
            throw new IllegalArgumentException("URI may nor be null");
        } else if (bVar != null) {
            return a(bVar.c(), bVar.a(), bVar.b(), a(uri.getRawPath()), uri.getRawQuery(), z ? null : uri.getRawFragment());
        } else {
            return a(null, null, -1, a(uri.getRawPath()), uri.getRawQuery(), z ? null : uri.getRawFragment());
        }
    }

    private static URI xa(URI uri, URI uri2) {
        URI uri3;
        if (uri == null) {
            throw new IllegalArgumentException("Base URI may nor be null");
        } else if (uri2 == null) {
            throw new IllegalArgumentException("Reference URI may nor be null");
        } else {
            String uri4 = uri2.toString();
            if (uri4.startsWith("?")) {
                String uri5 = uri.toString();
                if (uri5.indexOf(63) >= 0) {
                    uri5 = uri5.substring(0, uri5.indexOf(63));
                }
                return URI.create(uri5 + uri2.toString());
            }
            boolean z = uri4.length() == 0;
            URI resolve = uri.resolve(z ? URI.create("#") : uri2);
            if (z) {
                String uri6 = resolve.toString();
                uri3 = URI.create(uri6.substring(0, uri6.indexOf(35)));
            } else {
                uri3 = resolve;
            }
            return a(uri3);
        }
    }
}
