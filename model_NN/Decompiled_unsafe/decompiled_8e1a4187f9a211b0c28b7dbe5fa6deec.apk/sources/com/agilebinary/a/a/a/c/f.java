package com.agilebinary.a.a.a.c;

import com.agilebinary.a.a.a.ae;
import com.agilebinary.a.a.a.c.c.c;
import com.agilebinary.a.a.a.c.c.h;
import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.j.a;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;

public class f extends a implements ae {

    /* renamed from: a  reason: collision with root package name */
    private volatile boolean f83a;
    private volatile Socket b = null;

    private a xa(Socket socket, int i, e eVar) {
        return new c(socket, i, eVar);
    }

    private final void xa() {
        if (!this.f83a) {
            throw new IllegalStateException("Connection is not open");
        }
    }

    private final void xa(Socket socket, e eVar) {
        if (socket == null) {
            throw new IllegalArgumentException("Socket may not be null");
        } else if (eVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        } else {
            this.b = socket;
            if (eVar == null) {
                throw new IllegalArgumentException("HTTP parameters may not be null");
            }
            int a2 = eVar.a("http.socket.buffer-size", -1);
            a(a(socket, a2, eVar), b(socket, a2, eVar), eVar);
            this.f83a = true;
        }
    }

    private com.agilebinary.a.a.a.j.e xb(Socket socket, int i, e eVar) {
        return new h(socket, i, eVar);
    }

    private final void xb(int i) {
        a();
        if (this.b != null) {
            try {
                this.b.setSoTimeout(i);
            } catch (SocketException e) {
            }
        }
    }

    private final void xf() {
        if (this.f83a) {
            throw new IllegalStateException("Connection is already open");
        }
    }

    private Socket xi_() {
        return this.b;
    }

    private void xk() {
        if (this.f83a) {
            this.f83a = false;
            Socket socket = this.b;
            try {
                b();
                try {
                    socket.shutdownOutput();
                } catch (IOException e) {
                }
                try {
                    socket.shutdownInput();
                } catch (IOException | UnsupportedOperationException e2) {
                }
            } finally {
                socket.close();
            }
        }
    }

    private final boolean xl() {
        return this.f83a;
    }

    private void xm() {
        this.f83a = false;
        Socket socket = this.b;
        if (socket != null) {
            socket.close();
        }
    }

    private final InetAddress xp() {
        if (this.b != null) {
            return this.b.getInetAddress();
        }
        return null;
    }

    private final int xq() {
        if (this.b != null) {
            return this.b.getPort();
        }
        return -1;
    }

    /* access modifiers changed from: protected */
    public a a(Socket socket, int i, e eVar) {
        return xa(socket, i, eVar);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        xa();
    }

    /* access modifiers changed from: protected */
    public final void a(Socket socket, e eVar) {
        xa(socket, eVar);
    }

    /* access modifiers changed from: protected */
    public com.agilebinary.a.a.a.j.e b(Socket socket, int i, e eVar) {
        return xb(socket, i, eVar);
    }

    public final void b(int i) {
        xb(i);
    }

    /* access modifiers changed from: protected */
    public final void f() {
        xf();
    }

    /* access modifiers changed from: protected */
    public Socket i_() {
        return xi_();
    }

    public void k() {
        xk();
    }

    public final boolean l() {
        return xl();
    }

    public void m() {
        xm();
    }

    public final InetAddress p() {
        return xp();
    }

    public final int q() {
        return xq();
    }
}
