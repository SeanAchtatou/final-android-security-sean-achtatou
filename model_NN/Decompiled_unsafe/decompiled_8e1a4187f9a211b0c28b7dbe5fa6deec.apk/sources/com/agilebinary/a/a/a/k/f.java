package com.agilebinary.a.a.a.k;

import java.util.Locale;

public final class f {

    /* renamed from: a  reason: collision with root package name */
    private final String f119a;
    private final int b;
    private final String c;
    private final boolean d;

    public f(String str, int i, String str2, boolean z) {
        if (str == null) {
            throw new IllegalArgumentException("Host of origin may not be null");
        } else if (str.trim().length() == 0) {
            throw new IllegalArgumentException("Host of origin may not be blank");
        } else if (i < 0) {
            throw new IllegalArgumentException("Invalid port: " + i);
        } else if (str2 == null) {
            throw new IllegalArgumentException("Path of origin may not be null.");
        } else {
            this.f119a = str.toLowerCase(Locale.ENGLISH);
            this.b = i;
            if (str2.trim().length() != 0) {
                this.c = str2;
            } else {
                this.c = "/";
            }
            this.d = z;
        }
    }

    private final String xa() {
        return this.f119a;
    }

    private final String xb() {
        return this.c;
    }

    private final int xc() {
        return this.b;
    }

    private final boolean xd() {
        return this.d;
    }

    private final String xtoString() {
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        if (this.d) {
            sb.append("(secure)");
        }
        sb.append(this.f119a);
        sb.append(':');
        sb.append(Integer.toString(this.b));
        sb.append(this.c);
        sb.append(']');
        return sb.toString();
    }

    public final String a() {
        return xa();
    }

    public final String b() {
        return xb();
    }

    public final int c() {
        return xc();
    }

    public final boolean d() {
        return xd();
    }

    public final String toString() {
        return xtoString();
    }
}
