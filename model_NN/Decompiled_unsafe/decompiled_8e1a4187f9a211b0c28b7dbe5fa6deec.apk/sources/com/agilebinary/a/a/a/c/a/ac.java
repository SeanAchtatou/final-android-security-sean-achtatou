package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.k.b;
import com.agilebinary.a.a.a.k.e;
import java.util.Date;

public final class ac extends x {
    private final void xa(b bVar, String str) {
        if (bVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (str == null) {
            throw new e("Missing value for max-age attribute");
        } else {
            try {
                int parseInt = Integer.parseInt(str);
                if (parseInt < 0) {
                    throw new e("Negative max-age attribute: " + str);
                }
                bVar.a(new Date(System.currentTimeMillis() + (((long) parseInt) * 1000)));
            } catch (NumberFormatException e) {
                throw new e("Invalid max-age attribute: " + str);
            }
        }
    }

    public final void a(b bVar, String str) {
        xa(bVar, str);
    }
}
