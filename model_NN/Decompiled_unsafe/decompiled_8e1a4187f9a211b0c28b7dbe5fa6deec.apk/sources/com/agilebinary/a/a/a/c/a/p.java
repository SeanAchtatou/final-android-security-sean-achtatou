package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.k.c;
import com.agilebinary.a.a.a.k.f;
import com.agilebinary.a.a.a.k.h;
import com.agilebinary.a.a.a.k.j;
import com.agilebinary.a.a.a.m;
import com.agilebinary.a.a.a.t;
import java.util.Iterator;
import java.util.List;

public final class p implements j {

    /* renamed from: a  reason: collision with root package name */
    private final String[] f30a;
    private final boolean b;
    private ae c;
    private ag d;
    private w e;
    private q f;

    public p() {
        this(null, false);
    }

    public p(String[] strArr, boolean z) {
        this.f30a = strArr == null ? null : (String[]) strArr.clone();
        this.b = z;
    }

    private ae c() {
        return xc();
    }

    private ag d() {
        return xd();
    }

    private w e() {
        return xe();
    }

    private final int xa() {
        return c().a();
    }

    private final List xa(t tVar, f fVar) {
        if (tVar == null) {
            throw new IllegalArgumentException("Header may not be null");
        } else if (fVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else {
            m[] c2 = tVar.c();
            boolean z = false;
            boolean z2 = false;
            for (m mVar : c2) {
                if (mVar.a("version") != null) {
                    z2 = true;
                }
                if (mVar.a("expires") != null) {
                    z = true;
                }
            }
            if (z2) {
                return "Set-Cookie2".equals(tVar.a()) ? c().a(c2, fVar) : d().a(c2, fVar);
            }
            if (!z) {
                return e().a(c2, fVar);
            }
            if (this.f == null) {
                this.f = new q(this.f30a);
            }
            return this.f.a(tVar, fVar);
        }
    }

    private final List xa(List list) {
        int i;
        if (list == null) {
            throw new IllegalArgumentException("List of cookie may not be null");
        }
        int i2 = Integer.MAX_VALUE;
        boolean z = true;
        Iterator it = list.iterator();
        while (true) {
            i = i2;
            if (!it.hasNext()) {
                break;
            }
            c cVar = (c) it.next();
            if (!(cVar instanceof h)) {
                z = false;
            }
            i2 = cVar.h() < i ? cVar.h() : i;
        }
        return i > 0 ? z ? c().a(list) : d().a(list) : e().a(list);
    }

    private final void xa(c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (fVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else if (cVar.h() <= 0) {
            e().a(cVar, fVar);
        } else if (cVar instanceof h) {
            c().a(cVar, fVar);
        } else {
            d().a(cVar, fVar);
        }
    }

    private final t xb() {
        return c().b();
    }

    private final boolean xb(c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (fVar != null) {
            return cVar.h() > 0 ? cVar instanceof h ? c().b(cVar, fVar) : d().b(cVar, fVar) : e().b(cVar, fVar);
        } else {
            throw new IllegalArgumentException("Cookie origin may not be null");
        }
    }

    private ae xc() {
        if (this.c == null) {
            this.c = new ae(this.f30a, this.b);
        }
        return this.c;
    }

    private ag xd() {
        if (this.d == null) {
            this.d = new ag(this.f30a, this.b);
        }
        return this.d;
    }

    private w xe() {
        if (this.e == null) {
            this.e = new w(this.f30a);
        }
        return this.e;
    }

    private final String xtoString() {
        return "best-match";
    }

    public final int a() {
        return xa();
    }

    public final List a(t tVar, f fVar) {
        return xa(tVar, fVar);
    }

    public final List a(List list) {
        return xa(list);
    }

    public final void a(c cVar, f fVar) {
        xa(cVar, fVar);
    }

    public final t b() {
        return xb();
    }

    public final boolean b(c cVar, f fVar) {
        return xb(cVar, fVar);
    }

    public final String toString() {
        return xtoString();
    }
}
