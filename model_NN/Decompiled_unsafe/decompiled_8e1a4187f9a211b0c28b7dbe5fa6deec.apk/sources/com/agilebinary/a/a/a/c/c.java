package com.agilebinary.a.a.a.c;

import com.agilebinary.a.a.a.ac;
import com.agilebinary.a.a.a.b.k;
import com.agilebinary.a.a.a.h;
import com.agilebinary.a.a.a.i;
import com.agilebinary.a.a.a.j;
import java.util.Locale;

public final class c implements i {

    /* renamed from: a  reason: collision with root package name */
    private ac f55a;

    public c() {
        this(e.f80a);
    }

    private c(ac acVar) {
        if (acVar == null) {
            throw new IllegalArgumentException("Reason phrase catalog must not be null.");
        }
        this.f55a = acVar;
    }

    private final j xa(h hVar) {
        if (hVar == null) {
            throw new IllegalArgumentException("Status line may not be null");
        }
        return new k(hVar, this.f55a, Locale.getDefault());
    }

    public final j a(h hVar) {
        return xa(hVar);
    }
}
