package com.agilebinary.a.a.a.d.c;

import com.agilebinary.a.a.a.a;
import com.agilebinary.a.a.a.b.l;
import com.agilebinary.a.a.a.b.q;
import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.h.b;
import com.agilebinary.a.a.a.h.d;
import java.io.IOException;
import java.net.URI;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public abstract class f extends l implements b, g, Cloneable {
    private Lock c = new ReentrantLock();
    private boolean d;
    private URI e;
    private b f;
    private d g;

    private final com.agilebinary.a.a.a.d xa() {
        String b = b();
        a b2 = com.agilebinary.a.a.a.e.b.b(g());
        URI uri = this.e;
        String aSCIIString = uri != null ? uri.toASCIIString() : null;
        if (aSCIIString == null || aSCIIString.length() == 0) {
            aSCIIString = "/";
        }
        return new q(b, aSCIIString, b2);
    }

    private final void xa(b bVar) {
        this.c.lock();
        try {
            if (this.d) {
                throw new IOException("Request already aborted");
            }
            this.g = null;
            this.f = bVar;
        } finally {
            this.c.unlock();
        }
    }

    private final void xa(d dVar) {
        this.c.lock();
        try {
            if (this.d) {
                throw new IOException("Request already aborted");
            }
            this.f = null;
            this.g = dVar;
        } finally {
            this.c.unlock();
        }
    }

    private final void xa(URI uri) {
        this.e = uri;
    }

    private final a xc() {
        return com.agilebinary.a.a.a.e.b.b(g());
    }

    private Object xclone() {
        f fVar = (f) super.clone();
        fVar.c = new ReentrantLock();
        fVar.d = false;
        fVar.g = null;
        fVar.f = null;
        fVar.f15a = (com.agilebinary.a.a.a.b.b) com.agilebinary.a.a.a.d.a.a.a(this.f15a);
        fVar.b = (e) com.agilebinary.a.a.a.d.a.a.a(this.b);
        return fVar;
    }

    private final void xd() {
        this.c.lock();
        try {
            if (!this.d) {
                this.d = true;
                b bVar = this.f;
                d dVar = this.g;
                this.c.unlock();
                if (bVar != null) {
                    bVar.a();
                }
                if (dVar != null) {
                    try {
                        dVar.b();
                    } catch (IOException e2) {
                    }
                }
            }
        } finally {
            this.c.unlock();
        }
    }

    private final URI xe_() {
        return this.e;
    }

    public final com.agilebinary.a.a.a.d a() {
        return xa();
    }

    public final void a(b bVar) {
        xa(bVar);
    }

    public final void a(d dVar) {
        xa(dVar);
    }

    public final void a(URI uri) {
        xa(uri);
    }

    public abstract String b();

    public final a c() {
        return xc();
    }

    public Object clone() {
        return xclone();
    }

    public final void d() {
        xd();
    }

    public final URI e_() {
        return xe_();
    }
}
