package com.agilebinary.a.a.a.c.b.a;

import com.agilebinary.a.a.a.h.a.b;
import com.agilebinary.a.a.a.h.c.c;
import com.agilebinary.a.a.a.h.g;
import com.agilebinary.a.a.a.h.l;
import com.agilebinary.a.a.a.h.m;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import org.apache.commons.logging.Log;

public final class a extends j {
    private final Log c = com.agilebinary.a.a.b.a.a.a(getClass());
    /* access modifiers changed from: private */
    public final Lock d;
    private m e;
    private b f;
    private Set g;
    private Queue h;
    private Queue i;
    private Map j;
    private final long k;
    private final TimeUnit l;
    private volatile boolean m;
    private volatile int n;
    private volatile int o;

    public a(m mVar, b bVar, TimeUnit timeUnit) {
        if (mVar == null) {
            throw new IllegalArgumentException("Connection operator may not be null");
        } else if (bVar == null) {
            throw new IllegalArgumentException("Connections per route may not be null");
        } else {
            this.d = this.f41a;
            this.g = this.b;
            this.e = mVar;
            this.f = bVar;
            this.n = 20;
            this.h = new LinkedList();
            this.i = new LinkedList();
            this.j = new HashMap();
            this.k = 30;
            this.l = timeUnit;
        }
    }

    private c a(c cVar) {
        return xa(cVar);
    }

    private g a(c cVar, m mVar) {
        return xa(cVar, mVar);
    }

    private g a(c cVar, Object obj) {
        return xa(cVar, obj);
    }

    private void a(g gVar) {
        xa(gVar);
    }

    private void b(g gVar) {
        xb(gVar);
    }

    private c xa(c cVar) {
        this.d.lock();
        try {
            c cVar2 = (c) this.j.get(cVar);
            if (cVar2 == null) {
                cVar2 = new c(cVar, this.f);
                this.j.put(cVar, cVar2);
            }
            return cVar2;
        } finally {
            this.d.unlock();
        }
    }

    private g xa(c cVar, m mVar) {
        if (this.c.isDebugEnabled()) {
            this.c.debug("Creating new connection [" + cVar.a() + "]");
        }
        g gVar = new g(mVar, cVar.a(), this.k, this.l);
        this.d.lock();
        try {
            cVar.b(gVar);
            this.o++;
            this.g.add(gVar);
            return gVar;
        } finally {
            this.d.unlock();
        }
    }

    private g xa(c cVar, Object obj) {
        this.d.lock();
        g gVar = null;
        boolean z = false;
        while (!z) {
            try {
                gVar = cVar.a(obj);
                if (gVar != null) {
                    if (this.c.isDebugEnabled()) {
                        this.c.debug("Getting free connection [" + cVar.a() + "][" + obj + "]");
                    }
                    this.h.remove(gVar);
                    if (gVar.a(System.currentTimeMillis())) {
                        if (this.c.isDebugEnabled()) {
                            this.c.debug("Closing expired free connection [" + cVar.a() + "][" + obj + "]");
                        }
                        a(gVar);
                        cVar.e();
                        this.o--;
                    } else {
                        this.g.add(gVar);
                        z = true;
                    }
                } else if (this.c.isDebugEnabled()) {
                    this.c.debug("No free connections [" + cVar.a() + "][" + obj + "]");
                    z = true;
                } else {
                    z = true;
                }
            } catch (Throwable th) {
                this.d.unlock();
                throw th;
            }
        }
        this.d.unlock();
        return gVar;
    }

    private final g xa(c cVar, Object obj, long j2, TimeUnit timeUnit, f fVar) {
        Date date = j2 > 0 ? new Date(System.currentTimeMillis() + timeUnit.toMillis(j2)) : null;
        this.d.lock();
        c a2 = a(cVar);
        k kVar = null;
        g gVar = null;
        while (gVar == null) {
            if (!this.m) {
                if (this.c.isDebugEnabled()) {
                    this.c.debug("[" + cVar + "] total kept alive: " + this.h.size() + ", total issued: " + this.g.size() + ", total allocated: " + this.o + " out of " + this.n);
                }
                gVar = a(a2, obj);
                if (gVar != null) {
                    break;
                }
                boolean z = a2.d() > 0;
                if (this.c.isDebugEnabled()) {
                    this.c.debug("Available capacity: " + a2.d() + " out of " + a2.b() + " [" + cVar + "][" + obj + "]");
                }
                if (z && this.o < this.n) {
                    gVar = a(a2, this.e);
                } else if (!z || this.h.isEmpty()) {
                    if (this.c.isDebugEnabled()) {
                        this.c.debug("Need to wait for connection [" + cVar + "][" + obj + "]");
                    }
                    if (kVar == null) {
                        k kVar2 = new k(this.d.newCondition(), a2);
                        fVar.a(kVar2);
                        kVar = kVar2;
                    }
                    a2.a(kVar);
                    this.i.add(kVar);
                    boolean a3 = kVar.a(date);
                    a2.b(kVar);
                    this.i.remove(kVar);
                    if (!a3 && date != null && date.getTime() <= System.currentTimeMillis()) {
                        throw new l("Timeout waiting for connection");
                    }
                } else {
                    this.d.lock();
                    try {
                        g gVar2 = (g) this.h.remove();
                        if (gVar2 != null) {
                            b(gVar2);
                        } else if (this.c.isDebugEnabled()) {
                            this.c.debug("No free connection to delete");
                        }
                        this.d.unlock();
                        a2 = a(cVar);
                        gVar = a(a2, this.e);
                    } catch (Throwable th) {
                        this.d.unlock();
                        throw th;
                    }
                }
            } else {
                throw new IllegalStateException("Connection pool shut down");
            }
        }
        g gVar3 = gVar;
        this.d.unlock();
        return gVar3;
    }

    private final void xa() {
        this.d.lock();
        try {
            if (!this.m) {
                this.m = true;
                Iterator it = this.g.iterator();
                while (it.hasNext()) {
                    it.remove();
                    a((g) it.next());
                }
                Iterator it2 = this.h.iterator();
                while (it2.hasNext()) {
                    g gVar = (g) it2.next();
                    it2.remove();
                    if (this.c.isDebugEnabled()) {
                        this.c.debug("Closing connection [" + gVar.d() + "][" + gVar.a() + "]");
                    }
                    a(gVar);
                }
                Iterator it3 = this.i.iterator();
                while (it3.hasNext()) {
                    it3.remove();
                    ((k) it3.next()).a();
                }
                this.j.clear();
                this.d.unlock();
            }
        } finally {
            this.d.unlock();
        }
    }

    private final void xa(long j2, TimeUnit timeUnit) {
        if (timeUnit == null) {
            throw new IllegalArgumentException("Time unit must not be null.");
        }
        long j3 = j2 < 0 ? 0 : j2;
        if (this.c.isDebugEnabled()) {
            this.c.debug("Closing connections idle longer than " + j3 + " " + timeUnit);
        }
        long currentTimeMillis = System.currentTimeMillis() - timeUnit.toMillis(j3);
        this.d.lock();
        try {
            Iterator it = this.h.iterator();
            while (it.hasNext()) {
                g gVar = (g) it.next();
                if (gVar.e() <= currentTimeMillis) {
                    if (this.c.isDebugEnabled()) {
                        this.c.debug("Closing connection last used @ " + new Date(gVar.e()));
                    }
                    it.remove();
                    b(gVar);
                }
            }
        } finally {
            this.d.unlock();
        }
    }

    private void xa(g gVar) {
        g c2 = gVar.c();
        if (c2 != null) {
            try {
                c2.k();
            } catch (IOException e2) {
                this.c.debug("I/O error closing connection", e2);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x00f3 A[Catch:{ all -> 0x0147, all -> 0x0110 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void xa(com.agilebinary.a.a.a.c.b.a.g r7, boolean r8, long r9, java.util.concurrent.TimeUnit r11) {
        /*
            r6 = this;
            com.agilebinary.a.a.a.h.c.c r0 = r7.d()
            org.apache.commons.logging.Log r1 = r6.c
            boolean r1 = r1.isDebugEnabled()
            if (r1 == 0) goto L_0x0038
            org.apache.commons.logging.Log r1 = r6.c
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Releasing connection ["
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r3 = "]["
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.Object r3 = r7.a()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "]"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.debug(r2)
        L_0x0038:
            java.util.concurrent.locks.Lock r1 = r6.d
            r1.lock()
            boolean r1 = r6.m     // Catch:{ all -> 0x0110 }
            if (r1 == 0) goto L_0x004a
            r6.a(r7)     // Catch:{ all -> 0x0110 }
            java.util.concurrent.locks.Lock r0 = r6.d
            r0.unlock()
        L_0x0049:
            return
        L_0x004a:
            java.util.Set r1 = r6.g     // Catch:{ all -> 0x0110 }
            r1.remove(r7)     // Catch:{ all -> 0x0110 }
            com.agilebinary.a.a.a.c.b.a.c r1 = r6.a(r0)     // Catch:{ all -> 0x0110 }
            if (r8 == 0) goto L_0x0106
            org.apache.commons.logging.Log r2 = r6.c     // Catch:{ all -> 0x0110 }
            boolean r2 = r2.isDebugEnabled()     // Catch:{ all -> 0x0110 }
            if (r2 == 0) goto L_0x00aa
            r2 = 0
            int r2 = (r9 > r2 ? 1 : (r9 == r2 ? 0 : -1))
            if (r2 < 0) goto L_0x0102
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0110 }
            r2.<init>()     // Catch:{ all -> 0x0110 }
            java.lang.StringBuilder r2 = r2.append(r9)     // Catch:{ all -> 0x0110 }
            java.lang.String r3 = " "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0110 }
            java.lang.StringBuilder r2 = r2.append(r11)     // Catch:{ all -> 0x0110 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0110 }
        L_0x007a:
            org.apache.commons.logging.Log r3 = r6.c     // Catch:{ all -> 0x0110 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0110 }
            r4.<init>()     // Catch:{ all -> 0x0110 }
            java.lang.String r5 = "Pooling connection ["
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0110 }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ all -> 0x0110 }
            java.lang.String r4 = "]["
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ all -> 0x0110 }
            java.lang.Object r4 = r7.a()     // Catch:{ all -> 0x0110 }
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ all -> 0x0110 }
            java.lang.String r4 = "]; keep alive for "
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ all -> 0x0110 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ all -> 0x0110 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0110 }
            r3.debug(r0)     // Catch:{ all -> 0x0110 }
        L_0x00aa:
            r1.a(r7)     // Catch:{ all -> 0x0110 }
            r7.a(r9, r11)     // Catch:{ all -> 0x0110 }
            java.util.Queue r0 = r6.h     // Catch:{ all -> 0x0110 }
            r0.add(r7)     // Catch:{ all -> 0x0110 }
        L_0x00b5:
            r0 = 0
            java.util.concurrent.locks.Lock r2 = r6.d     // Catch:{ all -> 0x0110 }
            r2.lock()     // Catch:{ all -> 0x0110 }
            if (r1 == 0) goto L_0x0117
            boolean r2 = r1.f()     // Catch:{ all -> 0x0147 }
            if (r2 == 0) goto L_0x0117
            org.apache.commons.logging.Log r0 = r6.c     // Catch:{ all -> 0x0147 }
            boolean r0 = r0.isDebugEnabled()     // Catch:{ all -> 0x0147 }
            if (r0 == 0) goto L_0x00ed
            org.apache.commons.logging.Log r0 = r6.c     // Catch:{ all -> 0x0147 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0147 }
            r2.<init>()     // Catch:{ all -> 0x0147 }
            java.lang.String r3 = "Notifying thread waiting on pool ["
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0147 }
            com.agilebinary.a.a.a.h.c.c r3 = r1.a()     // Catch:{ all -> 0x0147 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0147 }
            java.lang.String r3 = "]"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0147 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0147 }
            r0.debug(r2)     // Catch:{ all -> 0x0147 }
        L_0x00ed:
            com.agilebinary.a.a.a.c.b.a.k r0 = r1.g()     // Catch:{ all -> 0x0147 }
        L_0x00f1:
            if (r0 == 0) goto L_0x00f6
            r0.a()     // Catch:{ all -> 0x0147 }
        L_0x00f6:
            java.util.concurrent.locks.Lock r0 = r6.d     // Catch:{ all -> 0x0110 }
            r0.unlock()     // Catch:{ all -> 0x0110 }
            java.util.concurrent.locks.Lock r0 = r6.d
            r0.unlock()
            goto L_0x0049
        L_0x0102:
            java.lang.String r2 = "ever"
            goto L_0x007a
        L_0x0106:
            r1.e()     // Catch:{ all -> 0x0110 }
            int r0 = r6.o     // Catch:{ all -> 0x0110 }
            r2 = 1
            int r0 = r0 - r2
            r6.o = r0     // Catch:{ all -> 0x0110 }
            goto L_0x00b5
        L_0x0110:
            r0 = move-exception
            java.util.concurrent.locks.Lock r1 = r6.d
            r1.unlock()
            throw r0
        L_0x0117:
            java.util.Queue r1 = r6.i     // Catch:{ all -> 0x0147 }
            boolean r1 = r1.isEmpty()     // Catch:{ all -> 0x0147 }
            if (r1 != 0) goto L_0x0137
            org.apache.commons.logging.Log r0 = r6.c     // Catch:{ all -> 0x0147 }
            boolean r0 = r0.isDebugEnabled()     // Catch:{ all -> 0x0147 }
            if (r0 == 0) goto L_0x012e
            org.apache.commons.logging.Log r0 = r6.c     // Catch:{ all -> 0x0147 }
            java.lang.String r1 = "Notifying thread waiting on any pool"
            r0.debug(r1)     // Catch:{ all -> 0x0147 }
        L_0x012e:
            java.util.Queue r0 = r6.i     // Catch:{ all -> 0x0147 }
            java.lang.Object r0 = r0.remove()     // Catch:{ all -> 0x0147 }
            com.agilebinary.a.a.a.c.b.a.k r0 = (com.agilebinary.a.a.a.c.b.a.k) r0     // Catch:{ all -> 0x0147 }
            goto L_0x00f1
        L_0x0137:
            org.apache.commons.logging.Log r1 = r6.c     // Catch:{ all -> 0x0147 }
            boolean r1 = r1.isDebugEnabled()     // Catch:{ all -> 0x0147 }
            if (r1 == 0) goto L_0x00f1
            org.apache.commons.logging.Log r1 = r6.c     // Catch:{ all -> 0x0147 }
            java.lang.String r2 = "Notifying no-one, there are no waiting threads"
            r1.debug(r2)     // Catch:{ all -> 0x0147 }
            goto L_0x00f1
        L_0x0147:
            r0 = move-exception
            java.util.concurrent.locks.Lock r1 = r6.d     // Catch:{ all -> 0x0110 }
            r1.unlock()     // Catch:{ all -> 0x0110 }
            throw r0     // Catch:{ all -> 0x0110 }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.c.b.a.a.xa(com.agilebinary.a.a.a.c.b.a.g, boolean, long, java.util.concurrent.TimeUnit):void");
    }

    private final void xb() {
        this.d.lock();
        try {
            this.n = 3;
        } finally {
            this.d.unlock();
        }
    }

    private void xb(g gVar) {
        c d2 = gVar.d();
        if (this.c.isDebugEnabled()) {
            this.c.debug("Deleting connection [" + d2 + "][" + gVar.a() + "]");
        }
        this.d.lock();
        try {
            a(gVar);
            c a2 = a(d2);
            a2.c(gVar);
            this.o--;
            if (a2.c()) {
                this.j.remove(d2);
            }
        } finally {
            this.d.unlock();
        }
    }

    /* access modifiers changed from: protected */
    public final g a(c cVar, Object obj, long j2, TimeUnit timeUnit, f fVar) {
        return xa(cVar, obj, j2, timeUnit, fVar);
    }

    public final void a() {
        xa();
    }

    public final void a(long j2, TimeUnit timeUnit) {
        xa(j2, timeUnit);
    }

    public final void a(g gVar, boolean z, long j2, TimeUnit timeUnit) {
        xa(gVar, z, j2, timeUnit);
    }

    public final void b() {
        xb();
    }
}
