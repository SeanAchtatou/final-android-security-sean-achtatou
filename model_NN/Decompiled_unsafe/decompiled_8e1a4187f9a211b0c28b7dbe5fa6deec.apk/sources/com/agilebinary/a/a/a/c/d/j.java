package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.a.h;
import com.agilebinary.a.a.a.b;
import com.agilebinary.a.a.a.d.g;
import java.util.HashMap;

public final class j implements g {

    /* renamed from: a  reason: collision with root package name */
    private final HashMap f71a = new HashMap();

    private final h xa(b bVar) {
        if (bVar != null) {
            return (h) this.f71a.get(bVar);
        }
        throw new IllegalArgumentException("HTTP host may not be null");
    }

    private final void xa(b bVar, h hVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("HTTP host may not be null");
        }
        this.f71a.put(bVar, hVar);
    }

    private final void xb(b bVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("HTTP host may not be null");
        }
        this.f71a.remove(bVar);
    }

    private final String xtoString() {
        return this.f71a.toString();
    }

    public final h a(b bVar) {
        return xa(bVar);
    }

    public final void a(b bVar, h hVar) {
        xa(bVar, hVar);
    }

    public final void b(b bVar) {
        xb(bVar);
    }

    public final String toString() {
        return xtoString();
    }
}
