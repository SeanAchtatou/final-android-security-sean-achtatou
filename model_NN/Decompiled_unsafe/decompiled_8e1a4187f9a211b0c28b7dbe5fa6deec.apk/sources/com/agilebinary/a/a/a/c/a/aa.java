package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.b.a;
import com.agilebinary.a.a.a.b.f;
import com.agilebinary.a.a.a.b.i;
import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.m;
import com.agilebinary.a.a.a.o;
import java.util.ArrayList;

public final class aa {

    /* renamed from: a  reason: collision with root package name */
    public static final aa f23a = new aa();
    private static final char[] b = {';'};
    private final f c = f.f11a;

    public static m a(c cVar, i iVar) {
        return xa(cVar, iVar);
    }

    private static m xa(c cVar, i iVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Char array buffer may not be null");
        } else if (iVar == null) {
            throw new IllegalArgumentException("Parser cursor may not be null");
        } else {
            o a2 = f.a(cVar, iVar, b);
            ArrayList arrayList = new ArrayList();
            while (!iVar.c()) {
                arrayList.add(f.a(cVar, iVar, b));
            }
            return new a(a2.a(), a2.b(), (o[]) arrayList.toArray(new o[arrayList.size()]));
        }
    }
}
