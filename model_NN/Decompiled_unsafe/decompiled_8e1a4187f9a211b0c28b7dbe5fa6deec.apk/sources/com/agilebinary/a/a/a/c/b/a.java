package com.agilebinary.a.a.a.c.b;

import com.agilebinary.a.a.a.b;
import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.h.c.c;
import com.agilebinary.a.a.a.h.c.f;
import com.agilebinary.a.a.a.h.g;
import com.agilebinary.a.a.a.h.m;
import java.io.IOException;

public abstract class a {

    /* renamed from: a  reason: collision with root package name */
    protected final g f35a;
    protected volatile c b;
    protected volatile f c;
    private m d;
    private volatile Object e;

    protected a(m mVar, c cVar) {
        if (mVar == null) {
            throw new IllegalArgumentException("Connection operator may not be null");
        }
        this.d = mVar;
        this.f35a = mVar.a();
        this.b = cVar;
        this.c = null;
    }

    private final Object xa() {
        return this.e;
    }

    private final void xa(e eVar) {
        if (eVar == null) {
            throw new IllegalArgumentException("Parameters must not be null.");
        } else if (this.c == null || !this.c.g()) {
            throw new IllegalStateException("Connection not open.");
        } else if (!this.c.d()) {
            throw new IllegalStateException("Protocol layering without a tunnel not supported.");
        } else if (this.c.e()) {
            throw new IllegalStateException("Multiple protocol layering not supported.");
        } else {
            this.d.a(this.f35a, this.c.a(), eVar);
            this.c.c(this.f35a.h_());
        }
    }

    private final void xa(c cVar, e eVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Route must not be null.");
        } else if (eVar == null) {
            throw new IllegalArgumentException("Parameters must not be null.");
        } else if (this.c == null || !this.c.g()) {
            this.c = new f(cVar);
            b g = cVar.g();
            this.d.a(this.f35a, g != null ? g : cVar.a(), cVar.b(), eVar);
            f fVar = this.c;
            if (fVar == null) {
                throw new IOException("Request aborted");
            } else if (g == null) {
                fVar.a(this.f35a.h_());
            } else {
                fVar.a(g, this.f35a.h_());
            }
        } else {
            throw new IllegalStateException("Connection already open.");
        }
    }

    private final void xa(Object obj) {
        this.e = obj;
    }

    private final void xa(boolean z, e eVar) {
        if (eVar == null) {
            throw new IllegalArgumentException("Parameters must not be null.");
        } else if (this.c == null || !this.c.g()) {
            throw new IllegalStateException("Connection not open.");
        } else if (this.c.d()) {
            throw new IllegalStateException("Connection is already tunnelled.");
        } else {
            this.f35a.a(null, this.c.a(), false, eVar);
            this.c.b(false);
        }
    }

    private void xb() {
        this.c = null;
        this.e = null;
    }

    public final Object a() {
        return xa();
    }

    public final void a(e eVar) {
        xa(eVar);
    }

    public final void a(c cVar, e eVar) {
        xa(cVar, eVar);
    }

    public final void a(Object obj) {
        xa(obj);
    }

    public final void a(boolean z, e eVar) {
        xa(z, eVar);
    }

    /* access modifiers changed from: protected */
    public void b() {
        xb();
    }
}
