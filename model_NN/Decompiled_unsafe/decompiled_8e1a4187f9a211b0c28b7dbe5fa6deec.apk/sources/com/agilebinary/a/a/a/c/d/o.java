package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.d.d;
import com.agilebinary.a.a.a.k.a;
import com.agilebinary.a.a.a.k.c;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeSet;

public final class o implements d, Serializable {

    /* renamed from: a  reason: collision with root package name */
    private final TreeSet f74a = new TreeSet(new a());

    private final synchronized List xa() {
        return new ArrayList(this.f74a);
    }

    private final synchronized void xa(c cVar) {
        if (cVar != null) {
            this.f74a.remove(cVar);
            if (!cVar.b(new Date())) {
                this.f74a.add(cVar);
            }
        }
    }

    private final synchronized String xtoString() {
        return this.f74a.toString();
    }

    public final synchronized List a() {
        return xa();
    }

    public final synchronized void a(c cVar) {
        xa(cVar);
    }

    public final synchronized String toString() {
        return xtoString();
    }
}
