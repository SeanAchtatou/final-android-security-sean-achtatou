package com.agilebinary.a.a.a.g;

import com.agilebinary.a.a.a.c;
import com.agilebinary.a.a.a.t;

public abstract class f implements c {

    /* renamed from: a  reason: collision with root package name */
    private t f97a;
    private t b;
    private boolean c;

    protected f() {
    }

    private final void xa(t tVar) {
        this.f97a = tVar;
    }

    private final void xa(boolean z) {
        this.c = z;
    }

    private final void xb(t tVar) {
        this.b = tVar;
    }

    private final t xd() {
        return this.f97a;
    }

    private final t xe() {
        return this.b;
    }

    private final boolean xk_() {
        return this.c;
    }

    public final void a(t tVar) {
        xa(tVar);
    }

    public final void a(boolean z) {
        xa(z);
    }

    public final void b(t tVar) {
        xb(tVar);
    }

    public final t d() {
        return xd();
    }

    public final t e() {
        return xe();
    }

    public final boolean k_() {
        return xk_();
    }
}
