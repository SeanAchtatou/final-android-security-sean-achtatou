package com.agilebinary.a.a.a.g;

import java.io.InputStream;
import java.io.OutputStream;

public final class c extends f {

    /* renamed from: a  reason: collision with root package name */
    private InputStream f95a;
    private long b = -1;

    private final void xa(long j) {
        this.b = j;
    }

    private final void xa(InputStream inputStream) {
        this.f95a = inputStream;
    }

    private final void xa(OutputStream outputStream) {
        if (outputStream == null) {
            throw new IllegalArgumentException("Output stream may not be null");
        }
        InputStream f = f();
        try {
            byte[] bArr = new byte[2048];
            while (true) {
                int read = f.read(bArr);
                if (read != -1) {
                    outputStream.write(bArr, 0, read);
                } else {
                    return;
                }
            }
        } finally {
            f.close();
        }
    }

    private final boolean xa() {
        return false;
    }

    private final long xc() {
        return this.b;
    }

    private final InputStream xf() {
        if (this.f95a != null) {
            return this.f95a;
        }
        throw new IllegalStateException("Content has not been provided");
    }

    private final boolean xg() {
        return this.f95a != null;
    }

    public final void a(long j) {
        xa(j);
    }

    public final void a(InputStream inputStream) {
        xa(inputStream);
    }

    public final void a(OutputStream outputStream) {
        xa(outputStream);
    }

    public final boolean a() {
        return xa();
    }

    public final long c() {
        return xc();
    }

    public final InputStream f() {
        return xf();
    }

    public final boolean g() {
        return xg();
    }
}
