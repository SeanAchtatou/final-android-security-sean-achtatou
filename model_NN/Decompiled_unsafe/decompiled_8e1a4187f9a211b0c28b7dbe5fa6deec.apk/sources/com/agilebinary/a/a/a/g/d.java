package com.agilebinary.a.a.a.g;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;

public final class d extends f implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private byte[] f96a;

    public d(byte[] bArr) {
        if (bArr == null) {
            throw new IllegalArgumentException("Source byte array may not be null");
        }
        this.f96a = bArr;
    }

    private final void xa(OutputStream outputStream) {
        if (outputStream == null) {
            throw new IllegalArgumentException("Output stream may not be null");
        }
        outputStream.write(this.f96a);
        outputStream.flush();
    }

    private final boolean xa() {
        return true;
    }

    private final long xc() {
        return (long) this.f96a.length;
    }

    private final Object xclone() {
        return super.clone();
    }

    private final InputStream xf() {
        return new ByteArrayInputStream(this.f96a);
    }

    private final boolean xg() {
        return false;
    }

    public final void a(OutputStream outputStream) {
        xa(outputStream);
    }

    public final boolean a() {
        return xa();
    }

    public final long c() {
        return xc();
    }

    public final Object clone() {
        return xclone();
    }

    public final InputStream f() {
        return xf();
    }

    public final boolean g() {
        return xg();
    }
}
