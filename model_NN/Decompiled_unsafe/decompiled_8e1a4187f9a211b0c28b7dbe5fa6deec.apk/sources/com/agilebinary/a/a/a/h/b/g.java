package com.agilebinary.a.a.a.h.b;

import com.agilebinary.a.a.a.i.f;
import java.util.Locale;

public final class g {

    /* renamed from: a  reason: collision with root package name */
    private final String f103a;
    private final i b;
    private final int c;
    private final boolean d;
    private String e;

    public g(String str, int i, i iVar) {
        if (str == null) {
            throw new IllegalArgumentException("Scheme name may not be null");
        } else if (i <= 0 || i > 65535) {
            throw new IllegalArgumentException("Port is invalid: " + i);
        } else if (iVar == null) {
            throw new IllegalArgumentException("Socket factory may not be null");
        } else {
            this.f103a = str.toLowerCase(Locale.ENGLISH);
            this.b = iVar;
            this.c = i;
            this.d = iVar instanceof f;
        }
    }

    public g(String str, a aVar, int i) {
        if (str == null) {
            throw new IllegalArgumentException("Scheme name may not be null");
        } else if (aVar == null) {
            throw new IllegalArgumentException("Socket factory may not be null");
        } else if (i <= 0 || i > 65535) {
            throw new IllegalArgumentException("Port is invalid: " + i);
        } else {
            this.f103a = str.toLowerCase(Locale.ENGLISH);
            if (aVar instanceof j) {
                this.b = new c((j) aVar);
                this.d = true;
            } else {
                this.b = new d(aVar);
                this.d = false;
            }
            this.c = i;
        }
    }

    private final int xa() {
        return this.c;
    }

    private final int xa(int i) {
        return i <= 0 ? this.c : i;
    }

    private final i xb() {
        return this.b;
    }

    private final String xc() {
        return this.f103a;
    }

    private final boolean xd() {
        return this.d;
    }

    private final boolean xequals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof g)) {
            return false;
        }
        g gVar = (g) obj;
        return this.f103a.equals(gVar.f103a) && this.c == gVar.c && this.d == gVar.d && this.b.equals(gVar.b);
    }

    private final int xhashCode() {
        return f.a((f.a(this.c + 629, this.f103a) * 37) + (this.d ? 1 : 0), this.b);
    }

    private final String xtoString() {
        if (this.e == null) {
            this.e = this.f103a + ':' + Integer.toString(this.c);
        }
        return this.e;
    }

    public final int a() {
        return xa();
    }

    public final int a(int i) {
        return xa(i);
    }

    public final i b() {
        return xb();
    }

    public final String c() {
        return xc();
    }

    public final boolean d() {
        return xd();
    }

    public final boolean equals(Object obj) {
        return xequals(obj);
    }

    public final int hashCode() {
        return xhashCode();
    }

    public final String toString() {
        return xtoString();
    }
}
