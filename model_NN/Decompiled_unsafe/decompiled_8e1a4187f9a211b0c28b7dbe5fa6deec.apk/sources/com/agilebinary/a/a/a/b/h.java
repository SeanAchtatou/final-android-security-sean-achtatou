package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.m;
import com.agilebinary.a.a.a.t;
import java.io.Serializable;

public final class h implements t, Serializable, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final String f13a;
    private final String b;

    public h(String str, String str2) {
        if (str == null) {
            throw new IllegalArgumentException("Name may not be null");
        }
        this.f13a = str;
        this.b = str2;
    }

    private final String xa() {
        return this.f13a;
    }

    private final String xb() {
        return this.b;
    }

    private final m[] xc() {
        return this.b != null ? f.a(this.b) : new m[0];
    }

    private final Object xclone() {
        return super.clone();
    }

    private final String xtoString() {
        return r.f20a.a((c) null, this).toString();
    }

    public final String a() {
        return xa();
    }

    public final String b() {
        return xb();
    }

    public final m[] c() {
        return xc();
    }

    public final Object clone() {
        return xclone();
    }

    public final String toString() {
        return xtoString();
    }
}
