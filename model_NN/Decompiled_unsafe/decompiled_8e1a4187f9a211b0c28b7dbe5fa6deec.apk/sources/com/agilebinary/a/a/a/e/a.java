package com.agilebinary.a.a.a.e;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class a extends d implements Serializable, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final HashMap f90a = new HashMap();

    private e xa(String str, Object obj) {
        this.f90a.put(str, obj);
        return this;
    }

    private Object xa(String str) {
        return this.f90a.get(str);
    }

    private Object xclone() {
        a aVar = (a) super.clone();
        for (Map.Entry entry : this.f90a.entrySet()) {
            if (entry.getKey() instanceof String) {
                aVar.a((String) entry.getKey(), entry.getValue());
            }
        }
        return aVar;
    }

    public e a(String str, Object obj) {
        return xa(str, obj);
    }

    public Object a(String str) {
        return xa(str);
    }

    public Object clone() {
        return xclone();
    }
}
