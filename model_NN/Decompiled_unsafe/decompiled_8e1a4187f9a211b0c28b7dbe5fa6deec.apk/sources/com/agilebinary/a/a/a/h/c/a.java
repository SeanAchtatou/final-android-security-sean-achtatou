package com.agilebinary.a.a.a.h.c;

public final class a implements d {
    private static int b(b bVar, b bVar2) {
        return xb(bVar, bVar2);
    }

    private final int xa(b bVar, b bVar2) {
        if (bVar == null) {
            throw new IllegalArgumentException("Planned route may not be null.");
        } else if (bVar2 == null || bVar2.c() <= 0) {
            return bVar.c() > 1 ? 2 : 1;
        } else {
            if (bVar.c() > 1) {
                return b(bVar, bVar2);
            }
            if (bVar2.c() > 1) {
                return -1;
            }
            if (!bVar.a().equals(bVar2.a())) {
                return -1;
            }
            if (bVar.f() != bVar2.f()) {
                return -1;
            }
            return (bVar.b() == null || bVar.b().equals(bVar2.b())) ? 0 : -1;
        }
    }

    private static int xb(b bVar, b bVar2) {
        if (bVar2.c() <= 1) {
            return -1;
        }
        if (!bVar.a().equals(bVar2.a())) {
            return -1;
        }
        int c = bVar.c();
        int c2 = bVar2.c();
        if (c < c2) {
            return -1;
        }
        for (int i = 0; i < c2 - 1; i++) {
            if (!bVar.a(i).equals(bVar2.a(i))) {
                return -1;
            }
        }
        if (c > c2) {
            return 4;
        }
        if ((bVar2.d() && !bVar.d()) || (bVar2.e() && !bVar.e())) {
            return -1;
        }
        if (bVar.d() && !bVar2.d()) {
            return 3;
        }
        if (!bVar.e() || bVar2.e()) {
            return bVar.f() != bVar2.f() ? -1 : 0;
        }
        return 5;
    }

    public final int a(b bVar, b bVar2) {
        return xa(bVar, bVar2);
    }
}
