package com.agilebinary.a.a.a.c.b;

import com.agilebinary.a.a.a.q;
import com.agilebinary.a.a.b.a.a;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.logging.Log;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    private final Log f45a = a.a(getClass());
    private final Map b = new HashMap();

    private final void xa() {
        this.b.clear();
    }

    private final void xa(long j) {
        long currentTimeMillis = System.currentTimeMillis() - j;
        if (this.f45a.isDebugEnabled()) {
            this.f45a.debug("Checking for connections, idle timeout: " + currentTimeMillis);
        }
        for (Map.Entry entry : this.b.entrySet()) {
            q qVar = (q) entry.getKey();
            long a2 = ((o) entry.getValue()).f53a;
            if (a2 <= currentTimeMillis) {
                if (this.f45a.isDebugEnabled()) {
                    this.f45a.debug("Closing idle connection, connection time: " + a2);
                }
                try {
                    qVar.k();
                } catch (IOException e) {
                    this.f45a.debug("I/O error closing connection", e);
                }
            }
        }
    }

    public final void a() {
        xa();
    }

    public final void a(long j) {
        xa(j);
    }
}
