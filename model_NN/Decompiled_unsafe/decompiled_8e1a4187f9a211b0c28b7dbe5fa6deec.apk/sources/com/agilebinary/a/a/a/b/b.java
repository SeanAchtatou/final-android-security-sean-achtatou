package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.t;
import com.agilebinary.a.a.a.w;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public final class b implements Serializable, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final List f8a = new ArrayList(16);

    private final void xa() {
        this.f8a.clear();
    }

    private final void xa(t tVar) {
        if (tVar != null) {
            this.f8a.add(tVar);
        }
    }

    private final void xa(t[] tVarArr) {
        a();
        if (tVarArr != null) {
            for (t add : tVarArr) {
                this.f8a.add(add);
            }
        }
    }

    private final t[] xa(String str) {
        ArrayList arrayList = new ArrayList();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.f8a.size()) {
                return (t[]) arrayList.toArray(new t[arrayList.size()]);
            }
            t tVar = (t) this.f8a.get(i2);
            if (tVar.a().equalsIgnoreCase(str)) {
                arrayList.add(tVar);
            }
            i = i2 + 1;
        }
    }

    private final t xb(String str) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.f8a.size()) {
                return null;
            }
            t tVar = (t) this.f8a.get(i2);
            if (tVar.a().equalsIgnoreCase(str)) {
                return tVar;
            }
            i = i2 + 1;
        }
    }

    private final void xb(t tVar) {
        if (tVar != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= this.f8a.size()) {
                    this.f8a.add(tVar);
                    return;
                } else if (((t) this.f8a.get(i2)).a().equalsIgnoreCase(tVar.a())) {
                    this.f8a.set(i2, tVar);
                    return;
                } else {
                    i = i2 + 1;
                }
            }
        }
    }

    private final t[] xb() {
        return (t[]) this.f8a.toArray(new t[this.f8a.size()]);
    }

    private final w xc() {
        return new g(this.f8a, null);
    }

    private final boolean xc(String str) {
        for (int i = 0; i < this.f8a.size(); i++) {
            if (((t) this.f8a.get(i)).a().equalsIgnoreCase(str)) {
                return true;
            }
        }
        return false;
    }

    private final Object xclone() {
        b bVar = (b) super.clone();
        bVar.f8a.clear();
        bVar.f8a.addAll(this.f8a);
        return bVar;
    }

    private final w xd(String str) {
        return new g(this.f8a, str);
    }

    private final String xtoString() {
        return this.f8a.toString();
    }

    public final void a() {
        xa();
    }

    public final void a(t tVar) {
        xa(tVar);
    }

    public final void a(t[] tVarArr) {
        xa(tVarArr);
    }

    public final t[] a(String str) {
        return xa(str);
    }

    public final t b(String str) {
        return xb(str);
    }

    public final void b(t tVar) {
        xb(tVar);
    }

    public final t[] b() {
        return xb();
    }

    public final w c() {
        return xc();
    }

    public final boolean c(String str) {
        return xc(str);
    }

    public final Object clone() {
        return xclone();
    }

    public final w d(String str) {
        return xd(str);
    }

    public final String toString() {
        return xtoString();
    }
}
