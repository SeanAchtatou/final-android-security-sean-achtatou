package com.agilebinary.a.a.a.c.c;

import com.agilebinary.a.a.a.j.e;
import java.io.IOException;
import java.io.OutputStream;

public final class j extends OutputStream {

    /* renamed from: a  reason: collision with root package name */
    private final e f64a;
    private final long b;
    private long c = 0;
    private boolean d = false;

    public j(e eVar, long j) {
        if (eVar == null) {
            throw new IllegalArgumentException("Session output buffer may not be null");
        } else if (j < 0) {
            throw new IllegalArgumentException("Content length may not be negative");
        } else {
            this.f64a = eVar;
            this.b = j;
        }
    }

    private final void xclose() {
        if (!this.d) {
            this.d = true;
            this.f64a.b();
        }
    }

    private final void xflush() {
        this.f64a.b();
    }

    private final void xwrite(int i) {
        if (this.d) {
            throw new IOException("Attempted write to closed stream.");
        } else if (this.c < this.b) {
            this.f64a.a(i);
            this.c++;
        }
    }

    private final void xwrite(byte[] bArr) {
        write(bArr, 0, bArr.length);
    }

    private final void xwrite(byte[] bArr, int i, int i2) {
        if (this.d) {
            throw new IOException("Attempted write to closed stream.");
        } else if (this.c < this.b) {
            long j = this.b - this.c;
            int i3 = ((long) i2) > j ? (int) j : i2;
            this.f64a.a(bArr, i, i3);
            this.c += (long) i3;
        }
    }

    public final void close() {
        xclose();
    }

    public final void flush() {
        xflush();
    }

    public final void write(int i) {
        xwrite(i);
    }

    public final void write(byte[] bArr) {
        xwrite(bArr);
    }

    public final void write(byte[] bArr, int i, int i2) {
        xwrite(bArr, i, i2);
    }
}
