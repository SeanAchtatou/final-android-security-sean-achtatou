package com.agilebinary.a.a.b.a;

import com.agilebinary.mobilemonitor.a.a.a.a;
import org.apache.commons.logging.Log;

public final class b implements Log {

    /* renamed from: a  reason: collision with root package name */
    private String f120a;

    public b(String str) {
        this.f120a = str;
    }

    private final void xdebug(Object obj) {
        obj.toString();
    }

    private final void xdebug(Object obj, Throwable th) {
        obj.toString();
        a.b(th);
    }

    private final void xerror(Object obj) {
        obj.toString();
    }

    private final void xerror(Object obj, Throwable th) {
        obj.toString();
        a.e(th);
    }

    private final void xfatal(Object obj) {
        obj.toString();
    }

    private final void xfatal(Object obj, Throwable th) {
        obj.toString();
        a.e(th);
    }

    private final void xinfo(Object obj) {
        obj.toString();
    }

    private final void xinfo(Object obj, Throwable th) {
        obj.toString();
        a.c(th);
    }

    private final boolean xisDebugEnabled() {
        if ("org.apache.http.wire".equals(this.f120a)) {
            return false;
        }
        return "org.apache.http.headers".equals(this.f120a) ? false : false;
    }

    private final boolean xisErrorEnabled() {
        return false;
    }

    private final boolean xisFatalEnabled() {
        return false;
    }

    private final boolean xisInfoEnabled() {
        return false;
    }

    private final boolean xisTraceEnabled() {
        return false;
    }

    private final boolean xisWarnEnabled() {
        return false;
    }

    private final void xtrace(Object obj) {
        obj.toString();
    }

    private final void xtrace(Object obj, Throwable th) {
        obj.toString();
        a.a(th);
    }

    private final void xwarn(Object obj) {
        obj.toString();
    }

    private final void xwarn(Object obj, Throwable th) {
        obj.toString();
        a.d(th);
    }

    public final void debug(Object obj) {
        xdebug(obj);
    }

    public final void debug(Object obj, Throwable th) {
        xdebug(obj, th);
    }

    public final void error(Object obj) {
        xerror(obj);
    }

    public final void error(Object obj, Throwable th) {
        xerror(obj, th);
    }

    public final void fatal(Object obj) {
        xfatal(obj);
    }

    public final void fatal(Object obj, Throwable th) {
        xfatal(obj, th);
    }

    public final void info(Object obj) {
        xinfo(obj);
    }

    public final void info(Object obj, Throwable th) {
        xinfo(obj, th);
    }

    public final boolean isDebugEnabled() {
        return xisDebugEnabled();
    }

    public final boolean isErrorEnabled() {
        return xisErrorEnabled();
    }

    public final boolean isFatalEnabled() {
        return xisFatalEnabled();
    }

    public final boolean isInfoEnabled() {
        return xisInfoEnabled();
    }

    public final boolean isTraceEnabled() {
        return xisTraceEnabled();
    }

    public final boolean isWarnEnabled() {
        return xisWarnEnabled();
    }

    public final void trace(Object obj) {
        xtrace(obj);
    }

    public final void trace(Object obj, Throwable th) {
        xtrace(obj, th);
    }

    public final void warn(Object obj) {
        xwarn(obj);
    }

    public final void warn(Object obj, Throwable th) {
        xwarn(obj, th);
    }
}
