package com.agilebinary.a.a.a.f;

import com.agilebinary.a.a.a.ab;
import com.agilebinary.a.a.a.ad;
import com.agilebinary.a.a.a.f;
import java.util.ArrayList;
import java.util.List;

public final class j implements c, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private List f93a = new ArrayList();
    private List b = new ArrayList();

    private final int xa() {
        return this.f93a.size();
    }

    private final ab xa(int i) {
        if (i < 0 || i >= this.f93a.size()) {
            return null;
        }
        return (ab) this.f93a.get(i);
    }

    private final void xa(ab abVar) {
        if (abVar != null) {
            this.f93a.add(abVar);
        }
    }

    private final void xa(ad adVar) {
        if (adVar != null) {
            this.b.add(adVar);
        }
    }

    private final void xa(f fVar, k kVar) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.f93a.size()) {
                ((ab) this.f93a.get(i2)).a(fVar, kVar);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    private final void xa(com.agilebinary.a.a.a.j jVar, k kVar) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.b.size()) {
                ((ad) this.b.get(i2)).a(jVar, kVar);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    private final int xb() {
        return this.b.size();
    }

    private final ad xb(int i) {
        if (i < 0 || i >= this.b.size()) {
            return null;
        }
        return (ad) this.b.get(i);
    }

    private final Object xclone() {
        j jVar = (j) super.clone();
        jVar.f93a.clear();
        jVar.f93a.addAll(this.f93a);
        jVar.b.clear();
        jVar.b.addAll(this.b);
        return jVar;
    }

    public final int a() {
        return xa();
    }

    public final ab a(int i) {
        return xa(i);
    }

    public final void a(ab abVar) {
        xa(abVar);
    }

    public final void a(ad adVar) {
        xa(adVar);
    }

    public final void a(f fVar, k kVar) {
        xa(fVar, kVar);
    }

    public final void a(com.agilebinary.a.a.a.j jVar, k kVar) {
        xa(jVar, kVar);
    }

    public final int b() {
        return xb();
    }

    public final ad b(int i) {
        return xb(i);
    }

    public final Object clone() {
        return xclone();
    }
}
