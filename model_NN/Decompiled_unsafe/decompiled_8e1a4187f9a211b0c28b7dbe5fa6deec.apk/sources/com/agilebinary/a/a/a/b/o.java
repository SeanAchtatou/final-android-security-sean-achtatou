package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.i.f;
import java.io.Serializable;

public final class o implements com.agilebinary.a.a.a.o, Serializable, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final String f17a;
    private final String b;

    public o(String str, String str2) {
        if (str == null) {
            throw new IllegalArgumentException("Name may not be null");
        }
        this.f17a = str;
        this.b = str2;
    }

    private final String xa() {
        return this.f17a;
    }

    private final String xb() {
        return this.b;
    }

    private final Object xclone() {
        return super.clone();
    }

    private final boolean xequals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof com.agilebinary.a.a.a.o)) {
            return false;
        }
        o oVar = (o) obj;
        return this.f17a.equals(oVar.f17a) && f.a(this.b, oVar.b);
    }

    private final int xhashCode() {
        return f.a(f.a(17, this.f17a), this.b);
    }

    private final String xtoString() {
        if (this.b == null) {
            return this.f17a;
        }
        c cVar = new c(this.f17a.length() + 1 + this.b.length());
        cVar.a(this.f17a);
        cVar.a("=");
        cVar.a(this.b);
        return cVar.toString();
    }

    public final String a() {
        return xa();
    }

    public final String b() {
        return xb();
    }

    public final Object clone() {
        return xclone();
    }

    public final boolean equals(Object obj) {
        return xequals(obj);
    }

    public final int hashCode() {
        return xhashCode();
    }

    public final String toString() {
        return xtoString();
    }
}
