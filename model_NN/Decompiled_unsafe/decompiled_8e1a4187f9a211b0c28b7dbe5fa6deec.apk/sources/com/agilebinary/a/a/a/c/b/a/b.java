package com.agilebinary.a.a.a.c.b.a;

import com.agilebinary.a.a.a.h.a.c;
import com.agilebinary.a.a.a.h.b.h;
import com.agilebinary.a.a.a.h.k;
import com.agilebinary.a.a.a.h.m;
import com.agilebinary.a.a.b.a.a;
import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;

public final class b implements k {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final Log f36a;
    private h b;
    private j c;
    private a d;
    private m e;
    private c f;

    public b(h hVar, TimeUnit timeUnit) {
        if (hVar == null) {
            throw new IllegalArgumentException("Scheme registry may not be null");
        }
        this.f36a = a.a(getClass());
        this.b = hVar;
        this.f = new c();
        this.e = new com.agilebinary.a.a.a.c.b.b(hVar);
        this.d = new a(this.e, this.f, timeUnit);
        this.c = this.d;
    }

    private final h xa() {
        return this.b;
    }

    private final com.agilebinary.a.a.a.h.b xa(com.agilebinary.a.a.a.h.c.c cVar, Object obj) {
        return new h(this, new i(this.d, new f(), cVar, obj), cVar);
    }

    private final void xa(long j, TimeUnit timeUnit) {
        if (this.f36a.isDebugEnabled()) {
            this.f36a.debug("Closing connections idle longer than " + j + " " + timeUnit);
        }
        this.d.a(j, timeUnit);
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:39:0x0078=Splitter:B:39:0x0078, B:21:0x003c=Splitter:B:21:0x003c} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void xa(com.agilebinary.a.a.a.h.a r8, long r9, java.util.concurrent.TimeUnit r11) {
        /*
            r7 = this;
            boolean r0 = r8 instanceof com.agilebinary.a.a.a.c.b.a.d
            if (r0 != 0) goto L_0x000c
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Connection class mismatch, connection not obtained from this manager."
            r0.<init>(r1)
            throw r0
        L_0x000c:
            com.agilebinary.a.a.a.c.b.a.d r8 = (com.agilebinary.a.a.a.c.b.a.d) r8
            com.agilebinary.a.a.a.c.b.a r0 = r8.i()
            if (r0 == 0) goto L_0x0022
            com.agilebinary.a.a.a.h.k r0 = r8.h()
            if (r0 == r7) goto L_0x0022
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Connection not obtained from this manager."
            r0.<init>(r1)
            throw r0
        L_0x0022:
            monitor-enter(r8)
            com.agilebinary.a.a.a.c.b.a r1 = r8.i()     // Catch:{ all -> 0x005d }
            com.agilebinary.a.a.a.c.b.a.g r1 = (com.agilebinary.a.a.a.c.b.a.g) r1     // Catch:{ all -> 0x005d }
            if (r1 != 0) goto L_0x002d
            monitor-exit(r8)     // Catch:{ all -> 0x005d }
        L_0x002c:
            return
        L_0x002d:
            boolean r0 = r8.l()     // Catch:{ IOException -> 0x0068 }
            if (r0 == 0) goto L_0x003c
            boolean r0 = r8.r()     // Catch:{ IOException -> 0x0068 }
            if (r0 != 0) goto L_0x003c
            r8.m()     // Catch:{ IOException -> 0x0068 }
        L_0x003c:
            boolean r2 = r8.r()     // Catch:{ all -> 0x005d }
            org.apache.commons.logging.Log r0 = r7.f36a     // Catch:{ all -> 0x005d }
            boolean r0 = r0.isDebugEnabled()     // Catch:{ all -> 0x005d }
            if (r0 == 0) goto L_0x0051
            if (r2 == 0) goto L_0x0060
            org.apache.commons.logging.Log r0 = r7.f36a     // Catch:{ all -> 0x005d }
            java.lang.String r3 = "Released connection is reusable."
            r0.debug(r3)     // Catch:{ all -> 0x005d }
        L_0x0051:
            r8.j()     // Catch:{ all -> 0x005d }
            com.agilebinary.a.a.a.c.b.a.a r0 = r7.d     // Catch:{ all -> 0x005d }
            r3 = r9
            r5 = r11
            r0.a(r1, r2, r3, r5)     // Catch:{ all -> 0x005d }
        L_0x005b:
            monitor-exit(r8)     // Catch:{ all -> 0x005d }
            goto L_0x002c
        L_0x005d:
            r0 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x005d }
            throw r0
        L_0x0060:
            org.apache.commons.logging.Log r0 = r7.f36a     // Catch:{ all -> 0x005d }
            java.lang.String r3 = "Released connection is not reusable."
            r0.debug(r3)     // Catch:{ all -> 0x005d }
            goto L_0x0051
        L_0x0068:
            r0 = move-exception
            org.apache.commons.logging.Log r2 = r7.f36a     // Catch:{ all -> 0x00a0 }
            boolean r2 = r2.isDebugEnabled()     // Catch:{ all -> 0x00a0 }
            if (r2 == 0) goto L_0x0078
            org.apache.commons.logging.Log r2 = r7.f36a     // Catch:{ all -> 0x00a0 }
            java.lang.String r3 = "Exception shutting down released connection."
            r2.debug(r3, r0)     // Catch:{ all -> 0x00a0 }
        L_0x0078:
            boolean r2 = r8.r()     // Catch:{ all -> 0x005d }
            org.apache.commons.logging.Log r0 = r7.f36a     // Catch:{ all -> 0x005d }
            boolean r0 = r0.isDebugEnabled()     // Catch:{ all -> 0x005d }
            if (r0 == 0) goto L_0x008d
            if (r2 == 0) goto L_0x0098
            org.apache.commons.logging.Log r0 = r7.f36a     // Catch:{ all -> 0x005d }
            java.lang.String r3 = "Released connection is reusable."
            r0.debug(r3)     // Catch:{ all -> 0x005d }
        L_0x008d:
            r8.j()     // Catch:{ all -> 0x005d }
            com.agilebinary.a.a.a.c.b.a.a r0 = r7.d     // Catch:{ all -> 0x005d }
            r3 = r9
            r5 = r11
            r0.a(r1, r2, r3, r5)     // Catch:{ all -> 0x005d }
            goto L_0x005b
        L_0x0098:
            org.apache.commons.logging.Log r0 = r7.f36a     // Catch:{ all -> 0x005d }
            java.lang.String r3 = "Released connection is not reusable."
            r0.debug(r3)     // Catch:{ all -> 0x005d }
            goto L_0x008d
        L_0x00a0:
            r0 = move-exception
            r6 = r0
            boolean r2 = r8.r()     // Catch:{ all -> 0x005d }
            org.apache.commons.logging.Log r0 = r7.f36a     // Catch:{ all -> 0x005d }
            boolean r0 = r0.isDebugEnabled()     // Catch:{ all -> 0x005d }
            if (r0 == 0) goto L_0x00b7
            if (r2 == 0) goto L_0x00c2
            org.apache.commons.logging.Log r0 = r7.f36a     // Catch:{ all -> 0x005d }
            java.lang.String r3 = "Released connection is reusable."
            r0.debug(r3)     // Catch:{ all -> 0x005d }
        L_0x00b7:
            r8.j()     // Catch:{ all -> 0x005d }
            com.agilebinary.a.a.a.c.b.a.a r0 = r7.d     // Catch:{ all -> 0x005d }
            r3 = r9
            r5 = r11
            r0.a(r1, r2, r3, r5)     // Catch:{ all -> 0x005d }
            throw r6     // Catch:{ all -> 0x005d }
        L_0x00c2:
            org.apache.commons.logging.Log r0 = r7.f36a     // Catch:{ all -> 0x005d }
            java.lang.String r3 = "Released connection is not reusable."
            r0.debug(r3)     // Catch:{ all -> 0x005d }
            goto L_0x00b7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.c.b.a.b.xa(com.agilebinary.a.a.a.h.a, long, java.util.concurrent.TimeUnit):void");
    }

    private final void xb() {
        this.d.b();
    }

    private final void xc() {
        this.f.a(3);
    }

    private final void xfinalize() {
        try {
            this.f36a.debug("Shutting down");
            this.d.a();
        } finally {
            super.finalize();
        }
    }

    public final h a() {
        return xa();
    }

    public final com.agilebinary.a.a.a.h.b a(com.agilebinary.a.a.a.h.c.c cVar, Object obj) {
        return xa(cVar, obj);
    }

    public final void a(long j, TimeUnit timeUnit) {
        xa(j, timeUnit);
    }

    public final void a(com.agilebinary.a.a.a.h.a aVar, long j, TimeUnit timeUnit) {
        xa(aVar, j, timeUnit);
    }

    public final void b() {
        xb();
    }

    public final void c() {
        xc();
    }

    /* access modifiers changed from: protected */
    public final void finalize() {
        xfinalize();
    }
}
