package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.c;
import com.agilebinary.a.a.a.g.b;
import java.io.InputStream;
import java.io.OutputStream;

final class h extends b {
    private /* synthetic */ k b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    h(k kVar, c cVar) {
        super(cVar);
        this.b = kVar;
    }

    private final void xa(OutputStream outputStream) {
        boolean unused = this.b.d = true;
        super.a(outputStream);
    }

    private final InputStream xf() {
        boolean unused = this.b.d = true;
        return super.f();
    }

    public final void a(OutputStream outputStream) {
        xa(outputStream);
    }

    public final InputStream f() {
        return xf();
    }
}
