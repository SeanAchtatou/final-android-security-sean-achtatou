package com.agilebinary.a.a.a.c;

import com.agilebinary.a.a.a.a;
import com.agilebinary.a.a.a.aa;
import com.agilebinary.a.a.a.c;
import com.agilebinary.a.a.a.f.k;
import com.agilebinary.a.a.a.j;
import com.agilebinary.a.a.a.q;
import com.agilebinary.a.a.a.s;
import com.agilebinary.a.a.a.u;
import com.agilebinary.a.a.a.w;

public final class d implements s {
    private final boolean xa(j jVar, k kVar) {
        if (jVar == null) {
            throw new IllegalArgumentException("HTTP response may not be null.");
        } else if (kVar == null) {
            throw new IllegalArgumentException("HTTP context may not be null.");
        } else {
            q qVar = (q) kVar.a("http.connection");
            if (qVar != null && !qVar.l()) {
                return false;
            }
            c b = jVar.b();
            a a2 = jVar.a().a();
            if (b != null && b.c() < 0 && (!b.k_() || a2.a(aa.c))) {
                return false;
            }
            w d = jVar.d("Connection");
            if (!d.hasNext()) {
                d = jVar.d("Proxy-Connection");
            }
            if (d.hasNext()) {
                try {
                    com.agilebinary.a.a.a.b.d dVar = new com.agilebinary.a.a.a.b.d(d);
                    boolean z = false;
                    while (dVar.hasNext()) {
                        String a3 = dVar.a();
                        if ("Close".equalsIgnoreCase(a3)) {
                            return false;
                        }
                        if ("Keep-Alive".equalsIgnoreCase(a3)) {
                            z = true;
                        }
                    }
                    if (z) {
                        return true;
                    }
                } catch (u e) {
                    return false;
                }
            }
            return !a2.a(aa.c);
        }
    }

    public final boolean a(j jVar, k kVar) {
        return xa(jVar, kVar);
    }
}
