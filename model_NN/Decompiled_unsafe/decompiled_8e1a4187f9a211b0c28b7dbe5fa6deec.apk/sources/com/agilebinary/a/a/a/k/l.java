package com.agilebinary.a.a.a.k;

import java.io.Serializable;
import java.util.Comparator;

public final class l implements Serializable, Comparator {
    private static String a(c cVar) {
        return xa(cVar);
    }

    private static String xa(c cVar) {
        String d = cVar.d();
        if (d == null) {
            d = "/";
        }
        return !d.endsWith("/") ? d + '/' : d;
    }

    /* access modifiers changed from: private */
    /* renamed from: xcompare */
    public final /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
        String a2 = a((c) obj);
        String a3 = a((c) obj2);
        if (!a2.equals(a3)) {
            if (a2.startsWith(a3)) {
                return -1;
            }
            if (a3.startsWith(a2)) {
                return 1;
            }
        }
        return 0;
    }
}
