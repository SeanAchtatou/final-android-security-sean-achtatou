package com.agilebinary.a.a.a.c.b;

import com.agilebinary.a.a.a.f;
import com.agilebinary.a.a.a.f.k;
import com.agilebinary.a.a.a.h.a;
import com.agilebinary.a.a.a.h.g;
import com.agilebinary.a.a.a.j;
import com.agilebinary.a.a.a.p;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;

public abstract class l implements k, a {

    /* renamed from: a  reason: collision with root package name */
    private volatile com.agilebinary.a.a.a.h.k f51a;
    private volatile g b;
    private volatile boolean c = false;
    private volatile boolean d = false;
    private volatile long e = Long.MAX_VALUE;

    protected l(com.agilebinary.a.a.a.h.k kVar, g gVar) {
        this.f51a = kVar;
        this.b = gVar;
    }

    private void a(g gVar) {
        xa(gVar);
    }

    private final synchronized Object xa(String str) {
        g gVar;
        gVar = this.b;
        a(gVar);
        return gVar instanceof k ? ((k) gVar).a(str) : null;
    }

    private final void xa(long j, TimeUnit timeUnit) {
        if (j > 0) {
            this.e = timeUnit.toMillis(j);
        } else {
            this.e = -1;
        }
    }

    private final void xa(f fVar) {
        g gVar = this.b;
        a(gVar);
        this.c = false;
        gVar.a(fVar);
    }

    private void xa(g gVar) {
        if (this.d || gVar == null) {
            throw new n();
        }
    }

    private final void xa(j jVar) {
        g gVar = this.b;
        a(gVar);
        this.c = false;
        gVar.a(jVar);
    }

    private final void xa(p pVar) {
        g gVar = this.b;
        a(gVar);
        this.c = false;
        gVar.a(pVar);
    }

    private final synchronized void xa(String str, Object obj) {
        g gVar = this.b;
        a(gVar);
        if (gVar instanceof k) {
            ((k) gVar).a(str, obj);
        }
    }

    private final boolean xa() {
        g gVar = this.b;
        a(gVar);
        return gVar.h_();
    }

    private final boolean xa(int i) {
        g gVar = this.b;
        a(gVar);
        return gVar.a(i);
    }

    private final synchronized void xb() {
        if (!this.d) {
            this.d = true;
            this.c = false;
            try {
                m();
            } catch (IOException e2) {
            }
            if (this.f51a != null) {
                this.f51a.a(this, this.e, TimeUnit.MILLISECONDS);
            }
        }
    }

    private final void xb(int i) {
        g gVar = this.b;
        a(gVar);
        gVar.b(i);
    }

    private final void xc() {
        g gVar = this.b;
        a(gVar);
        gVar.c();
    }

    private final j xd() {
        g gVar = this.b;
        a(gVar);
        this.c = false;
        return gVar.d();
    }

    private final synchronized void xd_() {
        if (!this.d) {
            this.d = true;
            if (this.f51a != null) {
                this.f51a.a(this, this.e, TimeUnit.MILLISECONDS);
            }
        }
    }

    private final boolean xe() {
        if (this.d) {
            return true;
        }
        g gVar = this.b;
        if (gVar == null) {
            return true;
        }
        return gVar.e();
    }

    private final SSLSession xf() {
        g gVar = this.b;
        a(gVar);
        if (!l()) {
            return null;
        }
        Socket i_ = gVar.i_();
        if (i_ instanceof SSLSocket) {
            return ((SSLSocket) i_).getSession();
        }
        return null;
    }

    private final void xg() {
        this.c = true;
    }

    private com.agilebinary.a.a.a.h.k xh() {
        return this.f51a;
    }

    private synchronized void xj() {
        this.b = null;
        this.f51a = null;
        this.e = Long.MAX_VALUE;
    }

    private final boolean xl() {
        g gVar = this.b;
        if (gVar == null) {
            return false;
        }
        return gVar.l();
    }

    private final g xn() {
        return this.b;
    }

    private final boolean xo() {
        return this.d;
    }

    private final InetAddress xp() {
        g gVar = this.b;
        a(gVar);
        return gVar.p();
    }

    private final int xq() {
        g gVar = this.b;
        a(gVar);
        return gVar.q();
    }

    private final boolean xr() {
        return this.c;
    }

    public final synchronized Object a(String str) {
        return xa(str);
    }

    public final void a(long j, TimeUnit timeUnit) {
        xa(j, timeUnit);
    }

    public final void a(f fVar) {
        xa(fVar);
    }

    public final void a(j jVar) {
        xa(jVar);
    }

    public final void a(p pVar) {
        xa(pVar);
    }

    public final synchronized void a(String str, Object obj) {
        xa(str, obj);
    }

    public final boolean a() {
        return xa();
    }

    public final boolean a(int i) {
        return xa(i);
    }

    public final synchronized void b() {
        xb();
    }

    public final void b(int i) {
        xb(i);
    }

    public final void c() {
        xc();
    }

    public final j d() {
        return xd();
    }

    public final synchronized void d_() {
        xd_();
    }

    public final boolean e() {
        return xe();
    }

    public final SSLSession f() {
        return xf();
    }

    public final void g() {
        xg();
    }

    /* access modifiers changed from: protected */
    public com.agilebinary.a.a.a.h.k h() {
        return xh();
    }

    /* access modifiers changed from: protected */
    public synchronized void j() {
        xj();
    }

    public final boolean l() {
        return xl();
    }

    /* access modifiers changed from: protected */
    public final g n() {
        return xn();
    }

    /* access modifiers changed from: protected */
    public final boolean o() {
        return xo();
    }

    public final InetAddress p() {
        return xp();
    }

    public final int q() {
        return xq();
    }

    public final boolean r() {
        return xr();
    }
}
