package com.agilebinary.a.a.a.c.c;

import com.agilebinary.a.a.a.j.a;
import com.agilebinary.a.a.a.j.g;
import java.io.InputStream;

public final class o extends InputStream {

    /* renamed from: a  reason: collision with root package name */
    private final a f67a;
    private boolean b = false;

    public o(a aVar) {
        if (aVar == null) {
            throw new IllegalArgumentException("Session input buffer may not be null");
        }
        this.f67a = aVar;
    }

    private final int xavailable() {
        if (this.f67a instanceof g) {
            return ((g) this.f67a).a();
        }
        return 0;
    }

    private final void xclose() {
        this.b = true;
    }

    private final int xread() {
        if (this.b) {
            return -1;
        }
        return this.f67a.d();
    }

    private final int xread(byte[] bArr, int i, int i2) {
        if (this.b) {
            return -1;
        }
        return this.f67a.a(bArr, i, i2);
    }

    public final int available() {
        return xavailable();
    }

    public final void close() {
        xclose();
    }

    public final int read() {
        return xread();
    }

    public final int read(byte[] bArr, int i, int i2) {
        return xread(bArr, i, i2);
    }
}
