package com.agilebinary.a.a.a.h.b;

import com.agilebinary.a.a.a.b;
import java.util.concurrent.ConcurrentHashMap;

public final class h {

    /* renamed from: a  reason: collision with root package name */
    private final ConcurrentHashMap f104a = new ConcurrentHashMap();

    private final g xa(b bVar) {
        if (bVar != null) {
            return a(bVar.c());
        }
        throw new IllegalArgumentException("Host must not be null.");
    }

    private final g xa(g gVar) {
        if (gVar != null) {
            return (g) this.f104a.put(gVar.c(), gVar);
        }
        throw new IllegalArgumentException("Scheme must not be null.");
    }

    private final g xa(String str) {
        g b = b(str);
        if (b != null) {
            return b;
        }
        throw new IllegalStateException("Scheme '" + str + "' not registered.");
    }

    private final g xb(String str) {
        if (str != null) {
            return (g) this.f104a.get(str);
        }
        throw new IllegalArgumentException("Name must not be null.");
    }

    public final g a(b bVar) {
        return xa(bVar);
    }

    public final g a(g gVar) {
        return xa(gVar);
    }

    public final g a(String str) {
        return xa(str);
    }

    public final g b(String str) {
        return xb(str);
    }
}
