package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.a;
import com.agilebinary.a.a.a.d;
import com.agilebinary.a.a.a.i.c;
import java.io.Serializable;

public final class q implements d, Serializable, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final a f19a;
    private final String b;
    private final String c;

    public q(String str, String str2, a aVar) {
        if (str == null) {
            throw new IllegalArgumentException("Method must not be null.");
        } else if (str2 == null) {
            throw new IllegalArgumentException("URI must not be null.");
        } else if (aVar == null) {
            throw new IllegalArgumentException("Protocol version must not be null.");
        } else {
            this.b = str;
            this.c = str2;
            this.f19a = aVar;
        }
    }

    private final String xa() {
        return this.b;
    }

    private final a xb() {
        return this.f19a;
    }

    private final String xc() {
        return this.c;
    }

    private final Object xclone() {
        return super.clone();
    }

    private final String xtoString() {
        return r.f20a.a((c) null, this).toString();
    }

    public final String a() {
        return xa();
    }

    public final a b() {
        return xb();
    }

    public final String c() {
        return xc();
    }

    public final Object clone() {
        return xclone();
    }

    public final String toString() {
        return xtoString();
    }
}
