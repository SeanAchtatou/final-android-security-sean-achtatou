package com.agilebinary.a.a.a.f;

import com.agilebinary.a.a.a.f;
import com.agilebinary.a.a.a.j;
import com.agilebinary.a.a.a.z;
import java.io.IOException;

public final class b {
    public static j a(f fVar, z zVar, k kVar) {
        return xa(fVar, zVar, kVar);
    }

    public static void a(f fVar, c cVar, k kVar) {
        xa(fVar, cVar, kVar);
    }

    public static void a(j jVar, c cVar, k kVar) {
        xa(jVar, cVar, kVar);
    }

    private static final void a(z zVar) {
        xa(zVar);
    }

    private static boolean a(f fVar, j jVar) {
        return xa(fVar, jVar);
    }

    private static j b(f fVar, z zVar, k kVar) {
        return xb(fVar, zVar, kVar);
    }

    /* JADX WARNING: Removed duplicated region for block: B:52:0x00cd A[Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.agilebinary.a.a.a.j xa(com.agilebinary.a.a.a.f r7, com.agilebinary.a.a.a.z r8, com.agilebinary.a.a.a.f.k r9) {
        /*
            r5 = 0
            if (r7 != 0) goto L_0x000b
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "HTTP request may not be null"
            r1.<init>(r2)
            throw r1
        L_0x000b:
            if (r8 != 0) goto L_0x0015
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "Client connection may not be null"
            r1.<init>(r2)
            throw r1
        L_0x0015:
            if (r9 != 0) goto L_0x001f
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "HTTP context may not be null"
            r1.<init>(r2)
            throw r1
        L_0x001f:
            if (r7 != 0) goto L_0x002e
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            java.lang.String r2 = "HTTP request may not be null"
            r1.<init>(r2)     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            throw r1     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
        L_0x0029:
            r1 = move-exception
            a(r8)
            throw r1
        L_0x002e:
            if (r8 != 0) goto L_0x003d
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            java.lang.String r2 = "HTTP connection may not be null"
            r1.<init>(r2)     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            throw r1     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
        L_0x0038:
            r1 = move-exception
            a(r8)
            throw r1
        L_0x003d:
            if (r9 != 0) goto L_0x004c
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            java.lang.String r2 = "HTTP context may not be null"
            r1.<init>(r2)     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            throw r1     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
        L_0x0047:
            r1 = move-exception
            a(r8)
            throw r1
        L_0x004c:
            java.lang.String r1 = "http.connection"
            r9.a(r1, r8)     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            java.lang.String r1 = "http.request_sent"
            java.lang.Boolean r2 = java.lang.Boolean.FALSE     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            r9.a(r1, r2)     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            r8.a(r7)     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            boolean r1 = r7 instanceof com.agilebinary.a.a.a.p     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            if (r1 == 0) goto L_0x00ee
            r2 = 1
            com.agilebinary.a.a.a.d r1 = r7.a()     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            com.agilebinary.a.a.a.a r3 = r1.b()     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            r0 = r7
            com.agilebinary.a.a.a.p r0 = (com.agilebinary.a.a.a.p) r0     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            r1 = r0
            boolean r1 = r1.g_()     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            if (r1 == 0) goto L_0x00eb
            com.agilebinary.a.a.a.aa r1 = com.agilebinary.a.a.a.aa.c     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            boolean r1 = r3.a(r1)     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            if (r1 != 0) goto L_0x00eb
            r8.c()     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            com.agilebinary.a.a.a.e.e r1 = r7.g()     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            java.lang.String r3 = "http.protocol.wait-for-continue"
            r4 = 2000(0x7d0, float:2.803E-42)
            int r1 = r1.a(r3, r4)     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            boolean r1 = r8.a(r1)     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            if (r1 == 0) goto L_0x00eb
            com.agilebinary.a.a.a.j r1 = r8.d()     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            boolean r3 = a(r7, r1)     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            if (r3 == 0) goto L_0x009c
            r8.a(r1)     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
        L_0x009c:
            com.agilebinary.a.a.a.h r3 = r1.a()     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            int r3 = r3.b()     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            r4 = 200(0xc8, float:2.8E-43)
            if (r3 >= r4) goto L_0x00e6
            r4 = 100
            if (r3 == r4) goto L_0x00c9
            java.net.ProtocolException r2 = new java.net.ProtocolException     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            r3.<init>()     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            java.lang.String r4 = "Unexpected response: "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            com.agilebinary.a.a.a.h r1 = r1.a()     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            r2.<init>(r1)     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            throw r2     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
        L_0x00c9:
            r1 = r2
            r2 = r5
        L_0x00cb:
            if (r1 == 0) goto L_0x00d4
            r0 = r7
            com.agilebinary.a.a.a.p r0 = (com.agilebinary.a.a.a.p) r0     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            r1 = r0
            r8.a(r1)     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
        L_0x00d4:
            r1 = r2
        L_0x00d5:
            r8.c()     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            java.lang.String r2 = "http.request_sent"
            java.lang.Boolean r3 = java.lang.Boolean.TRUE     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            r9.a(r2, r3)     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
            if (r1 != 0) goto L_0x00e5
            com.agilebinary.a.a.a.j r1 = b(r7, r8, r9)     // Catch:{ IOException -> 0x0029, k -> 0x0038, RuntimeException -> 0x0047 }
        L_0x00e5:
            return r1
        L_0x00e6:
            r2 = 0
            r6 = r2
            r2 = r1
            r1 = r6
            goto L_0x00cb
        L_0x00eb:
            r1 = r2
            r2 = r5
            goto L_0x00cb
        L_0x00ee:
            r1 = r5
            goto L_0x00d5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.f.b.xa(com.agilebinary.a.a.a.f, com.agilebinary.a.a.a.z, com.agilebinary.a.a.a.f.k):com.agilebinary.a.a.a.j");
    }

    private static void xa(f fVar, c cVar, k kVar) {
        if (fVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        } else if (cVar == null) {
            throw new IllegalArgumentException("HTTP processor may not be null");
        } else if (kVar == null) {
            throw new IllegalArgumentException("HTTP context may not be null");
        } else {
            kVar.a("http.request", fVar);
            cVar.a(fVar, kVar);
        }
    }

    private static void xa(j jVar, c cVar, k kVar) {
        if (jVar == null) {
            throw new IllegalArgumentException("HTTP response may not be null");
        } else if (cVar == null) {
            throw new IllegalArgumentException("HTTP processor may not be null");
        } else if (kVar == null) {
            throw new IllegalArgumentException("HTTP context may not be null");
        } else {
            kVar.a("http.response", jVar);
            cVar.a(jVar, kVar);
        }
    }

    private static final void xa(z zVar) {
        try {
            zVar.k();
        } catch (IOException e) {
        }
    }

    private static boolean xa(f fVar, j jVar) {
        if ("HEAD".equalsIgnoreCase(fVar.a().a())) {
            return false;
        }
        int b = jVar.a().b();
        return (b < 200 || b == 204 || b == 304 || b == 205) ? false : true;
    }

    private static j xb(f fVar, z zVar, k kVar) {
        if (fVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        } else if (zVar == null) {
            throw new IllegalArgumentException("HTTP connection may not be null");
        } else if (kVar == null) {
            throw new IllegalArgumentException("HTTP context may not be null");
        } else {
            j jVar = null;
            int i = 0;
            while (true) {
                if (jVar != null && i >= 200) {
                    return jVar;
                }
                j d = zVar.d();
                if (a(fVar, d)) {
                    zVar.a(d);
                }
                jVar = d;
                i = d.a().b();
            }
        }
    }
}
