package com.agilebinary.a.a.a.g;

import com.agilebinary.a.a.a.c;
import com.agilebinary.a.a.a.t;
import java.io.InputStream;
import java.io.OutputStream;

public class b implements c {

    /* renamed from: a  reason: collision with root package name */
    protected c f94a;

    public b(c cVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("wrapped entity must not be null");
        }
        this.f94a = cVar;
    }

    private void xa(OutputStream outputStream) {
        this.f94a.a(outputStream);
    }

    private boolean xa() {
        return this.f94a.a();
    }

    private long xc() {
        return this.f94a.c();
    }

    private final t xd() {
        return this.f94a.d();
    }

    private final t xe() {
        return this.f94a.e();
    }

    private InputStream xf() {
        return this.f94a.f();
    }

    private boolean xg() {
        return this.f94a.g();
    }

    private boolean xk_() {
        return this.f94a.k_();
    }

    public void a(OutputStream outputStream) {
        xa(outputStream);
    }

    public boolean a() {
        return xa();
    }

    public long c() {
        return xc();
    }

    public final t d() {
        return xd();
    }

    public final t e() {
        return xe();
    }

    public InputStream f() {
        return xf();
    }

    public boolean g() {
        return xg();
    }

    public boolean k_() {
        return xk_();
    }
}
