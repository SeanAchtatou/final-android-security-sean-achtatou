package com.agilebinary.a.a.a.c.b.a;

import com.agilebinary.a.a.a.h.c.c;
import java.util.concurrent.TimeUnit;

final class i implements e {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ f f40a;
    private /* synthetic */ c b;
    private /* synthetic */ Object c;
    private /* synthetic */ a d;

    i(a aVar, f fVar, c cVar, Object obj) {
        this.d = aVar;
        this.f40a = fVar;
        this.b = cVar;
        this.c = obj;
    }

    private final g xa(long j, TimeUnit timeUnit) {
        return this.d.a(this.b, this.c, j, timeUnit, this.f40a);
    }

    private final void xa() {
        this.d.d.lock();
        try {
            this.f40a.a();
        } finally {
            this.d.d.unlock();
        }
    }

    public final g a(long j, TimeUnit timeUnit) {
        return xa(j, timeUnit);
    }

    public final void a() {
        xa();
    }
}
