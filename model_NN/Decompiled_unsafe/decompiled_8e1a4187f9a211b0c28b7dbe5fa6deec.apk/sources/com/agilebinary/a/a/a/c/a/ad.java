package com.agilebinary.a.a.a.c.a;

import java.lang.ref.SoftReference;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

final class ad {

    /* renamed from: a  reason: collision with root package name */
    private static final ThreadLocal f24a = new a();

    ad() {
    }

    public static SimpleDateFormat a(String str) {
        return xa(str);
    }

    private static SimpleDateFormat xa(String str) {
        HashMap hashMap;
        Map map = (Map) ((SoftReference) f24a.get()).get();
        if (map == null) {
            HashMap hashMap2 = new HashMap();
            f24a.set(new SoftReference(hashMap2));
            hashMap = hashMap2;
        } else {
            hashMap = map;
        }
        SimpleDateFormat simpleDateFormat = (SimpleDateFormat) hashMap.get(str);
        if (simpleDateFormat != null) {
            return simpleDateFormat;
        }
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat(str, Locale.US);
        simpleDateFormat2.setTimeZone(TimeZone.getTimeZone("GMT"));
        hashMap.put(str, simpleDateFormat2);
        return simpleDateFormat2;
    }
}
