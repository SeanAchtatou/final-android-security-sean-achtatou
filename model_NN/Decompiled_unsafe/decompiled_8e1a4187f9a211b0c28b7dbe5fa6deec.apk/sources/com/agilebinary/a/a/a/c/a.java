package com.agilebinary.a.a.a.c;

import com.agilebinary.a.a.a.c.c.l;
import com.agilebinary.a.a.a.c.c.n;
import com.agilebinary.a.a.a.c.e.b;
import com.agilebinary.a.a.a.c.e.c;
import com.agilebinary.a.a.a.c.e.d;
import com.agilebinary.a.a.a.f;
import com.agilebinary.a.a.a.i;
import com.agilebinary.a.a.a.j;
import com.agilebinary.a.a.a.j.e;
import com.agilebinary.a.a.a.p;
import com.agilebinary.a.a.a.z;
import java.io.IOException;

public abstract class a implements z {

    /* renamed from: a  reason: collision with root package name */
    private final c f22a = new c(new b());
    private final com.agilebinary.a.a.a.c.e.a b = new com.agilebinary.a.a.a.c.e.a(new d());
    private com.agilebinary.a.a.a.j.a c = null;
    private e d = null;
    private com.agilebinary.a.a.a.j.b e = null;
    private com.agilebinary.a.a.a.j.d f = null;
    private com.agilebinary.a.a.a.j.c g = null;
    private b h = null;

    private boolean f() {
        return xf();
    }

    private com.agilebinary.a.a.a.j.d xa(com.agilebinary.a.a.a.j.a aVar, i iVar, com.agilebinary.a.a.a.e.e eVar) {
        return new n(aVar, iVar, eVar);
    }

    private void xa(f fVar) {
        if (fVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        }
        a();
        this.g.b(fVar);
        this.h.a();
    }

    private final void xa(com.agilebinary.a.a.a.j.a aVar, e eVar, com.agilebinary.a.a.a.e.e eVar2) {
        if (aVar == null) {
            throw new IllegalArgumentException("Input session buffer may not be null");
        } else if (eVar == null) {
            throw new IllegalArgumentException("Output session buffer may not be null");
        } else {
            this.c = aVar;
            this.d = eVar;
            if (aVar instanceof com.agilebinary.a.a.a.j.b) {
                this.e = (com.agilebinary.a.a.a.j.b) aVar;
            }
            this.f = a(aVar, new c(), eVar2);
            this.g = new l(eVar);
            this.h = new b(aVar.e(), eVar.c());
        }
    }

    private final void xa(j jVar) {
        if (jVar == null) {
            throw new IllegalArgumentException("HTTP response may not be null");
        }
        a();
        jVar.a(this.b.a(this.c, jVar));
    }

    private final void xa(p pVar) {
        if (pVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        }
        a();
        if (pVar.h() != null) {
            this.f22a.a(this.d, pVar, pVar.h());
        }
    }

    private final boolean xa(int i) {
        a();
        return this.c.a(i);
    }

    private final void xb() {
        this.d.b();
    }

    private final void xc() {
        a();
        b();
    }

    private j xd() {
        a();
        j jVar = (j) this.f.a();
        if (jVar.a().b() >= 200) {
            this.h.b();
        }
        return jVar;
    }

    private final boolean xe() {
        if (!l()) {
            return true;
        }
        if (f()) {
            return true;
        }
        try {
            this.c.a(1);
            return f();
        } catch (IOException e2) {
            return true;
        }
    }

    private boolean xf() {
        return this.e != null && this.e.f();
    }

    /* access modifiers changed from: protected */
    public com.agilebinary.a.a.a.j.d a(com.agilebinary.a.a.a.j.a aVar, i iVar, com.agilebinary.a.a.a.e.e eVar) {
        return xa(aVar, iVar, eVar);
    }

    /* access modifiers changed from: protected */
    public abstract void a();

    public void a(f fVar) {
        xa(fVar);
    }

    /* access modifiers changed from: protected */
    public final void a(com.agilebinary.a.a.a.j.a aVar, e eVar, com.agilebinary.a.a.a.e.e eVar2) {
        xa(aVar, eVar, eVar2);
    }

    public final void a(j jVar) {
        xa(jVar);
    }

    public final void a(p pVar) {
        xa(pVar);
    }

    public final boolean a(int i) {
        return xa(i);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        xb();
    }

    public final void c() {
        xc();
    }

    public j d() {
        return xd();
    }

    public final boolean e() {
        return xe();
    }
}
