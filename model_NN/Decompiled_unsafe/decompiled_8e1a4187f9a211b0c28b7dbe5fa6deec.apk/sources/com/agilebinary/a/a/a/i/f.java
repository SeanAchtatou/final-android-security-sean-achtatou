package com.agilebinary.a.a.a.i;

public final class f {
    private f() {
    }

    public static int a(int i, Object obj) {
        return xa(i, obj);
    }

    public static boolean a(Object obj, Object obj2) {
        return xa(obj, obj2);
    }

    private static int xa(int i, Object obj) {
        return (obj != null ? obj.hashCode() : 0) + (i * 37);
    }

    private static boolean xa(Object obj, Object obj2) {
        return obj == null ? obj2 == null : obj.equals(obj2);
    }
}
