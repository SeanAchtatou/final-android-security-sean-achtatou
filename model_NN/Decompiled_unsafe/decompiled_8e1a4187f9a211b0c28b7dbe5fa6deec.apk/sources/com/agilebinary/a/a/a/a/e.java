package com.agilebinary.a.a.a.a;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    private h f5a;
    private c b;
    private g c;

    private final void xa() {
        this.f5a = null;
        this.b = null;
        this.c = null;
    }

    private final void xa(c cVar) {
        this.b = cVar;
    }

    private final void xa(g gVar) {
        this.c = gVar;
    }

    private final void xa(h hVar) {
        if (hVar == null) {
            a();
        } else {
            this.f5a = hVar;
        }
    }

    private final boolean xb() {
        return this.f5a != null;
    }

    private final h xc() {
        return this.f5a;
    }

    private final g xd() {
        return this.c;
    }

    private final c xe() {
        return this.b;
    }

    private final String xtoString() {
        StringBuilder sb = new StringBuilder();
        sb.append("auth scope [");
        sb.append(this.b);
        sb.append("]; credentials set [");
        sb.append(this.c != null ? "true" : "false");
        sb.append("]");
        return sb.toString();
    }

    public final void a() {
        xa();
    }

    public final void a(c cVar) {
        xa(cVar);
    }

    public final void a(g gVar) {
        xa(gVar);
    }

    public final void a(h hVar) {
        xa(hVar);
    }

    public final boolean b() {
        return xb();
    }

    public final h c() {
        return xc();
    }

    public final g d() {
        return xd();
    }

    public final c e() {
        return xe();
    }

    public final String toString() {
        return xtoString();
    }
}
