package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.h.c.c;

public final class m {

    /* renamed from: a  reason: collision with root package name */
    private l f72a;
    private c b;

    public m(l lVar, c cVar) {
        this.f72a = lVar;
        this.b = cVar;
    }

    private final l xa() {
        return this.f72a;
    }

    private final c xb() {
        return this.b;
    }

    public final l a() {
        return xa();
    }

    public final c b() {
        return xb();
    }
}
