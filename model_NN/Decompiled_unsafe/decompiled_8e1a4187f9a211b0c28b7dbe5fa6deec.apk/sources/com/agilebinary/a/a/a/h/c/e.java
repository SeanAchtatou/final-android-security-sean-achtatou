package com.agilebinary.a.a.a.h.c;

public enum e {
    PLAIN,
    LAYERED;

    private static e xvalueOf(String str) {
        return (e) Enum.valueOf(e.class, str);
    }

    private static e[] xvalues() {
        return (e[]) c.clone();
    }
}
