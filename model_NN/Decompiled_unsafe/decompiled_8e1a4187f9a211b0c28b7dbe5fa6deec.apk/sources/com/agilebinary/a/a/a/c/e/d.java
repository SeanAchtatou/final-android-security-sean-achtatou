package com.agilebinary.a.a.a.c.e;

import com.agilebinary.a.a.a.g.a;
import com.agilebinary.a.a.a.l;
import com.agilebinary.a.a.a.m;
import com.agilebinary.a.a.a.t;
import com.agilebinary.a.a.a.u;
import com.agilebinary.a.a.a.x;

public final class d implements a {
    private final long xa(x xVar) {
        long j;
        if (xVar == null) {
            throw new IllegalArgumentException("HTTP message may not be null");
        }
        boolean c = xVar.g().c("http.protocol.strict-transfer-encoding");
        t c2 = xVar.c("Transfer-Encoding");
        t c3 = xVar.c("Content-Length");
        if (c2 != null) {
            try {
                m[] c4 = c2.c();
                if (c) {
                    int i = 0;
                    while (i < c4.length) {
                        String a2 = c4[i].a();
                        if (a2 == null || a2.length() <= 0 || a2.equalsIgnoreCase("chunked") || a2.equalsIgnoreCase("identity")) {
                            i++;
                        } else {
                            throw new l("Unsupported transfer encoding: " + a2);
                        }
                    }
                }
                int length = c4.length;
                if ("identity".equalsIgnoreCase(c2.b())) {
                    return -1;
                }
                if (length > 0 && "chunked".equalsIgnoreCase(c4[length - 1].a())) {
                    return -2;
                }
                if (!c) {
                    return -1;
                }
                throw new l("Chunk-encoding must be the last one applied");
            } catch (u e) {
                throw new l("Invalid Transfer-Encoding header value: " + c2, e);
            }
        } else if (c3 == null) {
            return -1;
        } else {
            t[] b = xVar.b("Content-Length");
            if (!c || b.length <= 1) {
                int length2 = b.length - 1;
                while (true) {
                    if (length2 < 0) {
                        j = -1;
                        break;
                    }
                    t tVar = b[length2];
                    try {
                        j = Long.parseLong(tVar.b());
                        break;
                    } catch (NumberFormatException e2) {
                        if (c) {
                            throw new l("Invalid content length: " + tVar.b());
                        }
                        length2--;
                    }
                }
                if (j < 0) {
                    return -1;
                }
                return j;
            }
            throw new l("Multiple content length headers");
        }
    }

    public final long a(x xVar) {
        return xa(xVar);
    }
}
