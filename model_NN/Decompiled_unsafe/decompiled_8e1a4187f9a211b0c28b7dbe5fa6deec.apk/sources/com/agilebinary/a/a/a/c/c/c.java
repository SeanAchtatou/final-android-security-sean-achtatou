package com.agilebinary.a.a.a.c.c;

import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.j.b;
import java.io.InterruptedIOException;
import java.net.Socket;

public final class c extends b implements b {

    /* renamed from: a  reason: collision with root package name */
    private static final Class f58a = g();
    private final Socket b;
    private boolean c;

    public c(Socket socket, int i, e eVar) {
        if (socket == null) {
            throw new IllegalArgumentException("Socket may not be null");
        }
        this.b = socket;
        this.c = false;
        int receiveBufferSize = i < 0 ? socket.getReceiveBufferSize() : i;
        a(socket.getInputStream(), receiveBufferSize < 1024 ? 1024 : receiveBufferSize, eVar);
    }

    private static Class g() {
        return xg();
    }

    private final boolean xa(int i) {
        boolean c2 = c();
        if (!c2) {
            int soTimeout = this.b.getSoTimeout();
            try {
                this.b.setSoTimeout(i);
                b();
                c2 = c();
            } catch (InterruptedIOException e) {
                if (!(f58a != null ? f58a.isInstance(e) : true)) {
                    throw e;
                }
            } finally {
                this.b.setSoTimeout(soTimeout);
            }
        }
        return c2;
    }

    private final int xb() {
        int b2 = super.b();
        this.c = b2 == -1;
        return b2;
    }

    private final boolean xf() {
        return this.c;
    }

    private static Class xg() {
        try {
            return Class.forName("java.net.SocketTimeoutException");
        } catch (ClassNotFoundException e) {
            return null;
        }
    }

    public final boolean a(int i) {
        return xa(i);
    }

    /* access modifiers changed from: protected */
    public final int b() {
        return xb();
    }

    public final boolean f() {
        return xf();
    }
}
