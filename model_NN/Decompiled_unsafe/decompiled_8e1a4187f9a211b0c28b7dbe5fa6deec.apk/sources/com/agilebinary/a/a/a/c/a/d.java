package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.k.b;
import com.agilebinary.a.a.a.k.m;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class d implements b, m, Serializable, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final String f27a;
    private Map b;
    private String c;
    private String d;
    private String e;
    private Date f;
    private String g;
    private boolean h;
    private int i;

    public d(String str, String str2) {
        if (str == null) {
            throw new IllegalArgumentException("Name may not be null");
        }
        this.f27a = str;
        this.b = new HashMap();
        this.c = str2;
    }

    private final String xa() {
        return this.f27a;
    }

    private final void xa(int i2) {
        this.i = i2;
    }

    private final void xa(String str) {
        this.d = str;
    }

    private final void xa(String str, String str2) {
        this.b.put(str, str2);
    }

    private final void xa(Date date) {
        this.f = date;
    }

    private final String xb() {
        return this.c;
    }

    private final void xb(String str) {
        if (str != null) {
            this.e = str.toLowerCase(Locale.ENGLISH);
        } else {
            this.e = null;
        }
    }

    private boolean xb(Date date) {
        if (date != null) {
            return this.f != null && this.f.getTime() <= date.getTime();
        }
        throw new IllegalArgumentException("Date may not be null");
    }

    private final String xc() {
        return this.e;
    }

    private final void xc(String str) {
        this.g = str;
    }

    private Object xclone() {
        d dVar = (d) super.clone();
        dVar.b = new HashMap(this.b);
        return dVar;
    }

    private final String xd() {
        return this.g;
    }

    private final String xd(String str) {
        return (String) this.b.get(str);
    }

    private final boolean xe() {
        return this.h;
    }

    private final boolean xe(String str) {
        return this.b.get(str) != null;
    }

    private final void xf() {
        this.h = true;
    }

    private int[] xg() {
        return null;
    }

    private final int xh() {
        return this.i;
    }

    private String xtoString() {
        return "[version: " + Integer.toString(this.i) + "]" + "[name: " + this.f27a + "]" + "[value: " + this.c + "]" + "[domain: " + this.e + "]" + "[path: " + this.g + "]" + "[expiry: " + this.f + "]";
    }

    public final String a() {
        return xa();
    }

    public final void a(int i2) {
        xa(i2);
    }

    public final void a(String str) {
        xa(str);
    }

    public final void a(String str, String str2) {
        xa(str, str2);
    }

    public final void a(Date date) {
        xa(date);
    }

    public final String b() {
        return xb();
    }

    public final void b(String str) {
        xb(str);
    }

    public boolean b(Date date) {
        return xb(date);
    }

    public final String c() {
        return xc();
    }

    public final void c(String str) {
        xc(str);
    }

    public Object clone() {
        return xclone();
    }

    public final String d() {
        return xd();
    }

    public final String d(String str) {
        return xd(str);
    }

    public final boolean e() {
        return xe();
    }

    public final boolean e(String str) {
        return xe(str);
    }

    public final void f() {
        xf();
    }

    public int[] g() {
        return xg();
    }

    public final int h() {
        return xh();
    }

    public String toString() {
        return xtoString();
    }
}
