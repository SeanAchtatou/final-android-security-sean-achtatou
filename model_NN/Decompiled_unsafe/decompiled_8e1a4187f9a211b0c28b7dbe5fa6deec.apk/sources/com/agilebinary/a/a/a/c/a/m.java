package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.k.b;
import com.agilebinary.a.a.a.k.c;
import com.agilebinary.a.a.a.k.f;
import com.agilebinary.a.a.a.k.i;
import com.agilebinary.a.a.a.k.k;

public final class m implements k {
    private final void xa(b bVar, String str) {
        if (bVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        }
        bVar.c((str == null || str.trim().length() == 0) ? "/" : str);
    }

    private final void xa(c cVar, f fVar) {
        if (!b(cVar, fVar)) {
            throw new i("Illegal path attribute \"" + cVar.d() + "\". Path of origin: \"" + fVar.b() + "\"");
        }
    }

    private final boolean xb(c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (fVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else {
            String b = fVar.b();
            String d = cVar.d();
            if (d == null) {
                d = "/";
            }
            if (d.length() > 1 && d.endsWith("/")) {
                d = d.substring(0, d.length() - 1);
            }
            boolean startsWith = b.startsWith(d);
            return (!startsWith || b.length() == d.length() || d.endsWith("/")) ? startsWith : b.charAt(d.length()) == '/';
        }
    }

    public final void a(b bVar, String str) {
        xa(bVar, str);
    }

    public final void a(c cVar, f fVar) {
        xa(cVar, fVar);
    }

    public final boolean b(c cVar, f fVar) {
        return xb(cVar, fVar);
    }
}
