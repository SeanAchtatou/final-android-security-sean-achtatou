package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.k.b;
import com.agilebinary.a.a.a.k.e;
import com.agilebinary.a.a.a.k.f;
import com.agilebinary.a.a.a.k.i;
import com.agilebinary.a.a.a.k.k;
import java.util.Locale;

public final class c implements k {
    private final void xa(b bVar, String str) {
        if (bVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (str == null) {
            throw new e("Missing value for domain attribute");
        } else if (str.trim().length() == 0) {
            throw new e("Blank value for domain attribute");
        } else {
            bVar.b(str);
        }
    }

    private final void xa(com.agilebinary.a.a.a.k.c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (fVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else {
            String a2 = fVar.a();
            String c = cVar.c();
            if (c == null) {
                throw new i("Cookie domain may not be null");
            } else if (c.equals(a2)) {
            } else {
                if (c.indexOf(46) == -1) {
                    throw new i("Domain attribute \"" + c + "\" does not match the host \"" + a2 + "\"");
                } else if (!c.startsWith(".")) {
                    throw new i("Domain attribute \"" + c + "\" violates RFC 2109: domain must start with a dot");
                } else {
                    int indexOf = c.indexOf(46, 1);
                    if (indexOf < 0 || indexOf == c.length() - 1) {
                        throw new i("Domain attribute \"" + c + "\" violates RFC 2109: domain must contain an embedded dot");
                    }
                    String lowerCase = a2.toLowerCase(Locale.ENGLISH);
                    if (!lowerCase.endsWith(c)) {
                        throw new i("Illegal domain attribute \"" + c + "\". Domain of origin: \"" + lowerCase + "\"");
                    } else if (lowerCase.substring(0, lowerCase.length() - c.length()).indexOf(46) != -1) {
                        throw new i("Domain attribute \"" + c + "\" violates RFC 2109: host minus domain may not contain any dots");
                    }
                }
            }
        }
    }

    private final boolean xb(com.agilebinary.a.a.a.k.c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (fVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else {
            String a2 = fVar.a();
            String c = cVar.c();
            if (c == null) {
                return false;
            }
            return a2.equals(c) || (c.startsWith(".") && a2.endsWith(c));
        }
    }

    public final void a(b bVar, String str) {
        xa(bVar, str);
    }

    public final void a(com.agilebinary.a.a.a.k.c cVar, f fVar) {
        xa(cVar, fVar);
    }

    public final boolean b(com.agilebinary.a.a.a.k.c cVar, f fVar) {
        return xb(cVar, fVar);
    }
}
