package com.agilebinary.a.a.a;

import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.i.f;
import java.io.Serializable;
import java.util.Locale;

public final class b implements Serializable, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private String f6a;
    private String b;
    private int c;
    private String d;

    public b(String str, int i) {
        this(str, i, null);
    }

    public b(String str, int i, String str2) {
        if (str == null) {
            throw new IllegalArgumentException("Host name may not be null");
        }
        this.f6a = str;
        this.b = str.toLowerCase(Locale.ENGLISH);
        if (str2 != null) {
            this.d = str2.toLowerCase(Locale.ENGLISH);
        } else {
            this.d = "http";
        }
        this.c = i;
    }

    private final String xa() {
        return this.f6a;
    }

    private final int xb() {
        return this.c;
    }

    private final String xc() {
        return this.d;
    }

    private final Object xclone() {
        return super.clone();
    }

    private final String xd() {
        if (this.c == -1) {
            return this.f6a;
        }
        c cVar = new c(this.f6a.length() + 6);
        cVar.a(this.f6a);
        cVar.a(":");
        cVar.a(Integer.toString(this.c));
        return cVar.toString();
    }

    private final boolean xequals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof b)) {
            return false;
        }
        b bVar = (b) obj;
        return this.b.equals(bVar.b) && this.c == bVar.c && this.d.equals(bVar.d);
    }

    private final int xhashCode() {
        return f.a((f.a(17, this.b) * 37) + this.c, this.d);
    }

    private final String xtoString() {
        c cVar = new c(32);
        cVar.a(this.d);
        cVar.a("://");
        cVar.a(this.f6a);
        if (this.c != -1) {
            cVar.a(':');
            cVar.a(Integer.toString(this.c));
        }
        return cVar.toString();
    }

    public final String a() {
        return xa();
    }

    public final int b() {
        return xb();
    }

    public final String c() {
        return xc();
    }

    public final Object clone() {
        return xclone();
    }

    public final String d() {
        return xd();
    }

    public final boolean equals(Object obj) {
        return xequals(obj);
    }

    public final int hashCode() {
        return xhashCode();
    }

    public final String toString() {
        return xtoString();
    }
}
