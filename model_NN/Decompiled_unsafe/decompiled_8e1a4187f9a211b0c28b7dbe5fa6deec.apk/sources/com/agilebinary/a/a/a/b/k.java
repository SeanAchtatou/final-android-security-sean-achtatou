package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.a;
import com.agilebinary.a.a.a.ac;
import com.agilebinary.a.a.a.c;
import com.agilebinary.a.a.a.h;
import com.agilebinary.a.a.a.j;
import java.util.Locale;

public final class k extends l implements j {
    private h c;
    private c d;
    private ac e;
    private Locale f;

    public k(h hVar, ac acVar, Locale locale) {
        if (hVar == null) {
            throw new IllegalArgumentException("Status line may not be null.");
        }
        this.c = hVar;
        this.e = acVar;
        this.f = locale != null ? locale : Locale.getDefault();
    }

    private final h xa() {
        return this.c;
    }

    private final void xa(c cVar) {
        this.d = cVar;
    }

    private final c xb() {
        return this.d;
    }

    private final a xc() {
        return this.c.a();
    }

    private final String xtoString() {
        return this.c + " " + this.f15a;
    }

    public final h a() {
        return xa();
    }

    public final void a(c cVar) {
        xa(cVar);
    }

    public final c b() {
        return xb();
    }

    public final a c() {
        return xc();
    }

    public final String toString() {
        return xtoString();
    }
}
