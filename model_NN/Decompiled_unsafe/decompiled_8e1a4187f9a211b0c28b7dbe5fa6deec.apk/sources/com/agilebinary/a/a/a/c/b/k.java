package com.agilebinary.a.a.a.c.b;

import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.j.a;
import com.agilebinary.a.a.a.j.b;
import com.agilebinary.a.a.a.j.f;

public final class k implements a, b {

    /* renamed from: a  reason: collision with root package name */
    private final a f50a;
    private final b b;
    private final q c;
    private final String d;

    public k(a aVar, q qVar, String str) {
        this.f50a = aVar;
        this.b = aVar instanceof b ? (b) aVar : null;
        this.c = qVar;
        this.d = str != null ? str : "ASCII";
    }

    private final int xa(c cVar) {
        int a2 = this.f50a.a(cVar);
        if (this.c.a() && a2 >= 0) {
            this.c.b((new String(cVar.b(), cVar.c() - a2, a2) + "\r\n").getBytes(this.d));
        }
        return a2;
    }

    private final int xa(byte[] bArr, int i, int i2) {
        int a2 = this.f50a.a(bArr, i, i2);
        if (this.c.a() && a2 > 0) {
            this.c.b(bArr, i, a2);
        }
        return a2;
    }

    private final boolean xa(int i) {
        return this.f50a.a(i);
    }

    private final int xd() {
        int d2 = this.f50a.d();
        if (this.c.a() && d2 != -1) {
            this.c.b(new byte[]{(byte) d2});
        }
        return d2;
    }

    private final f xe() {
        return this.f50a.e();
    }

    private final boolean xf() {
        if (this.b != null) {
            return this.b.f();
        }
        return false;
    }

    public final int a(c cVar) {
        return xa(cVar);
    }

    public final int a(byte[] bArr, int i, int i2) {
        return xa(bArr, i, i2);
    }

    public final boolean a(int i) {
        return xa(i);
    }

    public final int d() {
        return xd();
    }

    public final f e() {
        return xe();
    }

    public final boolean f() {
        return xf();
    }
}
