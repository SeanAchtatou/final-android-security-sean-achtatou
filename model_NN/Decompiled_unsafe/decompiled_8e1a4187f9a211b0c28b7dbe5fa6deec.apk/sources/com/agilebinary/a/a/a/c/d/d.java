package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.ab;
import com.agilebinary.a.a.a.ad;
import com.agilebinary.a.a.a.d.c;
import com.agilebinary.a.a.a.d.i;
import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.f.b;
import com.agilebinary.a.a.a.f.f;
import com.agilebinary.a.a.a.f.j;
import com.agilebinary.a.a.a.h.c.h;
import com.agilebinary.a.a.a.h.k;
import com.agilebinary.a.a.a.h.n;
import com.agilebinary.a.a.a.s;
import com.agilebinary.a.a.b.a.a;
import java.net.URI;
import org.apache.commons.logging.Log;

public abstract class d {

    /* renamed from: a  reason: collision with root package name */
    private final Log f69a = a.a(getClass());
    private e b;
    private b c;
    private k d;
    private s e;
    private n f;
    private com.agilebinary.a.a.a.k.d g;
    private com.agilebinary.a.a.a.a.a h;
    private j i;
    private f j;
    private com.agilebinary.a.a.a.d.k k;
    private c l;
    private com.agilebinary.a.a.a.d.b m;
    private com.agilebinary.a.a.a.d.b n;
    private com.agilebinary.a.a.a.d.d o;
    private com.agilebinary.a.a.a.d.e p;
    private h q;
    private com.agilebinary.a.a.a.d.f r;

    protected d(k kVar, e eVar) {
        this.b = eVar;
        this.d = kVar;
    }

    private synchronized c A() {
        return xA();
    }

    private synchronized com.agilebinary.a.a.a.d.b B() {
        return xB();
    }

    private synchronized com.agilebinary.a.a.a.d.b C() {
        return xC();
    }

    private synchronized h D() {
        return xD();
    }

    private synchronized com.agilebinary.a.a.a.d.f E() {
        return xE();
    }

    private synchronized j F() {
        return xF();
    }

    private final synchronized com.agilebinary.a.a.a.f.c G() {
        return xG();
    }

    private com.agilebinary.a.a.a.j a(com.agilebinary.a.a.a.b bVar, com.agilebinary.a.a.a.f fVar) {
        return xa(bVar, fVar);
    }

    private synchronized b w() {
        return xw();
    }

    private synchronized s x() {
        return xx();
    }

    private synchronized c xA() {
        if (this.l == null) {
            this.l = new p();
        }
        return this.l;
    }

    private synchronized com.agilebinary.a.a.a.d.b xB() {
        if (this.m == null) {
            this.m = k();
        }
        return this.m;
    }

    private synchronized com.agilebinary.a.a.a.d.b xC() {
        if (this.n == null) {
            this.n = l();
        }
        return this.n;
    }

    private synchronized h xD() {
        if (this.q == null) {
            this.q = o();
        }
        return this.q;
    }

    private synchronized com.agilebinary.a.a.a.d.f xE() {
        if (this.r == null) {
            this.r = p();
        }
        return this.r;
    }

    private synchronized j xF() {
        if (this.i == null) {
            this.i = i();
        }
        return this.i;
    }

    private final synchronized com.agilebinary.a.a.a.f.c xG() {
        if (this.j == null) {
            j F = F();
            int a2 = F.a();
            ab[] abVarArr = new ab[a2];
            for (int i2 = 0; i2 < a2; i2++) {
                abVarArr[i2] = F.a(i2);
            }
            int b2 = F.b();
            ad[] adVarArr = new ad[b2];
            for (int i3 = 0; i3 < b2; i3++) {
                adVarArr[i3] = F.b(i3);
            }
            this.j = new f(abVarArr, adVarArr);
        }
        return this.j;
    }

    private com.agilebinary.a.a.a.j xa(com.agilebinary.a.a.a.b bVar, com.agilebinary.a.a.a.f fVar) {
        com.agilebinary.a.a.a.f.k b2;
        s sVar;
        if (fVar == null) {
            throw new IllegalArgumentException("Request must not be null.");
        }
        synchronized (this) {
            b2 = b();
            sVar = new s(this.f69a, w(), r(), x(), y(), D(), G(), z(), A(), B(), C(), E(), new q(q(), fVar.g()));
        }
        try {
            return sVar.a(bVar, fVar, b2);
        } catch (com.agilebinary.a.a.a.k e2) {
            throw new i(e2);
        }
    }

    private final com.agilebinary.a.a.a.j xa(com.agilebinary.a.a.a.d.c.b bVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Request must not be null.");
        }
        com.agilebinary.a.a.a.b bVar2 = null;
        URI e_ = bVar.e_();
        if (e_.isAbsolute()) {
            String schemeSpecificPart = e_.getSchemeSpecificPart();
            String substring = schemeSpecificPart.substring(2, schemeSpecificPart.length());
            String substring2 = substring.substring(0, substring.indexOf(58) > 0 ? substring.indexOf(58) : substring.indexOf(47) > 0 ? substring.indexOf(47) : substring.indexOf(63) > 0 ? substring.indexOf(63) : substring.length());
            int port = e_.getPort();
            String scheme = e_.getScheme();
            if (substring2 == null || "".equals(substring2)) {
                throw new i("URI does not specify a valid host name: " + e_);
            }
            bVar2 = new com.agilebinary.a.a.a.b(substring2, port, scheme);
        }
        return a(bVar2, bVar);
    }

    private final synchronized void xa(ab abVar) {
        F().a(abVar);
        this.j = null;
    }

    private final synchronized void xa(ad adVar) {
        F().a(adVar);
        this.j = null;
    }

    private final synchronized void xa(c cVar) {
        this.l = cVar;
    }

    private final synchronized e xq() {
        if (this.b == null) {
            this.b = a();
        }
        return this.b;
    }

    private final synchronized k xr() {
        if (this.d == null) {
            this.d = d();
        }
        return this.d;
    }

    private final synchronized com.agilebinary.a.a.a.a.a xs() {
        if (this.h == null) {
            this.h = e();
        }
        return this.h;
    }

    private final synchronized com.agilebinary.a.a.a.k.d xt() {
        if (this.g == null) {
            this.g = f();
        }
        return this.g;
    }

    private final synchronized com.agilebinary.a.a.a.d.d xu() {
        if (this.o == null) {
            this.o = m();
        }
        return this.o;
    }

    private final synchronized com.agilebinary.a.a.a.d.e xv() {
        if (this.p == null) {
            this.p = n();
        }
        return this.p;
    }

    private synchronized b xw() {
        if (this.c == null) {
            this.c = c();
        }
        return this.c;
    }

    private synchronized s xx() {
        if (this.e == null) {
            this.e = g();
        }
        return this.e;
    }

    private synchronized n xy() {
        if (this.f == null) {
            this.f = h();
        }
        return this.f;
    }

    private synchronized com.agilebinary.a.a.a.d.k xz() {
        if (this.k == null) {
            this.k = j();
        }
        return this.k;
    }

    private synchronized n y() {
        return xy();
    }

    private synchronized com.agilebinary.a.a.a.d.k z() {
        return xz();
    }

    /* access modifiers changed from: protected */
    public abstract e a();

    public final com.agilebinary.a.a.a.j a(com.agilebinary.a.a.a.d.c.b bVar) {
        return xa(bVar);
    }

    public final synchronized void a(ab abVar) {
        xa(abVar);
    }

    public final synchronized void a(ad adVar) {
        xa(adVar);
    }

    public final synchronized void a(c cVar) {
        xa(cVar);
    }

    /* access modifiers changed from: protected */
    public abstract com.agilebinary.a.a.a.f.k b();

    /* access modifiers changed from: protected */
    public abstract b c();

    /* access modifiers changed from: protected */
    public abstract k d();

    /* access modifiers changed from: protected */
    public abstract com.agilebinary.a.a.a.a.a e();

    /* access modifiers changed from: protected */
    public abstract com.agilebinary.a.a.a.k.d f();

    /* access modifiers changed from: protected */
    public abstract s g();

    /* access modifiers changed from: protected */
    public abstract n h();

    /* access modifiers changed from: protected */
    public abstract j i();

    /* access modifiers changed from: protected */
    public abstract com.agilebinary.a.a.a.d.k j();

    /* access modifiers changed from: protected */
    public abstract com.agilebinary.a.a.a.d.b k();

    /* access modifiers changed from: protected */
    public abstract com.agilebinary.a.a.a.d.b l();

    /* access modifiers changed from: protected */
    public abstract com.agilebinary.a.a.a.d.d m();

    /* access modifiers changed from: protected */
    public abstract com.agilebinary.a.a.a.d.e n();

    /* access modifiers changed from: protected */
    public abstract h o();

    /* access modifiers changed from: protected */
    public abstract com.agilebinary.a.a.a.d.f p();

    public final synchronized e q() {
        return xq();
    }

    public final synchronized k r() {
        return xr();
    }

    public final synchronized com.agilebinary.a.a.a.a.a s() {
        return xs();
    }

    public final synchronized com.agilebinary.a.a.a.k.d t() {
        return xt();
    }

    public final synchronized com.agilebinary.a.a.a.d.d u() {
        return xu();
    }

    public final synchronized com.agilebinary.a.a.a.d.e v() {
        return xv();
    }
}
