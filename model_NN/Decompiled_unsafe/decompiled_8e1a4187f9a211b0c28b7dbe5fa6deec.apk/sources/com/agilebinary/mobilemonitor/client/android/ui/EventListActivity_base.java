package com.agilebinary.mobilemonitor.client.android.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.client.android.a.j;
import com.agilebinary.mobilemonitor.client.android.a.o;
import com.agilebinary.mobilemonitor.client.android.c.b;
import com.agilebinary.mobilemonitor.client.android.c.c;
import com.agilebinary.mobilemonitor.client.android.ui.a.e;
import com.agilebinary.mobilemonitor.client.android.ui.a.h;
import com.biige.client.android.R;
import com.jasonkostempski.android.calendar.n;
import java.util.Calendar;
import java.util.Date;

public abstract class EventListActivity_base extends BaseLoggedInActivity implements AdapterView.OnItemClickListener, o {
    private static String h = b.a();
    private long A;
    private boolean B;
    private Calendar C;

    /* renamed from: a  reason: collision with root package name */
    protected ListView f201a;
    protected LinearLayout b;
    protected a c;
    protected ba d;
    protected AlertDialog e;
    protected Cursor f;
    private byte i;
    private LinearLayout k;
    private TextView l;
    private ImageButton m;
    private Button n;
    private Button o;
    private ImageButton p;
    private ImageButton q;
    private Calendar r;
    private Calendar s;
    private j t;
    /* access modifiers changed from: private */
    public n u;
    private LinearLayout v;
    private TextView w;
    private int x;
    private ImageView y;
    private Animation z;

    private static Class a(byte b2) {
        return xa(b2);
    }

    private void a(int i2, int i3, int i4) {
        xa(i2, i3, i4);
    }

    private void a(long j, long j2, int i2, boolean z2) {
        xa(j, j2, i2, z2);
    }

    public static void a(Context context, byte b2) {
        xa(context, b2);
    }

    public static void a(Context context, byte b2, e eVar) {
        xa(context, b2, eVar);
    }

    private void a(Calendar calendar) {
        xa(calendar);
    }

    private void l() {
        xl();
    }

    private void m() {
        xm();
    }

    /* access modifiers changed from: private */
    public void n() {
        xn();
    }

    private void o() {
        xo();
    }

    private long[] p() {
        return xp();
    }

    private final int xa() {
        return R.layout.eventlist;
    }

    private static Class xa(byte b2) {
        switch (b2) {
            case 1:
                return EventListActivity_LOC.class;
            case 2:
                return EventListActivity_CLL.class;
            case 3:
                return EventListActivity_SMS.class;
            case 4:
                return EventListActivity_MMS.class;
            case 5:
            case 7:
            default:
                throw new IllegalArgumentException("not yet implemented");
            case 6:
                return EventListActivity_WEB.class;
            case 8:
                return EventListActivity_SYS.class;
            case 9:
                return EventListActivity_APP.class;
        }
    }

    private final void xa(int i2) {
        this.x = i2;
        this.w.setText(i2);
        a(ax.PROGRESS);
    }

    private void xa(int i2, int i3, int i4) {
        "" + i2;
        " / " + i3;
        " / " + i4;
        if (this.r == null) {
            this.r = Calendar.getInstance();
        }
        this.r.set(5, i2);
        this.r.set(2, i3);
        this.r.set(1, i4);
        this.r.set(11, 0);
        this.r.set(12, 0);
        this.r.set(13, 0);
        this.r.set(14, 0);
        o();
    }

    private void xa(long j, long j2, int i2, boolean z2) {
        if (this.r == null) {
            throw new IllegalArgumentException();
        }
        a(i2);
        com.agilebinary.mobilemonitor.client.android.ui.a.b a2 = com.agilebinary.mobilemonitor.client.android.ui.a.b.a(getClass());
        if (a2 != null) {
            a2.a(true);
        }
        new com.agilebinary.mobilemonitor.client.android.ui.a.b(this, this.t, this.i, getClass()).execute(new h(j, j2, z2));
    }

    private static void xa(Context context, byte b2) {
        Intent intent = new Intent(context, a(b2));
        intent.putExtra("EXTRA_EVENT_TYPE", b2);
        context.startActivity(intent);
    }

    private static void xa(Context context, byte b2, e eVar) {
        Intent intent = new Intent(context, a(b2));
        if (eVar != null) {
            intent.putExtra("EXTRA_DOWNLOAD_RESULT", eVar);
        }
        intent.putExtra("EXTRA_EVENT_TYPE", b2);
        intent.setFlags(536870912);
        context.startActivity(intent);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void xa(EventListActivity_base eventListActivity_base, Calendar calendar) {
        eventListActivity_base.a(calendar);
        if (calendar != null) {
            eventListActivity_base.m();
        }
    }

    private void xa(ax axVar) {
        this.k.setVisibility(axVar == ax.NO_EVENTS ? 0 : 8);
        this.f201a.setVisibility(axVar == ax.NO_EVENTS ? 8 : 0);
        this.f201a.setEnabled(axVar == ax.LIST);
        this.v.setVisibility(axVar == ax.PROGRESS ? 0 : 8);
        if (axVar == ax.PROGRESS) {
            this.y.startAnimation(this.z);
        } else {
            this.y.clearAnimation();
        }
        this.m.setEnabled(axVar != ax.PROGRESS);
        this.n.setEnabled(axVar != ax.PROGRESS);
        System.out.println("mFirstDayWithEvents: " + this.C.getTimeInMillis() + " / " + this.C.getTime());
        System.out.println("mCalendar: " + this.r.getTimeInMillis() + " / " + this.r.getTime());
        this.q.setEnabled(axVar != ax.PROGRESS && this.r.after(this.C));
        this.p.setEnabled(axVar != ax.PROGRESS && this.r.before(this.s));
    }

    private void xa(Calendar calendar) {
        "" + calendar;
        if (calendar != null) {
            a(calendar.get(5), calendar.get(2), calendar.get(1));
        }
    }

    private void xa(boolean z2) {
        try {
            com.agilebinary.mobilemonitor.client.android.ui.a.b a2 = com.agilebinary.mobilemonitor.client.android.ui.a.b.a(getClass());
            if (a2 != null) {
                try {
                    a2.a(z2);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            l();
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    private final boolean xa(long j) {
        return j > this.A;
    }

    private void xb_() {
        String string;
        this.c.a();
        long a2 = this.j.a(this.i);
        this.C.setTimeInMillis(a2);
        String string2 = a2 == 0 ? getString(R.string.label_event_filter_range_none_available_unknown) : c.a().b(a2);
        TextView textView = this.l;
        Object[] objArr = new Object[2];
        switch (this.i) {
            case 1:
                string = getString(R.string.label_event_location_singular);
                break;
            case 2:
                string = getString(R.string.label_event_call_singular);
                break;
            case 3:
                string = getString(R.string.label_event_sms_singular);
                break;
            case 4:
                string = getString(R.string.label_event_mms_singular);
                break;
            case 5:
            case 7:
            default:
                string = "";
                break;
            case 6:
                string = getString(R.string.label_event_web_singular);
                break;
            case 8:
                string = getString(R.string.label_event_system_singular);
                break;
            case 9:
                string = getString(R.string.label_event_application_singular);
                break;
        }
        objArr[0] = string;
        objArr[1] = string2;
        textView.setText(getString(R.string.label_event_filter_range_none_available, objArr));
        l();
    }

    private final void xe() {
        this.r.add(5, 1);
        o();
        m();
    }

    private final void xf() {
        this.r.add(5, -1);
        o();
        m();
    }

    private void xfinish() {
        try {
            a(true);
            this.j.d(this.i);
            this.g.a(this.i, 0);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        super.finish();
    }

    private final void xg() {
        long a2 = this.j.a(this.i);
        this.u = new n(this, new o(this));
        this.u.a(this.r, a2 > 0 ? new Date(a2) : new Date());
        this.u.show();
    }

    private final void xh() {
        this.c.a();
    }

    private void xl() {
        a(this.c.getCount() == 0 ? ax.NO_EVENTS : ax.LIST);
    }

    private void xm() {
        this.j.b(this.i, this.r);
        this.j.c(this.i);
        b_();
        long[] p2 = p();
        a(p2[0], p2[1], R.string.msg_progress_downloading, false);
    }

    private void xn() {
        if (!this.g.d()) {
            a(this.j.a(this.i, p()[0]) + 1, p()[1], R.string.msg_progress_updating, true);
        } else {
            l();
        }
    }

    private void xo() {
        this.n.setText(c.a().b(p()[0]));
    }

    private void xonCreate(Bundle bundle) {
        "onCreate..." + this;
        super.onCreate(bundle);
        this.s = Calendar.getInstance();
        this.s.set(11, 0);
        this.s.set(12, 0);
        this.s.set(13, 0);
        this.s.set(14, 0);
        this.C = Calendar.getInstance();
        this.C.setTimeInMillis(this.j.a(this.i));
        this.x = R.string.msg_progress_loading_eventcache;
        this.f201a = (ListView) findViewById(R.id.eventlist_list);
        this.b = (LinearLayout) findViewById(R.id.eventlist_parent);
        this.k = (LinearLayout) findViewById(R.id.eventlist_no_events);
        this.v = (LinearLayout) findViewById(R.id.eventlist_progress);
        this.w = (TextView) findViewById(R.id.eventlist_progress_text);
        if (this.x != 0) {
            this.w.setText(this.x);
        }
        this.y = (ImageView) findViewById(R.id.eventlist_progress_anim);
        this.z = AnimationUtils.loadAnimation(this, R.anim.spinner_black_76);
        this.o = (Button) findViewById(R.id.eventlist_progress_cancel);
        this.l = (TextView) findViewById(R.id.eventlist_no_events_label);
        this.p = (ImageButton) findViewById(R.id.eventlist_navigation_next);
        this.q = (ImageButton) findViewById(R.id.eventlist_navigation_prev);
        this.m = (ImageButton) findViewById(R.id.eventlist_refresh);
        this.n = (Button) findViewById(R.id.eventlist_pick_date);
        this.t = this.g.b().b();
        this.d = a_();
        this.o.setOnClickListener(new p(this));
        this.p.setOnClickListener(new q(this));
        this.q.setOnClickListener(new r(this));
        this.m.setOnClickListener(new s(this));
        this.n.setOnClickListener(new m(this));
        this.f201a.setOnItemClickListener(this);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage((int) R.string.error_downloading).setCancelable(true).setNeutralButton("OK", new n(this));
        this.e = builder.create();
        if (bundle != null) {
            this.i = bundle.getByte("EXTRA_EVENT_TYPE");
            a(bundle.getInt("EXTRA_DATE_DAY"), bundle.getInt("EXTRA_DATE_MONTH"), bundle.getInt("EXTRA_DATE_YEAR"));
            this.x = bundle.getInt("EXTRA_PROGRESS_TEXT_ID");
            if (bundle.getBoolean("EXTRA_ALERT_DIALOG_IS_SHOWING")) {
                this.e.show();
            }
            if (bundle.getBoolean("EXTRA_CALENDAR_IS_SHOWING")) {
                g();
            }
        } else {
            this.i = getIntent().getByteExtra("EXTRA_EVENT_TYPE", (byte) 0);
            Calendar d2 = this.j.d();
            if (d2 == null) {
                a(Calendar.getInstance());
            } else {
                a(d2);
            }
        }
        this.f = this.j.e(this.i);
        this.c = new a(this, this, this.f, this.d);
        this.f201a.setAdapter((ListAdapter) this.c);
        this.m.setEnabled(false);
        this.n.setEnabled(false);
        this.q.setEnabled(false);
        this.p.setEnabled(false);
    }

    private void xonDestroy() {
        "onDestroy" + this;
        if (this.f != null) {
            this.f.close();
        }
        super.onDestroy();
    }

    private void xonItemClick(AdapterView adapterView, View view, int i2, long j) {
        Class cls;
        a(true);
        long j2 = ((Cursor) adapterView.getItemAtPosition(i2)).getLong(0);
        byte b2 = this.i;
        switch (b2) {
            case 2:
                cls = EventDetailsActivity_CALL.class;
                break;
            case 3:
                cls = EventDetailsActivity_SMS.class;
                break;
            case 4:
                cls = EventDetailsActivity_MMS.class;
                break;
            case 5:
            case 7:
            default:
                throw new IllegalArgumentException("not yet implemented");
            case 6:
                cls = EventDetailsActivity_WEB.class;
                break;
            case 8:
                cls = EventDetailsActivity_SYS.class;
                break;
            case 9:
                cls = EventDetailsActivity_APP.class;
                break;
        }
        Intent intent = new Intent(this, cls);
        intent.putExtra("EXTRA_EVENT_ID", j2);
        intent.putExtra("EXTRA_EVENT_TYPE", b2);
        startActivity(intent);
    }

    private void xonNewIntent(Intent intent) {
        EventListActivity_base eventListActivity_base;
        super.onNewIntent(intent);
        e eVar = (e) intent.getSerializableExtra("EXTRA_DOWNLOAD_RESULT");
        if (eVar == null) {
            eventListActivity_base = this;
        } else if (eVar.a() == null) {
            eventListActivity_base = this;
        } else if (eVar.a().a()) {
            finish();
            this.g.a(this);
            return;
        } else {
            b_();
            if (!eVar.a().c()) {
                this.e.show();
                return;
            }
            return;
        }
        eventListActivity_base.b_();
    }

    private void xonSaveInstanceState(Bundle bundle) {
        bundle.putInt("EXTRA_DATE_DAY", this.r.get(5));
        bundle.putInt("EXTRA_DATE_MONTH", this.r.get(2));
        bundle.putInt("EXTRA_DATE_YEAR", this.r.get(1));
        bundle.putInt("EXTRA_PROGRESS_TEXT_ID", this.x);
        bundle.putByte("EXTRA_EVENT_TYPE", this.i);
        bundle.putBoolean("EXTRA_CALENDAR_IS_SHOWING", this.u != null);
        bundle.putBoolean("EXTRA_ALERT_DIALOG_IS_SHOWING", this.e.isShowing());
        super.onSaveInstanceState(bundle);
    }

    private void xonStart() {
        super.onStart();
        if (this.B) {
            this.B = false;
        } else if (this.i == 0) {
            new Exception().printStackTrace();
        } else {
            this.A = this.j.b(this.i);
            if (this.e.isShowing() || this.u != null) {
                b_();
                return;
            }
            a(this.x);
            com.agilebinary.mobilemonitor.client.android.ui.a.b a2 = com.agilebinary.mobilemonitor.client.android.ui.a.b.a(getClass());
            if (a2 != null && !a2.isCancelled()) {
                return;
            }
            if (!this.j.a(this.i, this.r)) {
                m();
            } else if (this.g.a(this.i)) {
                n();
            } else {
                b_();
            }
        }
    }

    private void xonStop() {
        super.onStop();
        this.B = true;
        if (this.u != null && this.u.isShowing()) {
            this.u.dismiss();
        }
        if (this.e != null && this.e.isShowing()) {
            this.e.dismiss();
        }
    }

    private long[] xp() {
        if (this.r == null) {
            throw new IllegalArgumentException();
        }
        Calendar calendar = (Calendar) this.r.clone();
        calendar.set(11, 0);
        calendar.set(12, 0);
        calendar.set(13, 0);
        calendar.set(14, 0);
        calendar.add(5, 1);
        calendar.add(13, -1);
        long[] jArr = {calendar.getTimeInMillis(), calendar.getTimeInMillis()};
        "" + jArr[0];
        " | " + jArr[1];
        return jArr;
    }

    /* access modifiers changed from: protected */
    public final int a() {
        return xa();
    }

    /* access modifiers changed from: protected */
    public final void a(int i2) {
        xa(i2);
    }

    /* access modifiers changed from: protected */
    public void a(ax axVar) {
        xa(axVar);
    }

    /* access modifiers changed from: protected */
    public void a(boolean z2) {
        xa(z2);
    }

    public final boolean a(long j) {
        return xa(j);
    }

    /* access modifiers changed from: protected */
    public abstract ba a_();

    /* access modifiers changed from: protected */
    public void b_() {
        xb_();
    }

    /* access modifiers changed from: protected */
    public final void e() {
        xe();
    }

    /* access modifiers changed from: protected */
    public final void f() {
        xf();
    }

    public void finish() {
        xfinish();
    }

    /* access modifiers changed from: protected */
    public final void g() {
        xg();
    }

    public final void h() {
        xh();
    }

    public void onCreate(Bundle bundle) {
        xonCreate(bundle);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        xonDestroy();
    }

    public void onItemClick(AdapterView adapterView, View view, int i2, long j) {
        xonItemClick(adapterView, view, i2, j);
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        xonNewIntent(intent);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        xonSaveInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        xonStart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        xonStop();
    }
}
