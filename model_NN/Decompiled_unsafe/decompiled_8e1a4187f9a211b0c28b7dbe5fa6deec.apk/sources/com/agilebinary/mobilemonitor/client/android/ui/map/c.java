package com.agilebinary.mobilemonitor.client.android.ui.map;

import android.graphics.Paint;

final class c {

    /* renamed from: a  reason: collision with root package name */
    int f279a;
    int b;
    float c;
    Paint d;
    Paint e;
    private /* synthetic */ m f;

    public c(m mVar, int i, int i2, float f2, Paint paint, Paint paint2) {
        this.f = mVar;
        this.f279a = i;
        this.b = i2;
        this.c = f2;
        this.d = paint;
        this.e = paint2;
    }

    private final boolean xequals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        c cVar = (c) obj;
        if (!this.f.equals(cVar.f)) {
            return false;
        }
        if (this.d == null) {
            if (cVar.d != null) {
                return false;
            }
        } else if (!this.d.equals(cVar.d)) {
            return false;
        }
        if (this.e == null) {
            if (cVar.e != null) {
                return false;
            }
        } else if (!this.e.equals(cVar.e)) {
            return false;
        }
        if (Float.floatToIntBits(this.c) != Float.floatToIntBits(cVar.c)) {
            return false;
        }
        if (this.f279a != cVar.f279a) {
            return false;
        }
        return this.b == cVar.b;
    }

    private final int xhashCode() {
        return ((((((((((this.f.hashCode() + 31) * 31) + (this.d == null ? 0 : this.d.hashCode())) * 31) + (this.e == null ? 0 : this.e.hashCode())) * 31) + Float.floatToIntBits(this.c)) * 31) + this.f279a) * 31) + this.b;
    }

    public final boolean equals(Object obj) {
        return xequals(obj);
    }

    public final int hashCode() {
        return xhashCode();
    }
}
