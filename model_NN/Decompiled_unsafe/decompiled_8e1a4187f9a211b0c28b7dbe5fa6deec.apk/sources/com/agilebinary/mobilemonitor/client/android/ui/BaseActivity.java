package com.agilebinary.mobilemonitor.client.android.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.client.android.MyApplication;
import com.agilebinary.mobilemonitor.client.android.a.t;
import com.agilebinary.mobilemonitor.client.android.c.b;
import com.biige.client.android.R;
import java.util.Stack;

public abstract class BaseActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private static final String f191a = b.a();
    private static Stack b = new Stack();
    private ProgressDialog c;
    private boolean d;
    private AlertDialog e;
    private int f;
    public MyApplication g;
    private int h;

    public static void k() {
        xk();
    }

    private void xa(int i) {
        this.h = i;
        this.c.setMessage(getString(i));
        this.c.show();
    }

    private void xb() {
    }

    private final void xb(int i) {
        this.f = i;
        this.e.setMessage(getString(i));
        this.e.show();
    }

    private final void xb(boolean z) {
        if (!this.c.isShowing()) {
            return;
        }
        if (z) {
            this.c.cancel();
        } else {
            this.c.dismiss();
        }
    }

    private void xc() {
    }

    private CharSequence xd() {
        return getString(R.string.app_title);
    }

    private void xfinish() {
        b(true);
        if (this.e.isShowing()) {
            this.e.dismiss();
        }
        super.finish();
    }

    private final void xi() {
        this.e.dismiss();
    }

    private boolean xj() {
        return requestWindowFeature(7);
    }

    private static void xk() {
        while (b.size() > 0) {
            "" + b.peek();
            ((BaseActivity) b.pop()).finish();
        }
    }

    private void xonCreate(Bundle bundle) {
        b.push(this);
        super.onCreate(bundle);
        this.g = (MyApplication) getApplication();
        this.d = j();
        setContentView(a());
        if (this.d) {
            getWindow().setFeatureInt(7, R.layout.titlebar);
            TextView textView = (TextView) findViewById(R.id.titlebar_text);
            if (textView != null) {
                textView.setText(d());
            }
        }
        this.c = new ProgressDialog(this);
        this.c.setIndeterminate(true);
        this.c.setCancelable(true);
        this.c.setTitle((int) R.string.label_dialog_title);
        this.c.setOnCancelListener(new d(this));
        this.c.setOnDismissListener(new f(this));
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true).setTitle((int) R.string.label_dialog_title).setNeutralButton((int) R.string.label_ok, new e(this));
        this.e = builder.create();
        if (bundle != null) {
            int i = bundle.getInt("EXTRA_PROGRESS_MSG");
            if (i != 0) {
                a(i);
                return;
            }
            int i2 = bundle.getInt("EXTRA_ERROR_MSG");
            if (i2 != 0) {
                b(i2);
            }
        }
    }

    private boolean xonCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.base, menu);
        MenuItem findItem = menu.findItem(R.id.menu_base_help);
        t.a();
        findItem.setVisible("".trim().length() > 0);
        return true;
    }

    private void xonDestroy() {
        super.onDestroy();
        b.remove(this);
    }

    private boolean xonOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.menu_base_help /*2131296408*/:
                c();
                t.a();
                try {
                    startActivity(new Intent("android.intent.action.VIEW", Uri.parse("")));
                } catch (ActivityNotFoundException e2) {
                }
                return true;
            case R.id.menu_base_about /*2131296409*/:
                c();
                startActivity(new Intent(this, AboutActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    private void xonSaveInstanceState(Bundle bundle) {
        bundle.putInt("EXTRA_PROGRESS_MSG", this.c.isShowing() ? this.h : 0);
        bundle.putInt("EXTRA_ERROR_MSG", this.e.isShowing() ? this.f : 0);
        super.onSaveInstanceState(bundle);
    }

    private void xonStart() {
        super.onStart();
    }

    private void xonStop() {
        b(false);
        if (this.e.isShowing()) {
            this.e.dismiss();
        }
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public abstract int a();

    /* access modifiers changed from: protected */
    public void a(int i) {
        xa(i);
    }

    /* access modifiers changed from: protected */
    public void b() {
        xb();
    }

    /* access modifiers changed from: protected */
    public final void b(int i) {
        xb(i);
    }

    /* access modifiers changed from: protected */
    public final void b(boolean z) {
        xb(z);
    }

    /* access modifiers changed from: protected */
    public void c() {
        xc();
    }

    /* access modifiers changed from: protected */
    public CharSequence d() {
        return xd();
    }

    public void finish() {
        xfinish();
    }

    /* access modifiers changed from: protected */
    public final void i() {
        xi();
    }

    /* access modifiers changed from: protected */
    public boolean j() {
        return xj();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        xonCreate(bundle);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return xonCreateOptionsMenu(menu);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        xonDestroy();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        return xonOptionsItemSelected(menuItem);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        xonSaveInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        xonStart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        xonStop();
    }
}
