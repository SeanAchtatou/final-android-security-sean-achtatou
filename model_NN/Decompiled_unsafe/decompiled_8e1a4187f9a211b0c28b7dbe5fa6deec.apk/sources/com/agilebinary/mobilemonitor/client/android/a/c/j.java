package com.agilebinary.mobilemonitor.client.android.a.c;

import com.agilebinary.mobilemonitor.client.android.a.a.a;
import com.agilebinary.mobilemonitor.client.android.a.a.b;
import java.util.List;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

final class j extends DefaultHandler {

    /* renamed from: a  reason: collision with root package name */
    private List f156a;
    private a b;
    private char[] c = new char[4096];
    private int d = 0;
    private /* synthetic */ i e;

    public j(i iVar, List list) {
        this.e = iVar;
        this.f156a = list;
    }

    private final void xcharacters(char[] cArr, int i, int i2) {
        System.arraycopy(cArr, i, this.c, this.d, i2);
        this.d += i2;
    }

    private final void xendElement(String str, String str2, String str3) {
        String replace = new String(this.c, 0, this.d).trim().replace("\n", "");
        this.d = 0;
        if ("mcc".equals(str2)) {
            this.b.b(Integer.parseInt(replace));
        } else if ("mnc".equals(str2)) {
            this.b.c(Integer.parseInt(replace));
        } else if ("cid".equals(str2)) {
            this.b.d(Integer.parseInt(replace));
        } else if ("lac".equals(str2)) {
            this.b.e(Integer.parseInt(replace));
        } else if ("lat".equals(str2)) {
            this.b.b().a(Double.parseDouble(replace));
        } else if ("lon".equals(str2)) {
            this.b.b().b(Double.parseDouble(replace));
        } else if ("acc".equals(str2)) {
            this.b.b().c(Double.parseDouble(replace));
        }
    }

    private final void xstartElement(String str, String str2, String str3, Attributes attributes) {
        if ("c".equals(str2)) {
            this.b = new a();
            this.b.a(new b());
            this.f156a.add(this.b);
        }
    }

    public final void characters(char[] cArr, int i, int i2) {
        xcharacters(cArr, i, i2);
    }

    public final void endElement(String str, String str2, String str3) {
        xendElement(str, str2, str3);
    }

    public final void startElement(String str, String str2, String str3, Attributes attributes) {
        xstartElement(str, str2, str3, attributes);
    }
}
