package com.agilebinary.mobilemonitor.client.android.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import com.agilebinary.mobilemonitor.client.android.c.b;
import com.agilebinary.mobilemonitor.client.android.ui.a.c;
import com.agilebinary.mobilemonitor.client.android.ui.a.f;
import com.biige.client.android.R;

public class ResetPasswordActivity extends BaseActivity {

    /* renamed from: a  reason: collision with root package name */
    private static final String f206a = b.a();
    /* access modifiers changed from: private */
    public EditText b;
    private Button c;
    private Button d;

    public static void a(Activity activity) {
        xa(activity);
    }

    public static void a(Context context, c cVar) {
        xa(context, cVar);
    }

    private final int xa() {
        return R.layout.reset_password;
    }

    private static void xa(Activity activity) {
        activity.startActivityForResult(new Intent(activity, ResetPasswordActivity.class), 2);
    }

    private static void xa(Context context, c cVar) {
        Intent intent = new Intent(context, ResetPasswordActivity.class);
        intent.putExtra("EXTRA_SERVICE_RESULT", cVar);
        intent.setFlags(536870912);
        context.startActivity(intent);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void xa(ResetPasswordActivity resetPasswordActivity, String str) {
        resetPasswordActivity.a((int) R.string.msg_progress_communicating);
        f fVar = new f(resetPasswordActivity);
        f.f213a = fVar;
        fVar.execute(str);
    }

    private final void xb() {
        if (f.f213a != null) {
            try {
                f.f213a.a();
            } catch (Exception e) {
            }
        }
    }

    private void xfinish() {
        super.finish();
    }

    private void xonCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.b = (EditText) findViewById(R.id.resetpassword_key);
        this.c = (Button) findViewById(R.id.resetpassword_cancel);
        this.d = (Button) findViewById(R.id.resetpassword_ok);
        this.d.setOnClickListener(new aq(this));
        this.c.setOnClickListener(new ap(this));
        if (bundle != null) {
            this.b.setText(bundle.getString("EXTRA_KEY"));
        }
    }

    private void xonNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.b.setText("");
        c cVar = (c) intent.getSerializableExtra("EXTRA_SERVICE_RESULT");
        b(false);
        if (cVar == null) {
            return;
        }
        if (cVar.b() != null) {
            if (cVar.b().c()) {
                b((int) R.string.error_service_account_general);
            } else if (cVar.b().b()) {
                b((int) R.string.error_service_account_general);
            } else if (cVar.b().d()) {
                b((int) R.string.error_password_reset_missing_email);
            } else {
                b((int) R.string.error_service_account_general);
            }
        } else if (cVar.a()) {
            setResult(-1);
            finish();
        }
    }

    private void xonSaveInstanceState(Bundle bundle) {
        bundle.putString("EXTRA_KEY", this.b.getText().toString());
        super.onSaveInstanceState(bundle);
    }

    private void xonStart() {
        super.onStart();
    }

    private void xonStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public final int a() {
        return xa();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        xb();
    }

    public void finish() {
        xfinish();
    }

    public void onCreate(Bundle bundle) {
        xonCreate(bundle);
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        xonNewIntent(intent);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        xonSaveInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        xonStart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        xonStop();
    }
}
