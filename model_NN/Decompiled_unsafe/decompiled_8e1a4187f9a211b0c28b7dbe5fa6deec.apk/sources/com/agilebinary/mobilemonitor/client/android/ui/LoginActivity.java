package com.agilebinary.mobilemonitor.client.android.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.client.android.a.t;
import com.agilebinary.mobilemonitor.client.android.c.b;
import com.agilebinary.mobilemonitor.client.android.c.e;
import com.agilebinary.mobilemonitor.client.android.d;
import com.agilebinary.mobilemonitor.client.android.ui.a.c;
import com.agilebinary.mobilemonitor.client.android.ui.a.i;
import com.biige.client.android.R;
import java.io.UnsupportedEncodingException;
import java.util.Locale;

public class LoginActivity extends BaseActivity {

    /* renamed from: a  reason: collision with root package name */
    private static final String f202a = b.a();
    /* access modifiers changed from: private */
    public static b[] j;
    private Button b;
    private Button c;
    /* access modifiers changed from: private */
    public EditText d;
    /* access modifiers changed from: private */
    public EditText e;
    private TextView f;
    private boolean h;
    private Spinner i;

    static {
        t.a();
        String[] split = "en,zh".split(",");
        j = new b[0];
        int i2 = 0;
        for (String str : split) {
            b bVar = "en".equals(str) ? new b(Locale.UK, R.drawable.flag_uk) : "de".equals(str) ? new b(Locale.GERMANY, R.drawable.flag_de) : "zh".equals(str) ? new b(Locale.CHINA, R.drawable.flag_cn) : null;
            if (bVar != null) {
                b[] bVarArr = new b[(i2 + 1)];
                System.arraycopy(j, 0, bVarArr, 0, j.length);
                j = bVarArr;
                bVarArr[i2] = bVar;
                i2++;
            }
        }
    }

    public static void a(Context context, c cVar) {
        xa(context, cVar);
    }

    private void f() {
        xf();
    }

    private final int xa() {
        return R.layout.login;
    }

    private static void xa(Context context, c cVar) {
        Intent intent = new Intent(context, LoginActivity.class);
        intent.putExtra("EXTRA_LOGIN_RESULT", cVar);
        intent.setFlags(536870912);
        context.startActivity(intent);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void xa(LoginActivity loginActivity, String str, String str2) {
        loginActivity.a((int) R.string.msg_progress_communicating);
        i iVar = new i(loginActivity);
        i.f216a = iVar;
        iVar.execute(str, str2);
    }

    private final void xb() {
        if (i.f216a != null) {
            try {
                i.f216a.a();
            } catch (Exception e2) {
            }
        }
    }

    private final CharSequence xd() {
        return getString(R.string.titlebar_fmt_login, new Object[]{getString(R.string.app_name)});
    }

    private void xf() {
        this.h = true;
        d a2 = this.g.a();
        String str = "";
        try {
            str = e.a(a2.b());
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        }
        if (this.g.d() || !str.equals(a2.c())) {
            onActivityResult(0, -1, null);
        } else {
            ChangePasswordActivity.a(this, R.string.label_changepassword_reason_default, true);
        }
    }

    private void xfinish() {
        super.finish();
    }

    private void xonActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 0) {
            String e2 = this.g.a().e();
            if (this.g.d() || !(e2 == null || e2.trim().length() == 0)) {
                onActivityResult(1, -1, null);
            } else {
                ChangeEmailActivity.a(this, R.string.label_changeemail_text_missing, true);
            }
        } else if (i2 == 1) {
            finish();
            this.h = false;
            startActivity(new Intent(this, MainActivity.class));
        } else if (i2 == 2 && i3 == -1) {
            b((int) R.string.msg_password_reset_description);
        }
    }

    private void xonCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.b = (Button) findViewById(R.id.login_login);
        this.c = (Button) findViewById(R.id.login_demo);
        this.d = (EditText) findViewById(R.id.login_key);
        this.e = (EditText) findViewById(R.id.login_password);
        this.f = (TextView) findViewById(R.id.login_link);
        this.f.setMovementMethod(LinkMovementMethod.getInstance());
        t.a();
        this.f.setText(Html.fromHtml("<a href='http://www.biige.com'>www.biige.com</a>"));
        if (bundle != null) {
            this.d.setText(bundle.getString("EXTRA_KEY"));
            this.e.setText(bundle.getString("EXTRA_PASSWORD"));
        }
        this.b.setOnClickListener(new bf(this));
        this.c.setOnClickListener(new bg(this));
        this.i = (Spinner) findViewById(R.id.login_locale);
        this.i.setAdapter((SpinnerAdapter) new ay(this, this));
        Spinner spinner = this.i;
        int i2 = 0;
        while (true) {
            if (i2 >= j.length) {
                i2 = 0;
                break;
            }
            if (Locale.getDefault().getLanguage().equals(j[i2].f241a.getLanguage())) {
                break;
            }
            i2++;
        }
        spinner.setSelection(i2);
        this.i.setOnItemSelectedListener(new bh(this));
    }

    private boolean xonCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.login, menu);
        return true;
    }

    private void xonNewIntent(Intent intent) {
        super.onNewIntent(intent);
        c cVar = (c) intent.getSerializableExtra("EXTRA_LOGIN_RESULT");
        b(false);
        if (cVar == null) {
            return;
        }
        if (cVar.b() != null) {
            if (cVar.b().c()) {
                return;
            }
            if (cVar.b().b()) {
                b((int) R.string.error_service_account_general);
            } else {
                b((int) R.string.error_service_account_login);
            }
        } else if (cVar.a()) {
            f();
        } else {
            b((int) R.string.error_service_account_login);
        }
    }

    private boolean xonOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.menu_login_forgottenpw /*2131296412*/:
                ResetPasswordActivity.a(this);
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    private void xonSaveInstanceState(Bundle bundle) {
        bundle.putString("EXTRA_KEY", this.d.getText().toString());
        bundle.putString("EXTRA_PASSWORD", this.d.getText().toString());
        super.onSaveInstanceState(bundle);
    }

    private void xonStart() {
        super.onStart();
        if (!this.h && this.g.c()) {
            f();
        }
    }

    private void xonStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public final int a() {
        return xa();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        xb();
    }

    /* access modifiers changed from: protected */
    public final CharSequence d() {
        return xd();
    }

    public void finish() {
        xfinish();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        xonActivityResult(i2, i3, intent);
    }

    public void onCreate(Bundle bundle) {
        xonCreate(bundle);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return xonCreateOptionsMenu(menu);
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        xonNewIntent(intent);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        return xonOptionsItemSelected(menuItem);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        xonSaveInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        xonStart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        xonStop();
    }
}
