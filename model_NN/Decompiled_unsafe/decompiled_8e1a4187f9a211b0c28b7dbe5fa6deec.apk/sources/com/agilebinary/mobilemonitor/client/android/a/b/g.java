package com.agilebinary.mobilemonitor.client.android.a.b;

import com.agilebinary.mobilemonitor.client.android.a.l;
import com.agilebinary.mobilemonitor.client.android.a.s;
import com.agilebinary.mobilemonitor.client.android.c.b;

public class g {
    private static final String b = b.a();

    /* renamed from: a  reason: collision with root package name */
    protected l f151a = l.a();

    protected static void a(s sVar) {
        xa(sVar);
    }

    protected static String b(s sVar) {
        return xb(sVar);
    }

    private static void xa(s sVar) {
        if (sVar != null) {
            sVar.e();
        }
    }

    private static String xb(s sVar) {
        if (sVar != null) {
            return sVar.g();
        }
        return null;
    }
}
