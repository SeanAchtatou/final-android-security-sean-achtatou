package com.agilebinary.mobilemonitor.client.a.b;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.a.a.c;
import com.agilebinary.mobilemonitor.client.a.e;
import com.biige.client.android.R;

public final class k extends a {
    public k(String str, long j, long j2, c cVar, e eVar) {
        super(str, j, j2, cVar, eVar);
    }

    private final String xb(Context context) {
        return context.getString(R.string.label_event_location_type_combined);
    }

    private final String xc(Context context) {
        return "";
    }

    private final byte xk() {
        return 4;
    }

    public final String b(Context context) {
        return xb(context);
    }

    public final String c(Context context) {
        return xc(context);
    }

    public final byte k() {
        return xk();
    }
}
