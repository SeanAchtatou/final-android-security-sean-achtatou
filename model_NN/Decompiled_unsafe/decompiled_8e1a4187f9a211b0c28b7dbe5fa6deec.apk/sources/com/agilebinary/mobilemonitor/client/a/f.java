package com.agilebinary.mobilemonitor.client.a;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.a.a.a;
import com.agilebinary.mobilemonitor.client.a.a.c;
import com.agilebinary.mobilemonitor.client.a.b.d;
import com.agilebinary.mobilemonitor.client.a.b.e;
import com.agilebinary.mobilemonitor.client.a.b.g;
import com.agilebinary.mobilemonitor.client.a.b.h;
import com.agilebinary.mobilemonitor.client.a.b.j;
import com.agilebinary.mobilemonitor.client.a.b.k;
import com.agilebinary.mobilemonitor.client.a.b.l;
import com.agilebinary.mobilemonitor.client.a.b.m;
import com.agilebinary.mobilemonitor.client.a.b.o;
import com.agilebinary.mobilemonitor.client.a.b.p;
import com.agilebinary.mobilemonitor.client.a.b.r;
import com.agilebinary.mobilemonitor.client.a.b.u;
import com.agilebinary.mobilemonitor.client.android.c.b;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.InputStream;
import java.util.zip.Inflater;

public final class f {

    /* renamed from: a  reason: collision with root package name */
    private static String f140a = b.a();
    private static byte[] b = new byte[8192];
    private static byte[] c = new byte[2048];
    private static Inflater d = new Inflater(false);

    public static synchronized o a(String str, long j, long j2, boolean z, boolean z2, int i, InputStream inputStream, e eVar, b bVar, Context context, ByteArrayOutputStream byteArrayOutputStream) {
        return xa(str, j, j2, z, z2, i, inputStream, eVar, bVar, context, byteArrayOutputStream);
    }

    private static synchronized o xa(String str, long j, long j2, boolean z, boolean z2, int i, InputStream inputStream, e eVar, b bVar, Context context, ByteArrayOutputStream byteArrayOutputStream) {
        c cVar;
        o gVar;
        synchronized (f.class) {
            int read = inputStream.read();
            int read2 = inputStream.read();
            if ((read | read2) < 0) {
                throw new EOFException();
            }
            short s = (short) ((read << 8) + (read2 << 0));
            if (s != 0) {
                throw new IllegalArgumentException("unknown encoding version: " + ((int) s));
            }
            int read3 = inputStream.read();
            if (read3 < 0) {
                throw new EOFException();
            }
            byte b2 = (byte) read3;
            if (z) {
                throw new IllegalArgumentException("PENDING: encryption not yet implemented");
            }
            com.agilebinary.mobilemonitor.client.a.a.b bVar2 = null;
            if (z2) {
                d.reset();
                bVar2 = new com.agilebinary.mobilemonitor.client.a.a.b(inputStream, d, c, i - 3);
                cVar = new c(new a(bVar2, b));
            } else {
                cVar = new c(inputStream);
            }
            switch (b2) {
                case 1:
                    gVar = new u(str, j, j2, cVar, eVar);
                    break;
                case 2:
                    gVar = new j(str, j, j2, cVar, eVar);
                    break;
                case 3:
                    gVar = new com.agilebinary.mobilemonitor.client.a.b.f(str, j, j2, cVar, eVar, bVar, context, byteArrayOutputStream);
                    break;
                case 4:
                    gVar = new k(str, j, j2, cVar, eVar);
                    break;
                case 5:
                    gVar = new com.agilebinary.mobilemonitor.client.a.b.b(str, j, j2, cVar, eVar);
                    break;
                case 6:
                    gVar = new m(str, j, j2, cVar, eVar);
                    break;
                case 7:
                    gVar = new h(str, j, j2, cVar, eVar);
                    break;
                case 8:
                    gVar = new r(str, j, j2, cVar, eVar);
                    break;
                case 9:
                    gVar = new p(str, j, j2, cVar, eVar);
                    break;
                case 10:
                    gVar = new d(str, j, j2, cVar, eVar);
                    break;
                case 11:
                    gVar = new com.agilebinary.mobilemonitor.client.a.b.c(str, j, j2, cVar, eVar);
                    break;
                case 12:
                    gVar = new l(str, j, j2, cVar, eVar);
                    break;
                case 13:
                    gVar = new e(str, j, j2, cVar, eVar);
                    break;
                case 14:
                    gVar = new g(str, j, j2, cVar, eVar);
                    break;
                default:
                    throw new IllegalArgumentException("unknown content type: " + ((int) b2));
            }
            if (bVar2 != null && bVar2.a() > 0) {
                "not all bytes read from compressed stream whhen deserializing " + gVar;
            }
        }
        return gVar;
    }
}
