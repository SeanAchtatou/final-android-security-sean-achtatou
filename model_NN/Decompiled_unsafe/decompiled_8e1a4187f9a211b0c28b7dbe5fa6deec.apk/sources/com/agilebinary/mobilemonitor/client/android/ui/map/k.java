package com.agilebinary.mobilemonitor.client.android.ui.map;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.agilebinary.mobilemonitor.client.a.b.s;
import java.util.List;

public final class k extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private List f286a;
    private Context b;

    public k(Context context) {
        this.b = context;
    }

    private final void xa(List list) {
        this.f286a = list;
        notifyDataSetChanged();
    }

    private final int xgetCount() {
        if (this.f286a == null) {
            return 0;
        }
        return this.f286a.size();
    }

    private final Object xgetItem(int i) {
        return this.f286a.get(i);
    }

    private final long xgetItemId(int i) {
        return ((s) this.f286a.get(i)).u();
    }

    private final View xgetView(int i, View view, ViewGroup viewGroup) {
        s sVar = (s) this.f286a.get(i);
        MapMarkerSelectDialogRowView mapMarkerSelectDialogRowView = view == null ? new MapMarkerSelectDialogRowView(this.b, null) : (MapMarkerSelectDialogRowView) view;
        mapMarkerSelectDialogRowView.a(sVar);
        return mapMarkerSelectDialogRowView;
    }

    public final void a(List list) {
        xa(list);
    }

    public final int getCount() {
        return xgetCount();
    }

    public final Object getItem(int i) {
        return xgetItem(i);
    }

    public final long getItemId(int i) {
        return xgetItemId(i);
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        return xgetView(i, view, viewGroup);
    }
}
