package com.agilebinary.mobilemonitor.client.a.a;

import java.io.EOFException;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

public final class b extends FilterInputStream {

    /* renamed from: a  reason: collision with root package name */
    private Inflater f124a;
    private byte[] b;
    private int c;
    private boolean d;
    private boolean e;
    private int f = 0;
    private int g;
    private int h;

    public b(InputStream inputStream, Inflater inflater, byte[] bArr, int i) {
        super(inputStream);
        if (inputStream == null || inflater == null) {
            throw new NullPointerException();
        }
        this.g = i;
        this.f124a = inflater;
        this.b = bArr;
    }

    private final int xa() {
        int i = this.g - this.h;
        if (i > 0) {
            System.out.println("################ AAAARRRRGGGGHHHHH, " + i + " bytes not read!!!");
            int i2 = i;
            while (i2 > 0) {
                i2 -= this.in.read(this.b, 0, Math.min(i2, this.b.length));
            }
        }
        return i;
    }

    private final int xavailable() {
        if (!this.d) {
            return this.e ? 0 : 1;
        }
        throw new IOException();
    }

    private final void xclose() {
        if (!this.d) {
            this.f124a.end();
            this.d = true;
            this.e = true;
            super.close();
        }
    }

    private final void xmark(int i) {
    }

    private final boolean xmarkSupported() {
        return false;
    }

    private final int xread() {
        byte[] bArr = new byte[1];
        if (read(bArr, 0, 1) == -1) {
            return -1;
        }
        return bArr[0] & 255;
    }

    private final int xread(byte[] bArr, int i, int i2) {
        if (this.d) {
            throw new IOException();
        } else if (bArr == null) {
            throw new NullPointerException();
        } else if (i < 0 || i2 < 0 || i + i2 > bArr.length) {
            throw new IndexOutOfBoundsException();
        } else if (i2 == 0) {
            return 0;
        } else {
            if (this.f124a.finished()) {
                this.e = true;
                return -1;
            } else if (i > bArr.length || i2 < 0 || i < 0 || bArr.length - i < i2) {
                throw new ArrayIndexOutOfBoundsException();
            } else {
                do {
                    if (this.f124a.needsInput()) {
                        if (this.d) {
                            throw new IOException();
                        }
                        this.c = this.in.read(this.b, 0, Math.min(this.b.length, this.g - this.h));
                        if (this.c > 0) {
                            this.h += this.c;
                            this.f124a.setInput(this.b, 0, this.c);
                        }
                    }
                    try {
                        int inflate = this.f124a.inflate(bArr, i, i2);
                        if (inflate > 0) {
                            return inflate;
                        }
                        if (this.f124a.finished()) {
                            this.e = true;
                            return -1;
                        } else if (this.f124a.needsDictionary()) {
                            return -1;
                        }
                    } catch (DataFormatException e2) {
                        if (this.c == -1) {
                            throw new EOFException();
                        }
                        throw ((IOException) new IOException().initCause(e2));
                    }
                } while (this.c != -1);
                throw new EOFException();
            }
        }
    }

    private final void xreset() {
        throw new IOException();
    }

    private final long xskip(long j) {
        if (j >= 0) {
            long j2 = 0;
            while (true) {
                if (j2 >= j) {
                    break;
                }
                long j3 = j - j2;
                int read = read(this.b, 0, j3 > ((long) this.b.length) ? this.b.length : (int) j3);
                if (read == -1) {
                    this.e = true;
                    break;
                }
                j2 += (long) read;
            }
            return j2;
        }
        throw new IllegalArgumentException();
    }

    public final int a() {
        return xa();
    }

    public final int available() {
        return xavailable();
    }

    public final void close() {
        xclose();
    }

    public final void mark(int i) {
        xmark(i);
    }

    public final boolean markSupported() {
        return xmarkSupported();
    }

    public final int read() {
        return xread();
    }

    public final int read(byte[] bArr, int i, int i2) {
        return xread(bArr, i, i2);
    }

    public final void reset() {
        xreset();
    }

    public final long skip(long j) {
        return xskip(j);
    }
}
