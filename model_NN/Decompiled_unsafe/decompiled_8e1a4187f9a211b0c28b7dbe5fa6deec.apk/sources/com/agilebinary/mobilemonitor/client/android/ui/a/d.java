package com.agilebinary.mobilemonitor.client.android.ui.a;

import android.os.AsyncTask;
import android.os.PowerManager;
import com.agilebinary.mobilemonitor.client.android.a.g;
import com.agilebinary.mobilemonitor.client.android.a.q;
import com.agilebinary.mobilemonitor.client.android.c.b;
import com.agilebinary.mobilemonitor.client.android.ui.BaseActivity;
import com.agilebinary.mobilemonitor.client.android.ui.ChangePasswordActivity;

public final class d extends AsyncTask implements g {

    /* renamed from: a  reason: collision with root package name */
    public static d f211a;
    private static String b = b.a();
    private final BaseActivity c;
    private com.agilebinary.a.a.a.d.c.b d;

    public d(BaseActivity baseActivity) {
        this.c = baseActivity;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public c xdoInBackground(String... strArr) {
        return xa(strArr);
    }

    private c xa(String... strArr) {
        PowerManager.WakeLock newWakeLock = ((PowerManager) this.c.getSystemService("power")).newWakeLock(6, b);
        newWakeLock.acquire();
        try {
            String str = strArr[0];
            this.c.g.b().a().a(this.c.g.a(), str, this);
            this.c.g.a(str);
            c cVar = new c(true);
            try {
                newWakeLock.release();
            } catch (Exception e) {
            }
            f211a = null;
            return cVar;
        } catch (q e2) {
            c cVar2 = new c(e2);
            try {
                newWakeLock.release();
            } catch (Exception e3) {
            }
            f211a = null;
            return cVar2;
        } catch (Throwable th) {
            try {
                newWakeLock.release();
            } catch (Exception e4) {
            }
            f211a = null;
            throw th;
        }
    }

    private final void xa() {
        cancel(false);
        if (this.d != null) {
            try {
                this.d.d();
            } catch (Exception e) {
            }
        }
    }

    private final void xa(com.agilebinary.a.a.a.d.c.b bVar) {
        this.d = bVar;
    }

    private final void xonCancelled() {
        ChangePasswordActivity.a(this.c, (c) null);
    }

    /* access modifiers changed from: private */
    /* renamed from: xonPostExecute */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        ChangePasswordActivity.a(this.c, (c) obj);
    }

    public final void a() {
        xa();
    }

    public final void a(com.agilebinary.a.a.a.d.c.b bVar) {
        xa(bVar);
    }

    /* access modifiers changed from: protected */
    public final void onCancelled() {
        xonCancelled();
    }
}
