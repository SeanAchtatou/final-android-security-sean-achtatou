package com.agilebinary.mobilemonitor.client.android.a.a;

import java.io.Serializable;

public final class g implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private String f149a;
    private String b;
    private String c;
    private boolean d;
    private boolean e;
    private long f;
    private String g;
    private String h;
    private String i;
    private long j;
    private long k;
    private String l;
    private String m;
    private String n;
    private String o;
    private String p;

    private final String xa() {
        return this.f149a;
    }

    private final String xb() {
        return this.b;
    }

    private final long xc() {
        return this.f;
    }

    private final String xd() {
        return this.g;
    }

    private final String xe() {
        return this.h;
    }

    private final String xf() {
        return this.i;
    }

    private final String xg() {
        return this.c;
    }

    private final boolean xh() {
        return this.d;
    }

    private final long xi() {
        return this.j;
    }

    private final boolean xj() {
        return this.e;
    }

    private final long xk() {
        return this.k;
    }

    private final String xl() {
        return this.l;
    }

    private final String xm() {
        return this.m;
    }

    private final String xn() {
        return this.n;
    }

    private final String xo() {
        return this.o;
    }

    private final String xp() {
        return this.p;
    }

    public final String a() {
        return xa();
    }

    public final String b() {
        return xb();
    }

    public final long c() {
        return xc();
    }

    public final String d() {
        return xd();
    }

    public final String e() {
        return xe();
    }

    public final String f() {
        return xf();
    }

    public final String g() {
        return xg();
    }

    public final boolean h() {
        return xh();
    }

    public final long i() {
        return xi();
    }

    public final boolean j() {
        return xj();
    }

    public final long k() {
        return xk();
    }

    public final String l() {
        return xl();
    }

    public final String m() {
        return xm();
    }

    public final String n() {
        return xn();
    }

    public final String o() {
        return xo();
    }

    public final String p() {
        return xp();
    }
}
