package com.agilebinary.mobilemonitor.client.android.a.c;

import com.agilebinary.mobilemonitor.client.android.a.a.f;
import com.agilebinary.mobilemonitor.client.android.a.g;

public final class d extends k implements com.agilebinary.mobilemonitor.client.android.a.d {

    /* renamed from: a  reason: collision with root package name */
    public static String f152a = "DEMO";
    private static String d = "DEMODEMODEMO";

    private final f xa(String str, String str2, g gVar) {
        f fVar = new f();
        fVar.a(f152a);
        fVar.f(d);
        return fVar;
    }

    private final void xa(com.agilebinary.mobilemonitor.client.android.d dVar, String str, g gVar) {
        throw new IllegalArgumentException();
    }

    private final void xb(com.agilebinary.mobilemonitor.client.android.d dVar, String str, g gVar) {
        throw new IllegalArgumentException();
    }

    public final f a(String str, String str2, g gVar) {
        return xa(str, str2, gVar);
    }

    public final void a(com.agilebinary.mobilemonitor.client.android.d dVar, String str, g gVar) {
        xa(dVar, str, gVar);
    }

    public final void b(com.agilebinary.mobilemonitor.client.android.d dVar, String str, g gVar) {
        xb(dVar, str, gVar);
    }
}
