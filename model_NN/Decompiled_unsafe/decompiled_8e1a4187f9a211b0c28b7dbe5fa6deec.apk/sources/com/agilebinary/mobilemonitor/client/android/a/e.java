package com.agilebinary.mobilemonitor.client.android.a;

import com.agilebinary.a.a.a.c;
import com.agilebinary.a.a.a.g.b;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

final class e extends b {
    public e(c cVar) {
        super(cVar);
    }

    private final long xc() {
        return -1;
    }

    private final InputStream xf() {
        return new GZIPInputStream(this.f94a.f());
    }

    public final long c() {
        return xc();
    }

    public final InputStream f() {
        return xf();
    }
}
