package com.agilebinary.mobilemonitor.client.android.ui;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;

public final class a extends CursorAdapter {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EventListActivity_base f207a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public a(EventListActivity_base eventListActivity_base, Context context, Cursor cursor, ba baVar) {
        super(context, cursor);
        this.f207a = eventListActivity_base;
        eventListActivity_base.d = baVar;
    }

    private final void xa() {
        onContentChanged();
    }

    private final void xbindView(View view, Context context, Cursor cursor) {
        ((k) view).a(cursor);
    }

    private final View xnewView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return this.f207a.d.a();
    }

    public final void a() {
        xa();
    }

    public final void bindView(View view, Context context, Cursor cursor) {
        xbindView(view, context, cursor);
    }

    public final View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return xnewView(context, cursor, viewGroup);
    }
}
