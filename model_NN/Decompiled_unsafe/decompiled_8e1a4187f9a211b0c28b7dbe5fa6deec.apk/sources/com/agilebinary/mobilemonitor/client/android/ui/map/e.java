package com.agilebinary.mobilemonitor.client.android.ui.map;

import android.graphics.drawable.Drawable;
import com.agilebinary.mobilemonitor.client.android.ui.MapActivity_GOOGLE;
import com.agilebinary.mobilemonitor.client.android.ui.ag;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.OverlayItem;
import java.util.List;

public final class e extends ItemizedOverlay {

    /* renamed from: a  reason: collision with root package name */
    private List f281a;
    private MapActivity_GOOGLE b;

    public e(Drawable drawable, List list, MapActivity_GOOGLE mapActivity_GOOGLE) {
        super(boundCenterBottom(drawable));
        this.f281a = list;
        this.b = mapActivity_GOOGLE;
        populate();
    }

    public static Drawable a(Drawable drawable) {
        return xa(drawable);
    }

    private static Drawable xa(Drawable drawable) {
        return boundCenterBottom(drawable);
    }

    private final void xa(int i) {
        setFocus((OverlayItem) this.f281a.get(i));
        this.b.e();
    }

    private final OverlayItem xcreateItem(int i) {
        if (this.f281a == null) {
            return null;
        }
        return (ag) this.f281a.get(i);
    }

    private final boolean xonTap(int i) {
        this.b.a((ag) this.f281a.get(i));
        return true;
    }

    private final int xsize() {
        if (this.f281a == null) {
            return 0;
        }
        return this.f281a.size();
    }

    public final void a(int i) {
        xa(i);
    }

    /* access modifiers changed from: protected */
    public final OverlayItem createItem(int i) {
        return xcreateItem(i);
    }

    /* access modifiers changed from: protected */
    public final boolean onTap(int i) {
        return xonTap(i);
    }

    public final int size() {
        return xsize();
    }
}
