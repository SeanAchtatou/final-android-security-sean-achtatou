package com.agilebinary.mobilemonitor.client.android.ui;

import android.app.Dialog;
import android.view.View;

final class bp implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ Dialog f254a;
    private /* synthetic */ MapActivity_OSM b;

    bp(MapActivity_OSM mapActivity_OSM, Dialog dialog) {
        this.b = mapActivity_OSM;
        this.f254a = dialog;
    }

    private final void xonClick(View view) {
        this.b.h.a(!this.b.h.a());
        this.b.b.invalidate();
        this.b.l.b("MAP_LAYER_ACCURACY__BOOL", this.b.f.a());
        this.f254a.dismiss();
    }

    public final void onClick(View view) {
        xonClick(view);
    }
}
