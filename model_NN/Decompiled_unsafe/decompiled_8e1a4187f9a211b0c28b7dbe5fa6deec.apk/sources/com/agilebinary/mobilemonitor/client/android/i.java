package com.agilebinary.mobilemonitor.client.android;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import com.agilebinary.mobilemonitor.client.a.b.h;
import com.agilebinary.mobilemonitor.client.a.b.r;
import com.agilebinary.mobilemonitor.client.a.c;
import com.agilebinary.mobilemonitor.client.android.a.a.a;
import com.agilebinary.mobilemonitor.client.android.a.a.d;
import com.agilebinary.mobilemonitor.client.android.a.a.e;
import com.agilebinary.mobilemonitor.client.android.a.g;
import com.agilebinary.mobilemonitor.client.android.a.k;
import com.agilebinary.mobilemonitor.client.android.a.p;
import com.agilebinary.mobilemonitor.client.android.c.b;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class i implements g {

    /* renamed from: a  reason: collision with root package name */
    private static final String f188a = b.a();
    private boolean b;
    private p c;
    private k d;
    private MyApplication e;
    private SQLiteStatement f;
    private SQLiteStatement g;
    private SQLiteDatabase h;
    private com.agilebinary.a.a.a.d.c.b i;
    private Map j = new HashMap();
    private String[] k;

    public i(MyApplication myApplication) {
        this.e = myApplication;
        this.c = myApplication.b().d();
        this.d = myApplication.b().e();
    }

    private static String a(h hVar) {
        return xa(hVar);
    }

    private static String a(r rVar) {
        return xa(rVar);
    }

    private com.agilebinary.mobilemonitor.client.android.a.a.b b(h hVar) {
        return xb(hVar);
    }

    private com.agilebinary.mobilemonitor.client.android.a.a.b b(r rVar) {
        return xb(rVar);
    }

    private static String xa(h hVar) {
        return hVar.d() + "-" + hVar.e() + "-" + hVar.b() + "-" + hVar.c();
    }

    private static String xa(r rVar) {
        return rVar.b() + "-" + rVar.c() + "-" + rVar.d();
    }

    private final synchronized void xa() {
        this.h = new a(this, this.e, "location_cache").getWritableDatabase();
        this.f = this.h.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s,%4$s,%5$s,%6$s,%7$s,%8$s,%9$s,%10$s) values (?,?,?,?,?,?,?,?,?) ", c.f178a, c.b, c.j, c.c, c.d, c.e, c.f, c.g, c.h, c.i));
        this.g = this.h.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s,%4$s,%5$s,%6$s,%7$s,%8$s,%9$s) values (?,?,?,?,?,?,?,?) ", b.f165a, b.b, b.i, b.d, b.c, b.e, b.f, b.g, b.h));
        SQLiteStatement compileStatement = this.h.compileStatement(String.format("DELETE FROM %1$s  where %2$s<?", c.f178a, c.b));
        SQLiteStatement compileStatement2 = this.h.compileStatement(String.format("DELETE FROM %1$s  where %2$s<?", b.f165a, b.b));
        compileStatement.bindLong(1, System.currentTimeMillis() - 1209600000);
        compileStatement.execute();
        compileStatement.close();
        compileStatement2.bindLong(1, System.currentTimeMillis() - 1209600000);
        compileStatement2.execute();
        compileStatement2.close();
    }

    private final void xa(com.agilebinary.a.a.a.d.c.b bVar) {
        this.i = bVar;
    }

    private final synchronized void xa(List list) {
        this.c = this.e.b().d();
        this.b = false;
        try {
            ArrayList arrayList = new ArrayList();
            HashSet hashSet = new HashSet();
            HashSet<c> hashSet2 = new HashSet<>();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                c cVar = (c) it.next();
                if (cVar.a() && (cVar instanceof h)) {
                    h hVar = (h) cVar;
                    com.agilebinary.mobilemonitor.client.android.a.a.b b2 = b(hVar);
                    if (b2 == null) {
                        String a2 = a(hVar);
                        if (!hashSet.contains(a2)) {
                            arrayList.add(new com.agilebinary.mobilemonitor.client.android.a.a.c(hVar.d(), hVar.e(), hVar.b(), hVar.c()));
                            hashSet.add(a2);
                        }
                        hashSet2.add(cVar);
                    } else {
                        cVar.a(b2);
                    }
                }
            }
            if (arrayList.size() > 0) {
                List<a> a3 = this.c.a(this.e.a(), arrayList, this);
                if (!this.b && a3 != null) {
                    for (a aVar : a3) {
                        com.agilebinary.mobilemonitor.client.android.a.a.b b3 = aVar.b();
                        "" + b3;
                        this.f.bindLong(1, System.currentTimeMillis());
                        this.f.bindLong(2, this.e.d() ? 1 : 0);
                        this.f.bindLong(3, (long) aVar.c());
                        this.f.bindLong(4, (long) aVar.d());
                        this.f.bindLong(5, (long) aVar.e());
                        this.f.bindLong(6, (long) aVar.f());
                        this.f.bindDouble(7, b3.a());
                        this.f.bindDouble(8, b3.b());
                        this.f.bindDouble(9, b3.c());
                        this.f.executeInsert();
                    }
                    for (c cVar2 : hashSet2) {
                        if (cVar2 instanceof h) {
                            cVar2.a(b((h) cVar2));
                        }
                    }
                }
            }
            ArrayList arrayList2 = new ArrayList();
            HashSet hashSet3 = new HashSet();
            HashSet<c> hashSet4 = new HashSet<>();
            Iterator it2 = list.iterator();
            while (it2.hasNext()) {
                c cVar3 = (c) it2.next();
                if (cVar3.a() && (cVar3 instanceof r)) {
                    r rVar = (r) cVar3;
                    com.agilebinary.mobilemonitor.client.android.a.a.b b4 = b(rVar);
                    "" + rVar.b();
                    if (b4 == null) {
                        String a4 = a(rVar);
                        if (!hashSet3.contains(a4)) {
                            arrayList2.add(new d(rVar.b(), rVar.c(), rVar.d()));
                            hashSet3.add(a4);
                        }
                        hashSet4.add(cVar3);
                    } else {
                        cVar3.a(b4);
                    }
                }
            }
            if (arrayList2.size() > 0) {
                List<e> a5 = this.d.a(this.e.a(), arrayList2, this);
                if (!this.b && a5 != null) {
                    for (e eVar : a5) {
                        com.agilebinary.mobilemonitor.client.android.a.a.b e2 = eVar.e();
                        "" + e2;
                        this.g.bindLong(1, System.currentTimeMillis());
                        this.g.bindLong(2, this.e.d() ? 1 : 0);
                        this.g.bindLong(3, (long) eVar.c());
                        this.g.bindLong(4, (long) eVar.b());
                        this.g.bindLong(5, (long) eVar.d());
                        this.g.bindDouble(6, e2.a());
                        this.g.bindDouble(7, e2.b());
                        this.g.bindDouble(8, e2.c());
                        this.g.executeInsert();
                    }
                    for (c cVar4 : hashSet4) {
                        if (cVar4 instanceof r) {
                            cVar4.a(b((r) cVar4));
                        }
                    }
                }
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
        return;
    }

    private final boolean xa(c cVar) {
        com.agilebinary.mobilemonitor.client.android.a.a.b bVar = null;
        try {
            if (cVar instanceof r) {
                bVar = b((r) cVar);
            }
            if (cVar instanceof h) {
                bVar = b((h) cVar);
            }
            if (bVar == null) {
                return false;
            }
            cVar.a(bVar);
            return true;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0106  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.agilebinary.mobilemonitor.client.android.a.a.b xb(com.agilebinary.mobilemonitor.client.a.b.h r11) {
        /*
            r10 = this;
            r9 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            int r1 = r11.e()
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = " / "
            java.lang.StringBuilder r0 = r0.append(r1)
            int r1 = r11.b()
            java.lang.StringBuilder r0 = r0.append(r1)
            r0.toString()
            java.lang.String r8 = a(r11)
            java.util.Map r0 = r10.j
            java.lang.Object r0 = r0.get(r8)
            com.agilebinary.mobilemonitor.client.android.a.a.b r0 = (com.agilebinary.mobilemonitor.client.android.a.a.b) r0
            if (r0 != 0) goto L_0x00da
            android.database.sqlite.SQLiteDatabase r0 = r10.h     // Catch:{ SQLiteException -> 0x00fb, all -> 0x010a }
            java.lang.String r1 = com.agilebinary.mobilemonitor.client.android.c.f178a     // Catch:{ SQLiteException -> 0x00fb, all -> 0x010a }
            java.lang.String[] r2 = com.agilebinary.mobilemonitor.client.android.c.k     // Catch:{ SQLiteException -> 0x00fb, all -> 0x010a }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x00fb, all -> 0x010a }
            r3.<init>()     // Catch:{ SQLiteException -> 0x00fb, all -> 0x010a }
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.c.j     // Catch:{ SQLiteException -> 0x00fb, all -> 0x010a }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x00fb, all -> 0x010a }
            java.lang.String r4 = "=? and "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x00fb, all -> 0x010a }
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.c.c     // Catch:{ SQLiteException -> 0x00fb, all -> 0x010a }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x00fb, all -> 0x010a }
            java.lang.String r4 = "=? and "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x00fb, all -> 0x010a }
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.c.d     // Catch:{ SQLiteException -> 0x00fb, all -> 0x010a }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x00fb, all -> 0x010a }
            java.lang.String r4 = "=? and "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x00fb, all -> 0x010a }
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.c.e     // Catch:{ SQLiteException -> 0x00fb, all -> 0x010a }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x00fb, all -> 0x010a }
            java.lang.String r4 = "=? and "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x00fb, all -> 0x010a }
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.c.f     // Catch:{ SQLiteException -> 0x00fb, all -> 0x010a }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x00fb, all -> 0x010a }
            java.lang.String r4 = "=?"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x00fb, all -> 0x010a }
            java.lang.String r3 = r3.toString()     // Catch:{ SQLiteException -> 0x00fb, all -> 0x010a }
            r4 = 5
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ SQLiteException -> 0x00fb, all -> 0x010a }
            r5 = 0
            com.agilebinary.mobilemonitor.client.android.MyApplication r6 = r10.e     // Catch:{ SQLiteException -> 0x00fb, all -> 0x010a }
            boolean r6 = r6.d()     // Catch:{ SQLiteException -> 0x00fb, all -> 0x010a }
            if (r6 == 0) goto L_0x00f6
            java.lang.String r6 = "1"
        L_0x0086:
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x00fb, all -> 0x010a }
            r5 = 1
            int r6 = r11.d()     // Catch:{ SQLiteException -> 0x00fb, all -> 0x010a }
            java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ SQLiteException -> 0x00fb, all -> 0x010a }
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x00fb, all -> 0x010a }
            r5 = 2
            int r6 = r11.e()     // Catch:{ SQLiteException -> 0x00fb, all -> 0x010a }
            java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ SQLiteException -> 0x00fb, all -> 0x010a }
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x00fb, all -> 0x010a }
            r5 = 3
            int r6 = r11.b()     // Catch:{ SQLiteException -> 0x00fb, all -> 0x010a }
            java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ SQLiteException -> 0x00fb, all -> 0x010a }
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x00fb, all -> 0x010a }
            r5 = 4
            int r6 = r11.c()     // Catch:{ SQLiteException -> 0x00fb, all -> 0x010a }
            java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ SQLiteException -> 0x00fb, all -> 0x010a }
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x00fb, all -> 0x010a }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r7 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x00fb, all -> 0x010a }
            boolean r0 = r7.moveToNext()     // Catch:{ SQLiteException -> 0x0110, all -> 0x010d }
            if (r0 == 0) goto L_0x00f9
            com.agilebinary.mobilemonitor.client.android.a.a.b r0 = new com.agilebinary.mobilemonitor.client.android.a.a.b     // Catch:{ SQLiteException -> 0x0110, all -> 0x010d }
            r1 = 0
            double r1 = r7.getDouble(r1)     // Catch:{ SQLiteException -> 0x0110, all -> 0x010d }
            r3 = 1
            double r3 = r7.getDouble(r3)     // Catch:{ SQLiteException -> 0x0110, all -> 0x010d }
            r5 = 2
            double r5 = r7.getDouble(r5)     // Catch:{ SQLiteException -> 0x0110, all -> 0x010d }
            r0.<init>(r1, r3, r5)     // Catch:{ SQLiteException -> 0x0110, all -> 0x010d }
        L_0x00d5:
            if (r7 == 0) goto L_0x00da
            r7.close()
        L_0x00da:
            if (r0 == 0) goto L_0x00e1
            java.util.Map r1 = r10.j
            r1.put(r8, r0)
        L_0x00e1:
            r10.i = r9
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = ""
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r0)
            r1.toString()
            return r0
        L_0x00f6:
            java.lang.String r6 = "0"
            goto L_0x0086
        L_0x00f9:
            r0 = r9
            goto L_0x00d5
        L_0x00fb:
            r0 = move-exception
            r1 = r9
        L_0x00fd:
            java.lang.Exception r2 = new java.lang.Exception     // Catch:{ all -> 0x0103 }
            r2.<init>(r0)     // Catch:{ all -> 0x0103 }
            throw r2     // Catch:{ all -> 0x0103 }
        L_0x0103:
            r0 = move-exception
        L_0x0104:
            if (r1 == 0) goto L_0x0109
            r1.close()
        L_0x0109:
            throw r0
        L_0x010a:
            r0 = move-exception
            r1 = r9
            goto L_0x0104
        L_0x010d:
            r0 = move-exception
            r1 = r7
            goto L_0x0104
        L_0x0110:
            r0 = move-exception
            r1 = r7
            goto L_0x00fd
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.i.xb(com.agilebinary.mobilemonitor.client.a.b.h):com.agilebinary.mobilemonitor.client.android.a.a.b");
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x0108  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.agilebinary.mobilemonitor.client.android.a.a.b xb(com.agilebinary.mobilemonitor.client.a.b.r r11) {
        /*
            r10 = this;
            r9 = 0
            java.lang.String r8 = a(r11)
            java.util.Map r0 = r10.j
            java.lang.Object r0 = r0.get(r8)
            com.agilebinary.mobilemonitor.client.android.a.a.b r0 = (com.agilebinary.mobilemonitor.client.android.a.a.b) r0
            if (r0 != 0) goto L_0x00ed
            r0 = 4
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            r1 = 0
            com.agilebinary.mobilemonitor.client.android.MyApplication r2 = r10.e     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            boolean r2 = r2.d()     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            if (r2 == 0) goto L_0x00f7
            java.lang.String r2 = "1"
        L_0x001d:
            r0[r1] = r2     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            r1 = 1
            int r2 = r11.b()     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            r0[r1] = r2     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            r1 = 2
            int r2 = r11.c()     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            r0[r1] = r2     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            r1 = 3
            int r2 = r11.d()     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            r0[r1] = r2     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            r10.k = r0     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            r0.<init>()     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            java.lang.String r1 = "querying CDMA table, demo="
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            java.lang.String[] r1 = r10.k     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            r2 = 0
            r1 = r1[r2]     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            java.lang.String r1 = " bssid="
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            java.lang.String[] r1 = r10.k     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            r2 = 1
            r1 = r1[r2]     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            java.lang.String r1 = " nid="
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            java.lang.String[] r1 = r10.k     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            r2 = 2
            r1 = r1[r2]     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            java.lang.String r1 = " sysid="
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            java.lang.String[] r1 = r10.k     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            r2 = 3
            r1 = r1[r2]     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            r0.toString()     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            android.database.sqlite.SQLiteDatabase r0 = r10.h     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            java.lang.String r1 = com.agilebinary.mobilemonitor.client.android.b.f165a     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            java.lang.String[] r2 = com.agilebinary.mobilemonitor.client.android.b.j     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            r3.<init>()     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.b.i     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            java.lang.String r4 = "=? and "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.b.c     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            java.lang.String r4 = "=? and "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.b.d     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            java.lang.String r4 = "=? and "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.b.e     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            java.lang.String r4 = "=?"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            java.lang.String r3 = r3.toString()     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            java.lang.String[] r4 = r10.k     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r7 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x00fd, all -> 0x010c }
            boolean r0 = r7.moveToNext()     // Catch:{ SQLiteException -> 0x0112, all -> 0x010f }
            if (r0 == 0) goto L_0x00fb
            com.agilebinary.mobilemonitor.client.android.a.a.b r0 = new com.agilebinary.mobilemonitor.client.android.a.a.b     // Catch:{ SQLiteException -> 0x0112, all -> 0x010f }
            r1 = 0
            double r1 = r7.getDouble(r1)     // Catch:{ SQLiteException -> 0x0112, all -> 0x010f }
            r3 = 1
            double r3 = r7.getDouble(r3)     // Catch:{ SQLiteException -> 0x0112, all -> 0x010f }
            r5 = 2
            double r5 = r7.getDouble(r5)     // Catch:{ SQLiteException -> 0x0112, all -> 0x010f }
            r0.<init>(r1, r3, r5)     // Catch:{ SQLiteException -> 0x0112, all -> 0x010f }
        L_0x00e8:
            if (r7 == 0) goto L_0x00ed
            r7.close()
        L_0x00ed:
            if (r0 == 0) goto L_0x00f4
            java.util.Map r1 = r10.j
            r1.put(r8, r0)
        L_0x00f4:
            r10.i = r9
            return r0
        L_0x00f7:
            java.lang.String r2 = "0"
            goto L_0x001d
        L_0x00fb:
            r0 = r9
            goto L_0x00e8
        L_0x00fd:
            r0 = move-exception
            r1 = r9
        L_0x00ff:
            java.lang.Exception r2 = new java.lang.Exception     // Catch:{ all -> 0x0105 }
            r2.<init>(r0)     // Catch:{ all -> 0x0105 }
            throw r2     // Catch:{ all -> 0x0105 }
        L_0x0105:
            r0 = move-exception
        L_0x0106:
            if (r1 == 0) goto L_0x010b
            r1.close()
        L_0x010b:
            throw r0
        L_0x010c:
            r0 = move-exception
            r1 = r9
            goto L_0x0106
        L_0x010f:
            r0 = move-exception
            r1 = r7
            goto L_0x0106
        L_0x0112:
            r0 = move-exception
            r1 = r7
            goto L_0x00ff
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.i.xb(com.agilebinary.mobilemonitor.client.a.b.r):com.agilebinary.mobilemonitor.client.android.a.a.b");
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final synchronized void xb() {
        /*
            r1 = this;
            monitor-enter(r1)
            android.database.sqlite.SQLiteStatement r0 = r1.f     // Catch:{ Exception -> 0x0015, all -> 0x0012 }
            r0.close()     // Catch:{ Exception -> 0x0015, all -> 0x0012 }
            android.database.sqlite.SQLiteStatement r0 = r1.g     // Catch:{ Exception -> 0x0015, all -> 0x0012 }
            r0.close()     // Catch:{ Exception -> 0x0015, all -> 0x0012 }
            android.database.sqlite.SQLiteDatabase r0 = r1.h     // Catch:{ Exception -> 0x0015, all -> 0x0012 }
            r0.close()     // Catch:{ Exception -> 0x0015, all -> 0x0012 }
        L_0x0010:
            monitor-exit(r1)
            return
        L_0x0012:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x0015:
            r0 = move-exception
            goto L_0x0010
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.i.xb():void");
    }

    private final synchronized void xc() {
        this.b = true;
        if (this.i != null) {
            try {
                this.i.d();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return;
    }

    public final synchronized void a() {
        xa();
    }

    public final void a(com.agilebinary.a.a.a.d.c.b bVar) {
        xa(bVar);
    }

    public final synchronized void a(List list) {
        xa(list);
    }

    public final boolean a(c cVar) {
        return xa(cVar);
    }

    public final synchronized void b() {
        xb();
    }

    public final synchronized void c() {
        xc();
    }
}
