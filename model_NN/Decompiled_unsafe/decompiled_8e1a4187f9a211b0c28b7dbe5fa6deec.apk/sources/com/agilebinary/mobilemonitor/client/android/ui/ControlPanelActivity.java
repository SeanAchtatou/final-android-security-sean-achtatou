package com.agilebinary.mobilemonitor.client.android.ui;

import android.os.Bundle;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.client.android.a.t;
import com.biige.client.android.R;

public class ControlPanelActivity extends BaseLoggedInActivity {
    private final int xa() {
        return R.layout.controlpanel;
    }

    private void xonCreate(Bundle bundle) {
        super.onCreate(bundle);
        t.a();
        ((TextView) findViewById(R.id.controlpanel_url)).setText("http://m.biige.com");
    }

    /* access modifiers changed from: protected */
    public final int a() {
        return xa();
    }

    public void onCreate(Bundle bundle) {
        xonCreate(bundle);
    }
}
