package com.agilebinary.mobilemonitor.client.a.b;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.a.c;
import com.agilebinary.mobilemonitor.client.a.e;
import com.agilebinary.mobilemonitor.client.android.a.a.b;
import com.biige.client.android.R;

public final class r extends n implements c {
    private int c = -1;
    private int d = -1;
    private int e = -1;
    private double f;
    private double g;

    public r(String str, long j, long j2, com.agilebinary.mobilemonitor.client.a.a.c cVar, e eVar) {
        super(str, j, j2, cVar, eVar);
        this.c = cVar.c();
        this.d = cVar.c();
        this.e = cVar.c();
        this.f = cVar.e();
        this.g = cVar.e();
    }

    private final void xa(b bVar) {
        this.b = bVar;
    }

    private final boolean xa() {
        return this.b == null;
    }

    private final int xb() {
        return this.c;
    }

    private final String xb(Context context) {
        return context.getString(R.string.label_event_location_type_cell_cdma);
    }

    private final int xc() {
        return this.d;
    }

    private final String xc(Context context) {
        return this.f134a;
    }

    private final int xd() {
        return this.e;
    }

    private final byte xk() {
        return 8;
    }

    public final void a(b bVar) {
        xa(bVar);
    }

    public final boolean a() {
        return xa();
    }

    public final int b() {
        return xb();
    }

    public final String b(Context context) {
        return xb(context);
    }

    public final int c() {
        return xc();
    }

    public final String c(Context context) {
        return xc(context);
    }

    public final int d() {
        return xd();
    }

    public final byte k() {
        return xk();
    }
}
