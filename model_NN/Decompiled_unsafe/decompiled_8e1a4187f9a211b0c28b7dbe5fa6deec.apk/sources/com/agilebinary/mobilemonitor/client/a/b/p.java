package com.agilebinary.mobilemonitor.client.a.b;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.a.a.c;
import com.agilebinary.mobilemonitor.client.a.e;
import com.biige.client.android.R;

public final class p extends n {
    private String c;

    public p(String str, long j, long j2, c cVar, e eVar) {
        super(str, j, j2, cVar, eVar);
        this.c = cVar.g();
    }

    private final CharSequence xa(Context context) {
        return null;
    }

    private final String xb(Context context) {
        return context.getString(R.string.label_event_location_type_cellbroadcast);
    }

    private final String xc(Context context) {
        return this.c;
    }

    private final Double xf() {
        return null;
    }

    private final boolean xj() {
        return false;
    }

    private final byte xk() {
        return 9;
    }

    public final CharSequence a(Context context) {
        return xa(context);
    }

    public final String b(Context context) {
        return xb(context);
    }

    public final String c(Context context) {
        return xc(context);
    }

    public final Double f() {
        return xf();
    }

    public final boolean j() {
        return xj();
    }

    public final byte k() {
        return xk();
    }
}
