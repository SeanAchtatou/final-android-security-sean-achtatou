package com.agilebinary.mobilemonitor.client.android.ui;

import android.view.View;

final class y implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ChangePasswordActivity f298a;

    y(ChangePasswordActivity changePasswordActivity) {
        this.f298a = changePasswordActivity;
    }

    private final void xonClick(View view) {
        this.f298a.setResult(0);
        this.f298a.finish();
    }

    public final void onClick(View view) {
        xonClick(view);
    }
}
