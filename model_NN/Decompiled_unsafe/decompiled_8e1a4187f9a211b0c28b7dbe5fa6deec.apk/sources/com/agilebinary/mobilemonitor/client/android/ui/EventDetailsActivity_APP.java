package com.agilebinary.mobilemonitor.client.android.ui;

import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.client.a.b.c;
import com.agilebinary.mobilemonitor.client.a.b.o;
import com.biige.client.android.R;

public class EventDetailsActivity_APP extends EventDetailsActivity_base {

    /* renamed from: a  reason: collision with root package name */
    private TextView f194a;
    private TextView b;
    private TextView c;
    private TextView d;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private final void xa(ViewGroup viewGroup) {
        ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.eventdetails_app, viewGroup, true);
        this.b = (TextView) findViewById(R.id.eventdetails_app_time);
        this.f194a = (TextView) findViewById(R.id.eventdetails_app_kind);
        this.c = (TextView) findViewById(R.id.eventdetails_app_name);
        this.d = (TextView) findViewById(R.id.eventdetails_app_details);
        this.d.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private final void xa(o oVar) {
        super.a(oVar);
        c cVar = (c) oVar;
        this.f194a.setText(cVar.a(this));
        this.c.setText(cVar.c());
        String[] split = cVar.b().split(":");
        String str = split.length > 1 ? split[1] : "";
        this.b.setText(com.agilebinary.mobilemonitor.client.android.c.c.a().c(cVar.a()));
        this.d.setText(Html.fromHtml(String.format("<a href=\"market://search?q=pname:%s\">%s</a>", str, getString(R.string.label_event_application_details))));
    }

    /* access modifiers changed from: protected */
    public final void a(ViewGroup viewGroup) {
        xa(viewGroup);
    }

    /* access modifiers changed from: protected */
    public final void a(o oVar) {
        xa(oVar);
    }
}
