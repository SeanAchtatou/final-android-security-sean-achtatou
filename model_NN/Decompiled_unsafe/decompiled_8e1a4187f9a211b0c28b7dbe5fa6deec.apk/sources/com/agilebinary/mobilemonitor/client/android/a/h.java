package com.agilebinary.mobilemonitor.client.android.a;

import com.agilebinary.a.a.a.ad;
import com.agilebinary.a.a.a.f.k;
import com.agilebinary.a.a.a.j;
import com.agilebinary.a.a.a.m;
import com.agilebinary.a.a.a.t;

final class h implements ad {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ l f157a;

    h(l lVar) {
        this.f157a = lVar;
    }

    private final void xa(j jVar, k kVar) {
        t e = jVar.b().e();
        if (e != null) {
            m[] c = e.c();
            for (m a2 : c) {
                if (a2.a().equalsIgnoreCase("gzip")) {
                    jVar.a(new e(jVar.b()));
                    return;
                }
            }
        }
    }

    public final void a(j jVar, k kVar) {
        xa(jVar, kVar);
    }
}
