package com.agilebinary.mobilemonitor.client.android.ui.map;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import com.agilebinary.mobilemonitor.client.a.b.s;
import com.agilebinary.mobilemonitor.client.a.d;
import com.biige.client.android.R;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.Projection;
import java.util.Iterator;
import java.util.List;

public final class f extends Overlay {

    /* renamed from: a  reason: collision with root package name */
    private Paint f282a = new Paint();
    private Path b;
    private List c;
    private int d = -1;
    private MapView e;
    private int f;
    private double g = 30.0d;
    private double h = 10.0d;
    private double i = 5.0d;
    private boolean j;

    public f(Context context, MapView mapView) {
        this.e = mapView;
        this.f282a.setColor(context.getResources().getColor(R.color.directions_line));
        this.f282a.setStrokeWidth(Float.parseFloat(context.getString(R.string.directions_line_width)));
        this.f282a.setStyle(Paint.Style.STROKE);
        this.f282a.setAntiAlias(true);
        this.g = Double.parseDouble(context.getString(R.string.directions_line_arrow_spacing));
        this.h = Double.parseDouble(context.getString(R.string.directions_line_arrow_head_length));
        this.i = Double.parseDouble(context.getString(R.string.directions_line_min_length));
    }

    private void a(double d2, double d3, double d4, double d5) {
        xa(d2, d3, d4, d5);
    }

    private void a(MapView mapView) {
        xa(mapView);
    }

    private void xa(double d2, double d3, double d4, double d5) {
        this.f++;
        float f2 = (float) (0.7853981633974483d + d4);
        if (((double) f2) > 3.141592653589793d) {
            f2 = (float) (((double) f2) - 6.283185307179586d);
        }
        float f3 = (float) (d4 - 0.7853981633974483d);
        if (((double) f3) <= -3.141592653589793d) {
            f3 = (float) (((double) f3) + 6.283185307179586d);
        }
        float sin = (float) ((Math.sin((double) f2) * d5) + d3);
        float sin2 = (float) ((Math.sin((double) f3) * d5) + d3);
        this.b.moveTo((float) (d2 - (Math.cos((double) f2) * d5)), sin);
        this.b.lineTo((float) d2, (float) d3);
        this.b.lineTo((float) (d2 - (Math.cos((double) f3) * d5)), sin2);
    }

    private void xa(MapView mapView) {
        Projection projection = mapView.getProjection();
        this.f = 0;
        Point point = new Point();
        Point point2 = new Point();
        projection.toPixels(((d) this.c.get(0)).i(), point);
        int i2 = 1;
        while (true) {
            int i3 = i2;
            if (i3 < this.c.size()) {
                projection.toPixels(((d) this.c.get(i3)).i(), point2);
                double d2 = (double) (point2.x - point.x);
                double d3 = (double) (point2.y - point.y);
                double sqrt = Math.sqrt((d2 * d2) + (d3 * d3));
                if (sqrt > this.i) {
                    double atan2 = Math.atan2(-d3, d2);
                    boolean z = this.f > 1500;
                    if (this.g == 0.0d) {
                        a((double) point2.x, (double) point2.y, atan2, this.h);
                        return;
                    } else if (this.g == 1.0d || z) {
                        a(((double) point.x) + ((sqrt / 2.0d) * Math.cos(atan2)), ((double) point.y) - ((sqrt / 2.0d) * Math.sin(atan2)), atan2, this.h);
                    } else {
                        double d4 = this.g;
                        while (true) {
                            double d5 = d4;
                            if (d5 >= sqrt) {
                                d4 = d5;
                                break;
                            }
                            a(((double) point.x) + (Math.cos(atan2) * d5), ((double) point.y) - (Math.sin(atan2) * d5), atan2, this.h);
                            d4 = this.g + d5;
                            if (this.f > 1500) {
                                break;
                            }
                        }
                        if (d4 == this.g) {
                            a(((double) point.x) + ((sqrt / 2.0d) * Math.cos(atan2)), ((double) point.y) - ((sqrt / 2.0d) * Math.sin(atan2)), atan2, this.h);
                        }
                    }
                }
                point.set(point2.x, point2.y);
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    private final void xa(List list) {
        this.c = list;
    }

    private final void xa(boolean z) {
        this.j = z;
    }

    private final boolean xa() {
        return this.j;
    }

    private final void xdraw(Canvas canvas, MapView mapView, boolean z) {
        f.super.draw(canvas, mapView, z);
        if (this.j && this.c.size() >= 2) {
            Projection projection = mapView.getProjection();
            this.b = new Path();
            Point point = new Point();
            int i2 = 0;
            Iterator it = this.c.iterator();
            while (true) {
                int i3 = i2;
                if (it.hasNext()) {
                    projection.toPixels(((d) ((s) it.next())).i(), point);
                    if (i3 == 0) {
                        this.b.moveTo((float) point.x, (float) point.y);
                    } else {
                        this.b.lineTo((float) point.x, (float) point.y);
                    }
                    i2 = i3 + 1;
                } else {
                    a(mapView);
                    canvas.drawPath(this.b, this.f282a);
                    return;
                }
            }
        }
    }

    public final void a(List list) {
        xa(list);
    }

    public final void a(boolean z) {
        xa(z);
    }

    public final boolean a() {
        return xa();
    }

    public final void draw(Canvas canvas, MapView mapView, boolean z) {
        xdraw(canvas, mapView, z);
    }
}
