package com.agilebinary.mobilemonitor.client.android.ui;

import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import com.biige.client.android.R;

public final class be extends k {
    private TextView c;
    private TextView d;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.agilebinary.mobilemonitor.client.android.ui.be, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public be(EventListActivity_base eventListActivity_base) {
        super(eventListActivity_base);
        ((LayoutInflater) eventListActivity_base.getSystemService("layout_inflater")).inflate((int) R.layout.eventlist_rowview_sms, (ViewGroup) this, true);
        a();
    }

    private final void xa() {
        super.a();
        this.c = (TextView) findViewById(R.id.eventlist_rowview_sms_text);
        this.d = (TextView) findViewById(R.id.eventlist_rowview_sms_dir);
    }

    private final void xa(Cursor cursor) {
        super.a(cursor);
        this.d.setText(cursor.getString(cursor.getColumnIndex("dir")));
        this.c.setText(cursor.getString(cursor.getColumnIndex("text")));
    }

    /* access modifiers changed from: protected */
    public final void a() {
        xa();
    }

    public final void a(Cursor cursor) {
        xa(cursor);
    }
}
