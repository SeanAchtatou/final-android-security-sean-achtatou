package com.agilebinary.mobilemonitor.client.android.ui;

import android.content.DialogInterface;

final class n implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EventListActivity_base f289a;

    n(EventListActivity_base eventListActivity_base) {
        this.f289a = eventListActivity_base;
    }

    private final void xonClick(DialogInterface dialogInterface, int i) {
        this.f289a.e.cancel();
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        xonClick(dialogInterface, i);
    }
}
