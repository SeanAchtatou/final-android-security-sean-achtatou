package com.agilebinary.mobilemonitor.client.android.a.b;

import com.agilebinary.mobilemonitor.client.android.a.a;
import com.agilebinary.mobilemonitor.client.android.a.c;
import com.agilebinary.mobilemonitor.client.android.a.d;
import com.agilebinary.mobilemonitor.client.android.a.j;
import com.agilebinary.mobilemonitor.client.android.a.k;
import com.agilebinary.mobilemonitor.client.android.a.m;
import com.agilebinary.mobilemonitor.client.android.a.p;

public final class f implements m {
    private final d xa() {
        return new a();
    }

    private final j xb() {
        return new d();
    }

    private final c xc() {
        return new e();
    }

    private final p xd() {
        return new h();
    }

    private final k xe() {
        return new c();
    }

    private final a xf() {
        return new b();
    }

    public final d a() {
        return xa();
    }

    public final j b() {
        return xb();
    }

    public final c c() {
        return xc();
    }

    public final p d() {
        return xd();
    }

    public final k e() {
        return xe();
    }

    public final a f() {
        return xf();
    }
}
