package com.agilebinary.mobilemonitor.client.android.a;

import java.io.Serializable;

public final class q extends Exception implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private int f161a;
    private Exception b;

    public q() {
    }

    public q(int i) {
        this.f161a = i;
    }

    public q(Exception exc) {
        this.b = exc;
    }

    private final boolean xa() {
        return s.a(this.f161a);
    }

    private final boolean xb() {
        return this.b != null;
    }

    private final boolean xc() {
        return s.b(this.f161a);
    }

    private final boolean xd() {
        return s.c(this.f161a);
    }

    public final boolean a() {
        return xa();
    }

    public final boolean b() {
        return xb();
    }

    public final boolean c() {
        return xc();
    }

    public final boolean d() {
        return xd();
    }
}
