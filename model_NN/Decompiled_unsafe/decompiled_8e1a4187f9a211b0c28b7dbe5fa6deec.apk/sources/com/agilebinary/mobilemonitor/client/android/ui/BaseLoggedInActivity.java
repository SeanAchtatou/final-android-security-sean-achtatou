package com.agilebinary.mobilemonitor.client.android.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import com.agilebinary.mobilemonitor.client.android.b.j;
import com.agilebinary.mobilemonitor.client.android.d;
import com.biige.client.android.R;

public abstract class BaseLoggedInActivity extends BaseActivity {
    protected j j;

    private void e() {
        xe();
    }

    private final CharSequence xd() {
        d a2 = this.g.a();
        String d = a2 == null ? null : a2.d();
        String b = a2 == null ? null : a2.b();
        if (!(d == null || d.trim().length() == 0)) {
            b = d;
        }
        getClass().getName();
        "########################" + b;
        return getString(R.string.titlebar_fmt, new Object[]{getString(R.string.app_title), b});
    }

    private void xe() {
        getClass().getName();
        if (!this.g.c()) {
            Intent intent = new Intent(this, LoginActivity.class);
            intent.addFlags(67108864);
            getClass().getName();
            startActivity(intent);
        }
        getClass().getName();
    }

    private void xonCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.j = this.g.e();
        e();
    }

    private boolean xonCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.base_loggedin, menu);
        menu.findItem(R.id.menu_base_loggedin_accountinfo).setVisible(!this.g.d());
        return true;
    }

    private void xonDestroy() {
        this.g.f();
        super.onDestroy();
    }

    private boolean xonOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.menu_base_loggedin_logout /*2131296410*/:
                finish();
                this.g.a(this);
                return true;
            case R.id.menu_base_loggedin_accountinfo /*2131296411*/:
                c();
                AccountInfoActivity.a(this);
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    private void xonStart() {
        e();
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public final CharSequence d() {
        return xd();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        xonCreate(bundle);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return xonCreateOptionsMenu(menu);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        xonDestroy();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        return xonOptionsItemSelected(menuItem);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        xonStart();
    }
}
