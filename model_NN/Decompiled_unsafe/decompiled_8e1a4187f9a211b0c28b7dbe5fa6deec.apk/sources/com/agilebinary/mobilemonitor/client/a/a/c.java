package com.agilebinary.mobilemonitor.client.a.a;

import java.io.EOFException;
import java.io.FilterInputStream;
import java.io.InputStream;

public final class c extends FilterInputStream {
    public c(InputStream inputStream) {
        super(inputStream);
    }

    private final void xa(byte[] bArr) {
        int i = 0;
        do {
            int read = this.in.read(bArr, i, bArr.length - i);
            if (read != -1) {
                i += read;
            } else {
                return;
            }
        } while (i != bArr.length);
    }

    private final boolean xa() {
        int read = this.in.read();
        if (read >= 0) {
            return read != 0;
        }
        throw new EOFException();
    }

    private final byte xb() {
        int read = this.in.read();
        if (read >= 0) {
            return (byte) read;
        }
        throw new EOFException();
    }

    private final int xc() {
        InputStream inputStream = this.in;
        int read = inputStream.read();
        int read2 = inputStream.read();
        int read3 = inputStream.read();
        int read4 = inputStream.read();
        if ((read | read2 | read3 | read4) < 0) {
            throw new EOFException();
        }
        return (read4 << 0) + (read << 24) + (read2 << 16) + (read3 << 8);
    }

    private final long xd() {
        return (((long) c()) << 32) + (((long) c()) & 4294967295L);
    }

    private final double xe() {
        int i = 0;
        char[] cArr = new char[this.in.read()];
        int length = (cArr.length / 2) + (cArr.length & 1);
        for (int i2 = 0; i2 < length; i2++) {
            int read = this.in.read();
            cArr[i] = (char) ((read >> 4) + 45);
            if (cArr[i] == '/') {
                cArr[i] = 'E';
            }
            i++;
            if (i < cArr.length) {
                cArr[i] = (char) ((read & 15) + 45);
                if (cArr[i] == '/') {
                    cArr[i] = 'E';
                }
                i++;
            }
        }
        try {
            return Double.valueOf(new String(cArr)).doubleValue();
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return 0.0d;
        }
    }

    private final String[] xf() {
        String[] strArr = new String[c()];
        for (int i = 0; i < strArr.length; i++) {
            strArr[i] = g();
        }
        return strArr;
    }

    private final String xg() {
        if (b() == 1) {
            return null;
        }
        InputStream inputStream = this.in;
        int read = inputStream.read();
        int read2 = inputStream.read();
        if ((read | read2) < 0) {
            throw new EOFException();
        }
        int i = (read2 << 0) + (read << 8);
        char[] cArr = new char[i];
        byte[] bArr = new byte[i];
        read(bArr, 0, i);
        int i2 = 0;
        int i3 = 0;
        while (i3 < i) {
            byte b = bArr[i3] & 255;
            switch (b >> 4) {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                    i3++;
                    cArr[i2] = (char) b;
                    i2++;
                    break;
                case 8:
                case 9:
                case 10:
                case 11:
                default:
                    throw new IllegalArgumentException();
                case 12:
                case 13:
                    i3 += 2;
                    if (i3 <= i) {
                        byte b2 = bArr[i3 - 1];
                        if ((b2 & 192) == 128) {
                            cArr[i2] = (char) (((b & 31) << 6) | (b2 & 63));
                            i2++;
                            break;
                        } else {
                            throw new IllegalArgumentException();
                        }
                    } else {
                        throw new IllegalArgumentException();
                    }
                case 14:
                    i3 += 3;
                    if (i3 <= i) {
                        byte b3 = bArr[i3 - 2];
                        byte b4 = bArr[i3 - 1];
                        if ((b3 & 192) == 128 && (b4 & 192) == 128) {
                            cArr[i2] = (char) (((b & 15) << 12) | ((b3 & 63) << 6) | ((b4 & 63) << 0));
                            i2++;
                            break;
                        } else {
                            throw new IllegalArgumentException();
                        }
                    } else {
                        throw new IllegalArgumentException();
                    }
                    break;
            }
        }
        return new String(cArr, 0, i2);
    }

    private final int xread(byte[] bArr, int i, int i2) {
        return this.in.read(bArr, i, i2);
    }

    public final void a(byte[] bArr) {
        xa(bArr);
    }

    public final boolean a() {
        return xa();
    }

    public final byte b() {
        return xb();
    }

    public final int c() {
        return xc();
    }

    public final long d() {
        return xd();
    }

    public final double e() {
        return xe();
    }

    public final String[] f() {
        return xf();
    }

    public final String g() {
        return xg();
    }

    public final int read(byte[] bArr, int i, int i2) {
        return xread(bArr, i, i2);
    }
}
