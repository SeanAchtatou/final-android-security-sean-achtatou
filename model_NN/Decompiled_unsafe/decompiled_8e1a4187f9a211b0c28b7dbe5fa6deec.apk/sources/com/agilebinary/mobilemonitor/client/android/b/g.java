package com.agilebinary.mobilemonitor.client.android.b;

import android.content.Context;
import android.content.SharedPreferences;

public abstract class g {

    /* renamed from: a  reason: collision with root package name */
    private SharedPreferences f172a;

    public g(Context context, String str) {
        this.f172a = context.getSharedPreferences(str, 0);
    }

    private final boolean xa(String str, boolean z) {
        return this.f172a.getBoolean(str, z);
    }

    private final void xb(String str, boolean z) {
        SharedPreferences.Editor edit = this.f172a.edit();
        edit.putBoolean(str, z);
        edit.commit();
    }

    public final boolean a(String str, boolean z) {
        return xa(str, z);
    }

    public final void b(String str, boolean z) {
        xb(str, z);
    }
}
