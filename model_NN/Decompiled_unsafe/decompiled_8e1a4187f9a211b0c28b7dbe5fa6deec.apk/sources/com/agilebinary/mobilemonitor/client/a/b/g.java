package com.agilebinary.mobilemonitor.client.a.b;

import com.agilebinary.mobilemonitor.client.a.a.c;
import com.agilebinary.mobilemonitor.client.a.e;

public final class g extends i {

    /* renamed from: a  reason: collision with root package name */
    private long f131a;
    private String b;
    private String c;
    private boolean d;
    private int e;

    public g(String str, long j, long j2, c cVar, e eVar) {
        super(str, j, j2, cVar, eVar);
        this.f131a = eVar.a(cVar.d());
        this.b = cVar.g();
        this.c = cVar.g();
        this.d = cVar.a();
        this.e = cVar.c();
    }

    private final long xa() {
        return this.f131a;
    }

    private final String xb() {
        return this.b;
    }

    private final String xc() {
        return this.c;
    }

    private final boolean xd() {
        return this.d;
    }

    private final int xe() {
        return this.e;
    }

    private final String xf() {
        return this.b;
    }

    private final String xg() {
        return this.c;
    }

    private final byte xk() {
        return 14;
    }

    public final long a() {
        return xa();
    }

    public final String b() {
        return xb();
    }

    public final String c() {
        return xc();
    }

    public final boolean d() {
        return xd();
    }

    public final int e() {
        return xe();
    }

    public final String f() {
        return xf();
    }

    public final String g() {
        return xg();
    }

    public final byte k() {
        return xk();
    }
}
