package com.agilebinary.mobilemonitor.client.android.ui;

import android.view.View;

final class aj implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ChangeEmailActivity f227a;

    aj(ChangeEmailActivity changeEmailActivity) {
        this.f227a = changeEmailActivity;
    }

    private final void xonClick(View view) {
        this.f227a.setResult(0);
        this.f227a.finish();
    }

    public final void onClick(View view) {
        xonClick(view);
    }
}
