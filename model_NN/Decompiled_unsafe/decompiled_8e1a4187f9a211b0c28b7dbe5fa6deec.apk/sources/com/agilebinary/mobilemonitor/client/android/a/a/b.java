package com.agilebinary.mobilemonitor.client.android.a.a;

import java.io.Serializable;

public final class b implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    double f144a;
    double b;
    double c;
    String d;
    String e;
    String f;
    String g;
    String h;

    public b() {
    }

    public b(double d2, double d3, double d4) {
        this.f144a = d2;
        this.b = d3;
        this.c = d4;
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = null;
    }

    private final double xa() {
        return this.f144a;
    }

    private final void xa(double d2) {
        this.f144a = d2;
    }

    private final double xb() {
        return this.b;
    }

    private final void xb(double d2) {
        this.b = d2;
    }

    private final double xc() {
        return this.c;
    }

    private final void xc(double d2) {
        this.c = d2;
    }

    private final String xtoString() {
        return "Location: " + ("{\"latitude\":\"" + this.f144a + "\", \"" + "longitude" + "\":\"" + this.b + "\", \"" + "accuracy" + "\":\"" + this.c + "\", \"" + "country" + "\":\"" + this.d + "\", \"" + "country_code" + "\":\"" + this.e + "\", \"" + "region" + "\":\"" + this.f + "\", \"" + "city" + "\":\"" + this.g + "\", \"" + "street" + "\":\"" + this.h + "\"}");
    }

    public final double a() {
        return xa();
    }

    public final void a(double d2) {
        xa(d2);
    }

    public final double b() {
        return xb();
    }

    public final void b(double d2) {
        xb(d2);
    }

    public final double c() {
        return xc();
    }

    public final void c(double d2) {
        xc(d2);
    }

    public final String toString() {
        return xtoString();
    }
}
