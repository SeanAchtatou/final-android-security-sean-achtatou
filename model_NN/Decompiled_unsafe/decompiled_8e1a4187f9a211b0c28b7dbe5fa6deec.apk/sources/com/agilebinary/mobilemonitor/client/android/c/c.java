package com.agilebinary.mobilemonitor.client.android.c;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private static c f181a;
    private DateFormat b = SimpleDateFormat.getDateInstance(3);
    private DateFormat c = SimpleDateFormat.getDateInstance(2);
    private DateFormat d = SimpleDateFormat.getTimeInstance(3);
    private DateFormat e = SimpleDateFormat.getTimeInstance(2);
    private DateFormat f = SimpleDateFormat.getDateTimeInstance(3, 2);
    private DateFormat g = SimpleDateFormat.getDateTimeInstance(2, 2);
    private TimeZone h = TimeZone.getDefault();
    private DateFormat i = new SimpleDateFormat("d MMM");

    public static synchronized c a() {
        return xa();
    }

    private static synchronized c xa() {
        c cVar;
        synchronized (c.class) {
            if (f181a == null) {
                f181a = new c();
            }
            cVar = f181a;
        }
        return cVar;
    }

    private final String xa(long j) {
        Date date = new Date(j);
        return String.format("%s\n%s", this.d.format(date), this.i.format(date));
    }

    private final String xb(long j) {
        return this.b.format(new Date(j));
    }

    private final String xc(long j) {
        if (j == 0) {
            return "";
        }
        Date date = new Date(j);
        if (this.h.getID().equals(TimeZone.getDefault().getID())) {
            return this.g.format(date);
        }
        return String.format("%s [%s]", this.g.format(date), this.h.getDisplayName());
    }

    private final String xd(long j) {
        if (j == 0) {
            return "";
        }
        Date date = new Date(j);
        if (this.h.getID().equals(TimeZone.getDefault().getID())) {
            return this.f.format(date);
        }
        return String.format("%s [%s]", this.f.format(date), this.h.getDisplayName());
    }

    public final String a(long j) {
        return xa(j);
    }

    public final String b(long j) {
        return xb(j);
    }

    public final String c(long j) {
        return xc(j);
    }

    public final String d(long j) {
        return xd(j);
    }
}
