package com.agilebinary.mobilemonitor.client.android.a.a;

import com.agilebinary.mobilemonitor.client.android.c.b;
import java.io.Serializable;

public class d implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private static final String f146a = b.a();
    private int b;
    private int c;
    private int d;

    public d() {
    }

    public d(int i, int i2, int i3) {
        this.b = i;
        this.c = i2;
        this.d = i3;
    }

    private String xa() {
        return "{\"base_station_id\":\"" + this.b + "\", \"" + "network_id" + "\":\"" + this.c + "\", \"" + "system_id" + "\":\"" + this.d + "\"}";
    }

    private final void xa(int i) {
        this.b = i;
    }

    private final int xb() {
        return this.b;
    }

    private final void xb(int i) {
        this.c = i;
    }

    private final int xc() {
        return this.c;
    }

    private final void xc(int i) {
        this.d = i;
    }

    private final int xd() {
        return this.d;
    }

    private String xtoString() {
        return "GeocodeCdmaCell: " + a();
    }

    public String a() {
        return xa();
    }

    public final void a(int i) {
        xa(i);
    }

    public final int b() {
        return xb();
    }

    public final void b(int i) {
        xb(i);
    }

    public final int c() {
        return xc();
    }

    public final void c(int i) {
        xc(i);
    }

    public final int d() {
        return xd();
    }

    public String toString() {
        return xtoString();
    }
}
