package com.agilebinary.mobilemonitor.client.android.a.a;

import java.io.Serializable;
import java.util.List;

public final class f implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private String f148a;
    private String b;
    private String c;
    private boolean d;
    private boolean e;
    private long f;
    private String g;
    private String h;
    private String i;
    private long j;
    private long k;
    private String l;
    private String m;
    private String n;
    private String o;
    private String p;
    private String q;
    private boolean r;
    private boolean s;
    private boolean t;
    private boolean u;
    private boolean v;
    private boolean w;
    private List x;

    public f() {
    }

    public f(String str, String str2, long j2, String str3, String str4, String str5, String str6, boolean z, long j3, boolean z2, long j4, String str7, String str8, String str9, String str10, String str11) {
        this(str, str2, j2, str3, str4, str5, str6, z, j3, z2, j4, str7, str8, str9, str10, str11, (byte) 0);
    }

    private f(String str, String str2, long j2, String str3, String str4, String str5, String str6, boolean z, long j3, boolean z2, long j4, String str7, String str8, String str9, String str10, String str11, byte b2) {
        this.f148a = str;
        this.b = str2;
        this.f = j2;
        this.g = str3;
        this.h = str4;
        this.i = str5;
        this.c = str6;
        this.d = z;
        this.j = j3;
        this.e = z2;
        this.k = j4;
        this.l = str7;
        this.m = str8;
        this.n = str9;
        this.o = str10;
        this.o = str10;
        this.p = str11;
        this.x = null;
        this.r = true;
        this.s = true;
        this.t = true;
        this.u = true;
        this.v = true;
        this.w = true;
    }

    private final String xa() {
        return this.f148a;
    }

    private final void xa(long j2) {
        this.f = j2;
    }

    private final void xa(String str) {
        this.f148a = str;
    }

    private final void xa(boolean z) {
        this.d = z;
    }

    private final String xb() {
        return this.b;
    }

    private final void xb(long j2) {
        this.j = j2;
    }

    private final void xb(String str) {
        this.b = str;
    }

    private final void xb(boolean z) {
        this.e = z;
    }

    private final String xc() {
        return this.g;
    }

    private final void xc(long j2) {
        this.k = j2;
    }

    private final void xc(String str) {
        this.g = str;
    }

    private final void xc(boolean z) {
        this.r = z;
    }

    private final String xd() {
        return this.h;
    }

    private final void xd(String str) {
        this.h = str;
    }

    private final void xd(boolean z) {
        this.s = z;
    }

    private final void xe(String str) {
        this.i = str;
    }

    private final void xe(boolean z) {
        this.t = z;
    }

    private final boolean xe() {
        return this.e;
    }

    private final String xf() {
        return this.l;
    }

    private final void xf(String str) {
        this.c = str;
    }

    private final void xf(boolean z) {
        this.u = z;
    }

    private final String xg() {
        return this.m;
    }

    private final void xg(String str) {
        this.l = str;
    }

    private final void xg(boolean z) {
        this.v = z;
    }

    private final String xh() {
        return this.n;
    }

    private final void xh(String str) {
        this.m = str;
    }

    private final void xh(boolean z) {
        this.w = z;
    }

    private final String xi() {
        return this.o;
    }

    private final void xi(String str) {
        this.n = str;
    }

    private final void xj(String str) {
        this.o = str;
    }

    private final boolean xj() {
        return this.r;
    }

    private final void xk(String str) {
        this.p = str;
    }

    private final boolean xk() {
        return this.s;
    }

    private final void xl(String str) {
        this.q = str;
    }

    private final boolean xl() {
        return this.t;
    }

    private final boolean xm() {
        return this.u;
    }

    private final boolean xn() {
        return this.v;
    }

    private final boolean xo() {
        return this.w;
    }

    public final String a() {
        return xa();
    }

    public final void a(long j2) {
        xa(j2);
    }

    public final void a(String str) {
        xa(str);
    }

    public final void a(boolean z) {
        xa(z);
    }

    public final String b() {
        return xb();
    }

    public final void b(long j2) {
        xb(j2);
    }

    public final void b(String str) {
        xb(str);
    }

    public final void b(boolean z) {
        xb(z);
    }

    public final String c() {
        return xc();
    }

    public final void c(long j2) {
        xc(j2);
    }

    public final void c(String str) {
        xc(str);
    }

    public final void c(boolean z) {
        xc(z);
    }

    public final String d() {
        return xd();
    }

    public final void d(String str) {
        xd(str);
    }

    public final void d(boolean z) {
        xd(z);
    }

    public final void e(String str) {
        xe(str);
    }

    public final void e(boolean z) {
        xe(z);
    }

    public final boolean e() {
        return xe();
    }

    public final String f() {
        return xf();
    }

    public final void f(String str) {
        xf(str);
    }

    public final void f(boolean z) {
        xf(z);
    }

    public final String g() {
        return xg();
    }

    public final void g(String str) {
        xg(str);
    }

    public final void g(boolean z) {
        xg(z);
    }

    public final String h() {
        return xh();
    }

    public final void h(String str) {
        xh(str);
    }

    public final void h(boolean z) {
        xh(z);
    }

    public final String i() {
        return xi();
    }

    public final void i(String str) {
        xi(str);
    }

    public final void j(String str) {
        xj(str);
    }

    public final boolean j() {
        return xj();
    }

    public final void k(String str) {
        xk(str);
    }

    public final boolean k() {
        return xk();
    }

    public final void l(String str) {
        xl(str);
    }

    public final boolean l() {
        return xl();
    }

    public final boolean m() {
        return xm();
    }

    public final boolean n() {
        return xn();
    }

    public final boolean o() {
        return xo();
    }
}
