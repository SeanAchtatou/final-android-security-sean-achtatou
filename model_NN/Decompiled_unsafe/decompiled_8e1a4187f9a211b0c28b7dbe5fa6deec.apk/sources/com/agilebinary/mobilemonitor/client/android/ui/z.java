package com.agilebinary.mobilemonitor.client.android.ui;

import android.view.View;

final class z implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ChangePasswordActivity f299a;

    z(ChangePasswordActivity changePasswordActivity) {
        this.f299a = changePasswordActivity;
    }

    private final void xonClick(View view) {
        this.f299a.setResult(0);
        this.f299a.finish();
    }

    public final void onClick(View view) {
        xonClick(view);
    }
}
