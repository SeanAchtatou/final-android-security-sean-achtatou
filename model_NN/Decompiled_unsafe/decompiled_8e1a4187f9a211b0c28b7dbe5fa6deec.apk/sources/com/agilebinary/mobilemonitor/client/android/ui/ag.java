package com.agilebinary.mobilemonitor.client.android.ui;

import android.content.Context;
import android.graphics.drawable.Drawable;
import com.agilebinary.mobilemonitor.client.a.b.a;
import com.agilebinary.mobilemonitor.client.a.b.s;
import com.agilebinary.mobilemonitor.client.a.d;
import com.agilebinary.mobilemonitor.client.android.ui.map.e;
import com.biige.client.android.R;
import com.google.android.maps.OverlayItem;

public final class ag extends OverlayItem {

    /* renamed from: a  reason: collision with root package name */
    s f224a;

    public ag(s sVar, Context context) {
        super(((d) sVar).i(), "foo", "bar");
        this.f224a = sVar;
        if (!(sVar instanceof a)) {
            setMarker(context.getResources().getDrawable(R.drawable.mapmarker_blue));
        } else if (((a) sVar).d()) {
            setMarker(context.getResources().getDrawable(R.drawable.mapmarker_green));
        } else {
            setMarker(context.getResources().getDrawable(R.drawable.mapmarker_red));
        }
    }

    private final Drawable xgetMarker(int i) {
        int[] iArr;
        if (this.mMarker == null) {
            return null;
        }
        if (i == 4) {
            iArr = new int[]{16842908};
            System.out.println("***** getMarker focused " + this.f224a.u());
        } else {
            iArr = new int[0];
        }
        this.mMarker.setState(iArr);
        return e.a(this.mMarker);
    }

    public final Drawable getMarker(int i) {
        return xgetMarker(i);
    }
}
