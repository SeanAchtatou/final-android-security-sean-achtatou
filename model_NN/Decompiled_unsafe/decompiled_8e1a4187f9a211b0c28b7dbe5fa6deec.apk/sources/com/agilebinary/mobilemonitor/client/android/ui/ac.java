package com.agilebinary.mobilemonitor.client.android.ui;

import android.content.DialogInterface;

final class ac implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ EventListActivity_LOC f220a;

    ac(EventListActivity_LOC eventListActivity_LOC) {
        this.f220a = eventListActivity_LOC;
    }

    private final void xonClick(DialogInterface dialogInterface, int i) {
        this.f220a.i = i;
        dialogInterface.cancel();
        this.f220a.l();
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        xonClick(dialogInterface, i);
    }
}
