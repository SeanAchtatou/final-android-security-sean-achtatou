package com.agilebinary.mobilemonitor.client.android.ui;

import android.content.DialogInterface;

final class d implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ BaseActivity f268a;

    d(BaseActivity baseActivity) {
        this.f268a = baseActivity;
    }

    private final void xonCancel(DialogInterface dialogInterface) {
        this.f268a.b();
    }

    public final void onCancel(DialogInterface dialogInterface) {
        xonCancel(dialogInterface);
    }
}
