package com.agilebinary.mobilemonitor.client.android;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.android.a.a.f;
import com.agilebinary.mobilemonitor.client.android.a.a.g;
import com.agilebinary.mobilemonitor.client.android.a.t;
import com.agilebinary.mobilemonitor.client.android.c.b;
import com.biige.client.android.R;
import java.io.Serializable;

public final class d implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private static String f184a = b.a();
    private f b;
    private boolean c;

    public d(f fVar, boolean z) {
        "" + this;
        this.b = fVar;
        this.c = z;
    }

    public d(g gVar, boolean z) {
        "" + this;
        this.b = new f(gVar.a(), gVar.b(), gVar.c(), gVar.d(), gVar.e(), gVar.f(), gVar.g(), gVar.h(), gVar.i(), gVar.j(), gVar.k(), gVar.l(), gVar.m(), gVar.n(), gVar.o(), gVar.p());
        this.c = z;
    }

    private final f xa() {
        return this.b;
    }

    private final CharSequence xa(Context context) {
        if (!this.b.e()) {
            return context.getString(R.string.label_account_target_notset);
        }
        Object[] objArr = new Object[2];
        objArr[0] = this.b.i();
        String h = this.b.h();
        if (h.equals("android")) {
            h = context.getString(R.string.application_platform_android);
        } else if (h.equals("blackberry")) {
            h = context.getString(R.string.application_platform_blackberry);
        } else if (h.equals("iphone")) {
            h = context.getString(R.string.application_platform_iphone);
        } else if (h.equals("symbian_s60")) {
            h = context.getString(R.string.application_platform_symbian_s60);
        } else if (h.equals("symbian_hat3")) {
            h = context.getString(R.string.application_platform_symbian_hat3);
        }
        objArr[1] = h;
        return context.getString(R.string.label_account_target_msg, objArr);
    }

    private final void xa(String str) {
        this.b.b(str);
    }

    private final String xb() {
        "" + this;
        return this.b.a();
    }

    private final void xb(String str) {
        this.b.d(str);
    }

    private final String xc() {
        return this.b.b();
    }

    private final String xd() {
        "" + this;
        return this.b.c();
    }

    private final String xe() {
        return this.b.d();
    }

    private final CharSequence xf() {
        return this.b.f() + " " + this.b.g();
    }

    private final boolean xg() {
        return this.b.e();
    }

    private final boolean xh() {
        return this.c || this.b.n();
    }

    private final boolean xi() {
        return this.c || this.b.k();
    }

    private final boolean xj() {
        return this.c || this.b.l();
    }

    private final boolean xk() {
        return this.c || (this.b.h().equals("android") && this.b.m());
    }

    private final boolean xl() {
        return this.c || (this.b.h().equals("android") && this.b.o());
    }

    private final boolean xm() {
        return this.c || (this.b.h().equals("android") && this.b.j());
    }

    private final boolean xn() {
        t.a();
        return "1".equals("1".trim()) && (!this.b.j() || !this.b.k() || !this.b.l() || !this.b.m() || !this.b.n() || !this.b.o());
    }

    public final f a() {
        return xa();
    }

    public final CharSequence a(Context context) {
        return xa(context);
    }

    public final void a(String str) {
        xa(str);
    }

    public final String b() {
        return xb();
    }

    public final void b(String str) {
        xb(str);
    }

    public final String c() {
        return xc();
    }

    public final String d() {
        return xd();
    }

    public final String e() {
        return xe();
    }

    public final CharSequence f() {
        return xf();
    }

    public final boolean g() {
        return xg();
    }

    public final boolean h() {
        return xh();
    }

    public final boolean i() {
        return xi();
    }

    public final boolean j() {
        return xj();
    }

    public final boolean k() {
        return xk();
    }

    public final boolean l() {
        return xl();
    }

    public final boolean m() {
        return xm();
    }

    public final boolean n() {
        return xn();
    }
}
