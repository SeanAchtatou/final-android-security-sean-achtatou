package com.agilebinary.mobilemonitor.client.android.a.c;

import com.agilebinary.mobilemonitor.client.android.a.l;
import com.agilebinary.mobilemonitor.client.android.a.s;
import com.agilebinary.mobilemonitor.client.android.a.t;
import java.util.Calendar;
import java.util.Locale;

public class k {
    public static final long b;
    protected l c;

    static {
        Calendar instance = Calendar.getInstance();
        instance.set(11, 0);
        instance.set(12, 0);
        instance.set(13, 0);
        instance.set(14, 0);
        b = instance.getTimeInMillis();
    }

    public static String a() {
        return xa();
    }

    protected static void a(s sVar) {
        xa(sVar);
    }

    private static String xa() {
        t.a();
        t.a();
        return (!Locale.getDefault().getLanguage().equals("de") || !"".contains("de")) ? "http://www.biige.com/demo/demodata/" : "http://www.biige.com/demo/demodata/" + "de_DE/";
    }

    private static void xa(s sVar) {
        if (sVar != null) {
            sVar.e();
        }
    }
}
