package com.agilebinary.mobilemonitor.client.a.b;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.a.a.c;
import com.agilebinary.mobilemonitor.client.a.d;
import com.agilebinary.mobilemonitor.client.a.e;
import com.agilebinary.mobilemonitor.client.android.a.a.b;
import org.osmdroid.util.GeoPoint;

public abstract class n extends s implements d {

    /* renamed from: a  reason: collision with root package name */
    protected String f134a;
    protected b b;

    public n(String str, long j, long j2, c cVar, e eVar) {
        super(str, j, j2, cVar, eVar);
        this.f134a = cVar.g();
    }

    private CharSequence xa(Context context) {
        return "";
    }

    private Double xf() {
        if (this.b == null) {
            return null;
        }
        return Double.valueOf(this.b.c());
    }

    private final double xg() {
        return f().doubleValue();
    }

    private final GeoPoint xh() {
        if (this.b == null) {
            return null;
        }
        return new GeoPoint(this.b.a(), this.b.b());
    }

    private final com.google.android.maps.GeoPoint xi() {
        GeoPoint h = h();
        if (h == null) {
            return null;
        }
        return new com.google.android.maps.GeoPoint(h.a(), h.b());
    }

    private boolean xj() {
        return this.b != null;
    }

    private final b xl() {
        return this.b;
    }

    public CharSequence a(Context context) {
        return xa(context);
    }

    public Double f() {
        return xf();
    }

    public final double g() {
        return xg();
    }

    public final GeoPoint h() {
        return xh();
    }

    public final com.google.android.maps.GeoPoint i() {
        return xi();
    }

    public boolean j() {
        return xj();
    }

    public final b l() {
        return xl();
    }
}
