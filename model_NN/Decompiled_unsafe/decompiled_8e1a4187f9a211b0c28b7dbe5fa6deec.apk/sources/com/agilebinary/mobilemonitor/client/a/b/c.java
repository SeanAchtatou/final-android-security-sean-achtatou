package com.agilebinary.mobilemonitor.client.a.b;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.a.e;
import com.biige.client.android.R;

public final class c extends i {

    /* renamed from: a  reason: collision with root package name */
    private long f127a;
    private int b;
    private String c;

    public c(String str, long j, long j2, com.agilebinary.mobilemonitor.client.a.a.c cVar, e eVar) {
        super(str, j, j2, cVar, eVar);
        this.f127a = eVar.a(cVar.d());
        this.b = cVar.c();
        this.c = cVar.g();
    }

    private final long xa() {
        return this.f127a;
    }

    private final String xa(Context context) {
        switch (this.b) {
            case 1:
                return context.getString(R.string.label_event_application_added);
            case 2:
                return context.getString(R.string.label_event_application_removed);
            case 3:
                return context.getString(R.string.label_event_application_updated);
            case 4:
                return context.getString(R.string.label_event_application_data_cleared);
            default:
                return "";
        }
    }

    private final String xb() {
        return this.c;
    }

    private final String xc() {
        String[] split = this.c.split(":");
        return split.length > 1 ? split[1] : "";
    }

    private final byte xk() {
        return 11;
    }

    public final long a() {
        return xa();
    }

    public final String a(Context context) {
        return xa(context);
    }

    public final String b() {
        return xb();
    }

    public final String c() {
        return xc();
    }

    public final byte k() {
        return xk();
    }
}
