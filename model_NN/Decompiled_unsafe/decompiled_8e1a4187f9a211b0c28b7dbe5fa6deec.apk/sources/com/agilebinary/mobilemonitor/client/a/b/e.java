package com.agilebinary.mobilemonitor.client.a.b;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.a.a.c;
import com.biige.client.android.R;

public final class e extends i {

    /* renamed from: a  reason: collision with root package name */
    private long f129a;
    private String b;

    public e(String str, long j, long j2, c cVar, com.agilebinary.mobilemonitor.client.a.e eVar) {
        super(str, j, j2, cVar, eVar);
        this.f129a = eVar.a(cVar.d());
        this.b = cVar.g();
    }

    private final long xa() {
        return this.f129a;
    }

    private final String xa(Context context) {
        return context.getString(R.string.label_event_web_search, this.b);
    }

    private final String xb() {
        return this.b;
    }

    private final byte xk() {
        return 13;
    }

    public final long a() {
        return xa();
    }

    public final String a(Context context) {
        return xa(context);
    }

    public final String b() {
        return xb();
    }

    public final byte k() {
        return xk();
    }
}
