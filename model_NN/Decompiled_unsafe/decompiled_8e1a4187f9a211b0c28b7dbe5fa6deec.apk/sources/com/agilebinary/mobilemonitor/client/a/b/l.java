package com.agilebinary.mobilemonitor.client.a.b;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.a.a.c;
import com.agilebinary.mobilemonitor.client.a.e;
import com.biige.client.android.R;

public final class l extends n {
    private int c = -1;

    public l(String str, long j, long j2, c cVar, e eVar) {
        super(str, j, j2, cVar, eVar);
        this.c = cVar.c();
    }

    private final String xb(Context context) {
        return context.getString(R.string.label_event_location_type_cell_iden);
    }

    private final String xc(Context context) {
        return this.f134a;
    }

    private final byte xk() {
        return 12;
    }

    public final String b(Context context) {
        return xb(context);
    }

    public final String c(Context context) {
        return xc(context);
    }

    public final byte k() {
        return xk();
    }
}
