package com.agilebinary.mobilemonitor.client.android.a.c;

import com.agilebinary.mobilemonitor.client.android.a.f;
import com.agilebinary.mobilemonitor.client.android.a.o;
import com.agilebinary.mobilemonitor.client.android.b.h;
import com.agilebinary.mobilemonitor.client.android.c.d;
import java.io.ByteArrayInputStream;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

final class e extends DefaultHandler {

    /* renamed from: a  reason: collision with root package name */
    private char[] f153a = new char[1048576];
    private int b = 0;
    private byte c;
    private f d;
    private o e;
    private byte f;
    private long g;
    private long h;
    private /* synthetic */ b i;

    public e(b bVar, f fVar, o oVar, byte b2) {
        this.i = bVar;
        this.d = fVar;
        this.e = oVar;
        this.c = b2;
    }

    private final void xcharacters(char[] cArr, int i2, int i3) {
        for (int i4 = 0; i4 < i3; i4++) {
            char c2 = cArr[i2 + i4];
            if (c2 != 10) {
                this.f153a[this.b] = c2;
                this.b++;
            }
        }
    }

    private final void xendElement(String str, String str2, String str3) {
        if ("i".equals(str2)) {
            this.g = (k.b + 86400000) - Long.parseLong(new String(this.f153a, 0, this.b));
            this.h = this.g + 1980000;
        } else if ("t".equals(str2)) {
            this.f = Byte.parseByte(new String(this.f153a, 0, this.b));
        }
        if ("d".equals(str2)) {
            byte[] a2 = d.a(this.f153a, this.b);
            try {
                this.d.a(d.f152a, this.f, this.g, this.h, false, true, a2.length, new ByteArrayInputStream(a2));
                this.e.h();
            } catch (h e2) {
                throw new SAXException(e2);
            }
        }
        this.b = 0;
    }

    private final void xstartElement(String str, String str2, String str3, Attributes attributes) {
    }

    public final void characters(char[] cArr, int i2, int i3) {
        xcharacters(cArr, i2, i3);
    }

    public final void endElement(String str, String str2, String str3) {
        xendElement(str, str2, str3);
    }

    public final void startElement(String str, String str2, String str3, Attributes attributes) {
        xstartElement(str, str2, str3, attributes);
    }
}
