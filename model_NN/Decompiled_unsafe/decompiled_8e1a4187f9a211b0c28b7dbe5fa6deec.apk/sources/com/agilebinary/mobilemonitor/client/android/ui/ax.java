package com.agilebinary.mobilemonitor.client.android.ui;

public enum ax {
    PROGRESS,
    NO_EVENTS,
    LIST;

    private static ax xvalueOf(String str) {
        return (ax) Enum.valueOf(ax.class, str);
    }

    private static ax[] xvalues() {
        return (ax[]) d.clone();
    }
}
