package com.agilebinary.mobilemonitor.client.android.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.client.android.c.b;
import com.agilebinary.mobilemonitor.client.android.d;
import com.agilebinary.mobilemonitor.client.android.ui.a.a;
import com.biige.client.android.R;

public class AccountInfoActivity extends BaseLoggedInActivity {

    /* renamed from: a  reason: collision with root package name */
    private static final String f190a = b.a();
    private Button b;
    private Button c;
    private TextView d;
    private TextView e;
    private TextView f;
    private TextView h;
    private TextView i;
    private LinearLayout k;
    private ImageView l;
    private Animation m;
    private View n;
    private d o;

    public static void a(Activity activity) {
        xa(activity);
    }

    public static void a(Context context, d dVar) {
        xa(context, dVar);
    }

    private void a(d dVar) {
        xa(dVar);
    }

    private final int xa() {
        return R.layout.accountinfo;
    }

    private static void xa(Activity activity) {
        activity.startActivity(new Intent(activity, AccountInfoActivity.class));
    }

    private static void xa(Context context, d dVar) {
        Intent intent = new Intent(context, AccountInfoActivity.class);
        intent.putExtra("EXTRA_ACC", dVar);
        intent.setFlags(536870912);
        context.startActivity(intent);
    }

    private void xa(d dVar) {
        this.o = dVar;
        this.d.setText(dVar.b());
        this.e.setText((dVar.e() == null || dVar.e().equals("")) ? getString(R.string.label_account_notset) : dVar.e());
        this.f.setText(dVar.f());
        this.h.setText(dVar.g() ? R.string.label_account_status_activated : R.string.label_account_status_deactivated);
        this.i.setText(dVar.a(this));
        this.n.setVisibility(0);
        this.k.setVisibility(8);
        this.l.clearAnimation();
    }

    private void xonCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.n = findViewById(R.id.accountinfo_main);
        this.n.setVisibility(8);
        this.k = (LinearLayout) findViewById(R.id.accountinfo_progress);
        this.l = (ImageView) findViewById(R.id.accountinfo_progress_anim);
        this.m = AnimationUtils.loadAnimation(this, R.anim.spinner_black_76);
        this.d = (TextView) findViewById(R.id.account_key);
        this.e = (TextView) findViewById(R.id.account_email);
        this.f = (TextView) findViewById(R.id.account_product);
        this.h = (TextView) findViewById(R.id.account_status);
        this.i = (TextView) findViewById(R.id.account_target);
        this.b = (Button) findViewById(R.id.accountinfo_changepassword);
        this.b.setEnabled(!this.g.d());
        this.b.setOnClickListener(new bc(this));
        this.c = (Button) findViewById(R.id.accountinfo_changeemail);
        this.c.setEnabled(!this.g.d());
        this.c.setOnClickListener(new bd(this));
        if (bundle != null) {
            this.o = (d) bundle.getSerializable("EXTRA_ACC");
        }
    }

    private void xonNewIntent(Intent intent) {
        super.onNewIntent(intent);
        d dVar = (d) intent.getSerializableExtra("EXTRA_ACC");
        b(false);
        if (dVar != null) {
            a(dVar);
        }
    }

    private void xonSaveInstanceState(Bundle bundle) {
        bundle.putSerializable("EXTRA_ACC", this.o);
        super.onSaveInstanceState(bundle);
    }

    private void xonStart() {
        super.onStart();
        if (this.o != null) {
            a(this.o);
        } else if (a.f208a == null) {
            this.n.setVisibility(8);
            this.k.setVisibility(0);
            this.l.startAnimation(this.m);
            a aVar = new a(this);
            a.f208a = aVar;
            aVar.execute(new Void[0]);
        }
    }

    /* access modifiers changed from: protected */
    public final int a() {
        return xa();
    }

    public void onCreate(Bundle bundle) {
        xonCreate(bundle);
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        xonNewIntent(intent);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        xonSaveInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        xonStart();
    }
}
