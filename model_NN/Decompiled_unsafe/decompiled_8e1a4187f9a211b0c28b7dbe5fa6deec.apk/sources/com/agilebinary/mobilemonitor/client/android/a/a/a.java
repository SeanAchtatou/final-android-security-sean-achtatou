package com.agilebinary.mobilemonitor.client.android.a.a;

import java.io.Serializable;

public final class a extends c implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private long f143a;
    private int b;
    private b c;

    private final String xa() {
        return "{\"create_date\":\"" + this.f143a + "\", \"geocoder_source_id\":\"" + this.b + "\", \"" + "mobile_country_code" + "\":\"" + super.c() + "\", \"" + "mobile_network_code" + "\":\"" + super.d() + "\", \"" + "cell_id" + "\":\"" + super.e() + "\", \"" + "location_area_code" + "\":\"" + super.f() + "\", \"" + "latitude" + "\":\"" + this.c.f144a + "\", \"" + "longitude" + "\":\"" + this.c.b + "\", \"" + "accuracy" + "\":\"" + this.c.c + "\", \"" + "country" + "\":\"" + this.c.d + "\", \"" + "country_code" + "\":\"" + this.c.e + "\", \"" + "region" + "\":\"" + this.c.f + "\", \"" + "city" + "\":\"" + this.c.g + "\", \"" + "street" + "\":\"" + this.c.h + "\"}";
    }

    private final void xa(int i) {
        this.b = i;
    }

    private final void xa(long j) {
        this.f143a = j;
    }

    private final void xa(b bVar) {
        this.c = bVar;
    }

    private final b xb() {
        return this.c;
    }

    private final String xtoString() {
        return "GeocodeGsmCellLocation: " + a();
    }

    public final String a() {
        return xa();
    }

    public final void a(int i) {
        xa(i);
    }

    public final void a(long j) {
        xa(j);
    }

    public final void a(b bVar) {
        xa(bVar);
    }

    public final b b() {
        return xb();
    }

    public final String toString() {
        return xtoString();
    }
}
