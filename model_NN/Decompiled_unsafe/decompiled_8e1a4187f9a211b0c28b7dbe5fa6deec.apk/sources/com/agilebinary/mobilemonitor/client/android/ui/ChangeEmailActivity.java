package com.agilebinary.mobilemonitor.client.android.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.client.android.c.b;
import com.agilebinary.mobilemonitor.client.android.ui.a.c;
import com.agilebinary.mobilemonitor.client.android.ui.a.j;
import com.biige.client.android.R;

public class ChangeEmailActivity extends BaseLoggedInActivity {

    /* renamed from: a  reason: collision with root package name */
    private static final String f192a = b.a();
    private TextView b;
    /* access modifiers changed from: private */
    public EditText c;
    /* access modifiers changed from: private */
    public EditText d;
    private Button e;
    private Button f;
    private Button h;
    private boolean i;

    public static void a(Activity activity, int i2, boolean z) {
        xa(activity, i2, z);
    }

    public static void a(Context context, c cVar) {
        xa(context, cVar);
    }

    private final int xa() {
        return R.layout.change_email;
    }

    private static void xa(Activity activity, int i2, boolean z) {
        Intent intent = new Intent(activity, ChangeEmailActivity.class);
        intent.putExtra("EXTRA_TEXT_ID", i2);
        intent.putExtra("EXTRA_SKIP", z);
        activity.startActivityForResult(intent, 1);
    }

    private static void xa(Context context, c cVar) {
        Intent intent = new Intent(context, LoginActivity.class);
        intent.putExtra("EXTRA_SERVICE_RESULT", cVar);
        intent.setFlags(536870912);
        context.startActivity(intent);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void xa(ChangeEmailActivity changeEmailActivity, String str) {
        changeEmailActivity.a((int) R.string.msg_progress_communicating);
        j jVar = new j(changeEmailActivity);
        j.f217a = jVar;
        jVar.execute(str);
    }

    private final void xb() {
        if (j.f217a != null) {
            try {
                j.f217a.a();
            } catch (Exception e2) {
            }
        }
    }

    private void xonCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.b = (TextView) findViewById(R.id.changeemail_caption);
        int intExtra = getIntent().getIntExtra("EXTRA_TEXT_ID", 0);
        this.i = getIntent().getBooleanExtra("EXTRA_SKIP", false);
        this.b.setText(intExtra);
        this.c = (EditText) findViewById(R.id.changeemail_email1);
        this.d = (EditText) findViewById(R.id.changeemail_email2);
        this.e = (Button) findViewById(R.id.changeemail_cancel);
        this.f = (Button) findViewById(R.id.changeemail_skip);
        this.e.setVisibility(!this.i ? 0 : 8);
        this.f.setVisibility(this.i ? 0 : 8);
        this.h = (Button) findViewById(R.id.changeemail_ok);
        this.h.setOnClickListener(new ai(this));
        this.e.setOnClickListener(new ah(this));
        this.f.setOnClickListener(new aj(this));
        if (bundle != null) {
            this.c.setText(bundle.getString("EXTRA_EMAIL1"));
            this.d.setText(bundle.getString("EXTRA_EMAIL2"));
        }
    }

    private void xonNewIntent(Intent intent) {
        super.onNewIntent(intent);
        c cVar = (c) intent.getSerializableExtra("EXTRA_SERVICE_RESULT");
        b(false);
        if (cVar == null) {
            return;
        }
        if (cVar.b() != null) {
            if (cVar.b().c()) {
                b((int) R.string.error_service_account_general);
            } else if (cVar.b().b()) {
                b((int) R.string.error_service_account_general);
            } else {
                b((int) R.string.error_service_account_general);
            }
        } else if (cVar.a()) {
            setResult(-1);
            finish();
        }
    }

    private void xonSaveInstanceState(Bundle bundle) {
        bundle.putString("EXTRA_EMAIL1", this.c.getText().toString());
        bundle.putString("EXTRA_EMAIL2", this.d.getText().toString());
        super.onSaveInstanceState(bundle);
    }

    private void xonStart() {
        super.onStart();
        if (this.f.getVisibility() == 0) {
            this.f.requestFocus();
        }
    }

    private void xonStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public final int a() {
        return xa();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        xb();
    }

    public void onCreate(Bundle bundle) {
        xonCreate(bundle);
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        xonNewIntent(intent);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        xonSaveInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        xonStart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        xonStop();
    }
}
