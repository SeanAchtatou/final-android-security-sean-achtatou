package com.agilebinary.mobilemonitor.client.android.ui;

import android.os.AsyncTask;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.agilebinary.mobilemonitor.client.a.b.h;
import com.agilebinary.mobilemonitor.client.a.b.s;
import com.agilebinary.mobilemonitor.client.a.d;
import com.agilebinary.mobilemonitor.client.android.ui.map.b;
import com.agilebinary.mobilemonitor.client.android.ui.map.g;
import com.agilebinary.mobilemonitor.client.android.ui.map.j;
import com.biige.client.android.R;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

final class l extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MapActivity_OSM f275a;

    l(MapActivity_OSM mapActivity_OSM) {
        this.f275a = mapActivity_OSM;
    }

    /* access modifiers changed from: private */
    /* renamed from: xdoInBackground */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
        List unused = this.f275a.k = this.f275a.j.a(((List[]) objArr)[0]);
        if (!isCancelled()) {
            Iterator it = this.f275a.k.iterator();
            while (it.hasNext()) {
                s sVar = (s) it.next();
                if (((d) sVar).h() == null) {
                    "aaargh, mappable point is null " + sVar.u() + "  / " + sVar.b(this.f275a) + " / " + sVar.c(this.f275a);
                    if (sVar instanceof h) {
                        "CID =" + ((h) sVar).b();
                    }
                    it.remove();
                }
            }
        }
        l unused2 = this.f275a.x = null;
        return null;
    }

    /* access modifiers changed from: private */
    /* renamed from: xonPostExecute */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        if (!isCancelled()) {
            b unused = this.f275a.e = new b(this.f275a, this.f275a.c);
            this.f275a.b.c().add(this.f275a.e);
            this.f275a.e.a(((float) (this.f275a.getResources().getDisplayMetrics().widthPixels / 2)) - (this.f275a.getResources().getDisplayMetrics().xdpi / 2.0f));
            this.f275a.e.a(this.f275a.l.a("MAP_LAYER_SCALEBAR__BOOL", true));
            j unused2 = this.f275a.f = new j(this.f275a, this.f275a.b);
            this.f275a.b.c().add(this.f275a.f);
            this.f275a.f.a(this.f275a.k);
            this.f275a.f.a(false);
            com.agilebinary.mobilemonitor.client.android.ui.map.l unused3 = this.f275a.h = new com.agilebinary.mobilemonitor.client.android.ui.map.l(this.f275a);
            this.f275a.b.c().add(this.f275a.h);
            this.f275a.h.a(this.f275a.k);
            this.f275a.h.a(this.f275a.l.a("MAP_LAYER_ACCURACY__BOOL", true));
            ArrayList arrayList = new ArrayList(this.f275a.k.size());
            for (s awVar : this.f275a.k) {
                arrayList.add(new aw(awVar, this.f275a));
            }
            g unused4 = this.f275a.i = new g(this.f275a, arrayList, this.f275a, this.f275a.c, (byte) 0);
            this.f275a.i.c();
            this.f275a.b.c().add(this.f275a.i);
            ImageView imageView = new ImageView(this.f275a);
            imageView.setImageResource(R.drawable.zoom_in);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.addRule(11);
            layoutParams.addRule(10);
            this.f275a.f205a.addView(imageView, layoutParams);
            imageView.setOnClickListener(new ar(this));
            ImageView imageView2 = new ImageView(this.f275a);
            imageView2.setImageResource(R.drawable.zoom_out);
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams2.addRule(9);
            layoutParams2.addRule(10);
            this.f275a.f205a.addView(imageView2, layoutParams2);
            imageView2.setOnClickListener(new as(this));
            this.f275a.f205a.postDelayed(new au(this), 500);
        }
    }
}
