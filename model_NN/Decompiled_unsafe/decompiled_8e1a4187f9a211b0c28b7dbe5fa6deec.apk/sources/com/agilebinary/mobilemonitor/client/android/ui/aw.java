package com.agilebinary.mobilemonitor.client.android.ui;

import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import com.agilebinary.mobilemonitor.client.a.b.a;
import com.agilebinary.mobilemonitor.client.a.b.s;
import com.agilebinary.mobilemonitor.client.a.d;
import com.biige.client.android.R;
import org.osmdroid.b.a.b;
import org.osmdroid.b.a.c;

public final class aw extends c {
    s c;

    public aw(s sVar, Context context) {
        super("foo", "bar", ((d) sVar).h(), (byte) 0);
        this.c = sVar;
        if (!(sVar instanceof a)) {
            a(context.getResources().getDrawable(R.drawable.mapmarker_blue));
        } else if (((a) sVar).d()) {
            a(context.getResources().getDrawable(R.drawable.mapmarker_green));
        } else {
            a(context.getResources().getDrawable(R.drawable.mapmarker_red));
        }
        a(b.BOTTOM_CENTER);
    }

    private final Drawable xa(int i) {
        if (this.b == null) {
            return null;
        }
        this.b.setState(i == 4 ? new int[]{16842908} : new int[0]);
        return this.b;
    }

    private final Point xb(int i) {
        return i == 0 ? new Point(10, 35) : new Point(14, 48);
    }

    public final Drawable a(int i) {
        return xa(i);
    }

    public final Point b(int i) {
        return xb(i);
    }
}
