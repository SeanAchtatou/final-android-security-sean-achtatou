package com.agilebinary.mobilemonitor.client.android.a.b;

import com.agilebinary.mobilemonitor.client.android.a.a.f;
import com.agilebinary.mobilemonitor.client.android.a.d;
import com.agilebinary.mobilemonitor.client.android.a.g;
import com.agilebinary.mobilemonitor.client.android.a.q;
import com.agilebinary.mobilemonitor.client.android.a.s;
import com.agilebinary.mobilemonitor.client.android.a.t;
import com.agilebinary.mobilemonitor.client.android.c.b;

public final class a extends g implements d {
    private static final String b = b.a();

    /* JADX WARNING: Unknown top exception splitter block from list: {B:22:0x0138=Splitter:B:22:0x0138, B:14:0x0129=Splitter:B:14:0x0129} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final com.agilebinary.mobilemonitor.client.android.a.a.f xa(java.lang.String r7, java.lang.String r8, com.agilebinary.mobilemonitor.client.android.a.g r9) {
        /*
            r6 = this;
            r0 = 0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x014d, IOException -> 0x0134, all -> 0x013e }
            r1.<init>()     // Catch:{ JSONException -> 0x014d, IOException -> 0x0134, all -> 0x013e }
            com.agilebinary.mobilemonitor.client.android.a.t.a()     // Catch:{ JSONException -> 0x014d, IOException -> 0x0134, all -> 0x013e }
            java.lang.String r2 = "https://c.biige.com/"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ JSONException -> 0x014d, IOException -> 0x0134, all -> 0x013e }
            java.lang.String r2 = "ServiceAccountRetrieve?ProtocolVersion=%1$s&Key=%2$s&Password=%3$s"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ JSONException -> 0x014d, IOException -> 0x0134, all -> 0x013e }
            java.lang.String r1 = r1.toString()     // Catch:{ JSONException -> 0x014d, IOException -> 0x0134, all -> 0x013e }
            r2 = 3
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ JSONException -> 0x014d, IOException -> 0x0134, all -> 0x013e }
            r3 = 0
            java.lang.String r4 = "1"
            r2[r3] = r4     // Catch:{ JSONException -> 0x014d, IOException -> 0x0134, all -> 0x013e }
            r3 = 1
            r2[r3] = r7     // Catch:{ JSONException -> 0x014d, IOException -> 0x0134, all -> 0x013e }
            r3 = 2
            r2[r3] = r8     // Catch:{ JSONException -> 0x014d, IOException -> 0x0134, all -> 0x013e }
            java.lang.String r1 = java.lang.String.format(r1, r2)     // Catch:{ JSONException -> 0x014d, IOException -> 0x0134, all -> 0x013e }
            com.agilebinary.mobilemonitor.client.android.a.l r2 = r6.f151a     // Catch:{ JSONException -> 0x014d, IOException -> 0x0134, all -> 0x013e }
            com.agilebinary.mobilemonitor.client.android.a.t.a()     // Catch:{ JSONException -> 0x014d, IOException -> 0x0134, all -> 0x013e }
            com.agilebinary.mobilemonitor.client.android.a.s r0 = r2.a(r1, r9)     // Catch:{ JSONException -> 0x014d, IOException -> 0x0134, all -> 0x013e }
            boolean r1 = r0.a()     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            if (r1 == 0) goto L_0x011b
            com.agilebinary.mobilemonitor.client.android.a.a.f r1 = new com.agilebinary.mobilemonitor.client.android.a.a.f     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            r1.<init>()     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            java.lang.String r3 = b(r0)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            r2.<init>(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            java.lang.String r3 = "key"
            java.lang.String r3 = r2.getString(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            r1.a(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            java.lang.String r3 = "password"
            java.lang.String r3 = r2.getString(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            r1.b(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            java.lang.String r3 = "create_date"
            long r3 = r2.getLong(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            r1.a(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            java.lang.String r3 = "alias"
            java.lang.String r3 = r2.getString(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            r1.c(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            java.lang.String r3 = "email"
            java.lang.String r3 = r2.getString(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            r1.d(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            java.lang.String r3 = "domain"
            java.lang.String r3 = r2.getString(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            r1.e(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            java.lang.String r3 = "imei"
            java.lang.String r3 = r2.getString(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            r1.f(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            java.lang.String r3 = "enabled"
            boolean r3 = r2.getBoolean(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            r1.a(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            java.lang.String r3 = "enabled_change_date"
            long r3 = r2.getLong(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            r1.b(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            java.lang.String r3 = "activated"
            boolean r3 = r2.getBoolean(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            r1.b(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            java.lang.String r3 = "activated_change_date"
            long r3 = r2.getLong(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            r1.c(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            java.lang.String r3 = "application_category"
            java.lang.String r3 = r2.getString(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            r1.g(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            java.lang.String r3 = "application_id"
            java.lang.String r3 = r2.getString(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            r1.h(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            java.lang.String r3 = "application_platform"
            java.lang.String r3 = r2.getString(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            r1.i(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            java.lang.String r3 = "application_version"
            java.lang.String r3 = r2.getString(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            r1.j(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            java.lang.String r3 = "activation_history"
            java.lang.String r3 = r2.getString(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            r1.k(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            java.lang.String r3 = "linked_key_list"
            java.lang.String r3 = r2.getString(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            r1.l(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            java.lang.String r3 = "enable_apps"
            boolean r3 = r2.getBoolean(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            r1.c(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            java.lang.String r3 = "enable_calls"
            boolean r3 = r2.getBoolean(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            r1.d(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            java.lang.String r3 = "enable_location"
            boolean r3 = r2.getBoolean(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            r1.e(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            java.lang.String r3 = "enable_mms"
            boolean r3 = r2.getBoolean(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            r1.f(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            java.lang.String r3 = "enable_sms"
            boolean r3 = r2.getBoolean(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            r1.g(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            java.lang.String r3 = "enable_web"
            boolean r2 = r2.getBoolean(r3)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            r1.h(r2)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            a(r0)
            return r1
        L_0x011b:
            com.agilebinary.mobilemonitor.client.android.a.q r1 = new com.agilebinary.mobilemonitor.client.android.a.q     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            int r2 = r0.b()     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            r1.<init>(r2)     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
            throw r1     // Catch:{ JSONException -> 0x0125, IOException -> 0x0148, all -> 0x0143 }
        L_0x0125:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x0129:
            com.agilebinary.mobilemonitor.client.android.a.q r2 = new com.agilebinary.mobilemonitor.client.android.a.q     // Catch:{ all -> 0x012f }
            r2.<init>(r0)     // Catch:{ all -> 0x012f }
            throw r2     // Catch:{ all -> 0x012f }
        L_0x012f:
            r0 = move-exception
        L_0x0130:
            a(r1)
            throw r0
        L_0x0134:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x0138:
            com.agilebinary.mobilemonitor.client.android.a.q r2 = new com.agilebinary.mobilemonitor.client.android.a.q     // Catch:{ all -> 0x012f }
            r2.<init>(r0)     // Catch:{ all -> 0x012f }
            throw r2     // Catch:{ all -> 0x012f }
        L_0x013e:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0130
        L_0x0143:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0130
        L_0x0148:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0138
        L_0x014d:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0129
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.a.b.a.xa(java.lang.String, java.lang.String, com.agilebinary.mobilemonitor.client.android.a.g):com.agilebinary.mobilemonitor.client.android.a.a.f");
    }

    private final void xa(com.agilebinary.mobilemonitor.client.android.d dVar, String str, g gVar) {
        s sVar;
        Throwable th;
        try {
            StringBuilder sb = new StringBuilder();
            t.a();
            String format = String.format(sb.append("https://c.biige.com/").append("ServiceAccountUpdatePassword?ProtocolVersion=%1$s&Key=%2$s&Password=%3$s&NewPassword=%4$s").toString(), "1", dVar.b(), dVar.c(), str);
            t.a();
            s a2 = this.f151a.a(format, new byte[0], gVar);
            try {
                if (!a2.a()) {
                    throw new q(a2.b());
                }
                a(a2);
            } catch (Throwable th2) {
                Throwable th3 = th2;
                sVar = a2;
                th = th3;
                a(sVar);
                throw th;
            }
        } catch (Throwable th4) {
            Throwable th5 = th4;
            sVar = null;
            th = th5;
        }
    }

    private final void xb(com.agilebinary.mobilemonitor.client.android.d dVar, String str, g gVar) {
        s sVar;
        Throwable th;
        try {
            StringBuilder sb = new StringBuilder();
            t.a();
            String format = String.format(sb.append("https://c.biige.com/").append("ServiceAccountUpdateEmail?ProtocolVersion=%1$s&Key=%2$s&Password=%3$s&Email=%4$s").toString(), "1", dVar.b(), dVar.c(), str);
            t.a();
            s a2 = this.f151a.a(format, new byte[0], gVar);
            try {
                if (!a2.a()) {
                    throw new q(a2.b());
                }
                a(a2);
            } catch (Throwable th2) {
                Throwable th3 = th2;
                sVar = a2;
                th = th3;
                a(sVar);
                throw th;
            }
        } catch (Throwable th4) {
            Throwable th5 = th4;
            sVar = null;
            th = th5;
        }
    }

    private final void xb(String str, String str2, g gVar) {
        s sVar;
        Throwable th;
        try {
            StringBuilder sb = new StringBuilder();
            t.a();
            String format = String.format(sb.append("https://c.biige.com/").append("ServiceAccountUpdatePasswordReset?ProtocolVersion=%1$s&Key=%2$s&Locale=%3$s").toString(), "1", str, str2);
            t.a();
            s a2 = this.f151a.a(format, new byte[0], gVar);
            try {
                if (!a2.a()) {
                    throw new q(a2.b());
                }
                a(a2);
            } catch (Throwable th2) {
                Throwable th3 = th2;
                sVar = a2;
                th = th3;
                a(sVar);
                throw th;
            }
        } catch (Throwable th4) {
            Throwable th5 = th4;
            sVar = null;
            th = th5;
        }
    }

    public final f a(String str, String str2, g gVar) {
        return xa(str, str2, gVar);
    }

    public final void a(com.agilebinary.mobilemonitor.client.android.d dVar, String str, g gVar) {
        xa(dVar, str, gVar);
    }

    public final void b(com.agilebinary.mobilemonitor.client.android.d dVar, String str, g gVar) {
        xb(dVar, str, gVar);
    }

    public final void b(String str, String str2, g gVar) {
        xb(str, str2, gVar);
    }
}
