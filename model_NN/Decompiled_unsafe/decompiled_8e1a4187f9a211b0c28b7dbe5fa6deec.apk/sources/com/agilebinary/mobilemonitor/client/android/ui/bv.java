package com.agilebinary.mobilemonitor.client.android.ui;

import android.app.Dialog;
import android.view.View;

final class bv implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ Dialog f260a;
    private /* synthetic */ MapActivity_GOOGLE b;

    bv(MapActivity_GOOGLE mapActivity_GOOGLE, Dialog dialog) {
        this.b = mapActivity_GOOGLE;
        this.f260a = dialog;
    }

    private final void xonClick(View view) {
        this.b.d.a(!this.b.d.a());
        this.b.b.invalidate();
        this.b.i.b("MAP_LAYER_SCALEBAR__BOOL", this.b.e.a());
        this.f260a.dismiss();
    }

    public final void onClick(View view) {
        xonClick(view);
    }
}
