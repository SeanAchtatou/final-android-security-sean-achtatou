package com.agilebinary.mobilemonitor.client.android.ui.map;

import android.app.Activity;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Picture;
import android.graphics.Rect;
import android.os.Build;
import org.osmdroid.b.a;
import org.osmdroid.b.a.e;
import org.osmdroid.b.f;
import org.osmdroid.util.GeoPoint;

public final class b extends e {

    /* renamed from: a  reason: collision with root package name */
    private float f278a = 10.0f;
    private float b = 10.0f;
    private float c = 2.0f;
    private int d = 12;
    private int e = 0;
    private boolean f = false;
    private boolean g = false;
    private boolean h = true;
    private boolean i = false;
    private final Activity j;
    private Picture k = new Picture();
    private int l = -1;
    private float m = 0.0f;
    private float n;
    private float o;
    private int p;
    private int q;
    private final org.osmdroid.b r;
    private Matrix s;
    private final Paint t;
    private final Paint u;
    private a v;

    public b(Activity activity, org.osmdroid.b bVar) {
        super(bVar);
        String str;
        this.r = bVar;
        this.j = activity;
        this.t = new Paint();
        this.t.setColor(-16777216);
        this.t.setAntiAlias(true);
        this.t.setStyle(Paint.Style.FILL);
        this.t.setAlpha(255);
        this.u = new Paint();
        this.u.setColor(-16777216);
        this.u.setAntiAlias(true);
        this.u.setStyle(Paint.Style.FILL);
        this.u.setAlpha(255);
        this.u.setTextSize((float) this.d);
        this.n = this.j.getResources().getDisplayMetrics().xdpi;
        this.o = this.j.getResources().getDisplayMetrics().ydpi;
        this.p = this.j.getResources().getDisplayMetrics().widthPixels;
        this.q = this.j.getResources().getDisplayMetrics().heightPixels;
        try {
            str = (String) Build.class.getField("MANUFACTURER").get(null);
        } catch (Exception e2) {
            str = null;
        }
        if (!"motorola".equals(str) || !"DROIDX".equals(Build.MODEL)) {
            if ("motorola".equals(str) && "Droid".equals(Build.MODEL)) {
                this.n = 264.0f;
                this.o = 264.0f;
            }
        } else if (activity.getWindowManager().getDefaultDisplay().getOrientation() > 0) {
            this.n = (float) (((double) this.p) / 3.75d);
            this.o = (float) (((double) this.q) / 2.1d);
        } else {
            this.n = (float) (((double) this.p) / 2.1d);
            this.o = (float) (((double) this.q) / 3.75d);
        }
    }

    private String a(int i2) {
        return xa(i2);
    }

    private String xa(int i2) {
        if (this.f) {
            if (((double) i2) >= 8046.72d) {
                return this.r.a(org.osmdroid.a.format_distance_miles, Integer.valueOf((int) (((double) i2) / 1609.344d)));
            } else if (((double) i2) >= 321.8688d) {
                return this.r.a(org.osmdroid.a.format_distance_miles, Double.valueOf(((double) ((int) (((double) i2) / 160.9344d))) / 10.0d));
            } else {
                return this.r.a(org.osmdroid.a.format_distance_feet, Integer.valueOf((int) (((double) i2) * 3.2808399d)));
            }
        } else if (this.g) {
            if (((double) i2) >= 9260.0d) {
                return this.r.a(org.osmdroid.a.format_distance_nautical_miles, Integer.valueOf((int) (((double) i2) / 1852.0d)));
            } else if (((double) i2) >= 370.4d) {
                return this.r.a(org.osmdroid.a.format_distance_nautical_miles, Double.valueOf(((double) ((int) (((double) i2) / 185.2d))) / 10.0d));
            } else {
                return this.r.a(org.osmdroid.a.format_distance_feet, Integer.valueOf((int) (((double) i2) * 3.2808399d)));
            }
        } else if (i2 >= 5000) {
            return this.r.a(org.osmdroid.a.format_distance_kilometers, Integer.valueOf(i2 / 1000));
        } else if (i2 >= 200) {
            return this.r.a(org.osmdroid.a.format_distance_kilometers, Double.valueOf(((double) ((int) (((double) i2) / 100.0d))) / 10.0d));
        } else {
            return this.r.a(org.osmdroid.a.format_distance_meters, Integer.valueOf(i2));
        }
    }

    private final void xa(float f2) {
        this.f278a = f2;
        this.b = 10.0f;
    }

    private final void xa(Canvas canvas) {
    }

    private final void xa(Canvas canvas, f fVar) {
        a e2;
        if (!fVar.i()) {
            int f2 = fVar.f();
            if (a() && f2 >= this.e && (e2 = fVar.e()) != null) {
                GeoPoint a2 = e2.a(this.p / 2, this.q / 2);
                if (!(f2 == this.l && ((int) (((double) a2.a()) / 1000000.0d)) == ((int) (((double) this.m) / 1000000.0d)))) {
                    this.l = f2;
                    this.m = (float) a2.a();
                    this.v = fVar.e();
                    if (this.v != null) {
                        int a3 = this.v.a(((float) (this.p / 2)) - (this.n / 2.0f), (float) (this.q / 2)).a(this.v.a(((float) (this.p / 2)) + (this.n / 2.0f), (float) (this.q / 2)));
                        int a4 = this.v.a((float) (this.p / 2), ((float) (this.q / 2)) - (this.o / 2.0f)).a(this.v.a((float) (this.p / 2), ((float) (this.q / 2)) + (this.o / 2.0f)));
                        Canvas beginRecording = this.k.beginRecording((int) this.n, (int) this.o);
                        if (this.h) {
                            String a5 = a(a3);
                            Rect rect = new Rect();
                            this.u.getTextBounds(a5, 0, a5.length(), rect);
                            int height = (int) (((double) rect.height()) / 5.0d);
                            beginRecording.drawRect(0.0f, 0.0f, this.n, this.c, this.t);
                            beginRecording.drawRect(this.n, 0.0f, this.n + this.c, ((float) rect.height()) + this.c + ((float) height), this.t);
                            if (!this.i) {
                                beginRecording.drawRect(0.0f, 0.0f, this.c, ((float) rect.height()) + this.c + ((float) height), this.t);
                            }
                            beginRecording.drawText(a5, (this.n / 2.0f) - ((float) (rect.width() / 2)), ((float) rect.height()) + this.c + ((float) height), this.u);
                        }
                        if (this.i) {
                            String a6 = a(a4);
                            Rect rect2 = new Rect();
                            this.u.getTextBounds(a6, 0, a6.length(), rect2);
                            int height2 = (int) (((double) rect2.height()) / 5.0d);
                            beginRecording.drawRect(0.0f, 0.0f, this.c, this.o, this.t);
                            beginRecording.drawRect(0.0f, this.o, ((float) rect2.height()) + this.c + ((float) height2), this.o + this.c, this.t);
                            if (!this.h) {
                                beginRecording.drawRect(0.0f, 0.0f, ((float) rect2.height()) + this.c + ((float) height2), this.c, this.t);
                            }
                            float height3 = ((float) rect2.height()) + this.c + ((float) height2);
                            float width = (this.o / 2.0f) + ((float) (rect2.width() / 2));
                            beginRecording.rotate(-90.0f, height3, width);
                            beginRecording.drawText(a6, height3, width + ((float) height2), this.u);
                        }
                        this.k.endRecording();
                    }
                }
                this.s = canvas.getMatrix();
                canvas.restore();
                canvas.save();
                canvas.translate(this.f278a, this.b);
                canvas.drawPicture(this.k);
                canvas.setMatrix(this.s);
            }
        }
    }

    public final void a(float f2) {
        xa(f2);
    }

    /* access modifiers changed from: protected */
    public final void a(Canvas canvas) {
        xa(canvas);
    }

    public final void a(Canvas canvas, f fVar) {
        xa(canvas, fVar);
    }
}
