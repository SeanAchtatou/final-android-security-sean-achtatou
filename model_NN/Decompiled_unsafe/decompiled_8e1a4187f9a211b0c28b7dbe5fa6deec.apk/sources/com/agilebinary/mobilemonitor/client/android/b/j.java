package com.agilebinary.mobilemonitor.client.android.b;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDoneException;
import android.database.sqlite.SQLiteStatement;
import com.agilebinary.mobilemonitor.a.a.a.a;
import com.agilebinary.mobilemonitor.client.a.b.o;
import com.agilebinary.mobilemonitor.client.a.b.s;
import com.agilebinary.mobilemonitor.client.a.c;
import com.agilebinary.mobilemonitor.client.android.MyApplication;
import com.agilebinary.mobilemonitor.client.android.a.f;
import com.agilebinary.mobilemonitor.client.android.c.b;
import com.agilebinary.mobilemonitor.client.android.i;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.OptionalDataException;
import java.io.StreamCorruptedException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public final class j implements f {

    /* renamed from: a  reason: collision with root package name */
    public static final byte[] f174a = {1, 2, 3, 4, 6, 8, 9};
    private static String b = b.a();
    private Context c;
    private SQLiteDatabase d;
    private SQLiteStatement e;
    private SQLiteStatement f;
    private SQLiteStatement g;
    private SQLiteStatement h;
    private SQLiteStatement i;
    private SQLiteStatement j;
    private SQLiteStatement k;
    private SQLiteStatement l;
    private SQLiteStatement m;
    private byte n = -1;
    private SQLiteStatement o;
    private SQLiteStatement p;
    private List q;
    private i r;
    private Calendar s;
    private ByteArrayOutputStream t = new ByteArrayOutputStream();
    private SQLiteStatement u;
    private SQLiteStatement v;
    private ByteArrayOutputStream w;

    public j(Context context, i iVar) {
        this.c = context;
        this.r = iVar;
        this.d = new e(this, this.c, "events").getWritableDatabase();
        this.e = this.d.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s,%4$s,%5$s,%6$s,%7$s,%8$s,%9$s,%10$s,%11$s,%12$s) values (?,?,?,?,?,?,?,?,?,?,?) ", "location", f.b, f.c, f.d, "line1", "line2", "accuracy", "lat", "lon", "contenttype", "powersave", "valloc"));
        this.f = this.d.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s,%4$s,%5$s,%6$s) values (?,?,?,?,?) ", "call", c.b, c.c, c.d, "line1", "line2"));
        this.g = this.d.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s,%4$s,%5$s,%6$s) values (?,?,?,?,?) ", "sms", a.b, a.c, a.d, "dir", "text"));
        this.h = this.d.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s,%4$s,%5$s,%6$s,%7$s) values (?,?,?,?,?,?) ", "mms", d.b, d.c, d.d, "dir", "text", "thumbnail"));
        this.i = this.d.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s,%4$s,%5$s,%6$s) values (?,?,?,?,?) ", "web", k.b, k.c, k.d, "line1", "line2"));
        this.j = this.d.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s,%4$s,%5$s) values (?,?,?,?) ", "syst_m", l.b, l.c, l.d, "line1"));
        this.k = this.d.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s,%4$s,%5$s,%6$s) values (?,?,?,?,?) ", i.f173a, i.b, i.c, i.d, i.f, i.g));
        this.l = this.d.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s) values (?,?) ", "curday", "eventtype", "day"));
        this.p = this.d.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s) values (?,?) ", "oldestevent", "eventtype", "oldest"));
        this.u = this.d.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s) values (?,?) ", "lastseen", "eventtype", "lastid"));
        this.m = this.d.compileStatement(String.format("SELECT %3$s FROM %1$s where %2$s = ? ", "curday", "eventtype", "day"));
        this.o = this.d.compileStatement(String.format("SELECT %3$s FROM %1$s where %2$s = ? ", "oldestevent", "eventtype", "oldest"));
        this.v = this.d.compileStatement(String.format("SELECT %3$s FROM %1$s where %2$s = ? ", "lastseen", "eventtype", "lastid"));
    }

    private static int a(Calendar calendar) {
        return xa(calendar);
    }

    private synchronized void a(byte b2, o oVar) {
        xa(b2, oVar);
    }

    private static void a(SQLiteDatabase sQLiteDatabase, String str) {
        xa(sQLiteDatabase, str);
    }

    public static void e() {
        xe();
    }

    private static String h(byte b2) {
        return xh(b2);
    }

    private synchronized void h(byte b2, long j2) {
        xh(b2, j2);
    }

    private static int xa(Calendar calendar) {
        return (calendar.get(1) * 1000) + calendar.get(6);
    }

    private final synchronized long xa(byte b2) {
        long j2;
        "" + ((int) b2);
        this.o.bindLong(1, (long) b2);
        j2 = 0;
        try {
            j2 = this.o.simpleQueryForLong();
        } catch (SQLiteDoneException e2) {
        }
        " =" + j2;
        return j2;
    }

    private final long xa(byte b2, long j2) {
        "" + ((int) b2);
        " startOfDay=" + j2;
        SQLiteStatement compileStatement = this.d.compileStatement(String.format("SELECT max(%2$s) FROM %1$s  ", h(b2), b.b));
        long simpleQueryForLong = compileStatement.simpleQueryForLong();
        compileStatement.close();
        " =" + simpleQueryForLong;
        return simpleQueryForLong == 0 ? j2 : simpleQueryForLong;
    }

    private final List xa(List list) {
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            arrayList.add((s) g((byte) 1, ((Long) it.next()).longValue()));
        }
        return arrayList;
    }

    private final void xa() {
        this.d.close();
    }

    private final synchronized void xa(byte b2, int i2, long j2) {
        " / " + ((int) b2);
        " / " + i2;
        " / " + new Date(j2);
        if (this.n != -1) {
            throw new IllegalStateException();
        }
        this.n = b2;
        if (b2 == 1) {
            this.q = new ArrayList();
            this.r.a();
        }
        h(b2, j2);
        this.w = new ByteArrayOutputStream(32000);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x005a A[Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x011a A[Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x01a5 A[Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x01c4 A[SYNTHETIC, Splitter:B:66:0x01c4] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x01ec A[Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x021d A[Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x022c A[Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0038 A[Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void xa(byte r12, com.agilebinary.mobilemonitor.client.a.b.o r13) {
        /*
            r11 = this;
            monitor-enter(r11)
            r1 = 0
            switch(r12) {
                case 1: goto L_0x003e;
                case 2: goto L_0x0042;
                case 3: goto L_0x0046;
                case 4: goto L_0x004a;
                case 5: goto L_0x0005;
                case 6: goto L_0x004e;
                case 7: goto L_0x0005;
                case 8: goto L_0x0052;
                case 9: goto L_0x0056;
                default: goto L_0x0005;
            }
        L_0x0005:
            r2 = r1
        L_0x0006:
            java.io.ByteArrayOutputStream r1 = r11.t     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r1.reset()     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            java.io.ObjectOutputStream r1 = new java.io.ObjectOutputStream     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            java.io.ByteArrayOutputStream r3 = r11.t     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r1.<init>(r3)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r1.writeObject(r13)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r1.flush()     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r2.clearBindings()     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r1 = 1
            long r3 = r13.u()     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r2.bindLong(r1, r3)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r1 = 2
            long r3 = r13.t()     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r2.bindLong(r1, r3)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r1 = 3
            java.io.ByteArrayOutputStream r3 = r11.t     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            byte[] r3 = r3.toByteArray()     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r2.bindBlob(r1, r3)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            switch(r12) {
                case 1: goto L_0x005a;
                case 2: goto L_0x011a;
                case 3: goto L_0x01a5;
                case 4: goto L_0x01c4;
                case 5: goto L_0x0038;
                case 6: goto L_0x01ec;
                case 7: goto L_0x0038;
                case 8: goto L_0x021d;
                case 9: goto L_0x022c;
                default: goto L_0x0038;
            }     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
        L_0x0038:
            r1 = r2
        L_0x0039:
            r1.executeInsert()     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
        L_0x003c:
            monitor-exit(r11)
            return
        L_0x003e:
            android.database.sqlite.SQLiteStatement r1 = r11.e     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r2 = r1
            goto L_0x0006
        L_0x0042:
            android.database.sqlite.SQLiteStatement r1 = r11.f     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r2 = r1
            goto L_0x0006
        L_0x0046:
            android.database.sqlite.SQLiteStatement r1 = r11.g     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r2 = r1
            goto L_0x0006
        L_0x004a:
            android.database.sqlite.SQLiteStatement r1 = r11.h     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r2 = r1
            goto L_0x0006
        L_0x004e:
            android.database.sqlite.SQLiteStatement r1 = r11.i     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r2 = r1
            goto L_0x0006
        L_0x0052:
            android.database.sqlite.SQLiteStatement r1 = r11.j     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r2 = r1
            goto L_0x0006
        L_0x0056:
            android.database.sqlite.SQLiteStatement r1 = r11.k     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r2 = r1
            goto L_0x0006
        L_0x005a:
            r0 = r13
            com.agilebinary.mobilemonitor.client.a.b.s r0 = (com.agilebinary.mobilemonitor.client.a.b.s) r0     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r1 = r0
            r3 = 4
            android.content.Context r4 = r11.c     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            java.lang.String r4 = r1.b(r4)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r2.bindString(r3, r4)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r3 = 5
            android.content.Context r4 = r11.c     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            java.lang.String r4 = r1.c(r4)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r2.bindString(r3, r4)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r3 = 6
            java.lang.Double r1 = r1.f()     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            if (r1 != 0) goto L_0x00bc
            r4 = -4616189618054758400(0xbff0000000000000, double:-1.0)
        L_0x007b:
            r2.bindDouble(r3, r4)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r1 = 9
            byte r3 = r13.k()     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            long r3 = (long) r3     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r2.bindLong(r1, r3)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            boolean r1 = r13 instanceof com.agilebinary.mobilemonitor.client.a.b.a     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            if (r1 == 0) goto L_0x00c7
            com.agilebinary.mobilemonitor.client.a.b.a r13 = (com.agilebinary.mobilemonitor.client.a.b.a) r13     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r1 = 7
            double r3 = r13.a()     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r2.bindDouble(r1, r3)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r1 = 8
            double r3 = r13.b()     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r2.bindDouble(r1, r3)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r1 = 10
            boolean r3 = r13.e()     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            if (r3 == 0) goto L_0x00c1
            r3 = 1
        L_0x00a9:
            r2.bindLong(r1, r3)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r1 = 11
            boolean r3 = r13.d()     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            if (r3 == 0) goto L_0x00c4
            r3 = 1
        L_0x00b6:
            r2.bindLong(r1, r3)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r1 = r2
            goto L_0x0039
        L_0x00bc:
            double r4 = r1.doubleValue()     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            goto L_0x007b
        L_0x00c1:
            r3 = 0
            goto L_0x00a9
        L_0x00c4:
            r3 = 0
            goto L_0x00b6
        L_0x00c7:
            boolean r1 = r13 instanceof com.agilebinary.mobilemonitor.client.a.b.n     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            if (r1 == 0) goto L_0x0104
            com.agilebinary.mobilemonitor.client.a.b.n r13 = (com.agilebinary.mobilemonitor.client.a.b.n) r13     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            com.agilebinary.mobilemonitor.client.android.a.a.b r1 = r13.l()     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            if (r1 == 0) goto L_0x00f1
            r3 = 7
            double r4 = r1.a()     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r2.bindDouble(r3, r4)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r3 = 8
            double r4 = r1.b()     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r2.bindDouble(r3, r4)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
        L_0x00e4:
            r1 = 10
            r2.bindNull(r1)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r1 = 11
            r2.bindNull(r1)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r1 = r2
            goto L_0x0039
        L_0x00f1:
            r1 = 7
            r2.bindNull(r1)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r1 = 8
            r2.bindNull(r1)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            goto L_0x00e4
        L_0x00fb:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x0101 }
            goto L_0x003c
        L_0x0101:
            r1 = move-exception
            monitor-exit(r11)
            throw r1
        L_0x0104:
            r1 = 7
            r2.bindNull(r1)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r1 = 8
            r2.bindNull(r1)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r1 = 10
            r2.bindNull(r1)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r1 = 11
            r2.bindNull(r1)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r1 = r2
            goto L_0x0039
        L_0x011a:
            com.agilebinary.mobilemonitor.client.a.b.u r13 = (com.agilebinary.mobilemonitor.client.a.b.u) r13     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r1 = 4
            java.lang.String r3 = r13.e()     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            java.lang.String r4 = r13.f()     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            java.lang.String r3 = com.agilebinary.mobilemonitor.client.android.c.a.b(r3, r4)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r2.bindString(r1, r3)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r1 = 5
            android.content.Context r3 = r11.c     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            java.lang.String r4 = ""
            java.lang.String r5 = ""
            long r6 = r13.c()     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            long r8 = r13.b()     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            long r6 = r6 - r8
            r8 = 1000(0x3e8, double:4.94E-321)
            long r6 = r6 / r8
            int r6 = (int) r6     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            byte r7 = r13.d()     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            switch(r7) {
                case -1: goto L_0x0196;
                case 0: goto L_0x0147;
                case 1: goto L_0x0159;
                case 2: goto L_0x0168;
                case 3: goto L_0x0187;
                default: goto L_0x0147;
            }     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
        L_0x0147:
            r3 = r4
            r4 = r5
        L_0x0149:
            r5 = 1
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r6 = 0
            r5[r6] = r4     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            java.lang.String r3 = java.lang.String.format(r3, r5)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r2.bindString(r1, r3)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r1 = r2
            goto L_0x0039
        L_0x0159:
            r4 = 2131099859(0x7f0600d3, float:1.7812083E38)
            java.lang.String r4 = r3.getString(r4)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r5 = 2131099864(0x7f0600d8, float:1.7812093E38)
            java.lang.String r3 = r3.getString(r5)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            goto L_0x0149
        L_0x0168:
            r4 = 2131099865(0x7f0600d9, float:1.7812095E38)
            java.lang.String r4 = r3.getString(r4)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            if (r6 <= 0) goto L_0x017c
            r5 = 2131099860(0x7f0600d4, float:1.7812085E38)
            java.lang.String r3 = r3.getString(r5)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r10 = r4
            r4 = r3
            r3 = r10
            goto L_0x0149
        L_0x017c:
            r5 = 2131099863(0x7f0600d7, float:1.7812091E38)
            java.lang.String r3 = r3.getString(r5)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r10 = r4
            r4 = r3
            r3 = r10
            goto L_0x0149
        L_0x0187:
            r4 = 2131099862(0x7f0600d6, float:1.781209E38)
            java.lang.String r4 = r3.getString(r4)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r5 = 2131099864(0x7f0600d8, float:1.7812093E38)
            java.lang.String r3 = r3.getString(r5)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            goto L_0x0149
        L_0x0196:
            r4 = 2131099861(0x7f0600d5, float:1.7812087E38)
            java.lang.String r4 = r3.getString(r4)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r5 = 2131099865(0x7f0600d9, float:1.7812095E38)
            java.lang.String r3 = r3.getString(r5)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            goto L_0x0149
        L_0x01a5:
            android.database.sqlite.SQLiteStatement r1 = r11.g     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            com.agilebinary.mobilemonitor.client.a.b.j r13 = (com.agilebinary.mobilemonitor.client.a.b.j) r13     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r2 = 4
            android.content.Context r3 = r11.c     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            java.lang.String r3 = r13.a(r3)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r1.bindString(r2, r3)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r2 = 5
            java.lang.String r3 = r13.d()     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r1.bindString(r2, r3)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            goto L_0x0039
        L_0x01bd:
            r1 = move-exception
            com.agilebinary.mobilemonitor.client.android.b.h r2 = new com.agilebinary.mobilemonitor.client.android.b.h     // Catch:{ all -> 0x0101 }
            r2.<init>(r1)     // Catch:{ all -> 0x0101 }
            throw r2     // Catch:{ all -> 0x0101 }
        L_0x01c4:
            com.agilebinary.mobilemonitor.client.a.b.f r13 = (com.agilebinary.mobilemonitor.client.a.b.f) r13     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r1 = 4
            android.content.Context r3 = r11.c     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            java.lang.String r3 = r13.a(r3)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r2.bindString(r1, r3)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r1 = 5
            java.lang.String r3 = r13.l()     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r2.bindString(r1, r3)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            byte[] r1 = r13.m()     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            if (r1 != 0) goto L_0x01e5
            r1 = 6
            r2.bindNull(r1)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r1 = r2
            goto L_0x0039
        L_0x01e5:
            r3 = 6
            r2.bindBlob(r3, r1)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r1 = r2
            goto L_0x0039
        L_0x01ec:
            boolean r1 = r13 instanceof com.agilebinary.mobilemonitor.client.a.b.e     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            if (r1 == 0) goto L_0x0204
            r0 = r13
            com.agilebinary.mobilemonitor.client.a.b.e r0 = (com.agilebinary.mobilemonitor.client.a.b.e) r0     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r1 = r0
            r3 = 4
            android.content.Context r4 = r11.c     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            java.lang.String r1 = r1.a(r4)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r2.bindString(r3, r1)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r1 = 5
            java.lang.String r3 = ""
            r2.bindString(r1, r3)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
        L_0x0204:
            boolean r1 = r13 instanceof com.agilebinary.mobilemonitor.client.a.b.g     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            if (r1 == 0) goto L_0x0038
            com.agilebinary.mobilemonitor.client.a.b.g r13 = (com.agilebinary.mobilemonitor.client.a.b.g) r13     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r1 = 4
            java.lang.String r3 = r13.f()     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r2.bindString(r1, r3)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r1 = 5
            java.lang.String r3 = r13.g()     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r2.bindString(r1, r3)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r1 = r2
            goto L_0x0039
        L_0x021d:
            com.agilebinary.mobilemonitor.client.a.b.d r13 = (com.agilebinary.mobilemonitor.client.a.b.d) r13     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r1 = 4
            android.content.Context r3 = r11.c     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            java.lang.String r3 = r13.a(r3)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r2.bindString(r1, r3)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r1 = r2
            goto L_0x0039
        L_0x022c:
            com.agilebinary.mobilemonitor.client.a.b.c r13 = (com.agilebinary.mobilemonitor.client.a.b.c) r13     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r1 = 4
            android.content.Context r3 = r11.c     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            java.lang.String r3 = r13.a(r3)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r2.bindString(r1, r3)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r1 = 5
            java.lang.String r3 = r13.c()     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            r2.bindString(r1, r3)     // Catch:{ Exception -> 0x00fb, OutOfMemoryError -> 0x01bd }
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.b.j.xa(byte, com.agilebinary.mobilemonitor.client.a.b.o):void");
    }

    private static void xa(SQLiteDatabase sQLiteDatabase, String str) {
        sQLiteDatabase.execSQL(String.format("DELETE FROM %1$s", str));
    }

    private final synchronized void xa(String str, byte b2, long j2, long j3, boolean z, boolean z2, int i2, InputStream inputStream) {
        " type=" + ((int) b2);
        " id=" + j2;
        if (this.n != -1) {
            try {
                o a2 = com.agilebinary.mobilemonitor.client.a.f.a(str, j2, j3, z, z2, i2, inputStream, MyApplication.g(), MyApplication.f141a, this.c, this.w);
                if (a2 != null) {
                    if (this.n != 1) {
                        a(b2, a2);
                    } else if (!(a2 instanceof c) || !((c) a2).a()) {
                        a(b2, a2);
                    } else {
                        if (this.r.a((c) a2)) {
                            a(b2, a2);
                        } else {
                            this.q.add((c) a2);
                        }
                    }
                }
            } catch (IOException e2) {
                e2.printStackTrace();
            } catch (OutOfMemoryError e3) {
                throw new h(e3);
            }
        }
        return;
    }

    private final synchronized void xa(boolean z) {
        "" + z;
        this.w = null;
        if (this.n != -1) {
            if (!z) {
                if (this.n == 1) {
                    this.r.a(this.q);
                    for (c cVar : this.q) {
                        try {
                            a(this.n, (s) cVar);
                        } catch (h e2) {
                            e2.printStackTrace();
                        }
                    }
                    this.q.clear();
                }
            }
            this.n = -1;
            this.q = null;
            this.r.b();
        }
    }

    private final synchronized boolean xa(byte b2, Calendar calendar) {
        boolean z;
        "" + ((int) b2);
        " c=" + calendar;
        this.m.bindLong(1, (long) b2);
        try {
            z = this.m.simpleQueryForLong() == ((long) a(calendar));
        } catch (SQLiteDoneException e2) {
            z = false;
        }
        "=" + z;
        return z;
    }

    private final synchronized long xb(byte b2) {
        long j2;
        "" + ((int) b2);
        this.v.bindLong(1, (long) b2);
        j2 = -1;
        try {
            j2 = this.v.simpleQueryForLong();
        } catch (SQLiteDoneException e2) {
        }
        " =" + j2;
        return j2;
    }

    private final void xb() {
        a(this.d, "location");
        a(this.d, "call");
        a(this.d, "sms");
        a(this.d, "mms");
        a(this.d, "web");
        a(this.d, "syst_m");
        a(this.d, i.f173a);
        a(this.d, "lastseen");
        a(this.d, "curday");
        a(this.d, "oldestevent");
    }

    private final synchronized void xb(byte b2, long j2) {
        "" + ((int) b2);
        " ts=" + j2;
        if (j2 > b(b2)) {
            "updateLastssenEventTs = " + j2;
            this.u.bindLong(1, (long) b2);
            this.u.bindLong(2, j2);
            this.u.executeInsert();
        }
    }

    private final synchronized void xb(byte b2, Calendar calendar) {
        "" + ((int) b2);
        " c=" + calendar;
        this.s = calendar;
        int a2 = a(calendar);
        this.l.bindLong(1, (long) b2);
        this.l.bindLong(2, (long) a2);
        this.l.executeInsert();
    }

    private final void xc() {
        this.r.c();
    }

    private final void xc(byte b2) {
        a(this.d, h(b2));
    }

    private final boolean xc(byte b2, long j2) {
        SQLiteStatement compileStatement = this.d.compileStatement(String.format("SELECT count(*) FROM %1$s where %2$s<? ", h(b2), b.b));
        compileStatement.bindLong(1, j2);
        try {
            long simpleQueryForLong = compileStatement.simpleQueryForLong();
            compileStatement.close();
            return simpleQueryForLong > 0;
        } catch (Exception e2) {
            a.e(e2);
            compileStatement.close();
            return false;
        }
    }

    private final o xd(byte b2, long j2) {
        SQLiteStatement compileStatement = this.d.compileStatement(String.format("SELECT max(%2$s) FROM %1$s where %2$s<? ", h(b2), b.b));
        compileStatement.bindLong(1, j2);
        try {
            long simpleQueryForLong = compileStatement.simpleQueryForLong();
            compileStatement.close();
            return g(b2, simpleQueryForLong);
        } catch (Exception e2) {
            a.e(e2);
            compileStatement.close();
            return null;
        }
    }

    private final Calendar xd() {
        "" + this.s;
        return this.s;
    }

    private final synchronized void xd(byte b2) {
        "" + ((int) b2);
        b(b2, a(b2, 0));
    }

    private final synchronized Cursor xe(byte b2) {
        Cursor query;
        String[] strArr = null;
        synchronized (this) {
            SQLiteDatabase sQLiteDatabase = this.d;
            String h2 = h(b2);
            switch (b2) {
                case 1:
                    strArr = f.f171a;
                    break;
                case 2:
                    strArr = c.f168a;
                    break;
                case 3:
                    strArr = a.f166a;
                    break;
                case 4:
                    strArr = d.f169a;
                    break;
                case 6:
                    strArr = k.f175a;
                    break;
                case 8:
                    strArr = l.f176a;
                    break;
                case 9:
                    strArr = i.h;
                    break;
            }
            query = sQLiteDatabase.query(h2, strArr, null, null, null, null, b.b + " DESC");
        }
        return query;
    }

    private static void xe() {
        SQLiteDatabase.releaseMemory();
    }

    private final boolean xe(byte b2, long j2) {
        SQLiteStatement compileStatement = this.d.compileStatement(String.format("SELECT count(*) FROM %1$s where %2$s>? ", h(b2), b.b));
        compileStatement.bindLong(1, j2);
        try {
            long simpleQueryForLong = compileStatement.simpleQueryForLong();
            compileStatement.close();
            return simpleQueryForLong > 0;
        } catch (Exception e2) {
            a.e(e2);
            compileStatement.close();
            return false;
        }
    }

    private final o xf(byte b2) {
        SQLiteStatement compileStatement = this.d.compileStatement(String.format("SELECT min(%2$s) FROM %1$s  ", h(b2), b.b));
        try {
            long simpleQueryForLong = compileStatement.simpleQueryForLong();
            compileStatement.close();
            return g(b2, simpleQueryForLong);
        } catch (Exception e2) {
            a.e(e2);
            compileStatement.close();
            return null;
        }
    }

    private final o xf(byte b2, long j2) {
        SQLiteStatement compileStatement = this.d.compileStatement(String.format("SELECT min(%2$s) FROM %1$s where %2$s>? ", h(b2), b.b));
        compileStatement.bindLong(1, j2);
        try {
            long simpleQueryForLong = compileStatement.simpleQueryForLong();
            compileStatement.close();
            return g(b2, simpleQueryForLong);
        } catch (Exception e2) {
            a.e(e2);
            compileStatement.close();
            return null;
        }
    }

    private final o xg(byte b2) {
        SQLiteStatement compileStatement = this.d.compileStatement(String.format("SELECT max(%2$s) FROM %1$s  ", h(b2), b.b));
        try {
            long simpleQueryForLong = compileStatement.simpleQueryForLong();
            compileStatement.close();
            return g(b2, simpleQueryForLong);
        } catch (Exception e2) {
            a.e(e2);
            compileStatement.close();
            return null;
        }
    }

    private final o xg(byte b2, long j2) {
        Cursor query = this.d.query(h(b2), b.e, b.b + "=?", new String[]{String.valueOf(j2)}, null, null, null);
        if (!query.moveToFirst()) {
            query.close();
            return null;
        }
        try {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(query.getBlob(0));
            ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
            o oVar = (o) objectInputStream.readObject();
            objectInputStream.close();
            byteArrayInputStream.close();
            query.close();
            return oVar;
        } catch (StreamCorruptedException e2) {
            a.e(e2);
            query.close();
            return null;
        } catch (OptionalDataException e3) {
            a.e(e3);
            query.close();
            return null;
        } catch (IOException e4) {
            a.e(e4);
            query.close();
            return null;
        } catch (ClassNotFoundException e5) {
            a.e(e5);
            query.close();
            return null;
        } catch (OutOfMemoryError e6) {
            a.e(e6);
            query.close();
            return null;
        }
    }

    private static String xh(byte b2) {
        switch (b2) {
            case 1:
                return "location";
            case 2:
                return "call";
            case 3:
                return "sms";
            case 4:
                return "mms";
            case 5:
            case 7:
            default:
                return null;
            case 6:
                return "web";
            case 8:
                return "syst_m";
            case 9:
                return i.f173a;
        }
    }

    private synchronized void xh(byte b2, long j2) {
        "" + ((int) b2);
        " ts=" + j2;
        this.p.bindLong(1, (long) b2);
        this.p.bindLong(2, j2);
        this.p.executeInsert();
    }

    public final synchronized long a(byte b2) {
        return xa(b2);
    }

    public final long a(byte b2, long j2) {
        return xa(b2, j2);
    }

    public final List a(List list) {
        return xa(list);
    }

    public final void a() {
        xa();
    }

    public final synchronized void a(byte b2, int i2, long j2) {
        xa(b2, i2, j2);
    }

    public final synchronized void a(String str, byte b2, long j2, long j3, boolean z, boolean z2, int i2, InputStream inputStream) {
        xa(str, b2, j2, j3, z, z2, i2, inputStream);
    }

    public final synchronized void a(boolean z) {
        xa(z);
    }

    public final synchronized boolean a(byte b2, Calendar calendar) {
        return xa(b2, calendar);
    }

    public final synchronized long b(byte b2) {
        return xb(b2);
    }

    public final void b() {
        xb();
    }

    public final synchronized void b(byte b2, long j2) {
        xb(b2, j2);
    }

    public final synchronized void b(byte b2, Calendar calendar) {
        xb(b2, calendar);
    }

    public final void c() {
        xc();
    }

    public final void c(byte b2) {
        xc(b2);
    }

    public final boolean c(byte b2, long j2) {
        return xc(b2, j2);
    }

    public final o d(byte b2, long j2) {
        return xd(b2, j2);
    }

    public final Calendar d() {
        return xd();
    }

    public final synchronized void d(byte b2) {
        xd(b2);
    }

    public final synchronized Cursor e(byte b2) {
        return xe(b2);
    }

    public final boolean e(byte b2, long j2) {
        return xe(b2, j2);
    }

    public final o f(byte b2) {
        return xf(b2);
    }

    public final o f(byte b2, long j2) {
        return xf(b2, j2);
    }

    public final o g(byte b2) {
        return xg(b2);
    }

    public final o g(byte b2, long j2) {
        return xg(b2, j2);
    }
}
