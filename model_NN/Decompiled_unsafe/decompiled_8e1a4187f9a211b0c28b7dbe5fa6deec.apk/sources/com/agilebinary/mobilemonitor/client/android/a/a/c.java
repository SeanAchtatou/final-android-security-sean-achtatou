package com.agilebinary.mobilemonitor.client.android.a.a;

import com.agilebinary.mobilemonitor.client.android.c.b;
import java.io.Serializable;
import java.util.logging.Logger;

public class c implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private static final String f145a = b.a();
    private static final Logger b = Logger.getLogger(c.class.getName());
    private int c;
    private int d;
    private int e;
    private int f;

    public c() {
    }

    public c(int i, int i2, int i3, int i4) {
        this.c = i;
        this.d = i2;
        this.e = i3;
        this.f = i4;
    }

    private String xa() {
        return "{\"mobile_country_code\":\"" + this.c + "\", \"" + "mobile_network_code" + "\":\"" + this.d + "\", \"" + "cell_id" + "\":\"" + this.e + "\", \"" + "location_area_code" + "\":\"" + this.f + "\"}";
    }

    private final void xb(int i) {
        this.c = i;
    }

    private final int xc() {
        return this.c;
    }

    private final void xc(int i) {
        this.d = i;
    }

    private final int xd() {
        return this.d;
    }

    private final void xd(int i) {
        this.e = i;
    }

    private final int xe() {
        return this.e;
    }

    private final void xe(int i) {
        this.f = i;
    }

    private final int xf() {
        return this.f;
    }

    private String xtoString() {
        return "GeocodeGsmCell: " + a();
    }

    public String a() {
        return xa();
    }

    public final void b(int i) {
        xb(i);
    }

    public final int c() {
        return xc();
    }

    public final void c(int i) {
        xc(i);
    }

    public final int d() {
        return xd();
    }

    public final void d(int i) {
        xd(i);
    }

    public final int e() {
        return xe();
    }

    public final void e(int i) {
        xe(i);
    }

    public final int f() {
        return xf();
    }

    public String toString() {
        return xtoString();
    }
}
