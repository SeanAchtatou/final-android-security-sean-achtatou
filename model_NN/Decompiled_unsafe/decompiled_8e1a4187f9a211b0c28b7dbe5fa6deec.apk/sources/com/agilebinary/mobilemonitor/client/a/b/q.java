package com.agilebinary.mobilemonitor.client.a.b;

import com.agilebinary.mobilemonitor.client.a.a.c;
import com.agilebinary.mobilemonitor.client.a.e;

public abstract class q extends i {

    /* renamed from: a  reason: collision with root package name */
    private long f136a;
    private long b;
    private byte c;
    private String d;

    public q(String str, long j, long j2, c cVar, e eVar) {
        super(str, j, j2, cVar, eVar);
        this.f136a = eVar.a(cVar.d());
        this.b = eVar.a(cVar.d());
        this.c = cVar.b();
        this.d = cVar.g();
    }

    private final long xv() {
        return this.f136a;
    }

    private final long xw() {
        return this.b;
    }

    private final byte xx() {
        return this.c;
    }

    public final long v() {
        return xv();
    }

    public final long w() {
        return xw();
    }

    public final byte x() {
        return xx();
    }
}
