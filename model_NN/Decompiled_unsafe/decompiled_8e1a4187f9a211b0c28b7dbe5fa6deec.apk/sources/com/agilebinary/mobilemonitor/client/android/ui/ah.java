package com.agilebinary.mobilemonitor.client.android.ui;

import android.view.View;

final class ah implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ChangeEmailActivity f225a;

    ah(ChangeEmailActivity changeEmailActivity) {
        this.f225a = changeEmailActivity;
    }

    private final void xonClick(View view) {
        this.f225a.setResult(0);
        this.f225a.finish();
    }

    public final void onClick(View view) {
        xonClick(view);
    }
}
