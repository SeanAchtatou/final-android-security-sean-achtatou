package com.agilebinary.mobilemonitor.client.android.ui.a;

import android.os.AsyncTask;
import android.os.PowerManager;
import com.agilebinary.mobilemonitor.client.android.a.g;
import com.agilebinary.mobilemonitor.client.android.a.q;
import com.agilebinary.mobilemonitor.client.android.c.b;
import com.agilebinary.mobilemonitor.client.android.ui.BaseActivity;
import com.agilebinary.mobilemonitor.client.android.ui.ChangeEmailActivity;

public final class j extends AsyncTask implements g {

    /* renamed from: a  reason: collision with root package name */
    public static j f217a;
    private static String b = b.a();
    private final BaseActivity c;
    private com.agilebinary.a.a.a.d.c.b d;

    public j(BaseActivity baseActivity) {
        this.c = baseActivity;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public c xdoInBackground(String... strArr) {
        return xa(strArr);
    }

    private c xa(String... strArr) {
        PowerManager.WakeLock newWakeLock = ((PowerManager) this.c.getSystemService("power")).newWakeLock(6, b);
        newWakeLock.acquire();
        try {
            this.c.g.b().a().b(this.c.g.a(), strArr[0], this);
            this.c.g.b(strArr[0]);
            c cVar = new c(true);
            try {
                newWakeLock.release();
            } catch (Exception e) {
            }
            f217a = null;
            return cVar;
        } catch (q e2) {
            c cVar2 = new c(e2);
            try {
                newWakeLock.release();
            } catch (Exception e3) {
            }
            f217a = null;
            return cVar2;
        } catch (Throwable th) {
            try {
                newWakeLock.release();
            } catch (Exception e4) {
            }
            f217a = null;
            throw th;
        }
    }

    private final void xa() {
        cancel(false);
        if (this.d != null) {
            try {
                this.d.d();
            } catch (Exception e) {
            }
        }
    }

    private final void xa(com.agilebinary.a.a.a.d.c.b bVar) {
        this.d = bVar;
    }

    private final void xonCancelled() {
        ChangeEmailActivity.a(this.c, (c) null);
    }

    /* access modifiers changed from: private */
    /* renamed from: xonPostExecute */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        ChangeEmailActivity.a(this.c, (c) obj);
    }

    public final void a() {
        xa();
    }

    public final void a(com.agilebinary.a.a.a.d.c.b bVar) {
        xa(bVar);
    }

    /* access modifiers changed from: protected */
    public final void onCancelled() {
        xonCancelled();
    }
}
