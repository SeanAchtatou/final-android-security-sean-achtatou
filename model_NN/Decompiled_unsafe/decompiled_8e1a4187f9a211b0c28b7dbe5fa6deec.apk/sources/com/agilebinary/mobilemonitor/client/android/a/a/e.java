package com.agilebinary.mobilemonitor.client.android.a.a;

import java.io.Serializable;

public final class e extends d implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private long f147a;
    private int b;
    private b c;

    private final String xa() {
        return "{\"create_date\":\"" + this.f147a + "\", \"geocoder_source_id\":\"" + this.b + "\", \"" + "base_station_id" + "\":\"" + super.b() + "\", \"" + "network_id" + "\":\"" + super.c() + "\", \"" + "system_id" + "\":\"" + super.d() + "\", \"" + "latitude" + "\":\"" + this.c.f144a + "\", \"" + "longitude" + "\":\"" + this.c.b + "\", \"" + "accuracy" + "\":\"" + this.c.c + "\", \"" + "country" + "\":\"" + this.c.d + "\", \"" + "country_code" + "\":\"" + this.c.e + "\", \"" + "region" + "\":\"" + this.c.f + "\", \"" + "city" + "\":\"" + this.c.g + "\", \"" + "street" + "\":\"" + this.c.h + "\"}";
    }

    private final void xa(long j) {
        this.f147a = j;
    }

    private final void xa(b bVar) {
        this.c = bVar;
    }

    private final void xd(int i) {
        this.b = i;
    }

    private final b xe() {
        return this.c;
    }

    private final String xtoString() {
        return "GeocodeCdmaCellLocation: " + a();
    }

    public final String a() {
        return xa();
    }

    public final void a(long j) {
        xa(j);
    }

    public final void a(b bVar) {
        xa(bVar);
    }

    public final void d(int i) {
        xd(i);
    }

    public final b e() {
        return xe();
    }

    public final String toString() {
        return xtoString();
    }
}
