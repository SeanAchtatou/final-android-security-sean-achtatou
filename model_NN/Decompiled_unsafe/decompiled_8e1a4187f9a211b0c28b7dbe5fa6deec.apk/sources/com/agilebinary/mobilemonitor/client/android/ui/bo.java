package com.agilebinary.mobilemonitor.client.android.ui;

import android.app.Dialog;
import android.view.View;

final class bo implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ Dialog f253a;
    private /* synthetic */ MapActivity_OSM b;

    bo(MapActivity_OSM mapActivity_OSM, Dialog dialog) {
        this.b = mapActivity_OSM;
        this.f253a = dialog;
    }

    private final void xonClick(View view) {
        this.b.e.a(!this.b.e.a());
        this.b.b.invalidate();
        this.b.l.b("MAP_LAYER_SCALEBAR__BOOL", this.b.f.a());
        this.f253a.dismiss();
    }

    public final void onClick(View view) {
        xonClick(view);
    }
}
