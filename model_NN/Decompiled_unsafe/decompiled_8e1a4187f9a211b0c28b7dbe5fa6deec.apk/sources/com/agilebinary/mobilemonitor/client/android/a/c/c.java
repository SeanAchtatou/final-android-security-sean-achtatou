package com.agilebinary.mobilemonitor.client.android.a.c;

import com.agilebinary.mobilemonitor.client.android.a.a;
import com.agilebinary.mobilemonitor.client.android.a.d;
import com.agilebinary.mobilemonitor.client.android.a.j;
import com.agilebinary.mobilemonitor.client.android.a.k;
import com.agilebinary.mobilemonitor.client.android.a.m;
import com.agilebinary.mobilemonitor.client.android.a.p;

public final class c implements m {
    private final d xa() {
        return new d();
    }

    private final j xb() {
        return new b();
    }

    private final com.agilebinary.mobilemonitor.client.android.a.c xc() {
        return new a();
    }

    private final p xd() {
        return new f();
    }

    private final k xe() {
        return new h(this);
    }

    private final a xf() {
        return new g(this);
    }

    public final d a() {
        return xa();
    }

    public final j b() {
        return xb();
    }

    public final com.agilebinary.mobilemonitor.client.android.a.c c() {
        return xc();
    }

    public final p d() {
        return xd();
    }

    public final k e() {
        return xe();
    }

    public final a f() {
        return xf();
    }
}
