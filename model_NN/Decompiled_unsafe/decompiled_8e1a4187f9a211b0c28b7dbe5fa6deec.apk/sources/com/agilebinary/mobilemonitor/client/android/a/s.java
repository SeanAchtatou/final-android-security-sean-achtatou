package com.agilebinary.mobilemonitor.client.android.a;

import com.agilebinary.a.a.a.c;
import com.agilebinary.a.a.a.j;
import com.agilebinary.a.a.a.t;
import com.agilebinary.mobilemonitor.client.android.c.b;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

public final class s implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private static final String f163a = b.a();
    private j b;
    private c c;
    private int d;
    private boolean e;
    private InputStream f;
    private int g;

    public s(j jVar) {
        this.b = jVar;
        this.d = jVar.a().b();
        this.c = jVar.b();
        try {
            if (this.c.g()) {
                this.f = this.c.f();
            }
        } catch (IllegalStateException e2) {
            e2.printStackTrace();
        } catch (IOException e3) {
            e3.printStackTrace();
        }
        String a2 = a("SI-S");
        this.g = -2;
        if (a2 != null) {
            try {
                this.g = Integer.parseInt(a2);
            } catch (NumberFormatException e4) {
                "failed to parse header-value of " + a2;
            }
        }
    }

    private String a(String str) {
        return xa(str);
    }

    public static boolean a(int i) {
        return xa(i);
    }

    public static boolean b(int i) {
        return xb(i);
    }

    public static boolean c(int i) {
        return xc(i);
    }

    private String xa(String str) {
        t[] b2 = this.b.b(str);
        if (b2.length == 0) {
            return null;
        }
        return b2[0].b();
    }

    private final boolean xa() {
        if (this.e) {
            return false;
        }
        int i = this.g;
        return this.d == 200 && (i == 0 || i == -2 || a("SI-S-T") != null);
    }

    private static boolean xa(int i) {
        return i == 8 || i == 9 || i == 5 || i == 7 || i == 11;
    }

    private final int xb() {
        return this.g;
    }

    private static boolean xb(int i) {
        return i == -3;
    }

    private final boolean xc() {
        return a(this.g);
    }

    private static boolean xc(int i) {
        return i == 304;
    }

    private final InputStream xd() {
        return this.f;
    }

    private final void xe() {
        if (this.f != null) {
            try {
                this.f.close();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
    }

    private final boolean xf() {
        return b(this.g);
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x006e A[Catch:{ all -> 0x0073 }, LOOP:0: B:26:0x0067->B:28:0x006e, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0078 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final java.lang.String xg() {
        /*
            r6 = this;
            r5 = 0
            r4 = 0
            java.io.InputStream r0 = r6.f
            if (r0 != 0) goto L_0x0008
            r0 = r5
        L_0x0007:
            return r0
        L_0x0008:
            com.agilebinary.a.a.a.c r0 = r6.c
            long r0 = r0.c()
            r2 = 2147483647(0x7fffffff, double:1.060997895E-314)
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 <= 0) goto L_0x001d
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "HTTP entity too large to be buffered in memory"
            r0.<init>(r1)
            throw r0
        L_0x001d:
            com.agilebinary.a.a.a.c r0 = r6.c
            long r0 = r0.c()
            int r0 = (int) r0
            if (r0 >= 0) goto L_0x0028
            r0 = 4096(0x1000, float:5.74E-42)
        L_0x0028:
            com.agilebinary.a.a.a.c r1 = r6.c
            if (r1 != 0) goto L_0x0034
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "HTTP entity may not be null"
            r0.<init>(r1)
            throw r0
        L_0x0034:
            com.agilebinary.a.a.a.t r2 = r1.d()
            if (r2 == 0) goto L_0x0080
            com.agilebinary.a.a.a.t r1 = r1.d()
            com.agilebinary.a.a.a.m[] r1 = r1.c()
            int r2 = r1.length
            if (r2 <= 0) goto L_0x0080
            r1 = r1[r4]
            java.lang.String r2 = "charset"
            com.agilebinary.a.a.a.o r1 = r1.a(r2)
            if (r1 == 0) goto L_0x0080
            java.lang.String r1 = r1.b()
        L_0x0053:
            if (r1 != 0) goto L_0x0057
            java.lang.String r1 = "ISO-8859-1"
        L_0x0057:
            java.io.InputStreamReader r2 = new java.io.InputStreamReader
            java.io.InputStream r3 = r6.f
            r2.<init>(r3, r1)
            com.agilebinary.a.a.a.i.c r1 = new com.agilebinary.a.a.a.i.c
            r1.<init>(r0)
            r0 = 1024(0x400, float:1.435E-42)
            char[] r0 = new char[r0]     // Catch:{ all -> 0x0073 }
        L_0x0067:
            int r3 = r2.read(r0)     // Catch:{ all -> 0x0073 }
            r4 = -1
            if (r3 == r4) goto L_0x0078
            r4 = 0
            r1.a(r0, r4, r3)     // Catch:{ all -> 0x0073 }
            goto L_0x0067
        L_0x0073:
            r0 = move-exception
            r2.close()
            throw r0
        L_0x0078:
            r2.close()
            java.lang.String r0 = r1.toString()
            goto L_0x0007
        L_0x0080:
            r1 = r5
            goto L_0x0053
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.a.s.xg():java.lang.String");
    }

    public final boolean a() {
        return xa();
    }

    public final int b() {
        return xb();
    }

    public final boolean c() {
        return xc();
    }

    public final InputStream d() {
        return xd();
    }

    public final void e() {
        xe();
    }

    public final boolean f() {
        return xf();
    }

    public final String g() {
        return xg();
    }
}
