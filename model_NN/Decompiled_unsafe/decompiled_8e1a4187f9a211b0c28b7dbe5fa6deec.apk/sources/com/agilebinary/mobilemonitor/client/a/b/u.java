package com.agilebinary.mobilemonitor.client.a.b;

import com.agilebinary.mobilemonitor.client.a.a.c;
import com.agilebinary.mobilemonitor.client.a.e;

public final class u extends i {

    /* renamed from: a  reason: collision with root package name */
    private long f139a;
    private long b;
    private long c;
    private byte d;
    private String e;
    private String f;

    public u(String str, long j, long j2, c cVar, e eVar) {
        super(str, j, j2, cVar, eVar);
        this.f139a = eVar.a(cVar.d());
        this.b = eVar.a(cVar.d());
        this.c = eVar.a(cVar.d());
        this.d = cVar.b();
        this.e = cVar.g();
        this.f = cVar.g();
    }

    private final long xa() {
        return this.f139a;
    }

    private final long xb() {
        return this.b;
    }

    private final long xc() {
        return this.c;
    }

    private final byte xd() {
        return this.d;
    }

    private final String xe() {
        return this.e;
    }

    private final String xf() {
        return this.f;
    }

    private final byte xk() {
        return 1;
    }

    public final long a() {
        return xa();
    }

    public final long b() {
        return xb();
    }

    public final long c() {
        return xc();
    }

    public final byte d() {
        return xd();
    }

    public final String e() {
        return xe();
    }

    public final String f() {
        return xf();
    }

    public final byte k() {
        return xk();
    }
}
