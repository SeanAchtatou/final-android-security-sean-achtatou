package com.agilebinary.mobilemonitor.client.android.ui;

import android.view.View;
import com.biige.client.android.R;

final class x implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ ChangePasswordActivity f297a;

    x(ChangePasswordActivity changePasswordActivity) {
        this.f297a = changePasswordActivity;
    }

    private final void xonClick(View view) {
        String obj = this.f297a.c.getText().toString();
        if (!obj.equals(this.f297a.d.getText().toString())) {
            this.f297a.c.setText("");
            this.f297a.d.setText("");
            this.f297a.b((int) R.string.error_password_match);
        } else if (obj.length() < 8) {
            this.f297a.c.setText("");
            this.f297a.d.setText("");
            this.f297a.b((int) R.string.error_password_length);
        } else {
            ChangePasswordActivity.xa(this.f297a, obj);
        }
    }

    public final void onClick(View view) {
        xonClick(view);
    }
}
