package com.agilebinary.mobilemonitor.client.a.b;

import com.agilebinary.mobilemonitor.client.a.a.c;
import java.io.Serializable;

public abstract class o implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private long f135a;
    private long b;
    private String c;
    private String d;
    private String e;

    protected o(String str, long j, long j2, c cVar) {
        this.c = str;
        this.f135a = j;
        this.b = j2;
        this.d = cVar.g();
        this.e = cVar.g();
    }

    private boolean xequals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        return this.f135a == ((o) obj).f135a;
    }

    private int xhashCode() {
        return ((int) (this.f135a ^ (this.f135a >>> 32))) + 31;
    }

    private final String xr() {
        return this.d;
    }

    private final String xs() {
        return this.e;
    }

    private final long xt() {
        return this.b;
    }

    private final long xu() {
        return this.f135a;
    }

    public boolean equals(Object obj) {
        return xequals(obj);
    }

    public int hashCode() {
        return xhashCode();
    }

    public abstract byte k();

    public final String r() {
        return xr();
    }

    public final String s() {
        return xs();
    }

    public final long t() {
        return xt();
    }

    public final long u() {
        return xu();
    }
}
