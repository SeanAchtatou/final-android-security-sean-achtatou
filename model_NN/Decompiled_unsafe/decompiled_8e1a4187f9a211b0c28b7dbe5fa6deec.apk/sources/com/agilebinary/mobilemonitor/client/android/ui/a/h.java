package com.agilebinary.mobilemonitor.client.android.ui.a;

public final class h {

    /* renamed from: a  reason: collision with root package name */
    private long f215a;
    private long b;
    private boolean c;

    public h(long j, long j2, boolean z) {
        this.c = z;
        this.f215a = j;
        this.b = j2;
    }

    private final long xa() {
        return this.f215a;
    }

    private final long xb() {
        return this.b;
    }

    public final long a() {
        return xa();
    }

    public final long b() {
        return xb();
    }
}
