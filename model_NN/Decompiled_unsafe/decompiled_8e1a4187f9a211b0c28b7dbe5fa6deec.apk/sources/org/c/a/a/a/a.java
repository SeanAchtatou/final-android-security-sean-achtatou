package org.c.a.a.a;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class a {
    private static MessageDigest a(String str) {
        return xa(str);
    }

    public static byte[] a(byte[] bArr) {
        return xa(bArr);
    }

    private static MessageDigest xa(String str) {
        try {
            return MessageDigest.getInstance(str);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    private static byte[] xa(byte[] bArr) {
        return a("SHA-256").digest(bArr);
    }
}
