package org.a.b;

import android.util.Log;
import org.a.a.a;
import org.a.a.h;

public final class c extends a {
    c(String str) {
        this.f315a = str;
    }

    private final void xa(String str) {
        Log.d(this.f315a, str);
    }

    private final void xa(String str, Object obj, Object obj2) {
        Log.e(this.f315a, h.a(str, new Object[]{obj, obj2}).a());
    }

    private final void xa(String str, Throwable th) {
        Log.i(this.f315a, str, th);
    }

    private final void xb(String str) {
        Log.i(this.f315a, str);
    }

    private final void xb(String str, Throwable th) {
        Log.w(this.f315a, str, th);
    }

    private final void xc(String str) {
        Log.w(this.f315a, str);
    }

    private final void xc(String str, Throwable th) {
        Log.e(this.f315a, str, th);
    }

    private final void xd(String str) {
        Log.e(this.f315a, str);
    }

    public final void a(String str) {
        xa(str);
    }

    public final void a(String str, Object obj, Object obj2) {
        xa(str, obj, obj2);
    }

    public final void a(String str, Throwable th) {
        xa(str, th);
    }

    public final void b(String str) {
        xb(str);
    }

    public final void b(String str, Throwable th) {
        xb(str, th);
    }

    public final void c(String str) {
        xc(str);
    }

    public final void c(String str, Throwable th) {
        xc(str, th);
    }

    public final void d(String str) {
        xd(str);
    }
}
