package org.a.b;

import android.util.Log;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import org.a.b;

public class a implements b {

    /* renamed from: a  reason: collision with root package name */
    private final Map f316a = new HashMap();

    /* access modifiers changed from: private */
    /* renamed from: b */
    public c xa(String str) {
        return xb(str);
    }

    private c xb(String str) {
        String str2;
        c cVar;
        String str3;
        if (str == null || str.length() <= 23) {
            str2 = str;
        } else {
            StringTokenizer stringTokenizer = new StringTokenizer(str, ".");
            if (stringTokenizer.hasMoreTokens()) {
                StringBuilder sb = new StringBuilder();
                do {
                    String nextToken = stringTokenizer.nextToken();
                    if (nextToken.length() == 1) {
                        sb.append(nextToken);
                        sb.append('.');
                    } else if (stringTokenizer.hasMoreTokens()) {
                        sb.append(nextToken.charAt(0));
                        sb.append("*.");
                    } else {
                        sb.append(nextToken);
                    }
                } while (stringTokenizer.hasMoreTokens());
                str3 = sb.toString();
            } else {
                str3 = str;
            }
            str2 = str3.length() > 23 ? str3.substring(0, 22) + '*' : str3;
        }
        synchronized (this) {
            cVar = (c) this.f316a.get(str2);
            if (cVar == null) {
                if (!str2.equals(str)) {
                    Log.i(a.class.getSimpleName(), "Logger name '" + str + "' exceeds maximum length of " + 23 + " characters, using '" + str2 + "' instead.");
                }
                cVar = new c(str2);
                this.f316a.put(str2, cVar);
            }
        }
        return cVar;
    }
}
