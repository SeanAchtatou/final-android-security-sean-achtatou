package org.a;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import org.a.a.c;
import org.a.a.d;
import org.a.a.f;
import org.a.b.b;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static int f312a = 0;
    private static c b = new c();
    private static d c = new d();
    private static final String[] d = {"1.6"};
    private static String e = "org/slf4j/impl/StaticLoggerBinder.class";

    private a() {
    }

    public static c a(Class cls) {
        return xa(cls);
    }

    private static final void a() {
        xa();
    }

    private static void a(Throwable th) {
        xa(th);
    }

    private static final void b() {
        xb();
    }

    private static c xa(Class cls) {
        b bVar;
        String name = cls.getName();
        if (f312a == 0) {
            f312a = 1;
            try {
                ClassLoader classLoader = a.class.getClassLoader();
                Enumeration<URL> systemResources = classLoader == null ? ClassLoader.getSystemResources(e) : classLoader.getResources(e);
                ArrayList arrayList = new ArrayList();
                while (systemResources.hasMoreElements()) {
                    arrayList.add(systemResources.nextElement());
                }
                if (arrayList.size() > 1) {
                    f.a("Class path contains multiple SLF4J bindings.");
                    for (int i = 0; i < arrayList.size(); i++) {
                        f.a("Found binding in [" + arrayList.get(i) + "]");
                    }
                    f.a("See http://www.slf4j.org/codes.html#multiple_bindings for an explanation.");
                }
            } catch (IOException e2) {
                f.a("Error getting resources from path", e2);
            }
            try {
                f312a = 3;
                a();
            } catch (NoClassDefFoundError e3) {
                String message = e3.getMessage();
                if (message == null || message.indexOf("org/slf4j/impl/StaticLoggerBinder") == -1) {
                    a(e3);
                    throw e3;
                }
                f312a = 4;
                f.a("Failed to load class \"org.slf4j.impl.StaticLoggerBinder\".");
                f.a("Defaulting to no-operation (NOP) logger implementation");
                f.a("See http://www.slf4j.org/codes.html#StaticLoggerBinder for further details.");
            } catch (NoSuchMethodError e4) {
                String message2 = e4.getMessage();
                if (!(message2 == null || message2.indexOf("org.slf4j.impl.StaticLoggerBinder.getSingleton()") == -1)) {
                    f312a = 2;
                    f.a("slf4j-api 1.6.x (or later) is incompatible with this binding.");
                    f.a("Your binding is version 1.5.5 or earlier.");
                    f.a("Upgrade your binding to version 1.6.x. or 2.0.x");
                }
                throw e4;
            } catch (Exception e5) {
                a(e5);
                throw new IllegalStateException("Unexpected initialization failure", e5);
            }
            if (f312a == 3) {
                b();
            }
        }
        switch (f312a) {
            case 1:
                bVar = b;
                break;
            case 2:
                throw new IllegalStateException("org.slf4j.LoggerFactory could not be successfully initialized. See also http://www.slf4j.org/codes.html#unsuccessfulInit");
            case 3:
                bVar = b.a().b();
                break;
            case 4:
                bVar = c;
                break;
            default:
                throw new IllegalStateException("Unreachable code");
        }
        return bVar.a(name);
    }

    private static final void xa() {
        List a2 = b.a();
        if (a2.size() != 0) {
            f.a("The following loggers will not work becasue they were created");
            f.a("during the default configuration phase of the underlying logging system.");
            f.a("See also http://www.slf4j.org/codes.html#substituteLogger");
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < a2.size()) {
                    f.a((String) a2.get(i2));
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    private static void xa(Throwable th) {
        f312a = 2;
        f.a("Failed to instantiate SLF4J LoggerFactory", th);
    }

    private static final void xb() {
        try {
            String str = b.f317a;
            boolean z = false;
            for (String startsWith : d) {
                if (str.startsWith(startsWith)) {
                    z = true;
                }
            }
            if (!z) {
                f.a("The requested version " + str + " by your slf4j binding is not compatible with " + Arrays.asList(d).toString());
                f.a("See http://www.slf4j.org/codes.html#version_mismatch for further details.");
            }
        } catch (NoSuchFieldError e2) {
        } catch (Throwable th) {
            f.a("Unexpected problem occured during version sanity check", th);
        }
    }
}
