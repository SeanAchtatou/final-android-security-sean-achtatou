package org.a.a;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private static b f313a = new b(null);
    private String b;
    private Throwable c;
    private Object[] d;

    public b(String str) {
        this(str, null, null);
    }

    public b(String str, Object[] objArr, Throwable th) {
        this.b = str;
        this.c = th;
        if (th == null) {
            this.d = objArr;
        } else if (objArr == null || objArr.length == 0) {
            throw new IllegalStateException("non-sensical empty or null argument array");
        } else {
            int length = objArr.length - 1;
            Object[] objArr2 = new Object[length];
            System.arraycopy(objArr, 0, objArr2, 0, length);
            this.d = objArr2;
        }
    }

    private final String xa() {
        return this.b;
    }

    public final String a() {
        return xa();
    }
}
