package org.a.a;

import java.util.ArrayList;
import java.util.List;
import org.a.b;

public final class c implements b {

    /* renamed from: a  reason: collision with root package name */
    private List f314a = new ArrayList();

    private final List xa() {
        ArrayList arrayList = new ArrayList();
        synchronized (this.f314a) {
            arrayList.addAll(this.f314a);
        }
        return arrayList;
    }

    private final org.a.c xa(String str) {
        synchronized (this.f314a) {
            this.f314a.add(str);
        }
        return g.b;
    }

    public final List a() {
        return xa();
    }

    public final org.a.c a(String str) {
        return xa(str);
    }
}
