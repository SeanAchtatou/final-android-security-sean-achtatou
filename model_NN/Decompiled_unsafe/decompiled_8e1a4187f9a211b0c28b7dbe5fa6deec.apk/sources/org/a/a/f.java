package org.a.a;

public final class f {
    public static final void a(String str) {
        xa(str);
    }

    public static final void a(String str, Throwable th) {
        xa(str, th);
    }

    private static final void xa(String str) {
        System.err.println("SLF4J: " + str);
    }

    private static final void xa(String str, Throwable th) {
        System.err.println(str);
        System.err.println("Reported exception:");
        th.printStackTrace();
    }
}
