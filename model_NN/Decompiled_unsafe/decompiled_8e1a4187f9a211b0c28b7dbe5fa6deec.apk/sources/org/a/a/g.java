package org.a.a;

public final class g extends a {
    public static final g b = new g();

    protected g() {
    }

    private final String xa() {
        return "NOP";
    }

    private final void xa(String str) {
    }

    private final void xa(String str, Object obj, Object obj2) {
    }

    private final void xa(String str, Throwable th) {
    }

    private final void xb(String str) {
    }

    private final void xb(String str, Throwable th) {
    }

    private final void xc(String str) {
    }

    private final void xc(String str, Throwable th) {
    }

    private final void xd(String str) {
    }

    public final String a() {
        return xa();
    }

    public final void a(String str) {
        xa(str);
    }

    public final void a(String str, Object obj, Object obj2) {
        xa(str, obj, obj2);
    }

    public final void a(String str, Throwable th) {
        xa(str, th);
    }

    public final void b(String str) {
        xb(str);
    }

    public final void b(String str, Throwable th) {
        xb(str, th);
    }

    public final void c(String str) {
        xc(str);
    }

    public final void c(String str, Throwable th) {
        xc(str, th);
    }

    public final void d(String str) {
        xd(str);
    }
}
