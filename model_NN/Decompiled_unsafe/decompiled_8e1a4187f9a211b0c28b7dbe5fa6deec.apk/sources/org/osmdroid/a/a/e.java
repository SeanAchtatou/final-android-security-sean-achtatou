package org.osmdroid.a.a;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import java.io.File;
import java.io.InputStream;
import java.util.Random;
import org.a.c;
import org.osmdroid.a.d.a;
import org.osmdroid.a.f;

public abstract class e implements f, a {
    private static final c f = org.a.a.a(e.class);
    private static int g = 0;
    protected final String c;
    protected final String d;
    protected final Random e = new Random();
    private final int h;
    private final int i;
    private final int j;
    private final int k;
    private final org.osmdroid.a l;

    public e(String str, org.osmdroid.a aVar, int i2, int i3, int i4, String str2) {
        this.l = aVar;
        int i5 = g;
        g = i5 + 1;
        this.j = i5;
        this.c = str;
        this.h = i2;
        this.i = i3;
        this.k = i4;
        this.d = str2;
    }

    private final Drawable xa(InputStream inputStream) {
        try {
            Bitmap decodeStream = BitmapFactory.decodeStream(inputStream);
            if (decodeStream != null) {
                return new BitmapDrawable(decodeStream);
            }
        } catch (OutOfMemoryError e2) {
            f.d("OutOfMemoryError loading bitmap");
            System.gc();
        }
        return null;
    }

    private String xa() {
        return this.c;
    }

    private final Drawable xb(String str) {
        try {
            Bitmap decodeFile = BitmapFactory.decodeFile(str);
            if (decodeFile != null) {
                return new BitmapDrawable(decodeFile);
            }
            try {
                new File(str).delete();
            } catch (Throwable th) {
                f.c("Error deleting invalid file: " + str, th);
            }
            return null;
        } catch (OutOfMemoryError e2) {
            f.d("OutOfMemoryError loading bitmap: " + str);
            System.gc();
        }
    }

    private final String xb(f fVar) {
        return a() + '/' + fVar.a() + '/' + fVar.b() + '/' + fVar.c() + this.d;
    }

    private final String xc() {
        return this.c;
    }

    private final int xd() {
        return this.h;
    }

    private final int xe() {
        return this.i;
    }

    private final int xf() {
        return this.k;
    }

    public final Drawable a(InputStream inputStream) {
        return xa(inputStream);
    }

    public String a() {
        return xa();
    }

    public final Drawable b(String str) {
        return xb(str);
    }

    public final String b(f fVar) {
        return xb(fVar);
    }

    public final String c() {
        return xc();
    }

    public final int d() {
        return xd();
    }

    public final int e() {
        return xe();
    }

    public final int f() {
        return xf();
    }
}
