package org.osmdroid.a.a;

import org.osmdroid.a;
import org.osmdroid.a.f;

public abstract class d extends e {
    private final String[] f;

    public d(String str, a aVar, int i, int i2, int i3, String str2, String... strArr) {
        super(str, aVar, i, i2, i3, str2);
        this.f = strArr;
    }

    private final String xb() {
        return this.f[this.e.nextInt(this.f.length)];
    }

    public abstract String a(f fVar);

    /* access modifiers changed from: protected */
    public final String b() {
        return xb();
    }
}
