package org.osmdroid.a.c;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import org.osmdroid.a.a;

public final class b implements a {

    /* renamed from: a  reason: collision with root package name */
    private final Context f338a;

    public b(Context context) {
        this.f338a = context;
    }

    private final Intent xa(BroadcastReceiver broadcastReceiver, IntentFilter intentFilter) {
        return this.f338a.registerReceiver(broadcastReceiver, intentFilter);
    }

    private final void xa(BroadcastReceiver broadcastReceiver) {
        this.f338a.unregisterReceiver(broadcastReceiver);
    }

    public final Intent a(BroadcastReceiver broadcastReceiver, IntentFilter intentFilter) {
        return xa(broadcastReceiver, intentFilter);
    }

    public final void a(BroadcastReceiver broadcastReceiver) {
        xa(broadcastReceiver);
    }
}
