package org.osmdroid.a.c;

import android.content.SharedPreferences;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import org.a.c;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

public class a implements org.osmdroid.a.d.a {
    private static final c c = org.a.a.a(a.class);
    private static String d = "android_id";
    private static String e = "";
    private static String f = "";
    private static SharedPreferences.Editor g;

    public static String a() {
        return xa();
    }

    public static String b() {
        return xb();
    }

    private static String xa() {
        return e;
    }

    private static String xb() {
        if (f.length() == 0) {
            synchronized (f) {
                if (f.length() == 0) {
                    try {
                        HttpResponse execute = new DefaultHttpClient().execute(new HttpPost("http://auth.cloudmade.com/token/" + e + "?userid=" + d));
                        if (execute.getStatusLine().getStatusCode() == 200) {
                            String trim = new BufferedReader(new InputStreamReader(execute.getEntity().getContent()), 8192).readLine().trim();
                            f = trim;
                            if (trim.length() > 0) {
                                g.putString("CLOUDMADE_TOKEN", f);
                                g.commit();
                                g = null;
                            } else {
                                c.d("No authorization token received from Cloudmade");
                            }
                        }
                    } catch (IOException e2) {
                        c.d("No authorization token received from Cloudmade: " + e2);
                    }
                }
            }
        }
        return f;
    }
}
