package org.osmdroid.a;

public final class f {

    /* renamed from: a  reason: collision with root package name */
    private final int f343a;
    private final int b;
    private final int c;

    public f(int i, int i2, int i3) {
        this.c = i;
        this.f343a = i2;
        this.b = i3;
    }

    private final int xa() {
        return this.c;
    }

    private final int xb() {
        return this.f343a;
    }

    private final int xc() {
        return this.b;
    }

    private final boolean xequals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof f)) {
            return false;
        }
        f fVar = (f) obj;
        return this.c == fVar.c && this.f343a == fVar.f343a && this.b == fVar.b;
    }

    private final int xhashCode() {
        return (this.c + 37) * 17 * (this.f343a + 37) * (this.b + 37);
    }

    private final String xtoString() {
        return "/" + this.c + "/" + this.f343a + "/" + this.b;
    }

    public final int a() {
        return xa();
    }

    public final int b() {
        return xb();
    }

    public final int c() {
        return xc();
    }

    public final boolean equals(Object obj) {
        return xequals(obj);
    }

    public final int hashCode() {
        return xhashCode();
    }

    public final String toString() {
        return xtoString();
    }
}
