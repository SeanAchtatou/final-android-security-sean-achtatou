package org.osmdroid.a;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import java.util.LinkedHashMap;
import java.util.Map;
import org.a.a;
import org.a.c;

public class b extends LinkedHashMap {

    /* renamed from: a  reason: collision with root package name */
    private static final c f323a = a.a(b.class);
    private int b = 9;

    public b() {
        super(11, 0.1f, true);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public Drawable xremove(Object obj) {
        return xa(obj);
    }

    private Drawable xa(Object obj) {
        Bitmap bitmap;
        Drawable drawable = (Drawable) super.remove(obj);
        if ((drawable instanceof BitmapDrawable) && (bitmap = ((BitmapDrawable) drawable).getBitmap()) != null) {
            bitmap.recycle();
        }
        return drawable;
    }

    private final void xa(int i) {
        if (i > this.b) {
            f323a.b("Tile cache increased from " + this.b + " to " + i);
            this.b = i;
        }
    }

    private void xclear() {
        while (size() > 0) {
            xremove(keySet().iterator().next());
        }
        super.clear();
    }

    private boolean xremoveEldestEntry(Map.Entry entry) {
        if (size() <= this.b) {
            return false;
        }
        xremove(entry.getKey());
        return false;
    }

    public final void a(int i) {
        xa(i);
    }

    public void clear() {
        xclear();
    }

    /* access modifiers changed from: protected */
    public boolean removeEldestEntry(Map.Entry entry) {
        return xremoveEldestEntry(entry);
    }
}
