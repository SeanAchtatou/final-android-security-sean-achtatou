package org.osmdroid.a.b;

import java.io.File;
import java.util.Comparator;

final class a implements Comparator {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ k f324a;

    a(k kVar) {
        this.f324a = kVar;
    }

    /* access modifiers changed from: private */
    /* renamed from: xcompare */
    public final /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
        return Long.valueOf(((File) obj).lastModified()).compareTo(Long.valueOf(((File) obj2).lastModified()));
    }
}
