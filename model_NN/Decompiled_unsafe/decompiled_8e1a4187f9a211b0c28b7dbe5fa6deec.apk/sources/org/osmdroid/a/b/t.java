package org.osmdroid.a.b;

import org.a.a;
import org.a.c;
import org.osmdroid.a.a.b;
import org.osmdroid.a.a.f;

public class t extends h {
    private static final c c = a.a(t.class);
    /* access modifiers changed from: private */
    public final long e;
    /* access modifiers changed from: private */
    public f f;

    public t(org.osmdroid.a.a aVar) {
        this(aVar, b.f322a, (byte) 0);
    }

    private t(org.osmdroid.a.a aVar, f fVar) {
        super(aVar);
        this.f = fVar;
        this.e = 604800000;
    }

    private t(org.osmdroid.a.a aVar, f fVar, byte b) {
        this(aVar, fVar);
    }

    private final boolean xe() {
        return false;
    }

    private final String xf() {
        return "filesystem";
    }

    private final Runnable xg() {
        return new u(this);
    }

    private final int xh() {
        if (this.f != null) {
            return this.f.d();
        }
        return 23;
    }

    private final int xi() {
        if (this.f != null) {
            return this.f.e();
        }
        return 0;
    }

    public final boolean e() {
        return xe();
    }

    /* access modifiers changed from: protected */
    public final String f() {
        return xf();
    }

    /* access modifiers changed from: protected */
    public final Runnable g() {
        return xg();
    }

    public final int h() {
        return xh();
    }

    public final int i() {
        return xi();
    }
}
