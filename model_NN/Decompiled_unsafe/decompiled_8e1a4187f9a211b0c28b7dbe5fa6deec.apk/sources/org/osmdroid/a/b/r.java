package org.osmdroid.a.b;

import org.a.a;
import org.a.c;
import org.osmdroid.a.a.d;
import org.osmdroid.a.a.f;

public class r extends v {
    /* access modifiers changed from: private */
    public static final c c = a.a(r.class);
    /* access modifiers changed from: private */
    public final c e;
    /* access modifiers changed from: private */
    public d f;
    /* access modifiers changed from: private */
    public final l g;

    public r(f fVar, c cVar, l lVar) {
        super(2);
        this.e = cVar;
        this.g = lVar;
        if (fVar instanceof d) {
            this.f = (d) fVar;
        } else {
            this.f = null;
        }
    }

    private final boolean xe() {
        return true;
    }

    private final String xf() {
        return "downloader";
    }

    private final Runnable xg() {
        return new d(this);
    }

    private final int xh() {
        if (this.f != null) {
            return this.f.d();
        }
        return 23;
    }

    private final int xi() {
        if (this.f != null) {
            return this.f.e();
        }
        return 0;
    }

    public final boolean e() {
        return xe();
    }

    /* access modifiers changed from: protected */
    public final String f() {
        return xf();
    }

    /* access modifiers changed from: protected */
    public final Runnable g() {
        return xg();
    }

    public final int h() {
        return xh();
    }

    public final int i() {
        return xi();
    }
}
