package org.osmdroid.a.b;

import android.database.sqlite.SQLiteException;
import java.io.File;
import java.io.IOException;
import org.a.a;
import org.a.c;

public class s {

    /* renamed from: a  reason: collision with root package name */
    private static final c f335a = a.a(s.class);

    public static m a(File file) {
        return xa(file);
    }

    private static m xa(File file) {
        if (file.getName().endsWith(".zip")) {
            try {
                return g.a(file);
            } catch (IOException e) {
                f335a.c("Error opening ZIP file", e);
            }
        }
        if (file.getName().endsWith(".sqlite")) {
            try {
                return e.a(file);
            } catch (SQLiteException e2) {
                f335a.c("Error opening SQL file", e2);
            }
        }
        if (file.getName().endsWith(".gemf")) {
            try {
                return f.a(file);
            } catch (IOException e3) {
                f335a.c("Error opening GEMF file", e3);
            }
        }
        return null;
    }
}
