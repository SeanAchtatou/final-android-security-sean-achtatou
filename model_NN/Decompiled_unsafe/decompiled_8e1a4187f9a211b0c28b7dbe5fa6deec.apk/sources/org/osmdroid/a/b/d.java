package org.osmdroid.a.b;

import android.graphics.drawable.Drawable;

final class d extends i {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ r f326a;

    /* synthetic */ d(r rVar) {
        this(rVar, (byte) 0);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    private d(r rVar, byte b) {
        super(rVar);
        this.f326a = rVar;
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:42:0x0125=Splitter:B:42:0x0125, B:31:0x00c7=Splitter:B:31:0x00c7, B:37:0x00f7=Splitter:B:37:0x00f7, B:22:0x008e=Splitter:B:22:0x008e} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final android.graphics.drawable.Drawable xa(org.osmdroid.a.d r10) {
        /*
            r9 = this;
            r7 = 0
            org.osmdroid.a.b.r r0 = r9.f326a
            org.osmdroid.a.a.d r0 = r0.f
            if (r0 != 0) goto L_0x000b
            r0 = r7
        L_0x000a:
            return r0
        L_0x000b:
            org.osmdroid.a.f r0 = r10.a()
            org.osmdroid.a.b.r r1 = r9.f326a     // Catch:{ UnknownHostException -> 0x008b, FileNotFoundException -> 0x00c4, IOException -> 0x00f4, Throwable -> 0x0122, all -> 0x0146 }
            org.osmdroid.a.b.l r1 = r1.g     // Catch:{ UnknownHostException -> 0x008b, FileNotFoundException -> 0x00c4, IOException -> 0x00f4, Throwable -> 0x0122, all -> 0x0146 }
            if (r1 == 0) goto L_0x002b
            org.osmdroid.a.b.r r1 = r9.f326a     // Catch:{ UnknownHostException -> 0x008b, FileNotFoundException -> 0x00c4, IOException -> 0x00f4, Throwable -> 0x0122, all -> 0x0146 }
            org.osmdroid.a.b.l r1 = r1.g     // Catch:{ UnknownHostException -> 0x008b, FileNotFoundException -> 0x00c4, IOException -> 0x00f4, Throwable -> 0x0122, all -> 0x0146 }
            boolean r1 = r1.a()     // Catch:{ UnknownHostException -> 0x008b, FileNotFoundException -> 0x00c4, IOException -> 0x00f4, Throwable -> 0x0122, all -> 0x0146 }
            if (r1 != 0) goto L_0x002b
            org.osmdroid.a.c.c.a(r7)
            org.osmdroid.a.c.c.a(r7)
            r0 = r7
            goto L_0x000a
        L_0x002b:
            org.osmdroid.a.b.r r1 = r9.f326a     // Catch:{ UnknownHostException -> 0x008b, FileNotFoundException -> 0x00c4, IOException -> 0x00f4, Throwable -> 0x0122, all -> 0x0146 }
            org.osmdroid.a.a.d r1 = r1.f     // Catch:{ UnknownHostException -> 0x008b, FileNotFoundException -> 0x00c4, IOException -> 0x00f4, Throwable -> 0x0122, all -> 0x0146 }
            java.lang.String r1 = r1.a(r0)     // Catch:{ UnknownHostException -> 0x008b, FileNotFoundException -> 0x00c4, IOException -> 0x00f4, Throwable -> 0x0122, all -> 0x0146 }
            java.io.BufferedInputStream r2 = new java.io.BufferedInputStream     // Catch:{ UnknownHostException -> 0x008b, FileNotFoundException -> 0x00c4, IOException -> 0x00f4, Throwable -> 0x0122, all -> 0x0146 }
            java.net.URL r3 = new java.net.URL     // Catch:{ UnknownHostException -> 0x008b, FileNotFoundException -> 0x00c4, IOException -> 0x00f4, Throwable -> 0x0122, all -> 0x0146 }
            r3.<init>(r1)     // Catch:{ UnknownHostException -> 0x008b, FileNotFoundException -> 0x00c4, IOException -> 0x00f4, Throwable -> 0x0122, all -> 0x0146 }
            java.io.InputStream r1 = r3.openStream()     // Catch:{ UnknownHostException -> 0x008b, FileNotFoundException -> 0x00c4, IOException -> 0x00f4, Throwable -> 0x0122, all -> 0x0146 }
            r3 = 8192(0x2000, float:1.14794E-41)
            r2.<init>(r1, r3)     // Catch:{ UnknownHostException -> 0x008b, FileNotFoundException -> 0x00c4, IOException -> 0x00f4, Throwable -> 0x0122, all -> 0x0146 }
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ UnknownHostException -> 0x0170, FileNotFoundException -> 0x0165, IOException -> 0x015c, Throwable -> 0x0153, all -> 0x014b }
            r1.<init>()     // Catch:{ UnknownHostException -> 0x0170, FileNotFoundException -> 0x0165, IOException -> 0x015c, Throwable -> 0x0153, all -> 0x014b }
            java.io.BufferedOutputStream r3 = new java.io.BufferedOutputStream     // Catch:{ UnknownHostException -> 0x0170, FileNotFoundException -> 0x0165, IOException -> 0x015c, Throwable -> 0x0153, all -> 0x014b }
            r4 = 8192(0x2000, float:1.14794E-41)
            r3.<init>(r1, r4)     // Catch:{ UnknownHostException -> 0x0170, FileNotFoundException -> 0x0165, IOException -> 0x015c, Throwable -> 0x0153, all -> 0x014b }
            org.osmdroid.a.c.c.a(r2, r3)     // Catch:{ UnknownHostException -> 0x0175, FileNotFoundException -> 0x016a, IOException -> 0x0160, Throwable -> 0x0157, all -> 0x014f }
            r3.flush()     // Catch:{ UnknownHostException -> 0x0175, FileNotFoundException -> 0x016a, IOException -> 0x0160, Throwable -> 0x0157, all -> 0x014f }
            byte[] r1 = r1.toByteArray()     // Catch:{ UnknownHostException -> 0x0175, FileNotFoundException -> 0x016a, IOException -> 0x0160, Throwable -> 0x0157, all -> 0x014f }
            java.io.ByteArrayInputStream r4 = new java.io.ByteArrayInputStream     // Catch:{ UnknownHostException -> 0x0175, FileNotFoundException -> 0x016a, IOException -> 0x0160, Throwable -> 0x0157, all -> 0x014f }
            r4.<init>(r1)     // Catch:{ UnknownHostException -> 0x0175, FileNotFoundException -> 0x016a, IOException -> 0x0160, Throwable -> 0x0157, all -> 0x014f }
            org.osmdroid.a.b.r r1 = r9.f326a     // Catch:{ UnknownHostException -> 0x0175, FileNotFoundException -> 0x016a, IOException -> 0x0160, Throwable -> 0x0157, all -> 0x014f }
            org.osmdroid.a.b.c r1 = r1.e     // Catch:{ UnknownHostException -> 0x0175, FileNotFoundException -> 0x016a, IOException -> 0x0160, Throwable -> 0x0157, all -> 0x014f }
            if (r1 == 0) goto L_0x007a
            org.osmdroid.a.b.r r1 = r9.f326a     // Catch:{ UnknownHostException -> 0x0175, FileNotFoundException -> 0x016a, IOException -> 0x0160, Throwable -> 0x0157, all -> 0x014f }
            org.osmdroid.a.b.c r1 = r1.e     // Catch:{ UnknownHostException -> 0x0175, FileNotFoundException -> 0x016a, IOException -> 0x0160, Throwable -> 0x0157, all -> 0x014f }
            org.osmdroid.a.b.r r5 = r9.f326a     // Catch:{ UnknownHostException -> 0x0175, FileNotFoundException -> 0x016a, IOException -> 0x0160, Throwable -> 0x0157, all -> 0x014f }
            org.osmdroid.a.a.d r5 = r5.f     // Catch:{ UnknownHostException -> 0x0175, FileNotFoundException -> 0x016a, IOException -> 0x0160, Throwable -> 0x0157, all -> 0x014f }
            r1.a(r5, r0, r4)     // Catch:{ UnknownHostException -> 0x0175, FileNotFoundException -> 0x016a, IOException -> 0x0160, Throwable -> 0x0157, all -> 0x014f }
            r4.reset()     // Catch:{ UnknownHostException -> 0x0175, FileNotFoundException -> 0x016a, IOException -> 0x0160, Throwable -> 0x0157, all -> 0x014f }
        L_0x007a:
            org.osmdroid.a.b.r r1 = r9.f326a     // Catch:{ UnknownHostException -> 0x0175, FileNotFoundException -> 0x016a, IOException -> 0x0160, Throwable -> 0x0157, all -> 0x014f }
            org.osmdroid.a.a.d r1 = r1.f     // Catch:{ UnknownHostException -> 0x0175, FileNotFoundException -> 0x016a, IOException -> 0x0160, Throwable -> 0x0157, all -> 0x014f }
            android.graphics.drawable.Drawable r0 = r1.a(r4)     // Catch:{ UnknownHostException -> 0x0175, FileNotFoundException -> 0x016a, IOException -> 0x0160, Throwable -> 0x0157, all -> 0x014f }
            org.osmdroid.a.c.c.a(r2)
            org.osmdroid.a.c.c.a(r3)
            goto L_0x000a
        L_0x008b:
            r1 = move-exception
            r2 = r7
            r3 = r7
        L_0x008e:
            org.a.c r4 = org.osmdroid.a.b.r.c     // Catch:{ all -> 0x00ba }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ba }
            r5.<init>()     // Catch:{ all -> 0x00ba }
            java.lang.String r6 = "UnknownHostException downloading MapTile: "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x00ba }
            java.lang.StringBuilder r0 = r5.append(r0)     // Catch:{ all -> 0x00ba }
            java.lang.String r5 = " : "
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ all -> 0x00ba }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x00ba }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00ba }
            r4.c(r0)     // Catch:{ all -> 0x00ba }
            org.osmdroid.a.b.p r0 = new org.osmdroid.a.b.p     // Catch:{ all -> 0x00ba }
            org.osmdroid.a.b.r r4 = r9.f326a     // Catch:{ all -> 0x00ba }
            r0.<init>(r4, r1)     // Catch:{ all -> 0x00ba }
            throw r0     // Catch:{ all -> 0x00ba }
        L_0x00ba:
            r0 = move-exception
            r1 = r2
            r2 = r3
        L_0x00bd:
            org.osmdroid.a.c.c.a(r2)
            org.osmdroid.a.c.c.a(r1)
            throw r0
        L_0x00c4:
            r1 = move-exception
            r2 = r7
            r3 = r7
        L_0x00c7:
            org.a.c r4 = org.osmdroid.a.b.r.c     // Catch:{ all -> 0x00ba }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ba }
            r5.<init>()     // Catch:{ all -> 0x00ba }
            java.lang.String r6 = "Tile not found: "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x00ba }
            java.lang.StringBuilder r0 = r5.append(r0)     // Catch:{ all -> 0x00ba }
            java.lang.String r5 = " : "
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ all -> 0x00ba }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x00ba }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00ba }
            r4.c(r0)     // Catch:{ all -> 0x00ba }
            org.osmdroid.a.c.c.a(r3)
            org.osmdroid.a.c.c.a(r2)
        L_0x00f1:
            r0 = r7
            goto L_0x000a
        L_0x00f4:
            r1 = move-exception
            r2 = r7
            r3 = r7
        L_0x00f7:
            org.a.c r4 = org.osmdroid.a.b.r.c     // Catch:{ all -> 0x00ba }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ba }
            r5.<init>()     // Catch:{ all -> 0x00ba }
            java.lang.String r6 = "IOException downloading MapTile: "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x00ba }
            java.lang.StringBuilder r0 = r5.append(r0)     // Catch:{ all -> 0x00ba }
            java.lang.String r5 = " : "
            java.lang.StringBuilder r0 = r0.append(r5)     // Catch:{ all -> 0x00ba }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x00ba }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00ba }
            r4.c(r0)     // Catch:{ all -> 0x00ba }
            org.osmdroid.a.c.c.a(r3)
            org.osmdroid.a.c.c.a(r2)
            goto L_0x00f1
        L_0x0122:
            r1 = move-exception
            r2 = r7
            r3 = r7
        L_0x0125:
            org.a.c r4 = org.osmdroid.a.b.r.c     // Catch:{ all -> 0x00ba }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ba }
            r5.<init>()     // Catch:{ all -> 0x00ba }
            java.lang.String r6 = "Error downloading MapTile: "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x00ba }
            java.lang.StringBuilder r0 = r5.append(r0)     // Catch:{ all -> 0x00ba }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00ba }
            r4.c(r0, r1)     // Catch:{ all -> 0x00ba }
            org.osmdroid.a.c.c.a(r3)
            org.osmdroid.a.c.c.a(r2)
            goto L_0x00f1
        L_0x0146:
            r0 = move-exception
            r1 = r7
            r2 = r7
            goto L_0x00bd
        L_0x014b:
            r0 = move-exception
            r1 = r7
            goto L_0x00bd
        L_0x014f:
            r0 = move-exception
            r1 = r3
            goto L_0x00bd
        L_0x0153:
            r1 = move-exception
            r3 = r2
            r2 = r7
            goto L_0x0125
        L_0x0157:
            r1 = move-exception
            r8 = r3
            r3 = r2
            r2 = r8
            goto L_0x0125
        L_0x015c:
            r1 = move-exception
            r3 = r2
            r2 = r7
            goto L_0x00f7
        L_0x0160:
            r1 = move-exception
            r8 = r3
            r3 = r2
            r2 = r8
            goto L_0x00f7
        L_0x0165:
            r1 = move-exception
            r3 = r2
            r2 = r7
            goto L_0x00c7
        L_0x016a:
            r1 = move-exception
            r8 = r3
            r3 = r2
            r2 = r8
            goto L_0x00c7
        L_0x0170:
            r1 = move-exception
            r3 = r2
            r2 = r7
            goto L_0x008e
        L_0x0175:
            r1 = move-exception
            r8 = r3
            r3 = r2
            r2 = r8
            goto L_0x008e
        */
        throw new UnsupportedOperationException("Method not decompiled: org.osmdroid.a.b.d.xa(org.osmdroid.a.d):android.graphics.drawable.Drawable");
    }

    public final Drawable a(org.osmdroid.a.d dVar) {
        return xa(dVar);
    }
}
