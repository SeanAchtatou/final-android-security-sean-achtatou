package org.osmdroid.a.b;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public final class w implements l {

    /* renamed from: a  reason: collision with root package name */
    private final ConnectivityManager f337a;

    public w(Context context) {
        this.f337a = (ConnectivityManager) context.getSystemService("connectivity");
    }

    private final boolean xa() {
        NetworkInfo activeNetworkInfo = this.f337a.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isAvailable();
    }

    public final boolean a() {
        return xa();
    }
}
