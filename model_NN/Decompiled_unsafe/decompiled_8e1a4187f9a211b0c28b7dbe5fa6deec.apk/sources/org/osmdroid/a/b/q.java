package org.osmdroid.a.b;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import org.a.a;
import org.a.c;
import org.osmdroid.a.a.f;

public class q extends h {
    /* access modifiers changed from: private */
    public static final c e = a.a(q.class);
    protected f c;
    private final ArrayList f = new ArrayList();

    public q(org.osmdroid.a.a aVar, f fVar) {
        super(aVar);
        this.c = fVar;
        k();
    }

    /* access modifiers changed from: private */
    public synchronized InputStream a(org.osmdroid.a.f fVar) {
        return xa(fVar);
    }

    private void k() {
        xk();
    }

    private synchronized InputStream xa(org.osmdroid.a.f fVar) {
        InputStream inputStream;
        Iterator it = this.f.iterator();
        while (true) {
            if (!it.hasNext()) {
                inputStream = null;
                break;
            }
            inputStream = ((m) it.next()).a(this.c, fVar);
            if (inputStream != null) {
                break;
            }
        }
        return inputStream;
    }

    private final void xc() {
        k();
    }

    private final void xd() {
        k();
    }

    private final boolean xe() {
        return false;
    }

    private final String xf() {
        return "filearchive";
    }

    private final Runnable xg() {
        return new j(this);
    }

    private final int xh() {
        if (this.c != null) {
            return this.c.d();
        }
        return 23;
    }

    private final int xi() {
        if (this.c != null) {
            return this.c.e();
        }
        return 0;
    }

    private void xk() {
        File[] listFiles;
        this.f.clear();
        if (a() && (listFiles = f342a.listFiles()) != null) {
            for (File a2 : listFiles) {
                m a3 = s.a(a2);
                if (a3 != null) {
                    this.f.add(a3);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void c() {
        xc();
    }

    /* access modifiers changed from: protected */
    public final void d() {
        xd();
    }

    public final boolean e() {
        return xe();
    }

    /* access modifiers changed from: protected */
    public final String f() {
        return xf();
    }

    /* access modifiers changed from: protected */
    public final Runnable g() {
        return xg();
    }

    public final int h() {
        return xh();
    }

    public final int i() {
        return xi();
    }
}
