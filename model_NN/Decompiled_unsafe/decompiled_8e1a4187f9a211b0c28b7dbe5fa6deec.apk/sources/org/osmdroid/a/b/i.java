package org.osmdroid.a.b;

import android.graphics.drawable.Drawable;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import org.osmdroid.a.d;
import org.osmdroid.a.f;

public abstract class i implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ v f330a;

    protected i(v vVar) {
        this.f330a = vVar;
    }

    private d a() {
        return xa();
    }

    private d xa() {
        d dVar;
        synchronized (this.f330a.d) {
            Iterator it = this.f330a.d.keySet().iterator();
            f fVar = null;
            while (it.hasNext()) {
                try {
                    f fVar2 = (f) it.next();
                    if (this.f330a.g.containsKey(fVar2)) {
                        fVar2 = fVar;
                    }
                    fVar = fVar2;
                } catch (ConcurrentModificationException e) {
                    if (fVar != null) {
                        break;
                    }
                    it = this.f330a.d.keySet().iterator();
                }
            }
            if (fVar != null) {
                this.f330a.g.put(fVar, this.f330a.d.get(fVar));
            }
            dVar = fVar != null ? (d) this.f330a.d.get(fVar) : null;
        }
        return dVar;
    }

    private final void xrun() {
        while (true) {
            d a2 = a();
            if (a2 != null) {
                Drawable drawable = null;
                try {
                    drawable = a(a2);
                } catch (p e) {
                    v.c.a("Tile loader can't continue", e);
                    this.f330a.a();
                } catch (Throwable th) {
                    v.c.c("Error downloading tile: " + a2, th);
                }
                if (drawable != null) {
                    v.xa(this.f330a, a2.a());
                    a2.b().a(a2, drawable);
                } else {
                    v.xa(this.f330a, a2.a());
                    a2.b().a(a2);
                }
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public abstract Drawable a(d dVar);

    public final void run() {
        xrun();
    }
}
