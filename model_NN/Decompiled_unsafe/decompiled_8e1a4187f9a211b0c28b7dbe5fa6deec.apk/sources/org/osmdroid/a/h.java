package org.osmdroid.a;

import android.graphics.drawable.Drawable;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.osmdroid.a.d.a;

public final class h implements a {
    private b c;
    private final ReadWriteLock d;

    private h() {
        this.d = new ReentrantReadWriteLock();
        this.c = new b();
    }

    public h(byte b) {
        this();
    }

    private final Drawable xa(f fVar) {
        this.d.readLock().lock();
        Drawable drawable = (Drawable) this.c.get(fVar);
        this.d.readLock().unlock();
        return drawable;
    }

    private final void xa(int i) {
        this.d.readLock().lock();
        this.c.a(i);
        this.d.readLock().unlock();
    }

    private final void xa(f fVar, Drawable drawable) {
        if (drawable != null) {
            this.d.writeLock().lock();
            this.c.put(fVar, drawable);
            this.d.writeLock().unlock();
        }
    }

    private final boolean xb(f fVar) {
        this.d.readLock().lock();
        boolean containsKey = this.c.containsKey(fVar);
        this.d.readLock().unlock();
        return containsKey;
    }

    public final Drawable a(f fVar) {
        return xa(fVar);
    }

    public final void a(int i) {
        xa(i);
    }

    public final void a(f fVar, Drawable drawable) {
        xa(fVar, drawable);
    }

    public final boolean b(f fVar) {
        return xb(fVar);
    }
}
