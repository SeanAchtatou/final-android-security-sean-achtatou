package org.osmdroid.b.b;

public final class b {
    private b() {
    }

    public static int a(float f) {
        return xa(f);
    }

    private static int xa(float f) {
        int i = 1;
        int i2 = 0;
        int i3 = 1;
        while (((float) i) <= f) {
            i *= 2;
            i2 = i3;
            i3++;
        }
        return i2;
    }
}
