package org.osmdroid.b.b;

import android.graphics.Point;

public final class a {
    private a() {
    }

    public static double a(int i, int i2) {
        return xa(i, i2);
    }

    public static Point a(double d, double d2, int i) {
        return xa(d, d2, i);
    }

    public static Point a(int i, int i2, int i3) {
        return xa(i, i2, i3);
    }

    public static double b(int i, int i2) {
        return xb(i, i2);
    }

    private static double xa(int i, int i2) {
        return ((((double) i) / ((double) (1 << i2))) * 360.0d) - 180.0d;
    }

    private static Point xa(double d, double d2, int i) {
        Point point = new Point(0, 0);
        point.x = (int) Math.floor(((180.0d + d2) / 360.0d) * ((double) (1 << i)));
        point.y = (int) Math.floor(((1.0d - (Math.log(Math.tan(d * 0.017453292519943295d) + (1.0d / Math.cos(d * 0.017453292519943295d))) / 3.141592653589793d)) / 2.0d) * ((double) (1 << i)));
        return point;
    }

    private static Point xa(int i, int i2, int i3) {
        return a(((double) i) * 1.0E-6d, ((double) i2) * 1.0E-6d, i3);
    }

    private static double xb(int i, int i2) {
        double d = 3.141592653589793d - ((6.283185307179586d * ((double) i)) / ((double) (1 << i2)));
        return Math.atan((Math.exp(d) - Math.exp(-d)) * 0.5d) * 57.29577951308232d;
    }
}
