package org.osmdroid.b;

import android.view.GestureDetector;
import android.view.MotionEvent;

final class e implements GestureDetector.OnDoubleTapListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ f f356a;

    /* synthetic */ e(f fVar) {
        this(fVar, (byte) 0);
    }

    private e(f fVar, byte b) {
        this.f356a = fVar;
    }

    private final boolean xonDoubleTap(MotionEvent motionEvent) {
        for (int size = this.f356a.e.size() - 1; size >= 0; size--) {
            this.f356a.e.get(size);
        }
        return this.f356a.a(this.f356a.e().a(motionEvent.getX(), motionEvent.getY()));
    }

    private final boolean xonDoubleTapEvent(MotionEvent motionEvent) {
        return false;
    }

    private final boolean xonSingleTapConfirmed(MotionEvent motionEvent) {
        return false;
    }

    public final boolean onDoubleTap(MotionEvent motionEvent) {
        return xonDoubleTap(motionEvent);
    }

    public final boolean onDoubleTapEvent(MotionEvent motionEvent) {
        return xonDoubleTapEvent(motionEvent);
    }

    public final boolean onSingleTapConfirmed(MotionEvent motionEvent) {
        return xonSingleTapConfirmed(motionEvent);
    }
}
