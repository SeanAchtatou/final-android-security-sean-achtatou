package org.osmdroid.b.a;

import android.graphics.Canvas;
import android.view.MotionEvent;
import org.osmdroid.b;
import org.osmdroid.b.f;

public abstract class e {

    /* renamed from: a  reason: collision with root package name */
    private b f348a;
    private boolean b = true;

    public e(b bVar) {
        this.f348a = bVar;
    }

    private final void xa(boolean z) {
        this.b = z;
    }

    private final boolean xa() {
        return this.b;
    }

    private boolean xa(MotionEvent motionEvent, f fVar) {
        return false;
    }

    private void xb() {
    }

    private final void xb(Canvas canvas, f fVar) {
        if (this.b) {
            a(canvas, fVar);
            a(canvas);
        }
    }

    private boolean xb(MotionEvent motionEvent, f fVar) {
        return false;
    }

    /* access modifiers changed from: protected */
    public abstract void a(Canvas canvas);

    /* access modifiers changed from: protected */
    public abstract void a(Canvas canvas, f fVar);

    public final void a(boolean z) {
        xa(z);
    }

    public final boolean a() {
        return xa();
    }

    public boolean a(MotionEvent motionEvent, f fVar) {
        return xa(motionEvent, fVar);
    }

    public void b() {
        xb();
    }

    public final void b(Canvas canvas, f fVar) {
        xb(canvas, fVar);
    }

    public boolean b(MotionEvent motionEvent, f fVar) {
        return xb(motionEvent, fVar);
    }
}
