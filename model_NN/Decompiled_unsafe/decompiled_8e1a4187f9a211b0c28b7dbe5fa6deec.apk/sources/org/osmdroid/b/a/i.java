package org.osmdroid.b.a;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import org.a.a;
import org.a.c;
import org.osmdroid.b;
import org.osmdroid.b.f;

public class i extends e {

    /* renamed from: a  reason: collision with root package name */
    private static final c f351a = a.a(i.class);
    private org.osmdroid.a.i b;
    private Paint c = new Paint();
    private final Rect d = new Rect();
    private final Rect e = new Rect();
    private int f;

    public i(org.osmdroid.a.i iVar, b bVar) {
        super(bVar);
        this.b = iVar;
    }

    private final void xa(Canvas canvas) {
    }

    private final void xa(Canvas canvas, f fVar) {
        org.osmdroid.b.a e2 = fVar.e();
        this.f = 1 << ((e2.c() + e2.b()) - 1);
        canvas.getClipBounds(this.e);
        this.e.offset(this.f, this.f);
        int c2 = e2.c();
        int a2 = e2.a();
        Rect rect = this.e;
        int a3 = f.a(a2);
        int i = (rect.left >> a3) - 1;
        int i2 = rect.right >> a3;
        int i3 = (rect.top >> a3) - 1;
        int i4 = rect.bottom >> a3;
        int i5 = 1 << c2;
        this.b.a(((i4 - i3) + 1) * ((i2 - i) + 1));
        while (i3 <= i4) {
            for (int i6 = i; i6 <= i2; i6++) {
                Drawable a4 = this.b.a(new org.osmdroid.a.f(c2, org.osmdroid.util.c.a(i6, i5), org.osmdroid.util.c.a(i3, i5)));
                if (a4 != null) {
                    this.d.set(i6 * a2, i3 * a2, (i6 * a2) + a2, (i3 * a2) + a2);
                    Rect rect2 = this.d;
                    rect2.offset(-this.f, -this.f);
                    a4.setBounds(rect2);
                    a4.draw(canvas);
                }
            }
            i3++;
        }
    }

    private final void xb() {
        this.b.a();
    }

    private final int xc() {
        return this.b.b();
    }

    private final int xd() {
        return this.b.c();
    }

    /* access modifiers changed from: protected */
    public final void a(Canvas canvas) {
        xa(canvas);
    }

    /* access modifiers changed from: protected */
    public final void a(Canvas canvas, f fVar) {
        xa(canvas, fVar);
    }

    public final void b() {
        xb();
    }

    public final int c() {
        return xc();
    }

    public final int d() {
        return xd();
    }
}
