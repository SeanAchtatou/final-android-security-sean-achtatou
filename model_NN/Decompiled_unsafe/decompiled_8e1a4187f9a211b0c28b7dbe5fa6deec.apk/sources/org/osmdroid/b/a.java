package org.osmdroid.b;

import android.graphics.Point;
import org.osmdroid.util.BoundingBoxE6;
import org.osmdroid.util.GeoPoint;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private final int f344a;
    private final int b;
    private final int c;
    private final int d;
    private final int e;
    private final BoundingBoxE6 f;
    private final int g;
    private final int h;
    private final int i;
    private final Point j;
    private final Point k;
    private /* synthetic */ f l;

    /* synthetic */ a(f fVar) {
        this(fVar, (byte) 0);
    }

    private a(f fVar, byte b2) {
        this.l = fVar;
        this.f344a = this.l.getWidth() / 2;
        this.b = this.l.getHeight() / 2;
        this.c = this.l.j() / 2;
        this.d = -this.c;
        this.e = -this.c;
        this.g = fVar.c;
        this.h = fVar.d;
        this.i = f.a(this.h);
        int i2 = this.h;
        int i3 = this.g;
        int a2 = f.a(i2);
        int i4 = 1 << (i3 - 1);
        this.j = new Point((this.l.getScrollX() >> a2) + i4, (this.l.getScrollY() >> a2) + i4);
        this.k = f.xa(fVar, this.j, this.h);
        this.f = fVar.d();
    }

    private final float xa(float f2) {
        return (f2 / 4.0075016E7f) * ((float) this.l.j());
    }

    private final int xa() {
        return this.h;
    }

    private final Point xa(int i2, int i3, Point point) {
        Point point2 = point != null ? point : new Point();
        point2.set(i2 - this.f344a, i3 - this.b);
        point2.offset(this.l.getScrollX(), this.l.getScrollY());
        return point2;
    }

    private final Point xa(GeoPoint geoPoint, Point point) {
        Point point2 = point != null ? point : new Point();
        Point a2 = org.osmdroid.b.b.a.a(geoPoint.a(), geoPoint.b(), this.l.k());
        point2.set(a2.x, a2.y);
        point2.offset(this.d, this.e);
        return point2;
    }

    private final GeoPoint xa(float f2, float f3) {
        return this.f.a(f2 / ((float) this.l.getWidth()), f3 / ((float) this.l.getHeight()));
    }

    private final GeoPoint xa(int i2, int i3) {
        return a((float) i2, (float) i3);
    }

    private final int xb() {
        return this.i;
    }

    private final Point xb(GeoPoint geoPoint, Point point) {
        return a(geoPoint, point);
    }

    private final int xc() {
        return this.g;
    }

    public final float a(float f2) {
        return xa(f2);
    }

    public final int a() {
        return xa();
    }

    public final Point a(int i2, int i3, Point point) {
        return xa(i2, i3, point);
    }

    public final Point a(GeoPoint geoPoint, Point point) {
        return xa(geoPoint, point);
    }

    public final GeoPoint a(float f2, float f3) {
        return xa(f2, f3);
    }

    public final GeoPoint a(int i2, int i3) {
        return xa(i2, i3);
    }

    public final int b() {
        return xb();
    }

    public final Point b(GeoPoint geoPoint, Point point) {
        return xb(geoPoint, point);
    }

    public final int c() {
        return xc();
    }
}
