package org.osmdroid;

public enum e {
    unknown,
    center,
    direction_arrow,
    marker_default,
    marker_default_focused_base,
    navto_small,
    next,
    previous,
    person;

    private static e xvalueOf(String str) {
        return (e) Enum.valueOf(e.class, str);
    }

    private static e[] xvalues() {
        return (e[]) j.clone();
    }
}
