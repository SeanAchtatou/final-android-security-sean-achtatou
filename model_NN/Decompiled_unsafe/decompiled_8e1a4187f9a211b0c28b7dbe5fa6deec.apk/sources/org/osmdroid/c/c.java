package org.osmdroid.c;

import org.osmdroid.b.f;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private f f361a;
    private int b;
    private int c;

    public c(f fVar, int i, int i2) {
        this.f361a = fVar;
        this.b = i;
        this.c = i2;
    }

    private final String xtoString() {
        return "ScrollEvent [source=" + this.f361a + ", x=" + this.b + ", y=" + this.c + "]";
    }

    public final String toString() {
        return xtoString();
    }
}
