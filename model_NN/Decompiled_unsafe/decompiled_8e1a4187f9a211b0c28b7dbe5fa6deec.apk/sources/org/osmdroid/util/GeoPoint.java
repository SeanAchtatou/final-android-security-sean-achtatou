package org.osmdroid.util;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import org.osmdroid.d.a;

public class GeoPoint implements Parcelable, Serializable, Cloneable, a {
    public static final Parcelable.Creator CREATOR = new e();

    /* renamed from: a  reason: collision with root package name */
    private int f365a;
    private int b;
    private int c;

    public GeoPoint(double d, double d2) {
        this.b = (int) (d * 1000000.0d);
        this.f365a = (int) (d2 * 1000000.0d);
    }

    public GeoPoint(int i, int i2) {
        this.b = i;
        this.f365a = i2;
    }

    /* synthetic */ GeoPoint(Parcel parcel) {
        this(parcel, (byte) 0);
    }

    private GeoPoint(Parcel parcel, byte b2) {
        this.b = parcel.readInt();
        this.f365a = parcel.readInt();
        this.c = parcel.readInt();
    }

    private final int xa() {
        return this.b;
    }

    private final int xa(GeoPoint geoPoint) {
        double d = ((double) (0.017453292f * ((float) this.b))) / 1000000.0d;
        double d2 = ((double) (0.017453292f * ((float) this.f365a))) / 1000000.0d;
        double d3 = ((double) (0.017453292f * ((float) geoPoint.b))) / 1000000.0d;
        double d4 = ((double) (0.017453292f * ((float) geoPoint.f365a))) / 1000000.0d;
        double cos = Math.cos(d);
        double cos2 = Math.cos(d3);
        return (int) (Math.acos((Math.sin(d) * Math.sin(d3)) + (Math.sin(d2) * cos * cos2 * Math.sin(d4)) + (Math.cos(d2) * cos * cos2 * Math.cos(d4))) * 6378137.0d);
    }

    private final int xb() {
        return this.f365a;
    }

    private Object xclone() {
        return new GeoPoint(this.b, this.f365a);
    }

    private int xdescribeContents() {
        return 0;
    }

    private boolean xequals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        GeoPoint geoPoint = (GeoPoint) obj;
        return geoPoint.b == this.b && geoPoint.f365a == this.f365a && geoPoint.c == this.c;
    }

    private String xtoString() {
        return this.b + "," + this.f365a + "," + this.c;
    }

    private void xwriteToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.b);
        parcel.writeInt(this.f365a);
        parcel.writeInt(this.c);
    }

    public final int a() {
        return xa();
    }

    public final int a(GeoPoint geoPoint) {
        return xa(geoPoint);
    }

    public final int b() {
        return xb();
    }

    public Object clone() {
        return xclone();
    }

    public int describeContents() {
        return xdescribeContents();
    }

    public boolean equals(Object obj) {
        return xequals(obj);
    }

    public String toString() {
        return xtoString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        xwriteToParcel(parcel, i);
    }
}
