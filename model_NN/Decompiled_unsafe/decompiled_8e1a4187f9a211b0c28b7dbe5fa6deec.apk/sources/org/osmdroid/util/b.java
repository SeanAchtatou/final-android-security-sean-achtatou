package org.osmdroid.util;

import android.os.Parcel;
import android.os.Parcelable;

final class b implements Parcelable.Creator {
    b() {
    }

    /* access modifiers changed from: private */
    /* renamed from: xcreateFromParcel */
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        return BoundingBoxE6.xa(parcel);
    }

    /* access modifiers changed from: private */
    /* renamed from: xnewArray */
    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new BoundingBoxE6[i];
    }
}
