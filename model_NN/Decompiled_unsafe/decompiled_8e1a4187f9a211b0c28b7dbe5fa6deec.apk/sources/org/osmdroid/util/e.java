package org.osmdroid.util;

import android.os.Parcel;
import android.os.Parcelable;

final class e implements Parcelable.Creator {
    e() {
    }

    /* access modifiers changed from: private */
    /* renamed from: xcreateFromParcel */
    public final /* bridge */ /* synthetic */ Object createFromParcel(Parcel parcel) {
        return new GeoPoint(parcel);
    }

    /* access modifiers changed from: private */
    /* renamed from: xnewArray */
    public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
        return new GeoPoint[i];
    }
}
