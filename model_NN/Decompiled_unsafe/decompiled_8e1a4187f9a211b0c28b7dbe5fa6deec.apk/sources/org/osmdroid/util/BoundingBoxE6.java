package org.osmdroid.util;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

public class BoundingBoxE6 implements Parcelable, Serializable {
    public static final Parcelable.Creator CREATOR = new b();

    /* renamed from: a  reason: collision with root package name */
    private int f364a;
    private int b;
    private int c;
    private int d;

    public BoundingBoxE6(double d2, double d3, double d4, double d5) {
        this.f364a = (int) (d2 * 1000000.0d);
        this.c = (int) (d3 * 1000000.0d);
        this.b = (int) (d4 * 1000000.0d);
        this.d = (int) (d5 * 1000000.0d);
    }

    private BoundingBoxE6(int i, int i2, int i3, int i4) {
        this.f364a = i;
        this.c = i2;
        this.b = i3;
        this.d = i4;
    }

    public static BoundingBoxE6 a(ArrayList arrayList) {
        return xa(arrayList);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ BoundingBoxE6 xa(Parcel parcel) {
        return new BoundingBoxE6(parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readInt());
    }

    private static BoundingBoxE6 xa(ArrayList arrayList) {
        int i = Integer.MIN_VALUE;
        Iterator it = arrayList.iterator();
        int i2 = Integer.MAX_VALUE;
        int i3 = Integer.MAX_VALUE;
        int i4 = Integer.MIN_VALUE;
        while (it.hasNext()) {
            GeoPoint geoPoint = (GeoPoint) it.next();
            int a2 = geoPoint.a();
            int b2 = geoPoint.b();
            i3 = Math.min(i3, a2);
            i2 = Math.min(i2, b2);
            i4 = Math.max(i4, a2);
            i = Math.max(i, b2);
        }
        return new BoundingBoxE6(i3, i2, i4, i);
    }

    private final GeoPoint xa() {
        return new GeoPoint((this.f364a + this.b) / 2, (this.c + this.d) / 2);
    }

    private final GeoPoint xa(float f, float f2) {
        int c2 = (int) (((float) this.f364a) - (((float) c()) * f2));
        int d2 = (int) (((float) this.d) + (((float) d()) * f));
        while (c2 > 90500000) {
            c2 -= 90500000;
        }
        while (c2 < -90500000) {
            c2 += 90500000;
        }
        while (d2 > 180000000) {
            d2 -= 180000000;
        }
        while (d2 < -180000000) {
            d2 += 180000000;
        }
        return new GeoPoint(c2, d2);
    }

    private final int xb() {
        return new GeoPoint(this.f364a, this.d).a(new GeoPoint(this.b, this.c));
    }

    private final int xc() {
        return Math.abs(this.f364a - this.b);
    }

    private final int xd() {
        return Math.abs(this.c - this.d);
    }

    private int xdescribeContents() {
        return 0;
    }

    private String xtoString() {
        return new StringBuffer().append("N:").append(this.f364a).append("; E:").append(this.c).append("; S:").append(this.b).append("; W:").append(this.d).toString();
    }

    private void xwriteToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f364a);
        parcel.writeInt(this.c);
        parcel.writeInt(this.b);
        parcel.writeInt(this.d);
    }

    public final GeoPoint a() {
        return xa();
    }

    public final GeoPoint a(float f, float f2) {
        return xa(f, f2);
    }

    public final int b() {
        return xb();
    }

    public final int c() {
        return xc();
    }

    public final int d() {
        return xd();
    }

    public int describeContents() {
        return xdescribeContents();
    }

    public String toString() {
        return xtoString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        xwriteToParcel(parcel, i);
    }
}
