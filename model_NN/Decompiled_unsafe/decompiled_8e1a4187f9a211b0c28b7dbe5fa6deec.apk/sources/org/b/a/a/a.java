package org.b.a.a;

public final class a {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public float f318a;
    /* access modifiers changed from: private */
    public float b;
    /* access modifiers changed from: private */
    public float c;
    /* access modifiers changed from: private */
    public float d;
    /* access modifiers changed from: private */
    public float e;
    /* access modifiers changed from: private */
    public float f;
    /* access modifiers changed from: private */
    public boolean g;
    /* access modifiers changed from: private */
    public boolean h;
    /* access modifiers changed from: private */
    public boolean i;

    private final float xa() {
        if (!this.g) {
            return 1.0f;
        }
        return this.c;
    }

    private final void xa(float f2) {
        this.f318a = 0.0f;
        this.b = 0.0f;
        this.g = true;
        this.c = f2 == 0.0f ? 1.0f : f2;
        this.h = false;
        this.d = 1.0f;
        this.e = 1.0f;
        this.i = false;
        this.f = 0.0f;
    }

    private final void xa(float f2, float f3, float f4, float f5, float f6, float f7) {
        this.f318a = f2;
        this.b = f3;
        this.c = f4 == 0.0f ? 1.0f : f4;
        this.d = f5 == 0.0f ? 1.0f : f5;
        this.e = f6 == 0.0f ? 1.0f : f6;
        this.f = f7;
    }

    public final float a() {
        return xa();
    }

    public final void a(float f2) {
        xa(f2);
    }

    /* access modifiers changed from: protected */
    public final void a(float f2, float f3, float f4, float f5, float f6, float f7) {
        xa(f2, f3, f4, f5, f6, f7);
    }
}
