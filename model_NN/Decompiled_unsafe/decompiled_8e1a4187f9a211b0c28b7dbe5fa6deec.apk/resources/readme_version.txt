these are the sources of
* httpcomponents-core-4.1
* httpcomponents-client-4.1 beta 1
with package names "org.apache.http"
refactored to "com.agilebinary.org.apache.http"

and package ...auth / Authentication support removed due to dependencies to jgss

and also some sources of apache commons codec thrown in to resolve dependencies

plus a cooked up "LogFactory" to connect to our own logging