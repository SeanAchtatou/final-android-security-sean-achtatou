package me.leolin.shortcutbadger;

import android.content.ComponentName;
import android.content.Context;
import android.util.Log;
import java.util.LinkedList;
import java.util.List;
import me.leolin.shortcutbadger.impl.AdwHomeBadger;
import me.leolin.shortcutbadger.impl.ApexHomeBadger;
import me.leolin.shortcutbadger.impl.AsusHomeLauncher;
import me.leolin.shortcutbadger.impl.HuaweiHomeBadger;
import me.leolin.shortcutbadger.impl.NewHtcHomeBadger;
import me.leolin.shortcutbadger.impl.NovaHomeBadger;
import me.leolin.shortcutbadger.impl.SamsungHomeBadger;
import me.leolin.shortcutbadger.impl.SonyHomeBadger;
import me.leolin.shortcutbadger.impl.XiaomiHomeBadger;
import me.leolin.shortcutbadger.impl.ZukHomeBadger;

public final class ShortcutBadger {
    private static final List<Class<? extends Badger>> BADGERS = new LinkedList();
    private static final String LOG_TAG = "ShortcutBadger";
    private static ComponentName sComponentName;
    private static Badger sShortcutBadger;

    static {
        BADGERS.add(AdwHomeBadger.class);
        BADGERS.add(ApexHomeBadger.class);
        BADGERS.add(NewHtcHomeBadger.class);
        BADGERS.add(NovaHomeBadger.class);
        BADGERS.add(SonyHomeBadger.class);
        BADGERS.add(XiaomiHomeBadger.class);
        BADGERS.add(AsusHomeLauncher.class);
        BADGERS.add(HuaweiHomeBadger.class);
        BADGERS.add(SamsungHomeBadger.class);
        BADGERS.add(ZukHomeBadger.class);
    }

    public static boolean applyCount(Context context, int badgeCount) {
        try {
            applyCountOrThrow(context, badgeCount);
            return true;
        } catch (ShortcutBadgeException e) {
            Log.e(LOG_TAG, "Unable to execute badge", e);
            return false;
        }
    }

    public static void applyCountOrThrow(Context context, int badgeCount) throws ShortcutBadgeException {
        if (sShortcutBadger != null || initBadger(context)) {
            try {
                sShortcutBadger.executeBadge(context, sComponentName, badgeCount);
            } catch (Exception e) {
                throw new ShortcutBadgeException("Unable to execute badge", e);
            }
        } else {
            throw new ShortcutBadgeException("No default launcher available");
        }
    }

    public static boolean removeCount(Context context) {
        return applyCount(context, 0);
    }

    public static void removeCountOrThrow(Context context) throws ShortcutBadgeException {
        applyCountOrThrow(context, 0);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v26, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: me.leolin.shortcutbadger.Badger} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean initBadger(android.content.Context r8) {
        /*
            android.content.pm.PackageManager r6 = r8.getPackageManager()
            java.lang.String r7 = r8.getPackageName()
            android.content.Intent r6 = r6.getLaunchIntentForPackage(r7)
            android.content.ComponentName r6 = r6.getComponent()
            me.leolin.shortcutbadger.ShortcutBadger.sComponentName = r6
            android.content.Intent r3 = new android.content.Intent
            java.lang.String r6 = "android.intent.action.MAIN"
            r3.<init>(r6)
            java.lang.String r6 = "android.intent.category.HOME"
            r3.addCategory(r6)
            android.content.pm.PackageManager r6 = r8.getPackageManager()
            r7 = 65536(0x10000, float:9.18355E-41)
            android.content.pm.ResolveInfo r4 = r6.resolveActivity(r3, r7)
            if (r4 == 0) goto L_0x003a
            android.content.pm.ActivityInfo r6 = r4.activityInfo
            java.lang.String r6 = r6.name
            java.lang.String r6 = r6.toLowerCase()
            java.lang.String r7 = "resolver"
            boolean r6 = r6.contains(r7)
            if (r6 == 0) goto L_0x003c
        L_0x003a:
            r6 = 0
        L_0x003b:
            return r6
        L_0x003c:
            android.content.pm.ActivityInfo r6 = r4.activityInfo
            java.lang.String r2 = r6.packageName
            java.util.List<java.lang.Class<? extends me.leolin.shortcutbadger.Badger>> r6 = me.leolin.shortcutbadger.ShortcutBadger.BADGERS
            java.util.Iterator r7 = r6.iterator()
        L_0x0046:
            boolean r6 = r7.hasNext()
            if (r6 == 0) goto L_0x0069
            java.lang.Object r1 = r7.next()
            java.lang.Class r1 = (java.lang.Class) r1
            r5 = 0
            java.lang.Object r6 = r1.newInstance()     // Catch:{ Exception -> 0x009a }
            r0 = r6
            me.leolin.shortcutbadger.Badger r0 = (me.leolin.shortcutbadger.Badger) r0     // Catch:{ Exception -> 0x009a }
            r5 = r0
        L_0x005b:
            if (r5 == 0) goto L_0x0046
            java.util.List r6 = r5.getSupportLaunchers()
            boolean r6 = r6.contains(r2)
            if (r6 == 0) goto L_0x0046
            me.leolin.shortcutbadger.ShortcutBadger.sShortcutBadger = r5
        L_0x0069:
            me.leolin.shortcutbadger.Badger r6 = me.leolin.shortcutbadger.ShortcutBadger.sShortcutBadger
            if (r6 != 0) goto L_0x007e
            java.lang.String r6 = android.os.Build.MANUFACTURER
            java.lang.String r7 = "Xiaomi"
            boolean r6 = r6.equalsIgnoreCase(r7)
            if (r6 == 0) goto L_0x0080
            me.leolin.shortcutbadger.impl.XiaomiHomeBadger r6 = new me.leolin.shortcutbadger.impl.XiaomiHomeBadger
            r6.<init>()
            me.leolin.shortcutbadger.ShortcutBadger.sShortcutBadger = r6
        L_0x007e:
            r6 = 1
            goto L_0x003b
        L_0x0080:
            java.lang.String r6 = android.os.Build.MANUFACTURER
            java.lang.String r7 = "ZUK"
            boolean r6 = r6.equalsIgnoreCase(r7)
            if (r6 == 0) goto L_0x0092
            me.leolin.shortcutbadger.impl.ZukHomeBadger r6 = new me.leolin.shortcutbadger.impl.ZukHomeBadger
            r6.<init>()
            me.leolin.shortcutbadger.ShortcutBadger.sShortcutBadger = r6
            goto L_0x007e
        L_0x0092:
            me.leolin.shortcutbadger.impl.DefaultBadger r6 = new me.leolin.shortcutbadger.impl.DefaultBadger
            r6.<init>()
            me.leolin.shortcutbadger.ShortcutBadger.sShortcutBadger = r6
            goto L_0x007e
        L_0x009a:
            r6 = move-exception
            goto L_0x005b
        */
        throw new UnsupportedOperationException("Method not decompiled: me.leolin.shortcutbadger.ShortcutBadger.initBadger(android.content.Context):boolean");
    }

    private ShortcutBadger() {
    }
}
