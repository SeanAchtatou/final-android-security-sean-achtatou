package com.bumptech.glide.request.target;

import android.widget.ImageView;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;

public class GlideDrawableImageViewTarget extends ImageViewTarget<GlideDrawable> {
    private static final float SQUARE_RATIO_MARGIN = 0.05f;
    private int maxLoopCount;
    private GlideDrawable resource;

    public /* bridge */ /* synthetic */ void onResourceReady(Object x0, GlideAnimation x1) {
        onResourceReady((GlideDrawable) x0, (GlideAnimation<? super GlideDrawable>) x1);
    }

    public GlideDrawableImageViewTarget(ImageView view) {
        this(view, -1);
    }

    public GlideDrawableImageViewTarget(ImageView view, int maxLoopCount2) {
        super(view);
        this.maxLoopCount = maxLoopCount2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bumptech.glide.request.target.ImageViewTarget.onResourceReady(java.lang.Object, com.bumptech.glide.request.animation.GlideAnimation):void
     arg types: [com.bumptech.glide.load.resource.drawable.GlideDrawable, com.bumptech.glide.request.animation.GlideAnimation<? super com.bumptech.glide.load.resource.drawable.GlideDrawable>]
     candidates:
      com.bumptech.glide.request.target.GlideDrawableImageViewTarget.onResourceReady(com.bumptech.glide.load.resource.drawable.GlideDrawable, com.bumptech.glide.request.animation.GlideAnimation<? super com.bumptech.glide.load.resource.drawable.GlideDrawable>):void
      com.bumptech.glide.request.target.ImageViewTarget.onResourceReady(java.lang.Object, com.bumptech.glide.request.animation.GlideAnimation):void */
    public void onResourceReady(GlideDrawable resource2, GlideAnimation<? super GlideDrawable> animation) {
        if (!resource2.isAnimated()) {
            float viewRatio = ((float) ((ImageView) this.view).getWidth()) / ((float) ((ImageView) this.view).getHeight());
            float drawableRatio = ((float) resource2.getIntrinsicWidth()) / ((float) resource2.getIntrinsicHeight());
            if (Math.abs(viewRatio - 1.0f) <= SQUARE_RATIO_MARGIN && Math.abs(drawableRatio - 1.0f) <= SQUARE_RATIO_MARGIN) {
                resource2 = new SquaringDrawable(resource2, ((ImageView) this.view).getWidth());
            }
        }
        super.onResourceReady((Object) resource2, (GlideAnimation) animation);
        this.resource = resource2;
        resource2.setLoopCount(this.maxLoopCount);
        resource2.start();
    }

    /* access modifiers changed from: protected */
    public void setResource(GlideDrawable resource2) {
        ((ImageView) this.view).setImageDrawable(resource2);
    }

    public void onStart() {
        if (this.resource != null) {
            this.resource.start();
        }
    }

    public void onStop() {
        if (this.resource != null) {
            this.resource.stop();
        }
    }
}
