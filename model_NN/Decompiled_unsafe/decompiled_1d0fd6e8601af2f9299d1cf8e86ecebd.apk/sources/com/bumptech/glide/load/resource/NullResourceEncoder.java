package com.bumptech.glide.load.resource;

import com.bumptech.glide.load.ResourceEncoder;
import com.bumptech.glide.load.engine.Resource;
import java.io.OutputStream;

public class NullResourceEncoder<T> implements ResourceEncoder<T> {
    private static final NullResourceEncoder<?> NULL_ENCODER = new NullResourceEncoder<>();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bumptech.glide.load.resource.NullResourceEncoder.encode(com.bumptech.glide.load.engine.Resource, java.io.OutputStream):boolean
     arg types: [java.lang.Object, java.io.OutputStream]
     candidates:
      com.bumptech.glide.load.resource.NullResourceEncoder.encode(java.lang.Object, java.io.OutputStream):boolean
      com.bumptech.glide.load.Encoder.encode(java.lang.Object, java.io.OutputStream):boolean
      com.bumptech.glide.load.resource.NullResourceEncoder.encode(com.bumptech.glide.load.engine.Resource, java.io.OutputStream):boolean */
    public /* bridge */ /* synthetic */ boolean encode(Object x0, OutputStream x1) {
        return encode((Resource) ((Resource) x0), x1);
    }

    public static <T> NullResourceEncoder<T> get() {
        return NULL_ENCODER;
    }

    public boolean encode(Resource<T> resource, OutputStream os) {
        return false;
    }

    public String getId() {
        return "";
    }
}
