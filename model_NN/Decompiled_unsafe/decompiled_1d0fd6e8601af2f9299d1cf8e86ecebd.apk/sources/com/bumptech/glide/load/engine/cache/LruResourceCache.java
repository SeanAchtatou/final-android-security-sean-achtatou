package com.bumptech.glide.load.engine.cache;

import android.annotation.SuppressLint;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.engine.cache.MemoryCache;
import com.bumptech.glide.util.LruCache;

public class LruResourceCache extends LruCache<Key, Resource<?>> implements MemoryCache {
    private MemoryCache.ResourceRemovedListener listener;

    /* access modifiers changed from: protected */
    public /* bridge */ /* synthetic */ int getSize(Object x0) {
        return getSize((Resource<?>) ((Resource) x0));
    }

    /* access modifiers changed from: protected */
    public /* bridge */ /* synthetic */ void onItemEvicted(Object x0, Object x1) {
        onItemEvicted((Key) x0, (Resource<?>) ((Resource) x1));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bumptech.glide.util.LruCache.put(java.lang.Object, java.lang.Object):Y
     arg types: [com.bumptech.glide.load.Key, com.bumptech.glide.load.engine.Resource]
     candidates:
      com.bumptech.glide.load.engine.cache.LruResourceCache.put(com.bumptech.glide.load.Key, com.bumptech.glide.load.engine.Resource):com.bumptech.glide.load.engine.Resource
      com.bumptech.glide.load.engine.cache.MemoryCache.put(com.bumptech.glide.load.Key, com.bumptech.glide.load.engine.Resource<?>):com.bumptech.glide.load.engine.Resource<?>
      com.bumptech.glide.util.LruCache.put(java.lang.Object, java.lang.Object):Y */
    public /* bridge */ /* synthetic */ Resource put(Key x0, Resource x1) {
        return (Resource) super.put((Object) x0, (Object) x1);
    }

    public /* bridge */ /* synthetic */ Resource remove(Key x0) {
        return (Resource) super.remove((Object) x0);
    }

    public LruResourceCache(int size) {
        super(size);
    }

    public void setResourceRemovedListener(MemoryCache.ResourceRemovedListener listener2) {
        this.listener = listener2;
    }

    /* access modifiers changed from: protected */
    public void onItemEvicted(Key key, Resource<?> item) {
        if (this.listener != null) {
            this.listener.onResourceRemoved(item);
        }
    }

    /* access modifiers changed from: protected */
    public int getSize(Resource<?> item) {
        return item.getSize();
    }

    @SuppressLint({"InlinedApi"})
    public void trimMemory(int level) {
        if (level >= 60) {
            clearMemory();
        } else if (level >= 40) {
            trimToSize(getCurrentSize() / 2);
        }
    }
}
