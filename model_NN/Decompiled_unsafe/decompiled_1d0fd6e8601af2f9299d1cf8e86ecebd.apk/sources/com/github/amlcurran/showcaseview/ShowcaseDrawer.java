package com.github.amlcurran.showcaseview;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.support.annotation.ColorInt;

public interface ShowcaseDrawer {
    void drawShowcase(Bitmap bitmap, float f, float f2, float f3);

    void drawToCanvas(Canvas canvas, Bitmap bitmap);

    void erase(Bitmap bitmap);

    float getBlockedRadius();

    int getShowcaseHeight();

    int getShowcaseWidth();

    void setBackgroundColour(@ColorInt int i);

    void setShowcaseColour(@ColorInt int i);
}
