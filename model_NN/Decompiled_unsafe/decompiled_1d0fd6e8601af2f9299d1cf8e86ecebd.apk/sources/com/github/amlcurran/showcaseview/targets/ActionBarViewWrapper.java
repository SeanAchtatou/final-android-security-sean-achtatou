package com.github.amlcurran.showcaseview.targets;

import android.util.Log;
import android.view.View;
import android.view.ViewParent;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

class ActionBarViewWrapper {
    private Class mAbsActionBarViewClass;
    private ViewParent mActionBarView;
    private Class mActionBarViewClass;

    public ActionBarViewWrapper(ViewParent actionBarView) {
        if (!actionBarView.getClass().getName().contains("ActionBarView")) {
            String previousP = actionBarView.getClass().getName();
            actionBarView = actionBarView.getParent();
            String throwP = actionBarView.getClass().getName();
            if (!actionBarView.getClass().getName().contains("ActionBarView")) {
                throw new IllegalStateException("Cannot find ActionBarView for Activity, instead found " + previousP + " and " + throwP);
            }
        }
        this.mActionBarView = actionBarView;
        this.mActionBarViewClass = actionBarView.getClass();
        this.mAbsActionBarViewClass = actionBarView.getClass().getSuperclass();
    }

    public View getSpinnerView() {
        try {
            Field spinnerField = this.mActionBarViewClass.getDeclaredField("mSpinner");
            spinnerField.setAccessible(true);
            return (View) spinnerField.get(this.mActionBarView);
        } catch (NoSuchFieldException e) {
            Log.e("TAG", "Failed to find actionbar spinner", e);
        } catch (IllegalAccessException e2) {
            Log.e("TAG", "Failed to access actionbar spinner", e2);
        }
        return null;
    }

    public View getTitleView() {
        try {
            Field mTitleViewField = this.mActionBarViewClass.getDeclaredField("mTitleView");
            mTitleViewField.setAccessible(true);
            return (View) mTitleViewField.get(this.mActionBarView);
        } catch (NoSuchFieldException e) {
            Log.e("TAG", "Failed to find actionbar title", e);
        } catch (IllegalAccessException e2) {
            Log.e("TAG", "Failed to access actionbar title", e2);
        }
        return null;
    }

    public View getOverflowView() {
        try {
            Field actionMenuPresenterField = this.mAbsActionBarViewClass.getDeclaredField("mActionMenuPresenter");
            actionMenuPresenterField.setAccessible(true);
            Object actionMenuPresenter = actionMenuPresenterField.get(this.mActionBarView);
            Field overflowButtonField = actionMenuPresenter.getClass().getDeclaredField("mOverflowButton");
            overflowButtonField.setAccessible(true);
            return (View) overflowButtonField.get(actionMenuPresenter);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e2) {
            e2.printStackTrace();
        }
        return null;
    }

    public View getMediaRouterButtonView() {
        try {
            Field actionMenuPresenterField = this.mActionBarViewClass.getDeclaredField("mOptionsMenu");
            actionMenuPresenterField.setAccessible(true);
            Object optionsMenu = actionMenuPresenterField.get(this.mActionBarView);
            Field actionItemsField = optionsMenu.getClass().getDeclaredField("mActionItems");
            actionItemsField.setAccessible(true);
            List actionItems = (ArrayList) actionItemsField.get(optionsMenu);
            if (actionItems != null) {
                for (Object obj : actionItems) {
                    System.out.println(obj);
                    Object view = getMediaRouteButton(obj);
                    if (view != null) {
                        return (View) view;
                    }
                }
            }
            return null;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return null;
        } catch (NoSuchFieldException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    private Object getMediaRouteButton(Object obj) {
        try {
            Field f = obj.getClass().getDeclaredField("mActionView");
            f.setAccessible(true);
            Object view = f.get(obj);
            if ("android.support.v7.app.MediaRouteButton".equals(view.getClass().getName())) {
                return view;
            }
            return null;
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e2) {
            e2.printStackTrace();
        }
    }

    public View getActionItem(int actionItemId) {
        Field mChField;
        try {
            Field actionMenuPresenterField = this.mAbsActionBarViewClass.getDeclaredField("mActionMenuPresenter");
            actionMenuPresenterField.setAccessible(true);
            Object actionMenuPresenter = actionMenuPresenterField.get(this.mActionBarView);
            Field menuViewField = actionMenuPresenter.getClass().getSuperclass().getDeclaredField("mMenuView");
            menuViewField.setAccessible(true);
            Object menuView = menuViewField.get(actionMenuPresenter);
            if (menuView.getClass().toString().contains("com.actionbarsherlock")) {
                mChField = menuView.getClass().getSuperclass().getSuperclass().getSuperclass().getSuperclass().getDeclaredField("mChildren");
            } else if (menuView.getClass().toString().contains("android.support.v7")) {
                mChField = menuView.getClass().getSuperclass().getSuperclass().getSuperclass().getDeclaredField("mChildren");
            } else {
                mChField = menuView.getClass().getSuperclass().getSuperclass().getDeclaredField("mChildren");
            }
            mChField.setAccessible(true);
            for (Object mCh : (Object[]) mChField.get(menuView)) {
                if (mCh != null) {
                    View v = (View) mCh;
                    if (v.getId() == actionItemId) {
                        return v;
                    }
                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e2) {
            e2.printStackTrace();
        }
        return null;
    }
}
