package com.github.amlcurran.showcaseview.targets;

import android.app.Activity;
import com.github.amlcurran.showcaseview.targets.Reflector;

class ReflectorFactory {
    ReflectorFactory() {
    }

    public static Reflector getReflectorForActivity(Activity activity) {
        switch (searchForActivitySuperClass(activity)) {
            case STANDARD:
                return new ActionBarReflector(activity);
            case APP_COMPAT:
                return new AppCompatReflector(activity);
            case ACTIONBAR_SHERLOCK:
                return new SherlockReflector(activity);
            default:
                return null;
        }
    }

    private static Reflector.ActionBarType searchForActivitySuperClass(Activity activity) {
        for (Class currentLevel = activity.getClass(); currentLevel != Activity.class; currentLevel = currentLevel.getSuperclass()) {
            if (currentLevel.getSimpleName().equals("SherlockActivity") || currentLevel.getSimpleName().equals("SherlockFragmentActivity")) {
                return Reflector.ActionBarType.ACTIONBAR_SHERLOCK;
            }
            if (currentLevel.getSimpleName().equals("ActionBarActivity")) {
                return Reflector.ActionBarType.APP_COMPAT;
            }
        }
        return Reflector.ActionBarType.STANDARD;
    }
}
