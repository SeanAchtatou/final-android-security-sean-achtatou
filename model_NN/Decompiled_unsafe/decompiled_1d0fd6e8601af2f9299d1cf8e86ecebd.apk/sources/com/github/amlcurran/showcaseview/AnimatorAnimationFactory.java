package com.github.amlcurran.showcaseview;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.graphics.Point;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import com.github.amlcurran.showcaseview.AnimationFactory;

@TargetApi(11)
class AnimatorAnimationFactory implements AnimationFactory {
    private static final String ALPHA = "alpha";
    private static final float INVISIBLE = 0.0f;
    private static final float VISIBLE = 1.0f;
    private final AccelerateDecelerateInterpolator interpolator = new AccelerateDecelerateInterpolator();

    public void fadeInView(View target, long duration, final AnimationFactory.AnimationStartListener listener) {
        ObjectAnimator oa = ObjectAnimator.ofFloat(target, ALPHA, 0.0f, 1.0f);
        oa.setDuration(duration).addListener(new AnimatorListenerAdapter() {
            public void onAnimationStart(Animator animator) {
                listener.onAnimationStart();
            }
        });
        oa.start();
    }

    public void fadeOutView(View target, long duration, final AnimationFactory.AnimationEndListener listener) {
        ObjectAnimator oa = ObjectAnimator.ofFloat(target, ALPHA, 0.0f);
        oa.setDuration(duration).addListener(new AnimatorListenerAdapter() {
            public void onAnimationEnd(Animator animator) {
                listener.onAnimationEnd();
            }
        });
        oa.start();
    }

    public void animateTargetToPoint(ShowcaseView showcaseView, Point point) {
        AnimatorSet set = new AnimatorSet();
        set.playTogether(ObjectAnimator.ofInt(showcaseView, "showcaseX", point.x), ObjectAnimator.ofInt(showcaseView, "showcaseY", point.y));
        set.setInterpolator(this.interpolator);
        set.start();
    }
}
