package com.github.amlcurran.showcaseview.targets;

import android.app.Activity;
import android.graphics.Point;

public class ActionViewTarget implements Target {
    ActionBarViewWrapper mActionBarWrapper;
    private final Activity mActivity;
    Reflector mReflector;
    private final Type mType;

    public enum Type {
        SPINNER,
        HOME,
        TITLE,
        OVERFLOW,
        MEDIA_ROUTE_BUTTON
    }

    public ActionViewTarget(Activity activity, Type type) {
        this.mActivity = activity;
        this.mType = type;
    }

    /* access modifiers changed from: protected */
    public void setUp() {
        this.mReflector = ReflectorFactory.getReflectorForActivity(this.mActivity);
        this.mActionBarWrapper = new ActionBarViewWrapper(this.mReflector.getActionBarView());
    }

    public Point getPoint() {
        Target internal = null;
        setUp();
        switch (this.mType) {
            case SPINNER:
                internal = new ViewTarget(this.mActionBarWrapper.getSpinnerView());
                break;
            case HOME:
                internal = new ViewTarget(this.mReflector.getHomeButton());
                break;
            case OVERFLOW:
                internal = new ViewTarget(this.mActionBarWrapper.getOverflowView());
                break;
            case TITLE:
                internal = new ViewTarget(this.mActionBarWrapper.getTitleView());
                break;
            case MEDIA_ROUTE_BUTTON:
                internal = new ViewTarget(this.mActionBarWrapper.getMediaRouterButtonView());
                break;
        }
        return internal.getPoint();
    }
}
