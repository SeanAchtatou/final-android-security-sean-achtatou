package com.github.amlcurran.showcaseview;

public final class R {

    public static final class attr {
        public static final int showcaseViewStyle = 2130772166;
        public static final int sv_backgroundColor = 2130772309;
        public static final int sv_buttonBackgroundColor = 2130772312;
        public static final int sv_buttonForegroundColor = 2130772313;
        public static final int sv_buttonText = 2130772314;
        public static final int sv_detailTextAppearance = 2130772315;
        public static final int sv_detailTextColor = 2130772310;
        public static final int sv_showcaseColor = 2130772317;
        public static final int sv_tintButtonColor = 2130772318;
        public static final int sv_titleTextAppearance = 2130772316;
        public static final int sv_titleTextColor = 2130772311;
    }

    public static final class dimen {
        public static final int action_bar_offset = 2131361876;
        public static final int button_margin = 2131361877;
        public static final int showcase_radius = 2131361941;
        public static final int showcase_radius_inner = 2131361942;
        public static final int showcase_radius_material = 2131361943;
        public static final int showcase_radius_outer = 2131361944;
        public static final int text_padding = 2131361945;
    }

    public static final class drawable {
        public static final int btn_cling_normal = 2130837599;
        public static final int btn_cling_pressed = 2130837600;
        public static final int button = 2130837617;
        public static final int button_normal = 2130837618;
        public static final int cling = 2130837621;
        public static final int cling_bleached = 2130837622;
        public static final int cling_button_bg = 2130837623;
        public static final int hand = 2130837661;
    }

    public static final class id {
        public static final int showcase_button = 2131755023;
        public static final int showcase_sub_text = 2131755024;
        public static final int showcase_title_text = 2131755025;
    }

    public static final class layout {
        public static final int handy = 2130968648;
        public static final int showcase_button = 2130968696;
    }

    public static final class string {
        public static final int ok = 2131231714;
    }

    public static final class style {
        public static final int ShowcaseButton = 2131427559;
        public static final int ShowcaseView = 2131427561;
        public static final int ShowcaseView_Light = 2131427562;
        public static final int TextAppearance_ShowcaseView_Detail = 2131427617;
        public static final int TextAppearance_ShowcaseView_Detail_Light = 2131427618;
        public static final int TextAppearance_ShowcaseView_Title = 2131427619;
        public static final int TextAppearance_ShowcaseView_Title_Light = 2131427620;
    }

    public static final class styleable {
        public static final int[] CustomTheme = {yahoo.mail.app.R.attr.showcaseViewStyle};
        public static final int CustomTheme_showcaseViewStyle = 0;
        public static final int[] ShowcaseView = {yahoo.mail.app.R.attr.sv_backgroundColor, yahoo.mail.app.R.attr.sv_detailTextColor, yahoo.mail.app.R.attr.sv_titleTextColor, yahoo.mail.app.R.attr.sv_buttonBackgroundColor, yahoo.mail.app.R.attr.sv_buttonForegroundColor, yahoo.mail.app.R.attr.sv_buttonText, yahoo.mail.app.R.attr.sv_detailTextAppearance, yahoo.mail.app.R.attr.sv_titleTextAppearance, yahoo.mail.app.R.attr.sv_showcaseColor, yahoo.mail.app.R.attr.sv_tintButtonColor};
        public static final int ShowcaseView_sv_backgroundColor = 0;
        public static final int ShowcaseView_sv_buttonBackgroundColor = 3;
        public static final int ShowcaseView_sv_buttonForegroundColor = 4;
        public static final int ShowcaseView_sv_buttonText = 5;
        public static final int ShowcaseView_sv_detailTextAppearance = 6;
        public static final int ShowcaseView_sv_detailTextColor = 1;
        public static final int ShowcaseView_sv_showcaseColor = 8;
        public static final int ShowcaseView_sv_tintButtonColor = 9;
        public static final int ShowcaseView_sv_titleTextAppearance = 7;
        public static final int ShowcaseView_sv_titleTextColor = 2;
    }
}
