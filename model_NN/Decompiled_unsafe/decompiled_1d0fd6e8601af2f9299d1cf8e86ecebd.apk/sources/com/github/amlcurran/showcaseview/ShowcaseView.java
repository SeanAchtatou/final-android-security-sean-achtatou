package com.github.amlcurran.showcaseview;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.text.Layout;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import com.github.amlcurran.showcaseview.AnimationFactory;
import com.github.amlcurran.showcaseview.targets.Target;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class ShowcaseView extends RelativeLayout implements View.OnTouchListener, ShowcaseViewApi {
    public static final int ABOVE_SHOWCASE = 1;
    public static final int BELOW_SHOWCASE = 3;
    private static final int HOLO_BLUE = Color.parseColor("#33B5E5");
    public static final int LEFT_OF_SHOWCASE = 0;
    public static final int RIGHT_OF_SHOWCASE = 2;
    public static final int UNDEFINED = -1;
    /* access modifiers changed from: private */
    public final AnimationFactory animationFactory;
    private int backgroundColor;
    private Bitmap bitmapBuffer;
    private boolean blockAllTouches;
    private boolean blockTouches;
    private long fadeInMillis;
    private long fadeOutMillis;
    private boolean hasAlteredText;
    private boolean hasCustomClickListener;
    /* access modifiers changed from: private */
    public boolean hasNoTarget;
    private View.OnClickListener hideOnClickListener;
    private boolean hideOnTouch;
    /* access modifiers changed from: private */
    public boolean isShowing;
    private Button mEndButton;
    /* access modifiers changed from: private */
    public OnShowcaseEventListener mEventListener;
    private final int[] positionInWindow;
    private float scaleMultiplier;
    /* access modifiers changed from: private */
    public final ShotStateStore shotStateStore;
    private boolean shouldCentreText;
    private final ShowcaseAreaCalculator showcaseAreaCalculator;
    private int showcaseColor;
    private ShowcaseDrawer showcaseDrawer;
    private int showcaseX;
    private int showcaseY;
    private final TextDrawer textDrawer;

    @Retention(RetentionPolicy.SOURCE)
    public @interface TextPosition {
    }

    protected ShowcaseView(Context context, boolean newStyle) {
        this(context, null, R.styleable.CustomTheme_showcaseViewStyle, newStyle);
    }

    protected ShowcaseView(Context context, AttributeSet attrs, int defStyle, boolean newStyle) {
        super(context, attrs, defStyle);
        this.showcaseX = -1;
        this.showcaseY = -1;
        this.scaleMultiplier = 1.0f;
        this.hasCustomClickListener = false;
        this.blockTouches = true;
        this.hideOnTouch = false;
        this.mEventListener = OnShowcaseEventListener.NONE;
        this.hasAlteredText = false;
        this.hasNoTarget = false;
        this.positionInWindow = new int[2];
        this.hideOnClickListener = new View.OnClickListener() {
            public void onClick(View v) {
                ShowcaseView.this.hide();
            }
        };
        if (new ApiUtils().isCompatWithHoneycomb()) {
            this.animationFactory = new AnimatorAnimationFactory();
        } else {
            this.animationFactory = new NoAnimationFactory();
        }
        this.showcaseAreaCalculator = new ShowcaseAreaCalculator();
        this.shotStateStore = new ShotStateStore(context);
        TypedArray styled = context.getTheme().obtainStyledAttributes(attrs, R.styleable.ShowcaseView, R.attr.showcaseViewStyle, R.style.ShowcaseView);
        this.fadeInMillis = (long) getResources().getInteger(17694721);
        this.fadeOutMillis = (long) getResources().getInteger(17694721);
        this.mEndButton = (Button) LayoutInflater.from(context).inflate(R.layout.showcase_button, (ViewGroup) null);
        if (newStyle) {
            this.showcaseDrawer = new NewShowcaseDrawer(getResources(), context.getTheme());
        } else {
            this.showcaseDrawer = new StandardShowcaseDrawer(getResources(), context.getTheme());
        }
        this.textDrawer = new TextDrawer(getResources(), getContext());
        updateStyle(styled, false);
        init();
    }

    private void init() {
        setOnTouchListener(this);
        if (this.mEndButton.getParent() == null) {
            int margin = (int) getResources().getDimension(R.dimen.button_margin);
            RelativeLayout.LayoutParams lps = (RelativeLayout.LayoutParams) generateDefaultLayoutParams();
            lps.addRule(12);
            lps.addRule(11);
            lps.setMargins(margin, margin, margin, margin);
            this.mEndButton.setLayoutParams(lps);
            this.mEndButton.setText(17039370);
            if (!this.hasCustomClickListener) {
                this.mEndButton.setOnClickListener(this.hideOnClickListener);
            }
            addView(this.mEndButton);
        }
    }

    private boolean hasShot() {
        return this.shotStateStore.hasShot();
    }

    /* access modifiers changed from: package-private */
    public void setShowcasePosition(Point point) {
        setShowcasePosition(point.x, point.y);
    }

    /* access modifiers changed from: package-private */
    public void setShowcasePosition(int x, int y) {
        if (!this.shotStateStore.hasShot()) {
            getLocationInWindow(this.positionInWindow);
            this.showcaseX = x - this.positionInWindow[0];
            this.showcaseY = y - this.positionInWindow[1];
            recalculateText();
            invalidate();
        }
    }

    public void setTarget(Target target) {
        setShowcase(target, false);
    }

    public void setShowcase(final Target target, final boolean animate) {
        postDelayed(new Runnable() {
            public void run() {
                if (!ShowcaseView.this.shotStateStore.hasShot()) {
                    if (ShowcaseView.this.canUpdateBitmap()) {
                        ShowcaseView.this.updateBitmap();
                    }
                    Point targetPoint = target.getPoint();
                    if (targetPoint != null) {
                        boolean unused = ShowcaseView.this.hasNoTarget = false;
                        if (animate) {
                            ShowcaseView.this.animationFactory.animateTargetToPoint(ShowcaseView.this, targetPoint);
                        } else {
                            ShowcaseView.this.setShowcasePosition(targetPoint);
                        }
                    } else {
                        boolean unused2 = ShowcaseView.this.hasNoTarget = true;
                        ShowcaseView.this.invalidate();
                    }
                }
            }
        }, 100);
    }

    /* access modifiers changed from: private */
    public void updateBitmap() {
        if (this.bitmapBuffer == null || haveBoundsChanged()) {
            if (this.bitmapBuffer != null) {
                this.bitmapBuffer.recycle();
            }
            this.bitmapBuffer = Bitmap.createBitmap(getMeasuredWidth(), getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        }
    }

    private boolean haveBoundsChanged() {
        return (getMeasuredWidth() == this.bitmapBuffer.getWidth() && getMeasuredHeight() == this.bitmapBuffer.getHeight()) ? false : true;
    }

    public boolean hasShowcaseView() {
        return (this.showcaseX == 1000000 || this.showcaseY == 1000000 || this.hasNoTarget) ? false : true;
    }

    public void setShowcaseX(int x) {
        setShowcasePosition(x, this.showcaseY);
    }

    public void setShowcaseY(int y) {
        setShowcasePosition(this.showcaseX, y);
    }

    public int getShowcaseX() {
        return this.showcaseX;
    }

    public int getShowcaseY() {
        return this.showcaseY;
    }

    public void overrideButtonClick(View.OnClickListener listener) {
        if (!this.shotStateStore.hasShot()) {
            if (this.mEndButton != null) {
                if (listener != null) {
                    this.mEndButton.setOnClickListener(listener);
                } else {
                    this.mEndButton.setOnClickListener(this.hideOnClickListener);
                }
            }
            this.hasCustomClickListener = true;
        }
    }

    public void setOnShowcaseEventListener(OnShowcaseEventListener listener) {
        if (listener != null) {
            this.mEventListener = listener;
        } else {
            this.mEventListener = OnShowcaseEventListener.NONE;
        }
    }

    public void setButtonText(CharSequence text) {
        if (this.mEndButton != null) {
            this.mEndButton.setText(text);
        }
    }

    private void recalculateText() {
        boolean recalculateText;
        if (this.showcaseAreaCalculator.calculateShowcaseRect((float) this.showcaseX, (float) this.showcaseY, this.showcaseDrawer) || this.hasAlteredText) {
            recalculateText = true;
        } else {
            recalculateText = false;
        }
        if (recalculateText) {
            this.textDrawer.calculateTextPosition(getMeasuredWidth(), getMeasuredHeight(), this.shouldCentreText, hasShowcaseView() ? this.showcaseAreaCalculator.getShowcaseRect() : new Rect());
        }
        this.hasAlteredText = false;
    }

    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        if (this.showcaseX < 0 || this.showcaseY < 0 || this.shotStateStore.hasShot() || this.bitmapBuffer == null) {
            super.dispatchDraw(canvas);
            return;
        }
        this.showcaseDrawer.erase(this.bitmapBuffer);
        if (!this.hasNoTarget) {
            this.showcaseDrawer.drawShowcase(this.bitmapBuffer, (float) this.showcaseX, (float) this.showcaseY, this.scaleMultiplier);
            this.showcaseDrawer.drawToCanvas(canvas, this.bitmapBuffer);
        }
        this.textDrawer.draw(canvas);
        super.dispatchDraw(canvas);
    }

    public void hide() {
        this.shotStateStore.storeShot();
        this.mEventListener.onShowcaseViewHide(this);
        fadeOutShowcase();
    }

    /* access modifiers changed from: private */
    public void clearBitmap() {
        if (this.bitmapBuffer != null && !this.bitmapBuffer.isRecycled()) {
            this.bitmapBuffer.recycle();
            this.bitmapBuffer = null;
        }
    }

    private void fadeOutShowcase() {
        this.animationFactory.fadeOutView(this, this.fadeOutMillis, new AnimationFactory.AnimationEndListener() {
            public void onAnimationEnd() {
                ShowcaseView.this.setVisibility(8);
                ShowcaseView.this.clearBitmap();
                boolean unused = ShowcaseView.this.isShowing = false;
                ShowcaseView.this.mEventListener.onShowcaseViewDidHide(ShowcaseView.this);
            }
        });
    }

    public void show() {
        this.isShowing = true;
        if (canUpdateBitmap()) {
            updateBitmap();
        }
        this.mEventListener.onShowcaseViewShow(this);
        fadeInShowcase();
    }

    /* access modifiers changed from: private */
    public boolean canUpdateBitmap() {
        return getMeasuredHeight() > 0 && getMeasuredWidth() > 0;
    }

    private void fadeInShowcase() {
        this.animationFactory.fadeInView(this, this.fadeInMillis, new AnimationFactory.AnimationStartListener() {
            public void onAnimationStart() {
                ShowcaseView.this.setVisibility(0);
            }
        });
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        boolean blocked = true;
        if (this.blockAllTouches) {
            this.mEventListener.onShowcaseViewTouchBlocked(motionEvent);
        } else {
            double distanceFromFocus = Math.sqrt(Math.pow((double) Math.abs(motionEvent.getRawX() - ((float) this.showcaseX)), 2.0d) + Math.pow((double) Math.abs(motionEvent.getRawY() - ((float) this.showcaseY)), 2.0d));
            if (1 != motionEvent.getAction() || !this.hideOnTouch || distanceFromFocus <= ((double) this.showcaseDrawer.getBlockedRadius())) {
                if (!this.blockTouches || distanceFromFocus <= ((double) this.showcaseDrawer.getBlockedRadius())) {
                    blocked = false;
                }
                if (blocked) {
                    this.mEventListener.onShowcaseViewTouchBlocked(motionEvent);
                }
            } else {
                hide();
            }
        }
        return blocked;
    }

    /* access modifiers changed from: private */
    public static void insertShowcaseView(ShowcaseView showcaseView, ViewGroup parent, int parentIndex) {
        parent.addView(showcaseView, parentIndex);
        if (!showcaseView.hasShot()) {
            showcaseView.show();
        } else {
            showcaseView.hideImmediate();
        }
    }

    private void hideImmediate() {
        this.isShowing = false;
        setVisibility(8);
    }

    public void setContentTitle(CharSequence title) {
        this.textDrawer.setContentTitle(title);
    }

    public void setContentText(CharSequence text) {
        this.textDrawer.setContentText(text);
    }

    private void setScaleMultiplier(float scaleMultiplier2) {
        this.scaleMultiplier = scaleMultiplier2;
    }

    public void hideButton() {
        this.mEndButton.setVisibility(8);
    }

    public void showButton() {
        this.mEndButton.setVisibility(0);
    }

    public static class Builder {
        private final Activity activity;
        private ViewGroup parent;
        private int parentIndex;
        private final ShowcaseView showcaseView;

        public Builder(Activity activity2) {
            this(activity2, false);
        }

        @Deprecated
        public Builder(Activity activity2, boolean useNewStyle) {
            this.activity = activity2;
            this.showcaseView = new ShowcaseView(activity2, useNewStyle);
            this.showcaseView.setTarget(Target.NONE);
            this.parent = (ViewGroup) activity2.findViewById(16908290);
            this.parentIndex = this.parent.getChildCount();
        }

        public ShowcaseView build() {
            ShowcaseView.insertShowcaseView(this.showcaseView, this.parent, this.parentIndex);
            return this.showcaseView;
        }

        public Builder withHoloShowcase() {
            return setShowcaseDrawer(new StandardShowcaseDrawer(this.activity.getResources(), this.activity.getTheme()));
        }

        public Builder withNewStyleShowcase() {
            return setShowcaseDrawer(new NewShowcaseDrawer(this.activity.getResources(), this.activity.getTheme()));
        }

        public Builder withMaterialShowcase() {
            return setShowcaseDrawer(new MaterialShowcaseDrawer(this.activity.getResources()));
        }

        public Builder setShowcaseDrawer(ShowcaseDrawer showcaseDrawer) {
            this.showcaseView.setShowcaseDrawer(showcaseDrawer);
            return this;
        }

        public Builder setContentTitle(int resId) {
            return setContentTitle(this.activity.getString(resId));
        }

        public Builder setContentTitle(CharSequence title) {
            this.showcaseView.setContentTitle(title);
            return this;
        }

        public Builder setContentText(int resId) {
            return setContentText(this.activity.getString(resId));
        }

        public Builder setContentText(CharSequence text) {
            this.showcaseView.setContentText(text);
            return this;
        }

        public Builder setTarget(Target target) {
            this.showcaseView.setTarget(target);
            return this;
        }

        public Builder setStyle(int theme) {
            this.showcaseView.setStyle(theme);
            return this;
        }

        public Builder setOnClickListener(View.OnClickListener onClickListener) {
            this.showcaseView.overrideButtonClick(onClickListener);
            return this;
        }

        public Builder doNotBlockTouches() {
            this.showcaseView.setBlocksTouches(false);
            return this;
        }

        public Builder hideOnTouchOutside() {
            this.showcaseView.setBlocksTouches(true);
            this.showcaseView.setHideOnTouchOutside(true);
            return this;
        }

        public Builder singleShot(long shotId) {
            this.showcaseView.setSingleShot(shotId);
            return this;
        }

        public Builder setShowcaseEventListener(OnShowcaseEventListener showcaseEventListener) {
            this.showcaseView.setOnShowcaseEventListener(showcaseEventListener);
            return this;
        }

        public Builder setParent(ViewGroup parent2, int index) {
            this.parent = parent2;
            this.parentIndex = index;
            return this;
        }

        public Builder setContentTextPaint(TextPaint textPaint) {
            this.showcaseView.setContentTextPaint(textPaint);
            return this;
        }

        public Builder setContentTitlePaint(TextPaint textPaint) {
            this.showcaseView.setContentTitlePaint(textPaint);
            return this;
        }

        public Builder replaceEndButton(Button button) {
            this.showcaseView.setEndButton(button);
            return this;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, com.github.amlcurran.showcaseview.ShowcaseView, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public Builder replaceEndButton(int buttonResourceId) {
            View view = LayoutInflater.from(this.activity).inflate(buttonResourceId, (ViewGroup) this.showcaseView, false);
            if (view instanceof Button) {
                return replaceEndButton((Button) view);
            }
            throw new IllegalArgumentException("Attempted to replace showcase button with a layout which isn't a button");
        }

        public Builder blockAllTouches() {
            this.showcaseView.setBlockAllTouches(true);
            return this;
        }

        public Builder useDecorViewAsParent() {
            this.parent = (ViewGroup) this.activity.getWindow().getDecorView();
            this.parentIndex = -1;
            return this;
        }
    }

    /* access modifiers changed from: private */
    public void setEndButton(Button button) {
        this.mEndButton.setOnClickListener(null);
        removeView(this.mEndButton);
        this.mEndButton = button;
        button.setOnClickListener(this.hideOnClickListener);
        button.setLayoutParams((RelativeLayout.LayoutParams) this.mEndButton.getLayoutParams());
        addView(button);
    }

    /* access modifiers changed from: private */
    public void setShowcaseDrawer(ShowcaseDrawer showcaseDrawer2) {
        this.showcaseDrawer = showcaseDrawer2;
        this.showcaseDrawer.setBackgroundColour(this.backgroundColor);
        this.showcaseDrawer.setShowcaseColour(this.showcaseColor);
        this.hasAlteredText = true;
        invalidate();
    }

    /* access modifiers changed from: private */
    public void setContentTitlePaint(TextPaint textPaint) {
        this.textDrawer.setTitlePaint(textPaint);
        this.hasAlteredText = true;
        invalidate();
    }

    /* access modifiers changed from: private */
    public void setContentTextPaint(TextPaint paint) {
        this.textDrawer.setContentPaint(paint);
        this.hasAlteredText = true;
        invalidate();
    }

    public void setShouldCentreText(boolean shouldCentreText2) {
        this.shouldCentreText = shouldCentreText2;
        this.hasAlteredText = true;
        invalidate();
    }

    /* access modifiers changed from: private */
    public void setSingleShot(long shotId) {
        this.shotStateStore.setSingleShot(shotId);
    }

    public void setButtonPosition(RelativeLayout.LayoutParams layoutParams) {
        this.mEndButton.setLayoutParams(layoutParams);
    }

    public void setDetailTextAlignment(Layout.Alignment textAlignment) {
        this.textDrawer.setDetailTextAlignment(textAlignment);
        this.hasAlteredText = true;
        invalidate();
    }

    public void setTitleTextAlignment(Layout.Alignment textAlignment) {
        this.textDrawer.setTitleTextAlignment(textAlignment);
        this.hasAlteredText = true;
        invalidate();
    }

    private void setFadeDurations(long fadeInMillis2, long fadeOutMillis2) {
        this.fadeInMillis = fadeInMillis2;
        this.fadeOutMillis = fadeOutMillis2;
    }

    public void forceTextPosition(int textPosition) {
        this.textDrawer.forceTextPosition(textPosition);
        this.hasAlteredText = true;
        invalidate();
    }

    public void setHideOnTouchOutside(boolean hideOnTouch2) {
        this.hideOnTouch = hideOnTouch2;
    }

    public void setBlocksTouches(boolean blockTouches2) {
        this.blockTouches = blockTouches2;
    }

    /* access modifiers changed from: private */
    public void setBlockAllTouches(boolean blockAllTouches2) {
        this.blockAllTouches = blockAllTouches2;
    }

    public void setStyle(int theme) {
        updateStyle(getContext().obtainStyledAttributes(theme, R.styleable.ShowcaseView), true);
    }

    public boolean isShowing() {
        return this.isShowing;
    }

    private void updateStyle(TypedArray styled, boolean invalidate) {
        this.backgroundColor = styled.getColor(R.styleable.ShowcaseView_sv_backgroundColor, Color.argb(128, 80, 80, 80));
        this.showcaseColor = styled.getColor(R.styleable.ShowcaseView_sv_showcaseColor, HOLO_BLUE);
        String buttonText = styled.getString(R.styleable.ShowcaseView_sv_buttonText);
        if (TextUtils.isEmpty(buttonText)) {
            buttonText = getResources().getString(17039370);
        }
        boolean tintButton = styled.getBoolean(R.styleable.ShowcaseView_sv_tintButtonColor, true);
        int titleTextAppearance = styled.getResourceId(R.styleable.ShowcaseView_sv_titleTextAppearance, R.style.TextAppearance_ShowcaseView_Title);
        int detailTextAppearance = styled.getResourceId(R.styleable.ShowcaseView_sv_detailTextAppearance, R.style.TextAppearance_ShowcaseView_Detail);
        styled.recycle();
        this.showcaseDrawer.setShowcaseColour(this.showcaseColor);
        this.showcaseDrawer.setBackgroundColour(this.backgroundColor);
        tintButton(this.showcaseColor, tintButton);
        this.mEndButton.setText(buttonText);
        this.textDrawer.setTitleStyling(titleTextAppearance);
        this.textDrawer.setDetailStyling(detailTextAppearance);
        this.hasAlteredText = true;
        if (invalidate) {
            invalidate();
        }
    }

    private void tintButton(int showcaseColor2, boolean tintButton) {
        if (tintButton) {
            this.mEndButton.getBackground().setColorFilter(showcaseColor2, PorterDuff.Mode.MULTIPLY);
        } else {
            this.mEndButton.getBackground().setColorFilter(HOLO_BLUE, PorterDuff.Mode.MULTIPLY);
        }
    }
}
