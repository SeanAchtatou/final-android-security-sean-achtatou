package com.github.amlcurran.showcaseview;

import android.graphics.Point;
import android.view.View;
import com.github.amlcurran.showcaseview.AnimationFactory;

class NoAnimationFactory implements AnimationFactory {
    NoAnimationFactory() {
    }

    public void fadeInView(View target, long duration, AnimationFactory.AnimationStartListener listener) {
        listener.onAnimationStart();
    }

    public void fadeOutView(View target, long duration, AnimationFactory.AnimationEndListener listener) {
        listener.onAnimationEnd();
    }

    public void animateTargetToPoint(ShowcaseView showcaseView, Point point) {
        showcaseView.setShowcasePosition(point.x, point.y);
    }
}
