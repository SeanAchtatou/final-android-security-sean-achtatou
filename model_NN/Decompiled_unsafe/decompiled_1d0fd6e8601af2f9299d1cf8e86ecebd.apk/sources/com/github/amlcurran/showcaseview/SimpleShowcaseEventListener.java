package com.github.amlcurran.showcaseview;

import android.view.MotionEvent;

public class SimpleShowcaseEventListener implements OnShowcaseEventListener {
    public void onShowcaseViewHide(ShowcaseView showcaseView) {
    }

    public void onShowcaseViewDidHide(ShowcaseView showcaseView) {
    }

    public void onShowcaseViewShow(ShowcaseView showcaseView) {
    }

    public void onShowcaseViewTouchBlocked(MotionEvent motionEvent) {
    }
}
