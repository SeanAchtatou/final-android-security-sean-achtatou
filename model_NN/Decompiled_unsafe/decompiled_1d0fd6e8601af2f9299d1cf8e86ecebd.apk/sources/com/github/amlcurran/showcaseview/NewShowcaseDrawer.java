package com.github.amlcurran.showcaseview;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;

class NewShowcaseDrawer extends StandardShowcaseDrawer {
    private static final int ALPHA_60_PERCENT = 153;
    private final float innerRadius;
    private final float outerRadius;

    public NewShowcaseDrawer(Resources resources, Resources.Theme theme) {
        super(resources, theme);
        this.outerRadius = resources.getDimension(R.dimen.showcase_radius_outer);
        this.innerRadius = resources.getDimension(R.dimen.showcase_radius_inner);
    }

    public void setShowcaseColour(int color) {
        this.eraserPaint.setColor(color);
    }

    public void drawShowcase(Bitmap buffer, float x, float y, float scaleMultiplier) {
        Canvas bufferCanvas = new Canvas(buffer);
        this.eraserPaint.setAlpha(ALPHA_60_PERCENT);
        bufferCanvas.drawCircle(x, y, this.outerRadius, this.eraserPaint);
        this.eraserPaint.setAlpha(0);
        bufferCanvas.drawCircle(x, y, this.innerRadius, this.eraserPaint);
    }

    public int getShowcaseWidth() {
        return (int) (this.outerRadius * 2.0f);
    }

    public int getShowcaseHeight() {
        return (int) (this.outerRadius * 2.0f);
    }

    public float getBlockedRadius() {
        return this.innerRadius;
    }
}
