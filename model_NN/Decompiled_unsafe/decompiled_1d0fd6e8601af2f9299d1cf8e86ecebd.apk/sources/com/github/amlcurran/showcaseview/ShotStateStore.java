package com.github.amlcurran.showcaseview;

import android.content.Context;

class ShotStateStore {
    private static final int INVALID_SHOT_ID = -1;
    private static final String PREFS_SHOWCASE_INTERNAL = "showcase_internal";
    private final Context context;
    long shotId = -1;

    public ShotStateStore(Context context2) {
        this.context = context2;
    }

    /* access modifiers changed from: package-private */
    public boolean hasShot() {
        return isSingleShot() && this.context.getSharedPreferences(PREFS_SHOWCASE_INTERNAL, 0).getBoolean(new StringBuilder().append("hasShot").append(this.shotId).toString(), false);
    }

    /* access modifiers changed from: package-private */
    public boolean isSingleShot() {
        return this.shotId != -1;
    }

    /* access modifiers changed from: package-private */
    public void storeShot() {
        if (isSingleShot()) {
            this.context.getSharedPreferences(PREFS_SHOWCASE_INTERNAL, 0).edit().putBoolean("hasShot" + this.shotId, true).apply();
        }
    }

    /* access modifiers changed from: package-private */
    public void setSingleShot(long shotId2) {
        this.shotId = shotId2;
    }
}
