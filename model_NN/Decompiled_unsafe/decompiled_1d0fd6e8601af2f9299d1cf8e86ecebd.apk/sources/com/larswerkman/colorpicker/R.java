package com.larswerkman.colorpicker;

public final class R {

    public static final class attr {
        public static final int pointer_size = 2130772154;
        public static final int wheel_size = 2130772153;
    }

    public static final class styleable {
        public static final int[] ColorPicker = {yahoo.mail.app.R.attr.wheel_size, yahoo.mail.app.R.attr.pointer_size};
        public static final int ColorPicker_pointer_size = 1;
        public static final int ColorPicker_wheel_size = 0;
    }
}
