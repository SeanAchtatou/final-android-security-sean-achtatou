package com.larswerkman.colorpicker;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.SweepGradient;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.internal.view.SupportMenu;
import android.support.v4.view.InputDeviceCompat;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class ColorPicker extends View {
    private static final int[] COLORS = {SupportMenu.CATEGORY_MASK, -65281, -16776961, -16711681, -16711936, InputDeviceCompat.SOURCE_ANY, SupportMenu.CATEGORY_MASK};
    private static final String STATE_ANGLE = "angle";
    private static final String STATE_PARENT = "parent";
    private float mAngle;
    private Paint mColorWheelPaint;
    private float mColorWheelRadius;
    private RectF mColorWheelRectangle = new RectF();
    private int mColorWheelStrokeWidth;
    private Paint mPointerColor;
    private Paint mPointerHaloPaint;
    private int mPointerRadius;
    private float mTranslationOffset;
    private boolean mUserIsMovingPointer = false;

    public static int getRandomColor() {
        return calculateColor((float) (Math.random() * 2.0d * 3.141592653589793d));
    }

    private static int ave(int s, int d, float p) {
        return Math.round(((float) (d - s)) * p) + s;
    }

    private static int calculateColor(float angle) {
        float unit = (float) (((double) angle) / 6.283185307179586d);
        if (unit < 0.0f) {
            unit += 1.0f;
        }
        if (unit <= 0.0f) {
            return COLORS[0];
        }
        if (unit >= 1.0f) {
            return COLORS[COLORS.length - 1];
        }
        float p = unit * ((float) (COLORS.length - 1));
        int i = (int) p;
        float p2 = p - ((float) i);
        int c0 = COLORS[i];
        int c1 = COLORS[i + 1];
        return Color.argb(ave(Color.alpha(c0), Color.alpha(c1), p2), ave(Color.red(c0), Color.red(c1), p2), ave(Color.green(c0), Color.green(c1), p2), ave(Color.blue(c0), Color.blue(c1), p2));
    }

    public ColorPicker(Context context) {
        super(context);
        init(null, 0);
    }

    public ColorPicker(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public ColorPicker(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.SweepGradient.<init>(float, float, int[], float[]):void}
     arg types: [int, int, int[], ?[OBJECT, ARRAY]]
     candidates:
      ClspMth{android.graphics.SweepGradient.<init>(float, float, long, long):void}
      ClspMth{android.graphics.SweepGradient.<init>(float, float, int, int):void}
      ClspMth{android.graphics.SweepGradient.<init>(float, float, long[], float[]):void}
      ClspMth{android.graphics.SweepGradient.<init>(float, float, int[], float[]):void} */
    private void init(AttributeSet attrs, int defStyle) {
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.ColorPicker, defStyle, 0);
        this.mColorWheelStrokeWidth = a.getInteger(R.styleable.ColorPicker_wheel_size, 16);
        this.mPointerRadius = a.getInteger(R.styleable.ColorPicker_pointer_size, 48);
        a.recycle();
        Shader s = new SweepGradient(0.0f, 0.0f, COLORS, (float[]) null);
        this.mColorWheelPaint = new Paint(1);
        this.mColorWheelPaint.setShader(s);
        this.mColorWheelPaint.setStyle(Paint.Style.STROKE);
        this.mColorWheelPaint.setStrokeWidth((float) this.mColorWheelStrokeWidth);
        this.mPointerHaloPaint = new Paint(1);
        this.mPointerHaloPaint.setColor((int) ViewCompat.MEASURED_STATE_MASK);
        this.mPointerHaloPaint.setStrokeWidth(5.0f);
        this.mPointerHaloPaint.setAlpha(96);
        this.mPointerColor = new Paint(1);
        this.mPointerColor.setStrokeWidth(5.0f);
        this.mAngle = -1.5707964f;
        this.mPointerColor.setColor(calculateColor(this.mAngle));
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        canvas.translate(this.mTranslationOffset, this.mTranslationOffset);
        canvas.drawOval(this.mColorWheelRectangle, this.mColorWheelPaint);
        float[] pointerPosition = calculatePointerPosition(this.mAngle);
        canvas.drawCircle(pointerPosition[0], pointerPosition[1], (float) this.mPointerRadius, this.mPointerHaloPaint);
        canvas.drawCircle(pointerPosition[0], pointerPosition[1], (float) (((double) this.mPointerRadius) / 1.2d), this.mPointerColor);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int min = Math.min(getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec), getDefaultSize(getSuggestedMinimumHeight(), heightMeasureSpec));
        setMeasuredDimension(min, min);
        this.mTranslationOffset = ((float) min) * 0.5f;
        this.mColorWheelRadius = this.mTranslationOffset - ((float) this.mPointerRadius);
        this.mColorWheelRectangle.set(-this.mColorWheelRadius, -this.mColorWheelRadius, this.mColorWheelRadius, this.mColorWheelRadius);
    }

    public int getColor() {
        return calculateColor(this.mAngle);
    }

    public void setColor(int color) {
        this.mAngle = colorToAngle(color);
        this.mPointerColor.setColor(calculateColor(this.mAngle));
        invalidate();
    }

    private float colorToAngle(int color) {
        int index;
        double value;
        int[] colorInfo = normalizeColor(color);
        int normColor = colorInfo[0];
        int colorMask = colorInfo[1];
        int shiftValue = colorInfo[2];
        int anchorColor = normColor & (colorMask ^ -1);
        for (int i = 0; i < COLORS.length - 1; i++) {
            if (COLORS[i] == anchorColor) {
                int nextValue = COLORS[i + 1];
                double decimals = ((double) ((normColor >> shiftValue) & 255)) / 255.0d;
                if ((nextValue & colorMask) == (anchorColor & colorMask)) {
                    if (i == 0) {
                        index = COLORS.length - 1;
                    } else {
                        index = i;
                    }
                    if (COLORS[index - 1] < anchorColor) {
                        value = ((double) (index - 1)) + decimals;
                    } else {
                        value = ((double) index) - decimals;
                    }
                } else if (nextValue < anchorColor) {
                    value = ((double) (i + 1)) - decimals;
                } else {
                    value = ((double) i) + decimals;
                }
                float angle = (float) ((6.283185307179586d * value) / ((double) (COLORS.length - 1)));
                if (((double) angle) > 3.141592653589793d) {
                    return (float) (((double) angle) - 6.283185307179586d);
                }
                return angle;
            }
        }
        return 0.0f;
    }

    private int[] normalizeColor(int color) {
        int shiftValue;
        int red = Color.red(color);
        int green = Color.green(color);
        int blue = Color.blue(color);
        int newRed = red;
        int newGreen = green;
        int newBlue = blue;
        int maskRed = 0;
        int maskGreen = 0;
        int maskBlue = 0;
        if (red < green && red < blue) {
            newRed = 0;
            if (green > blue) {
                shiftValue = 0;
                maskBlue = 255;
                newGreen = 255;
            } else {
                shiftValue = 8;
                maskGreen = 255;
                newBlue = 255;
            }
        } else if (green >= red || green >= blue) {
            newBlue = 0;
            if (red > green) {
                shiftValue = 8;
                maskGreen = 255;
                newRed = 255;
            } else {
                shiftValue = 16;
                maskRed = 255;
                newGreen = 255;
            }
        } else {
            newGreen = 0;
            if (red > blue) {
                shiftValue = 0;
                maskBlue = 255;
                newRed = 255;
            } else {
                shiftValue = 16;
                maskRed = 255;
                newBlue = 255;
            }
        }
        return new int[]{Color.argb(255, newRed, newGreen, newBlue), Color.argb(0, maskRed, maskGreen, maskBlue), shiftValue};
    }

    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX() - this.mTranslationOffset;
        float y = event.getY() - this.mTranslationOffset;
        switch (event.getAction()) {
            case 0:
                float[] pointerPosition = calculatePointerPosition(this.mAngle);
                if (x >= pointerPosition[0] - 48.0f && x <= pointerPosition[0] + 48.0f && y >= pointerPosition[1] - 48.0f && y <= pointerPosition[1] + 48.0f) {
                    this.mUserIsMovingPointer = true;
                    invalidate();
                    break;
                }
            case 1:
                this.mUserIsMovingPointer = false;
                break;
            case 2:
                if (this.mUserIsMovingPointer) {
                    this.mAngle = (float) Math.atan2((double) y, (double) x);
                    this.mPointerColor.setColor(calculateColor(this.mAngle));
                    invalidate();
                    break;
                }
                break;
        }
        return true;
    }

    private float[] calculatePointerPosition(float angle) {
        return new float[]{(float) (((double) this.mColorWheelRadius) * Math.cos((double) angle)), (float) (((double) this.mColorWheelRadius) * Math.sin((double) angle))};
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        Bundle state = new Bundle();
        state.putParcelable("parent", superState);
        state.putFloat(STATE_ANGLE, this.mAngle);
        return state;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable state) {
        Bundle savedState = (Bundle) state;
        super.onRestoreInstanceState(savedState.getParcelable("parent"));
        this.mAngle = savedState.getFloat(STATE_ANGLE);
        this.mPointerColor.setColor(calculateColor(this.mAngle));
    }
}
