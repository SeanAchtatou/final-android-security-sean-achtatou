package com.tokenautocomplete;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.text.style.ReplacementSpan;
import android.view.View;
import android.view.ViewGroup;

public class ViewSpan extends ReplacementSpan {
    private int maxWidth;
    protected View view;

    public ViewSpan(View v, int maxWidth2) {
        this.maxWidth = maxWidth2;
        this.view = v;
        this.view.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
    }

    private void prepView() {
        this.view.measure(View.MeasureSpec.makeMeasureSpec(this.maxWidth, Integer.MIN_VALUE), View.MeasureSpec.makeMeasureSpec(0, 0));
        this.view.layout(0, 0, this.view.getMeasuredWidth(), this.view.getMeasuredHeight());
    }

    public void draw(Canvas canvas, CharSequence text, int start, int end, float x, int top, int y, int bottom, Paint paint) {
        prepView();
        canvas.save();
        canvas.translate(x, (float) ((bottom - this.view.getBottom()) - (((bottom - top) - this.view.getBottom()) / 2)));
        this.view.draw(canvas);
        canvas.restore();
    }

    public int getSize(Paint paint, CharSequence charSequence, int i, int i2, Paint.FontMetricsInt fm) {
        int need;
        prepView();
        if (fm != null && (need = this.view.getMeasuredHeight() - (fm.descent - fm.ascent)) > 0) {
            int ascent = need / 2;
            fm.descent += need - ascent;
            fm.ascent -= ascent;
            fm.bottom += need - ascent;
            fm.top -= need / 2;
        }
        return this.view.getRight();
    }
}
