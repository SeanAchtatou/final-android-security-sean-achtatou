package com.tokenautocomplete;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Layout;
import android.text.Selection;
import android.text.SpanWatcher;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.QwertyKeyListener;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.ExtractedText;
import android.view.inputmethod.ExtractedTextRequest;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputConnectionWrapper;
import android.view.inputmethod.InputMethodManager;
import android.widget.Filter;
import android.widget.MultiAutoCompleteTextView;
import android.widget.TextView;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public abstract class TokenCompleteTextView<T> extends MultiAutoCompleteTextView implements TextView.OnEditorActionListener {
    static final /* synthetic */ boolean $assertionsDisabled = (!TokenCompleteTextView.class.desiredAssertionStatus());
    public static final String TAG = "TokenAutoComplete";
    private boolean allowCollapse = true;
    /* access modifiers changed from: private */
    public boolean allowDuplicates = true;
    private TokenDeleteStyle deletionStyle = TokenDeleteStyle._Parent;
    /* access modifiers changed from: private */
    public boolean focusChanging = false;
    /* access modifiers changed from: private */
    public List<TokenCompleteTextView<T>.TokenImageSpan> hiddenSpans;
    private boolean hintVisible = false;
    boolean inInvalidate = false;
    private boolean initialized = false;
    private Layout lastLayout = null;
    /* access modifiers changed from: private */
    public TokenListener<T> listener;
    /* access modifiers changed from: private */
    public ArrayList<T> objects;
    private boolean performBestGuess = true;
    /* access modifiers changed from: private */
    public CharSequence prefix = "";
    /* access modifiers changed from: private */
    public boolean savingState = false;
    private T selectedObject;
    private boolean shouldFocusNext = false;
    /* access modifiers changed from: private */
    public TokenCompleteTextView<T>.TokenSpanWatcher spanWatcher;
    private char[] splitChar = {',', ';'};
    private TokenCompleteTextView<T>.TokenTextWatcher textWatcher;
    /* access modifiers changed from: private */
    public TokenClickStyle tokenClickStyle = TokenClickStyle.None;
    /* access modifiers changed from: private */
    public int tokenLimit = -1;
    private MultiAutoCompleteTextView.Tokenizer tokenizer;

    public enum TokenDeleteStyle {
        _Parent,
        Clear,
        PartialCompletion,
        ToString
    }

    public interface TokenListener<T> {
        void onTokenAdded(T t);

        void onTokenRemoved(T t);
    }

    /* access modifiers changed from: protected */
    public abstract T defaultObject(String str);

    /* access modifiers changed from: protected */
    public abstract View getViewForObject(Object obj);

    public enum TokenClickStyle {
        None(false),
        Delete(false),
        Select(true),
        SelectDeselect(true);
        
        private boolean mIsSelectable = false;

        private TokenClickStyle(boolean selectable) {
            this.mIsSelectable = selectable;
        }

        public boolean isSelectable() {
            return this.mIsSelectable;
        }
    }

    /* access modifiers changed from: protected */
    public void addListeners() {
        Editable text = getText();
        if (text != null) {
            text.setSpan(this.spanWatcher, 0, text.length(), 18);
            addTextChangedListener(this.textWatcher);
        }
    }

    /* access modifiers changed from: protected */
    public void removeListeners() {
        Editable text = getText();
        if (text != null) {
            for (TokenCompleteTextView<T>.TokenSpanWatcher watcher : (TokenSpanWatcher[]) text.getSpans(0, text.length(), TokenSpanWatcher.class)) {
                text.removeSpan(watcher);
            }
            removeTextChangedListener(this.textWatcher);
        }
    }

    private void init() {
        if (!this.initialized) {
            setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
            this.objects = new ArrayList<>();
            Editable text = getText();
            if ($assertionsDisabled || text != null) {
                this.spanWatcher = new TokenSpanWatcher();
                this.textWatcher = new TokenTextWatcher();
                this.hiddenSpans = new ArrayList();
                addListeners();
                setTextIsSelectable(false);
                setLongClickable(false);
                setInputType(getInputType() | 524288 | 65536);
                setHorizontallyScrolling(false);
                setOnEditorActionListener(this);
                setFilters(new InputFilter[]{new InputFilter() {
                    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                        if (TokenCompleteTextView.this.tokenLimit != -1 && TokenCompleteTextView.this.objects.size() == TokenCompleteTextView.this.tokenLimit) {
                            return "";
                        }
                        if (source.length() == 1 && TokenCompleteTextView.this.isSplitChar(source.charAt(0))) {
                            TokenCompleteTextView.this.performCompletion();
                            return "";
                        } else if (dstart >= TokenCompleteTextView.this.prefix.length()) {
                            return null;
                        } else {
                            if (dstart == 0 && dend == 0) {
                                return null;
                            }
                            if (dend <= TokenCompleteTextView.this.prefix.length()) {
                                return TokenCompleteTextView.this.prefix.subSequence(dstart, dend);
                            }
                            return TokenCompleteTextView.this.prefix.subSequence(dstart, TokenCompleteTextView.this.prefix.length());
                        }
                    }
                }});
                setDeletionStyle(TokenDeleteStyle.Clear);
                this.initialized = true;
                return;
            }
            throw new AssertionError();
        }
    }

    public TokenCompleteTextView(Context context) {
        super(context);
        init();
    }

    public TokenCompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TokenCompleteTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    /* access modifiers changed from: protected */
    public void performFiltering(@NonNull CharSequence text, int start, int end, int keyCode) {
        if (start < this.prefix.length()) {
            start = this.prefix.length();
        }
        Filter filter = getFilter();
        if (filter == null) {
            return;
        }
        if (this.hintVisible) {
            filter.filter("");
        } else {
            filter.filter(text.subSequence(start, end), this);
        }
    }

    public void setTokenizer(MultiAutoCompleteTextView.Tokenizer t) {
        super.setTokenizer(t);
        this.tokenizer = t;
    }

    public void setDeletionStyle(TokenDeleteStyle dStyle) {
        this.deletionStyle = dStyle;
    }

    public void setTokenClickStyle(TokenClickStyle cStyle) {
        this.tokenClickStyle = cStyle;
    }

    public void setTokenListener(TokenListener tokenListener) {
        this.listener = tokenListener;
    }

    public boolean isTokenRemovable(T t) {
        return true;
    }

    public void setPrefix(CharSequence p) {
        this.prefix = "";
        Editable text = getText();
        if (text != null) {
            text.insert(0, p);
        }
        this.prefix = p;
        updateHint();
    }

    public List<T> getObjects() {
        return this.objects;
    }

    public void setSplitChar(char[] splitChar2) {
        char[] fixed = splitChar2;
        if (splitChar2[0] == ' ') {
            fixed = new char[(splitChar2.length + 1)];
            fixed[0] = 167;
            System.arraycopy(splitChar2, 0, fixed, 1, splitChar2.length);
        }
        this.splitChar = fixed;
        setTokenizer(new CharacterTokenizer(splitChar2));
    }

    public void setSplitChar(char splitChar2) {
        setSplitChar(new char[]{splitChar2});
    }

    /* access modifiers changed from: private */
    public boolean isSplitChar(char c) {
        for (char split : this.splitChar) {
            if (c == split) {
                return true;
            }
        }
        return false;
    }

    public void allowDuplicates(boolean allow) {
        this.allowDuplicates = allow;
    }

    public void performBestGuess(boolean guess) {
        this.performBestGuess = guess;
    }

    public void allowCollapse(boolean allowCollapse2) {
        this.allowCollapse = allowCollapse2;
    }

    public void setTokenLimit(int tokenLimit2) {
        this.tokenLimit = tokenLimit2;
    }

    public CharSequence getTextForAccessibility() {
        if (getObjects().size() == 0) {
            return getText();
        }
        SpannableStringBuilder description = new SpannableStringBuilder();
        Editable text = getText();
        int selectionStart = -1;
        int selectionEnd = -1;
        int i = 0;
        while (i < text.length()) {
            if (i == Selection.getSelectionStart(text)) {
                selectionStart = description.length();
            }
            if (i == Selection.getSelectionEnd(text)) {
                selectionEnd = description.length();
            }
            TokenCompleteTextView<T>.TokenImageSpan[] tokens = (TokenImageSpan[]) text.getSpans(i, i, TokenImageSpan.class);
            if (tokens.length > 0) {
                TokenCompleteTextView<T>.TokenImageSpan token = tokens[0];
                description = description.append(this.tokenizer.terminateToken(token.getToken().toString()));
                i = text.getSpanEnd(token);
            } else {
                description = description.append(text.subSequence(i, i + 1));
            }
            i++;
        }
        if (i == Selection.getSelectionStart(text)) {
            selectionStart = description.length();
        }
        if (i == Selection.getSelectionEnd(text)) {
            selectionEnd = description.length();
        }
        if (selectionStart < 0 || selectionEnd < 0) {
            return description;
        }
        Selection.setSelection(description, selectionStart, selectionEnd);
        return description;
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent event) {
        super.onInitializeAccessibilityEvent(event);
        if (event.getEventType() == 8192) {
            CharSequence text = getTextForAccessibility();
            event.setFromIndex(Selection.getSelectionStart(text));
            event.setToIndex(Selection.getSelectionEnd(text));
            event.setItemCount(text.length());
        }
    }

    private int getCorrectedTokenEnd() {
        return this.tokenizer.findTokenEnd(getText(), getSelectionEnd());
    }

    private int getCorrectedTokenBeginning(int end) {
        int start = this.tokenizer.findTokenStart(getText(), end);
        if (start < this.prefix.length()) {
            return this.prefix.length();
        }
        return start;
    }

    /* access modifiers changed from: protected */
    public String currentCompletionText() {
        if (this.hintVisible) {
            return "";
        }
        Editable editable = getText();
        int end = getCorrectedTokenEnd();
        return TextUtils.substring(editable, getCorrectedTokenBeginning(end), end);
    }

    /* access modifiers changed from: protected */
    public float maxTextWidth() {
        return (float) ((getWidth() - getPaddingLeft()) - getPaddingRight());
    }

    @TargetApi(16)
    private void api16Invalidate() {
        if (this.initialized && !this.inInvalidate) {
            this.inInvalidate = true;
            setShadowLayer(getShadowRadius(), getShadowDx(), getShadowDy(), getShadowColor());
            this.inInvalidate = false;
        }
    }

    public void invalidate() {
        if (Build.VERSION.SDK_INT >= 16) {
            api16Invalidate();
        }
        super.invalidate();
    }

    public boolean enoughToFilter() {
        boolean z = true;
        if (this.tokenizer == null || this.hintVisible || getSelectionEnd() < 0) {
            return false;
        }
        int end = getCorrectedTokenEnd();
        if (end - getCorrectedTokenBeginning(end) < Math.max(getThreshold(), 1)) {
            z = false;
        }
        return z;
    }

    public void performCompletion() {
        Object bestGuess;
        if ((getAdapter() == null || getListSelection() == -1) && enoughToFilter()) {
            if (getAdapter() == null || getAdapter().getCount() <= 0 || !this.performBestGuess) {
                bestGuess = defaultObject(currentCompletionText());
            } else {
                bestGuess = getAdapter().getItem(0);
            }
            replaceText(convertSelectionToString(bestGuess));
            return;
        }
        super.performCompletion();
    }

    public InputConnection onCreateInputConnection(@NonNull EditorInfo outAttrs) {
        InputConnection superConn = super.onCreateInputConnection(outAttrs);
        if (superConn == null) {
            return null;
        }
        TokenCompleteTextView<T>.TokenInputConnection conn = new TokenInputConnection(superConn, true);
        outAttrs.imeOptions &= -1073741825;
        outAttrs.imeOptions |= 268435456;
        return conn;
    }

    private void handleDone() {
        performCompletion();
        ((InputMethodManager) getContext().getSystemService("input_method")).hideSoftInputFromWindow(getWindowToken(), 0);
    }

    public boolean onKeyUp(int keyCode, @NonNull KeyEvent event) {
        boolean handled = super.onKeyUp(keyCode, event);
        if (this.shouldFocusNext) {
            this.shouldFocusNext = false;
            handleDone();
        }
        return handled;
    }

    public boolean onKeyDown(int keyCode, @NonNull KeyEvent event) {
        boolean handled = false;
        switch (keyCode) {
            case 23:
            case 61:
            case 66:
                if (event.hasNoModifiers()) {
                    this.shouldFocusNext = true;
                    handled = true;
                    break;
                }
                break;
            case 67:
                if (canDeleteSelection(1) && !deleteSelectedObject(false)) {
                    handled = false;
                    break;
                } else {
                    handled = true;
                    break;
                }
        }
        if (handled || super.onKeyDown(keyCode, event)) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: private */
    public boolean deleteSelectedObject(boolean handled) {
        int i = 0;
        if (this.tokenClickStyle != null && this.tokenClickStyle.isSelectable()) {
            Editable text = getText();
            if (text != null) {
                TokenCompleteTextView<T>.TokenImageSpan[] spans = (TokenImageSpan[]) text.getSpans(0, text.length(), TokenImageSpan.class);
                int length = spans.length;
                while (true) {
                    if (i >= length) {
                        break;
                    }
                    TokenCompleteTextView<T>.TokenImageSpan span = spans[i];
                    if (span.view.isSelected()) {
                        removeSpan(span);
                        handled = true;
                        break;
                    }
                    i++;
                }
            } else {
                return handled;
            }
        }
        return handled;
    }

    public boolean onEditorAction(TextView view, int action, KeyEvent keyEvent) {
        if (action != 6) {
            return false;
        }
        handleDone();
        return true;
    }

    public boolean onTouchEvent(@NonNull MotionEvent event) {
        int offset;
        int action = event.getActionMasked();
        Editable text = getText();
        boolean handled = false;
        if (this.tokenClickStyle == TokenClickStyle.None) {
            handled = super.onTouchEvent(event);
        }
        if (!(!isFocused() || text == null || this.lastLayout == null || action != 1 || (offset = getOffsetForPosition(event.getX(), event.getY())) == -1)) {
            TokenCompleteTextView<T>.TokenImageSpan[] links = (TokenImageSpan[]) text.getSpans(offset, offset, TokenImageSpan.class);
            if (links.length > 0) {
                links[0].onClick();
                handled = true;
            } else {
                clearSelections();
            }
        }
        if (handled || this.tokenClickStyle == TokenClickStyle.None) {
            return handled;
        }
        return super.onTouchEvent(event);
    }

    /* access modifiers changed from: protected */
    public void onSelectionChanged(int selStart, int selEnd) {
        if (this.hintVisible) {
            selStart = 0;
        }
        int selEnd2 = selStart;
        if (!(this.tokenClickStyle == null || !this.tokenClickStyle.isSelectable() || getText() == null)) {
            clearSelections();
        }
        if (this.prefix == null || (selStart >= this.prefix.length() && selEnd2 >= this.prefix.length())) {
            Editable text = getText();
            if (text != null) {
                TokenCompleteTextView<T>.TokenImageSpan[] spans = (TokenImageSpan[]) text.getSpans(selStart, selEnd2, TokenImageSpan.class);
                int length = spans.length;
                int i = 0;
                while (i < length) {
                    TokenCompleteTextView<T>.TokenImageSpan span = spans[i];
                    int spanEnd = text.getSpanEnd(span);
                    if (selStart > spanEnd || text.getSpanStart(span) >= selStart) {
                        i++;
                    } else if (spanEnd == text.length()) {
                        setSelection(spanEnd);
                        return;
                    } else {
                        setSelection(spanEnd + 1);
                        return;
                    }
                }
            }
            super.onSelectionChanged(selStart, selEnd2);
            return;
        }
        setSelection(this.prefix.length());
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        this.lastLayout = getLayout();
    }

    public void performCollapse(boolean hasFocus) {
        this.focusChanging = true;
        if (!hasFocus) {
            Editable text = getText();
            if (!(text == null || this.lastLayout == null)) {
                int lastPosition = this.lastLayout.getLineVisibleEnd(0);
                TokenCompleteTextView<T>.TokenImageSpan[] tokens = (TokenImageSpan[]) text.getSpans(0, lastPosition, TokenImageSpan.class);
                int count = this.objects.size() - tokens.length;
                CountSpan[] countSpans = (CountSpan[]) text.getSpans(0, lastPosition, CountSpan.class);
                if (count > 0 && countSpans.length == 0) {
                    int lastPosition2 = lastPosition + 1;
                    CountSpan cs = new CountSpan(count, getContext(), getCurrentTextColor(), (int) getTextSize(), (int) maxTextWidth());
                    text.insert(lastPosition2, cs.text);
                    if (Layout.getDesiredWidth(text, 0, cs.text.length() + lastPosition2, this.lastLayout.getPaint()) > maxTextWidth()) {
                        text.delete(lastPosition2, cs.text.length() + lastPosition2);
                        if (tokens.length > 0) {
                            lastPosition2 = text.getSpanStart(tokens[tokens.length - 1]);
                            cs.setCount(count + 1);
                        } else {
                            lastPosition2 = this.prefix.length();
                        }
                        text.insert(lastPosition2, cs.text);
                    }
                    text.setSpan(cs, lastPosition2, cs.text.length() + lastPosition2, 33);
                    this.hiddenSpans = new ArrayList(Arrays.asList((TokenImageSpan[]) text.getSpans(cs.text.length() + lastPosition2, text.length(), TokenImageSpan.class)));
                    for (TokenCompleteTextView<T>.TokenImageSpan span : this.hiddenSpans) {
                        removeSpan(span);
                    }
                }
            }
        } else {
            final Editable text2 = getText();
            if (text2 != null) {
                for (CountSpan count2 : (CountSpan[]) text2.getSpans(0, text2.length(), CountSpan.class)) {
                    text2.delete(text2.getSpanStart(count2), text2.getSpanEnd(count2));
                    text2.removeSpan(count2);
                }
                for (TokenCompleteTextView<T>.TokenImageSpan span2 : this.hiddenSpans) {
                    insertSpan(span2);
                }
                this.hiddenSpans.clear();
                if (this.hintVisible) {
                    setSelection(this.prefix.length());
                } else {
                    postDelayed(new Runnable() {
                        public void run() {
                            TokenCompleteTextView.this.setSelection(text2.length());
                        }
                    }, 10);
                }
                if (((TokenSpanWatcher[]) getText().getSpans(0, getText().length(), TokenSpanWatcher.class)).length == 0) {
                    text2.setSpan(this.spanWatcher, 0, text2.length(), 18);
                }
            }
        }
        this.focusChanging = false;
    }

    public void onFocusChanged(boolean hasFocus, int direction, Rect previous) {
        super.onFocusChanged(hasFocus, direction, previous);
        if (!hasFocus) {
            performCompletion();
        }
        if (this.allowCollapse) {
            performCollapse(hasFocus);
        }
    }

    /* access modifiers changed from: protected */
    public CharSequence convertSelectionToString(Object object) {
        this.selectedObject = object;
        switch (this.deletionStyle) {
            case Clear:
                return "";
            case PartialCompletion:
                return currentCompletionText();
            case ToString:
                return object != null ? object.toString() : "";
            default:
                return super.convertSelectionToString(object);
        }
    }

    private SpannableStringBuilder buildSpannableForText(CharSequence text) {
        return new SpannableStringBuilder(String.valueOf(this.splitChar[0]) + ((Object) this.tokenizer.terminateToken(text)));
    }

    /* access modifiers changed from: protected */
    public TokenCompleteTextView<T>.TokenImageSpan buildSpanForObject(Object obj) {
        if (obj == null) {
            return null;
        }
        return new TokenImageSpan(getViewForObject(obj), obj, (int) maxTextWidth());
    }

    /* access modifiers changed from: protected */
    public void replaceText(CharSequence text) {
        clearComposingText();
        if (this.selectedObject != null && !this.selectedObject.toString().equals("")) {
            SpannableStringBuilder ssb = buildSpannableForText(text);
            TokenCompleteTextView<T>.TokenImageSpan tokenSpan = buildSpanForObject(this.selectedObject);
            Editable editable = getText();
            int cursorPosition = getSelectionEnd();
            int end = cursorPosition;
            int start = cursorPosition;
            if (!this.hintVisible) {
                end = getCorrectedTokenEnd();
                start = getCorrectedTokenBeginning(end);
            }
            String original = TextUtils.substring(editable, start, end);
            if (editable == null) {
                return;
            }
            if (tokenSpan == null) {
                editable.replace(start, end, "");
            } else if (this.allowDuplicates || !this.objects.contains(tokenSpan.getToken())) {
                QwertyKeyListener.markAsReplaced(editable, start, end, original);
                editable.replace(start, end, ssb);
                editable.setSpan(tokenSpan, start, (ssb.length() + start) - 1, 33);
            } else {
                editable.replace(start, end, "");
            }
        }
    }

    public boolean extractText(@NonNull ExtractedTextRequest request, @NonNull ExtractedText outText) {
        try {
            return super.extractText(request, outText);
        } catch (IndexOutOfBoundsException ignored) {
            Log.d(TAG, "extractText hit IndexOutOfBoundsException. This may be normal.", ignored);
            return false;
        }
    }

    public void addObject(final T object, final CharSequence sourceText) {
        post(new Runnable() {
            public void run() {
                if (object != null) {
                    if (!TokenCompleteTextView.this.allowDuplicates && TokenCompleteTextView.this.objects.contains(object)) {
                        return;
                    }
                    if (TokenCompleteTextView.this.tokenLimit == -1 || TokenCompleteTextView.this.objects.size() != TokenCompleteTextView.this.tokenLimit) {
                        TokenCompleteTextView.this.insertSpan(object, sourceText);
                        if (TokenCompleteTextView.this.getText() != null && TokenCompleteTextView.this.isFocused()) {
                            TokenCompleteTextView.this.setSelection(TokenCompleteTextView.this.getText().length());
                        }
                    }
                }
            }
        });
    }

    public void addObject(T object) {
        addObject(object, "");
    }

    public void removeObject(final T object) {
        post(new Runnable() {
            public void run() {
                Editable text = TokenCompleteTextView.this.getText();
                if (text != null) {
                    ArrayList<TokenCompleteTextView<T>.TokenImageSpan> toRemove = new ArrayList<>();
                    for (TokenCompleteTextView<T>.TokenImageSpan span : TokenCompleteTextView.this.hiddenSpans) {
                        if (span.getToken().equals(object)) {
                            toRemove.add(span);
                        }
                    }
                    Iterator it = toRemove.iterator();
                    while (it.hasNext()) {
                        TokenCompleteTextView<T>.TokenImageSpan span2 = (TokenImageSpan) it.next();
                        TokenCompleteTextView.this.hiddenSpans.remove(span2);
                        TokenCompleteTextView.this.spanWatcher.onSpanRemoved(text, span2, 0, 0);
                    }
                    TokenCompleteTextView.this.updateCountSpan();
                    for (TokenCompleteTextView<T>.TokenImageSpan span3 : (TokenImageSpan[]) text.getSpans(0, text.length(), TokenImageSpan.class)) {
                        if (span3.getToken().equals(object)) {
                            TokenCompleteTextView.this.removeSpan(span3);
                        }
                    }
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void updateCountSpan() {
        Editable text = getText();
        CountSpan[] counts = (CountSpan[]) text.getSpans(0, text.length(), CountSpan.class);
        int newCount = this.hiddenSpans.size();
        for (CountSpan count : counts) {
            if (newCount == 0) {
                text.delete(text.getSpanStart(count), text.getSpanEnd(count));
                text.removeSpan(count);
            } else {
                count.setCount(this.hiddenSpans.size());
                text.setSpan(count, text.getSpanStart(count), text.getSpanEnd(count), 33);
            }
        }
    }

    /* access modifiers changed from: private */
    public void removeSpan(TokenCompleteTextView<T>.TokenImageSpan span) {
        Editable text = getText();
        if (text != null) {
            if (((TokenSpanWatcher[]) text.getSpans(0, text.length(), TokenSpanWatcher.class)).length == 0) {
                this.spanWatcher.onSpanRemoved(text, span, text.getSpanStart(span), text.getSpanEnd(span));
            }
            text.delete(text.getSpanStart(span), text.getSpanEnd(span) + 1);
            if (this.allowCollapse && !isFocused()) {
                updateCountSpan();
            }
        }
    }

    /* access modifiers changed from: private */
    public void insertSpan(T object, CharSequence sourceText) {
        SpannableStringBuilder ssb = buildSpannableForText(sourceText);
        TokenCompleteTextView<T>.TokenImageSpan tokenSpan = buildSpanForObject(object);
        Editable editable = getText();
        if (editable != null) {
            if (!this.allowCollapse || isFocused() || this.hiddenSpans.isEmpty()) {
                int offset = editable.length();
                if (this.hintVisible) {
                    offset = this.prefix.length();
                    editable.insert(offset, ssb);
                } else {
                    String completionText = currentCompletionText();
                    if (completionText != null && completionText.length() > 0) {
                        offset = TextUtils.indexOf(editable, completionText);
                    }
                    editable.insert(offset, ssb);
                }
                editable.setSpan(tokenSpan, offset, (ssb.length() + offset) - 1, 33);
                if (!isFocused() && this.allowCollapse) {
                    performCollapse(false);
                }
                if (!this.objects.contains(object)) {
                    this.spanWatcher.onSpanAdded(editable, tokenSpan, 0, 0);
                    return;
                }
                return;
            }
            this.hiddenSpans.add(tokenSpan);
            this.spanWatcher.onSpanAdded(editable, tokenSpan, 0, 0);
            updateCountSpan();
        }
    }

    private void insertSpan(T object) {
        String spanString;
        if (this.deletionStyle == TokenDeleteStyle.ToString) {
            spanString = object != null ? object.toString() : "";
        } else {
            spanString = "";
        }
        insertSpan(object, spanString);
    }

    private void insertSpan(TokenCompleteTextView<T>.TokenImageSpan span) {
        insertSpan(span.getToken());
    }

    public void clear() {
        post(new Runnable() {
            public void run() {
                Editable text = TokenCompleteTextView.this.getText();
                if (text != null) {
                    for (TokenCompleteTextView<T>.TokenImageSpan span : (TokenImageSpan[]) text.getSpans(0, text.length(), TokenImageSpan.class)) {
                        TokenCompleteTextView.this.removeSpan(span);
                        TokenCompleteTextView.this.spanWatcher.onSpanRemoved(text, span, text.getSpanStart(span), text.getSpanEnd(span));
                    }
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void updateHint() {
        Editable text = getText();
        CharSequence hintText = getHint();
        if (text != null && hintText != null && this.prefix.length() > 0) {
            HintSpan[] hints = (HintSpan[]) text.getSpans(0, text.length(), HintSpan.class);
            HintSpan hint = null;
            int testLength = this.prefix.length();
            if (hints.length > 0) {
                hint = hints[0];
                testLength += text.getSpanEnd(hint) - text.getSpanStart(hint);
            }
            if (text.length() == testLength) {
                this.hintVisible = true;
                if (hint == null) {
                    Typeface tf = getTypeface();
                    int style = 0;
                    if (tf != null) {
                        style = tf.getStyle();
                    }
                    ColorStateList colors = getHintTextColors();
                    HintSpan hintSpan = new HintSpan(null, style, (int) getTextSize(), colors, colors);
                    text.insert(this.prefix.length(), hintText);
                    text.setSpan(hintSpan, this.prefix.length(), this.prefix.length() + getHint().length(), 33);
                    setSelection(this.prefix.length());
                }
            } else if (hint != null) {
                int sStart = text.getSpanStart(hint);
                int sEnd = text.getSpanEnd(hint);
                text.removeSpan(hint);
                text.replace(sStart, sEnd, "");
                this.hintVisible = false;
            }
        }
    }

    /* access modifiers changed from: private */
    public void clearSelections() {
        Editable text;
        if (this.tokenClickStyle != null && this.tokenClickStyle.isSelectable() && (text = getText()) != null) {
            for (TokenCompleteTextView<T>.TokenImageSpan token : (TokenImageSpan[]) text.getSpans(0, text.length(), TokenImageSpan.class)) {
                token.view.setSelected(false);
            }
            invalidate();
        }
    }

    protected class TokenImageSpan extends ViewSpan {
        /* access modifiers changed from: private */
        public T token;

        public TokenImageSpan(View d, T token2, int maxWidth) {
            super(d, maxWidth);
            this.token = token2;
        }

        public T getToken() {
            return this.token;
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        public void onClick() {
            Editable text = TokenCompleteTextView.this.getText();
            if (text != null) {
                switch (TokenCompleteTextView.this.tokenClickStyle) {
                    case Select:
                    case SelectDeselect:
                        if (!this.view.isSelected()) {
                            TokenCompleteTextView.this.clearSelections();
                            this.view.setSelected(true);
                            return;
                        } else if (TokenCompleteTextView.this.tokenClickStyle == TokenClickStyle.SelectDeselect || !TokenCompleteTextView.this.isTokenRemovable(this.token)) {
                            this.view.setSelected(false);
                            TokenCompleteTextView.this.invalidate();
                            return;
                        }
                    case Delete:
                        break;
                    default:
                        if (TokenCompleteTextView.this.getSelectionStart() != text.getSpanEnd(this) + 1) {
                            TokenCompleteTextView.this.setSelection(text.getSpanEnd(this) + 1);
                            return;
                        }
                        return;
                }
                if (TokenCompleteTextView.this.isTokenRemovable(this.token)) {
                    TokenCompleteTextView.this.removeSpan(this);
                }
            }
        }
    }

    private class TokenSpanWatcher implements SpanWatcher {
        private TokenSpanWatcher() {
        }

        public void onSpanAdded(Spannable text, Object what, int start, int end) {
            if ((what instanceof TokenImageSpan) && !TokenCompleteTextView.this.savingState && !TokenCompleteTextView.this.focusChanging) {
                TokenCompleteTextView<T>.TokenImageSpan token = (TokenImageSpan) what;
                TokenCompleteTextView.this.objects.add(token.getToken());
                if (TokenCompleteTextView.this.listener != null) {
                    TokenCompleteTextView.this.listener.onTokenAdded(token.getToken());
                }
            }
        }

        public void onSpanRemoved(Spannable text, Object what, int start, int end) {
            if ((what instanceof TokenImageSpan) && !TokenCompleteTextView.this.savingState && !TokenCompleteTextView.this.focusChanging) {
                TokenCompleteTextView<T>.TokenImageSpan token = (TokenImageSpan) what;
                if (TokenCompleteTextView.this.objects.contains(token.getToken())) {
                    TokenCompleteTextView.this.objects.remove(token.getToken());
                }
                if (TokenCompleteTextView.this.listener != null) {
                    TokenCompleteTextView.this.listener.onTokenRemoved(token.getToken());
                }
            }
        }

        public void onSpanChanged(Spannable text, Object what, int ostart, int oend, int nstart, int nend) {
        }
    }

    private class TokenTextWatcher implements TextWatcher {
        ArrayList<TokenCompleteTextView<T>.TokenImageSpan> spansToRemove;

        private TokenTextWatcher() {
            this.spansToRemove = new ArrayList<>();
        }

        /* access modifiers changed from: protected */
        public void removeToken(TokenCompleteTextView<T>.TokenImageSpan token, Editable text) {
            text.removeSpan(token);
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            if (count > 0 && TokenCompleteTextView.this.getText() != null) {
                Editable text = TokenCompleteTextView.this.getText();
                int end = start + count;
                if (text.charAt(start) == ' ') {
                    start--;
                }
                TokenCompleteTextView<T>.TokenImageSpan[] spans = (TokenImageSpan[]) text.getSpans(start, end, TokenImageSpan.class);
                this.spansToRemove = new ArrayList<>();
                for (TokenCompleteTextView<T>.TokenImageSpan token : spans) {
                    if (text.getSpanStart(token) < end && start < text.getSpanEnd(token)) {
                        this.spansToRemove.add(token);
                    }
                }
            }
        }

        public void afterTextChanged(Editable text) {
            Iterator it = new ArrayList<>(this.spansToRemove).iterator();
            while (it.hasNext()) {
                TokenCompleteTextView<T>.TokenImageSpan token = (TokenImageSpan) it.next();
                int spanStart = text.getSpanStart(token);
                int spanEnd = text.getSpanEnd(token);
                removeToken(token, text);
                int spanEnd2 = spanEnd - 1;
                if (spanEnd2 >= 0 && TokenCompleteTextView.this.isSplitChar(text.charAt(spanEnd2))) {
                    text.delete(spanEnd2, spanEnd2 + 1);
                }
                if (spanStart >= 0 && TokenCompleteTextView.this.isSplitChar(text.charAt(spanStart))) {
                    text.delete(spanStart, spanStart + 1);
                }
            }
            TokenCompleteTextView.this.clearSelections();
            TokenCompleteTextView.this.updateHint();
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    }

    /* access modifiers changed from: protected */
    public ArrayList<Serializable> getSerializableObjects() {
        ArrayList<Serializable> serializables = new ArrayList<>();
        for (Object obj : getObjects()) {
            if (obj instanceof Serializable) {
                serializables.add((Serializable) obj);
            } else {
                Log.e(TAG, "Unable to save '" + obj + "'");
            }
        }
        if (serializables.size() != this.objects.size()) {
            Log.e(TAG, "You should make your objects Serializable or override\ngetSerializableObjects and convertSerializableArrayToObjectArray");
        }
        return serializables;
    }

    /* access modifiers changed from: protected */
    public ArrayList<T> convertSerializableArrayToObjectArray(ArrayList<Serializable> s) {
        return s;
    }

    public Parcelable onSaveInstanceState() {
        ArrayList<Serializable> baseObjects = getSerializableObjects();
        removeListeners();
        this.savingState = true;
        Parcelable superState = super.onSaveInstanceState();
        this.savingState = false;
        SavedState state = new SavedState(superState);
        state.prefix = this.prefix;
        state.allowCollapse = this.allowCollapse;
        state.allowDuplicates = this.allowDuplicates;
        state.performBestGuess = this.performBestGuess;
        state.tokenClickStyle = this.tokenClickStyle;
        state.tokenDeleteStyle = this.deletionStyle;
        state.baseObjects = baseObjects;
        state.splitChar = this.splitChar;
        addListeners();
        return state;
    }

    public void onRestoreInstanceState(Parcelable state) {
        if (!(state instanceof SavedState)) {
            super.onRestoreInstanceState(state);
            return;
        }
        SavedState ss = (SavedState) state;
        super.onRestoreInstanceState(ss.getSuperState());
        setText(ss.prefix);
        this.prefix = ss.prefix;
        updateHint();
        this.allowCollapse = ss.allowCollapse;
        this.allowDuplicates = ss.allowDuplicates;
        this.performBestGuess = ss.performBestGuess;
        this.tokenClickStyle = ss.tokenClickStyle;
        this.deletionStyle = ss.tokenDeleteStyle;
        this.splitChar = ss.splitChar;
        addListeners();
        Iterator it = convertSerializableArrayToObjectArray(ss.baseObjects).iterator();
        while (it.hasNext()) {
            addObject(it.next());
        }
        if (!isFocused() && this.allowCollapse) {
            post(new Runnable() {
                public void run() {
                    TokenCompleteTextView.this.performCollapse(TokenCompleteTextView.this.isFocused());
                }
            });
        }
    }

    private static class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }

            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };
        boolean allowCollapse;
        boolean allowDuplicates;
        ArrayList<Serializable> baseObjects;
        boolean performBestGuess;
        CharSequence prefix;
        char[] splitChar;
        TokenClickStyle tokenClickStyle;
        TokenDeleteStyle tokenDeleteStyle;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        SavedState(Parcel in) {
            super(in);
            boolean z;
            boolean z2 = true;
            this.prefix = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(in);
            this.allowCollapse = in.readInt() != 0;
            if (in.readInt() != 0) {
                z = true;
            } else {
                z = false;
            }
            this.allowDuplicates = z;
            this.performBestGuess = in.readInt() == 0 ? false : z2;
            this.tokenClickStyle = TokenClickStyle.values()[in.readInt()];
            this.tokenDeleteStyle = TokenDeleteStyle.values()[in.readInt()];
            this.baseObjects = (ArrayList) in.readSerializable();
            this.splitChar = in.createCharArray();
        }

        SavedState(Parcelable superState) {
            super(superState);
        }

        public void writeToParcel(@NonNull Parcel out, int flags) {
            int i;
            int i2 = 1;
            super.writeToParcel(out, flags);
            TextUtils.writeToParcel(this.prefix, out, 0);
            out.writeInt(this.allowCollapse ? 1 : 0);
            if (this.allowDuplicates) {
                i = 1;
            } else {
                i = 0;
            }
            out.writeInt(i);
            if (!this.performBestGuess) {
                i2 = 0;
            }
            out.writeInt(i2);
            out.writeInt(this.tokenClickStyle.ordinal());
            out.writeInt(this.tokenDeleteStyle.ordinal());
            out.writeSerializable(this.baseObjects);
            out.writeCharArray(this.splitChar);
        }

        public String toString() {
            return ("TokenCompleteTextView.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " tokens=" + this.baseObjects) + "}";
        }
    }

    public boolean canDeleteSelection(int beforeLength) {
        if (this.objects.size() < 1) {
            return true;
        }
        int endSelection = getSelectionEnd();
        int startSelection = beforeLength == 1 ? getSelectionStart() : endSelection - beforeLength;
        Editable text = getText();
        for (TokenCompleteTextView<T>.TokenImageSpan span : (TokenImageSpan[]) text.getSpans(0, text.length(), TokenImageSpan.class)) {
            int startTokenSelection = text.getSpanStart(span);
            int endTokenSelection = text.getSpanEnd(span);
            if (!isTokenRemovable(span.token)) {
                if (startSelection == endSelection) {
                    if (endTokenSelection + 1 == endSelection) {
                        return false;
                    }
                } else if (startSelection <= startTokenSelection && endTokenSelection + 1 <= endSelection) {
                    return false;
                }
            }
        }
        return true;
    }

    private class TokenInputConnection extends InputConnectionWrapper {
        public TokenInputConnection(InputConnection target, boolean mutable) {
            super(target, mutable);
        }

        public boolean deleteSurroundingText(int beforeLength, int afterLength) {
            if (!TokenCompleteTextView.this.canDeleteSelection(beforeLength)) {
                return false;
            }
            if (TokenCompleteTextView.this.getSelectionStart() > TokenCompleteTextView.this.prefix.length()) {
                return super.deleteSurroundingText(beforeLength, afterLength);
            }
            if (TokenCompleteTextView.this.deleteSelectedObject(false) || super.deleteSurroundingText(0, afterLength)) {
                return true;
            }
            return false;
        }
    }
}
