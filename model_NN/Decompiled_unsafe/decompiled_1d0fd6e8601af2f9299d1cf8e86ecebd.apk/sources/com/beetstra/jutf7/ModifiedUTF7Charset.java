package com.beetstra.jutf7;

class ModifiedUTF7Charset extends UTF7StyleCharset {
    private static final String MODIFIED_BASE64_ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+,";

    ModifiedUTF7Charset(String name, String[] aliases) {
        super(name, aliases, MODIFIED_BASE64_ALPHABET, true);
    }

    /* access modifiers changed from: package-private */
    public boolean canEncodeDirectly(char ch) {
        if (ch != shift() && ch >= ' ' && ch <= '~') {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public byte shift() {
        return 38;
    }

    /* access modifiers changed from: package-private */
    public byte unshift() {
        return 45;
    }
}
