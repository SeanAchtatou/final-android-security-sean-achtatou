package com.beetstra.jutf7;

class UTF7Charset extends UTF7StyleCharset {
    private static final String BASE64_ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    private static final String RULE_3 = " \t\r\n";
    private static final String SET_D = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'(),-./:?";
    private static final String SET_O = "!\"#$%&*;<=>@[]^_`{|}";
    final String directlyEncoded;

    UTF7Charset(String name, String[] aliases, boolean includeOptional) {
        super(name, aliases, BASE64_ALPHABET, false);
        if (includeOptional) {
            this.directlyEncoded = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'(),-./:?!\"#$%&*;<=>@[]^_`{|} \t\r\n";
        } else {
            this.directlyEncoded = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'(),-./:? \t\r\n";
        }
    }

    /* access modifiers changed from: package-private */
    public boolean canEncodeDirectly(char ch) {
        return this.directlyEncoded.indexOf(ch) >= 0;
    }

    /* access modifiers changed from: package-private */
    public byte shift() {
        return 43;
    }

    /* access modifiers changed from: package-private */
    public byte unshift() {
        return 45;
    }
}
