package com.fsck.k9;

public interface BaseAccount {
    String getDescription();

    String getEmail();

    String getUuid();

    void setDescription(String str);

    void setEmail(String str);
}
