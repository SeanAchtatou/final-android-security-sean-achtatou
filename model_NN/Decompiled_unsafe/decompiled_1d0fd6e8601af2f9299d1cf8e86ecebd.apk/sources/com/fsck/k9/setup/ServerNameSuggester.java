package com.fsck.k9.setup;

import com.fsck.k9.mail.ServerSettings;

public class ServerNameSuggester {
    public String suggestServerName(ServerSettings.Type serverType, String domainPart) {
        switch (serverType) {
            case IMAP:
                return "imap." + domainPart;
            case SMTP:
                return "smtp." + domainPart;
            case WebDAV:
                return "exchange." + domainPart;
            case POP3:
                return "pop3." + domainPart;
            default:
                throw new AssertionError("Missed case: " + serverType);
        }
    }
}
