package com.fsck.k9.mail.filter;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class LineWrapOutputStream extends FilterOutputStream {
    private static final byte[] CRLF = {13, 10};
    private byte[] buffer;
    private int bufferStart = 0;
    private int endOfLastWord = 0;
    private int lineLength = 0;

    public LineWrapOutputStream(OutputStream out, int maxLineLength) {
        super(out);
        this.buffer = new byte[(maxLineLength - 2)];
    }

    public void write(int oneByte) throws IOException {
        if (this.lineLength == this.buffer.length) {
            if (this.endOfLastWord > 0) {
                this.out.write(this.buffer, this.bufferStart, this.endOfLastWord - this.bufferStart);
                this.out.write(CRLF);
                this.bufferStart = 0;
                this.endOfLastWord++;
                this.lineLength = this.buffer.length - this.endOfLastWord;
                if (this.lineLength > 0) {
                    System.arraycopy(this.buffer, this.endOfLastWord + 0, this.buffer, 0, this.lineLength);
                }
                this.endOfLastWord = 0;
            } else {
                this.out.write(this.buffer, this.bufferStart, this.buffer.length - this.bufferStart);
                this.out.write(CRLF);
                this.lineLength = 0;
                this.bufferStart = 0;
            }
        }
        if (oneByte == 10 || oneByte == 13) {
            if (this.lineLength - this.bufferStart > 0) {
                this.out.write(this.buffer, this.bufferStart, this.lineLength - this.bufferStart);
            }
            this.out.write(oneByte);
            this.lineLength = 0;
            this.bufferStart = 0;
            this.endOfLastWord = 0;
            return;
        }
        if (oneByte == 32) {
            this.endOfLastWord = this.lineLength;
        }
        this.buffer[this.lineLength] = (byte) oneByte;
        this.lineLength++;
    }

    public void flush() throws IOException {
        if (this.lineLength > this.bufferStart) {
            this.out.write(this.buffer, this.bufferStart, this.lineLength - this.bufferStart);
            this.bufferStart = this.lineLength == this.buffer.length ? 0 : this.lineLength;
            this.endOfLastWord = 0;
        }
        this.out.flush();
    }
}
