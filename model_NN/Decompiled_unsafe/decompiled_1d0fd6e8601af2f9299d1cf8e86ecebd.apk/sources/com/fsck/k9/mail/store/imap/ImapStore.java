package com.fsck.k9.mail.store.imap;

import android.net.ConnectivityManager;
import android.util.Log;
import com.fsck.k9.mail.AuthType;
import com.fsck.k9.mail.ConnectionSecurity;
import com.fsck.k9.mail.Flag;
import com.fsck.k9.mail.K9MailLib;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.NetworkType;
import com.fsck.k9.mail.PushReceiver;
import com.fsck.k9.mail.Pusher;
import com.fsck.k9.mail.ServerSettings;
import com.fsck.k9.mail.oauth.OAuth2TokenProvider;
import com.fsck.k9.mail.ssl.TrustedSocketFactory;
import com.fsck.k9.mail.store.RemoteStore;
import com.fsck.k9.mail.store.StoreConfig;
import java.io.IOException;
import java.nio.charset.CharacterCodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ImapStore extends RemoteStore {
    /* access modifiers changed from: private */
    public AuthType authType;
    /* access modifiers changed from: private */
    public String clientCertificateAlias;
    /* access modifiers changed from: private */
    public String combinedPrefix = null;
    /* access modifiers changed from: private */
    public ConnectionSecurity connectionSecurity;
    private final Deque<ImapConnection> connections = new LinkedList();
    private ConnectivityManager connectivityManager;
    private final Map<String, ImapFolder> folderCache = new HashMap();
    private FolderNameCodec folderNameCodec;
    /* access modifiers changed from: private */
    public String host;
    private OAuth2TokenProvider oauthTokenProvider;
    /* access modifiers changed from: private */
    public String password;
    /* access modifiers changed from: private */
    public String pathDelimiter = null;
    /* access modifiers changed from: private */
    public String pathPrefix;
    private Set<Flag> permanentFlagsIndex = EnumSet.noneOf(Flag.class);
    /* access modifiers changed from: private */
    public int port;
    /* access modifiers changed from: private */
    public String username;

    public static ImapStoreSettings decodeUri(String uri) {
        return ImapStoreUriDecoder.decode(uri);
    }

    public static String createUri(ServerSettings server) {
        return ImapStoreUriCreator.create(server);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ImapStore(StoreConfig storeConfig, TrustedSocketFactory trustedSocketFactory, ConnectivityManager connectivityManager2, OAuth2TokenProvider oauthTokenProvider2) throws MessagingException {
        super(storeConfig, trustedSocketFactory);
        String str = null;
        try {
            ImapStoreSettings settings = decodeUri(storeConfig.getStoreUri());
            this.host = settings.host;
            this.port = settings.port;
            this.connectionSecurity = settings.connectionSecurity;
            this.connectivityManager = connectivityManager2;
            this.oauthTokenProvider = oauthTokenProvider2;
            this.authType = settings.authenticationType;
            this.username = settings.username;
            this.password = settings.password;
            this.clientCertificateAlias = settings.clientCertificateAlias;
            this.pathPrefix = !settings.autoDetectNamespace ? settings.pathPrefix : str;
            this.folderNameCodec = FolderNameCodec.newInstance();
        } catch (IllegalArgumentException e) {
            throw new MessagingException("Error while decoding store URI", e);
        }
    }

    public ImapFolder getFolder(String name) {
        ImapFolder folder;
        synchronized (this.folderCache) {
            folder = this.folderCache.get(name);
            if (folder == null) {
                folder = new ImapFolder(this, name);
                this.folderCache.put(name, folder);
            }
        }
        return folder;
    }

    /* access modifiers changed from: package-private */
    public String getCombinedPrefix() {
        if (this.combinedPrefix == null) {
            if (this.pathPrefix != null) {
                String tmpPrefix = this.pathPrefix.trim();
                String tmpDelim = this.pathDelimiter != null ? this.pathDelimiter.trim() : "";
                if (tmpPrefix.endsWith(tmpDelim)) {
                    this.combinedPrefix = tmpPrefix;
                } else if (tmpPrefix.length() > 0) {
                    this.combinedPrefix = tmpPrefix + tmpDelim;
                } else {
                    this.combinedPrefix = "";
                }
            } else {
                this.combinedPrefix = "";
            }
        }
        return this.combinedPrefix;
    }

    public List<ImapFolder> getPersonalNamespaces(boolean forceListAll) throws MessagingException {
        Exception ioe;
        List<ImapFolder> list;
        ImapConnection connection = getConnection();
        try {
            Set<String> folderNames = listFolders(connection, false);
            if (forceListAll || !this.mStoreConfig.subscribedFoldersOnly()) {
                list = getFolders(folderNames);
                releaseConnection(connection);
            } else {
                folderNames.retainAll(listFolders(connection, true));
                list = getFolders(folderNames);
                releaseConnection(connection);
            }
            return list;
        } catch (IOException e) {
            ioe = e;
            try {
                connection.close();
                throw new MessagingException("Unable to get folder list.", ioe);
            } catch (Throwable th) {
                releaseConnection(connection);
                throw th;
            }
        } catch (MessagingException e2) {
            ioe = e2;
            connection.close();
            throw new MessagingException("Unable to get folder list.", ioe);
        }
    }

    private Set<String> listFolders(ImapConnection connection, boolean subscribedOnly) throws IOException, MessagingException {
        List<ImapResponse> responses = connection.executeSimpleCommand(String.format("%s \"\" %s", subscribedOnly ? Responses.LSUB : "LIST", ImapUtility.encodeString(getCombinedPrefix() + "*")));
        List<ListResponse> listResponses = subscribedOnly ? ListResponse.parseLsub(responses) : ListResponse.parseList(responses);
        Set<String> folderNames = new HashSet<>(listResponses.size());
        for (ListResponse listResponse : listResponses) {
            boolean includeFolder = true;
            try {
                String decodedFolderName = this.folderNameCodec.decode(listResponse.getName());
                String folder = decodedFolderName;
                if (this.pathDelimiter == null) {
                    this.pathDelimiter = listResponse.getHierarchyDelimiter();
                    this.combinedPrefix = null;
                }
                if (!folder.equalsIgnoreCase(this.mStoreConfig.getInboxFolderName()) && !folder.equals(this.mStoreConfig.getOutboxFolderName())) {
                    int prefixLength = getCombinedPrefix().length();
                    if (prefixLength > 0) {
                        if (folder.length() >= prefixLength) {
                            folder = folder.substring(prefixLength);
                        }
                        if (!decodedFolderName.equalsIgnoreCase(getCombinedPrefix() + folder)) {
                            includeFolder = false;
                        }
                    }
                    if (listResponse.hasAttribute("\\NoSelect")) {
                        includeFolder = false;
                    }
                    if (includeFolder) {
                        folderNames.add(folder);
                    }
                }
            } catch (CharacterCodingException e) {
                Log.w("k9", "Folder name not correctly encoded with the UTF-7 variant as defined by RFC 3501: " + listResponse.getName(), e);
            }
        }
        folderNames.add(this.mStoreConfig.getInboxFolderName());
        return folderNames;
    }

    /* access modifiers changed from: package-private */
    public void autoconfigureFolders(ImapConnection connection) throws IOException, MessagingException {
        if (connection.hasCapability(Capabilities.SPECIAL_USE)) {
            if (K9MailLib.isDebug()) {
                Log.d("k9", "Folder auto-configuration: Using RFC6154/SPECIAL-USE.");
            }
            for (ListResponse listResponse : ListResponse.parseList(connection.executeSimpleCommand(String.format("LIST (SPECIAL-USE) \"\" %s", ImapUtility.encodeString(getCombinedPrefix() + "*"))))) {
                try {
                    String decodedFolderName = this.folderNameCodec.decode(listResponse.getName());
                    if (this.pathDelimiter == null) {
                        this.pathDelimiter = listResponse.getHierarchyDelimiter();
                        this.combinedPrefix = null;
                    }
                    if (listResponse.hasAttribute("\\Archive") || listResponse.hasAttribute("\\All")) {
                        this.mStoreConfig.setArchiveFolderName(decodedFolderName);
                        if (K9MailLib.isDebug()) {
                            Log.d("k9", "Folder auto-configuration detected Archive folder: " + decodedFolderName);
                        }
                    } else if (listResponse.hasAttribute("\\Drafts")) {
                        this.mStoreConfig.setDraftsFolderName(decodedFolderName);
                        if (K9MailLib.isDebug()) {
                            Log.d("k9", "Folder auto-configuration detected Drafts folder: " + decodedFolderName);
                        }
                    } else if (listResponse.hasAttribute("\\Sent")) {
                        this.mStoreConfig.setSentFolderName(decodedFolderName);
                        if (K9MailLib.isDebug()) {
                            Log.d("k9", "Folder auto-configuration detected Sent folder: " + decodedFolderName);
                        }
                    } else if (listResponse.hasAttribute("\\Junk")) {
                        this.mStoreConfig.setSpamFolderName(decodedFolderName);
                        if (K9MailLib.isDebug()) {
                            Log.d("k9", "Folder auto-configuration detected Spam folder: " + decodedFolderName);
                        }
                    } else if (listResponse.hasAttribute("\\Trash")) {
                        this.mStoreConfig.setTrashFolderName(decodedFolderName);
                        if (K9MailLib.isDebug()) {
                            Log.d("k9", "Folder auto-configuration detected Trash folder: " + decodedFolderName);
                        }
                    }
                } catch (CharacterCodingException e) {
                    Log.w("k9", "Folder name not correctly encoded with the UTF-7 variant as defined by RFC 3501: " + listResponse.getName(), e);
                }
            }
        } else if (K9MailLib.isDebug()) {
            Log.d("k9", "No detected folder auto-configuration methods.");
        }
    }

    public void checkSettings() throws MessagingException {
        try {
            ImapConnection connection = createImapConnection();
            connection.open();
            autoconfigureFolders(connection);
            connection.close();
        } catch (IOException ioe) {
            throw new MessagingException("Unable to connect", ioe);
        }
    }

    /* access modifiers changed from: package-private */
    public ImapConnection getConnection() throws MessagingException {
        ImapConnection connection;
        while (true) {
            connection = pollConnection();
            if (connection == null) {
                break;
            }
            try {
                connection.executeSimpleCommand(Commands.NOOP);
                break;
            } catch (IOException e) {
                connection.close();
            }
        }
        if (connection == null) {
            return createImapConnection();
        }
        return connection;
    }

    private ImapConnection pollConnection() {
        ImapConnection poll;
        synchronized (this.connections) {
            poll = this.connections.poll();
        }
        return poll;
    }

    /* access modifiers changed from: package-private */
    public void releaseConnection(ImapConnection connection) {
        if (connection != null && connection.isConnected()) {
            synchronized (this.connections) {
                this.connections.offer(connection);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public ImapConnection createImapConnection() {
        return new ImapConnection(new StoreImapSettings(), this.mTrustedSocketFactory, this.connectivityManager, this.oauthTokenProvider);
    }

    /* access modifiers changed from: package-private */
    public FolderNameCodec getFolderNameCodec() {
        return this.folderNameCodec;
    }

    private List<ImapFolder> getFolders(Collection<String> folderNames) {
        List<ImapFolder> folders = new ArrayList<>(folderNames.size());
        for (String folderName : folderNames) {
            folders.add(getFolder(folderName));
        }
        return folders;
    }

    public boolean isMoveCapable() {
        return true;
    }

    public boolean isCopyCapable() {
        return true;
    }

    public boolean isPushCapable() {
        return true;
    }

    public boolean isExpungeCapable() {
        return true;
    }

    /* access modifiers changed from: package-private */
    public StoreConfig getStoreConfig() {
        return this.mStoreConfig;
    }

    /* access modifiers changed from: package-private */
    public Set<Flag> getPermanentFlagsIndex() {
        return this.permanentFlagsIndex;
    }

    public Pusher getPusher(PushReceiver receiver) {
        return new ImapPusher(this, receiver);
    }

    private class StoreImapSettings implements ImapSettings {
        private StoreImapSettings() {
        }

        public String getHost() {
            return ImapStore.this.host;
        }

        public int getPort() {
            return ImapStore.this.port;
        }

        public ConnectionSecurity getConnectionSecurity() {
            return ImapStore.this.connectionSecurity;
        }

        public AuthType getAuthType() {
            return ImapStore.this.authType;
        }

        public String getUsername() {
            return ImapStore.this.username;
        }

        public String getPassword() {
            return ImapStore.this.password;
        }

        public String getClientCertificateAlias() {
            return ImapStore.this.clientCertificateAlias;
        }

        public boolean useCompression(NetworkType type) {
            return ImapStore.this.mStoreConfig.useCompression(type);
        }

        public String getPathPrefix() {
            return ImapStore.this.pathPrefix;
        }

        public void setPathPrefix(String prefix) {
            String unused = ImapStore.this.pathPrefix = prefix;
        }

        public String getPathDelimiter() {
            return ImapStore.this.pathDelimiter;
        }

        public void setPathDelimiter(String delimiter) {
            String unused = ImapStore.this.pathDelimiter = delimiter;
        }

        public String getCombinedPrefix() {
            return ImapStore.this.combinedPrefix;
        }

        public void setCombinedPrefix(String prefix) {
            String unused = ImapStore.this.combinedPrefix = prefix;
        }
    }
}
