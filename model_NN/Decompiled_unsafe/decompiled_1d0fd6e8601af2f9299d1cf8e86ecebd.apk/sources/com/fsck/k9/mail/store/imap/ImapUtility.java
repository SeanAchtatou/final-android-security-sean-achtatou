package com.fsck.k9.mail.store.imap;

import android.util.Log;
import java.util.ArrayList;
import java.util.List;

class ImapUtility {
    ImapUtility() {
    }

    public static List<String> getImapSequenceValues(String set) {
        List<String> list = new ArrayList<>();
        if (set != null) {
            for (String item : set.split(",")) {
                if (item.indexOf(58) != -1) {
                    list.addAll(getImapRangeValues(item));
                } else if (isNumberValid(item)) {
                    list.add(item);
                }
            }
        }
        return list;
    }

    public static List<String> getImapRangeValues(String range) {
        List<String> list = new ArrayList<>();
        if (range != null) {
            try {
                int colonPos = range.indexOf(58);
                if (colonPos > 0) {
                    long first = Long.parseLong(range.substring(0, colonPos));
                    long second = Long.parseLong(range.substring(colonPos + 1));
                    if (!is32bitValue(first) || !is32bitValue(second)) {
                        Log.d("k9", "Invalid range: " + range);
                    } else if (first < second) {
                        for (long i = first; i <= second; i++) {
                            list.add(Long.toString(i));
                        }
                    } else {
                        for (long i2 = first; i2 >= second; i2--) {
                            list.add(Long.toString(i2));
                        }
                    }
                }
            } catch (NumberFormatException e) {
                Log.d("k9", "Invalid range value: " + range, e);
            }
        }
        return list;
    }

    private static boolean isNumberValid(String number) {
        try {
            if (is32bitValue(Long.parseLong(number))) {
                return true;
            }
        } catch (NumberFormatException e) {
        }
        Log.d("k9", "Invalid UID value: " + number);
        return false;
    }

    private static boolean is32bitValue(long value) {
        return (-4294967296L & value) == 0;
    }

    public static String encodeString(String str) {
        return "\"" + str.replace("\\", "\\\\").replace("\"", "\\\"") + "\"";
    }

    public static ImapResponse getLastResponse(List<ImapResponse> responses) {
        return responses.get(responses.size() - 1);
    }
}
