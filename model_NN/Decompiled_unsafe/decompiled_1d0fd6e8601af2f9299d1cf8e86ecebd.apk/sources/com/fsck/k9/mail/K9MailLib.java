package com.fsck.k9.mail;

public class K9MailLib {
    public static boolean DEBUG_PROTOCOL_IMAP = true;
    public static boolean DEBUG_PROTOCOL_POP3 = true;
    public static boolean DEBUG_PROTOCOL_SMTP = true;
    public static boolean DEBUG_PROTOCOL_WEBDAV = true;
    public static final String IDENTITY_HEADER = "X-K9mail-Identity";
    public static final String LOG_TAG = "k9";
    public static final int PUSH_WAKE_LOCK_TIMEOUT = 60000;
    private static DebugStatus debugStatus = new DefaultDebugStatus();

    public interface DebugStatus {
        boolean debugSensitive();

        boolean enabled();
    }

    private interface WritableDebugStatus extends DebugStatus {
        void setEnabled(boolean z);

        void setSensitive(boolean z);
    }

    private K9MailLib() {
    }

    public static boolean isDebug() {
        return debugStatus.enabled();
    }

    public static boolean isDebugSensitive() {
        return debugStatus.debugSensitive();
    }

    public static void setDebugSensitive(boolean b) {
        if (debugStatus instanceof WritableDebugStatus) {
            ((WritableDebugStatus) debugStatus).setSensitive(b);
        }
    }

    public static void setDebug(boolean b) {
        if (debugStatus instanceof WritableDebugStatus) {
            ((WritableDebugStatus) debugStatus).setEnabled(b);
        }
    }

    public static void setDebugStatus(DebugStatus status) {
        if (status == null) {
            throw new IllegalArgumentException("status cannot be null");
        }
        debugStatus = status;
    }

    private static class DefaultDebugStatus implements WritableDebugStatus {
        private boolean enabled;
        private boolean sensitive;

        private DefaultDebugStatus() {
        }

        public boolean enabled() {
            return this.enabled;
        }

        public boolean debugSensitive() {
            return this.sensitive;
        }

        public void setEnabled(boolean enabled2) {
            this.enabled = enabled2;
        }

        public void setSensitive(boolean sensitive2) {
            this.sensitive = sensitive2;
        }
    }
}
