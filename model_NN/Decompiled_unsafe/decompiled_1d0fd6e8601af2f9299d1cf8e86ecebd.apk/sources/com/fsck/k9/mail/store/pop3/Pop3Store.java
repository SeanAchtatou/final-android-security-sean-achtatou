package com.fsck.k9.mail.store.pop3;

import android.annotation.SuppressLint;
import android.util.Log;
import com.fsck.k9.mail.AuthType;
import com.fsck.k9.mail.Authentication;
import com.fsck.k9.mail.AuthenticationFailedException;
import com.fsck.k9.mail.CertificateValidationException;
import com.fsck.k9.mail.ConnectionSecurity;
import com.fsck.k9.mail.FetchProfile;
import com.fsck.k9.mail.Flag;
import com.fsck.k9.mail.Folder;
import com.fsck.k9.mail.K9MailLib;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.MessageRetrievalListener;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.ServerSettings;
import com.fsck.k9.mail.filter.Base64;
import com.fsck.k9.mail.filter.Hex;
import com.fsck.k9.mail.helper.UrlEncodingHelper;
import com.fsck.k9.mail.internet.MimeMessage;
import com.fsck.k9.mail.ssl.TrustedSocketFactory;
import com.fsck.k9.mail.store.RemoteStore;
import com.fsck.k9.mail.store.StoreConfig;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import javax.net.ssl.SSLException;

public class Pop3Store extends RemoteStore {
    private static final String AUTH_COMMAND = "AUTH";
    private static final String AUTH_CRAM_MD5_CAPABILITY = "CRAM-MD5";
    private static final String AUTH_EXTERNAL_CAPABILITY = "EXTERNAL";
    private static final String AUTH_PLAIN_CAPABILITY = "PLAIN";
    private static final String CAPA_COMMAND = "CAPA";
    private static final String DELE_COMMAND = "DELE";
    private static final String LIST_COMMAND = "LIST";
    private static final String PASS_COMMAND = "PASS";
    private static final String QUIT_COMMAND = "QUIT";
    private static final String RETR_COMMAND = "RETR";
    private static final String SASL_CAPABILITY = "SASL";
    private static final String STAT_COMMAND = "STAT";
    private static final String STLS_CAPABILITY = "STLS";
    private static final String STLS_COMMAND = "STLS";
    private static final String TOP_CAPABILITY = "TOP";
    private static final String TOP_COMMAND = "TOP";
    private static final String UIDL_CAPABILITY = "UIDL";
    private static final String UIDL_COMMAND = "UIDL";
    private static final String USER_COMMAND = "USER";
    /* access modifiers changed from: private */
    public AuthType mAuthType;
    /* access modifiers changed from: private */
    public Pop3Capabilities mCapabilities;
    /* access modifiers changed from: private */
    public String mClientCertificateAlias;
    /* access modifiers changed from: private */
    public ConnectionSecurity mConnectionSecurity;
    private Map<String, Folder> mFolders = new HashMap();
    /* access modifiers changed from: private */
    public String mHost;
    /* access modifiers changed from: private */
    public String mPassword;
    /* access modifiers changed from: private */
    public int mPort;
    /* access modifiers changed from: private */
    public boolean mTopNotSupported;
    /* access modifiers changed from: private */
    public String mUsername;

    public static ServerSettings decodeUri(String uri) {
        ConnectionSecurity connectionSecurity;
        int port;
        String username = null;
        String password = null;
        String clientCertificateAlias = null;
        try {
            URI pop3Uri = new URI(uri);
            String scheme = pop3Uri.getScheme();
            if (scheme.equals("pop3")) {
                connectionSecurity = ConnectionSecurity.NONE;
                port = ServerSettings.Type.POP3.defaultPort;
            } else if (scheme.startsWith("pop3+tls")) {
                connectionSecurity = ConnectionSecurity.STARTTLS_REQUIRED;
                port = ServerSettings.Type.POP3.defaultPort;
            } else if (scheme.startsWith("pop3+ssl")) {
                connectionSecurity = ConnectionSecurity.SSL_TLS_REQUIRED;
                port = ServerSettings.Type.POP3.defaultTlsPort;
            } else {
                throw new IllegalArgumentException("Unsupported protocol (" + scheme + ")");
            }
            String host = pop3Uri.getHost();
            if (pop3Uri.getPort() != -1) {
                port = pop3Uri.getPort();
            }
            AuthType authType = AuthType.PLAIN;
            if (pop3Uri.getUserInfo() != null) {
                int userIndex = 0;
                int passwordIndex = 1;
                String userinfo = pop3Uri.getUserInfo();
                String[] userInfoParts = userinfo.split(":");
                if (userInfoParts.length > 2 || userinfo.endsWith(":")) {
                    userIndex = 0 + 1;
                    passwordIndex = 1 + 1;
                    authType = AuthType.valueOf(userInfoParts[0]);
                }
                username = UrlEncodingHelper.decodeUtf8(userInfoParts[userIndex]);
                if (userInfoParts.length > passwordIndex) {
                    if (authType == AuthType.EXTERNAL) {
                        clientCertificateAlias = UrlEncodingHelper.decodeUtf8(userInfoParts[passwordIndex]);
                    } else {
                        password = UrlEncodingHelper.decodeUtf8(userInfoParts[passwordIndex]);
                    }
                }
            }
            return new ServerSettings(ServerSettings.Type.POP3, host, port, connectionSecurity, authType, username, password, clientCertificateAlias);
        } catch (URISyntaxException use) {
            throw new IllegalArgumentException("Invalid Pop3Store URI", use);
        }
    }

    public static String createUri(ServerSettings server) {
        String scheme;
        String userInfo;
        String userEnc = UrlEncodingHelper.encodeUtf8(server.username);
        String passwordEnc = server.password != null ? UrlEncodingHelper.encodeUtf8(server.password) : "";
        String clientCertificateAliasEnc = server.clientCertificateAlias != null ? UrlEncodingHelper.encodeUtf8(server.clientCertificateAlias) : "";
        switch (server.connectionSecurity) {
            case SSL_TLS_REQUIRED:
                scheme = "pop3+ssl+";
                break;
            case STARTTLS_REQUIRED:
                scheme = "pop3+tls+";
                break;
            default:
                scheme = "pop3";
                break;
        }
        AuthType authType = server.authenticationType;
        if (AuthType.EXTERNAL == authType) {
            userInfo = authType.name() + ":" + userEnc + ":" + clientCertificateAliasEnc;
        } else {
            userInfo = authType.name() + ":" + userEnc + ":" + passwordEnc;
        }
        try {
            return new URI(scheme, userInfo, server.host, server.port, null, null, null).toString();
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException("Can't create Pop3Store URI", e);
        }
    }

    public Pop3Store(StoreConfig storeConfig, TrustedSocketFactory socketFactory) throws MessagingException {
        super(storeConfig, socketFactory);
        try {
            ServerSettings settings = decodeUri(storeConfig.getStoreUri());
            this.mHost = settings.host;
            this.mPort = settings.port;
            this.mConnectionSecurity = settings.connectionSecurity;
            this.mUsername = settings.username;
            this.mPassword = settings.password;
            this.mClientCertificateAlias = settings.clientCertificateAlias;
            this.mAuthType = settings.authenticationType;
        } catch (IllegalArgumentException e) {
            throw new MessagingException("Error while decoding store URI", e);
        }
    }

    public Folder getFolder(String name) {
        Folder folder = this.mFolders.get(name);
        if (folder != null) {
            return folder;
        }
        Folder folder2 = new Pop3Folder(name);
        this.mFolders.put(folder2.getName(), folder2);
        return folder2;
    }

    public List<? extends Folder> getPersonalNamespaces(boolean forceListAll) throws MessagingException {
        List<Folder> folders = new LinkedList<>();
        folders.add(getFolder(this.mStoreConfig.getInboxFolderName()));
        return folders;
    }

    public void checkSettings() throws MessagingException {
        Pop3Folder folder = new Pop3Folder(this.mStoreConfig.getInboxFolderName());
        try {
            folder.open(0);
            if (!this.mCapabilities.uidl) {
                String unused = folder.executeSimpleCommand("UIDL");
            }
        } finally {
            folder.close();
        }
    }

    public boolean isSeenFlagSupported() {
        return false;
    }

    class Pop3Folder extends Folder<Pop3Message> {
        private InputStream mIn;
        private int mMessageCount;
        @SuppressLint({"UseSparseArrays"})
        private Map<Integer, Pop3Message> mMsgNumToMsgMap = new HashMap();
        private String mName;
        private OutputStream mOut;
        private Socket mSocket;
        private Map<String, Pop3Message> mUidToMsgMap = new HashMap();
        private Map<String, Integer> mUidToMsgNumMap = new HashMap();

        public Pop3Folder(String name) {
            this.mName = name;
            if (this.mName.equalsIgnoreCase(Pop3Store.this.mStoreConfig.getInboxFolderName())) {
                this.mName = Pop3Store.this.mStoreConfig.getInboxFolderName();
            }
        }

        public synchronized void open(int mode) throws MessagingException {
            if (!isOpen()) {
                if (!this.mName.equalsIgnoreCase(Pop3Store.this.mStoreConfig.getInboxFolderName())) {
                    throw new MessagingException("Folder does not exist");
                }
                try {
                    SocketAddress socketAddress = new InetSocketAddress(Pop3Store.this.mHost, Pop3Store.this.mPort);
                    if (Pop3Store.this.mConnectionSecurity == ConnectionSecurity.SSL_TLS_REQUIRED) {
                        this.mSocket = Pop3Store.this.mTrustedSocketFactory.createSocket(null, Pop3Store.this.mHost, Pop3Store.this.mPort, Pop3Store.this.mClientCertificateAlias);
                    } else {
                        this.mSocket = new Socket();
                    }
                    this.mSocket.connect(socketAddress, 30000);
                    this.mIn = new BufferedInputStream(this.mSocket.getInputStream(), 1024);
                    this.mOut = new BufferedOutputStream(this.mSocket.getOutputStream(), 512);
                    this.mSocket.setSoTimeout(60000);
                    if (!isOpen()) {
                        throw new MessagingException("Unable to connect socket");
                    }
                    String serverGreeting = executeSimpleCommand(null);
                    Pop3Capabilities unused = Pop3Store.this.mCapabilities = getCapabilities();
                    if (Pop3Store.this.mConnectionSecurity == ConnectionSecurity.STARTTLS_REQUIRED) {
                        if (Pop3Store.this.mCapabilities.stls) {
                            executeSimpleCommand("STLS");
                            this.mSocket = Pop3Store.this.mTrustedSocketFactory.createSocket(this.mSocket, Pop3Store.this.mHost, Pop3Store.this.mPort, Pop3Store.this.mClientCertificateAlias);
                            this.mSocket.setSoTimeout(60000);
                            this.mIn = new BufferedInputStream(this.mSocket.getInputStream(), 1024);
                            this.mOut = new BufferedOutputStream(this.mSocket.getOutputStream(), 512);
                            if (!isOpen()) {
                                throw new MessagingException("Unable to connect socket");
                            }
                            Pop3Capabilities unused2 = Pop3Store.this.mCapabilities = getCapabilities();
                        } else {
                            throw new CertificateValidationException("STARTTLS connection security not available");
                        }
                    }
                    switch (Pop3Store.this.mAuthType) {
                        case PLAIN:
                            if (!Pop3Store.this.mCapabilities.authPlain) {
                                login();
                                break;
                            } else {
                                authPlain();
                                break;
                            }
                        case CRAM_MD5:
                            if (!Pop3Store.this.mCapabilities.cramMD5) {
                                authAPOP(serverGreeting);
                                break;
                            } else {
                                authCramMD5();
                                break;
                            }
                        case EXTERNAL:
                            if (Pop3Store.this.mCapabilities.external) {
                                authExternal();
                                break;
                            } else {
                                throw new CertificateValidationException(CertificateValidationException.Reason.MissingCapability);
                            }
                        default:
                            throw new MessagingException("Unhandled authentication method found in the server settings (bug).");
                    }
                    this.mMessageCount = Integer.parseInt(executeSimpleCommand(Pop3Store.STAT_COMMAND).split(" ")[1]);
                    this.mUidToMsgMap.clear();
                    this.mMsgNumToMsgMap.clear();
                    this.mUidToMsgNumMap.clear();
                } catch (SSLException e) {
                    if (e.getCause() instanceof CertificateException) {
                        throw new CertificateValidationException(e.getMessage(), e);
                    }
                    throw new MessagingException("Unable to connect", e);
                } catch (GeneralSecurityException gse) {
                    throw new MessagingException("Unable to open connection to POP server due to security error.", gse);
                } catch (IOException ioe) {
                    throw new MessagingException("Unable to open connection to POP server.", ioe);
                }
            }
        }

        private void login() throws MessagingException {
            executeSimpleCommand("USER " + Pop3Store.this.mUsername);
            try {
                executeSimpleCommand("PASS " + Pop3Store.this.mPassword, true);
            } catch (Pop3ErrorResponse e) {
                throw new AuthenticationFailedException("POP3 login authentication failed: " + e.getMessage(), e);
            }
        }

        private void authPlain() throws MessagingException {
            executeSimpleCommand("AUTH PLAIN");
            try {
                executeSimpleCommand(new String(Base64.encodeBase64(("\u0000" + Pop3Store.this.mUsername + "\u0000" + Pop3Store.this.mPassword).getBytes())), true);
            } catch (Pop3ErrorResponse e) {
                throw new AuthenticationFailedException("POP3 SASL auth PLAIN authentication failed: " + e.getMessage(), e);
            }
        }

        private void authAPOP(String serverGreeting) throws MessagingException {
            String timestamp = serverGreeting.replaceFirst("^\\+OK *(?:\\[[^\\]]+\\])?[^<]*(<[^>]*>)?[^<]*$", "$1");
            if ("".equals(timestamp)) {
                throw new MessagingException("APOP authentication is not supported");
            }
            try {
                try {
                    executeSimpleCommand("APOP " + Pop3Store.this.mUsername + " " + new String(Hex.encodeHex(MessageDigest.getInstance("MD5").digest((timestamp + Pop3Store.this.mPassword).getBytes()))), true);
                } catch (Pop3ErrorResponse e) {
                    throw new AuthenticationFailedException("POP3 APOP authentication failed: " + e.getMessage(), e);
                }
            } catch (NoSuchAlgorithmException e2) {
                throw new MessagingException("MD5 failure during POP3 auth APOP", e2);
            }
        }

        private void authCramMD5() throws MessagingException {
            try {
                executeSimpleCommand(Authentication.computeCramMd5(Pop3Store.this.mUsername, Pop3Store.this.mPassword, executeSimpleCommand("AUTH CRAM-MD5").replace("+ ", "")), true);
            } catch (Pop3ErrorResponse e) {
                throw new AuthenticationFailedException("POP3 CRAM-MD5 authentication failed: " + e.getMessage(), e);
            }
        }

        private void authExternal() throws MessagingException {
            try {
                executeSimpleCommand(String.format("AUTH EXTERNAL %s", Base64.encode(Pop3Store.this.mUsername)), false);
            } catch (Pop3ErrorResponse e) {
                throw new CertificateValidationException("POP3 client certificate authentication failed: " + e.getMessage(), e);
            }
        }

        public boolean isOpen() {
            return (this.mIn == null || this.mOut == null || this.mSocket == null || !this.mSocket.isConnected() || this.mSocket.isClosed()) ? false : true;
        }

        public int getMode() {
            return 0;
        }

        public void close() {
            try {
                if (isOpen()) {
                    executeSimpleCommand(Pop3Store.QUIT_COMMAND);
                }
            } catch (Exception e) {
            }
            closeIO();
        }

        private void closeIO() {
            try {
                this.mIn.close();
            } catch (Exception e) {
            }
            try {
                this.mOut.close();
            } catch (Exception e2) {
            }
            try {
                this.mSocket.close();
            } catch (Exception e3) {
            }
            this.mIn = null;
            this.mOut = null;
            this.mSocket = null;
        }

        public String getName() {
            return this.mName;
        }

        public boolean create(Folder.FolderType type) throws MessagingException {
            return false;
        }

        public boolean exists() throws MessagingException {
            return this.mName.equalsIgnoreCase(Pop3Store.this.mStoreConfig.getInboxFolderName());
        }

        public int getMessageCount() {
            return this.mMessageCount;
        }

        public int getUnreadMessageCount() throws MessagingException {
            return -1;
        }

        public int getFlaggedMessageCount() throws MessagingException {
            return -1;
        }

        public Pop3Message getMessage(String uid) throws MessagingException {
            Pop3Message message = this.mUidToMsgMap.get(uid);
            if (message == null) {
                return new Pop3Message(uid, this);
            }
            return message;
        }

        public List<Pop3Message> getMessages(int start, int end, Date earliestDate, MessageRetrievalListener<Pop3Message> listener) throws MessagingException {
            int i;
            if (start < 1 || end < 1 || end < start) {
                throw new MessagingException(String.format(Locale.US, "Invalid message set %d %d", Integer.valueOf(start), Integer.valueOf(end)));
            }
            try {
                indexMsgNums(start, end);
                List<Pop3Message> messages = new ArrayList<>();
                int msgNum = start;
                int i2 = 0;
                while (msgNum <= end) {
                    Pop3Message message = this.mMsgNumToMsgMap.get(Integer.valueOf(msgNum));
                    if (message == null) {
                        i = i2;
                    } else {
                        if (listener != null) {
                            i = i2 + 1;
                            listener.messageStarted(message.getUid(), i2, (end - start) + 1);
                        } else {
                            i = i2;
                        }
                        messages.add(message);
                        if (listener != null) {
                            listener.messageFinished(message, i, (end - start) + 1);
                            i++;
                        }
                    }
                    msgNum++;
                    i2 = i;
                }
                return messages;
            } catch (IOException ioe) {
                throw new MessagingException("getMessages", ioe);
            }
        }

        public boolean areMoreMessagesAvailable(int indexOfOldestMessage, Date earliestDate) {
            return indexOfOldestMessage > 1;
        }

        private void indexMsgNums(int start, int end) throws MessagingException, IOException {
            int unindexedMessageCount = 0;
            for (int msgNum = start; msgNum <= end; msgNum++) {
                if (this.mMsgNumToMsgMap.get(Integer.valueOf(msgNum)) == null) {
                    unindexedMessageCount++;
                }
            }
            if (unindexedMessageCount != 0) {
                if (unindexedMessageCount >= 50 || this.mMessageCount <= 5000) {
                    String executeSimpleCommand = executeSimpleCommand("UIDL");
                    while (true) {
                        String response = readLine();
                        if (response != null && !response.equals(".")) {
                            String[] uidParts = response.split(" +");
                            if (uidParts.length >= 3 && "+OK".equals(uidParts[0])) {
                                uidParts[0] = uidParts[1];
                                uidParts[1] = uidParts[2];
                            }
                            if (uidParts.length >= 2) {
                                Integer msgNum2 = Integer.valueOf(uidParts[0]);
                                String msgUid = uidParts[1];
                                if (msgNum2.intValue() >= start && msgNum2.intValue() <= end && this.mMsgNumToMsgMap.get(msgNum2) == null) {
                                    indexMessage(msgNum2.intValue(), new Pop3Message(msgUid, this));
                                }
                            }
                        } else {
                            return;
                        }
                    }
                } else {
                    for (int msgNum3 = start; msgNum3 <= end; msgNum3++) {
                        if (this.mMsgNumToMsgMap.get(Integer.valueOf(msgNum3)) == null) {
                            String response2 = executeSimpleCommand("UIDL " + msgNum3);
                            String[] uidParts2 = response2.split(" +");
                            if (uidParts2.length < 3 || !"+OK".equals(uidParts2[0])) {
                                Log.e("k9", "ERR response: " + response2);
                                return;
                            }
                            indexMessage(msgNum3, new Pop3Message(uidParts2[2], this));
                        }
                    }
                }
            }
        }

        private void indexUids(List<String> uids) throws MessagingException, IOException {
            Set<String> unindexedUids = new HashSet<>();
            for (String uid : uids) {
                if (this.mUidToMsgMap.get(uid) == null) {
                    if (K9MailLib.isDebug() && K9MailLib.DEBUG_PROTOCOL_POP3) {
                        Log.d("k9", "Need to index UID " + uid);
                    }
                    unindexedUids.add(uid);
                }
            }
            if (!unindexedUids.isEmpty()) {
                String executeSimpleCommand = executeSimpleCommand("UIDL");
                while (true) {
                    String response = readLine();
                    if (response != null && !response.equals(".")) {
                        String[] uidParts = response.split(" +");
                        if (uidParts.length >= 2) {
                            Integer msgNum = Integer.valueOf(uidParts[0]);
                            String msgUid = uidParts[1];
                            if (unindexedUids.contains(msgUid)) {
                                if (K9MailLib.isDebug() && K9MailLib.DEBUG_PROTOCOL_POP3) {
                                    Log.d("k9", "Got msgNum " + msgNum + " for UID " + msgUid);
                                }
                                Pop3Message message = this.mUidToMsgMap.get(msgUid);
                                if (message == null) {
                                    message = new Pop3Message(msgUid, this);
                                }
                                indexMessage(msgNum.intValue(), message);
                            }
                        }
                    } else {
                        return;
                    }
                }
            }
        }

        private void indexMessage(int msgNum, Pop3Message message) {
            if (K9MailLib.isDebug() && K9MailLib.DEBUG_PROTOCOL_POP3) {
                Log.d("k9", "Adding index for UID " + message.getUid() + " to msgNum " + msgNum);
            }
            this.mMsgNumToMsgMap.put(Integer.valueOf(msgNum), message);
            this.mUidToMsgMap.put(message.getUid(), message);
            this.mUidToMsgNumMap.put(message.getUid(), Integer.valueOf(msgNum));
        }

        public void fetch(List<Pop3Message> messages, FetchProfile fp, MessageRetrievalListener<Pop3Message> listener) throws MessagingException {
            MessageRetrievalListener<Pop3Message> messageRetrievalListener = null;
            if (messages != null && !messages.isEmpty()) {
                List<String> uids = new ArrayList<>();
                for (Pop3Message message : messages) {
                    uids.add(message.getUid());
                }
                try {
                    indexUids(uids);
                    try {
                        if (fp.contains(FetchProfile.Item.ENVELOPE)) {
                            if (fp.size() == 1) {
                                messageRetrievalListener = listener;
                            }
                            fetchEnvelope(messages, messageRetrievalListener);
                        }
                        int count = messages.size();
                        for (int i = 0; i < count; i++) {
                            Pop3Message pop3Message = messages.get(i);
                            if (listener != null) {
                                try {
                                    if (!fp.contains(FetchProfile.Item.ENVELOPE)) {
                                        listener.messageStarted(pop3Message.getUid(), i, count);
                                    }
                                } catch (IOException ioe) {
                                    throw new MessagingException("Unable to fetch message", ioe);
                                }
                            }
                            if (fp.contains(FetchProfile.Item.BODY)) {
                                fetchBody(pop3Message, -1);
                            } else if (fp.contains(FetchProfile.Item.BODY_SANE)) {
                                if (Pop3Store.this.mStoreConfig.getMaximumAutoDownloadMessageSize() > 0) {
                                    fetchBody(pop3Message, Pop3Store.this.mStoreConfig.getMaximumAutoDownloadMessageSize() / 76);
                                } else {
                                    fetchBody(pop3Message, -1);
                                }
                            } else if (fp.contains(FetchProfile.Item.STRUCTURE)) {
                                pop3Message.setBody(null);
                            }
                            if (listener != null && (!fp.contains(FetchProfile.Item.ENVELOPE) || fp.size() != 1)) {
                                listener.messageFinished(pop3Message, i, count);
                            }
                        }
                    } catch (IOException ioe2) {
                        throw new MessagingException("fetch", ioe2);
                    }
                } catch (IOException ioe3) {
                    throw new MessagingException("fetch", ioe3);
                }
            }
        }

        private void fetchEnvelope(List<Pop3Message> messages, MessageRetrievalListener<Pop3Message> listener) throws IOException, MessagingException {
            int unsizedMessages = 0;
            for (Pop3Message message : messages) {
                if (message.getSize() == -1) {
                    unsizedMessages++;
                }
            }
            if (unsizedMessages != 0) {
                if (unsizedMessages >= 50 || this.mMessageCount <= 5000) {
                    Set<String> msgUidIndex = new HashSet<>();
                    for (Pop3Message message2 : messages) {
                        msgUidIndex.add(message2.getUid());
                    }
                    int i = 0;
                    int count = messages.size();
                    String executeSimpleCommand = executeSimpleCommand("LIST");
                    while (true) {
                        String response = readLine();
                        if (response != null && !response.equals(".")) {
                            String[] listParts = response.split(" ");
                            int msgNum = Integer.parseInt(listParts[0]);
                            int msgSize = Integer.parseInt(listParts[1]);
                            Pop3Message pop3Message = this.mMsgNumToMsgMap.get(Integer.valueOf(msgNum));
                            if (pop3Message != null && msgUidIndex.contains(pop3Message.getUid())) {
                                if (listener != null) {
                                    listener.messageStarted(pop3Message.getUid(), i, count);
                                }
                                pop3Message.setSize(msgSize);
                                if (listener != null) {
                                    listener.messageFinished(pop3Message, i, count);
                                }
                                i++;
                            }
                        } else {
                            return;
                        }
                    }
                } else {
                    int count2 = messages.size();
                    for (int i2 = 0; i2 < count2; i2++) {
                        Pop3Message message3 = messages.get(i2);
                        if (listener != null) {
                            listener.messageStarted(message3.getUid(), i2, count2);
                        }
                        message3.setSize(Integer.parseInt(executeSimpleCommand(String.format(Locale.US, "LIST %d", this.mUidToMsgNumMap.get(message3.getUid()))).split(" ")[2]));
                        if (listener != null) {
                            listener.messageFinished(message3, i2, count2);
                        }
                    }
                }
            }
        }

        private void fetchBody(Pop3Message message, int lines) throws IOException, MessagingException {
            String response = null;
            if (lines != -1 && (!Pop3Store.this.mTopNotSupported || Pop3Store.this.mCapabilities.top)) {
                try {
                    if (K9MailLib.isDebug() && K9MailLib.DEBUG_PROTOCOL_POP3 && !Pop3Store.this.mCapabilities.top) {
                        Log.d("k9", "This server doesn't support the CAPA command. Checking to see if the TOP command is supported nevertheless.");
                    }
                    response = executeSimpleCommand(String.format(Locale.US, "TOP %d %d", this.mUidToMsgNumMap.get(message.getUid()), Integer.valueOf(lines)));
                    Pop3Store.this.mCapabilities.top = true;
                } catch (Pop3ErrorResponse e) {
                    if (Pop3Store.this.mCapabilities.top) {
                        throw e;
                    }
                    if (K9MailLib.isDebug() && K9MailLib.DEBUG_PROTOCOL_POP3) {
                        Log.d("k9", "The server really doesn't support the TOP command. Using RETR instead.");
                    }
                    boolean unused = Pop3Store.this.mTopNotSupported = true;
                }
            }
            if (response == null) {
                executeSimpleCommand(String.format(Locale.US, "RETR %d", this.mUidToMsgNumMap.get(message.getUid())));
            }
            try {
                message.parse(new Pop3ResponseInputStream(this.mIn));
                if (lines == -1 || !Pop3Store.this.mCapabilities.top) {
                    message.setFlag(Flag.X_DOWNLOADED_FULL, true);
                }
            } catch (MessagingException me2) {
                if (lines == -1) {
                    throw me2;
                }
            }
        }

        public Map<String, String> appendMessages(List<? extends Message> list) throws MessagingException {
            return null;
        }

        public void delete(boolean recurse) throws MessagingException {
        }

        public void delete(List<? extends Message> msgs, String trashFolderName) throws MessagingException {
            setFlags(msgs, Collections.singleton(Flag.DELETED), true);
        }

        public String getUidFromMessageId(Message message) throws MessagingException {
            return null;
        }

        public void setFlags(Set<Flag> set, boolean value) throws MessagingException {
            throw new UnsupportedOperationException("POP3: No setFlags(Set<Flag>,boolean)");
        }

        public void setFlags(List<? extends Message> messages, Set<Flag> flags, boolean value) throws MessagingException {
            if (value && flags.contains(Flag.DELETED)) {
                List<String> uids = new ArrayList<>();
                try {
                    for (Message message : messages) {
                        uids.add(message.getUid());
                    }
                    indexUids(uids);
                    for (Message message2 : messages) {
                        Integer msgNum = this.mUidToMsgNumMap.get(message2.getUid());
                        if (msgNum == null) {
                            MessagingException me2 = new MessagingException("Could not delete message " + message2.getUid() + " because no msgNum found; permanent error");
                            me2.setPermanentFailure(true);
                            throw me2;
                        }
                        executeSimpleCommand(String.format("DELE %s", msgNum));
                    }
                } catch (IOException ioe) {
                    throw new MessagingException("Could not get message number for uid " + uids, ioe);
                }
            }
        }

        private String readLine() throws IOException {
            StringBuilder sb = new StringBuilder();
            int d = this.mIn.read();
            if (d == -1) {
                throw new IOException("End of stream reached while trying to read line.");
            }
            do {
                if (((char) d) != 13) {
                    if (((char) d) == 10) {
                        break;
                    }
                    sb.append((char) d);
                }
                d = this.mIn.read();
            } while (d != -1);
            String ret = sb.toString();
            if (K9MailLib.isDebug() && K9MailLib.DEBUG_PROTOCOL_POP3) {
                Log.d("k9", "<<< " + ret);
            }
            return ret;
        }

        private void writeLine(String s) throws IOException {
            this.mOut.write(s.getBytes());
            this.mOut.write(13);
            this.mOut.write(10);
            this.mOut.flush();
        }

        private Pop3Capabilities getCapabilities() throws IOException {
            Pop3Capabilities capabilities = new Pop3Capabilities();
            try {
                String executeSimpleCommand = executeSimpleCommand(Pop3Store.AUTH_COMMAND);
                while (true) {
                    String response = readLine();
                    if (response == null || response.equals(".")) {
                        break;
                    }
                    String response2 = response.toUpperCase(Locale.US);
                    if (response2.equals(Pop3Store.AUTH_PLAIN_CAPABILITY)) {
                        capabilities.authPlain = true;
                    } else if (response2.equals(Pop3Store.AUTH_CRAM_MD5_CAPABILITY)) {
                        capabilities.cramMD5 = true;
                    } else if (response2.equals(Pop3Store.AUTH_EXTERNAL_CAPABILITY)) {
                        capabilities.external = true;
                    }
                }
            } catch (MessagingException e) {
            }
            try {
                String executeSimpleCommand2 = executeSimpleCommand(Pop3Store.CAPA_COMMAND);
                while (true) {
                    String response3 = readLine();
                    if (response3 != null && !response3.equals(".")) {
                        String response4 = response3.toUpperCase(Locale.US);
                        if (response4.equals("STLS")) {
                            capabilities.stls = true;
                        } else if (response4.equals("UIDL")) {
                            capabilities.uidl = true;
                        } else if (response4.equals("TOP")) {
                            capabilities.top = true;
                        } else if (response4.startsWith(Pop3Store.SASL_CAPABILITY)) {
                            List<String> saslAuthMechanisms = Arrays.asList(response4.split(" "));
                            if (saslAuthMechanisms.contains(Pop3Store.AUTH_PLAIN_CAPABILITY)) {
                                capabilities.authPlain = true;
                            }
                            if (saslAuthMechanisms.contains(Pop3Store.AUTH_CRAM_MD5_CAPABILITY)) {
                                capabilities.cramMD5 = true;
                            }
                        }
                    }
                }
                if (!capabilities.top) {
                    boolean unused = Pop3Store.this.mTopNotSupported = true;
                }
            } catch (MessagingException e2) {
            }
            return capabilities;
        }

        /* access modifiers changed from: private */
        public String executeSimpleCommand(String command) throws MessagingException {
            return executeSimpleCommand(command, false);
        }

        private String executeSimpleCommand(String command, boolean sensitive) throws MessagingException {
            try {
                open(0);
                if (command != null) {
                    if (K9MailLib.isDebug() && K9MailLib.DEBUG_PROTOCOL_POP3) {
                        if (!sensitive || K9MailLib.isDebugSensitive()) {
                            Log.d("k9", ">>> " + command);
                        } else {
                            Log.d("k9", ">>> [Command Hidden, Enable Sensitive Debug Logging To Show]");
                        }
                    }
                    writeLine(command);
                }
                String response = readLine();
                if (response.length() != 0 && response.charAt(0) == '+') {
                    return response;
                }
                throw new Pop3ErrorResponse(response);
            } catch (MessagingException me2) {
                throw me2;
            } catch (Exception e) {
                closeIO();
                throw new MessagingException("Unable to execute POP3 command", e);
            }
        }

        public boolean isFlagSupported(Flag flag) {
            return flag == Flag.DELETED;
        }

        public boolean supportsFetchingFlags() {
            return false;
        }

        public boolean equals(Object o) {
            if (o instanceof Pop3Folder) {
                return ((Pop3Folder) o).mName.equals(this.mName);
            }
            return super.equals(o);
        }

        public int hashCode() {
            return this.mName.hashCode();
        }
    }

    static class Pop3Message extends MimeMessage {
        Pop3Message(String uid, Pop3Folder folder) {
            this.mUid = uid;
            this.mFolder = folder;
            this.mSize = -1;
        }

        public void setSize(int size) {
            this.mSize = size;
        }

        public void setFlag(Flag flag, boolean set) throws MessagingException {
            super.setFlag(flag, set);
            this.mFolder.setFlags(Collections.singletonList(this), Collections.singleton(flag), set);
        }

        public void delete(String trashFolderName) throws MessagingException {
            setFlag(Flag.DELETED, true);
        }
    }

    static class Pop3Capabilities {
        public boolean authPlain;
        public boolean cramMD5;
        public boolean external;
        public boolean stls;
        public boolean top;
        public boolean uidl;

        Pop3Capabilities() {
        }

        public String toString() {
            return String.format("CRAM-MD5 %b, PLAIN %b, STLS %b, TOP %b, UIDL %b, EXTERNAL %b", Boolean.valueOf(this.cramMD5), Boolean.valueOf(this.authPlain), Boolean.valueOf(this.stls), Boolean.valueOf(this.top), Boolean.valueOf(this.uidl), Boolean.valueOf(this.external));
        }
    }

    static class Pop3ResponseInputStream extends InputStream {
        private boolean mFinished;
        private InputStream mIn;
        private boolean mStartOfLine = true;

        public Pop3ResponseInputStream(InputStream in) {
            this.mIn = in;
        }

        public int read() throws IOException {
            boolean z = true;
            if (this.mFinished) {
                return -1;
            }
            int d = this.mIn.read();
            if (this.mStartOfLine && d == 46 && (d = this.mIn.read()) == 13) {
                this.mFinished = true;
                this.mIn.read();
                return -1;
            }
            if (d != 10) {
                z = false;
            }
            this.mStartOfLine = z;
            return d;
        }
    }

    static class Pop3ErrorResponse extends MessagingException {
        private static final long serialVersionUID = 3672087845857867174L;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.fsck.k9.mail.MessagingException.<init>(java.lang.String, boolean):void
         arg types: [java.lang.String, int]
         candidates:
          com.fsck.k9.mail.MessagingException.<init>(java.lang.String, java.lang.Throwable):void
          com.fsck.k9.mail.MessagingException.<init>(java.lang.String, boolean):void */
        public Pop3ErrorResponse(String message) {
            super(message, true);
        }
    }
}
