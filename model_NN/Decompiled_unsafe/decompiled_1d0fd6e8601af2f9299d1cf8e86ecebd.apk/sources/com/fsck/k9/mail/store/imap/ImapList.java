package com.fsck.k9.mail.store.imap;

import com.fsck.k9.mail.MessagingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

class ImapList extends ArrayList<Object> {
    private static final DateFormat BAD_DATE_TIME_FORMAT = new SimpleDateFormat("dd MMM yyyy HH:mm:ss Z", Locale.US);
    private static final DateFormat BAD_DATE_TIME_FORMAT_2 = new SimpleDateFormat("E, dd MMM yyyy HH:mm:ss Z", Locale.US);
    private static final DateFormat BAD_DATE_TIME_FORMAT_3 = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", Locale.US);
    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss Z", Locale.US);
    private static final long serialVersionUID = -4067248341419617583L;

    ImapList() {
    }

    public ImapList getList(int index) {
        return (ImapList) get(index);
    }

    public boolean isList(int index) {
        return inRange(index) && (get(index) instanceof ImapList);
    }

    public Object getObject(int index) {
        return get(index);
    }

    public String getString(int index) {
        return (String) get(index);
    }

    public boolean isString(int index) {
        return inRange(index) && (get(index) instanceof String);
    }

    public long getLong(int index) {
        return Long.parseLong(getString(index));
    }

    public int getNumber(int index) {
        return Integer.parseInt(getString(index));
    }

    public Date getDate(int index) throws MessagingException {
        return getDate(getString(index));
    }

    public Date getKeyedDate(String key) throws MessagingException {
        return getDate(getKeyedString(key));
    }

    private Date getDate(String value) throws MessagingException {
        if (value == null) {
            return null;
        }
        try {
            return parseDate(value);
        } catch (ParseException pe) {
            throw new MessagingException("Unable to parse IMAP datetime '" + value + "' ", pe);
        }
    }

    public Object getKeyedValue(String key) {
        int count = size() - 1;
        for (int i = 0; i < count; i++) {
            if (ImapResponseParser.equalsIgnoreCase(get(i), key)) {
                return get(i + 1);
            }
        }
        return null;
    }

    public ImapList getKeyedList(String key) {
        return (ImapList) getKeyedValue(key);
    }

    public String getKeyedString(String key) {
        return (String) getKeyedValue(key);
    }

    public int getKeyedNumber(String key) {
        return Integer.parseInt(getKeyedString(key));
    }

    public boolean containsKey(String key) {
        if (key == null) {
            return false;
        }
        int count = size() - 1;
        for (int i = 0; i < count; i++) {
            if (ImapResponseParser.equalsIgnoreCase(get(i), key)) {
                return true;
            }
        }
        return false;
    }

    public int getKeyIndex(String key) {
        int count = size() - 1;
        for (int i = 0; i < count; i++) {
            if (ImapResponseParser.equalsIgnoreCase(get(i), key)) {
                return i;
            }
        }
        throw new IllegalArgumentException("getKeyIndex() only works for keys that are in the collection.");
    }

    private boolean inRange(int index) {
        return index >= 0 && index < size();
    }

    private Date parseDate(String value) throws ParseException {
        Date parse;
        try {
            synchronized (DATE_FORMAT) {
                parse = DATE_FORMAT.parse(value);
            }
        } catch (Exception e) {
            try {
                synchronized (BAD_DATE_TIME_FORMAT) {
                    parse = BAD_DATE_TIME_FORMAT.parse(value);
                }
            } catch (Exception e2) {
                try {
                    synchronized (BAD_DATE_TIME_FORMAT_2) {
                        parse = BAD_DATE_TIME_FORMAT_2.parse(value);
                    }
                } catch (Exception e3) {
                    synchronized (BAD_DATE_TIME_FORMAT_3) {
                        parse = BAD_DATE_TIME_FORMAT_3.parse(value);
                    }
                }
            }
        }
        return parse;
    }
}
