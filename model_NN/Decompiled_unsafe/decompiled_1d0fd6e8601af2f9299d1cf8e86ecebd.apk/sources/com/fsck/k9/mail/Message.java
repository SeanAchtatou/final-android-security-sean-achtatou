package com.fsck.k9.mail;

import android.support.annotation.NonNull;
import android.util.Log;
import com.fsck.k9.mail.filter.CountingOutputStream;
import com.fsck.k9.mail.filter.EOLConvertingOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.EnumSet;
import java.util.Set;

public abstract class Message implements Part, Body {
    private Set<Flag> mFlags = EnumSet.noneOf(Flag.class);
    /* access modifiers changed from: protected */
    public Folder mFolder;
    private Date mInternalDate;
    protected String mUid;

    public enum RecipientType {
        TO,
        CC,
        BCC
    }

    public abstract void addHeader(String str, String str2);

    public abstract void addRawHeader(String str, String str2);

    public abstract Message clone();

    public abstract Body getBody();

    public abstract Address[] getFrom();

    @NonNull
    public abstract String[] getHeader(String str);

    public abstract Set<String> getHeaderNames();

    public abstract long getId();

    public abstract String getMessageId();

    public abstract Address[] getRecipients(RecipientType recipientType);

    public abstract String[] getReferences();

    public abstract Address[] getReplyTo();

    public abstract Date getSentDate();

    public abstract int getSize();

    public abstract String getSubject();

    public abstract boolean hasAttachments();

    public abstract void removeHeader(String str);

    public abstract void setBody(Body body);

    public abstract void setCharset(String str) throws MessagingException;

    public abstract void setEncoding(String str) throws MessagingException;

    public abstract void setFrom(Address address);

    public abstract void setHeader(String str, String str2);

    public abstract void setInReplyTo(String str);

    public abstract void setRecipients(RecipientType recipientType, Address[] addressArr);

    public abstract void setReferences(String str);

    public abstract void setReplyTo(Address[] addressArr);

    public abstract void setSentDate(Date date, boolean z);

    public abstract void setSubject(String str);

    public boolean olderThan(Date earliestDate) {
        if (earliestDate == null) {
            return false;
        }
        Date myDate = getSentDate();
        if (myDate == null) {
            myDate = getInternalDate();
        }
        if (myDate != null) {
            return myDate.before(earliestDate);
        }
        return false;
    }

    public boolean equals(Object o) {
        if (o == null || !(o instanceof Message)) {
            return false;
        }
        Message other = (Message) o;
        if (!getUid().equals(other.getUid()) || !getFolder().getName().equals(other.getFolder().getName())) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return ((this.mFolder.getName().hashCode() + 31) * 31) + this.mUid.hashCode();
    }

    public String getUid() {
        return this.mUid;
    }

    public void setUid(String uid) {
        this.mUid = uid;
    }

    public Folder getFolder() {
        return this.mFolder;
    }

    public Date getInternalDate() {
        return this.mInternalDate;
    }

    public void setInternalDate(Date internalDate) {
        this.mInternalDate = internalDate;
    }

    public void setRecipient(RecipientType type, Address address) {
        setRecipients(type, new Address[]{address});
    }

    public void delete(String trashFolderName) throws MessagingException {
    }

    public Set<Flag> getFlags() {
        return Collections.unmodifiableSet(this.mFlags);
    }

    public void setFlag(Flag flag, boolean set) throws MessagingException {
        if (set) {
            this.mFlags.add(flag);
        } else {
            this.mFlags.remove(flag);
        }
    }

    public void setFlags(Set<Flag> flags, boolean set) throws MessagingException {
        for (Flag flag : flags) {
            setFlag(flag, set);
        }
    }

    public boolean isSet(Flag flag) {
        return this.mFlags.contains(flag);
    }

    public void destroy() throws MessagingException {
    }

    public long calculateSize() {
        try {
            CountingOutputStream out = new CountingOutputStream();
            EOLConvertingOutputStream eolOut = new EOLConvertingOutputStream(out);
            writeTo(eolOut);
            eolOut.flush();
            return out.getCount();
        } catch (IOException e) {
            Log.e("k9", "Failed to calculate a message size", e);
        } catch (MessagingException e2) {
            Log.e("k9", "Failed to calculate a message size", e2);
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public void copy(Message destination) {
        destination.mUid = this.mUid;
        destination.mInternalDate = this.mInternalDate;
        destination.mFolder = this.mFolder;
        destination.mFlags = EnumSet.copyOf(this.mFlags);
    }
}
