package com.fsck.k9.mail;

public enum AuthType {
    PLAIN,
    CRAM_MD5,
    EXTERNAL,
    XOAUTH2,
    AUTOMATIC,
    LOGIN
}
