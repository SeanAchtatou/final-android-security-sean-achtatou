package com.fsck.k9.mail;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

public class CertificateChainException extends CertificateException {
    private static final long serialVersionUID = 1103894512106650107L;
    private X509Certificate[] mCertChain;

    public CertificateChainException(String msg, X509Certificate[] chain, Throwable cause) {
        super(msg, cause);
        setCertChain(chain);
    }

    public void setCertChain(X509Certificate[] chain) {
        this.mCertChain = chain;
    }

    public X509Certificate[] getCertChain() {
        return this.mCertChain;
    }
}
