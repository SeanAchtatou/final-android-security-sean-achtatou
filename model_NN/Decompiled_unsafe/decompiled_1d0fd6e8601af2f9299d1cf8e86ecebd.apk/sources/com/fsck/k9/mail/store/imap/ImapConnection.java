package com.fsck.k9.mail.store.imap;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import com.fsck.k9.mail.Authentication;
import com.fsck.k9.mail.AuthenticationFailedException;
import com.fsck.k9.mail.CertificateValidationException;
import com.fsck.k9.mail.ConnectionSecurity;
import com.fsck.k9.mail.K9MailLib;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.NetworkType;
import com.fsck.k9.mail.filter.Base64;
import com.fsck.k9.mail.filter.PeekableInputStream;
import com.fsck.k9.mail.oauth.OAuth2TokenProvider;
import com.fsck.k9.mail.ssl.TrustedSocketFactory;
import com.jcraft.jzlib.ZOutputStream;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.security.GeneralSecurityException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.security.cert.CertificateException;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;
import javax.net.ssl.SSLException;
import org.apache.commons.io.IOUtils;

class ImapConnection {
    private static final int BUFFER_SIZE = 1024;
    private String authToken = null;
    private boolean authTokenExpired = false;
    private Set<String> capabilities = new HashSet();
    private final ConnectivityManager connectivityManager;
    private PeekableInputStream inputStream;
    private int nextCommandTag;
    private final OAuth2TokenProvider oauthTokenProvider;
    private boolean open = false;
    /* access modifiers changed from: private */
    public OutputStream outputStream;
    private ImapResponseParser responseParser;
    private ImapSettings settings;
    private Socket socket;
    private final int socketConnectTimeout;
    private final TrustedSocketFactory socketFactory;
    private final int socketReadTimeout;
    private Exception stacktraceForClose;

    public ImapConnection(ImapSettings settings2, TrustedSocketFactory socketFactory2, ConnectivityManager connectivityManager2, OAuth2TokenProvider oauthTokenProvider2) {
        this.settings = settings2;
        this.socketFactory = socketFactory2;
        this.connectivityManager = connectivityManager2;
        this.oauthTokenProvider = oauthTokenProvider2;
        this.socketConnectTimeout = 30000;
        this.socketReadTimeout = 60000;
    }

    ImapConnection(ImapSettings settings2, TrustedSocketFactory socketFactory2, ConnectivityManager connectivityManager2, OAuth2TokenProvider oauthTokenProvider2, int socketConnectTimeout2, int socketReadTimeout2) {
        this.settings = settings2;
        this.socketFactory = socketFactory2;
        this.connectivityManager = connectivityManager2;
        this.oauthTokenProvider = oauthTokenProvider2;
        this.socketConnectTimeout = socketConnectTimeout2;
        this.socketReadTimeout = socketReadTimeout2;
    }

    public void open() throws IOException, MessagingException {
        if (!this.open) {
            if (this.stacktraceForClose != null) {
                throw new IllegalStateException("open() called after close(). Check wrapped exception to see where close() was called.", this.stacktraceForClose);
            }
            this.open = true;
            boolean authSuccess = false;
            this.nextCommandTag = 1;
            adjustDNSCacheTTL();
            try {
                this.socket = connect();
                configureSocket();
                setUpStreamsAndParserFromSocket();
                readInitialResponse();
                requestCapabilitiesIfNecessary();
                upgradeToTlsIfNecessary();
                authenticate();
                authSuccess = true;
                enableCompressionIfRequested();
                retrievePathPrefixIfNecessary();
                retrievePathDelimiterIfNecessary();
                if (1 == 0) {
                    Log.e("k9", "Failed to login, closing connection for " + getLogId());
                    close();
                }
            } catch (SSLException e) {
                handleSslException(e);
                if (!authSuccess) {
                    Log.e("k9", "Failed to login, closing connection for " + getLogId());
                    close();
                }
            } catch (ConnectException e2) {
                handleConnectException(e2);
                if (!authSuccess) {
                    Log.e("k9", "Failed to login, closing connection for " + getLogId());
                    close();
                }
            } catch (GeneralSecurityException e3) {
                throw new MessagingException("Unable to open connection to IMAP server due to security error.", e3);
            } catch (Throwable th) {
                if (!authSuccess) {
                    Log.e("k9", "Failed to login, closing connection for " + getLogId());
                    close();
                }
                throw th;
            }
        }
    }

    private void handleSslException(SSLException e) throws CertificateValidationException, SSLException {
        if (e.getCause() instanceof CertificateException) {
            throw new CertificateValidationException(e.getMessage(), e);
        }
        throw e;
    }

    private void handleConnectException(ConnectException e) throws ConnectException {
        String[] tokens = e.getMessage().split("-");
        if (tokens.length <= 1 || tokens[1] == null) {
            throw e;
        }
        Log.e("k9", "Stripping host/port from ConnectionException for " + getLogId(), e);
        throw new ConnectException(tokens[1].trim());
    }

    public boolean isConnected() {
        return (this.inputStream == null || this.outputStream == null || this.socket == null || !this.socket.isConnected() || this.socket.isClosed()) ? false : true;
    }

    private void adjustDNSCacheTTL() {
        try {
            Security.setProperty("networkaddress.cache.ttl", "0");
        } catch (Exception e) {
            Log.w("k9", "Could not set DNS ttl to 0 for " + getLogId(), e);
        }
        try {
            Security.setProperty("networkaddress.cache.negative.ttl", "0");
        } catch (Exception e2) {
            Log.w("k9", "Could not set DNS negative ttl to 0 for " + getLogId(), e2);
        }
    }

    private Socket connect() throws GeneralSecurityException, MessagingException, IOException {
        Exception connectException = null;
        InetAddress[] inetAddresses = InetAddress.getAllByName(this.settings.getHost());
        int length = inetAddresses.length;
        int i = 0;
        while (i < length) {
            InetAddress address = inetAddresses[i];
            try {
                return connectToAddress(address);
            } catch (IOException e) {
                Log.w("k9", "Could not connect to " + address, e);
                connectException = e;
                i++;
            }
        }
        throw new MessagingException("Cannot connect to host", connectException);
    }

    private Socket connectToAddress(InetAddress address) throws NoSuchAlgorithmException, KeyManagementException, MessagingException, IOException {
        Socket socket2;
        String host = this.settings.getHost();
        int port = this.settings.getPort();
        String clientCertificateAlias = this.settings.getClientCertificateAlias();
        if (K9MailLib.isDebug() && K9MailLib.DEBUG_PROTOCOL_IMAP) {
            Log.d("k9", "Connecting to " + host + " as " + address);
        }
        SocketAddress socketAddress = new InetSocketAddress(address, port);
        if (this.settings.getConnectionSecurity() == ConnectionSecurity.SSL_TLS_REQUIRED) {
            socket2 = this.socketFactory.createSocket(null, host, port, clientCertificateAlias);
        } else {
            socket2 = new Socket();
        }
        socket2.connect(socketAddress, this.socketConnectTimeout);
        return socket2;
    }

    private void configureSocket() throws SocketException {
        this.socket.setSoTimeout(this.socketReadTimeout);
    }

    private void setUpStreamsAndParserFromSocket() throws IOException {
        setUpStreamsAndParser(this.socket.getInputStream(), this.socket.getOutputStream());
    }

    private void setUpStreamsAndParser(InputStream input, OutputStream output) {
        this.inputStream = new PeekableInputStream(new BufferedInputStream(input, 1024));
        this.responseParser = new ImapResponseParser(this.inputStream);
        this.outputStream = new BufferedOutputStream(output, 1024);
    }

    private void readInitialResponse() throws IOException {
        ImapResponse initialResponse = this.responseParser.readResponse();
        if (K9MailLib.isDebug() && K9MailLib.DEBUG_PROTOCOL_IMAP) {
            Log.v("k9", getLogId() + "<<<" + initialResponse);
        }
        extractCapabilities(Collections.singletonList(initialResponse));
    }

    private List<ImapResponse> extractCapabilities(List<ImapResponse> responses) {
        CapabilityResponse capabilityResponse = CapabilityResponse.parse(responses);
        if (capabilityResponse != null) {
            Set<String> receivedCapabilities = capabilityResponse.getCapabilities();
            if (K9MailLib.isDebug()) {
                Log.d("k9", "Saving " + receivedCapabilities + " capabilities for " + getLogId());
            }
            this.capabilities = receivedCapabilities;
        }
        return responses;
    }

    private void requestCapabilitiesIfNecessary() throws IOException, MessagingException {
        if (this.capabilities.isEmpty()) {
            if (K9MailLib.isDebug()) {
                Log.i("k9", "Did not get capabilities in banner, requesting CAPABILITY for " + getLogId());
            }
            requestCapabilities();
        }
    }

    private void requestCapabilities() throws IOException, MessagingException {
        if (extractCapabilities(executeSimpleCommand("CAPABILITY")).size() != 2) {
            throw new MessagingException("Invalid CAPABILITY response received");
        }
    }

    private void upgradeToTlsIfNecessary() throws IOException, MessagingException, GeneralSecurityException {
        if (this.settings.getConnectionSecurity() == ConnectionSecurity.STARTTLS_REQUIRED) {
            upgradeToTls();
        }
    }

    private void upgradeToTls() throws IOException, MessagingException, GeneralSecurityException {
        if (!hasCapability("STARTTLS")) {
            throw new CertificateValidationException("STARTTLS connection security not available");
        }
        startTLS();
    }

    private void startTLS() throws IOException, MessagingException, GeneralSecurityException {
        executeSimpleCommand("STARTTLS");
        this.socket = this.socketFactory.createSocket(this.socket, this.settings.getHost(), this.settings.getPort(), this.settings.getClientCertificateAlias());
        configureSocket();
        setUpStreamsAndParserFromSocket();
        if (K9MailLib.isDebug()) {
            Log.i("k9", "Updating capabilities after STARTTLS for " + getLogId());
        }
        requestCapabilities();
    }

    private void authenticate() throws MessagingException, IOException {
        switch (this.settings.getAuthType()) {
            case XOAUTH2:
                if (!hasCapability(Capabilities.AUTH_XOAUTH2) || !hasCapability(Capabilities.SASL_IR)) {
                    throw new MessagingException("Server doesn't support SASL XOAUTH2.");
                }
                authXoauth2withSASLIR();
                return;
            case CRAM_MD5:
                if (hasCapability(Capabilities.AUTH_CRAM_MD5)) {
                    authCramMD5();
                    return;
                }
                throw new MessagingException("Server doesn't support encrypted passwords using CRAM-MD5.");
            case PLAIN:
                if (hasCapability(Capabilities.AUTH_PLAIN)) {
                    saslAuthPlainWithLoginFallback();
                    return;
                } else if (!hasCapability(Capabilities.LOGINDISABLED)) {
                    login();
                    return;
                } else {
                    throw new MessagingException("Server doesn't support unencrypted passwords using AUTH=PLAIN and LOGIN is disabled.");
                }
            case EXTERNAL:
                if (hasCapability(Capabilities.AUTH_EXTERNAL)) {
                    saslAuthExternal();
                    return;
                }
                throw new CertificateValidationException(CertificateValidationException.Reason.MissingCapability);
            default:
                throw new MessagingException("Unhandled authentication method found in the server settings (bug).");
        }
    }

    private void authXoauth2withSASLIR() throws IOException, MessagingException {
        try {
            attemptXOAuth2();
        } catch (NegativeImapResponseException e) {
            Log.v("k9", "Authentication exception, invalidating token and re-trying", e);
            this.oauthTokenProvider.invalidateToken(this.settings.getUsername());
            try {
                attemptXOAuth2();
            } catch (NegativeImapResponseException e2) {
                Log.v("k9", "Authentication exception for new token, permanent error assumed", e);
                this.oauthTokenProvider.invalidateToken(this.settings.getUsername());
                throw new AuthenticationFailedException(e2.getMessage(), e);
            }
        }
    }

    private void attemptXOAuth2() throws MessagingException, IOException {
        extractCapabilities(this.responseParser.readStatusResponse(sendSaslIrCommand(Commands.AUTHENTICATE_XOAUTH2, Authentication.computeXoauth(this.settings.getUsername(), this.oauthTokenProvider.getToken(this.settings.getUsername(), 30000)), true), Commands.AUTHENTICATE_XOAUTH2, getLogId(), new UntaggedHandler() {
            public void handleAsyncUntaggedResponse(ImapResponse response) throws IOException {
                if (response.isContinuationRequested()) {
                    ImapConnection.this.outputStream.write("\r\n".getBytes());
                    ImapConnection.this.outputStream.flush();
                }
            }
        }));
    }

    private void authCramMD5() throws MessagingException, IOException {
        String tag = sendCommand(Commands.AUTHENTICATE_CRAM_MD5, false);
        ImapResponse response = readContinuationResponse(tag);
        if (response.size() != 1 || !(response.get(0) instanceof String)) {
            throw new MessagingException("Invalid Cram-MD5 nonce received");
        }
        this.outputStream.write(Authentication.computeCramMd5Bytes(this.settings.getUsername(), this.settings.getPassword(), response.getString(0).getBytes()));
        this.outputStream.write(13);
        this.outputStream.write(10);
        this.outputStream.flush();
        try {
            extractCapabilities(this.responseParser.readStatusResponse(tag, Commands.AUTHENTICATE_CRAM_MD5, getLogId(), null));
        } catch (NegativeImapResponseException e) {
            throw new AuthenticationFailedException(e.getMessage());
        }
    }

    private void saslAuthPlainWithLoginFallback() throws IOException, MessagingException {
        try {
            saslAuthPlain();
        } catch (AuthenticationFailedException e) {
            login();
        }
    }

    private void saslAuthPlain() throws IOException, MessagingException {
        String tag = sendCommand(Commands.AUTHENTICATE_PLAIN, false);
        readContinuationResponse(tag);
        this.outputStream.write(Base64.encodeBase64(("\u0000" + this.settings.getUsername() + "\u0000" + this.settings.getPassword()).getBytes()));
        this.outputStream.write(13);
        this.outputStream.write(10);
        this.outputStream.flush();
        try {
            extractCapabilities(this.responseParser.readStatusResponse(tag, Commands.AUTHENTICATE_PLAIN, getLogId(), null));
        } catch (NegativeImapResponseException e) {
            throw new AuthenticationFailedException(e.getMessage());
        }
    }

    private void login() throws IOException, MessagingException {
        Pattern p = Pattern.compile("[\\\\\"]");
        try {
            extractCapabilities(executeSimpleCommand(String.format("LOGIN \"%s\" \"%s\"", p.matcher(this.settings.getUsername()).replaceAll("\\\\$0"), p.matcher(this.settings.getPassword()).replaceAll("\\\\$0")), true));
        } catch (NegativeImapResponseException e) {
            throw new AuthenticationFailedException(e.getMessage());
        }
    }

    private void saslAuthExternal() throws IOException, MessagingException {
        try {
            extractCapabilities(executeSimpleCommand("AUTHENTICATE EXTERNAL " + Base64.encode(this.settings.getUsername()), false));
        } catch (NegativeImapResponseException e) {
            throw new CertificateValidationException(e.getMessage());
        }
    }

    private void enableCompressionIfRequested() throws IOException, MessagingException {
        if (hasCapability(Capabilities.COMPRESS_DEFLATE) && shouldEnableCompression()) {
            enableCompression();
        }
    }

    private boolean shouldEnableCompression() {
        boolean useCompression = true;
        NetworkInfo networkInfo = this.connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null) {
            int type = networkInfo.getType();
            if (K9MailLib.isDebug()) {
                Log.d("k9", "On network type " + type);
            }
            useCompression = this.settings.useCompression(NetworkType.fromConnectivityManagerType(type));
        }
        if (K9MailLib.isDebug()) {
            Log.d("k9", "useCompression " + useCompression);
        }
        return useCompression;
    }

    private void enableCompression() throws IOException, MessagingException {
        try {
            executeSimpleCommand(Commands.COMPRESS_DEFLATE);
            try {
                InflaterInputStream input = new InflaterInputStream(this.socket.getInputStream(), new Inflater(true));
                ZOutputStream output = new ZOutputStream(this.socket.getOutputStream(), 1, true);
                output.setFlushMode(1);
                setUpStreamsAndParser(input, output);
                if (K9MailLib.isDebug()) {
                    Log.i("k9", "Compression enabled for " + getLogId());
                }
            } catch (IOException e) {
                close();
                Log.e("k9", "Error enabling compression", e);
            }
        } catch (NegativeImapResponseException e2) {
            Log.d("k9", "Unable to negotiate compression: " + e2.getMessage());
        }
    }

    private void retrievePathPrefixIfNecessary() throws IOException, MessagingException {
        if (this.settings.getPathPrefix() == null) {
            if (hasCapability("NAMESPACE")) {
                if (K9MailLib.isDebug()) {
                    Log.i("k9", "pathPrefix is unset and server has NAMESPACE capability");
                }
                handleNamespace();
                return;
            }
            if (K9MailLib.isDebug()) {
                Log.i("k9", "pathPrefix is unset but server does not have NAMESPACE capability");
            }
            this.settings.setPathPrefix("");
        }
    }

    private void handleNamespace() throws IOException, MessagingException {
        NamespaceResponse namespaceResponse = NamespaceResponse.parse(executeSimpleCommand("NAMESPACE"));
        if (namespaceResponse != null) {
            String prefix = namespaceResponse.getPrefix();
            String hierarchyDelimiter = namespaceResponse.getHierarchyDelimiter();
            this.settings.setPathPrefix(prefix);
            this.settings.setPathDelimiter(hierarchyDelimiter);
            this.settings.setCombinedPrefix(null);
            if (K9MailLib.isDebug()) {
                Log.d("k9", "Got path '" + prefix + "' and separator '" + hierarchyDelimiter + "'");
            }
        }
    }

    private void retrievePathDelimiterIfNecessary() throws IOException, MessagingException {
        if (this.settings.getPathDelimiter() == null) {
            retrievePathDelimiter();
        }
    }

    private void retrievePathDelimiter() throws IOException, MessagingException {
        try {
            for (ImapResponse response : executeSimpleCommand("LIST \"\" \"\"")) {
                if (isListResponse(response)) {
                    this.settings.setPathDelimiter(response.getString(2));
                    this.settings.setCombinedPrefix(null);
                    if (K9MailLib.isDebug()) {
                        Log.d("k9", "Got path delimiter '" + this.settings.getPathDelimiter() + "' for " + getLogId());
                        return;
                    }
                    return;
                }
            }
        } catch (NegativeImapResponseException e) {
            Log.d("k9", "Error getting path delimiter using LIST command", e);
        }
    }

    private boolean isListResponse(ImapResponse response) {
        boolean responseTooShort;
        boolean z = true;
        if (response.size() < 4) {
            responseTooShort = true;
        } else {
            responseTooShort = false;
        }
        if (responseTooShort) {
            return false;
        }
        boolean isListResponse = ImapResponseParser.equalsIgnoreCase(response.get(0), "LIST");
        boolean hierarchyDelimiterValid = response.get(2) instanceof String;
        if (!isListResponse || !hierarchyDelimiterValid) {
            z = false;
        }
        return z;
    }

    /* access modifiers changed from: protected */
    public boolean hasCapability(String capability) {
        return this.capabilities.contains(capability.toUpperCase(Locale.US));
    }

    /* access modifiers changed from: protected */
    public boolean isIdleCapable() {
        if (K9MailLib.isDebug()) {
            Log.v("k9", "Connection " + getLogId() + " has " + this.capabilities.size() + " capabilities");
        }
        return this.capabilities.contains("IDLE");
    }

    public void close() {
        this.open = false;
        this.stacktraceForClose = new Exception();
        IOUtils.closeQuietly((InputStream) this.inputStream);
        IOUtils.closeQuietly(this.outputStream);
        IOUtils.closeQuietly(this.socket);
        this.inputStream = null;
        this.outputStream = null;
        this.socket = null;
    }

    public OutputStream getOutputStream() {
        return this.outputStream;
    }

    /* access modifiers changed from: protected */
    public String getLogId() {
        return "conn" + hashCode();
    }

    public List<ImapResponse> executeSimpleCommand(String command) throws IOException, MessagingException {
        return executeSimpleCommand(command, false);
    }

    public List<ImapResponse> executeSimpleCommand(String command, boolean sensitive) throws IOException, MessagingException {
        String commandToLog = command;
        if (sensitive && !K9MailLib.isDebugSensitive()) {
            commandToLog = "*sensitive*";
        }
        try {
            return this.responseParser.readStatusResponse(sendCommand(command, sensitive), commandToLog, getLogId(), null);
        } catch (IOException e) {
            close();
            throw e;
        }
    }

    public List<ImapResponse> readStatusResponse(String tag, String commandToLog, UntaggedHandler untaggedHandler) throws IOException, NegativeImapResponseException {
        return this.responseParser.readStatusResponse(tag, commandToLog, getLogId(), untaggedHandler);
    }

    public String sendSaslIrCommand(String command, String initialClientResponse, boolean sensitive) throws IOException, MessagingException {
        try {
            open();
            int i = this.nextCommandTag;
            this.nextCommandTag = i + 1;
            String tag = Integer.toString(i);
            this.outputStream.write((tag + " " + command + " " + initialClientResponse + "\r\n").getBytes());
            this.outputStream.flush();
            if (K9MailLib.isDebug() && K9MailLib.DEBUG_PROTOCOL_IMAP) {
                if (!sensitive || K9MailLib.isDebugSensitive()) {
                    Log.v("k9", getLogId() + ">>> " + tag + " " + command + " " + initialClientResponse);
                } else {
                    Log.v("k9", getLogId() + ">>> [Command Hidden, Enable Sensitive Debug Logging To Show]");
                }
            }
            return tag;
        } catch (MessagingException | IOException e) {
            close();
            throw e;
        }
    }

    public String sendCommand(String command, boolean sensitive) throws MessagingException, IOException {
        try {
            open();
            int i = this.nextCommandTag;
            this.nextCommandTag = i + 1;
            String tag = Integer.toString(i);
            this.outputStream.write((tag + " " + command + "\r\n").getBytes());
            this.outputStream.flush();
            if (K9MailLib.isDebug() && K9MailLib.DEBUG_PROTOCOL_IMAP) {
                if (!sensitive || K9MailLib.isDebugSensitive()) {
                    Log.v("k9", getLogId() + ">>> " + tag + " " + command);
                } else {
                    Log.v("k9", getLogId() + ">>> [Command Hidden, Enable Sensitive Debug Logging To Show]");
                }
            }
            return tag;
        } catch (MessagingException | IOException e) {
            close();
            throw e;
        }
    }

    public void sendContinuation(String continuation) throws IOException {
        this.outputStream.write(continuation.getBytes());
        this.outputStream.write(13);
        this.outputStream.write(10);
        this.outputStream.flush();
        if (K9MailLib.isDebug() && K9MailLib.DEBUG_PROTOCOL_IMAP) {
            Log.v("k9", getLogId() + ">>> " + continuation);
        }
    }

    public ImapResponse readResponse() throws IOException, MessagingException {
        return readResponse(null);
    }

    public ImapResponse readResponse(ImapResponseCallback callback) throws IOException {
        try {
            ImapResponse response = this.responseParser.readResponse(callback);
            if (K9MailLib.isDebug() && K9MailLib.DEBUG_PROTOCOL_IMAP) {
                Log.v("k9", getLogId() + "<<<" + response);
            }
            return response;
        } catch (IOException e) {
            close();
            throw e;
        }
    }

    /* access modifiers changed from: protected */
    public void setReadTimeout(int millis) throws SocketException {
        Socket sock = this.socket;
        if (sock != null) {
            sock.setSoTimeout(millis);
        }
    }

    private ImapResponse readContinuationResponse(String tag) throws IOException, MessagingException {
        ImapResponse response;
        do {
            response = readResponse();
            String responseTag = response.getTag();
            if (responseTag != null) {
                if (responseTag.equalsIgnoreCase(tag)) {
                    throw new MessagingException("Command continuation aborted: " + response);
                }
                Log.w("k9", "After sending tag " + tag + ", got tag response from previous command " + response + " for " + getLogId());
            }
        } while (!response.isContinuationRequested());
        return response;
    }
}
