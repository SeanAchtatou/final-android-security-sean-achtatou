package com.fsck.k9.mail.internet;

import android.support.annotation.VisibleForTesting;
import com.fsck.k9.Account;
import com.fsck.k9.mail.Address;
import com.fsck.k9.mail.Message;
import java.util.Locale;
import java.util.UUID;

public class MessageIdGenerator {
    public static MessageIdGenerator getInstance() {
        return new MessageIdGenerator();
    }

    @VisibleForTesting
    MessageIdGenerator() {
    }

    public String generateMessageId(Message message) {
        Address[] replyTo;
        String hostname = null;
        Address[] from = message.getFrom();
        if (from != null && from.length >= 1) {
            hostname = from[0].getHostname();
        }
        if (hostname == null && (replyTo = message.getReplyTo()) != null && replyTo.length >= 1) {
            hostname = replyTo[0].getHostname();
        }
        if (hostname == null) {
            hostname = "email.android.com";
        }
        return "<" + generateUuid() + "@" + hostname + Account.DEFAULT_QUOTE_PREFIX;
    }

    /* access modifiers changed from: protected */
    @VisibleForTesting
    public String generateUuid() {
        return UUID.randomUUID().toString().toUpperCase(Locale.US);
    }
}
