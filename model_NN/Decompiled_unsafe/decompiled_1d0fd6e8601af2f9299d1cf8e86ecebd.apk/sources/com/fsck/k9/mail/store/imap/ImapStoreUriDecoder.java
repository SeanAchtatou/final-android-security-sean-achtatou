package com.fsck.k9.mail.store.imap;

import com.fsck.k9.mail.AuthType;
import com.fsck.k9.mail.ConnectionSecurity;
import com.fsck.k9.mail.ServerSettings;
import com.fsck.k9.mail.helper.UrlEncodingHelper;
import java.net.URI;
import java.net.URISyntaxException;

class ImapStoreUriDecoder {
    ImapStoreUriDecoder() {
    }

    public static ImapStoreSettings decode(String uri) {
        ConnectionSecurity connectionSecurity;
        int port;
        AuthType authenticationType = null;
        String username = null;
        String password = null;
        String clientCertificateAlias = null;
        String pathPrefix = null;
        boolean autoDetectNamespace = true;
        try {
            URI imapUri = new URI(uri);
            String scheme = imapUri.getScheme();
            if (scheme.equals("imap")) {
                connectionSecurity = ConnectionSecurity.NONE;
                port = ServerSettings.Type.IMAP.defaultPort;
            } else if (scheme.startsWith("imap+tls")) {
                connectionSecurity = ConnectionSecurity.STARTTLS_REQUIRED;
                port = ServerSettings.Type.IMAP.defaultPort;
            } else if (scheme.startsWith("imap+ssl")) {
                connectionSecurity = ConnectionSecurity.SSL_TLS_REQUIRED;
                port = ServerSettings.Type.IMAP.defaultTlsPort;
            } else {
                throw new IllegalArgumentException("Unsupported protocol (" + scheme + ")");
            }
            String host = imapUri.getHost();
            if (imapUri.getPort() != -1) {
                port = imapUri.getPort();
            }
            if (imapUri.getUserInfo() != null) {
                String userinfo = imapUri.getUserInfo();
                String[] userInfoParts = userinfo.split(":");
                if (userinfo.endsWith(":")) {
                    if (userInfoParts.length > 1) {
                        authenticationType = AuthType.valueOf(userInfoParts[0]);
                        username = UrlEncodingHelper.decodeUtf8(userInfoParts[1]);
                    } else {
                        authenticationType = AuthType.PLAIN;
                        username = UrlEncodingHelper.decodeUtf8(userInfoParts[0]);
                    }
                } else if (userInfoParts.length == 2) {
                    authenticationType = AuthType.PLAIN;
                    username = UrlEncodingHelper.decodeUtf8(userInfoParts[0]);
                    password = UrlEncodingHelper.decodeUtf8(userInfoParts[1]);
                } else if (userInfoParts.length == 3) {
                    authenticationType = AuthType.valueOf(userInfoParts[0]);
                    username = UrlEncodingHelper.decodeUtf8(userInfoParts[1]);
                    if (AuthType.EXTERNAL == authenticationType) {
                        clientCertificateAlias = UrlEncodingHelper.decodeUtf8(userInfoParts[2]);
                    } else {
                        password = UrlEncodingHelper.decodeUtf8(userInfoParts[2]);
                    }
                }
            }
            String path = imapUri.getPath();
            if (path != null && path.length() > 1) {
                String cleanPath = path.substring(1);
                if (cleanPath.length() >= 2 && cleanPath.charAt(1) == '|') {
                    autoDetectNamespace = cleanPath.charAt(0) == '1';
                    if (!autoDetectNamespace) {
                        pathPrefix = cleanPath.substring(2);
                    }
                } else if (cleanPath.length() > 0) {
                    pathPrefix = cleanPath;
                    autoDetectNamespace = false;
                }
            }
            return new ImapStoreSettings(host, port, connectionSecurity, authenticationType, username, password, clientCertificateAlias, autoDetectNamespace, pathPrefix);
        } catch (URISyntaxException use) {
            throw new IllegalArgumentException("Invalid ImapStore URI", use);
        }
    }
}
