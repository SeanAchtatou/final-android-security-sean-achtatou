package com.fsck.k9.mail.power;

import android.content.Context;
import android.os.PowerManager;
import android.util.Log;
import com.fsck.k9.mail.K9MailLib;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;

public class TracingPowerManager {
    private static final boolean TRACE = false;
    private static TracingPowerManager tracingPowerManager;
    public static AtomicInteger wakeLockId = new AtomicInteger(0);
    PowerManager pm = null;
    /* access modifiers changed from: private */
    public Timer timer = null;

    public static synchronized TracingPowerManager getPowerManager(Context context) {
        TracingPowerManager tracingPowerManager2;
        synchronized (TracingPowerManager.class) {
            Context appContext = context.getApplicationContext();
            if (tracingPowerManager == null) {
                if (K9MailLib.isDebug()) {
                    Log.v("k9", "Creating TracingPowerManager");
                }
                tracingPowerManager = new TracingPowerManager(appContext);
            }
            tracingPowerManager2 = tracingPowerManager;
        }
        return tracingPowerManager2;
    }

    private TracingPowerManager(Context context) {
        this.pm = (PowerManager) context.getSystemService("power");
    }

    public TracingWakeLock newWakeLock(int flags, String tag) {
        return new TracingWakeLock(flags, tag);
    }

    public class TracingWakeLock {
        final int id;
        volatile Long startTime = null;
        final String tag;
        volatile Long timeout = null;
        volatile TimerTask timerTask;
        final PowerManager.WakeLock wakeLock;

        public TracingWakeLock(int flags, String ntag) {
            this.tag = ntag;
            this.wakeLock = TracingPowerManager.this.pm.newWakeLock(flags, this.tag);
            this.id = TracingPowerManager.wakeLockId.getAndIncrement();
            if (K9MailLib.isDebug()) {
                Log.v("k9", "TracingWakeLock for tag " + this.tag + " / id " + this.id + ": Create");
            }
        }

        public void acquire(long timeout2) {
            synchronized (this.wakeLock) {
                this.wakeLock.acquire(timeout2);
            }
            if (K9MailLib.isDebug()) {
                Log.v("k9", "TracingWakeLock for tag " + this.tag + " / id " + this.id + " for " + timeout2 + " ms: acquired");
            }
            raiseNotification();
            if (this.startTime == null) {
                this.startTime = Long.valueOf(System.currentTimeMillis());
            }
            this.timeout = Long.valueOf(timeout2);
        }

        public void acquire() {
            synchronized (this.wakeLock) {
                this.wakeLock.acquire();
            }
            raiseNotification();
            if (K9MailLib.isDebug()) {
                Log.w("k9", "TracingWakeLock for tag " + this.tag + " / id " + this.id + ": acquired with no timeout.  K-9 Mail should not do this");
            }
            if (this.startTime == null) {
                this.startTime = Long.valueOf(System.currentTimeMillis());
            }
            this.timeout = null;
        }

        public void setReferenceCounted(boolean counted) {
            synchronized (this.wakeLock) {
                this.wakeLock.setReferenceCounted(counted);
            }
        }

        public void release() {
            if (this.startTime != null) {
                Long endTime = Long.valueOf(System.currentTimeMillis());
                if (K9MailLib.isDebug()) {
                    Log.v("k9", "TracingWakeLock for tag " + this.tag + " / id " + this.id + ": releasing after " + (endTime.longValue() - this.startTime.longValue()) + " ms, timeout = " + this.timeout + " ms");
                }
            } else if (K9MailLib.isDebug()) {
                Log.v("k9", "TracingWakeLock for tag " + this.tag + " / id " + this.id + ", timeout = " + this.timeout + " ms: releasing");
            }
            cancelNotification();
            synchronized (this.wakeLock) {
                this.wakeLock.release();
            }
            this.startTime = null;
        }

        private void cancelNotification() {
            if (TracingPowerManager.this.timer != null) {
                synchronized (TracingPowerManager.this.timer) {
                    if (this.timerTask != null) {
                        this.timerTask.cancel();
                    }
                }
            }
        }

        private void raiseNotification() {
            if (TracingPowerManager.this.timer != null) {
                synchronized (TracingPowerManager.this.timer) {
                    if (this.timerTask != null) {
                        this.timerTask.cancel();
                        this.timerTask = null;
                    }
                    this.timerTask = new TimerTask() {
                        public void run() {
                            if (TracingWakeLock.this.startTime != null) {
                                Log.i("k9", "TracingWakeLock for tag " + TracingWakeLock.this.tag + " / id " + TracingWakeLock.this.id + ": has been active for " + (Long.valueOf(System.currentTimeMillis()).longValue() - TracingWakeLock.this.startTime.longValue()) + " ms, timeout = " + TracingWakeLock.this.timeout + " ms");
                                return;
                            }
                            Log.i("k9", "TracingWakeLock for tag " + TracingWakeLock.this.tag + " / id " + TracingWakeLock.this.id + ": still active, timeout = " + TracingWakeLock.this.timeout + " ms");
                        }
                    };
                    TracingPowerManager.this.timer.schedule(this.timerTask, 1000, 1000);
                }
            }
        }
    }
}
