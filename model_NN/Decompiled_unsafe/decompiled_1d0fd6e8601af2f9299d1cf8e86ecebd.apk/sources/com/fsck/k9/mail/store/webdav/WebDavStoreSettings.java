package com.fsck.k9.mail.store.webdav;

import com.fsck.k9.mail.AuthType;
import com.fsck.k9.mail.ConnectionSecurity;
import com.fsck.k9.mail.ServerSettings;
import java.util.HashMap;
import java.util.Map;

public class WebDavStoreSettings extends ServerSettings {
    public static final String ALIAS_KEY = "alias";
    public static final String AUTH_PATH_KEY = "authPath";
    public static final String MAILBOX_PATH_KEY = "mailboxPath";
    public static final String PATH_KEY = "path";
    public final String alias;
    public final String authPath;
    public final String mailboxPath;
    public final String path;

    protected WebDavStoreSettings(String host, int port, ConnectionSecurity connectionSecurity, AuthType authenticationType, String username, String password, String clientCertificateAlias, String alias2, String path2, String authPath2, String mailboxPath2) {
        super(ServerSettings.Type.WebDAV, host, port, connectionSecurity, authenticationType, username, password, clientCertificateAlias);
        this.alias = alias2;
        this.path = path2;
        this.authPath = authPath2;
        this.mailboxPath = mailboxPath2;
    }

    public Map<String, String> getExtra() {
        Map<String, String> extra = new HashMap<>();
        putIfNotNull(extra, ALIAS_KEY, this.alias);
        putIfNotNull(extra, PATH_KEY, this.path);
        putIfNotNull(extra, AUTH_PATH_KEY, this.authPath);
        putIfNotNull(extra, MAILBOX_PATH_KEY, this.mailboxPath);
        return extra;
    }

    public ServerSettings newPassword(String newPassword) {
        return new WebDavStoreSettings(this.host, this.port, this.connectionSecurity, this.authenticationType, this.username, newPassword, this.clientCertificateAlias, this.alias, this.path, this.authPath, this.mailboxPath);
    }
}
