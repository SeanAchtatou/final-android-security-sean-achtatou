package com.fsck.k9.mail.ssl;

import android.content.Context;
import android.os.Build;
import android.security.KeyChain;
import android.security.KeyChainException;
import android.util.Log;
import com.fsck.k9.mail.CertificateValidationException;
import com.fsck.k9.mail.MessagingException;
import java.net.Socket;
import java.security.Principal;
import java.security.PrivateKey;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.X509ExtendedKeyManager;

class KeyChainKeyManager extends X509ExtendedKeyManager {
    private static PrivateKey sClientCertificateReferenceWorkaround;
    private final String mAlias;
    private final X509Certificate[] mChain;
    private final PrivateKey mPrivateKey;

    private static synchronized void savePrivateKeyReference(PrivateKey privateKey) {
        synchronized (KeyChainKeyManager.class) {
            if (sClientCertificateReferenceWorkaround == null) {
                sClientCertificateReferenceWorkaround = privateKey;
            }
        }
    }

    public KeyChainKeyManager(Context context, String alias) throws MessagingException {
        this.mAlias = alias;
        try {
            this.mChain = fetchCertificateChain(context, alias);
            this.mPrivateKey = fetchPrivateKey(context, alias);
        } catch (KeyChainException e) {
            throw new CertificateValidationException(e.getMessage(), CertificateValidationException.Reason.RetrievalFailure, alias);
        } catch (InterruptedException e2) {
            throw new CertificateValidationException(e2.getMessage(), CertificateValidationException.Reason.RetrievalFailure, alias);
        }
    }

    private X509Certificate[] fetchCertificateChain(Context context, String alias) throws KeyChainException, InterruptedException, MessagingException {
        X509Certificate[] chain = KeyChain.getCertificateChain(context, alias);
        if (chain == null || chain.length == 0) {
            throw new MessagingException("No certificate chain found for: " + alias);
        }
        try {
            for (X509Certificate certificate : chain) {
                certificate.checkValidity();
            }
            return chain;
        } catch (CertificateException e) {
            throw new CertificateValidationException(e.getMessage(), CertificateValidationException.Reason.Expired, alias);
        }
    }

    private PrivateKey fetchPrivateKey(Context context, String alias) throws KeyChainException, InterruptedException, MessagingException {
        PrivateKey privateKey = KeyChain.getPrivateKey(context, alias);
        if (privateKey == null) {
            throw new MessagingException("No private key found for: " + alias);
        }
        if (Build.VERSION.SDK_INT < 17) {
            savePrivateKeyReference(privateKey);
        }
        return privateKey;
    }

    public String chooseClientAlias(String[] keyTypes, Principal[] issuers, Socket socket) {
        return chooseAlias(keyTypes, issuers);
    }

    public X509Certificate[] getCertificateChain(String alias) {
        if (this.mAlias.equals(alias)) {
            return this.mChain;
        }
        return null;
    }

    public PrivateKey getPrivateKey(String alias) {
        if (this.mAlias.equals(alias)) {
            return this.mPrivateKey;
        }
        return null;
    }

    public String chooseServerAlias(String keyType, Principal[] issuers, Socket socket) {
        return chooseAlias(new String[]{keyType}, issuers);
    }

    public String[] getClientAliases(String keyType, Principal[] issuers) {
        String al = chooseAlias(new String[]{keyType}, issuers);
        if (al == null) {
            return null;
        }
        return new String[]{al};
    }

    public String[] getServerAliases(String keyType, Principal[] issuers) {
        String al = chooseAlias(new String[]{keyType}, issuers);
        if (al == null) {
            return null;
        }
        return new String[]{al};
    }

    public String chooseEngineClientAlias(String[] keyTypes, Principal[] issuers, SSLEngine engine) {
        return chooseAlias(keyTypes, issuers);
    }

    public String chooseEngineServerAlias(String keyType, Principal[] issuers, SSLEngine engine) {
        return chooseAlias(new String[]{keyType}, issuers);
    }

    private String chooseAlias(String[] keyTypes, Principal[] issuers) {
        String sigAlgorithm;
        if (keyTypes == null || keyTypes.length == 0) {
            return null;
        }
        X509Certificate cert = this.mChain[0];
        String certKeyAlg = cert.getPublicKey().getAlgorithm();
        String certSigAlg = cert.getSigAlgName().toUpperCase(Locale.US);
        int length = keyTypes.length;
        for (int i = 0; i < length; i++) {
            String keyAlgorithm = keyTypes[i];
            if (keyAlgorithm != null) {
                int index = keyAlgorithm.indexOf(95);
                if (index == -1) {
                    sigAlgorithm = null;
                } else {
                    sigAlgorithm = keyAlgorithm.substring(index + 1);
                    keyAlgorithm = keyAlgorithm.substring(0, index);
                }
                if (certKeyAlg.equals(keyAlgorithm) && (sigAlgorithm == null || certSigAlg == null || certSigAlg.contains(sigAlgorithm))) {
                    if (issuers == null || issuers.length == 0) {
                        return this.mAlias;
                    }
                    List<Principal> issuersList = Arrays.asList(issuers);
                    for (X509Certificate certFromChain : this.mChain) {
                        if (issuersList.contains(certFromChain.getIssuerX500Principal())) {
                            return this.mAlias;
                        }
                    }
                    Log.w("k9", "Client certificate " + this.mAlias + " not issued by any of the requested issuers");
                    return null;
                }
            }
        }
        Log.w("k9", "Client certificate " + this.mAlias + " does not match any of the requested key types");
        return null;
    }
}
