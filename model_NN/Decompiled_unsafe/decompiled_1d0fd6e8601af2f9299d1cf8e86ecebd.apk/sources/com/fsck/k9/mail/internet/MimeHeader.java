package com.fsck.k9.mail.internet;

import android.support.annotation.NonNull;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class MimeHeader implements Cloneable {
    public static final String HEADER_CONTENT_DISPOSITION = "Content-Disposition";
    public static final String HEADER_CONTENT_ID = "Content-ID";
    public static final String HEADER_CONTENT_TRANSFER_ENCODING = "Content-Transfer-Encoding";
    public static final String HEADER_CONTENT_TYPE = "Content-Type";
    private String mCharset = null;
    private List<Field> mFields = new ArrayList();

    public void clear() {
        this.mFields.clear();
    }

    public String getFirstHeader(String name) {
        String[] header = getHeader(name);
        if (header.length == 0) {
            return null;
        }
        return header[0];
    }

    public void addHeader(String name, String value) {
        this.mFields.add(Field.newNameValueField(name, MimeUtility.foldAndEncode(value)));
    }

    /* access modifiers changed from: package-private */
    public void addRawHeader(String name, String raw) {
        this.mFields.add(Field.newRawField(name, raw));
    }

    public void setHeader(String name, String value) {
        if (name != null && value != null) {
            removeHeader(name);
            addHeader(name, value);
        }
    }

    @NonNull
    public Set<String> getHeaderNames() {
        Set<String> names = new LinkedHashSet<>();
        for (Field field : this.mFields) {
            names.add(field.getName());
        }
        return names;
    }

    @NonNull
    public String[] getHeader(String name) {
        List<String> values = new ArrayList<>();
        for (Field field : this.mFields) {
            if (field.getName().equalsIgnoreCase(name)) {
                values.add(field.getValue());
            }
        }
        return (String[]) values.toArray(new String[values.size()]);
    }

    public void removeHeader(String name) {
        List<Field> removeFields = new ArrayList<>();
        for (Field field : this.mFields) {
            if (field.getName().equalsIgnoreCase(name)) {
                removeFields.add(field);
            }
        }
        this.mFields.removeAll(removeFields);
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (Field field : this.mFields) {
            if (field.hasRawData()) {
                builder.append(field.getRaw());
            } else {
                writeNameValueField(builder, field);
            }
            builder.append(13).append(10);
        }
        return builder.toString();
    }

    public void writeTo(OutputStream out) throws IOException {
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out), 1024);
        for (Field field : this.mFields) {
            if (field.hasRawData()) {
                writer.write(field.getRaw());
            } else {
                writeNameValueField(writer, field);
            }
            writer.write("\r\n");
        }
        writer.flush();
    }

    private void writeNameValueField(BufferedWriter writer, Field field) throws IOException {
        String value = field.getValue();
        if (hasToBeEncoded(value)) {
            Charset charset = null;
            if (this.mCharset != null) {
                charset = Charset.forName(this.mCharset);
            }
            value = EncoderUtil.encodeEncodedWord(field.getValue(), charset);
        }
        writer.write(field.getName());
        writer.write(": ");
        writer.write(value);
    }

    private void writeNameValueField(StringBuilder builder, Field field) {
        String value = field.getValue();
        if (hasToBeEncoded(value)) {
            Charset charset = null;
            if (this.mCharset != null) {
                charset = Charset.forName(this.mCharset);
            }
            value = EncoderUtil.encodeEncodedWord(field.getValue(), charset);
        }
        builder.append(field.getName());
        builder.append(": ");
        builder.append(value);
    }

    private boolean hasToBeEncoded(String text) {
        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            if ((c < ' ' || '~' < c) && c != 10 && c != 13 && c != 9) {
                return true;
            }
        }
        return false;
    }

    private static class Field {
        private final String name;
        private final String raw;
        private final String value;

        public static Field newNameValueField(String name2, String value2) {
            if (value2 != null) {
                return new Field(name2, value2, null);
            }
            throw new NullPointerException("Argument 'value' cannot be null");
        }

        public static Field newRawField(String name2, String raw2) {
            if (raw2 == null) {
                throw new NullPointerException("Argument 'raw' cannot be null");
            } else if (name2 == null || raw2.startsWith(name2 + ":")) {
                return new Field(name2, null, raw2);
            } else {
                throw new IllegalArgumentException("The value of 'raw' needs to start with the supplied field name followed by a colon");
            }
        }

        private Field(String name2, String value2, String raw2) {
            if (name2 == null) {
                throw new NullPointerException("Argument 'name' cannot be null");
            }
            this.name = name2;
            this.value = value2;
            this.raw = raw2;
        }

        public String getName() {
            return this.name;
        }

        public String getValue() {
            if (this.value != null) {
                return this.value;
            }
            int delimiterIndex = this.raw.indexOf(58);
            if (delimiterIndex == this.raw.length() - 1) {
                return "";
            }
            return this.raw.substring(delimiterIndex + 1).trim();
        }

        public String getRaw() {
            return this.raw;
        }

        public boolean hasRawData() {
            return this.raw != null;
        }

        public String toString() {
            return hasRawData() ? getRaw() : getName() + ": " + getValue();
        }
    }

    public void setCharset(String charset) {
        this.mCharset = charset;
    }

    public MimeHeader clone() {
        try {
            MimeHeader header = (MimeHeader) super.clone();
            header.mFields = new ArrayList(this.mFields);
            return header;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }
}
