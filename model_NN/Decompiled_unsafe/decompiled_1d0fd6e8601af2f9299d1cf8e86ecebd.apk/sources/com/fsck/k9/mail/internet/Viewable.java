package com.fsck.k9.mail.internet;

import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.Part;
import java.util.List;

public interface Viewable {

    public static abstract class Textual implements Viewable {
        private Part mPart;

        public Textual(Part part) {
            this.mPart = part;
        }

        public Part getPart() {
            return this.mPart;
        }
    }

    public static class Text extends Textual {
        public Text(Part part) {
            super(part);
        }
    }

    public static class Html extends Textual {
        public Html(Part part) {
            super(part);
        }
    }

    public static class MessageHeader implements Viewable {
        private Part mContainerPart;
        private Message mMessage;

        public MessageHeader(Part containerPart, Message message) {
            this.mContainerPart = containerPart;
            this.mMessage = message;
        }

        public Part getContainerPart() {
            return this.mContainerPart;
        }

        public Message getMessage() {
            return this.mMessage;
        }
    }

    public static class Alternative implements Viewable {
        private List<Viewable> mHtml;
        private List<Viewable> mText;

        public Alternative(List<Viewable> text, List<Viewable> html) {
            this.mText = text;
            this.mHtml = html;
        }

        public List<Viewable> getText() {
            return this.mText;
        }

        public List<Viewable> getHtml() {
            return this.mHtml;
        }
    }
}
