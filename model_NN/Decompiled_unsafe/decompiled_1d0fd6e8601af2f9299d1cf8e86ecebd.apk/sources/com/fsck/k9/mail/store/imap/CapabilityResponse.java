package com.fsck.k9.mail.store.imap;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

class CapabilityResponse {
    private final Set<String> capabilities;

    private CapabilityResponse(Set<String> capabilities2) {
        this.capabilities = Collections.unmodifiableSet(capabilities2);
    }

    public static CapabilityResponse parse(List<ImapResponse> responses) {
        CapabilityResponse result;
        for (ImapResponse response : responses) {
            if (!response.isEmpty() && ImapResponseParser.equalsIgnoreCase(response.get(0), Responses.OK) && response.isList(1)) {
                result = parse(response.getList(1));
                continue;
            } else if (response.getTag() == null) {
                result = parse((ImapList) response);
                continue;
            } else {
                result = null;
                continue;
            }
            if (result != null) {
                return result;
            }
        }
        return null;
    }

    static CapabilityResponse parse(ImapList capabilityList) {
        if (capabilityList.isEmpty() || !ImapResponseParser.equalsIgnoreCase(capabilityList.get(0), "CAPABILITY")) {
            return null;
        }
        int size = capabilityList.size();
        HashSet<String> capabilities2 = new HashSet<>(size - 1);
        for (int i = 1; i < size; i++) {
            if (!capabilityList.isString(i)) {
                return null;
            }
            capabilities2.add(capabilityList.getString(i).toUpperCase(Locale.US));
        }
        return new CapabilityResponse(capabilities2);
    }

    public Set<String> getCapabilities() {
        return this.capabilities;
    }
}
