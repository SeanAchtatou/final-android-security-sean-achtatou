package com.fsck.k9.mail;

import android.util.Log;
import com.fsck.k9.mail.Message;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

public abstract class Folder<T extends Message> {
    public static final int OPEN_MODE_RO = 1;
    public static final int OPEN_MODE_RW = 0;
    private long lastChecked = 0;
    private long lastPush = 0;
    private String status = null;

    public enum FolderClass {
        NONE,
        NO_CLASS,
        INHERITED,
        FIRST_CLASS,
        SECOND_CLASS
    }

    public enum FolderType {
        HOLDS_FOLDERS,
        HOLDS_MESSAGES
    }

    public abstract Map<String, String> appendMessages(List<? extends Message> list) throws MessagingException;

    public abstract boolean areMoreMessagesAvailable(int i, Date date) throws IOException, MessagingException;

    public abstract void close();

    public abstract boolean create(FolderType folderType) throws MessagingException;

    public abstract void delete(boolean z) throws MessagingException;

    public abstract boolean exists() throws MessagingException;

    public abstract void fetch(List<T> list, FetchProfile fetchProfile, MessageRetrievalListener<T> messageRetrievalListener) throws MessagingException;

    public abstract int getFlaggedMessageCount() throws MessagingException;

    public abstract T getMessage(String str) throws MessagingException;

    public abstract int getMessageCount() throws MessagingException;

    public abstract List<T> getMessages(int i, int i2, Date date, MessageRetrievalListener<T> messageRetrievalListener) throws MessagingException;

    public abstract int getMode();

    public abstract String getName();

    public abstract String getUidFromMessageId(Message message) throws MessagingException;

    public abstract int getUnreadMessageCount() throws MessagingException;

    public abstract boolean isOpen();

    public abstract void open(int i) throws MessagingException;

    public abstract void setFlags(List<? extends Message> list, Set<Flag> set, boolean z) throws MessagingException;

    public abstract void setFlags(Set<Flag> set, boolean z) throws MessagingException;

    public boolean create(FolderType type, int displayLimit) throws MessagingException {
        return create(type);
    }

    public Map<String, String> copyMessages(List<? extends Message> list, Folder folder) throws MessagingException {
        return null;
    }

    public Map<String, String> moveMessages(List<? extends Message> list, Folder folder) throws MessagingException {
        return null;
    }

    public void delete(List<? extends Message> msgs, String trashFolderName) throws MessagingException {
        for (Message message : msgs) {
            getMessage(message.getUid()).delete(trashFolderName);
        }
    }

    public void expunge() throws MessagingException {
    }

    public void fetchPart(Message message, Part part, MessageRetrievalListener<Message> messageRetrievalListener) throws MessagingException {
        if (K9MailLib.isDebug()) {
            Log.d("k9", "fetchPart() not implemented.");
        }
    }

    public String getNewPushState(String oldPushState, Message message) {
        return null;
    }

    public boolean isFlagSupported(Flag flag) {
        return true;
    }

    public boolean supportsFetchingFlags() {
        return true;
    }

    public String toString() {
        return getName();
    }

    public long getLastChecked() {
        return this.lastChecked;
    }

    public void setLastChecked(long lastChecked2) throws MessagingException {
        this.lastChecked = lastChecked2;
    }

    public long getLastPush() {
        return this.lastPush;
    }

    public void setLastPush(long lastCheckedDisplay) throws MessagingException {
        this.lastPush = lastCheckedDisplay;
    }

    public long getLastUpdate() {
        return Math.max(getLastChecked(), getLastPush());
    }

    public FolderClass getDisplayClass() {
        return FolderClass.NO_CLASS;
    }

    public FolderClass getSyncClass() {
        return getDisplayClass();
    }

    public FolderClass getPushClass() {
        return getSyncClass();
    }

    public boolean isInTopGroup() {
        return false;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status2) throws MessagingException {
        this.status = status2;
    }

    public List<T> search(String queryString, Set<Flag> set, Set<Flag> set2) throws MessagingException {
        throw new MessagingException("K-9 does not support searches on this folder type");
    }
}
