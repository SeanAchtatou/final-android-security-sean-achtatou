package com.fsck.k9.mail.ssl;

import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import org.apache.commons.io.IOUtils;

public class LocalKeyStore {
    private static final int KEY_STORE_FILE_VERSION = 1;
    private static String sKeyStoreLocation;
    private KeyStore mKeyStore;
    private File mKeyStoreFile;

    public static void setKeyStoreLocation(String directory) {
        sKeyStoreLocation = directory;
    }

    private static class LocalKeyStoreHolder {
        static final LocalKeyStore INSTANCE = new LocalKeyStore();

        private LocalKeyStoreHolder() {
        }
    }

    public static LocalKeyStore getInstance() {
        return LocalKeyStoreHolder.INSTANCE;
    }

    private LocalKeyStore() {
        try {
            upgradeKeyStoreFile();
            setKeyStoreFile(null);
        } catch (CertificateException e) {
            Log.w("k9", "Local key store has not been initialized");
        }
    }

    public synchronized void setKeyStoreFile(File file) throws CertificateException {
        if (file == null) {
            file = new File(getKeyStoreFilePath(1));
        }
        if (file.length() == 0 && file.exists() && !file.delete()) {
            Log.d("k9", "Failed to delete empty keystore file: " + file.getAbsolutePath());
        }
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
        } catch (FileNotFoundException e) {
        }
        try {
            KeyStore store = KeyStore.getInstance(KeyStore.getDefaultType());
            store.load(fis, "".toCharArray());
            this.mKeyStore = store;
            this.mKeyStoreFile = file;
            IOUtils.closeQuietly((InputStream) fis);
        } catch (Exception e2) {
            Log.e("k9", "Failed to initialize local key store", e2);
            this.mKeyStore = null;
            this.mKeyStoreFile = null;
            IOUtils.closeQuietly((InputStream) fis);
        } catch (Throwable th) {
            IOUtils.closeQuietly((InputStream) fis);
            throw th;
        }
        return;
    }

    public synchronized void addCertificate(String host, int port, X509Certificate certificate) throws CertificateException {
        if (this.mKeyStore == null) {
            throw new CertificateException("Certificate not added because key store not initialized");
        }
        try {
            this.mKeyStore.setCertificateEntry(getCertKey(host, port), certificate);
            writeCertificateFile();
        } catch (KeyStoreException e) {
            throw new CertificateException("Failed to add certificate to local key store", e);
        }
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:8:0x0018=Splitter:B:8:0x0018, B:15:0x003b=Splitter:B:15:0x003b} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void writeCertificateFile() throws java.security.cert.CertificateException {
        /*
            r6 = this;
            r1 = 0
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x0017, CertificateException -> 0x003a, IOException -> 0x0058, NoSuchAlgorithmException -> 0x0076, KeyStoreException -> 0x0094 }
            java.io.File r3 = r6.mKeyStoreFile     // Catch:{ FileNotFoundException -> 0x0017, CertificateException -> 0x003a, IOException -> 0x0058, NoSuchAlgorithmException -> 0x0076, KeyStoreException -> 0x0094 }
            r2.<init>(r3)     // Catch:{ FileNotFoundException -> 0x0017, CertificateException -> 0x003a, IOException -> 0x0058, NoSuchAlgorithmException -> 0x0076, KeyStoreException -> 0x0094 }
            java.security.KeyStore r3 = r6.mKeyStore     // Catch:{ FileNotFoundException -> 0x00c2, CertificateException -> 0x00be, IOException -> 0x00bb, NoSuchAlgorithmException -> 0x00b8, KeyStoreException -> 0x00b5, all -> 0x00b2 }
            java.lang.String r4 = ""
            char[] r4 = r4.toCharArray()     // Catch:{ FileNotFoundException -> 0x00c2, CertificateException -> 0x00be, IOException -> 0x00bb, NoSuchAlgorithmException -> 0x00b8, KeyStoreException -> 0x00b5, all -> 0x00b2 }
            r3.store(r2, r4)     // Catch:{ FileNotFoundException -> 0x00c2, CertificateException -> 0x00be, IOException -> 0x00bb, NoSuchAlgorithmException -> 0x00b8, KeyStoreException -> 0x00b5, all -> 0x00b2 }
            org.apache.commons.io.IOUtils.closeQuietly(r2)
            return
        L_0x0017:
            r0 = move-exception
        L_0x0018:
            java.security.cert.CertificateException r3 = new java.security.cert.CertificateException     // Catch:{ all -> 0x0035 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0035 }
            r4.<init>()     // Catch:{ all -> 0x0035 }
            java.lang.String r5 = "Unable to write KeyStore: "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0035 }
            java.lang.String r5 = r0.getMessage()     // Catch:{ all -> 0x0035 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0035 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0035 }
            r3.<init>(r4)     // Catch:{ all -> 0x0035 }
            throw r3     // Catch:{ all -> 0x0035 }
        L_0x0035:
            r3 = move-exception
        L_0x0036:
            org.apache.commons.io.IOUtils.closeQuietly(r1)
            throw r3
        L_0x003a:
            r0 = move-exception
        L_0x003b:
            java.security.cert.CertificateException r3 = new java.security.cert.CertificateException     // Catch:{ all -> 0x0035 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0035 }
            r4.<init>()     // Catch:{ all -> 0x0035 }
            java.lang.String r5 = "Unable to write KeyStore: "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0035 }
            java.lang.String r5 = r0.getMessage()     // Catch:{ all -> 0x0035 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0035 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0035 }
            r3.<init>(r4)     // Catch:{ all -> 0x0035 }
            throw r3     // Catch:{ all -> 0x0035 }
        L_0x0058:
            r0 = move-exception
        L_0x0059:
            java.security.cert.CertificateException r3 = new java.security.cert.CertificateException     // Catch:{ all -> 0x0035 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0035 }
            r4.<init>()     // Catch:{ all -> 0x0035 }
            java.lang.String r5 = "Unable to write KeyStore: "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0035 }
            java.lang.String r5 = r0.getMessage()     // Catch:{ all -> 0x0035 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0035 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0035 }
            r3.<init>(r4)     // Catch:{ all -> 0x0035 }
            throw r3     // Catch:{ all -> 0x0035 }
        L_0x0076:
            r0 = move-exception
        L_0x0077:
            java.security.cert.CertificateException r3 = new java.security.cert.CertificateException     // Catch:{ all -> 0x0035 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0035 }
            r4.<init>()     // Catch:{ all -> 0x0035 }
            java.lang.String r5 = "Unable to write KeyStore: "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0035 }
            java.lang.String r5 = r0.getMessage()     // Catch:{ all -> 0x0035 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0035 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0035 }
            r3.<init>(r4)     // Catch:{ all -> 0x0035 }
            throw r3     // Catch:{ all -> 0x0035 }
        L_0x0094:
            r0 = move-exception
        L_0x0095:
            java.security.cert.CertificateException r3 = new java.security.cert.CertificateException     // Catch:{ all -> 0x0035 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0035 }
            r4.<init>()     // Catch:{ all -> 0x0035 }
            java.lang.String r5 = "Unable to write KeyStore: "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0035 }
            java.lang.String r5 = r0.getMessage()     // Catch:{ all -> 0x0035 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0035 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0035 }
            r3.<init>(r4)     // Catch:{ all -> 0x0035 }
            throw r3     // Catch:{ all -> 0x0035 }
        L_0x00b2:
            r3 = move-exception
            r1 = r2
            goto L_0x0036
        L_0x00b5:
            r0 = move-exception
            r1 = r2
            goto L_0x0095
        L_0x00b8:
            r0 = move-exception
            r1 = r2
            goto L_0x0077
        L_0x00bb:
            r0 = move-exception
            r1 = r2
            goto L_0x0059
        L_0x00be:
            r0 = move-exception
            r1 = r2
            goto L_0x003b
        L_0x00c2:
            r0 = move-exception
            r1 = r2
            goto L_0x0018
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fsck.k9.mail.ssl.LocalKeyStore.writeCertificateFile():void");
    }

    public synchronized boolean isValidCertificate(Certificate certificate, String host, int port) {
        boolean z = false;
        synchronized (this) {
            if (this.mKeyStore != null) {
                try {
                    Certificate storedCert = this.mKeyStore.getCertificate(getCertKey(host, port));
                    if (storedCert != null && storedCert.equals(certificate)) {
                        z = true;
                    }
                } catch (KeyStoreException e) {
                }
            }
        }
        return z;
    }

    private static String getCertKey(String host, int port) {
        return host + ":" + port;
    }

    public synchronized void deleteCertificate(String oldHost, int oldPort) {
        if (this.mKeyStore != null) {
            try {
                this.mKeyStore.deleteEntry(getCertKey(oldHost, oldPort));
                writeCertificateFile();
            } catch (KeyStoreException e) {
            } catch (CertificateException e2) {
                Log.e("k9", "Error updating the local key store file", e2);
            }
        }
        return;
    }

    private void upgradeKeyStoreFile() throws CertificateException {
        File versionZeroFile = new File(getKeyStoreFilePath(0));
        if (versionZeroFile.exists() && !versionZeroFile.delete()) {
            Log.d("k9", "Failed to delete old key-store file: " + versionZeroFile.getAbsolutePath());
        }
    }

    private String getKeyStoreFilePath(int version) throws CertificateException {
        if (sKeyStoreLocation == null) {
            throw new CertificateException("Local key store location has not been initialized");
        } else if (version < 1) {
            return sKeyStoreLocation + File.separator + "KeyStore.bks";
        } else {
            return sKeyStoreLocation + File.separator + "KeyStore_v" + version + ".bks";
        }
    }
}
