package com.fsck.k9.mail;

import android.security.KeyChainException;
import java.security.cert.CertPathValidatorException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.SSLHandshakeException;

public class CertificateValidationException extends MessagingException {
    public static final long serialVersionUID = -1;
    private String mAlias;
    private X509Certificate[] mCertChain;
    private boolean mNeedsUserAttention;
    private final Reason mReason;

    public enum Reason {
        Unknown,
        UseMessage,
        Expired,
        MissingCapability,
        RetrievalFailure
    }

    public CertificateValidationException(String message) {
        this(message, Reason.UseMessage, null);
    }

    public CertificateValidationException(Reason reason) {
        this(null, reason, null);
    }

    public CertificateValidationException(String message, Reason reason, String alias) {
        super(message);
        this.mNeedsUserAttention = false;
        this.mNeedsUserAttention = true;
        this.mReason = reason;
        this.mAlias = alias;
    }

    public CertificateValidationException(String message, Throwable throwable) {
        super(message, throwable);
        this.mNeedsUserAttention = false;
        this.mReason = Reason.Unknown;
        scanForCause();
    }

    public String getAlias() {
        return this.mAlias;
    }

    public Reason getReason() {
        return this.mReason;
    }

    private void scanForCause() {
        Throwable throwable = getCause();
        while (throwable != null && !(throwable instanceof CertPathValidatorException) && !(throwable instanceof CertificateException) && !(throwable instanceof KeyChainException) && !(throwable instanceof SSLHandshakeException)) {
            throwable = throwable.getCause();
        }
        if (throwable != null) {
            this.mNeedsUserAttention = true;
            if (throwable instanceof SSLHandshakeException) {
                while (throwable != null && !(throwable instanceof CertificateChainException)) {
                    throwable = throwable.getCause();
                }
            }
            if (throwable != null && (throwable instanceof CertificateChainException)) {
                this.mCertChain = ((CertificateChainException) throwable).getCertChain();
            }
        }
    }

    public boolean needsUserAttention() {
        return this.mNeedsUserAttention;
    }

    public X509Certificate[] getCertChain() {
        return this.mCertChain;
    }
}
