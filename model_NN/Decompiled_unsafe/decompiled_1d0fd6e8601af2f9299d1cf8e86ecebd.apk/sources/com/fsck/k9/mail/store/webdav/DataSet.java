package com.fsck.k9.mail.store.webdav;

import android.util.Log;
import com.fsck.k9.provider.EmailProvider;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

class DataSet {
    private Map<String, Map<String, String>> mData = new HashMap();
    private Map<String, String> mTempData = new HashMap();
    private StringBuilder mUid = new StringBuilder();

    DataSet() {
    }

    public void addValue(String value, String tagName) {
        if (tagName.equals(EmailProvider.MessageColumns.UID)) {
            this.mUid.append(value);
        }
        if (this.mTempData.containsKey(tagName)) {
            this.mTempData.put(tagName, this.mTempData.get(tagName) + value);
        } else {
            this.mTempData.put(tagName, value);
        }
    }

    public void finish() {
        String uid = this.mUid.toString();
        if (this.mTempData != null) {
            this.mData.put(uid, this.mTempData);
        } else if (this.mTempData != null) {
        }
        this.mUid = new StringBuilder();
        this.mTempData = new HashMap();
    }

    public Map<String, String> getSpecialFolderToUrl() {
        Iterator<Map<String, String>> it = this.mData.values().iterator();
        if (it.hasNext()) {
            return it.next();
        }
        return new HashMap();
    }

    public Map<String, String> getUidToUrl() {
        Map<String, String> uidToUrl = new HashMap<>();
        for (String uid : this.mData.keySet()) {
            String value = (String) this.mData.get(uid).get("href");
            if (value != null && !value.equals("")) {
                uidToUrl.put(uid, value);
            }
        }
        return uidToUrl;
    }

    public Map<String, Boolean> getUidToRead() {
        boolean z;
        Map<String, Boolean> uidToRead = new HashMap<>();
        for (String uid : this.mData.keySet()) {
            String readStatus = (String) this.mData.get(uid).get(EmailProvider.MessageColumns.READ);
            if (readStatus == null || readStatus.equals("")) {
                uidToRead.put(uid, false);
            } else {
                if (!readStatus.equals("0")) {
                    z = true;
                } else {
                    z = false;
                }
                uidToRead.put(uid, Boolean.valueOf(z));
            }
        }
        return uidToRead;
    }

    public String[] getHrefs() {
        List<String> hrefs = new ArrayList<>();
        for (String uid : this.mData.keySet()) {
            hrefs.add((String) this.mData.get(uid).get("href"));
        }
        return (String[]) hrefs.toArray(WebDavConstants.EMPTY_STRING_ARRAY);
    }

    public String[] getUids() {
        List<String> uids = new ArrayList<>();
        for (String uid : this.mData.keySet()) {
            uids.add(uid);
        }
        return (String[]) uids.toArray(WebDavConstants.EMPTY_STRING_ARRAY);
    }

    public int getMessageCount() {
        int messageCount = 0;
        for (String uid : this.mData.keySet()) {
            String count = (String) this.mData.get(uid).get("visiblecount");
            if (count != null && !count.equals("")) {
                messageCount = Integer.parseInt(count);
            }
        }
        return messageCount;
    }

    public Map<String, ParsedMessageEnvelope> getMessageEnvelopes() {
        Map<String, ParsedMessageEnvelope> envelopes = new HashMap<>();
        for (String uid : this.mData.keySet()) {
            ParsedMessageEnvelope envelope = new ParsedMessageEnvelope();
            Map<String, String> data = this.mData.get(uid);
            if (data != null) {
                for (Map.Entry<String, String> entry : data.entrySet()) {
                    String header = (String) entry.getKey();
                    if (header.equals(EmailProvider.MessageColumns.READ)) {
                        envelope.setReadStatus(!((String) entry.getValue()).equals("0"));
                    } else if (header.equals("date")) {
                        String date = (String) entry.getValue();
                        String date2 = date.substring(0, date.length() - 1);
                        String tempDate = "";
                        try {
                            tempDate = new SimpleDateFormat("EEE, d MMM yy HH:mm:ss Z", Locale.US).format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US).parse(date2));
                        } catch (ParseException pe) {
                            Log.e("k9", "Error parsing date: " + pe + "\nTrace: " + WebDavUtils.processException(pe));
                        }
                        envelope.addHeader(header, tempDate);
                    } else {
                        envelope.addHeader(header, (String) entry.getValue());
                    }
                }
            }
            envelopes.put(uid, envelope);
        }
        return envelopes;
    }
}
