package com.fsck.k9.mail.store.imap;

import android.text.TextUtils;
import android.util.Log;
import com.fsck.k9.mail.Body;
import com.fsck.k9.mail.Flag;
import com.fsck.k9.mail.Folder;
import com.fsck.k9.mail.K9MailLib;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.MessageRetrievalListener;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.Part;
import com.fsck.k9.mail.filter.EOLConvertingOutputStream;
import com.fsck.k9.mail.internet.MimeBodyPart;
import com.fsck.k9.mail.internet.MimeMessageHelper;
import com.fsck.k9.mail.internet.MimeMultipart;
import com.fsck.k9.mail.internet.MimeUtility;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.james.mime4j.dom.field.ContentDispositionField;
import org.apache.james.mime4j.dom.field.FieldName;

class ImapFolder extends Folder<ImapMessage> {
    private static final int FETCH_WINDOW_SIZE = 100;
    private static final int MORE_MESSAGES_WINDOW_SIZE = 500;
    private static final ThreadLocal<SimpleDateFormat> RFC3501_DATE = new ThreadLocal<SimpleDateFormat>() {
        /* access modifiers changed from: protected */
        public SimpleDateFormat initialValue() {
            return new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
        }
    };
    private boolean canCreateKeywords;
    protected volatile ImapConnection connection;
    private volatile boolean exists;
    private final FolderNameCodec folderNameCodec;
    private boolean inSearch;
    protected volatile int messageCount;
    private int mode;
    protected Map<Long, String> msgSeqUidMap;
    private final String name;
    protected ImapStore store;
    protected volatile long uidNext;

    public ImapFolder(ImapStore store2, String name2) {
        this(store2, name2, store2.getFolderNameCodec());
    }

    ImapFolder(ImapStore store2, String name2, FolderNameCodec folderNameCodec2) {
        this.messageCount = -1;
        this.uidNext = -1;
        this.store = null;
        this.msgSeqUidMap = new ConcurrentHashMap();
        this.inSearch = false;
        this.canCreateKeywords = false;
        this.store = store2;
        this.name = name2;
        this.folderNameCodec = folderNameCodec2;
    }

    private String getPrefixedName() throws MessagingException {
        ImapConnection connection2;
        String prefixedName = "";
        if (!this.store.getStoreConfig().getInboxFolderName().equalsIgnoreCase(this.name)) {
            synchronized (this) {
                if (this.connection == null) {
                    connection2 = this.store.getConnection();
                } else {
                    connection2 = this.connection;
                }
            }
            try {
                connection2.open();
                if (this.connection == null) {
                    this.store.releaseConnection(connection2);
                }
                prefixedName = this.store.getCombinedPrefix();
            } catch (IOException ioe) {
                throw new MessagingException("Unable to get IMAP prefix", ioe);
            } catch (Throwable th) {
                if (this.connection == null) {
                    this.store.releaseConnection(connection2);
                }
                throw th;
            }
        }
        return prefixedName + this.name;
    }

    /* access modifiers changed from: private */
    public List<ImapResponse> executeSimpleCommand(String command) throws MessagingException, IOException {
        return handleUntaggedResponses(this.connection.executeSimpleCommand(command));
    }

    public void open(int mode2) throws MessagingException {
        internalOpen(mode2);
        if (this.messageCount == -1) {
            throw new MessagingException("Did not find message count during open");
        }
    }

    /* access modifiers changed from: protected */
    public List<ImapResponse> internalOpen(int mode2) throws MessagingException {
        if (isOpen() && this.mode == mode2) {
            try {
                return executeSimpleCommand(Commands.NOOP);
            } catch (IOException ioe) {
                ioExceptionHandler(this.connection, ioe);
            }
        }
        this.store.releaseConnection(this.connection);
        synchronized (this) {
            this.connection = this.store.getConnection();
        }
        try {
            this.msgSeqUidMap.clear();
            List<ImapResponse> responses = executeSimpleCommand(String.format("%s %s", mode2 == 0 ? "SELECT" : "EXAMINE", ImapUtility.encodeString(this.folderNameCodec.encode(getPrefixedName()))));
            this.mode = mode2;
            for (ImapResponse response : responses) {
                handlePermanentFlags(response);
            }
            handleSelectOrExamineOkResponse(ImapUtility.getLastResponse(responses));
            this.exists = true;
            return responses;
        } catch (IOException ioe2) {
            throw ioExceptionHandler(this.connection, ioe2);
        } catch (MessagingException me2) {
            Log.e("k9", "Unable to open connection for " + getLogId(), me2);
            throw me2;
        }
    }

    private void handlePermanentFlags(ImapResponse response) {
        PermanentFlagsResponse permanentFlagsResponse = PermanentFlagsResponse.parse(response);
        if (permanentFlagsResponse != null) {
            this.store.getPermanentFlagsIndex().addAll(permanentFlagsResponse.getFlags());
            this.canCreateKeywords = permanentFlagsResponse.canCreateKeywords();
        }
    }

    private void handleSelectOrExamineOkResponse(ImapResponse response) {
        SelectOrExamineResponse selectOrExamineResponse = SelectOrExamineResponse.parse(response);
        if (selectOrExamineResponse != null && selectOrExamineResponse.hasOpenMode()) {
            this.mode = selectOrExamineResponse.getOpenMode();
        }
    }

    public boolean isOpen() {
        return this.connection != null;
    }

    public int getMode() {
        return this.mode;
    }

    public void close() {
        this.messageCount = -1;
        if (isOpen()) {
            synchronized (this) {
                if (!this.inSearch || this.connection == null) {
                    this.store.releaseConnection(this.connection);
                } else {
                    Log.i("k9", "IMAP search was aborted, shutting down connection.");
                    this.connection.close();
                }
                this.connection = null;
            }
        }
    }

    public String getName() {
        return this.name;
    }

    private boolean exists(String escapedFolderName) throws MessagingException {
        try {
            this.connection.executeSimpleCommand(String.format("STATUS %s (RECENT)", escapedFolderName));
            return true;
        } catch (IOException ioe) {
            throw ioExceptionHandler(this.connection, ioe);
        } catch (NegativeImapResponseException e) {
            return false;
        }
    }

    public boolean exists() throws MessagingException {
        ImapConnection connection2;
        if (this.exists) {
            return true;
        }
        synchronized (this) {
            if (this.connection == null) {
                connection2 = this.store.getConnection();
            } else {
                connection2 = this.connection;
            }
        }
        try {
            connection2.executeSimpleCommand(String.format("STATUS %s (UIDVALIDITY)", ImapUtility.encodeString(this.folderNameCodec.encode(getPrefixedName()))));
            this.exists = true;
            if (this.connection != null) {
                return true;
            }
            this.store.releaseConnection(connection2);
            return true;
        } catch (NegativeImapResponseException e) {
            if (this.connection == null) {
                this.store.releaseConnection(connection2);
            }
            return false;
        } catch (IOException ioe) {
            throw ioExceptionHandler(connection2, ioe);
        } catch (Throwable th) {
            if (this.connection == null) {
                this.store.releaseConnection(connection2);
            }
            throw th;
        }
    }

    public boolean create(Folder.FolderType type) throws MessagingException {
        ImapConnection connection2;
        synchronized (this) {
            if (this.connection == null) {
                connection2 = this.store.getConnection();
            } else {
                connection2 = this.connection;
            }
        }
        try {
            connection2.executeSimpleCommand(String.format("CREATE %s", ImapUtility.encodeString(this.folderNameCodec.encode(getPrefixedName()))));
            if (this.connection != null) {
                return true;
            }
            this.store.releaseConnection(connection2);
            return true;
        } catch (NegativeImapResponseException e) {
            if (this.connection == null) {
                this.store.releaseConnection(connection2);
            }
            return false;
        } catch (IOException ioe) {
            throw ioExceptionHandler(this.connection, ioe);
        } catch (Throwable th) {
            if (this.connection == null) {
                this.store.releaseConnection(connection2);
            }
            throw th;
        }
    }

    public Map<String, String> copyMessages(List<? extends Message> messages, Folder folder) throws MessagingException {
        if (!(folder instanceof ImapFolder)) {
            throw new MessagingException("ImapFolder.copyMessages passed non-ImapFolder");
        } else if (messages.isEmpty()) {
            return null;
        } else {
            ImapFolder imapFolder = (ImapFolder) folder;
            checkOpen();
            String[] uids = new String[messages.size()];
            int count = messages.size();
            for (int i = 0; i < count; i++) {
                uids[i] = ((Message) messages.get(i)).getUid();
            }
            try {
                String escapedDestinationFolderName = ImapUtility.encodeString(this.folderNameCodec.encode(imapFolder.getPrefixedName()));
                if (!exists(escapedDestinationFolderName)) {
                    if (K9MailLib.isDebug()) {
                        Log.i("k9", "ImapFolder.copyMessages: attempting to create remote folder '" + escapedDestinationFolderName + "' for " + getLogId());
                    }
                    imapFolder.create(Folder.FolderType.HOLDS_MESSAGES);
                }
                CopyUidResponse copyUidResponse = CopyUidResponse.parse(ImapUtility.getLastResponse(executeSimpleCommand(String.format("UID COPY %s %s", combine(uids, ','), escapedDestinationFolderName))));
                if (copyUidResponse == null) {
                    return null;
                }
                return copyUidResponse.getUidMapping();
            } catch (IOException ioe) {
                throw ioExceptionHandler(this.connection, ioe);
            }
        }
    }

    public Map<String, String> moveMessages(List<? extends Message> messages, Folder folder) throws MessagingException {
        if (messages.isEmpty()) {
            return null;
        }
        Map<String, String> copyMessages = copyMessages(messages, folder);
        setFlags(messages, Collections.singleton(Flag.DELETED), true);
        return copyMessages;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fsck.k9.mail.MessagingException.<init>(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.fsck.k9.mail.MessagingException.<init>(java.lang.String, java.lang.Throwable):void
      com.fsck.k9.mail.MessagingException.<init>(java.lang.String, boolean):void */
    public void delete(List<? extends Message> messages, String trashFolderName) throws MessagingException {
        if (!messages.isEmpty()) {
            if (trashFolderName == null || getName().equalsIgnoreCase(trashFolderName)) {
                setFlags(messages, Collections.singleton(Flag.DELETED), true);
                return;
            }
            ImapFolder remoteTrashFolder = getStore().getFolder(trashFolderName);
            String escapedTrashFolderName = ImapUtility.encodeString(this.folderNameCodec.encode(remoteTrashFolder.getPrefixedName()));
            if (!exists(escapedTrashFolderName)) {
                if (K9MailLib.isDebug()) {
                    Log.i("k9", "IMAPMessage.delete: attempting to create remote '" + trashFolderName + "' folder " + "for " + getLogId());
                }
                remoteTrashFolder.create(Folder.FolderType.HOLDS_MESSAGES);
            }
            if (exists(escapedTrashFolderName)) {
                if (K9MailLib.isDebug()) {
                    Log.d("k9", "IMAPMessage.delete: copying remote " + messages.size() + " messages to '" + trashFolderName + "' for " + getLogId());
                }
                moveMessages(messages, remoteTrashFolder);
                return;
            }
            throw new MessagingException("IMAPMessage.delete: remote Trash folder " + trashFolderName + " does not exist and could not be created for " + getLogId(), true);
        }
    }

    public int getMessageCount() {
        return this.messageCount;
    }

    private int getRemoteMessageCount(String criteria) throws MessagingException {
        checkOpen();
        int count = 0;
        try {
            for (ImapResponse response : executeSimpleCommand(String.format(Locale.US, "SEARCH %d:* %s", 1, criteria))) {
                if (ImapResponseParser.equalsIgnoreCase(response.get(0), Responses.SEARCH)) {
                    count += response.size() - 1;
                }
            }
            return count;
        } catch (IOException ioe) {
            throw ioExceptionHandler(this.connection, ioe);
        }
    }

    public int getUnreadMessageCount() throws MessagingException {
        return getRemoteMessageCount("UNSEEN NOT DELETED");
    }

    public int getFlaggedMessageCount() throws MessagingException {
        return getRemoteMessageCount("FLAGGED NOT DELETED");
    }

    /* access modifiers changed from: protected */
    public long getHighestUid() throws MessagingException {
        try {
            return extractHighestUid(SearchResponse.parse(executeSimpleCommand("UID SEARCH *:*")));
        } catch (NegativeImapResponseException e) {
            return -1;
        } catch (IOException ioe) {
            throw ioExceptionHandler(this.connection, ioe);
        }
    }

    private long extractHighestUid(SearchResponse searchResponse) {
        List<Long> uids = searchResponse.getNumbers();
        if (uids.isEmpty()) {
            return -1;
        }
        if (uids.size() == 1) {
            return uids.get(0).longValue();
        }
        Collections.sort(uids, Collections.reverseOrder());
        return uids.get(0).longValue();
    }

    public void delete(boolean recurse) throws MessagingException {
        throw new Error("ImapStore.delete() not yet implemented");
    }

    public ImapMessage getMessage(String uid) throws MessagingException {
        return new ImapMessage(uid, this);
    }

    public List<ImapMessage> getMessages(int start, int end, Date earliestDate, MessageRetrievalListener<ImapMessage> listener) throws MessagingException {
        return getMessages(start, end, earliestDate, false, listener);
    }

    /* access modifiers changed from: protected */
    public List<ImapMessage> getMessages(int start, int end, Date earliestDate, boolean includeDeleted, MessageRetrievalListener<ImapMessage> listener) throws MessagingException {
        if (start < 1 || end < 1 || end < start) {
            throw new MessagingException(String.format(Locale.US, "Invalid message set %d %d", Integer.valueOf(start), Integer.valueOf(end)));
        }
        final String dateSearchString = getDateSearchString(earliestDate);
        final int i = start;
        final int i2 = end;
        final boolean z = includeDeleted;
        return search(new ImapSearcher() {
            public List<ImapResponse> search() throws IOException, MessagingException {
                Locale locale = Locale.US;
                Object[] objArr = new Object[4];
                objArr[0] = Integer.valueOf(i);
                objArr[1] = Integer.valueOf(i2);
                objArr[2] = dateSearchString;
                objArr[3] = z ? "" : " NOT DELETED";
                return ImapFolder.this.executeSimpleCommand(String.format(locale, "UID SEARCH %d:%d%s%s", objArr));
            }
        }, listener);
    }

    private String getDateSearchString(Date earliestDate) {
        if (earliestDate == null) {
            return "";
        }
        return " SINCE " + RFC3501_DATE.get().format(earliestDate);
    }

    public boolean areMoreMessagesAvailable(int indexOfOldestMessage, Date earliestDate) throws IOException, MessagingException {
        checkOpen();
        if (indexOfOldestMessage == 1) {
            return false;
        }
        String dateSearchString = getDateSearchString(earliestDate);
        for (int endIndex = indexOfOldestMessage - 1; endIndex > 0; endIndex -= 500) {
            if (existsNonDeletedMessageInRange(Math.max(0, endIndex - 500) + 1, endIndex, dateSearchString)) {
                return true;
            }
        }
        return false;
    }

    private boolean existsNonDeletedMessageInRange(int startIndex, int endIndex, String dateSearchString) throws MessagingException, IOException {
        for (ImapResponse response : executeSimpleCommand(String.format(Locale.US, "SEARCH %d:%d%s NOT DELETED", Integer.valueOf(startIndex), Integer.valueOf(endIndex), dateSearchString))) {
            if (response.getTag() == null && ImapResponseParser.equalsIgnoreCase(response.get(0), Responses.SEARCH) && response.size() > 1) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public List<ImapMessage> getMessages(final List<Long> mesgSeqs, final boolean includeDeleted, MessageRetrievalListener<ImapMessage> listener) throws MessagingException {
        return search(new ImapSearcher() {
            public List<ImapResponse> search() throws IOException, MessagingException {
                Object[] objArr = new Object[2];
                objArr[0] = ImapFolder.combine(mesgSeqs.toArray(), ',');
                objArr[1] = includeDeleted ? "" : " NOT DELETED";
                return ImapFolder.this.executeSimpleCommand(String.format("UID SEARCH %s%s", objArr));
            }
        }, listener);
    }

    /* access modifiers changed from: protected */
    public List<ImapMessage> getMessagesFromUids(final List<String> mesgUids) throws MessagingException {
        return search(new ImapSearcher() {
            public List<ImapResponse> search() throws IOException, MessagingException {
                return ImapFolder.this.executeSimpleCommand(String.format("UID SEARCH UID %s", ImapFolder.combine(mesgUids.toArray(), ',')));
            }
        }, null);
    }

    private List<ImapMessage> search(ImapSearcher searcher, MessageRetrievalListener<ImapMessage> listener) throws MessagingException {
        checkOpen();
        List<ImapMessage> messages = new ArrayList<>();
        try {
            List<Long> uids = SearchResponse.parse(searcher.search()).getNumbers();
            Collections.sort(uids, Collections.reverseOrder());
            int count = uids.size();
            for (int i = 0; i < count; i++) {
                String uid = uids.get(i).toString();
                if (listener != null) {
                    listener.messageStarted(uid, i, count);
                }
                ImapMessage message = new ImapMessage(uid, this);
                messages.add(message);
                if (listener != null) {
                    listener.messageFinished(message, i, count);
                }
            }
            return messages;
        } catch (IOException ioe) {
            throw ioExceptionHandler(this.connection, ioe);
        }
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x018b A[Catch:{ IOException -> 0x028b }] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0251 A[LOOP:1: B:25:0x00fa->B:55:0x0251, LOOP_END] */
    public void fetch(java.util.List<com.fsck.k9.mail.store.imap.ImapMessage> r35, com.fsck.k9.mail.FetchProfile r36, com.fsck.k9.mail.MessageRetrievalListener<com.fsck.k9.mail.store.imap.ImapMessage> r37) throws com.fsck.k9.mail.MessagingException {
        /*
            r34 = this;
            if (r35 == 0) goto L_0x0008
            boolean r29 = r35.isEmpty()
            if (r29 == 0) goto L_0x0009
        L_0x0008:
            return
        L_0x0009:
            r34.checkOpen()
            java.util.ArrayList r26 = new java.util.ArrayList
            int r29 = r35.size()
            r0 = r26
            r1 = r29
            r0.<init>(r1)
            java.util.HashMap r17 = new java.util.HashMap
            r17.<init>()
            java.util.Iterator r29 = r35.iterator()
        L_0x0022:
            boolean r30 = r29.hasNext()
            if (r30 == 0) goto L_0x0043
            java.lang.Object r16 = r29.next()
            com.fsck.k9.mail.Message r16 = (com.fsck.k9.mail.Message) r16
            java.lang.String r24 = r16.getUid()
            r0 = r26
            r1 = r24
            r0.add(r1)
            r0 = r17
            r1 = r24
            r2 = r16
            r0.put(r1, r2)
            goto L_0x0022
        L_0x0043:
            java.util.LinkedHashSet r10 = new java.util.LinkedHashSet
            r10.<init>()
            java.lang.String r29 = "UID"
            r0 = r29
            r10.add(r0)
            com.fsck.k9.mail.FetchProfile$Item r29 = com.fsck.k9.mail.FetchProfile.Item.FLAGS
            r0 = r36
            r1 = r29
            boolean r29 = r0.contains(r1)
            if (r29 == 0) goto L_0x0062
            java.lang.String r29 = "FLAGS"
            r0 = r29
            r10.add(r0)
        L_0x0062:
            com.fsck.k9.mail.FetchProfile$Item r29 = com.fsck.k9.mail.FetchProfile.Item.ENVELOPE
            r0 = r36
            r1 = r29
            boolean r29 = r0.contains(r1)
            if (r29 == 0) goto L_0x0083
            java.lang.String r29 = "INTERNALDATE"
            r0 = r29
            r10.add(r0)
            java.lang.String r29 = "RFC822.SIZE"
            r0 = r29
            r10.add(r0)
            java.lang.String r29 = "BODY.PEEK[HEADER.FIELDS (date subject from content-type to cc reply-to message-id references in-reply-to X-K9mail-Identity)]"
            r0 = r29
            r10.add(r0)
        L_0x0083:
            com.fsck.k9.mail.FetchProfile$Item r29 = com.fsck.k9.mail.FetchProfile.Item.STRUCTURE
            r0 = r36
            r1 = r29
            boolean r29 = r0.contains(r1)
            if (r29 == 0) goto L_0x0096
            java.lang.String r29 = "BODYSTRUCTURE"
            r0 = r29
            r10.add(r0)
        L_0x0096:
            com.fsck.k9.mail.FetchProfile$Item r29 = com.fsck.k9.mail.FetchProfile.Item.BODY_SANE
            r0 = r36
            r1 = r29
            boolean r29 = r0.contains(r1)
            if (r29 == 0) goto L_0x00cf
            r0 = r34
            com.fsck.k9.mail.store.imap.ImapStore r0 = r0.store
            r29 = r0
            com.fsck.k9.mail.store.StoreConfig r29 = r29.getStoreConfig()
            int r15 = r29.getMaximumAutoDownloadMessageSize()
            if (r15 <= 0) goto L_0x0255
            java.util.Locale r29 = java.util.Locale.US
            java.lang.String r30 = "BODY.PEEK[]<0.%d>"
            r31 = 1
            r0 = r31
            java.lang.Object[] r0 = new java.lang.Object[r0]
            r31 = r0
            r32 = 0
            java.lang.Integer r33 = java.lang.Integer.valueOf(r15)
            r31[r32] = r33
            java.lang.String r29 = java.lang.String.format(r29, r30, r31)
            r0 = r29
            r10.add(r0)
        L_0x00cf:
            com.fsck.k9.mail.FetchProfile$Item r29 = com.fsck.k9.mail.FetchProfile.Item.BODY
            r0 = r36
            r1 = r29
            boolean r29 = r0.contains(r1)
            if (r29 == 0) goto L_0x00e2
            java.lang.String r29 = "BODY.PEEK[]"
            r0 = r29
            r10.add(r0)
        L_0x00e2:
            int r29 = r10.size()
            r0 = r29
            java.lang.String[] r0 = new java.lang.String[r0]
            r29 = r0
            r0 = r29
            java.lang.Object[] r29 = r10.toArray(r0)
            r30 = 32
            java.lang.String r23 = combine(r29, r30)
            r28 = 0
        L_0x00fa:
            int r29 = r35.size()
            r0 = r28
            r1 = r29
            if (r0 >= r1) goto L_0x0008
            int r29 = r28 + 100
            int r30 = r35.size()
            int r27 = java.lang.Math.min(r29, r30)
            r0 = r26
            r1 = r28
            r2 = r27
            java.util.List r25 = r0.subList(r1, r2)
            int r29 = r25.size()     // Catch:{ IOException -> 0x028b }
            r0 = r29
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ IOException -> 0x028b }
            r29 = r0
            r0 = r25
            r1 = r29
            java.lang.Object[] r29 = r0.toArray(r1)     // Catch:{ IOException -> 0x028b }
            r30 = 44
            java.lang.String r7 = combine(r29, r30)     // Catch:{ IOException -> 0x028b }
            java.lang.String r29 = "UID FETCH %s (%s)"
            r30 = 2
            r0 = r30
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ IOException -> 0x028b }
            r30 = r0
            r31 = 0
            r30[r31] = r7     // Catch:{ IOException -> 0x028b }
            r31 = 1
            r30[r31] = r23     // Catch:{ IOException -> 0x028b }
            java.lang.String r8 = java.lang.String.format(r29, r30)     // Catch:{ IOException -> 0x028b }
            r0 = r34
            com.fsck.k9.mail.store.imap.ImapConnection r0 = r0.connection     // Catch:{ IOException -> 0x028b }
            r29 = r0
            r30 = 0
            r0 = r29
            r1 = r30
            r0.sendCommand(r8, r1)     // Catch:{ IOException -> 0x028b }
            r18 = 0
            r6 = 0
            com.fsck.k9.mail.FetchProfile$Item r29 = com.fsck.k9.mail.FetchProfile.Item.BODY     // Catch:{ IOException -> 0x028b }
            r0 = r36
            r1 = r29
            boolean r29 = r0.contains(r1)     // Catch:{ IOException -> 0x028b }
            if (r29 != 0) goto L_0x0170
            com.fsck.k9.mail.FetchProfile$Item r29 = com.fsck.k9.mail.FetchProfile.Item.BODY_SANE     // Catch:{ IOException -> 0x028b }
            r0 = r36
            r1 = r29
            boolean r29 = r0.contains(r1)     // Catch:{ IOException -> 0x028b }
            if (r29 == 0) goto L_0x02fd
        L_0x0170:
            com.fsck.k9.mail.store.imap.FetchBodyCallback r6 = new com.fsck.k9.mail.store.imap.FetchBodyCallback     // Catch:{ IOException -> 0x028b }
            r0 = r17
            r6.<init>(r0)     // Catch:{ IOException -> 0x028b }
            r19 = r18
        L_0x0179:
            r0 = r34
            com.fsck.k9.mail.store.imap.ImapConnection r0 = r0.connection     // Catch:{ IOException -> 0x028b }
            r29 = r0
            r0 = r29
            com.fsck.k9.mail.store.imap.ImapResponse r22 = r0.readResponse(r6)     // Catch:{ IOException -> 0x028b }
            java.lang.String r29 = r22.getTag()     // Catch:{ IOException -> 0x028b }
            if (r29 != 0) goto L_0x02f2
            r29 = 1
            r0 = r22
            r1 = r29
            java.lang.Object r29 = r0.get(r1)     // Catch:{ IOException -> 0x028b }
            java.lang.String r30 = "FETCH"
            boolean r29 = com.fsck.k9.mail.store.imap.ImapResponseParser.equalsIgnoreCase(r29, r30)     // Catch:{ IOException -> 0x028b }
            if (r29 == 0) goto L_0x02f2
            java.lang.String r29 = "FETCH"
            r0 = r22
            r1 = r29
            java.lang.Object r11 = r0.getKeyedValue(r1)     // Catch:{ IOException -> 0x028b }
            com.fsck.k9.mail.store.imap.ImapList r11 = (com.fsck.k9.mail.store.imap.ImapList) r11     // Catch:{ IOException -> 0x028b }
            java.lang.String r29 = "UID"
            r0 = r29
            java.lang.String r24 = r11.getKeyedString(r0)     // Catch:{ IOException -> 0x028b }
            r29 = 0
            r0 = r22
            r1 = r29
            long r20 = r0.getLong(r1)     // Catch:{ IOException -> 0x028b }
            if (r24 == 0) goto L_0x0206
            r0 = r34
            java.util.Map<java.lang.Long, java.lang.String> r0 = r0.msgSeqUidMap     // Catch:{ Exception -> 0x025e }
            r29 = r0
            java.lang.Long r30 = java.lang.Long.valueOf(r20)     // Catch:{ Exception -> 0x025e }
            r0 = r29
            r1 = r30
            r2 = r24
            r0.put(r1, r2)     // Catch:{ Exception -> 0x025e }
            boolean r29 = com.fsck.k9.mail.K9MailLib.isDebug()     // Catch:{ Exception -> 0x025e }
            if (r29 == 0) goto L_0x0206
            java.lang.String r29 = "k9"
            java.lang.StringBuilder r30 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x025e }
            r30.<init>()     // Catch:{ Exception -> 0x025e }
            java.lang.String r31 = "Stored uid '"
            java.lang.StringBuilder r30 = r30.append(r31)     // Catch:{ Exception -> 0x025e }
            r0 = r30
            r1 = r24
            java.lang.StringBuilder r30 = r0.append(r1)     // Catch:{ Exception -> 0x025e }
            java.lang.String r31 = "' for msgSeq "
            java.lang.StringBuilder r30 = r30.append(r31)     // Catch:{ Exception -> 0x025e }
            r0 = r30
            r1 = r20
            java.lang.StringBuilder r30 = r0.append(r1)     // Catch:{ Exception -> 0x025e }
            java.lang.String r31 = " into map"
            java.lang.StringBuilder r30 = r30.append(r31)     // Catch:{ Exception -> 0x025e }
            java.lang.String r30 = r30.toString()     // Catch:{ Exception -> 0x025e }
            android.util.Log.v(r29, r30)     // Catch:{ Exception -> 0x025e }
        L_0x0206:
            r0 = r17
            r1 = r24
            java.lang.Object r16 = r0.get(r1)     // Catch:{ IOException -> 0x028b }
            com.fsck.k9.mail.Message r16 = (com.fsck.k9.mail.Message) r16     // Catch:{ IOException -> 0x028b }
            if (r16 != 0) goto L_0x029b
            boolean r29 = com.fsck.k9.mail.K9MailLib.isDebug()     // Catch:{ IOException -> 0x028b }
            if (r29 == 0) goto L_0x0242
            java.lang.String r29 = "k9"
            java.lang.StringBuilder r30 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x028b }
            r30.<init>()     // Catch:{ IOException -> 0x028b }
            java.lang.String r31 = "Do not have message in messageMap for UID "
            java.lang.StringBuilder r30 = r30.append(r31)     // Catch:{ IOException -> 0x028b }
            r0 = r30
            r1 = r24
            java.lang.StringBuilder r30 = r0.append(r1)     // Catch:{ IOException -> 0x028b }
            java.lang.String r31 = " for "
            java.lang.StringBuilder r30 = r30.append(r31)     // Catch:{ IOException -> 0x028b }
            java.lang.String r31 = r34.getLogId()     // Catch:{ IOException -> 0x028b }
            java.lang.StringBuilder r30 = r30.append(r31)     // Catch:{ IOException -> 0x028b }
            java.lang.String r30 = r30.toString()     // Catch:{ IOException -> 0x028b }
            android.util.Log.d(r29, r30)     // Catch:{ IOException -> 0x028b }
        L_0x0242:
            r0 = r34
            r1 = r22
            r0.handleUntaggedResponse(r1)     // Catch:{ IOException -> 0x028b }
            r18 = r19
        L_0x024b:
            java.lang.String r29 = r22.getTag()     // Catch:{ IOException -> 0x028b }
            if (r29 == 0) goto L_0x02fd
            int r28 = r28 + 100
            goto L_0x00fa
        L_0x0255:
            java.lang.String r29 = "BODY.PEEK[]"
            r0 = r29
            r10.add(r0)
            goto L_0x00cf
        L_0x025e:
            r9 = move-exception
            java.lang.String r29 = "k9"
            java.lang.StringBuilder r30 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x028b }
            r30.<init>()     // Catch:{ IOException -> 0x028b }
            java.lang.String r31 = "Unable to store uid '"
            java.lang.StringBuilder r30 = r30.append(r31)     // Catch:{ IOException -> 0x028b }
            r0 = r30
            r1 = r24
            java.lang.StringBuilder r30 = r0.append(r1)     // Catch:{ IOException -> 0x028b }
            java.lang.String r31 = "' for msgSeq "
            java.lang.StringBuilder r30 = r30.append(r31)     // Catch:{ IOException -> 0x028b }
            r0 = r30
            r1 = r20
            java.lang.StringBuilder r30 = r0.append(r1)     // Catch:{ IOException -> 0x028b }
            java.lang.String r30 = r30.toString()     // Catch:{ IOException -> 0x028b }
            android.util.Log.e(r29, r30)     // Catch:{ IOException -> 0x028b }
            goto L_0x0206
        L_0x028b:
            r13 = move-exception
            r0 = r34
            com.fsck.k9.mail.store.imap.ImapConnection r0 = r0.connection
            r29 = r0
            r0 = r34
            r1 = r29
            com.fsck.k9.mail.MessagingException r29 = r0.ioExceptionHandler(r1, r13)
            throw r29
        L_0x029b:
            if (r37 == 0) goto L_0x0301
            int r18 = r19 + 1
            int r29 = r17.size()     // Catch:{ IOException -> 0x028b }
            r0 = r37
            r1 = r24
            r2 = r19
            r3 = r29
            r0.messageStarted(r1, r2, r3)     // Catch:{ IOException -> 0x028b }
        L_0x02ae:
            r0 = r16
            com.fsck.k9.mail.store.imap.ImapMessage r0 = (com.fsck.k9.mail.store.imap.ImapMessage) r0     // Catch:{ IOException -> 0x028b }
            r12 = r0
            r0 = r34
            java.lang.Object r14 = r0.handleFetchResponse(r12, r11)     // Catch:{ IOException -> 0x028b }
            if (r14 == 0) goto L_0x02d3
            boolean r0 = r14 instanceof java.lang.String     // Catch:{ IOException -> 0x028b }
            r29 = r0
            if (r29 == 0) goto L_0x02e4
            r0 = r14
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ IOException -> 0x028b }
            r5 = r0
            java.io.ByteArrayInputStream r4 = new java.io.ByteArrayInputStream     // Catch:{ IOException -> 0x028b }
            byte[] r29 = r5.getBytes()     // Catch:{ IOException -> 0x028b }
            r0 = r29
            r4.<init>(r0)     // Catch:{ IOException -> 0x028b }
            r12.parse(r4)     // Catch:{ IOException -> 0x028b }
        L_0x02d3:
            if (r37 == 0) goto L_0x024b
            int r29 = r17.size()     // Catch:{ IOException -> 0x028b }
            r0 = r37
            r1 = r18
            r2 = r29
            r0.messageFinished(r12, r1, r2)     // Catch:{ IOException -> 0x028b }
            goto L_0x024b
        L_0x02e4:
            boolean r0 = r14 instanceof java.lang.Integer     // Catch:{ IOException -> 0x028b }
            r29 = r0
            if (r29 != 0) goto L_0x02d3
            com.fsck.k9.mail.MessagingException r29 = new com.fsck.k9.mail.MessagingException     // Catch:{ IOException -> 0x028b }
            java.lang.String r30 = "Got FETCH response with bogus parameters"
            r29.<init>(r30)     // Catch:{ IOException -> 0x028b }
            throw r29     // Catch:{ IOException -> 0x028b }
        L_0x02f2:
            r0 = r34
            r1 = r22
            r0.handleUntaggedResponse(r1)     // Catch:{ IOException -> 0x028b }
            r18 = r19
            goto L_0x024b
        L_0x02fd:
            r19 = r18
            goto L_0x0179
        L_0x0301:
            r18 = r19
            goto L_0x02ae
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fsck.k9.mail.store.imap.ImapFolder.fetch(java.util.List, com.fsck.k9.mail.FetchProfile, com.fsck.k9.mail.MessageRetrievalListener):void");
    }

    public void fetchPart(Message message, Part part, MessageRetrievalListener<Message> listener) throws MessagingException {
        String fetch;
        checkOpen();
        String partId = part.getServerExtra();
        if ("TEXT".equalsIgnoreCase(partId)) {
            int maximumAutoDownloadMessageSize = this.store.getStoreConfig().getMaximumAutoDownloadMessageSize();
            fetch = String.format(Locale.US, "BODY.PEEK[TEXT]<0.%d>", Integer.valueOf(maximumAutoDownloadMessageSize));
        } else {
            fetch = String.format("BODY.PEEK[%s]", partId);
        }
        try {
            this.connection.sendCommand(String.format("UID FETCH %s (UID %s)", message.getUid(), fetch), false);
            int messageNumber = 0;
            ImapResponseCallback callback = new FetchPartCallback(part);
            while (true) {
                int messageNumber2 = messageNumber;
                ImapResponse response = this.connection.readResponse(callback);
                if (response.getTag() != null || !ImapResponseParser.equalsIgnoreCase(response.get(1), "FETCH")) {
                    handleUntaggedResponse(response);
                    messageNumber = messageNumber2;
                } else {
                    ImapList fetchList = (ImapList) response.getKeyedValue("FETCH");
                    String uid = fetchList.getKeyedString("UID");
                    if (!message.getUid().equals(uid)) {
                        if (K9MailLib.isDebug()) {
                            Log.d("k9", "Did not ask for UID " + uid + " for " + getLogId());
                        }
                        handleUntaggedResponse(response);
                        messageNumber = messageNumber2;
                    } else {
                        if (listener != null) {
                            messageNumber = messageNumber2 + 1;
                            listener.messageStarted(uid, messageNumber2, 1);
                        } else {
                            messageNumber = messageNumber2;
                        }
                        Object literal = handleFetchResponse((ImapMessage) message, fetchList);
                        if (literal != null) {
                            if (literal instanceof Body) {
                                MimeMessageHelper.setBody(part, (Body) literal);
                            } else if (literal instanceof String) {
                                MimeMessageHelper.setBody(part, MimeUtility.createBody(new ByteArrayInputStream(((String) literal).getBytes()), part.getHeader("Content-Transfer-Encoding")[0], part.getHeader("Content-Type")[0]));
                            } else {
                                throw new MessagingException("Got FETCH response with bogus parameters");
                            }
                        }
                        if (listener != null) {
                            listener.messageFinished(message, messageNumber, 1);
                        }
                    }
                }
                if (response.getTag() != null) {
                    return;
                }
            }
        } catch (IOException ioe) {
            throw ioExceptionHandler(this.connection, ioe);
        }
    }

    private Object handleFetchResponse(ImapMessage message, ImapList fetchList) throws MessagingException {
        int index;
        int size;
        ImapList bs;
        ImapList flags;
        if (fetchList.containsKey("FLAGS") && (flags = fetchList.getKeyedList("FLAGS")) != null) {
            int count = flags.size();
            for (int i = 0; i < count; i++) {
                String flag = flags.getString(i);
                if (flag.equalsIgnoreCase("\\Deleted")) {
                    message.setFlagInternal(Flag.DELETED, true);
                } else if (flag.equalsIgnoreCase("\\Answered")) {
                    message.setFlagInternal(Flag.ANSWERED, true);
                } else if (flag.equalsIgnoreCase("\\Seen")) {
                    message.setFlagInternal(Flag.SEEN, true);
                } else if (flag.equalsIgnoreCase("\\Flagged")) {
                    message.setFlagInternal(Flag.FLAGGED, true);
                } else if (flag.equalsIgnoreCase("$Forwarded")) {
                    message.setFlagInternal(Flag.FORWARDED, true);
                    this.store.getPermanentFlagsIndex().add(Flag.FORWARDED);
                }
            }
        }
        if (fetchList.containsKey("INTERNALDATE")) {
            message.setInternalDate(fetchList.getKeyedDate("INTERNALDATE"));
        }
        if (fetchList.containsKey("RFC822.SIZE")) {
            message.setSize(fetchList.getKeyedNumber("RFC822.SIZE"));
        }
        if (fetchList.containsKey("BODYSTRUCTURE") && (bs = fetchList.getKeyedList("BODYSTRUCTURE")) != null) {
            try {
                parseBodyStructure(bs, message, "TEXT");
            } catch (MessagingException e) {
                if (K9MailLib.isDebug()) {
                    Log.d("k9", "Error handling message for " + getLogId(), e);
                }
                message.setBody(null);
            }
        }
        if (!fetchList.containsKey("BODY") || (index = fetchList.getKeyIndex("BODY") + 2) >= (size = fetchList.size())) {
            return null;
        }
        Object result = fetchList.getObject(index);
        if (!(result instanceof String) || !((String) result).startsWith("<") || index + 1 >= size) {
            return result;
        }
        return fetchList.getObject(index + 1);
    }

    /* access modifiers changed from: protected */
    public List<ImapResponse> handleUntaggedResponses(List<ImapResponse> responses) {
        for (ImapResponse response : responses) {
            handleUntaggedResponse(response);
        }
        return responses;
    }

    /* access modifiers changed from: protected */
    public void handlePossibleUidNext(ImapResponse response) {
        if (ImapResponseParser.equalsIgnoreCase(response.get(0), Responses.OK) && response.size() > 1) {
            Object bracketedObj = response.get(1);
            if (bracketedObj instanceof ImapList) {
                ImapList bracketed = (ImapList) bracketedObj;
                if (bracketed.size() > 1) {
                    Object keyObj = bracketed.get(0);
                    if ((keyObj instanceof String) && "UIDNEXT".equalsIgnoreCase((String) keyObj)) {
                        this.uidNext = bracketed.getLong(1);
                        if (K9MailLib.isDebug()) {
                            Log.d("k9", "Got UidNext = " + this.uidNext + " for " + getLogId());
                        }
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void handleUntaggedResponse(ImapResponse response) {
        if (response.getTag() == null && response.size() > 1) {
            if (ImapResponseParser.equalsIgnoreCase(response.get(1), Responses.EXISTS)) {
                this.messageCount = response.getNumber(0);
                if (K9MailLib.isDebug()) {
                    Log.d("k9", "Got untagged EXISTS with value " + this.messageCount + " for " + getLogId());
                }
            }
            handlePossibleUidNext(response);
            if (ImapResponseParser.equalsIgnoreCase(response.get(1), Responses.EXPUNGE) && this.messageCount > 0) {
                this.messageCount--;
                if (K9MailLib.isDebug()) {
                    Log.d("k9", "Got untagged EXPUNGE with messageCount " + this.messageCount + " for " + getLogId());
                }
            }
        }
    }

    private void parseBodyStructure(ImapList bs, Part part, String id) throws MessagingException {
        if (bs.get(0) instanceof ImapList) {
            MimeMultipart mp = MimeMultipart.newInstance();
            int i = 0;
            int count = bs.size();
            while (true) {
                if (i < count) {
                    if (!(bs.get(i) instanceof ImapList)) {
                        mp.setSubType(bs.getString(i).toLowerCase(Locale.US));
                        break;
                    }
                    MimeBodyPart bp = new MimeBodyPart();
                    if (id.equalsIgnoreCase("TEXT")) {
                        parseBodyStructure(bs.getList(i), bp, Integer.toString(i + 1));
                    } else {
                        parseBodyStructure(bs.getList(i), bp, id + "." + (i + 1));
                    }
                    mp.addBodyPart(bp);
                    i++;
                } else {
                    break;
                }
            }
            MimeMessageHelper.setBody(part, mp);
            return;
        }
        String type = bs.getString(0);
        String mimeType = (type + "/" + bs.getString(1)).toLowerCase(Locale.US);
        ImapList bodyParams = null;
        if (bs.get(2) instanceof ImapList) {
            bodyParams = bs.getList(2);
        }
        String encoding = bs.getString(5);
        int size = bs.getNumber(6);
        if (MimeUtility.isMessage(mimeType)) {
            throw new MessagingException("BODYSTRUCTURE message/rfc822 not yet supported.");
        }
        StringBuilder contentType = new StringBuilder();
        contentType.append(mimeType);
        if (bodyParams != null) {
            int count2 = bodyParams.size();
            for (int i2 = 0; i2 < count2; i2 += 2) {
                contentType.append(String.format(";\r\n %s=\"%s\"", bodyParams.getString(i2), bodyParams.getString(i2 + 1)));
            }
        }
        part.setHeader("Content-Type", contentType.toString());
        ImapList bodyDisposition = null;
        if ("text".equalsIgnoreCase(type) && bs.size() > 9 && (bs.get(9) instanceof ImapList)) {
            bodyDisposition = bs.getList(9);
        } else if (!"text".equalsIgnoreCase(type) && bs.size() > 8 && (bs.get(8) instanceof ImapList)) {
            bodyDisposition = bs.getList(8);
        }
        StringBuilder contentDisposition = new StringBuilder();
        if (bodyDisposition != null && !bodyDisposition.isEmpty()) {
            if (!"NIL".equalsIgnoreCase(bodyDisposition.getString(0))) {
                contentDisposition.append(bodyDisposition.getString(0).toLowerCase(Locale.US));
            }
            if (bodyDisposition.size() > 1 && (bodyDisposition.get(1) instanceof ImapList)) {
                ImapList bodyDispositionParams = bodyDisposition.getList(1);
                int count3 = bodyDispositionParams.size();
                for (int i3 = 0; i3 < count3; i3 += 2) {
                    contentDisposition.append(String.format(";\r\n %s=\"%s\"", bodyDispositionParams.getString(i3).toLowerCase(Locale.US), bodyDispositionParams.getString(i3 + 1)));
                }
            }
        }
        if (MimeUtility.getHeaderParameter(contentDisposition.toString(), ContentDispositionField.PARAM_SIZE) == null) {
            contentDisposition.append(String.format(Locale.US, ";\r\n size=%d", Integer.valueOf(size)));
        }
        part.setHeader("Content-Disposition", contentDisposition.toString());
        part.setHeader("Content-Transfer-Encoding", encoding);
        if (part instanceof ImapMessage) {
            ((ImapMessage) part).setSize(size);
        }
        part.setServerExtra(id);
    }

    public Map<String, String> appendMessages(List<? extends Message> messages) throws MessagingException {
        ImapResponse response;
        open(0);
        checkOpen();
        try {
            Map<String, String> uidMap = new HashMap<>();
            for (Message message : messages) {
                long messageSize = message.calculateSize();
                String escapedFolderName = ImapUtility.encodeString(this.folderNameCodec.encode(getPrefixedName()));
                this.connection.sendCommand(String.format(Locale.US, "APPEND %s (%s) {%d}", escapedFolderName, combineFlags(message.getFlags()), Long.valueOf(messageSize)), false);
                do {
                    response = this.connection.readResponse();
                    handleUntaggedResponse(response);
                    if (response.isContinuationRequested()) {
                        EOLConvertingOutputStream eolOut = new EOLConvertingOutputStream(this.connection.getOutputStream());
                        message.writeTo(eolOut);
                        eolOut.write(13);
                        eolOut.write(10);
                        eolOut.flush();
                    }
                } while (response.getTag() == null);
                if (response.size() > 1) {
                    Object responseList = response.get(1);
                    if (responseList instanceof ImapList) {
                        ImapList appendList = (ImapList) responseList;
                        if (appendList.size() >= 3 && appendList.getString(0).equals("APPENDUID")) {
                            String newUid = appendList.getString(2);
                            if (!TextUtils.isEmpty(newUid)) {
                                message.setUid(newUid);
                                uidMap.put(message.getUid(), newUid);
                            }
                        }
                    }
                }
                String newUid2 = getUidFromMessageId(message);
                if (K9MailLib.isDebug()) {
                    Log.d("k9", "Got UID " + newUid2 + " for message for " + getLogId());
                }
                if (!TextUtils.isEmpty(newUid2)) {
                    uidMap.put(message.getUid(), newUid2);
                    message.setUid(newUid2);
                }
            }
            if (uidMap.isEmpty()) {
                return null;
            }
            return uidMap;
        } catch (IOException ioe) {
            throw ioExceptionHandler(this.connection, ioe);
        }
    }

    public String getUidFromMessageId(Message message) throws MessagingException {
        try {
            String[] messageIdHeader = message.getHeader(FieldName.MESSAGE_ID);
            if (messageIdHeader.length != 0) {
                String messageId = messageIdHeader[0];
                if (K9MailLib.isDebug()) {
                    Log.d("k9", "Looking for UID for message with message-id " + messageId + " for " + getLogId());
                }
                for (ImapResponse response : executeSimpleCommand(String.format("UID SEARCH HEADER MESSAGE-ID %s", ImapUtility.encodeString(messageId)))) {
                    if (response.getTag() == null && ImapResponseParser.equalsIgnoreCase(response.get(0), Responses.SEARCH) && response.size() > 1) {
                        return response.getString(1);
                    }
                }
                return null;
            } else if (!K9MailLib.isDebug()) {
                return null;
            } else {
                Log.d("k9", "Did not get a message-id in order to search for UID  for " + getLogId());
                return null;
            }
        } catch (IOException ioe) {
            throw new MessagingException("Could not find UID for message based on Message-ID", ioe);
        }
    }

    public void expunge() throws MessagingException {
        open(0);
        checkOpen();
        try {
            executeSimpleCommand(Responses.EXPUNGE);
        } catch (IOException ioe) {
            throw ioExceptionHandler(this.connection, ioe);
        }
    }

    private String combineFlags(Iterable<Flag> flags) {
        List<String> flagNames = new ArrayList<>();
        for (Flag flag : flags) {
            if (flag == Flag.SEEN) {
                flagNames.add("\\Seen");
            } else if (flag == Flag.DELETED) {
                flagNames.add("\\Deleted");
            } else if (flag == Flag.ANSWERED) {
                flagNames.add("\\Answered");
            } else if (flag == Flag.FLAGGED) {
                flagNames.add("\\Flagged");
            } else if (flag == Flag.FORWARDED && (this.canCreateKeywords || this.store.getPermanentFlagsIndex().contains(Flag.FORWARDED))) {
                flagNames.add("$Forwarded");
            }
        }
        return combine(flagNames.toArray(new String[flagNames.size()]), ' ');
    }

    public void setFlags(Set<Flag> flags, boolean value) throws MessagingException {
        open(0);
        checkOpen();
        try {
            Object[] objArr = new Object[2];
            objArr[0] = value ? "+" : "-";
            objArr[1] = combineFlags(flags);
            executeSimpleCommand(String.format("UID STORE 1:* %sFLAGS.SILENT (%s)", objArr));
        } catch (IOException ioe) {
            throw ioExceptionHandler(this.connection, ioe);
        }
    }

    public String getNewPushState(String oldSerializedPushState, Message message) {
        try {
            long messageUid = Long.parseLong(message.getUid());
            if (messageUid >= ImapPushState.parse(oldSerializedPushState).uidNext) {
                return new ImapPushState(messageUid + 1).toString();
            }
            return null;
        } catch (Exception e) {
            Log.e("k9", "Exception while updated push state for " + getLogId(), e);
            return null;
        }
    }

    public void setFlags(List<? extends Message> messages, Set<Flag> flags, boolean value) throws MessagingException {
        open(0);
        checkOpen();
        String[] uids = new String[messages.size()];
        int count = messages.size();
        for (int i = 0; i < count; i++) {
            uids[i] = ((Message) messages.get(i)).getUid();
        }
        try {
            Object[] objArr = new Object[3];
            objArr[0] = combine(uids, ',');
            objArr[1] = value ? "+" : "-";
            objArr[2] = combineFlags(flags);
            executeSimpleCommand(String.format("UID STORE %s %sFLAGS.SILENT (%s)", objArr));
        } catch (IOException ioe) {
            throw ioExceptionHandler(this.connection, ioe);
        }
    }

    private void checkOpen() throws MessagingException {
        if (!isOpen()) {
            throw new MessagingException("Folder " + getPrefixedName() + " is not open.");
        }
    }

    private MessagingException ioExceptionHandler(ImapConnection connection2, IOException ioe) {
        Log.e("k9", "IOException for " + getLogId(), ioe);
        if (connection2 != null) {
            connection2.close();
        }
        close();
        return new MessagingException("IO Error", ioe);
    }

    public boolean equals(Object other) {
        if (other instanceof ImapFolder) {
            return ((ImapFolder) other).getName().equalsIgnoreCase(getName());
        }
        return super.equals(other);
    }

    public int hashCode() {
        return getName().hashCode();
    }

    private ImapStore getStore() {
        return this.store;
    }

    /* access modifiers changed from: protected */
    public String getLogId() {
        String id = this.store.getStoreConfig().toString() + ":" + getName() + "/" + Thread.currentThread().getName();
        if (this.connection != null) {
            return id + "/" + this.connection.getLogId();
        }
        return id;
    }

    /* JADX INFO: finally extract failed */
    public List<ImapMessage> search(final String queryString, final Set<Flag> requiredFlags, final Set<Flag> forbiddenFlags) throws MessagingException {
        if (!this.store.getStoreConfig().allowRemoteSearch()) {
            throw new MessagingException("Your settings do not allow remote searching of this account");
        }
        ImapSearcher searcher = new ImapSearcher() {
            public List<ImapResponse> search() throws IOException, MessagingException {
                String imapQuery;
                String imapQuery2 = "UID SEARCH ";
                if (requiredFlags != null) {
                    for (Flag flag : requiredFlags) {
                        switch (AnonymousClass6.$SwitchMap$com$fsck$k9$mail$Flag[flag.ordinal()]) {
                            case 1:
                                imapQuery2 = imapQuery2 + "DELETED ";
                                break;
                            case 2:
                                imapQuery2 = imapQuery2 + "SEEN ";
                                break;
                            case 3:
                                imapQuery2 = imapQuery2 + "ANSWERED ";
                                break;
                            case 4:
                                imapQuery2 = imapQuery2 + "FLAGGED ";
                                break;
                            case 5:
                                imapQuery2 = imapQuery2 + "DRAFT ";
                                break;
                            case 6:
                                imapQuery2 = imapQuery2 + "RECENT ";
                                break;
                        }
                    }
                }
                if (forbiddenFlags != null) {
                    for (Flag flag2 : forbiddenFlags) {
                        switch (AnonymousClass6.$SwitchMap$com$fsck$k9$mail$Flag[flag2.ordinal()]) {
                            case 1:
                                imapQuery2 = imapQuery2 + "UNDELETED ";
                                break;
                            case 2:
                                imapQuery2 = imapQuery2 + "UNSEEN ";
                                break;
                            case 3:
                                imapQuery2 = imapQuery2 + "UNANSWERED ";
                                break;
                            case 4:
                                imapQuery2 = imapQuery2 + "UNFLAGGED ";
                                break;
                            case 5:
                                imapQuery2 = imapQuery2 + "UNDRAFT ";
                                break;
                            case 6:
                                imapQuery2 = imapQuery2 + "UNRECENT ";
                                break;
                        }
                    }
                }
                String encodedQuery = ImapUtility.encodeString(queryString);
                if (ImapFolder.this.store.getStoreConfig().isRemoteSearchFullText()) {
                    imapQuery = imapQuery2 + "TEXT " + encodedQuery;
                } else {
                    imapQuery = imapQuery2 + "OR SUBJECT " + encodedQuery + " FROM " + encodedQuery;
                }
                return ImapFolder.this.executeSimpleCommand(imapQuery);
            }
        };
        try {
            open(1);
            checkOpen();
            this.inSearch = true;
            List<ImapMessage> search = search(searcher, null);
            this.inSearch = false;
            return search;
        } catch (Throwable th) {
            this.inSearch = false;
            throw th;
        }
    }

    /* renamed from: com.fsck.k9.mail.store.imap.ImapFolder$6  reason: invalid class name */
    static /* synthetic */ class AnonymousClass6 {
        static final /* synthetic */ int[] $SwitchMap$com$fsck$k9$mail$Flag = new int[Flag.values().length];

        static {
            try {
                $SwitchMap$com$fsck$k9$mail$Flag[Flag.DELETED.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$fsck$k9$mail$Flag[Flag.SEEN.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$fsck$k9$mail$Flag[Flag.ANSWERED.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$fsck$k9$mail$Flag[Flag.FLAGGED.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$fsck$k9$mail$Flag[Flag.DRAFT.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$fsck$k9$mail$Flag[Flag.RECENT.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
        }
    }

    /* access modifiers changed from: private */
    public static String combine(Object[] parts, char separator) {
        if (parts == null) {
            return null;
        }
        return TextUtils.join(String.valueOf(separator), parts);
    }
}
