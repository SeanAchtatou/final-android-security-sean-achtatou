package com.fsck.k9.mail.store.imap;

import com.fsck.k9.mail.Part;
import com.fsck.k9.mail.filter.FixedLengthInputStream;
import com.fsck.k9.mail.internet.MimeUtility;
import java.io.IOException;

class FetchPartCallback implements ImapResponseCallback {
    private Part mPart;

    FetchPartCallback(Part part) {
        this.mPart = part;
    }

    public Object foundLiteral(ImapResponse response, FixedLengthInputStream literal) throws IOException {
        if (response.getTag() != null || !ImapResponseParser.equalsIgnoreCase(response.get(1), "FETCH")) {
            return null;
        }
        return MimeUtility.createBody(literal, this.mPart.getHeader("Content-Transfer-Encoding")[0], this.mPart.getHeader("Content-Type")[0]);
    }
}
