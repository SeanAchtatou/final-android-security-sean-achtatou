package com.fsck.k9.mail;

import java.util.List;

public abstract class Store {
    public abstract void checkSettings() throws MessagingException;

    public abstract Folder<? extends Message> getFolder(String str);

    public abstract List<? extends Folder> getPersonalNamespaces(boolean z) throws MessagingException;

    public boolean isCopyCapable() {
        return false;
    }

    public boolean isMoveCapable() {
        return false;
    }

    public boolean isPushCapable() {
        return false;
    }

    public boolean isSendCapable() {
        return false;
    }

    public boolean isExpungeCapable() {
        return false;
    }

    public boolean isSeenFlagSupported() {
        return true;
    }

    public void sendMessages(List<? extends Message> list) throws MessagingException {
    }

    public Pusher getPusher(PushReceiver receiver) {
        return null;
    }
}
