package com.fsck.k9.mail.filter;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class SignSafeOutputStream extends FilterOutputStream {
    private static final int DEFAULT_BUFFER_SIZE = 1024;
    private static final byte[] ESCAPED_SPACE = {61, 50, 48};
    private boolean closed = false;
    private final byte[] outBuffer = new byte[1024];
    private int outputIndex;
    private State state = State.cr_FROM;

    enum State {
        INIT {
            public State nextState(int b) {
                switch (b) {
                    case 13:
                        return lf_FROM;
                    default:
                        return INIT;
                }
            }
        },
        lf_FROM {
            public State nextState(int b) {
                switch (b) {
                    case 10:
                        return cr_FROM;
                    case 11:
                    case 12:
                    default:
                        return INIT;
                    case 13:
                        return lf_FROM;
                }
            }
        },
        cr_FROM {
            public State nextState(int b) {
                switch (b) {
                    case 13:
                        return lf_FROM;
                    case 70:
                        return F_FROM;
                    default:
                        return INIT;
                }
            }
        },
        F_FROM {
            public State nextState(int b) {
                switch (b) {
                    case 13:
                        return lf_FROM;
                    case 114:
                        return R_FROM;
                    default:
                        return INIT;
                }
            }
        },
        R_FROM {
            public State nextState(int b) {
                switch (b) {
                    case 13:
                        return lf_FROM;
                    case 111:
                        return O_FROM;
                    default:
                        return INIT;
                }
            }
        },
        O_FROM {
            public State nextState(int b) {
                switch (b) {
                    case 13:
                        return lf_FROM;
                    case 109:
                        return M_FROM;
                    default:
                        return INIT;
                }
            }
        },
        M_FROM {
            public State nextState(int b) {
                switch (b) {
                    case 13:
                        return lf_FROM;
                    case 32:
                        return SPACE_FROM;
                    default:
                        return INIT;
                }
            }
        },
        SPACE_FROM {
            public State nextState(int b) {
                switch (b) {
                    case 13:
                        return lf_FROM;
                    default:
                        return INIT;
                }
            }
        };

        public abstract State nextState(int i);
    }

    public SignSafeOutputStream(OutputStream out) {
        super(out);
    }

    public void encode(byte next) throws IOException {
        State nextState = this.state.nextState(next);
        if (nextState == State.SPACE_FROM) {
            this.state = State.INIT;
            writeToBuffer(ESCAPED_SPACE[0]);
            writeToBuffer(ESCAPED_SPACE[1]);
            writeToBuffer(ESCAPED_SPACE[2]);
            return;
        }
        this.state = nextState;
        writeToBuffer(next);
    }

    private void writeToBuffer(byte next) throws IOException {
        byte[] bArr = this.outBuffer;
        int i = this.outputIndex;
        this.outputIndex = i + 1;
        bArr[i] = next;
        if (this.outputIndex >= this.outBuffer.length) {
            flushOutput();
        }
    }

    /* access modifiers changed from: package-private */
    public void flushOutput() throws IOException {
        if (this.outputIndex < this.outBuffer.length) {
            this.out.write(this.outBuffer, 0, this.outputIndex);
        } else {
            this.out.write(this.outBuffer);
        }
        this.outputIndex = 0;
    }

    public void write(int b) throws IOException {
        if (this.closed) {
            throw new IOException("Stream has been closed");
        }
        encode((byte) b);
    }

    public void write(byte[] b, int off, int len) throws IOException {
        if (this.closed) {
            throw new IOException("Stream has been closed");
        }
        for (int inputIndex = off; inputIndex < len + off; inputIndex++) {
            encode(b[inputIndex]);
        }
    }

    public void flush() throws IOException {
        flushOutput();
        this.out.flush();
    }

    public void close() throws IOException {
        if (!this.closed) {
            try {
                flush();
            } finally {
                this.closed = true;
            }
        }
    }
}
