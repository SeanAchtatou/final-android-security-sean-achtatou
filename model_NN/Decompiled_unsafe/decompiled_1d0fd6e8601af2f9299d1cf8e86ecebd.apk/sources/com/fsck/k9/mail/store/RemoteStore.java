package com.fsck.k9.mail.store;

import android.content.Context;
import android.net.ConnectivityManager;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.ServerSettings;
import com.fsck.k9.mail.Store;
import com.fsck.k9.mail.oauth.OAuth2TokenProvider;
import com.fsck.k9.mail.ssl.DefaultTrustedSocketFactory;
import com.fsck.k9.mail.ssl.TrustedSocketFactory;
import com.fsck.k9.mail.store.imap.ImapStore;
import com.fsck.k9.mail.store.pop3.Pop3Store;
import com.fsck.k9.mail.store.webdav.WebDavHttpClient;
import com.fsck.k9.mail.store.webdav.WebDavStore;
import java.util.HashMap;
import java.util.Map;

public abstract class RemoteStore extends Store {
    public static final int SOCKET_CONNECT_TIMEOUT = 30000;
    public static final int SOCKET_READ_TIMEOUT = 60000;
    private static Map<String, Store> sStores = new HashMap();
    /* access modifiers changed from: protected */
    public StoreConfig mStoreConfig;
    /* access modifiers changed from: protected */
    public TrustedSocketFactory mTrustedSocketFactory;

    public RemoteStore(StoreConfig storeConfig, TrustedSocketFactory trustedSocketFactory) {
        this.mStoreConfig = storeConfig;
        this.mTrustedSocketFactory = trustedSocketFactory;
    }

    public static synchronized Store getInstance(Context context, StoreConfig storeConfig, OAuth2TokenProvider oAuth2TokenProvider) throws MessagingException {
        Store store;
        synchronized (RemoteStore.class) {
            String uri = storeConfig.getStoreUri();
            if (uri.startsWith("local")) {
                throw new RuntimeException("Asked to get non-local Store object but given LocalStore URI");
            }
            store = sStores.get(uri);
            if (store == null) {
                if (uri.startsWith("imap")) {
                    store = new ImapStore(storeConfig, new DefaultTrustedSocketFactory(context), (ConnectivityManager) context.getSystemService("connectivity"), oAuth2TokenProvider);
                } else if (uri.startsWith("pop3")) {
                    store = new Pop3Store(storeConfig, new DefaultTrustedSocketFactory(context));
                } else if (uri.startsWith("webdav")) {
                    store = new WebDavStore(storeConfig, new WebDavHttpClient.WebDavHttpClientFactory());
                }
                if (store != null) {
                    sStores.put(uri, store);
                }
            }
            if (store == null) {
                throw new MessagingException("Unable to locate an applicable Store for " + uri);
            }
        }
        return store;
    }

    public static void removeInstance(StoreConfig storeConfig) {
        String uri = storeConfig.getStoreUri();
        if (uri.startsWith("local")) {
            throw new RuntimeException("Asked to get non-local Store object but given LocalStore URI");
        }
        sStores.remove(uri);
    }

    public static ServerSettings decodeStoreUri(String uri) {
        if (uri.startsWith("imap")) {
            return ImapStore.decodeUri(uri);
        }
        if (uri.startsWith("pop3")) {
            return Pop3Store.decodeUri(uri);
        }
        if (uri.startsWith("webdav")) {
            return WebDavStore.decodeUri(uri);
        }
        throw new IllegalArgumentException("Not a valid store URI");
    }

    public static String createStoreUri(ServerSettings server) {
        if (ServerSettings.Type.IMAP == server.type) {
            return ImapStore.createUri(server);
        }
        if (ServerSettings.Type.POP3 == server.type) {
            return Pop3Store.createUri(server);
        }
        if (ServerSettings.Type.WebDAV == server.type) {
            return WebDavStore.createUri(server);
        }
        throw new IllegalArgumentException("Not a valid store URI");
    }
}
