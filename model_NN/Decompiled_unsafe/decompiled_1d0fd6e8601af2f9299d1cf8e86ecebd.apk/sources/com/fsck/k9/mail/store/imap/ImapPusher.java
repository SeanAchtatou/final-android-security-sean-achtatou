package com.fsck.k9.mail.store.imap;

import android.util.Log;
import com.fsck.k9.mail.K9MailLib;
import com.fsck.k9.mail.PushReceiver;
import com.fsck.k9.mail.Pusher;
import java.util.ArrayList;
import java.util.List;

class ImapPusher implements Pusher {
    private final List<ImapFolderPusher> folderPushers = new ArrayList();
    private long lastRefresh = -1;
    private final PushReceiver pushReceiver;
    private final ImapStore store;

    public ImapPusher(ImapStore store2, PushReceiver pushReceiver2) {
        this.store = store2;
        this.pushReceiver = pushReceiver2;
    }

    public void start(List<String> folderNames) {
        synchronized (this.folderPushers) {
            stop();
            setLastRefresh(currentTimeMillis());
            for (String folderName : folderNames) {
                ImapFolderPusher pusher = createImapFolderPusher(folderName);
                this.folderPushers.add(pusher);
                pusher.start();
            }
        }
    }

    public void refresh() {
        synchronized (this.folderPushers) {
            for (ImapFolderPusher folderPusher : this.folderPushers) {
                try {
                    folderPusher.refresh();
                } catch (Exception e) {
                    Log.e("k9", "Got exception while refreshing for " + folderPusher.getName(), e);
                }
            }
        }
    }

    public void stop() {
        if (K9MailLib.isDebug()) {
            Log.i("k9", "Requested stop of IMAP pusher");
        }
        synchronized (this.folderPushers) {
            for (ImapFolderPusher folderPusher : this.folderPushers) {
                try {
                    if (K9MailLib.isDebug()) {
                        Log.i("k9", "Requesting stop of IMAP folderPusher " + folderPusher.getName());
                    }
                    folderPusher.stop();
                } catch (Exception e) {
                    Log.e("k9", "Got exception while stopping " + folderPusher.getName(), e);
                }
            }
            this.folderPushers.clear();
        }
    }

    public int getRefreshInterval() {
        return this.store.getStoreConfig().getIdleRefreshMinutes() * 60 * 1000;
    }

    public long getLastRefresh() {
        return this.lastRefresh;
    }

    public void setLastRefresh(long lastRefresh2) {
        this.lastRefresh = lastRefresh2;
    }

    /* access modifiers changed from: package-private */
    public ImapFolderPusher createImapFolderPusher(String folderName) {
        return new ImapFolderPusher(this.store, folderName, this.pushReceiver);
    }

    /* access modifiers changed from: package-private */
    public long currentTimeMillis() {
        return System.currentTimeMillis();
    }
}
