package com.fsck.k9.mail.internet;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.BitSet;
import org.apache.james.mime4j.codec.EncoderUtil;
import org.apache.james.mime4j.util.CharsetUtil;

class EncoderUtil {
    private static final int ENCODED_WORD_MAX_LENGTH = 75;
    private static final String ENC_WORD_PREFIX = "=?";
    private static final String ENC_WORD_SUFFIX = "?=";
    private static final BitSet Q_RESTRICTED_CHARS = initChars("=_?\"#$%&'(),.:;<>@[\\]^`{|}~");

    public enum Encoding {
        B,
        Q
    }

    private static BitSet initChars(String specials) {
        BitSet bs = new BitSet(128);
        for (char ch = '!'; ch < 127; ch = (char) (ch + 1)) {
            if (specials.indexOf(ch) == -1) {
                bs.set(ch);
            }
        }
        return bs;
    }

    private EncoderUtil() {
    }

    public static String encodeEncodedWord(String text, Charset charset) {
        if (text == null) {
            throw new IllegalArgumentException();
        }
        if (charset == null) {
            charset = determineCharset(text);
        }
        String mimeCharset = CharsetSupport.getExternalCharset(charset.name());
        byte[] bytes = encode(text, charset);
        if (determineEncoding(bytes) == Encoding.B) {
            return encodeB(ENC_WORD_PREFIX + mimeCharset + "?B?", text, charset, bytes);
        }
        return encodeQ(ENC_WORD_PREFIX + mimeCharset + "?Q?", text, charset, bytes);
    }

    private static String encodeB(String prefix, String text, Charset charset, byte[] bytes) {
        if (prefix.length() + bEncodedLength(bytes) + ENC_WORD_SUFFIX.length() <= 75) {
            return prefix + org.apache.james.mime4j.codec.EncoderUtil.encodeB(bytes) + ENC_WORD_SUFFIX;
        }
        String part1 = text.substring(0, text.length() / 2);
        String word1 = encodeB(prefix, part1, charset, encode(part1, charset));
        String part2 = text.substring(text.length() / 2);
        return word1 + " " + encodeB(prefix, part2, charset, encode(part2, charset));
    }

    private static int bEncodedLength(byte[] bytes) {
        return ((bytes.length + 2) / 3) * 4;
    }

    private static String encodeQ(String prefix, String text, Charset charset, byte[] bytes) {
        if (prefix.length() + qEncodedLength(bytes) + ENC_WORD_SUFFIX.length() <= 75) {
            return prefix + org.apache.james.mime4j.codec.EncoderUtil.encodeQ(bytes, EncoderUtil.Usage.WORD_ENTITY) + ENC_WORD_SUFFIX;
        }
        String part1 = text.substring(0, text.length() / 2);
        String word1 = encodeQ(prefix, part1, charset, encode(part1, charset));
        String part2 = text.substring(text.length() / 2);
        return word1 + " " + encodeQ(prefix, part2, charset, encode(part2, charset));
    }

    private static int qEncodedLength(byte[] bytes) {
        int count = 0;
        for (byte b : bytes) {
            int v = b & 255;
            if (v == 32) {
                count++;
            } else if (!Q_RESTRICTED_CHARS.get(v)) {
                count += 3;
            } else {
                count++;
            }
        }
        return count;
    }

    private static byte[] encode(String text, Charset charset) {
        ByteBuffer buffer = charset.encode(text);
        byte[] bytes = new byte[buffer.limit()];
        buffer.get(bytes);
        return bytes;
    }

    private static Charset determineCharset(String text) {
        boolean ascii = true;
        int len = text.length();
        for (int index = 0; index < len; index++) {
            char ch = text.charAt(index);
            if (ch > 255) {
                return CharsetUtil.UTF_8;
            }
            if (ch > 127) {
                ascii = false;
            }
        }
        return ascii ? CharsetUtil.US_ASCII : CharsetUtil.ISO_8859_1;
    }

    private static Encoding determineEncoding(byte[] bytes) {
        if (bytes.length == 0) {
            return Encoding.Q;
        }
        int qEncoded = 0;
        for (byte b : bytes) {
            int v = b & 255;
            if (v != 32 && !Q_RESTRICTED_CHARS.get(v)) {
                qEncoded++;
            }
        }
        return (qEncoded * 100) / bytes.length > 30 ? Encoding.B : Encoding.Q;
    }
}
