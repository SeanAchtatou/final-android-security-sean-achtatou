package com.fsck.k9.mail;

import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;
import android.text.util.Rfc822Token;
import android.text.util.Rfc822Tokenizer;
import android.util.Log;
import com.fsck.k9.Account;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.codec.EncoderUtil;
import org.apache.james.mime4j.dom.address.Mailbox;
import org.apache.james.mime4j.dom.address.MailboxList;
import org.apache.james.mime4j.field.address.AddressBuilder;

public class Address implements Serializable {
    private static final Pattern ATOM = Pattern.compile("^(?:[a-zA-Z0-9!#$%&'*+\\-/=?^_`{|}~]|\\s)+$");
    private static final Address[] EMPTY_ADDRESS_ARRAY = new Address[0];
    private String mAddress;
    private String mPersonal;

    public Address(Address address) {
        this.mAddress = address.mAddress;
        this.mPersonal = address.mPersonal;
    }

    public Address(String address, String personal) {
        this(address, personal, true);
    }

    public Address(String address) {
        this(address, null, true);
    }

    private Address(String address, String personal, boolean parse) {
        if (parse) {
            Rfc822Token[] tokens = Rfc822Tokenizer.tokenize(address);
            if (tokens.length > 0) {
                Rfc822Token token = tokens[0];
                this.mAddress = token.getAddress();
                String name = token.getName();
                if (!TextUtils.isEmpty(name)) {
                    this.mPersonal = name;
                } else {
                    this.mPersonal = personal == null ? null : personal.trim();
                }
            }
        } else {
            this.mAddress = address;
            this.mPersonal = personal;
        }
    }

    public String getAddress() {
        return this.mAddress;
    }

    public String getHostname() {
        int hostIdx = this.mAddress.lastIndexOf("@");
        if (hostIdx == -1) {
            return null;
        }
        return this.mAddress.substring(hostIdx + 1);
    }

    public void setAddress(String address) {
        this.mAddress = address;
    }

    public String getPersonal() {
        return this.mPersonal;
    }

    public void setPersonal(String personal) {
        if ("".equals(personal)) {
            personal = null;
        }
        if (personal != null) {
            personal = personal.trim();
        }
        this.mPersonal = personal;
    }

    public static Address[] parseUnencoded(String addressList) {
        List<Address> addresses = new ArrayList<>();
        if (!TextUtils.isEmpty(addressList)) {
            for (Rfc822Token token : Rfc822Tokenizer.tokenize(addressList)) {
                if (!TextUtils.isEmpty(token.getAddress())) {
                    addresses.add(new Address(token.getAddress(), token.getName(), false));
                }
            }
        }
        return (Address[]) addresses.toArray(EMPTY_ADDRESS_ARRAY);
    }

    public static Address[] parse(String addressList) {
        if (TextUtils.isEmpty(addressList)) {
            return EMPTY_ADDRESS_ARRAY;
        }
        List<Address> addresses = new ArrayList<>();
        try {
            MailboxList parsedList = AddressBuilder.DEFAULT.parseAddressList(addressList).flatten();
            int count = parsedList.size();
            for (int i = 0; i < count; i++) {
                Mailbox mailbox = parsedList.get(i);
                if (mailbox instanceof Mailbox) {
                    Mailbox mailbox2 = mailbox;
                    addresses.add(new Address(mailbox2.getLocalPart() + "@" + mailbox2.getDomain(), mailbox2.getName(), false));
                } else {
                    Log.e("k9", "Unknown address type from Mime4J: " + mailbox.getClass().toString());
                }
            }
        } catch (MimeException pe) {
            Log.e("k9", "MimeException in Address.parse()", pe);
            addresses.add(new Address(null, addressList, false));
        }
        return (Address[]) addresses.toArray(EMPTY_ADDRESS_ARRAY);
    }

    public boolean equals(Object o) {
        if (!(o instanceof Address)) {
            return super.equals(o);
        }
        Address other = (Address) o;
        if (this.mPersonal == null || other.mPersonal == null || this.mPersonal.equals(other.mPersonal)) {
            return this.mAddress.equals(other.mAddress);
        }
        return false;
    }

    public int hashCode() {
        int hash = this.mAddress.hashCode();
        if (this.mPersonal != null) {
            return hash + (this.mPersonal.hashCode() * 3);
        }
        return hash;
    }

    public String toString() {
        if (!TextUtils.isEmpty(this.mPersonal)) {
            return quoteAtoms(this.mPersonal) + " <" + this.mAddress + Account.DEFAULT_QUOTE_PREFIX;
        }
        return this.mAddress;
    }

    public static String toString(Address[] addresses) {
        if (addresses == null) {
            return null;
        }
        return TextUtils.join(", ", addresses);
    }

    public String toEncodedString() {
        if (!TextUtils.isEmpty(this.mPersonal)) {
            return EncoderUtil.encodeAddressDisplayName(this.mPersonal) + " <" + this.mAddress + Account.DEFAULT_QUOTE_PREFIX;
        }
        return this.mAddress;
    }

    public static String toEncodedString(Address[] addresses) {
        if (addresses == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < addresses.length; i++) {
            sb.append(addresses[i].toEncodedString());
            if (i < addresses.length - 1) {
                sb.append(',');
            }
        }
        return sb.toString();
    }

    public static Address[] unpack(String addressList) {
        String address;
        if (addressList == null) {
            return new Address[0];
        }
        List<Address> addresses = new ArrayList<>();
        int length = addressList.length();
        int pairStartIndex = 0;
        while (pairStartIndex < length) {
            int pairEndIndex = addressList.indexOf(",||", pairStartIndex);
            if (pairEndIndex == -1) {
                pairEndIndex = length;
            }
            int addressEndIndex = addressList.indexOf(";||", pairStartIndex);
            String personal = null;
            if (addressEndIndex == -1 || addressEndIndex > pairEndIndex) {
                address = addressList.substring(pairStartIndex, pairEndIndex);
            } else {
                address = addressList.substring(pairStartIndex, addressEndIndex);
                personal = addressList.substring(addressEndIndex + 3, pairEndIndex);
            }
            addresses.add(new Address(address, personal, false));
            pairStartIndex = pairEndIndex + 3;
        }
        return (Address[]) addresses.toArray(new Address[addresses.size()]);
    }

    public static String pack(Address[] addresses) {
        if (addresses == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        int count = addresses.length;
        for (int i = 0; i < count; i++) {
            Address address = addresses[i];
            sb.append(address.getAddress());
            String personal = address.getPersonal();
            if (personal != null) {
                sb.append(";||");
                sb.append(personal.replaceAll("\"", "\\\""));
            }
            if (i < count - 1) {
                sb.append(",||");
            }
        }
        return sb.toString();
    }

    public static String quoteAtoms(String text) {
        return ATOM.matcher(text).matches() ? text : quoteString(text);
    }

    @VisibleForTesting
    static String quoteString(String s) {
        if (s == null) {
            return null;
        }
        if (!s.matches("^\".*\"$")) {
            return "\"" + s + "\"";
        }
        return s;
    }
}
