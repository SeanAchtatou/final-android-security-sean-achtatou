package com.fsck.k9.mail;

import android.content.Context;
import com.fsck.k9.mail.ServerSettings;
import com.fsck.k9.mail.oauth.OAuth2TokenProvider;
import com.fsck.k9.mail.ssl.DefaultTrustedSocketFactory;
import com.fsck.k9.mail.store.StoreConfig;
import com.fsck.k9.mail.transport.SmtpTransport;
import com.fsck.k9.mail.transport.WebDavTransport;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

public abstract class Transport {
    protected static final int SOCKET_CONNECT_TIMEOUT = 10000;
    protected static final int SOCKET_READ_TIMEOUT = 300000;

    public abstract void close();

    public abstract void open() throws MessagingException;

    public abstract void sendMessage(Message message) throws MessagingException;

    public static synchronized Transport getInstance(Context context, StoreConfig storeConfig, OAuth2TokenProvider oauth2TokenProvider) throws MessagingException {
        Transport webDavTransport;
        synchronized (Transport.class) {
            String uri = storeConfig.getTransportUri();
            if (uri.startsWith("smtp")) {
                webDavTransport = new SmtpTransport(storeConfig, new DefaultTrustedSocketFactory(context), oauth2TokenProvider);
            } else if (uri.startsWith("webdav")) {
                webDavTransport = new WebDavTransport(storeConfig);
            } else {
                throw new MessagingException("Unable to locate an applicable Transport for " + uri);
            }
        }
        return webDavTransport;
    }

    public static ServerSettings decodeTransportUri(String uri) {
        if (uri.startsWith("smtp")) {
            return SmtpTransport.decodeUri(uri);
        }
        if (uri.startsWith("webdav")) {
            return WebDavTransport.decodeUri(uri);
        }
        throw new IllegalArgumentException("Not a valid transport URI");
    }

    public static String createTransportUri(ServerSettings server) {
        if (ServerSettings.Type.SMTP == server.type) {
            return SmtpTransport.createUri(server);
        }
        if (ServerSettings.Type.WebDAV == server.type) {
            return WebDavTransport.createUri(server);
        }
        throw new IllegalArgumentException("Not a valid transport URI");
    }

    protected static String encodeUtf8(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("UTF-8 not found");
        }
    }

    protected static String decodeUtf8(String s) {
        try {
            return URLDecoder.decode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("UTF-8 not found");
        }
    }
}
