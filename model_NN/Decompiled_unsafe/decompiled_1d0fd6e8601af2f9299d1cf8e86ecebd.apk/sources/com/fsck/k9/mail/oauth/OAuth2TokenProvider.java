package com.fsck.k9.mail.oauth;

import android.app.Activity;
import com.fsck.k9.mail.AuthenticationFailedException;
import java.util.List;

public interface OAuth2TokenProvider {
    public static final int OAUTH2_TIMEOUT = 30000;

    public interface OAuth2TokenProviderAuthCallback {
        void failure(AuthorizationException authorizationException);

        void success();
    }

    void authorizeAPI(String str, Activity activity, OAuth2TokenProviderAuthCallback oAuth2TokenProviderAuthCallback);

    List<String> getAccounts();

    String getToken(String str, long j) throws AuthenticationFailedException;

    void invalidateToken(String str);
}
