package com.fsck.k9.mail.filter;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

public class PeekableInputStream extends FilterInputStream {
    private boolean peeked;
    private int peekedByte;

    public PeekableInputStream(InputStream in) {
        super(in);
    }

    public int read() throws IOException {
        if (!this.peeked) {
            return this.in.read();
        }
        this.peeked = false;
        return this.peekedByte;
    }

    public int peek() throws IOException {
        if (!this.peeked) {
            this.peekedByte = this.in.read();
            this.peeked = true;
        }
        return this.peekedByte;
    }

    public int read(byte[] buffer, int offset, int length) throws IOException {
        if (!this.peeked) {
            return this.in.read(buffer, offset, length);
        }
        buffer[offset] = (byte) this.peekedByte;
        this.peeked = false;
        int r = this.in.read(buffer, offset + 1, length - 1);
        if (r == -1) {
            return 1;
        }
        return r + 1;
    }

    public int read(byte[] buffer) throws IOException {
        return read(buffer, 0, buffer.length);
    }

    public String toString() {
        return String.format(Locale.US, "PeekableInputStream(in=%s, peeked=%b, peekedByte=%d)", this.in.toString(), Boolean.valueOf(this.peeked), Integer.valueOf(this.peekedByte));
    }
}
