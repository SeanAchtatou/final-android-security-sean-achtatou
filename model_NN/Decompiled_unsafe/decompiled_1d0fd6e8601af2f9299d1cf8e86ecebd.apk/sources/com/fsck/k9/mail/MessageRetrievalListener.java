package com.fsck.k9.mail;

import com.fsck.k9.mail.Message;

public interface MessageRetrievalListener<T extends Message> {
    void messageFinished(Message message, int i, int i2);

    void messageStarted(String str, int i, int i2);

    void messagesFinished(int i);
}
