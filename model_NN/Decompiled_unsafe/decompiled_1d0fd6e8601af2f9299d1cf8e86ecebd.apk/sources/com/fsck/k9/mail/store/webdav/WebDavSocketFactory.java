package com.fsck.k9.mail.store.webdav;

import com.fsck.k9.mail.ssl.DefaultTrustedSocketFactory;
import com.fsck.k9.mail.ssl.TrustManagerFactory;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.TrustManager;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.scheme.LayeredSocketFactory;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.params.HttpParams;

public class WebDavSocketFactory implements LayeredSocketFactory {
    private SSLSocketFactory mSchemeSocketFactory = SSLSocketFactory.getSocketFactory();
    private javax.net.ssl.SSLSocketFactory mSocketFactory;

    public WebDavSocketFactory(String host, int port) throws NoSuchAlgorithmException, KeyManagementException {
        SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(null, new TrustManager[]{TrustManagerFactory.get(host, port)}, null);
        this.mSocketFactory = sslContext.getSocketFactory();
        this.mSchemeSocketFactory.setHostnameVerifier(SSLSocketFactory.STRICT_HOSTNAME_VERIFIER);
    }

    public Socket connectSocket(Socket sock, String host, int port, InetAddress localAddress, int localPort, HttpParams params) throws IOException, ConnectTimeoutException {
        return this.mSchemeSocketFactory.connectSocket(sock, host, port, localAddress, localPort, params);
    }

    public Socket createSocket() throws IOException {
        return this.mSocketFactory.createSocket();
    }

    public boolean isSecure(Socket sock) throws IllegalArgumentException {
        return this.mSchemeSocketFactory.isSecure(sock);
    }

    public Socket createSocket(Socket socket, String host, int port, boolean autoClose) throws IOException {
        SSLSocket sslSocket = (SSLSocket) this.mSocketFactory.createSocket(socket, host, port, autoClose);
        DefaultTrustedSocketFactory.setSniHost(this.mSocketFactory, sslSocket, host);
        return sslSocket;
    }
}
