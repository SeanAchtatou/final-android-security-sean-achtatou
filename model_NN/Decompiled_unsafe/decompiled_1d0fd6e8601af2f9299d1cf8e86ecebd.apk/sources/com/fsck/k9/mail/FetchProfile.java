package com.fsck.k9.mail;

import java.util.ArrayList;

public class FetchProfile extends ArrayList<Item> {
    private static final long serialVersionUID = -5520076119120964166L;

    public enum Item {
        FLAGS,
        ENVELOPE,
        STRUCTURE,
        BODY_SANE,
        BODY
    }
}
