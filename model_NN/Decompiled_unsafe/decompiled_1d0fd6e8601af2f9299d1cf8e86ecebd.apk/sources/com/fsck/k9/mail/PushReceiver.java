package com.fsck.k9.mail;

import android.content.Context;
import com.fsck.k9.mail.power.TracingPowerManager;
import java.util.List;

public interface PushReceiver {
    void authenticationFailed(String str);

    Context getContext();

    String getPushState(String str);

    void messagesArrived(Folder folder, List<Message> list);

    void messagesFlagsChanged(Folder folder, List<Message> list);

    void messagesRemoved(Folder folder, List<Message> list);

    void pushError(String str, Exception exc);

    void setPushActive(String str, boolean z);

    void sleep(TracingPowerManager.TracingWakeLock tracingWakeLock, long j);

    void syncFolder(Folder folder);
}
