package com.fsck.k9.mail.store.imap;

import java.util.ArrayList;
import java.util.List;

class SearchResponse {
    private final List<Long> numbers;

    private SearchResponse(List<Long> numbers2) {
        this.numbers = numbers2;
    }

    public static SearchResponse parse(List<ImapResponse> responses) {
        List<Long> numbers2 = new ArrayList<>();
        for (ImapResponse response : responses) {
            parseSingleLine(response, numbers2);
        }
        return new SearchResponse(numbers2);
    }

    private static void parseSingleLine(ImapResponse response, List<Long> numbers2) {
        if (!response.isTagged() && response.size() >= 2 && ImapResponseParser.equalsIgnoreCase(response.get(0), Responses.SEARCH)) {
            int end = response.size();
            int i = 1;
            while (i < end) {
                try {
                    numbers2.add(Long.valueOf(response.getLong(i)));
                    i++;
                } catch (NumberFormatException e) {
                    return;
                }
            }
        }
    }

    public List<Long> getNumbers() {
        return this.numbers;
    }
}
