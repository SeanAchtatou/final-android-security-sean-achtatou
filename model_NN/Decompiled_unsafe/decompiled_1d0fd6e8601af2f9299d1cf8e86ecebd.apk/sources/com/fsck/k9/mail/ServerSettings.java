package com.fsck.k9.mail;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ServerSettings {
    public final AuthType authenticationType;
    public final String clientCertificateAlias;
    public final ConnectionSecurity connectionSecurity;
    private final Map<String, String> extra;
    public final String host;
    public final String password;
    public final int port;
    public final Type type;
    public final String username;

    public enum Type {
        IMAP(143, 993),
        SMTP(587, 465),
        WebDAV(80, 443),
        POP3(110, 995);
        
        public final int defaultPort;
        public final int defaultTlsPort;

        private Type(int defaultPort2, int defaultTlsPort2) {
            this.defaultPort = defaultPort2;
            this.defaultTlsPort = defaultTlsPort2;
        }
    }

    public ServerSettings(Type type2, String host2, int port2, ConnectionSecurity connectionSecurity2, AuthType authenticationType2, String username2, String password2, String clientCertificateAlias2) {
        this.type = type2;
        this.host = host2;
        this.port = port2;
        this.connectionSecurity = connectionSecurity2;
        this.authenticationType = authenticationType2;
        this.username = username2;
        this.password = password2;
        this.clientCertificateAlias = clientCertificateAlias2;
        this.extra = null;
    }

    public ServerSettings(Type type2, String host2, int port2, ConnectionSecurity connectionSecurity2, AuthType authenticationType2, String username2, String password2, String clientCertificateAlias2, Map<String, String> extra2) {
        this.type = type2;
        this.host = host2;
        this.port = port2;
        this.connectionSecurity = connectionSecurity2;
        this.authenticationType = authenticationType2;
        this.username = username2;
        this.password = password2;
        this.clientCertificateAlias = clientCertificateAlias2;
        this.extra = extra2 != null ? Collections.unmodifiableMap(new HashMap(extra2)) : null;
    }

    public ServerSettings(Type type2) {
        this.type = type2;
        this.host = null;
        this.port = -1;
        this.connectionSecurity = ConnectionSecurity.NONE;
        this.authenticationType = null;
        this.username = null;
        this.password = null;
        this.clientCertificateAlias = null;
        this.extra = null;
    }

    public Map<String, String> getExtra() {
        return this.extra;
    }

    /* access modifiers changed from: protected */
    public void putIfNotNull(Map<String, String> map, String key, String value) {
        if (value != null) {
            map.put(key, value);
        }
    }

    public ServerSettings newPassword(String newPassword) {
        return new ServerSettings(this.type, this.host, this.port, this.connectionSecurity, this.authenticationType, this.username, newPassword, this.clientCertificateAlias);
    }

    public ServerSettings newClientCertificateAlias(String newAlias) {
        return new ServerSettings(this.type, this.host, this.port, this.connectionSecurity, AuthType.EXTERNAL, this.username, this.password, newAlias);
    }
}
