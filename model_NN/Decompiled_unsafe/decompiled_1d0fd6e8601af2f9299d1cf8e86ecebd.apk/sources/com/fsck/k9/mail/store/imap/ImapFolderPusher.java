package com.fsck.k9.mail.store.imap;

import android.util.Log;
import com.fsck.k9.mail.AuthenticationFailedException;
import com.fsck.k9.mail.Flag;
import com.fsck.k9.mail.K9MailLib;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.PushReceiver;
import com.fsck.k9.mail.power.TracingPowerManager;
import java.io.IOException;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

class ImapFolderPusher extends ImapFolder {
    private static final int IDLE_FAILURE_COUNT_LIMIT = 10;
    private static final int IDLE_READ_TIMEOUT_INCREMENT = 300000;
    private static final int MAX_DELAY_TIME = 300000;
    private static final int NORMAL_DELAY_TIME = 5000;
    /* access modifiers changed from: private */
    public final IdleStopper idleStopper = new IdleStopper();
    /* access modifiers changed from: private */
    public volatile boolean idling = false;
    private Thread listeningThread;
    /* access modifiers changed from: private */
    public final PushReceiver pushReceiver;
    /* access modifiers changed from: private */
    public volatile boolean stop = false;
    /* access modifiers changed from: private */
    public final List<ImapResponse> storedUntaggedResponses = new ArrayList();
    private final Object threadLock = new Object();
    /* access modifiers changed from: private */
    public final TracingPowerManager.TracingWakeLock wakeLock;

    public ImapFolderPusher(ImapStore store, String name, PushReceiver pushReceiver2) {
        super(store, name);
        this.pushReceiver = pushReceiver2;
        this.wakeLock = TracingPowerManager.getPowerManager(pushReceiver2.getContext()).newWakeLock(1, "ImapFolderPusher " + store.getStoreConfig().toString() + ":" + getName());
        this.wakeLock.setReferenceCounted(false);
    }

    public void start() {
        synchronized (this.threadLock) {
            if (this.listeningThread != null) {
                throw new IllegalStateException("start() called twice");
            }
            this.listeningThread = new Thread(new PushRunnable());
            this.listeningThread.start();
        }
    }

    public void refresh() throws IOException, MessagingException {
        if (this.idling) {
            this.wakeLock.acquire(60000);
            this.idleStopper.stopIdle();
        }
    }

    public void stop() {
        synchronized (this.threadLock) {
            if (this.listeningThread == null) {
                throw new IllegalStateException("stop() called twice");
            }
            this.stop = true;
            this.listeningThread.interrupt();
            this.listeningThread = null;
        }
        ImapConnection conn = this.connection;
        if (conn != null) {
            if (K9MailLib.isDebug()) {
                Log.v("k9", "Closing connection to stop pushing for " + getLogId());
            }
            conn.close();
            return;
        }
        Log.w("k9", "Attempt to interrupt null connection to stop pushing on folderPusher for " + getLogId());
    }

    /* access modifiers changed from: protected */
    public void handleUntaggedResponse(ImapResponse response) {
        if (response.getTag() == null && response.size() > 1) {
            Object responseType = response.get(1);
            if (ImapResponseParser.equalsIgnoreCase(responseType, "FETCH") || ImapResponseParser.equalsIgnoreCase(responseType, Responses.EXPUNGE) || ImapResponseParser.equalsIgnoreCase(responseType, Responses.EXISTS)) {
                if (K9MailLib.isDebug()) {
                    Log.d("k9", "Storing response " + response + " for later processing");
                }
                synchronized (this.storedUntaggedResponses) {
                    this.storedUntaggedResponses.add(response);
                }
            }
            handlePossibleUidNext(response);
        }
    }

    /* access modifiers changed from: private */
    public void superHandleUntaggedResponse(ImapResponse response) {
        super.handleUntaggedResponse(response);
    }

    private class PushRunnable implements Runnable, UntaggedHandler {
        private int delayTime;
        private int idleFailureCount;
        private boolean needsPoll;

        private PushRunnable() {
            this.delayTime = ImapFolderPusher.NORMAL_DELAY_TIME;
            this.idleFailureCount = 0;
            this.needsPoll = false;
        }

        public void run() {
            ImapFolderPusher.this.wakeLock.acquire(60000);
            if (K9MailLib.isDebug()) {
                Log.i("k9", "Pusher starting for " + ImapFolderPusher.this.getLogId());
            }
            long lastUidNext = -1;
            while (!ImapFolderPusher.this.stop) {
                try {
                    long oldUidNext = getOldUidNext();
                    if (oldUidNext < lastUidNext) {
                        oldUidNext = lastUidNext;
                    }
                    boolean openedNewConnection = openConnectionIfNecessary();
                    if (!ImapFolderPusher.this.stop) {
                        if (ImapFolderPusher.this.store.getStoreConfig().isPushPollOnConnect() && (openedNewConnection || this.needsPoll)) {
                            this.needsPoll = false;
                            syncFolderOnConnect();
                        }
                        if (ImapFolderPusher.this.stop) {
                            break;
                        }
                        long newUidNext = getNewUidNext();
                        lastUidNext = newUidNext;
                        long startUid = getStartUid(oldUidNext, newUidNext);
                        if (newUidNext > startUid) {
                            notifyMessagesArrived(startUid, newUidNext);
                        } else {
                            processStoredUntaggedResponses();
                            if (K9MailLib.isDebug()) {
                                Log.i("k9", "About to IDLE for " + ImapFolderPusher.this.getLogId());
                            }
                            prepareForIdle();
                            ImapConnection conn = ImapFolderPusher.this.connection;
                            setReadTimeoutForIdle(conn);
                            sendIdle(conn);
                            returnFromIdle();
                        }
                    } else {
                        break;
                    }
                } catch (AuthenticationFailedException e) {
                    reacquireWakeLockAndCleanUp();
                    if (K9MailLib.isDebug()) {
                        Log.e("k9", "Authentication failed. Stopping ImapFolderPusher.", e);
                    }
                    ImapFolderPusher.this.pushReceiver.authenticationFailed(ImapFolderPusher.this.getName());
                    boolean unused = ImapFolderPusher.this.stop = true;
                } catch (Exception e2) {
                    reacquireWakeLockAndCleanUp();
                    if (ImapFolderPusher.this.stop) {
                        Log.i("k9", "Got exception while idling, but stop is set for " + ImapFolderPusher.this.getLogId());
                    } else {
                        ImapFolderPusher.this.pushReceiver.pushError("Push error for " + ImapFolderPusher.this.getName(), e2);
                        Log.e("k9", "Got exception while idling for " + ImapFolderPusher.this.getLogId(), e2);
                        ImapFolderPusher.this.pushReceiver.sleep(ImapFolderPusher.this.wakeLock, (long) this.delayTime);
                        this.delayTime = this.delayTime * 2;
                        if (this.delayTime > 300000) {
                            this.delayTime = 300000;
                        }
                        this.idleFailureCount = this.idleFailureCount + 1;
                        if (this.idleFailureCount > 10) {
                            Log.e("k9", "Disabling pusher for " + ImapFolderPusher.this.getLogId() + " after " + this.idleFailureCount + " consecutive errors");
                            ImapFolderPusher.this.pushReceiver.pushError("Push disabled for " + ImapFolderPusher.this.getName() + " after " + this.idleFailureCount + " consecutive errors", e2);
                            boolean unused2 = ImapFolderPusher.this.stop = true;
                        }
                    }
                }
            }
            ImapFolderPusher.this.pushReceiver.setPushActive(ImapFolderPusher.this.getName(), false);
            try {
                if (K9MailLib.isDebug()) {
                    Log.i("k9", "Pusher for " + ImapFolderPusher.this.getLogId() + " is exiting");
                }
                ImapFolderPusher.this.close();
                ImapFolderPusher.this.wakeLock.release();
            } catch (Exception me2) {
                Log.e("k9", "Got exception while closing for " + ImapFolderPusher.this.getLogId(), me2);
                ImapFolderPusher.this.wakeLock.release();
            } catch (Throwable th) {
                ImapFolderPusher.this.wakeLock.release();
                throw th;
            }
        }

        private void reacquireWakeLockAndCleanUp() {
            ImapFolderPusher.this.wakeLock.acquire(60000);
            clearStoredUntaggedResponses();
            boolean unused = ImapFolderPusher.this.idling = false;
            ImapFolderPusher.this.pushReceiver.setPushActive(ImapFolderPusher.this.getName(), false);
            try {
                ImapFolderPusher.this.connection.close();
            } catch (Exception me2) {
                Log.e("k9", "Got exception while closing for exception for " + ImapFolderPusher.this.getLogId(), me2);
            }
            ImapFolderPusher.this.connection = null;
        }

        private long getNewUidNext() throws MessagingException {
            long newUidNext = ImapFolderPusher.this.uidNext;
            if (newUidNext != -1) {
                return newUidNext;
            }
            if (K9MailLib.isDebug()) {
                Log.d("k9", "uidNext is -1, using search to find highest UID");
            }
            long highestUid = ImapFolderPusher.this.getHighestUid();
            if (highestUid == -1) {
                return -1;
            }
            long newUidNext2 = highestUid + 1;
            if (K9MailLib.isDebug()) {
                Log.d("k9", "highest UID = " + highestUid + ", set newUidNext to " + newUidNext2);
            }
            return newUidNext2;
        }

        private long getStartUid(long oldUidNext, long newUidNext) {
            long startUid = oldUidNext;
            int displayCount = ImapFolderPusher.this.store.getStoreConfig().getDisplayCount();
            if (startUid < newUidNext - ((long) displayCount)) {
                startUid = newUidNext - ((long) displayCount);
            }
            if (startUid < 1) {
                return 1;
            }
            return startUid;
        }

        private void prepareForIdle() {
            ImapFolderPusher.this.pushReceiver.setPushActive(ImapFolderPusher.this.getName(), true);
            boolean unused = ImapFolderPusher.this.idling = true;
        }

        private void sendIdle(ImapConnection conn) throws MessagingException, IOException {
            try {
                List<ImapResponse> responses = conn.readStatusResponse(conn.sendCommand("IDLE", false), "IDLE", this);
                ImapFolderPusher.this.idleStopper.stopAcceptingDoneContinuation();
                ImapFolderPusher.this.handleUntaggedResponses(responses);
            } catch (IOException e) {
                conn.close();
                throw e;
            } catch (Throwable th) {
                ImapFolderPusher.this.idleStopper.stopAcceptingDoneContinuation();
                throw th;
            }
        }

        private void returnFromIdle() {
            boolean unused = ImapFolderPusher.this.idling = false;
            this.delayTime = ImapFolderPusher.NORMAL_DELAY_TIME;
            this.idleFailureCount = 0;
        }

        private boolean openConnectionIfNecessary() throws MessagingException {
            ImapConnection oldConnection = ImapFolderPusher.this.connection;
            ImapFolderPusher.this.internalOpen(1);
            ImapConnection conn = ImapFolderPusher.this.connection;
            checkConnectionNotNull(conn);
            checkConnectionIdleCapable(conn);
            if (conn != oldConnection) {
                return true;
            }
            return false;
        }

        private void checkConnectionNotNull(ImapConnection conn) throws MessagingException {
            if (conn == null) {
                ImapFolderPusher.this.pushReceiver.pushError("Could not establish connection for IDLE", null);
                throw new MessagingException("Could not establish connection for IDLE");
            }
        }

        private void checkConnectionIdleCapable(ImapConnection conn) throws MessagingException {
            if (!conn.isIdleCapable()) {
                boolean unused = ImapFolderPusher.this.stop = true;
                String message = "IMAP server is not IDLE capable: " + conn.toString();
                ImapFolderPusher.this.pushReceiver.pushError(message, null);
                throw new MessagingException(message);
            }
        }

        private void setReadTimeoutForIdle(ImapConnection conn) throws SocketException {
            conn.setReadTimeout(300000 + (ImapFolderPusher.this.store.getStoreConfig().getIdleRefreshMinutes() * 60 * 1000));
        }

        public void handleAsyncUntaggedResponse(ImapResponse response) {
            if (K9MailLib.isDebug()) {
                Log.v("k9", "Got async response: " + response);
            }
            if (ImapFolderPusher.this.stop) {
                if (K9MailLib.isDebug()) {
                    Log.d("k9", "Got async untagged response: " + response + ", but stop is set for " + ImapFolderPusher.this.getLogId());
                }
                ImapFolderPusher.this.idleStopper.stopIdle();
            } else if (response.getTag() != null) {
            } else {
                if (response.size() > 1) {
                    Object responseType = response.get(1);
                    if (ImapResponseParser.equalsIgnoreCase(responseType, Responses.EXISTS) || ImapResponseParser.equalsIgnoreCase(responseType, Responses.EXPUNGE) || ImapResponseParser.equalsIgnoreCase(responseType, "FETCH")) {
                        ImapFolderPusher.this.wakeLock.acquire(60000);
                        if (K9MailLib.isDebug()) {
                            Log.d("k9", "Got useful async untagged response: " + response + " for " + ImapFolderPusher.this.getLogId());
                        }
                        ImapFolderPusher.this.idleStopper.stopIdle();
                    }
                } else if (response.isContinuationRequested()) {
                    if (K9MailLib.isDebug()) {
                        Log.d("k9", "Idling " + ImapFolderPusher.this.getLogId());
                    }
                    ImapFolderPusher.this.idleStopper.startAcceptingDoneContinuation(ImapFolderPusher.this.connection);
                    ImapFolderPusher.this.wakeLock.release();
                }
            }
        }

        private void clearStoredUntaggedResponses() {
            synchronized (ImapFolderPusher.this.storedUntaggedResponses) {
                ImapFolderPusher.this.storedUntaggedResponses.clear();
            }
        }

        private void processStoredUntaggedResponses() throws MessagingException {
            while (true) {
                List<ImapResponse> untaggedResponses = getAndClearStoredUntaggedResponses();
                if (!untaggedResponses.isEmpty()) {
                    if (K9MailLib.isDebug()) {
                        Log.i("k9", "Processing " + untaggedResponses.size() + " untagged responses from previous " + "commands for " + ImapFolderPusher.this.getLogId());
                    }
                    processUntaggedResponses(untaggedResponses);
                } else {
                    return;
                }
            }
        }

        private List<ImapResponse> getAndClearStoredUntaggedResponses() {
            List<ImapResponse> arrayList;
            synchronized (ImapFolderPusher.this.storedUntaggedResponses) {
                if (ImapFolderPusher.this.storedUntaggedResponses.isEmpty()) {
                    arrayList = Collections.emptyList();
                } else {
                    arrayList = new ArrayList<>(ImapFolderPusher.this.storedUntaggedResponses);
                    ImapFolderPusher.this.storedUntaggedResponses.clear();
                }
            }
            return arrayList;
        }

        private void processUntaggedResponses(List<ImapResponse> responses) throws MessagingException {
            boolean skipSync = false;
            int oldMessageCount = ImapFolderPusher.this.messageCount;
            if (oldMessageCount == -1) {
                skipSync = true;
            }
            List<Long> flagSyncMsgSeqs = new ArrayList<>();
            List<String> removeMsgUids = new LinkedList<>();
            for (ImapResponse response : responses) {
                oldMessageCount += processUntaggedResponse((long) oldMessageCount, response, flagSyncMsgSeqs, removeMsgUids);
            }
            if (!skipSync) {
                if (oldMessageCount < 0) {
                    oldMessageCount = 0;
                }
                if (ImapFolderPusher.this.messageCount > oldMessageCount) {
                    syncMessages(ImapFolderPusher.this.messageCount);
                }
            }
            if (K9MailLib.isDebug()) {
                Log.d("k9", "UIDs for messages needing flag sync are " + flagSyncMsgSeqs + "  for " + ImapFolderPusher.this.getLogId());
            }
            if (!flagSyncMsgSeqs.isEmpty()) {
                syncMessages(flagSyncMsgSeqs);
            }
            if (!removeMsgUids.isEmpty()) {
                removeMessages(removeMsgUids);
            }
        }

        private int processUntaggedResponse(long oldMessageCount, ImapResponse response, List<Long> flagSyncMsgSeqs, List<String> removeMsgUids) {
            ImapFolderPusher.this.superHandleUntaggedResponse(response);
            int messageCountDelta = 0;
            if (response.getTag() == null && response.size() > 1) {
                try {
                    Object responseType = response.get(1);
                    if (ImapResponseParser.equalsIgnoreCase(responseType, "FETCH")) {
                        Log.i("k9", "Got FETCH " + response);
                        long msgSeq = response.getLong(0);
                        if (K9MailLib.isDebug()) {
                            Log.d("k9", "Got untagged FETCH for msgseq " + msgSeq + " for " + ImapFolderPusher.this.getLogId());
                        }
                        if (!flagSyncMsgSeqs.contains(Long.valueOf(msgSeq))) {
                            flagSyncMsgSeqs.add(Long.valueOf(msgSeq));
                        }
                    }
                    if (ImapResponseParser.equalsIgnoreCase(responseType, Responses.EXPUNGE)) {
                        long msgSeq2 = response.getLong(0);
                        if (msgSeq2 <= oldMessageCount) {
                            messageCountDelta = -1;
                        }
                        if (K9MailLib.isDebug()) {
                            Log.d("k9", "Got untagged EXPUNGE for msgseq " + msgSeq2 + " for " + ImapFolderPusher.this.getLogId());
                        }
                        List<Long> newSeqs = new ArrayList<>();
                        Iterator<Long> flagIter = flagSyncMsgSeqs.iterator();
                        while (flagIter.hasNext()) {
                            long flagMsg = flagIter.next().longValue();
                            if (flagMsg >= msgSeq2) {
                                flagIter.remove();
                                if (flagMsg > msgSeq2) {
                                    newSeqs.add(Long.valueOf(flagMsg));
                                }
                            }
                        }
                        flagSyncMsgSeqs.addAll(newSeqs);
                        List<Long> msgSeqs = new ArrayList<>(ImapFolderPusher.this.msgSeqUidMap.keySet());
                        Collections.sort(msgSeqs);
                        for (Long longValue : msgSeqs) {
                            long msgSeqNum = longValue.longValue();
                            if (K9MailLib.isDebug()) {
                                Log.v("k9", "Comparing EXPUNGEd msgSeq " + msgSeq2 + " to " + msgSeqNum);
                            }
                            if (msgSeqNum == msgSeq2) {
                                String uid = (String) ImapFolderPusher.this.msgSeqUidMap.get(Long.valueOf(msgSeqNum));
                                if (K9MailLib.isDebug()) {
                                    Log.d("k9", "Scheduling removal of UID " + uid + " because msgSeq " + msgSeqNum + " was expunged");
                                }
                                removeMsgUids.add(uid);
                                ImapFolderPusher.this.msgSeqUidMap.remove(Long.valueOf(msgSeqNum));
                            } else if (msgSeqNum > msgSeq2) {
                                String uid2 = (String) ImapFolderPusher.this.msgSeqUidMap.get(Long.valueOf(msgSeqNum));
                                if (K9MailLib.isDebug()) {
                                    Log.d("k9", "Reducing msgSeq for UID " + uid2 + " from " + msgSeqNum + " to " + (msgSeqNum - 1));
                                }
                                ImapFolderPusher.this.msgSeqUidMap.remove(Long.valueOf(msgSeqNum));
                                ImapFolderPusher.this.msgSeqUidMap.put(Long.valueOf(msgSeqNum - 1), uid2);
                            }
                        }
                    }
                } catch (Exception e) {
                    Log.e("k9", "Could not handle untagged FETCH for " + ImapFolderPusher.this.getLogId(), e);
                }
            }
            return messageCountDelta;
        }

        private void syncMessages(int end) throws MessagingException {
            long oldUidNext = getOldUidNext();
            List<ImapMessage> messageList = ImapFolderPusher.this.getMessages(end, end, null, true, null);
            if (messageList != null && messageList.size() > 0) {
                long newUid = Long.parseLong(messageList.get(0).getUid());
                if (K9MailLib.isDebug()) {
                    Log.i("k9", "Got newUid " + newUid + " for message " + end + " on " + ImapFolderPusher.this.getLogId());
                }
                long startUid = oldUidNext;
                if (startUid < newUid - 10) {
                    startUid = newUid - 10;
                }
                if (startUid < 1) {
                    startUid = 1;
                }
                if (newUid >= startUid) {
                    if (K9MailLib.isDebug()) {
                        Log.i("k9", "Needs sync from uid " + startUid + " to " + newUid + " for " + ImapFolderPusher.this.getLogId());
                    }
                    List<Message> messages = new ArrayList<>();
                    for (long uid = startUid; uid <= newUid; uid++) {
                        messages.add(new ImapMessage(Long.toString(uid), ImapFolderPusher.this));
                    }
                    if (!messages.isEmpty()) {
                        ImapFolderPusher.this.pushReceiver.messagesArrived(ImapFolderPusher.this, messages);
                    }
                }
            }
        }

        private void syncMessages(List<Long> flagSyncMsgSeqs) {
            try {
                List<? extends Message> messageList = ImapFolderPusher.this.getMessages(flagSyncMsgSeqs, true, null);
                List<Message> messages = new ArrayList<>();
                messages.addAll(messageList);
                ImapFolderPusher.this.pushReceiver.messagesFlagsChanged(ImapFolderPusher.this, messages);
            } catch (Exception e) {
                ImapFolderPusher.this.pushReceiver.pushError("Exception while processing Push untagged responses", e);
            }
        }

        private void removeMessages(List<String> removeUids) {
            List<Message> messages = new ArrayList<>(removeUids.size());
            try {
                for (ImapMessage existingMessage : ImapFolderPusher.this.getMessagesFromUids(removeUids)) {
                    this.needsPoll = true;
                    ImapFolderPusher.this.msgSeqUidMap.clear();
                    String existingUid = existingMessage.getUid();
                    Log.w("k9", "Message with UID " + existingUid + " still exists on server, not expunging");
                    removeUids.remove(existingUid);
                }
                for (String uid : removeUids) {
                    ImapMessage message = new ImapMessage(uid, ImapFolderPusher.this);
                    try {
                        message.setFlagInternal(Flag.DELETED, true);
                    } catch (MessagingException e) {
                        Log.e("k9", "Unable to set DELETED flag on message " + message.getUid());
                    }
                    messages.add(message);
                }
                ImapFolderPusher.this.pushReceiver.messagesRemoved(ImapFolderPusher.this, messages);
            } catch (Exception e2) {
                Log.e("k9", "Cannot remove EXPUNGEd messages", e2);
            }
        }

        private void syncFolderOnConnect() throws MessagingException {
            processStoredUntaggedResponses();
            if (ImapFolderPusher.this.messageCount == -1) {
                throw new MessagingException("Message count = -1 for idling");
            }
            ImapFolderPusher.this.pushReceiver.syncFolder(ImapFolderPusher.this);
        }

        private void notifyMessagesArrived(long startUid, long uidNext) {
            if (K9MailLib.isDebug()) {
                Log.i("k9", "Needs sync from uid " + startUid + " to " + uidNext + " for " + ImapFolderPusher.this.getLogId());
            }
            List<Message> messages = new ArrayList<>((int) (uidNext - startUid));
            for (long uid = startUid; uid < uidNext; uid++) {
                messages.add(new ImapMessage(Long.toString(uid), ImapFolderPusher.this));
            }
            ImapFolderPusher.this.pushReceiver.messagesArrived(ImapFolderPusher.this, messages);
        }

        private long getOldUidNext() {
            long oldUidNext = -1;
            try {
                oldUidNext = ImapPushState.parse(ImapFolderPusher.this.pushReceiver.getPushState(ImapFolderPusher.this.getName())).uidNext;
                if (K9MailLib.isDebug()) {
                    Log.i("k9", "Got oldUidNext " + oldUidNext + " for " + ImapFolderPusher.this.getLogId());
                }
            } catch (Exception e) {
                Log.e("k9", "Unable to get oldUidNext for " + ImapFolderPusher.this.getLogId(), e);
            }
            return oldUidNext;
        }
    }

    private static class IdleStopper {
        private boolean acceptDoneContinuation;
        private ImapConnection imapConnection;

        private IdleStopper() {
            this.acceptDoneContinuation = false;
        }

        public synchronized void startAcceptingDoneContinuation(ImapConnection connection) {
            if (connection == null) {
                throw new NullPointerException("connection must not be null");
            }
            this.acceptDoneContinuation = true;
            this.imapConnection = connection;
        }

        public synchronized void stopAcceptingDoneContinuation() {
            this.acceptDoneContinuation = false;
            this.imapConnection = null;
        }

        public synchronized void stopIdle() {
            if (this.acceptDoneContinuation) {
                this.acceptDoneContinuation = false;
                sendDone();
            }
        }

        private void sendDone() {
            try {
                this.imapConnection.setReadTimeout(60000);
                this.imapConnection.sendContinuation("DONE");
            } catch (IOException e) {
                this.imapConnection.close();
            }
        }
    }
}
