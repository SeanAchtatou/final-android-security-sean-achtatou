package com.fsck.k9.mail.internet;

import android.support.annotation.NonNull;
import com.fsck.k9.mail.Body;
import com.fsck.k9.mail.BodyPart;
import com.fsck.k9.mail.MessagingException;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import org.apache.james.mime4j.dom.field.ContentTypeField;

public class MimeBodyPart extends BodyPart {
    private Body mBody;
    private final MimeHeader mHeader;

    public MimeBodyPart() throws MessagingException {
        this(null);
    }

    public MimeBodyPart(Body body) throws MessagingException {
        this(body, (String) null);
    }

    public MimeBodyPart(Body body, String mimeType) throws MessagingException {
        this.mHeader = new MimeHeader();
        if (mimeType != null) {
            addHeader("Content-Type", mimeType);
        }
        MimeMessageHelper.setBody(this, body);
    }

    MimeBodyPart(MimeHeader header, Body body) throws MessagingException {
        this.mHeader = header;
        MimeMessageHelper.setBody(this, body);
    }

    private String getFirstHeader(String name) {
        return this.mHeader.getFirstHeader(name);
    }

    public void addHeader(String name, String value) {
        this.mHeader.addHeader(name, value);
    }

    public void addRawHeader(String name, String raw) {
        this.mHeader.addRawHeader(name, raw);
    }

    public void setHeader(String name, String value) {
        this.mHeader.setHeader(name, value);
    }

    @NonNull
    public String[] getHeader(String name) {
        return this.mHeader.getHeader(name);
    }

    public void removeHeader(String name) {
        this.mHeader.removeHeader(name);
    }

    public Body getBody() {
        return this.mBody;
    }

    public void setBody(Body body) {
        this.mBody = body;
    }

    public void setEncoding(String encoding) throws MessagingException {
        if (this.mBody != null) {
            this.mBody.setEncoding(encoding);
        }
        setHeader("Content-Transfer-Encoding", encoding);
    }

    public String getContentType() {
        String contentType = getFirstHeader("Content-Type");
        return contentType == null ? ContentTypeField.TYPE_TEXT_PLAIN : MimeUtility.unfoldAndDecode(contentType);
    }

    public String getDisposition() {
        return getFirstHeader("Content-Disposition");
    }

    public String getContentId() {
        String contentId = getFirstHeader("Content-ID");
        if (contentId == null) {
            return null;
        }
        int first = contentId.indexOf(60);
        int last = contentId.lastIndexOf(62);
        if (first == -1 || last == -1) {
            return contentId;
        }
        return contentId.substring(first + 1, last);
    }

    public String getMimeType() {
        return MimeUtility.getHeaderParameter(getContentType(), null);
    }

    public boolean isMimeType(String mimeType) {
        return getMimeType().equalsIgnoreCase(mimeType);
    }

    public void writeTo(OutputStream out) throws IOException, MessagingException {
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out), 1024);
        this.mHeader.writeTo(out);
        writer.write("\r\n");
        writer.flush();
        if (this.mBody != null) {
            this.mBody.writeTo(out);
        }
    }

    public void writeHeaderTo(OutputStream out) throws IOException, MessagingException {
        this.mHeader.writeTo(out);
    }
}
