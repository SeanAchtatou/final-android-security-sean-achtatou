package com.fsck.k9.mail;

import java.util.List;

public interface Pusher {
    long getLastRefresh();

    int getRefreshInterval();

    void refresh();

    void setLastRefresh(long j);

    void start(List<String> list);

    void stop();
}
