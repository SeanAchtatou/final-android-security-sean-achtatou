package com.fsck.k9.mail.internet;

import com.fsck.k9.mail.BodyPart;
import com.fsck.k9.mail.BoundaryGenerator;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.Multipart;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import org.apache.james.mime4j.dom.field.ContentTypeField;

public class MimeMultipart extends Multipart {
    private final String boundary;
    private byte[] epilogue;
    private String mimeType;
    private byte[] preamble;

    public static MimeMultipart newInstance() {
        return new MimeMultipart(BoundaryGenerator.getInstance().generateBoundary());
    }

    public MimeMultipart(String boundary2) {
        this("multipart/mixed", boundary2);
    }

    public MimeMultipart(String mimeType2, String boundary2) {
        if (mimeType2 == null) {
            throw new IllegalArgumentException("mimeType can't be null");
        } else if (boundary2 == null) {
            throw new IllegalArgumentException("boundary can't be null");
        } else {
            this.mimeType = mimeType2;
            this.boundary = boundary2;
        }
    }

    public String getBoundary() {
        return this.boundary;
    }

    public byte[] getPreamble() {
        return this.preamble;
    }

    public void setPreamble(byte[] preamble2) {
        this.preamble = preamble2;
    }

    public byte[] getEpilogue() {
        return this.epilogue;
    }

    public void setEpilogue(byte[] epilogue2) {
        this.epilogue = epilogue2;
    }

    public String getMimeType() {
        return this.mimeType;
    }

    public void setSubType(String subType) {
        this.mimeType = ContentTypeField.TYPE_MULTIPART_PREFIX + subType;
    }

    public void writeTo(OutputStream out) throws IOException, MessagingException {
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out), 1024);
        if (this.preamble != null) {
            out.write(this.preamble);
            writer.write("\r\n");
        }
        if (getBodyParts().isEmpty()) {
            writer.write("--");
            writer.write(this.boundary);
            writer.write("\r\n");
        } else {
            for (BodyPart bodyPart : getBodyParts()) {
                writer.write("--");
                writer.write(this.boundary);
                writer.write("\r\n");
                writer.flush();
                bodyPart.writeTo(out);
                writer.write("\r\n");
            }
        }
        writer.write("--");
        writer.write(this.boundary);
        writer.write("--\r\n");
        writer.flush();
        if (this.epilogue != null) {
            out.write(this.epilogue);
        }
    }

    public InputStream getInputStream() throws MessagingException {
        return null;
    }
}
