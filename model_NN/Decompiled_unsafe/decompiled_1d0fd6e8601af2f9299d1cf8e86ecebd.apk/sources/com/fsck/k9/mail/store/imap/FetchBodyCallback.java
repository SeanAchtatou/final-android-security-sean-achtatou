package com.fsck.k9.mail.store.imap;

import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.filter.FixedLengthInputStream;
import java.io.IOException;
import java.util.Map;

class FetchBodyCallback implements ImapResponseCallback {
    private Map<String, Message> mMessageMap;

    FetchBodyCallback(Map<String, Message> messageMap) {
        this.mMessageMap = messageMap;
    }

    public Object foundLiteral(ImapResponse response, FixedLengthInputStream literal) throws MessagingException, IOException {
        if (response.getTag() != null || !ImapResponseParser.equalsIgnoreCase(response.get(1), "FETCH")) {
            return null;
        }
        ((ImapMessage) this.mMessageMap.get(((ImapList) response.getKeyedValue("FETCH")).getKeyedString("UID"))).parse(literal);
        return 1;
    }
}
