package com.fsck.k9.mail.store.webdav;

import android.util.Log;
import com.fsck.k9.mail.K9MailLib;
import com.fsck.k9.mail.helper.UrlEncodingHelper;
import java.net.URI;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;

public class HttpGeneric extends HttpEntityEnclosingRequestBase {
    public String METHOD_NAME = "POST";

    public HttpGeneric() {
    }

    public HttpGeneric(URI uri) {
        setURI(uri);
    }

    public HttpGeneric(String uri) {
        if (K9MailLib.isDebug()) {
            Log.v("k9", "Starting uri = '" + uri + "'");
        }
        String[] urlParts = uri.split("/");
        int length = urlParts.length;
        String end = urlParts[length - 1];
        String url = "";
        if (length > 3) {
            try {
                end = UrlEncodingHelper.encodeUtf8(UrlEncodingHelper.decodeUtf8(end)).replaceAll("\\+", "%20");
            } catch (IllegalArgumentException iae) {
                Log.e("k9", "IllegalArgumentException caught in HttpGeneric(String uri): " + iae + "\nTrace: " + WebDavUtils.processException(iae));
            }
        }
        for (int i = 0; i < length - 1; i++) {
            if (i != 0) {
                url = url + "/" + urlParts[i];
            } else {
                url = urlParts[i];
            }
        }
        if (K9MailLib.isDebug() && K9MailLib.DEBUG_PROTOCOL_WEBDAV) {
            Log.v("k9", "url = '" + url + "' length = " + url.length() + ", end = '" + end + "' length = " + end.length());
        }
        String url2 = url + "/" + end;
        Log.i("k9", "url = " + url2);
        setURI(URI.create(url2));
    }

    public String getMethod() {
        return this.METHOD_NAME;
    }

    public void setMethod(String method) {
        if (method != null) {
            this.METHOD_NAME = method;
        }
    }
}
