package com.fsck.k9.mail.store.imap;

import com.beetstra.jutf7.CharsetProvider;
import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CodingErrorAction;

class FolderNameCodec {
    private final Charset asciiCharset = Charset.forName("US-ASCII");
    private final Charset modifiedUtf7Charset = new CharsetProvider().charsetForName("X-RFC-3501");

    public static FolderNameCodec newInstance() {
        return new FolderNameCodec();
    }

    private FolderNameCodec() {
    }

    public String encode(String folderName) {
        ByteBuffer byteBuffer = this.modifiedUtf7Charset.encode(folderName);
        byte[] bytes = new byte[byteBuffer.limit()];
        byteBuffer.get(bytes);
        return new String(bytes, this.asciiCharset);
    }

    public String decode(String encodedFolderName) throws CharacterCodingException {
        return this.modifiedUtf7Charset.newDecoder().onMalformedInput(CodingErrorAction.REPORT).decode(ByteBuffer.wrap(encodedFolderName.getBytes(this.asciiCharset))).toString();
    }
}
