package com.fsck.k9.mail.store.webdav;

import android.util.Log;
import com.fsck.k9.mail.FetchProfile;
import com.fsck.k9.mail.Flag;
import com.fsck.k9.mail.Folder;
import com.fsck.k9.mail.K9MailLib;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.MessageRetrievalListener;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.filter.EOLConvertingOutputStream;
import com.fsck.k9.mail.helper.UrlEncodingHelper;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.StringEntity;
import org.apache.james.mime4j.dom.field.ContentTypeField;

class WebDavFolder extends Folder<WebDavMessage> {
    private String mFolderUrl;
    private boolean mIsOpen = false;
    private int mMessageCount = 0;
    private String mName;
    private int mUnreadMessageCount = 0;
    private WebDavStore store;

    /* access modifiers changed from: protected */
    public WebDavStore getStore() {
        return this.store;
    }

    public WebDavFolder(WebDavStore nStore, String name) {
        this.store = nStore;
        this.mName = name;
        buildFolderUrl();
    }

    private void buildFolderUrl() {
        String[] urlParts = this.mName.split("/");
        String url = "";
        int count = urlParts.length;
        for (int i = 0; i < count; i++) {
            if (i != 0) {
                url = url + "/" + UrlEncodingHelper.encodeUtf8(urlParts[i]);
            } else {
                url = UrlEncodingHelper.encodeUtf8(urlParts[i]);
            }
        }
        String encodedName = url.replaceAll("\\+", "%20");
        this.mFolderUrl = this.store.getUrl();
        if (!this.store.getUrl().endsWith("/")) {
            this.mFolderUrl += "/";
        }
        this.mFolderUrl += encodedName;
    }

    public void setUrl(String url) {
        if (url != null) {
            this.mFolderUrl = url;
        }
    }

    public void open(int mode) throws MessagingException {
        this.store.getHttpClient();
        this.mIsOpen = true;
    }

    public Map<String, String> copyMessages(List<? extends Message> messages, Folder folder) throws MessagingException {
        moveOrCopyMessages(messages, folder.getName(), false);
        return null;
    }

    public Map<String, String> moveMessages(List<? extends Message> messages, Folder folder) throws MessagingException {
        moveOrCopyMessages(messages, folder.getName(), true);
        return null;
    }

    public void delete(List<? extends Message> msgs, String trashFolderName) throws MessagingException {
        moveOrCopyMessages(msgs, trashFolderName, true);
    }

    private void moveOrCopyMessages(List<? extends Message> messages, String folderName, boolean isMove) throws MessagingException {
        String[] uids = new String[messages.size()];
        int count = messages.size();
        for (int i = 0; i < count; i++) {
            uids[i] = ((Message) messages.get(i)).getUid();
        }
        Map<String, String> headers = new HashMap<>();
        Map<String, String> uidToUrl = getMessageUrls(uids);
        String[] urls = new String[uids.length];
        int count2 = uids.length;
        for (int i2 = 0; i2 < count2; i2++) {
            urls[i2] = uidToUrl.get(uids[i2]);
            if (urls[i2] == null && (messages.get(i2) instanceof WebDavMessage)) {
                urls[i2] = ((WebDavMessage) messages.get(i2)).getUrl();
            }
        }
        String messageBody = this.store.getMoveOrCopyMessagesReadXml(urls, isMove);
        WebDavFolder destFolder = this.store.getFolder(folderName);
        headers.put("Destination", destFolder.mFolderUrl);
        headers.put("Brief", "t");
        headers.put("If-Match", "*");
        String action = isMove ? "BMOVE" : "BCOPY";
        Log.i("k9", "Moving " + messages.size() + " messages to " + destFolder.mFolderUrl);
        this.store.processRequest(this.mFolderUrl, action, messageBody, headers, false);
    }

    private int getMessageCount(boolean read) throws MessagingException {
        String isRead;
        int messageCount = 0;
        Map<String, String> headers = new HashMap<>();
        if (read) {
            isRead = "True";
        } else {
            isRead = "False";
        }
        String messageBody = this.store.getMessageCountXml(isRead);
        headers.put("Brief", "t");
        DataSet dataset = this.store.processRequest(this.mFolderUrl, Responses.SEARCH, messageBody, headers);
        if (dataset != null) {
            messageCount = dataset.getMessageCount();
        }
        if (K9MailLib.isDebug() && K9MailLib.DEBUG_PROTOCOL_WEBDAV) {
            Log.v("k9", "Counted messages and webdav returned: " + messageCount);
        }
        return messageCount;
    }

    public int getMessageCount() throws MessagingException {
        open(0);
        this.mMessageCount = getMessageCount(true);
        return this.mMessageCount;
    }

    public int getUnreadMessageCount() throws MessagingException {
        open(0);
        this.mUnreadMessageCount = getMessageCount(false);
        return this.mUnreadMessageCount;
    }

    public int getFlaggedMessageCount() throws MessagingException {
        return -1;
    }

    public boolean isOpen() {
        return this.mIsOpen;
    }

    public int getMode() {
        return 0;
    }

    public String getName() {
        return this.mName;
    }

    public boolean exists() {
        return true;
    }

    public void close() {
        this.mMessageCount = 0;
        this.mUnreadMessageCount = 0;
        this.mIsOpen = false;
    }

    public boolean create(Folder.FolderType type) throws MessagingException {
        return true;
    }

    public void delete(boolean recursive) throws MessagingException {
        throw new Error("WebDavFolder.delete() not implemeneted");
    }

    public WebDavMessage getMessage(String uid) throws MessagingException {
        return new WebDavMessage(uid, this);
    }

    public List<WebDavMessage> getMessages(int start, int end, Date earliestDate, MessageRetrievalListener<WebDavMessage> listener) throws MessagingException {
        List<WebDavMessage> messages = new ArrayList<>();
        Map<String, String> headers = new HashMap<>();
        int prevStart = start;
        int start2 = this.mMessageCount - end;
        int end2 = start2 + (end - prevStart);
        if (start2 < 0 || end2 < 0 || end2 < start2) {
            throw new MessagingException(String.format(Locale.US, "Invalid message set %d %d", Integer.valueOf(start2), Integer.valueOf(end2)));
        }
        if (start2 == 0 && end2 < 10) {
            end2 = 10;
        }
        String messageBody = this.store.getMessagesXml();
        headers.put("Brief", "t");
        headers.put("Range", "rows=" + start2 + "-" + end2);
        DataSet dataset = this.store.processRequest(this.mFolderUrl, Responses.SEARCH, messageBody, headers);
        String[] uids = dataset.getUids();
        Map<String, String> uidToUrl = dataset.getUidToUrl();
        int uidsLength = uids.length;
        for (int i = 0; i < uidsLength; i++) {
            if (listener != null) {
                listener.messageStarted(uids[i], i, uidsLength);
            }
            WebDavMessage message = new WebDavMessage(uids[i], this);
            message.setUrl(uidToUrl.get(uids[i]));
            messages.add(message);
            if (listener != null) {
                listener.messageFinished(message, i, uidsLength);
            }
        }
        return messages;
    }

    public boolean areMoreMessagesAvailable(int indexOfOldestMessage, Date earliestDate) {
        return indexOfOldestMessage > 1;
    }

    private Map<String, String> getMessageUrls(String[] uids) throws MessagingException {
        Map<String, String> headers = new HashMap<>();
        String messageBody = this.store.getMessageUrlsXml(uids);
        headers.put("Brief", "t");
        return this.store.processRequest(this.mFolderUrl, Responses.SEARCH, messageBody, headers).getUidToUrl();
    }

    public void fetch(List<WebDavMessage> messages, FetchProfile fp, MessageRetrievalListener<WebDavMessage> listener) throws MessagingException {
        if (messages != null && !messages.isEmpty()) {
            if (fp.contains(FetchProfile.Item.ENVELOPE)) {
                fetchEnvelope(messages, listener);
            }
            if (fp.contains(FetchProfile.Item.FLAGS)) {
                fetchFlags(messages, listener);
            }
            if (fp.contains(FetchProfile.Item.BODY_SANE)) {
                int maximumAutoDownloadSize = this.store.getStoreConfig().getMaximumAutoDownloadMessageSize();
                if (maximumAutoDownloadSize > 0) {
                    fetchMessages(messages, listener, maximumAutoDownloadSize / 76);
                } else {
                    fetchMessages(messages, listener, -1);
                }
            }
            if (fp.contains(FetchProfile.Item.BODY)) {
                fetchMessages(messages, listener, -1);
            }
        }
    }

    private void fetchMessages(List<WebDavMessage> messages, MessageRetrievalListener<WebDavMessage> listener, int lines) throws MessagingException {
        WebDavHttpClient httpclient = this.store.getHttpClient();
        int i = 0;
        int count = messages.size();
        while (i < count) {
            WebDavMessage wdMessage = messages.get(i);
            int statusCode = 0;
            if (listener != null) {
                listener.messageStarted(wdMessage.getUid(), i, count);
            }
            if (wdMessage.getUrl().equals("")) {
                wdMessage.setUrl(getMessageUrls(new String[]{wdMessage.getUid()}).get(wdMessage.getUid()));
                Log.i("k9", "Fetching messages with UID = '" + wdMessage.getUid() + "', URL = '" + wdMessage.getUrl() + "'");
                if (wdMessage.getUrl().equals("")) {
                    throw new MessagingException("Unable to get URL for message");
                }
            }
            try {
                Log.i("k9", "Fetching message with UID = '" + wdMessage.getUid() + "', URL = '" + wdMessage.getUrl() + "'");
                HttpGet httpget = new HttpGet(new URI(wdMessage.getUrl()));
                httpget.setHeader("translate", "f");
                if (this.store.getAuthentication() == 1) {
                    httpget.setHeader("Authorization", this.store.getAuthString());
                }
                HttpResponse response = httpclient.executeOverride(httpget, this.store.getContext());
                statusCode = response.getStatusLine().getStatusCode();
                HttpEntity entity = response.getEntity();
                if (statusCode < 200 || statusCode > 300) {
                    throw new IOException("Error during with code " + statusCode + " during fetch: " + response.getStatusLine().toString());
                }
                if (entity != null) {
                    InputStream istream = null;
                    StringBuilder buffer = new StringBuilder();
                    BufferedReader reader = null;
                    int currentLines = 0;
                    try {
                        istream = WebDavHttpClient.getUngzippedContent(entity);
                        if (lines != -1) {
                            BufferedReader reader2 = new BufferedReader(new InputStreamReader(istream), 8192);
                            while (true) {
                                try {
                                    String tempText = reader2.readLine();
                                    if (tempText == null || currentLines >= lines) {
                                        IOUtils.closeQuietly(istream);
                                    } else {
                                        buffer.append(tempText).append("\r\n");
                                        currentLines++;
                                    }
                                } catch (IOException e) {
                                    ioe = e;
                                    reader = reader2;
                                    try {
                                        Log.e("k9", "IOException: " + ioe.getMessage() + "\nTrace: " + WebDavUtils.processException(ioe));
                                        throw new MessagingException("I/O Error", ioe);
                                    } catch (Throwable th) {
                                        th = th;
                                        IOUtils.closeQuietly((Reader) reader);
                                        IOUtils.closeQuietly(istream);
                                        throw th;
                                    }
                                } catch (Throwable th2) {
                                    th = th2;
                                    reader = reader2;
                                    IOUtils.closeQuietly((Reader) reader);
                                    IOUtils.closeQuietly(istream);
                                    throw th;
                                }
                            }
                            IOUtils.closeQuietly(istream);
                            reader = reader2;
                            istream = new ByteArrayInputStream(buffer.toString().getBytes("UTF-8"));
                        }
                        wdMessage.parse(istream);
                        IOUtils.closeQuietly((Reader) reader);
                        IOUtils.closeQuietly(istream);
                    } catch (IOException e2) {
                        ioe = e2;
                        Log.e("k9", "IOException: " + ioe.getMessage() + "\nTrace: " + WebDavUtils.processException(ioe));
                        throw new MessagingException("I/O Error", ioe);
                    }
                } else {
                    Log.v("k9", "Empty response");
                }
                if (listener != null) {
                    listener.messageFinished(wdMessage, i, count);
                }
                i++;
            } catch (IllegalArgumentException iae) {
                Log.e("k9", "IllegalArgumentException caught " + iae + "\nTrace: " + WebDavUtils.processException(iae));
                throw new MessagingException("IllegalArgumentException caught", iae);
            } catch (URISyntaxException use) {
                Log.e("k9", "URISyntaxException caught " + use + "\nTrace: " + WebDavUtils.processException(use));
                throw new MessagingException("URISyntaxException caught", use);
            } catch (IOException ioe) {
                Log.e("k9", "Non-success response code loading message, response code was " + statusCode + "\nURL: " + wdMessage.getUrl() + "\nError: " + ioe.getMessage() + "\nTrace: " + WebDavUtils.processException(ioe));
                throw new MessagingException("Failure code " + statusCode, ioe);
            }
        }
    }

    private void fetchFlags(List<WebDavMessage> startMessages, MessageRetrievalListener<WebDavMessage> listener) throws MessagingException {
        HashMap<String, String> headers = new HashMap<>();
        List<Message> messages = new ArrayList<>(20);
        if (startMessages != null && !startMessages.isEmpty()) {
            if (startMessages.size() > 20) {
                List<WebDavMessage> newMessages = new ArrayList<>(startMessages.size() - 20);
                int count = startMessages.size();
                for (int i = 0; i < count; i++) {
                    if (i < 20) {
                        messages.add(startMessages.get(i));
                    } else {
                        newMessages.add(startMessages.get(i));
                    }
                }
                fetchFlags(newMessages, listener);
            } else {
                messages.addAll(startMessages);
            }
            String[] uids = new String[messages.size()];
            int count2 = messages.size();
            for (int i2 = 0; i2 < count2; i2++) {
                uids[i2] = ((Message) messages.get(i2)).getUid();
            }
            String messageBody = this.store.getMessageFlagsXml(uids);
            headers.put("Brief", "t");
            DataSet dataset = this.store.processRequest(this.mFolderUrl, Responses.SEARCH, messageBody, headers);
            if (dataset == null) {
                throw new MessagingException("Data Set from request was null");
            }
            Map<String, Boolean> uidToReadStatus = dataset.getUidToRead();
            int count3 = messages.size();
            for (int i3 = 0; i3 < count3; i3++) {
                if (!(messages.get(i3) instanceof WebDavMessage)) {
                    throw new MessagingException("WebDavStore fetch called with non-WebDavMessage");
                }
                WebDavMessage wdMessage = (WebDavMessage) messages.get(i3);
                try {
                    wdMessage.setFlagInternal(Flag.SEEN, uidToReadStatus.get(wdMessage.getUid()).booleanValue());
                } catch (NullPointerException e) {
                    Log.v("k9", "Under some weird circumstances, setting the read status when syncing from webdav threw an NPE. Skipping.");
                }
            }
        }
    }

    private void fetchEnvelope(List<WebDavMessage> startMessages, MessageRetrievalListener<WebDavMessage> listener) throws MessagingException {
        Map<String, String> headers = new HashMap<>();
        List<WebDavMessage> messages = new ArrayList<>(10);
        if (startMessages != null && !startMessages.isEmpty()) {
            if (startMessages.size() > 10) {
                List<WebDavMessage> newMessages = new ArrayList<>(startMessages.size() - 10);
                int count = startMessages.size();
                for (int i = 0; i < count; i++) {
                    if (i < 10) {
                        messages.add(i, startMessages.get(i));
                    } else {
                        newMessages.add(i - 10, startMessages.get(i));
                    }
                }
                fetchEnvelope(newMessages, listener);
            } else {
                messages.addAll(startMessages);
            }
            String[] uids = new String[messages.size()];
            int count2 = messages.size();
            for (int i2 = 0; i2 < count2; i2++) {
                uids[i2] = ((WebDavMessage) messages.get(i2)).getUid();
            }
            String messageBody = this.store.getMessageEnvelopeXml(uids);
            headers.put("Brief", "t");
            Map<String, ParsedMessageEnvelope> envelopes = this.store.processRequest(this.mFolderUrl, Responses.SEARCH, messageBody, headers).getMessageEnvelopes();
            int count3 = messages.size();
            for (int i3 = messages.size() - 1; i3 >= 0; i3--) {
                WebDavMessage message = (WebDavMessage) messages.get(i3);
                if (listener != null) {
                    listener.messageStarted(((WebDavMessage) messages.get(i3)).getUid(), i3, count3);
                }
                ParsedMessageEnvelope envelope = envelopes.get(message.getUid());
                if (envelope != null) {
                    message.setNewHeaders(envelope);
                    message.setFlagInternal(Flag.SEEN, envelope.getReadStatus());
                } else {
                    Log.e("k9", "Asked to get metadata for a non-existent message: " + message.getUid());
                }
                if (listener != null) {
                    listener.messageFinished((Message) messages.get(i3), i3, count3);
                }
            }
        }
    }

    public void setFlags(List<? extends Message> messages, Set<Flag> flags, boolean value) throws MessagingException {
        String[] uids = new String[messages.size()];
        int count = messages.size();
        for (int i = 0; i < count; i++) {
            uids[i] = ((Message) messages.get(i)).getUid();
        }
        for (Flag flag : flags) {
            if (flag == Flag.SEEN) {
                markServerMessagesRead(uids, value);
            } else if (flag == Flag.DELETED) {
                deleteServerMessages(uids);
            }
        }
    }

    private void markServerMessagesRead(String[] uids, boolean read) throws MessagingException {
        Map<String, String> headers = new HashMap<>();
        Map<String, String> uidToUrl = getMessageUrls(uids);
        String[] urls = new String[uids.length];
        int count = uids.length;
        for (int i = 0; i < count; i++) {
            urls[i] = uidToUrl.get(uids[i]);
        }
        String messageBody = this.store.getMarkMessagesReadXml(urls, read);
        headers.put("Brief", "t");
        headers.put("If-Match", "*");
        this.store.processRequest(this.mFolderUrl, "BPROPPATCH", messageBody, headers, false);
    }

    private void deleteServerMessages(String[] uids) throws MessagingException {
        Map<String, String> uidToUrl = getMessageUrls(uids);
        for (String uid : uids) {
            Map<String, String> headers = new HashMap<>();
            String url = uidToUrl.get(uid);
            if (generateDeleteUrl(url).equals(url)) {
                headers.put("Brief", "t");
                this.store.processRequest(url, "DELETE", null, headers, false);
            } else {
                headers.put("Destination", generateDeleteUrl(url));
                headers.put("Brief", "t");
                this.store.processRequest(url, "MOVE", null, headers, false);
            }
        }
    }

    private String generateDeleteUrl(String startUrl) {
        String[] urlParts = startUrl.split("/");
        return this.store.getUrl() + "Deleted%20Items/" + urlParts[urlParts.length - 1];
    }

    public Map<String, String> appendMessages(List<? extends Message> messages) throws MessagingException {
        appendWebDavMessages(messages);
        return null;
    }

    public List<? extends Message> appendWebDavMessages(List<? extends Message> messages) throws MessagingException {
        List<Message> retMessages = new ArrayList<>(messages.size());
        WebDavHttpClient httpclient = this.store.getHttpClient();
        for (Message message : messages) {
            try {
                ByteArrayOutputStream out = new ByteArrayOutputStream(message.getSize());
                open(0);
                EOLConvertingOutputStream msgOut = new EOLConvertingOutputStream(new BufferedOutputStream(out, 1024));
                message.writeTo(msgOut);
                msgOut.flush();
                StringEntity bodyEntity = new StringEntity(out.toString(), "UTF-8");
                bodyEntity.setContentType(ContentTypeField.TYPE_MESSAGE_RFC822);
                String messageURL = this.mFolderUrl;
                if (!messageURL.endsWith("/")) {
                    messageURL = messageURL + "/";
                }
                String messageURL2 = messageURL + UrlEncodingHelper.encodeUtf8(message.getUid() + ":" + System.currentTimeMillis() + ".eml");
                Log.i("k9", "Uploading message as " + messageURL2);
                HttpGeneric httpmethod = new HttpGeneric(messageURL2);
                httpmethod.setMethod("PUT");
                httpmethod.setEntity(bodyEntity);
                String mAuthString = this.store.getAuthString();
                if (mAuthString != null) {
                    httpmethod.setHeader("Authorization", mAuthString);
                }
                HttpResponse response = httpclient.executeOverride(httpmethod, this.store.getContext());
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode < 200 || statusCode > 300) {
                    throw new IOException("Error with status code " + statusCode + " while sending/appending message.  Response = " + response.getStatusLine().toString() + " for message " + messageURL2);
                }
                WebDavMessage retMessage = new WebDavMessage(message.getUid(), this);
                retMessage.setUrl(messageURL2);
                retMessages.add(retMessage);
            } catch (Exception e) {
                throw new MessagingException("Unable to append", e);
            }
        }
        return retMessages;
    }

    public boolean equals(Object o) {
        if (o instanceof WebDavFolder) {
            return ((WebDavFolder) o).mName.equals(this.mName);
        }
        return super.equals(o);
    }

    public String getUidFromMessageId(Message message) throws MessagingException {
        Log.e("k9", "Unimplemented method getUidFromMessageId in WebDavStore.WebDavFolder could lead to duplicate messages  being uploaded to the Sent folder");
        return null;
    }

    public void setFlags(Set<Flag> set, boolean value) throws MessagingException {
        Log.e("k9", "Unimplemented method setFlags(Set<Flag>, boolean) breaks markAllMessagesAsRead and EmptyTrash");
    }

    public String getUrl() {
        return this.mFolderUrl;
    }
}
