package com.fsck.k9.mail.store.webdav;

class ConnectionInfo {
    public String guessedAuthUrl;
    public String redirectUrl;
    public short requiredAuthType;
    public int statusCode;

    ConnectionInfo() {
    }
}
