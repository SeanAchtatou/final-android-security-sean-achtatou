package com.fsck.k9.mail.store.imap;

import com.fsck.k9.mail.AuthType;
import com.fsck.k9.mail.ConnectionSecurity;
import com.fsck.k9.mail.NetworkType;

interface ImapSettings {
    AuthType getAuthType();

    String getClientCertificateAlias();

    String getCombinedPrefix();

    ConnectionSecurity getConnectionSecurity();

    String getHost();

    String getPassword();

    String getPathDelimiter();

    String getPathPrefix();

    int getPort();

    String getUsername();

    void setCombinedPrefix(String str);

    void setPathDelimiter(String str);

    void setPathPrefix(String str);

    boolean useCompression(NetworkType networkType);
}
