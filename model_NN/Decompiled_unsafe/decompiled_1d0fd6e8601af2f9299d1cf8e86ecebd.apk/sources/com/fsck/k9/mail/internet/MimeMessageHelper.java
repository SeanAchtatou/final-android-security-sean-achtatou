package com.fsck.k9.mail.internet;

import com.fsck.k9.mail.Body;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.Multipart;
import com.fsck.k9.mail.Part;
import org.apache.james.mime4j.dom.field.FieldName;
import org.apache.james.mime4j.util.MimeUtil;

public class MimeMessageHelper {
    private MimeMessageHelper() {
    }

    public static void setBody(Part part, Body body) throws MessagingException {
        String contentType;
        part.setBody(body);
        if (part instanceof Message) {
            part.setHeader(FieldName.MIME_VERSION, "1.0");
        }
        if (body instanceof Multipart) {
            Multipart multipart = (Multipart) body;
            multipart.setParent(part);
            part.setHeader("Content-Type", String.format("%s; boundary=\"%s\"", multipart.getMimeType(), multipart.getBoundary()));
            setEncoding(part, MimeUtil.ENC_7BIT);
        } else if (body instanceof TextBody) {
            if (MimeUtility.mimeTypeMatches(part.getMimeType(), "text/*")) {
                contentType = String.format("%s;\r\n charset=utf-8", part.getMimeType());
                String name = MimeUtility.getHeaderParameter(part.getContentType(), "name");
                if (name != null) {
                    contentType = contentType + String.format(";\r\n name=\"%s\"", name);
                }
            } else {
                contentType = part.getMimeType();
            }
            part.setHeader("Content-Type", contentType);
            setEncoding(part, MimeUtil.ENC_QUOTED_PRINTABLE);
        }
    }

    public static void setEncoding(Part part, String encoding) throws MessagingException {
        Body body = part.getBody();
        if (body != null) {
            body.setEncoding(encoding);
        }
        part.setHeader("Content-Transfer-Encoding", encoding);
    }
}
