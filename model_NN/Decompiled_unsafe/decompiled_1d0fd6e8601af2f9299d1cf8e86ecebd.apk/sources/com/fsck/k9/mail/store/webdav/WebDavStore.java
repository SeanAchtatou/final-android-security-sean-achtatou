package com.fsck.k9.mail.store.webdav;

import android.util.Log;
import com.fsck.k9.mail.CertificateValidationException;
import com.fsck.k9.mail.ConnectionSecurity;
import com.fsck.k9.mail.Folder;
import com.fsck.k9.mail.K9MailLib;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.ServerSettings;
import com.fsck.k9.mail.filter.Base64;
import com.fsck.k9.mail.helper.UrlEncodingHelper;
import com.fsck.k9.mail.store.RemoteStore;
import com.fsck.k9.mail.store.StoreConfig;
import com.fsck.k9.mail.store.webdav.WebDavHttpClient;
import com.fsck.k9.preferences.SettingsExporter;
import com.fsck.k9.provider.EmailProvider;
import exts.whats.Constants;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.net.ssl.SSLException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

public class WebDavStore extends RemoteStore {
    private String mAlias;
    private CookieStore mAuthCookies = null;
    private String mAuthPath;
    private String mAuthString;
    private short mAuthentication = 0;
    private String mCachedLoginUrl;
    private ConnectionSecurity mConnectionSecurity;
    private HttpContext mContext = null;
    private Map<String, WebDavFolder> mFolderList = new HashMap();
    private String mHost;
    private WebDavHttpClient mHttpClient = null;
    private final WebDavHttpClient.WebDavHttpClientFactory mHttpClientFactory;
    private String mMailboxPath;
    private String mPassword;
    private String mPath;
    private int mPort;
    private Folder mSendFolder = null;
    private String mUrl;
    private String mUsername;

    public static WebDavStoreSettings decodeUri(String uri) {
        ConnectionSecurity connectionSecurity;
        String username = null;
        String password = null;
        String alias = null;
        String path = null;
        String authPath = null;
        String mailboxPath = null;
        try {
            URI webDavUri = new URI(uri);
            String scheme = webDavUri.getScheme();
            if (scheme.equals("webdav")) {
                connectionSecurity = ConnectionSecurity.NONE;
            } else if (scheme.startsWith("webdav+")) {
                connectionSecurity = ConnectionSecurity.SSL_TLS_REQUIRED;
            } else {
                throw new IllegalArgumentException("Unsupported protocol (" + scheme + ")");
            }
            String host = webDavUri.getHost();
            if (host.startsWith("http")) {
                String[] hostParts = host.split("://", 2);
                if (hostParts.length > 1) {
                    host = hostParts[1];
                }
            }
            int port = webDavUri.getPort();
            String userInfo = webDavUri.getUserInfo();
            if (userInfo != null) {
                String[] userInfoParts = userInfo.split(":");
                username = UrlEncodingHelper.decodeUtf8(userInfoParts[0]);
                String[] userParts = username.split("\\\\", 2);
                if (userParts.length > 1) {
                    alias = userParts[1];
                } else {
                    alias = username;
                }
                if (userInfoParts.length > 1) {
                    password = UrlEncodingHelper.decodeUtf8(userInfoParts[1]);
                }
            }
            String[] pathParts = webDavUri.getPath().split("\\|");
            int count = pathParts.length;
            for (int i = 0; i < count; i++) {
                if (i == 0) {
                    if (pathParts[0] != null && pathParts[0].length() > 1) {
                        path = pathParts[0];
                    }
                } else if (i == 1) {
                    if (pathParts[1] != null && pathParts[1].length() > 1) {
                        authPath = pathParts[1];
                    }
                } else if (i == 2 && pathParts[2] != null && pathParts[2].length() > 1) {
                    mailboxPath = pathParts[2];
                }
            }
            return new WebDavStoreSettings(host, port, connectionSecurity, null, username, password, null, alias, path, authPath, mailboxPath);
        } catch (URISyntaxException use) {
            throw new IllegalArgumentException("Invalid WebDavStore URI", use);
        }
    }

    public static String createUri(ServerSettings server) {
        String scheme;
        String uriPath;
        String userEnc = UrlEncodingHelper.encodeUtf8(server.username);
        String passwordEnc = server.password != null ? UrlEncodingHelper.encodeUtf8(server.password) : "";
        switch (server.connectionSecurity) {
            case SSL_TLS_REQUIRED:
                scheme = "webdav+ssl+";
                break;
            default:
                scheme = "webdav";
                break;
        }
        String userInfo = userEnc + ":" + passwordEnc;
        Map<String, String> extra = server.getExtra();
        if (extra != null) {
            String path = extra.get(WebDavStoreSettings.PATH_KEY);
            if (path == null) {
                path = "";
            }
            String authPath = extra.get(WebDavStoreSettings.AUTH_PATH_KEY);
            if (authPath == null) {
                authPath = "";
            }
            String mailboxPath = extra.get(WebDavStoreSettings.MAILBOX_PATH_KEY);
            if (mailboxPath == null) {
                mailboxPath = "";
            }
            uriPath = "/" + path + "|" + authPath + "|" + mailboxPath;
        } else {
            uriPath = "/||";
        }
        try {
            return new URI(scheme, userInfo, server.host, server.port, uriPath, null, null).toString();
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException("Can't create WebDavStore URI", e);
        }
    }

    public WebDavStore(StoreConfig storeConfig, WebDavHttpClient.WebDavHttpClientFactory clientFactory) throws MessagingException {
        super(storeConfig, null);
        this.mHttpClientFactory = clientFactory;
        try {
            WebDavStoreSettings settings = decodeUri(storeConfig.getStoreUri());
            this.mHost = settings.host;
            this.mPort = settings.port;
            this.mConnectionSecurity = settings.connectionSecurity;
            this.mUsername = settings.username;
            this.mPassword = settings.password;
            this.mAlias = settings.alias;
            this.mPath = settings.path;
            this.mAuthPath = settings.authPath;
            this.mMailboxPath = settings.mailboxPath;
            if (this.mPath == null || this.mPath.equals("")) {
                this.mPath = "/Exchange";
            } else if (!this.mPath.startsWith("/")) {
                this.mPath = "/" + this.mPath;
            }
            if (this.mMailboxPath == null || this.mMailboxPath.equals("")) {
                this.mMailboxPath = "/" + this.mAlias;
            } else if (!this.mMailboxPath.startsWith("/")) {
                this.mMailboxPath = "/" + this.mMailboxPath;
            }
            if (this.mAuthPath != null && !this.mAuthPath.equals("") && !this.mAuthPath.startsWith("/")) {
                this.mAuthPath = "/" + this.mAuthPath;
            }
            this.mUrl = getRoot() + this.mPath + this.mMailboxPath;
            this.mAuthString = "Basic " + Base64.encode(this.mUsername + ":" + this.mPassword);
        } catch (IllegalArgumentException e) {
            throw new MessagingException("Error while decoding store URI", e);
        }
    }

    private String getRoot() {
        String root;
        if (this.mConnectionSecurity == ConnectionSecurity.SSL_TLS_REQUIRED) {
            root = "https";
        } else {
            root = "http";
        }
        return root + "://" + this.mHost + ":" + this.mPort;
    }

    /* access modifiers changed from: package-private */
    public HttpContext getContext() {
        return this.mContext;
    }

    /* access modifiers changed from: package-private */
    public short getAuthentication() {
        return this.mAuthentication;
    }

    /* access modifiers changed from: package-private */
    public StoreConfig getStoreConfig() {
        return this.mStoreConfig;
    }

    public void checkSettings() throws MessagingException {
        authenticate();
    }

    public List<? extends Folder> getPersonalNamespaces(boolean forceListAll) throws MessagingException {
        List<Folder> folderList = new LinkedList<>();
        getHttpClient();
        Map<String, String> headers = new HashMap<>();
        headers.put("Depth", "0");
        headers.put("Brief", "t");
        Map<String, String> specialFoldersMap = processRequest(this.mUrl, "PROPFIND", getSpecialFoldersList(), headers).getSpecialFolderToUrl();
        String folderName = getFolderName(specialFoldersMap.get("inbox"));
        if (folderName != null) {
            this.mStoreConfig.setAutoExpandFolderName(folderName);
            this.mStoreConfig.setInboxFolderName(folderName);
        }
        String folderName2 = getFolderName(specialFoldersMap.get("drafts"));
        if (folderName2 != null) {
            this.mStoreConfig.setDraftsFolderName(folderName2);
        }
        String folderName3 = getFolderName(specialFoldersMap.get("deleteditems"));
        if (folderName3 != null) {
            this.mStoreConfig.setTrashFolderName(folderName3);
        }
        String folderName4 = getFolderName(specialFoldersMap.get("junkemail"));
        if (folderName4 != null) {
            this.mStoreConfig.setSpamFolderName(folderName4);
        }
        String folderName5 = getFolderName(specialFoldersMap.get("sentitems"));
        if (folderName5 != null) {
            this.mStoreConfig.setSentFolderName(folderName5);
        }
        Map<String, String> headers2 = new HashMap<>();
        headers2.put("Brief", "t");
        for (String tempUrl : processRequest(this.mUrl, Responses.SEARCH, getFolderListXml(), headers2).getHrefs()) {
            WebDavFolder folder = createFolder(tempUrl);
            if (folder != null) {
                folderList.add(folder);
            }
        }
        return folderList;
    }

    private WebDavFolder createFolder(String folderUrl) {
        String folderName;
        if (folderUrl == null || (folderName = getFolderName(folderUrl)) == null) {
            return null;
        }
        WebDavFolder wdFolder = getFolder(folderName);
        if (wdFolder == null) {
            return wdFolder;
        }
        wdFolder.setUrl(folderUrl);
        return wdFolder;
    }

    private String getFolderName(String folderUrl) {
        String fullPathName;
        if (folderUrl == null) {
            return null;
        }
        int folderSlash = -1;
        for (int j = 0; j < 5 && (folderSlash = folderUrl.indexOf(47, folderSlash + 1)) >= 0; j++) {
        }
        if (folderSlash <= 0) {
            return null;
        }
        if (folderUrl.charAt(folderUrl.length() - 1) == '/') {
            fullPathName = folderUrl.substring(folderSlash + 1, folderUrl.length() - 1);
        } else {
            fullPathName = folderUrl.substring(folderSlash + 1);
        }
        return UrlEncodingHelper.decodeUtf8(fullPathName);
    }

    public WebDavFolder getFolder(String name) {
        WebDavFolder folder = this.mFolderList.get(name);
        if (folder != null) {
            return folder;
        }
        WebDavFolder folder2 = new WebDavFolder(this, name);
        this.mFolderList.put(name, folder2);
        return folder2;
    }

    public Folder getSendSpoolFolder() throws MessagingException {
        if (this.mSendFolder == null) {
            this.mSendFolder = getFolder("##DavMailSubmissionURI##");
        }
        return this.mSendFolder;
    }

    public boolean isMoveCapable() {
        return true;
    }

    public boolean isCopyCapable() {
        return true;
    }

    private String getSpecialFoldersList() {
        StringBuilder builder = new StringBuilder(200);
        builder.append("<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"no\"?>");
        builder.append("<propfind xmlns=\"DAV:\">");
        builder.append("<prop>");
        builder.append("<").append("inbox").append(" xmlns=\"urn:schemas:httpmail:\"/>");
        builder.append("<").append("drafts").append(" xmlns=\"urn:schemas:httpmail:\"/>");
        builder.append("<").append("outbox").append(" xmlns=\"urn:schemas:httpmail:\"/>");
        builder.append("<").append("sentitems").append(" xmlns=\"urn:schemas:httpmail:\"/>");
        builder.append("<").append("deleteditems").append(" xmlns=\"urn:schemas:httpmail:\"/>");
        builder.append("<").append("junkemail").append(" xmlns=\"urn:schemas:httpmail:\"/>");
        builder.append("</prop>");
        builder.append("</propfind>");
        return builder.toString();
    }

    private String getFolderListXml() {
        StringBuilder builder = new StringBuilder(200);
        builder.append("<?xml version='1.0' ?>");
        builder.append("<a:searchrequest xmlns:a='DAV:'><a:sql>\r\n");
        builder.append("SELECT \"DAV:uid\", \"DAV:ishidden\"\r\n");
        builder.append(" FROM SCOPE('deep traversal of \"").append(this.mUrl).append("\"')\r\n");
        builder.append(" WHERE \"DAV:ishidden\"=False AND \"DAV:isfolder\"=True\r\n");
        builder.append("</a:sql></a:searchrequest>\r\n");
        return builder.toString();
    }

    /* access modifiers changed from: package-private */
    public String getMessageCountXml(String messageState) {
        StringBuilder builder = new StringBuilder(200);
        builder.append("<?xml version='1.0' ?>");
        builder.append("<a:searchrequest xmlns:a='DAV:'><a:sql>\r\n");
        builder.append("SELECT \"DAV:visiblecount\"\r\n");
        builder.append(" FROM \"\"\r\n");
        builder.append(" WHERE \"DAV:ishidden\"=False AND \"DAV:isfolder\"=False AND \"urn:schemas:httpmail:read\"=").append(messageState).append("\r\n");
        builder.append(" GROUP BY \"DAV:ishidden\"\r\n");
        builder.append("</a:sql></a:searchrequest>\r\n");
        return builder.toString();
    }

    /* access modifiers changed from: package-private */
    public String getMessageEnvelopeXml(String[] uids) {
        StringBuilder buffer = new StringBuilder(200);
        buffer.append("<?xml version='1.0' ?>");
        buffer.append("<a:searchrequest xmlns:a='DAV:'><a:sql>\r\n");
        buffer.append("SELECT \"DAV:uid\", \"DAV:getcontentlength\",");
        buffer.append(" \"urn:schemas:mailheader:mime-version\",");
        buffer.append(" \"urn:schemas:mailheader:content-type\",");
        buffer.append(" \"urn:schemas:mailheader:subject\",");
        buffer.append(" \"urn:schemas:mailheader:date\",");
        buffer.append(" \"urn:schemas:mailheader:thread-topic\",");
        buffer.append(" \"urn:schemas:mailheader:thread-index\",");
        buffer.append(" \"urn:schemas:mailheader:from\",");
        buffer.append(" \"urn:schemas:mailheader:to\",");
        buffer.append(" \"urn:schemas:mailheader:in-reply-to\",");
        buffer.append(" \"urn:schemas:mailheader:cc\",");
        buffer.append(" \"urn:schemas:httpmail:read\"");
        buffer.append(" \r\n");
        buffer.append(" FROM \"\"\r\n");
        buffer.append(" WHERE \"DAV:ishidden\"=False AND \"DAV:isfolder\"=False AND ");
        int count = uids.length;
        for (int i = 0; i < count; i++) {
            if (i != 0) {
                buffer.append("  OR ");
            }
            buffer.append(" \"DAV:uid\"='").append(uids[i]).append("' ");
        }
        buffer.append("\r\n");
        buffer.append("</a:sql></a:searchrequest>\r\n");
        return buffer.toString();
    }

    /* access modifiers changed from: package-private */
    public String getMessagesXml() {
        StringBuilder builder = new StringBuilder(200);
        builder.append("<?xml version='1.0' ?>");
        builder.append("<a:searchrequest xmlns:a='DAV:'><a:sql>\r\n");
        builder.append("SELECT \"DAV:uid\"\r\n");
        builder.append(" FROM \"\"\r\n");
        builder.append(" WHERE \"DAV:ishidden\"=False AND \"DAV:isfolder\"=False\r\n");
        builder.append("</a:sql></a:searchrequest>\r\n");
        return builder.toString();
    }

    /* access modifiers changed from: package-private */
    public String getMessageUrlsXml(String[] uids) {
        StringBuilder buffer = new StringBuilder(600);
        buffer.append("<?xml version='1.0' ?>");
        buffer.append("<a:searchrequest xmlns:a='DAV:'><a:sql>\r\n");
        buffer.append("SELECT \"urn:schemas:httpmail:read\", \"DAV:uid\"\r\n");
        buffer.append(" FROM \"\"\r\n");
        buffer.append(" WHERE \"DAV:ishidden\"=False AND \"DAV:isfolder\"=False AND ");
        int count = uids.length;
        for (int i = 0; i < count; i++) {
            if (i != 0) {
                buffer.append("  OR ");
            }
            buffer.append(" \"DAV:uid\"='").append(uids[i]).append("' ");
        }
        buffer.append("\r\n");
        buffer.append("</a:sql></a:searchrequest>\r\n");
        return buffer.toString();
    }

    /* access modifiers changed from: package-private */
    public String getMessageFlagsXml(String[] uids) throws MessagingException {
        if (uids.length == 0) {
            throw new MessagingException("Attempt to get flags on 0 length array for uids");
        }
        StringBuilder buffer = new StringBuilder(200);
        buffer.append("<?xml version='1.0' ?>");
        buffer.append("<a:searchrequest xmlns:a='DAV:'><a:sql>\r\n");
        buffer.append("SELECT \"urn:schemas:httpmail:read\", \"DAV:uid\"\r\n");
        buffer.append(" FROM \"\"\r\n");
        buffer.append(" WHERE \"DAV:ishidden\"=False AND \"DAV:isfolder\"=False AND ");
        int count = uids.length;
        for (int i = 0; i < count; i++) {
            if (i != 0) {
                buffer.append(" OR ");
            }
            buffer.append(" \"DAV:uid\"='").append(uids[i]).append("' ");
        }
        buffer.append("\r\n");
        buffer.append("</a:sql></a:searchrequest>\r\n");
        return buffer.toString();
    }

    /* access modifiers changed from: package-private */
    public String getMarkMessagesReadXml(String[] urls, boolean read) {
        StringBuilder buffer = new StringBuilder(600);
        buffer.append("<?xml version='1.0' ?>\r\n");
        buffer.append("<a:propertyupdate xmlns:a='DAV:' xmlns:b='urn:schemas:httpmail:'>\r\n");
        buffer.append("<a:target>\r\n");
        for (String url : urls) {
            buffer.append(" <a:href>").append(url).append("</a:href>\r\n");
        }
        buffer.append("</a:target>\r\n");
        buffer.append("<a:set>\r\n");
        buffer.append(" <a:prop>\r\n");
        buffer.append("  <b:read>").append(read ? Constants.INSTALL_ID : "0").append("</b:read>\r\n");
        buffer.append(" </a:prop>\r\n");
        buffer.append("</a:set>\r\n");
        buffer.append("</a:propertyupdate>\r\n");
        return buffer.toString();
    }

    /* access modifiers changed from: package-private */
    public String getMoveOrCopyMessagesReadXml(String[] urls, boolean isMove) {
        String action = isMove ? "move" : "copy";
        StringBuilder buffer = new StringBuilder(600);
        buffer.append("<?xml version='1.0' ?>\r\n");
        buffer.append("<a:").append(action).append(" xmlns:a='DAV:' xmlns:b='urn:schemas:httpmail:'>\r\n");
        buffer.append("<a:target>\r\n");
        for (String url : urls) {
            buffer.append(" <a:href>").append(url).append("</a:href>\r\n");
        }
        buffer.append("</a:target>\r\n");
        buffer.append("</a:").append(action).append(">\r\n");
        return buffer.toString();
    }

    public boolean authenticate() throws MessagingException {
        try {
            if (this.mAuthentication == 0) {
                ConnectionInfo info = doInitialConnection();
                if (info.requiredAuthType == 1) {
                    HttpGeneric request = new HttpGeneric(this.mUrl);
                    request.setMethod("GET");
                    request.setHeader("Authorization", this.mAuthString);
                    HttpResponse response = getHttpClient().executeOverride(request, this.mContext);
                    int statusCode = response.getStatusLine().getStatusCode();
                    if (statusCode >= 200 && statusCode < 300) {
                        this.mAuthentication = 1;
                    } else if (statusCode == 401) {
                        throw new MessagingException("Invalid username or password for authentication.");
                    } else {
                        throw new MessagingException("Error with code " + response.getStatusLine().getStatusCode() + " during request processing: " + response.getStatusLine().toString());
                    }
                } else if (info.requiredAuthType == 2) {
                    doFBA(info);
                }
            } else if (this.mAuthentication != 1 && this.mAuthentication == 2) {
                doFBA(null);
            }
            if (this.mAuthentication != 0) {
                return true;
            }
            return false;
        } catch (IOException ioe) {
            Log.e("k9", "Error during authentication: " + ioe + "\nStack: " + WebDavUtils.processException(ioe));
            throw new MessagingException("Error during authentication", ioe);
        }
    }

    private ConnectionInfo doInitialConnection() throws MessagingException {
        ConnectionInfo info = new ConnectionInfo();
        WebDavHttpClient httpClient = getHttpClient();
        HttpGeneric request = new HttpGeneric(this.mUrl);
        request.setMethod("GET");
        try {
            HttpResponse response = httpClient.executeOverride(request, this.mContext);
            info.statusCode = response.getStatusLine().getStatusCode();
            if (info.statusCode == 401) {
                info.requiredAuthType = 1;
            } else if ((info.statusCode < 200 || info.statusCode >= 300) && ((info.statusCode < 300 || info.statusCode >= 400) && info.statusCode != 440)) {
                throw new IOException("Error with code " + info.statusCode + " during request processing: " + response.getStatusLine().toString());
            } else {
                info.requiredAuthType = 2;
                if (this.mAuthPath == null || this.mAuthPath.equals("")) {
                    info.guessedAuthUrl = getRoot() + "/exchweb/bin/auth/owaauth.dll";
                } else {
                    info.guessedAuthUrl = getRoot() + this.mAuthPath;
                }
                Header location = response.getFirstHeader("Location");
                if (location != null) {
                    info.redirectUrl = location.getValue();
                }
            }
            return info;
        } catch (SSLException e) {
            throw new CertificateValidationException(e.getMessage(), e);
        } catch (IOException ioe) {
            Log.e("k9", "IOException: " + ioe + "\nTrace: " + WebDavUtils.processException(ioe));
            throw new MessagingException("IOException", ioe);
        }
    }

    public void doFBA(ConnectionInfo info) throws IOException, MessagingException {
        String loginUrl;
        String loginUrl2;
        HttpGeneric request;
        String urlPath;
        if (this.mAuthCookies != null) {
            this.mAuthCookies.clear();
        }
        WebDavHttpClient httpClient = getHttpClient();
        if (info != null) {
            loginUrl = info.guessedAuthUrl;
        } else if (this.mCachedLoginUrl == null || this.mCachedLoginUrl.equals("")) {
            throw new MessagingException("No valid login URL available for form-based authentication.");
        } else {
            loginUrl = this.mCachedLoginUrl;
        }
        HttpGeneric httpGeneric = new HttpGeneric(loginUrl2);
        httpGeneric.setMethod("POST");
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("destination", this.mUrl));
        arrayList.add(new BasicNameValuePair(SettingsExporter.USERNAME_ELEMENT, this.mUsername));
        arrayList.add(new BasicNameValuePair(SettingsExporter.PASSWORD_ELEMENT, this.mPassword));
        arrayList.add(new BasicNameValuePair(EmailProvider.MessageColumns.FLAGS, "0"));
        arrayList.add(new BasicNameValuePair("SubmitCreds", "Log+On"));
        arrayList.add(new BasicNameValuePair("forcedownlevel", "0"));
        arrayList.add(new BasicNameValuePair("trusted", "0"));
        UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(arrayList);
        httpGeneric.setEntity(formEntity);
        HttpResponse response = httpClient.executeOverride(httpGeneric, this.mContext);
        boolean authenticated = testAuthenticationResponse(response);
        if (!authenticated) {
            String formAction = findFormAction(WebDavHttpClient.getUngzippedContent(response.getEntity()));
            if (formAction != null || info == null || info.redirectUrl == null || info.redirectUrl.equals("")) {
                request = httpGeneric;
            } else {
                loginUrl2 = info.redirectUrl;
                HttpGeneric httpGeneric2 = new HttpGeneric(loginUrl2);
                httpGeneric2.setMethod("GET");
                formAction = findFormAction(WebDavHttpClient.getUngzippedContent(httpClient.executeOverride(httpGeneric2, this.mContext).getEntity()));
                request = httpGeneric2;
            }
            if (formAction != null) {
                try {
                    URI formActionUri = new URI(formAction);
                    URI loginUri = new URI(loginUrl2);
                    if (formActionUri.isAbsolute()) {
                        loginUrl2 = formAction;
                    } else {
                        if (formAction.startsWith("/")) {
                            urlPath = formAction;
                        } else {
                            urlPath = loginUri.getPath();
                            int lastPathPos = urlPath.lastIndexOf(47);
                            if (lastPathPos > -1) {
                                urlPath = urlPath.substring(0, lastPathPos + 1).concat(formAction);
                            }
                        }
                        loginUrl2 = new URI(loginUri.getScheme(), loginUri.getUserInfo(), loginUri.getHost(), loginUri.getPort(), urlPath, null, null).toString();
                    }
                    HttpGeneric httpGeneric3 = new HttpGeneric(loginUrl2);
                    try {
                        httpGeneric3.setMethod("POST");
                        httpGeneric3.setEntity(formEntity);
                        authenticated = testAuthenticationResponse(httpClient.executeOverride(httpGeneric3, this.mContext));
                    } catch (URISyntaxException e) {
                        e = e;
                    }
                } catch (URISyntaxException e2) {
                    e = e2;
                    Log.e("k9", "URISyntaxException caught " + e + "\nTrace: " + WebDavUtils.processException(e));
                    throw new MessagingException("URISyntaxException caught", e);
                }
            } else {
                throw new MessagingException("A valid URL for Exchange authentication could not be found.");
            }
        }
        if (authenticated) {
            this.mAuthentication = 2;
            this.mCachedLoginUrl = loginUrl2;
            return;
        }
        throw new MessagingException("Invalid credentials provided for authentication.");
    }

    private String findFormAction(InputStream istream) throws IOException {
        int closePos;
        int quesPos;
        String formAction = null;
        BufferedReader reader = new BufferedReader(new InputStreamReader(istream), 4096);
        while (true) {
            String tempText = reader.readLine();
            if (tempText == null || formAction != null) {
                return formAction;
            }
            if (tempText.contains(" action=")) {
                String[] actionParts = tempText.split(" action=");
                if (actionParts.length > 1 && actionParts[1].length() > 1 && (closePos = actionParts[1].indexOf(actionParts[1].charAt(0), 1)) > 1 && (quesPos = (formAction = actionParts[1].substring(1, closePos)).indexOf(63)) != -1) {
                    formAction = formAction.substring(0, quesPos);
                }
            }
        }
        return formAction;
    }

    private boolean testAuthenticationResponse(HttpResponse response) throws MessagingException {
        int statusCode = response.getStatusLine().getStatusCode();
        if (((statusCode < 200 || statusCode >= 300) && statusCode != 302) || this.mAuthCookies == null || this.mAuthCookies.getCookies().isEmpty()) {
            return false;
        }
        ConnectionInfo info = doInitialConnection();
        if (info.statusCode >= 200 && info.statusCode < 300) {
            return true;
        }
        if (info.statusCode != 302) {
            return false;
        }
        try {
            String thisPath = new URI(this.mUrl).getPath();
            String redirectPath = new URI(info.redirectUrl).getPath();
            if (!thisPath.endsWith("/")) {
                thisPath = thisPath.concat("/");
            }
            if (!redirectPath.endsWith("/")) {
                redirectPath = redirectPath.concat("/");
            }
            if (redirectPath.equalsIgnoreCase(thisPath)) {
                return true;
            }
            int found = thisPath.indexOf(47, 1);
            if (found == -1 || !redirectPath.replace("/owa/", thisPath.substring(0, found + 1)).equalsIgnoreCase(thisPath)) {
                return false;
            }
            return true;
        } catch (URISyntaxException e) {
            Log.e("k9", "URISyntaxException caught " + e + "\nTrace: " + WebDavUtils.processException(e));
            throw new MessagingException("URISyntaxException caught", e);
        }
    }

    public CookieStore getAuthCookies() {
        return this.mAuthCookies;
    }

    public String getAlias() {
        return this.mAlias;
    }

    public String getUrl() {
        return this.mUrl;
    }

    public WebDavHttpClient getHttpClient() throws MessagingException {
        if (this.mHttpClient == null) {
            this.mHttpClient = this.mHttpClientFactory.create();
            this.mHttpClient.getParams().setBooleanParameter("http.protocol.handle-redirects", false);
            this.mContext = new BasicHttpContext();
            this.mAuthCookies = new BasicCookieStore();
            this.mContext.setAttribute("http.cookie-store", this.mAuthCookies);
            try {
                this.mHttpClient.getConnectionManager().getSchemeRegistry().register(new Scheme("https", new WebDavSocketFactory(this.mHost, 443), 443));
            } catch (NoSuchAlgorithmException nsa) {
                Log.e("k9", "NoSuchAlgorithmException in getHttpClient: " + nsa);
                throw new MessagingException("NoSuchAlgorithmException in getHttpClient: " + nsa);
            } catch (KeyManagementException kme) {
                Log.e("k9", "KeyManagementException in getHttpClient: " + kme);
                throw new MessagingException("KeyManagementException in getHttpClient: " + kme);
            }
        }
        return this.mHttpClient;
    }

    private InputStream sendRequest(String url, String method, StringEntity messageBody, Map<String, String> headers, boolean tryAuth) throws MessagingException {
        if (url == null || method == null) {
            return null;
        }
        WebDavHttpClient httpClient = getHttpClient();
        try {
            HttpGeneric httpMethod = new HttpGeneric(url);
            if (messageBody != null) {
                httpMethod.setEntity(messageBody);
            }
            if (headers != null) {
                for (Map.Entry<String, String> entry : headers.entrySet()) {
                    httpMethod.setHeader((String) entry.getKey(), (String) entry.getValue());
                }
            }
            if (this.mAuthentication == 0) {
                if (!tryAuth || !authenticate()) {
                    throw new MessagingException("Unable to authenticate in sendRequest().");
                }
            } else if (this.mAuthentication == 1) {
                httpMethod.setHeader("Authorization", this.mAuthString);
            }
            httpMethod.setMethod(method);
            HttpResponse response = httpClient.executeOverride(httpMethod, this.mContext);
            int statusCode = response.getStatusLine().getStatusCode();
            HttpEntity entity = response.getEntity();
            if (statusCode == 401) {
                throw new MessagingException("Invalid username or password for Basic authentication.");
            }
            if (statusCode == 440) {
                if (!tryAuth || this.mAuthentication != 2) {
                    throw new MessagingException("Authentication failure in sendRequest().");
                }
                doFBA(null);
                sendRequest(url, method, messageBody, headers, false);
            } else if (statusCode < 200 || statusCode >= 300) {
                throw new IOException("Error with code " + statusCode + " during request processing: " + response.getStatusLine().toString());
            }
            if (entity != null) {
                return WebDavHttpClient.getUngzippedContent(entity);
            }
            return null;
        } catch (UnsupportedEncodingException uee) {
            Log.e("k9", "UnsupportedEncodingException: " + uee + "\nTrace: " + WebDavUtils.processException(uee));
            throw new MessagingException("UnsupportedEncodingException", uee);
        } catch (IOException ioe) {
            Log.e("k9", "IOException: " + ioe + "\nTrace: " + WebDavUtils.processException(ioe));
            throw new MessagingException("IOException", ioe);
        }
    }

    public String getAuthString() {
        return this.mAuthString;
    }

    /* access modifiers changed from: package-private */
    public DataSet processRequest(String url, String method, String messageBody, Map<String, String> headers) throws MessagingException {
        return processRequest(url, method, messageBody, headers, true);
    }

    /* access modifiers changed from: package-private */
    public DataSet processRequest(String url, String method, String messageBody, Map<String, String> headers, boolean needsParsing) throws MessagingException {
        StringEntity messageEntity;
        DataSet dataset = new DataSet();
        if (K9MailLib.isDebug() && K9MailLib.DEBUG_PROTOCOL_WEBDAV) {
            Log.v("k9", "processRequest url = '" + url + "', method = '" + method + "', messageBody = '" + messageBody + "'");
        }
        if (url == null || method == null) {
            return dataset;
        }
        getHttpClient();
        StringEntity messageEntity2 = null;
        if (messageBody != null) {
            try {
                messageEntity = new StringEntity(messageBody);
            } catch (SAXException se) {
                Log.e("k9", "SAXException in processRequest() " + se + "\nTrace: " + WebDavUtils.processException(se));
                throw new MessagingException("SAXException in processRequest() ", se);
            } catch (ParserConfigurationException pce) {
                Log.e("k9", "ParserConfigurationException in processRequest() " + pce + "\nTrace: " + WebDavUtils.processException(pce));
                throw new MessagingException("ParserConfigurationException in processRequest() ", pce);
            } catch (UnsupportedEncodingException e) {
                uee = e;
            } catch (IOException e2) {
                ioe = e2;
                Log.e("k9", "IOException: " + ioe + "\nTrace: " + WebDavUtils.processException(ioe));
                throw new MessagingException("IOException in processRequest() ", ioe);
            }
            try {
                messageEntity.setContentType("text/xml");
                messageEntity2 = messageEntity;
            } catch (UnsupportedEncodingException e3) {
                uee = e3;
                Log.e("k9", "UnsupportedEncodingException: " + uee + "\nTrace: " + WebDavUtils.processException(uee));
                throw new MessagingException("UnsupportedEncodingException in processRequest() ", uee);
            } catch (IOException e4) {
                ioe = e4;
                Log.e("k9", "IOException: " + ioe + "\nTrace: " + WebDavUtils.processException(ioe));
                throw new MessagingException("IOException in processRequest() ", ioe);
            }
        }
        InputStream istream = sendRequest(url, method, messageEntity2, headers, true);
        if (istream != null && needsParsing) {
            SAXParserFactory spf = SAXParserFactory.newInstance();
            spf.setNamespaceAware(true);
            XMLReader xr = spf.newSAXParser().getXMLReader();
            WebDavHandler myHandler = new WebDavHandler();
            xr.setContentHandler(myHandler);
            xr.parse(new InputSource(istream));
            dataset = myHandler.getDataSet();
            istream.close();
        }
        return dataset;
    }

    public boolean isSendCapable() {
        return true;
    }

    public void sendMessages(List<? extends Message> messages) throws MessagingException {
        WebDavFolder tmpFolder = getFolder(this.mStoreConfig.getDraftsFolderName());
        try {
            tmpFolder.open(0);
            tmpFolder.moveMessages(tmpFolder.appendWebDavMessages(messages), getSendSpoolFolder());
        } finally {
            if (tmpFolder != null) {
                tmpFolder.close();
            }
        }
    }
}
