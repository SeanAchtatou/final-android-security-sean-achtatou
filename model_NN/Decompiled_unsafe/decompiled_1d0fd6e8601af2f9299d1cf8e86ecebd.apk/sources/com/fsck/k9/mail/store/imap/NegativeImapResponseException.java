package com.fsck.k9.mail.store.imap;

import com.fsck.k9.mail.MessagingException;

class NegativeImapResponseException extends MessagingException {
    private static final long serialVersionUID = 3725007182205882394L;
    private final String alertText;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fsck.k9.mail.MessagingException.<init>(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.fsck.k9.mail.MessagingException.<init>(java.lang.String, java.lang.Throwable):void
      com.fsck.k9.mail.MessagingException.<init>(java.lang.String, boolean):void */
    public NegativeImapResponseException(String message, String alertText2) {
        super(message, true);
        this.alertText = alertText2;
    }

    public String getAlertText() {
        return this.alertText;
    }
}
