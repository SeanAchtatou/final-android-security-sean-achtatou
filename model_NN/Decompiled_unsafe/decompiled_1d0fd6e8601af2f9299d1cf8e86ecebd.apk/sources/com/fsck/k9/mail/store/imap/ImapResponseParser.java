package com.fsck.k9.mail.store.imap;

import android.util.Log;
import com.fsck.k9.mail.K9MailLib;
import com.fsck.k9.mail.filter.FixedLengthInputStream;
import com.fsck.k9.mail.filter.PeekableInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class ImapResponseParser {
    private Exception exception;
    private PeekableInputStream inputStream;
    private ImapResponse response;

    public ImapResponseParser(PeekableInputStream in) {
        this.inputStream = in;
    }

    public ImapResponse readResponse() throws IOException {
        return readResponse(null);
    }

    public ImapResponse readResponse(ImapResponseCallback callback) throws IOException {
        try {
            int peek = this.inputStream.peek();
            if (peek == 43) {
                readContinuationRequest(callback);
            } else if (peek == 42) {
                readUntaggedResponse(callback);
            } else {
                readTaggedResponse(callback);
            }
            if (this.exception == null) {
                return this.response;
            }
            throw new RuntimeException("readResponse(): Exception in callback method", this.exception);
        } finally {
            this.response = null;
            this.exception = null;
        }
    }

    private void readContinuationRequest(ImapResponseCallback callback) throws IOException {
        parseCommandContinuationRequest();
        this.response = ImapResponse.newContinuationRequest(callback);
        skipIfSpace();
        this.response.add(readStringUntilEndOfLine());
    }

    private void readUntaggedResponse(ImapResponseCallback callback) throws IOException {
        parseUntaggedResponse();
        this.response = ImapResponse.newUntaggedResponse(callback);
        readTokens(this.response);
    }

    private void readTaggedResponse(ImapResponseCallback callback) throws IOException {
        this.response = ImapResponse.newTaggedResponse(callback, parseTaggedResponse());
        readTokens(this.response);
    }

    /* access modifiers changed from: package-private */
    public List<ImapResponse> readStatusResponse(String tag, String commandToLog, String logId, UntaggedHandler untaggedHandler) throws IOException, NegativeImapResponseException {
        ImapResponse response2;
        List<ImapResponse> responses = new ArrayList<>();
        while (true) {
            response2 = readResponse();
            if (K9MailLib.isDebug() && K9MailLib.DEBUG_PROTOCOL_IMAP) {
                Log.v("k9", logId + "<<<" + response2);
            }
            if (response2.getTag() == null || response2.getTag().equalsIgnoreCase(tag)) {
                if (untaggedHandler != null) {
                    untaggedHandler.handleAsyncUntaggedResponse(response2);
                }
                responses.add(response2);
            } else {
                Log.w("k9", "After sending tag " + tag + ", got tag response from previous command " + response2 + " for " + logId);
                Iterator<ImapResponse> responseIterator = responses.iterator();
                while (responseIterator.hasNext()) {
                    ImapResponse delResponse = (ImapResponse) responseIterator.next();
                    if (delResponse.getTag() != null || delResponse.size() < 2 || (!equalsIgnoreCase(delResponse.get(1), Responses.EXISTS) && !equalsIgnoreCase(delResponse.get(1), Responses.EXPUNGE))) {
                        responseIterator.remove();
                    }
                }
                response2 = null;
            }
            if (response2 != null && response2.getTag() != null) {
                break;
            }
        }
        if (response2.size() >= 1 && equalsIgnoreCase(response2.get(0), Responses.OK)) {
            return responses;
        }
        throw new NegativeImapResponseException("Command: " + commandToLog + "; response: " + response2.toString(), AlertResponse.getAlertText(response2));
    }

    private void readTokens(ImapResponse response2) throws IOException {
        response2.clear();
        Object firstToken = readToken(response2);
        checkTokenIsString(firstToken);
        String symbol = (String) firstToken;
        response2.add(symbol);
        if (isStatusResponse(symbol)) {
            parseResponseText(response2);
        } else if (equalsIgnoreCase(symbol, "LIST") || equalsIgnoreCase(symbol, Responses.LSUB)) {
            parseListResponse(response2);
        } else {
            while (true) {
                Object token = readToken(response2);
                if (token == null) {
                    return;
                }
                if (!(token instanceof ImapList)) {
                    response2.add(token);
                }
            }
        }
    }

    private void parseResponseText(ImapResponse parent) throws IOException {
        skipIfSpace();
        if (this.inputStream.peek() == 91) {
            parseList(parent, '[', ']');
            skipIfSpace();
        }
        String rest = readStringUntilEndOfLine();
        if (rest != null && !rest.isEmpty()) {
            parent.add(rest);
        }
    }

    private void parseListResponse(ImapResponse response2) throws IOException {
        expect(' ');
        parseList(response2, '(', ')');
        expect(' ');
        response2.add(parseQuoted());
        expect(' ');
        response2.add(parseString());
        expect(13);
        expect(10);
    }

    private void skipIfSpace() throws IOException {
        if (this.inputStream.peek() == 32) {
            expect(' ');
        }
    }

    private Object readToken(ImapResponse response2) throws IOException {
        Object token;
        while (true) {
            token = parseToken(response2);
            if (token == null || (!token.equals(")") && !token.equals("]"))) {
                return token;
            }
        }
        return token;
    }

    private Object parseToken(ImapList parent) throws IOException {
        while (true) {
            int ch = this.inputStream.peek();
            if (ch == 40) {
                return parseList(parent, '(', ')');
            }
            if (ch == 91) {
                return parseList(parent, '[', ']');
            }
            if (ch == 41) {
                expect(')');
                return ")";
            } else if (ch == 93) {
                expect(']');
                return "]";
            } else if (ch == 34) {
                return parseQuoted();
            } else {
                if (ch == 123) {
                    return parseLiteral();
                }
                if (ch == 32) {
                    expect(' ');
                } else if (ch == 13) {
                    expect(13);
                    expect(10);
                    return null;
                } else if (ch == 10) {
                    expect(10);
                    return null;
                } else if (ch != 9) {
                    return parseBareString(true);
                } else {
                    expect(9);
                }
            }
        }
    }

    private String parseString() throws IOException {
        int ch = this.inputStream.peek();
        if (ch == 34) {
            return parseQuoted();
        }
        if (ch == 123) {
            return (String) parseLiteral();
        }
        return parseBareString(false);
    }

    private boolean parseCommandContinuationRequest() throws IOException {
        expect('+');
        return true;
    }

    private void parseUntaggedResponse() throws IOException {
        expect('*');
        expect(' ');
    }

    private String parseTaggedResponse() throws IOException {
        return readStringUntil(' ');
    }

    private ImapList parseList(ImapList parent, char start, char end) throws IOException {
        expect(start);
        ImapList list = new ImapList();
        parent.add(list);
        String endString = String.valueOf(end);
        while (true) {
            Object token = parseToken(list);
            if (token == null) {
                return null;
            }
            if (token.equals(endString)) {
                return list;
            }
            if (!(token instanceof ImapList)) {
                list.add(token);
            }
        }
    }

    private String parseBareString(boolean allowBrackets) throws IOException {
        int ch;
        StringBuilder sb = new StringBuilder();
        while (true) {
            ch = this.inputStream.peek();
            if (ch == -1) {
                throw new IOException("parseBareString(): end of stream reached");
            } else if (ch != 40 && ch != 41 && ((!allowBrackets || (ch != 91 && ch != 93)) && ch != 123 && ch != 32 && ch != 34 && ((ch < 0 || ch > 31) && ch != 127))) {
                sb.append((char) this.inputStream.read());
            }
        }
        if (sb.length() != 0) {
            return sb.toString();
        }
        throw new IOException(String.format("parseBareString(): (%04x %c)", Integer.valueOf(ch), Integer.valueOf(ch)));
    }

    private Object parseLiteral() throws IOException {
        expect('{');
        int size = Integer.parseInt(readStringUntil('}'));
        expect(13);
        expect(10);
        if (size == 0) {
            return "";
        }
        if (this.response.getCallback() != null) {
            FixedLengthInputStream fixed = new FixedLengthInputStream(this.inputStream, size);
            Object result = null;
            try {
                result = this.response.getCallback().foundLiteral(this.response, fixed);
            } catch (IOException e) {
                throw e;
            } catch (Exception e2) {
                this.exception = e2;
            }
            int available = fixed.available();
            if (available > 0 && available != size) {
                while (fixed.available() > 0) {
                    fixed.skip((long) fixed.available());
                }
            }
            if (result != null) {
                return result;
            }
        }
        byte[] data = new byte[size];
        int read = 0;
        while (read != size) {
            int count = this.inputStream.read(data, read, size - read);
            if (count == -1) {
                throw new IOException("parseLiteral(): end of stream reached");
            }
            read += count;
        }
        return new String(data, "US-ASCII");
    }

    private String parseQuoted() throws IOException {
        expect('\"');
        StringBuilder sb = new StringBuilder();
        boolean escape = false;
        while (true) {
            int ch = this.inputStream.read();
            if (ch == -1) {
                throw new IOException("parseQuoted(): end of stream reached");
            } else if (!escape && ch == 92) {
                escape = true;
            } else if (!escape && ch == 34) {
                return sb.toString();
            } else {
                sb.append((char) ch);
                escape = false;
            }
        }
    }

    private String readStringUntil(char end) throws IOException {
        StringBuilder sb = new StringBuilder();
        while (true) {
            int ch = this.inputStream.read();
            if (ch == -1) {
                throw new IOException("readStringUntil(): end of stream reached");
            } else if (ch == end) {
                return sb.toString();
            } else {
                sb.append((char) ch);
            }
        }
    }

    private String readStringUntilEndOfLine() throws IOException {
        String rest = readStringUntil(13);
        expect(10);
        return rest;
    }

    private void expect(char expected) throws IOException {
        int readByte = this.inputStream.read();
        if (readByte != expected) {
            throw new IOException(String.format("Expected %04x (%c) but got %04x (%c)", Integer.valueOf(expected), Character.valueOf(expected), Integer.valueOf(readByte), Character.valueOf((char) readByte)));
        }
    }

    private boolean isStatusResponse(String symbol) {
        return symbol.equalsIgnoreCase(Responses.OK) || symbol.equalsIgnoreCase(Responses.NO) || symbol.equalsIgnoreCase(Responses.BAD) || symbol.equalsIgnoreCase(Responses.PREAUTH) || symbol.equalsIgnoreCase(Responses.BYE);
    }

    static boolean equalsIgnoreCase(Object token, String symbol) {
        if (token == null || !(token instanceof String)) {
            return false;
        }
        return symbol.equalsIgnoreCase((String) token);
    }

    private void checkTokenIsString(Object token) throws IOException {
        if (!(token instanceof String)) {
            throw new IOException("Unexpected non-string token: " + token);
        }
    }
}
