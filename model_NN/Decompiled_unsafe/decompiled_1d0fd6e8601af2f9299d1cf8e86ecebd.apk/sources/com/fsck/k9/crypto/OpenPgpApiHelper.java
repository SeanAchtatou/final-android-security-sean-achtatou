package com.fsck.k9.crypto;

import android.text.TextUtils;
import com.fsck.k9.Account;
import com.fsck.k9.Identity;

public class OpenPgpApiHelper {
    public static String buildUserId(Identity identity) {
        StringBuilder sb = new StringBuilder();
        String name = identity.getName();
        if (!TextUtils.isEmpty(name)) {
            sb.append(name).append(" ");
        }
        sb.append("<").append(identity.getEmail()).append(Account.DEFAULT_QUOTE_PREFIX);
        return sb.toString();
    }
}
