package com.fsck.k9;

import android.content.Context;
import com.fsck.k9.mail.oauth.OAuth2TokenProvider;

public class Globals {
    private static Context context;
    private static OAuth2TokenProvider oAuth2TokenProvider;

    static void setContext(Context context2) {
        context = context2;
    }

    static void setOAuth2TokenProvider(OAuth2TokenProvider oAuth2TokenProvider2) {
        oAuth2TokenProvider = oAuth2TokenProvider2;
    }

    public static Context getContext() {
        if (context != null) {
            return context;
        }
        throw new IllegalStateException("No context provided");
    }

    public static OAuth2TokenProvider getOAuth2TokenProvider() {
        if (oAuth2TokenProvider != null) {
            return oAuth2TokenProvider;
        }
        throw new IllegalStateException("No OAuth 2.0 Token Provider provided");
    }
}
