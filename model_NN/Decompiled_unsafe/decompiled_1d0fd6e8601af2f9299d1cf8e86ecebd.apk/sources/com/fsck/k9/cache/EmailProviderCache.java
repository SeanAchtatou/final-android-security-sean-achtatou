package com.fsck.k9.cache;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.LocalBroadcastManager;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mailstore.LocalMessage;
import com.fsck.k9.provider.EmailProvider;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EmailProviderCache {
    public static final String ACTION_CACHE_UPDATED = "EmailProviderCache.ACTION_CACHE_UPDATED";
    private static Context sContext;
    private static Map<String, EmailProviderCache> sInstances = new HashMap();
    private String mAccountUuid;
    private final Map<Long, Long> mHiddenMessageCache = new HashMap();
    private final Map<Long, Map<String, String>> mMessageCache = new HashMap();
    private final Map<Long, Map<String, String>> mThreadCache = new HashMap();

    public static synchronized EmailProviderCache getCache(String accountUuid, Context context) {
        EmailProviderCache instance;
        synchronized (EmailProviderCache.class) {
            if (sContext == null) {
                sContext = context.getApplicationContext();
            }
            instance = sInstances.get(accountUuid);
            if (instance == null) {
                instance = new EmailProviderCache(accountUuid);
                sInstances.put(accountUuid, instance);
            }
        }
        return instance;
    }

    private EmailProviderCache(String accountUuid) {
        this.mAccountUuid = accountUuid;
    }

    public String getValueForMessage(Long messageId, String columnName) {
        String str;
        synchronized (this.mMessageCache) {
            Map<String, String> map = this.mMessageCache.get(messageId);
            str = map == null ? null : (String) map.get(columnName);
        }
        return str;
    }

    public String getValueForThread(Long threadRootId, String columnName) {
        String str;
        synchronized (this.mThreadCache) {
            Map<String, String> map = this.mThreadCache.get(threadRootId);
            str = map == null ? null : (String) map.get(columnName);
        }
        return str;
    }

    public void setValueForMessages(List<Long> messageIds, String columnName, String value) {
        synchronized (this.mMessageCache) {
            for (Long messageId : messageIds) {
                Map<String, String> map = this.mMessageCache.get(messageId);
                if (map == null) {
                    map = new HashMap<>();
                    this.mMessageCache.put(messageId, map);
                }
                map.put(columnName, value);
            }
        }
        notifyChange();
    }

    public void setValueForThreads(List<Long> threadRootIds, String columnName, String value) {
        synchronized (this.mThreadCache) {
            for (Long threadRootId : threadRootIds) {
                Map<String, String> map = this.mThreadCache.get(threadRootId);
                if (map == null) {
                    map = new HashMap<>();
                    this.mThreadCache.put(threadRootId, map);
                }
                map.put(columnName, value);
            }
        }
        notifyChange();
    }

    public void removeValueForMessages(List<Long> messageIds, String columnName) {
        synchronized (this.mMessageCache) {
            for (Long messageId : messageIds) {
                Map<String, String> map = this.mMessageCache.get(messageId);
                if (map != null) {
                    map.remove(columnName);
                    if (map.isEmpty()) {
                        this.mMessageCache.remove(messageId);
                    }
                }
            }
        }
    }

    public void removeValueForThreads(List<Long> threadRootIds, String columnName) {
        synchronized (this.mThreadCache) {
            for (Long threadRootId : threadRootIds) {
                Map<String, String> map = this.mThreadCache.get(threadRootId);
                if (map != null) {
                    map.remove(columnName);
                    if (map.isEmpty()) {
                        this.mThreadCache.remove(threadRootId);
                    }
                }
            }
        }
    }

    public void hideMessages(List<LocalMessage> messages) {
        synchronized (this.mHiddenMessageCache) {
            for (LocalMessage message : messages) {
                this.mHiddenMessageCache.put(Long.valueOf(message.getId()), Long.valueOf(message.getFolder().getId()));
            }
        }
        notifyChange();
    }

    public boolean isMessageHidden(Long messageId, long folderId) {
        boolean z;
        synchronized (this.mHiddenMessageCache) {
            Long hiddenInFolder = this.mHiddenMessageCache.get(messageId);
            z = hiddenInFolder != null && hiddenInFolder.longValue() == folderId;
        }
        return z;
    }

    public void unhideMessages(List<? extends Message> messages) {
        synchronized (this.mHiddenMessageCache) {
            for (Message message : messages) {
                LocalMessage localMessage = (LocalMessage) message;
                long messageId = localMessage.getId();
                long folderId = localMessage.getFolder().getId();
                Long hiddenInFolder = this.mHiddenMessageCache.get(Long.valueOf(messageId));
                if (hiddenInFolder != null && hiddenInFolder.longValue() == folderId) {
                    this.mHiddenMessageCache.remove(Long.valueOf(messageId));
                }
            }
        }
    }

    private void notifyChange() {
        LocalBroadcastManager.getInstance(sContext).sendBroadcast(new Intent(ACTION_CACHE_UPDATED));
        sContext.getContentResolver().notifyChange(Uri.withAppendedPath(EmailProvider.CONTENT_URI, "account/" + this.mAccountUuid + "/messages"), null);
    }
}
