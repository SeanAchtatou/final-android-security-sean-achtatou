package com.fsck.k9.cache;

import android.content.Context;
import android.util.Log;
import com.fsck.k9.K9;
import com.fsck.k9.helper.FileHelper;
import java.io.File;
import java.io.IOException;

public class TemporaryAttachmentStore {
    private static long MAX_FILE_AGE = 43200000;
    private static String TEMPORARY_ATTACHMENT_DIRECTORY = "attachments";

    public static File getFile(Context context, String attachmentName) {
        return new File(getTemporaryAttachmentDirectory(context), FileHelper.sanitizeFilename(attachmentName));
    }

    public static File getFileForWriting(Context context, String attachmentName) throws IOException {
        return new File(createOrCleanAttachmentDirectory(context), FileHelper.sanitizeFilename(attachmentName));
    }

    private static File createOrCleanAttachmentDirectory(Context context) throws IOException {
        File directory = getTemporaryAttachmentDirectory(context);
        if (directory.exists()) {
            cleanOldFiles(directory);
        } else if (!directory.mkdir()) {
            throw new IOException("Couldn't create temporary attachment store: " + directory.getAbsolutePath());
        }
        return directory;
    }

    private static File getTemporaryAttachmentDirectory(Context context) {
        return new File(context.getExternalCacheDir(), TEMPORARY_ATTACHMENT_DIRECTORY);
    }

    private static void cleanOldFiles(File directory) {
        File[] files = directory.listFiles();
        if (files != null) {
            long cutOffTime = System.currentTimeMillis() - MAX_FILE_AGE;
            for (File file : files) {
                if (file.lastModified() < cutOffTime) {
                    if (!file.delete()) {
                        Log.w("k9", "Couldn't delete from temporary attachment store: " + file.getName());
                    } else if (K9.DEBUG) {
                        Log.d("k9", "Deleted from temporary attachment store: " + file.getName());
                    }
                }
            }
        }
    }
}
