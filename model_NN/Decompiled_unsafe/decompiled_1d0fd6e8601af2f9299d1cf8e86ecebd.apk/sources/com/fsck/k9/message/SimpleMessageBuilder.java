package com.fsck.k9.message;

import android.content.Context;
import android.content.Intent;
import com.fsck.k9.Globals;
import com.fsck.k9.mail.BoundaryGenerator;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.internet.MessageIdGenerator;

public class SimpleMessageBuilder extends MessageBuilder {
    public static SimpleMessageBuilder newInstance() {
        return new SimpleMessageBuilder(Globals.getContext(), MessageIdGenerator.getInstance(), BoundaryGenerator.getInstance());
    }

    SimpleMessageBuilder(Context context, MessageIdGenerator messageIdGenerator, BoundaryGenerator boundaryGenerator) {
        super(context, messageIdGenerator, boundaryGenerator);
    }

    /* access modifiers changed from: protected */
    public void buildMessageInternal() {
        try {
            queueMessageBuildSuccess(build());
        } catch (MessagingException me2) {
            queueMessageBuildException(me2);
        }
    }

    /* access modifiers changed from: protected */
    public void buildMessageOnActivityResult(int requestCode, Intent data) {
        throw new UnsupportedOperationException();
    }
}
