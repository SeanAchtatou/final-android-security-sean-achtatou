package com.fsck.k9.message.extractors;

import android.support.annotation.NonNull;

public class PreviewResult {
    private final String previewText;
    private final PreviewType previewType;

    public enum PreviewType {
        NONE,
        TEXT,
        ENCRYPTED,
        ERROR
    }

    private PreviewResult(PreviewType previewType2, String previewText2) {
        this.previewType = previewType2;
        this.previewText = previewText2;
    }

    public static PreviewResult text(@NonNull String previewText2) {
        return new PreviewResult(PreviewType.TEXT, previewText2);
    }

    public static PreviewResult encrypted() {
        return new PreviewResult(PreviewType.ENCRYPTED, null);
    }

    public static PreviewResult none() {
        return new PreviewResult(PreviewType.NONE, null);
    }

    public static PreviewResult error() {
        return new PreviewResult(PreviewType.ERROR, null);
    }

    public PreviewType getPreviewType() {
        return this.previewType;
    }

    public boolean isPreviewTextAvailable() {
        return this.previewType == PreviewType.TEXT;
    }

    public String getPreviewText() {
        if (isPreviewTextAvailable()) {
            return this.previewText;
        }
        throw new IllegalStateException("Preview is not available");
    }
}
