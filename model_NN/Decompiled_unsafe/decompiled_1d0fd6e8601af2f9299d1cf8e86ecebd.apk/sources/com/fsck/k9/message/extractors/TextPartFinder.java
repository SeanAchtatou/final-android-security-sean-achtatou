package com.fsck.k9.message.extractors;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.fsck.k9.mail.Body;
import com.fsck.k9.mail.BodyPart;
import com.fsck.k9.mail.Multipart;
import com.fsck.k9.mail.Part;
import com.fsck.k9.mail.internet.MimeUtility;
import org.apache.james.mime4j.dom.field.ContentTypeField;

class TextPartFinder {
    TextPartFinder() {
    }

    @Nullable
    public Part findFirstTextPart(@NonNull Part part) {
        String mimeType = part.getMimeType();
        Body body = part.getBody();
        if (body instanceof Multipart) {
            Multipart multipart = (Multipart) body;
            if (MimeUtility.isSameMimeType(mimeType, "multipart/alternative")) {
                return findTextPartInMultipartAlternative(multipart);
            }
            return findTextPartInMultipart(multipart);
        } else if (MimeUtility.isSameMimeType(mimeType, ContentTypeField.TYPE_TEXT_PLAIN) || MimeUtility.isSameMimeType(mimeType, "text/html")) {
            return part;
        } else {
            return null;
        }
    }

    private Part findTextPartInMultipartAlternative(Multipart multipart) {
        Part htmlPart = null;
        for (BodyPart bodyPart : multipart.getBodyParts()) {
            String mimeType = bodyPart.getMimeType();
            if (bodyPart.getBody() instanceof Multipart) {
                Part candidatePart = findFirstTextPart(bodyPart);
                if (candidatePart == null) {
                    continue;
                } else if (!MimeUtility.isSameMimeType(candidatePart.getMimeType(), "text/html")) {
                    return candidatePart;
                } else {
                    htmlPart = candidatePart;
                }
            } else if (MimeUtility.isSameMimeType(mimeType, ContentTypeField.TYPE_TEXT_PLAIN)) {
                return bodyPart;
            } else {
                if (MimeUtility.isSameMimeType(mimeType, "text/html") && htmlPart == null) {
                    htmlPart = bodyPart;
                }
            }
        }
        if (htmlPart != null) {
            return htmlPart;
        }
        return null;
    }

    private Part findTextPartInMultipart(Multipart multipart) {
        for (BodyPart bodyPart : multipart.getBodyParts()) {
            String mimeType = bodyPart.getMimeType();
            if (bodyPart.getBody() instanceof Multipart) {
                Part candidatePart = findFirstTextPart(bodyPart);
                if (candidatePart != null) {
                    return candidatePart;
                }
            } else if (MimeUtility.isSameMimeType(mimeType, ContentTypeField.TYPE_TEXT_PLAIN) || MimeUtility.isSameMimeType(mimeType, "text/html")) {
                return bodyPart;
            }
        }
        return null;
    }
}
