package com.fsck.k9.message.extractors;

import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.Part;
import com.fsck.k9.mail.internet.MessageExtractor;
import java.util.ArrayList;
import java.util.List;

public class AttachmentCounter {
    private final EncryptionDetector encryptionDetector;

    AttachmentCounter(EncryptionDetector encryptionDetector2) {
        this.encryptionDetector = encryptionDetector2;
    }

    public static AttachmentCounter newInstance() {
        return new AttachmentCounter(new EncryptionDetector(new TextPartFinder()));
    }

    public int getAttachmentCount(Message message) throws MessagingException {
        if (this.encryptionDetector.isEncrypted(message)) {
            return 0;
        }
        List<Part> attachmentParts = new ArrayList<>();
        MessageExtractor.findViewablesAndAttachments(message, null, attachmentParts);
        return attachmentParts.size();
    }
}
