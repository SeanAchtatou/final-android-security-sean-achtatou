package com.fsck.k9.message.extractors;

import android.support.annotation.NonNull;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.Part;

public class MessagePreviewCreator {
    private final EncryptionDetector encryptionDetector;
    private final PreviewTextExtractor previewTextExtractor;
    private final TextPartFinder textPartFinder;

    MessagePreviewCreator(TextPartFinder textPartFinder2, PreviewTextExtractor previewTextExtractor2, EncryptionDetector encryptionDetector2) {
        this.textPartFinder = textPartFinder2;
        this.previewTextExtractor = previewTextExtractor2;
        this.encryptionDetector = encryptionDetector2;
    }

    public static MessagePreviewCreator newInstance() {
        TextPartFinder textPartFinder2 = new TextPartFinder();
        return new MessagePreviewCreator(textPartFinder2, new PreviewTextExtractor(), new EncryptionDetector(textPartFinder2));
    }

    public PreviewResult createPreview(@NonNull Message message) {
        if (this.encryptionDetector.isEncrypted(message)) {
            return PreviewResult.encrypted();
        }
        return extractText(message);
    }

    private PreviewResult extractText(Message message) {
        Part textPart = this.textPartFinder.findFirstTextPart(message);
        if (textPart == null || hasEmptyBody(textPart)) {
            return PreviewResult.none();
        }
        try {
            return PreviewResult.text(this.previewTextExtractor.extractPreview(textPart));
        } catch (PreviewExtractionException e) {
            return PreviewResult.error();
        }
    }

    private boolean hasEmptyBody(Part textPart) {
        return textPart.getBody() == null;
    }
}
