package com.fsck.k9.message.extractors;

import android.support.annotation.NonNull;
import com.fsck.k9.crypto.MessageDecryptVerifier;
import com.fsck.k9.mail.Body;
import com.fsck.k9.mail.BodyPart;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.Multipart;
import com.fsck.k9.mail.Part;
import com.fsck.k9.mail.internet.MimeUtility;

class EncryptionDetector {
    private final TextPartFinder textPartFinder;

    EncryptionDetector(TextPartFinder textPartFinder2) {
        this.textPartFinder = textPartFinder2;
    }

    public boolean isEncrypted(@NonNull Message message) {
        return isPgpMimeOrSMimeEncrypted(message) || containsInlinePgpEncryptedText(message);
    }

    private boolean isPgpMimeOrSMimeEncrypted(Message message) {
        return containsPartWithMimeType(message, "multipart/encrypted", "application/pkcs7-mime");
    }

    private boolean containsInlinePgpEncryptedText(Message message) {
        return MessageDecryptVerifier.isPartPgpInlineEncrypted(this.textPartFinder.findFirstTextPart(message));
    }

    private boolean containsPartWithMimeType(Part part, String... wantedMimeTypes) {
        if (isMimeTypeAnyOf(part.getMimeType(), wantedMimeTypes)) {
            return true;
        }
        Body body = part.getBody();
        if (body instanceof Multipart) {
            for (BodyPart bodyPart : ((Multipart) body).getBodyParts()) {
                if (containsPartWithMimeType(bodyPart, wantedMimeTypes)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isMimeTypeAnyOf(String mimeType, String... wantedMimeTypes) {
        for (String wantedMimeType : wantedMimeTypes) {
            if (MimeUtility.isSameMimeType(mimeType, wantedMimeType)) {
                return true;
            }
        }
        return false;
    }
}
