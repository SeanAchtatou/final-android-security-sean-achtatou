package com.fsck.k9.message.extractors;

import android.support.annotation.NonNull;
import com.fsck.k9.helper.HtmlConverter;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.Part;
import com.fsck.k9.mail.internet.MessageExtractor;
import com.fsck.k9.mail.internet.MimeUtility;

public class MessageFulltextCreator {
    private static final int MAX_CHARACTERS_CHECKED_FOR_FTS = 204800;
    private final EncryptionDetector encryptionDetector;
    private final TextPartFinder textPartFinder;

    MessageFulltextCreator(TextPartFinder textPartFinder2, EncryptionDetector encryptionDetector2) {
        this.textPartFinder = textPartFinder2;
        this.encryptionDetector = encryptionDetector2;
    }

    public static MessageFulltextCreator newInstance() {
        TextPartFinder textPartFinder2 = new TextPartFinder();
        return new MessageFulltextCreator(textPartFinder2, new EncryptionDetector(textPartFinder2));
    }

    public String createFulltext(@NonNull Message message) {
        if (this.encryptionDetector.isEncrypted(message)) {
            return null;
        }
        return extractText(message);
    }

    private String extractText(Message message) {
        Part textPart = this.textPartFinder.findFirstTextPart(message);
        if (textPart == null || hasEmptyBody(textPart)) {
            return null;
        }
        String text = MessageExtractor.getTextFromPart(textPart, 204800);
        return MimeUtility.isSameMimeType(textPart.getMimeType(), "text/html") ? HtmlConverter.htmlToText(text) : text;
    }

    private boolean hasEmptyBody(Part textPart) {
        return textPart.getBody() == null;
    }
}
