package com.fsck.k9.message;

import android.net.Uri;
import android.util.Log;
import com.fsck.k9.Account;
import com.fsck.k9.Identity;
import com.fsck.k9.K9;
import com.fsck.k9.activity.MessageReference;
import com.fsck.k9.mail.internet.TextBody;

public class IdentityHeaderBuilder {
    private TextBody body;
    private TextBody bodyPlain;
    private int cursorPosition;
    private Identity identity;
    private boolean identityChanged;
    private SimpleMessageFormat messageFormat;
    private MessageReference messageReference;
    private Account.QuoteStyle quoteStyle;
    private InsertableHtmlContent quotedHtmlContent;
    private QuotedTextMode quotedTextMode;
    private String signature;
    private boolean signatureChanged;
    private Uri.Builder uri;

    public String build() {
        this.uri = new Uri.Builder();
        if (this.body.getComposedMessageLength() == null || this.body.getComposedMessageOffset() == null) {
            appendValue(IdentityField.LENGTH, this.body.getRawText().length());
            appendValue(IdentityField.OFFSET, 0);
        } else {
            appendValue(IdentityField.LENGTH, this.body.getComposedMessageLength());
            appendValue(IdentityField.OFFSET, this.body.getComposedMessageOffset());
        }
        if (this.quotedHtmlContent != null) {
            appendValue(IdentityField.FOOTER_OFFSET, this.quotedHtmlContent.getFooterInsertionPoint());
        }
        if (this.bodyPlain != null) {
            Integer composedMessageLength = this.bodyPlain.getComposedMessageLength();
            Integer composedMessageOffset = this.bodyPlain.getComposedMessageOffset();
            if (composedMessageLength == null || composedMessageOffset == null) {
                appendValue(IdentityField.PLAIN_LENGTH, this.body.getRawText().length());
                appendValue(IdentityField.PLAIN_OFFSET, 0);
            } else {
                appendValue(IdentityField.PLAIN_LENGTH, composedMessageLength);
                appendValue(IdentityField.PLAIN_OFFSET, composedMessageOffset);
            }
        }
        appendValue(IdentityField.QUOTE_STYLE, this.quoteStyle);
        appendValue(IdentityField.MESSAGE_FORMAT, this.messageFormat);
        if (this.identity.getSignatureUse() && this.signatureChanged) {
            appendValue(IdentityField.SIGNATURE, this.signature);
        }
        if (this.identityChanged) {
            appendValue(IdentityField.NAME, this.identity.getName());
            appendValue(IdentityField.EMAIL, this.identity.getEmail());
        }
        if (this.messageReference != null) {
            appendValue(IdentityField.ORIGINAL_MESSAGE, this.messageReference.toIdentityString());
        }
        appendValue(IdentityField.CURSOR_POSITION, this.cursorPosition);
        appendValue(IdentityField.QUOTED_TEXT_MODE, this.quotedTextMode);
        String k9identity = "!" + this.uri.build().getEncodedQuery();
        if (K9.DEBUG) {
            Log.d("k9", "Generated identity: " + k9identity);
        }
        return k9identity;
    }

    private void appendValue(IdentityField field, int value) {
        appendValue(field, Integer.toString(value));
    }

    private void appendValue(IdentityField field, Integer value) {
        appendValue(field, value.toString());
    }

    private void appendValue(IdentityField field, Enum<?> value) {
        appendValue(field, value.name());
    }

    private void appendValue(IdentityField field, String value) {
        this.uri.appendQueryParameter(field.value(), value);
    }

    public IdentityHeaderBuilder setQuotedHtmlContent(InsertableHtmlContent quotedHtmlContent2) {
        this.quotedHtmlContent = quotedHtmlContent2;
        return this;
    }

    public IdentityHeaderBuilder setQuoteStyle(Account.QuoteStyle quoteStyle2) {
        this.quoteStyle = quoteStyle2;
        return this;
    }

    public IdentityHeaderBuilder setQuoteTextMode(QuotedTextMode quotedTextMode2) {
        this.quotedTextMode = quotedTextMode2;
        return this;
    }

    public IdentityHeaderBuilder setMessageFormat(SimpleMessageFormat messageFormat2) {
        this.messageFormat = messageFormat2;
        return this;
    }

    public IdentityHeaderBuilder setIdentity(Identity identity2) {
        this.identity = identity2;
        return this;
    }

    public IdentityHeaderBuilder setIdentityChanged(boolean identityChanged2) {
        this.identityChanged = identityChanged2;
        return this;
    }

    public IdentityHeaderBuilder setSignature(String signature2) {
        this.signature = signature2;
        return this;
    }

    public IdentityHeaderBuilder setSignatureChanged(boolean signatureChanged2) {
        this.signatureChanged = signatureChanged2;
        return this;
    }

    public IdentityHeaderBuilder setMessageReference(MessageReference messageReference2) {
        this.messageReference = messageReference2;
        return this;
    }

    public IdentityHeaderBuilder setBody(TextBody body2) {
        this.body = body2;
        return this;
    }

    public IdentityHeaderBuilder setBodyPlain(TextBody bodyPlain2) {
        this.bodyPlain = bodyPlain2;
        return this;
    }

    public IdentityHeaderBuilder setCursorPosition(int cursorPosition2) {
        this.cursorPosition = cursorPosition2;
        return this;
    }
}
