package com.fsck.k9.message;

import com.fsck.k9.crypto.MessageDecryptVerifier;
import com.fsck.k9.mail.Message;

public class ComposePgpInlineDecider {
    public boolean shouldReplyInline(Message localMessage) {
        return messageHasPgpInlineParts(localMessage);
    }

    private boolean messageHasPgpInlineParts(Message localMessage) {
        return !MessageDecryptVerifier.findPgpInlineParts(localMessage).isEmpty();
    }
}
