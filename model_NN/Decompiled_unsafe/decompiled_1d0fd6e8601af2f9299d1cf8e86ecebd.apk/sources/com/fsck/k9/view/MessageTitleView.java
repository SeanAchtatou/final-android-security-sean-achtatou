package com.fsck.k9.view;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.TextView;

public class MessageTitleView extends TextView {
    private static final String ELLIPSIS = "…";
    private static final int MAX_LINES = 2;
    private MessageHeader mHeader;
    private boolean mNeedEllipsizeCheck;

    public MessageTitleView(Context context) {
        this(context, null);
    }

    public MessageTitleView(Context context, AttributeSet attrs) {
        this(context, attrs, 16842884);
    }

    public MessageTitleView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mNeedEllipsizeCheck = true;
    }

    /* access modifiers changed from: protected */
    public void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {
        super.onTextChanged(text, start, lengthBefore, lengthAfter);
        this.mNeedEllipsizeCheck = true;
    }

    public void onDraw(Canvas canvas) {
        if (!(!this.mNeedEllipsizeCheck || getLayout() == null || this.mHeader == null)) {
            if (getLayout().getLineCount() > 2) {
                setText(((Object) getText().subSequence(0, getLayout().getLineEnd(1) - 2)) + ELLIPSIS);
                this.mHeader.showSubjectLine();
            }
            this.mNeedEllipsizeCheck = false;
        }
        super.onDraw(canvas);
    }

    public void setMessageHeader(MessageHeader header) {
        this.mHeader = header;
        if (this.mHeader != null) {
            this.mHeader.showSubjectLine();
        }
    }
}
