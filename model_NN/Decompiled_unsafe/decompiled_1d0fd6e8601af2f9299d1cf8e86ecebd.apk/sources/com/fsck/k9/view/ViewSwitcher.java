package com.fsck.k9.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.widget.ViewAnimator;
import com.fsck.k9.K9;

public class ViewSwitcher extends ViewAnimator implements Animation.AnimationListener {
    private Animation mFirstInAnimation;
    private Animation mFirstOutAnimation;
    private OnSwitchCompleteListener mListener;
    private Animation mSecondInAnimation;
    private Animation mSecondOutAnimation;

    public interface OnSwitchCompleteListener {
        void onSwitchComplete(int i);
    }

    public ViewSwitcher(Context context) {
        super(context);
    }

    public ViewSwitcher(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void showFirstView() {
        if (getDisplayedChild() != 0) {
            setupAnimations(this.mFirstInAnimation, this.mFirstOutAnimation);
            setDisplayedChild(0);
            handleSwitchCompleteCallback();
        }
    }

    public void showSecondView() {
        if (getDisplayedChild() != 1) {
            setupAnimations(this.mSecondInAnimation, this.mSecondOutAnimation);
            setDisplayedChild(1);
            handleSwitchCompleteCallback();
        }
    }

    private void setupAnimations(Animation in, Animation out) {
        if (K9.showAnimations()) {
            setInAnimation(in);
            setOutAnimation(out);
            out.setAnimationListener(this);
            return;
        }
        setInAnimation(null);
        setOutAnimation(null);
    }

    private void handleSwitchCompleteCallback() {
        if (!K9.showAnimations()) {
            onAnimationEnd(null);
        }
    }

    public Animation getFirstInAnimation() {
        return this.mFirstInAnimation;
    }

    public void setFirstInAnimation(Animation inAnimation) {
        this.mFirstInAnimation = inAnimation;
    }

    public Animation getmFirstOutAnimation() {
        return this.mFirstOutAnimation;
    }

    public void setFirstOutAnimation(Animation outAnimation) {
        this.mFirstOutAnimation = outAnimation;
    }

    public Animation getSecondInAnimation() {
        return this.mSecondInAnimation;
    }

    public void setSecondInAnimation(Animation inAnimation) {
        this.mSecondInAnimation = inAnimation;
    }

    public Animation getSecondOutAnimation() {
        return this.mSecondOutAnimation;
    }

    public void setSecondOutAnimation(Animation outAnimation) {
        this.mSecondOutAnimation = outAnimation;
    }

    public void setOnSwitchCompleteListener(OnSwitchCompleteListener listener) {
        this.mListener = listener;
    }

    public void onAnimationEnd(Animation animation) {
        if (this.mListener != null) {
            this.mListener.onSwitchComplete(getDisplayedChild());
        }
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }
}
