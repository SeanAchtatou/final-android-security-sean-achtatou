package com.fsck.k9.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ViewAnimator;
import com.fsck.k9.R;

public class LinearViewAnimator extends ViewAnimator {
    private Animation downInAnimation;
    private Animation downOutAnimation;
    private Animation upInAnimation;
    private Animation upOutAnimation;

    public LinearViewAnimator(Context context) {
        super(context);
    }

    public LinearViewAnimator(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public LinearViewAnimator(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.LinearViewAnimator);
        int resource = a.getResourceId(0, 0);
        if (resource > 0) {
            setDownInAnimation(context, resource);
        }
        int resource2 = a.getResourceId(1, 0);
        if (resource2 > 0) {
            setDownOutAnimation(context, resource2);
        }
        int resource3 = a.getResourceId(2, 0);
        if (resource3 > 0) {
            setUpInAnimation(context, resource3);
        }
        int resource4 = a.getResourceId(3, 0);
        if (resource4 > 0) {
            setUpOutAnimation(context, resource4);
        }
        a.recycle();
    }

    public void setUpOutAnimation(Context context, int resourceID) {
        setUpOutAnimation(AnimationUtils.loadAnimation(context, resourceID));
    }

    public void setUpOutAnimation(Animation animation) {
        this.upOutAnimation = animation;
    }

    public void setUpInAnimation(Context context, int resourceID) {
        setUpInAnimation(AnimationUtils.loadAnimation(context, resourceID));
    }

    public void setUpInAnimation(Animation animation) {
        this.upInAnimation = animation;
    }

    public void setDownOutAnimation(Context context, int resourceID) {
        setDownOutAnimation(AnimationUtils.loadAnimation(context, resourceID));
    }

    public void setDownOutAnimation(Animation animation) {
        this.downOutAnimation = animation;
    }

    public void setDownInAnimation(Context context, int resourceID) {
        setDownInAnimation(AnimationUtils.loadAnimation(context, resourceID));
    }

    public void setDownInAnimation(Animation animation) {
        this.downInAnimation = animation;
    }

    public void setDisplayedChild(int whichChild) {
        setDisplayedChild(whichChild, true);
    }

    public void setDisplayedChild(int whichChild, boolean animate) {
        int displayedChild = getDisplayedChild();
        if (displayedChild != whichChild) {
            if (!animate) {
                setInAnimation(null);
                setOutAnimation(null);
            } else if (displayedChild < whichChild) {
                setInAnimation(this.downInAnimation);
                setOutAnimation(this.downOutAnimation);
            } else {
                setInAnimation(this.upInAnimation);
                setOutAnimation(this.upOutAnimation);
            }
            super.setDisplayedChild(whichChild);
        }
    }
}
