package com.fsck.k9.view;

import android.content.Context;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.webkit.WebSettings;
import android.widget.Toast;
import com.fsck.k9.K9;
import com.fsck.k9.mailstore.AttachmentResolver;
import yahoo.mail.app.R;

public class MessageWebView extends RigidWebView {

    public interface OnPageFinishedListener {
        void onPageFinished();
    }

    public MessageWebView(Context context) {
        super(context);
    }

    public MessageWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MessageWebView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void blockNetworkData(boolean shouldBlockNetworkData) {
        getSettings().setBlockNetworkLoads(shouldBlockNetworkData);
    }

    public void configure() {
        setVerticalScrollBarEnabled(true);
        setVerticalScrollbarOverlay(true);
        setScrollBarStyle(0);
        setLongClickable(true);
        if (K9.getK9MessageViewTheme() == K9.Theme.DARK) {
            setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
        }
        WebSettings webSettings = getSettings();
        webSettings.setCacheMode(2);
        webSettings.setSupportZoom(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setUseWideViewPort(true);
        if (K9.autofitWidth()) {
            webSettings.setLoadWithOverviewMode(true);
        }
        disableDisplayZoomControls();
        webSettings.setJavaScriptEnabled(false);
        webSettings.setLoadsImagesAutomatically(true);
        webSettings.setRenderPriority(WebSettings.RenderPriority.HIGH);
        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        setOverScrollMode(2);
        webSettings.setTextZoom(K9.getFontSizes().getMessageViewContentAsPercent());
        blockNetworkData(true);
    }

    private void disableDisplayZoomControls() {
        boolean supportsMultiTouch;
        boolean z = true;
        PackageManager pm = getContext().getPackageManager();
        if (pm.hasSystemFeature("android.hardware.touchscreen.multitouch") || pm.hasSystemFeature("android.hardware.faketouch.multitouch.distinct")) {
            supportsMultiTouch = true;
        } else {
            supportsMultiTouch = false;
        }
        WebSettings settings = getSettings();
        if (supportsMultiTouch) {
            z = false;
        }
        settings.setDisplayZoomControls(z);
    }

    public void displayHtmlContentWithInlineAttachments(@NonNull String htmlText, @Nullable AttachmentResolver attachmentResolver, @Nullable OnPageFinishedListener onPageFinishedListener) {
        setWebViewClient(attachmentResolver, onPageFinishedListener);
        setHtmlContent(htmlText);
    }

    private void setWebViewClient(@Nullable AttachmentResolver attachmentResolver, @Nullable OnPageFinishedListener onPageFinishedListener) {
        K9WebViewClient webViewClient = K9WebViewClient.newInstance(attachmentResolver);
        if (onPageFinishedListener != null) {
            webViewClient.setOnPageFinishedListener(onPageFinishedListener);
        }
        setWebViewClient(webViewClient);
    }

    private void setHtmlContent(@NonNull String htmlText) {
        loadDataWithBaseURL("about:blank", htmlText, "text/html", "utf-8", null);
        resumeTimers();
    }

    public void emulateShiftHeld() {
        try {
            new KeyEvent(0, 0, 0, 59, 0, 0).dispatch(this, null, null);
            Toast.makeText(getContext(), (int) R.string.select_text_now, 0).show();
        } catch (Exception e) {
            Log.e("k9", "Exception in emulateShiftHeld()", e);
        }
    }
}
