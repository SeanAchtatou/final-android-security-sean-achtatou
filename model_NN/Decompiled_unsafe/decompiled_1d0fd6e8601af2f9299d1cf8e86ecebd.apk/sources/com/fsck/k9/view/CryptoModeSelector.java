package com.fsck.k9.view;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.SeekBar;
import yahoo.mail.app.R;

public class CryptoModeSelector extends FrameLayout implements SeekBar.OnSeekBarChangeListener {
    private static final ArgbEvaluator ARGB_EVALUATOR = new ArgbEvaluator();
    public static final float CROSSFADE_DIVISOR_2 = 50.0f;
    public static final float CROSSFADE_DIVISOR_3 = 50.0f;
    public static final float CROSSFADE_DIVISOR_4 = 50.0f;
    public static final int CROSSFADE_THRESH_2_HIGH = 150;
    public static final int CROSSFADE_THRESH_2_LOW = 50;
    public static final int CROSSFADE_THRESH_3_HIGH = 250;
    public static final int CROSSFADE_THRESH_3_LOW = 150;
    public static final int CROSSFADE_THRESH_4_LOW = 250;
    private CryptoStatusSelectedListener cryptoStatusListener;
    private int currentCryptoStatus;
    private ObjectAnimator currentSeekbarAnim;
    private ImageView modeIcon2;
    private ImageView modeIcon3;
    private ImageView modeIcon4;
    private SeekBar seekbar;

    public interface CryptoStatusSelectedListener {
        void onCryptoStatusSelected(int i);
    }

    public CryptoModeSelector(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CryptoModeSelector(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init() {
        inflate(getContext(), R.layout.crypto_mode_selector, this);
        this.seekbar = (SeekBar) findViewById(R.id.seek_bar);
        this.modeIcon2 = (ImageView) findViewById(R.id.icon_2);
        this.modeIcon3 = (ImageView) findViewById(R.id.icon_3);
        this.modeIcon4 = (ImageView) findViewById(R.id.icon_4);
        this.seekbar.setOnSeekBarChangeListener(this);
        onProgressChanged(this.seekbar, this.seekbar.getProgress(), false);
    }

    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        float crossfadeValue2;
        float crossfadeValue3;
        float crossfadeValue4;
        int grey = ThemeUtils.getStyledColor(getContext(), (int) R.attr.openpgp_grey);
        if (progress <= 50 || progress >= 150) {
            crossfadeValue2 = 0.0f;
        } else {
            crossfadeValue2 = ((float) (progress - 50)) / 50.0f;
            if (crossfadeValue2 > 1.0f) {
                crossfadeValue2 = 2.0f - crossfadeValue2;
            }
        }
        if (progress <= 150 || progress >= 250) {
            crossfadeValue3 = 0.0f;
        } else {
            crossfadeValue3 = ((float) (progress - 150)) / 50.0f;
            if (crossfadeValue3 > 1.0f) {
                crossfadeValue3 = 2.0f - crossfadeValue3;
            }
        }
        if (progress > 250) {
            crossfadeValue4 = ((float) (progress - 250)) / 50.0f;
        } else {
            crossfadeValue4 = 0.0f;
        }
        this.modeIcon2.setColorFilter(crossfadeColor(grey, ThemeUtils.getStyledColor(getContext(), (int) R.attr.openpgp_blue), crossfadeValue2), PorterDuff.Mode.SRC_ATOP);
        this.modeIcon3.setColorFilter(crossfadeColor(grey, ThemeUtils.getStyledColor(getContext(), (int) R.attr.openpgp_orange), crossfadeValue3), PorterDuff.Mode.SRC_ATOP);
        this.modeIcon4.setColorFilter(crossfadeColor(grey, ThemeUtils.getStyledColor(getContext(), (int) R.attr.openpgp_green), crossfadeValue4), PorterDuff.Mode.SRC_ATOP);
    }

    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    public void onStopTrackingTouch(SeekBar seekBar) {
        int newCryptoStatus;
        int progress = this.seekbar.getProgress();
        if (progress < 50) {
            animateSnapTo(0);
            newCryptoStatus = 0;
        } else if (progress < 150) {
            animateSnapTo(100);
            newCryptoStatus = 1;
        } else if (progress < 250) {
            animateSnapTo(200);
            newCryptoStatus = 2;
        } else {
            animateSnapTo(300);
            newCryptoStatus = 3;
        }
        if (this.currentCryptoStatus != newCryptoStatus) {
            this.currentCryptoStatus = newCryptoStatus;
            this.cryptoStatusListener.onCryptoStatusSelected(newCryptoStatus);
        }
    }

    private void animateSnapTo(int value) {
        if (this.currentSeekbarAnim != null) {
            this.currentSeekbarAnim.cancel();
        }
        this.currentSeekbarAnim = ObjectAnimator.ofInt(this.seekbar, "progress", this.seekbar.getProgress(), value);
        this.currentSeekbarAnim.setDuration(150L);
        this.currentSeekbarAnim.start();
    }

    public static int crossfadeColor(int color1, int color2, float factor) {
        return ((Integer) ARGB_EVALUATOR.evaluate(factor, Integer.valueOf(color1), Integer.valueOf(color2))).intValue();
    }

    public void setCryptoStatusListener(CryptoStatusSelectedListener cryptoStatusListener2) {
        this.cryptoStatusListener = cryptoStatusListener2;
    }

    public void setCryptoStatus(int status) {
        this.currentCryptoStatus = status;
        if (status == 0) {
            this.seekbar.setProgress(0);
        } else if (status == 1) {
            this.seekbar.setProgress(100);
        } else if (status == 2) {
            this.seekbar.setProgress(200);
        } else {
            this.seekbar.setProgress(300);
        }
    }
}
