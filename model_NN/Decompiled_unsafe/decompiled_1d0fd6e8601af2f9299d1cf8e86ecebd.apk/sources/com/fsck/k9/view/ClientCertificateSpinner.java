package com.fsck.k9.view;

import android.app.Activity;
import android.content.Context;
import android.security.KeyChain;
import android.security.KeyChainAliasCallback;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import com.fsck.k9.K9;
import yahoo.mail.app.R;

public class ClientCertificateSpinner extends LinearLayout {
    Activity mActivity;
    String mAlias;
    ImageButton mDeleteButton;
    OnClientCertificateChangedListener mListener;
    Button mSelection;

    public interface OnClientCertificateChangedListener {
        void onClientCertificateChanged(String str);
    }

    public void setOnClientCertificateChangedListener(OnClientCertificateChangedListener listener) {
        this.mListener = listener;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.fsck.k9.view.ClientCertificateSpinner, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public ClientCertificateSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (context instanceof Activity) {
            this.mActivity = (Activity) context;
        } else {
            Log.e("k9", "ClientCertificateSpinner init failed! Please inflate with Activity!");
        }
        setOrientation(0);
        ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.client_certificate_spinner, (ViewGroup) this, true);
        this.mSelection = (Button) findViewById(R.id.client_certificate_spinner_button);
        this.mSelection.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ClientCertificateSpinner.this.chooseCertificate();
            }
        });
        this.mDeleteButton = (ImageButton) findViewById(R.id.client_certificate_spinner_delete);
        this.mDeleteButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ClientCertificateSpinner.this.onDelete();
            }
        });
    }

    public void setAlias(String alias) {
        if (alias != null && alias.equals("")) {
            alias = null;
        }
        this.mAlias = alias;
        this.mActivity.runOnUiThread(new Runnable() {
            public void run() {
                ClientCertificateSpinner.this.updateView();
                if (ClientCertificateSpinner.this.mListener != null) {
                    ClientCertificateSpinner.this.mListener.onClientCertificateChanged(ClientCertificateSpinner.this.mAlias);
                }
            }
        });
    }

    public String getAlias() {
        String alias = this.mSelection.getText().toString();
        if (alias.equals(this.mActivity.getString(R.string.client_certificate_spinner_empty))) {
            return null;
        }
        return alias;
    }

    /* access modifiers changed from: private */
    public void onDelete() {
        setAlias(null);
    }

    public void chooseCertificate() {
        KeyChain.choosePrivateKeyAlias(this.mActivity, new KeyChainAliasCallback() {
            public void alias(String alias) {
                if (K9.DEBUG) {
                    Log.d("k9", "User has selected client certificate alias: " + alias);
                }
                ClientCertificateSpinner.this.setAlias(alias);
            }
        }, null, null, null, -1, getAlias());
    }

    /* access modifiers changed from: private */
    public void updateView() {
        if (this.mAlias != null) {
            this.mSelection.setText(this.mAlias);
        } else {
            this.mSelection.setText((int) R.string.client_certificate_spinner_empty);
        }
    }
}
