package com.fsck.k9.view;

import android.support.annotation.AttrRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import com.fsck.k9.mailstore.CryptoResultAnnotation;
import org.openintents.openpgp.OpenPgpDecryptionResult;
import org.openintents.openpgp.OpenPgpSignatureResult;
import yahoo.mail.app.R;

public enum MessageCryptoDisplayStatus {
    LOADING(R.attr.openpgp_grey, R.drawable.status_lock),
    CANCELLED(R.attr.openpgp_black, R.drawable.status_lock, R.string.crypto_msg_cancelled),
    DISABLED(R.attr.openpgp_grey, R.drawable.status_lock_disabled, R.string.crypto_msg_disabled),
    UNENCRYPTED_SIGN_UNKNOWN(R.attr.openpgp_black, R.drawable.status_signature_unverified_cutout, Integer.valueOf((int) R.drawable.status_dots), R.string.crypto_msg_signed_unencrypted, Integer.valueOf((int) R.string.crypto_msg_sign_unknown)),
    UNENCRYPTED_SIGN_VERIFIED(R.attr.openpgp_blue, R.drawable.status_signature_verified_cutout, Integer.valueOf((int) R.drawable.status_none_dots_3), R.string.crypto_msg_signed_unencrypted, Integer.valueOf((int) R.string.crypto_msg_sign_verified)),
    UNENCRYPTED_SIGN_UNVERIFIED(R.attr.openpgp_orange, R.drawable.status_signature_verified_cutout, Integer.valueOf((int) R.drawable.status_none_dots_2), R.string.crypto_msg_signed_unencrypted, Integer.valueOf((int) R.string.crypto_msg_sign_unverified)),
    UNENCRYPTED_SIGN_MISMATCH(R.attr.openpgp_red, R.drawable.status_signature_verified_cutout, Integer.valueOf((int) R.drawable.status_none_dots_1), R.string.crypto_msg_signed_unencrypted, Integer.valueOf((int) R.string.crypto_msg_sign_mismatch)),
    UNENCRYPTED_SIGN_EXPIRED(R.attr.openpgp_red, R.drawable.status_signature_verified_cutout, Integer.valueOf((int) R.drawable.status_none_dots_1), R.string.crypto_msg_signed_unencrypted, Integer.valueOf((int) R.string.crypto_msg_sign_expired)),
    UNENCRYPTED_SIGN_REVOKED(R.attr.openpgp_red, R.drawable.status_signature_verified_cutout, Integer.valueOf((int) R.drawable.status_none_dots_1), R.string.crypto_msg_signed_unencrypted, Integer.valueOf((int) R.string.crypto_msg_sign_revoked)),
    UNENCRYPTED_SIGN_INSECURE(R.attr.openpgp_red, R.drawable.status_signature_verified_cutout, Integer.valueOf((int) R.drawable.status_none_dots_1), R.string.crypto_msg_signed_unencrypted, Integer.valueOf((int) R.string.crypto_msg_sign_insecure)),
    UNENCRYPTED_SIGN_ERROR(R.attr.openpgp_red, R.drawable.status_signature_verified_cutout, Integer.valueOf((int) R.drawable.status_dots), R.string.crypto_msg_signed_error, null),
    ENCRYPTED_SIGN_UNKNOWN(R.attr.openpgp_black, R.drawable.status_lock_opportunistic, Integer.valueOf((int) R.drawable.status_dots), R.string.crypto_msg_signed_encrypted, Integer.valueOf((int) R.string.crypto_msg_sign_unknown)),
    ENCRYPTED_SIGN_VERIFIED(R.attr.openpgp_green, R.drawable.status_lock, Integer.valueOf((int) R.drawable.status_none_dots_3), R.string.crypto_msg_signed_encrypted, Integer.valueOf((int) R.string.crypto_msg_sign_verified)),
    ENCRYPTED_SIGN_UNVERIFIED(R.attr.openpgp_orange, R.drawable.status_lock, Integer.valueOf((int) R.drawable.status_none_dots_2), R.string.crypto_msg_signed_encrypted, Integer.valueOf((int) R.string.crypto_msg_sign_unverified)),
    ENCRYPTED_SIGN_MISMATCH(R.attr.openpgp_red, R.drawable.status_lock, Integer.valueOf((int) R.drawable.status_none_dots_1), R.string.crypto_msg_signed_encrypted, Integer.valueOf((int) R.string.crypto_msg_sign_mismatch)),
    ENCRYPTED_SIGN_EXPIRED(R.attr.openpgp_red, R.drawable.status_lock, Integer.valueOf((int) R.drawable.status_none_dots_1), R.string.crypto_msg_signed_encrypted, Integer.valueOf((int) R.string.crypto_msg_sign_expired)),
    ENCRYPTED_SIGN_REVOKED(R.attr.openpgp_red, R.drawable.status_lock, Integer.valueOf((int) R.drawable.status_none_dots_1), R.string.crypto_msg_signed_encrypted, Integer.valueOf((int) R.string.crypto_msg_sign_revoked)),
    ENCRYPTED_SIGN_INSECURE(R.attr.openpgp_red, R.drawable.status_lock, Integer.valueOf((int) R.drawable.status_none_dots_1), R.string.crypto_msg_signed_encrypted, Integer.valueOf((int) R.string.crypto_msg_sign_insecure)),
    ENCRYPTED_UNSIGNED(R.attr.openpgp_red, R.drawable.status_lock, Integer.valueOf((int) R.drawable.status_dots), R.string.crypto_msg_encrypted_unsigned, Integer.valueOf((int) R.string.crypto_msg_unsigned_encrypted)),
    ENCRYPTED_SIGN_ERROR(R.attr.openpgp_red, R.drawable.status_lock, Integer.valueOf((int) R.drawable.status_dots), R.string.crypto_msg_signed_encrypted, Integer.valueOf((int) R.string.crypto_msg_sign_error)),
    ENCRYPTED_ERROR(R.attr.openpgp_red, R.drawable.status_lock_error, R.string.crypto_msg_encrypted_error),
    INCOMPLETE_ENCRYPTED(R.attr.openpgp_black, R.drawable.status_lock_opportunistic, R.string.crypto_msg_incomplete_encrypted),
    INCOMPLETE_SIGNED(R.attr.openpgp_black, R.drawable.status_signature_unverified_cutout, Integer.valueOf((int) R.drawable.status_dots), R.string.crypto_msg_signed_unencrypted, Integer.valueOf((int) R.string.crypto_msg_sign_incomplete)),
    UNSUPPORTED_ENCRYPTED(R.attr.openpgp_red, R.drawable.status_lock_error, R.string.crypto_msg_unsupported_encrypted),
    UNSUPPORTED_SIGNED(R.attr.openpgp_grey, R.drawable.status_lock_disabled, R.string.crypto_msg_unsupported_signed);
    
    @AttrRes
    public final int colorAttr;
    @DrawableRes
    public final Integer statusDotsRes;
    @DrawableRes
    public final int statusIconRes;
    @StringRes
    public final Integer textResBottom;
    @StringRes
    public final Integer textResTop;

    private MessageCryptoDisplayStatus(@AttrRes int colorAttr2, @DrawableRes int statusIconRes2, @DrawableRes Integer statusDotsRes2, @StringRes int textResTop2, @StringRes Integer textResBottom2) {
        this.colorAttr = colorAttr2;
        this.statusIconRes = statusIconRes2;
        this.statusDotsRes = statusDotsRes2;
        this.textResTop = Integer.valueOf(textResTop2);
        this.textResBottom = textResBottom2;
    }

    private MessageCryptoDisplayStatus(@AttrRes int colorAttr2, @DrawableRes int statusIconRes2, @StringRes int textResTop2) {
        this.colorAttr = colorAttr2;
        this.statusIconRes = statusIconRes2;
        this.statusDotsRes = null;
        this.textResTop = Integer.valueOf(textResTop2);
        this.textResBottom = null;
    }

    private MessageCryptoDisplayStatus(@AttrRes int colorAttr2, @DrawableRes int statusIconRes2) {
        this.colorAttr = colorAttr2;
        this.statusIconRes = statusIconRes2;
        this.statusDotsRes = null;
        this.textResTop = null;
        this.textResBottom = null;
    }

    @NonNull
    public static MessageCryptoDisplayStatus fromResultAnnotation(CryptoResultAnnotation cryptoResult) {
        if (cryptoResult == null) {
            return DISABLED;
        }
        switch (cryptoResult.getErrorType()) {
            case OPENPGP_OK:
                return getDisplayStatusForPgpResult(cryptoResult);
            case OPENPGP_ENCRYPTED_BUT_INCOMPLETE:
                return INCOMPLETE_ENCRYPTED;
            case OPENPGP_SIGNED_BUT_INCOMPLETE:
                return INCOMPLETE_SIGNED;
            case ENCRYPTED_BUT_UNSUPPORTED:
                return UNSUPPORTED_ENCRYPTED;
            case SIGNED_BUT_UNSUPPORTED:
                return UNSUPPORTED_SIGNED;
            case OPENPGP_UI_CANCELED:
                return CANCELLED;
            case OPENPGP_API_RETURNED_ERROR:
                return ENCRYPTED_ERROR;
            default:
                throw new IllegalStateException("Unhandled case!");
        }
    }

    @NonNull
    private static MessageCryptoDisplayStatus getDisplayStatusForPgpResult(CryptoResultAnnotation cryptoResult) {
        OpenPgpSignatureResult signatureResult = cryptoResult.getOpenPgpSignatureResult();
        OpenPgpDecryptionResult decryptionResult = cryptoResult.getOpenPgpDecryptionResult();
        if (decryptionResult == null || signatureResult == null) {
            throw new AssertionError("Both OpenPGP results must be non-null at this point!");
        }
        if (signatureResult.getResult() == -1 && cryptoResult.hasEncapsulatedResult()) {
            CryptoResultAnnotation encapsulatedResult = cryptoResult.getEncapsulatedResult();
            if (encapsulatedResult.isOpenPgpResult() && (signatureResult = encapsulatedResult.getOpenPgpSignatureResult()) == null) {
                throw new AssertionError("OpenPGP must contain signature result at this point!");
            }
        }
        switch (decryptionResult.getResult()) {
            case -1:
                return getStatusForPgpUnencryptedResult(signatureResult);
            case 0:
                return ENCRYPTED_ERROR;
            case 1:
                return getStatusForPgpEncryptedResult(signatureResult);
            default:
                throw new AssertionError("all cases must be handled, this is a bug!");
        }
    }

    @NonNull
    private static MessageCryptoDisplayStatus getStatusForPgpEncryptedResult(OpenPgpSignatureResult signatureResult) {
        switch (signatureResult.getResult()) {
            case -1:
                return ENCRYPTED_UNSIGNED;
            case 0:
                return ENCRYPTED_SIGN_ERROR;
            case 1:
            case 3:
                switch (signatureResult.getSenderResult()) {
                    case 0:
                        return ENCRYPTED_SIGN_UNVERIFIED;
                    case 1:
                        return ENCRYPTED_SIGN_VERIFIED;
                    case 2:
                        return ENCRYPTED_SIGN_UNVERIFIED;
                    case 3:
                        return ENCRYPTED_SIGN_MISMATCH;
                    default:
                        throw new IllegalStateException("unhandled encrypted result case!");
                }
            case 2:
                return ENCRYPTED_SIGN_UNKNOWN;
            case 4:
                return ENCRYPTED_SIGN_REVOKED;
            case 5:
                return ENCRYPTED_SIGN_EXPIRED;
            case 6:
                return ENCRYPTED_SIGN_INSECURE;
            default:
                throw new IllegalStateException("unhandled encrypted result case!");
        }
    }

    @NonNull
    private static MessageCryptoDisplayStatus getStatusForPgpUnencryptedResult(OpenPgpSignatureResult signatureResult) {
        switch (signatureResult.getResult()) {
            case -1:
                return DISABLED;
            case 0:
                return UNENCRYPTED_SIGN_ERROR;
            case 1:
            case 3:
                switch (signatureResult.getSenderResult()) {
                    case 0:
                        return UNENCRYPTED_SIGN_UNVERIFIED;
                    case 1:
                        return UNENCRYPTED_SIGN_VERIFIED;
                    case 2:
                        return UNENCRYPTED_SIGN_UNVERIFIED;
                    case 3:
                        return UNENCRYPTED_SIGN_MISMATCH;
                    default:
                        throw new IllegalStateException("unhandled encrypted result case!");
                }
            case 2:
                return UNENCRYPTED_SIGN_UNKNOWN;
            case 4:
                return UNENCRYPTED_SIGN_REVOKED;
            case 5:
                return UNENCRYPTED_SIGN_EXPIRED;
            case 6:
                return UNENCRYPTED_SIGN_INSECURE;
            default:
                throw new IllegalStateException("unhandled encrypted result case!");
        }
    }

    public boolean hasAssociatedKey() {
        switch (this) {
            case ENCRYPTED_SIGN_UNKNOWN:
            case ENCRYPTED_SIGN_VERIFIED:
            case ENCRYPTED_SIGN_UNVERIFIED:
            case ENCRYPTED_SIGN_MISMATCH:
            case ENCRYPTED_SIGN_EXPIRED:
            case ENCRYPTED_SIGN_REVOKED:
            case ENCRYPTED_SIGN_INSECURE:
            case UNENCRYPTED_SIGN_UNKNOWN:
            case UNENCRYPTED_SIGN_VERIFIED:
            case UNENCRYPTED_SIGN_UNVERIFIED:
            case UNENCRYPTED_SIGN_MISMATCH:
            case UNENCRYPTED_SIGN_EXPIRED:
            case UNENCRYPTED_SIGN_REVOKED:
            case UNENCRYPTED_SIGN_INSECURE:
                return true;
            default:
                return false;
        }
    }
}
