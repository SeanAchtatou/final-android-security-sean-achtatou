package com.fsck.k9.helper;

import android.text.Annotation;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import com.fsck.k9.K9;
import com.fsck.k9.preferences.SettingsExporter;
import java.util.Collections;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.xml.sax.XMLReader;

public class HtmlConverter {
    private static final String BITCOIN_URI_PATTERN = "bitcoin:[1-9a-km-zA-HJ-NP-Z]{27,34}(\\?[a-zA-Z0-9$\\-_.+!*'(),%:@&=]*)?";
    private static final String GOOD_IRI_CHAR = "a-zA-Z0-9 -퟿豈-﷏ﷰ-￯";
    private static final String HTML_BLOCKQUOTE_COLOR_TOKEN = "$$COLOR$$";
    private static final String HTML_BLOCKQUOTE_END = "</blockquote>";
    private static final String HTML_BLOCKQUOTE_START = "<blockquote class=\"gmail_quote\" style=\"margin: 0pt 0pt 1ex 0.8ex; border-left: 1px solid $$COLOR$$; padding-left: 1ex;\">";
    private static final String HTML_NEWLINE = "<br />";
    private static final String K9MAIL_CSS_CLASS = "k9mail";
    private static final int MAX_SMART_HTMLIFY_MESSAGE_LENGTH = 262144;
    private static final char NBSP_CHARACTER = ' ';
    private static final char NBSP_REPLACEMENT = ' ';
    private static final char PREVIEW_OBJECT_CHARACTER = '￼';
    private static final char PREVIEW_OBJECT_REPLACEMENT = ' ';
    protected static final String QUOTE_COLOR_DEFAULT = "#ccc";
    protected static final String QUOTE_COLOR_LEVEL_1 = "#729fcf";
    protected static final String QUOTE_COLOR_LEVEL_2 = "#ad7fa8";
    protected static final String QUOTE_COLOR_LEVEL_3 = "#8ae234";
    protected static final String QUOTE_COLOR_LEVEL_4 = "#fcaf3e";
    protected static final String QUOTE_COLOR_LEVEL_5 = "#e9b96e";
    private static final int TEXT_TO_HTML_EXTRA_BUFFER_LENGTH = 512;
    private static final String TOP_LEVEL_DOMAIN_STR_FOR_WEB_URL = "(?:(?:aero|arpa|asia|a[cdefgilmnoqrstuwxz])|(?:biz|b[abdefghijmnorstvwyz])|(?:cat|com|coop|c[acdfghiklmnoruvxyz])|d[ejkmoz]|(?:edu|e[cegrstu])|f[ijkmor]|(?:gov|g[abdefghilmnpqrstuwy])|h[kmnrtu]|(?:info|int|i[delmnoqrst])|(?:jobs|j[emop])|k[eghimnprwyz]|l[abcikrstuvy]|(?:mil|mobi|museum|m[acdeghklmnopqrstuvwxyz])|(?:name|net|n[acefgilopruz])|(?:org|om)|(?:pro|p[aefghklmnrstwy])|qa|r[eosuw]|s[abcdeghijklmnortuvyz]|(?:tel|travel|t[cdfghjklmnoprtvwz])|u[agksyz]|v[aceginu]|w[fs]|(?:xn\\-\\-0zwm56d|xn\\-\\-11b5bs3a9aj6g|xn\\-\\-80akhbyknj4f|xn\\-\\-9t4b11yi5a|xn\\-\\-deba0ad|xn\\-\\-fiqs8s|xn\\-\\-fiqz9s|xn\\-\\-fzc2c9e2c|xn\\-\\-g6w251d|xn\\-\\-hgbk6aj7f53bba|xn\\-\\-hlcj6aya9esc7a|xn\\-\\-j6w193g|xn\\-\\-jxalpdlp|xn\\-\\-kgbechtv|xn\\-\\-kprw13d|xn\\-\\-kpry57d|xn\\-\\-mgbaam7a8h|xn\\-\\-mgbayh7gpa|xn\\-\\-mgberp4a5d4ar|xn\\-\\-o3cw4h|xn\\-\\-p1ai|xn\\-\\-pgbs0dh|xn\\-\\-wgbh1c|xn\\-\\-wgbl6a|xn\\-\\-xkc2al3hye2a|xn\\-\\-ygbi2ammx|xn\\-\\-zckzah)|y[et]|z[amw]))";
    private static final Pattern WEB_URL_PATTERN = Pattern.compile("((?:(http|https|Http|Https|rtsp|Rtsp):\\/\\/(?:(?:[a-zA-Z0-9\\$\\-\\_\\.\\+\\!\\*\\'\\(\\)\\,\\;\\?\\&\\=]|(?:\\%[a-fA-F0-9]{2})){1,64}(?:\\:(?:[a-zA-Z0-9\\$\\-\\_\\.\\+\\!\\*\\'\\(\\)\\,\\;\\?\\&\\=]|(?:\\%[a-fA-F0-9]{2})){1,25})?\\@)?)?((?:(?:[a-zA-Z0-9 -퟿豈-﷏ﷰ-￯][a-zA-Z0-9 -퟿豈-﷏ﷰ-￯\\-]{0,64}\\.)+(?:(?:aero|arpa|asia|a[cdefgilmnoqrstuwxz])|(?:biz|b[abdefghijmnorstvwyz])|(?:cat|com|coop|c[acdfghiklmnoruvxyz])|d[ejkmoz]|(?:edu|e[cegrstu])|f[ijkmor]|(?:gov|g[abdefghilmnpqrstuwy])|h[kmnrtu]|(?:info|int|i[delmnoqrst])|(?:jobs|j[emop])|k[eghimnprwyz]|l[abcikrstuvy]|(?:mil|mobi|museum|m[acdeghklmnopqrstuvwxyz])|(?:name|net|n[acefgilopruz])|(?:org|om)|(?:pro|p[aefghklmnrstwy])|qa|r[eosuw]|s[abcdeghijklmnortuvyz]|(?:tel|travel|t[cdfghjklmnoprtvwz])|u[agksyz]|v[aceginu]|w[fs]|(?:xn\\-\\-0zwm56d|xn\\-\\-11b5bs3a9aj6g|xn\\-\\-80akhbyknj4f|xn\\-\\-9t4b11yi5a|xn\\-\\-deba0ad|xn\\-\\-fiqs8s|xn\\-\\-fiqz9s|xn\\-\\-fzc2c9e2c|xn\\-\\-g6w251d|xn\\-\\-hgbk6aj7f53bba|xn\\-\\-hlcj6aya9esc7a|xn\\-\\-j6w193g|xn\\-\\-jxalpdlp|xn\\-\\-kgbechtv|xn\\-\\-kprw13d|xn\\-\\-kpry57d|xn\\-\\-mgbaam7a8h|xn\\-\\-mgbayh7gpa|xn\\-\\-mgberp4a5d4ar|xn\\-\\-o3cw4h|xn\\-\\-p1ai|xn\\-\\-pgbs0dh|xn\\-\\-wgbh1c|xn\\-\\-wgbl6a|xn\\-\\-xkc2al3hye2a|xn\\-\\-ygbi2ammx|xn\\-\\-zckzah)|y[et]|z[amw]))|(?:(?:25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9])\\.(?:25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(?:25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(?:25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[0-9])))(?:\\:\\d{1,5})?)(\\/(?:(?:[a-zA-Z0-9 -퟿豈-﷏ﷰ-￯\\;\\/\\?\\:\\@\\&\\=\\#\\~\\-\\.\\+\\!\\*\\'\\(\\)\\,\\_])|(?:\\%[a-fA-F0-9]{2}))*)?(?:\\b|$)");

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [?, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static String htmlToText(String html) {
        return Html.fromHtml(html, null, new HtmlToTextTagHandler()).toString().replace((char) PREVIEW_OBJECT_CHARACTER, ' ').replace(160, ' ');
    }

    private static class HtmlToTextTagHandler implements Html.TagHandler {
        private static final String IGNORED_ANNOTATION_KEY = "K9_ANNOTATION";
        private static final String IGNORED_ANNOTATION_VALUE = "hiddenSpan";
        private static final Set<String> TAGS_WITH_IGNORED_CONTENT;

        private HtmlToTextTagHandler() {
        }

        static {
            Set<String> set = new HashSet<>();
            set.add("style");
            set.add("script");
            set.add("title");
            set.add("!");
            TAGS_WITH_IGNORED_CONTENT = Collections.unmodifiableSet(set);
        }

        public void handleTag(boolean opening, String tag, Editable output, XMLReader xmlReader) {
            String tag2 = tag.toLowerCase(Locale.US);
            if (tag2.equals("hr") && opening) {
                output.append((CharSequence) "_____________________________________________\r\n");
            } else if (TAGS_WITH_IGNORED_CONTENT.contains(tag2)) {
                handleIgnoredTag(opening, output);
            }
        }

        private void handleIgnoredTag(boolean opening, Editable output) {
            int len = output.length();
            if (opening) {
                output.setSpan(new Annotation(IGNORED_ANNOTATION_KEY, IGNORED_ANNOTATION_VALUE), len, len, 17);
                return;
            }
            Object start = getOpeningAnnotation(output);
            if (start != null) {
                int where = output.getSpanStart(start);
                output.removeSpan(start);
                output.delete(where, len);
            }
        }

        private Object getOpeningAnnotation(Editable output) {
            Object[] objs = output.getSpans(0, output.length(), Annotation.class);
            for (int i = objs.length - 1; i >= 0; i--) {
                Annotation span = (Annotation) objs[i];
                if (output.getSpanFlags(objs[i]) == 17 && span.getKey().equals(IGNORED_ANNOTATION_KEY) && span.getValue().equals(IGNORED_ANNOTATION_VALUE)) {
                    return objs[i];
                }
            }
            return null;
        }
    }

    private static String simpleTextToHtml(String text) {
        String text2 = TextUtils.htmlEncode(text);
        StringBuilder buff = new StringBuilder(text2.length() + 512);
        buff.append(htmlifyMessageHeader());
        for (int index = 0; index < text2.length(); index++) {
            char c = text2.charAt(index);
            switch (c) {
                case 10:
                    buff.append(HTML_NEWLINE);
                    break;
                case 11:
                case 12:
                default:
                    buff.append(c);
                    break;
                case 13:
                    break;
            }
        }
        buff.append(htmlifyMessageFooter());
        return buff.toString();
    }

    public static String textToHtml(String text) {
        if (text.length() > 262144) {
            return simpleTextToHtml(text);
        }
        StringBuilder buff = new StringBuilder(text.length() + 512);
        boolean isStartOfLine = true;
        int spaces = 0;
        int quoteDepth = 0;
        int quotesThisLine = 0;
        for (int index = 0; index < text.length(); index++) {
            char c = text.charAt(index);
            if (isStartOfLine) {
                switch (c) {
                    case 10:
                        appendbq(buff, quotesThisLine, quoteDepth);
                        quoteDepth = quotesThisLine;
                        appendsp(buff, spaces);
                        spaces = 0;
                        appendchar(buff, c);
                        isStartOfLine = true;
                        quotesThisLine = 0;
                        continue;
                    case ' ':
                        spaces++;
                        continue;
                    case '>':
                        quotesThisLine++;
                        spaces = 0;
                        continue;
                    default:
                        appendbq(buff, quotesThisLine, quoteDepth);
                        quoteDepth = quotesThisLine;
                        appendsp(buff, spaces);
                        spaces = 0;
                        appendchar(buff, c);
                        isStartOfLine = false;
                        continue;
                }
            } else {
                appendchar(buff, c);
                if (c == 10) {
                    isStartOfLine = true;
                    quotesThisLine = 0;
                }
            }
        }
        if (quoteDepth > 0) {
            for (int i = quoteDepth; i > 0; i--) {
                buff.append(HTML_BLOCKQUOTE_END);
            }
        }
        String text2 = buff.toString().replaceAll("\\Q<br />\\E((\\Q<br />\\E)+?)\\Q</blockquote>\\E", "</blockquote>$1").replaceAll("\\s*([-=_]{30,}+)\\s*", "<hr />");
        StringBuffer sb = new StringBuffer(text2.length() + 512);
        sb.append(htmlifyMessageHeader());
        linkifyText(text2, sb);
        sb.append(htmlifyMessageFooter());
        return sb.toString().replaceAll("<gt>", "&gt;");
    }

    private static void appendchar(StringBuilder buff, int c) {
        switch (c) {
            case 10:
                buff.append(HTML_NEWLINE);
                return;
            case 13:
                return;
            case 38:
                buff.append("&amp;");
                return;
            case 60:
                buff.append("&lt;");
                return;
            case 62:
                buff.append("<gt>");
                return;
            default:
                buff.append((char) c);
                return;
        }
    }

    private static void appendsp(StringBuilder buff, int spaces) {
        while (spaces > 0) {
            buff.append(' ');
            spaces--;
        }
    }

    private static void appendbq(StringBuilder buff, int quotesThisLine, int quoteDepth) {
        if (quotesThisLine > quoteDepth) {
            for (int i = quoteDepth; i < quotesThisLine; i++) {
                buff.append(HTML_BLOCKQUOTE_START.replace(HTML_BLOCKQUOTE_COLOR_TOKEN, getQuoteColor(i + 1)));
            }
        } else if (quotesThisLine < quoteDepth) {
            for (int i2 = quoteDepth; i2 > quotesThisLine; i2--) {
                buff.append(HTML_BLOCKQUOTE_END);
            }
        }
    }

    protected static String getQuoteColor(int level) {
        switch (level) {
            case 1:
                return QUOTE_COLOR_LEVEL_1;
            case 2:
                return QUOTE_COLOR_LEVEL_2;
            case 3:
                return QUOTE_COLOR_LEVEL_3;
            case 4:
                return QUOTE_COLOR_LEVEL_4;
            case 5:
                return QUOTE_COLOR_LEVEL_5;
            default:
                return QUOTE_COLOR_DEFAULT;
        }
    }

    protected static void linkifyText(String text, StringBuffer outputBuffer) {
        String prepared = text.replaceAll(BITCOIN_URI_PATTERN, "<a href=\"$0\">$0</a>");
        Matcher m = WEB_URL_PATTERN.matcher(prepared);
        while (m.find()) {
            int start = m.start();
            if (start != 0 && (start == 0 || prepared.charAt(start - 1) == '@')) {
                m.appendReplacement(outputBuffer, "$0");
            } else if (m.group().indexOf(58) > 0) {
                m.appendReplacement(outputBuffer, "<a href=\"$0\">$0</a>");
            } else {
                m.appendReplacement(outputBuffer, "<a href=\"http://$0\">$0</a>");
            }
        }
        m.appendTail(outputBuffer);
    }

    private static boolean hasEmoji(String html) {
        for (int i = 0; i < html.length(); i++) {
            char c = html.charAt(i);
            if (c >= 56248 && c < 56252) {
                return true;
            }
        }
        return false;
    }

    public static String convertEmoji2Img(String html) {
        if (!hasEmoji(html)) {
            return html;
        }
        StringBuilder buff = new StringBuilder(html.length() + 512);
        int i = 0;
        while (i < html.length()) {
            int codePoint = html.codePointAt(i);
            String emoji = getEmojiForCodePoint(codePoint);
            if (emoji != null) {
                buff.append("<img src=\"file:///android_asset/emoticons/").append(emoji).append(".gif\" alt=\"").append(emoji).append("\" />");
            } else {
                buff.appendCodePoint(codePoint);
            }
            i = html.offsetByCodePoints(i, 1);
        }
        return buff.toString();
    }

    private static String getEmojiForCodePoint(int codePoint) {
        switch (codePoint) {
            case 1040384:
                return "sun";
            case 1040385:
                return "cloud";
            case 1040386:
                return "rain";
            case 1040387:
                return "snow";
            case 1040388:
                return "thunder";
            case 1040389:
                return "typhoon";
            case 1040390:
                return "mist";
            case 1040391:
                return "sprinkle";
            case 1040392:
                return "night";
            case 1040393:
                return "sun";
            case 1040394:
                return "sun";
            case 1040396:
                return "sun";
            case 1040400:
                return "night";
            case 1040401:
                return "newmoon";
            case 1040402:
                return "moon1";
            case 1040403:
                return "moon2";
            case 1040404:
                return "moon3";
            case 1040405:
                return "fullmoon";
            case 1040406:
                return "moon2";
            case 1040408:
                return "soon";
            case 1040409:
                return "on";
            case 1040410:
                return "end";
            case 1040411:
                return "sandclock";
            case 1040412:
                return "sandclock";
            case 1040413:
                return "watch";
            case 1040414:
                return "clock";
            case 1040415:
                return "clock";
            case 1040416:
                return "clock";
            case 1040417:
                return "clock";
            case 1040418:
                return "clock";
            case 1040419:
                return "clock";
            case 1040420:
                return "clock";
            case 1040421:
                return "clock";
            case 1040422:
                return "clock";
            case 1040423:
                return "clock";
            case 1040424:
                return "clock";
            case 1040425:
                return "clock";
            case 1040426:
                return "clock";
            case 1040427:
                return "aries";
            case 1040428:
                return "taurus";
            case 1040429:
                return "gemini";
            case 1040430:
                return "cancer";
            case 1040431:
                return "leo";
            case 1040432:
                return "virgo";
            case 1040433:
                return "libra";
            case 1040434:
                return "scorpius";
            case 1040435:
                return "sagittarius";
            case 1040436:
                return "capricornus";
            case 1040437:
                return "aquarius";
            case 1040438:
                return "pisces";
            case 1040440:
                return "wave";
            case 1040443:
                return "night";
            case 1040444:
                return "clover";
            case 1040445:
                return "tulip";
            case 1040446:
                return "bud";
            case 1040447:
                return "maple";
            case 1040448:
                return "cherryblossom";
            case 1040450:
                return "maple";
            case 1040462:
                return "clover";
            case 1040463:
                return "cherry";
            case 1040464:
                return "banana";
            case 1040465:
                return "apple";
            case 1040475:
                return "apple";
            case 1040784:
                return "eye";
            case 1040785:
                return "ear";
            case 1040787:
                return "kissmark";
            case 1040788:
                return "bleah";
            case 1040789:
                return "rouge";
            case 1040792:
                return "hairsalon";
            case 1040794:
                return "shadow";
            case 1040795:
                return "happy01";
            case 1040796:
                return "happy01";
            case 1040797:
                return "happy01";
            case 1040798:
                return "happy01";
            case 1040823:
                return "dog";
            case 1040824:
                return "cat";
            case 1040825:
                return "snail";
            case 1040826:
                return "chick";
            case 1040827:
                return "chick";
            case 1040828:
                return "penguin";
            case 1040829:
                return "fish";
            case 1040830:
                return "horse";
            case 1040831:
                return "pig";
            case 1040840:
                return "chick";
            case 1040841:
                return "fish";
            case 1040847:
                return "aries";
            case 1040848:
                return "dog";
            case 1040856:
                return "dog";
            case 1040857:
                return "fish";
            case 1040859:
                return "foot";
            case 1040861:
                return "chick";
            case 1040864:
                return "pig";
            case 1040867:
                return "cancer";
            case 1041184:
                return "angry";
            case 1041185:
                return "sad";
            case 1041186:
                return "wobbly";
            case 1041187:
                return "despair";
            case 1041188:
                return "wobbly";
            case 1041189:
                return "coldsweats02";
            case 1041190:
                return "gawk";
            case 1041191:
                return "lovely";
            case 1041192:
                return "smile";
            case 1041193:
                return "bleah";
            case 1041194:
                return "bleah";
            case 1041195:
                return "delicious";
            case 1041196:
                return "lovely";
            case 1041197:
                return "lovely";
            case 1041199:
                return "happy02";
            case 1041200:
                return "happy01";
            case 1041201:
                return "coldsweats01";
            case 1041202:
                return "happy02";
            case 1041203:
                return "smile";
            case 1041204:
                return "happy02";
            case 1041205:
                return "delicious";
            case 1041206:
                return "happy01";
            case 1041207:
                return "happy01";
            case 1041208:
                return "coldsweats01";
            case 1041209:
                return "weep";
            case 1041210:
                return "crying";
            case 1041211:
                return "shock";
            case 1041212:
                return "bearing";
            case 1041213:
                return "pout";
            case 1041214:
                return "confident";
            case 1041215:
                return "sad";
            case 1041216:
                return "think";
            case 1041217:
                return "shock";
            case 1041218:
                return "sleepy";
            case 1041219:
                return "catface";
            case 1041220:
                return "coldsweats02";
            case 1041221:
                return "coldsweats02";
            case 1041222:
                return "bearing";
            case 1041223:
                return "wink";
            case 1041224:
                return "happy01";
            case 1041225:
                return "smile";
            case 1041226:
                return "happy02";
            case 1041227:
                return "lovely";
            case 1041228:
                return "lovely";
            case 1041229:
                return "weep";
            case 1041230:
                return "pout";
            case 1041231:
                return "smile";
            case 1041232:
                return "sad";
            case 1041233:
                return "ng";
            case 1041234:
                return "ok";
            case 1041239:
                return "paper";
            case 1041241:
                return "sad";
            case 1041242:
                return "angry";
            case 1041584:
                return "house";
            case 1041585:
                return "house";
            case 1041586:
                return "building";
            case 1041587:
                return "postoffice";
            case 1041588:
                return "hospital";
            case 1041589:
                return "bank";
            case 1041590:
                return "atm";
            case 1041591:
                return "hotel";
            case 1041593:
                return "24hours";
            case 1041594:
                return "school";
            case 1041601:
                return "ship";
            case 1041602:
                return "bottle";
            case 1041603:
                return "fuji";
            case 1041609:
                return "wrench";
            case 1041612:
                return "shoe";
            case 1041613:
                return "shoe";
            case 1041614:
                return "eyeglass";
            case 1041615:
                return "t-shirt";
            case 1041616:
                return "denim";
            case 1041617:
                return "crown";
            case 1041618:
                return "crown";
            case 1041622:
                return "boutique";
            case 1041623:
                return "boutique";
            case 1041627:
                return "t-shirt";
            case 1041628:
                return "moneybag";
            case 1041629:
                return "dollar";
            case 1041632:
                return "dollar";
            case 1041634:
                return "yen";
            case 1041635:
                return "dollar";
            case 1041647:
                return "camera";
            case 1041648:
                return "bag";
            case 1041649:
                return "pouch";
            case 1041650:
                return "bell";
            case 1041651:
                return "door";
            case 1041657:
                return "movie";
            case 1041659:
                return "flair";
            case 1041661:
                return "sign05";
            case 1041663:
                return "book";
            case 1041664:
                return "book";
            case 1041665:
                return "book";
            case 1041666:
                return "book";
            case 1041667:
                return "book";
            case 1041669:
                return "spa";
            case 1041670:
                return "toilet";
            case 1041671:
                return "toilet";
            case 1041672:
                return "toilet";
            case 1041679:
                return "ribbon";
            case 1041680:
                return "present";
            case 1041681:
                return "birthday";
            case 1041682:
                return "xmas";
            case 1041698:
                return "pocketbell";
            case 1041699:
                return "telephone";
            case 1041700:
                return "telephone";
            case 1041701:
                return "mobilephone";
            case 1041702:
                return "phoneto";
            case 1041703:
                return "memo";
            case 1041704:
                return "faxto";
            case 1041705:
                return "mail";
            case 1041706:
                return "mailto";
            case 1041707:
                return "mailto";
            case 1041708:
                return "postoffice";
            case 1041709:
                return "postoffice";
            case 1041710:
                return "postoffice";
            case 1041717:
                return "present";
            case 1041718:
                return "pen";
            case 1041719:
                return "chair";
            case 1041720:
                return "pc";
            case 1041721:
                return "pencil";
            case 1041722:
                return "clip";
            case 1041723:
                return "bag";
            case 1041726:
                return "hairsalon";
            case 1041728:
                return "memo";
            case 1041729:
                return "memo";
            case 1041733:
                return "book";
            case 1041734:
                return "book";
            case 1041735:
                return "book";
            case 1041736:
                return "memo";
            case 1041741:
                return "book";
            case 1041743:
                return "book";
            case 1041746:
                return "memo";
            case 1041747:
                return "foot";
            case 1042384:
                return "sports";
            case 1042385:
                return "baseball";
            case 1042386:
                return "golf";
            case 1042387:
                return "tennis";
            case 1042388:
                return "soccer";
            case 1042389:
                return "ski";
            case 1042390:
                return "basketball";
            case 1042391:
                return "motorsports";
            case 1042392:
                return "snowboard";
            case 1042393:
                return "run";
            case 1042394:
                return "snowboard";
            case 1042396:
                return "horse";
            case 1042399:
                return "train";
            case 1042400:
                return "subway";
            case 1042401:
                return "subway";
            case 1042402:
                return "bullettrain";
            case 1042403:
                return "bullettrain";
            case 1042404:
                return "car";
            case 1042405:
                return "rvcar";
            case 1042406:
                return "bus";
            case 1042408:
                return "ship";
            case 1042409:
                return "airplane";
            case 1042410:
                return "yacht";
            case 1042411:
                return "bicycle";
            case 1042414:
                return "yacht";
            case 1042415:
                return "car";
            case 1042416:
                return "run";
            case 1042421:
                return "gasstation";
            case 1042422:
                return "parking";
            case 1042423:
                return "signaler";
            case 1042426:
                return "spa";
            case 1042428:
                return "carouselpony";
            case 1042431:
                return "fish";
            case 1042432:
                return "karaoke";
            case 1042433:
                return "movie";
            case 1042434:
                return "movie";
            case 1042435:
                return "music";
            case 1042436:
                return "art";
            case 1042437:
                return "drama";
            case 1042438:
                return "event";
            case 1042439:
                return "ticket";
            case 1042440:
                return "slate";
            case 1042441:
                return "drama";
            case 1042442:
                return "game";
            case 1042451:
                return "note";
            case 1042452:
                return "notes";
            case 1042458:
                return "notes";
            case 1042460:
                return "tv";
            case 1042461:
                return "cd";
            case 1042462:
                return "cd";
            case 1042467:
                return "kissmark";
            case 1042468:
                return "loveletter";
            case 1042469:
                return "ring";
            case 1042470:
                return "ring";
            case 1042471:
                return "kissmark";
            case 1042473:
                return "heart02";
            case 1042475:
                return "freedial";
            case 1042476:
                return "sharp";
            case 1042477:
                return "mobaq";
            case 1042478:
                return "one";
            case 1042479:
                return "two";
            case 1042480:
                return "three";
            case 1042481:
                return "four";
            case 1042482:
                return "five";
            case 1042483:
                return "six";
            case 1042484:
                return "seven";
            case 1042485:
                return "eight";
            case 1042486:
                return "nine";
            case 1042487:
                return "zero";
            case 1042784:
                return "fastfood";
            case 1042785:
                return "riceball";
            case 1042786:
                return "cake";
            case 1042787:
                return "noodle";
            case 1042788:
                return "bread";
            case 1042794:
                return "noodle";
            case 1042803:
                return "typhoon";
            case 1042816:
                return "restaurant";
            case 1042817:
                return "cafe";
            case 1042818:
                return "bar";
            case 1042819:
                return "beer";
            case 1042820:
                return "japanesetea";
            case 1042821:
                return "bottle";
            case 1042822:
                return "wine";
            case 1042823:
                return "beer";
            case 1042824:
                return "bar";
            case 1043184:
                return "upwardright";
            case 1043185:
                return "downwardright";
            case 1043186:
                return "upwardleft";
            case 1043187:
                return "downwardleft";
            case 1043188:
                return "up";
            case 1043189:
                return "down";
            case 1043190:
                return "leftright";
            case 1043191:
                return "updown";
            case 1043204:
                return "sign01";
            case 1043205:
                return "sign02";
            case 1043206:
                return "sign03";
            case 1043207:
                return "sign04";
            case 1043208:
                return "sign05";
            case 1043211:
                return "sign01";
            case 1043212:
                return "heart01";
            case 1043213:
                return "heart02";
            case 1043214:
                return "heart03";
            case 1043215:
                return "heart04";
            case 1043216:
                return "heart01";
            case 1043217:
                return "heart02";
            case 1043218:
                return "heart01";
            case 1043219:
                return "heart01";
            case 1043220:
                return "heart01";
            case 1043221:
                return "heart01";
            case 1043222:
                return "heart01";
            case 1043223:
                return "heart01";
            case 1043224:
                return "heart02";
            case 1043225:
                return "cute";
            case 1043226:
                return "heart";
            case 1043227:
                return "spade";
            case 1043228:
                return "diamond";
            case 1043229:
                return "club";
            case 1043230:
                return "smoking";
            case 1043231:
                return "nosmoking";
            case 1043232:
                return "wheelchair";
            case 1043233:
                return "free";
            case 1043234:
                return "flag";
            case 1043235:
                return "danger";
            case 1043238:
                return "ng";
            case 1043239:
                return "ok";
            case 1043240:
                return "ng";
            case 1043241:
                return "copyright";
            case 1043242:
                return "tm";
            case 1043243:
                return "secret";
            case 1043244:
                return "recycle";
            case 1043245:
                return "r-mark";
            case 1043246:
                return "ban";
            case 1043247:
                return "empty";
            case 1043248:
                return "pass";
            case 1043249:
                return "full";
            case 1043254:
                return "new";
            case 1043268:
                return "fullmoon";
            case 1043272:
                return "ban";
            case 1043285:
                return "cute";
            case 1043286:
                return "flair";
            case 1043287:
                return "annoy";
            case 1043288:
                return "bomb";
            case 1043289:
                return "sleepy";
            case 1043290:
                return "impact";
            case 1043291:
                return "sweat01";
            case 1043292:
                return "sweat02";
            case 1043293:
                return "dash";
            case 1043295:
                return "sad";
            case 1043296:
                return "shine";
            case 1043297:
                return "cute";
            case 1043298:
                return "cute";
            case 1043299:
                return "newmoon";
            case 1043300:
                return "newmoon";
            case 1043301:
                return "newmoon";
            case 1043302:
                return "newmoon";
            case 1043303:
                return "newmoon";
            case 1043319:
                return "shine";
            case 1043329:
                return "id";
            case 1043330:
                return SettingsExporter.KEY_ATTRIBUTE;
            case 1043331:
                return "enter";
            case 1043332:
                return "clear";
            case 1043333:
                return "search";
            case 1043334:
                return SettingsExporter.KEY_ATTRIBUTE;
            case 1043335:
                return SettingsExporter.KEY_ATTRIBUTE;
            case 1043338:
                return SettingsExporter.KEY_ATTRIBUTE;
            case 1043341:
                return "search";
            case 1043344:
                return SettingsExporter.KEY_ATTRIBUTE;
            case 1043345:
                return "recycle";
            case 1043346:
                return "mail";
            case 1043347:
                return "rock";
            case 1043348:
                return "scissors";
            case 1043349:
                return "paper";
            case 1043350:
                return "punch";
            case 1043351:
                return "good";
            case 1043357:
                return "paper";
            case 1043359:
                return "ok";
            case 1043360:
                return "down";
            case 1043361:
                return "paper";
            case 1043984:
                return "info01";
            case 1043985:
                return "info02";
            case 1043986:
                return "by-d";
            case 1043987:
                return "d-point";
            case 1043988:
                return "appli01";
            case 1043989:
                return "appli02";
            case 1043996:
                return "movie";
            default:
                return null;
        }
    }

    private static String htmlifyMessageHeader() {
        return "<pre class=\"k9mail\">";
    }

    private static String htmlifyMessageFooter() {
        return "</pre>";
    }

    public static String wrapStatusMessage(CharSequence status) {
        return wrapMessageContent("<div style=\"text-align:center; color: grey;\">" + ((Object) status) + "</div>");
    }

    public static String wrapMessageContent(CharSequence messageContent) {
        return "<html><head><meta name=\"viewport\" content=\"width=device-width\"/>" + cssStyleTheme() + cssStylePre() + "</head><body>" + ((Object) messageContent) + "</body></html>";
    }

    private static String cssStyleTheme() {
        if (K9.getK9MessageViewTheme() == K9.Theme.DARK) {
            return "<style type=\"text/css\">* { background: black ! important; color: #F3F3F3 !important }:link, :link * { color: #CCFF33 !important }:visited, :visited * { color: #551A8B !important }</style> ";
        }
        return "";
    }

    public static String cssStylePre() {
        return "<style type=\"text/css\"> pre.k9mail {white-space: pre-wrap; word-wrap:break-word; font-family: " + (K9.messageViewFixedWidthFont() ? "monospace" : "sans-serif") + "; margin-top: 0px}</style>";
    }

    public static String textToHtmlFragment(String text) {
        String htmlified = TextUtils.htmlEncode(text);
        StringBuffer linkified = new StringBuffer(htmlified.length() + 512);
        linkifyText(htmlified, linkified);
        return linkified.toString().replaceAll("\r?\n", "<br>\r\n").replace("&apos;", "&#39;");
    }

    public static Spanned htmlToSpanned(String html) {
        return Html.fromHtml(html, null, new ListTagHandler());
    }

    public static class ListTagHandler implements Html.TagHandler {
        public void handleTag(boolean opening, String tag, Editable output, XMLReader xmlReader) {
            if (tag.equals("ul")) {
                if (opening) {
                    char lastChar = 0;
                    if (output.length() > 0) {
                        lastChar = output.charAt(output.length() - 1);
                    }
                    if (lastChar != 10) {
                        output.append((CharSequence) "\r\n");
                    }
                } else {
                    output.append((CharSequence) "\r\n");
                }
            }
            if (!tag.equals("li")) {
                return;
            }
            if (opening) {
                output.append((CharSequence) "\t•  ");
            } else {
                output.append((CharSequence) "\r\n");
            }
        }
    }
}
