package com.fsck.k9.helper;

import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.database.CharArrayBuffer;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.net.Uri;
import android.os.Bundle;
import java.util.Comparator;

public class MergeCursor implements Cursor {
    protected Cursor mActiveCursor;
    protected int mActiveCursorIndex;
    private final Comparator<Cursor> mComparator;
    private int mCount = -1;
    protected final Cursor[] mCursors;
    protected int mPosition;

    public MergeCursor(Cursor[] cursors, Comparator<Cursor> comparator) {
        this.mCursors = (Cursor[]) cursors.clone();
        this.mComparator = comparator;
        resetCursors();
    }

    private void resetCursors() {
        this.mActiveCursorIndex = -1;
        this.mActiveCursor = null;
        this.mPosition = -1;
        int len = this.mCursors.length;
        for (int i = 0; i < len; i++) {
            Cursor cursor = this.mCursors[i];
            if (cursor != null) {
                cursor.moveToPosition(-1);
                if (this.mActiveCursor == null) {
                    this.mActiveCursorIndex = i;
                    this.mActiveCursor = this.mCursors[this.mActiveCursorIndex];
                }
            }
        }
    }

    public void close() {
        for (Cursor cursor : this.mCursors) {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public void copyStringToBuffer(int columnIndex, CharArrayBuffer buffer) {
        this.mActiveCursor.copyStringToBuffer(columnIndex, buffer);
    }

    public void deactivate() {
        for (Cursor cursor : this.mCursors) {
            if (cursor != null) {
                cursor.deactivate();
            }
        }
    }

    public byte[] getBlob(int columnIndex) {
        return this.mActiveCursor.getBlob(columnIndex);
    }

    public int getColumnCount() {
        return this.mActiveCursor.getColumnCount();
    }

    public int getColumnIndex(String columnName) {
        return this.mActiveCursor.getColumnIndex(columnName);
    }

    public int getColumnIndexOrThrow(String columnName) throws IllegalArgumentException {
        return this.mActiveCursor.getColumnIndexOrThrow(columnName);
    }

    public String getColumnName(int columnIndex) {
        return this.mActiveCursor.getColumnName(columnIndex);
    }

    public String[] getColumnNames() {
        return this.mActiveCursor.getColumnNames();
    }

    public int getCount() {
        if (this.mCount == -1) {
            int count = 0;
            for (Cursor cursor : this.mCursors) {
                if (cursor != null) {
                    count += cursor.getCount();
                }
            }
            this.mCount = count;
        }
        return this.mCount;
    }

    public double getDouble(int columnIndex) {
        return this.mActiveCursor.getDouble(columnIndex);
    }

    public float getFloat(int columnIndex) {
        return this.mActiveCursor.getFloat(columnIndex);
    }

    public int getInt(int columnIndex) {
        return this.mActiveCursor.getInt(columnIndex);
    }

    public long getLong(int columnIndex) {
        return this.mActiveCursor.getLong(columnIndex);
    }

    public int getPosition() {
        return this.mPosition;
    }

    public short getShort(int columnIndex) {
        return this.mActiveCursor.getShort(columnIndex);
    }

    public String getString(int columnIndex) {
        return this.mActiveCursor.getString(columnIndex);
    }

    public int getType(int columnIndex) {
        return this.mActiveCursor.getType(columnIndex);
    }

    public boolean getWantsAllOnMoveCalls() {
        return this.mActiveCursor.getWantsAllOnMoveCalls();
    }

    @TargetApi(23)
    public void setExtras(Bundle extras) {
        this.mActiveCursor.setExtras(extras);
    }

    public boolean isAfterLast() {
        int count = getCount();
        if (count == 0 || this.mPosition == count) {
            return true;
        }
        return false;
    }

    public boolean isBeforeFirst() {
        if (getCount() == 0 || this.mPosition == -1) {
            return true;
        }
        return false;
    }

    public boolean isClosed() {
        return this.mActiveCursor.isClosed();
    }

    public boolean isFirst() {
        if (getCount() != 0 && this.mPosition == 0) {
            return true;
        }
        return false;
    }

    public boolean isLast() {
        int count = getCount();
        if (count != 0 && this.mPosition == count - 1) {
            return true;
        }
        return false;
    }

    public boolean isNull(int columnIndex) {
        return this.mActiveCursor.isNull(columnIndex);
    }

    public boolean move(int offset) {
        return moveToPosition(this.mPosition + offset);
    }

    public boolean moveToFirst() {
        return moveToPosition(0);
    }

    public boolean moveToLast() {
        return moveToPosition(getCount() - 1);
    }

    public boolean moveToNext() {
        int count = getCount();
        if (this.mPosition == count) {
            return false;
        }
        if (this.mPosition == count - 1) {
            this.mActiveCursor.moveToNext();
            this.mPosition++;
            return false;
        }
        int smallest = -1;
        int len = this.mCursors.length;
        for (int i = 0; i < len; i++) {
            if (!(this.mCursors[i] == null || this.mCursors[i].getCount() == 0 || this.mCursors[i].isLast())) {
                if (smallest == -1) {
                    smallest = i;
                    this.mCursors[smallest].moveToNext();
                } else {
                    Cursor left = this.mCursors[smallest];
                    Cursor right = this.mCursors[i];
                    right.moveToNext();
                    if (this.mComparator.compare(left, right) > 0) {
                        smallest = i;
                        left.moveToPrevious();
                    } else {
                        right.moveToPrevious();
                    }
                }
            }
        }
        this.mPosition++;
        if (smallest != -1) {
            this.mActiveCursorIndex = smallest;
            this.mActiveCursor = this.mCursors[this.mActiveCursorIndex];
        }
        return true;
    }

    public boolean moveToPosition(int position) {
        int count = getCount();
        if (position >= count) {
            this.mPosition = count;
            return false;
        } else if (position < 0) {
            this.mPosition = -1;
            return false;
        } else if (position == this.mPosition) {
            return true;
        } else {
            if (position > this.mPosition) {
                int end = position - this.mPosition;
                for (int i = 0; i < end; i++) {
                    if (!moveToNext()) {
                        return false;
                    }
                }
            } else {
                int end2 = this.mPosition - position;
                for (int i2 = 0; i2 < end2; i2++) {
                    if (!moveToPrevious()) {
                        return false;
                    }
                }
            }
            return true;
        }
    }

    public boolean moveToPrevious() {
        if (this.mPosition < 0) {
            return false;
        }
        this.mActiveCursor.moveToPrevious();
        if (this.mPosition == 0) {
            this.mPosition = -1;
            return false;
        }
        int greatest = -1;
        int len = this.mCursors.length;
        for (int i = 0; i < len; i++) {
            if (this.mCursors[i] != null && !this.mCursors[i].isBeforeFirst()) {
                if (greatest == -1) {
                    greatest = i;
                } else {
                    if (this.mComparator.compare(this.mCursors[greatest], this.mCursors[i]) <= 0) {
                        greatest = i;
                    }
                }
            }
        }
        this.mPosition--;
        if (greatest != -1) {
            this.mActiveCursorIndex = greatest;
            this.mActiveCursor = this.mCursors[this.mActiveCursorIndex];
        }
        return true;
    }

    public void registerContentObserver(ContentObserver observer) {
        for (Cursor cursor : this.mCursors) {
            cursor.registerContentObserver(observer);
        }
    }

    public void registerDataSetObserver(DataSetObserver observer) {
        for (Cursor cursor : this.mCursors) {
            cursor.registerDataSetObserver(observer);
        }
    }

    @Deprecated
    public boolean requery() {
        boolean success = true;
        for (Cursor cursor : this.mCursors) {
            success &= cursor.requery();
        }
        return success;
    }

    public void setNotificationUri(ContentResolver cr, Uri uri) {
        for (Cursor cursor : this.mCursors) {
            cursor.setNotificationUri(cr, uri);
        }
    }

    public void unregisterContentObserver(ContentObserver observer) {
        for (Cursor cursor : this.mCursors) {
            cursor.unregisterContentObserver(observer);
        }
    }

    public void unregisterDataSetObserver(DataSetObserver observer) {
        for (Cursor cursor : this.mCursors) {
            cursor.unregisterDataSetObserver(observer);
        }
    }

    public Bundle getExtras() {
        throw new RuntimeException("Not implemented");
    }

    public Bundle respond(Bundle extras) {
        throw new RuntimeException("Not implemented");
    }

    public Uri getNotificationUri() {
        return null;
    }
}
