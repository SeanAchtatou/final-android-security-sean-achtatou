package com.fsck.k9.helper;

import android.content.Context;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import com.fsck.k9.Account;
import com.fsck.k9.K9;
import com.fsck.k9.activity.FolderInfoHolder;
import com.fsck.k9.activity.MessageInfoHolder;
import com.fsck.k9.mail.Address;
import com.fsck.k9.mail.Flag;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mailstore.LocalMessage;
import yahoo.mail.app.R;

public class MessageHelper {
    private static final int TOO_MANY_ADDRESSES = 50;
    private static MessageHelper sInstance;
    private Context mContext;

    public static synchronized MessageHelper getInstance(Context context) {
        MessageHelper messageHelper;
        synchronized (MessageHelper.class) {
            if (sInstance == null) {
                sInstance = new MessageHelper(context);
            }
            messageHelper = sInstance;
        }
        return messageHelper;
    }

    private MessageHelper(Context context) {
        this.mContext = context;
    }

    public void populate(MessageInfoHolder target, LocalMessage message, FolderInfoHolder folder, Account account) {
        Contacts contactHelper = K9.showContactName() ? Contacts.getInstance(this.mContext) : null;
        target.message = message;
        target.compareArrival = message.getInternalDate();
        target.compareDate = message.getSentDate();
        if (target.compareDate == null) {
            target.compareDate = message.getInternalDate();
        }
        target.folder = folder;
        target.read = message.isSet(Flag.SEEN);
        target.answered = message.isSet(Flag.ANSWERED);
        target.forwarded = message.isSet(Flag.FORWARDED);
        target.flagged = message.isSet(Flag.FLAGGED);
        Address[] addrs = message.getFrom();
        if (addrs.length <= 0 || !account.isAnIdentity(addrs[0])) {
            target.sender = toFriendly(addrs, contactHelper);
            target.compareCounterparty = target.sender.toString();
        } else {
            CharSequence to = toFriendly(message.getRecipients(Message.RecipientType.TO), contactHelper);
            target.compareCounterparty = to.toString();
            target.sender = new SpannableStringBuilder(this.mContext.getString(R.string.message_to_label)).append(to);
        }
        if (addrs.length > 0) {
            target.senderAddress = addrs[0].getAddress();
        } else {
            target.senderAddress = target.compareCounterparty;
        }
        target.uid = message.getUid();
        target.account = message.getFolder().getAccountUuid();
        target.uri = message.getUri();
    }

    public CharSequence getDisplayName(Account account, Address[] fromAddrs, Address[] toAddrs) {
        Contacts contactHelper = K9.showContactName() ? Contacts.getInstance(this.mContext) : null;
        if (fromAddrs.length <= 0 || !account.isAnIdentity(fromAddrs[0])) {
            return toFriendly(fromAddrs, contactHelper);
        }
        return new SpannableStringBuilder(this.mContext.getString(R.string.message_to_label)).append(toFriendly(toAddrs, contactHelper));
    }

    public boolean toMe(Account account, Address[] toAddrs) {
        for (Address address : toAddrs) {
            if (account.isAnIdentity(address)) {
                return true;
            }
        }
        return false;
    }

    public static CharSequence toFriendly(Address address, Contacts contacts) {
        return toFriendly(address, contacts, K9.showCorrespondentNames(), K9.changeContactNameColor(), K9.getContactNameColor());
    }

    public static CharSequence toFriendly(Address[] addresses, Contacts contacts) {
        if (addresses == null) {
            return null;
        }
        if (addresses.length >= 50) {
            contacts = null;
        }
        SpannableStringBuilder sb = new SpannableStringBuilder();
        for (int i = 0; i < addresses.length; i++) {
            sb.append(toFriendly(addresses[i], contacts));
            if (i < addresses.length - 1) {
                sb.append(',');
            }
        }
        return sb;
    }

    static CharSequence toFriendly(Address address, Contacts contacts, boolean showCorrespondentNames, boolean changeContactNameColor, int contactNameColor) {
        String name;
        if (!showCorrespondentNames) {
            return address.getAddress();
        }
        if (contacts == null || (name = contacts.getNameForAddress(address.getAddress())) == null) {
            return !TextUtils.isEmpty(address.getPersonal()) ? address.getPersonal() : address.getAddress();
        } else if (!changeContactNameColor) {
            return name;
        } else {
            SpannableString coloredName = new SpannableString(name);
            coloredName.setSpan(new ForegroundColorSpan(contactNameColor), 0, coloredName.length(), 33);
            return coloredName;
        }
    }
}
