package com.fsck.k9.helper;

import android.net.Uri;
import com.fsck.k9.mail.Address;
import java.util.ArrayList;
import java.util.List;

public final class MailTo {
    private static final String BCC = "bcc";
    private static final String BODY = "body";
    private static final String CC = "cc";
    private static final Address[] EMPTY_ADDRESS_LIST = new Address[0];
    private static final String MAILTO_SCHEME = "mailto";
    private static final String SUBJECT = "subject";
    private static final String TO = "to";
    private final Address[] bccAddresses;
    private final String body;
    private final Address[] ccAddresses;
    private final String subject;
    private final Address[] toAddresses;

    public static boolean isMailTo(Uri uri) {
        return uri != null && MAILTO_SCHEME.equals(uri.getScheme());
    }

    public static MailTo parse(Uri uri) throws NullPointerException, IllegalArgumentException {
        if (uri == null || uri.toString() == null) {
            throw new NullPointerException("Argument 'uri' must not be null");
        } else if (!isMailTo(uri)) {
            throw new IllegalArgumentException("Not a mailto scheme");
        } else {
            String schemaSpecific = uri.getSchemeSpecificPart();
            int end = schemaSpecific.indexOf(63);
            if (end == -1) {
                end = schemaSpecific.length();
            }
            CaseInsensitiveParamWrapper params = new CaseInsensitiveParamWrapper(Uri.parse("foo://bar?" + uri.getEncodedQuery()));
            String recipient = Uri.decode(schemaSpecific.substring(0, end));
            List<String> toList = params.getQueryParameters(TO);
            if (recipient.length() != 0) {
                toList.add(0, recipient);
            }
            return new MailTo(toAddressArray(toList), toAddressArray(params.getQueryParameters(CC)), toAddressArray(params.getQueryParameters(BCC)), getFirstParameterValue(params, "subject"), getFirstParameterValue(params, BODY));
        }
    }

    private static String getFirstParameterValue(CaseInsensitiveParamWrapper params, String paramName) {
        List<String> paramValues = params.getQueryParameters(paramName);
        if (paramValues.isEmpty()) {
            return null;
        }
        return paramValues.get(0);
    }

    private static Address[] toAddressArray(List<String> recipients) {
        if (recipients.isEmpty()) {
            return EMPTY_ADDRESS_LIST;
        }
        return Address.parse(toCommaSeparatedString(recipients));
    }

    private static String toCommaSeparatedString(List<String> list) {
        StringBuilder stringBuilder = new StringBuilder();
        for (String item : list) {
            stringBuilder.append(item).append(',');
        }
        stringBuilder.setLength(stringBuilder.length() - 1);
        return stringBuilder.toString();
    }

    private MailTo(Address[] toAddresses2, Address[] ccAddresses2, Address[] bccAddresses2, String subject2, String body2) {
        this.toAddresses = toAddresses2;
        this.ccAddresses = ccAddresses2;
        this.bccAddresses = bccAddresses2;
        this.subject = subject2;
        this.body = body2;
    }

    public Address[] getTo() {
        return this.toAddresses;
    }

    public Address[] getCc() {
        return this.ccAddresses;
    }

    public Address[] getBcc() {
        return this.bccAddresses;
    }

    public String getSubject() {
        return this.subject;
    }

    public String getBody() {
        return this.body;
    }

    static class CaseInsensitiveParamWrapper {
        private final Uri uri;

        public CaseInsensitiveParamWrapper(Uri uri2) {
            this.uri = uri2;
        }

        public List<String> getQueryParameters(String key) {
            List<String> params = new ArrayList<>();
            for (String paramName : this.uri.getQueryParameterNames()) {
                if (paramName.equalsIgnoreCase(key)) {
                    params.addAll(this.uri.getQueryParameters(paramName));
                }
            }
            return params;
        }
    }
}
