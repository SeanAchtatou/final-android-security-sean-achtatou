package com.fsck.k9.helper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.widget.EditText;
import com.fsck.k9.K9;
import java.io.File;
import yahoo.mail.app.R;

public class FileBrowserHelper {
    private static final String[][] PICK_DIRECTORY_INTENTS = {new String[]{"org.openintents.action.PICK_DIRECTORY", "file://"}, new String[]{"com.estrongs.action.PICK_DIRECTORY", "file://"}, new String[]{"android.intent.action.PICK", "folder://"}, new String[]{"com.androidworkz.action.PICK_DIRECTORY", "file://"}};
    private static FileBrowserHelper sInstance;

    public interface FileBrowserFailOverCallback {
        void onCancel();

        void onPathEntered(String str);
    }

    private FileBrowserHelper() {
    }

    public static synchronized FileBrowserHelper getInstance() {
        FileBrowserHelper fileBrowserHelper;
        synchronized (FileBrowserHelper.class) {
            if (sInstance == null) {
                sInstance = new FileBrowserHelper();
            }
            fileBrowserHelper = sInstance;
        }
        return fileBrowserHelper;
    }

    public boolean showFileBrowserActivity(Activity c, File startPath, int requestcode, FileBrowserFailOverCallback callback) {
        boolean success = false;
        if (startPath == null) {
            startPath = new File(K9.getAttachmentDefaultPath());
        }
        int listIndex = 0;
        do {
            String intentAction = PICK_DIRECTORY_INTENTS[listIndex][0];
            String uriPrefix = PICK_DIRECTORY_INTENTS[listIndex][1];
            Intent intent = new Intent(intentAction);
            intent.setData(Uri.parse(uriPrefix + startPath.getPath()));
            try {
                c.startActivityForResult(intent, requestcode);
                success = true;
            } catch (ActivityNotFoundException e) {
                listIndex++;
            }
            if (success) {
                break;
            }
        } while (listIndex < PICK_DIRECTORY_INTENTS.length);
        if (listIndex != PICK_DIRECTORY_INTENTS.length) {
            return success;
        }
        showPathTextInput(c, startPath, callback);
        return false;
    }

    public boolean showFileBrowserActivity(Fragment c, File startPath, int requestcode, FileBrowserFailOverCallback callback) {
        boolean success = false;
        if (startPath == null) {
            startPath = new File(K9.getAttachmentDefaultPath());
        }
        int listIndex = 0;
        do {
            String intentAction = PICK_DIRECTORY_INTENTS[listIndex][0];
            String uriPrefix = PICK_DIRECTORY_INTENTS[listIndex][1];
            Intent intent = new Intent(intentAction);
            intent.setData(Uri.parse(uriPrefix + startPath.getPath()));
            try {
                c.startActivityForResult(intent, requestcode);
                success = true;
            } catch (ActivityNotFoundException e) {
                listIndex++;
            }
            if (success) {
                break;
            }
        } while (listIndex < PICK_DIRECTORY_INTENTS.length);
        if (listIndex != PICK_DIRECTORY_INTENTS.length) {
            return success;
        }
        showPathTextInput(c.getActivity(), startPath, callback);
        return false;
    }

    private void showPathTextInput(Activity c, File startPath, final FileBrowserFailOverCallback callback) {
        AlertDialog.Builder alert = new AlertDialog.Builder(c);
        alert.setTitle(c.getString(R.string.attachment_save_title));
        alert.setMessage(c.getString(R.string.attachment_save_desc));
        final EditText input = new EditText(c);
        input.setInputType(1);
        if (startPath != null) {
            input.setText(startPath.toString());
        }
        alert.setView(input);
        alert.setPositiveButton(c.getString(R.string.okay_action), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                callback.onPathEntered(input.getText().toString());
            }
        });
        alert.setNegativeButton(c.getString(R.string.cancel_action), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                callback.onCancel();
            }
        });
        alert.show();
    }
}
