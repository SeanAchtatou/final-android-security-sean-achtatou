package com.fsck.k9.helper;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Build;
import android.os.PowerManager;

public class K9AlarmManager {
    private final AlarmManager alarmManager;
    private final String packageName;
    private final PowerManager powerManager;

    K9AlarmManager(Context context) {
        this.alarmManager = (AlarmManager) context.getSystemService("alarm");
        this.powerManager = (PowerManager) context.getSystemService("power");
        this.packageName = context.getPackageName();
    }

    public static K9AlarmManager getAlarmManager(Context context) {
        return new K9AlarmManager(context);
    }

    public void set(int type, long triggerAtMillis, PendingIntent operation) {
        if (!isDozeSupported() || !isDozeWhiteListed()) {
            this.alarmManager.set(type, triggerAtMillis, operation);
        } else {
            setAndAllowWhileIdle(type, triggerAtMillis, operation);
        }
    }

    @TargetApi(23)
    public void setAndAllowWhileIdle(int type, long triggerAtMillis, PendingIntent operation) {
        this.alarmManager.setAndAllowWhileIdle(type, triggerAtMillis, operation);
    }

    public void cancel(PendingIntent operation) {
        this.alarmManager.cancel(operation);
    }

    /* access modifiers changed from: protected */
    public boolean isDozeSupported() {
        return Build.VERSION.SDK_INT >= 23;
    }

    @TargetApi(23)
    private boolean isDozeWhiteListed() {
        return this.powerManager.isIgnoringBatteryOptimizations(this.packageName);
    }
}
