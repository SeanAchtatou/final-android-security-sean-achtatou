package com.fsck.k9.helper;

import android.content.Context;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.QuickContactBadge;
import android.widget.TextView;
import com.fsck.k9.mail.Address;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.james.mime4j.util.MimeUtil;

public class Utility {
    private static final Pattern IMG_PATTERN = Pattern.compile(IMG_SRC_REGEX);
    private static final String IMG_SRC_REGEX = "(?is:<img[^>]+src\\s*=\\s*['\"]?([a-z]+)\\:)";
    private static final Pattern MESSAGE_ID = Pattern.compile("<(?:[a-zA-Z0-9!#$%&'*+\\-/=?^_`{|}~]+(?:\\.[a-zA-Z0-9!#$%&'*+\\-/=?^_`{|}~]+)*|\"(?:[^\\\\\"]|\\\\.)*\")@(?:[a-zA-Z0-9!#$%&'*+\\-/=?^_`{|}~]+(?:\\.[a-zA-Z0-9!#$%&'*+\\-/=?^_`{|}~]+)*|\\[(?:[^\\\\\\]]|\\\\.)*\\])>");
    private static final String NEWLINE_REGEX = "(?:\\r?\\n)";
    private static final Pattern RESPONSE_PATTERN = Pattern.compile("((Re|Fw|Fwd|Aw|R\\u00E9f\\.)(\\[\\d+\\])?[\\u00A0 ]?: *)+", 2);
    private static final Pattern TAG_PATTERN = Pattern.compile("\\[[-_a-z0-9]+\\] ", 2);
    private static Handler sMainThreadHandler;

    public static boolean arrayContains(Object[] a, Object o) {
        for (Object element : a) {
            if (element.equals(o)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isAnyMimeType(String o, String... a) {
        for (String element : a) {
            if (MimeUtil.isSameMimeType(element, o)) {
                return true;
            }
        }
        return false;
    }

    public static boolean arrayContainsAny(Object[] a, Object... o) {
        for (Object element : a) {
            if (arrayContains(o, element)) {
                return true;
            }
        }
        return false;
    }

    public static String combine(Object[] parts, char separator) {
        if (parts == null) {
            return null;
        }
        return TextUtils.join(String.valueOf(separator), parts);
    }

    public static String combine(Iterable<?> parts, char separator) {
        if (parts == null) {
            return null;
        }
        return TextUtils.join(String.valueOf(separator), parts);
    }

    public static boolean requiredFieldValid(TextView view) {
        return view.getText() != null && view.getText().length() > 0;
    }

    public static boolean requiredFieldValid(Editable s) {
        return s != null && s.length() > 0;
    }

    public static boolean domainFieldValid(EditText view) {
        if (view.getText() != null) {
            String s = view.getText().toString();
            if ((!s.matches("^([a-zA-Z0-9]([a-zA-Z0-9\\-]{0,61}[a-zA-Z0-9])?\\.)*[a-zA-Z0-9]([a-zA-Z0-9\\-]{0,61}[a-zA-Z0-9])?$") || s.length() > 253) && !s.matches("^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$")) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static String fastUrlDecode(String s) {
        byte[] bytes = s.getBytes(Charset.forName("UTF-8"));
        int length = 0;
        int i = 0;
        int count = bytes.length;
        while (i < count) {
            byte ch = bytes[i];
            if (ch == 37) {
                int h = bytes[i + 1] - 48;
                int l = bytes[i + 2] - 48;
                if (h > 9) {
                    h -= 7;
                }
                if (l > 9) {
                    l -= 7;
                }
                bytes[length] = (byte) ((h << 4) | l);
                i += 2;
            } else if (ch == 43) {
                bytes[length] = 32;
            } else {
                bytes[length] = bytes[i];
            }
            length++;
            i++;
        }
        return new String(bytes, 0, length, Charset.forName("UTF-8"));
    }

    public static void setCompoundDrawablesAlpha(TextView view, int alpha) {
    }

    public static String wrap(String str, int wrapLength) {
        StringBuilder result = new StringBuilder();
        for (String piece : str.split(NEWLINE_REGEX)) {
            result.append(wrap(piece, wrapLength, null, false));
            result.append("\r\n");
        }
        return result.toString();
    }

    public static String wrap(String str, int wrapLength, String newLineStr, boolean wrapLongWords) {
        if (str == null) {
            return null;
        }
        if (newLineStr == null) {
            newLineStr = "\r\n";
        }
        if (wrapLength < 1) {
            wrapLength = 1;
        }
        int inputLineLength = str.length();
        int offset = 0;
        StringBuilder wrappedLine = new StringBuilder(inputLineLength + 32);
        while (inputLineLength - offset > wrapLength) {
            if (str.charAt(offset) == ' ') {
                offset++;
            } else {
                int spaceToWrapAt = str.lastIndexOf(32, wrapLength + offset);
                if (spaceToWrapAt >= offset) {
                    wrappedLine.append(str.substring(offset, spaceToWrapAt));
                    wrappedLine.append(newLineStr);
                    offset = spaceToWrapAt + 1;
                } else if (wrapLongWords) {
                    wrappedLine.append(str.substring(offset, wrapLength + offset));
                    wrappedLine.append(newLineStr);
                    offset += wrapLength;
                } else {
                    int spaceToWrapAt2 = str.indexOf(32, wrapLength + offset);
                    if (spaceToWrapAt2 >= 0) {
                        wrappedLine.append(str.substring(offset, spaceToWrapAt2));
                        wrappedLine.append(newLineStr);
                        offset = spaceToWrapAt2 + 1;
                    } else {
                        wrappedLine.append(str.substring(offset));
                        offset = inputLineLength;
                    }
                }
            }
        }
        wrappedLine.append(str.substring(offset));
        return wrappedLine.toString();
    }

    public static String stripSubject(String subject) {
        int lastPrefix;
        int lastPrefix2 = 0;
        Matcher tagMatcher = TAG_PATTERN.matcher(subject);
        String tag = null;
        boolean tagPresent = false;
        boolean tagStripped = false;
        if (tagMatcher.find(0)) {
            tagPresent = true;
            if (tagMatcher.start() == 0) {
                tag = tagMatcher.group();
                lastPrefix2 = tagMatcher.end();
                tagStripped = true;
            }
        }
        Matcher matcher = RESPONSE_PATTERN.matcher(subject);
        while (lastPrefix < subject.length() - 1 && matcher.find(lastPrefix) && matcher.start() == lastPrefix && (!tagPresent || tag == null || subject.regionMatches(matcher.end(), tag, 0, tag.length()))) {
            lastPrefix = matcher.end();
            if (tagPresent) {
                tagStripped = false;
                if (tag == null) {
                    if (tagMatcher.start() == lastPrefix) {
                        tag = tagMatcher.group();
                        lastPrefix += tag.length();
                        tagStripped = true;
                    }
                } else if (lastPrefix < subject.length() - 1 && subject.startsWith(tag, lastPrefix)) {
                    lastPrefix += tag.length();
                    tagStripped = true;
                }
            }
        }
        if (tagStripped && tag != null) {
            lastPrefix -= tag.length();
        }
        if (lastPrefix <= -1 || lastPrefix >= subject.length() - 1) {
            return subject.trim();
        }
        return subject.substring(lastPrefix).trim();
    }

    /* JADX WARNING: Removed duplicated region for block: B:3:0x000d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean hasExternalImages(java.lang.String r5) {
        /*
            r2 = 1
            java.util.regex.Pattern r3 = com.fsck.k9.helper.Utility.IMG_PATTERN
            java.util.regex.Matcher r0 = r3.matcher(r5)
        L_0x0007:
            boolean r3 = r0.find()
            if (r3 == 0) goto L_0x002d
            java.lang.String r1 = r0.group(r2)
            java.lang.String r3 = "http"
            boolean r3 = r1.equals(r3)
            if (r3 != 0) goto L_0x0021
            java.lang.String r3 = "https"
            boolean r3 = r1.equals(r3)
            if (r3 == 0) goto L_0x0007
        L_0x0021:
            boolean r3 = com.fsck.k9.K9.DEBUG
            if (r3 == 0) goto L_0x002c
            java.lang.String r3 = "k9"
            java.lang.String r4 = "External images found"
            android.util.Log.d(r3, r4)
        L_0x002c:
            return r2
        L_0x002d:
            boolean r2 = com.fsck.k9.K9.DEBUG
            if (r2 == 0) goto L_0x0038
            java.lang.String r2 = "k9"
            java.lang.String r3 = "No external images."
            android.util.Log.d(r2, r3)
        L_0x0038:
            r2 = 0
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fsck.k9.helper.Utility.hasExternalImages(java.lang.String):boolean");
    }

    public static void closeQuietly(Cursor cursor) {
        if (cursor != null) {
            cursor.close();
        }
    }

    public static boolean hasConnectivity(Context context) {
        NetworkInfo netInfo;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null || (netInfo = connectivityManager.getActiveNetworkInfo()) == null || netInfo.getState() != NetworkInfo.State.CONNECTED) {
            return false;
        }
        return true;
    }

    public static List<String> extractMessageIds(String text) {
        List<String> messageIds = new ArrayList<>();
        Matcher matcher = MESSAGE_ID.matcher(text);
        for (int start = 0; matcher.find(start); start = matcher.end()) {
            messageIds.add(text.substring(matcher.start(), matcher.end()));
        }
        return messageIds;
    }

    public static String extractMessageId(String text) {
        Matcher matcher = MESSAGE_ID.matcher(text);
        if (matcher.find()) {
            return text.substring(matcher.start(), matcher.end());
        }
        return null;
    }

    public static Handler getMainThreadHandler() {
        if (sMainThreadHandler == null) {
            sMainThreadHandler = new Handler(Looper.getMainLooper());
        }
        return sMainThreadHandler;
    }

    public static void setContactForBadge(QuickContactBadge contactBadge, Address address) {
        if (Build.VERSION.SDK_INT >= 18) {
            Bundle extraContactInfo = new Bundle();
            extraContactInfo.putString("name", address.getPersonal());
            contactBadge.assignContactFromEmail(address.getAddress(), true, extraContactInfo);
            return;
        }
        contactBadge.assignContactFromEmail(address.getAddress(), true);
    }
}
