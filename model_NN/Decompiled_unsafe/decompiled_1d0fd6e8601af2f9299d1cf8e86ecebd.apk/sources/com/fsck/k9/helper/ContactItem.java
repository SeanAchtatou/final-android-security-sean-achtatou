package com.fsck.k9.helper;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

public class ContactItem implements Serializable {
    private static final long serialVersionUID = 4893328130147843375L;
    public final String displayName;
    public final List<String> emailAddresses;

    public ContactItem(String displayName2, List<String> emailAddresses2) {
        this.displayName = displayName2;
        this.emailAddresses = Collections.unmodifiableList(emailAddresses2);
    }
}
