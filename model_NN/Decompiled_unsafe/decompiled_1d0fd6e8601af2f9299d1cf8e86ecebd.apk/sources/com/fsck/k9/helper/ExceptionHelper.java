package com.fsck.k9.helper;

import com.fsck.k9.mail.MessagingException;

public class ExceptionHelper {
    public static String getRootCauseMessage(Throwable t) {
        Throwable nextCause;
        Throwable rootCause = t;
        do {
            nextCause = rootCause.getCause();
            if (nextCause != null) {
                rootCause = nextCause;
                continue;
            }
        } while (nextCause != null);
        if (rootCause instanceof MessagingException) {
            return rootCause.getMessage();
        }
        String simpleName = rootCause.getClass().getSimpleName();
        if (rootCause.getLocalizedMessage() != null) {
            return simpleName + ": " + rootCause.getLocalizedMessage();
        }
        return simpleName;
    }
}
