package com.fsck.k9.helper;

import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Locale;

public class FileHelper {
    private static final String INVALID_CHARACTERS = "[^\\w !#$%&'()\\-@\\^`{}~.,]";
    private static final String REPLACEMENT_CHARACTER = "_";

    public static File createUniqueFile(File directory, String filename) {
        String format;
        File file = new File(directory, filename);
        if (!file.exists()) {
            return file;
        }
        int index = filename.lastIndexOf(46);
        if (index != -1) {
            format = filename.substring(0, index) + "-%d" + filename.substring(index);
        } else {
            format = filename + "-%d";
        }
        for (int i = 2; i < Integer.MAX_VALUE; i++) {
            File file2 = new File(directory, String.format(Locale.US, format, Integer.valueOf(i)));
            if (!file2.exists()) {
                return file2;
            }
        }
        return null;
    }

    public static void touchFile(File parentDir, String name) {
        File file = new File(parentDir, name);
        try {
            if (!file.exists()) {
                if (!file.createNewFile()) {
                    Log.d("k9", "Unable to create file: " + file.getAbsolutePath());
                }
            } else if (!file.setLastModified(System.currentTimeMillis())) {
                Log.d("k9", "Unable to change last modification date: " + file.getAbsolutePath());
            }
        } catch (Exception e) {
            Log.d("k9", "Unable to touch file: " + file.getAbsolutePath(), e);
        }
    }

    public static boolean move(File from, File to) {
        FileOutputStream out;
        if (to.exists() && !to.delete()) {
            Log.d("k9", "Unable to delete file: " + to.getAbsolutePath());
        }
        if (!to.getParentFile().mkdirs()) {
            Log.d("k9", "Unable to make directories: " + to.getParentFile().getAbsolutePath());
        }
        try {
            FileInputStream in = new FileInputStream(from);
            try {
                out = new FileOutputStream(to);
                byte[] buffer = new byte[1024];
                while (true) {
                    int count = in.read(buffer);
                    if (count <= 0) {
                        break;
                    }
                    out.write(buffer, 0, count);
                }
                out.close();
                try {
                    in.close();
                } catch (Throwable th) {
                }
                if (!from.delete()) {
                    Log.d("k9", "Unable to delete file: " + from.getAbsolutePath());
                }
                return true;
            } catch (Throwable th2) {
                in.close();
                throw th2;
            }
        } catch (Exception e) {
            Log.w("k9", "cannot move " + from.getAbsolutePath() + " to " + to.getAbsolutePath(), e);
            return false;
        } catch (Throwable th3) {
        }
    }

    public static void moveRecursive(File fromDir, File toDir) {
        if (fromDir.exists()) {
            if (!fromDir.isDirectory()) {
                if (toDir.exists() && !toDir.delete()) {
                    Log.w("k9", "cannot delete already existing file/directory " + toDir.getAbsolutePath());
                }
                if (!fromDir.renameTo(toDir)) {
                    Log.w("k9", "cannot rename " + fromDir.getAbsolutePath() + " to " + toDir.getAbsolutePath() + " - moving instead");
                    move(fromDir, toDir);
                    return;
                }
                return;
            }
            if (!toDir.exists() || !toDir.isDirectory()) {
                if (toDir.exists() && !toDir.delete()) {
                    Log.d("k9", "Unable to delete file: " + toDir.getAbsolutePath());
                }
                if (!toDir.mkdirs()) {
                    Log.w("k9", "cannot create directory " + toDir.getAbsolutePath());
                }
            }
            for (File file : fromDir.listFiles()) {
                if (file.isDirectory()) {
                    moveRecursive(file, new File(toDir, file.getName()));
                    if (!file.delete()) {
                        Log.d("k9", "Unable to delete file: " + toDir.getAbsolutePath());
                    }
                } else {
                    File target = new File(toDir, file.getName());
                    if (!file.renameTo(target)) {
                        Log.w("k9", "cannot rename " + file.getAbsolutePath() + " to " + target.getAbsolutePath() + " - moving instead");
                        move(file, target);
                    }
                }
            }
            if (!fromDir.delete()) {
                Log.w("k9", "cannot delete " + fromDir.getAbsolutePath());
            }
        }
    }

    public static String sanitizeFilename(String filename) {
        return filename.replaceAll(INVALID_CHARACTERS, REPLACEMENT_CHARACTER);
    }
}
