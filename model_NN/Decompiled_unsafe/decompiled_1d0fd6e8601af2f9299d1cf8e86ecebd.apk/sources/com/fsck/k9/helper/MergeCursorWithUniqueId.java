package com.fsck.k9.helper;

import android.database.Cursor;
import com.fsck.k9.provider.AttachmentProvider;
import java.util.Comparator;

public class MergeCursorWithUniqueId extends MergeCursor {
    private static final long MAX_CURSORS = 32768;
    private static final long MAX_ID = 281474976710655L;
    private static final int SHIFT = 48;
    private int mColumnCount = -1;
    private int mIdColumnIndex = -1;

    public MergeCursorWithUniqueId(Cursor[] cursors, Comparator<Cursor> comparator) {
        super(cursors, comparator);
        if (((long) cursors.length) > 32768) {
            throw new IllegalArgumentException("This class only supports up to 32768 cursors");
        }
    }

    public int getColumnCount() {
        if (this.mColumnCount == -1) {
            this.mColumnCount = super.getColumnCount();
        }
        return this.mColumnCount + 1;
    }

    public int getColumnIndex(String columnName) {
        if (AttachmentProvider.AttachmentProviderColumns._ID.equals(columnName)) {
            return getUniqueIdColumnIndex();
        }
        return super.getColumnIndexOrThrow(columnName);
    }

    public int getColumnIndexOrThrow(String columnName) throws IllegalArgumentException {
        if (AttachmentProvider.AttachmentProviderColumns._ID.equals(columnName)) {
            return getUniqueIdColumnIndex();
        }
        return super.getColumnIndexOrThrow(columnName);
    }

    public long getLong(int columnIndex) {
        if (columnIndex != getUniqueIdColumnIndex()) {
            return super.getLong(columnIndex);
        }
        long id = getPerCursorId();
        if (id <= MAX_ID) {
            return (((long) this.mActiveCursorIndex) << 48) + id;
        }
        throw new RuntimeException("Sorry, " + getClass().getName() + " can only handle '_id' values up to " + 48 + " bits. This id is: " + id + " for column index: " + columnIndex + " columName: " + getColumnName(columnIndex));
    }

    /* access modifiers changed from: protected */
    public int getUniqueIdColumnIndex() {
        if (this.mColumnCount == -1) {
            this.mColumnCount = super.getColumnCount();
        }
        return this.mColumnCount;
    }

    /* access modifiers changed from: protected */
    public long getPerCursorId() {
        if (this.mIdColumnIndex == -1) {
            this.mIdColumnIndex = super.getColumnIndexOrThrow(AttachmentProvider.AttachmentProviderColumns._ID);
        }
        return super.getLong(this.mIdColumnIndex);
    }
}
