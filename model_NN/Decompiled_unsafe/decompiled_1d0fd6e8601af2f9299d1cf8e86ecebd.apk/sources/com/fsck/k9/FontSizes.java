package com.fsck.k9;

import android.widget.TextView;
import com.fsck.k9.preferences.GlobalSettings;
import com.fsck.k9.preferences.Storage;
import com.fsck.k9.preferences.StorageEditor;

public class FontSizes {
    private static final String ACCOUNT_DESCRIPTION = "fontSizeAccountDescription";
    private static final String ACCOUNT_NAME = "fontSizeAccountName";
    private static final String FOLDER_NAME = "fontSizeFolderName";
    private static final String FOLDER_STATUS = "fontSizeFolderStatus";
    public static final int FONT_10SP = 10;
    public static final int FONT_12SP = 12;
    public static final int FONT_16SP = 16;
    public static final int FONT_20SP = 20;
    public static final int FONT_DEFAULT = -1;
    public static final int LARGE = 22;
    public static final int MEDIUM = 18;
    private static final String MESSAGE_COMPOSE_INPUT = "fontSizeMessageComposeInput";
    private static final String MESSAGE_LIST_DATE = "fontSizeMessageListDate";
    private static final String MESSAGE_LIST_PREVIEW = "fontSizeMessageListPreview";
    private static final String MESSAGE_LIST_SENDER = "fontSizeMessageListSender";
    private static final String MESSAGE_LIST_SUBJECT = "fontSizeMessageListSubject";
    private static final String MESSAGE_VIEW_ADDITIONAL_HEADERS = "fontSizeMessageViewAdditionalHeaders";
    private static final String MESSAGE_VIEW_CC = "fontSizeMessageViewCC";
    private static final String MESSAGE_VIEW_CONTENT = "fontSizeMessageViewContent";
    private static final String MESSAGE_VIEW_CONTENT_PERCENT = "fontSizeMessageViewContentPercent";
    private static final String MESSAGE_VIEW_DATE = "fontSizeMessageViewDate";
    private static final String MESSAGE_VIEW_SENDER = "fontSizeMessageViewSender";
    private static final String MESSAGE_VIEW_SUBJECT = "fontSizeMessageViewSubject";
    private static final String MESSAGE_VIEW_TO = "fontSizeMessageViewTo";
    public static final int SMALL = 14;
    private int accountDescription = 12;
    private int accountName = 16;
    private int folderName = 16;
    private int folderStatus = 12;
    private int messageComposeInput = 16;
    private int messageListDate = 12;
    private int messageListPreview = 12;
    private int messageListSender = 16;
    private int messageListSubject = 12;
    private int messageViewAdditionalHeaders = -1;
    private int messageViewCC = -1;
    private int messageViewContentPercent = 100;
    private int messageViewDate = -1;
    private int messageViewSender = -1;
    private int messageViewSubject = -1;
    private int messageViewTo = -1;

    public void save(StorageEditor editor) {
        editor.putInt(ACCOUNT_NAME, this.accountName);
        editor.putInt(ACCOUNT_DESCRIPTION, this.accountDescription);
        editor.putInt(FOLDER_NAME, this.folderName);
        editor.putInt(FOLDER_STATUS, this.folderStatus);
        editor.putInt(MESSAGE_LIST_SUBJECT, this.messageListSubject);
        editor.putInt(MESSAGE_LIST_SENDER, this.messageListSender);
        editor.putInt(MESSAGE_LIST_DATE, this.messageListDate);
        editor.putInt(MESSAGE_LIST_PREVIEW, this.messageListPreview);
        editor.putInt(MESSAGE_VIEW_SENDER, this.messageViewSender);
        editor.putInt(MESSAGE_VIEW_TO, this.messageViewTo);
        editor.putInt(MESSAGE_VIEW_CC, this.messageViewCC);
        editor.putInt(MESSAGE_VIEW_ADDITIONAL_HEADERS, this.messageViewAdditionalHeaders);
        editor.putInt(MESSAGE_VIEW_SUBJECT, this.messageViewSubject);
        editor.putInt(MESSAGE_VIEW_DATE, this.messageViewDate);
        editor.putInt(MESSAGE_VIEW_CONTENT_PERCENT, getMessageViewContentAsPercent());
        editor.putInt(MESSAGE_COMPOSE_INPUT, this.messageComposeInput);
    }

    public void load(Storage storage) {
        this.accountName = storage.getInt(ACCOUNT_NAME, this.accountName);
        this.accountDescription = storage.getInt(ACCOUNT_DESCRIPTION, this.accountDescription);
        this.folderName = storage.getInt(FOLDER_NAME, this.folderName);
        this.folderStatus = storage.getInt(FOLDER_STATUS, this.folderStatus);
        this.messageListSubject = storage.getInt(MESSAGE_LIST_SUBJECT, this.messageListSubject);
        this.messageListSender = storage.getInt(MESSAGE_LIST_SENDER, this.messageListSender);
        this.messageListDate = storage.getInt(MESSAGE_LIST_DATE, this.messageListDate);
        this.messageListPreview = storage.getInt(MESSAGE_LIST_PREVIEW, this.messageListPreview);
        this.messageViewSender = storage.getInt(MESSAGE_VIEW_SENDER, this.messageViewSender);
        this.messageViewTo = storage.getInt(MESSAGE_VIEW_TO, this.messageViewTo);
        this.messageViewCC = storage.getInt(MESSAGE_VIEW_CC, this.messageViewCC);
        this.messageViewAdditionalHeaders = storage.getInt(MESSAGE_VIEW_ADDITIONAL_HEADERS, this.messageViewAdditionalHeaders);
        this.messageViewSubject = storage.getInt(MESSAGE_VIEW_SUBJECT, this.messageViewSubject);
        this.messageViewDate = storage.getInt(MESSAGE_VIEW_DATE, this.messageViewDate);
        loadMessageViewContentPercent(storage);
        this.messageComposeInput = storage.getInt(MESSAGE_COMPOSE_INPUT, this.messageComposeInput);
    }

    private void loadMessageViewContentPercent(Storage storage) {
        int fallbackValue = 100;
        if (!storage.contains(MESSAGE_VIEW_CONTENT_PERCENT)) {
            fallbackValue = GlobalSettings.SettingsUpgraderV31.convertFromOldSize(storage.getInt(MESSAGE_VIEW_CONTENT, 3));
        }
        setMessageViewContentAsPercent(storage.getInt(MESSAGE_VIEW_CONTENT_PERCENT, fallbackValue));
    }

    public int getAccountName() {
        return this.accountName;
    }

    public void setAccountName(int accountName2) {
        this.accountName = accountName2;
    }

    public int getAccountDescription() {
        return this.accountDescription;
    }

    public void setAccountDescription(int accountDescription2) {
        this.accountDescription = accountDescription2;
    }

    public int getFolderName() {
        return this.folderName;
    }

    public void setFolderName(int folderName2) {
        this.folderName = folderName2;
    }

    public int getFolderStatus() {
        return this.folderStatus;
    }

    public void setFolderStatus(int folderStatus2) {
        this.folderStatus = folderStatus2;
    }

    public int getMessageListSubject() {
        return this.messageListSubject;
    }

    public void setMessageListSubject(int messageListSubject2) {
        this.messageListSubject = messageListSubject2;
    }

    public int getMessageListSender() {
        return this.messageListSender;
    }

    public void setMessageListSender(int messageListSender2) {
        this.messageListSender = messageListSender2;
    }

    public int getMessageListDate() {
        return this.messageListDate;
    }

    public void setMessageListDate(int messageListDate2) {
        this.messageListDate = messageListDate2;
    }

    public int getMessageListPreview() {
        return this.messageListPreview;
    }

    public void setMessageListPreview(int messageListPreview2) {
        this.messageListPreview = messageListPreview2;
    }

    public int getMessageViewSender() {
        return this.messageViewSender;
    }

    public void setMessageViewSender(int messageViewSender2) {
        this.messageViewSender = messageViewSender2;
    }

    public int getMessageViewTo() {
        return this.messageViewTo;
    }

    public void setMessageViewTo(int messageViewTo2) {
        this.messageViewTo = messageViewTo2;
    }

    public int getMessageViewCC() {
        return this.messageViewCC;
    }

    public void setMessageViewCC(int messageViewCC2) {
        this.messageViewCC = messageViewCC2;
    }

    public int getMessageViewAdditionalHeaders() {
        return this.messageViewAdditionalHeaders;
    }

    public void setMessageViewAdditionalHeaders(int messageViewAdditionalHeaders2) {
        this.messageViewAdditionalHeaders = messageViewAdditionalHeaders2;
    }

    public int getMessageViewSubject() {
        return this.messageViewSubject;
    }

    public void setMessageViewSubject(int messageViewSubject2) {
        this.messageViewSubject = messageViewSubject2;
    }

    public int getMessageViewDate() {
        return this.messageViewDate;
    }

    public void setMessageViewDate(int messageViewDate2) {
        this.messageViewDate = messageViewDate2;
    }

    public int getMessageViewContentAsPercent() {
        return this.messageViewContentPercent;
    }

    public void setMessageViewContentAsPercent(int size) {
        this.messageViewContentPercent = size;
    }

    public int getMessageComposeInput() {
        return this.messageComposeInput;
    }

    public void setMessageComposeInput(int messageComposeInput2) {
        this.messageComposeInput = messageComposeInput2;
    }

    public void setViewTextSize(TextView v, int fontSize) {
        if (fontSize != -1) {
            v.setTextSize(2, (float) fontSize);
        }
    }
}
