package com.fsck.k9.fragment;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import atomicgonza.Views;
import com.fsck.k9.Account;
import com.fsck.k9.FontSizes;
import com.fsck.k9.K9;
import com.fsck.k9.Preferences;
import com.fsck.k9.activity.ActivityListener;
import com.fsck.k9.activity.ChooseFolder;
import com.fsck.k9.activity.FolderInfoHolder;
import com.fsck.k9.activity.MessageReference;
import com.fsck.k9.activity.Search;
import com.fsck.k9.activity.misc.ContactPictureLoader;
import com.fsck.k9.cache.EmailProviderCache;
import com.fsck.k9.controller.MessagingController;
import com.fsck.k9.fragment.ConfirmationDialogFragment;
import com.fsck.k9.fragment.MessageListFragmentComparators;
import com.fsck.k9.helper.ContactPicture;
import com.fsck.k9.helper.MergeCursorWithUniqueId;
import com.fsck.k9.helper.MessageHelper;
import com.fsck.k9.helper.Utility;
import com.fsck.k9.mail.Address;
import com.fsck.k9.mail.Flag;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mailstore.DatabasePreviewType;
import com.fsck.k9.mailstore.LocalFolder;
import com.fsck.k9.preferences.StorageEditor;
import com.fsck.k9.provider.AttachmentProvider;
import com.fsck.k9.provider.EmailProvider;
import com.fsck.k9.search.ConditionsTreeNode;
import com.fsck.k9.search.LocalSearch;
import com.fsck.k9.search.SearchSpecification;
import com.fsck.k9.search.SqlQueryBuilder;
import com.handmark.pulltorefresh.library.ILoadingLayout;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;
import yahoo.mail.app.R;

public class MessageListFragment extends Fragment implements AdapterView.OnItemClickListener, ConfirmationDialogFragment.ConfirmationDialogFragmentListener, LoaderManager.LoaderCallbacks<Cursor> {
    private static final int ACCOUNT_UUID_COLUMN = 17;
    private static final int ACTIVITY_CHOOSE_FOLDER_COPY = 2;
    private static final int ACTIVITY_CHOOSE_FOLDER_MOVE = 1;
    private static final int ANSWERED_COLUMN = 10;
    private static final String ARG_IS_THREAD_DISPLAY = "isThreadedDisplay";
    private static final String ARG_SEARCH = "searchObject";
    private static final String ARG_THREADED_LIST = "threadedList";
    static final int ATTACHMENT_COUNT_COLUMN = 12;
    private static final int CC_LIST_COLUMN = 7;
    static final int DATE_COLUMN = 4;
    static final int FLAGGED_COLUMN = 9;
    private static final int FOLDER_ID_COLUMN = 13;
    private static final int FOLDER_NAME_COLUMN = 18;
    private static final int FORWARDED_COLUMN = 11;
    private static final int ID_COLUMN = 0;
    static final int INTERNAL_DATE_COLUMN = 2;
    private static final int PREVIEW_COLUMN = 15;
    private static final int PREVIEW_TYPE_COLUMN = 14;
    private static final String[] PROJECTION = ((String[]) Arrays.copyOf(THREADED_PROJECTION, 19));
    static final int READ_COLUMN = 8;
    private static final int SENDER_LIST_COLUMN = 5;
    private static final Map<Account.SortType, Comparator<Cursor>> SORT_COMPARATORS;
    private static final String STATE_ACTIVE_MESSAGE = "activeMessage";
    private static final String STATE_MESSAGE_LIST = "listState";
    private static final String STATE_REMOTE_SEARCH_PERFORMED = "remoteSearchPerformed";
    private static final String STATE_SELECTED_MESSAGES = "selectedMessages";
    static final int SUBJECT_COLUMN = 3;
    private static final String[] THREADED_PROJECTION = {"id", EmailProvider.MessageColumns.UID, EmailProvider.MessageColumns.INTERNAL_DATE, "subject", "date", EmailProvider.MessageColumns.SENDER_LIST, EmailProvider.MessageColumns.TO_LIST, EmailProvider.MessageColumns.CC_LIST, EmailProvider.MessageColumns.READ, EmailProvider.MessageColumns.FLAGGED, EmailProvider.MessageColumns.ANSWERED, EmailProvider.MessageColumns.FORWARDED, EmailProvider.MessageColumns.ATTACHMENT_COUNT, EmailProvider.MessageColumns.FOLDER_ID, EmailProvider.MessageColumns.PREVIEW_TYPE, "preview", EmailProvider.ThreadColumns.ROOT, "account_uuid", "name", EmailProvider.SpecialColumns.THREAD_COUNT};
    private static final int THREAD_COUNT_COLUMN = 19;
    private static final int THREAD_ROOT_COLUMN = 16;
    private static final int TO_LIST_COLUMN = 6;
    private static final int UID_COLUMN = 1;
    Animation anim_out;
    /* access modifiers changed from: private */
    public Account mAccount;
    /* access modifiers changed from: private */
    public String[] mAccountUuids;
    /* access modifiers changed from: private */
    public ActionMode mActionMode;
    private ActionModeCallback mActionModeCallback = new ActionModeCallback();
    /* access modifiers changed from: private */
    public MessageReference mActiveMessage;
    private List<MessageReference> mActiveMessages;
    /* access modifiers changed from: private */
    public MessageListAdapter mAdapter;
    private boolean mAllAccounts;
    private BroadcastReceiver mCacheBroadcastReceiver;
    private IntentFilter mCacheIntentFilter;
    /* access modifiers changed from: private */
    public boolean mCheckboxes = true;
    /* access modifiers changed from: private */
    public ContactPictureLoader mContactsPictureLoader;
    /* access modifiers changed from: private */
    public Context mContext;
    private long mContextMenuUniqueId = 0;
    /* access modifiers changed from: private */
    public MessagingController mController;
    /* access modifiers changed from: private */
    public FolderInfoHolder mCurrentFolder;
    private boolean[] mCursorValid;
    private Cursor[] mCursors;
    public List<Message> mExtraSearchResults;
    /* access modifiers changed from: private */
    public String mFolderName;
    /* access modifiers changed from: private */
    public FontSizes mFontSizes = K9.getFontSizes();
    private View mFooterView;
    /* access modifiers changed from: private */
    public MessageListFragmentListener mFragmentListener;
    /* access modifiers changed from: private */
    public MessageListHandler mHandler = new MessageListHandler(this);
    private Boolean mHasConnectivity;
    /* access modifiers changed from: private */
    public LayoutInflater mInflater;
    private boolean mInitialized = false;
    private boolean mIsThreadDisplay;
    /* access modifiers changed from: private */
    public ListView mListView;
    private final ActivityListener mListener = new MessageListActivityListener();
    private boolean mLoaderJustInitialized;
    private LocalBroadcastManager mLocalBroadcastManager;
    MessageHelper mMessageHelper;
    /* access modifiers changed from: private */
    public Preferences mPreferences;
    /* access modifiers changed from: private */
    public int mPreviewLines = 0;
    /* access modifiers changed from: private */
    public PullToRefreshListView mPullToRefreshView;
    private Future<?> mRemoteSearchFuture = null;
    private boolean mRemoteSearchPerformed = false;
    /* access modifiers changed from: private */
    public Parcelable mSavedListState;
    /* access modifiers changed from: private */
    public LocalSearch mSearch = null;
    /* access modifiers changed from: private */
    public Set<Long> mSelected = new HashSet();
    /* access modifiers changed from: private */
    public int mSelectedCount = 0;
    /* access modifiers changed from: private */
    public boolean mSenderAboveSubject = false;
    /* access modifiers changed from: private */
    public boolean mSingleAccountMode;
    private boolean mSingleFolderMode;
    private boolean mSortAscending = true;
    private boolean mSortDateAscending = false;
    private Account.SortType mSortType = Account.SortType.SORT_DATE;
    /* access modifiers changed from: private */
    public boolean mStars = true;
    /* access modifiers changed from: private */
    public boolean mThreadedList;
    private String mTitle;
    /* access modifiers changed from: private */
    public int mUniqueIdColumn;
    /* access modifiers changed from: private */
    public int mUnreadMessageCount = 0;

    private enum FolderOperation {
        COPY,
        MOVE
    }

    public interface MessageListFragmentListener {
        void enableActionBarProgress(boolean z);

        void goBack();

        void onCompose(Account account);

        void onForward(MessageReference messageReference);

        void onReply(MessageReference messageReference);

        void onReplyAll(MessageReference messageReference);

        void onResendMessage(MessageReference messageReference);

        void openMessage(MessageReference messageReference);

        void remoteSearchStarted();

        void setMessageListProgress(int i);

        void setMessageListSubTitle(String str);

        void setMessageListTitle(String str);

        void setUnreadCount(int i);

        void showMoreFromSameSender(String str);

        void showThread(Account account, String str, long j);

        boolean startSearch(Account account, String str);

        void updateMenu();
    }

    public /* bridge */ /* synthetic */ void onLoadFinished(Loader loader, Object obj) {
        onLoadFinished((Loader<Cursor>) loader, (Cursor) obj);
    }

    static {
        Map<Account.SortType, Comparator<Cursor>> map = new EnumMap<>(Account.SortType.class);
        map.put(Account.SortType.SORT_ATTACHMENT, new MessageListFragmentComparators.AttachmentComparator());
        map.put(Account.SortType.SORT_DATE, new MessageListFragmentComparators.DateComparator());
        map.put(Account.SortType.SORT_ARRIVAL, new MessageListFragmentComparators.ArrivalComparator());
        map.put(Account.SortType.SORT_FLAGGED, new MessageListFragmentComparators.FlaggedComparator());
        map.put(Account.SortType.SORT_SUBJECT, new MessageListFragmentComparators.SubjectComparator());
        map.put(Account.SortType.SORT_SENDER, new MessageListFragmentComparators.SenderComparator());
        map.put(Account.SortType.SORT_UNREAD, new MessageListFragmentComparators.UnreadComparator());
        SORT_COMPARATORS = Collections.unmodifiableMap(map);
    }

    public static MessageListFragment newInstance(LocalSearch search, boolean isThreadDisplay, boolean threadedList) {
        MessageListFragment fragment = new MessageListFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_SEARCH, search);
        args.putBoolean(ARG_IS_THREAD_DISPLAY, isThreadDisplay);
        args.putBoolean(ARG_THREADED_LIST, threadedList);
        fragment.setArguments(args);
        return fragment;
    }

    static class MessageListHandler extends Handler {
        private static final int ACTION_FOLDER_LOADING = 1;
        private static final int ACTION_GO_BACK = 5;
        private static final int ACTION_OPEN_MESSAGE = 7;
        private static final int ACTION_PROGRESS = 3;
        private static final int ACTION_REFRESH_TITLE = 2;
        private static final int ACTION_REMOTE_SEARCH_FINISHED = 4;
        private static final int ACTION_RESTORE_LIST_POSITION = 6;
        private static final int ACTION_SET_REMOTE_PROGRESS = 8;
        /* access modifiers changed from: private */
        public WeakReference<MessageListFragment> mFragment;

        public MessageListHandler(MessageListFragment fragment) {
            this.mFragment = new WeakReference<>(fragment);
        }

        public void folderLoading(String folder, boolean loading) {
            int i;
            if (loading) {
                i = 1;
            } else {
                i = 0;
            }
            sendMessage(android.os.Message.obtain(this, 1, i, 0, folder));
        }

        public void refreshTitle() {
            sendMessage(android.os.Message.obtain(this, 2));
        }

        public void progress(boolean progress) {
            int i;
            if (progress) {
                i = 1;
            } else {
                i = 0;
            }
            sendMessage(android.os.Message.obtain(this, 3, i, 0));
        }

        public void remoteSearchFinished() {
            sendMessage(android.os.Message.obtain(this, 4));
        }

        public void updateFooter(final String message) {
            post(new Runnable() {
                public void run() {
                    MessageListFragment fragment = (MessageListFragment) MessageListHandler.this.mFragment.get();
                    if (fragment != null) {
                        fragment.updateFooter(message);
                    }
                }
            });
        }

        public void showRemoteProgress(boolean show) {
            sendMessage(android.os.Message.obtain(this, 8, Boolean.valueOf(show)));
        }

        public void goBack() {
            sendMessage(android.os.Message.obtain(this, 5));
        }

        public void restoreListPosition() {
            MessageListFragment fragment = this.mFragment.get();
            if (fragment != null) {
                android.os.Message msg = android.os.Message.obtain(this, 6, fragment.mSavedListState);
                Parcelable unused = fragment.mSavedListState = null;
                sendMessage(msg);
            }
        }

        public void openMessage(MessageReference messageReference) {
            sendMessage(android.os.Message.obtain(this, 7, messageReference));
        }

        public void handleMessage(android.os.Message msg) {
            boolean progress;
            boolean loading = true;
            MessageListFragment fragment = this.mFragment.get();
            if (fragment != null) {
                switch (msg.what) {
                    case 4:
                        fragment.remoteSearchFinished();
                        return;
                    default:
                        if (fragment.getActivity() != null) {
                            switch (msg.what) {
                                case 1:
                                    String folder = (String) msg.obj;
                                    if (msg.arg1 != 1) {
                                        loading = false;
                                    }
                                    fragment.folderLoading(folder, loading);
                                    return;
                                case 2:
                                    fragment.updateTitle();
                                    return;
                                case 3:
                                    if (msg.arg1 == 1) {
                                        progress = true;
                                    } else {
                                        progress = false;
                                    }
                                    fragment.progress(progress);
                                    return;
                                case 4:
                                default:
                                    return;
                                case 5:
                                    fragment.mFragmentListener.goBack();
                                    return;
                                case 6:
                                    fragment.mListView.onRestoreInstanceState((Parcelable) msg.obj);
                                    return;
                                case 7:
                                    fragment.mFragmentListener.openMessage((MessageReference) msg.obj);
                                    return;
                                case 8:
                                    fragment.setRemoteProgress(((Boolean) msg.obj).booleanValue());
                                    return;
                            }
                        } else {
                            return;
                        }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public Comparator<Cursor> getComparator() {
        List<Comparator<Cursor>> chain = new ArrayList<>(3);
        Comparator<Cursor> comparator = SORT_COMPARATORS.get(this.mSortType);
        if (this.mSortAscending) {
            chain.add(comparator);
        } else {
            chain.add(new MessageListFragmentComparators.ReverseComparator(comparator));
        }
        if (!(this.mSortType == Account.SortType.SORT_DATE || this.mSortType == Account.SortType.SORT_ARRIVAL)) {
            Comparator<Cursor> dateComparator = SORT_COMPARATORS.get(Account.SortType.SORT_DATE);
            if (this.mSortDateAscending) {
                chain.add(dateComparator);
            } else {
                chain.add(new MessageListFragmentComparators.ReverseComparator(dateComparator));
            }
        }
        chain.add(new MessageListFragmentComparators.ReverseIdComparator());
        return new MessageListFragmentComparators.ComparatorChain(chain);
    }

    /* access modifiers changed from: private */
    public void folderLoading(String folder, boolean loading) {
        if (this.mCurrentFolder != null && this.mCurrentFolder.name.equals(folder)) {
            this.mCurrentFolder.loading = loading;
        }
        updateMoreMessagesOfCurrentFolder();
        updateFooterView();
    }

    public void updateTitle() {
        if (this.mInitialized) {
            setWindowTitle();
            if (!this.mSearch.isManualSearch()) {
                setWindowProgress();
            }
        }
    }

    private void setWindowProgress() {
        int divisor;
        int level = 10000;
        if (this.mCurrentFolder != null && this.mCurrentFolder.loading && this.mListener.getFolderTotal() > 0 && (divisor = this.mListener.getFolderTotal()) != 0 && (level = (10000 / divisor) * this.mListener.getFolderCompleted()) > 10000) {
            level = 10000;
        }
        this.mFragmentListener.setMessageListProgress(level);
    }

    private void setWindowTitle() {
        if (isManualSearch() || !this.mSingleFolderMode) {
            if (this.mTitle != null) {
                this.mFragmentListener.setMessageListTitle(this.mTitle);
            } else {
                this.mFragmentListener.setMessageListTitle(getString(R.string.search_results));
            }
            this.mFragmentListener.setMessageListSubTitle(null);
        } else {
            Activity activity = getActivity();
            this.mFragmentListener.setMessageListTitle(FolderInfoHolder.getDisplayName(activity, this.mAccount, this.mFolderName));
            String operation = this.mListener.getOperation(activity);
            if (operation.length() < 1) {
                this.mFragmentListener.setMessageListSubTitle(this.mAccount.getEmail());
            } else {
                this.mFragmentListener.setMessageListSubTitle(operation);
            }
        }
        if (this.mUnreadMessageCount <= 0) {
            this.mFragmentListener.setUnreadCount(0);
        } else if (this.mSingleFolderMode || this.mTitle != null) {
            this.mFragmentListener.setUnreadCount(this.mUnreadMessageCount);
        } else {
            this.mFragmentListener.setUnreadCount(0);
        }
    }

    /* access modifiers changed from: private */
    public void progress(boolean progress) {
        this.mFragmentListener.enableActionBarProgress(progress);
        if (this.mPullToRefreshView != null && !progress) {
            this.mPullToRefreshView.onRefreshComplete();
        }
    }

    /* access modifiers changed from: private */
    public void setRemoteProgress(boolean progress) {
        if (progress) {
            Views.show(getActivity().findViewById(R.id.progress_load));
        } else {
            Views.hide(getActivity().findViewById(R.id.progress_load));
        }
    }

    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (view != this.mFooterView) {
            Cursor cursor = (Cursor) parent.getItemAtPosition(position);
            if (cursor == null) {
                return;
            }
            if (this.mSelectedCount > 0) {
                toggleMessageSelect(position);
                setImageViewAnimatedChange(this.mContext, (ImageView) view.findViewById(R.id.contact_badge));
            } else if (!this.mThreadedList || cursor.getInt(19) <= 1) {
                openMessageAtPosition(listViewToAdapterPosition(position));
            } else {
                this.mFragmentListener.showThread(getAccountFromCursor(cursor), cursor.getString(18), cursor.getLong(16));
            }
        } else if (this.mCurrentFolder != null && !this.mSearch.isManualSearch() && this.mCurrentFolder.moreMessages) {
            this.mController.loadMoreMessages(this.mAccount, this.mFolderName, null);
        } else if (this.mCurrentFolder != null && isRemoteSearch() && this.mExtraSearchResults != null && this.mExtraSearchResults.size() > 0) {
            int numResults = this.mExtraSearchResults.size();
            int limit = this.mAccount.getRemoteSearchNumResults();
            List<Message> toProcess = this.mExtraSearchResults;
            if (limit <= 0 || numResults <= limit) {
                this.mExtraSearchResults = null;
                updateFooter(null);
            } else {
                toProcess = toProcess.subList(0, limit);
                this.mExtraSearchResults = this.mExtraSearchResults.subList(limit, this.mExtraSearchResults.size());
            }
            this.mController.loadSearchResults(this.mAccount, this.mCurrentFolder.name, toProcess, this.mListener);
        }
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.mContext = activity.getApplicationContext();
        try {
            this.mFragmentListener = (MessageListFragmentListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.getClass() + " must implement MessageListFragmentListener");
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Context appContext = getActivity().getApplicationContext();
        this.mPreferences = Preferences.getPreferences(appContext);
        this.mController = MessagingController.getInstance(getActivity().getApplication());
        this.mPreviewLines = K9.messageListPreviewLines();
        this.mCheckboxes = K9.messageListCheckboxes();
        this.mStars = K9.messageListStars();
        if (K9.showContactPicture()) {
            this.mContactsPictureLoader = ContactPicture.getContactPictureLoader(getActivity());
        }
        restoreInstanceState(savedInstanceState);
        decodeArguments();
        createCacheBroadcastReceiver(appContext);
        this.mInitialized = true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.mInflater = inflater;
        View view = inflater.inflate((int) R.layout.message_list_fragment, container, false);
        initializePullToRefresh(inflater, view);
        initializeLayout();
        this.mListView.setVerticalFadingEdgeEnabled(false);
        return view;
    }

    public void onDestroyView() {
        this.mSavedListState = this.mListView.onSaveInstanceState();
        super.onDestroyView();
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.mMessageHelper = MessageHelper.getInstance(getActivity());
        initializeMessageList();
        initializeSortSettings();
        this.mLoaderJustInitialized = true;
        LoaderManager loaderManager = getLoaderManager();
        int len = this.mAccountUuids.length;
        this.mCursors = new Cursor[len];
        this.mCursorValid = new boolean[len];
        for (int i = 0; i < len; i++) {
            loaderManager.initLoader(i, null, this);
            this.mCursorValid[i] = false;
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        saveSelectedMessages(outState);
        saveListState(outState);
        outState.putBoolean(STATE_REMOTE_SEARCH_PERFORMED, this.mRemoteSearchPerformed);
        outState.putParcelable(STATE_ACTIVE_MESSAGE, this.mActiveMessage);
    }

    private void restoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            restoreSelectedMessages(savedInstanceState);
            this.mRemoteSearchPerformed = savedInstanceState.getBoolean(STATE_REMOTE_SEARCH_PERFORMED);
            this.mSavedListState = savedInstanceState.getParcelable(STATE_MESSAGE_LIST);
            this.mActiveMessage = (MessageReference) savedInstanceState.getParcelable(STATE_ACTIVE_MESSAGE);
        }
    }

    private void saveSelectedMessages(Bundle outState) {
        long[] selected = new long[this.mSelected.size()];
        int i = 0;
        for (Long id : this.mSelected) {
            selected[i] = id.longValue();
            i++;
        }
        outState.putLongArray(STATE_SELECTED_MESSAGES, selected);
    }

    private void restoreSelectedMessages(Bundle savedInstanceState) {
        for (long id : savedInstanceState.getLongArray(STATE_SELECTED_MESSAGES)) {
            this.mSelected.add(Long.valueOf(id));
        }
    }

    private void saveListState(Bundle outState) {
        if (this.mSavedListState != null) {
            outState.putParcelable(STATE_MESSAGE_LIST, this.mSavedListState);
        } else if (this.mListView != null) {
            outState.putParcelable(STATE_MESSAGE_LIST, this.mListView.onSaveInstanceState());
        }
    }

    private void initializeSortSettings() {
        if (this.mSingleAccountMode) {
            this.mSortType = this.mAccount.getSortType();
            this.mSortAscending = this.mAccount.isSortAscending(this.mSortType);
            this.mSortDateAscending = this.mAccount.isSortAscending(Account.SortType.SORT_DATE);
            return;
        }
        this.mSortType = K9.getSortType();
        this.mSortAscending = K9.isSortAscending(this.mSortType);
        this.mSortDateAscending = K9.isSortAscending(Account.SortType.SORT_DATE);
    }

    private void decodeArguments() {
        Bundle args = getArguments();
        this.mThreadedList = args.getBoolean(ARG_THREADED_LIST, false);
        this.mIsThreadDisplay = args.getBoolean(ARG_IS_THREAD_DISPLAY, false);
        this.mSearch = (LocalSearch) args.getParcelable(ARG_SEARCH);
        this.mTitle = this.mSearch.getName();
        String[] accountUuids = this.mSearch.getAccountUuids();
        this.mSingleAccountMode = false;
        if (accountUuids.length == 1 && !this.mSearch.searchAllAccounts()) {
            this.mSingleAccountMode = true;
            this.mAccount = this.mPreferences.getAccount(accountUuids[0]);
        }
        this.mSingleFolderMode = false;
        if (this.mSingleAccountMode && this.mSearch.getFolderNames().size() == 1) {
            this.mSingleFolderMode = true;
            this.mFolderName = this.mSearch.getFolderNames().get(0);
            this.mCurrentFolder = getFolderInfoHolder(this.mFolderName, this.mAccount);
        }
        this.mAllAccounts = false;
        if (this.mSingleAccountMode) {
            this.mAccountUuids = new String[]{this.mAccount.getUuid()};
        } else if (accountUuids.length != 1 || !accountUuids[0].equals(SearchSpecification.ALL_ACCOUNTS)) {
            this.mAccountUuids = accountUuids;
        } else {
            this.mAllAccounts = true;
            List<Account> accounts = this.mPreferences.getAccounts();
            this.mAccountUuids = new String[accounts.size()];
            int len = accounts.size();
            for (int i = 0; i < len; i++) {
                this.mAccountUuids[i] = accounts.get(i).getUuid();
            }
            if (this.mAccountUuids.length == 1) {
                this.mSingleAccountMode = true;
                this.mAccount = accounts.get(0);
            }
        }
    }

    private void initializeMessageList() {
        this.mAdapter = new MessageListAdapter();
        if (this.mFolderName != null) {
            this.mCurrentFolder = getFolderInfoHolder(this.mFolderName, this.mAccount);
        }
        if (this.mSingleFolderMode) {
            this.mListView.addFooterView(getFooterView(this.mListView));
            updateFooterView();
        }
        this.mListView.setAdapter((ListAdapter) this.mAdapter);
    }

    private void createCacheBroadcastReceiver(Context appContext) {
        this.mLocalBroadcastManager = LocalBroadcastManager.getInstance(appContext);
        this.mCacheBroadcastReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                MessageListFragment.this.mAdapter.notifyDataSetChanged();
            }
        };
        this.mCacheIntentFilter = new IntentFilter(EmailProviderCache.ACTION_CACHE_UPDATED);
    }

    private FolderInfoHolder getFolderInfoHolder(String folderName, Account account) {
        try {
            return new FolderInfoHolder(this.mContext, getFolder(folderName, account), account);
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    private LocalFolder getFolder(String folderName, Account account) throws MessagingException {
        LocalFolder localFolder = account.getLocalStore().getFolder(folderName);
        localFolder.open(1);
        return localFolder;
    }

    public void onPause() {
        super.onPause();
        this.mLocalBroadcastManager.unregisterReceiver(this.mCacheBroadcastReceiver);
        this.mListener.onPause(getActivity());
        this.mController.removeListener(this.mListener);
    }

    public void onResume() {
        List<Account> accountsWithNotification;
        super.onResume();
        this.mSenderAboveSubject = K9.messageListSenderAboveSubject();
        if (!this.mLoaderJustInitialized) {
            restartLoader();
        } else {
            this.mLoaderJustInitialized = false;
        }
        if (this.mHasConnectivity == null) {
            this.mHasConnectivity = Boolean.valueOf(Utility.hasConnectivity(getActivity().getApplication()));
        }
        this.mLocalBroadcastManager.registerReceiver(this.mCacheBroadcastReceiver, this.mCacheIntentFilter);
        this.mListener.onResume(getActivity());
        this.mController.addListener(this.mListener);
        Account account = this.mAccount;
        if (account != null) {
            accountsWithNotification = Collections.singletonList(account);
        } else {
            accountsWithNotification = this.mPreferences.getAccounts();
        }
        for (Account accountWithNotification : accountsWithNotification) {
            this.mController.cancelNotificationsForAccount(accountWithNotification);
        }
        if (!(this.mAccount == null || this.mFolderName == null || this.mSearch.isManualSearch())) {
            this.mController.getFolderUnreadMessageCount(this.mAccount, this.mFolderName, this.mListener);
        }
        updateTitle();
        if ((getActivity() instanceof Search) && this.mAccount != null && this.mCurrentFolder != null && this.mSavedListState == null) {
            onRemoteSearch();
        }
        if (!(getActivity() instanceof Search) && this.mController != null && this.mController.isRemoteSearchRequested) {
            this.mController.isRemoteSearchRequested = false;
            if (this.mController.isFetchingSearchList) {
                this.mController.stopFetchingSearchedMessages = true;
            }
            if (account != null) {
                this.mController.synchronizeMailbox(this.mAccount, this.mFolderName, this.mListener, null);
            }
        }
    }

    private void restartLoader() {
        if (this.mCursorValid != null) {
            LoaderManager loaderManager = getLoaderManager();
            for (int i = 0; i < this.mAccountUuids.length; i++) {
                loaderManager.restartLoader(i, null, this);
                this.mCursorValid[i] = false;
            }
        }
    }

    private void initializePullToRefresh(LayoutInflater inflater, View layout) {
        this.mPullToRefreshView = (PullToRefreshListView) layout.findViewById(R.id.message_list);
        this.mPullToRefreshView.setEmptyView(inflater.inflate((int) R.layout.message_list_loading, (ViewGroup) null));
        if (isRemoteSearchAllowed()) {
            this.mPullToRefreshView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
                public void onRefresh(PullToRefreshBase<ListView> pullToRefreshBase) {
                    MessageListFragment.this.mPullToRefreshView.onRefreshComplete();
                    MessageListFragment.this.onRemoteSearchRequested();
                }
            });
            ILoadingLayout proxy = this.mPullToRefreshView.getLoadingLayoutProxy();
            proxy.setPullLabel(getString(R.string.pull_to_refresh_remote_search_from_local_search_pull));
            proxy.setReleaseLabel(getString(R.string.pull_to_refresh_remote_search_from_local_search_release));
        } else if (isCheckMailSupported()) {
            this.mPullToRefreshView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
                public void onRefresh(PullToRefreshBase<ListView> pullToRefreshBase) {
                    MessageListFragment.this.checkMail();
                }
            });
        }
        setPullToRefreshEnabled(false);
    }

    private void setPullToRefreshEnabled(boolean enable) {
        this.mPullToRefreshView.setMode(enable ? PullToRefreshBase.Mode.PULL_FROM_START : PullToRefreshBase.Mode.DISABLED);
    }

    private void initializeLayout() {
        this.mListView = (ListView) this.mPullToRefreshView.getRefreshableView();
        this.mListView.setLongClickable(true);
        this.mListView.setOnItemClickListener(this);
        registerForContextMenu(this.mListView);
    }

    public void onCompose() {
        if (!this.mSingleAccountMode) {
            this.mFragmentListener.onCompose(null);
        } else {
            this.mFragmentListener.onCompose(this.mAccount);
        }
    }

    public void onReply(MessageReference messageReference) {
        this.mFragmentListener.onReply(messageReference);
    }

    public void onReplyAll(MessageReference messageReference) {
        this.mFragmentListener.onReplyAll(messageReference);
    }

    public void onForward(MessageReference messageReference) {
        this.mFragmentListener.onForward(messageReference);
    }

    public void onResendMessage(MessageReference messageReference) {
        this.mFragmentListener.onResendMessage(messageReference);
    }

    public void changeSort(Account.SortType sortType) {
        Boolean sortAscending;
        if (this.mSortType == sortType) {
            sortAscending = Boolean.valueOf(!this.mSortAscending);
        } else {
            sortAscending = null;
        }
        changeSort(sortType, sortAscending);
    }

    public void onRemoteSearchRequested() {
        String searchAccount = this.mAccount.getUuid();
        String searchFolder = this.mCurrentFolder.name;
        String queryString = this.mSearch.getRemoteSearchArguments();
        this.mRemoteSearchPerformed = true;
        this.mController.stopFetchingSearchedMessages = false;
        this.mRemoteSearchFuture = this.mController.searchRemoteMessages(searchAccount, searchFolder, queryString, null, null, this.mListener);
        setPullToRefreshEnabled(false);
        this.mFragmentListener.remoteSearchStarted();
    }

    private void changeSort(Account.SortType sortType, Boolean sortAscending) {
        this.mSortType = sortType;
        Account account = this.mAccount;
        if (account != null) {
            account.setSortType(this.mSortType);
            if (sortAscending == null) {
                this.mSortAscending = account.isSortAscending(this.mSortType);
            } else {
                this.mSortAscending = sortAscending.booleanValue();
            }
            account.setSortAscending(this.mSortType, this.mSortAscending);
            this.mSortDateAscending = account.isSortAscending(Account.SortType.SORT_DATE);
            account.save(this.mPreferences);
        } else {
            K9.setSortType(this.mSortType);
            if (sortAscending == null) {
                this.mSortAscending = K9.isSortAscending(this.mSortType);
            } else {
                this.mSortAscending = sortAscending.booleanValue();
            }
            K9.setSortAscending(this.mSortType, this.mSortAscending);
            this.mSortDateAscending = K9.isSortAscending(Account.SortType.SORT_DATE);
            StorageEditor editor = this.mPreferences.getStorage().edit();
            K9.save(editor);
            editor.commit();
        }
        reSort();
    }

    private void reSort() {
        Toast.makeText(getActivity(), this.mSortType.getToast(this.mSortAscending), 0).show();
        LoaderManager loaderManager = getLoaderManager();
        int len = this.mAccountUuids.length;
        for (int i = 0; i < len; i++) {
            loaderManager.restartLoader(i, null, this);
        }
    }

    public void onCycleSort() {
        Account.SortType[] sorts = Account.SortType.values();
        int curIndex = 0;
        int i = 0;
        while (true) {
            if (i >= sorts.length) {
                break;
            } else if (sorts[i] == this.mSortType) {
                curIndex = i;
                break;
            } else {
                i++;
            }
        }
        int curIndex2 = curIndex + 1;
        if (curIndex2 == sorts.length) {
            curIndex2 = 0;
        }
        changeSort(sorts[curIndex2]);
    }

    private void onDelete(MessageReference message) {
        onDelete(Collections.singletonList(message));
    }

    /* access modifiers changed from: private */
    public void onDelete(List<MessageReference> messages) {
        if (K9.confirmDelete()) {
            this.mActiveMessages = messages;
            showDialog(R.id.dialog_confirm_delete);
            return;
        }
        onDeleteConfirmed(messages);
    }

    private void onDeleteConfirmed(List<MessageReference> messages) {
        if (this.mThreadedList) {
            this.mController.deleteThreads(messages);
        } else {
            this.mController.deleteMessages(messages, null);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == -1) {
            switch (requestCode) {
                case 1:
                case 2:
                    if (data != null) {
                        String destFolderName = data.getStringExtra(ChooseFolder.EXTRA_NEW_FOLDER);
                        List<MessageReference> messages = this.mActiveMessages;
                        if (destFolderName != null) {
                            this.mActiveMessages = null;
                            if (messages.size() > 0) {
                                try {
                                    MessageReference firstMsg = messages.get(0);
                                    getFolder(firstMsg.getFolderName(), this.mPreferences.getAccount(firstMsg.getAccountUuid())).setLastSelectedFolderName(destFolderName);
                                } catch (MessagingException e) {
                                    Log.e("k9", "Error getting folder for setLastSelectedFolderName()", e);
                                }
                            }
                            switch (requestCode) {
                                case 1:
                                    move(messages, destFolderName);
                                    return;
                                case 2:
                                    copy(messages, destFolderName);
                                    return;
                                default:
                                    return;
                            }
                        } else {
                            return;
                        }
                    } else {
                        return;
                    }
                default:
                    return;
            }
        }
    }

    public void onExpunge() {
        if (this.mCurrentFolder != null) {
            onExpunge(this.mAccount, this.mCurrentFolder.name);
        }
    }

    private void onExpunge(Account account, String folderName) {
        this.mController.expunge(account, folderName);
    }

    private void showDialog(int dialogId) {
        DialogFragment fragment;
        switch (dialogId) {
            case R.id.dialog_confirm_delete /*2131755014*/:
                String title = getString(R.string.dialog_confirm_delete_title);
                int selectionSize = this.mActiveMessages.size();
                fragment = ConfirmationDialogFragment.newInstance(dialogId, title, getResources().getQuantityString(R.plurals.dialog_confirm_delete_messages, selectionSize, Integer.valueOf(selectionSize)), getString(R.string.dialog_confirm_delete_confirm_button), getString(R.string.dialog_confirm_delete_cancel_button));
                break;
            case R.id.dialog_confirm_mark_all_as_read /*2131755015*/:
                fragment = ConfirmationDialogFragment.newInstance(dialogId, getString(R.string.dialog_confirm_mark_all_as_read_title), getString(R.string.dialog_confirm_mark_all_as_read_message), getString(R.string.dialog_confirm_mark_all_as_read_confirm_button), getString(R.string.dialog_confirm_mark_all_as_read_cancel_button));
                break;
            case R.id.dialog_confirm_spam /*2131755016*/:
                String title2 = getString(R.string.dialog_confirm_spam_title);
                int selectionSize2 = this.mActiveMessages.size();
                fragment = ConfirmationDialogFragment.newInstance(dialogId, title2, getResources().getQuantityString(R.plurals.dialog_confirm_spam_message, selectionSize2, Integer.valueOf(selectionSize2)), getString(R.string.dialog_confirm_spam_confirm_button), getString(R.string.dialog_confirm_spam_cancel_button));
                break;
            default:
                throw new RuntimeException("Called showDialog(int) with unknown dialog id.");
        }
        fragment.setTargetFragment(this, dialogId);
        fragment.show(getFragmentManager(), getDialogTag(dialogId));
    }

    private String getDialogTag(int dialogId) {
        return "dialog-" + dialogId;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        switch (itemId) {
            case R.id.select_all /*2131755489*/:
                selectAll();
                return true;
            case R.id.set_sort_date /*2131755502*/:
                changeSort(Account.SortType.SORT_DATE);
                return true;
            case R.id.set_sort_arrival /*2131755503*/:
                changeSort(Account.SortType.SORT_ARRIVAL);
                return true;
            case R.id.set_sort_subject /*2131755504*/:
                changeSort(Account.SortType.SORT_SUBJECT);
                return true;
            case R.id.set_sort_sender /*2131755505*/:
                changeSort(Account.SortType.SORT_SENDER);
                return true;
            case R.id.set_sort_flag /*2131755506*/:
                changeSort(Account.SortType.SORT_FLAGGED);
                return true;
            case R.id.set_sort_unread /*2131755507*/:
                changeSort(Account.SortType.SORT_UNREAD);
                return true;
            case R.id.set_sort_attach /*2131755508*/:
                changeSort(Account.SortType.SORT_ATTACHMENT);
                return true;
            default:
                if (!this.mSingleAccountMode) {
                    return false;
                }
                switch (itemId) {
                    case R.id.send_messages /*2131755461*/:
                        onSendPendingMessages();
                        return true;
                    case R.id.expunge /*2131755520*/:
                        if (this.mCurrentFolder == null) {
                            return true;
                        }
                        onExpunge(this.mAccount, this.mCurrentFolder.name);
                        return true;
                    default:
                        return super.onOptionsItemSelected(item);
                }
        }
    }

    public void onSendPendingMessages() {
        this.mController.sendPendingMessages(this.mAccount, null);
    }

    public boolean onContextItemSelected(MenuItem item) {
        int adapterPosition;
        if (this.mContextMenuUniqueId == 0 || (adapterPosition = getPositionForUniqueId(this.mContextMenuUniqueId)) == -1) {
            return false;
        }
        switch (item.getItemId()) {
            case R.id.delete /*2131755480*/:
                onDelete(getMessageAtPosition(adapterPosition));
                break;
            case R.id.mark_as_read /*2131755481*/:
                setFlag(adapterPosition, Flag.SEEN, true);
                break;
            case R.id.mark_as_unread /*2131755482*/:
                setFlag(adapterPosition, Flag.SEEN, false);
                break;
            case R.id.archive /*2131755483*/:
                onArchive(getMessageAtPosition(adapterPosition));
                break;
            case R.id.move /*2131755484*/:
                onMove(getMessageAtPosition(adapterPosition));
                break;
            case R.id.copy /*2131755485*/:
                onCopy(getMessageAtPosition(adapterPosition));
                break;
            case R.id.flag /*2131755486*/:
                setFlag(adapterPosition, Flag.FLAGGED, true);
                break;
            case R.id.unflag /*2131755487*/:
                setFlag(adapterPosition, Flag.FLAGGED, false);
                break;
            case R.id.spam /*2131755488*/:
                onSpam(getMessageAtPosition(adapterPosition));
                break;
            case R.id.select /*2131755490*/:
            case R.id.deselect /*2131755491*/:
                toggleMessageSelectWithAdapterPosition(adapterPosition);
                break;
            case R.id.reply_all /*2131755492*/:
                onReplyAll(getMessageAtPosition(adapterPosition));
                break;
            case R.id.reply /*2131755493*/:
                onReply(getMessageAtPosition(adapterPosition));
                break;
            case R.id.send_again /*2131755494*/:
                onResendMessage(getMessageAtPosition(adapterPosition));
                this.mSelectedCount = 0;
                break;
            case R.id.forward /*2131755495*/:
                onForward(getMessageAtPosition(adapterPosition));
                break;
            case R.id.same_sender /*2131755496*/:
                String senderAddress = getSenderAddressFromCursor((Cursor) this.mAdapter.getItem(adapterPosition));
                if (senderAddress != null) {
                    this.mFragmentListener.showMoreFromSameSender(senderAddress);
                    break;
                }
                break;
            case R.id.debug_delete_locally /*2131755497*/:
                onDebugClearLocally(getMessageAtPosition(adapterPosition));
                break;
        }
        this.mContextMenuUniqueId = 0;
        return true;
    }

    static String getSenderAddressFromCursor(Cursor cursor) {
        Address[] fromAddrs = Address.unpack(cursor.getString(5));
        if (fromAddrs.length > 0) {
            return fromAddrs[0].getAddress();
        }
        return null;
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        boolean read;
        boolean flagged;
        super.onCreateContextMenu(menu, v, menuInfo);
        Cursor cursor = (Cursor) this.mListView.getItemAtPosition(((AdapterView.AdapterContextMenuInfo) menuInfo).position);
        if (cursor != null) {
            getActivity().getMenuInflater().inflate(R.menu.message_list_item_context, menu);
            menu.findItem(R.id.debug_delete_locally).setVisible(false);
            this.mContextMenuUniqueId = cursor.getLong(this.mUniqueIdColumn);
            Account account = getAccountFromCursor(cursor);
            String subject = cursor.getString(3);
            if (cursor.getInt(8) == 1) {
                read = true;
            } else {
                read = false;
            }
            if (cursor.getInt(9) == 1) {
                flagged = true;
            } else {
                flagged = false;
            }
            menu.setHeaderTitle(subject);
            if (this.mSelected.contains(Long.valueOf(this.mContextMenuUniqueId))) {
                menu.findItem(R.id.select).setVisible(false);
            } else {
                menu.findItem(R.id.deselect).setVisible(false);
            }
            if (read) {
                menu.findItem(R.id.mark_as_read).setVisible(false);
            } else {
                menu.findItem(R.id.mark_as_unread).setVisible(false);
            }
            if (flagged) {
                menu.findItem(R.id.flag).setVisible(false);
            } else {
                menu.findItem(R.id.unflag).setVisible(false);
            }
            if (!this.mController.isCopyCapable(account)) {
                menu.findItem(R.id.copy).setVisible(false);
            }
            if (!this.mController.isMoveCapable(account)) {
                menu.findItem(R.id.move).setVisible(false);
                menu.findItem(R.id.archive).setVisible(false);
                menu.findItem(R.id.spam).setVisible(false);
            }
            if (!account.hasArchiveFolder()) {
                menu.findItem(R.id.archive).setVisible(false);
            }
            if (!account.hasSpamFolder()) {
                menu.findItem(R.id.spam).setVisible(false);
            }
        }
    }

    public void onSwipeRightToLeft(MotionEvent e1, MotionEvent e2) {
        handleSwipe(e1, false);
    }

    public void onSwipeLeftToRight(MotionEvent e1, MotionEvent e2) {
        handleSwipe(e1, true);
    }

    private void handleSwipe(MotionEvent downMotion, boolean selected) {
        int x = (int) downMotion.getRawX();
        int y = (int) downMotion.getRawY();
        Rect headerRect = new Rect();
        this.mListView.getGlobalVisibleRect(headerRect);
        if (headerRect.contains(x, y)) {
            int[] listPosition = new int[2];
            this.mListView.getLocationOnScreen(listPosition);
            toggleMessageSelect(this.mListView.pointToPosition(x - listPosition[0], y - listPosition[1]));
        }
    }

    private int listViewToAdapterPosition(int position) {
        if (position <= 0 || position > this.mAdapter.getCount()) {
            return -1;
        }
        return position - 1;
    }

    private int adapterToListViewPosition(int position) {
        if (position < 0 || position >= this.mAdapter.getCount()) {
            return -1;
        }
        return position + 1;
    }

    class MessageListActivityListener extends ActivityListener {
        MessageListActivityListener() {
        }

        public void remoteSearchFailed(String folder, String err) {
            MessageListFragment.this.mHandler.post(new Runnable() {
                public void run() {
                    Activity activity = MessageListFragment.this.getActivity();
                    if (activity != null) {
                        Toast.makeText(activity, (int) R.string.remote_search_failed_later_atomicgonza, 1).show();
                    }
                }
            });
            MessageListFragment.this.mHandler.showRemoteProgress(false);
        }

        public void remoteSearchStarted(String folder) {
            MessageListFragment.this.mHandler.progress(true);
            MessageListFragment.this.mHandler.updateFooter(MessageListFragment.this.mContext.getString(R.string.remote_search_sending_query));
            MessageListFragment.this.mHandler.showRemoteProgress(true);
        }

        public void enableProgressIndicator(boolean enable) {
            MessageListFragment.this.mHandler.progress(enable);
        }

        public void remoteSearchFinished(String folder, int numResults, int maxResults, List<Message> extraResults) {
            MessageListFragment.this.mHandler.progress(false);
            MessageListFragment.this.mHandler.remoteSearchFinished();
            MessageListFragment.this.mExtraSearchResults = extraResults;
            if (extraResults == null || extraResults.size() <= 0) {
                MessageListFragment.this.mHandler.updateFooter(null);
            } else {
                MessageListFragment.this.mHandler.updateFooter(String.format(MessageListFragment.this.mContext.getString(R.string.load_more_messages_fmt), Integer.valueOf(maxResults)));
            }
            MessageListFragment.this.mFragmentListener.setMessageListProgress(10000);
            MessageListFragment.this.mHandler.showRemoteProgress(false);
        }

        public String getSearchQueryIfExist() {
            if (!(MessageListFragment.this.getActivity() instanceof Search) || MessageListFragment.this.mAccount == null || MessageListFragment.this.mCurrentFolder == null || MessageListFragment.this.mSearch == null) {
                return null;
            }
            return MessageListFragment.this.mSearch.getRemoteSearchArguments();
        }

        public void remoteSearchFetchedMessage(int count, int total) {
            MessageListFragment.this.mHandler.showRemoteProgress(false);
        }

        public void remoteSearchStartLoadMoreMessages() {
            MessageListFragment.this.mHandler.showRemoteProgress(true);
        }

        public void remoteSearchServerQueryComplete(String folderName, int numResults, int maxResults) {
            MessageListFragment.this.mHandler.progress(true);
            if (maxResults == 0 || numResults <= maxResults) {
                MessageListFragment.this.mHandler.updateFooter(MessageListFragment.this.mContext.getString(R.string.remote_search_downloading, Integer.valueOf(numResults)));
                MessageListFragment.this.mHandler.showRemoteProgress(false);
            } else {
                MessageListFragment.this.mHandler.updateFooter(MessageListFragment.this.mContext.getString(R.string.remote_search_downloading_limited, Integer.valueOf(maxResults), Integer.valueOf(numResults)));
            }
            MessageListFragment.this.mFragmentListener.setMessageListProgress(0);
            MessageListFragment.this.mHandler.showRemoteProgress(false);
        }

        public void informUserOfStatus() {
            MessageListFragment.this.mHandler.refreshTitle();
        }

        public void synchronizeMailboxStarted(Account account, String folder) {
            if (updateForMe(account, folder)) {
                MessageListFragment.this.mHandler.progress(true);
                MessageListFragment.this.mHandler.folderLoading(folder, true);
            }
            super.synchronizeMailboxStarted(account, folder);
        }

        public void synchronizeMailboxFinished(Account account, String folder, int totalMessagesInMailbox, int numNewMessages) {
            if (updateForMe(account, folder)) {
                MessageListFragment.this.mHandler.progress(false);
                MessageListFragment.this.mHandler.folderLoading(folder, false);
            }
            super.synchronizeMailboxFinished(account, folder, totalMessagesInMailbox, numNewMessages);
        }

        public void synchronizeMailboxFailed(Account account, String folder, String message) {
            if (updateForMe(account, folder)) {
                MessageListFragment.this.mHandler.progress(false);
                MessageListFragment.this.mHandler.folderLoading(folder, false);
            }
            super.synchronizeMailboxFailed(account, folder, message);
        }

        public void folderStatusChanged(Account account, String folder, int unreadMessageCount) {
            if (MessageListFragment.this.isSingleAccountMode() && MessageListFragment.this.isSingleFolderMode() && MessageListFragment.this.mAccount.equals(account) && MessageListFragment.this.mFolderName.equals(folder)) {
                int unused = MessageListFragment.this.mUnreadMessageCount = unreadMessageCount;
            }
            super.folderStatusChanged(account, folder, unreadMessageCount);
        }

        private boolean updateForMe(Account account, String folder) {
            if (account == null || folder == null || !Utility.arrayContains(MessageListFragment.this.mAccountUuids, account.getUuid())) {
                return false;
            }
            List<String> folderNames = MessageListFragment.this.mSearch.getFolderNames();
            if (folderNames.isEmpty() || folderNames.contains(folder)) {
                return true;
            }
            return false;
        }
    }

    class MessageListAdapter extends CursorAdapter {
        private Drawable mAnsweredIcon;
        private Drawable mAttachmentIcon;
        private Drawable mForwardedAnsweredIcon;
        private Drawable mForwardedIcon;
        int margin = 0;

        MessageListAdapter() {
            super(MessageListFragment.this.getActivity(), (Cursor) null, 0);
            this.mAttachmentIcon = MessageListFragment.this.getResources().getDrawable(R.drawable.ic_email_attachment_small);
            this.mAnsweredIcon = MessageListFragment.this.getResources().getDrawable(R.drawable.ic_email_answered_small);
            this.mForwardedIcon = MessageListFragment.this.getResources().getDrawable(R.drawable.ic_email_forwarded_small);
            this.mForwardedAnsweredIcon = MessageListFragment.this.getResources().getDrawable(R.drawable.ic_email_forwarded_answered_small);
        }

        private String recipientSigil(boolean toMe, boolean ccMe) {
            if (toMe) {
                return MessageListFragment.this.getString(R.string.messagelist_sent_to_me_sigil);
            }
            if (ccMe) {
                return MessageListFragment.this.getString(R.string.messagelist_sent_cc_me_sigil);
            }
            return "";
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            int i = 0;
            View view = MessageListFragment.this.mInflater.inflate((int) R.layout.message_list_item, parent, false);
            MessageViewHolder holder = new MessageViewHolder();
            holder.date = (TextView) view.findViewById(R.id.date);
            holder.chip = view.findViewById(R.id.chip);
            if (MessageListFragment.this.mPreviewLines == 0 && MessageListFragment.this.mContactsPictureLoader == null) {
                view.findViewById(R.id.preview).setVisibility(8);
                holder.preview = (TextView) view.findViewById(R.id.sender_compact);
                holder.flagged = (CheckBox) view.findViewById(R.id.flagged_center_right);
                view.findViewById(R.id.flagged_bottom_right).setVisibility(8);
            } else {
                view.findViewById(R.id.sender_compact).setVisibility(8);
                holder.preview = (TextView) view.findViewById(R.id.preview);
                holder.flagged = (CheckBox) view.findViewById(R.id.flagged_bottom_right);
                view.findViewById(R.id.flagged_center_right).setVisibility(8);
            }
            ImageView contactBadge = (ImageView) view.findViewById(R.id.contact_badge);
            if (MessageListFragment.this.mContactsPictureLoader != null) {
                holder.contactBadge = contactBadge;
            } else {
                contactBadge.setVisibility(8);
            }
            holder.from = (TextView) view.findViewById(R.id.subject);
            if (MessageListFragment.this.mSenderAboveSubject) {
                holder.from = (TextView) view.findViewById(R.id.subject);
                MessageListFragment.this.mFontSizes.setViewTextSize(holder.from, MessageListFragment.this.mFontSizes.getMessageListSender());
            } else {
                holder.subject = (TextView) view.findViewById(R.id.subject);
                MessageListFragment.this.mFontSizes.setViewTextSize(holder.subject, MessageListFragment.this.mFontSizes.getMessageListSubject());
            }
            MessageListFragment.this.mFontSizes.setViewTextSize(holder.date, MessageListFragment.this.mFontSizes.getMessageListDate());
            holder.preview.setLines(Math.max(MessageListFragment.this.mPreviewLines, 1));
            MessageListFragment.this.mFontSizes.setViewTextSize(holder.preview, MessageListFragment.this.mFontSizes.getMessageListPreview());
            holder.threadCount = (TextView) view.findViewById(R.id.thread_count);
            MessageListFragment.this.mFontSizes.setViewTextSize(holder.threadCount, MessageListFragment.this.mFontSizes.getMessageListSubject());
            view.findViewById(R.id.selected_checkbox_wrapper).setVisibility(MessageListFragment.this.mCheckboxes ? 0 : 8);
            CheckBox checkBox = holder.flagged;
            if (!MessageListFragment.this.mStars) {
                i = 8;
            }
            checkBox.setVisibility(i);
            holder.flagged.setOnClickListener(holder);
            holder.selected = (CheckBox) view.findViewById(R.id.selected_checkbox);
            holder.selected.setOnClickListener(holder);
            holder.contactBadge.setOnClickListener(holder);
            view.setTag(holder);
            return view;
        }

        public void bindView(View view, Context context, Cursor cursor) {
            CharSequence beforePreviewText;
            int res;
            Account account = MessageListFragment.this.getAccountFromCursor(cursor);
            String fromList = cursor.getString(5);
            String toList = cursor.getString(6);
            String ccList = cursor.getString(7);
            Address[] fromAddrs = Address.unpack(fromList);
            Address[] toAddrs = Address.unpack(toList);
            Address[] ccAddrs = Address.unpack(ccList);
            boolean fromMe = MessageListFragment.this.mMessageHelper.toMe(account, fromAddrs);
            boolean toMe = MessageListFragment.this.mMessageHelper.toMe(account, toAddrs);
            boolean ccMe = MessageListFragment.this.mMessageHelper.toMe(account, ccAddrs);
            CharSequence displayName = MessageListFragment.this.mMessageHelper.getDisplayName(account, fromAddrs, toAddrs);
            CharSequence displayDate = DateUtils.getRelativeTimeSpanString(context, cursor.getLong(4));
            Address counterpartyAddress = null;
            if (fromMe) {
                if (toAddrs.length > 0) {
                    counterpartyAddress = toAddrs[0];
                } else if (ccAddrs.length > 0) {
                    counterpartyAddress = ccAddrs[0];
                }
            } else if (fromAddrs.length > 0) {
                counterpartyAddress = fromAddrs[0];
            }
            int threadCount = MessageListFragment.this.mThreadedList ? cursor.getInt(19) : 0;
            String subject = cursor.getString(3);
            if (TextUtils.isEmpty(subject)) {
                subject = MessageListFragment.this.getString(R.string.general_no_subject);
            } else if (threadCount > 1) {
                subject = Utility.stripSubject(subject);
            }
            boolean read = cursor.getInt(8) == 1;
            boolean flagged = cursor.getInt(9) == 1;
            boolean answered = cursor.getInt(10) == 1;
            boolean forwarded = cursor.getInt(11) == 1;
            boolean hasAttachments = cursor.getInt(12) > 0;
            MessageViewHolder holder = (MessageViewHolder) view.getTag();
            int maybeBoldTypeface = read ? 0 : 1;
            boolean selected = MessageListFragment.this.mSelected.contains(Long.valueOf(cursor.getLong(MessageListFragment.this.mUniqueIdColumn)));
            if (!MessageListFragment.this.isSingleAccountMode() || K9.isChipEnabledOnSingleAccount()) {
                holder.chip.setBackgroundColor(account.getChipColor());
            } else {
                holder.chip.setVisibility(8);
                if (holder.contactBadge != null) {
                    LinearLayout.LayoutParams parms = (LinearLayout.LayoutParams) holder.contactBadge.getLayoutParams();
                    if (this.margin == 0) {
                        this.margin = (int) (10.0f * context.getResources().getDisplayMetrics().density);
                    }
                    parms.leftMargin = this.margin;
                    parms.rightMargin = this.margin;
                }
            }
            if (MessageListFragment.this.mCheckboxes) {
                holder.selected.setChecked(selected);
            }
            if (MessageListFragment.this.mStars) {
                holder.flagged.setChecked(flagged);
            }
            holder.position = cursor.getPosition();
            if (holder.contactBadge != null) {
                if (selected) {
                    holder.contactBadge.setImageResource(R.drawable.ic_check_circle_white_24dp);
                } else if (counterpartyAddress != null) {
                    holder.contactBadge.setPadding(0, 0, 0, 0);
                    MessageListFragment.this.mContactsPictureLoader.loadContactPicture(counterpartyAddress, holder.contactBadge);
                } else {
                    holder.contactBadge.setImageResource(R.drawable.ic_contact_picture);
                }
            }
            if (selected || K9.useBackgroundAsUnreadIndicator()) {
                if (selected) {
                    res = R.attr.messageListSelectedBackgroundColor;
                } else if (read) {
                    res = R.attr.messageListReadItemBackgroundColor;
                } else {
                    res = R.attr.messageListUnreadItemBackgroundColor;
                }
                TypedValue outValue = new TypedValue();
                MessageListFragment.this.getActivity().getTheme().resolveAttribute(res, outValue, true);
                view.setBackgroundColor(outValue.data);
            } else {
                view.setBackgroundColor(0);
            }
            if (MessageListFragment.this.mActiveMessage != null) {
                String uid = cursor.getString(1);
                String folderName = cursor.getString(18);
                if (account.getUuid().equals(MessageListFragment.this.mActiveMessage.getAccountUuid()) && folderName.equals(MessageListFragment.this.mActiveMessage.getFolderName()) && uid.equals(MessageListFragment.this.mActiveMessage.getUid())) {
                    TypedValue outValue2 = new TypedValue();
                    MessageListFragment.this.getActivity().getTheme().resolveAttribute(R.attr.messageListActiveItemBackgroundColor, outValue2, true);
                    view.setBackgroundColor(outValue2.data);
                }
            }
            if (threadCount > 1) {
                holder.threadCount.setText(String.format("%d", Integer.valueOf(threadCount)));
                holder.threadCount.setVisibility(0);
            } else {
                holder.threadCount.setVisibility(8);
            }
            if (MessageListFragment.this.mSenderAboveSubject) {
                beforePreviewText = subject;
            } else {
                beforePreviewText = displayName;
            }
            String sigil = recipientSigil(toMe, ccMe);
            SpannableStringBuilder messageStringBuilder = new SpannableStringBuilder(sigil).append(beforePreviewText);
            if (MessageListFragment.this.mPreviewLines > 0) {
                messageStringBuilder.append((CharSequence) " ").append((CharSequence) getPreview(cursor));
            }
            holder.preview.setText(messageStringBuilder, TextView.BufferType.SPANNABLE);
            Spannable str = (Spannable) holder.preview.getText();
            str.setSpan(new AbsoluteSizeSpan(MessageListFragment.this.mSenderAboveSubject ? MessageListFragment.this.mFontSizes.getMessageListSubject() : MessageListFragment.this.mFontSizes.getMessageListSender(), true), 0, beforePreviewText.length() + sigil.length(), 33);
            str.setSpan(new ForegroundColorSpan(K9.getK9Theme() == K9.Theme.LIGHT ? Color.rgb(105, 105, 105) : Color.rgb(160, 160, 160)), beforePreviewText.length() + sigil.length(), str.length(), 33);
            Drawable statusHolder = null;
            if (forwarded && answered) {
                statusHolder = this.mForwardedAnsweredIcon;
            } else if (answered) {
                statusHolder = this.mAnsweredIcon;
            } else if (forwarded) {
                statusHolder = this.mForwardedIcon;
            }
            if (holder.from != null) {
                holder.from.setTypeface(Typeface.create(holder.from.getTypeface(), maybeBoldTypeface));
                if (MessageListFragment.this.mSenderAboveSubject) {
                    holder.from.setCompoundDrawablesWithIntrinsicBounds(statusHolder, (Drawable) null, hasAttachments ? this.mAttachmentIcon : null, (Drawable) null);
                    holder.from.setText(displayName);
                } else {
                    holder.from.setText(new SpannableStringBuilder(sigil).append(displayName));
                }
                if (read) {
                    holder.from.setTextColor(ContextCompat.getColor(context, R.color.read_grey));
                } else {
                    holder.from.setTextColor(ContextCompat.getColor(context, R.color.unread_black));
                }
            }
            if (holder.subject != null) {
                if (!MessageListFragment.this.mSenderAboveSubject) {
                    holder.subject.setCompoundDrawablesWithIntrinsicBounds(statusHolder, (Drawable) null, hasAttachments ? this.mAttachmentIcon : null, (Drawable) null);
                }
                holder.subject.setTypeface(Typeface.create(holder.subject.getTypeface(), maybeBoldTypeface));
                holder.subject.setText(subject);
            }
            holder.date.setText(displayDate);
        }

        private String getPreview(Cursor cursor) {
            DatabasePreviewType previewType = DatabasePreviewType.fromDatabaseValue(cursor.getString(14));
            switch (previewType) {
                case NONE:
                    return "";
                case ENCRYPTED:
                    return MessageListFragment.this.getString(R.string.preview_encrypted);
                case TEXT:
                    return cursor.getString(15);
                default:
                    throw new AssertionError("Unknown preview type: " + previewType);
            }
        }
    }

    class MessageViewHolder implements View.OnClickListener {
        public View chip;
        public ImageView contactBadge;
        public TextView date;
        public CheckBox flagged;
        public TextView from;
        public int position = -1;
        public TextView preview;
        public CheckBox selected;
        public TextView subject;
        public TextView threadCount;
        public TextView time;

        MessageViewHolder() {
        }

        public void onClick(View view) {
            if (this.position != -1) {
                switch (view.getId()) {
                    case R.id.selected_checkbox /*2131755348*/:
                        MessageListFragment.this.toggleMessageSelectWithAdapterPosition(this.position);
                        MessageListFragment.this.setImageViewAnimatedChange(MessageListFragment.this.mContext, this.contactBadge);
                        return;
                    case R.id.contact_badge /*2131755349*/:
                        MessageListFragment.this.toggleMessageSelectWithAdapterPosition(this.position);
                        MessageListFragment.this.setImageViewAnimatedChange(MessageListFragment.this.mContext, this.contactBadge);
                        return;
                    case R.id.flagged_bottom_right /*2131755352*/:
                    case R.id.flagged_center_right /*2131755357*/:
                        MessageListFragment.this.toggleMessageFlagWithAdapterPosition(this.position);
                        return;
                    default:
                        return;
                }
            }
        }
    }

    public void setImageViewAnimatedChange(Context c, ImageView v) {
        if (this.anim_out == null) {
            this.anim_out = AnimationUtils.loadAnimation(c, 17432578);
        }
        v.startAnimation(this.anim_out);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private View getFooterView(ViewGroup parent) {
        if (this.mFooterView == null) {
            this.mFooterView = this.mInflater.inflate((int) R.layout.message_list_item_footer, parent, false);
            FooterViewHolder holder = new FooterViewHolder();
            holder.main = (TextView) this.mFooterView.findViewById(R.id.main_text);
            this.mFooterView.setTag(holder);
        }
        return this.mFooterView;
    }

    private void updateFooterView() {
        String message;
        if (this.mSearch.isManualSearch() || this.mCurrentFolder == null || this.mAccount == null) {
            updateFooter(null);
        } else if (this.mCurrentFolder.loading) {
            updateFooter(this.mContext.getString(R.string.status_loading_more));
        } else if (!this.mCurrentFolder.moreMessages) {
            updateFooter(null);
        } else {
            if (this.mCurrentFolder.lastCheckFailed) {
                message = this.mContext.getString(R.string.status_loading_more_failed);
            } else if (this.mAccount.getDisplayCount() == 0) {
                message = this.mContext.getString(R.string.message_list_load_more_messages_action);
            } else {
                message = String.format(this.mContext.getString(R.string.load_more_messages_fmt), Integer.valueOf(this.mAccount.getDisplayCount()));
            }
            updateFooter(message);
        }
    }

    public void updateFooter(String text) {
        if (this.mFooterView != null) {
            FooterViewHolder holder = (FooterViewHolder) this.mFooterView.getTag();
            if (text != null) {
                holder.main.setText(text);
                holder.main.setVisibility(0);
                return;
            }
            holder.main.setVisibility(8);
        }
    }

    static class FooterViewHolder {
        public TextView main;

        FooterViewHolder() {
        }
    }

    /* access modifiers changed from: private */
    public void setSelectionState(boolean selected) {
        if (!selected) {
            this.mSelected.clear();
            this.mSelectedCount = 0;
            if (this.mActionMode != null) {
                this.mActionMode.finish();
                this.mActionMode = null;
            }
        } else if (this.mAdapter.getCount() != 0) {
            this.mSelectedCount = 0;
            int end = this.mAdapter.getCount();
            for (int i = 0; i < end; i++) {
                Cursor cursor = (Cursor) this.mAdapter.getItem(i);
                this.mSelected.add(Long.valueOf(cursor.getLong(this.mUniqueIdColumn)));
                if (this.mThreadedList) {
                    int threadCount = cursor.getInt(19);
                    int i2 = this.mSelectedCount;
                    if (threadCount <= 1) {
                        threadCount = 1;
                    }
                    this.mSelectedCount = i2 + threadCount;
                } else {
                    this.mSelectedCount++;
                }
            }
            if (this.mActionMode == null) {
                startAndPrepareActionMode();
            }
            computeBatchDirection();
            updateActionModeTitle();
            computeSelectAllVisibility();
        } else {
            return;
        }
        this.mAdapter.notifyDataSetChanged();
    }

    private void toggleMessageSelect(int listViewPosition) {
        int adapterPosition = listViewToAdapterPosition(listViewPosition);
        if (adapterPosition != -1) {
            toggleMessageSelectWithAdapterPosition(adapterPosition);
        }
    }

    /* access modifiers changed from: private */
    public void toggleMessageFlagWithAdapterPosition(int adapterPosition) {
        boolean flagged;
        boolean z = true;
        if (((Cursor) this.mAdapter.getItem(adapterPosition)).getInt(9) == 1) {
            flagged = true;
        } else {
            flagged = false;
        }
        Flag flag = Flag.FLAGGED;
        if (flagged) {
            z = false;
        }
        setFlag(adapterPosition, flag, z);
    }

    /* access modifiers changed from: private */
    public void toggleMessageSelectWithAdapterPosition(int adapterPosition) {
        int threadCount;
        Cursor cursor = (Cursor) this.mAdapter.getItem(adapterPosition);
        long uniqueId = cursor.getLong(this.mUniqueIdColumn);
        boolean selected = this.mSelected.contains(Long.valueOf(uniqueId));
        if (!selected) {
            this.mSelected.add(Long.valueOf(uniqueId));
        } else {
            this.mSelected.remove(Long.valueOf(uniqueId));
        }
        int selectedCountDelta = 1;
        if (this.mThreadedList && (threadCount = cursor.getInt(19)) > 1) {
            selectedCountDelta = threadCount;
        }
        if (this.mActionMode == null) {
            startAndPrepareActionMode();
        } else if (this.mSelectedCount == selectedCountDelta && selected) {
            this.mActionMode.finish();
            this.mActionMode = null;
            return;
        }
        if (selected) {
            this.mSelectedCount -= selectedCountDelta;
        } else {
            this.mSelectedCount += selectedCountDelta;
        }
        computeBatchDirection();
        updateActionModeTitle();
        computeSelectAllVisibility();
        this.mAdapter.notifyDataSetChanged();
    }

    private void updateActionModeTitle() {
        this.mActionMode.setTitle("" + this.mSelectedCount);
    }

    private void computeSelectAllVisibility() {
        this.mActionModeCallback.showSelectAll(this.mSelected.size() != this.mAdapter.getCount());
    }

    private void computeBatchDirection() {
        boolean read;
        boolean flagged;
        boolean isBatchFlag = false;
        boolean isBatchRead = false;
        int end = this.mAdapter.getCount();
        for (int i = 0; i < end; i++) {
            Cursor cursor = (Cursor) this.mAdapter.getItem(i);
            if (this.mSelected.contains(Long.valueOf(cursor.getLong(this.mUniqueIdColumn)))) {
                if (cursor.getInt(8) == 1) {
                    read = true;
                } else {
                    read = false;
                }
                if (cursor.getInt(9) == 1) {
                    flagged = true;
                } else {
                    flagged = false;
                }
                if (!flagged) {
                    isBatchFlag = true;
                }
                if (!read) {
                    isBatchRead = true;
                }
                if (isBatchFlag && isBatchRead) {
                    break;
                }
            }
        }
        this.mActionModeCallback.showMarkAsRead(isBatchRead);
        this.mActionModeCallback.showFlag(isBatchFlag);
    }

    private void setFlag(int adapterPosition, Flag flag, boolean newState) {
        if (adapterPosition != -1) {
            Cursor cursor = (Cursor) this.mAdapter.getItem(adapterPosition);
            Account account = this.mPreferences.getAccount(cursor.getString(17));
            if (!this.mThreadedList || cursor.getInt(19) <= 1) {
                this.mController.setFlag(account, Collections.singletonList(Long.valueOf(cursor.getLong(0))), flag, newState);
            } else {
                this.mController.setFlagForThreads(account, Collections.singletonList(Long.valueOf(cursor.getLong(16))), flag, newState);
            }
            computeBatchDirection();
        }
    }

    /* access modifiers changed from: private */
    public void setFlagForSelected(Flag flag, boolean newState) {
        if (!this.mSelected.isEmpty()) {
            Map<Account, List<Long>> messageMap = new HashMap<>();
            Map<Account, List<Long>> threadMap = new HashMap<>();
            Set<Account> accounts = new HashSet<>();
            int end = this.mAdapter.getCount();
            for (int position = 0; position < end; position++) {
                Cursor cursor = (Cursor) this.mAdapter.getItem(position);
                if (this.mSelected.contains(Long.valueOf(cursor.getLong(this.mUniqueIdColumn)))) {
                    Account account = this.mPreferences.getAccount(cursor.getString(17));
                    accounts.add(account);
                    if (!this.mThreadedList || cursor.getInt(19) <= 1) {
                        List<Long> messageIdList = messageMap.get(account);
                        if (messageIdList == null) {
                            messageIdList = new ArrayList<>();
                            messageMap.put(account, messageIdList);
                        }
                        messageIdList.add(Long.valueOf(cursor.getLong(0)));
                    } else {
                        List<Long> threadRootIdList = threadMap.get(account);
                        if (threadRootIdList == null) {
                            threadRootIdList = new ArrayList<>();
                            threadMap.put(account, threadRootIdList);
                        }
                        threadRootIdList.add(Long.valueOf(cursor.getLong(16)));
                    }
                }
            }
            for (Account account2 : accounts) {
                List<Long> messageIds = messageMap.get(account2);
                List<Long> threadRootIds = threadMap.get(account2);
                if (messageIds != null) {
                    this.mController.setFlag(account2, messageIds, flag, newState);
                }
                if (threadRootIds != null) {
                    this.mController.setFlagForThreads(account2, threadRootIds, flag, newState);
                }
            }
            computeBatchDirection();
        }
    }

    private void onMove(MessageReference message) {
        onMove(Collections.singletonList(message));
    }

    /* access modifiers changed from: private */
    public void onMove(List<MessageReference> messages) {
        String folderName;
        if (checkCopyOrMovePossible(messages, FolderOperation.MOVE)) {
            if (this.mIsThreadDisplay) {
                folderName = messages.get(0).getFolderName();
            } else if (this.mSingleFolderMode) {
                folderName = this.mCurrentFolder.folder.getName();
            } else {
                folderName = null;
            }
            displayFolderChoice(1, folderName, messages.get(0).getAccountUuid(), null, messages);
        }
    }

    private void onCopy(MessageReference message) {
        onCopy(Collections.singletonList(message));
    }

    /* access modifiers changed from: private */
    public void onCopy(List<MessageReference> messages) {
        String folderName;
        if (checkCopyOrMovePossible(messages, FolderOperation.COPY)) {
            if (this.mIsThreadDisplay) {
                folderName = messages.get(0).getFolderName();
            } else if (this.mSingleFolderMode) {
                folderName = this.mCurrentFolder.folder.getName();
            } else {
                folderName = null;
            }
            displayFolderChoice(2, folderName, messages.get(0).getAccountUuid(), null, messages);
        }
    }

    private void onDebugClearLocally(MessageReference message) {
        this.mController.debugClearMessagesLocally(Collections.singletonList(message));
    }

    private void displayFolderChoice(int requestCode, String sourceFolderName, String accountUuid, String lastSelectedFolderName, List<MessageReference> messages) {
        Intent intent = new Intent(getActivity(), ChooseFolder.class);
        intent.putExtra(ChooseFolder.EXTRA_ACCOUNT, accountUuid);
        intent.putExtra(ChooseFolder.EXTRA_SEL_FOLDER, lastSelectedFolderName);
        if (sourceFolderName == null) {
            intent.putExtra(ChooseFolder.EXTRA_SHOW_CURRENT, "yes");
        } else {
            intent.putExtra(ChooseFolder.EXTRA_CUR_FOLDER, sourceFolderName);
        }
        this.mActiveMessages = messages;
        startActivityForResult(intent, requestCode);
    }

    private void onArchive(MessageReference message) {
        onArchive(Collections.singletonList(message));
    }

    /* access modifiers changed from: private */
    public void onArchive(List<MessageReference> messages) {
        for (Map.Entry<Account, List<MessageReference>> entry : groupMessagesByAccount(messages).entrySet()) {
            String archiveFolder = ((Account) entry.getKey()).getArchiveFolderName();
            if (!K9.FOLDER_NONE.equals(archiveFolder)) {
                move((List) entry.getValue(), archiveFolder);
            }
        }
    }

    private Map<Account, List<MessageReference>> groupMessagesByAccount(List<MessageReference> messages) {
        Map<Account, List<MessageReference>> messagesByAccount = new HashMap<>();
        for (MessageReference message : messages) {
            Account account = this.mPreferences.getAccount(message.getAccountUuid());
            List<MessageReference> msgList = messagesByAccount.get(account);
            if (msgList == null) {
                msgList = new ArrayList<>();
                messagesByAccount.put(account, msgList);
            }
            msgList.add(message);
        }
        return messagesByAccount;
    }

    private void onSpam(MessageReference message) {
        onSpam(Collections.singletonList(message));
    }

    /* access modifiers changed from: private */
    public void onSpam(List<MessageReference> messages) {
        if (K9.confirmSpam()) {
            this.mActiveMessages = messages;
            showDialog(R.id.dialog_confirm_spam);
            return;
        }
        onSpamConfirmed(messages);
    }

    private void onSpamConfirmed(List<MessageReference> messages) {
        for (Map.Entry<Account, List<MessageReference>> entry : groupMessagesByAccount(messages).entrySet()) {
            String spamFolder = ((Account) entry.getKey()).getSpamFolderName();
            if (!K9.FOLDER_NONE.equals(spamFolder)) {
                move((List) entry.getValue(), spamFolder);
            }
        }
    }

    private boolean checkCopyOrMovePossible(List<MessageReference> messages, FolderOperation operation) {
        if (messages.isEmpty()) {
            return false;
        }
        boolean first = true;
        for (MessageReference message : messages) {
            if (first) {
                first = false;
                Account account = this.mPreferences.getAccount(message.getAccountUuid());
                if (operation == FolderOperation.MOVE && !this.mController.isMoveCapable(account)) {
                    return false;
                }
                if (operation == FolderOperation.COPY && !this.mController.isCopyCapable(account)) {
                    return false;
                }
            }
            if ((operation == FolderOperation.MOVE && !this.mController.isMoveCapable(message)) || (operation == FolderOperation.COPY && !this.mController.isCopyCapable(message))) {
                Toast.makeText(getActivity(), (int) R.string.move_copy_cannot_copy_unsynced_message, 1).show();
                return false;
            }
        }
        return true;
    }

    private void copy(List<MessageReference> messages, String destination) {
        copyOrMove(messages, destination, FolderOperation.COPY);
    }

    private void move(List<MessageReference> messages, String destination) {
        copyOrMove(messages, destination, FolderOperation.MOVE);
    }

    private void copyOrMove(List<MessageReference> messages, String destination, FolderOperation operation) {
        Map<String, List<MessageReference>> folderMap = new HashMap<>();
        for (MessageReference message : messages) {
            if ((operation != FolderOperation.MOVE || this.mController.isMoveCapable(message)) && (operation != FolderOperation.COPY || this.mController.isCopyCapable(message))) {
                String folderName = message.getFolderName();
                if (!folderName.equals(destination)) {
                    List<MessageReference> outMessages = folderMap.get(folderName);
                    if (outMessages == null) {
                        outMessages = new ArrayList<>();
                        folderMap.put(folderName, outMessages);
                    }
                    outMessages.add(message);
                }
            } else {
                Toast.makeText(getActivity(), (int) R.string.move_copy_cannot_copy_unsynced_message, 1).show();
                return;
            }
        }
        for (Map.Entry<String, List<MessageReference>> entry : folderMap.entrySet()) {
            String folderName2 = (String) entry.getKey();
            List<MessageReference> outMessages2 = entry.getValue();
            Account account = this.mPreferences.getAccount(((MessageReference) outMessages2.get(0)).getAccountUuid());
            if (operation == FolderOperation.MOVE) {
                if (this.mThreadedList) {
                    this.mController.moveMessagesInThread(account, folderName2, outMessages2, destination);
                } else {
                    this.mController.moveMessages(account, folderName2, outMessages2, destination);
                }
            } else if (this.mThreadedList) {
                this.mController.copyMessagesInThread(account, folderName2, outMessages2, destination);
            } else {
                this.mController.copyMessages(account, folderName2, outMessages2, destination);
            }
        }
    }

    class ActionModeCallback implements ActionMode.Callback {
        private MenuItem mFlag;
        private MenuItem mMarkAsRead;
        private MenuItem mMarkAsUnread;
        private MenuItem mSelectAll;
        private MenuItem mUnflag;

        ActionModeCallback() {
        }

        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            this.mSelectAll = menu.findItem(R.id.select_all);
            this.mMarkAsRead = menu.findItem(R.id.mark_as_read);
            this.mMarkAsUnread = menu.findItem(R.id.mark_as_unread);
            this.mFlag = menu.findItem(R.id.flag);
            this.mUnflag = menu.findItem(R.id.unflag);
            if (!MessageListFragment.this.mSingleAccountMode) {
                menu.findItem(R.id.move).setVisible(true);
                menu.findItem(R.id.archive).setVisible(true);
                menu.findItem(R.id.spam).setVisible(true);
                menu.findItem(R.id.copy).setVisible(true);
                for (String accountUuid : getAccountUuidsForSelected()) {
                    Account account = MessageListFragment.this.mPreferences.getAccount(accountUuid);
                    if (account != null) {
                        setContextCapabilities(account, menu);
                    }
                }
            }
            return true;
        }

        private Set<String> getAccountUuidsForSelected() {
            Set<String> accountUuids = new HashSet<>(MessageListFragment.this.mAccountUuids.length);
            int end = MessageListFragment.this.mAdapter.getCount();
            for (int position = 0; position < end; position++) {
                Cursor cursor = (Cursor) MessageListFragment.this.mAdapter.getItem(position);
                if (MessageListFragment.this.mSelected.contains(Long.valueOf(cursor.getLong(MessageListFragment.this.mUniqueIdColumn)))) {
                    accountUuids.add(cursor.getString(17));
                    if (accountUuids.size() == MessageListFragment.this.mAccountUuids.length) {
                        break;
                    }
                }
            }
            return accountUuids;
        }

        public void onDestroyActionMode(ActionMode mode) {
            ActionMode unused = MessageListFragment.this.mActionMode = null;
            this.mSelectAll = null;
            this.mMarkAsRead = null;
            this.mMarkAsUnread = null;
            this.mFlag = null;
            this.mUnflag = null;
            MessageListFragment.this.setSelectionState(false);
            if (Build.VERSION.SDK_INT >= 21) {
                Window win = MessageListFragment.this.getActivity().getWindow();
                win.addFlags(Integer.MIN_VALUE);
                win.setStatusBarColor(MessageListFragment.this.getResources().getColor(R.color.statusBarBackgroundNormal));
            }
        }

        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.message_list_context, menu);
            setContextCapabilities(MessageListFragment.this.mAccount, menu);
            if (Build.VERSION.SDK_INT < 21) {
                return true;
            }
            Window win = MessageListFragment.this.getActivity().getWindow();
            win.addFlags(Integer.MIN_VALUE);
            win.setStatusBarColor(MessageListFragment.this.getResources().getColor(R.color.statusBarBackgroundOnSelection));
            return true;
        }

        private void setContextCapabilities(Account account, Menu menu) {
            if (!MessageListFragment.this.mSingleAccountMode) {
                menu.findItem(R.id.move).setVisible(false);
                menu.findItem(R.id.copy).setVisible(false);
                menu.findItem(R.id.archive).setVisible(false);
                menu.findItem(R.id.spam).setVisible(false);
                return;
            }
            if (!MessageListFragment.this.mController.isCopyCapable(account)) {
                menu.findItem(R.id.copy).setVisible(false);
            }
            if (!MessageListFragment.this.mController.isMoveCapable(account)) {
                menu.findItem(R.id.move).setVisible(false);
                menu.findItem(R.id.archive).setVisible(false);
                menu.findItem(R.id.spam).setVisible(false);
            }
            if (!account.hasArchiveFolder()) {
                menu.findItem(R.id.archive).setVisible(false);
            }
            if (!account.hasSpamFolder()) {
                menu.findItem(R.id.spam).setVisible(false);
            }
        }

        public void showSelectAll(boolean show) {
            if (MessageListFragment.this.mActionMode != null) {
                this.mSelectAll.setVisible(show);
            }
        }

        public void showMarkAsRead(boolean show) {
            if (MessageListFragment.this.mActionMode != null) {
                this.mMarkAsRead.setVisible(show);
                this.mMarkAsUnread.setVisible(!show);
            }
        }

        public void showFlag(boolean show) {
            if (MessageListFragment.this.mActionMode != null) {
                this.mFlag.setVisible(show);
                this.mUnflag.setVisible(!show);
            }
        }

        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.delete /*2131755480*/:
                    MessageListFragment.this.onDelete(MessageListFragment.this.getCheckedMessages());
                    int unused = MessageListFragment.this.mSelectedCount = 0;
                    break;
                case R.id.mark_as_read /*2131755481*/:
                    MessageListFragment.this.setFlagForSelected(Flag.SEEN, true);
                    break;
                case R.id.mark_as_unread /*2131755482*/:
                    MessageListFragment.this.setFlagForSelected(Flag.SEEN, false);
                    break;
                case R.id.archive /*2131755483*/:
                    MessageListFragment.this.onArchive(MessageListFragment.this.getCheckedMessages());
                    int unused2 = MessageListFragment.this.mSelectedCount = 0;
                    break;
                case R.id.move /*2131755484*/:
                    MessageListFragment.this.onMove(MessageListFragment.this.getCheckedMessages());
                    int unused3 = MessageListFragment.this.mSelectedCount = 0;
                    break;
                case R.id.copy /*2131755485*/:
                    MessageListFragment.this.onCopy(MessageListFragment.this.getCheckedMessages());
                    int unused4 = MessageListFragment.this.mSelectedCount = 0;
                    break;
                case R.id.flag /*2131755486*/:
                    MessageListFragment.this.setFlagForSelected(Flag.FLAGGED, true);
                    break;
                case R.id.unflag /*2131755487*/:
                    MessageListFragment.this.setFlagForSelected(Flag.FLAGGED, false);
                    break;
                case R.id.spam /*2131755488*/:
                    MessageListFragment.this.onSpam(MessageListFragment.this.getCheckedMessages());
                    int unused5 = MessageListFragment.this.mSelectedCount = 0;
                    break;
                case R.id.select_all /*2131755489*/:
                    MessageListFragment.this.selectAll();
                    break;
            }
            if (MessageListFragment.this.mSelectedCount == 0) {
                MessageListFragment.this.mActionMode.finish();
            }
            return true;
        }
    }

    public void doPositiveClick(int dialogId) {
        switch (dialogId) {
            case R.id.dialog_confirm_delete /*2131755014*/:
                onDeleteConfirmed(this.mActiveMessages);
                this.mActiveMessage = null;
                return;
            case R.id.dialog_confirm_mark_all_as_read /*2131755015*/:
                markAllAsRead();
                return;
            case R.id.dialog_confirm_spam /*2131755016*/:
                onSpamConfirmed(this.mActiveMessages);
                this.mActiveMessages = null;
                return;
            default:
                return;
        }
    }

    public void doNegativeClick(int dialogId) {
        switch (dialogId) {
            case R.id.dialog_confirm_delete /*2131755014*/:
            case R.id.dialog_confirm_spam /*2131755016*/:
                this.mActiveMessages = null;
                return;
            case R.id.dialog_confirm_mark_all_as_read /*2131755015*/:
            default:
                return;
        }
    }

    public void dialogCancelled(int dialogId) {
        doNegativeClick(dialogId);
    }

    public void checkMail() {
        if (isSingleAccountMode() && isSingleFolderMode()) {
            this.mController.synchronizeMailbox(this.mAccount, this.mFolderName, this.mListener, null);
            this.mController.sendPendingMessages(this.mAccount, this.mListener);
        } else if (this.mAllAccounts) {
            this.mController.checkMail(this.mContext, null, true, true, this.mListener);
        } else {
            for (String accountUuid : this.mAccountUuids) {
                this.mController.checkMail(this.mContext, this.mPreferences.getAccount(accountUuid), true, true, this.mListener);
            }
        }
    }

    public void onStop() {
        if (isRemoteSearch() && this.mRemoteSearchFuture != null) {
            try {
                Log.i("k9", "Remote search in progress, attempting to abort...");
                if (!this.mRemoteSearchFuture.cancel(true)) {
                    Log.e("k9", "Could not cancel remote search future.");
                }
                Account searchAccount = this.mAccount;
                this.mCurrentFolder.folder.close();
                this.mListener.remoteSearchFinished(this.mCurrentFolder.name, 0, searchAccount.getRemoteSearchNumResults(), null);
            } catch (Exception e) {
                Log.e("k9", "Could not abort remote search before going back", e);
            }
        }
        super.onStop();
    }

    public void selectAll() {
        setSelectionState(true);
    }

    public void onMoveUp() {
        int currentPosition = this.mListView.getSelectedItemPosition();
        if (currentPosition == -1 || this.mListView.isInTouchMode()) {
            currentPosition = this.mListView.getFirstVisiblePosition();
        }
        if (currentPosition > 0) {
            this.mListView.setSelection(currentPosition - 1);
        }
    }

    public void onMoveDown() {
        int currentPosition = this.mListView.getSelectedItemPosition();
        if (currentPosition == -1 || this.mListView.isInTouchMode()) {
            currentPosition = this.mListView.getFirstVisiblePosition();
        }
        if (currentPosition < this.mListView.getCount()) {
            this.mListView.setSelection(currentPosition + 1);
        }
    }

    public boolean openPrevious(MessageReference messageReference) {
        int position = getPosition(messageReference);
        if (position <= 0) {
            return false;
        }
        openMessageAtPosition(position - 1);
        return true;
    }

    public boolean openNext(MessageReference messageReference) {
        int position = getPosition(messageReference);
        if (position < 0 || position == this.mAdapter.getCount() - 1) {
            return false;
        }
        openMessageAtPosition(position + 1);
        return true;
    }

    public boolean isFirst(MessageReference messageReference) {
        return this.mAdapter.isEmpty() || messageReference.equals(getReferenceForPosition(0));
    }

    public boolean isLast(MessageReference messageReference) {
        return this.mAdapter.isEmpty() || messageReference.equals(getReferenceForPosition(this.mAdapter.getCount() + -1));
    }

    private MessageReference getReferenceForPosition(int position) {
        Cursor cursor = (Cursor) this.mAdapter.getItem(position);
        return new MessageReference(cursor.getString(17), cursor.getString(18), cursor.getString(1), null);
    }

    private void openMessageAtPosition(int position) {
        int listViewPosition = adapterToListViewPosition(position);
        if (listViewPosition != -1 && (listViewPosition < this.mListView.getFirstVisiblePosition() || listViewPosition > this.mListView.getLastVisiblePosition())) {
            this.mListView.setSelection(listViewPosition);
        }
        this.mHandler.openMessage(getReferenceForPosition(position));
    }

    private int getPosition(MessageReference messageReference) {
        int len = this.mAdapter.getCount();
        for (int i = 0; i < len; i++) {
            Cursor cursor = (Cursor) this.mAdapter.getItem(i);
            String accountUuid = cursor.getString(17);
            String folderName = cursor.getString(18);
            String uid = cursor.getString(1);
            if (accountUuid.equals(messageReference.getAccountUuid()) && folderName.equals(messageReference.getFolderName()) && uid.equals(messageReference.getUid())) {
                return i;
            }
        }
        return -1;
    }

    public void onReverseSort() {
        changeSort(this.mSortType);
    }

    private MessageReference getSelectedMessage() {
        return getMessageAtPosition(listViewToAdapterPosition(this.mListView.getSelectedItemPosition()));
    }

    private int getAdapterPositionForSelectedMessage() {
        return listViewToAdapterPosition(this.mListView.getSelectedItemPosition());
    }

    private int getPositionForUniqueId(long uniqueId) {
        int end = this.mAdapter.getCount();
        for (int position = 0; position < end; position++) {
            if (((Cursor) this.mAdapter.getItem(position)).getLong(this.mUniqueIdColumn) == uniqueId) {
                return position;
            }
        }
        return -1;
    }

    private MessageReference getMessageAtPosition(int adapterPosition) {
        if (adapterPosition == -1) {
            return null;
        }
        Cursor cursor = (Cursor) this.mAdapter.getItem(adapterPosition);
        return new MessageReference(cursor.getString(17), cursor.getString(18), cursor.getString(1), null);
    }

    /* access modifiers changed from: private */
    public List<MessageReference> getCheckedMessages() {
        MessageReference message;
        List<MessageReference> messages = new ArrayList<>(this.mSelected.size());
        int end = this.mAdapter.getCount();
        for (int position = 0; position < end; position++) {
            if (this.mSelected.contains(Long.valueOf(((Cursor) this.mAdapter.getItem(position)).getLong(this.mUniqueIdColumn))) && (message = getMessageAtPosition(position)) != null) {
                messages.add(message);
            }
        }
        return messages;
    }

    public void onDelete() {
        MessageReference message = getSelectedMessage();
        if (message != null) {
            onDelete(Collections.singletonList(message));
        }
    }

    public void toggleMessageSelect() {
        toggleMessageSelect(this.mListView.getSelectedItemPosition());
    }

    public void onToggleFlagged() {
        onToggleFlag(Flag.FLAGGED, 9);
    }

    public void onToggleRead() {
        onToggleFlag(Flag.SEEN, 8);
    }

    private void onToggleFlag(Flag flag, int flagColumn) {
        boolean flagState;
        boolean z = true;
        int adapterPosition = getAdapterPositionForSelectedMessage();
        if (adapterPosition != -1) {
            if (((Cursor) this.mAdapter.getItem(adapterPosition)).getInt(flagColumn) == 1) {
                flagState = true;
            } else {
                flagState = false;
            }
            if (flagState) {
                z = false;
            }
            setFlag(adapterPosition, flag, z);
        }
    }

    public void onMove() {
        MessageReference message = getSelectedMessage();
        if (message != null) {
            onMove(message);
        }
    }

    public void onArchive() {
        MessageReference message = getSelectedMessage();
        if (message != null) {
            onArchive(message);
        }
    }

    public void onCopy() {
        MessageReference message = getSelectedMessage();
        if (message != null) {
            onCopy(message);
        }
    }

    public boolean isOutbox() {
        return this.mFolderName != null && this.mFolderName.equals(this.mAccount.getOutboxFolderName());
    }

    public boolean isErrorFolder() {
        return K9.ERROR_FOLDER_NAME.equals(this.mFolderName);
    }

    public boolean isRemoteFolder() {
        if (this.mSearch.isManualSearch() || isOutbox() || isErrorFolder()) {
            return false;
        }
        if (this.mController.isMoveCapable(this.mAccount)) {
            return true;
        }
        if (this.mFolderName == null || !this.mFolderName.equals(this.mAccount.getInboxFolderName())) {
            return false;
        }
        return true;
    }

    public boolean isManualSearch() {
        return this.mSearch.isManualSearch();
    }

    public boolean isAccountExpungeCapable() {
        try {
            return this.mAccount != null && this.mAccount.getRemoteStore().isExpungeCapable();
        } catch (Exception e) {
            return false;
        }
    }

    public void onRemoteSearch() {
        if (this.mHasConnectivity.booleanValue()) {
            onRemoteSearchRequested();
        } else {
            Toast.makeText(getActivity(), getText(R.string.remote_search_unavailable_no_network), 0).show();
        }
    }

    public boolean isRemoteSearch() {
        return this.mRemoteSearchPerformed;
    }

    public boolean isRemoteSearchAllowed() {
        Account searchAccount;
        if (!this.mSearch.isManualSearch() || this.mRemoteSearchPerformed || !this.mSingleFolderMode || (searchAccount = this.mAccount) == null) {
            return false;
        }
        return searchAccount.allowRemoteSearch();
    }

    public boolean onSearchRequested() {
        if (this.mCurrentFolder.loading) {
            Toast toast = Toast.makeText(getActivity(), getString(R.string.remote_search_wait_sync_atomicgonza), 1);
            toast.setGravity(17, 0, 0);
            toast.show();
            return false;
        }
        return this.mFragmentListener.startSearch(this.mAccount, this.mCurrentFolder != null ? this.mCurrentFolder.name : null);
    }

    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri uri;
        String[] projection;
        boolean needConditions;
        String accountUuid = this.mAccountUuids[id];
        Account account = this.mPreferences.getAccount(accountUuid);
        String threadId = getThreadId(this.mSearch);
        if (threadId != null) {
            uri = Uri.withAppendedPath(EmailProvider.CONTENT_URI, "account/" + accountUuid + "/thread/" + threadId);
            projection = PROJECTION;
            needConditions = false;
        } else if (this.mThreadedList) {
            uri = Uri.withAppendedPath(EmailProvider.CONTENT_URI, "account/" + accountUuid + "/messages/threaded");
            projection = THREADED_PROJECTION;
            needConditions = true;
        } else {
            uri = Uri.withAppendedPath(EmailProvider.CONTENT_URI, "account/" + accountUuid + "/messages");
            projection = PROJECTION;
            needConditions = true;
        }
        StringBuilder query = new StringBuilder();
        List<String> queryArgs = new ArrayList<>();
        if (needConditions) {
            boolean selectActive = this.mActiveMessage != null && this.mActiveMessage.getAccountUuid().equals(accountUuid);
            if (selectActive) {
                query.append("(uid = ? AND name = ?) OR (");
                queryArgs.add(this.mActiveMessage.getUid());
                queryArgs.add(this.mActiveMessage.getFolderName());
            }
            SqlQueryBuilder.buildWhereClause(account, this.mSearch.getConditions(), query, queryArgs);
            if (selectActive) {
                query.append(')');
            }
        }
        return new CursorLoader(getActivity(), uri, projection, query.toString(), (String[]) queryArgs.toArray(new String[0]), buildSortOrder());
    }

    private String getThreadId(LocalSearch search) {
        for (ConditionsTreeNode node : search.getLeafSet()) {
            SearchSpecification.SearchCondition condition = node.mCondition;
            if (condition.field == SearchSpecification.SearchField.THREAD_ID) {
                return condition.value;
            }
        }
        return null;
    }

    private String buildSortOrder() {
        String sortColumn;
        String secondarySort;
        switch (this.mSortType) {
            case SORT_ARRIVAL:
                sortColumn = EmailProvider.MessageColumns.INTERNAL_DATE;
                break;
            case SORT_ATTACHMENT:
                sortColumn = "(attachment_count < 1)";
                break;
            case SORT_FLAGGED:
                sortColumn = "(flagged != 1)";
                break;
            case SORT_SENDER:
                sortColumn = EmailProvider.MessageColumns.SENDER_LIST;
                break;
            case SORT_SUBJECT:
                sortColumn = "subject COLLATE NOCASE";
                break;
            case SORT_UNREAD:
                sortColumn = EmailProvider.MessageColumns.READ;
                break;
            default:
                sortColumn = "date";
                break;
        }
        String sortDirection = this.mSortAscending ? " ASC" : " DESC";
        if (this.mSortType == Account.SortType.SORT_DATE || this.mSortType == Account.SortType.SORT_ARRIVAL) {
            secondarySort = "";
        } else {
            secondarySort = "date" + (this.mSortDateAscending ? " ASC, " : " DESC, ");
        }
        return sortColumn + sortDirection + ", " + secondarySort + "id" + " DESC";
    }

    public void onLoaderReset(Loader<Cursor> loader) {
        this.mSelected.clear();
        this.mAdapter.swapCursor(null);
    }

    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        Cursor cursor;
        if (!this.mIsThreadDisplay || data.getCount() != 0) {
            this.mPullToRefreshView.setEmptyView(null);
            setPullToRefreshEnabled(isPullToRefreshAllowed());
            int loaderId = loader.getId();
            this.mCursors[loaderId] = data;
            this.mCursorValid[loaderId] = true;
            if (this.mCursors.length > 1) {
                cursor = new MergeCursorWithUniqueId(this.mCursors, getComparator());
                this.mUniqueIdColumn = cursor.getColumnIndex(AttachmentProvider.AttachmentProviderColumns._ID);
            } else {
                cursor = data;
                this.mUniqueIdColumn = 0;
            }
            if (this.mIsThreadDisplay && cursor.moveToFirst()) {
                this.mTitle = cursor.getString(3);
                if (!TextUtils.isEmpty(this.mTitle)) {
                    this.mTitle = Utility.stripSubject(this.mTitle);
                }
                if (TextUtils.isEmpty(this.mTitle)) {
                    this.mTitle = getString(R.string.general_no_subject);
                }
                updateTitle();
            }
            cleanupSelected(cursor);
            updateContextMenu(cursor);
            this.mAdapter.swapCursor(cursor);
            resetActionMode();
            computeBatchDirection();
            if (isLoadFinished()) {
                if (this.mSavedListState != null) {
                    this.mHandler.restoreListPosition();
                }
                this.mFragmentListener.updateMenu();
                return;
            }
            return;
        }
        this.mHandler.goBack();
    }

    private void updateMoreMessagesOfCurrentFolder() {
        if (this.mFolderName != null) {
            try {
                this.mCurrentFolder.setMoreMessagesFromFolder(getFolder(this.mFolderName, this.mAccount));
            } catch (MessagingException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public boolean isLoadFinished() {
        if (this.mCursorValid == null) {
            return false;
        }
        for (boolean cursorValid : this.mCursorValid) {
            if (!cursorValid) {
                return false;
            }
        }
        return true;
    }

    private void updateContextMenu(Cursor cursor) {
        if (this.mContextMenuUniqueId != 0) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                if (cursor.getLong(this.mUniqueIdColumn) != this.mContextMenuUniqueId) {
                    cursor.moveToNext();
                } else {
                    return;
                }
            }
            this.mContextMenuUniqueId = 0;
            Activity activity = getActivity();
            if (activity != null) {
                activity.closeContextMenu();
            }
        }
    }

    private void cleanupSelected(Cursor cursor) {
        if (!this.mSelected.isEmpty()) {
            Set<Long> selected = new HashSet<>();
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                long uniqueId = cursor.getLong(this.mUniqueIdColumn);
                if (this.mSelected.contains(Long.valueOf(uniqueId))) {
                    selected.add(Long.valueOf(uniqueId));
                }
                cursor.moveToNext();
            }
            this.mSelected = selected;
        }
    }

    private void resetActionMode() {
        if (!this.mSelected.isEmpty()) {
            if (this.mActionMode == null) {
                startAndPrepareActionMode();
            }
            recalculateSelectionCount();
            updateActionModeTitle();
        } else if (this.mActionMode != null) {
            this.mActionMode.finish();
        }
    }

    private void startAndPrepareActionMode() {
        this.mActionMode = getActivity().startActionMode(this.mActionModeCallback);
        this.mActionMode.invalidate();
    }

    private void recalculateSelectionCount() {
        if (!this.mThreadedList) {
            this.mSelectedCount = this.mSelected.size();
            return;
        }
        this.mSelectedCount = 0;
        int end = this.mAdapter.getCount();
        for (int i = 0; i < end; i++) {
            Cursor cursor = (Cursor) this.mAdapter.getItem(i);
            if (this.mSelected.contains(Long.valueOf(cursor.getLong(this.mUniqueIdColumn)))) {
                int threadCount = cursor.getInt(19);
                int i2 = this.mSelectedCount;
                if (threadCount <= 1) {
                    threadCount = 1;
                }
                this.mSelectedCount = i2 + threadCount;
            }
        }
    }

    /* access modifiers changed from: private */
    public Account getAccountFromCursor(Cursor cursor) {
        return this.mPreferences.getAccount(cursor.getString(17));
    }

    /* access modifiers changed from: private */
    public void remoteSearchFinished() {
        this.mRemoteSearchFuture = null;
        this.mFragmentListener.enableActionBarProgress(false);
    }

    public void setActiveMessage(MessageReference messageReference) {
        this.mActiveMessage = messageReference;
        if (isAdded()) {
            restartLoader();
        }
        if (this.mAdapter != null) {
            this.mAdapter.notifyDataSetChanged();
        }
    }

    public boolean isSingleAccountMode() {
        return this.mSingleAccountMode;
    }

    public boolean isSingleFolderMode() {
        return this.mSingleFolderMode;
    }

    public boolean isInitialized() {
        return this.mInitialized;
    }

    public boolean isMarkAllAsReadSupported() {
        return isSingleAccountMode() && isSingleFolderMode();
    }

    public void confirmMarkAllAsRead() {
        showDialog(R.id.dialog_confirm_mark_all_as_read);
    }

    public void markAllAsRead() {
        if (isMarkAllAsReadSupported()) {
            this.mController.markAllMessagesRead(this.mAccount, this.mFolderName);
        }
    }

    public boolean isCheckMailSupported() {
        return this.mAllAccounts || !isSingleAccountMode() || !isSingleFolderMode() || isRemoteFolder();
    }

    private boolean isCheckMailAllowed() {
        return !isManualSearch() && isCheckMailSupported();
    }

    private boolean isPullToRefreshAllowed() {
        return isRemoteSearchAllowed() || isCheckMailAllowed();
    }
}
