package com.fsck.k9.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import com.fsck.k9.K9;
import com.jcraft.jzlib.JZlib;

public class ConfirmationDialogFragment extends DialogFragment implements DialogInterface.OnClickListener, DialogInterface.OnCancelListener {
    private static final String ARG_CANCEL_TEXT = "cancel";
    private static final String ARG_CONFIRM_TEXT = "confirm";
    private static final String ARG_DIALOG_ID = "dialog_id";
    private static final String ARG_MESSAGE = "message";
    private static final String ARG_TITLE = "title";
    private ConfirmationDialogFragmentListener mListener;

    public interface ConfirmationDialogFragmentListener {
        void dialogCancelled(int i);

        void doNegativeClick(int i);

        void doPositiveClick(int i);
    }

    public static ConfirmationDialogFragment newInstance(int dialogId, String title, String message, String confirmText, String cancelText) {
        ConfirmationDialogFragment fragment = new ConfirmationDialogFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_DIALOG_ID, dialogId);
        args.putString(ARG_TITLE, title);
        args.putString(ARG_MESSAGE, message);
        args.putString(ARG_CONFIRM_TEXT, confirmText);
        args.putString(ARG_CANCEL_TEXT, cancelText);
        fragment.setArguments(args);
        return fragment;
    }

    public static ConfirmationDialogFragment newInstance(int dialogId, String title, String message, String cancelText) {
        return newInstance(dialogId, title, message, null, cancelText);
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle args = getArguments();
        String title = args.getString(ARG_TITLE);
        String message = args.getString(ARG_MESSAGE);
        String confirmText = args.getString(ARG_CONFIRM_TEXT);
        String cancelText = args.getString(ARG_CANCEL_TEXT);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title);
        builder.setMessage(message);
        if (confirmText != null && cancelText != null) {
            builder.setPositiveButton(confirmText, this);
            builder.setNegativeButton(cancelText, this);
        } else if (cancelText != null) {
            builder.setNeutralButton(cancelText, this);
        } else {
            throw new RuntimeException("Set at least cancelText!");
        }
        return builder.create();
    }

    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case JZlib.Z_DATA_ERROR /*-3*/:
                getListener().doNegativeClick(getDialogId());
                return;
            case -2:
                getListener().doNegativeClick(getDialogId());
                return;
            case -1:
                getListener().doPositiveClick(getDialogId());
                return;
            default:
                return;
        }
    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        getListener().dialogCancelled(getDialogId());
    }

    private int getDialogId() {
        return getArguments().getInt(ARG_DIALOG_ID);
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.mListener = (ConfirmationDialogFragmentListener) activity;
        } catch (ClassCastException e) {
            if (K9.DEBUG) {
                Log.d("k9", activity.toString() + " did not implement ConfirmationDialogFragmentListener");
            }
        }
    }

    private ConfirmationDialogFragmentListener getListener() {
        if (this.mListener != null) {
            return this.mListener;
        }
        try {
            return (ConfirmationDialogFragmentListener) getTargetFragment();
        } catch (ClassCastException e) {
            throw new ClassCastException(getTargetFragment().getClass() + " must implement ConfirmationDialogFragmentListener");
        }
    }
}
