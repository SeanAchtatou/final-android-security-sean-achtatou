package com.fsck.k9.mailstore;

class ThreadInfo {
    public final String messageId;
    public final long msgId;
    public final long parentId;
    public final long rootId;
    public final long threadId;

    public ThreadInfo(long threadId2, long msgId2, String messageId2, long rootId2, long parentId2) {
        this.threadId = threadId2;
        this.msgId = msgId2;
        this.messageId = messageId2;
        this.rootId = rootId2;
        this.parentId = parentId2;
    }
}
