package com.fsck.k9.mailstore;

import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.Part;
import java.util.List;

public class MessageViewInfo {
    public final AttachmentResolver attachmentResolver;
    public final List<AttachmentViewInfo> attachments;
    public final CryptoResultAnnotation cryptoResultAnnotation;
    public final List<AttachmentViewInfo> extraAttachments;
    public final String extraText;
    public final boolean isMessageIncomplete;
    public final Message message;
    public final Part rootPart;
    public final String text;

    public MessageViewInfo(Message message2, boolean isMessageIncomplete2, Part rootPart2, String text2, List<AttachmentViewInfo> attachments2, CryptoResultAnnotation cryptoResultAnnotation2, AttachmentResolver attachmentResolver2, String extraText2, List<AttachmentViewInfo> extraAttachments2) {
        this.message = message2;
        this.isMessageIncomplete = isMessageIncomplete2;
        this.rootPart = rootPart2;
        this.text = text2;
        this.cryptoResultAnnotation = cryptoResultAnnotation2;
        this.attachmentResolver = attachmentResolver2;
        this.attachments = attachments2;
        this.extraText = extraText2;
        this.extraAttachments = extraAttachments2;
    }

    public static MessageViewInfo createWithExtractedContent(Message message2, boolean isMessageIncomplete2, Part rootPart2, String text2, List<AttachmentViewInfo> attachments2, CryptoResultAnnotation cryptoResultAnnotation2, AttachmentResolver attachmentResolver2, String extraText2, List<AttachmentViewInfo> extraAttachments2) {
        return new MessageViewInfo(message2, isMessageIncomplete2, rootPart2, text2, attachments2, cryptoResultAnnotation2, attachmentResolver2, extraText2, extraAttachments2);
    }

    public static MessageViewInfo createWithErrorState(Message message2, boolean isMessageIncomplete2) {
        return new MessageViewInfo(message2, isMessageIncomplete2, null, null, null, null, null, null, null);
    }
}
