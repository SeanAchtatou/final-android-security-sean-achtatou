package com.fsck.k9.mailstore;

import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.internet.SizeAware;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class TempFileBody extends BinaryAttachmentBody implements SizeAware {
    private final File mFile;

    public /* bridge */ /* synthetic */ String getEncoding() {
        return super.getEncoding();
    }

    public /* bridge */ /* synthetic */ void setEncoding(String str) throws MessagingException {
        super.setEncoding(str);
    }

    public /* bridge */ /* synthetic */ void writeTo(OutputStream outputStream) throws IOException, MessagingException {
        super.writeTo(outputStream);
    }

    public TempFileBody(String filename) {
        this.mFile = new File(filename);
    }

    public InputStream getInputStream() {
        try {
            return new FileInputStream(this.mFile);
        } catch (FileNotFoundException e) {
            return new ByteArrayInputStream(LocalStore.EMPTY_BYTE_ARRAY);
        }
    }

    public long getSize() {
        return this.mFile.length();
    }
}
