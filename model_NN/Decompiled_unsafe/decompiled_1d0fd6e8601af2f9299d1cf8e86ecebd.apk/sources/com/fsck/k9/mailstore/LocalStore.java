package com.fsck.k9.mailstore;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import com.fsck.k9.Account;
import com.fsck.k9.K9;
import com.fsck.k9.Preferences;
import com.fsck.k9.helper.UrlEncodingHelper;
import com.fsck.k9.helper.Utility;
import com.fsck.k9.mail.Flag;
import com.fsck.k9.mail.Folder;
import com.fsck.k9.mail.MessageRetrievalListener;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.Store;
import com.fsck.k9.mailstore.LocalFolder;
import com.fsck.k9.mailstore.LockableDatabase;
import com.fsck.k9.message.extractors.AttachmentCounter;
import com.fsck.k9.message.extractors.MessageFulltextCreator;
import com.fsck.k9.message.extractors.MessagePreviewCreator;
import com.fsck.k9.preferences.SettingsExporter;
import com.fsck.k9.preferences.Storage;
import com.fsck.k9.provider.EmailProvider;
import com.fsck.k9.search.LocalSearch;
import com.fsck.k9.search.SearchSpecification;
import com.fsck.k9.search.SqlQueryBuilder;
import exts.whats.Constants;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.apache.james.mime4j.codec.Base64InputStream;
import org.apache.james.mime4j.codec.QuotedPrintableInputStream;
import org.apache.james.mime4j.util.MimeUtil;

public class LocalStore extends Store implements Serializable {
    public static final int DB_VERSION = 55;
    static final byte[] EMPTY_BYTE_ARRAY = new byte[0];
    static final String[] EMPTY_STRING_ARRAY = new String[0];
    private static final int FLAG_UPDATE_BATCH_SIZE = 500;
    static final int FOLDER_DISPLAY_CLASS_INDEX = 11;
    static final int FOLDER_ID_INDEX = 0;
    static final int FOLDER_INTEGRATE_INDEX = 7;
    static final int FOLDER_LAST_CHECKED_INDEX = 3;
    static final int FOLDER_LAST_PUSHED_INDEX = 6;
    static final int FOLDER_NAME_INDEX = 1;
    static final int FOLDER_NOTIFY_CLASS_INDEX = 12;
    static final int FOLDER_PUSH_CLASS_INDEX = 10;
    static final int FOLDER_PUSH_STATE_INDEX = 5;
    static final int FOLDER_STATUS_INDEX = 4;
    static final int FOLDER_SYNC_CLASS_INDEX = 9;
    static final int FOLDER_TOP_GROUP_INDEX = 8;
    static final int FOLDER_VISIBLE_LIMIT_INDEX = 2;
    static final String GET_FOLDER_COLS = "folders.id, name, visible_limit, last_updated, status, push_state, last_pushed, integrate, top_group, poll_class, push_class, display_class, notify_class, more_messages";
    static String GET_MESSAGES_COLS = "subject, sender_list, date, uid, flags, messages.id, to_list, cc_list, bcc_list, reply_to_list, attachment_count, internal_date, messages.message_id, folder_id, preview, threads.id, threads.root, deleted, read, flagged, answered, forwarded, message_part_id, messages.mime_type, preview_type, header ";
    static final int MORE_MESSAGES_INDEX = 13;
    private static final int THREAD_FLAG_UPDATE_BATCH_SIZE = 500;
    static final int UID_CHECK_BATCH_SIZE = 500;
    static final String[] UID_CHECK_PROJECTION = {EmailProvider.MessageColumns.UID};
    private static ConcurrentMap<String, Object> sAccountLocks = new ConcurrentHashMap();
    private static ConcurrentMap<String, LocalStore> sLocalStores = new ConcurrentHashMap();
    private static final long serialVersionUID = -5142141896809423072L;
    private final AttachmentCounter attachmentCounter;
    final Context context;
    LockableDatabase database;
    /* access modifiers changed from: private */
    public final Account mAccount;
    private ContentResolver mContentResolver;
    private final MessageFulltextCreator messageFulltextCreator;
    private final MessagePreviewCreator messagePreviewCreator;
    protected String uUid = null;

    public static class AttachmentInfo {
        public String name;
        public long size;
        public String type;
    }

    public interface BatchSetSelection {
        void doDbWork(SQLiteDatabase sQLiteDatabase, String str, String[] strArr) throws UnavailableStorageException;

        String getListItem(int i);

        int getListSize();

        void postDbWork();
    }

    public static String getColumnNameForFlag(Flag flag) {
        switch (flag) {
            case SEEN:
                return EmailProvider.MessageColumns.READ;
            case FLAGGED:
                return EmailProvider.MessageColumns.FLAGGED;
            case ANSWERED:
                return EmailProvider.MessageColumns.ANSWERED;
            case FORWARDED:
                return EmailProvider.MessageColumns.FORWARDED;
            default:
                throw new IllegalArgumentException("Flag must be a special column flag");
        }
    }

    private LocalStore(Account account, Context context2) throws MessagingException {
        this.mAccount = account;
        this.database = new LockableDatabase(context2, account.getUuid(), new StoreSchemaDefinition(this));
        this.context = context2;
        this.mContentResolver = context2.getContentResolver();
        this.database.setStorageProviderId(account.getLocalStorageProviderId());
        this.uUid = account.getUuid();
        this.messagePreviewCreator = MessagePreviewCreator.newInstance();
        this.messageFulltextCreator = MessageFulltextCreator.newInstance();
        this.attachmentCounter = AttachmentCounter.newInstance();
        this.database.open();
    }

    public static LocalStore getInstance(Account account, Context context2) throws MessagingException {
        LocalStore store;
        String accountUuid = account.getUuid();
        sAccountLocks.putIfAbsent(accountUuid, new Object());
        synchronized (sAccountLocks.get(accountUuid)) {
            store = sLocalStores.get(accountUuid);
            if (store == null) {
                store = new LocalStore(account, context2);
                sLocalStores.put(accountUuid, store);
            }
        }
        return store;
    }

    public static void removeAccount(Account account) {
        try {
            removeInstance(account);
        } catch (Exception e) {
            Log.e("k9", "Failed to reset local store for account " + account.getUuid(), e);
        }
    }

    private static void removeInstance(Account account) {
        sLocalStores.remove(account.getUuid());
    }

    public void switchLocalStorage(String newStorageProviderId) throws MessagingException {
        this.database.switchProvider(newStorageProviderId);
    }

    /* access modifiers changed from: protected */
    public Account getAccount() {
        return this.mAccount;
    }

    /* access modifiers changed from: protected */
    public Storage getStorage() {
        return Preferences.getPreferences(this.context).getStorage();
    }

    public long getSize() throws MessagingException {
        final StorageManager storageManager = StorageManager.getInstance(this.context);
        final File attachmentDirectory = storageManager.getAttachmentDirectory(this.uUid, this.database.getStorageProviderId());
        return ((Long) this.database.execute(false, new LockableDatabase.DbCallback<Long>() {
            public Long doDbWork(SQLiteDatabase db) {
                File[] files = attachmentDirectory.listFiles();
                long attachmentLength = 0;
                if (files != null) {
                    for (File file : files) {
                        if (file.exists()) {
                            attachmentLength += file.length();
                        }
                    }
                }
                return Long.valueOf(storageManager.getDatabase(LocalStore.this.uUid, LocalStore.this.database.getStorageProviderId()).length() + attachmentLength);
            }
        })).longValue();
    }

    public void compact() throws MessagingException {
        if (K9.DEBUG) {
            Log.i("k9", "Before compaction size = " + getSize());
        }
        this.database.execute(false, new LockableDatabase.DbCallback<Void>() {
            public Void doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException {
                db.execSQL("VACUUM");
                return null;
            }
        });
        if (K9.DEBUG) {
            Log.i("k9", "After compaction size = " + getSize());
        }
    }

    public void clear() throws MessagingException {
        if (K9.DEBUG) {
            Log.i("k9", "Before prune size = " + getSize());
        }
        deleteAllMessageDataFromDisk();
        if (K9.DEBUG) {
            Log.i("k9", "After prune / before compaction size = " + getSize());
            Log.i("k9", "Before clear folder count = " + getFolderCount());
            Log.i("k9", "Before clear message count = " + getMessageCount());
            Log.i("k9", "After prune / before clear size = " + getSize());
        }
        this.database.execute(false, new LockableDatabase.DbCallback<Void>() {
            public Void doDbWork(SQLiteDatabase db) {
                db.delete("threads", null, null);
                db.delete("messages", "deleted = 0", null);
                return null;
            }
        });
        compact();
        if (K9.DEBUG) {
            Log.i("k9", "After clear message count = " + getMessageCount());
            Log.i("k9", "After clear size = " + getSize());
        }
    }

    public int getMessageCount() throws MessagingException {
        return ((Integer) this.database.execute(false, new LockableDatabase.DbCallback<Integer>() {
            public Integer doDbWork(SQLiteDatabase db) {
                Cursor cursor = null;
                try {
                    cursor = db.rawQuery("SELECT COUNT(*) FROM messages", null);
                    cursor.moveToFirst();
                    return Integer.valueOf(cursor.getInt(0));
                } finally {
                    Utility.closeQuietly(cursor);
                }
            }
        })).intValue();
    }

    public int getFolderCount() throws MessagingException {
        return ((Integer) this.database.execute(false, new LockableDatabase.DbCallback<Integer>() {
            public Integer doDbWork(SQLiteDatabase db) {
                Cursor cursor = null;
                try {
                    cursor = db.rawQuery("SELECT COUNT(*) FROM folders", null);
                    cursor.moveToFirst();
                    return Integer.valueOf(cursor.getInt(0));
                } finally {
                    Utility.closeQuietly(cursor);
                }
            }
        })).intValue();
    }

    public LocalFolder getFolder(String name) {
        return new LocalFolder(this, name);
    }

    public List<LocalFolder> getPersonalNamespaces(boolean forceListAll) throws MessagingException {
        final List<LocalFolder> folders = new LinkedList<>();
        try {
            this.database.execute(false, new LockableDatabase.DbCallback<List<? extends Folder>>() {
                public List<? extends Folder> doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException {
                    Cursor cursor = null;
                    try {
                        cursor = db.rawQuery("SELECT folders.id, name, visible_limit, last_updated, status, push_state, last_pushed, integrate, top_group, poll_class, push_class, display_class, notify_class, more_messages FROM folders ORDER BY name ASC", null);
                        while (cursor.moveToNext()) {
                            if (!cursor.isNull(0)) {
                                LocalFolder folder = new LocalFolder(LocalStore.this, cursor.getString(1));
                                folder.open(cursor);
                                folders.add(folder);
                            }
                        }
                        List<? extends Folder> list = folders;
                        Utility.closeQuietly(cursor);
                        return list;
                    } catch (MessagingException e) {
                        throw new LockableDatabase.WrappedException(e);
                    } catch (Throwable th) {
                        Utility.closeQuietly(cursor);
                        throw th;
                    }
                }
            });
            return folders;
        } catch (LockableDatabase.WrappedException e) {
            throw ((MessagingException) e.getCause());
        }
    }

    public void checkSettings() throws MessagingException {
    }

    public void delete() throws UnavailableStorageException {
        this.database.delete();
    }

    public void recreate() throws UnavailableStorageException {
        this.database.recreate();
    }

    private void deleteAllMessageDataFromDisk() throws MessagingException {
        markAllMessagePartsDataAsMissing();
        deleteAllMessagePartsDataFromDisk();
    }

    private void markAllMessagePartsDataAsMissing() throws MessagingException {
        this.database.execute(false, new LockableDatabase.DbCallback<Void>() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
             arg types: [java.lang.String, int]
             candidates:
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
              ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
            public Void doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException {
                ContentValues cv = new ContentValues();
                cv.put("data_location", (Integer) 0);
                db.update("message_parts", cv, null, null);
                return null;
            }
        });
    }

    private void deleteAllMessagePartsDataFromDisk() {
        File[] files = StorageManager.getInstance(this.context).getAttachmentDirectory(this.uUid, this.database.getStorageProviderId()).listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.exists() && !file.delete()) {
                    file.deleteOnExit();
                }
            }
        }
    }

    public void resetVisibleLimits(int visibleLimit) throws MessagingException {
        final ContentValues cv = new ContentValues();
        cv.put(EmailProvider.FolderColumns.VISIBLE_LIMIT, Integer.toString(visibleLimit));
        cv.put("more_messages", LocalFolder.MoreMessages.UNKNOWN.getDatabaseName());
        this.database.execute(false, new LockableDatabase.DbCallback<Void>() {
            public Void doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException {
                db.update(SettingsExporter.FOLDERS_ELEMENT, cv, null, null);
                return null;
            }
        });
    }

    public List<PendingCommand> getPendingCommands() throws MessagingException {
        return (List) this.database.execute(false, new LockableDatabase.DbCallback<List<PendingCommand>>() {
            public List<PendingCommand> doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException {
                Cursor cursor = null;
                try {
                    cursor = db.query("pending_commands", new String[]{"id", "command", "arguments"}, null, null, null, null, "id ASC");
                    List<PendingCommand> commands = new ArrayList<>();
                    while (cursor.moveToNext()) {
                        PendingCommand command = new PendingCommand();
                        long unused = command.mId = cursor.getLong(0);
                        command.command = cursor.getString(1);
                        command.arguments = cursor.getString(2).split(",");
                        for (int i = 0; i < command.arguments.length; i++) {
                            command.arguments[i] = Utility.fastUrlDecode(command.arguments[i]);
                        }
                        commands.add(command);
                    }
                    return commands;
                } finally {
                    Utility.closeQuietly(cursor);
                }
            }
        });
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fsck.k9.helper.Utility.combine(java.lang.Object[], char):java.lang.String
     arg types: [java.lang.String[], int]
     candidates:
      com.fsck.k9.helper.Utility.combine(java.lang.Iterable<?>, char):java.lang.String
      com.fsck.k9.helper.Utility.combine(java.lang.Object[], char):java.lang.String */
    public void addPendingCommand(PendingCommand command) throws MessagingException {
        for (int i = 0; i < command.arguments.length; i++) {
            command.arguments[i] = UrlEncodingHelper.encodeUtf8(command.arguments[i]);
        }
        final ContentValues cv = new ContentValues();
        cv.put("command", command.command);
        cv.put("arguments", Utility.combine((Object[]) command.arguments, ','));
        this.database.execute(false, new LockableDatabase.DbCallback<Void>() {
            public Void doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException {
                db.insert("pending_commands", "command", cv);
                return null;
            }
        });
    }

    public void removePendingCommand(final PendingCommand command) throws MessagingException {
        this.database.execute(false, new LockableDatabase.DbCallback<Void>() {
            public Void doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException {
                db.delete("pending_commands", "id = ?", new String[]{Long.toString(command.mId)});
                return null;
            }
        });
    }

    public void removePendingCommands() throws MessagingException {
        this.database.execute(false, new LockableDatabase.DbCallback<Void>() {
            public Void doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException {
                db.delete("pending_commands", null, null);
                return null;
            }
        });
    }

    public static class PendingCommand {
        public String[] arguments;
        public String command;
        /* access modifiers changed from: private */
        public long mId;

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(this.command);
            sb.append(": ");
            for (String argument : this.arguments) {
                sb.append(", ");
                sb.append(argument);
            }
            return sb.toString();
        }
    }

    public boolean isMoveCapable() {
        return true;
    }

    public boolean isCopyCapable() {
        return true;
    }

    public List<LocalMessage> searchForMessages(MessageRetrievalListener<LocalMessage> retrievalListener, LocalSearch search) throws MessagingException {
        StringBuilder query = new StringBuilder();
        List<String> queryArgs = new ArrayList<>();
        SqlQueryBuilder.buildWhereClause(this.mAccount, search.getConditions(), query, queryArgs);
        String where = SqlQueryBuilder.addPrefixToSelection(new String[]{"id"}, "messages.", query.toString());
        String[] selectionArgs = (String[]) queryArgs.toArray(new String[queryArgs.size()]);
        String sqlQuery = "SELECT " + GET_MESSAGES_COLS + "FROM messages " + "LEFT JOIN threads ON (threads.message_id = messages.id) " + "LEFT JOIN message_parts ON (message_parts.id = messages.message_part_id) " + "LEFT JOIN folders ON (folders.id = messages.folder_id) WHERE " + "(empty = 0 AND deleted = 0)" + (!TextUtils.isEmpty(where) ? " AND (" + where + ")" : "") + " ORDER BY date DESC";
        if (K9.DEBUG) {
            Log.d("k9", "Query = " + sqlQuery);
        }
        return getMessages(retrievalListener, null, sqlQuery, selectionArgs);
    }

    /* access modifiers changed from: package-private */
    public List<LocalMessage> getMessages(MessageRetrievalListener<LocalMessage> listener, LocalFolder folder, String queryString, String[] placeHolders) throws MessagingException {
        final List<LocalMessage> messages = new ArrayList<>();
        final String str = queryString;
        final String[] strArr = placeHolders;
        final LocalFolder localFolder = folder;
        final MessageRetrievalListener<LocalMessage> messageRetrievalListener = listener;
        int j = ((Integer) this.database.execute(false, new LockableDatabase.DbCallback<Integer>() {
            public Integer doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException {
                Cursor cursor = null;
                int i = 0;
                try {
                    Cursor cursor2 = db.rawQuery(str + " LIMIT 10", strArr);
                    while (cursor2.moveToNext()) {
                        LocalMessage message = new LocalMessage(LocalStore.this, null, localFolder);
                        message.populateFromGetMessageCursor(cursor2);
                        messages.add(message);
                        if (messageRetrievalListener != null) {
                            messageRetrievalListener.messageFinished(message, i, -1);
                        }
                        i++;
                    }
                    cursor2.close();
                    cursor = db.rawQuery(str + " LIMIT -1 OFFSET 10", strArr);
                    while (cursor.moveToNext()) {
                        LocalMessage message2 = new LocalMessage(LocalStore.this, null, localFolder);
                        message2.populateFromGetMessageCursor(cursor);
                        messages.add(message2);
                        if (messageRetrievalListener != null) {
                            messageRetrievalListener.messageFinished(message2, i, -1);
                        }
                        i++;
                    }
                } catch (Exception e) {
                    Log.d("k9", "Got an exception", e);
                } finally {
                    Utility.closeQuietly(cursor);
                }
                return Integer.valueOf(i);
            }
        })).intValue();
        if (listener != null) {
            listener.messagesFinished(j);
        }
        return Collections.unmodifiableList(messages);
    }

    public List<LocalMessage> getMessagesInThread(long rootId) throws MessagingException {
        String rootIdString = Long.toString(rootId);
        LocalSearch search = new LocalSearch();
        search.and(SearchSpecification.SearchField.THREAD_ID, rootIdString, SearchSpecification.Attribute.EQUALS);
        return searchForMessages(null, search);
    }

    public AttachmentInfo getAttachmentInfo(final String attachmentId) throws MessagingException {
        return (AttachmentInfo) this.database.execute(false, new LockableDatabase.DbCallback<AttachmentInfo>() {
            public AttachmentInfo doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException {
                Cursor cursor = db.query("message_parts", new String[]{"display_name", "decoded_body_size", EmailProvider.InternalMessageColumns.MIME_TYPE}, "id = ?", new String[]{attachmentId}, null, null, null);
                try {
                    if (!cursor.moveToFirst()) {
                        return null;
                    }
                    String name = cursor.getString(0);
                    long size = cursor.getLong(1);
                    String mimeType = cursor.getString(2);
                    AttachmentInfo attachmentInfo = new AttachmentInfo();
                    attachmentInfo.name = name;
                    attachmentInfo.size = size;
                    attachmentInfo.type = mimeType;
                    cursor.close();
                    return attachmentInfo;
                } finally {
                    cursor.close();
                }
            }
        });
    }

    @Nullable
    public InputStream getAttachmentInputStream(final String attachmentId) throws MessagingException {
        return (InputStream) this.database.execute(false, new LockableDatabase.DbCallback<InputStream>() {
            public InputStream doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException {
                InputStream inputStream = null;
                Cursor cursor = db.query("message_parts", new String[]{"data_location", "data", "encoding"}, "id = ?", new String[]{attachmentId}, null, null, null);
                try {
                    if (cursor.moveToFirst()) {
                        int location = cursor.getInt(0);
                        String encoding = cursor.getString(2);
                        inputStream = LocalStore.this.getDecodingInputStream(LocalStore.this.getRawAttachmentInputStream(cursor, location, attachmentId), encoding);
                        cursor.close();
                    }
                    return inputStream;
                } finally {
                    cursor.close();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    @Nullable
    public InputStream getRawAttachmentInputStream(Cursor cursor, int location, String attachmentId) {
        switch (location) {
            case 1:
                return new ByteArrayInputStream(cursor.getBlob(1));
            case 2:
                try {
                    return new FileInputStream(getAttachmentFile(attachmentId));
                } catch (FileNotFoundException e) {
                    return null;
                }
            default:
                throw new IllegalStateException("No attachment data available");
        }
    }

    /* access modifiers changed from: package-private */
    @Nullable
    public InputStream getDecodingInputStream(@Nullable final InputStream rawInputStream, @Nullable String encoding) {
        if (rawInputStream == null) {
            return null;
        }
        if (MimeUtil.ENC_BASE64.equals(encoding)) {
            return new Base64InputStream(rawInputStream) {
                public void close() throws IOException {
                    super.close();
                    rawInputStream.close();
                }
            };
        }
        if (MimeUtil.ENC_QUOTED_PRINTABLE.equals(encoding)) {
            return new QuotedPrintableInputStream(rawInputStream) {
                public void close() throws IOException {
                    super.close();
                    rawInputStream.close();
                }
            };
        }
        return rawInputStream;
    }

    /* access modifiers changed from: package-private */
    public File getAttachmentFile(String attachmentId) {
        return new File(StorageManager.getInstance(this.context).getAttachmentDirectory(this.uUid, this.database.getStorageProviderId()), attachmentId);
    }

    public void createFolders(final List<LocalFolder> foldersToCreate, final int visibleLimit) throws MessagingException {
        this.database.execute(true, new LockableDatabase.DbCallback<Void>() {
            public Void doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException {
                int i;
                int i2;
                for (LocalFolder folder : foldersToCreate) {
                    String name = folder.getName();
                    folder.getClass();
                    LocalFolder.PreferencesHolder prefHolder = new LocalFolder.PreferencesHolder();
                    if (LocalStore.this.mAccount.isSpecialFolder(name)) {
                        prefHolder.inTopGroup = true;
                        prefHolder.displayClass = Folder.FolderClass.FIRST_CLASS;
                        if (name.equalsIgnoreCase(LocalStore.this.mAccount.getInboxFolderName())) {
                            prefHolder.integrate = true;
                            prefHolder.notifyClass = Folder.FolderClass.FIRST_CLASS;
                            prefHolder.pushClass = Folder.FolderClass.FIRST_CLASS;
                        } else {
                            prefHolder.pushClass = Folder.FolderClass.INHERITED;
                        }
                        if (name.equalsIgnoreCase(LocalStore.this.mAccount.getInboxFolderName()) || name.equalsIgnoreCase(LocalStore.this.mAccount.getDraftsFolderName())) {
                            prefHolder.syncClass = Folder.FolderClass.FIRST_CLASS;
                        } else {
                            prefHolder.syncClass = Folder.FolderClass.NO_CLASS;
                        }
                    }
                    folder.refresh(name, prefHolder);
                    Object[] objArr = new Object[8];
                    objArr[0] = name;
                    objArr[1] = Integer.valueOf(visibleLimit);
                    if (prefHolder.inTopGroup) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    objArr[2] = Integer.valueOf(i);
                    objArr[3] = prefHolder.displayClass.name();
                    objArr[4] = prefHolder.syncClass.name();
                    objArr[5] = prefHolder.notifyClass.name();
                    objArr[6] = prefHolder.pushClass.name();
                    if (prefHolder.integrate) {
                        i2 = 1;
                    } else {
                        i2 = 0;
                    }
                    objArr[7] = Integer.valueOf(i2);
                    db.execSQL("INSERT INTO folders (name, visible_limit, top_group, display_class, poll_class, notify_class, push_class, integrate) VALUES (?, ?, ?, ?, ?, ?, ?, ?)", objArr);
                }
                return null;
            }
        });
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fsck.k9.helper.Utility.combine(java.lang.Iterable<?>, char):java.lang.String
     arg types: [java.util.List<com.fsck.k9.mail.Flag>, int]
     candidates:
      com.fsck.k9.helper.Utility.combine(java.lang.Object[], char):java.lang.String
      com.fsck.k9.helper.Utility.combine(java.lang.Iterable<?>, char):java.lang.String */
    static String serializeFlags(Iterable<Flag> flags) {
        List<Flag> extraFlags = new ArrayList<>();
        for (Flag flag : flags) {
            switch (flag) {
                case SEEN:
                case FLAGGED:
                case ANSWERED:
                case FORWARDED:
                case DELETED:
                    break;
                default:
                    extraFlags.add(flag);
                    break;
            }
        }
        return Utility.combine((Iterable<?>) extraFlags, ',').toUpperCase(Locale.US);
    }

    public LockableDatabase getDatabase() {
        return this.database;
    }

    public MessagePreviewCreator getMessagePreviewCreator() {
        return this.messagePreviewCreator;
    }

    public MessageFulltextCreator getMessageFulltextCreator() {
        return this.messageFulltextCreator;
    }

    public AttachmentCounter getAttachmentCounter() {
        return this.attachmentCounter;
    }

    /* access modifiers changed from: package-private */
    public void notifyChange() {
        this.mContentResolver.notifyChange(Uri.withAppendedPath(EmailProvider.CONTENT_URI, "account/" + this.uUid + "/messages"), null);
    }

    public void doBatchSetSelection(final BatchSetSelection selectionCallback, int batchSize) throws MessagingException {
        final List<String> selectionArgs = new ArrayList<>();
        int start = 0;
        while (start < selectionCallback.getListSize()) {
            final StringBuilder selection = new StringBuilder();
            selection.append(" IN (");
            int count = Math.min(selectionCallback.getListSize() - start, batchSize);
            int end = start + count;
            for (int i = start; i < end; i++) {
                if (i > start) {
                    selection.append(",?");
                } else {
                    selection.append("?");
                }
                selectionArgs.add(selectionCallback.getListItem(i));
            }
            selection.append(")");
            try {
                this.database.execute(true, new LockableDatabase.DbCallback<Void>() {
                    public Void doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException, UnavailableStorageException {
                        selectionCallback.doDbWork(db, selection.toString(), (String[]) selectionArgs.toArray(new String[selectionArgs.size()]));
                        return null;
                    }
                });
                selectionCallback.postDbWork();
                selectionArgs.clear();
                start += count;
            } catch (LockableDatabase.WrappedException e) {
                throw ((MessagingException) e.getCause());
            }
        }
    }

    public void setFlag(final List<Long> messageIds, Flag flag, boolean newState) throws MessagingException {
        final ContentValues cv = new ContentValues();
        cv.put(getColumnNameForFlag(flag), Boolean.valueOf(newState));
        doBatchSetSelection(new BatchSetSelection() {
            public int getListSize() {
                return messageIds.size();
            }

            public String getListItem(int index) {
                return Long.toString(((Long) messageIds.get(index)).longValue());
            }

            public void doDbWork(SQLiteDatabase db, String selectionSet, String[] selectionArgs) throws UnavailableStorageException {
                db.update("messages", cv, "empty = 0 AND id" + selectionSet, selectionArgs);
            }

            public void postDbWork() {
                LocalStore.this.notifyChange();
            }
        }, 500);
    }

    public void setFlagForThreads(final List<Long> threadRootIds, Flag flag, final boolean newState) throws MessagingException {
        final String flagColumn = getColumnNameForFlag(flag);
        doBatchSetSelection(new BatchSetSelection() {
            public int getListSize() {
                return threadRootIds.size();
            }

            public String getListItem(int index) {
                return Long.toString(((Long) threadRootIds.get(index)).longValue());
            }

            public void doDbWork(SQLiteDatabase db, String selectionSet, String[] selectionArgs) throws UnavailableStorageException {
                db.execSQL("UPDATE messages SET " + flagColumn + " = " + (newState ? Constants.INSTALL_ID : "0") + " WHERE id IN (" + "SELECT m.id FROM threads t " + "LEFT JOIN messages m ON (t.message_id = m.id) " + "WHERE m.empty = 0 AND m.deleted = 0 " + "AND t.root" + selectionSet + ")", selectionArgs);
            }

            public void postDbWork() {
                LocalStore.this.notifyChange();
            }
        }, 500);
    }

    public Map<String, List<String>> getFoldersAndUids(final List<Long> messageIds, final boolean threadedList) throws MessagingException {
        final Map<String, List<String>> folderMap = new HashMap<>();
        doBatchSetSelection(new BatchSetSelection() {
            public int getListSize() {
                return messageIds.size();
            }

            public String getListItem(int index) {
                return Long.toString(((Long) messageIds.get(index)).longValue());
            }

            public void doDbWork(SQLiteDatabase db, String selectionSet, String[] selectionArgs) throws UnavailableStorageException {
                if (threadedList) {
                    getDataFromCursor(db.rawQuery("SELECT m.uid, f.name FROM threads t LEFT JOIN messages m ON (t.message_id = m.id) LEFT JOIN folders f ON (m.folder_id = f.id) WHERE m.empty = 0 AND m.deleted = 0 AND t.root" + selectionSet, selectionArgs));
                } else {
                    getDataFromCursor(db.rawQuery("SELECT m.uid, f.name FROM messages m LEFT JOIN folders f ON (m.folder_id = f.id) WHERE m.empty = 0 AND m.id" + selectionSet, selectionArgs));
                }
            }

            private void getDataFromCursor(Cursor cursor) {
                while (cursor.moveToNext()) {
                    try {
                        String uid = cursor.getString(0);
                        String folderName = cursor.getString(1);
                        List<String> uidList = (List) folderMap.get(folderName);
                        if (uidList == null) {
                            uidList = new ArrayList<>();
                            folderMap.put(folderName, uidList);
                        }
                        uidList.add(uid);
                    } finally {
                        cursor.close();
                    }
                }
            }

            public void postDbWork() {
                LocalStore.this.notifyChange();
            }
        }, 500);
        return folderMap;
    }
}
