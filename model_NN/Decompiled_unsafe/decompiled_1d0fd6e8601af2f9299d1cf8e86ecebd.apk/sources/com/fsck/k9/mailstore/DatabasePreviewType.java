package com.fsck.k9.mailstore;

import com.fsck.k9.message.extractors.PreviewResult;
import org.openintents.openpgp.util.OpenPgpApi;

public enum DatabasePreviewType {
    NONE("none", PreviewResult.PreviewType.NONE),
    TEXT("text", PreviewResult.PreviewType.TEXT),
    ENCRYPTED("encrypted", PreviewResult.PreviewType.ENCRYPTED),
    ERROR(OpenPgpApi.RESULT_ERROR, PreviewResult.PreviewType.ERROR);
    
    private final String databaseValue;
    private final PreviewResult.PreviewType previewType;

    private DatabasePreviewType(String databaseValue2, PreviewResult.PreviewType previewType2) {
        this.databaseValue = databaseValue2;
        this.previewType = previewType2;
    }

    public static DatabasePreviewType fromDatabaseValue(String databaseValue2) {
        for (DatabasePreviewType databasePreviewType : values()) {
            if (databasePreviewType.getDatabaseValue().equals(databaseValue2)) {
                return databasePreviewType;
            }
        }
        throw new AssertionError("Unknown database value: " + databaseValue2);
    }

    public static DatabasePreviewType fromPreviewType(PreviewResult.PreviewType previewType2) {
        for (DatabasePreviewType databasePreviewType : values()) {
            if (databasePreviewType.previewType == previewType2) {
                return databasePreviewType;
            }
        }
        throw new AssertionError("Unknown preview type: " + previewType2);
    }

    public String getDatabaseValue() {
        return this.databaseValue;
    }

    public PreviewResult.PreviewType getPreviewType() {
        return this.previewType;
    }
}
