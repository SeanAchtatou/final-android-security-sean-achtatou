package com.fsck.k9.mailstore;

import com.fsck.k9.mail.Body;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.filter.Base64OutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.apache.commons.io.IOUtils;
import org.apache.james.mime4j.codec.QuotedPrintableOutputStream;
import org.apache.james.mime4j.util.MimeUtil;

abstract class BinaryAttachmentBody implements Body {
    protected String mEncoding;

    public abstract InputStream getInputStream();

    BinaryAttachmentBody() {
    }

    public void writeTo(OutputStream out) throws IOException, MessagingException {
        InputStream in = getInputStream();
        boolean closeStream = false;
        try {
            if (MimeUtil.isBase64Encoding(this.mEncoding)) {
                closeStream = true;
                out = new Base64OutputStream(out);
            } else if (MimeUtil.isQuotedPrintableEncoded(this.mEncoding)) {
                closeStream = true;
                out = new QuotedPrintableOutputStream(out, false);
            }
            IOUtils.copy(in, out);
            if (closeStream) {
                out.close();
            }
            in.close();
        } catch (Throwable th) {
            in.close();
            throw th;
        }
    }

    public void setEncoding(String encoding) throws MessagingException {
        this.mEncoding = encoding;
    }

    public String getEncoding() {
        return this.mEncoding;
    }
}
