package com.fsck.k9.mailstore.migrations;

import android.util.Log;
import com.fsck.k9.mail.Folder;
import com.fsck.k9.mailstore.LocalFolder;
import com.fsck.k9.mailstore.LocalStore;
import com.fsck.k9.preferences.Storage;
import com.fsck.k9.preferences.StorageEditor;
import java.util.List;

class MigrationTo42 {
    MigrationTo42() {
    }

    public static void from41MoveFolderPreferences(MigrationsHelper migrationsHelper) {
        try {
            LocalStore localStore = migrationsHelper.getLocalStore();
            Storage storage = migrationsHelper.getStorage();
            long startTime = System.currentTimeMillis();
            StorageEditor editor = storage.edit();
            List<? extends Folder> folders = localStore.getPersonalNamespaces(true);
            for (Folder folder : folders) {
                if (folder instanceof LocalFolder) {
                    ((LocalFolder) folder).save(editor);
                }
            }
            editor.commit();
            Log.i("k9", "Putting folder preferences for " + folders.size() + " folders back into Preferences took " + (System.currentTimeMillis() - startTime) + " ms");
        } catch (Exception e) {
            Log.e("k9", "Could not replace Preferences in upgrade from DB_VERSION 41", e);
        }
    }
}
