package com.fsck.k9.mailstore;

import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.util.Log;
import com.fsck.k9.mail.Body;
import com.fsck.k9.mail.BodyPart;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.Multipart;
import com.fsck.k9.mail.Part;
import com.fsck.k9.message.extractors.AttachmentInfoExtractor;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class AttachmentResolver {
    Map<String, Uri> contentIdToAttachmentUriMap;

    private AttachmentResolver(Map<String, Uri> contentIdToAttachmentUriMap2) {
        this.contentIdToAttachmentUriMap = contentIdToAttachmentUriMap2;
    }

    @Nullable
    public Uri getAttachmentUriForContentId(String cid) {
        return this.contentIdToAttachmentUriMap.get(cid);
    }

    @WorkerThread
    public static AttachmentResolver createFromPart(Part part) {
        return new AttachmentResolver(buildCidToAttachmentUriMap(AttachmentInfoExtractor.getInstance(), part));
    }

    static Map<String, Uri> buildCidToAttachmentUriMap(AttachmentInfoExtractor attachmentInfoExtractor, Part rootPart) {
        HashMap<String, Uri> result = new HashMap<>();
        Stack<Part> partsToCheck = new Stack<>();
        partsToCheck.push(rootPart);
        while (!partsToCheck.isEmpty()) {
            Part part = (Part) partsToCheck.pop();
            Body body = part.getBody();
            if (body instanceof Multipart) {
                for (BodyPart bodyPart : ((Multipart) body).getBodyParts()) {
                    partsToCheck.push(bodyPart);
                }
            } else {
                try {
                    String contentId = part.getContentId();
                    if (contentId != null) {
                        result.put(contentId, attachmentInfoExtractor.extractAttachmentInfo(part).internalUri);
                    }
                } catch (MessagingException e) {
                    Log.e("k9", "Error extracting attachment info", e);
                }
            }
        }
        return Collections.unmodifiableMap(result);
    }
}
