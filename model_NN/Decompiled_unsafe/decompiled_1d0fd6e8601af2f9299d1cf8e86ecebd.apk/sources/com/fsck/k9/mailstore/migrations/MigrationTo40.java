package com.fsck.k9.mailstore.migrations;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

class MigrationTo40 {
    MigrationTo40() {
    }

    public static void addMimeTypeColumn(SQLiteDatabase db) {
        try {
            db.execSQL("ALTER TABLE messages ADD mime_type TEXT");
        } catch (SQLiteException e) {
            Log.e("k9", "Unable to add mime_type column to messages");
        }
    }
}
