package com.fsck.k9.mailstore;

import com.fsck.k9.mail.Body;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.internet.RawDataBody;
import com.fsck.k9.mail.internet.SizeAware;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.apache.commons.io.IOUtils;

public class FileBackedBody implements Body, SizeAware, RawDataBody {
    private final String encoding;
    private final File file;

    public FileBackedBody(File file2, String encoding2) {
        this.file = file2;
        this.encoding = encoding2;
    }

    public InputStream getInputStream() throws MessagingException {
        try {
            return new FileInputStream(this.file);
        } catch (FileNotFoundException e) {
            throw new MessagingException("File not found", e);
        }
    }

    public void setEncoding(String encoding2) throws MessagingException {
        throw new RuntimeException("not supported");
    }

    public void writeTo(OutputStream out) throws IOException, MessagingException {
        InputStream in = getInputStream();
        try {
            IOUtils.copy(in, out);
        } finally {
            in.close();
        }
    }

    public long getSize() {
        return this.file.length();
    }

    public String getEncoding() {
        return this.encoding;
    }
}
