package com.fsck.k9.mailstore;

import com.fsck.k9.mail.Body;
import com.fsck.k9.mail.BodyPart;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.Multipart;
import com.fsck.k9.mail.Part;
import com.fsck.k9.mail.internet.MimeBodyPart;
import com.fsck.k9.mail.internet.MimeMessage;
import com.fsck.k9.mail.internet.MimeMultipart;
import com.fsck.k9.mailstore.util.FileFactory;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Stack;
import org.apache.commons.io.IOUtils;
import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.io.EOLConvertingInputStream;
import org.apache.james.mime4j.parser.ContentHandler;
import org.apache.james.mime4j.parser.MimeStreamParser;
import org.apache.james.mime4j.stream.BodyDescriptor;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.stream.MimeConfig;

public class MimePartStreamParser {
    public static MimeBodyPart parse(FileFactory fileFactory, InputStream inputStream) throws MessagingException, IOException {
        MimeBodyPart parsedRootPart = new MimeBodyPart();
        MimeConfig parserConfig = new MimeConfig();
        parserConfig.setMaxHeaderLen(-1);
        parserConfig.setMaxLineLen(-1);
        parserConfig.setMaxHeaderCount(-1);
        MimeStreamParser parser = new MimeStreamParser(parserConfig);
        parser.setContentHandler(new PartBuilder(fileFactory, parsedRootPart));
        parser.setRecurse();
        try {
            parser.parse(new EOLConvertingInputStream(inputStream));
            return parsedRootPart;
        } catch (MimeException e) {
            throw new MessagingException("Failed to parse decrypted content", e);
        }
    }

    /* access modifiers changed from: private */
    public static Body createBody(InputStream inputStream, String transferEncoding, FileFactory fileFactory) throws IOException {
        DeferredFileBody body = new DeferredFileBody(fileFactory, transferEncoding);
        OutputStream outputStream = body.getOutputStream();
        try {
            IOUtils.copy(inputStream, outputStream);
            return body;
        } finally {
            outputStream.close();
        }
    }

    private static class PartBuilder implements ContentHandler {
        private final MimeBodyPart decryptedRootPart;
        private final FileFactory fileFactory;
        private final Stack<Object> stack = new Stack<>();

        public PartBuilder(FileFactory fileFactory2, MimeBodyPart decryptedRootPart2) throws MessagingException {
            this.fileFactory = fileFactory2;
            this.decryptedRootPart = decryptedRootPart2;
        }

        public void startMessage() throws MimeException {
            if (this.stack.isEmpty()) {
                this.stack.push(this.decryptedRootPart);
                return;
            }
            Message innerMessage = new MimeMessage();
            ((Part) this.stack.peek()).setBody(innerMessage);
            this.stack.push(innerMessage);
        }

        public void endMessage() throws MimeException {
            this.stack.pop();
        }

        public void startBodyPart() throws MimeException {
            try {
                BodyPart bodyPart = new MimeBodyPart();
                ((Multipart) this.stack.peek()).addBodyPart(bodyPart);
                this.stack.push(bodyPart);
            } catch (MessagingException e) {
                throw new MimeException(e);
            }
        }

        public void endBodyPart() throws MimeException {
            this.stack.pop();
        }

        public void startHeader() throws MimeException {
        }

        public void field(Field parsedField) throws MimeException {
            ((Part) this.stack.peek()).addRawHeader(parsedField.getName(), parsedField.getRaw().toString());
        }

        public void endHeader() throws MimeException {
        }

        public void preamble(InputStream is) throws MimeException, IOException {
        }

        public void epilogue(InputStream is) throws MimeException, IOException {
        }

        public void startMultipart(BodyDescriptor bd) throws MimeException {
            MimeMultipart multipart = new MimeMultipart(bd.getMimeType(), bd.getBoundary());
            ((Part) this.stack.peek()).setBody(multipart);
            this.stack.push(multipart);
        }

        public void endMultipart() throws MimeException {
            this.stack.pop();
        }

        public void body(BodyDescriptor bd, InputStream inputStream) throws MimeException, IOException {
            ((Part) this.stack.peek()).setBody(MimePartStreamParser.createBody(inputStream, bd.getTransferEncoding(), this.fileFactory));
        }

        public void raw(InputStream is) throws MimeException, IOException {
            throw new IllegalStateException("Not implemented");
        }
    }
}
