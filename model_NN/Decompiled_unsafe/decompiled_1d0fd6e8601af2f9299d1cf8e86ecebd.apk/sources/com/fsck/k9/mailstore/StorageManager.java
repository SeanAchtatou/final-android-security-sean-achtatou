package com.fsck.k9.mailstore;

import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import com.fsck.k9.K9;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.IdentityHashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import yahoo.mail.app.R;

public class StorageManager {
    private static transient StorageManager instance;
    protected final Context context;
    private List<StorageListener> mListeners = new ArrayList();
    private final Map<StorageProvider, SynchronizationAid> mProviderLocks = new IdentityHashMap();
    private final Map<String, StorageProvider> mProviders = new LinkedHashMap();

    public interface StorageListener {
        void onMount(String str);

        void onUnmount(String str);
    }

    public interface StorageProvider {
        File getAttachmentDirectory(Context context, String str);

        File getDatabase(Context context, String str);

        String getId();

        String getName(Context context);

        File getRoot(Context context);

        void init(Context context);

        boolean isReady(Context context);

        boolean isSupported(Context context);
    }

    public static abstract class FixedStorageProviderBase implements StorageProvider {
        private File mApplicationDir;
        private File mRoot;

        /* access modifiers changed from: protected */
        public abstract File computeRoot(Context context);

        /* access modifiers changed from: protected */
        public abstract boolean supportsVendor();

        public void init(Context context) {
            this.mRoot = computeRoot(context);
            this.mApplicationDir = new File(this.mRoot, "k9");
        }

        public boolean isReady(Context context) {
            try {
                if (!StorageManager.isMountPoint(this.mRoot.getCanonicalFile()) || !"mounted".equals(Environment.getExternalStorageState())) {
                    return false;
                }
                return true;
            } catch (IOException e) {
                Log.w("k9", "Specified root isn't ready: " + this.mRoot, e);
                return false;
            }
        }

        public final boolean isSupported(Context context) {
            return this.mRoot.isDirectory() && supportsVendor();
        }

        public File getDatabase(Context context, String id) {
            return new File(this.mApplicationDir, id + ".db");
        }

        public File getAttachmentDirectory(Context context, String id) {
            return new File(this.mApplicationDir, id + ".db_att");
        }

        public final File getRoot(Context context) {
            return this.mRoot;
        }
    }

    public static class InternalStorageProvider implements StorageProvider {
        public static final String ID = "InternalStorage";
        private File mRoot;

        public String getId() {
            return ID;
        }

        public void init(Context context) {
            this.mRoot = new File("/");
        }

        public String getName(Context context) {
            return context.getString(R.string.local_storage_provider_internal_label);
        }

        public boolean isSupported(Context context) {
            return true;
        }

        public File getDatabase(Context context, String id) {
            return context.getDatabasePath(id + ".db");
        }

        public File getAttachmentDirectory(Context context, String id) {
            return context.getDatabasePath(id + ".db_att");
        }

        public boolean isReady(Context context) {
            return true;
        }

        public File getRoot(Context context) {
            return this.mRoot;
        }
    }

    public static class ExternalStorageProvider implements StorageProvider {
        public static final String ID = "ExternalStorage";
        private File mApplicationDirectory;
        private File mRoot;

        public String getId() {
            return ID;
        }

        public void init(Context context) {
            this.mRoot = Environment.getExternalStorageDirectory();
            this.mApplicationDirectory = new File(new File(new File(new File(this.mRoot, "Android"), "data"), context.getPackageName()), "files");
        }

        public String getName(Context context) {
            return context.getString(R.string.local_storage_provider_external_label);
        }

        public boolean isSupported(Context context) {
            return true;
        }

        public File getDatabase(Context context, String id) {
            return new File(this.mApplicationDirectory, id + ".db");
        }

        public File getAttachmentDirectory(Context context, String id) {
            return new File(this.mApplicationDirectory, id + ".db_att");
        }

        public boolean isReady(Context context) {
            return "mounted".equals(Environment.getExternalStorageState());
        }

        public File getRoot(Context context) {
            return this.mRoot;
        }
    }

    public static class HtcIncredibleStorageProvider extends FixedStorageProviderBase {
        public static final String ID = "HtcIncredibleStorage";

        public String getId() {
            return ID;
        }

        public String getName(Context context) {
            return context.getString(R.string.local_storage_provider_samsunggalaxy_label, Build.MODEL);
        }

        /* access modifiers changed from: protected */
        public boolean supportsVendor() {
            return "inc".equals(Build.DEVICE);
        }

        /* access modifiers changed from: protected */
        public File computeRoot(Context context) {
            return new File("/emmc");
        }
    }

    public static class SamsungGalaxySStorageProvider extends FixedStorageProviderBase {
        public static final String ID = "SamsungGalaxySStorage";

        public String getId() {
            return ID;
        }

        public String getName(Context context) {
            return context.getString(R.string.local_storage_provider_samsunggalaxy_label, Build.MODEL);
        }

        /* access modifiers changed from: protected */
        public boolean supportsVendor() {
            return "GT-I5800".equals(Build.DEVICE) || "GT-I9000".equals(Build.DEVICE) || "SGH-T959".equals(Build.DEVICE) || "SGH-I897".equals(Build.DEVICE);
        }

        /* access modifiers changed from: protected */
        public File computeRoot(Context context) {
            return Environment.getExternalStorageDirectory();
        }
    }

    public static class SynchronizationAid {
        public final Lock readLock;
        public boolean unmounting = false;
        public final Lock writeLock;

        public SynchronizationAid() {
            ReadWriteLock readWriteLock = new ReentrantReadWriteLock(true);
            this.readLock = readWriteLock.readLock();
            this.writeLock = readWriteLock.writeLock();
        }
    }

    public static synchronized StorageManager getInstance(Context context2) {
        StorageManager storageManager;
        synchronized (StorageManager.class) {
            if (instance == null) {
                instance = new StorageManager(context2.getApplicationContext());
            }
            storageManager = instance;
        }
        return storageManager;
    }

    public static boolean isMountPoint(File file) {
        for (File root : File.listRoots()) {
            if (root.equals(file)) {
                return true;
            }
        }
        return false;
    }

    protected StorageManager(Context context2) throws NullPointerException {
        if (context2 == null) {
            throw new NullPointerException("No Context given");
        }
        this.context = context2;
        for (StorageProvider provider : Arrays.asList(new InternalStorageProvider(), new ExternalStorageProvider())) {
            if (provider.isSupported(context2)) {
                provider.init(context2);
                this.mProviders.put(provider.getId(), provider);
                this.mProviderLocks.put(provider, new SynchronizationAid());
            }
        }
    }

    public String getDefaultProviderId() {
        return this.mProviders.keySet().iterator().next();
    }

    /* access modifiers changed from: protected */
    public StorageProvider getProvider(String providerId) {
        return this.mProviders.get(providerId);
    }

    public File getDatabase(String dbName, String providerId) {
        return getProvider(providerId).getDatabase(this.context, dbName);
    }

    public File getAttachmentDirectory(String dbName, String providerId) {
        return getProvider(providerId).getAttachmentDirectory(this.context, dbName);
    }

    public boolean isReady(String providerId) {
        StorageProvider provider = getProvider(providerId);
        if (provider != null) {
            return provider.isReady(this.context);
        }
        Log.w("k9", "Storage-Provider \"" + providerId + "\" does not exist");
        return false;
    }

    public Map<String, String> getAvailableProviders() {
        Map<String, String> result = new LinkedHashMap<>();
        for (Map.Entry<String, StorageProvider> entry : this.mProviders.entrySet()) {
            result.put(entry.getKey(), ((StorageProvider) entry.getValue()).getName(this.context));
        }
        return result;
    }

    public void onBeforeUnmount(String path) {
        Log.i("k9", "storage path \"" + path + "\" unmounting");
        StorageProvider provider = resolveProvider(path);
        if (provider != null) {
            for (StorageListener listener : this.mListeners) {
                try {
                    listener.onUnmount(provider.getId());
                } catch (Exception e) {
                    Log.w("k9", "Error while notifying StorageListener", e);
                }
            }
            SynchronizationAid sync = this.mProviderLocks.get(resolveProvider(path));
            sync.writeLock.lock();
            sync.unmounting = true;
            sync.writeLock.unlock();
        }
    }

    public void onAfterUnmount(String path) {
        Log.i("k9", "storage path \"" + path + "\" unmounted");
        if (resolveProvider(path) != null) {
            SynchronizationAid sync = this.mProviderLocks.get(resolveProvider(path));
            sync.writeLock.lock();
            sync.unmounting = false;
            sync.writeLock.unlock();
            K9.setServicesEnabled(this.context);
        }
    }

    public void onMount(String path, boolean readOnly) {
        StorageProvider provider;
        Log.i("k9", "storage path \"" + path + "\" mounted readOnly=" + readOnly);
        if (!readOnly && (provider = resolveProvider(path)) != null) {
            for (StorageListener listener : this.mListeners) {
                try {
                    listener.onMount(provider.getId());
                } catch (Exception e) {
                    Log.w("k9", "Error while notifying StorageListener", e);
                }
            }
            K9.setServicesEnabled(this.context);
        }
    }

    /* access modifiers changed from: protected */
    public StorageProvider resolveProvider(String path) {
        for (StorageProvider provider : this.mProviders.values()) {
            if (path.equals(provider.getRoot(this.context).getAbsolutePath())) {
                return provider;
            }
        }
        return null;
    }

    public void addListener(StorageListener listener) {
        this.mListeners.add(listener);
    }

    public void removeListener(StorageListener listener) {
        this.mListeners.remove(listener);
    }

    public void lockProvider(String providerId) throws UnavailableStorageException {
        StorageProvider provider = getProvider(providerId);
        if (provider == null) {
            throw new UnavailableStorageException("StorageProvider not found: " + providerId);
        }
        SynchronizationAid sync = this.mProviderLocks.get(provider);
        boolean locked = sync.readLock.tryLock();
        if (!locked || (locked && sync.unmounting)) {
            if (locked) {
                sync.readLock.unlock();
            }
            throw new UnavailableStorageException("StorageProvider is unmounting");
        } else if (locked && !provider.isReady(this.context)) {
            sync.readLock.unlock();
            throw new UnavailableStorageException("StorageProvider not ready");
        }
    }

    public void unlockProvider(String providerId) {
        this.mProviderLocks.get(getProvider(providerId)).readLock.unlock();
    }
}
