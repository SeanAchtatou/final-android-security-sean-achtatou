package com.fsck.k9.mailstore.migrations;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

class MigrationTo37 {
    MigrationTo37() {
    }

    public static void addAttachmentsContentDispositionColumn(SQLiteDatabase db) {
        try {
            db.execSQL("ALTER TABLE attachments ADD content_disposition TEXT");
        } catch (SQLiteException e) {
            Log.e("k9", "Unable to add content_disposition column to attachments");
        }
    }
}
