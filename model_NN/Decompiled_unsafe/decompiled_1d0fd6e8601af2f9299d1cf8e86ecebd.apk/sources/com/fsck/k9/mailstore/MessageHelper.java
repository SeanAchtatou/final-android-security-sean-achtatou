package com.fsck.k9.mailstore;

import com.fsck.k9.mail.Body;
import com.fsck.k9.mail.BodyPart;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.Multipart;
import com.fsck.k9.mail.Part;
import com.fsck.k9.mail.internet.MimeBodyPart;
import java.util.Stack;

public class MessageHelper {
    public static boolean isCompletePartAvailable(Part part) {
        boolean isBodyMissing;
        Stack<Part> partsToCheck = new Stack<>();
        partsToCheck.push(part);
        while (!partsToCheck.isEmpty()) {
            Body body = ((Part) partsToCheck.pop()).getBody();
            if (body == null) {
                isBodyMissing = true;
            } else {
                isBodyMissing = false;
            }
            if (isBodyMissing) {
                return false;
            }
            if (body instanceof Multipart) {
                for (BodyPart bodyPart : ((Multipart) body).getBodyParts()) {
                    partsToCheck.push(bodyPart);
                }
            }
        }
        return true;
    }

    public static MimeBodyPart createEmptyPart() {
        try {
            return new MimeBodyPart(null);
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
}
