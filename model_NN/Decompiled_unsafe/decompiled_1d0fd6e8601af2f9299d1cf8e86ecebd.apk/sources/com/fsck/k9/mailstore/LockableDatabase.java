package com.fsck.k9.mailstore;

import android.annotation.TargetApi;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Build;
import android.util.Log;
import com.fsck.k9.K9;
import com.fsck.k9.helper.FileHelper;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mailstore.StorageManager;
import java.io.File;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class LockableDatabase {
    private Context context;
    private ThreadLocal<Boolean> inTransaction = new ThreadLocal<>();
    /* access modifiers changed from: private */
    public SQLiteDatabase mDb;
    private final Lock mReadLock;
    private SchemaDefinition mSchemaDefinition;
    private final StorageListener mStorageListener = new StorageListener();
    /* access modifiers changed from: private */
    public String mStorageProviderId;
    private final Lock mWriteLock;
    /* access modifiers changed from: private */
    public String uUid;

    public interface DbCallback<T> {
        T doDbWork(SQLiteDatabase sQLiteDatabase) throws WrappedException, MessagingException;
    }

    public interface SchemaDefinition {
        void doDbUpgrade(SQLiteDatabase sQLiteDatabase);

        int getVersion();
    }

    public static class WrappedException extends RuntimeException {
        private static final long serialVersionUID = 8184421232587399369L;

        public WrappedException(Exception cause) {
            super(cause);
        }
    }

    private class StorageListener implements StorageManager.StorageListener {
        private StorageListener() {
        }

        public void onUnmount(String providerId) {
            if (providerId.equals(LockableDatabase.this.mStorageProviderId)) {
                if (K9.DEBUG) {
                    Log.d("k9", "LockableDatabase: Closing DB " + LockableDatabase.this.uUid + " due to unmount event on StorageProvider: " + providerId);
                }
                try {
                    LockableDatabase.this.lockWrite();
                    LockableDatabase.this.mDb.close();
                    LockableDatabase.this.unlockWrite();
                } catch (UnavailableStorageException e) {
                    Log.w("k9", "Unable to writelock on unmount", e);
                } catch (Throwable th) {
                    LockableDatabase.this.unlockWrite();
                    throw th;
                }
            }
        }

        public void onMount(String providerId) {
            if (providerId.equals(LockableDatabase.this.mStorageProviderId)) {
                if (K9.DEBUG) {
                    Log.d("k9", "LockableDatabase: Opening DB " + LockableDatabase.this.uUid + " due to mount event on StorageProvider: " + providerId);
                }
                try {
                    LockableDatabase.this.openOrCreateDataspace();
                } catch (UnavailableStorageException e) {
                    Log.e("k9", "Unable to open DB on mount", e);
                }
            }
        }
    }

    public LockableDatabase(Context context2, String uUid2, SchemaDefinition schemaDefinition) {
        ReadWriteLock lock = new ReentrantReadWriteLock(true);
        this.mReadLock = lock.readLock();
        this.mWriteLock = lock.writeLock();
        this.context = context2;
        this.uUid = uUid2;
        this.mSchemaDefinition = schemaDefinition;
    }

    public void setStorageProviderId(String mStorageProviderId2) {
        this.mStorageProviderId = mStorageProviderId2;
    }

    public String getStorageProviderId() {
        return this.mStorageProviderId;
    }

    private StorageManager getStorageManager() {
        return StorageManager.getInstance(this.context);
    }

    /* access modifiers changed from: protected */
    public void lockRead() throws UnavailableStorageException {
        this.mReadLock.lock();
        try {
            getStorageManager().lockProvider(this.mStorageProviderId);
        } catch (UnavailableStorageException | RuntimeException e) {
            this.mReadLock.unlock();
            throw e;
        }
    }

    /* access modifiers changed from: protected */
    public void unlockRead() {
        getStorageManager().unlockProvider(this.mStorageProviderId);
        this.mReadLock.unlock();
    }

    /* access modifiers changed from: protected */
    public void lockWrite() throws UnavailableStorageException {
        lockWrite(this.mStorageProviderId);
    }

    /* access modifiers changed from: protected */
    public void lockWrite(String providerId) throws UnavailableStorageException {
        this.mWriteLock.lock();
        try {
            getStorageManager().lockProvider(providerId);
        } catch (UnavailableStorageException | RuntimeException e) {
            this.mWriteLock.unlock();
            throw e;
        }
    }

    /* access modifiers changed from: protected */
    public void unlockWrite() {
        unlockWrite(this.mStorageProviderId);
    }

    /* access modifiers changed from: protected */
    public void unlockWrite(String providerId) {
        getStorageManager().unlockProvider(providerId);
        this.mWriteLock.unlock();
    }

    public <T> T execute(boolean transactional, DbCallback<T> callback) throws MessagingException {
        boolean debug;
        long begin;
        long begin2;
        boolean doTransaction = true;
        lockRead();
        if (!transactional || this.inTransaction.get() != null) {
            doTransaction = false;
        }
        try {
            debug = K9.DEBUG;
            if (doTransaction) {
                this.inTransaction.set(Boolean.TRUE);
                this.mDb.beginTransaction();
            }
            T result = callback.doDbWork(this.mDb);
            if (doTransaction) {
                this.mDb.setTransactionSuccessful();
            }
            if (doTransaction) {
                if (debug) {
                    begin2 = System.currentTimeMillis();
                } else {
                    begin2 = 0;
                }
                this.mDb.endTransaction();
                if (debug) {
                    Log.v("k9", "LockableDatabase: Transaction ended, took " + Long.toString(System.currentTimeMillis() - begin2) + "ms / " + new Exception().getStackTrace()[1].toString());
                }
            }
            if (doTransaction) {
                this.inTransaction.set(null);
            }
            unlockRead();
            return result;
        } catch (Throwable th) {
            if (doTransaction) {
                this.inTransaction.set(null);
            }
            unlockRead();
            throw th;
        }
    }

    public void switchProvider(String oldProviderId) throws MessagingException {
        String oldProviderId2;
        if (oldProviderId2.equals(this.mStorageProviderId)) {
            Log.v("k9", "LockableDatabase: Ignoring provider switch request as they are equal: " + oldProviderId2);
            return;
        }
        oldProviderId2 = this.mStorageProviderId;
        lockWrite(oldProviderId2);
        try {
            lockWrite(oldProviderId2);
            try {
                this.mDb.close();
            } catch (Exception e) {
                Log.i("k9", "Unable to close DB on local store migration", e);
            } catch (Throwable th) {
                unlockWrite(oldProviderId2);
                throw th;
            }
            StorageManager storageManager = getStorageManager();
            File oldDatabase = storageManager.getDatabase(this.uUid, oldProviderId2);
            prepareStorage(oldProviderId2);
            FileHelper.moveRecursive(oldDatabase, storageManager.getDatabase(this.uUid, oldProviderId2));
            FileHelper.moveRecursive(storageManager.getAttachmentDirectory(this.uUid, oldProviderId2), storageManager.getAttachmentDirectory(this.uUid, oldProviderId2));
            deleteDatabase(oldDatabase);
            this.mStorageProviderId = oldProviderId2;
            openOrCreateDataspace();
            unlockWrite(oldProviderId2);
        } finally {
            unlockWrite(oldProviderId2);
        }
    }

    /* JADX INFO: finally extract failed */
    public void open() throws UnavailableStorageException {
        lockWrite();
        try {
            openOrCreateDataspace();
            unlockWrite();
            StorageManager.getInstance(this.context).addListener(this.mStorageListener);
        } catch (Throwable th) {
            unlockWrite();
            throw th;
        }
    }

    /* access modifiers changed from: private */
    public void openOrCreateDataspace() throws UnavailableStorageException {
        File databaseFile;
        lockWrite();
        try {
            databaseFile = prepareStorage(this.mStorageProviderId);
            doOpenOrCreateDb(databaseFile);
        } catch (SQLiteException e) {
            Log.w("k9", "Unable to open DB " + databaseFile + " - removing file and retrying", e);
            if (databaseFile.exists() && !databaseFile.delete()) {
                Log.d("k9", "Failed to remove " + databaseFile + " that couldn't be opened");
            }
            doOpenOrCreateDb(databaseFile);
        } catch (Throwable th) {
            unlockWrite();
            throw th;
        }
        if (this.mDb.getVersion() != this.mSchemaDefinition.getVersion()) {
            this.mSchemaDefinition.doDbUpgrade(this.mDb);
        }
        unlockWrite();
    }

    private void doOpenOrCreateDb(File databaseFile) {
        if (StorageManager.InternalStorageProvider.ID.equals(this.mStorageProviderId)) {
            this.mDb = this.context.openOrCreateDatabase(databaseFile.getName(), 0, null);
        } else {
            this.mDb = SQLiteDatabase.openOrCreateDatabase(databaseFile, (SQLiteDatabase.CursorFactory) null);
        }
    }

    /* access modifiers changed from: protected */
    public File prepareStorage(String providerId) throws UnavailableStorageException {
        StorageManager storageManager = getStorageManager();
        File databaseFile = storageManager.getDatabase(this.uUid, providerId);
        File databaseParentDir = databaseFile.getParentFile();
        if (databaseParentDir.isFile()) {
            databaseParentDir.delete();
        }
        if (!databaseParentDir.exists()) {
            if (!databaseParentDir.mkdirs()) {
                throw new UnavailableStorageException("Unable to access: " + databaseParentDir);
            }
            FileHelper.touchFile(databaseParentDir, ".nomedia");
        }
        File attachmentDir = storageManager.getAttachmentDirectory(this.uUid, providerId);
        File attachmentParentDir = attachmentDir.getParentFile();
        if (!attachmentParentDir.exists()) {
            attachmentParentDir.mkdirs();
            FileHelper.touchFile(attachmentParentDir, ".nomedia");
        }
        if (!attachmentDir.exists()) {
            attachmentDir.mkdirs();
        }
        return databaseFile;
    }

    public void delete() throws UnavailableStorageException {
        delete(false);
    }

    public void recreate() throws UnavailableStorageException {
        delete(true);
    }

    private void delete(boolean recreate) throws UnavailableStorageException {
        lockWrite();
        try {
            this.mDb.close();
        } catch (Exception e) {
            if (K9.DEBUG) {
                Log.d("k9", "Exception caught in DB close: " + e.getMessage());
            }
        } catch (Throwable th) {
            unlockWrite();
            throw th;
        }
        StorageManager storageManager = getStorageManager();
        try {
            File attachmentDirectory = storageManager.getAttachmentDirectory(this.uUid, this.mStorageProviderId);
            for (File attachment : attachmentDirectory.listFiles()) {
                if (attachment.exists() && !attachment.delete() && K9.DEBUG) {
                    Log.d("k9", "Attachment was not deleted!");
                }
            }
            if (attachmentDirectory.exists() && !attachmentDirectory.delete() && K9.DEBUG) {
                Log.d("k9", "Attachment directory was not deleted!");
            }
        } catch (Exception e2) {
            if (K9.DEBUG) {
                Log.d("k9", "Exception caught in clearing attachments: " + e2.getMessage());
            }
        }
        try {
            deleteDatabase(storageManager.getDatabase(this.uUid, this.mStorageProviderId));
        } catch (Exception e3) {
            Log.i("k9", "LockableDatabase: delete(): Unable to delete backing DB file", e3);
        }
        if (recreate) {
            openOrCreateDataspace();
        } else {
            getStorageManager().removeListener(this.mStorageListener);
        }
        unlockWrite();
    }

    @TargetApi(16)
    private void deleteDatabase(File database) {
        boolean deleted;
        if (Build.VERSION.SDK_INT >= 16) {
            deleted = SQLiteDatabase.deleteDatabase(database);
        } else {
            deleted = database.delete() | new File(database.getPath() + "-journal").delete();
        }
        if (!deleted) {
            Log.i("k9", "LockableDatabase: deleteDatabase(): No files deleted.");
        }
    }
}
