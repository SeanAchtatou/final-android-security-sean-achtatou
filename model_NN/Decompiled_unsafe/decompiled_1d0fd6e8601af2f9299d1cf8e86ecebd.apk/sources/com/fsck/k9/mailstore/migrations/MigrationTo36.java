package com.fsck.k9.mailstore.migrations;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

class MigrationTo36 {
    MigrationTo36() {
    }

    public static void addAttachmentsContentIdColumn(SQLiteDatabase db) {
        try {
            db.execSQL("ALTER TABLE attachments ADD content_id TEXT");
        } catch (SQLiteException e) {
            Log.e("k9", "Unable to add content_id column to attachments");
        }
    }
}
