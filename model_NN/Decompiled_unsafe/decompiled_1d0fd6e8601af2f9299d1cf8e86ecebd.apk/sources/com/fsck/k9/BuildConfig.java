package com.fsck.k9;

public final class BuildConfig {
    public static final String APPLICATION_ID = "yahoo.mail.app";
    public static final String BUILD_TYPE = "release";
    public static final boolean DEBUG = false;
    public static final boolean DEVELOPER_MODE = false;
    public static final String FLAVOR = "";
    public static final int VERSION_CODE = 8;
    public static final String VERSION_NAME = "1.8";
}
