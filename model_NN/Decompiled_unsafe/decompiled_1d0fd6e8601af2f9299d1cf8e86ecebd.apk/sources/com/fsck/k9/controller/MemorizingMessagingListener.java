package com.fsck.k9.controller;

import com.fsck.k9.Account;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

class MemorizingMessagingListener extends MessagingListener {
    Map<String, Memory> memories = new HashMap(31);

    private enum MemorizingState {
        STARTED,
        FINISHED,
        FAILED
    }

    MemorizingMessagingListener() {
    }

    private Memory getMemory(Account account, String folderName) {
        Memory memory = this.memories.get(getMemoryKey(account, folderName));
        if (memory != null) {
            return memory;
        }
        Memory memory2 = new Memory(account, folderName);
        this.memories.put(getMemoryKey(memory2.account, memory2.folderName), memory2);
        return memory2;
    }

    /* access modifiers changed from: package-private */
    public synchronized void removeAccount(Account account) {
        Iterator<Map.Entry<String, Memory>> memIt = this.memories.entrySet().iterator();
        while (memIt.hasNext()) {
            if (((Memory) memIt.next().getValue()).account.getUuid().equals(account.getUuid())) {
                memIt.remove();
            }
        }
    }

    public synchronized void synchronizeMailboxStarted(Account account, String folder) {
        Memory memory = getMemory(account, folder);
        memory.syncingState = MemorizingState.STARTED;
        memory.folderCompleted = 0;
        memory.folderTotal = 0;
    }

    public synchronized void synchronizeMailboxFinished(Account account, String folder, int totalMessagesInMailbox, int numNewMessages) {
        Memory memory = getMemory(account, folder);
        memory.syncingState = MemorizingState.FINISHED;
        memory.syncingTotalMessagesInMailbox = totalMessagesInMailbox;
        memory.syncingNumNewMessages = numNewMessages;
    }

    public synchronized void synchronizeMailboxFailed(Account account, String folder, String message) {
        Memory memory = getMemory(account, folder);
        memory.syncingState = MemorizingState.FAILED;
        memory.failureMessage = message;
    }

    /* access modifiers changed from: package-private */
    public synchronized void refreshOther(MessagingListener other) {
        if (other != null) {
            Memory syncStarted = null;
            Memory sendStarted = null;
            Memory processingStarted = null;
            for (Memory memory : this.memories.values()) {
                if (memory.syncingState != null) {
                    switch (memory.syncingState) {
                        case STARTED:
                            syncStarted = memory;
                            break;
                        case FINISHED:
                            other.synchronizeMailboxFinished(memory.account, memory.folderName, memory.syncingTotalMessagesInMailbox, memory.syncingNumNewMessages);
                            break;
                        case FAILED:
                            other.synchronizeMailboxFailed(memory.account, memory.folderName, memory.failureMessage);
                            break;
                    }
                }
                if (memory.sendingState != null) {
                    switch (memory.sendingState) {
                        case STARTED:
                            sendStarted = memory;
                            break;
                        case FINISHED:
                            other.sendPendingMessagesCompleted(memory.account);
                            break;
                        case FAILED:
                            other.sendPendingMessagesFailed(memory.account);
                            break;
                    }
                }
                if (memory.pushingState != null) {
                    switch (memory.pushingState) {
                        case STARTED:
                            other.setPushActive(memory.account, memory.folderName, true);
                            break;
                        case FINISHED:
                            other.setPushActive(memory.account, memory.folderName, false);
                            break;
                    }
                }
                if (memory.processingState != null) {
                    switch (memory.processingState) {
                        case STARTED:
                            processingStarted = memory;
                            continue;
                        case FINISHED:
                        case FAILED:
                            other.pendingCommandsFinished(memory.account);
                            continue;
                        default:
                            continue;
                    }
                }
            }
            Memory somethingStarted = null;
            if (syncStarted != null) {
                other.synchronizeMailboxStarted(syncStarted.account, syncStarted.folderName);
                somethingStarted = syncStarted;
            }
            if (sendStarted != null) {
                other.sendPendingMessagesStarted(sendStarted.account);
                somethingStarted = sendStarted;
            }
            if (processingStarted != null) {
                other.pendingCommandsProcessing(processingStarted.account);
                if (processingStarted.processingCommandTitle != null) {
                    other.pendingCommandStarted(processingStarted.account, processingStarted.processingCommandTitle);
                } else {
                    other.pendingCommandCompleted(processingStarted.account, null);
                }
                somethingStarted = processingStarted;
            }
            if (somethingStarted != null && somethingStarted.folderTotal > 0) {
                other.synchronizeMailboxProgress(somethingStarted.account, somethingStarted.folderName, somethingStarted.folderCompleted, somethingStarted.folderTotal);
            }
        }
    }

    public synchronized void setPushActive(Account account, String folderName, boolean active) {
        getMemory(account, folderName).pushingState = active ? MemorizingState.STARTED : MemorizingState.FINISHED;
    }

    public synchronized void sendPendingMessagesStarted(Account account) {
        Memory memory = getMemory(account, null);
        memory.sendingState = MemorizingState.STARTED;
        memory.folderCompleted = 0;
        memory.folderTotal = 0;
    }

    public synchronized void sendPendingMessagesCompleted(Account account) {
        getMemory(account, null).sendingState = MemorizingState.FINISHED;
    }

    public synchronized void sendPendingMessagesFailed(Account account) {
        getMemory(account, null).sendingState = MemorizingState.FAILED;
    }

    public synchronized void synchronizeMailboxProgress(Account account, String folderName, int completed, int total) {
        Memory memory = getMemory(account, folderName);
        memory.folderCompleted = completed;
        memory.folderTotal = total;
    }

    public synchronized void pendingCommandsProcessing(Account account) {
        Memory memory = getMemory(account, null);
        memory.processingState = MemorizingState.STARTED;
        memory.folderCompleted = 0;
        memory.folderTotal = 0;
    }

    public synchronized void pendingCommandsFinished(Account account) {
        getMemory(account, null).processingState = MemorizingState.FINISHED;
    }

    public synchronized void pendingCommandStarted(Account account, String commandTitle) {
        getMemory(account, null).processingCommandTitle = commandTitle;
    }

    public synchronized void pendingCommandCompleted(Account account, String commandTitle) {
        getMemory(account, null).processingCommandTitle = null;
    }

    private static String getMemoryKey(Account taccount, String tfolderName) {
        return taccount.getDescription() + ":" + tfolderName;
    }

    private static class Memory {
        Account account;
        String failureMessage = null;
        int folderCompleted = 0;
        String folderName;
        int folderTotal = 0;
        String processingCommandTitle = null;
        MemorizingState processingState = null;
        MemorizingState pushingState = null;
        MemorizingState sendingState = null;
        int syncingNumNewMessages;
        MemorizingState syncingState = null;
        int syncingTotalMessagesInMailbox;

        Memory(Account nAccount, String nFolderName) {
            this.account = nAccount;
            this.folderName = nFolderName;
        }
    }
}
