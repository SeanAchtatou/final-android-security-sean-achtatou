package com.fsck.k9.controller;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import atomicgonza.App;
import atomicgonza.GmailUtils;
import com.fsck.k9.Account;
import com.fsck.k9.K9;
import com.fsck.k9.mail.Folder;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.PushReceiver;
import com.fsck.k9.mail.power.TracingPowerManager;
import com.fsck.k9.mailstore.LocalFolder;
import com.fsck.k9.service.SleepService;
import java.util.List;
import java.util.concurrent.CountDownLatch;

public class MessagingControllerPushReceiver implements PushReceiver {
    final Account account;
    final Context context;
    final MessagingController controller;

    public MessagingControllerPushReceiver(Context context2, Account nAccount, MessagingController nController) {
        this.account = nAccount;
        this.controller = nController;
        this.context = context2;
    }

    public void messagesFlagsChanged(Folder folder, List<Message> messages) {
        this.controller.messagesArrived(this.account, folder, messages, true);
    }

    public void messagesArrived(Folder folder, List<Message> messages) {
        this.controller.messagesArrived(this.account, folder, messages, false);
    }

    public void messagesRemoved(Folder folder, List<Message> messages) {
        this.controller.messagesArrived(this.account, folder, messages, true);
    }

    public void syncFolder(Folder folder) {
        if (K9.DEBUG) {
            Log.v("k9", "syncFolder(" + folder.getName() + ")");
        }
        final CountDownLatch latch = new CountDownLatch(1);
        this.controller.synchronizeMailbox(this.account, folder.getName(), new MessagingListener() {
            public void synchronizeMailboxFinished(Account account, String folder, int totalMessagesInMailbox, int numNewMessages) {
                latch.countDown();
            }

            public void synchronizeMailboxFailed(Account account, String folder, String message) {
                latch.countDown();
            }
        }, folder);
        if (K9.DEBUG) {
            Log.v("k9", "syncFolder(" + folder.getName() + ") about to await latch release");
        }
        try {
            latch.await();
            if (K9.DEBUG) {
                Log.v("k9", "syncFolder(" + folder.getName() + ") got latch release");
            }
        } catch (Exception e) {
            Log.e("k9", "Interrupted while awaiting latch release", e);
        }
    }

    public void sleep(TracingPowerManager.TracingWakeLock wakeLock, long millis) {
        SleepService.sleep(this.context, millis, wakeLock, 60000);
    }

    public void pushError(String errorMessage, Exception e) {
        String errMess = errorMessage;
        this.controller.notifyUserIfCertificateProblem(this.account, e, true);
        if (errMess == null && e != null) {
            errMess = e.getMessage();
        }
        this.controller.addErrorMessage(this.account, errMess, e);
    }

    public void authenticationFailed(final String mFolderName) {
        if (this.account != null) {
            String mail = this.account.getEmail();
            if (GmailUtils.isGmailAccount(mail)) {
                SharedPreferences pref = App.getPrefToken(this.context);
                long timeExpireToken = App.getTimeExpireTokenGmailAccount(pref, mail);
                if (timeExpireToken > 3600000 && timeExpireToken < System.currentTimeMillis()) {
                    GmailUtils.queryAccessToken(App.getRefreshTokenGmailAccount(pref, mail), this.context, this.account, true, false, new GmailUtils.CallBackAccessToken() {
                        public void sucess() {
                            MessagingControllerPushReceiver.this.controller.addErrorMessage(MessagingControllerPushReceiver.this.account, "Sucess getting token at MessagingControllerPusher.java 106 ", new Exception());
                            MessagingControllerPushReceiver.this.controller.synchronizeMailbox(MessagingControllerPushReceiver.this.account, mFolderName, new MessagingListener() {
                            }, null);
                        }

                        public void error(String msg) {
                            MessagingControllerPushReceiver.this.controller.handleAuthenticationFailure(MessagingControllerPushReceiver.this.account, true);
                            MessagingControllerPushReceiver.this.controller.addErrorMessage(MessagingControllerPushReceiver.this.account, "Error getting token Error: " + msg + " at MessagingControllerPusher.java 119", new Exception());
                        }
                    });
                    return;
                }
            }
        }
        this.controller.handleAuthenticationFailure(this.account, true);
    }

    public String getPushState(String folderName) {
        String str;
        LocalFolder localFolder = null;
        try {
            localFolder = this.account.getLocalStore().getFolder(folderName);
            localFolder.open(0);
            str = localFolder.getPushState();
            if (localFolder != null) {
                localFolder.close();
            }
        } catch (Exception e) {
            Log.e("k9", "Unable to get push state from account " + this.account.getDescription() + ", folder " + folderName, e);
            str = null;
            if (localFolder != null) {
                localFolder.close();
            }
        } catch (Throwable th) {
            if (localFolder != null) {
                localFolder.close();
            }
            throw th;
        }
        return str;
    }

    public void setPushActive(String folderName, boolean enabled) {
        for (MessagingListener l : this.controller.getListeners()) {
            l.setPushActive(this.account, folderName, enabled);
        }
    }

    public Context getContext() {
        return this.context;
    }
}
