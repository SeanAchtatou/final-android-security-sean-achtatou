package com.fsck.k9.controller;

import android.util.Log;
import com.fsck.k9.mail.Message;
import java.util.Comparator;

public class UidComparator implements Comparator<Message> {
    public int compare(Message messageLeft, Message messageRight) {
        Long uidLeft = getUidForMessage(messageLeft);
        Long uidRight = getUidForMessage(messageRight);
        if (uidLeft == null && uidRight == null) {
            return 0;
        }
        if (uidLeft == null) {
            return 1;
        }
        if (uidRight == null) {
            return -1;
        }
        return uidLeft.compareTo(uidRight);
    }

    private Long getUidForMessage(Message message) {
        try {
            return Long.valueOf(Long.parseLong(message.getUid()));
        } catch (NullPointerException | NumberFormatException e) {
            Log.e("AtomicGonza", "okAG Exception uidForMessage " + e.getMessage() + " " + new Exception().getStackTrace()[0].toString());
            return null;
        }
    }
}
