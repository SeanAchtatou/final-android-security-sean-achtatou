package com.fsck.k9.service;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;
import com.fsck.k9.Account;
import com.fsck.k9.K9;
import com.fsck.k9.Preferences;
import com.fsck.k9.preferences.StorageEditor;
import com.fsck.k9.remotecontrol.K9RemoteControl;
import yahoo.mail.app.R;

public class RemoteControlService extends CoreService {
    private static final String PUSH_RESTART_ACTION = "com.fsck.k9.service.RemoteControlService.PUSH_RESTART_ACTION";
    public static final int REMOTE_CONTROL_SERVICE_WAKE_LOCK_TIMEOUT = 20000;
    private static final String RESCHEDULE_ACTION = "com.fsck.k9.service.RemoteControlService.RESCHEDULE_ACTION";
    private static final String SET_ACTION = "com.fsck.k9.service.RemoteControlService.SET_ACTION";

    public static void set(Context context, Intent i, Integer wakeLockId) {
        i.setClass(context, RemoteControlService.class);
        i.setAction(SET_ACTION);
        addWakeLockId(context, i, wakeLockId, true);
        context.startService(i);
    }

    public int startService(final Intent intent, int startId) {
        if (K9.DEBUG) {
            Log.i("k9", "RemoteControlService started with startId = " + startId);
        }
        final Preferences preferences = Preferences.getPreferences(this);
        if (RESCHEDULE_ACTION.equals(intent.getAction())) {
            if (K9.DEBUG) {
                Log.i("k9", "RemoteControlService requesting MailService poll reschedule");
            }
            MailService.actionReschedulePoll(this, null);
        }
        if (PUSH_RESTART_ACTION.equals(intent.getAction())) {
            if (K9.DEBUG) {
                Log.i("k9", "RemoteControlService requesting MailService push restart");
            }
            MailService.actionRestartPushers(this, null);
            return 2;
        } else if (!SET_ACTION.equals(intent.getAction())) {
            return 2;
        } else {
            if (K9.DEBUG) {
                Log.i("k9", "RemoteControlService got request to change settings");
            }
            execute(getApplication(), new Runnable() {
                public void run() {
                    boolean needsReschedule = false;
                    boolean needsPushRestart = false;
                    try {
                        String uuid = intent.getStringExtra(K9RemoteControl.K9_ACCOUNT_UUID);
                        boolean allAccounts = intent.getBooleanExtra(K9RemoteControl.K9_ALL_ACCOUNTS, false);
                        if (K9.DEBUG) {
                            if (allAccounts) {
                                Log.i("k9", "RemoteControlService changing settings for all accounts");
                            } else {
                                Log.i("k9", "RemoteControlService changing settings for account with UUID " + uuid);
                            }
                        }
                        for (Account account : preferences.getAccounts()) {
                            if (allAccounts || account.getUuid().equals(uuid)) {
                                if (K9.DEBUG) {
                                    Log.i("k9", "RemoteControlService changing settings for account " + account.getDescription());
                                }
                                String notificationEnabled = intent.getStringExtra(K9RemoteControl.K9_NOTIFICATION_ENABLED);
                                String ringEnabled = intent.getStringExtra(K9RemoteControl.K9_RING_ENABLED);
                                String vibrateEnabled = intent.getStringExtra(K9RemoteControl.K9_VIBRATE_ENABLED);
                                String pushClasses = intent.getStringExtra(K9RemoteControl.K9_PUSH_CLASSES);
                                String pollClasses = intent.getStringExtra(K9RemoteControl.K9_POLL_CLASSES);
                                String pollFrequency = intent.getStringExtra(K9RemoteControl.K9_POLL_FREQUENCY);
                                if (notificationEnabled != null) {
                                    account.setNotifyNewMail(Boolean.parseBoolean(notificationEnabled));
                                }
                                if (ringEnabled != null) {
                                    account.getNotificationSetting().setRing(Boolean.parseBoolean(ringEnabled));
                                }
                                if (vibrateEnabled != null) {
                                    account.getNotificationSetting().setVibrate(Boolean.parseBoolean(vibrateEnabled));
                                }
                                if (pushClasses != null) {
                                    needsPushRestart |= account.setFolderPushMode(Account.FolderMode.valueOf(pushClasses));
                                }
                                if (pollClasses != null) {
                                    needsReschedule |= account.setFolderSyncMode(Account.FolderMode.valueOf(pollClasses));
                                }
                                if (pollFrequency != null) {
                                    String[] allowedFrequencies = RemoteControlService.this.getResources().getStringArray(R.array.account_settings_check_frequency_values);
                                    int length = allowedFrequencies.length;
                                    for (int i = 0; i < length; i++) {
                                        String allowedFrequency = allowedFrequencies[i];
                                        if (allowedFrequency.equals(pollFrequency)) {
                                            needsReschedule |= account.setAutomaticCheckIntervalMinutes(Integer.valueOf(Integer.parseInt(allowedFrequency)).intValue());
                                        }
                                    }
                                }
                                account.save(Preferences.getPreferences(RemoteControlService.this));
                            }
                        }
                        if (K9.DEBUG) {
                            Log.i("k9", "RemoteControlService changing global settings");
                        }
                        String backgroundOps = intent.getStringExtra(K9RemoteControl.K9_BACKGROUND_OPERATIONS);
                        if (K9RemoteControl.K9_BACKGROUND_OPERATIONS_ALWAYS.equals(backgroundOps) || K9RemoteControl.K9_BACKGROUND_OPERATIONS_NEVER.equals(backgroundOps) || K9RemoteControl.K9_BACKGROUND_OPERATIONS_WHEN_CHECKED_AUTO_SYNC.equals(backgroundOps)) {
                            boolean needsReset = K9.setBackgroundOps(K9.BACKGROUND_OPS.valueOf(backgroundOps));
                            needsPushRestart |= needsReset;
                            needsReschedule |= needsReset;
                        }
                        String theme = intent.getStringExtra(K9RemoteControl.K9_THEME);
                        if (theme != null) {
                            K9.setK9Theme(K9RemoteControl.K9_THEME_DARK.equals(theme) ? K9.Theme.DARK : K9.Theme.LIGHT);
                        }
                        StorageEditor editor = preferences.getStorage().edit();
                        K9.save(editor);
                        editor.commit();
                        if (needsReschedule) {
                            Intent i2 = new Intent(RemoteControlService.this, RemoteControlService.class);
                            i2.setAction(RemoteControlService.RESCHEDULE_ACTION);
                            BootReceiver.scheduleIntent(RemoteControlService.this, System.currentTimeMillis() + 10000, i2);
                        }
                        if (needsPushRestart) {
                            Intent i3 = new Intent(RemoteControlService.this, RemoteControlService.class);
                            i3.setAction(RemoteControlService.PUSH_RESTART_ACTION);
                            BootReceiver.scheduleIntent(RemoteControlService.this, System.currentTimeMillis() + 10000, i3);
                        }
                    } catch (Exception e) {
                        Log.e("k9", "Could not handle K9_SET", e);
                        Toast.makeText(RemoteControlService.this, e.getMessage(), 1).show();
                    }
                }
            }, 20000, Integer.valueOf(startId));
            return 2;
        }
    }
}
