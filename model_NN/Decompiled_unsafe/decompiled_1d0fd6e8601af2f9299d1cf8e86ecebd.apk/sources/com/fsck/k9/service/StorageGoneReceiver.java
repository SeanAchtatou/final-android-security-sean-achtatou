package com.fsck.k9.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import com.fsck.k9.K9;
import com.fsck.k9.mailstore.StorageManager;

public class StorageGoneReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Uri uri = intent.getData();
        if (uri != null && uri.getPath() != null) {
            if (K9.DEBUG) {
                Log.v("k9", "StorageGoneReceiver: " + intent.toString());
            }
            String path = uri.getPath();
            if ("android.intent.action.MEDIA_EJECT".equals(action)) {
                StorageManager.getInstance(context).onBeforeUnmount(path);
            } else if ("android.intent.action.MEDIA_UNMOUNTED".equals(action)) {
                StorageManager.getInstance(context).onAfterUnmount(path);
            }
        }
    }
}
