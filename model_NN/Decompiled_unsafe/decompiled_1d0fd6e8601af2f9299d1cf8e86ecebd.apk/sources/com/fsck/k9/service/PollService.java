package com.fsck.k9.service;

import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import com.fsck.k9.Account;
import com.fsck.k9.K9;
import com.fsck.k9.controller.MessagingController;
import com.fsck.k9.controller.MessagingListener;
import com.fsck.k9.mail.power.TracingPowerManager;
import java.util.HashMap;
import java.util.Map;

public class PollService extends CoreService {
    private static String START_SERVICE = "com.fsck.k9.service.PollService.startService";
    private static String STOP_SERVICE = "com.fsck.k9.service.PollService.stopService";
    private Listener mListener = new Listener();

    public static void startService(Context context) {
        Intent i = new Intent();
        i.setClass(context, PollService.class);
        i.setAction(START_SERVICE);
        addWakeLock(context, i);
        context.startService(i);
    }

    public static void stopService(Context context) {
        Intent i = new Intent();
        i.setClass(context, PollService.class);
        i.setAction(STOP_SERVICE);
        addWakeLock(context, i);
        context.startService(i);
    }

    public void onCreate() {
        super.onCreate();
        setAutoShutdown(false);
    }

    public int startService(Intent intent, int startId) {
        if (START_SERVICE.equals(intent.getAction())) {
            if (K9.DEBUG) {
                Log.i("k9", "PollService started with startId = " + startId);
            }
            MessagingController controller = MessagingController.getInstance(getApplication());
            Listener listener = (Listener) controller.getCheckMailListener();
            if (listener == null) {
                if (K9.DEBUG) {
                    Log.i("k9", "***** PollService *****: starting new check");
                }
                this.mListener.setStartId(startId);
                this.mListener.wakeLockAcquire();
                controller.setCheckMailListener(this.mListener);
                controller.checkMail(this, null, false, false, this.mListener);
                return 2;
            }
            if (K9.DEBUG) {
                Log.i("k9", "***** PollService *****: renewing WakeLock");
            }
            listener.setStartId(startId);
            listener.wakeLockAcquire();
            return 2;
        } else if (!STOP_SERVICE.equals(intent.getAction())) {
            return 2;
        } else {
            if (K9.DEBUG) {
                Log.i("k9", "PollService stopping");
            }
            stopSelf();
            return 2;
        }
    }

    public IBinder onBind(Intent arg0) {
        return null;
    }

    class Listener extends MessagingListener {
        Map<String, Integer> accountsChecked = new HashMap();
        private int startId = -1;
        private TracingPowerManager.TracingWakeLock wakeLock = null;

        Listener() {
        }

        public synchronized void wakeLockAcquire() {
            TracingPowerManager.TracingWakeLock oldWakeLock = this.wakeLock;
            this.wakeLock = TracingPowerManager.getPowerManager(PollService.this).newWakeLock(1, "PollService wakeLockAcquire");
            this.wakeLock.setReferenceCounted(false);
            this.wakeLock.acquire(600000);
            if (oldWakeLock != null) {
                oldWakeLock.release();
            }
        }

        public synchronized void wakeLockRelease() {
            if (this.wakeLock != null) {
                this.wakeLock.release();
                this.wakeLock = null;
            }
        }

        public void checkMailStarted(Context context, Account account) {
            this.accountsChecked.clear();
        }

        public void checkMailFailed(Context context, Account account, String reason) {
            release();
        }

        public void synchronizeMailboxFinished(Account account, String folder, int totalMessagesInMailbox, int numNewMessages) {
            if (account.isNotifyNewMail()) {
                Integer existingNewMessages = this.accountsChecked.get(account.getUuid());
                if (existingNewMessages == null) {
                    existingNewMessages = 0;
                }
                this.accountsChecked.put(account.getUuid(), Integer.valueOf(existingNewMessages.intValue() + numNewMessages));
            }
        }

        private void release() {
            MessagingController.getInstance(PollService.this.getApplication()).setCheckMailListener(null);
            MailService.saveLastCheckEnd(PollService.this.getApplication());
            MailService.actionReschedulePoll(PollService.this, null);
            wakeLockRelease();
            if (K9.DEBUG) {
                Log.i("k9", "PollService stopping with startId = " + this.startId);
            }
            PollService.this.stopSelf(this.startId);
        }

        public void checkMailFinished(Context context, Account account) {
            if (K9.DEBUG) {
                Log.v("k9", "***** PollService *****: checkMailFinished");
            }
            release();
        }

        public int getStartId() {
            return this.startId;
        }

        public void setStartId(int startId2) {
            this.startId = startId2;
        }
    }
}
