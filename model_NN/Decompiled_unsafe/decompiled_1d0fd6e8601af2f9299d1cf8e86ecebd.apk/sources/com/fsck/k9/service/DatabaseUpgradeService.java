package com.fsck.k9.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import com.fsck.k9.Account;
import com.fsck.k9.K9;
import com.fsck.k9.Preferences;
import com.fsck.k9.mail.power.TracingPowerManager;
import com.fsck.k9.mailstore.UnavailableStorageException;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class DatabaseUpgradeService extends Service {
    private static final String ACTION_START_SERVICE = "com.fsck.k9.service.DatabaseUpgradeService.startService";
    public static final String ACTION_UPGRADE_COMPLETE = "DatabaseUpgradeService.upgradeComplete";
    public static final String ACTION_UPGRADE_PROGRESS = "DatabaseUpgradeService.upgradeProgress";
    public static final String EXTRA_ACCOUNT_UUID = "account_uuid";
    public static final String EXTRA_PROGRESS = "progress";
    public static final String EXTRA_PROGRESS_END = "progress_end";
    private static final String WAKELOCK_TAG = "DatabaseUpgradeService";
    private static final long WAKELOCK_TIMEOUT = 600000;
    private String mAccountUuid;
    private LocalBroadcastManager mLocalBroadcastManager;
    private int mProgress;
    private int mProgressEnd;
    private AtomicBoolean mRunning = new AtomicBoolean(false);
    private TracingPowerManager.TracingWakeLock mWakeLock;

    public static void startService(Context context) {
        Intent i = new Intent();
        i.setClass(context, DatabaseUpgradeService.class);
        i.setAction(ACTION_START_SERVICE);
        context.startService(i);
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        this.mLocalBroadcastManager = LocalBroadcastManager.getInstance(this);
    }

    public final int onStartCommand(Intent intent, int flags, int startId) {
        if (this.mRunning.compareAndSet(false, true)) {
            if (K9.DEBUG) {
                Log.i("k9", "DatabaseUpgradeService started");
            }
            acquireWakelock();
            startUpgradeInBackground();
        } else {
            sendProgressBroadcast(this.mAccountUuid, this.mProgress, this.mProgressEnd);
        }
        return 1;
    }

    private void acquireWakelock() {
        this.mWakeLock = TracingPowerManager.getPowerManager(this).newWakeLock(1, WAKELOCK_TAG);
        this.mWakeLock.setReferenceCounted(false);
        this.mWakeLock.acquire(WAKELOCK_TIMEOUT);
    }

    private void releaseWakelock() {
        this.mWakeLock.release();
    }

    /* access modifiers changed from: private */
    public void stopService() {
        stopSelf();
        if (K9.DEBUG) {
            Log.i("k9", "DatabaseUpgradeService stopped");
        }
        releaseWakelock();
        this.mRunning.set(false);
    }

    private void startUpgradeInBackground() {
        new Thread(WAKELOCK_TAG) {
            public void run() {
                DatabaseUpgradeService.this.upgradeDatabases();
                DatabaseUpgradeService.this.stopService();
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public void upgradeDatabases() {
        List<Account> accounts = Preferences.getPreferences(this).getAccounts();
        this.mProgressEnd = accounts.size();
        this.mProgress = 0;
        for (Account account : accounts) {
            this.mAccountUuid = account.getUuid();
            sendProgressBroadcast(this.mAccountUuid, this.mProgress, this.mProgressEnd);
            try {
                account.getLocalStore();
            } catch (UnavailableStorageException e) {
                Log.e("k9", "Database unavailable");
            } catch (Exception e2) {
                Log.e("k9", "Error while upgrading database", e2);
            }
            this.mProgress++;
        }
        K9.setDatabasesUpToDate(true);
        sendUpgradeCompleteBroadcast();
    }

    private void sendProgressBroadcast(String accountUuid, int progress, int progressEnd) {
        Intent intent = new Intent();
        intent.setAction(ACTION_UPGRADE_PROGRESS);
        intent.putExtra("account_uuid", accountUuid);
        intent.putExtra("progress", progress);
        intent.putExtra(EXTRA_PROGRESS_END, progressEnd);
        this.mLocalBroadcastManager.sendBroadcast(intent);
    }

    private void sendUpgradeCompleteBroadcast() {
        Intent intent = new Intent();
        intent.setAction(ACTION_UPGRADE_COMPLETE);
        this.mLocalBroadcastManager.sendBroadcast(intent);
    }
}
