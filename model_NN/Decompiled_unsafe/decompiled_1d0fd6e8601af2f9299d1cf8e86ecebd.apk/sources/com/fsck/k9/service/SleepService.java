package com.fsck.k9.service;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.fsck.k9.K9;
import com.fsck.k9.mail.power.TracingPowerManager;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class SleepService extends CoreService {
    private static String ALARM_FIRED = "com.fsck.k9.service.SleepService.ALARM_FIRED";
    private static String LATCH_ID = "com.fsck.k9.service.SleepService.LATCH_ID_EXTRA";
    private static AtomicInteger latchId = new AtomicInteger();
    private static ConcurrentHashMap<Integer, SleepDatum> sleepData = new ConcurrentHashMap<>();

    public static void sleep(Context context, long sleepTime, TracingPowerManager.TracingWakeLock wakeLock, long wakeLockTimeout) {
        Integer id = Integer.valueOf(latchId.getAndIncrement());
        if (K9.DEBUG) {
            Log.d("k9", "SleepService Preparing CountDownLatch with id = " + id + ", thread " + Thread.currentThread().getName());
        }
        SleepDatum sleepDatum = new SleepDatum();
        CountDownLatch latch = new CountDownLatch(1);
        sleepDatum.latch = latch;
        sleepDatum.reacquireLatch = new CountDownLatch(1);
        sleepData.put(id, sleepDatum);
        Intent i = new Intent(context, SleepService.class);
        i.putExtra(LATCH_ID, id);
        i.setAction(ALARM_FIRED + "." + id);
        long startTime = System.currentTimeMillis();
        BootReceiver.scheduleIntent(context, startTime + sleepTime, i);
        if (wakeLock != null) {
            sleepDatum.wakeLock = wakeLock;
            sleepDatum.timeout = wakeLockTimeout;
            wakeLock.release();
        }
        try {
            if (!latch.await(sleepTime, TimeUnit.MILLISECONDS) && K9.DEBUG) {
                Log.d("k9", "SleepService latch timed out for id = " + id + ", thread " + Thread.currentThread().getName());
            }
        } catch (InterruptedException ie) {
            Log.e("k9", "SleepService Interrupted while awaiting latch", ie);
        }
        SleepDatum releaseDatum = sleepData.remove(id);
        if (releaseDatum == null) {
            try {
                if (K9.DEBUG) {
                    Log.d("k9", "SleepService waiting for reacquireLatch for id = " + id + ", thread " + Thread.currentThread().getName());
                }
                if (!sleepDatum.reacquireLatch.await(5000, TimeUnit.MILLISECONDS)) {
                    Log.w("k9", "SleepService reacquireLatch timed out for id = " + id + ", thread " + Thread.currentThread().getName());
                } else if (K9.DEBUG) {
                    Log.d("k9", "SleepService reacquireLatch finished for id = " + id + ", thread " + Thread.currentThread().getName());
                }
            } catch (InterruptedException ie2) {
                Log.e("k9", "SleepService Interrupted while awaiting reacquireLatch", ie2);
            }
        } else {
            reacquireWakeLock(releaseDatum);
        }
        long actualSleep = System.currentTimeMillis() - startTime;
        if (actualSleep < sleepTime) {
            Log.w("k9", "SleepService sleep time too short: requested was " + sleepTime + ", actual was " + actualSleep);
        } else if (K9.DEBUG) {
            Log.d("k9", "SleepService requested sleep time was " + sleepTime + ", actual was " + actualSleep);
        }
    }

    private static void endSleep(Integer id) {
        if (id.intValue() != -1) {
            SleepDatum sleepDatum = sleepData.remove(id);
            if (sleepDatum != null) {
                CountDownLatch latch = sleepDatum.latch;
                if (latch == null) {
                    Log.e("k9", "SleepService No CountDownLatch available with id = " + id);
                } else {
                    if (K9.DEBUG) {
                        Log.d("k9", "SleepService Counting down CountDownLatch with id = " + id);
                    }
                    latch.countDown();
                }
                reacquireWakeLock(sleepDatum);
                sleepDatum.reacquireLatch.countDown();
            } else if (K9.DEBUG) {
                Log.d("k9", "SleepService Sleep for id " + id + " already finished");
            }
        }
    }

    private static void reacquireWakeLock(SleepDatum sleepDatum) {
        TracingPowerManager.TracingWakeLock wakeLock = sleepDatum.wakeLock;
        if (wakeLock != null) {
            synchronized (wakeLock) {
                long timeout = sleepDatum.timeout;
                if (K9.DEBUG) {
                    Log.d("k9", "SleepService Acquiring wakeLock for " + timeout + "ms");
                }
                wakeLock.acquire(timeout);
            }
        }
    }

    public int startService(Intent intent, int startId) {
        try {
            if (intent.getAction().startsWith(ALARM_FIRED)) {
                endSleep(Integer.valueOf(intent.getIntExtra(LATCH_ID, -1)));
            }
            return 2;
        } finally {
            stopSelf(startId);
        }
    }

    private static class SleepDatum {
        CountDownLatch latch;
        CountDownLatch reacquireLatch;
        long timeout;
        TracingPowerManager.TracingWakeLock wakeLock;

        private SleepDatum() {
        }
    }
}
