package com.fsck.k9.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.fsck.k9.K9;
import com.fsck.k9.mail.power.TracingPowerManager;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class CoreReceiver extends BroadcastReceiver {
    public static final String WAKE_LOCK_ID = "com.fsck.k9.service.CoreReceiver.wakeLockId";
    public static final String WAKE_LOCK_RELEASE = "com.fsck.k9.service.CoreReceiver.wakeLockRelease";
    private static AtomicInteger wakeLockSeq = new AtomicInteger(0);
    private static ConcurrentHashMap<Integer, TracingPowerManager.TracingWakeLock> wakeLocks = new ConcurrentHashMap<>();

    private static Integer getWakeLock(Context context) {
        TracingPowerManager.TracingWakeLock wakeLock = TracingPowerManager.getPowerManager(context).newWakeLock(1, "CoreReceiver getWakeLock");
        wakeLock.setReferenceCounted(false);
        wakeLock.acquire(60000);
        Integer tmpWakeLockId = Integer.valueOf(wakeLockSeq.getAndIncrement());
        wakeLocks.put(tmpWakeLockId, wakeLock);
        if (K9.DEBUG) {
            Log.v("k9", "CoreReceiver Created wakeLock " + tmpWakeLockId);
        }
        return tmpWakeLockId;
    }

    private static void releaseWakeLock(Integer wakeLockId) {
        if (wakeLockId != null) {
            TracingPowerManager.TracingWakeLock wl = wakeLocks.remove(wakeLockId);
            if (wl != null) {
                if (K9.DEBUG) {
                    Log.v("k9", "CoreReceiver Releasing wakeLock " + wakeLockId);
                }
                wl.release();
                return;
            }
            Log.w("k9", "BootReceiver WakeLock " + wakeLockId + " doesn't exist");
        }
    }

    public void onReceive(Context context, Intent intent) {
        Integer wakeLockId = getWakeLock(context);
        try {
            if (K9.DEBUG) {
                Log.i("k9", "CoreReceiver.onReceive" + intent);
            }
            if (WAKE_LOCK_RELEASE.equals(intent.getAction())) {
                wakeLockId = Integer.valueOf(intent.getIntExtra(WAKE_LOCK_ID, -1));
                if (wakeLockId.intValue() != -1) {
                    if (K9.DEBUG) {
                        Log.v("k9", "CoreReceiver Release wakeLock " + wakeLockId);
                    }
                }
            } else {
                wakeLockId = receive(context, intent, wakeLockId);
            }
            releaseWakeLock(wakeLockId);
        } finally {
            releaseWakeLock(wakeLockId);
        }
    }

    public Integer receive(Context context, Intent intent, Integer wakeLockId) {
        return wakeLockId;
    }

    public static void releaseWakeLock(Context context, int wakeLockId) {
        if (K9.DEBUG) {
            Log.v("k9", "CoreReceiver Got request to release wakeLock " + wakeLockId);
        }
        Intent i = new Intent();
        i.setClass(context, CoreReceiver.class);
        i.setAction(WAKE_LOCK_RELEASE);
        i.putExtra(WAKE_LOCK_ID, wakeLockId);
        context.sendBroadcast(i);
    }
}
