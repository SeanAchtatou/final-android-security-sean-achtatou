package com.fsck.k9;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.util.Log;
import com.fsck.k9.activity.setup.AccountSetupCheckSettings;
import com.fsck.k9.helper.Utility;
import com.fsck.k9.mail.Address;
import com.fsck.k9.mail.Folder;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.NetworkType;
import com.fsck.k9.mail.Store;
import com.fsck.k9.mail.filter.Base64;
import com.fsck.k9.mail.ssl.LocalKeyStore;
import com.fsck.k9.mail.store.RemoteStore;
import com.fsck.k9.mail.store.StoreConfig;
import com.fsck.k9.mailstore.LocalStore;
import com.fsck.k9.mailstore.StorageManager;
import com.fsck.k9.preferences.Storage;
import com.fsck.k9.preferences.StorageEditor;
import com.fsck.k9.provider.EmailProvider;
import com.fsck.k9.search.ConditionsTreeNode;
import com.fsck.k9.search.LocalSearch;
import com.fsck.k9.search.SearchSpecification;
import com.fsck.k9.search.SqlQueryBuilder;
import com.fsck.k9.view.ColorChip;
import com.larswerkman.colorpicker.ColorPicker;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import yahoo.mail.app.R;

public class Account implements BaseAccount, StoreConfig {
    public static final String ACCOUNT_DESCRIPTION_KEY = "description";
    public static final MessageFormat DEFAULT_MESSAGE_FORMAT = MessageFormat.HTML;
    public static final boolean DEFAULT_MESSAGE_FORMAT_AUTO = false;
    public static final boolean DEFAULT_MESSAGE_READ_RECEIPT = false;
    public static final boolean DEFAULT_QUOTED_TEXT_SHOWN = true;
    public static final String DEFAULT_QUOTE_PREFIX = ">";
    public static final QuoteStyle DEFAULT_QUOTE_STYLE = QuoteStyle.PREFIX;
    public static final int DEFAULT_REMOTE_SEARCH_NUM_RESULTS = 25;
    public static final boolean DEFAULT_REPLY_AFTER_QUOTE = false;
    public static final boolean DEFAULT_SORT_ASCENDING = false;
    public static final SortType DEFAULT_SORT_TYPE = SortType.SORT_DATE;
    public static final boolean DEFAULT_STRIP_SIGNATURE = true;
    public static final String IDENTITY_DESCRIPTION_KEY = "description";
    public static final String IDENTITY_EMAIL_KEY = "email";
    public static final String IDENTITY_NAME_KEY = "name";
    public static final String INBOX = "INBOX";
    public static final long NO_OPENPGP_KEY = 0;
    public static final String NO_OPENPGP_PROVIDER = "";
    public static final String OUTBOX = "K9MAIL_INTERNAL_OUTBOX";
    public static final Integer[] PREDEFINED_COLORS = {Integer.valueOf(Color.parseColor("#0099CC")), Integer.valueOf(Color.parseColor("#669900")), Integer.valueOf(Color.parseColor("#FF8800")), Integer.valueOf(Color.parseColor("#CC0000")), Integer.valueOf(Color.parseColor("#9933CC"))};
    public static final String STORE_URI_KEY = "storeUri";
    public static final String TRANSPORT_URI_KEY = "transportUri";
    private final Map<NetworkType, Boolean> compressionMap;
    private boolean goToUnreadMessageSearch;
    private List<Identity> identities;
    private String lastSelectedFolderName;
    private int mAccountNumber;
    private boolean mAllowRemoteSearch;
    private String mAlwaysBcc;
    private boolean mAlwaysShowCcBcc;
    private String mArchiveFolderName;
    private String mAutoExpandFolderName;
    private int mAutomaticCheckIntervalMinutes;
    private int mChipColor;
    private String mCryptoApp;
    private long mCryptoKey;
    private boolean mDefaultQuotedTextShown;
    private DeletePolicy mDeletePolicy;
    private String mDescription;
    private int mDisplayCount;
    private String mDraftsFolderName;
    private boolean mEnabled;
    private Expunge mExpungePolicy;
    private ColorChip mFlaggedReadColorChip;
    private ColorChip mFlaggedUnreadColorChip;
    private FolderMode mFolderDisplayMode;
    private FolderMode mFolderNotifyNewMailMode;
    private FolderMode mFolderPushMode;
    private FolderMode mFolderSyncMode;
    private FolderMode mFolderTargetMode;
    private int mIdleRefreshMinutes;
    private String mInboxFolderName;
    private boolean mIsSignatureBeforeQuotedText;
    private long mLastAutomaticCheckTime;
    private long mLatestOldMessageSeenTime;
    private String mLocalStorageProviderId;
    private boolean mMarkMessageAsReadOnView;
    private int mMaxPushFolders;
    private MessageFormat mMessageFormat;
    private boolean mMessageFormatAuto;
    private boolean mMessageReadReceipt;
    private NotificationSetting mNotificationSetting;
    private boolean mNotifyContactsMailOnly;
    private boolean mNotifyNewMail;
    private boolean mNotifySelfNewMail;
    private boolean mNotifySync;
    private boolean mPushPollOnConnect;
    private String mQuotePrefix;
    private QuoteStyle mQuoteStyle;
    private ColorChip mReadColorChip;
    private boolean mRemoteSearchFullText;
    private int mRemoteSearchNumResults;
    private boolean mReplyAfterQuote;
    private boolean mRingNotified;
    private String mSentFolderName;
    private ShowPictures mShowPictures;
    private Map<SortType, Boolean> mSortAscending;
    private SortType mSortType;
    private String mSpamFolderName;
    private String mStoreUri;
    private boolean mStripSignature;
    private boolean mSyncRemoteDeletions;
    private String mTransportUri;
    private String mTrashFolderName;
    private ColorChip mUnreadColorChip;
    private final String mUuid;
    private int maximumAutoDownloadMessageSize;
    private int maximumPolledMessageAge;
    private Searchable searchableFolders;
    private boolean subscribedFoldersOnly;

    public enum Expunge {
        EXPUNGE_IMMEDIATELY,
        EXPUNGE_MANUALLY,
        EXPUNGE_ON_POLL
    }

    public enum FolderMode {
        NONE,
        ALL,
        FIRST_CLASS,
        FIRST_AND_SECOND_CLASS,
        NOT_SECOND_CLASS
    }

    public enum MessageFormat {
        TEXT,
        HTML,
        AUTO
    }

    public enum QuoteStyle {
        PREFIX,
        HEADER
    }

    public enum Searchable {
        ALL,
        DISPLAYABLE,
        NONE
    }

    public enum ShowPictures {
        NEVER,
        ALWAYS,
        ONLY_FROM_CONTACTS
    }

    public enum DeletePolicy {
        NEVER(0),
        SEVEN_DAYS(1),
        ON_DELETE(2),
        MARK_AS_READ(3);
        
        public final int setting;

        private DeletePolicy(int setting2) {
            this.setting = setting2;
        }

        public String preferenceString() {
            return Integer.toString(this.setting);
        }

        public static DeletePolicy fromInt(int initialSetting) {
            for (DeletePolicy policy : values()) {
                if (policy.setting == initialSetting) {
                    return policy;
                }
            }
            throw new IllegalArgumentException("DeletePolicy " + initialSetting + " unknown");
        }
    }

    public enum SortType {
        SORT_DATE(R.string.sort_earliest_first, R.string.sort_latest_first, false),
        SORT_ARRIVAL(R.string.sort_earliest_first, R.string.sort_latest_first, false),
        SORT_SUBJECT(R.string.sort_subject_alpha, R.string.sort_subject_re_alpha, true),
        SORT_SENDER(R.string.sort_sender_alpha, R.string.sort_sender_re_alpha, true),
        SORT_UNREAD(R.string.sort_unread_first, R.string.sort_unread_last, false),
        SORT_FLAGGED(R.string.sort_flagged_first, R.string.sort_flagged_last, true),
        SORT_ATTACHMENT(R.string.sort_attach_first, R.string.sort_unattached_first, true);
        
        private int ascendingToast;
        private boolean defaultAscending;
        private int descendingToast;

        private SortType(int ascending, int descending, boolean ndefaultAscending) {
            this.ascendingToast = ascending;
            this.descendingToast = descending;
            this.defaultAscending = ndefaultAscending;
        }

        public int getToast(boolean ascending) {
            return ascending ? this.ascendingToast : this.descendingToast;
        }

        public boolean isDefaultAscending() {
            return this.defaultAscending;
        }
    }

    protected Account(Context context) {
        this.mDeletePolicy = DeletePolicy.NEVER;
        this.mSortAscending = new HashMap();
        this.mExpungePolicy = Expunge.EXPUNGE_IMMEDIATELY;
        this.compressionMap = new ConcurrentHashMap();
        this.lastSelectedFolderName = null;
        this.mNotificationSetting = new NotificationSetting();
        this.mUuid = UUID.randomUUID().toString();
        this.mLocalStorageProviderId = StorageManager.getInstance(context).getDefaultProviderId();
        this.mAutomaticCheckIntervalMinutes = 120;
        this.mIdleRefreshMinutes = 24;
        this.mPushPollOnConnect = true;
        this.mDisplayCount = 25;
        this.mAccountNumber = -1;
        this.mNotifyNewMail = true;
        this.mFolderNotifyNewMailMode = FolderMode.ALL;
        this.mNotifySync = true;
        this.mNotifySelfNewMail = true;
        this.mNotifyContactsMailOnly = false;
        this.mFolderDisplayMode = FolderMode.NOT_SECOND_CLASS;
        this.mFolderSyncMode = FolderMode.FIRST_CLASS;
        this.mFolderPushMode = FolderMode.FIRST_CLASS;
        this.mFolderTargetMode = FolderMode.NOT_SECOND_CLASS;
        this.mSortType = DEFAULT_SORT_TYPE;
        this.mSortAscending.put(DEFAULT_SORT_TYPE, false);
        this.mShowPictures = ShowPictures.ALWAYS;
        this.mIsSignatureBeforeQuotedText = false;
        this.mExpungePolicy = Expunge.EXPUNGE_IMMEDIATELY;
        this.mAutoExpandFolderName = INBOX;
        this.mInboxFolderName = INBOX;
        this.mMaxPushFolders = 10;
        this.mChipColor = pickColor(context);
        this.goToUnreadMessageSearch = false;
        this.subscribedFoldersOnly = false;
        this.maximumPolledMessageAge = -1;
        this.maximumAutoDownloadMessageSize = 32768;
        this.mMessageFormat = DEFAULT_MESSAGE_FORMAT;
        this.mMessageFormatAuto = false;
        this.mMessageReadReceipt = false;
        this.mQuoteStyle = DEFAULT_QUOTE_STYLE;
        this.mQuotePrefix = DEFAULT_QUOTE_PREFIX;
        this.mDefaultQuotedTextShown = true;
        this.mReplyAfterQuote = false;
        this.mStripSignature = true;
        this.mSyncRemoteDeletions = true;
        this.mCryptoApp = "";
        this.mCryptoKey = 0;
        this.mAllowRemoteSearch = true;
        this.mRemoteSearchFullText = true;
        this.mRemoteSearchNumResults = 25;
        this.mEnabled = true;
        this.mMarkMessageAsReadOnView = true;
        this.mAlwaysShowCcBcc = false;
        this.searchableFolders = Searchable.ALL;
        this.identities = new ArrayList();
        Identity identity = new Identity();
        identity.setSignatureUse(true);
        identity.setSignature(context.getString(R.string.default_signature2));
        identity.setDescription(context.getString(R.string.default_identity_description));
        this.identities.add(identity);
        this.mNotificationSetting = new NotificationSetting();
        this.mNotificationSetting.setVibrate(true);
        this.mNotificationSetting.setVibratePattern(5);
        this.mNotificationSetting.setVibrateTimes(1);
        this.mNotificationSetting.setRing(true);
        this.mNotificationSetting.setLed(true);
        this.mNotificationSetting.setRingtone("content://settings/system/notification_sound");
        this.mNotificationSetting.setLedColor(this.mChipColor);
        cacheChips();
    }

    private int pickColor(Context context) {
        List<Account> accounts = Preferences.getPreferences(context).getAccounts();
        List<Integer> availableColors = new ArrayList<>(PREDEFINED_COLORS.length);
        Collections.addAll(availableColors, PREDEFINED_COLORS);
        for (Account account : accounts) {
            Integer color = Integer.valueOf(account.getChipColor());
            if (availableColors.contains(color)) {
                availableColors.remove(color);
                if (availableColors.isEmpty()) {
                    break;
                }
            }
        }
        return availableColors.isEmpty() ? ColorPicker.getRandomColor() : ((Integer) availableColors.get(0)).intValue();
    }

    protected Account(Preferences preferences, String uuid) {
        this.mDeletePolicy = DeletePolicy.NEVER;
        this.mSortAscending = new HashMap();
        this.mExpungePolicy = Expunge.EXPUNGE_IMMEDIATELY;
        this.compressionMap = new ConcurrentHashMap();
        this.lastSelectedFolderName = null;
        this.mNotificationSetting = new NotificationSetting();
        this.mUuid = uuid;
        loadAccount(preferences);
    }

    private synchronized void loadAccount(Preferences preferences) {
        Storage storage = preferences.getStorage();
        this.mStoreUri = Base64.decode(storage.getString(this.mUuid + ".storeUri", null));
        this.mLocalStorageProviderId = storage.getString(this.mUuid + ".localStorageProvider", StorageManager.getInstance(K9.app).getDefaultProviderId());
        this.mTransportUri = Base64.decode(storage.getString(this.mUuid + ".transportUri", null));
        this.mDescription = storage.getString(this.mUuid + ".description", null);
        this.mAlwaysBcc = storage.getString(this.mUuid + ".alwaysBcc", this.mAlwaysBcc);
        this.mAutomaticCheckIntervalMinutes = storage.getInt(this.mUuid + ".automaticCheckIntervalMinutes", 120);
        this.mIdleRefreshMinutes = storage.getInt(this.mUuid + ".idleRefreshMinutes", 24);
        this.mPushPollOnConnect = storage.getBoolean(this.mUuid + ".pushPollOnConnect", true);
        this.mDisplayCount = storage.getInt(this.mUuid + ".displayCount", 25);
        if (this.mDisplayCount < 0) {
            this.mDisplayCount = 25;
        }
        this.mLastAutomaticCheckTime = storage.getLong(this.mUuid + ".lastAutomaticCheckTime", 0);
        this.mLatestOldMessageSeenTime = storage.getLong(this.mUuid + ".latestOldMessageSeenTime", 0);
        this.mNotifyNewMail = storage.getBoolean(this.mUuid + ".notifyNewMail", false);
        this.mFolderNotifyNewMailMode = (FolderMode) Preferences.getEnumStringPref(storage, this.mUuid + ".folderNotifyNewMailMode", FolderMode.ALL);
        this.mNotifySelfNewMail = storage.getBoolean(this.mUuid + ".notifySelfNewMail", true);
        this.mNotifyContactsMailOnly = storage.getBoolean(this.mUuid + ".notifyContactsMailOnly", false);
        this.mNotifySync = storage.getBoolean(this.mUuid + ".notifyMailCheck", false);
        this.mDeletePolicy = DeletePolicy.fromInt(storage.getInt(this.mUuid + ".deletePolicy", DeletePolicy.NEVER.setting));
        this.mInboxFolderName = storage.getString(this.mUuid + ".inboxFolderName", INBOX);
        this.mDraftsFolderName = storage.getString(this.mUuid + ".draftsFolderName", "Drafts");
        this.mSentFolderName = storage.getString(this.mUuid + ".sentFolderName", "Sent");
        this.mTrashFolderName = storage.getString(this.mUuid + ".trashFolderName", "Trash");
        this.mArchiveFolderName = storage.getString(this.mUuid + ".archiveFolderName", "Archive");
        this.mSpamFolderName = storage.getString(this.mUuid + ".spamFolderName", "Spam");
        this.mExpungePolicy = (Expunge) Preferences.getEnumStringPref(storage, this.mUuid + ".expungePolicy", Expunge.EXPUNGE_IMMEDIATELY);
        this.mSyncRemoteDeletions = storage.getBoolean(this.mUuid + ".syncRemoteDeletions", true);
        this.mMaxPushFolders = storage.getInt(this.mUuid + ".maxPushFolders", 10);
        this.goToUnreadMessageSearch = storage.getBoolean(this.mUuid + ".goToUnreadMessageSearch", false);
        this.subscribedFoldersOnly = storage.getBoolean(this.mUuid + ".subscribedFoldersOnly", false);
        this.maximumPolledMessageAge = storage.getInt(this.mUuid + ".maximumPolledMessageAge", -1);
        this.maximumAutoDownloadMessageSize = storage.getInt(this.mUuid + ".maximumAutoDownloadMessageSize", 32768);
        this.mMessageFormat = (MessageFormat) Preferences.getEnumStringPref(storage, this.mUuid + ".messageFormat", DEFAULT_MESSAGE_FORMAT);
        this.mMessageFormatAuto = storage.getBoolean(this.mUuid + ".messageFormatAuto", false);
        if (this.mMessageFormatAuto && this.mMessageFormat == MessageFormat.TEXT) {
            this.mMessageFormat = MessageFormat.AUTO;
        }
        this.mMessageReadReceipt = storage.getBoolean(this.mUuid + ".messageReadReceipt", false);
        this.mQuoteStyle = (QuoteStyle) Preferences.getEnumStringPref(storage, this.mUuid + ".quoteStyle", DEFAULT_QUOTE_STYLE);
        this.mQuotePrefix = storage.getString(this.mUuid + ".quotePrefix", DEFAULT_QUOTE_PREFIX);
        this.mDefaultQuotedTextShown = storage.getBoolean(this.mUuid + ".defaultQuotedTextShown", true);
        this.mReplyAfterQuote = storage.getBoolean(this.mUuid + ".replyAfterQuote", false);
        this.mStripSignature = storage.getBoolean(this.mUuid + ".stripSignature", true);
        for (NetworkType type : NetworkType.values()) {
            this.compressionMap.put(type, Boolean.valueOf(storage.getBoolean(this.mUuid + ".useCompression." + type, true)));
        }
        this.mAutoExpandFolderName = storage.getString(this.mUuid + ".autoExpandFolderName", INBOX);
        this.mAccountNumber = storage.getInt(this.mUuid + ".accountNumber", 0);
        this.mChipColor = storage.getInt(this.mUuid + ".chipColor", ColorPicker.getRandomColor());
        this.mSortType = (SortType) Preferences.getEnumStringPref(storage, this.mUuid + ".sortTypeEnum", SortType.SORT_DATE);
        this.mSortAscending.put(this.mSortType, Boolean.valueOf(storage.getBoolean(this.mUuid + ".sortAscending", false)));
        this.mShowPictures = (ShowPictures) Preferences.getEnumStringPref(storage, this.mUuid + ".showPicturesEnum", ShowPictures.ALWAYS);
        this.mNotificationSetting.setVibrate(storage.getBoolean(this.mUuid + ".vibrate", false));
        this.mNotificationSetting.setVibratePattern(storage.getInt(this.mUuid + ".vibratePattern", 0));
        this.mNotificationSetting.setVibrateTimes(storage.getInt(this.mUuid + ".vibrateTimes", 5));
        this.mNotificationSetting.setRing(storage.getBoolean(this.mUuid + ".ring", true));
        this.mNotificationSetting.setRingtone(storage.getString(this.mUuid + ".ringtone", "content://settings/system/notification_sound"));
        this.mNotificationSetting.setLed(storage.getBoolean(this.mUuid + ".led", true));
        this.mNotificationSetting.setLedColor(storage.getInt(this.mUuid + ".ledColor", this.mChipColor));
        this.mFolderDisplayMode = (FolderMode) Preferences.getEnumStringPref(storage, this.mUuid + ".folderDisplayMode", FolderMode.NOT_SECOND_CLASS);
        this.mFolderSyncMode = (FolderMode) Preferences.getEnumStringPref(storage, this.mUuid + ".folderSyncMode", FolderMode.FIRST_CLASS);
        this.mFolderPushMode = (FolderMode) Preferences.getEnumStringPref(storage, this.mUuid + ".folderPushMode", FolderMode.FIRST_CLASS);
        this.mFolderTargetMode = (FolderMode) Preferences.getEnumStringPref(storage, this.mUuid + ".folderTargetMode", FolderMode.NOT_SECOND_CLASS);
        this.searchableFolders = (Searchable) Preferences.getEnumStringPref(storage, this.mUuid + ".searchableFolders", Searchable.ALL);
        this.mIsSignatureBeforeQuotedText = storage.getBoolean(this.mUuid + ".signatureBeforeQuotedText", false);
        this.identities = loadIdentities(storage);
        setCryptoApp(storage.getString(this.mUuid + ".cryptoApp", ""));
        this.mCryptoKey = storage.getLong(this.mUuid + ".cryptoKey", 0);
        this.mAllowRemoteSearch = storage.getBoolean(this.mUuid + ".allowRemoteSearch", true);
        this.mRemoteSearchFullText = storage.getBoolean(this.mUuid + ".remoteSearchFullText", true);
        this.mRemoteSearchNumResults = storage.getInt(this.mUuid + ".remoteSearchNumResults", 25);
        this.mEnabled = storage.getBoolean(this.mUuid + ".enabled", true);
        this.mMarkMessageAsReadOnView = storage.getBoolean(this.mUuid + ".markMessageAsReadOnView", true);
        this.mAlwaysShowCcBcc = storage.getBoolean(this.mUuid + ".alwaysShowCcBcc", false);
        cacheChips();
        if (this.mDescription == null) {
            this.mDescription = getEmail();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fsck.k9.helper.Utility.combine(java.lang.Object[], char):java.lang.String
     arg types: [java.lang.Object[], int]
     candidates:
      com.fsck.k9.helper.Utility.combine(java.lang.Iterable<?>, char):java.lang.String
      com.fsck.k9.helper.Utility.combine(java.lang.Object[], char):java.lang.String */
    /* access modifiers changed from: protected */
    public synchronized void delete(Preferences preferences) {
        synchronized (this) {
            String[] uuids = preferences.getStorage().getString("accountUuids", "").split(",");
            List<String> newUuids = new ArrayList<>(uuids.length);
            for (String uuid : uuids) {
                if (!uuid.equals(this.mUuid)) {
                    newUuids.add(uuid);
                }
            }
            StorageEditor editor = preferences.getStorage().edit();
            if (newUuids.size() < uuids.length) {
                editor.putString("accountUuids", Utility.combine(newUuids.toArray(), ','));
            }
            editor.remove(this.mUuid + ".storeUri");
            editor.remove(this.mUuid + ".transportUri");
            editor.remove(this.mUuid + ".description");
            editor.remove(this.mUuid + ".name");
            editor.remove(this.mUuid + ".email");
            editor.remove(this.mUuid + ".alwaysBcc");
            editor.remove(this.mUuid + ".automaticCheckIntervalMinutes");
            editor.remove(this.mUuid + ".pushPollOnConnect");
            editor.remove(this.mUuid + ".idleRefreshMinutes");
            editor.remove(this.mUuid + ".lastAutomaticCheckTime");
            editor.remove(this.mUuid + ".latestOldMessageSeenTime");
            editor.remove(this.mUuid + ".notifyNewMail");
            editor.remove(this.mUuid + ".notifySelfNewMail");
            editor.remove(this.mUuid + ".deletePolicy");
            editor.remove(this.mUuid + ".draftsFolderName");
            editor.remove(this.mUuid + ".sentFolderName");
            editor.remove(this.mUuid + ".trashFolderName");
            editor.remove(this.mUuid + ".archiveFolderName");
            editor.remove(this.mUuid + ".spamFolderName");
            editor.remove(this.mUuid + ".autoExpandFolderName");
            editor.remove(this.mUuid + ".accountNumber");
            editor.remove(this.mUuid + ".vibrate");
            editor.remove(this.mUuid + ".vibratePattern");
            editor.remove(this.mUuid + ".vibrateTimes");
            editor.remove(this.mUuid + ".ring");
            editor.remove(this.mUuid + ".ringtone");
            editor.remove(this.mUuid + ".folderDisplayMode");
            editor.remove(this.mUuid + ".folderSyncMode");
            editor.remove(this.mUuid + ".folderPushMode");
            editor.remove(this.mUuid + ".folderTargetMode");
            editor.remove(this.mUuid + ".signatureBeforeQuotedText");
            editor.remove(this.mUuid + ".expungePolicy");
            editor.remove(this.mUuid + ".syncRemoteDeletions");
            editor.remove(this.mUuid + ".maxPushFolders");
            editor.remove(this.mUuid + ".searchableFolders");
            editor.remove(this.mUuid + ".chipColor");
            editor.remove(this.mUuid + ".led");
            editor.remove(this.mUuid + ".ledColor");
            editor.remove(this.mUuid + ".goToUnreadMessageSearch");
            editor.remove(this.mUuid + ".subscribedFoldersOnly");
            editor.remove(this.mUuid + ".maximumPolledMessageAge");
            editor.remove(this.mUuid + ".maximumAutoDownloadMessageSize");
            editor.remove(this.mUuid + ".messageFormatAuto");
            editor.remove(this.mUuid + ".quoteStyle");
            editor.remove(this.mUuid + ".quotePrefix");
            editor.remove(this.mUuid + ".sortTypeEnum");
            editor.remove(this.mUuid + ".sortAscending");
            editor.remove(this.mUuid + ".showPicturesEnum");
            editor.remove(this.mUuid + ".replyAfterQuote");
            editor.remove(this.mUuid + ".stripSignature");
            editor.remove(this.mUuid + ".cryptoApp");
            editor.remove(this.mUuid + ".cryptoAutoSignature");
            editor.remove(this.mUuid + ".cryptoAutoEncrypt");
            editor.remove(this.mUuid + ".enabled");
            editor.remove(this.mUuid + ".markMessageAsReadOnView");
            editor.remove(this.mUuid + ".alwaysShowCcBcc");
            editor.remove(this.mUuid + ".allowRemoteSearch");
            editor.remove(this.mUuid + ".remoteSearchFullText");
            editor.remove(this.mUuid + ".remoteSearchNumResults");
            editor.remove(this.mUuid + ".defaultQuotedTextShown");
            editor.remove(this.mUuid + ".displayCount");
            editor.remove(this.mUuid + ".inboxFolderName");
            editor.remove(this.mUuid + ".localStorageProvider");
            editor.remove(this.mUuid + ".messageFormat");
            editor.remove(this.mUuid + ".messageReadReceipt");
            editor.remove(this.mUuid + ".notifyMailCheck");
            NetworkType[] values = NetworkType.values();
            int length = values.length;
            for (int i = 0; i < length; i++) {
                editor.remove(this.mUuid + ".useCompression." + values[i].name());
            }
            deleteIdentities(preferences.getStorage(), editor);
            editor.commit();
        }
    }

    public static int findNewAccountNumber(List<Integer> accountNumbers) {
        int accountNumber;
        int newAccountNumber = -1;
        Collections.sort(accountNumbers);
        Iterator<Integer> it = accountNumbers.iterator();
        while (it.hasNext() && (accountNumber = it.next().intValue()) <= newAccountNumber + 1) {
            newAccountNumber = accountNumber;
        }
        return newAccountNumber + 1;
    }

    public static List<Integer> getExistingAccountNumbers(Preferences preferences) {
        List<Account> accounts = preferences.getAccounts();
        List<Integer> accountNumbers = new ArrayList<>(accounts.size());
        for (Account a : accounts) {
            accountNumbers.add(Integer.valueOf(a.getAccountNumber()));
        }
        return accountNumbers;
    }

    public static int generateAccountNumber(Preferences preferences) {
        return findNewAccountNumber(getExistingAccountNumbers(preferences));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fsck.k9.helper.Utility.combine(java.lang.Object[], char):java.lang.String
     arg types: [java.lang.String[], int]
     candidates:
      com.fsck.k9.helper.Utility.combine(java.lang.Iterable<?>, char):java.lang.String
      com.fsck.k9.helper.Utility.combine(java.lang.Object[], char):java.lang.String */
    public void move(Preferences preferences, boolean moveUp) {
        String[] uuids = preferences.getStorage().getString("accountUuids", "").split(",");
        StorageEditor editor = preferences.getStorage().edit();
        String[] newUuids = new String[uuids.length];
        if (moveUp) {
            for (int i = 0; i < uuids.length; i++) {
                if (i <= 0 || !uuids[i].equals(this.mUuid)) {
                    newUuids[i] = uuids[i];
                } else {
                    newUuids[i] = newUuids[i - 1];
                    newUuids[i - 1] = this.mUuid;
                }
            }
        } else {
            for (int i2 = uuids.length - 1; i2 >= 0; i2--) {
                if (i2 >= uuids.length - 1 || !uuids[i2].equals(this.mUuid)) {
                    newUuids[i2] = uuids[i2];
                } else {
                    newUuids[i2] = newUuids[i2 + 1];
                    newUuids[i2 + 1] = this.mUuid;
                }
            }
        }
        editor.putString("accountUuids", Utility.combine((Object[]) newUuids, ','));
        editor.commit();
        preferences.loadAccounts();
    }

    public synchronized void save(Preferences preferences) {
        StorageEditor editor = preferences.getStorage().edit();
        if (!preferences.getStorage().getString("accountUuids", "").contains(this.mUuid)) {
            List<Account> accounts = preferences.getAccounts();
            int[] accountNumbers = new int[accounts.size()];
            for (int i = 0; i < accounts.size(); i++) {
                accountNumbers[i] = accounts.get(i).getAccountNumber();
            }
            Arrays.sort(accountNumbers);
            for (int accountNumber : accountNumbers) {
                if (accountNumber > this.mAccountNumber + 1) {
                    break;
                }
                this.mAccountNumber = accountNumber;
            }
            this.mAccountNumber++;
            String accountUuids = preferences.getStorage().getString("accountUuids", "");
            editor.putString("accountUuids", accountUuids + (accountUuids.length() != 0 ? "," : "") + this.mUuid);
        }
        editor.putString(this.mUuid + ".storeUri", Base64.encode(this.mStoreUri));
        editor.putString(this.mUuid + ".localStorageProvider", this.mLocalStorageProviderId);
        editor.putString(this.mUuid + ".transportUri", Base64.encode(this.mTransportUri));
        editor.putString(this.mUuid + ".description", this.mDescription);
        editor.putString(this.mUuid + ".alwaysBcc", this.mAlwaysBcc);
        editor.putInt(this.mUuid + ".automaticCheckIntervalMinutes", this.mAutomaticCheckIntervalMinutes);
        editor.putInt(this.mUuid + ".idleRefreshMinutes", this.mIdleRefreshMinutes);
        editor.putBoolean(this.mUuid + ".pushPollOnConnect", this.mPushPollOnConnect);
        editor.putInt(this.mUuid + ".displayCount", this.mDisplayCount);
        editor.putLong(this.mUuid + ".lastAutomaticCheckTime", this.mLastAutomaticCheckTime);
        editor.putLong(this.mUuid + ".latestOldMessageSeenTime", this.mLatestOldMessageSeenTime);
        editor.putBoolean(this.mUuid + ".notifyNewMail", this.mNotifyNewMail);
        editor.putString(this.mUuid + ".folderNotifyNewMailMode", this.mFolderNotifyNewMailMode.name());
        editor.putBoolean(this.mUuid + ".notifySelfNewMail", this.mNotifySelfNewMail);
        editor.putBoolean(this.mUuid + ".notifyContactsMailOnly", this.mNotifyContactsMailOnly);
        editor.putBoolean(this.mUuid + ".notifyMailCheck", this.mNotifySync);
        editor.putInt(this.mUuid + ".deletePolicy", this.mDeletePolicy.setting);
        editor.putString(this.mUuid + ".inboxFolderName", this.mInboxFolderName);
        editor.putString(this.mUuid + ".draftsFolderName", this.mDraftsFolderName);
        editor.putString(this.mUuid + ".sentFolderName", this.mSentFolderName);
        editor.putString(this.mUuid + ".trashFolderName", this.mTrashFolderName);
        editor.putString(this.mUuid + ".archiveFolderName", this.mArchiveFolderName);
        editor.putString(this.mUuid + ".spamFolderName", this.mSpamFolderName);
        editor.putString(this.mUuid + ".autoExpandFolderName", this.mAutoExpandFolderName);
        editor.putInt(this.mUuid + ".accountNumber", this.mAccountNumber);
        editor.putString(this.mUuid + ".sortTypeEnum", this.mSortType.name());
        editor.putBoolean(this.mUuid + ".sortAscending", this.mSortAscending.get(this.mSortType).booleanValue());
        editor.putString(this.mUuid + ".showPicturesEnum", this.mShowPictures.name());
        editor.putString(this.mUuid + ".folderDisplayMode", this.mFolderDisplayMode.name());
        editor.putString(this.mUuid + ".folderSyncMode", this.mFolderSyncMode.name());
        editor.putString(this.mUuid + ".folderPushMode", this.mFolderPushMode.name());
        editor.putString(this.mUuid + ".folderTargetMode", this.mFolderTargetMode.name());
        editor.putBoolean(this.mUuid + ".signatureBeforeQuotedText", this.mIsSignatureBeforeQuotedText);
        editor.putString(this.mUuid + ".expungePolicy", this.mExpungePolicy.name());
        editor.putBoolean(this.mUuid + ".syncRemoteDeletions", this.mSyncRemoteDeletions);
        editor.putInt(this.mUuid + ".maxPushFolders", this.mMaxPushFolders);
        editor.putString(this.mUuid + ".searchableFolders", this.searchableFolders.name());
        editor.putInt(this.mUuid + ".chipColor", this.mChipColor);
        editor.putBoolean(this.mUuid + ".goToUnreadMessageSearch", this.goToUnreadMessageSearch);
        editor.putBoolean(this.mUuid + ".subscribedFoldersOnly", this.subscribedFoldersOnly);
        editor.putInt(this.mUuid + ".maximumPolledMessageAge", this.maximumPolledMessageAge);
        editor.putInt(this.mUuid + ".maximumAutoDownloadMessageSize", this.maximumAutoDownloadMessageSize);
        if (MessageFormat.AUTO.equals(this.mMessageFormat)) {
            editor.putString(this.mUuid + ".messageFormat", MessageFormat.TEXT.name());
            this.mMessageFormatAuto = true;
        } else {
            editor.putString(this.mUuid + ".messageFormat", this.mMessageFormat.name());
            this.mMessageFormatAuto = false;
        }
        editor.putBoolean(this.mUuid + ".messageFormatAuto", this.mMessageFormatAuto);
        editor.putBoolean(this.mUuid + ".messageReadReceipt", this.mMessageReadReceipt);
        editor.putString(this.mUuid + ".quoteStyle", this.mQuoteStyle.name());
        editor.putString(this.mUuid + ".quotePrefix", this.mQuotePrefix);
        editor.putBoolean(this.mUuid + ".defaultQuotedTextShown", this.mDefaultQuotedTextShown);
        editor.putBoolean(this.mUuid + ".replyAfterQuote", this.mReplyAfterQuote);
        editor.putBoolean(this.mUuid + ".stripSignature", this.mStripSignature);
        editor.putString(this.mUuid + ".cryptoApp", this.mCryptoApp);
        editor.putLong(this.mUuid + ".cryptoKey", this.mCryptoKey);
        editor.putBoolean(this.mUuid + ".allowRemoteSearch", this.mAllowRemoteSearch);
        editor.putBoolean(this.mUuid + ".remoteSearchFullText", this.mRemoteSearchFullText);
        editor.putInt(this.mUuid + ".remoteSearchNumResults", this.mRemoteSearchNumResults);
        editor.putBoolean(this.mUuid + ".enabled", this.mEnabled);
        editor.putBoolean(this.mUuid + ".markMessageAsReadOnView", this.mMarkMessageAsReadOnView);
        editor.putBoolean(this.mUuid + ".alwaysShowCcBcc", this.mAlwaysShowCcBcc);
        editor.putBoolean(this.mUuid + ".vibrate", this.mNotificationSetting.shouldVibrate());
        editor.putInt(this.mUuid + ".vibratePattern", this.mNotificationSetting.getVibratePattern());
        editor.putInt(this.mUuid + ".vibrateTimes", this.mNotificationSetting.getVibrateTimes());
        editor.putBoolean(this.mUuid + ".ring", this.mNotificationSetting.shouldRing());
        editor.putString(this.mUuid + ".ringtone", this.mNotificationSetting.getRingtone());
        editor.putBoolean(this.mUuid + ".led", this.mNotificationSetting.isLed());
        editor.putInt(this.mUuid + ".ledColor", this.mNotificationSetting.getLedColor());
        for (NetworkType type : NetworkType.values()) {
            Boolean useCompression = this.compressionMap.get(type);
            if (useCompression != null) {
                editor.putBoolean(this.mUuid + ".useCompression." + type, useCompression.booleanValue());
            }
        }
        saveIdentities(preferences.getStorage(), editor);
        editor.commit();
    }

    public void resetVisibleLimits() {
        try {
            getLocalStore().resetVisibleLimits(getDisplayCount());
        } catch (MessagingException e) {
            Log.e("k9", "Unable to reset visible limits", e);
        }
    }

    /* JADX INFO: finally extract failed */
    public AccountStats getStats(Context context) throws MessagingException {
        if (!isAvailable(context)) {
            return null;
        }
        AccountStats stats = new AccountStats();
        LocalSearch search = new LocalSearch();
        excludeSpecialFolders(search);
        limitToDisplayableFolders(search);
        StringBuilder query = new StringBuilder();
        List<String> queryArgs = new ArrayList<>();
        SqlQueryBuilder.buildWhereClause(this, search.getConditions(), query, queryArgs);
        Cursor cursor = context.getContentResolver().query(Uri.withAppendedPath(EmailProvider.CONTENT_URI, "account/" + getUuid() + "/stats"), new String[]{"unread_count", "flagged_count"}, query.toString(), (String[]) queryArgs.toArray(new String[0]), null);
        try {
            if (cursor.moveToFirst()) {
                stats.unreadMessageCount = cursor.getInt(0);
                stats.flaggedMessageCount = cursor.getInt(1);
            }
            cursor.close();
            LocalStore localStore = getLocalStore();
            if (!K9.measureAccounts()) {
                return stats;
            }
            stats.size = localStore.getSize();
            return stats;
        } catch (Throwable th) {
            cursor.close();
            throw th;
        }
    }

    public synchronized void setChipColor(int color) {
        this.mChipColor = color;
        cacheChips();
    }

    public synchronized void cacheChips() {
        this.mReadColorChip = new ColorChip(this.mChipColor, true, ColorChip.CIRCULAR);
        this.mUnreadColorChip = new ColorChip(this.mChipColor, false, ColorChip.CIRCULAR);
        this.mFlaggedReadColorChip = new ColorChip(this.mChipColor, true, ColorChip.STAR);
        this.mFlaggedUnreadColorChip = new ColorChip(this.mChipColor, false, ColorChip.STAR);
    }

    public synchronized int getChipColor() {
        return this.mChipColor;
    }

    public ColorChip generateColorChip(boolean messageRead, boolean toMe, boolean ccMe, boolean fromMe, boolean messageFlagged) {
        if (messageRead) {
            if (messageFlagged) {
                return this.mFlaggedReadColorChip;
            }
            return this.mReadColorChip;
        } else if (messageFlagged) {
            return this.mFlaggedUnreadColorChip;
        } else {
            return this.mUnreadColorChip;
        }
    }

    public String getUuid() {
        return this.mUuid;
    }

    public Uri getContentUri() {
        return Uri.parse("content://accounts/" + getUuid());
    }

    public synchronized String getStoreUri() {
        return this.mStoreUri;
    }

    public synchronized void setStoreUri(String storeUri) {
        this.mStoreUri = storeUri;
    }

    public synchronized String getTransportUri() {
        return this.mTransportUri;
    }

    public synchronized void setTransportUri(String transportUri) {
        this.mTransportUri = transportUri;
    }

    public synchronized String getDescription() {
        return this.mDescription;
    }

    public synchronized void setDescription(String description) {
        this.mDescription = description;
    }

    public synchronized String getName() {
        return this.identities.get(0).getName();
    }

    public synchronized void setName(String name) {
        this.identities.get(0).setName(name);
    }

    public synchronized boolean getSignatureUse() {
        return this.identities.get(0).getSignatureUse();
    }

    public synchronized void setSignatureUse(boolean signatureUse) {
        this.identities.get(0).setSignatureUse(signatureUse);
    }

    public synchronized String getSignature() {
        return this.identities.get(0).getSignature();
    }

    public synchronized void setSignature(String signature) {
        this.identities.get(0).setSignature(signature);
    }

    public synchronized String getEmail() {
        return this.identities.get(0).getEmail();
    }

    public synchronized void setEmail(String email) {
        this.identities.get(0).setEmail(email);
    }

    public synchronized String getAlwaysBcc() {
        return this.mAlwaysBcc;
    }

    public synchronized void setAlwaysBcc(String alwaysBcc) {
        this.mAlwaysBcc = alwaysBcc;
    }

    public boolean isRingNotified() {
        return this.mRingNotified;
    }

    public void setRingNotified(boolean ringNotified) {
        this.mRingNotified = ringNotified;
    }

    public String getLocalStorageProviderId() {
        return this.mLocalStorageProviderId;
    }

    public void setLocalStorageProviderId(String id) {
        if (!this.mLocalStorageProviderId.equals(id)) {
            try {
                switchLocalStorage(id);
                if (1 == 0) {
                    return;
                }
            } catch (MessagingException e) {
                Log.e("k9", "Switching local storage provider from " + this.mLocalStorageProviderId + " to " + id + " failed.", e);
                if (0 == 0) {
                    return;
                }
            } catch (Throwable th) {
                if (0 != 0) {
                    throw th;
                }
                return;
            }
            this.mLocalStorageProviderId = id;
        }
    }

    public synchronized int getAutomaticCheckIntervalMinutes() {
        return this.mAutomaticCheckIntervalMinutes;
    }

    public synchronized boolean setAutomaticCheckIntervalMinutes(int automaticCheckIntervalMinutes) {
        int oldInterval;
        oldInterval = this.mAutomaticCheckIntervalMinutes;
        this.mAutomaticCheckIntervalMinutes = automaticCheckIntervalMinutes;
        return oldInterval != automaticCheckIntervalMinutes;
    }

    public synchronized int getDisplayCount() {
        return this.mDisplayCount;
    }

    public synchronized void setDisplayCount(int displayCount) {
        if (displayCount != -1) {
            this.mDisplayCount = displayCount;
        } else {
            this.mDisplayCount = 25;
        }
        resetVisibleLimits();
    }

    public synchronized long getLastAutomaticCheckTime() {
        return this.mLastAutomaticCheckTime;
    }

    public synchronized void setLastAutomaticCheckTime(long lastAutomaticCheckTime) {
        this.mLastAutomaticCheckTime = lastAutomaticCheckTime;
    }

    public synchronized long getLatestOldMessageSeenTime() {
        return this.mLatestOldMessageSeenTime;
    }

    public synchronized void setLatestOldMessageSeenTime(long latestOldMessageSeenTime) {
        this.mLatestOldMessageSeenTime = latestOldMessageSeenTime;
    }

    public synchronized boolean isNotifyNewMail() {
        return this.mNotifyNewMail;
    }

    public synchronized void setNotifyNewMail(boolean notifyNewMail) {
        this.mNotifyNewMail = notifyNewMail;
    }

    public synchronized FolderMode getFolderNotifyNewMailMode() {
        return this.mFolderNotifyNewMailMode;
    }

    public synchronized void setFolderNotifyNewMailMode(FolderMode folderNotifyNewMailMode) {
        this.mFolderNotifyNewMailMode = folderNotifyNewMailMode;
    }

    public synchronized DeletePolicy getDeletePolicy() {
        return this.mDeletePolicy;
    }

    public synchronized void setDeletePolicy(DeletePolicy deletePolicy) {
        this.mDeletePolicy = deletePolicy;
    }

    public boolean isSpecialFolder(String folderName) {
        if (folderName == null || (!folderName.equalsIgnoreCase(getInboxFolderName()) && !folderName.equals(getTrashFolderName()) && !folderName.equals(getDraftsFolderName()) && !folderName.equals(getArchiveFolderName()) && !folderName.equals(getSpamFolderName()) && !folderName.equals(getOutboxFolderName()) && !folderName.equals(getSentFolderName()) && !folderName.equals(getErrorFolderName()))) {
            return false;
        }
        return true;
    }

    public synchronized String getDraftsFolderName() {
        return this.mDraftsFolderName;
    }

    public synchronized void setDraftsFolderName(String name) {
        this.mDraftsFolderName = name;
    }

    public synchronized boolean hasDraftsFolder() {
        return !K9.FOLDER_NONE.equalsIgnoreCase(this.mDraftsFolderName);
    }

    public synchronized String getSentFolderName() {
        return this.mSentFolderName;
    }

    public synchronized String getErrorFolderName() {
        return K9.ERROR_FOLDER_NAME;
    }

    public synchronized void setSentFolderName(String name) {
        this.mSentFolderName = name;
    }

    public synchronized boolean hasSentFolder() {
        return !K9.FOLDER_NONE.equalsIgnoreCase(this.mSentFolderName);
    }

    public synchronized String getTrashFolderName() {
        return this.mTrashFolderName;
    }

    public synchronized void setTrashFolderName(String name) {
        this.mTrashFolderName = name;
    }

    public synchronized boolean hasTrashFolder() {
        return !K9.FOLDER_NONE.equalsIgnoreCase(this.mTrashFolderName);
    }

    public synchronized String getArchiveFolderName() {
        return this.mArchiveFolderName;
    }

    public synchronized void setArchiveFolderName(String archiveFolderName) {
        this.mArchiveFolderName = archiveFolderName;
    }

    public synchronized boolean hasArchiveFolder() {
        return !K9.FOLDER_NONE.equalsIgnoreCase(this.mArchiveFolderName);
    }

    public synchronized String getSpamFolderName() {
        return this.mSpamFolderName;
    }

    public synchronized void setSpamFolderName(String name) {
        this.mSpamFolderName = name;
    }

    public synchronized boolean hasSpamFolder() {
        return !K9.FOLDER_NONE.equalsIgnoreCase(this.mSpamFolderName);
    }

    public synchronized String getOutboxFolderName() {
        return OUTBOX;
    }

    public synchronized String getAutoExpandFolderName() {
        return this.mAutoExpandFolderName;
    }

    public synchronized void setAutoExpandFolderName(String name) {
        this.mAutoExpandFolderName = name;
    }

    public synchronized int getAccountNumber() {
        return this.mAccountNumber;
    }

    public synchronized FolderMode getFolderDisplayMode() {
        return this.mFolderDisplayMode;
    }

    public synchronized boolean setFolderDisplayMode(FolderMode displayMode) {
        FolderMode oldDisplayMode;
        oldDisplayMode = this.mFolderDisplayMode;
        this.mFolderDisplayMode = displayMode;
        return oldDisplayMode != displayMode;
    }

    public synchronized FolderMode getFolderSyncMode() {
        return this.mFolderSyncMode;
    }

    public synchronized boolean setFolderSyncMode(FolderMode syncMode) {
        boolean z = true;
        synchronized (this) {
            FolderMode oldSyncMode = this.mFolderSyncMode;
            this.mFolderSyncMode = syncMode;
            if (syncMode != FolderMode.NONE || oldSyncMode == FolderMode.NONE) {
                if (syncMode == FolderMode.NONE || oldSyncMode != FolderMode.NONE) {
                    z = false;
                }
            }
        }
        return z;
    }

    public synchronized FolderMode getFolderPushMode() {
        return this.mFolderPushMode;
    }

    public synchronized boolean setFolderPushMode(FolderMode pushMode) {
        FolderMode oldPushMode;
        oldPushMode = this.mFolderPushMode;
        this.mFolderPushMode = pushMode;
        return pushMode != oldPushMode;
    }

    public synchronized boolean isShowOngoing() {
        return this.mNotifySync;
    }

    public synchronized void setShowOngoing(boolean showOngoing) {
        this.mNotifySync = showOngoing;
    }

    public synchronized SortType getSortType() {
        return this.mSortType;
    }

    public synchronized void setSortType(SortType sortType) {
        this.mSortType = sortType;
    }

    public synchronized boolean isSortAscending(SortType sortType) {
        if (this.mSortAscending.get(sortType) == null) {
            this.mSortAscending.put(sortType, Boolean.valueOf(sortType.isDefaultAscending()));
        }
        return this.mSortAscending.get(sortType).booleanValue();
    }

    public synchronized void setSortAscending(SortType sortType, boolean sortAscending) {
        this.mSortAscending.put(sortType, Boolean.valueOf(sortAscending));
    }

    public synchronized ShowPictures getShowPictures() {
        return this.mShowPictures;
    }

    public synchronized void setShowPictures(ShowPictures showPictures) {
        this.mShowPictures = showPictures;
    }

    public synchronized FolderMode getFolderTargetMode() {
        return this.mFolderTargetMode;
    }

    public synchronized void setFolderTargetMode(FolderMode folderTargetMode) {
        this.mFolderTargetMode = folderTargetMode;
    }

    public synchronized boolean isSignatureBeforeQuotedText() {
        return this.mIsSignatureBeforeQuotedText;
    }

    public synchronized void setSignatureBeforeQuotedText(boolean mIsSignatureBeforeQuotedText2) {
        this.mIsSignatureBeforeQuotedText = mIsSignatureBeforeQuotedText2;
    }

    public synchronized boolean isNotifySelfNewMail() {
        return this.mNotifySelfNewMail;
    }

    public synchronized void setNotifySelfNewMail(boolean notifySelfNewMail) {
        this.mNotifySelfNewMail = notifySelfNewMail;
    }

    public synchronized boolean isNotifyContactsMailOnly() {
        return this.mNotifyContactsMailOnly;
    }

    public synchronized void setNotifyContactsMailOnly(boolean notifyContactsMailOnly) {
        this.mNotifyContactsMailOnly = notifyContactsMailOnly;
    }

    public synchronized Expunge getExpungePolicy() {
        return this.mExpungePolicy;
    }

    public synchronized void setExpungePolicy(Expunge expungePolicy) {
        this.mExpungePolicy = expungePolicy;
    }

    public synchronized int getMaxPushFolders() {
        return this.mMaxPushFolders;
    }

    public synchronized boolean setMaxPushFolders(int maxPushFolders) {
        int oldMaxPushFolders;
        oldMaxPushFolders = this.mMaxPushFolders;
        this.mMaxPushFolders = maxPushFolders;
        return oldMaxPushFolders != maxPushFolders;
    }

    public LocalStore getLocalStore() throws MessagingException {
        return LocalStore.getInstance(this, K9.app);
    }

    public Store getRemoteStore() throws MessagingException {
        return RemoteStore.getInstance(K9.app, this, Globals.getOAuth2TokenProvider());
    }

    public boolean isSearchByDateCapable() {
        return getStoreUri().startsWith("imap");
    }

    public synchronized String toString() {
        return this.mDescription;
    }

    public synchronized void setCompression(NetworkType networkType, boolean useCompression) {
        this.compressionMap.put(networkType, Boolean.valueOf(useCompression));
    }

    public synchronized boolean useCompression(NetworkType networkType) {
        boolean booleanValue;
        Boolean useCompression = this.compressionMap.get(networkType);
        if (useCompression == null) {
            booleanValue = true;
        } else {
            booleanValue = useCompression.booleanValue();
        }
        return booleanValue;
    }

    public boolean equals(Object o) {
        if (o instanceof Account) {
            return ((Account) o).mUuid.equals(this.mUuid);
        }
        return super.equals(o);
    }

    public int hashCode() {
        return this.mUuid.hashCode();
    }

    private synchronized List<Identity> loadIdentities(Storage storage) {
        List<Identity> newIdentities;
        boolean gotOne;
        newIdentities = new ArrayList<>();
        int ident = 0;
        do {
            gotOne = false;
            String name = storage.getString(this.mUuid + "." + "name" + "." + ident, null);
            String email = storage.getString(this.mUuid + "." + "email" + "." + ident, null);
            boolean signatureUse = storage.getBoolean(this.mUuid + ".signatureUse." + ident, true);
            String signature = storage.getString(this.mUuid + ".signature." + ident, null);
            String description = storage.getString(this.mUuid + "." + "description" + "." + ident, null);
            String replyTo = storage.getString(this.mUuid + ".replyTo." + ident, null);
            if (email != null) {
                Identity identity = new Identity();
                identity.setName(name);
                identity.setEmail(email);
                identity.setSignatureUse(signatureUse);
                identity.setSignature(signature);
                identity.setDescription(description);
                identity.setReplyTo(replyTo);
                newIdentities.add(identity);
                gotOne = true;
            }
            ident++;
        } while (gotOne);
        if (newIdentities.isEmpty()) {
            String name2 = storage.getString(this.mUuid + ".name", null);
            String email2 = storage.getString(this.mUuid + ".email", null);
            boolean signatureUse2 = storage.getBoolean(this.mUuid + ".signatureUse", true);
            String signature2 = storage.getString(this.mUuid + ".signature", null);
            Identity identity2 = new Identity();
            identity2.setName(name2);
            identity2.setEmail(email2);
            identity2.setSignatureUse(signatureUse2);
            identity2.setSignature(signature2);
            identity2.setDescription(email2);
            newIdentities.add(identity2);
        }
        return newIdentities;
    }

    private synchronized void deleteIdentities(Storage storage, StorageEditor editor) {
        boolean gotOne;
        int ident = 0;
        do {
            gotOne = false;
            if (storage.getString(this.mUuid + "." + "email" + "." + ident, null) != null) {
                editor.remove(this.mUuid + "." + "name" + "." + ident);
                editor.remove(this.mUuid + "." + "email" + "." + ident);
                editor.remove(this.mUuid + ".signatureUse." + ident);
                editor.remove(this.mUuid + ".signature." + ident);
                editor.remove(this.mUuid + "." + "description" + "." + ident);
                editor.remove(this.mUuid + ".replyTo." + ident);
                gotOne = true;
            }
            ident++;
        } while (gotOne);
    }

    private synchronized void saveIdentities(Storage storage, StorageEditor editor) {
        deleteIdentities(storage, editor);
        int ident = 0;
        for (Identity identity : this.identities) {
            editor.putString(this.mUuid + "." + "name" + "." + ident, identity.getName());
            editor.putString(this.mUuid + "." + "email" + "." + ident, identity.getEmail());
            editor.putBoolean(this.mUuid + ".signatureUse." + ident, identity.getSignatureUse());
            editor.putString(this.mUuid + ".signature." + ident, identity.getSignature());
            editor.putString(this.mUuid + "." + "description" + "." + ident, identity.getDescription());
            editor.putString(this.mUuid + ".replyTo." + ident, identity.getReplyTo());
            ident++;
        }
    }

    public synchronized List<Identity> getIdentities() {
        return this.identities;
    }

    public synchronized void setIdentities(List<Identity> newIdentities) {
        this.identities = new ArrayList(newIdentities);
    }

    public synchronized Identity getIdentity(int i) {
        if (i < this.identities.size()) {
        } else {
            throw new IllegalArgumentException("Identity with index " + i + " not found");
        }
        return this.identities.get(i);
    }

    public boolean isAnIdentity(Address[] addrs) {
        if (addrs == null) {
            return false;
        }
        for (Address addr : addrs) {
            if (findIdentity(addr) != null) {
                return true;
            }
        }
        return false;
    }

    public boolean isAnIdentity(Address addr) {
        return findIdentity(addr) != null;
    }

    public synchronized Identity findIdentity(Address addr) {
        Identity identity;
        Iterator<Identity> it = this.identities.iterator();
        while (true) {
            if (!it.hasNext()) {
                identity = null;
                break;
            }
            identity = it.next();
            String email = identity.getEmail();
            if (email != null && email.equalsIgnoreCase(addr.getAddress())) {
                break;
            }
        }
        return identity;
    }

    public synchronized Searchable getSearchableFolders() {
        return this.searchableFolders;
    }

    public synchronized void setSearchableFolders(Searchable searchableFolders2) {
        this.searchableFolders = searchableFolders2;
    }

    public synchronized int getIdleRefreshMinutes() {
        return this.mIdleRefreshMinutes;
    }

    public synchronized void setIdleRefreshMinutes(int idleRefreshMinutes) {
        this.mIdleRefreshMinutes = idleRefreshMinutes;
    }

    public synchronized boolean isPushPollOnConnect() {
        return this.mPushPollOnConnect;
    }

    public synchronized void setPushPollOnConnect(boolean pushPollOnConnect) {
        this.mPushPollOnConnect = pushPollOnConnect;
    }

    public void switchLocalStorage(String newStorageProviderId) throws MessagingException {
        if (!this.mLocalStorageProviderId.equals(newStorageProviderId)) {
            getLocalStore().switchLocalStorage(newStorageProviderId);
        }
    }

    public synchronized boolean goToUnreadMessageSearch() {
        return this.goToUnreadMessageSearch;
    }

    public synchronized void setGoToUnreadMessageSearch(boolean goToUnreadMessageSearch2) {
        this.goToUnreadMessageSearch = goToUnreadMessageSearch2;
    }

    public synchronized boolean subscribedFoldersOnly() {
        return this.subscribedFoldersOnly;
    }

    public synchronized void setSubscribedFoldersOnly(boolean subscribedFoldersOnly2) {
        this.subscribedFoldersOnly = subscribedFoldersOnly2;
    }

    public synchronized int getMaximumPolledMessageAge() {
        return this.maximumPolledMessageAge;
    }

    public synchronized void setMaximumPolledMessageAge(int maximumPolledMessageAge2) {
        this.maximumPolledMessageAge = maximumPolledMessageAge2;
    }

    public synchronized int getMaximumAutoDownloadMessageSize() {
        return this.maximumAutoDownloadMessageSize;
    }

    public synchronized void setMaximumAutoDownloadMessageSize(int maximumAutoDownloadMessageSize2) {
        this.maximumAutoDownloadMessageSize = maximumAutoDownloadMessageSize2;
    }

    public Date getEarliestPollDate() {
        int age = getMaximumPolledMessageAge();
        if (age < 0) {
            return null;
        }
        Calendar now = Calendar.getInstance();
        now.set(11, 0);
        now.set(12, 0);
        now.set(13, 0);
        now.set(14, 0);
        if (age >= 28) {
            switch (age) {
                case 28:
                    now.add(2, -1);
                    break;
                case 56:
                    now.add(2, -2);
                    break;
                case 84:
                    now.add(2, -3);
                    break;
                case 168:
                    now.add(2, -6);
                    break;
                case 365:
                    now.add(1, -1);
                    break;
            }
        } else {
            now.add(5, age * -1);
        }
        return now.getTime();
    }

    public MessageFormat getMessageFormat() {
        return this.mMessageFormat;
    }

    public void setMessageFormat(MessageFormat messageFormat) {
        this.mMessageFormat = messageFormat;
    }

    public synchronized boolean isMessageReadReceiptAlways() {
        return this.mMessageReadReceipt;
    }

    public synchronized void setMessageReadReceipt(boolean messageReadReceipt) {
        this.mMessageReadReceipt = messageReadReceipt;
    }

    public QuoteStyle getQuoteStyle() {
        return this.mQuoteStyle;
    }

    public void setQuoteStyle(QuoteStyle quoteStyle) {
        this.mQuoteStyle = quoteStyle;
    }

    public synchronized String getQuotePrefix() {
        return this.mQuotePrefix;
    }

    public synchronized void setQuotePrefix(String quotePrefix) {
        this.mQuotePrefix = quotePrefix;
    }

    public synchronized boolean isDefaultQuotedTextShown() {
        return this.mDefaultQuotedTextShown;
    }

    public synchronized void setDefaultQuotedTextShown(boolean shown) {
        this.mDefaultQuotedTextShown = shown;
    }

    public synchronized boolean isReplyAfterQuote() {
        return this.mReplyAfterQuote;
    }

    public synchronized void setReplyAfterQuote(boolean replyAfterQuote) {
        this.mReplyAfterQuote = replyAfterQuote;
    }

    public synchronized boolean isStripSignature() {
        return this.mStripSignature;
    }

    public synchronized void setStripSignature(boolean stripSignature) {
        this.mStripSignature = stripSignature;
    }

    public String getCryptoApp() {
        return this.mCryptoApp;
    }

    public void setCryptoApp(String cryptoApp) {
        if (cryptoApp == null || cryptoApp.equals("apg")) {
            this.mCryptoApp = "";
        } else {
            this.mCryptoApp = cryptoApp;
        }
    }

    public long getCryptoKey() {
        return this.mCryptoKey;
    }

    public void setCryptoKey(long keyId) {
        this.mCryptoKey = keyId;
    }

    public boolean allowRemoteSearch() {
        return this.mAllowRemoteSearch;
    }

    public void setAllowRemoteSearch(boolean val) {
        this.mAllowRemoteSearch = val;
    }

    public int getRemoteSearchNumResults() {
        return this.mRemoteSearchNumResults;
    }

    public void setRemoteSearchNumResults(int val) {
        if (val < 0) {
            val = 0;
        }
        this.mRemoteSearchNumResults = val;
    }

    public String getInboxFolderName() {
        return this.mInboxFolderName;
    }

    public void setInboxFolderName(String name) {
        this.mInboxFolderName = name;
    }

    public synchronized boolean syncRemoteDeletions() {
        return this.mSyncRemoteDeletions;
    }

    public synchronized void setSyncRemoteDeletions(boolean syncRemoteDeletions) {
        this.mSyncRemoteDeletions = syncRemoteDeletions;
    }

    public synchronized String getLastSelectedFolderName() {
        return this.lastSelectedFolderName;
    }

    public synchronized void setLastSelectedFolderName(String folderName) {
        this.lastSelectedFolderName = folderName;
    }

    public synchronized String getOpenPgpProvider() {
        String cryptoApp;
        if (!isOpenPgpProviderConfigured()) {
            cryptoApp = null;
        } else {
            cryptoApp = getCryptoApp();
        }
        return cryptoApp;
    }

    public synchronized boolean isOpenPgpProviderConfigured() {
        return !"".equals(getCryptoApp());
    }

    public synchronized NotificationSetting getNotificationSetting() {
        return this.mNotificationSetting;
    }

    public boolean isAvailable(Context context) {
        String localStorageProviderId = getLocalStorageProviderId();
        if (localStorageProviderId == null) {
            return true;
        }
        return StorageManager.getInstance(context).isReady(localStorageProviderId);
    }

    public synchronized boolean isEnabled() {
        return this.mEnabled;
    }

    public synchronized void setEnabled(boolean enabled) {
        this.mEnabled = enabled;
    }

    public synchronized boolean isMarkMessageAsReadOnView() {
        return this.mMarkMessageAsReadOnView;
    }

    public synchronized void setMarkMessageAsReadOnView(boolean value) {
        this.mMarkMessageAsReadOnView = value;
    }

    public synchronized boolean isAlwaysShowCcBcc() {
        return this.mAlwaysShowCcBcc;
    }

    public synchronized void setAlwaysShowCcBcc(boolean show) {
        this.mAlwaysShowCcBcc = show;
    }

    public boolean isRemoteSearchFullText() {
        return this.mRemoteSearchFullText;
    }

    public void setRemoteSearchFullText(boolean val) {
        this.mRemoteSearchFullText = val;
    }

    public void limitToDisplayableFolders(LocalSearch search) {
        switch (getFolderDisplayMode()) {
            case FIRST_CLASS:
                search.and(SearchSpecification.SearchField.DISPLAY_CLASS, Folder.FolderClass.FIRST_CLASS.name(), SearchSpecification.Attribute.EQUALS);
                return;
            case FIRST_AND_SECOND_CLASS:
                search.and(SearchSpecification.SearchField.DISPLAY_CLASS, Folder.FolderClass.FIRST_CLASS.name(), SearchSpecification.Attribute.EQUALS);
                SearchSpecification.SearchCondition searchCondition = new SearchSpecification.SearchCondition(SearchSpecification.SearchField.DISPLAY_CLASS, SearchSpecification.Attribute.EQUALS, Folder.FolderClass.SECOND_CLASS.name());
                ConditionsTreeNode root = search.getConditions();
                if (root.mRight != null) {
                    root.mRight.or(searchCondition);
                    return;
                } else {
                    search.or(searchCondition);
                    return;
                }
            case NOT_SECOND_CLASS:
                search.and(SearchSpecification.SearchField.DISPLAY_CLASS, Folder.FolderClass.SECOND_CLASS.name(), SearchSpecification.Attribute.NOT_EQUALS);
                return;
            default:
                return;
        }
    }

    public void excludeSpecialFolders(LocalSearch search) {
        excludeSpecialFolder(search, getTrashFolderName());
        excludeSpecialFolder(search, getDraftsFolderName());
        excludeSpecialFolder(search, getSpamFolderName());
        excludeSpecialFolder(search, getOutboxFolderName());
        excludeSpecialFolder(search, getSentFolderName());
        excludeSpecialFolder(search, getErrorFolderName());
        search.or(new SearchSpecification.SearchCondition(SearchSpecification.SearchField.FOLDER, SearchSpecification.Attribute.EQUALS, getInboxFolderName()));
    }

    public void excludeUnwantedFolders(LocalSearch search) {
        excludeSpecialFolder(search, getTrashFolderName());
        excludeSpecialFolder(search, getSpamFolderName());
        excludeSpecialFolder(search, getOutboxFolderName());
        search.or(new SearchSpecification.SearchCondition(SearchSpecification.SearchField.FOLDER, SearchSpecification.Attribute.EQUALS, getInboxFolderName()));
    }

    private void excludeSpecialFolder(LocalSearch search, String folderName) {
        if (!K9.FOLDER_NONE.equals(folderName)) {
            search.and(SearchSpecification.SearchField.FOLDER, folderName, SearchSpecification.Attribute.NOT_EQUALS);
        }
    }

    public void addCertificate(AccountSetupCheckSettings.CheckDirection direction, X509Certificate certificate) throws CertificateException {
        Uri uri;
        if (direction == AccountSetupCheckSettings.CheckDirection.INCOMING) {
            uri = Uri.parse(getStoreUri());
        } else {
            uri = Uri.parse(getTransportUri());
        }
        LocalKeyStore.getInstance().addCertificate(uri.getHost(), uri.getPort(), certificate);
    }

    public void deleteCertificate(String newHost, int newPort, AccountSetupCheckSettings.CheckDirection direction) {
        Uri uri;
        if (direction == AccountSetupCheckSettings.CheckDirection.INCOMING) {
            uri = Uri.parse(getStoreUri());
        } else {
            uri = Uri.parse(getTransportUri());
        }
        String oldHost = uri.getHost();
        int oldPort = uri.getPort();
        if (oldPort != -1) {
            if (!newHost.equals(oldHost) || newPort != oldPort) {
                LocalKeyStore.getInstance().deleteCertificate(oldHost, oldPort);
            }
        }
    }

    public void deleteCertificates() {
        LocalKeyStore localKeyStore = LocalKeyStore.getInstance();
        String storeUri = getStoreUri();
        if (storeUri != null) {
            Uri uri = Uri.parse(storeUri);
            localKeyStore.deleteCertificate(uri.getHost(), uri.getPort());
        }
        String transportUri = getTransportUri();
        if (transportUri != null) {
            Uri uri2 = Uri.parse(transportUri);
            localKeyStore.deleteCertificate(uri2.getHost(), uri2.getPort());
        }
    }
}
