package com.fsck.k9.activity.setup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import com.fsck.k9.FontSizes;
import com.fsck.k9.K9;
import com.fsck.k9.Preferences;
import com.fsck.k9.activity.K9PreferenceActivity;
import com.fsck.k9.preferences.StorageEditor;
import yahoo.mail.app.R;

public class FontSizeSettings extends K9PreferenceActivity {
    private static final int FONT_PERCENT_MAX = 250;
    private static final int FONT_PERCENT_MIN = 40;
    private static final String PREFERENCE_ACCOUNT_DESCRIPTION_FONT = "account_description_font";
    private static final String PREFERENCE_ACCOUNT_NAME_FONT = "account_name_font";
    private static final String PREFERENCE_FOLDER_NAME_FONT = "folder_name_font";
    private static final String PREFERENCE_FOLDER_STATUS_FONT = "folder_status_font";
    private static final String PREFERENCE_MESSAGE_COMPOSE_INPUT_FONT = "message_compose_input_font";
    private static final String PREFERENCE_MESSAGE_LIST_DATE_FONT = "message_list_date_font";
    private static final String PREFERENCE_MESSAGE_LIST_PREVIEW_FONT = "message_list_preview_font";
    private static final String PREFERENCE_MESSAGE_LIST_SENDER_FONT = "message_list_sender_font";
    private static final String PREFERENCE_MESSAGE_LIST_SUBJECT_FONT = "message_list_subject_font";
    private static final String PREFERENCE_MESSAGE_VIEW_ADDITIONAL_HEADERS_FONT = "message_view_additional_headers_font";
    private static final String PREFERENCE_MESSAGE_VIEW_CC_FONT = "message_view_cc_font";
    private static final String PREFERENCE_MESSAGE_VIEW_CONTENT_FONT_SLIDER = "message_view_content_font_slider";
    private static final String PREFERENCE_MESSAGE_VIEW_DATE_FONT = "message_view_date_font";
    private static final String PREFERENCE_MESSAGE_VIEW_SENDER_FONT = "message_view_sender_font";
    private static final String PREFERENCE_MESSAGE_VIEW_SUBJECT_FONT = "message_view_subject_font";
    private static final String PREFERENCE_MESSAGE_VIEW_TO_FONT = "message_view_to_font";
    private ListPreference mAccountDescription;
    private ListPreference mAccountName;
    private ListPreference mFolderName;
    private ListPreference mFolderStatus;
    private ListPreference mMessageComposeInput;
    private ListPreference mMessageListDate;
    private ListPreference mMessageListPreview;
    private ListPreference mMessageListSender;
    private ListPreference mMessageListSubject;
    private ListPreference mMessageViewAdditionalHeaders;
    private ListPreference mMessageViewCC;
    private SliderPreference mMessageViewContentSlider;
    private ListPreference mMessageViewDate;
    private ListPreference mMessageViewSender;
    private ListPreference mMessageViewSubject;
    private ListPreference mMessageViewTo;

    public static void actionEditSettings(Context context) {
        context.startActivity(new Intent(context, FontSizeSettings.class));
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FontSizes fontSizes = K9.getFontSizes();
        addPreferencesFromResource(R.xml.font_preferences);
        this.mAccountName = setupListPreference(PREFERENCE_ACCOUNT_NAME_FONT, Integer.toString(fontSizes.getAccountName()));
        this.mAccountDescription = setupListPreference(PREFERENCE_ACCOUNT_DESCRIPTION_FONT, Integer.toString(fontSizes.getAccountDescription()));
        this.mFolderName = setupListPreference(PREFERENCE_FOLDER_NAME_FONT, Integer.toString(fontSizes.getFolderName()));
        this.mFolderStatus = setupListPreference(PREFERENCE_FOLDER_STATUS_FONT, Integer.toString(fontSizes.getFolderStatus()));
        this.mMessageListSubject = setupListPreference(PREFERENCE_MESSAGE_LIST_SUBJECT_FONT, Integer.toString(fontSizes.getMessageListSubject()));
        this.mMessageListSender = setupListPreference(PREFERENCE_MESSAGE_LIST_SENDER_FONT, Integer.toString(fontSizes.getMessageListSender()));
        this.mMessageListDate = setupListPreference(PREFERENCE_MESSAGE_LIST_DATE_FONT, Integer.toString(fontSizes.getMessageListDate()));
        this.mMessageListPreview = setupListPreference(PREFERENCE_MESSAGE_LIST_PREVIEW_FONT, Integer.toString(fontSizes.getMessageListPreview()));
        this.mMessageViewSender = setupListPreference(PREFERENCE_MESSAGE_VIEW_SENDER_FONT, Integer.toString(fontSizes.getMessageViewSender()));
        this.mMessageViewTo = setupListPreference(PREFERENCE_MESSAGE_VIEW_TO_FONT, Integer.toString(fontSizes.getMessageViewTo()));
        this.mMessageViewCC = setupListPreference(PREFERENCE_MESSAGE_VIEW_CC_FONT, Integer.toString(fontSizes.getMessageViewCC()));
        this.mMessageViewAdditionalHeaders = setupListPreference(PREFERENCE_MESSAGE_VIEW_ADDITIONAL_HEADERS_FONT, Integer.toString(fontSizes.getMessageViewAdditionalHeaders()));
        this.mMessageViewSubject = setupListPreference(PREFERENCE_MESSAGE_VIEW_SUBJECT_FONT, Integer.toString(fontSizes.getMessageViewSubject()));
        this.mMessageViewDate = setupListPreference(PREFERENCE_MESSAGE_VIEW_DATE_FONT, Integer.toString(fontSizes.getMessageViewDate()));
        this.mMessageViewContentSlider = (SliderPreference) findPreference(PREFERENCE_MESSAGE_VIEW_CONTENT_FONT_SLIDER);
        final String summaryFormat = getString(R.string.font_size_message_view_content_summary);
        final String titleFormat = getString(R.string.font_size_message_view_content_dialog_title);
        this.mMessageViewContentSlider.setValue(scaleFromInt(fontSizes.getMessageViewContentAsPercent()));
        this.mMessageViewContentSlider.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                SliderPreference slider = (SliderPreference) preference;
                slider.setSummary(String.format(summaryFormat, Integer.valueOf(FontSizeSettings.this.scaleToInt(((Float) newValue).floatValue()))));
                slider.setDialogTitle(String.format(titleFormat, slider.getTitle(), slider.getSummary()));
                if (slider.getDialog() != null) {
                    slider.getDialog().setTitle(slider.getDialogTitle());
                }
                return true;
            }
        });
        this.mMessageViewContentSlider.getOnPreferenceChangeListener().onPreferenceChange(this.mMessageViewContentSlider, Float.valueOf(this.mMessageViewContentSlider.getValue()));
        this.mMessageComposeInput = setupListPreference(PREFERENCE_MESSAGE_COMPOSE_INPUT_FONT, Integer.toString(fontSizes.getMessageComposeInput()));
    }

    private void saveSettings() {
        FontSizes fontSizes = K9.getFontSizes();
        fontSizes.setAccountName(Integer.parseInt(this.mAccountName.getValue()));
        fontSizes.setAccountDescription(Integer.parseInt(this.mAccountDescription.getValue()));
        fontSizes.setFolderName(Integer.parseInt(this.mFolderName.getValue()));
        fontSizes.setFolderStatus(Integer.parseInt(this.mFolderStatus.getValue()));
        fontSizes.setMessageListSubject(Integer.parseInt(this.mMessageListSubject.getValue()));
        fontSizes.setMessageListSender(Integer.parseInt(this.mMessageListSender.getValue()));
        fontSizes.setMessageListDate(Integer.parseInt(this.mMessageListDate.getValue()));
        fontSizes.setMessageListPreview(Integer.parseInt(this.mMessageListPreview.getValue()));
        fontSizes.setMessageViewSender(Integer.parseInt(this.mMessageViewSender.getValue()));
        fontSizes.setMessageViewTo(Integer.parseInt(this.mMessageViewTo.getValue()));
        fontSizes.setMessageViewCC(Integer.parseInt(this.mMessageViewCC.getValue()));
        fontSizes.setMessageViewAdditionalHeaders(Integer.parseInt(this.mMessageViewAdditionalHeaders.getValue()));
        fontSizes.setMessageViewSubject(Integer.parseInt(this.mMessageViewSubject.getValue()));
        fontSizes.setMessageViewDate(Integer.parseInt(this.mMessageViewDate.getValue()));
        fontSizes.setMessageViewContentAsPercent(scaleToInt(this.mMessageViewContentSlider.getValue()));
        fontSizes.setMessageComposeInput(Integer.parseInt(this.mMessageComposeInput.getValue()));
        StorageEditor editor = Preferences.getPreferences(this).getStorage().edit();
        fontSizes.save(editor);
        editor.commit();
    }

    /* access modifiers changed from: private */
    public int scaleToInt(float sliderValue) {
        return (int) (40.0f + (210.0f * sliderValue));
    }

    private float scaleFromInt(int value) {
        return ((float) (value - 40)) / 210.0f;
    }

    public void onBackPressed() {
        saveSettings();
        super.onBackPressed();
    }
}
