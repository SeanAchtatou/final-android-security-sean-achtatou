package com.fsck.k9.activity.setup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import com.fsck.k9.Account;
import com.fsck.k9.K9;
import com.fsck.k9.Preferences;
import com.fsck.k9.activity.K9Activity;
import com.fsck.k9.ui.messageview.MessageTopView;
import yahoo.mail.app.R;

public class AccountSetupOptions extends K9Activity implements View.OnClickListener {
    private static final String EXTRA_ACCOUNT = "account";
    private static final String EXTRA_MAKE_DEFAULT = "makeDefault";
    private Account mAccount;
    private Spinner mCheckFrequencyView;
    private Spinner mDisplayCountView;
    private CheckBox mNotifySyncView;
    private CheckBox mNotifyView;
    private CheckBox mPushEnable;

    public static void actionOptions(Context context, Account account, boolean makeDefault) {
        Intent i = new Intent(context, AccountSetupOptions.class);
        i.putExtra("account", account.getUuid());
        i.putExtra(EXTRA_MAKE_DEFAULT, makeDefault);
        context.startActivity(i);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.account_setup_options);
        this.mCheckFrequencyView = (Spinner) findViewById(R.id.account_check_frequency);
        this.mDisplayCountView = (Spinner) findViewById(R.id.account_display_count);
        this.mNotifyView = (CheckBox) findViewById(R.id.account_notify);
        this.mNotifySyncView = (CheckBox) findViewById(R.id.account_notify_sync);
        this.mPushEnable = (CheckBox) findViewById(R.id.account_enable_push);
        findViewById(R.id.next).setOnClickListener(this);
        ArrayAdapter<SpinnerOption> checkFrequenciesAdapter = new ArrayAdapter<>(this, 17367048, new SpinnerOption[]{new SpinnerOption(-1, getString(R.string.account_setup_options_mail_check_frequency_never)), new SpinnerOption(1, getString(R.string.account_setup_options_mail_check_frequency_1min)), new SpinnerOption(5, getString(R.string.account_setup_options_mail_check_frequency_5min)), new SpinnerOption(10, getString(R.string.account_setup_options_mail_check_frequency_10min)), new SpinnerOption(15, getString(R.string.account_setup_options_mail_check_frequency_15min)), new SpinnerOption(30, getString(R.string.account_setup_options_mail_check_frequency_30min)), new SpinnerOption(60, getString(R.string.account_setup_options_mail_check_frequency_1hour)), new SpinnerOption(120, getString(R.string.account_setup_options_mail_check_frequency_2hour)), new SpinnerOption(Integer.valueOf((int) MessageTopView.PROGRESS_STEP_DURATION), getString(R.string.account_setup_options_mail_check_frequency_3hour)), new SpinnerOption(360, getString(R.string.account_setup_options_mail_check_frequency_6hour)), new SpinnerOption(720, getString(R.string.account_setup_options_mail_check_frequency_12hour)), new SpinnerOption(1440, getString(R.string.account_setup_options_mail_check_frequency_24hour))});
        checkFrequenciesAdapter.setDropDownViewResource(17367049);
        this.mCheckFrequencyView.setAdapter((SpinnerAdapter) checkFrequenciesAdapter);
        ArrayAdapter<SpinnerOption> displayCountsAdapter = new ArrayAdapter<>(this, 17367048, new SpinnerOption[]{new SpinnerOption(10, getString(R.string.account_setup_options_mail_display_count_10)), new SpinnerOption(25, getString(R.string.account_setup_options_mail_display_count_25)), new SpinnerOption(50, getString(R.string.account_setup_options_mail_display_count_50)), new SpinnerOption(100, getString(R.string.account_setup_options_mail_display_count_100)), new SpinnerOption(250, getString(R.string.account_setup_options_mail_display_count_250)), new SpinnerOption(500, getString(R.string.account_setup_options_mail_display_count_500)), new SpinnerOption(1000, getString(R.string.account_setup_options_mail_display_count_1000))});
        displayCountsAdapter.setDropDownViewResource(17367049);
        this.mDisplayCountView.setAdapter((SpinnerAdapter) displayCountsAdapter);
        this.mAccount = Preferences.getPreferences(this).getAccount(getIntent().getStringExtra("account"));
        this.mNotifyView.setChecked(this.mAccount.isNotifyNewMail());
        this.mNotifySyncView.setChecked(this.mAccount.isShowOngoing());
        SpinnerOption.setSpinnerOptionValue(this.mCheckFrequencyView, Integer.valueOf(this.mAccount.getAutomaticCheckIntervalMinutes()));
        SpinnerOption.setSpinnerOptionValue(this.mDisplayCountView, Integer.valueOf(this.mAccount.getDisplayCount()));
        boolean isPushCapable = false;
        try {
            isPushCapable = this.mAccount.getRemoteStore().isPushCapable();
        } catch (Exception e) {
            Log.e("k9", "Could not get remote store", e);
        }
        if (!isPushCapable) {
            this.mPushEnable.setVisibility(8);
        } else {
            this.mPushEnable.setChecked(true);
        }
    }

    private void onDone() {
        this.mAccount.setDescription(this.mAccount.getEmail());
        this.mAccount.setNotifyNewMail(this.mNotifyView.isChecked());
        this.mAccount.setShowOngoing(this.mNotifySyncView.isChecked());
        this.mAccount.setAutomaticCheckIntervalMinutes(((Integer) ((SpinnerOption) this.mCheckFrequencyView.getSelectedItem()).value).intValue());
        this.mAccount.setDisplayCount(((Integer) ((SpinnerOption) this.mDisplayCountView.getSelectedItem()).value).intValue());
        if (this.mPushEnable.isChecked()) {
            this.mAccount.setFolderPushMode(Account.FolderMode.FIRST_CLASS);
        } else {
            this.mAccount.setFolderPushMode(Account.FolderMode.NONE);
        }
        this.mAccount.save(Preferences.getPreferences(this));
        if (this.mAccount.equals(Preferences.getPreferences(this).getDefaultAccount()) || getIntent().getBooleanExtra(EXTRA_MAKE_DEFAULT, false)) {
            Preferences.getPreferences(this).setDefaultAccount(this.mAccount);
        }
        K9.setServicesEnabled(this);
        AccountSetupNames.actionSetNames(this, this.mAccount);
        finish();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.next /*2131755153*/:
                onDone();
                return;
            default:
                return;
        }
    }
}
