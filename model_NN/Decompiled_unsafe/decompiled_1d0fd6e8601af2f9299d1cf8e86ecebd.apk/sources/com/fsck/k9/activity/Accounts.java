package com.fsck.k9.activity;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.fsck.k9.Account;
import com.fsck.k9.AccountStats;
import com.fsck.k9.BaseAccount;
import com.fsck.k9.FontSizes;
import com.fsck.k9.K9;
import com.fsck.k9.Preferences;
import com.fsck.k9.activity.compose.MessageActions;
import com.fsck.k9.activity.misc.ExtendedAsyncTask;
import com.fsck.k9.activity.misc.NonConfigurationInstance;
import com.fsck.k9.activity.setup.AccountSettings;
import com.fsck.k9.activity.setup.AccountSetupBasics;
import com.fsck.k9.activity.setup.Prefs;
import com.fsck.k9.activity.setup.WelcomeMessage;
import com.fsck.k9.controller.MessagingController;
import com.fsck.k9.helper.SizeFormatter;
import com.fsck.k9.mail.AuthType;
import com.fsck.k9.mail.ServerSettings;
import com.fsck.k9.mail.Transport;
import com.fsck.k9.mail.store.RemoteStore;
import com.fsck.k9.mailstore.StorageManager;
import com.fsck.k9.preferences.SettingsExporter;
import com.fsck.k9.preferences.SettingsImportExportException;
import com.fsck.k9.preferences.SettingsImporter;
import com.fsck.k9.search.LocalSearch;
import com.fsck.k9.search.SearchAccount;
import com.fsck.k9.search.SearchSpecification;
import com.fsck.k9.view.ColorChip;
import com.tokenautocomplete.TokenCompleteTextView;
import exts.whats.Constants;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import yahoo.mail.app.R;

public class Accounts extends K9ListActivity implements AdapterView.OnItemClickListener {
    private static String ACCOUNT_STATS = "accountStats";
    public static final String ACTION_IMPORT_SETTINGS = "importSettings";
    private static final int ACTIVITY_REQUEST_PICK_SETTINGS_FILE = 1;
    private static final String ANDROID_MARKET_URL = "https://play.google.com/store/apps/details?id=org.openintents.filemanager";
    private static final int DIALOG_CLEAR_ACCOUNT = 2;
    private static final int DIALOG_NO_FILE_MANAGER = 4;
    private static final int DIALOG_RECREATE_ACCOUNT = 3;
    private static final int DIALOG_REMOVE_ACCOUNT = 1;
    public static final String EXTRA_STARTUP = "startup";
    private static String SELECTED_CONTEXT_ACCOUNT = "selectedContextAccount";
    private static final int SPECIAL_ACCOUNTS_COUNT = 2;
    private static String STATE_UNREAD_COUNT = "unreadCount";
    private static String[][] USED_LIBRARIES = {new String[]{"jutf7", "http://jutf7.sourceforge.net/"}, new String[]{"JZlib", "http://www.jcraft.com/jzlib/"}, new String[]{"Commons IO", "http://commons.apache.org/io/"}, new String[]{"Mime4j", "http://james.apache.org/mime4j/"}, new String[]{"HtmlCleaner", "http://htmlcleaner.sourceforge.net/"}, new String[]{"Android-PullToRefresh", "https://github.com/chrisbanes/Android-PullToRefresh"}, new String[]{"ckChangeLog", "https://github.com/cketti/ckChangeLog"}, new String[]{"HoloColorPicker", "https://github.com/LarsWerkman/HoloColorPicker"}, new String[]{"Glide", "https://github.com/bumptech/glide"}, new String[]{TokenCompleteTextView.TAG, "https://github.com/splitwise/TokenAutoComplete/"}};
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, AccountStats> accountStats = new ConcurrentHashMap<>();
    private List<BaseAccount> accounts = new ArrayList();
    private ActionBar mActionBar;
    /* access modifiers changed from: private */
    public TextView mActionBarSubTitle;
    /* access modifiers changed from: private */
    public TextView mActionBarTitle;
    /* access modifiers changed from: private */
    public TextView mActionBarUnread;
    /* access modifiers changed from: private */
    public AccountsAdapter mAdapter;
    private SearchAccount mAllMessagesAccount = null;
    /* access modifiers changed from: private */
    public FontSizes mFontSizes = K9.getFontSizes();
    /* access modifiers changed from: private */
    public AccountsHandler mHandler = new AccountsHandler();
    ActivityListener mListener = new ActivityListener() {
        public void informUserOfStatus() {
            Accounts.this.mHandler.refreshTitle();
        }

        public void folderStatusChanged(Account account, String folderName, int unreadMessageCount) {
            try {
                AccountStats stats = account.getStats(Accounts.this);
                if (stats == null) {
                    Log.w("k9", "Unable to get account stats");
                } else {
                    accountStatusChanged(account, stats);
                }
            } catch (Exception e) {
                Log.e("k9", "Unable to get account stats", e);
            }
        }

        public void accountStatusChanged(BaseAccount account, AccountStats stats) {
            AccountStats oldStats = (AccountStats) Accounts.this.accountStats.get(account.getUuid());
            int oldUnreadMessageCount = 0;
            if (oldStats != null) {
                oldUnreadMessageCount = oldStats.unreadMessageCount;
            }
            if (stats == null) {
                stats = new AccountStats();
                stats.available = false;
            }
            Accounts.this.accountStats.put(account.getUuid(), stats);
            if (account instanceof Account) {
                int unused = Accounts.this.mUnreadMessageCount = Accounts.this.mUnreadMessageCount + (stats.unreadMessageCount - oldUnreadMessageCount);
            }
            Accounts.this.mHandler.dataChanged();
            Accounts.this.pendingWork.remove(account);
            if (Accounts.this.pendingWork.isEmpty()) {
                Accounts.this.mHandler.progress(10000);
                Accounts.this.mHandler.refreshTitle();
                return;
            }
            Accounts.this.mHandler.progress((10000 / Accounts.this.mAdapter.getCount()) * (Accounts.this.mAdapter.getCount() - Accounts.this.pendingWork.size()));
        }

        public void accountSizeChanged(Account account, long oldSize, long newSize) {
            Accounts.this.mHandler.accountSizeChanged(account, oldSize, newSize);
        }

        public void synchronizeMailboxFinished(Account account, String folder, int totalMessagesInMailbox, int numNewMessages) {
            MessagingController.getInstance(Accounts.this.getApplication()).getAccountStats(Accounts.this, account, Accounts.this.mListener);
            super.synchronizeMailboxFinished(account, folder, totalMessagesInMailbox, numNewMessages);
            Accounts.this.mHandler.progress(false);
        }

        public void synchronizeMailboxStarted(Account account, String folder) {
            super.synchronizeMailboxStarted(account, folder);
            Accounts.this.mHandler.progress(true);
        }

        public void synchronizeMailboxFailed(Account account, String folder, String message) {
            super.synchronizeMailboxFailed(account, folder, message);
            Accounts.this.mHandler.progress(false);
        }
    };
    private NonConfigurationInstance mNonConfigurationInstance;
    /* access modifiers changed from: private */
    public MenuItem mRefreshMenuItem;
    /* access modifiers changed from: private */
    public BaseAccount mSelectedContextAccount;
    private SearchAccount mUnifiedInboxAccount = null;
    /* access modifiers changed from: private */
    public int mUnreadMessageCount = 0;
    /* access modifiers changed from: private */
    public ConcurrentMap<BaseAccount, String> pendingWork = new ConcurrentHashMap();
    private StorageManager.StorageListener storageListener = new StorageManager.StorageListener() {
        public void onUnmount(String providerId) {
            Accounts.this.refresh();
        }

        public void onMount(String providerId) {
            Accounts.this.refresh();
        }
    };

    private enum ACCOUNT_LOCATION {
        TOP,
        MIDDLE,
        BOTTOM
    }

    class AccountsHandler extends Handler {
        AccountsHandler() {
        }

        public void refreshTitle() {
            Accounts.this.runOnUiThread(new Runnable() {
                public void run() {
                    AccountsHandler.this.setViewTitle();
                }
            });
        }

        /* access modifiers changed from: private */
        public void setViewTitle() {
            Accounts.this.mActionBarTitle.setText(Accounts.this.getString(R.string.accounts_title));
            if (Accounts.this.mUnreadMessageCount == 0) {
                Accounts.this.mActionBarUnread.setVisibility(8);
            } else {
                Accounts.this.mActionBarUnread.setText(String.format("%d", Integer.valueOf(Accounts.this.mUnreadMessageCount)));
                Accounts.this.mActionBarUnread.setVisibility(0);
            }
            String operation = Accounts.this.mListener.getOperation(Accounts.this).trim();
            if (operation.length() < 1) {
                Accounts.this.mActionBarSubTitle.setVisibility(8);
                return;
            }
            Accounts.this.mActionBarSubTitle.setVisibility(0);
            Accounts.this.mActionBarSubTitle.setText(operation);
        }

        public void dataChanged() {
            Accounts.this.runOnUiThread(new Runnable() {
                public void run() {
                    if (Accounts.this.mAdapter != null) {
                        Accounts.this.mAdapter.notifyDataSetChanged();
                    }
                }
            });
        }

        public void workingAccount(final Account account, final int res) {
            Accounts.this.runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(Accounts.this.getApplication(), Accounts.this.getString(res, new Object[]{account.getDescription()}), 0).show();
                }
            });
        }

        public void accountSizeChanged(Account account, long oldSize, long newSize) {
            final Account account2 = account;
            final long j = newSize;
            final long j2 = oldSize;
            Accounts.this.runOnUiThread(new Runnable() {
                public void run() {
                    AccountStats stats = (AccountStats) Accounts.this.accountStats.get(account2.getUuid());
                    if (!(j == -1 || stats == null || !K9.measureAccounts())) {
                        stats.size = j;
                    }
                    Toast.makeText(Accounts.this.getApplication(), Accounts.this.getString(R.string.account_size_changed, new Object[]{account2.getDescription(), SizeFormatter.formatSize(Accounts.this.getApplication(), j2), SizeFormatter.formatSize(Accounts.this.getApplication(), j)}), 1).show();
                    if (Accounts.this.mAdapter != null) {
                        Accounts.this.mAdapter.notifyDataSetChanged();
                    }
                }
            });
        }

        public void progress(final boolean progress) {
            if (Accounts.this.mRefreshMenuItem != null) {
                Accounts.this.runOnUiThread(new Runnable() {
                    public void run() {
                        if (progress) {
                            Accounts.this.mRefreshMenuItem.setActionView((int) R.layout.actionbar_indeterminate_progress_actionview);
                        } else {
                            Accounts.this.mRefreshMenuItem.setActionView((View) null);
                        }
                    }
                });
            }
        }

        public void progress(final int progress) {
            Accounts.this.runOnUiThread(new Runnable() {
                public void run() {
                    Accounts.this.getWindow().setFeatureInt(2, progress);
                }
            });
        }
    }

    public void setProgress(boolean progress) {
        this.mHandler.progress(progress);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static void listAccounts(Context context) {
        Intent intent = new Intent(context, Accounts.class);
        intent.addFlags(872448000);
        intent.putExtra(EXTRA_STARTUP, false);
        context.startActivity(intent);
    }

    public static void importSettings(Context context) {
        Intent intent = new Intent(context, Accounts.class);
        intent.setAction(ACTION_IMPORT_SETTINGS);
        context.startActivity(intent);
    }

    public static LocalSearch createUnreadSearch(Context context, BaseAccount account) {
        LocalSearch search;
        String searchTitle = context.getString(R.string.search_title, account.getDescription(), context.getString(R.string.unread_modifier));
        if (account instanceof SearchAccount) {
            search = ((SearchAccount) account).getRelatedSearch().clone();
            search.setName(searchTitle);
        } else {
            search = new LocalSearch(searchTitle);
            search.addAccountUuid(account.getUuid());
            Account realAccount = (Account) account;
            realAccount.excludeSpecialFolders(search);
            realAccount.limitToDisplayableFolders(search);
        }
        search.and(SearchSpecification.SearchField.READ, Constants.INSTALL_ID, SearchSpecification.Attribute.NOT_EQUALS);
        return search;
    }

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().addFlags(Integer.MIN_VALUE);
            getWindow().setStatusBarColor(getResources().getColor(R.color.actionbar));
        }
        if (!K9.isHideSpecialAccounts()) {
            createSpecialAccounts();
        }
        List<Account> accounts2 = Preferences.getPreferences(this).getAccounts();
        Intent intent = getIntent();
        if (ACTION_IMPORT_SETTINGS.equals(intent.getAction())) {
            onImport();
        } else if (accounts2.size() < 1) {
            WelcomeMessage.showWelcomeMessage(this);
            finish();
            return;
        }
        if (UpgradeDatabases.actionUpgradeDatabases(this, intent)) {
            finish();
            return;
        }
        boolean startup = intent.getBooleanExtra(EXTRA_STARTUP, true);
        if (startup && K9.startIntegratedInbox() && !K9.isHideSpecialAccounts()) {
            onOpenAccount(this.mUnifiedInboxAccount);
            finish();
        } else if (!startup || accounts2.size() != 1 || !onOpenAccount(accounts2.get(0))) {
            this.mActionBar = getActionBar();
            initializeActionBar();
            setContentView((int) R.layout.accounts);
            ListView listView = getListView();
            listView.setOnItemClickListener(this);
            listView.setItemsCanFocus(false);
            listView.setScrollingCacheEnabled(false);
            registerForContextMenu(listView);
            if (icicle != null && icicle.containsKey(SELECTED_CONTEXT_ACCOUNT)) {
                this.mSelectedContextAccount = Preferences.getPreferences(this).getAccount(icicle.getString("selectedContextAccount"));
            }
            restoreAccountStats(icicle);
            this.mHandler.setViewTitle();
            this.mNonConfigurationInstance = (NonConfigurationInstance) getLastNonConfigurationInstance();
            if (this.mNonConfigurationInstance != null) {
                this.mNonConfigurationInstance.restore(this);
            }
        } else {
            finish();
        }
    }

    private void initializeActionBar() {
        this.mActionBar.setDisplayShowCustomEnabled(true);
        this.mActionBar.setCustomView((int) R.layout.actionbar_custom);
        View customView = this.mActionBar.getCustomView();
        this.mActionBarTitle = (TextView) customView.findViewById(R.id.actionbar_title_first);
        this.mActionBarSubTitle = (TextView) customView.findViewById(R.id.actionbar_title_sub);
        this.mActionBarUnread = (TextView) customView.findViewById(R.id.actionbar_unread_count);
        this.mActionBar.setDisplayHomeAsUpEnabled(false);
        this.mActionBar.setDisplayShowHomeEnabled(false);
        this.mActionBar.setDisplayShowTitleEnabled(false);
        ((LinearLayout.LayoutParams) customView.findViewById(R.id.title_layout).getLayoutParams()).leftMargin = 35;
    }

    private void createSpecialAccounts() {
        this.mUnifiedInboxAccount = SearchAccount.createUnifiedInboxAccount(this);
        this.mAllMessagesAccount = SearchAccount.createAllMessagesAccount(this);
    }

    private void restoreAccountStats(Bundle icicle) {
        if (icicle != null) {
            Map<String, AccountStats> oldStats = (Map) icicle.get(ACCOUNT_STATS);
            if (oldStats != null) {
                this.accountStats.putAll(oldStats);
            }
            this.mUnreadMessageCount = icicle.getInt(STATE_UNREAD_COUNT);
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (this.mSelectedContextAccount != null) {
            outState.putString(SELECTED_CONTEXT_ACCOUNT, this.mSelectedContextAccount.getUuid());
        }
        outState.putSerializable(STATE_UNREAD_COUNT, Integer.valueOf(this.mUnreadMessageCount));
        outState.putSerializable(ACCOUNT_STATS, this.accountStats);
    }

    public void onResume() {
        super.onResume();
        refresh();
        MessagingController.getInstance(getApplication()).addListener(this.mListener);
        StorageManager.getInstance(getApplication()).addListener(this.storageListener);
        this.mListener.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MessagingController.getInstance(getApplication()).removeListener(this.mListener);
        StorageManager.getInstance(getApplication()).removeListener(this.storageListener);
        this.mListener.onPause(this);
    }

    public Object onRetainNonConfigurationInstance() {
        if (this.mNonConfigurationInstance == null || !this.mNonConfigurationInstance.retain()) {
            return null;
        }
        return this.mNonConfigurationInstance;
    }

    private EnumSet<ACCOUNT_LOCATION> accountLocation(BaseAccount account) {
        EnumSet<ACCOUNT_LOCATION> accountLocation = EnumSet.of(ACCOUNT_LOCATION.MIDDLE);
        if (this.accounts.size() > 0) {
            if (this.accounts.get(0).equals(account)) {
                accountLocation.remove(ACCOUNT_LOCATION.MIDDLE);
                accountLocation.add(ACCOUNT_LOCATION.TOP);
            }
            if (this.accounts.get(this.accounts.size() - 1).equals(account)) {
                accountLocation.remove(ACCOUNT_LOCATION.MIDDLE);
                accountLocation.add(ACCOUNT_LOCATION.BOTTOM);
            }
        }
        return accountLocation;
    }

    /* access modifiers changed from: private */
    public void refresh() {
        List<BaseAccount> newAccounts;
        this.accounts.clear();
        this.accounts.addAll(Preferences.getPreferences(this).getAccounts());
        if (K9.isHideSpecialAccounts() || this.accounts.size() <= 0) {
            newAccounts = new ArrayList<>(this.accounts.size());
        } else {
            if (this.mUnifiedInboxAccount == null || this.mAllMessagesAccount == null) {
                createSpecialAccounts();
            }
            newAccounts = new ArrayList<>(this.accounts.size() + 2);
            newAccounts.add(this.mUnifiedInboxAccount);
            newAccounts.add(this.mAllMessagesAccount);
        }
        newAccounts.addAll(this.accounts);
        this.mAdapter = new AccountsAdapter(newAccounts);
        getListView().setAdapter((ListAdapter) this.mAdapter);
        this.pendingWork.clear();
        this.mHandler.refreshTitle();
        MessagingController controller = MessagingController.getInstance(getApplication());
        for (BaseAccount account : newAccounts) {
            this.pendingWork.put(account, "true");
            if (account instanceof Account) {
                controller.getAccountStats(this, (Account) account, this.mListener);
            } else if (K9.countSearchMessages() && (account instanceof SearchAccount)) {
                controller.getSearchAccountStats((SearchAccount) account, this.mListener);
            }
        }
    }

    private void onAddNewAccount() {
        AccountSetupBasics.actionNewAccount(this);
    }

    private void onEditPrefs() {
        Prefs.actionPrefs(this);
    }

    private void onCheckMail(Account account) {
        MessagingController.getInstance(getApplication()).checkMail(this, account, true, true, null);
        if (account == null) {
            MessagingController.getInstance(getApplication()).sendPendingMessages(null);
        } else {
            MessagingController.getInstance(getApplication()).sendPendingMessages(account, null);
        }
    }

    private void onClearCommands(Account account) {
        MessagingController.getInstance(getApplication()).clearAllPending(account);
    }

    private void onEmptyTrash(Account account) {
        MessagingController.getInstance(getApplication()).emptyTrash(account, null);
    }

    private void onCompose() {
        Account defaultAccount = Preferences.getPreferences(this).getDefaultAccount();
        if (defaultAccount != null) {
            MessageActions.actionCompose(this, defaultAccount);
        } else {
            onAddNewAccount();
        }
    }

    private boolean onOpenAccount(BaseAccount account) {
        if (account instanceof SearchAccount) {
            MessageList.actionDisplaySearch(this, ((SearchAccount) account).getRelatedSearch(), false, false);
        } else {
            Account realAccount = (Account) account;
            if (!realAccount.isEnabled()) {
                onActivateAccount(realAccount);
                return false;
            } else if (!realAccount.isAvailable(this)) {
                Toast.makeText(getApplication(), getString(R.string.account_unavailable, new Object[]{account.getDescription()}), 0).show();
                Log.i("k9", "refusing to open account that is not available");
                return false;
            } else if (K9.FOLDER_NONE.equals(realAccount.getAutoExpandFolderName())) {
                FolderList.actionHandleAccount(this, realAccount);
            } else {
                LocalSearch search = new LocalSearch(realAccount.getAutoExpandFolderName());
                search.addAllowedFolder(realAccount.getAutoExpandFolderName());
                search.addAccountUuid(realAccount.getUuid());
                MessageList.actionDisplaySearch(this, search, false, true);
            }
        }
        return true;
    }

    private void onActivateAccount(Account account) {
        List<Account> disabledAccounts = new ArrayList<>();
        disabledAccounts.add(account);
        promptForServerPasswords(disabledAccounts);
    }

    /* access modifiers changed from: private */
    public void promptForServerPasswords(List<Account> disabledAccounts) {
        PasswordPromptDialog dialog = new PasswordPromptDialog(disabledAccounts.remove(0), disabledAccounts);
        setNonConfigurationInstance(dialog);
        dialog.show(this);
    }

    private static class PasswordPromptDialog implements NonConfigurationInstance, TextWatcher {
        /* access modifiers changed from: private */
        public Account mAccount;
        private AlertDialog mDialog;
        private String mIncomingPassword;
        /* access modifiers changed from: private */
        public EditText mIncomingPasswordView;
        private String mOutgoingPassword;
        /* access modifiers changed from: private */
        public EditText mOutgoingPasswordView;
        /* access modifiers changed from: private */
        public List<Account> mRemainingAccounts;
        private boolean mUseIncoming;
        /* access modifiers changed from: private */
        public CheckBox mUseIncomingView;

        PasswordPromptDialog(Account account, List<Account> accounts) {
            this.mAccount = account;
            this.mRemainingAccounts = accounts;
        }

        public void restore(Activity activity) {
            show((Accounts) activity, true);
        }

        public boolean retain() {
            if (this.mDialog == null) {
                return false;
            }
            if (this.mIncomingPasswordView != null) {
                this.mIncomingPassword = this.mIncomingPasswordView.getText().toString();
            }
            if (this.mOutgoingPasswordView != null) {
                this.mOutgoingPassword = this.mOutgoingPasswordView.getText().toString();
                this.mUseIncoming = this.mUseIncomingView.isChecked();
            }
            this.mDialog.dismiss();
            this.mDialog = null;
            this.mIncomingPasswordView = null;
            this.mOutgoingPasswordView = null;
            this.mUseIncomingView = null;
            return true;
        }

        public void show(Accounts activity) {
            show(activity, false);
        }

        private void show(Accounts activity, boolean restore) {
            ServerSettings incoming = RemoteStore.decodeStoreUri(this.mAccount.getStoreUri());
            ServerSettings outgoing = Transport.decodeTransportUri(this.mAccount.getTransportUri());
            boolean configureOutgoingServer = (AuthType.EXTERNAL == outgoing.authenticationType || AuthType.XOAUTH2 == outgoing.authenticationType || ServerSettings.Type.WebDAV == outgoing.type || outgoing.username == null || outgoing.username.isEmpty() || (outgoing.password != null && !outgoing.password.isEmpty())) ? false : true;
            boolean configureIncomingServer = (AuthType.EXTERNAL == incoming.authenticationType || AuthType.XOAUTH2 == incoming.authenticationType || (incoming.password != null && !incoming.password.isEmpty())) ? false : true;
            ScrollView scrollView = new ScrollView(activity);
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setTitle(activity.getString(R.string.settings_import_activate_account_header));
            builder.setView(scrollView);
            final Accounts accounts = activity;
            builder.setPositiveButton(activity.getString(R.string.okay_action), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    String incomingPassword = null;
                    if (PasswordPromptDialog.this.mIncomingPasswordView != null) {
                        incomingPassword = PasswordPromptDialog.this.mIncomingPasswordView.getText().toString();
                    }
                    String outgoingPassword = null;
                    if (PasswordPromptDialog.this.mOutgoingPasswordView != null) {
                        if (PasswordPromptDialog.this.mUseIncomingView.isChecked()) {
                            outgoingPassword = incomingPassword;
                        } else {
                            outgoingPassword = PasswordPromptDialog.this.mOutgoingPasswordView.getText().toString();
                        }
                    }
                    dialog.dismiss();
                    SetPasswordsAsyncTask asyncTask = new SetPasswordsAsyncTask(accounts, PasswordPromptDialog.this.mAccount, incomingPassword, outgoingPassword, PasswordPromptDialog.this.mRemainingAccounts);
                    accounts.setNonConfigurationInstance(asyncTask);
                    asyncTask.execute(new Void[0]);
                }
            });
            final Accounts accounts2 = activity;
            builder.setNegativeButton(activity.getString(R.string.cancel_action), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    accounts2.setNonConfigurationInstance(null);
                }
            });
            this.mDialog = builder.create();
            View layout = this.mDialog.getLayoutInflater().inflate((int) R.layout.accounts_password_prompt, scrollView);
            ((TextView) layout.findViewById(R.id.password_prompt_intro)).setText(activity.getString(R.string.settings_import_activate_account_intro, new Object[]{this.mAccount.getDescription(), activity.getResources().getQuantityString(R.plurals.settings_import_server_passwords, (!configureIncomingServer || !configureOutgoingServer) ? 1 : 2)}));
            if (configureIncomingServer) {
                ((TextView) layout.findViewById(R.id.password_prompt_incoming_server)).setText(activity.getString(R.string.settings_import_incoming_server, new Object[]{incoming.host}));
                this.mIncomingPasswordView = (EditText) layout.findViewById(R.id.incoming_server_password);
                this.mIncomingPasswordView.addTextChangedListener(this);
            } else {
                layout.findViewById(R.id.incoming_server_prompt).setVisibility(8);
            }
            if (configureOutgoingServer) {
                ((TextView) layout.findViewById(R.id.password_prompt_outgoing_server)).setText(activity.getString(R.string.settings_import_outgoing_server, new Object[]{outgoing.host}));
                this.mOutgoingPasswordView = (EditText) layout.findViewById(R.id.outgoing_server_password);
                this.mOutgoingPasswordView.addTextChangedListener(this);
                this.mUseIncomingView = (CheckBox) layout.findViewById(R.id.use_incoming_server_password);
                if (configureIncomingServer) {
                    this.mUseIncomingView.setChecked(true);
                    this.mUseIncomingView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            if (isChecked) {
                                PasswordPromptDialog.this.mOutgoingPasswordView.setText((CharSequence) null);
                                PasswordPromptDialog.this.mOutgoingPasswordView.setEnabled(false);
                                return;
                            }
                            PasswordPromptDialog.this.mOutgoingPasswordView.setText(PasswordPromptDialog.this.mIncomingPasswordView.getText());
                            PasswordPromptDialog.this.mOutgoingPasswordView.setEnabled(true);
                        }
                    });
                } else {
                    this.mUseIncomingView.setChecked(false);
                    this.mUseIncomingView.setVisibility(8);
                    this.mOutgoingPasswordView.setEnabled(true);
                }
            } else {
                layout.findViewById(R.id.outgoing_server_prompt).setVisibility(8);
            }
            this.mDialog.show();
            if (restore) {
                if (configureIncomingServer) {
                    this.mIncomingPasswordView.setText(this.mIncomingPassword);
                }
                if (configureOutgoingServer) {
                    this.mOutgoingPasswordView.setText(this.mOutgoingPassword);
                    this.mUseIncomingView.setChecked(this.mUseIncoming);
                }
            } else if (configureIncomingServer) {
                this.mIncomingPasswordView.setText(this.mIncomingPasswordView.getText());
            } else {
                this.mOutgoingPasswordView.setText(this.mOutgoingPasswordView.getText());
            }
        }

        public void afterTextChanged(Editable arg0) {
            boolean enable = false;
            if (this.mIncomingPasswordView == null) {
                enable = this.mOutgoingPasswordView.getText().length() > 0;
            } else if (this.mIncomingPasswordView.getText().length() > 0) {
                if (this.mOutgoingPasswordView == null) {
                    enable = true;
                } else if (this.mUseIncomingView.isChecked() || this.mOutgoingPasswordView.getText().length() > 0) {
                    enable = true;
                }
            }
            this.mDialog.getButton(-1).setEnabled(enable);
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    }

    private static class SetPasswordsAsyncTask extends ExtendedAsyncTask<Void, Void, Void> {
        private Account mAccount;
        private Application mApplication = this.mActivity.getApplication();
        private String mIncomingPassword;
        private String mOutgoingPassword;
        private List<Account> mRemainingAccounts;

        protected SetPasswordsAsyncTask(Activity activity, Account account, String incomingPassword, String outgoingPassword, List<Account> remainingAccounts) {
            super(activity);
            this.mAccount = account;
            this.mIncomingPassword = incomingPassword;
            this.mOutgoingPassword = outgoingPassword;
            this.mRemainingAccounts = remainingAccounts;
        }

        /* access modifiers changed from: protected */
        public void showProgressDialog() {
            this.mProgressDialog = ProgressDialog.show(this.mActivity, this.mActivity.getString(R.string.settings_import_activate_account_header), this.mActivity.getResources().getQuantityString(R.plurals.settings_import_setting_passwords, this.mOutgoingPassword == null ? 1 : 2), true);
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... params) {
            try {
                if (this.mIncomingPassword != null) {
                    this.mAccount.setStoreUri(RemoteStore.createStoreUri(RemoteStore.decodeStoreUri(this.mAccount.getStoreUri()).newPassword(this.mIncomingPassword)));
                }
                if (this.mOutgoingPassword != null) {
                    this.mAccount.setTransportUri(Transport.createTransportUri(Transport.decodeTransportUri(this.mAccount.getTransportUri()).newPassword(this.mOutgoingPassword)));
                }
                this.mAccount.setEnabled(true);
                this.mAccount.save(Preferences.getPreferences(this.mContext));
                K9.setServicesEnabled(this.mContext);
                MessagingController.getInstance(this.mApplication).listFolders(this.mAccount, true, null);
            } catch (Exception e) {
                Log.e("k9", "Something went while setting account passwords", e);
            }
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void result) {
            Accounts activity = (Accounts) this.mActivity;
            activity.setNonConfigurationInstance(null);
            activity.refresh();
            removeProgressDialog();
            if (this.mRemainingAccounts.size() > 0) {
                activity.promptForServerPasswords(this.mRemainingAccounts);
            }
        }
    }

    private void onDeleteAccount(Account account) {
        this.mSelectedContextAccount = account;
        showDialog(1);
    }

    private void onEditAccount(Account account) {
        AccountSettings.actionSettings(this, account);
    }

    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                if (this.mSelectedContextAccount == null) {
                    return null;
                }
                return ConfirmationDialog.create(this, id, (int) R.string.account_delete_dlg_title, getString(R.string.account_delete_dlg_instructions_fmt, new Object[]{this.mSelectedContextAccount.getDescription()}), (int) R.string.okay_action, (int) R.string.cancel_action, new Runnable() {
                    public void run() {
                        if (Accounts.this.mSelectedContextAccount instanceof Account) {
                            Account realAccount = (Account) Accounts.this.mSelectedContextAccount;
                            try {
                                realAccount.getLocalStore().delete();
                            } catch (Exception e) {
                            }
                            MessagingController.getInstance(Accounts.this.getApplication()).deleteAccount(realAccount);
                            Preferences.getPreferences(Accounts.this).deleteAccount(realAccount);
                            K9.setServicesEnabled(Accounts.this);
                            Accounts.this.refresh();
                        }
                    }
                });
            case 2:
                if (this.mSelectedContextAccount == null) {
                    return null;
                }
                return ConfirmationDialog.create(this, id, (int) R.string.account_clear_dlg_title, getString(R.string.account_clear_dlg_instructions_fmt, new Object[]{this.mSelectedContextAccount.getDescription()}), (int) R.string.okay_action, (int) R.string.cancel_action, new Runnable() {
                    public void run() {
                        if (Accounts.this.mSelectedContextAccount instanceof Account) {
                            Account realAccount = (Account) Accounts.this.mSelectedContextAccount;
                            Accounts.this.mHandler.workingAccount(realAccount, R.string.clearing_account);
                            MessagingController.getInstance(Accounts.this.getApplication()).clear(realAccount, null);
                        }
                    }
                });
            case 3:
                if (this.mSelectedContextAccount == null) {
                    return null;
                }
                return ConfirmationDialog.create(this, id, (int) R.string.account_recreate_dlg_title, getString(R.string.account_recreate_dlg_instructions_fmt, new Object[]{this.mSelectedContextAccount.getDescription()}), (int) R.string.okay_action, (int) R.string.cancel_action, new Runnable() {
                    public void run() {
                        if (Accounts.this.mSelectedContextAccount instanceof Account) {
                            Account realAccount = (Account) Accounts.this.mSelectedContextAccount;
                            Accounts.this.mHandler.workingAccount(realAccount, R.string.recreating_account);
                            MessagingController.getInstance(Accounts.this.getApplication()).recreate(realAccount, null);
                        }
                    }
                });
            case 4:
                return ConfirmationDialog.create(this, id, (int) R.string.import_dialog_error_title, getString(R.string.import_dialog_error_message), (int) R.string.open_market, (int) R.string.close, new Runnable() {
                    public void run() {
                        Accounts.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(Accounts.ANDROID_MARKET_URL)));
                    }
                });
            default:
                return super.onCreateDialog(id);
        }
    }

    public void onPrepareDialog(int id, Dialog d) {
        AlertDialog alert = (AlertDialog) d;
        switch (id) {
            case 1:
                alert.setMessage(getString(R.string.account_delete_dlg_instructions_fmt, new Object[]{this.mSelectedContextAccount.getDescription()}));
                break;
            case 2:
                alert.setMessage(getString(R.string.account_clear_dlg_instructions_fmt, new Object[]{this.mSelectedContextAccount.getDescription()}));
                break;
            case 3:
                alert.setMessage(getString(R.string.account_recreate_dlg_instructions_fmt, new Object[]{this.mSelectedContextAccount.getDescription()}));
                break;
        }
        super.onPrepareDialog(id, d);
    }

    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo menuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        if (menuInfo != null) {
            this.mSelectedContextAccount = (BaseAccount) getListView().getItemAtPosition(menuInfo.position);
        }
        if (this.mSelectedContextAccount instanceof Account) {
            Account realAccount = (Account) this.mSelectedContextAccount;
            switch (item.getItemId()) {
                case R.id.move_up /*2131755436*/:
                    onMove(realAccount, true);
                    break;
                case R.id.move_down /*2131755437*/:
                    onMove(realAccount, false);
                    break;
                case R.id.empty_trash /*2131755438*/:
                    onEmptyTrash(realAccount);
                    break;
                case R.id.account_settings /*2131755439*/:
                    onEditAccount(realAccount);
                    break;
                case R.id.delete_account /*2131755440*/:
                    onDeleteAccount(realAccount);
                    break;
                case R.id.clear /*2131755441*/:
                    onClear(realAccount);
                    break;
                case R.id.recreate /*2131755442*/:
                    onRecreate(realAccount);
                    break;
                case R.id.clear_pending /*2131755443*/:
                    onClearCommands(realAccount);
                    break;
                case R.id.export /*2131755444*/:
                    onExport(false, realAccount);
                    break;
                case R.id.activate /*2131755452*/:
                    onActivateAccount(realAccount);
                    break;
            }
        }
        return true;
    }

    private void onClear(Account account) {
        showDialog(2);
    }

    private void onRecreate(Account account) {
        showDialog(3);
    }

    private void onMove(Account account, boolean up) {
        MoveAccountAsyncTask asyncTask = new MoveAccountAsyncTask(this, account, up);
        setNonConfigurationInstance(asyncTask);
        asyncTask.execute(new Void[0]);
    }

    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        onOpenAccount((BaseAccount) parent.getItemAtPosition(position));
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.import_settings /*2131755435*/:
                onImport();
                return true;
            case R.id.search /*2131755445*/:
                onSearchRequested();
                return true;
            case R.id.check_mail /*2131755446*/:
                onCheckMail(null);
                return true;
            case R.id.compose /*2131755447*/:
                onCompose();
                return true;
            case R.id.add_new_account /*2131755448*/:
                onAddNewAccount();
                return true;
            case R.id.export_all /*2131755450*/:
                onExport(true, null);
                return true;
            case R.id.edit_prefs /*2131755451*/:
                onEditPrefs();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void onAbout() {
        String appName = getString(R.string.app_name);
        int year = Calendar.getInstance().get(1);
        WebView wv = new WebView(this);
        StringBuilder html = new StringBuilder().append("<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />").append("<img src=\"file:///android_asset/icon.png\" alt=\"").append(appName).append("\"/>").append("<h1>").append(String.format(getString(R.string.about_title_fmt), "<a href=\"" + getString(R.string.app_webpage_url)) + "\">").append(appName).append("</a>").append("</h1><p>").append(appName).append(" ").append(String.format(getString(R.string.debug_version_fmt), getVersionNumber())).append("</p><p>").append(String.format(getString(R.string.app_authors_fmt), getString(R.string.app_authors))).append("</p><p>").append(String.format(getString(R.string.app_revision_fmt), "<a href=\"" + getString(R.string.app_revision_url) + "\">" + getString(R.string.app_revision_url) + "</a>")).append("</p><hr/><p>").append(String.format(getString(R.string.app_copyright_fmt), Integer.valueOf(year), Integer.valueOf(year))).append("</p><hr/><p>").append(getString(R.string.app_license)).append("</p><hr/><p>");
        StringBuilder libs = new StringBuilder().append("<ul>");
        for (String[] library : USED_LIBRARIES) {
            libs.append("<li><a href=\"").append(library[1]).append("\">").append(library[0]).append("</a></li>");
        }
        libs.append("</ul>");
        html.append(String.format(getString(R.string.app_libraries), libs.toString())).append("</p><hr/><p>").append(String.format(getString(R.string.app_emoji_icons), "<div>TypePad 絵文字アイコン画像 (<a href=\"http://typepad.jp/\">Six Apart Ltd</a>) / <a href=\"http://creativecommons.org/licenses/by/2.1/jp/\">CC BY 2.1</a></div>")).append("</p><hr/><p>").append(getString(R.string.app_htmlcleaner_license));
        wv.loadDataWithBaseURL("file:///android_res/drawable/", html.toString(), "text/html", "utf-8", null);
        new AlertDialog.Builder(this).setView(wv).setCancelable(true).setPositiveButton((int) R.string.okay_action, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface d, int c) {
                d.dismiss();
            }
        }).setNeutralButton((int) R.string.changelog_full_title, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface d, int c) {
            }
        }).show();
    }

    private String getVersionNumber() {
        try {
            return getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            return "?";
        }
    }

    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
        return true;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.accounts_option, menu);
        this.mRefreshMenuItem = menu.findItem(R.id.check_mail);
        return true;
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle((int) R.string.accounts_context_menu_title);
        BaseAccount account = (BaseAccount) this.mAdapter.getItem(((AdapterView.AdapterContextMenuInfo) menuInfo).position);
        if (!(account instanceof Account) || ((Account) account).isEnabled()) {
            getMenuInflater().inflate(R.menu.accounts_context, menu);
        } else {
            getMenuInflater().inflate(R.menu.disabled_accounts_context, menu);
        }
        if (account instanceof SearchAccount) {
            for (int i = 0; i < menu.size(); i++) {
                menu.getItem(i).setVisible(false);
            }
            return;
        }
        EnumSet<ACCOUNT_LOCATION> accountLocation = accountLocation(account);
        if (accountLocation.contains(ACCOUNT_LOCATION.TOP)) {
            menu.findItem(R.id.move_up).setEnabled(false);
        } else {
            menu.findItem(R.id.move_up).setEnabled(true);
        }
        if (accountLocation.contains(ACCOUNT_LOCATION.BOTTOM)) {
            menu.findItem(R.id.move_down).setEnabled(false);
        } else {
            menu.findItem(R.id.move_down).setEnabled(true);
        }
    }

    private void onImport() {
        Intent i = new Intent("android.intent.action.GET_CONTENT");
        i.addCategory("android.intent.category.OPENABLE");
        i.setType("*/*");
        if (getPackageManager().queryIntentActivities(i, 0).size() > 0) {
            startActivityForResult(Intent.createChooser(i, null), 1);
        } else {
            showDialog(4);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i("k9", "onActivityResult requestCode = " + requestCode + ", resultCode = " + resultCode + ", data = " + data);
        if (resultCode == -1 && data != null) {
            switch (requestCode) {
                case 1:
                    onImport(data.getData());
                    return;
                default:
                    return;
            }
        }
    }

    private void onImport(Uri uri) {
        ListImportContentsAsyncTask asyncTask = new ListImportContentsAsyncTask(uri);
        setNonConfigurationInstance(asyncTask);
        asyncTask.execute(new Void[0]);
    }

    /* access modifiers changed from: private */
    public void showSimpleDialog(int headerRes, int messageRes, Object... args) {
        SimpleDialog dialog = new SimpleDialog(headerRes, messageRes, args);
        dialog.show(this);
        setNonConfigurationInstance(dialog);
    }

    private static class SimpleDialog implements NonConfigurationInstance {
        private Object[] mArguments;
        private Dialog mDialog;
        private final int mHeaderRes;
        private final int mMessageRes;

        SimpleDialog(int headerRes, int messageRes, Object... args) {
            this.mHeaderRes = headerRes;
            this.mMessageRes = messageRes;
            this.mArguments = args;
        }

        public void restore(Activity activity) {
            show((Accounts) activity);
        }

        public boolean retain() {
            if (this.mDialog == null) {
                return false;
            }
            this.mDialog.dismiss();
            this.mDialog = null;
            return true;
        }

        public void show(final Accounts activity) {
            String message = generateMessage(activity);
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setTitle(this.mHeaderRes);
            builder.setMessage(message);
            builder.setPositiveButton((int) R.string.okay_action, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    activity.setNonConfigurationInstance(null);
                    SimpleDialog.this.okayAction(activity);
                }
            });
            this.mDialog = builder.show();
        }

        /* access modifiers changed from: protected */
        public String generateMessage(Accounts activity) {
            return activity.getString(this.mMessageRes, this.mArguments);
        }

        /* access modifiers changed from: protected */
        public void okayAction(Accounts activity) {
        }
    }

    /* access modifiers changed from: private */
    public void showAccountsImportedDialog(SettingsImporter.ImportResults importResults, String filename) {
        AccountsImportedDialog dialog = new AccountsImportedDialog(importResults, filename);
        dialog.show(this);
        setNonConfigurationInstance(dialog);
    }

    private static class AccountsImportedDialog extends SimpleDialog {
        private String mFilename;
        private SettingsImporter.ImportResults mImportResults;

        AccountsImportedDialog(SettingsImporter.ImportResults importResults, String filename) {
            super(R.string.settings_import_success_header, R.string.settings_import_success, new Object[0]);
            this.mImportResults = importResults;
            this.mFilename = filename;
        }

        /* access modifiers changed from: protected */
        public String generateMessage(Accounts activity) {
            int imported = this.mImportResults.importedAccounts.size();
            return activity.getString(R.string.settings_import_success, new Object[]{activity.getResources().getQuantityString(R.plurals.settings_import_accounts, imported, Integer.valueOf(imported)), this.mFilename});
        }

        /* access modifiers changed from: protected */
        public void okayAction(Accounts activity) {
            Preferences preferences = Preferences.getPreferences(activity.getApplicationContext());
            List<Account> disabledAccounts = new ArrayList<>();
            for (SettingsImporter.AccountDescriptionPair accountPair : this.mImportResults.importedAccounts) {
                Account account = preferences.getAccount(accountPair.imported.uuid);
                if (account != null && !account.isEnabled()) {
                    disabledAccounts.add(account);
                }
            }
            if (disabledAccounts.size() > 0) {
                activity.promptForServerPasswords(disabledAccounts);
            } else {
                activity.setNonConfigurationInstance(null);
            }
        }
    }

    /* access modifiers changed from: private */
    public void showImportSelectionDialog(SettingsImporter.ImportContents importContents, Uri uri) {
        ImportSelectionDialog dialog = new ImportSelectionDialog(importContents, uri);
        dialog.show(this);
        setNonConfigurationInstance(dialog);
    }

    private static class ImportSelectionDialog implements NonConfigurationInstance {
        private AlertDialog mDialog;
        /* access modifiers changed from: private */
        public SettingsImporter.ImportContents mImportContents;
        private SparseBooleanArray mSelection;
        /* access modifiers changed from: private */
        public Uri mUri;

        ImportSelectionDialog(SettingsImporter.ImportContents importContents, Uri uri) {
            this.mImportContents = importContents;
            this.mUri = uri;
        }

        public void restore(Activity activity) {
            show((Accounts) activity, this.mSelection);
        }

        public boolean retain() {
            if (this.mDialog == null) {
                return false;
            }
            this.mSelection = this.mDialog.getListView().getCheckedItemPositions();
            this.mDialog.dismiss();
            this.mDialog = null;
            return true;
        }

        public void show(Accounts activity) {
            show(activity, null);
        }

        public void show(final Accounts activity, SparseBooleanArray selection) {
            List<String> contents = new ArrayList<>();
            if (this.mImportContents.globalSettings) {
                contents.add(activity.getString(R.string.settings_import_global_settings));
            }
            for (SettingsImporter.AccountDescription account : this.mImportContents.accounts) {
                contents.add(account.name);
            }
            int count = contents.size();
            boolean[] checkedItems = new boolean[count];
            if (selection != null) {
                for (int i = 0; i < count; i++) {
                    checkedItems[i] = selection.get(i);
                }
            } else {
                for (int i2 = 0; i2 < count; i2++) {
                    checkedItems[i2] = true;
                }
            }
            DialogInterface.OnMultiChoiceClickListener listener = new DialogInterface.OnMultiChoiceClickListener() {
                public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                    ((AlertDialog) dialog).getListView().setItemChecked(which, isChecked);
                }
            };
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setMultiChoiceItems((CharSequence[]) contents.toArray(new String[0]), checkedItems, listener);
            builder.setTitle(activity.getString(R.string.settings_import_selection));
            builder.setInverseBackgroundForced(true);
            builder.setPositiveButton((int) R.string.okay_action, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    boolean includeGlobals;
                    int start;
                    ListView listView = ((AlertDialog) dialog).getListView();
                    SparseBooleanArray pos = listView.getCheckedItemPositions();
                    if (ImportSelectionDialog.this.mImportContents.globalSettings) {
                        includeGlobals = pos.get(0);
                    } else {
                        includeGlobals = false;
                    }
                    List<String> accountUuids = new ArrayList<>();
                    if (ImportSelectionDialog.this.mImportContents.globalSettings) {
                        start = 1;
                    } else {
                        start = 0;
                    }
                    int end = listView.getCount();
                    for (int i = start; i < end; i++) {
                        if (pos.get(i)) {
                            accountUuids.add(ImportSelectionDialog.this.mImportContents.accounts.get(i - start).uuid);
                        }
                    }
                    dialog.dismiss();
                    activity.setNonConfigurationInstance(null);
                    ImportAsyncTask importAsyncTask = new ImportAsyncTask(includeGlobals, accountUuids, false, ImportSelectionDialog.this.mUri);
                    activity.setNonConfigurationInstance(importAsyncTask);
                    importAsyncTask.execute(new Void[0]);
                }
            });
            builder.setNegativeButton((int) R.string.cancel_action, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    activity.setNonConfigurationInstance(null);
                }
            });
            this.mDialog = builder.show();
        }
    }

    /* access modifiers changed from: private */
    public void setNonConfigurationInstance(NonConfigurationInstance inst) {
        this.mNonConfigurationInstance = inst;
    }

    class AccountsAdapter extends ArrayAdapter<BaseAccount> {
        public AccountsAdapter(List<BaseAccount> accounts) {
            super(Accounts.this, 0, accounts);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;
            final BaseAccount account = (BaseAccount) getItem(position);
            if (convertView != null) {
                view = convertView;
            } else {
                view = Accounts.this.getLayoutInflater().inflate((int) R.layout.accounts_item, parent, false);
            }
            AccountViewHolder holder = (AccountViewHolder) view.getTag();
            if (holder == null) {
                holder = new AccountViewHolder();
                holder.description = (TextView) view.findViewById(R.id.description);
                holder.email = (TextView) view.findViewById(R.id.email);
                holder.newMessageCount = (TextView) view.findViewById(R.id.new_message_count);
                holder.flaggedMessageCount = (TextView) view.findViewById(R.id.flagged_message_count);
                holder.newMessageCountWrapper = view.findViewById(R.id.new_message_count_wrapper);
                holder.flaggedMessageCountWrapper = view.findViewById(R.id.flagged_message_count_wrapper);
                holder.newMessageCountIcon = view.findViewById(R.id.new_message_count_icon);
                holder.flaggedMessageCountIcon = view.findViewById(R.id.flagged_message_count_icon);
                holder.activeIcons = (RelativeLayout) view.findViewById(R.id.active_icons);
                holder.chip = view.findViewById(R.id.chip);
                holder.folders = (ImageButton) view.findViewById(R.id.folders);
                holder.accountsItemLayout = (LinearLayout) view.findViewById(R.id.accounts_item_layout);
                view.setTag(holder);
            }
            AccountStats stats = (AccountStats) Accounts.this.accountStats.get(account.getUuid());
            if (stats != null && (account instanceof Account) && stats.size >= 0) {
                holder.email.setText(SizeFormatter.formatSize(Accounts.this, stats.size));
                holder.email.setVisibility(0);
            } else if (account.getEmail().equals(account.getDescription())) {
                holder.email.setVisibility(8);
            } else {
                holder.email.setVisibility(0);
                holder.email.setText(account.getEmail());
            }
            String description = account.getDescription();
            if (description == null || description.isEmpty()) {
                description = account.getEmail();
            }
            holder.description.setText(description);
            if (stats != null) {
                Integer unreadMessageCount = Integer.valueOf(stats.unreadMessageCount);
                holder.newMessageCount.setText(String.format("%d", unreadMessageCount));
                holder.newMessageCountWrapper.setVisibility(unreadMessageCount.intValue() > 0 ? 0 : 8);
                holder.flaggedMessageCount.setText(String.format("%d", Integer.valueOf(stats.flaggedMessageCount)));
                holder.flaggedMessageCountWrapper.setVisibility((!K9.messageListStars() || stats.flaggedMessageCount <= 0) ? 8 : 0);
                holder.flaggedMessageCountWrapper.setOnClickListener(createFlaggedSearchListener(account));
                holder.newMessageCountWrapper.setOnClickListener(createUnreadSearchListener(account));
                holder.activeIcons.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        Toast.makeText(Accounts.this.getApplication(), Accounts.this.getString(R.string.tap_hint), 0).show();
                    }
                });
            } else {
                holder.newMessageCountWrapper.setVisibility(8);
                holder.flaggedMessageCountWrapper.setVisibility(8);
            }
            if (account instanceof Account) {
                Account realAccount = (Account) account;
                holder.chip.setBackgroundColor(realAccount.getChipColor());
                holder.flaggedMessageCountIcon.setBackgroundDrawable(realAccount.generateColorChip(false, false, false, false, true).drawable());
                holder.newMessageCountIcon.setBackgroundDrawable(realAccount.generateColorChip(false, false, false, false, false).drawable());
            } else {
                holder.chip.setBackgroundColor(-6710887);
                holder.newMessageCountIcon.setBackgroundDrawable(new ColorChip(-6710887, false, ColorChip.CIRCULAR).drawable());
                holder.flaggedMessageCountIcon.setBackgroundDrawable(new ColorChip(-6710887, false, ColorChip.STAR).drawable());
            }
            Accounts.this.mFontSizes.setViewTextSize(holder.description, Accounts.this.mFontSizes.getAccountName());
            Accounts.this.mFontSizes.setViewTextSize(holder.email, Accounts.this.mFontSizes.getAccountDescription());
            if (account instanceof SearchAccount) {
                holder.folders.setVisibility(8);
            } else {
                holder.folders.setVisibility(0);
                holder.folders.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        FolderList.actionHandleAccount(Accounts.this, (Account) account);
                    }
                });
            }
            return view;
        }

        private View.OnClickListener createFlaggedSearchListener(BaseAccount account) {
            LocalSearch search;
            String searchTitle = Accounts.this.getString(R.string.search_title, new Object[]{account.getDescription(), Accounts.this.getString(R.string.flagged_modifier)});
            if (account instanceof SearchAccount) {
                search = ((SearchAccount) account).getRelatedSearch().clone();
                search.setName(searchTitle);
            } else {
                search = new LocalSearch(searchTitle);
                search.addAccountUuid(account.getUuid());
                Account realAccount = (Account) account;
                realAccount.excludeSpecialFolders(search);
                realAccount.limitToDisplayableFolders(search);
            }
            search.and(SearchSpecification.SearchField.FLAGGED, Constants.INSTALL_ID, SearchSpecification.Attribute.EQUALS);
            return new AccountClickListener(search);
        }

        private View.OnClickListener createUnreadSearchListener(BaseAccount account) {
            return new AccountClickListener(Accounts.createUnreadSearch(Accounts.this, account));
        }

        class AccountViewHolder {
            public LinearLayout accountsItemLayout;
            public RelativeLayout activeIcons;
            public View chip;
            public TextView description;
            public TextView email;
            public TextView flaggedMessageCount;
            public View flaggedMessageCountIcon;
            public View flaggedMessageCountWrapper;
            public ImageButton folders;
            public TextView newMessageCount;
            public View newMessageCountIcon;
            public View newMessageCountWrapper;

            AccountViewHolder() {
            }
        }
    }

    private class AccountClickListener implements View.OnClickListener {
        final LocalSearch search;

        AccountClickListener(LocalSearch search2) {
            this.search = search2;
        }

        public void onClick(View v) {
            MessageList.actionDisplaySearch(Accounts.this, this.search, true, false);
        }
    }

    public void onExport(boolean includeGlobals, Account account) {
        Set<String> accountUuids = null;
        if (account != null) {
            accountUuids = new HashSet<>();
            accountUuids.add(account.getUuid());
        }
        ExportAsyncTask asyncTask = new ExportAsyncTask(includeGlobals, accountUuids);
        setNonConfigurationInstance(asyncTask);
        asyncTask.execute(new Void[0]);
    }

    private static class ExportAsyncTask extends ExtendedAsyncTask<Void, Void, Boolean> {
        private Set<String> mAccountUuids;
        private String mFileName;
        private boolean mIncludeGlobals;

        private ExportAsyncTask(Accounts activity, boolean includeGlobals, Set<String> accountUuids) {
            super(activity);
            this.mIncludeGlobals = includeGlobals;
            this.mAccountUuids = accountUuids;
        }

        /* access modifiers changed from: protected */
        public void showProgressDialog() {
            this.mProgressDialog = ProgressDialog.show(this.mActivity, this.mContext.getString(R.string.settings_export_dialog_title), this.mContext.getString(R.string.settings_exporting), true);
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(Void... params) {
            try {
                this.mFileName = SettingsExporter.exportToFile(this.mContext, this.mIncludeGlobals, this.mAccountUuids);
                return true;
            } catch (SettingsImportExportException e) {
                Log.w("k9", "Exception during export", e);
                return false;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean success) {
            Accounts activity = (Accounts) this.mActivity;
            activity.setNonConfigurationInstance(null);
            removeProgressDialog();
            if (success.booleanValue()) {
                activity.showSimpleDialog(R.string.settings_export_success_header, R.string.settings_export_success, this.mFileName);
                return;
            }
            activity.showSimpleDialog(R.string.settings_export_failed_header, R.string.settings_export_failure, new Object[0]);
        }
    }

    private static class ImportAsyncTask extends ExtendedAsyncTask<Void, Void, Boolean> {
        private List<String> mAccountUuids;
        private SettingsImporter.ImportResults mImportResults;
        private boolean mIncludeGlobals;
        private boolean mOverwrite;
        private Uri mUri;

        private ImportAsyncTask(Accounts activity, boolean includeGlobals, List<String> accountUuids, boolean overwrite, Uri uri) {
            super(activity);
            this.mIncludeGlobals = includeGlobals;
            this.mAccountUuids = accountUuids;
            this.mOverwrite = overwrite;
            this.mUri = uri;
        }

        /* access modifiers changed from: protected */
        public void showProgressDialog() {
            this.mProgressDialog = ProgressDialog.show(this.mActivity, this.mContext.getString(R.string.settings_import_dialog_title), this.mContext.getString(R.string.settings_importing), true);
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(Void... params) {
            InputStream is;
            try {
                is = this.mContext.getContentResolver().openInputStream(this.mUri);
                this.mImportResults = SettingsImporter.importSettings(this.mContext, is, this.mIncludeGlobals, this.mAccountUuids, this.mOverwrite);
                try {
                    is.close();
                } catch (IOException e) {
                }
                return true;
            } catch (SettingsImportExportException e2) {
                Log.w("k9", "Exception during import", e2);
                return false;
            } catch (FileNotFoundException e3) {
                Log.w("k9", "Couldn't open import file", e3);
                return false;
            } catch (Exception e4) {
                Log.w("k9", "Unknown error", e4);
                return false;
            } catch (Throwable th) {
                try {
                    is.close();
                } catch (IOException e5) {
                }
                throw th;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean success) {
            Accounts activity = (Accounts) this.mActivity;
            activity.setNonConfigurationInstance(null);
            removeProgressDialog();
            String filename = this.mUri.getLastPathSegment();
            boolean globalSettings = this.mImportResults.globalSettings;
            int imported = this.mImportResults.importedAccounts.size();
            if (!success.booleanValue() || (!globalSettings && imported <= 0)) {
                activity.showSimpleDialog(R.string.settings_import_failed_header, R.string.settings_import_failure, filename);
                return;
            }
            if (imported == 0) {
                activity.showSimpleDialog(R.string.settings_import_success_header, R.string.settings_import_global_settings_success, filename);
            } else {
                activity.showAccountsImportedDialog(this.mImportResults, filename);
            }
            activity.refresh();
        }
    }

    private static class ListImportContentsAsyncTask extends ExtendedAsyncTask<Void, Void, Boolean> {
        private SettingsImporter.ImportContents mImportContents;
        private Uri mUri;

        private ListImportContentsAsyncTask(Accounts activity, Uri uri) {
            super(activity);
            this.mUri = uri;
        }

        /* access modifiers changed from: protected */
        public void showProgressDialog() {
            this.mProgressDialog = ProgressDialog.show(this.mActivity, this.mContext.getString(R.string.settings_import_dialog_title), this.mContext.getString(R.string.settings_import_scanning_file), true);
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(Void... params) {
            InputStream is;
            try {
                is = this.mContext.getContentResolver().openInputStream(this.mUri);
                this.mImportContents = SettingsImporter.getImportStreamContents(is);
                try {
                    is.close();
                } catch (IOException e) {
                }
                return true;
            } catch (SettingsImportExportException e2) {
                Log.w("k9", "Exception during export", e2);
                return false;
            } catch (FileNotFoundException e3) {
                Log.w("k9", "Couldn't read content from URI " + this.mUri);
                return false;
            } catch (Throwable th) {
                try {
                    is.close();
                } catch (IOException e4) {
                }
                throw th;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean success) {
            Accounts activity = (Accounts) this.mActivity;
            activity.setNonConfigurationInstance(null);
            removeProgressDialog();
            if (success.booleanValue()) {
                activity.showImportSelectionDialog(this.mImportContents, this.mUri);
                return;
            }
            activity.showSimpleDialog(R.string.settings_import_failed_header, R.string.settings_import_failure, this.mUri.getLastPathSegment());
        }
    }

    private static class MoveAccountAsyncTask extends ExtendedAsyncTask<Void, Void, Void> {
        private Account mAccount;
        private boolean mUp;

        protected MoveAccountAsyncTask(Activity activity, Account account, boolean up) {
            super(activity);
            this.mAccount = account;
            this.mUp = up;
        }

        /* access modifiers changed from: protected */
        public void showProgressDialog() {
            this.mProgressDialog = ProgressDialog.show(this.mActivity, null, this.mActivity.getString(R.string.manage_accounts_moving_message), true);
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... args) {
            this.mAccount.move(Preferences.getPreferences(this.mContext), this.mUp);
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void arg) {
            Accounts activity = (Accounts) this.mActivity;
            activity.setNonConfigurationInstance(null);
            activity.refresh();
            removeProgressDialog();
        }
    }
}
