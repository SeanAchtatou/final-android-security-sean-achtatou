package com.fsck.k9.activity.setup;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceScreen;
import android.preference.RingtonePreference;
import android.util.Log;
import com.fsck.k9.Account;
import com.fsck.k9.K9;
import com.fsck.k9.NotificationSetting;
import com.fsck.k9.Preferences;
import com.fsck.k9.activity.ChooseFolder;
import com.fsck.k9.activity.ChooseIdentity;
import com.fsck.k9.activity.ColorPickerDialog;
import com.fsck.k9.activity.K9PreferenceActivity;
import com.fsck.k9.activity.ManageIdentities;
import com.fsck.k9.crypto.OpenPgpApiHelper;
import com.fsck.k9.mail.Folder;
import com.fsck.k9.mail.Store;
import com.fsck.k9.mailstore.StorageManager;
import com.fsck.k9.service.MailService;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.openintents.openpgp.util.OpenPgpAppPreference;
import org.openintents.openpgp.util.OpenPgpKeyPreference;
import org.openintents.openpgp.util.OpenPgpUtils;
import yahoo.mail.app.R;

public class AccountSettings extends K9PreferenceActivity {
    private static final int ACTIVITY_MANAGE_IDENTITIES = 2;
    private static final int DIALOG_COLOR_PICKER_ACCOUNT = 1;
    private static final int DIALOG_COLOR_PICKER_LED = 2;
    private static final String EXTRA_ACCOUNT = "account";
    private static final String PREFERENCE_ALWAYS_SHOW_CC_BCC = "always_show_cc_bcc";
    private static final String PREFERENCE_ARCHIVE_FOLDER = "archive_folder";
    private static final String PREFERENCE_AUTO_EXPAND_FOLDER = "account_setup_auto_expand_folder";
    private static final String PREFERENCE_CATEGORY_FOLDERS = "folders";
    private static final String PREFERENCE_CHIP_COLOR = "chip_color";
    private static final String PREFERENCE_CLOUD_SEARCH_ENABLED = "remote_search_enabled";
    private static final String PREFERENCE_COMPOSITION = "composition";
    private static final String PREFERENCE_CRYPTO = "crypto";
    private static final String PREFERENCE_CRYPTO_APP = "crypto_app";
    private static final String PREFERENCE_CRYPTO_KEY = "crypto_key";
    private static final String PREFERENCE_DEFAULT = "account_default";
    private static final String PREFERENCE_DEFAULT_QUOTED_TEXT_SHOWN = "default_quoted_text_shown";
    private static final String PREFERENCE_DELETE_POLICY = "delete_policy";
    private static final String PREFERENCE_DESCRIPTION = "account_description";
    private static final String PREFERENCE_DISPLAY_COUNT = "account_display_count";
    private static final String PREFERENCE_DISPLAY_MODE = "folder_display_mode";
    private static final String PREFERENCE_DRAFTS_FOLDER = "drafts_folder";
    private static final String PREFERENCE_EXPUNGE_POLICY = "expunge_policy";
    private static final String PREFERENCE_FREQUENCY = "account_check_frequency";
    private static final String PREFERENCE_IDLE_REFRESH_PERIOD = "idle_refresh_period";
    private static final String PREFERENCE_INCOMING = "incoming";
    private static final String PREFERENCE_LED_COLOR = "led_color";
    private static final String PREFERENCE_LOCAL_STORAGE_PROVIDER = "local_storage_provider";
    private static final String PREFERENCE_MANAGE_IDENTITIES = "manage_identities";
    private static final String PREFERENCE_MARK_MESSAGE_AS_READ_ON_VIEW = "mark_message_as_read_on_view";
    private static final String PREFERENCE_MAX_PUSH_FOLDERS = "max_push_folders";
    private static final String PREFERENCE_MESSAGE_AGE = "account_message_age";
    private static final String PREFERENCE_MESSAGE_FORMAT = "message_format";
    private static final String PREFERENCE_MESSAGE_READ_RECEIPT = "message_read_receipt";
    private static final String PREFERENCE_MESSAGE_SIZE = "account_autodownload_size";
    private static final String PREFERENCE_NOTIFICATION_LED = "account_led";
    private static final String PREFERENCE_NOTIFICATION_OPENS_UNREAD = "notification_opens_unread";
    private static final String PREFERENCE_NOTIFY = "account_notify";
    private static final String PREFERENCE_NOTIFY_CONTACTS_MAIL_ONLY = "account_notify_contacts_mail_only";
    private static final String PREFERENCE_NOTIFY_NEW_MAIL_MODE = "folder_notify_new_mail_mode";
    private static final String PREFERENCE_NOTIFY_SELF = "account_notify_self";
    private static final String PREFERENCE_NOTIFY_SYNC = "account_notify_sync";
    private static final String PREFERENCE_OUTGOING = "outgoing";
    private static final String PREFERENCE_PUSH_MODE = "folder_push_mode";
    private static final String PREFERENCE_PUSH_POLL_ON_CONNECT = "push_poll_on_connect";
    private static final String PREFERENCE_QUOTE_PREFIX = "account_quote_prefix";
    private static final String PREFERENCE_QUOTE_STYLE = "quote_style";
    private static final String PREFERENCE_REMOTE_SEARCH_FULL_TEXT = "account_remote_search_full_text";
    private static final String PREFERENCE_REMOTE_SEARCH_NUM_RESULTS = "account_remote_search_num_results";
    private static final String PREFERENCE_REPLY_AFTER_QUOTE = "reply_after_quote";
    private static final String PREFERENCE_RINGTONE = "account_ringtone";
    private static final String PREFERENCE_SCREEN_COMPOSING = "composing";
    private static final String PREFERENCE_SCREEN_INCOMING = "incoming_prefs";
    private static final String PREFERENCE_SCREEN_MAIN = "main";
    private static final String PREFERENCE_SCREEN_PUSH_ADVANCED = "push_advanced";
    private static final String PREFERENCE_SCREEN_SEARCH = "search";
    private static final String PREFERENCE_SEARCHABLE_FOLDERS = "searchable_folders";
    private static final String PREFERENCE_SENT_FOLDER = "sent_folder";
    private static final String PREFERENCE_SHOW_PICTURES = "show_pictures_enum";
    private static final String PREFERENCE_SPAM_FOLDER = "spam_folder";
    private static final String PREFERENCE_STRIP_SIGNATURE = "strip_signature";
    private static final String PREFERENCE_SYNC_MODE = "folder_sync_mode";
    private static final String PREFERENCE_SYNC_REMOTE_DELETIONS = "account_sync_remote_deletetions";
    private static final String PREFERENCE_TARGET_MODE = "folder_target_mode";
    private static final String PREFERENCE_TRASH_FOLDER = "trash_folder";
    private static final String PREFERENCE_VIBRATE = "account_vibrate";
    private static final String PREFERENCE_VIBRATE_PATTERN = "account_vibrate_pattern";
    private static final String PREFERENCE_VIBRATE_TIMES = "account_vibrate_times";
    private static final int SELECT_AUTO_EXPAND_FOLDER = 1;
    /* access modifiers changed from: private */
    public Account mAccount;
    private CheckBoxPreference mAccountDefault;
    private CheckBoxPreference mAccountDefaultQuotedTextShown;
    /* access modifiers changed from: private */
    public EditTextPreference mAccountDescription;
    private CheckBoxPreference mAccountLed;
    private CheckBoxPreference mAccountNotify;
    private CheckBoxPreference mAccountNotifyContactsMailOnly;
    /* access modifiers changed from: private */
    public ListPreference mAccountNotifyNewMailMode;
    private CheckBoxPreference mAccountNotifySelf;
    private CheckBoxPreference mAccountNotifySync;
    /* access modifiers changed from: private */
    public EditTextPreference mAccountQuotePrefix;
    private RingtonePreference mAccountRingtone;
    /* access modifiers changed from: private */
    public ListPreference mAccountShowPictures;
    private CheckBoxPreference mAccountVibrate;
    /* access modifiers changed from: private */
    public ListPreference mAccountVibratePattern;
    /* access modifiers changed from: private */
    public ListPreference mAccountVibrateTimes;
    private CheckBoxPreference mAlwaysShowCcBcc;
    /* access modifiers changed from: private */
    public ListPreference mArchiveFolder;
    /* access modifiers changed from: private */
    public ListPreference mAutoExpandFolder;
    /* access modifiers changed from: private */
    public ListPreference mCheckFrequency;
    private Preference mChipColor;
    private CheckBoxPreference mCloudSearchEnabled;
    /* access modifiers changed from: private */
    public PreferenceScreen mComposingScreen;
    /* access modifiers changed from: private */
    public OpenPgpAppPreference mCryptoApp;
    /* access modifiers changed from: private */
    public OpenPgpKeyPreference mCryptoKey;
    /* access modifiers changed from: private */
    public ListPreference mDeletePolicy;
    /* access modifiers changed from: private */
    public ListPreference mDisplayCount;
    /* access modifiers changed from: private */
    public ListPreference mDisplayMode;
    /* access modifiers changed from: private */
    public ListPreference mDraftsFolder;
    /* access modifiers changed from: private */
    public ListPreference mExpungePolicy;
    private boolean mHasCrypto = false;
    /* access modifiers changed from: private */
    public ListPreference mIdleRefreshPeriod;
    /* access modifiers changed from: private */
    public boolean mIncomingChanged = false;
    private boolean mIsExpungeCapable = false;
    /* access modifiers changed from: private */
    public boolean mIsMoveCapable = false;
    private boolean mIsPushCapable = false;
    private boolean mIsSeenFlagSupported = false;
    private Preference mLedColor;
    /* access modifiers changed from: private */
    public ListPreference mLocalStorageProvider;
    private PreferenceScreen mMainScreen;
    private CheckBoxPreference mMarkMessageAsReadOnView;
    /* access modifiers changed from: private */
    public ListPreference mMaxPushFolders;
    /* access modifiers changed from: private */
    public ListPreference mMessageAge;
    /* access modifiers changed from: private */
    public ListPreference mMessageFormat;
    private CheckBoxPreference mMessageReadReceipt;
    /* access modifiers changed from: private */
    public ListPreference mMessageSize;
    private CheckBoxPreference mNotificationOpensUnread;
    /* access modifiers changed from: private */
    public ListPreference mPushMode;
    private CheckBoxPreference mPushPollOnConnect;
    /* access modifiers changed from: private */
    public ListPreference mQuoteStyle;
    private CheckBoxPreference mRemoteSearchFullText;
    private ListPreference mRemoteSearchNumResults;
    /* access modifiers changed from: private */
    public CheckBoxPreference mReplyAfterQuote;
    private PreferenceScreen mSearchScreen;
    /* access modifiers changed from: private */
    public ListPreference mSearchableFolders;
    /* access modifiers changed from: private */
    public ListPreference mSentFolder;
    /* access modifiers changed from: private */
    public ListPreference mSpamFolder;
    private CheckBoxPreference mStripSignature;
    /* access modifiers changed from: private */
    public ListPreference mSyncMode;
    private CheckBoxPreference mSyncRemoteDeletions;
    /* access modifiers changed from: private */
    public ListPreference mTargetMode;
    /* access modifiers changed from: private */
    public ListPreference mTrashFolder;

    public static void actionSettings(Context context, Account account) {
        Intent i = new Intent(context, AccountSettings.class);
        i.putExtra("account", account.getUuid());
        context.startActivity(i);
    }

    public void onCreate(Bundle savedInstanceState) {
        String currentRingtone;
        super.onCreate(savedInstanceState);
        this.mAccount = Preferences.getPreferences(this).getAccount(getIntent().getStringExtra("account"));
        try {
            Store store = this.mAccount.getRemoteStore();
            this.mIsMoveCapable = store.isMoveCapable();
            this.mIsPushCapable = store.isPushCapable();
            this.mIsExpungeCapable = store.isExpungeCapable();
            this.mIsSeenFlagSupported = store.isSeenFlagSupported();
        } catch (Exception e) {
            Log.e("k9", "Could not get remote store", e);
        }
        addPreferencesFromResource(R.xml.account_settings_preferences);
        this.mMainScreen = (PreferenceScreen) findPreference(PREFERENCE_SCREEN_MAIN);
        this.mAccountDescription = (EditTextPreference) findPreference(PREFERENCE_DESCRIPTION);
        this.mAccountDescription.setSummary(this.mAccount.getDescription());
        this.mAccountDescription.setText(this.mAccount.getDescription());
        this.mAccountDescription.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                String summary = newValue.toString();
                AccountSettings.this.mAccountDescription.setSummary(summary);
                AccountSettings.this.mAccountDescription.setText(summary);
                return false;
            }
        });
        this.mMarkMessageAsReadOnView = (CheckBoxPreference) findPreference(PREFERENCE_MARK_MESSAGE_AS_READ_ON_VIEW);
        this.mMarkMessageAsReadOnView.setChecked(this.mAccount.isMarkMessageAsReadOnView());
        this.mMessageFormat = (ListPreference) findPreference(PREFERENCE_MESSAGE_FORMAT);
        this.mMessageFormat.setValue(this.mAccount.getMessageFormat().name());
        this.mMessageFormat.setSummary(this.mMessageFormat.getEntry());
        this.mMessageFormat.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                String summary = newValue.toString();
                AccountSettings.this.mMessageFormat.setSummary(AccountSettings.this.mMessageFormat.getEntries()[AccountSettings.this.mMessageFormat.findIndexOfValue(summary)]);
                AccountSettings.this.mMessageFormat.setValue(summary);
                return false;
            }
        });
        this.mAlwaysShowCcBcc = (CheckBoxPreference) findPreference(PREFERENCE_ALWAYS_SHOW_CC_BCC);
        this.mAlwaysShowCcBcc.setChecked(this.mAccount.isAlwaysShowCcBcc());
        this.mMessageReadReceipt = (CheckBoxPreference) findPreference(PREFERENCE_MESSAGE_READ_RECEIPT);
        this.mMessageReadReceipt.setChecked(this.mAccount.isMessageReadReceiptAlways());
        this.mAccountQuotePrefix = (EditTextPreference) findPreference(PREFERENCE_QUOTE_PREFIX);
        this.mAccountQuotePrefix.setSummary(this.mAccount.getQuotePrefix());
        this.mAccountQuotePrefix.setText(this.mAccount.getQuotePrefix());
        this.mAccountQuotePrefix.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                String value = newValue.toString();
                AccountSettings.this.mAccountQuotePrefix.setSummary(value);
                AccountSettings.this.mAccountQuotePrefix.setText(value);
                return false;
            }
        });
        this.mAccountDefaultQuotedTextShown = (CheckBoxPreference) findPreference(PREFERENCE_DEFAULT_QUOTED_TEXT_SHOWN);
        this.mAccountDefaultQuotedTextShown.setChecked(this.mAccount.isDefaultQuotedTextShown());
        this.mReplyAfterQuote = (CheckBoxPreference) findPreference(PREFERENCE_REPLY_AFTER_QUOTE);
        this.mReplyAfterQuote.setChecked(this.mAccount.isReplyAfterQuote());
        this.mStripSignature = (CheckBoxPreference) findPreference(PREFERENCE_STRIP_SIGNATURE);
        this.mStripSignature.setChecked(this.mAccount.isStripSignature());
        this.mComposingScreen = (PreferenceScreen) findPreference(PREFERENCE_SCREEN_COMPOSING);
        Preference.OnPreferenceChangeListener quoteStyleListener = new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                Account.QuoteStyle style = Account.QuoteStyle.valueOf(newValue.toString());
                AccountSettings.this.mQuoteStyle.setSummary(AccountSettings.this.mQuoteStyle.getEntries()[AccountSettings.this.mQuoteStyle.findIndexOfValue(newValue.toString())]);
                if (style == Account.QuoteStyle.PREFIX) {
                    AccountSettings.this.mComposingScreen.addPreference(AccountSettings.this.mAccountQuotePrefix);
                    AccountSettings.this.mComposingScreen.addPreference(AccountSettings.this.mReplyAfterQuote);
                    return true;
                } else if (style != Account.QuoteStyle.HEADER) {
                    return true;
                } else {
                    AccountSettings.this.mComposingScreen.removePreference(AccountSettings.this.mAccountQuotePrefix);
                    AccountSettings.this.mComposingScreen.removePreference(AccountSettings.this.mReplyAfterQuote);
                    return true;
                }
            }
        };
        this.mQuoteStyle = (ListPreference) findPreference(PREFERENCE_QUOTE_STYLE);
        this.mQuoteStyle.setValue(this.mAccount.getQuoteStyle().name());
        this.mQuoteStyle.setSummary(this.mQuoteStyle.getEntry());
        this.mQuoteStyle.setOnPreferenceChangeListener(quoteStyleListener);
        quoteStyleListener.onPreferenceChange(this.mQuoteStyle, this.mAccount.getQuoteStyle().name());
        this.mCheckFrequency = (ListPreference) findPreference(PREFERENCE_FREQUENCY);
        this.mCheckFrequency.setValue(String.valueOf(this.mAccount.getAutomaticCheckIntervalMinutes()));
        this.mCheckFrequency.setSummary(this.mCheckFrequency.getEntry());
        this.mCheckFrequency.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                String summary = newValue.toString();
                AccountSettings.this.mCheckFrequency.setSummary(AccountSettings.this.mCheckFrequency.getEntries()[AccountSettings.this.mCheckFrequency.findIndexOfValue(summary)]);
                AccountSettings.this.mCheckFrequency.setValue(summary);
                return false;
            }
        });
        this.mDisplayMode = (ListPreference) findPreference(PREFERENCE_DISPLAY_MODE);
        this.mDisplayMode.setValue(this.mAccount.getFolderDisplayMode().name());
        this.mDisplayMode.setSummary(this.mDisplayMode.getEntry());
        this.mDisplayMode.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                String summary = newValue.toString();
                AccountSettings.this.mDisplayMode.setSummary(AccountSettings.this.mDisplayMode.getEntries()[AccountSettings.this.mDisplayMode.findIndexOfValue(summary)]);
                AccountSettings.this.mDisplayMode.setValue(summary);
                return false;
            }
        });
        this.mSyncMode = (ListPreference) findPreference(PREFERENCE_SYNC_MODE);
        this.mSyncMode.setValue(this.mAccount.getFolderSyncMode().name());
        this.mSyncMode.setSummary(this.mSyncMode.getEntry());
        this.mSyncMode.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                String summary = newValue.toString();
                AccountSettings.this.mSyncMode.setSummary(AccountSettings.this.mSyncMode.getEntries()[AccountSettings.this.mSyncMode.findIndexOfValue(summary)]);
                AccountSettings.this.mSyncMode.setValue(summary);
                return false;
            }
        });
        this.mTargetMode = (ListPreference) findPreference(PREFERENCE_TARGET_MODE);
        this.mTargetMode.setValue(this.mAccount.getFolderTargetMode().name());
        this.mTargetMode.setSummary(this.mTargetMode.getEntry());
        this.mTargetMode.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                String summary = newValue.toString();
                AccountSettings.this.mTargetMode.setSummary(AccountSettings.this.mTargetMode.getEntries()[AccountSettings.this.mTargetMode.findIndexOfValue(summary)]);
                AccountSettings.this.mTargetMode.setValue(summary);
                return false;
            }
        });
        this.mDeletePolicy = (ListPreference) findPreference(PREFERENCE_DELETE_POLICY);
        if (!this.mIsSeenFlagSupported) {
            removeListEntry(this.mDeletePolicy, Account.DeletePolicy.MARK_AS_READ.preferenceString());
        }
        this.mDeletePolicy.setValue(this.mAccount.getDeletePolicy().preferenceString());
        this.mDeletePolicy.setSummary(this.mDeletePolicy.getEntry());
        this.mDeletePolicy.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                String summary = newValue.toString();
                AccountSettings.this.mDeletePolicy.setSummary(AccountSettings.this.mDeletePolicy.getEntries()[AccountSettings.this.mDeletePolicy.findIndexOfValue(summary)]);
                AccountSettings.this.mDeletePolicy.setValue(summary);
                return false;
            }
        });
        this.mExpungePolicy = (ListPreference) findPreference(PREFERENCE_EXPUNGE_POLICY);
        if (this.mIsExpungeCapable) {
            this.mExpungePolicy.setValue(this.mAccount.getExpungePolicy().name());
            this.mExpungePolicy.setSummary(this.mExpungePolicy.getEntry());
            this.mExpungePolicy.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    String summary = newValue.toString();
                    AccountSettings.this.mExpungePolicy.setSummary(AccountSettings.this.mExpungePolicy.getEntries()[AccountSettings.this.mExpungePolicy.findIndexOfValue(summary)]);
                    AccountSettings.this.mExpungePolicy.setValue(summary);
                    return false;
                }
            });
        } else {
            ((PreferenceScreen) findPreference(PREFERENCE_SCREEN_INCOMING)).removePreference(this.mExpungePolicy);
        }
        this.mSyncRemoteDeletions = (CheckBoxPreference) findPreference(PREFERENCE_SYNC_REMOTE_DELETIONS);
        this.mSyncRemoteDeletions.setChecked(this.mAccount.syncRemoteDeletions());
        this.mSearchableFolders = (ListPreference) findPreference(PREFERENCE_SEARCHABLE_FOLDERS);
        this.mSearchableFolders.setValue(this.mAccount.getSearchableFolders().name());
        this.mSearchableFolders.setSummary(this.mSearchableFolders.getEntry());
        this.mSearchableFolders.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                String summary = newValue.toString();
                AccountSettings.this.mSearchableFolders.setSummary(AccountSettings.this.mSearchableFolders.getEntries()[AccountSettings.this.mSearchableFolders.findIndexOfValue(summary)]);
                AccountSettings.this.mSearchableFolders.setValue(summary);
                return false;
            }
        });
        this.mDisplayCount = (ListPreference) findPreference(PREFERENCE_DISPLAY_COUNT);
        this.mDisplayCount.setValue(String.valueOf(this.mAccount.getDisplayCount()));
        this.mDisplayCount.setSummary(this.mDisplayCount.getEntry());
        this.mDisplayCount.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                String summary = newValue.toString();
                AccountSettings.this.mDisplayCount.setSummary(AccountSettings.this.mDisplayCount.getEntries()[AccountSettings.this.mDisplayCount.findIndexOfValue(summary)]);
                AccountSettings.this.mDisplayCount.setValue(summary);
                return false;
            }
        });
        this.mMessageAge = (ListPreference) findPreference(PREFERENCE_MESSAGE_AGE);
        if (!this.mAccount.isSearchByDateCapable()) {
            ((PreferenceScreen) findPreference(PREFERENCE_SCREEN_INCOMING)).removePreference(this.mMessageAge);
        } else {
            this.mMessageAge.setValue(String.valueOf(this.mAccount.getMaximumPolledMessageAge()));
            this.mMessageAge.setSummary(this.mMessageAge.getEntry());
            this.mMessageAge.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    String summary = newValue.toString();
                    AccountSettings.this.mMessageAge.setSummary(AccountSettings.this.mMessageAge.getEntries()[AccountSettings.this.mMessageAge.findIndexOfValue(summary)]);
                    AccountSettings.this.mMessageAge.setValue(summary);
                    return false;
                }
            });
        }
        this.mMessageSize = (ListPreference) findPreference(PREFERENCE_MESSAGE_SIZE);
        this.mMessageSize.setValue(String.valueOf(this.mAccount.getMaximumAutoDownloadMessageSize()));
        this.mMessageSize.setSummary(this.mMessageSize.getEntry());
        this.mMessageSize.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                String summary = newValue.toString();
                AccountSettings.this.mMessageSize.setSummary(AccountSettings.this.mMessageSize.getEntries()[AccountSettings.this.mMessageSize.findIndexOfValue(summary)]);
                AccountSettings.this.mMessageSize.setValue(summary);
                return false;
            }
        });
        this.mAccountDefault = (CheckBoxPreference) findPreference(PREFERENCE_DEFAULT);
        this.mAccountDefault.setChecked(this.mAccount.equals(Preferences.getPreferences(this).getDefaultAccount()));
        this.mAccountShowPictures = (ListPreference) findPreference(PREFERENCE_SHOW_PICTURES);
        this.mAccountShowPictures.setValue("" + this.mAccount.getShowPictures());
        this.mAccountShowPictures.setSummary(this.mAccountShowPictures.getEntry());
        this.mAccountShowPictures.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                String summary = newValue.toString();
                AccountSettings.this.mAccountShowPictures.setSummary(AccountSettings.this.mAccountShowPictures.getEntries()[AccountSettings.this.mAccountShowPictures.findIndexOfValue(summary)]);
                AccountSettings.this.mAccountShowPictures.setValue(summary);
                return false;
            }
        });
        this.mLocalStorageProvider = (ListPreference) findPreference(PREFERENCE_LOCAL_STORAGE_PROVIDER);
        final Map<String, String> providers = StorageManager.getInstance(this).getAvailableProviders();
        int i = 0;
        String[] providerLabels = new String[providers.size()];
        String[] providerIds = new String[providers.size()];
        for (Map.Entry<String, String> entry : providers.entrySet()) {
            providerIds[i] = (String) entry.getKey();
            providerLabels[i] = (String) entry.getValue();
            i++;
        }
        this.mLocalStorageProvider.setEntryValues(providerIds);
        this.mLocalStorageProvider.setEntries(providerLabels);
        this.mLocalStorageProvider.setValue(this.mAccount.getLocalStorageProviderId());
        this.mLocalStorageProvider.setSummary(providers.get(this.mAccount.getLocalStorageProviderId()));
        this.mLocalStorageProvider.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                AccountSettings.this.mLocalStorageProvider.setSummary((CharSequence) providers.get(newValue));
                return true;
            }
        });
        this.mSearchScreen = (PreferenceScreen) findPreference(PREFERENCE_SCREEN_SEARCH);
        this.mCloudSearchEnabled = (CheckBoxPreference) findPreference(PREFERENCE_CLOUD_SEARCH_ENABLED);
        this.mRemoteSearchNumResults = (ListPreference) findPreference(PREFERENCE_REMOTE_SEARCH_NUM_RESULTS);
        this.mRemoteSearchNumResults.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference pref, Object newVal) {
                AccountSettings.this.updateRemoteSearchLimit((String) newVal);
                return true;
            }
        });
        this.mRemoteSearchFullText = (CheckBoxPreference) findPreference(PREFERENCE_REMOTE_SEARCH_FULL_TEXT);
        this.mPushPollOnConnect = (CheckBoxPreference) findPreference(PREFERENCE_PUSH_POLL_ON_CONNECT);
        this.mIdleRefreshPeriod = (ListPreference) findPreference(PREFERENCE_IDLE_REFRESH_PERIOD);
        this.mMaxPushFolders = (ListPreference) findPreference(PREFERENCE_MAX_PUSH_FOLDERS);
        if (this.mIsPushCapable) {
            this.mPushPollOnConnect.setChecked(this.mAccount.isPushPollOnConnect());
            this.mCloudSearchEnabled.setChecked(this.mAccount.allowRemoteSearch());
            String searchNumResults = Integer.toString(this.mAccount.getRemoteSearchNumResults());
            this.mRemoteSearchNumResults.setValue(searchNumResults);
            updateRemoteSearchLimit(searchNumResults);
            this.mRemoteSearchFullText.setChecked(this.mAccount.isRemoteSearchFullText());
            this.mIdleRefreshPeriod.setValue(String.valueOf(this.mAccount.getIdleRefreshMinutes()));
            this.mIdleRefreshPeriod.setSummary(this.mIdleRefreshPeriod.getEntry());
            this.mIdleRefreshPeriod.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    String summary = newValue.toString();
                    AccountSettings.this.mIdleRefreshPeriod.setSummary(AccountSettings.this.mIdleRefreshPeriod.getEntries()[AccountSettings.this.mIdleRefreshPeriod.findIndexOfValue(summary)]);
                    AccountSettings.this.mIdleRefreshPeriod.setValue(summary);
                    return false;
                }
            });
            this.mMaxPushFolders.setValue(String.valueOf(this.mAccount.getMaxPushFolders()));
            this.mMaxPushFolders.setSummary(this.mMaxPushFolders.getEntry());
            this.mMaxPushFolders.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    String summary = newValue.toString();
                    AccountSettings.this.mMaxPushFolders.setSummary(AccountSettings.this.mMaxPushFolders.getEntries()[AccountSettings.this.mMaxPushFolders.findIndexOfValue(summary)]);
                    AccountSettings.this.mMaxPushFolders.setValue(summary);
                    return false;
                }
            });
            this.mPushMode = (ListPreference) findPreference(PREFERENCE_PUSH_MODE);
            this.mPushMode.setValue(this.mAccount.getFolderPushMode().name());
            this.mPushMode.setSummary(this.mPushMode.getEntry());
            this.mPushMode.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    String summary = newValue.toString();
                    AccountSettings.this.mPushMode.setSummary(AccountSettings.this.mPushMode.getEntries()[AccountSettings.this.mPushMode.findIndexOfValue(summary)]);
                    AccountSettings.this.mPushMode.setValue(summary);
                    return false;
                }
            });
        } else {
            PreferenceScreen incomingPrefs = (PreferenceScreen) findPreference(PREFERENCE_SCREEN_INCOMING);
            incomingPrefs.removePreference((PreferenceScreen) findPreference(PREFERENCE_SCREEN_PUSH_ADVANCED));
            incomingPrefs.removePreference((ListPreference) findPreference(PREFERENCE_PUSH_MODE));
            this.mMainScreen.removePreference(this.mSearchScreen);
        }
        this.mAccountNotify = (CheckBoxPreference) findPreference(PREFERENCE_NOTIFY);
        this.mAccountNotify.setChecked(this.mAccount.isNotifyNewMail());
        this.mAccountNotifyNewMailMode = (ListPreference) findPreference(PREFERENCE_NOTIFY_NEW_MAIL_MODE);
        this.mAccountNotifyNewMailMode.setValue(this.mAccount.getFolderNotifyNewMailMode().name());
        this.mAccountNotifyNewMailMode.setSummary(this.mAccountNotifyNewMailMode.getEntry());
        this.mAccountNotifyNewMailMode.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                String summary = newValue.toString();
                AccountSettings.this.mAccountNotifyNewMailMode.setSummary(AccountSettings.this.mAccountNotifyNewMailMode.getEntries()[AccountSettings.this.mAccountNotifyNewMailMode.findIndexOfValue(summary)]);
                AccountSettings.this.mAccountNotifyNewMailMode.setValue(summary);
                return false;
            }
        });
        this.mAccountNotifySelf = (CheckBoxPreference) findPreference(PREFERENCE_NOTIFY_SELF);
        this.mAccountNotifySelf.setChecked(this.mAccount.isNotifySelfNewMail());
        this.mAccountNotifyContactsMailOnly = (CheckBoxPreference) findPreference(PREFERENCE_NOTIFY_CONTACTS_MAIL_ONLY);
        this.mAccountNotifyContactsMailOnly.setChecked(this.mAccount.isNotifyContactsMailOnly());
        this.mAccountNotifySync = (CheckBoxPreference) findPreference(PREFERENCE_NOTIFY_SYNC);
        this.mAccountNotifySync.setChecked(this.mAccount.isShowOngoing());
        this.mAccountRingtone = (RingtonePreference) findPreference(PREFERENCE_RINGTONE);
        SharedPreferences prefs = this.mAccountRingtone.getPreferenceManager().getSharedPreferences();
        if (!this.mAccount.getNotificationSetting().shouldRing()) {
            currentRingtone = null;
        } else {
            currentRingtone = this.mAccount.getNotificationSetting().getRingtone();
        }
        prefs.edit().putString(PREFERENCE_RINGTONE, currentRingtone).commit();
        this.mAccountVibrate = (CheckBoxPreference) findPreference(PREFERENCE_VIBRATE);
        this.mAccountVibrate.setChecked(this.mAccount.getNotificationSetting().shouldVibrate());
        this.mAccountVibratePattern = (ListPreference) findPreference(PREFERENCE_VIBRATE_PATTERN);
        this.mAccountVibratePattern.setValue(String.valueOf(this.mAccount.getNotificationSetting().getVibratePattern()));
        this.mAccountVibratePattern.setSummary(this.mAccountVibratePattern.getEntry());
        this.mAccountVibratePattern.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                String summary = newValue.toString();
                AccountSettings.this.mAccountVibratePattern.setSummary(AccountSettings.this.mAccountVibratePattern.getEntries()[AccountSettings.this.mAccountVibratePattern.findIndexOfValue(summary)]);
                AccountSettings.this.mAccountVibratePattern.setValue(summary);
                AccountSettings.this.doVibrateTest(preference);
                return false;
            }
        });
        this.mAccountVibrateTimes = (ListPreference) findPreference(PREFERENCE_VIBRATE_TIMES);
        this.mAccountVibrateTimes.setValue(String.valueOf(this.mAccount.getNotificationSetting().getVibrateTimes()));
        this.mAccountVibrateTimes.setSummary(String.valueOf(this.mAccount.getNotificationSetting().getVibrateTimes()));
        this.mAccountVibrateTimes.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                String value = newValue.toString();
                AccountSettings.this.mAccountVibrateTimes.setSummary(value);
                AccountSettings.this.mAccountVibrateTimes.setValue(value);
                AccountSettings.this.doVibrateTest(preference);
                return false;
            }
        });
        this.mAccountLed = (CheckBoxPreference) findPreference(PREFERENCE_NOTIFICATION_LED);
        this.mAccountLed.setChecked(this.mAccount.getNotificationSetting().isLed());
        this.mNotificationOpensUnread = (CheckBoxPreference) findPreference(PREFERENCE_NOTIFICATION_OPENS_UNREAD);
        this.mNotificationOpensUnread.setChecked(this.mAccount.goToUnreadMessageSearch());
        new PopulateFolderPrefsTask().execute(new Void[0]);
        this.mChipColor = findPreference(PREFERENCE_CHIP_COLOR);
        this.mChipColor.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                AccountSettings.this.onChooseChipColor();
                return false;
            }
        });
        this.mLedColor = findPreference(PREFERENCE_LED_COLOR);
        this.mLedColor.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                AccountSettings.this.onChooseLedColor();
                return false;
            }
        });
        findPreference(PREFERENCE_COMPOSITION).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                AccountSettings.this.onCompositionSettings();
                return true;
            }
        });
        findPreference(PREFERENCE_MANAGE_IDENTITIES).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                AccountSettings.this.onManageIdentities();
                return true;
            }
        });
        findPreference(PREFERENCE_INCOMING).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                boolean unused = AccountSettings.this.mIncomingChanged = true;
                AccountSettings.this.onIncomingSettings();
                return true;
            }
        });
        findPreference(PREFERENCE_OUTGOING).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                AccountSettings.this.onOutgoingSettings();
                return true;
            }
        });
        this.mHasCrypto = OpenPgpUtils.isAvailable(this);
        if (this.mHasCrypto) {
            this.mCryptoApp = (OpenPgpAppPreference) findPreference(PREFERENCE_CRYPTO_APP);
            this.mCryptoKey = (OpenPgpKeyPreference) findPreference(PREFERENCE_CRYPTO_KEY);
            this.mCryptoApp.setValue(String.valueOf(this.mAccount.getCryptoApp()));
            this.mCryptoApp.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    String value = newValue.toString();
                    AccountSettings.this.mCryptoApp.setValue(value);
                    AccountSettings.this.mCryptoKey.setOpenPgpProvider(value);
                    return false;
                }
            });
            this.mCryptoKey.setValue(this.mAccount.getCryptoKey());
            this.mCryptoKey.setOpenPgpProvider(this.mCryptoApp.getValue());
            this.mCryptoKey.setDefaultUserId(OpenPgpApiHelper.buildUserId(this.mAccount.getIdentity(0)));
            this.mCryptoKey.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    AccountSettings.this.mCryptoKey.setValue(((Long) newValue).longValue());
                    return false;
                }
            });
            return;
        }
        Preference mCryptoMenu = findPreference(PREFERENCE_CRYPTO);
        mCryptoMenu.setEnabled(false);
        mCryptoMenu.setSummary(R.string.account_settings_no_openpgp_provider_installed);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v0, resolved type: java.lang.String[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v0, resolved type: java.lang.String[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void removeListEntry(android.preference.ListPreference r9, java.lang.String r10) {
        /*
            r8 = this;
            java.lang.CharSequence[] r1 = r9.getEntryValues()
            java.lang.CharSequence[] r0 = r9.getEntries()
            int r7 = r1.length
            int r7 = r7 + -1
            java.lang.String[] r4 = new java.lang.String[r7]
            int r7 = r1.length
            int r7 = r7 + -1
            java.lang.String[] r3 = new java.lang.String[r7]
            r2 = 0
            r5 = 0
        L_0x0014:
            int r7 = r1.length
            if (r2 >= r7) goto L_0x002a
            r6 = r1[r2]
            boolean r7 = r6.equals(r10)
            if (r7 != 0) goto L_0x0027
            r4[r5] = r6
            r7 = r0[r2]
            r3[r5] = r7
            int r5 = r5 + 1
        L_0x0027:
            int r2 = r2 + 1
            goto L_0x0014
        L_0x002a:
            r9.setEntryValues(r4)
            r9.setEntries(r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fsck.k9.activity.setup.AccountSettings.removeListEntry(android.preference.ListPreference, java.lang.String):void");
    }

    private void saveSettings() {
        if (this.mAccountDefault.isChecked()) {
            Preferences.getPreferences(this).setDefaultAccount(this.mAccount);
        }
        this.mAccount.setDescription(this.mAccountDescription.getText());
        this.mAccount.setMarkMessageAsReadOnView(this.mMarkMessageAsReadOnView.isChecked());
        this.mAccount.setNotifyNewMail(this.mAccountNotify.isChecked());
        this.mAccount.setFolderNotifyNewMailMode(Account.FolderMode.valueOf(this.mAccountNotifyNewMailMode.getValue()));
        this.mAccount.setNotifySelfNewMail(this.mAccountNotifySelf.isChecked());
        this.mAccount.setNotifyContactsMailOnly(this.mAccountNotifyContactsMailOnly.isChecked());
        this.mAccount.setShowOngoing(this.mAccountNotifySync.isChecked());
        this.mAccount.setDisplayCount(Integer.parseInt(this.mDisplayCount.getValue()));
        this.mAccount.setMaximumAutoDownloadMessageSize(Integer.parseInt(this.mMessageSize.getValue()));
        if (this.mAccount.isSearchByDateCapable()) {
            this.mAccount.setMaximumPolledMessageAge(Integer.parseInt(this.mMessageAge.getValue()));
        }
        this.mAccount.getNotificationSetting().setVibrate(this.mAccountVibrate.isChecked());
        this.mAccount.getNotificationSetting().setVibratePattern(Integer.parseInt(this.mAccountVibratePattern.getValue()));
        this.mAccount.getNotificationSetting().setVibrateTimes(Integer.parseInt(this.mAccountVibrateTimes.getValue()));
        this.mAccount.getNotificationSetting().setLed(this.mAccountLed.isChecked());
        this.mAccount.setGoToUnreadMessageSearch(this.mNotificationOpensUnread.isChecked());
        this.mAccount.setFolderTargetMode(Account.FolderMode.valueOf(this.mTargetMode.getValue()));
        this.mAccount.setDeletePolicy(Account.DeletePolicy.fromInt(Integer.parseInt(this.mDeletePolicy.getValue())));
        if (this.mIsExpungeCapable) {
            this.mAccount.setExpungePolicy(Account.Expunge.valueOf(this.mExpungePolicy.getValue()));
        }
        this.mAccount.setSyncRemoteDeletions(this.mSyncRemoteDeletions.isChecked());
        this.mAccount.setSearchableFolders(Account.Searchable.valueOf(this.mSearchableFolders.getValue()));
        this.mAccount.setMessageFormat(Account.MessageFormat.valueOf(this.mMessageFormat.getValue()));
        this.mAccount.setAlwaysShowCcBcc(this.mAlwaysShowCcBcc.isChecked());
        this.mAccount.setMessageReadReceipt(this.mMessageReadReceipt.isChecked());
        this.mAccount.setQuoteStyle(Account.QuoteStyle.valueOf(this.mQuoteStyle.getValue()));
        this.mAccount.setQuotePrefix(this.mAccountQuotePrefix.getText());
        this.mAccount.setDefaultQuotedTextShown(this.mAccountDefaultQuotedTextShown.isChecked());
        this.mAccount.setReplyAfterQuote(this.mReplyAfterQuote.isChecked());
        this.mAccount.setStripSignature(this.mStripSignature.isChecked());
        this.mAccount.setLocalStorageProviderId(this.mLocalStorageProvider.getValue());
        if (this.mHasCrypto) {
            this.mAccount.setCryptoApp(this.mCryptoApp.getValue());
            this.mAccount.setCryptoKey(this.mCryptoKey.getValue());
        } else {
            this.mAccount.setCryptoApp("");
            this.mAccount.setCryptoKey(0);
        }
        if (this.mAccount.getStoreUri().startsWith("webdav")) {
            this.mAccount.setAutoExpandFolderName(this.mAutoExpandFolder.getValue());
        } else {
            this.mAccount.setAutoExpandFolderName(reverseTranslateFolder(this.mAutoExpandFolder.getValue()));
        }
        if (this.mIsMoveCapable) {
            this.mAccount.setArchiveFolderName(this.mArchiveFolder.getValue());
            this.mAccount.setDraftsFolderName(this.mDraftsFolder.getValue());
            this.mAccount.setSentFolderName(this.mSentFolder.getValue());
            this.mAccount.setSpamFolderName(this.mSpamFolder.getValue());
            this.mAccount.setTrashFolderName(this.mTrashFolder.getValue());
        }
        if (this.mIsPushCapable) {
            this.mAccount.setPushPollOnConnect(this.mPushPollOnConnect.isChecked());
            this.mAccount.setIdleRefreshMinutes(Integer.parseInt(this.mIdleRefreshPeriod.getValue()));
            this.mAccount.setMaxPushFolders(Integer.parseInt(this.mMaxPushFolders.getValue()));
            this.mAccount.setAllowRemoteSearch(this.mCloudSearchEnabled.isChecked());
            this.mAccount.setRemoteSearchNumResults(Integer.parseInt(this.mRemoteSearchNumResults.getValue()));
            this.mAccount.setRemoteSearchFullText(this.mRemoteSearchFullText.isChecked());
        }
        boolean needsRefresh = this.mAccount.setAutomaticCheckIntervalMinutes(Integer.parseInt(this.mCheckFrequency.getValue())) | this.mAccount.setFolderSyncMode(Account.FolderMode.valueOf(this.mSyncMode.getValue()));
        boolean displayModeChanged = this.mAccount.setFolderDisplayMode(Account.FolderMode.valueOf(this.mDisplayMode.getValue()));
        String newRingtone = this.mAccountRingtone.getPreferenceManager().getSharedPreferences().getString(PREFERENCE_RINGTONE, null);
        if (newRingtone != null) {
            this.mAccount.getNotificationSetting().setRing(true);
            this.mAccount.getNotificationSetting().setRingtone(newRingtone);
        } else if (this.mAccount.getNotificationSetting().shouldRing()) {
            this.mAccount.getNotificationSetting().setRingtone(null);
        }
        this.mAccount.setShowPictures(Account.ShowPictures.valueOf(this.mAccountShowPictures.getValue()));
        if (this.mIsPushCapable) {
            boolean needsPushRestart = this.mAccount.setFolderPushMode(Account.FolderMode.valueOf(this.mPushMode.getValue()));
            if (this.mAccount.getFolderPushMode() != Account.FolderMode.NONE) {
                needsPushRestart = needsPushRestart | displayModeChanged | this.mIncomingChanged;
            }
            if (needsRefresh && needsPushRestart) {
                MailService.actionReset(this, null);
            } else if (needsRefresh) {
                MailService.actionReschedulePoll(this, null);
            } else if (needsPushRestart) {
                MailService.actionRestartPushers(this, null);
            }
        }
        this.mAccount.save(Preferences.getPreferences(this));
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (this.mCryptoKey == null || !this.mCryptoKey.handleOnActivityResult(requestCode, resultCode, data)) {
            if (resultCode == -1) {
                switch (requestCode) {
                    case 1:
                        this.mAutoExpandFolder.setSummary(translateFolder(data.getStringExtra(ChooseFolder.EXTRA_NEW_FOLDER)));
                        break;
                }
            }
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        saveSettings();
        super.onPause();
    }

    /* access modifiers changed from: private */
    public void onCompositionSettings() {
        AccountSetupComposition.actionEditCompositionSettings(this, this.mAccount);
    }

    /* access modifiers changed from: private */
    public void onManageIdentities() {
        Intent intent = new Intent(this, ManageIdentities.class);
        intent.putExtra(ChooseIdentity.EXTRA_ACCOUNT, this.mAccount.getUuid());
        startActivityForResult(intent, 2);
    }

    /* access modifiers changed from: private */
    public void onIncomingSettings() {
        AccountSetupIncoming.actionEditIncomingSettings(this, this.mAccount);
    }

    /* access modifiers changed from: private */
    public void onOutgoingSettings() {
        AccountSetupOutgoing.actionEditOutgoingSettings(this, this.mAccount);
    }

    public void onChooseChipColor() {
        showDialog(1);
    }

    public void onChooseLedColor() {
        showDialog(2);
    }

    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                return new ColorPickerDialog(this, new ColorPickerDialog.OnColorChangedListener() {
                    public void colorChanged(int color) {
                        AccountSettings.this.mAccount.setChipColor(color);
                    }
                }, this.mAccount.getChipColor());
            case 2:
                return new ColorPickerDialog(this, new ColorPickerDialog.OnColorChangedListener() {
                    public void colorChanged(int color) {
                        AccountSettings.this.mAccount.getNotificationSetting().setLedColor(color);
                    }
                }, this.mAccount.getNotificationSetting().getLedColor());
            default:
                return null;
        }
    }

    public void onPrepareDialog(int id, Dialog dialog) {
        switch (id) {
            case 1:
                ((ColorPickerDialog) dialog).setColor(this.mAccount.getChipColor());
                return;
            case 2:
                ((ColorPickerDialog) dialog).setColor(this.mAccount.getNotificationSetting().getLedColor());
                return;
            default:
                return;
        }
    }

    public void onChooseAutoExpandFolder() {
        Intent selectIntent = new Intent(this, ChooseFolder.class);
        selectIntent.putExtra(ChooseFolder.EXTRA_ACCOUNT, this.mAccount.getUuid());
        selectIntent.putExtra(ChooseFolder.EXTRA_CUR_FOLDER, this.mAutoExpandFolder.getSummary());
        selectIntent.putExtra(ChooseFolder.EXTRA_SHOW_CURRENT, "yes");
        selectIntent.putExtra(ChooseFolder.EXTRA_SHOW_FOLDER_NONE, "yes");
        selectIntent.putExtra(ChooseFolder.EXTRA_SHOW_DISPLAYABLE_ONLY, "yes");
        startActivityForResult(selectIntent, 1);
    }

    private String translateFolder(String in) {
        if (this.mAccount.getInboxFolderName().equalsIgnoreCase(in)) {
            return getString(R.string.special_mailbox_name_inbox);
        }
        return in;
    }

    private String reverseTranslateFolder(String in) {
        if (getString(R.string.special_mailbox_name_inbox).equals(in)) {
            return this.mAccount.getInboxFolderName();
        }
        return in;
    }

    /* access modifiers changed from: private */
    public void doVibrateTest(Preference preference) {
        ((Vibrator) preference.getContext().getSystemService("vibrator")).vibrate(NotificationSetting.getVibration(Integer.parseInt(this.mAccountVibratePattern.getValue()), Integer.parseInt(this.mAccountVibrateTimes.getValue())), -1);
    }

    /* access modifiers changed from: private */
    public void updateRemoteSearchLimit(String maxResults) {
        if (maxResults != null) {
            if (maxResults.equals("0")) {
                maxResults = getString(R.string.account_settings_remote_search_num_results_entries_all);
            }
            this.mRemoteSearchNumResults.setSummary(String.format(getString(R.string.account_settings_remote_search_num_summary), maxResults));
        }
    }

    private class PopulateFolderPrefsTask extends AsyncTask<Void, Void, Void> {
        String[] allFolderLabels;
        String[] allFolderValues;
        List<? extends Folder> folders;

        private PopulateFolderPrefsTask() {
            this.folders = new LinkedList();
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... params) {
            try {
                this.folders = AccountSettings.this.mAccount.getLocalStore().getPersonalNamespaces(false);
            } catch (Exception e) {
            }
            Iterator<? extends Folder> iter = this.folders.iterator();
            while (iter.hasNext()) {
                if (AccountSettings.this.mAccount.getOutboxFolderName().equals(((Folder) iter.next()).getName())) {
                    iter.remove();
                }
            }
            this.allFolderValues = new String[(this.folders.size() + 1)];
            this.allFolderLabels = new String[(this.folders.size() + 1)];
            this.allFolderValues[0] = K9.FOLDER_NONE;
            this.allFolderLabels[0] = K9.FOLDER_NONE;
            int i = 1;
            for (Folder folder : this.folders) {
                this.allFolderLabels[i] = folder.getName();
                this.allFolderValues[i] = folder.getName();
                i++;
            }
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            ListPreference unused = AccountSettings.this.mAutoExpandFolder = (ListPreference) AccountSettings.this.findPreference(AccountSettings.PREFERENCE_AUTO_EXPAND_FOLDER);
            AccountSettings.this.mAutoExpandFolder.setEnabled(false);
            ListPreference unused2 = AccountSettings.this.mArchiveFolder = (ListPreference) AccountSettings.this.findPreference(AccountSettings.PREFERENCE_ARCHIVE_FOLDER);
            AccountSettings.this.mArchiveFolder.setEnabled(false);
            ListPreference unused3 = AccountSettings.this.mDraftsFolder = (ListPreference) AccountSettings.this.findPreference(AccountSettings.PREFERENCE_DRAFTS_FOLDER);
            AccountSettings.this.mDraftsFolder.setEnabled(false);
            ListPreference unused4 = AccountSettings.this.mSentFolder = (ListPreference) AccountSettings.this.findPreference(AccountSettings.PREFERENCE_SENT_FOLDER);
            AccountSettings.this.mSentFolder.setEnabled(false);
            ListPreference unused5 = AccountSettings.this.mSpamFolder = (ListPreference) AccountSettings.this.findPreference(AccountSettings.PREFERENCE_SPAM_FOLDER);
            AccountSettings.this.mSpamFolder.setEnabled(false);
            ListPreference unused6 = AccountSettings.this.mTrashFolder = (ListPreference) AccountSettings.this.findPreference(AccountSettings.PREFERENCE_TRASH_FOLDER);
            AccountSettings.this.mTrashFolder.setEnabled(false);
            if (!AccountSettings.this.mIsMoveCapable) {
                PreferenceScreen foldersCategory = (PreferenceScreen) AccountSettings.this.findPreference("folders");
                foldersCategory.removePreference(AccountSettings.this.mArchiveFolder);
                foldersCategory.removePreference(AccountSettings.this.mSpamFolder);
                foldersCategory.removePreference(AccountSettings.this.mDraftsFolder);
                foldersCategory.removePreference(AccountSettings.this.mSentFolder);
                foldersCategory.removePreference(AccountSettings.this.mTrashFolder);
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Void res) {
            AccountSettings.this.initListPreference(AccountSettings.this.mAutoExpandFolder, AccountSettings.this.mAccount.getAutoExpandFolderName(), this.allFolderLabels, this.allFolderValues);
            AccountSettings.this.mAutoExpandFolder.setEnabled(true);
            if (AccountSettings.this.mIsMoveCapable) {
                AccountSettings.this.initListPreference(AccountSettings.this.mArchiveFolder, AccountSettings.this.mAccount.getArchiveFolderName(), this.allFolderLabels, this.allFolderValues);
                AccountSettings.this.initListPreference(AccountSettings.this.mDraftsFolder, AccountSettings.this.mAccount.getDraftsFolderName(), this.allFolderLabels, this.allFolderValues);
                AccountSettings.this.initListPreference(AccountSettings.this.mSentFolder, AccountSettings.this.mAccount.getSentFolderName(), this.allFolderLabels, this.allFolderValues);
                AccountSettings.this.initListPreference(AccountSettings.this.mSpamFolder, AccountSettings.this.mAccount.getSpamFolderName(), this.allFolderLabels, this.allFolderValues);
                AccountSettings.this.initListPreference(AccountSettings.this.mTrashFolder, AccountSettings.this.mAccount.getTrashFolderName(), this.allFolderLabels, this.allFolderValues);
                AccountSettings.this.mArchiveFolder.setEnabled(true);
                AccountSettings.this.mSpamFolder.setEnabled(true);
                AccountSettings.this.mDraftsFolder.setEnabled(true);
                AccountSettings.this.mSentFolder.setEnabled(true);
                AccountSettings.this.mTrashFolder.setEnabled(true);
            }
        }
    }
}
