package com.fsck.k9.activity.misc;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;

public class SwipeGestureDetector extends GestureDetector.SimpleOnGestureListener {
    public static final int BEZEL_SWIPE_THRESHOLD = 20;
    private static final float SWIPE_MAX_OFF_PATH_DIP = 250.0f;
    private static final float SWIPE_THRESHOLD_VELOCITY_DIP = 325.0f;
    private MotionEvent mLastOnDownEvent = null;
    private final OnSwipeGestureListener mListener;
    private int mMaxOffPath;
    private int mMinVelocity;

    public interface OnSwipeGestureListener {
        void onSwipeLeftToRight(MotionEvent motionEvent, MotionEvent motionEvent2);

        void onSwipeRightToLeft(MotionEvent motionEvent, MotionEvent motionEvent2);
    }

    public SwipeGestureDetector(Context context, OnSwipeGestureListener listener) {
        if (listener == null) {
            throw new IllegalArgumentException("'listener' may not be null");
        }
        this.mListener = listener;
        float gestureScale = context.getResources().getDisplayMetrics().density;
        this.mMinVelocity = (int) ((SWIPE_THRESHOLD_VELOCITY_DIP * gestureScale) + 0.5f);
        this.mMaxOffPath = (int) ((SWIPE_MAX_OFF_PATH_DIP * gestureScale) + 0.5f);
    }

    public boolean onDown(MotionEvent e) {
        this.mLastOnDownEvent = e;
        return super.onDown(e);
    }

    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        if (e1 == null) {
            e1 = this.mLastOnDownEvent;
        }
        if (!(e1 == null || e2 == null)) {
            float deltaX = e2.getX() - e1.getX();
            float deltaY = e2.getY() - e1.getY();
            int minDistance = (int) Math.abs(4.0f * deltaY);
            try {
                if (Math.abs(deltaY) <= ((float) this.mMaxOffPath) && Math.abs(velocityX) >= ((float) this.mMinVelocity)) {
                    if (deltaX < ((float) (minDistance * -1))) {
                        this.mListener.onSwipeRightToLeft(e1, e2);
                    } else if (deltaX > ((float) minDistance)) {
                        this.mListener.onSwipeLeftToRight(e1, e2);
                    }
                    e2.setAction(3);
                }
            } catch (Exception e) {
            }
        }
        return false;
    }
}
