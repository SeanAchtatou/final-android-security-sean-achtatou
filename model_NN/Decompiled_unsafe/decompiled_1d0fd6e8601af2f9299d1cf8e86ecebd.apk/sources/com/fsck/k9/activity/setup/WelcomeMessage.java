package com.fsck.k9.activity.setup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.TextView;
import com.fsck.k9.activity.Accounts;
import com.fsck.k9.activity.K9Activity;
import com.fsck.k9.helper.HtmlConverter;
import yahoo.mail.app.R;

public class WelcomeMessage extends K9Activity implements View.OnClickListener {
    public static void showWelcomeMessage(Context context) {
        context.startActivity(new Intent(context, WelcomeMessage.class));
    }

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView((int) R.layout.welcome_message);
        TextView welcome = (TextView) findViewById(R.id.welcome_message);
        welcome.setText(HtmlConverter.htmlToSpanned(getString(R.string.accounts_welcome)));
        welcome.setMovementMethod(LinkMovementMethod.getInstance());
        findViewById(R.id.next).setOnClickListener(this);
        findViewById(R.id.import_settings).setOnClickListener(this);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.next /*2131755153*/:
                AccountSetupBasics.actionNewAccount(this);
                finish();
                return;
            case R.id.import_settings /*2131755435*/:
                Accounts.importSettings(this);
                finish();
                return;
            default:
                return;
        }
    }
}
