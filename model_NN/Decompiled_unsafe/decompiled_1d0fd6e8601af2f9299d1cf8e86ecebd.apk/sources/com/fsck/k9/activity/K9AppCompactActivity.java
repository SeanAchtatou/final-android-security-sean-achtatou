package com.fsck.k9.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import com.fsck.k9.activity.K9ActivityCommon;
import com.fsck.k9.activity.misc.SwipeGestureDetector;

public class K9AppCompactActivity extends AppCompatActivity implements K9ActivityCommon.K9ActivityMagic {
    private K9ActivityCommon mBase;

    public void onCreate(Bundle savedInstanceState) {
        this.mBase = K9ActivityCommon.newInstance(this);
        super.onCreate(savedInstanceState);
    }

    public boolean dispatchTouchEvent(MotionEvent event) {
        this.mBase.preDispatchTouchEvent(event);
        return super.dispatchTouchEvent(event);
    }

    public void setupGestureDetector(SwipeGestureDetector.OnSwipeGestureListener listener) {
        this.mBase.setupGestureDetector(listener);
    }
}
