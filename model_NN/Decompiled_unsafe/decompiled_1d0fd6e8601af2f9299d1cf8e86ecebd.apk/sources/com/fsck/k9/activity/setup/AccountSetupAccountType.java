package com.fsck.k9.activity.setup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import com.fsck.k9.Account;
import com.fsck.k9.Preferences;
import com.fsck.k9.activity.K9Activity;
import com.fsck.k9.helper.EmailHelper;
import com.fsck.k9.mail.ServerSettings;
import com.fsck.k9.setup.ServerNameSuggester;
import java.net.URI;
import java.net.URISyntaxException;
import yahoo.mail.app.R;

public class AccountSetupAccountType extends K9Activity implements View.OnClickListener {
    private static final String EXTRA_ACCOUNT = "account";
    private static final String EXTRA_MAKE_DEFAULT = "makeDefault";
    private Account mAccount;
    private boolean mMakeDefault;
    private final ServerNameSuggester serverNameSuggester = new ServerNameSuggester();

    public static void actionSelectAccountType(Context context, Account account, boolean makeDefault) {
        Intent i = new Intent(context, AccountSetupAccountType.class);
        i.putExtra("account", account.getUuid());
        i.putExtra(EXTRA_MAKE_DEFAULT, makeDefault);
        context.startActivity(i);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.account_setup_account_type);
        findViewById(R.id.pop).setOnClickListener(this);
        findViewById(R.id.imap).setOnClickListener(this);
        findViewById(R.id.webdav).setOnClickListener(this);
        this.mAccount = Preferences.getPreferences(this).getAccount(getIntent().getStringExtra("account"));
        this.mMakeDefault = getIntent().getBooleanExtra(EXTRA_MAKE_DEFAULT, false);
    }

    private void setupStoreAndSmtpTransport(ServerSettings.Type serverType, String schemePrefix) throws URISyntaxException {
        String domainPart = EmailHelper.getDomainFromEmailAddress(this.mAccount.getEmail());
        String suggestedStoreServerName = this.serverNameSuggester.suggestServerName(serverType, domainPart);
        URI storeUriForDecode = new URI(this.mAccount.getStoreUri());
        this.mAccount.setStoreUri(new URI(schemePrefix, storeUriForDecode.getUserInfo(), suggestedStoreServerName, storeUriForDecode.getPort(), null, null, null).toString());
        String suggestedTransportServerName = this.serverNameSuggester.suggestServerName(ServerSettings.Type.SMTP, domainPart);
        URI transportUriForDecode = new URI(this.mAccount.getTransportUri());
        this.mAccount.setTransportUri(new URI("smtp+tls+", transportUriForDecode.getUserInfo(), suggestedTransportServerName, transportUriForDecode.getPort(), null, null, null).toString());
    }

    private void setupDav() throws URISyntaxException {
        URI uriForDecode = new URI(this.mAccount.getStoreUri());
        String userPass = "";
        String[] userInfo = uriForDecode.getUserInfo().split(":");
        if (userInfo.length > 1) {
            userPass = userInfo[1];
        }
        if (userInfo.length > 2) {
            userPass = userPass + ":" + userInfo[2];
        }
        this.mAccount.setStoreUri(new URI("webdav+ssl+", userPass, this.serverNameSuggester.suggestServerName(ServerSettings.Type.WebDAV, EmailHelper.getDomainFromEmailAddress(this.mAccount.getEmail())), uriForDecode.getPort(), null, null, null).toString());
    }

    public void onClick(View v) {
        try {
            switch (v.getId()) {
                case R.id.imap /*2131755141*/:
                    setupStoreAndSmtpTransport(ServerSettings.Type.IMAP, "imap+ssl+");
                    break;
                case R.id.pop /*2131755142*/:
                    setupStoreAndSmtpTransport(ServerSettings.Type.POP3, "pop3+ssl+");
                    break;
                case R.id.webdav /*2131755143*/:
                    setupDav();
                    break;
            }
        } catch (Exception ex) {
            failure(ex);
        }
        AccountSetupIncoming.actionIncomingSettings(this, this.mAccount, this.mMakeDefault, true);
        finish();
    }

    private void failure(Exception use) {
        Log.e("k9", "Failure", use);
        Toast.makeText(getApplication(), getString(R.string.account_setup_bad_uri, new Object[]{use.getMessage()}), 1).show();
    }
}
