package com.fsck.k9.activity;

import android.content.Intent;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;
import com.fsck.k9.Identity;
import com.fsck.k9.Preferences;
import yahoo.mail.app.R;

public class ManageIdentities extends ChooseIdentity {
    private static final int ACTIVITY_EDIT_IDENTITY = 1;
    private boolean mIdentitiesChanged = false;

    /* access modifiers changed from: protected */
    public void setupClickListeners() {
        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                ManageIdentities.this.editItem(position);
            }
        });
        registerForContextMenu(getListView());
    }

    /* access modifiers changed from: private */
    public void editItem(int i) {
        Intent intent = new Intent(this, EditIdentity.class);
        intent.putExtra(EditIdentity.EXTRA_ACCOUNT, this.mAccount.getUuid());
        intent.putExtra(EditIdentity.EXTRA_IDENTITY, this.mAccount.getIdentity(i));
        intent.putExtra(EditIdentity.EXTRA_IDENTITY_INDEX, i);
        startActivityForResult(intent, 1);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.manage_identities_option, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.new_identity /*2131755469*/:
                Intent intent = new Intent(this, EditIdentity.class);
                intent.putExtra(EditIdentity.EXTRA_ACCOUNT, this.mAccount.getUuid());
                startActivityForResult(intent, 1);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle((int) R.string.manage_identities_context_menu_title);
        getMenuInflater().inflate(R.menu.manage_identities_context, menu);
    }

    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo menuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.up /*2131755029*/:
                if (menuInfo.position > 0) {
                    this.identities.add(menuInfo.position - 1, (Identity) this.identities.remove(menuInfo.position));
                    this.mIdentitiesChanged = true;
                    refreshView();
                    break;
                }
                break;
            case R.id.top /*2131755064*/:
                this.identities.add(0, (Identity) this.identities.remove(menuInfo.position));
                this.mIdentitiesChanged = true;
                refreshView();
                break;
            case R.id.edit /*2131755466*/:
                editItem(menuInfo.position);
                break;
            case R.id.down /*2131755467*/:
                if (menuInfo.position < this.identities.size() - 1) {
                    this.identities.add(menuInfo.position + 1, (Identity) this.identities.remove(menuInfo.position));
                    this.mIdentitiesChanged = true;
                    refreshView();
                    break;
                }
                break;
            case R.id.remove /*2131755468*/:
                if (this.identities.size() <= 1) {
                    Toast.makeText(this, getString(R.string.no_removable_identity), 1).show();
                    break;
                } else {
                    this.identities.remove(menuInfo.position);
                    this.mIdentitiesChanged = true;
                    refreshView();
                    break;
                }
        }
        return true;
    }

    public void onResume() {
        super.onResume();
        refreshView();
    }

    public void onBackPressed() {
        saveIdentities();
        super.onBackPressed();
    }

    private void saveIdentities() {
        if (this.mIdentitiesChanged) {
            this.mAccount.setIdentities(this.identities);
            this.mAccount.save(Preferences.getPreferences(getApplication().getApplicationContext()));
        }
        finish();
    }
}
