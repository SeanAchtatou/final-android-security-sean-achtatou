package com.fsck.k9.activity.compose;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fsck.k9.activity.compose.RecipientPresenter;
import com.fsck.k9.view.CryptoModeSelector;
import com.fsck.k9.view.LinearViewAnimator;
import yahoo.mail.app.R;

public class CryptoSettingsDialog extends DialogFragment implements CryptoModeSelector.CryptoStatusSelectedListener {
    private static final String ARG_CURRENT_MODE = "current_override";
    private CryptoModeSelector cryptoModeSelector;
    private LinearViewAnimator cryptoStatusText;
    private RecipientPresenter.CryptoMode currentMode;

    public interface OnCryptoModeChangedListener {
        void onCryptoModeChanged(RecipientPresenter.CryptoMode cryptoMode);
    }

    public static CryptoSettingsDialog newInstance(RecipientPresenter.CryptoMode initialOverride) {
        CryptoSettingsDialog dialog = new CryptoSettingsDialog();
        Bundle args = new Bundle();
        args.putString(ARG_CURRENT_MODE, initialOverride.toString());
        dialog.setArguments(args);
        return dialog;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity()).inflate((int) R.layout.crypto_settings_dialog, (ViewGroup) null);
        this.cryptoModeSelector = (CryptoModeSelector) view.findViewById(R.id.crypto_status_selector);
        this.cryptoStatusText = (LinearViewAnimator) view.findViewById(R.id.crypto_status_text);
        this.cryptoModeSelector.setCryptoStatusListener(this);
        this.currentMode = RecipientPresenter.CryptoMode.valueOf((savedInstanceState != null ? savedInstanceState : getArguments()).getString(ARG_CURRENT_MODE));
        updateView(false);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
        builder.setPositiveButton((int) R.string.crypto_settings_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        return builder.create();
    }

    /* access modifiers changed from: package-private */
    public void updateView(boolean animate) {
        switch (this.currentMode) {
            case DISABLE:
                this.cryptoModeSelector.setCryptoStatus(0);
                this.cryptoStatusText.setDisplayedChild(0, animate);
                return;
            case SIGN_ONLY:
                this.cryptoModeSelector.setCryptoStatus(1);
                this.cryptoStatusText.setDisplayedChild(1, animate);
                return;
            case OPPORTUNISTIC:
                this.cryptoModeSelector.setCryptoStatus(2);
                this.cryptoStatusText.setDisplayedChild(2, animate);
                return;
            case PRIVATE:
                this.cryptoModeSelector.setCryptoStatus(3);
                this.cryptoStatusText.setDisplayedChild(3, animate);
                return;
            default:
                return;
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(ARG_CURRENT_MODE, this.currentMode.toString());
    }

    public void onCryptoStatusSelected(int status) {
        if (status == 0) {
            this.currentMode = RecipientPresenter.CryptoMode.DISABLE;
        } else if (status == 1) {
            this.currentMode = RecipientPresenter.CryptoMode.SIGN_ONLY;
        } else if (status == 2) {
            this.currentMode = RecipientPresenter.CryptoMode.OPPORTUNISTIC;
        } else {
            this.currentMode = RecipientPresenter.CryptoMode.PRIVATE;
        }
        updateView(true);
        Activity activity = getActivity();
        if (activity != null) {
            if (!(activity instanceof OnCryptoModeChangedListener)) {
                throw new AssertionError("This dialog must be called by an OnCryptoModeChangedListener!");
            }
            ((OnCryptoModeChangedListener) activity).onCryptoModeChanged(this.currentMode);
        }
    }
}
