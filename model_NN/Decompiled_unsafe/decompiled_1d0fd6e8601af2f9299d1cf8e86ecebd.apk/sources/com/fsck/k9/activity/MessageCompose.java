package com.fsck.k9.activity;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.annotation.StringRes;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.fsck.k9.Account;
import com.fsck.k9.FontSizes;
import com.fsck.k9.Identity;
import com.fsck.k9.K9;
import com.fsck.k9.Preferences;
import com.fsck.k9.activity.MessageLoaderHelper;
import com.fsck.k9.activity.compose.AttachmentPresenter;
import com.fsck.k9.activity.compose.CryptoSettingsDialog;
import com.fsck.k9.activity.compose.IdentityAdapter;
import com.fsck.k9.activity.compose.PgpInlineDialog;
import com.fsck.k9.activity.compose.RecipientMvpView;
import com.fsck.k9.activity.compose.RecipientPresenter;
import com.fsck.k9.activity.compose.SaveMessageTask;
import com.fsck.k9.activity.misc.Attachment;
import com.fsck.k9.controller.MessagingController;
import com.fsck.k9.controller.MessagingListener;
import com.fsck.k9.fragment.ProgressDialogFragment;
import com.fsck.k9.helper.Contacts;
import com.fsck.k9.helper.IdentityHelper;
import com.fsck.k9.helper.MailTo;
import com.fsck.k9.helper.ReplyToParser;
import com.fsck.k9.helper.SimpleTextWatcher;
import com.fsck.k9.mail.Address;
import com.fsck.k9.mail.Flag;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.internet.MimeMessage;
import com.fsck.k9.mailstore.LocalMessage;
import com.fsck.k9.mailstore.MessageViewInfo;
import com.fsck.k9.message.ComposePgpInlineDecider;
import com.fsck.k9.message.IdentityField;
import com.fsck.k9.message.IdentityHeaderParser;
import com.fsck.k9.message.MessageBuilder;
import com.fsck.k9.message.QuotedTextMode;
import com.fsck.k9.message.SimpleMessageFormat;
import com.fsck.k9.ui.EolConvertingEditText;
import com.fsck.k9.ui.compose.QuotedMessageMvpView;
import com.fsck.k9.ui.compose.QuotedMessagePresenter;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;
import yahoo.mail.app.R;

public class MessageCompose extends K9Activity implements View.OnClickListener, ProgressDialogFragment.CancelListener, View.OnFocusChangeListener, CryptoSettingsDialog.OnCryptoModeChangedListener, PgpInlineDialog.OnOpenPgpInlineChangeListener, MessageBuilder.Callback {
    public static final String ACTION_COMPOSE = "com.fsck.k9.intent.action.COMPOSE";
    public static final String ACTION_EDIT_DRAFT = "com.fsck.k9.intent.action.EDIT_DRAFT";
    public static final String ACTION_FORWARD = "com.fsck.k9.intent.action.FORWARD";
    public static final String ACTION_REPLY = "com.fsck.k9.intent.action.REPLY";
    public static final String ACTION_REPLY_ALL = "com.fsck.k9.intent.action.REPLY_ALL";
    private static final int DIALOG_CHOOSE_IDENTITY = 3;
    private static final int DIALOG_CONFIRM_DISCARD = 4;
    private static final int DIALOG_CONFIRM_DISCARD_ON_BACK = 2;
    private static final int DIALOG_SAVE_OR_DISCARD_DRAFT_MESSAGE = 1;
    public static final String EXTRA_ACCOUNT = "account";
    public static final String EXTRA_MESSAGE_DECRYPTION_RESULT = "message_decryption_result";
    public static final String EXTRA_MESSAGE_REFERENCE = "message_reference";
    private static final String FRAGMENT_WAITING_FOR_ATTACHMENT = "waitingForAttachment";
    private static final long INVALID_DRAFT_ID = -1;
    private static final int MSG_DISCARDED_DRAFT = 5;
    private static final int MSG_PROGRESS_OFF = 2;
    private static final int MSG_PROGRESS_ON = 1;
    public static final int MSG_SAVED_DRAFT = 4;
    private static final Pattern PREFIX = Pattern.compile("^AW[:\\s]\\s*", 2);
    private static final int REQUEST_MASK_ATTACHMENT_PRESENTER = 1024;
    private static final int REQUEST_MASK_LOADER_HELPER = 512;
    private static final int REQUEST_MASK_MESSAGE_BUILDER = 2048;
    private static final int REQUEST_MASK_RECIPIENT_PRESENTER = 256;
    private static final String STATE_ALREADY_NOTIFIED_USER_OF_EMPTY_SUBJECT = "alreadyNotifiedUserOfEmptySubject";
    private static final String STATE_IDENTITY = "com.fsck.k9.activity.MessageCompose.identity";
    private static final String STATE_IDENTITY_CHANGED = "com.fsck.k9.activity.MessageCompose.identityChanged";
    private static final String STATE_IN_REPLY_TO = "com.fsck.k9.activity.MessageCompose.inReplyTo";
    private static final String STATE_KEY_DRAFT_ID = "com.fsck.k9.activity.MessageCompose.draftId";
    private static final String STATE_KEY_DRAFT_NEEDS_SAVING = "com.fsck.k9.activity.MessageCompose.draftNeedsSaving";
    private static final String STATE_KEY_READ_RECEIPT = "com.fsck.k9.activity.MessageCompose.messageReadReceipt";
    private static final String STATE_KEY_SOURCE_MESSAGE_PROCED = "com.fsck.k9.activity.MessageCompose.stateKeySourceMessageProced";
    private static final String STATE_REFERENCES = "com.fsck.k9.activity.MessageCompose.references";
    private boolean alreadyNotifiedUserOfEmptySubject = false;
    AttachmentPresenter.AttachmentMvpView attachmentMvpView = new AttachmentPresenter.AttachmentMvpView() {
        private HashMap<Uri, View> attachmentViews = new HashMap<>();

        public void showWaitingForAttachmentDialog(AttachmentPresenter.WaitingAction waitingAction) {
            String title;
            switch (AnonymousClass14.$SwitchMap$com$fsck$k9$activity$compose$AttachmentPresenter$WaitingAction[waitingAction.ordinal()]) {
                case 1:
                    title = MessageCompose.this.getString(R.string.fetching_attachment_dialog_title_send);
                    break;
                case 2:
                    title = MessageCompose.this.getString(R.string.fetching_attachment_dialog_title_save);
                    break;
                default:
                    return;
            }
            ProgressDialogFragment.newInstance(title, MessageCompose.this.getString(R.string.fetching_attachment_dialog_message)).show(MessageCompose.this.getFragmentManager(), MessageCompose.FRAGMENT_WAITING_FOR_ATTACHMENT);
        }

        public void dismissWaitingForAttachmentDialog() {
            ProgressDialogFragment fragment = (ProgressDialogFragment) MessageCompose.this.getFragmentManager().findFragmentByTag(MessageCompose.FRAGMENT_WAITING_FOR_ATTACHMENT);
            if (fragment != null) {
                fragment.dismiss();
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
        @SuppressLint({"InlinedApi"})
        public void showPickAttachmentDialog(int requestCode) {
            Intent i = new Intent("android.intent.action.GET_CONTENT");
            i.putExtra("android.intent.extra.ALLOW_MULTIPLE", true);
            i.addCategory("android.intent.category.OPENABLE");
            i.setType("*/*");
            boolean unused = MessageCompose.this.isInSubActivity = true;
            MessageCompose.this.startActivityForResult(Intent.createChooser(i, null), requestCode | 1024);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.widget.LinearLayout, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public void addAttachmentView(final Attachment attachment) {
            View view = MessageCompose.this.getLayoutInflater().inflate((int) R.layout.message_compose_attachment, (ViewGroup) MessageCompose.this.mAttachments, false);
            this.attachmentViews.put(attachment.uri, view);
            view.findViewById(R.id.attachment_delete).setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    MessageCompose.this.attachmentPresenter.onClickRemoveAttachment(attachment.uri);
                }
            });
            updateAttachmentView(attachment);
            MessageCompose.this.mAttachments.addView(view);
        }

        public void updateAttachmentView(Attachment attachment) {
            boolean hasMetadata;
            boolean isLoadingComplete = true;
            int i = 0;
            View view = this.attachmentViews.get(attachment.uri);
            if (view == null) {
                throw new IllegalArgumentException();
            }
            TextView nameView = (TextView) view.findViewById(R.id.attachment_name);
            if (attachment.state != Attachment.LoadingState.URI_ONLY) {
                hasMetadata = true;
            } else {
                hasMetadata = false;
            }
            if (hasMetadata) {
                nameView.setText(attachment.name);
            } else {
                nameView.setText((int) R.string.loading_attachment);
            }
            View progressBar = view.findViewById(R.id.progressBar);
            if (attachment.state != Attachment.LoadingState.COMPLETE) {
                isLoadingComplete = false;
            }
            if (isLoadingComplete) {
                i = 8;
            }
            progressBar.setVisibility(i);
        }

        public void removeAttachmentView(Attachment attachment) {
            MessageCompose.this.mAttachments.removeView(this.attachmentViews.get(attachment.uri));
            this.attachmentViews.remove(attachment.uri);
        }

        public void performSendAfterChecks() {
            MessageCompose.this.performSendAfterChecks();
        }

        public void performSaveAfterChecks() {
            MessageCompose.this.performSaveAfterChecks();
        }

        public void showMissingAttachmentsPartialMessageWarning() {
            Toast.makeText(MessageCompose.this, MessageCompose.this.getString(R.string.message_compose_attachments_skipped_toast), 1).show();
        }
    };
    /* access modifiers changed from: private */
    public AttachmentPresenter attachmentPresenter;
    private MessageBuilder currentMessageBuilder;
    /* access modifiers changed from: private */
    public boolean draftNeedsSaving = false;
    /* access modifiers changed from: private */
    public boolean isInSubActivity = false;
    private Account mAccount;
    /* access modifiers changed from: private */
    public Action mAction;
    /* access modifiers changed from: private */
    public LinearLayout mAttachments;
    private TextView mChooseIdentityButton;
    private Contacts mContacts;
    /* access modifiers changed from: private */
    public long mDraftId = -1;
    private boolean mFinishAfterDraftSaved;
    private FontSizes mFontSizes = K9.getFontSizes();
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    MessageCompose.this.setProgressBarIndeterminateVisibility(true);
                    return;
                case 2:
                    MessageCompose.this.setProgressBarIndeterminateVisibility(false);
                    return;
                case 3:
                default:
                    super.handleMessage(msg);
                    return;
                case 4:
                    long unused = MessageCompose.this.mDraftId = ((Long) msg.obj).longValue();
                    Toast.makeText(MessageCompose.this, MessageCompose.this.getString(R.string.message_saved_toast), 1).show();
                    return;
                case 5:
                    Toast.makeText(MessageCompose.this, MessageCompose.this.getString(R.string.message_discarded_toast), 1).show();
                    return;
            }
        }
    };
    private Identity mIdentity;
    private boolean mIdentityChanged = false;
    private String mInReplyTo;
    private EolConvertingEditText mMessageContentView;
    private SimpleMessageFormat mMessageFormat;
    /* access modifiers changed from: private */
    public MessageReference mMessageReference;
    private boolean mReadReceipt = false;
    private String mReferences;
    /* access modifiers changed from: private */
    public boolean mSignatureChanged = false;
    private EolConvertingEditText mSignatureView;
    private boolean mSourceMessageProcessed = false;
    private boolean mSourceProcessed = false;
    private EditText mSubjectView;
    private MessageLoaderHelper.MessageLoaderCallbacks messageLoaderCallbacks = new MessageLoaderHelper.MessageLoaderCallbacks() {
        public void onMessageDataLoadFinished(LocalMessage message) {
        }

        public void onMessageDataLoadFailed() {
            MessageCompose.this.mHandler.sendEmptyMessage(2);
            Toast.makeText(MessageCompose.this, (int) R.string.status_invalid_id_error, 1).show();
        }

        public void onMessageViewInfoLoadFinished(MessageViewInfo messageViewInfo) {
            MessageCompose.this.mHandler.sendEmptyMessage(2);
            MessageCompose.this.loadLocalMessageForDisplay(messageViewInfo, MessageCompose.this.mAction);
        }

        public void onMessageViewInfoLoadFailed(MessageViewInfo messageViewInfo) {
            MessageCompose.this.mHandler.sendEmptyMessage(2);
            Toast.makeText(MessageCompose.this, (int) R.string.status_invalid_id_error, 1).show();
        }

        public void setLoadingProgress(int current, int max) {
        }

        public void startIntentSenderForMessageLoaderHelper(IntentSender si, int requestCode, Intent fillIntent, int flagsMask, int flagValues, int extraFlags) {
            try {
                MessageCompose.this.startIntentSenderForResult(si, requestCode | 512, fillIntent, flagsMask, flagValues, extraFlags);
            } catch (IntentSender.SendIntentException e) {
                Log.e("k9", "Irrecoverable error calling PendingIntent!", e);
            }
        }

        public void onDownloadErrorMessageNotFound() {
            MessageCompose.this.runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(MessageCompose.this, (int) R.string.status_invalid_id_error, 1).show();
                }
            });
        }

        public void onDownloadErrorNetworkError() {
            MessageCompose.this.runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(MessageCompose.this, (int) R.string.status_network_error, 1).show();
                }
            });
        }
    };
    private MessageLoaderHelper messageLoaderHelper;
    public MessagingListener messagingListener = new MessagingListener() {
        public void messageUidChanged(Account account, String folder, String oldUid, String newUid) {
            if (MessageCompose.this.mMessageReference != null) {
                if (account.equals(Preferences.getPreferences(MessageCompose.this).getAccount(MessageCompose.this.mMessageReference.getAccountUuid())) && folder.equals(MessageCompose.this.mMessageReference.getFolderName()) && oldUid.equals(MessageCompose.this.mMessageReference.getUid())) {
                    MessageReference unused = MessageCompose.this.mMessageReference = MessageCompose.this.mMessageReference.withModifiedUid(newUid);
                }
            }
        }
    };
    private QuotedMessagePresenter quotedMessagePresenter;
    private RecipientPresenter recipientPresenter;

    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.message_content /*2131755278*/:
            case R.id.subject /*2131755286*/:
                if (hasFocus) {
                    this.recipientPresenter.onNonRecipientFieldFocused();
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onCryptoModeChanged(RecipientPresenter.CryptoMode cryptoMode) {
        this.recipientPresenter.onCryptoModeChanged(cryptoMode);
    }

    public void onOpenPgpInlineChange(boolean enabled) {
        this.recipientPresenter.onCryptoPgpInlineChanged(enabled);
    }

    public enum Action {
        COMPOSE(R.string.compose_title_compose),
        REPLY(R.string.compose_title_reply),
        REPLY_ALL(R.string.compose_title_reply_all),
        FORWARD(R.string.compose_title_forward),
        EDIT_DRAFT(R.string.compose_title_compose);
        
        private final int titleResource;

        private Action(@StringRes int titleResource2) {
            this.titleResource = titleResource2;
        }

        @StringRes
        public int getTitleResource() {
            return this.titleResource;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        String accountUuid;
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().addFlags(Integer.MIN_VALUE);
            getWindow().setStatusBarColor(getResources().getColor(R.color.actionbar));
        }
        if (UpgradeDatabases.actionUpgradeDatabases(this, getIntent())) {
            finish();
            return;
        }
        requestWindowFeature(5);
        if (K9.getK9ComposerThemeSetting() != K9.Theme.USE_GLOBAL) {
            ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(this, K9.getK9ThemeResourceId(K9.getK9ComposerTheme()));
            View v = LayoutInflater.from(contextThemeWrapper).inflate((int) R.layout.message_compose, (ViewGroup) null);
            TypedValue outValue = new TypedValue();
            contextThemeWrapper.getTheme().resolveAttribute(R.attr.messageViewBackgroundColor, outValue, true);
            v.setBackgroundColor(outValue.data);
            setContentView(v);
        } else {
            setContentView((int) R.layout.message_compose);
        }
        setProgressBarIndeterminateVisibility(false);
        ActionBar bar = getActionBar();
        bar.setDisplayShowHomeEnabled(false);
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setDisplayShowTitleEnabled(true);
        Intent intent = getIntent();
        this.mMessageReference = (MessageReference) intent.getParcelableExtra(EXTRA_MESSAGE_REFERENCE);
        if (this.mMessageReference != null) {
            accountUuid = this.mMessageReference.getAccountUuid();
        } else {
            accountUuid = intent.getStringExtra("account");
        }
        this.mAccount = Preferences.getPreferences(this).getAccount(accountUuid);
        if (this.mAccount == null) {
            this.mAccount = Preferences.getPreferences(this).getDefaultAccount();
        }
        if (this.mAccount == null) {
            startActivity(new Intent(this, Accounts.class));
            this.draftNeedsSaving = false;
            finish();
            return;
        }
        this.mContacts = Contacts.getInstance(this);
        this.mChooseIdentityButton = (TextView) findViewById(R.id.identity);
        this.mChooseIdentityButton.setOnClickListener(this);
        RecipientMvpView recipientMvpView = new RecipientMvpView(this);
        this.recipientPresenter = new RecipientPresenter(getApplicationContext(), getLoaderManager(), recipientMvpView, this.mAccount, new ComposePgpInlineDecider(), new ReplyToParser());
        this.mSubjectView = (EditText) findViewById(R.id.subject);
        this.mSubjectView.getInputExtras(true).putBoolean("allowEmoji", true);
        EolConvertingEditText upperSignature = (EolConvertingEditText) findViewById(R.id.upper_signature);
        EolConvertingEditText lowerSignature = (EolConvertingEditText) findViewById(R.id.lower_signature);
        QuotedMessageMvpView quotedMessageMvpView = new QuotedMessageMvpView(this);
        this.quotedMessagePresenter = new QuotedMessagePresenter(this, quotedMessageMvpView, this.mAccount);
        this.attachmentPresenter = new AttachmentPresenter(getApplicationContext(), this.attachmentMvpView, getLoaderManager());
        this.mMessageContentView = (EolConvertingEditText) findViewById(R.id.message_content);
        this.mMessageContentView.getInputExtras(true).putBoolean("allowEmoji", true);
        this.mAttachments = (LinearLayout) findViewById(R.id.attachments);
        TextWatcher draftNeedsChangingTextWatcher = new SimpleTextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                boolean unused = MessageCompose.this.draftNeedsSaving = true;
            }
        };
        AnonymousClass3 r0 = new SimpleTextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                boolean unused = MessageCompose.this.draftNeedsSaving = true;
                boolean unused2 = MessageCompose.this.mSignatureChanged = true;
            }
        };
        recipientMvpView.addTextChangedListener(draftNeedsChangingTextWatcher);
        quotedMessageMvpView.addTextChangedListener(draftNeedsChangingTextWatcher);
        this.mSubjectView.addTextChangedListener(draftNeedsChangingTextWatcher);
        this.mMessageContentView.addTextChangedListener(draftNeedsChangingTextWatcher);
        this.quotedMessagePresenter.showOrHideQuotedText(QuotedTextMode.NONE);
        this.mSubjectView.setOnFocusChangeListener(this);
        this.mMessageContentView.setOnFocusChangeListener(this);
        if (savedInstanceState != null) {
            this.mSourceMessageProcessed = savedInstanceState.getBoolean(STATE_KEY_SOURCE_MESSAGE_PROCED, false);
        }
        if (initFromIntent(intent)) {
            this.mAction = Action.COMPOSE;
            this.draftNeedsSaving = true;
        } else {
            String action = intent.getAction();
            if (ACTION_COMPOSE.equals(action)) {
                this.mAction = Action.COMPOSE;
            } else if (ACTION_REPLY.equals(action)) {
                this.mAction = Action.REPLY;
            } else if (ACTION_REPLY_ALL.equals(action)) {
                this.mAction = Action.REPLY_ALL;
            } else if (ACTION_FORWARD.equals(action)) {
                this.mAction = Action.FORWARD;
            } else if (ACTION_EDIT_DRAFT.equals(action)) {
                this.mAction = Action.EDIT_DRAFT;
            } else {
                Log.w("k9", "MessageCompose was started with an unsupported action");
                this.mAction = Action.COMPOSE;
            }
        }
        if (this.mIdentity == null) {
            this.mIdentity = this.mAccount.getIdentity(0);
        }
        if (this.mAccount.isSignatureBeforeQuotedText()) {
            this.mSignatureView = upperSignature;
            lowerSignature.setVisibility(8);
        } else {
            this.mSignatureView = lowerSignature;
            upperSignature.setVisibility(8);
        }
        updateSignature();
        this.mSignatureView.addTextChangedListener(r0);
        if (!this.mIdentity.getSignatureUse()) {
            this.mSignatureView.setVisibility(8);
        }
        this.mReadReceipt = this.mAccount.isMessageReadReceiptAlways();
        updateFrom();
        if (!this.mSourceMessageProcessed) {
            if (this.mAction == Action.REPLY || this.mAction == Action.REPLY_ALL || this.mAction == Action.FORWARD || this.mAction == Action.EDIT_DRAFT) {
                this.messageLoaderHelper = new MessageLoaderHelper(this, getLoaderManager(), getFragmentManager(), this.messageLoaderCallbacks);
                this.mHandler.sendEmptyMessage(1);
                this.messageLoaderHelper.asyncStartOrResumeLoadingMessage(this.mMessageReference, intent.getParcelableExtra(EXTRA_MESSAGE_DECRYPTION_RESULT));
            }
            if (this.mAction != Action.EDIT_DRAFT) {
                String alwaysBccString = this.mAccount.getAlwaysBcc();
                if (!TextUtils.isEmpty(alwaysBccString)) {
                    this.recipientPresenter.addBccAddresses(Address.parse(alwaysBccString));
                }
            }
        }
        if (this.mAction == Action.REPLY || this.mAction == Action.REPLY_ALL) {
            this.mMessageReference = this.mMessageReference.withModifiedFlag(Flag.ANSWERED);
        }
        if (this.mAction == Action.REPLY || this.mAction == Action.REPLY_ALL || this.mAction == Action.EDIT_DRAFT) {
            this.mMessageContentView.requestFocus();
        } else {
            recipientMvpView.requestFocusOnToField();
        }
        if (this.mAction == Action.FORWARD) {
            this.mMessageReference = this.mMessageReference.withModifiedFlag(Flag.FORWARDED);
        }
        updateMessageFormat();
        int fontSize = this.mFontSizes.getMessageComposeInput();
        recipientMvpView.setFontSizes(this.mFontSizes, fontSize);
        quotedMessageMvpView.setFontSizes(this.mFontSizes, fontSize);
        this.mFontSizes.setViewTextSize(this.mSubjectView, fontSize);
        this.mFontSizes.setViewTextSize(this.mMessageContentView, fontSize);
        this.mFontSizes.setViewTextSize(this.mSignatureView, fontSize);
        updateMessageFormat();
        setTitle();
        this.currentMessageBuilder = (MessageBuilder) getLastNonConfigurationInstance();
        if (this.currentMessageBuilder != null) {
            setProgressBarIndeterminateVisibility(true);
            this.currentMessageBuilder.reattachCallback(this);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        this.recipientPresenter.onActivityDestroy();
    }

    private boolean initFromIntent(Intent intent) {
        boolean startedByExternalIntent = false;
        String action = intent.getAction();
        if (("android.intent.action.VIEW".equals(action) || "android.intent.action.SENDTO".equals(action)) && intent.getData() != null) {
            Uri uri = intent.getData();
            if (MailTo.isMailTo(uri)) {
                initializeFromMailto(MailTo.parse(uri));
            }
        }
        if ("android.intent.action.SEND".equals(action) || "android.intent.action.SEND_MULTIPLE".equals(action) || "android.intent.action.SENDTO".equals(action) || "android.intent.action.VIEW".equals(action)) {
            startedByExternalIntent = true;
            CharSequence text = intent.getCharSequenceExtra("android.intent.extra.TEXT");
            if (text != null && this.mMessageContentView.getText().length() == 0) {
                this.mMessageContentView.setCharacters(text);
            }
            String type = intent.getType();
            if ("android.intent.action.SEND".equals(action)) {
                Uri stream = (Uri) intent.getParcelableExtra("android.intent.extra.STREAM");
                if (stream != null) {
                    this.attachmentPresenter.addAttachment(stream, type);
                }
            } else {
                List<Parcelable> list = intent.getParcelableArrayListExtra("android.intent.extra.STREAM");
                if (list != null) {
                    for (Parcelable parcelable : list) {
                        Uri stream2 = (Uri) parcelable;
                        if (stream2 != null) {
                            this.attachmentPresenter.addAttachment(stream2, type);
                        }
                    }
                }
            }
            String subject = intent.getStringExtra("android.intent.extra.SUBJECT");
            if (subject != null && this.mSubjectView.getText().length() == 0) {
                this.mSubjectView.setText(subject);
            }
            this.recipientPresenter.initFromSendOrViewIntent(intent);
        }
        return startedByExternalIntent;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        MessagingController.getInstance(this).addListener(this.messagingListener);
    }

    public void onPause() {
        boolean isPausingOnConfigurationChange;
        boolean isCurrentlyBuildingMessage;
        super.onPause();
        MessagingController.getInstance(this).removeListener(this.messagingListener);
        if ((getChangingConfigurations() & 128) == 128) {
            isPausingOnConfigurationChange = true;
        } else {
            isPausingOnConfigurationChange = false;
        }
        if (this.currentMessageBuilder != null) {
            isCurrentlyBuildingMessage = true;
        } else {
            isCurrentlyBuildingMessage = false;
        }
        if (!isPausingOnConfigurationChange && !isCurrentlyBuildingMessage && !this.isInSubActivity) {
            checkToSaveDraftImplicitly();
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(STATE_KEY_SOURCE_MESSAGE_PROCED, this.mSourceMessageProcessed);
        outState.putLong(STATE_KEY_DRAFT_ID, this.mDraftId);
        outState.putSerializable(STATE_IDENTITY, this.mIdentity);
        outState.putBoolean(STATE_IDENTITY_CHANGED, this.mIdentityChanged);
        outState.putString(STATE_IN_REPLY_TO, this.mInReplyTo);
        outState.putString(STATE_REFERENCES, this.mReferences);
        outState.putBoolean(STATE_KEY_READ_RECEIPT, this.mReadReceipt);
        outState.putBoolean(STATE_KEY_DRAFT_NEEDS_SAVING, this.draftNeedsSaving);
        outState.putBoolean(STATE_ALREADY_NOTIFIED_USER_OF_EMPTY_SUBJECT, this.alreadyNotifiedUserOfEmptySubject);
        this.recipientPresenter.onSaveInstanceState(outState);
        this.quotedMessagePresenter.onSaveInstanceState(outState);
        this.attachmentPresenter.onSaveInstanceState(outState);
    }

    public Object onRetainNonConfigurationInstance() {
        if (this.currentMessageBuilder != null) {
            this.currentMessageBuilder.detachCallback();
        }
        return this.currentMessageBuilder;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        this.mAttachments.removeAllViews();
        this.mReadReceipt = savedInstanceState.getBoolean(STATE_KEY_READ_RECEIPT);
        this.recipientPresenter.onRestoreInstanceState(savedInstanceState);
        this.quotedMessagePresenter.onRestoreInstanceState(savedInstanceState);
        this.attachmentPresenter.onRestoreInstanceState(savedInstanceState);
        this.mDraftId = savedInstanceState.getLong(STATE_KEY_DRAFT_ID);
        this.mIdentity = (Identity) savedInstanceState.getSerializable(STATE_IDENTITY);
        this.mIdentityChanged = savedInstanceState.getBoolean(STATE_IDENTITY_CHANGED);
        this.mInReplyTo = savedInstanceState.getString(STATE_IN_REPLY_TO);
        this.mReferences = savedInstanceState.getString(STATE_REFERENCES);
        this.draftNeedsSaving = savedInstanceState.getBoolean(STATE_KEY_DRAFT_NEEDS_SAVING);
        this.alreadyNotifiedUserOfEmptySubject = savedInstanceState.getBoolean(STATE_ALREADY_NOTIFIED_USER_OF_EMPTY_SUBJECT);
        updateFrom();
        updateMessageFormat();
    }

    private void setTitle() {
        setTitle(this.mAction.getTitleResource());
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: com.fsck.k9.message.SimpleMessageBuilder} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: com.fsck.k9.message.SimpleMessageBuilder} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v0, resolved type: com.fsck.k9.message.PgpMessageBuilder} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: com.fsck.k9.message.SimpleMessageBuilder} */
    /* JADX WARNING: Multi-variable type inference failed */
    @android.support.annotation.Nullable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.fsck.k9.message.MessageBuilder createMessageBuilder(boolean r7) {
        /*
            r6 = this;
            com.fsck.k9.activity.compose.RecipientPresenter r4 = r6.recipientPresenter
            r4.updateCryptoStatus()
            com.fsck.k9.activity.compose.RecipientPresenter r4 = r6.recipientPresenter
            com.fsck.k9.activity.compose.ComposeCryptoStatus r1 = r4.getCurrentCryptoStatus()
            if (r7 != 0) goto L_0x00df
            boolean r4 = r1.shouldUsePgpMessageBuilder()
            if (r4 == 0) goto L_0x00df
            com.fsck.k9.activity.compose.ComposeCryptoStatus$SendErrorState r2 = r1.getSendErrorStateOrNull()
            if (r2 == 0) goto L_0x0020
            com.fsck.k9.activity.compose.RecipientPresenter r4 = r6.recipientPresenter
            r4.showPgpSendError(r2)
            r0 = 0
        L_0x001f:
            return r0
        L_0x0020:
            com.fsck.k9.message.PgpMessageBuilder r3 = com.fsck.k9.message.PgpMessageBuilder.newInstance()
            com.fsck.k9.activity.compose.RecipientPresenter r4 = r6.recipientPresenter
            r4.builderSetProperties(r3)
            r0 = r3
        L_0x002a:
            android.widget.EditText r4 = r6.mSubjectView
            android.text.Editable r4 = r4.getText()
            java.lang.String r4 = r4.toString()
            com.fsck.k9.message.MessageBuilder r4 = r0.setSubject(r4)
            java.util.Date r5 = new java.util.Date
            r5.<init>()
            com.fsck.k9.message.MessageBuilder r4 = r4.setSentDate(r5)
            boolean r5 = com.fsck.k9.K9.hideTimeZone()
            com.fsck.k9.message.MessageBuilder r4 = r4.setHideTimeZone(r5)
            com.fsck.k9.activity.compose.RecipientPresenter r5 = r6.recipientPresenter
            java.util.List r5 = r5.getToAddresses()
            com.fsck.k9.message.MessageBuilder r4 = r4.setTo(r5)
            com.fsck.k9.activity.compose.RecipientPresenter r5 = r6.recipientPresenter
            java.util.List r5 = r5.getCcAddresses()
            com.fsck.k9.message.MessageBuilder r4 = r4.setCc(r5)
            com.fsck.k9.activity.compose.RecipientPresenter r5 = r6.recipientPresenter
            java.util.List r5 = r5.getBccAddresses()
            com.fsck.k9.message.MessageBuilder r4 = r4.setBcc(r5)
            java.lang.String r5 = r6.mInReplyTo
            com.fsck.k9.message.MessageBuilder r4 = r4.setInReplyTo(r5)
            java.lang.String r5 = r6.mReferences
            com.fsck.k9.message.MessageBuilder r4 = r4.setReferences(r5)
            boolean r5 = r6.mReadReceipt
            com.fsck.k9.message.MessageBuilder r4 = r4.setRequestReadReceipt(r5)
            com.fsck.k9.Identity r5 = r6.mIdentity
            com.fsck.k9.message.MessageBuilder r4 = r4.setIdentity(r5)
            com.fsck.k9.message.SimpleMessageFormat r5 = r6.mMessageFormat
            com.fsck.k9.message.MessageBuilder r4 = r4.setMessageFormat(r5)
            com.fsck.k9.ui.EolConvertingEditText r5 = r6.mMessageContentView
            java.lang.String r5 = r5.getCharacters()
            com.fsck.k9.message.MessageBuilder r4 = r4.setText(r5)
            com.fsck.k9.activity.compose.AttachmentPresenter r5 = r6.attachmentPresenter
            java.util.ArrayList r5 = r5.createAttachmentList()
            com.fsck.k9.message.MessageBuilder r4 = r4.setAttachments(r5)
            com.fsck.k9.ui.EolConvertingEditText r5 = r6.mSignatureView
            java.lang.String r5 = r5.getCharacters()
            java.lang.String r5 = atomicgonza.Signature.formatSignature(r5)
            com.fsck.k9.message.MessageBuilder r4 = r4.setSignature(r5)
            com.fsck.k9.Account r5 = r6.mAccount
            boolean r5 = r5.isSignatureBeforeQuotedText()
            com.fsck.k9.message.MessageBuilder r4 = r4.setSignatureBeforeQuotedText(r5)
            boolean r5 = r6.mIdentityChanged
            com.fsck.k9.message.MessageBuilder r4 = r4.setIdentityChanged(r5)
            boolean r5 = r6.mSignatureChanged
            com.fsck.k9.message.MessageBuilder r4 = r4.setSignatureChanged(r5)
            com.fsck.k9.ui.EolConvertingEditText r5 = r6.mMessageContentView
            int r5 = r5.getSelectionStart()
            com.fsck.k9.message.MessageBuilder r4 = r4.setCursorPosition(r5)
            com.fsck.k9.activity.MessageReference r5 = r6.mMessageReference
            com.fsck.k9.message.MessageBuilder r4 = r4.setMessageReference(r5)
            com.fsck.k9.message.MessageBuilder r4 = r4.setDraft(r7)
            boolean r5 = r1.isPgpInlineModeEnabled()
            r4.setIsPgpInlineEnabled(r5)
            com.fsck.k9.ui.compose.QuotedMessagePresenter r4 = r6.quotedMessagePresenter
            r4.builderSetProperties(r0)
            goto L_0x001f
        L_0x00df:
            com.fsck.k9.message.SimpleMessageBuilder r0 = com.fsck.k9.message.SimpleMessageBuilder.newInstance()
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fsck.k9.activity.MessageCompose.createMessageBuilder(boolean):com.fsck.k9.message.MessageBuilder");
    }

    private void checkToSendMessage() {
        if (this.mSubjectView.getText().length() == 0 && !this.alreadyNotifiedUserOfEmptySubject) {
            Toast.makeText(this, (int) R.string.empty_subject, 1).show();
            this.alreadyNotifiedUserOfEmptySubject = true;
        } else if (!this.recipientPresenter.checkRecipientsOkForSending() && !this.attachmentPresenter.checkOkForSendingOrDraftSaving()) {
            performSendAfterChecks();
        }
    }

    /* access modifiers changed from: private */
    public void checkToSaveDraftAndSave() {
        if (!this.mAccount.hasDraftsFolder()) {
            Toast.makeText(this, (int) R.string.compose_error_no_draft_folder, 0).show();
        } else if (!this.attachmentPresenter.checkOkForSendingOrDraftSaving()) {
            this.mFinishAfterDraftSaved = true;
            performSaveAfterChecks();
        }
    }

    private void checkToSaveDraftImplicitly() {
        if (this.mAccount.hasDraftsFolder() && this.draftNeedsSaving) {
            this.mFinishAfterDraftSaved = false;
            performSaveAfterChecks();
        }
    }

    /* access modifiers changed from: private */
    public void performSaveAfterChecks() {
        this.currentMessageBuilder = createMessageBuilder(true);
        if (this.currentMessageBuilder != null) {
            setProgressBarIndeterminateVisibility(true);
            this.currentMessageBuilder.buildAsync(this);
        }
    }

    public void performSendAfterChecks() {
        this.currentMessageBuilder = createMessageBuilder(false);
        if (this.currentMessageBuilder != null) {
            this.draftNeedsSaving = false;
            setProgressBarIndeterminateVisibility(true);
            this.currentMessageBuilder.buildAsync(this);
        }
    }

    /* access modifiers changed from: private */
    public void onDiscard() {
        if (this.mDraftId != -1) {
            MessagingController.getInstance(getApplication()).deleteDraft(this.mAccount, this.mDraftId);
            this.mDraftId = -1;
        }
        this.mHandler.sendEmptyMessage(5);
        this.draftNeedsSaving = false;
        finish();
    }

    private void onReadReceipt() {
        CharSequence txt;
        if (!this.mReadReceipt) {
            txt = getString(R.string.read_receipt_enabled);
            this.mReadReceipt = true;
        } else {
            txt = getString(R.string.read_receipt_disabled);
            this.mReadReceipt = false;
        }
        Toast.makeText(getApplicationContext(), txt, 0).show();
    }

    public void showContactPicker(int requestCode) {
        this.isInSubActivity = true;
        startActivityForResult(this.mContacts.contactPickerIntent(), requestCode | 256);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        this.isInSubActivity = false;
        if ((requestCode & 2048) == 2048) {
            int requestCode2 = requestCode ^ 2048;
            if (this.currentMessageBuilder == null) {
                Log.e("k9", "Got a message builder activity result for no message builder, this is an illegal state!");
            } else {
                this.currentMessageBuilder.onActivityResult(requestCode2, resultCode, data, this);
            }
        } else if ((requestCode & 256) == 256) {
            this.recipientPresenter.onActivityResult(requestCode ^ 256, resultCode, data);
        } else if ((requestCode & 512) == 512) {
            this.messageLoaderHelper.onActivityResult(requestCode ^ 512, resultCode, data);
        } else if ((requestCode & 1024) == 1024) {
            this.attachmentPresenter.onActivityResult(resultCode, requestCode ^ 1024, data);
        }
    }

    /* access modifiers changed from: private */
    public void onAccountChosen(Account account, Identity identity) {
        if (!this.mAccount.equals(account)) {
            if (K9.DEBUG) {
                Log.v("k9", "Switching account from " + this.mAccount + " to " + account);
            }
            if (this.mAction == Action.EDIT_DRAFT) {
                this.mMessageReference = null;
            }
            if (this.draftNeedsSaving || this.mDraftId != -1) {
                long previousDraftId = this.mDraftId;
                Account previousAccount = this.mAccount;
                this.mDraftId = -1;
                this.mAccount = account;
                if (K9.DEBUG) {
                    Log.v("k9", "Account switch, saving new draft in new account");
                }
                checkToSaveDraftImplicitly();
                if (previousDraftId != -1) {
                    if (K9.DEBUG) {
                        Log.v("k9", "Account switch, deleting draft from previous account: " + previousDraftId);
                    }
                    MessagingController.getInstance(getApplication()).deleteDraft(previousAccount, previousDraftId);
                }
            } else {
                this.mAccount = account;
            }
            this.recipientPresenter.onSwitchAccount(this.mAccount);
            this.quotedMessagePresenter.onSwitchAccount(this.mAccount);
        }
        switchToIdentity(identity);
    }

    private void switchToIdentity(Identity identity) {
        this.mIdentity = identity;
        this.mIdentityChanged = true;
        this.draftNeedsSaving = true;
        updateFrom();
        updateSignature();
        updateMessageFormat();
        this.recipientPresenter.onSwitchIdentity(identity);
    }

    private void updateFrom() {
        this.mChooseIdentityButton.setText(this.mIdentity.getEmail());
    }

    private void updateSignature() {
        if (this.mIdentity.getSignatureUse()) {
            this.mSignatureView.setCharacters(this.mIdentity.getSignature());
            this.mSignatureView.setVisibility(0);
            return;
        }
        this.mSignatureView.setVisibility(8);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.identity /*2131755300*/:
                showDialog(3);
                return;
            default:
                return;
        }
    }

    private void askBeforeDiscard() {
        if (K9.confirmDiscardMessage()) {
            showDialog(4);
        } else {
            onDiscard();
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                onBackPressed();
                return true;
            case R.id.add_attachment /*2131755472*/:
                this.attachmentPresenter.onClickAddAttachment(this.recipientPresenter);
                return true;
            case R.id.send /*2131755473*/:
                checkToSendMessage();
                return true;
            case R.id.add_from_contacts /*2131755474*/:
                this.recipientPresenter.onMenuAddFromContacts();
                return true;
            case R.id.save /*2131755475*/:
                checkToSaveDraftAndSave();
                return true;
            case R.id.discard /*2131755476*/:
                askBeforeDiscard();
                return true;
            case R.id.read_receipt /*2131755477*/:
                onReadReceipt();
                return true;
            case R.id.openpgp_inline_enable /*2131755478*/:
                this.recipientPresenter.onMenuSetPgpInline(true);
                return true;
            case R.id.openpgp_inline_disable /*2131755479*/:
                this.recipientPresenter.onMenuSetPgpInline(false);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.message_compose_option, menu);
        if (this.mAccount.hasDraftsFolder()) {
            return true;
        }
        menu.findItem(R.id.save).setEnabled(false);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if (this.recipientPresenter != null) {
            this.recipientPresenter.onPrepareOptionsMenu(menu);
        }
        return true;
    }

    public void onBackPressed() {
        if (this.draftNeedsSaving) {
            if (!this.mAccount.hasDraftsFolder()) {
                showDialog(2);
            } else {
                showDialog(1);
            }
        } else if (this.mDraftId == -1) {
            onDiscard();
        } else {
            super.onBackPressed();
        }
    }

    public void onProgressCancel(ProgressDialogFragment fragment) {
        this.attachmentPresenter.attachmentProgressDialogCancelled();
    }

    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                return new AlertDialog.Builder(this).setTitle((int) R.string.save_or_discard_draft_message_dlg_title).setMessage((int) R.string.save_or_discard_draft_message_instructions_fmt).setPositiveButton((int) R.string.save_draft_action, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        MessageCompose.this.dismissDialog(1);
                        MessageCompose.this.checkToSaveDraftAndSave();
                    }
                }).setNegativeButton((int) R.string.discard_action, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        MessageCompose.this.dismissDialog(1);
                        MessageCompose.this.onDiscard();
                    }
                }).create();
            case 2:
                return new AlertDialog.Builder(this).setTitle((int) R.string.confirm_discard_draft_message_title).setMessage((int) R.string.confirm_discard_draft_message).setPositiveButton((int) R.string.cancel_action, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        MessageCompose.this.dismissDialog(2);
                    }
                }).setNegativeButton((int) R.string.discard_action, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        MessageCompose.this.dismissDialog(2);
                        Toast.makeText(MessageCompose.this, MessageCompose.this.getString(R.string.message_discarded_toast), 1).show();
                        MessageCompose.this.onDiscard();
                    }
                }).create();
            case 3:
                Context context = new ContextThemeWrapper(this, K9.getK9Theme() == K9.Theme.LIGHT ? com.fsck.k9.R.style.Theme_K9_Dialog_Light : com.fsck.k9.R.style.Theme_K9_Dialog_Dark);
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle((int) R.string.send_as);
                final IdentityAdapter adapter = new IdentityAdapter(context);
                builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        IdentityAdapter.IdentityContainer container = (IdentityAdapter.IdentityContainer) adapter.getItem(which);
                        MessageCompose.this.onAccountChosen(container.account, container.identity);
                    }
                });
                return builder.create();
            case 4:
                return new AlertDialog.Builder(this).setTitle((int) R.string.dialog_confirm_delete_title).setMessage((int) R.string.dialog_confirm_delete_message).setPositiveButton((int) R.string.dialog_confirm_delete_confirm_button, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        MessageCompose.this.onDiscard();
                    }
                }).setNegativeButton((int) R.string.dialog_confirm_delete_cancel_button, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).create();
            default:
                return super.onCreateDialog(id);
        }
    }

    public void saveDraftEventually() {
        this.draftNeedsSaving = true;
    }

    public void loadQuotedTextForEdit() {
        if (this.mMessageReference == null) {
            throw new IllegalStateException("tried to edit quoted message with no referenced message");
        }
        this.messageLoaderHelper.asyncStartOrResumeLoadingMessage(this.mMessageReference, null);
    }

    private void processSourceMessage(MessageViewInfo messageViewInfo) {
        try {
            switch (this.mAction) {
                case REPLY:
                case REPLY_ALL:
                    processMessageToReplyTo(messageViewInfo);
                    break;
                case FORWARD:
                    processMessageToForward(messageViewInfo);
                    break;
                case EDIT_DRAFT:
                    processDraftMessage(messageViewInfo);
                    break;
                default:
                    Log.w("k9", "processSourceMessage() called with unsupported action");
                    break;
            }
        } catch (MessagingException me2) {
            Log.e("k9", "Error while processing source message: ", me2);
        } finally {
            this.mSourceMessageProcessed = true;
            this.draftNeedsSaving = false;
        }
        updateMessageFormat();
    }

    private void processMessageToReplyTo(MessageViewInfo messageViewInfo) throws MessagingException {
        boolean isReplyAll;
        Identity useIdentity;
        com.fsck.k9.mail.Message message = messageViewInfo.message;
        if (message.getSubject() != null) {
            String subject = PREFIX.matcher(message.getSubject()).replaceFirst("");
            if (!subject.toLowerCase(Locale.US).startsWith("re:")) {
                this.mSubjectView.setText("Re: " + subject);
            } else {
                this.mSubjectView.setText(subject);
            }
        } else {
            this.mSubjectView.setText("");
        }
        if (this.mAction == Action.REPLY_ALL) {
            isReplyAll = true;
        } else {
            isReplyAll = false;
        }
        this.recipientPresenter.initFromReplyToMessage(message, isReplyAll);
        if (message.getMessageId() != null && message.getMessageId().length() > 0) {
            this.mInReplyTo = message.getMessageId();
            String[] refs = message.getReferences();
            if (refs == null || refs.length <= 0) {
                this.mReferences = this.mInReplyTo;
            } else {
                this.mReferences = TextUtils.join("", refs) + " " + this.mInReplyTo;
            }
        } else if (K9.DEBUG) {
            Log.d("k9", "could not get Message-ID.");
        }
        this.quotedMessagePresenter.initFromReplyToMessage(messageViewInfo, this.mAction);
        if ((this.mAction == Action.REPLY || this.mAction == Action.REPLY_ALL) && (useIdentity = IdentityHelper.getRecipientIdentityFromMessage(this.mAccount, message)) != this.mAccount.getIdentity(0)) {
            switchToIdentity(useIdentity);
        }
    }

    private void processMessageToForward(MessageViewInfo messageViewInfo) throws MessagingException {
        com.fsck.k9.mail.Message message = messageViewInfo.message;
        String subject = message.getSubject();
        if (subject == null || subject.toLowerCase(Locale.US).startsWith("fwd:")) {
            this.mSubjectView.setText(subject);
        } else {
            this.mSubjectView.setText("Fwd: " + subject);
        }
        if (!TextUtils.isEmpty(message.getMessageId())) {
            this.mInReplyTo = message.getMessageId();
            this.mReferences = this.mInReplyTo;
        } else if (K9.DEBUG) {
            Log.d("k9", "could not get Message-ID.");
        }
        this.quotedMessagePresenter.processMessageToForward(messageViewInfo);
        this.attachmentPresenter.processMessageToForward(messageViewInfo);
    }

    private void processDraftMessage(MessageViewInfo messageViewInfo) {
        com.fsck.k9.mail.Message message = messageViewInfo.message;
        this.mDraftId = MessagingController.getInstance(getApplication()).getId(message);
        this.mSubjectView.setText(message.getSubject());
        this.recipientPresenter.initFromDraftMessage(message);
        String[] inReplyTo = message.getHeader("In-Reply-To");
        if (inReplyTo.length >= 1) {
            this.mInReplyTo = inReplyTo[0];
        }
        String[] references = message.getHeader("References");
        if (references.length >= 1) {
            this.mReferences = references[0];
        }
        if (!this.mSourceMessageProcessed) {
            this.attachmentPresenter.loadNonInlineAttachments(messageViewInfo);
        }
        Map<IdentityField, String> k9identity = new HashMap<>();
        String[] identityHeaders = message.getHeader("X-K9mail-Identity");
        if (identityHeaders.length > 0 && identityHeaders[0] != null) {
            k9identity = IdentityHeaderParser.parse(identityHeaders[0]);
        }
        Identity newIdentity = new Identity();
        if (k9identity.containsKey(IdentityField.SIGNATURE)) {
            newIdentity.setSignatureUse(true);
            newIdentity.setSignature((String) k9identity.get(IdentityField.SIGNATURE));
            this.mSignatureChanged = true;
        } else {
            if (message instanceof LocalMessage) {
                newIdentity.setSignatureUse(((LocalMessage) message).getFolder().getSignatureUse());
            }
            newIdentity.setSignature(this.mIdentity.getSignature());
        }
        if (k9identity.containsKey(IdentityField.NAME)) {
            newIdentity.setName((String) k9identity.get(IdentityField.NAME));
            this.mIdentityChanged = true;
        } else {
            newIdentity.setName(this.mIdentity.getName());
        }
        if (k9identity.containsKey(IdentityField.EMAIL)) {
            newIdentity.setEmail((String) k9identity.get(IdentityField.EMAIL));
            this.mIdentityChanged = true;
        } else {
            newIdentity.setEmail(this.mIdentity.getEmail());
        }
        if (k9identity.containsKey(IdentityField.ORIGINAL_MESSAGE)) {
            this.mMessageReference = null;
            try {
                MessageReference messageReference = new MessageReference((String) k9identity.get(IdentityField.ORIGINAL_MESSAGE));
                if (Preferences.getPreferences(getApplicationContext()).getAccount(messageReference.getAccountUuid()) != null) {
                    this.mMessageReference = messageReference;
                }
            } catch (MessagingException e) {
                Log.e("k9", "Could not decode message reference in identity.", e);
            }
        }
        this.mIdentity = newIdentity;
        updateSignature();
        updateFrom();
        this.quotedMessagePresenter.processDraftMessage(messageViewInfo, k9identity);
    }

    static class SendMessageTask extends AsyncTask<Void, Void, Void> {
        final Account account;
        final Contacts contacts;
        final Context context;
        final Long draftId;
        final com.fsck.k9.mail.Message message;
        final MessageReference messageReference;

        SendMessageTask(Context context2, Account account2, Contacts contacts2, com.fsck.k9.mail.Message message2, Long draftId2, MessageReference messageReference2) {
            this.context = context2;
            this.account = account2;
            this.contacts = contacts2;
            this.message = message2;
            this.draftId = draftId2;
            this.messageReference = messageReference2;
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Void... params) {
            try {
                this.contacts.markAsContacted(this.message.getRecipients(Message.RecipientType.TO));
                this.contacts.markAsContacted(this.message.getRecipients(Message.RecipientType.CC));
                this.contacts.markAsContacted(this.message.getRecipients(Message.RecipientType.BCC));
                updateReferencedMessage();
            } catch (Exception e) {
                Log.e("k9", "Failed to mark contact as contacted.", e);
            }
            MessagingController.getInstance(this.context).sendMessage(this.account, this.message, null);
            if (this.draftId != null) {
                MessagingController.getInstance(this.context).deleteDraft(this.account, this.draftId.longValue());
            }
            return null;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.fsck.k9.controller.MessagingController.setFlag(com.fsck.k9.Account, java.lang.String, java.lang.String, com.fsck.k9.mail.Flag, boolean):void
         arg types: [com.fsck.k9.Account, java.lang.String, java.lang.String, com.fsck.k9.mail.Flag, int]
         candidates:
          com.fsck.k9.controller.MessagingController.setFlag(com.fsck.k9.Account, java.lang.String, java.util.List<? extends com.fsck.k9.mail.Message>, com.fsck.k9.mail.Flag, boolean):void
          com.fsck.k9.controller.MessagingController.setFlag(com.fsck.k9.Account, java.lang.String, java.lang.String, com.fsck.k9.mail.Flag, boolean):void */
        private void updateReferencedMessage() {
            if (this.messageReference != null && this.messageReference.getFlag() != null) {
                if (K9.DEBUG) {
                    Log.d("k9", "Setting referenced message (" + this.messageReference.getFolderName() + ", " + this.messageReference.getUid() + ") flag to " + this.messageReference.getFlag());
                }
                MessagingController.getInstance(this.context).setFlag(Preferences.getPreferences(this.context).getAccount(this.messageReference.getAccountUuid()), this.messageReference.getFolderName(), this.messageReference.getUid(), this.messageReference.getFlag(), true);
            }
        }
    }

    private void initializeFromMailto(MailTo mailTo) {
        this.recipientPresenter.initFromMailto(mailTo);
        String subject = mailTo.getSubject();
        if (subject != null && !subject.isEmpty()) {
            this.mSubjectView.setText(subject);
        }
        String body = mailTo.getBody();
        if (body != null && !body.isEmpty()) {
            this.mMessageContentView.setCharacters(body);
        }
    }

    private void setMessageFormat(SimpleMessageFormat format) {
        this.mMessageFormat = format;
    }

    public void updateMessageFormat() {
        SimpleMessageFormat messageFormat;
        Account.MessageFormat origMessageFormat = this.mAccount.getMessageFormat();
        if (origMessageFormat == Account.MessageFormat.TEXT) {
            messageFormat = SimpleMessageFormat.TEXT;
        } else if (this.quotedMessagePresenter.isForcePlainText() && this.quotedMessagePresenter.includeQuotedText()) {
            messageFormat = SimpleMessageFormat.TEXT;
        } else if (this.recipientPresenter.isForceTextMessageFormat()) {
            messageFormat = SimpleMessageFormat.TEXT;
        } else if (origMessageFormat != Account.MessageFormat.AUTO) {
            messageFormat = SimpleMessageFormat.HTML;
        } else if (this.mAction == Action.COMPOSE || this.quotedMessagePresenter.isQuotedTextText() || !this.quotedMessagePresenter.includeQuotedText()) {
            messageFormat = SimpleMessageFormat.TEXT;
        } else {
            messageFormat = SimpleMessageFormat.HTML;
        }
        setMessageFormat(messageFormat);
    }

    public void onMessageBuildSuccess(MimeMessage message, boolean isDraft) {
        Long l = null;
        if (isDraft) {
            this.draftNeedsSaving = false;
            this.currentMessageBuilder = null;
            if (this.mAction == Action.EDIT_DRAFT && this.mMessageReference != null) {
                message.setUid(this.mMessageReference.getUid());
            }
            MimeMessage mimeMessage = message;
            new SaveMessageTask(getApplicationContext(), this.mAccount, this.mContacts, this.mHandler, mimeMessage, this.mDraftId, this.recipientPresenter.isAllowSavingDraftRemotely()).execute(new Void[0]);
            if (this.mFinishAfterDraftSaved) {
                finish();
            } else {
                setProgressBarIndeterminateVisibility(false);
            }
        } else {
            this.currentMessageBuilder = null;
            Context applicationContext = getApplicationContext();
            Account account = this.mAccount;
            Contacts contacts = this.mContacts;
            if (this.mDraftId != -1) {
                l = Long.valueOf(this.mDraftId);
            }
            new SendMessageTask(applicationContext, account, contacts, message, l, this.mMessageReference).execute(new Void[0]);
            finish();
        }
    }

    public void onMessageBuildCancel() {
        this.currentMessageBuilder = null;
        setProgressBarIndeterminateVisibility(false);
    }

    public void onMessageBuildException(MessagingException me2) {
        Log.e("k9", "Error sending message", me2);
        Toast.makeText(this, getString(R.string.send_failed_reason, new Object[]{me2.getLocalizedMessage()}), 1).show();
        this.currentMessageBuilder = null;
        setProgressBarIndeterminateVisibility(false);
    }

    public void onMessageBuildReturnPendingIntent(PendingIntent pendingIntent, int requestCode) {
        try {
            startIntentSenderForResult(pendingIntent.getIntentSender(), requestCode | 2048, null, 0, 0, 0);
        } catch (IntentSender.SendIntentException e) {
            Log.e("k9", "Error starting pending intent from builder!", e);
        }
    }

    public void launchUserInteractionPendingIntent(PendingIntent pendingIntent, int requestCode) {
        try {
            startIntentSenderForResult(pendingIntent.getIntentSender(), requestCode | 256, null, 0, 0, 0);
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }
    }

    public void loadLocalMessageForDisplay(MessageViewInfo messageViewInfo, Action action) {
        if (this.mSourceMessageProcessed) {
            try {
                this.quotedMessagePresenter.populateUIWithQuotedMessage(messageViewInfo, true, action);
            } catch (MessagingException e) {
                this.quotedMessagePresenter.showOrHideQuotedText(QuotedTextMode.HIDE);
                Log.e("k9", "Could not re-process source message; deleting quoted text to be safe.", e);
            }
            updateMessageFormat();
            return;
        }
        processSourceMessage(messageViewInfo);
        this.mSourceMessageProcessed = true;
    }

    /* renamed from: com.fsck.k9.activity.MessageCompose$14  reason: invalid class name */
    static /* synthetic */ class AnonymousClass14 {
        static final /* synthetic */ int[] $SwitchMap$com$fsck$k9$activity$compose$AttachmentPresenter$WaitingAction = new int[AttachmentPresenter.WaitingAction.values().length];

        static {
            try {
                $SwitchMap$com$fsck$k9$activity$compose$AttachmentPresenter$WaitingAction[AttachmentPresenter.WaitingAction.SEND.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$fsck$k9$activity$compose$AttachmentPresenter$WaitingAction[AttachmentPresenter.WaitingAction.SAVE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            $SwitchMap$com$fsck$k9$activity$MessageCompose$Action = new int[Action.values().length];
            try {
                $SwitchMap$com$fsck$k9$activity$MessageCompose$Action[Action.REPLY.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$fsck$k9$activity$MessageCompose$Action[Action.REPLY_ALL.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$fsck$k9$activity$MessageCompose$Action[Action.FORWARD.ordinal()] = 3;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$fsck$k9$activity$MessageCompose$Action[Action.EDIT_DRAFT.ordinal()] = 4;
            } catch (NoSuchFieldError e6) {
            }
        }
    }
}
