package com.fsck.k9.activity.setup;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import com.fsck.k9.Account;
import com.fsck.k9.Preferences;
import com.fsck.k9.activity.K9Activity;
import yahoo.mail.app.R;

public class AccountSetupComposition extends K9Activity {
    private static final String EXTRA_ACCOUNT = "account";
    /* access modifiers changed from: private */
    public Account mAccount;
    private EditText mAccountAlwaysBcc;
    private EditText mAccountEmail;
    private EditText mAccountName;
    /* access modifiers changed from: private */
    public EditText mAccountSignature;
    /* access modifiers changed from: private */
    public RadioButton mAccountSignatureAfterLocation;
    /* access modifiers changed from: private */
    public RadioButton mAccountSignatureBeforeLocation;
    /* access modifiers changed from: private */
    public LinearLayout mAccountSignatureLayout;
    private CheckBox mAccountSignatureUse;

    public static void actionEditCompositionSettings(Activity context, Account account) {
        Intent i = new Intent(context, AccountSetupComposition.class);
        i.setAction("android.intent.action.EDIT");
        i.putExtra("account", account.getUuid());
        context.startActivity(i);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mAccount = Preferences.getPreferences(this).getAccount(getIntent().getStringExtra("account"));
        setContentView((int) R.layout.account_setup_composition);
        if (savedInstanceState != null && savedInstanceState.containsKey("account")) {
            this.mAccount = Preferences.getPreferences(this).getAccount(savedInstanceState.getString("account"));
        }
        this.mAccountName = (EditText) findViewById(R.id.account_name);
        this.mAccountName.setText(this.mAccount.getName());
        this.mAccountEmail = (EditText) findViewById(R.id.account_email);
        this.mAccountEmail.setText(this.mAccount.getEmail());
        this.mAccountAlwaysBcc = (EditText) findViewById(R.id.account_always_bcc);
        this.mAccountAlwaysBcc.setText(this.mAccount.getAlwaysBcc());
        this.mAccountSignatureLayout = (LinearLayout) findViewById(R.id.account_signature_layout);
        this.mAccountSignatureUse = (CheckBox) findViewById(R.id.account_signature_use);
        boolean useSignature = this.mAccount.getSignatureUse();
        this.mAccountSignatureUse.setChecked(useSignature);
        this.mAccountSignatureUse.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                boolean z = false;
                if (isChecked) {
                    AccountSetupComposition.this.mAccountSignatureLayout.setVisibility(0);
                    AccountSetupComposition.this.mAccountSignature.setText(AccountSetupComposition.this.mAccount.getSignature());
                    boolean isSignatureBeforeQuotedText = AccountSetupComposition.this.mAccount.isSignatureBeforeQuotedText();
                    AccountSetupComposition.this.mAccountSignatureBeforeLocation.setChecked(isSignatureBeforeQuotedText);
                    RadioButton access$400 = AccountSetupComposition.this.mAccountSignatureAfterLocation;
                    if (!isSignatureBeforeQuotedText) {
                        z = true;
                    }
                    access$400.setChecked(z);
                    return;
                }
                AccountSetupComposition.this.mAccountSignatureLayout.setVisibility(8);
            }
        });
        this.mAccountSignature = (EditText) findViewById(R.id.account_signature);
        this.mAccountSignatureBeforeLocation = (RadioButton) findViewById(R.id.account_signature_location_before_quoted_text);
        this.mAccountSignatureAfterLocation = (RadioButton) findViewById(R.id.account_signature_location_after_quoted_text);
        if (useSignature) {
            this.mAccountSignature.setText(this.mAccount.getSignature());
            boolean isSignatureBeforeQuotedText = this.mAccount.isSignatureBeforeQuotedText();
            this.mAccountSignatureBeforeLocation.setChecked(isSignatureBeforeQuotedText);
            this.mAccountSignatureAfterLocation.setChecked(!isSignatureBeforeQuotedText);
            return;
        }
        this.mAccountSignatureLayout.setVisibility(8);
    }

    private void saveSettings() {
        this.mAccount.setEmail(this.mAccountEmail.getText().toString());
        this.mAccount.setAlwaysBcc(this.mAccountAlwaysBcc.getText().toString());
        this.mAccount.setName(this.mAccountName.getText().toString());
        this.mAccount.setSignatureUse(this.mAccountSignatureUse.isChecked());
        if (this.mAccountSignatureUse.isChecked()) {
            this.mAccount.setSignature(this.mAccountSignature.getText().toString());
            this.mAccount.setSignatureBeforeQuotedText(this.mAccountSignatureBeforeLocation.isChecked());
        }
        this.mAccount.save(Preferences.getPreferences(this));
    }

    public void onBackPressed() {
        saveSettings();
        super.onBackPressed();
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("account", this.mAccount.getUuid());
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        this.mAccount.save(Preferences.getPreferences(this));
        finish();
    }
}
