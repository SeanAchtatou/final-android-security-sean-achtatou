package com.fsck.k9.activity.setup;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceScreen;
import android.text.TextUtils;
import android.widget.Toast;
import com.fsck.k9.K9;
import com.fsck.k9.Preferences;
import com.fsck.k9.activity.ColorPickerDialog;
import com.fsck.k9.activity.K9PreferenceActivity;
import com.fsck.k9.helper.FileBrowserHelper;
import com.fsck.k9.notification.NotificationController;
import com.fsck.k9.preferences.CheckBoxListPreference;
import com.fsck.k9.preferences.SettingsExporter;
import com.fsck.k9.preferences.Storage;
import com.fsck.k9.preferences.StorageEditor;
import com.fsck.k9.preferences.TimePickerPreference;
import com.fsck.k9.service.MailService;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import yahoo.mail.app.R;

public class Prefs extends K9PreferenceActivity {
    private static final int ACTIVITY_CHOOSE_FOLDER = 1;
    private static final CharSequence[] EMPTY_CHAR_SEQUENCE_ARRAY = new CharSequence[0];
    private static final String PREFERENCE_ANIMATIONS = "animations";
    private static final String PREFERENCE_ATTACHMENT_DEF_PATH = "attachment_default_path";
    private static final String PREFERENCE_AUTOFIT_WIDTH = "messageview_autofit_width";
    private static final String PREFERENCE_BACKGROUND_AS_UNREAD_INDICATOR = "messagelist_background_as_unread_indicator";
    private static final String PREFERENCE_BACKGROUND_OPS = "background_ops";
    private static final String PREFERENCE_COMPOSER_THEME = "messageComposeTheme";
    private static final String PREFERENCE_CONFIRM_ACTIONS = "confirm_actions";
    private static final String PREFERENCE_COUNT_SEARCH = "count_search";
    private static final String PREFERENCE_ChipEnabledOnSingleAccount = "ui_chip_enabled_on_account";
    private static final String PREFERENCE_DEBUG_LOGGING = "debug_logging";
    private static final String PREFERENCE_DISABLE_NOTIFICATION_DURING_QUIET_TIME = "disable_notifications_during_quiet_time";
    private static final String PREFERENCE_FIXED_MESSAGE_THEME = "fixedMessageViewTheme";
    private static final String PREFERENCE_FOLDERLIST_WRAP_NAME = "folderlist_wrap_folder_name";
    private static final String PREFERENCE_FONT_SIZE = "font_size";
    private static final String PREFERENCE_GESTURES = "gestures";
    private static final String PREFERENCE_HIDE_SPECIAL_ACCOUNTS = "hide_special_accounts";
    private static final String PREFERENCE_HIDE_TIMEZONE = "privacy_hide_timezone";
    private static final String PREFERENCE_HIDE_USERAGENT = "privacy_hide_useragent";
    private static final String PREFERENCE_LANGUAGE = "language";
    private static final String PREFERENCE_LOCK_SCREEN_NOTIFICATION_VISIBILITY = "lock_screen_notification_visibility";
    private static final String PREFERENCE_MEASURE_ACCOUNTS = "measure_accounts";
    private static final String PREFERENCE_MESSAGELIST_CHECKBOXES = "messagelist_checkboxes";
    private static final String PREFERENCE_MESSAGELIST_COLORIZE_MISSING_CONTACT_PICTURES = "messagelist_colorize_missing_contact_pictures";
    private static final String PREFERENCE_MESSAGELIST_CONTACT_NAME_COLOR = "messagelist_contact_name_color";
    private static final String PREFERENCE_MESSAGELIST_PREVIEW_LINES = "messagelist_preview_lines";
    private static final String PREFERENCE_MESSAGELIST_SENDER_ABOVE_SUBJECT = "messagelist_sender_above_subject";
    private static final String PREFERENCE_MESSAGELIST_SHOW_CONTACT_NAME = "messagelist_show_contact_name";
    private static final String PREFERENCE_MESSAGELIST_SHOW_CONTACT_PICTURE = "messagelist_show_contact_picture";
    private static final String PREFERENCE_MESSAGELIST_SHOW_CORRESPONDENT_NAMES = "messagelist_show_correspondent_names";
    private static final String PREFERENCE_MESSAGELIST_STARS = "messagelist_stars";
    private static final String PREFERENCE_MESSAGEVIEW_FIXEDWIDTH = "messageview_fixedwidth_font";
    private static final String PREFERENCE_MESSAGEVIEW_RETURN_TO_LIST = "messageview_return_to_list";
    private static final String PREFERENCE_MESSAGEVIEW_SHOW_NEXT = "messageview_show_next";
    private static final String PREFERENCE_MESSAGEVIEW_VISIBLE_REFILE_ACTIONS = "messageview_visible_refile_actions";
    private static final String PREFERENCE_MESSAGE_VIEW_THEME = "messageViewTheme";
    private static final String PREFERENCE_NOTIFICATION_HIDE_SUBJECT = "notification_hide_subject";
    private static final String PREFERENCE_NOTIF_QUICK_DELETE = "notification_quick_delete";
    private static final String PREFERENCE_QUIET_TIME_ENABLED = "quiet_time_enabled";
    private static final String PREFERENCE_QUIET_TIME_ENDS = "quiet_time_ends";
    private static final String PREFERENCE_QUIET_TIME_STARTS = "quiet_time_starts";
    private static final String PREFERENCE_SENSITIVE_LOGGING = "sensitive_logging";
    private static final String PREFERENCE_SPLITVIEW_MODE = "splitview_mode";
    private static final String PREFERENCE_START_INTEGRATED_INBOX = "start_integrated_inbox";
    private static final String PREFERENCE_THEME = "theme";
    private static final String PREFERENCE_THREADED_VIEW = "threaded_view";
    private static final String PREFERENCE_TOOLBAR_COLLAPSIBLE = "ui_toolbar_collapsible";
    private static final String PREFERENCE_VOLUME_NAVIGATION = "volumeNavigation";
    private static final int VISIBLE_REFILE_ACTIONS_ARCHIVE = 1;
    private static final int VISIBLE_REFILE_ACTIONS_COPY = 3;
    private static final int VISIBLE_REFILE_ACTIONS_DELETE = 0;
    private static final int VISIBLE_REFILE_ACTIONS_MOVE = 2;
    private static final int VISIBLE_REFILE_ACTIONS_SPAM = 4;
    private CheckBoxPreference mAnimations;
    /* access modifiers changed from: private */
    public Preference mAttachmentPathPreference;
    private CheckBoxPreference mAutofitWidth;
    private CheckBoxPreference mBackgroundAsUnreadIndicator;
    private ListPreference mBackgroundOps;
    /* access modifiers changed from: private */
    public CheckBoxPreference mChangeContactNameColor;
    private CheckBoxPreference mCheckboxes;
    private CheckBoxPreference mChipEnabledOnSingleAccount;
    private CheckBoxPreference mColorizeMissingContactPictures;
    private ListPreference mComposerTheme;
    private CheckBoxListPreference mConfirmActions;
    private CheckBoxPreference mCountSearch;
    private CheckBoxPreference mDebugLogging;
    private CheckBoxPreference mDisableNotificationDuringQuietTime;
    private CheckBoxPreference mFixedMessageTheme;
    private CheckBoxPreference mFixedWidth;
    private CheckBoxPreference mGestures;
    private CheckBoxPreference mHideSpecialAccounts;
    private CheckBoxPreference mHideTimeZone;
    private CheckBoxPreference mHideUserAgent;
    private ListPreference mLanguage;
    private ListPreference mLockScreenNotificationVisibility;
    private CheckBoxPreference mMeasureAccounts;
    private ListPreference mMessageTheme;
    private ListPreference mNotificationHideSubject;
    private ListPreference mNotificationQuickDelete;
    private ListPreference mPreviewLines;
    private CheckBoxPreference mQuietTimeEnabled;
    /* access modifiers changed from: private */
    public TimePickerPreference mQuietTimeEnds;
    /* access modifiers changed from: private */
    public TimePickerPreference mQuietTimeStarts;
    private CheckBoxPreference mReturnToList;
    private CheckBoxPreference mSenderAboveSubject;
    private CheckBoxPreference mSensitiveLogging;
    private CheckBoxPreference mShowContactName;
    private CheckBoxPreference mShowContactPicture;
    private CheckBoxPreference mShowCorrespondentNames;
    private CheckBoxPreference mShowNext;
    private ListPreference mSplitViewMode;
    private CheckBoxPreference mStars;
    private CheckBoxPreference mStartIntegratedInbox;
    private ListPreference mTheme;
    private CheckBoxPreference mThreadedView;
    private CheckBoxListPreference mVisibleRefileActions;
    private CheckBoxListPreference mVolumeNavigation;
    private CheckBoxPreference mWrapFolderNames;
    private CheckBoxPreference mtoolBarCollasible;

    public static void actionPrefs(Context context) {
        context.startActivity(new Intent(context, Prefs.class));
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.global_preferences);
        this.mLanguage = (ListPreference) findPreference(PREFERENCE_LANGUAGE);
        List<CharSequence> entryVector = new ArrayList<>(Arrays.asList(this.mLanguage.getEntries()));
        List<CharSequence> entryValueVector = new ArrayList<>(Arrays.asList(this.mLanguage.getEntryValues()));
        Set<String> supportedLanguageSet = new HashSet<>(Arrays.asList(getResources().getStringArray(R.array.supported_languages)));
        for (int i = entryVector.size() - 1; i > -1; i--) {
            if (!supportedLanguageSet.contains(entryValueVector.get(i))) {
                entryVector.remove(i);
                entryValueVector.remove(i);
            }
        }
        initListPreference(this.mLanguage, K9.getK9Language(), (CharSequence[]) entryVector.toArray(EMPTY_CHAR_SEQUENCE_ARRAY), (CharSequence[]) entryValueVector.toArray(EMPTY_CHAR_SEQUENCE_ARRAY));
        this.mTheme = setupListPreference(PREFERENCE_THEME, themeIdToName(K9.getK9Theme()));
        this.mFixedMessageTheme = (CheckBoxPreference) findPreference(PREFERENCE_FIXED_MESSAGE_THEME);
        this.mFixedMessageTheme.setChecked(K9.useFixedMessageViewTheme());
        this.mMessageTheme = setupListPreference(PREFERENCE_MESSAGE_VIEW_THEME, themeIdToName(K9.getK9MessageViewThemeSetting()));
        this.mComposerTheme = setupListPreference(PREFERENCE_COMPOSER_THEME, themeIdToName(K9.getK9ComposerThemeSetting()));
        findPreference(PREFERENCE_FONT_SIZE).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                Prefs.this.onFontSizeSettings();
                return true;
            }
        });
        this.mAnimations = (CheckBoxPreference) findPreference(PREFERENCE_ANIMATIONS);
        this.mAnimations.setChecked(K9.showAnimations());
        this.mGestures = (CheckBoxPreference) findPreference(PREFERENCE_GESTURES);
        this.mGestures.setChecked(K9.gesturesEnabled());
        this.mVolumeNavigation = (CheckBoxListPreference) findPreference(PREFERENCE_VOLUME_NAVIGATION);
        this.mVolumeNavigation.setItems(new CharSequence[]{getString(R.string.volume_navigation_message), getString(R.string.volume_navigation_list)});
        this.mVolumeNavigation.setCheckedItems(new boolean[]{K9.useVolumeKeysForNavigationEnabled(), K9.useVolumeKeysForListNavigationEnabled()});
        this.mStartIntegratedInbox = (CheckBoxPreference) findPreference(PREFERENCE_START_INTEGRATED_INBOX);
        this.mStartIntegratedInbox.setChecked(K9.startIntegratedInbox());
        this.mConfirmActions = (CheckBoxListPreference) findPreference(PREFERENCE_CONFIRM_ACTIONS);
        boolean canDeleteFromNotification = NotificationController.platformSupportsExtendedNotifications();
        CharSequence[] confirmActionEntries = new CharSequence[(canDeleteFromNotification ? 5 : 4)];
        boolean[] confirmActionValues = new boolean[(canDeleteFromNotification ? 5 : 4)];
        confirmActionEntries[0] = getString(R.string.global_settings_confirm_action_delete);
        int index = 0 + 1;
        confirmActionValues[0] = K9.confirmDelete();
        confirmActionEntries[index] = getString(R.string.global_settings_confirm_action_delete_starred);
        int index2 = index + 1;
        confirmActionValues[index] = K9.confirmDeleteStarred();
        if (canDeleteFromNotification) {
            confirmActionEntries[index2] = getString(R.string.global_settings_confirm_action_delete_notif);
            confirmActionValues[index2] = K9.confirmDeleteFromNotification();
            index2++;
        }
        confirmActionEntries[index2] = getString(R.string.global_settings_confirm_action_spam);
        int index3 = index2 + 1;
        confirmActionValues[index2] = K9.confirmSpam();
        confirmActionEntries[index3] = getString(R.string.global_settings_confirm_menu_discard);
        int i2 = index3 + 1;
        confirmActionValues[index3] = K9.confirmDiscardMessage();
        this.mConfirmActions.setItems(confirmActionEntries);
        this.mConfirmActions.setCheckedItems(confirmActionValues);
        this.mNotificationHideSubject = setupListPreference(PREFERENCE_NOTIFICATION_HIDE_SUBJECT, K9.getNotificationHideSubject().toString());
        this.mMeasureAccounts = (CheckBoxPreference) findPreference(PREFERENCE_MEASURE_ACCOUNTS);
        this.mMeasureAccounts.setChecked(K9.measureAccounts());
        this.mCountSearch = (CheckBoxPreference) findPreference(PREFERENCE_COUNT_SEARCH);
        this.mCountSearch.setChecked(K9.countSearchMessages());
        this.mHideSpecialAccounts = (CheckBoxPreference) findPreference(PREFERENCE_HIDE_SPECIAL_ACCOUNTS);
        this.mHideSpecialAccounts.setChecked(K9.isHideSpecialAccounts());
        this.mPreviewLines = setupListPreference(PREFERENCE_MESSAGELIST_PREVIEW_LINES, Integer.toString(K9.messageListPreviewLines()));
        this.mSenderAboveSubject = (CheckBoxPreference) findPreference(PREFERENCE_MESSAGELIST_SENDER_ABOVE_SUBJECT);
        this.mSenderAboveSubject.setChecked(K9.messageListSenderAboveSubject());
        this.mCheckboxes = (CheckBoxPreference) findPreference(PREFERENCE_MESSAGELIST_CHECKBOXES);
        this.mCheckboxes.setChecked(K9.messageListCheckboxes());
        this.mStars = (CheckBoxPreference) findPreference(PREFERENCE_MESSAGELIST_STARS);
        this.mStars.setChecked(K9.messageListStars());
        this.mShowCorrespondentNames = (CheckBoxPreference) findPreference(PREFERENCE_MESSAGELIST_SHOW_CORRESPONDENT_NAMES);
        this.mShowCorrespondentNames.setChecked(K9.showCorrespondentNames());
        this.mShowContactName = (CheckBoxPreference) findPreference(PREFERENCE_MESSAGELIST_SHOW_CONTACT_NAME);
        this.mShowContactName.setChecked(K9.showContactName());
        this.mShowContactPicture = (CheckBoxPreference) findPreference(PREFERENCE_MESSAGELIST_SHOW_CONTACT_PICTURE);
        this.mShowContactPicture.setChecked(K9.showContactPicture());
        this.mColorizeMissingContactPictures = (CheckBoxPreference) findPreference(PREFERENCE_MESSAGELIST_COLORIZE_MISSING_CONTACT_PICTURES);
        this.mColorizeMissingContactPictures.setChecked(K9.isColorizeMissingContactPictures());
        this.mBackgroundAsUnreadIndicator = (CheckBoxPreference) findPreference(PREFERENCE_BACKGROUND_AS_UNREAD_INDICATOR);
        this.mBackgroundAsUnreadIndicator.setChecked(K9.useBackgroundAsUnreadIndicator());
        this.mChangeContactNameColor = (CheckBoxPreference) findPreference(PREFERENCE_MESSAGELIST_CONTACT_NAME_COLOR);
        this.mChangeContactNameColor.setChecked(K9.changeContactNameColor());
        this.mThreadedView = (CheckBoxPreference) findPreference(PREFERENCE_THREADED_VIEW);
        this.mThreadedView.setChecked(K9.isThreadedViewEnabled());
        if (K9.changeContactNameColor()) {
            this.mChangeContactNameColor.setSummary((int) R.string.global_settings_registered_name_color_changed);
        } else {
            this.mChangeContactNameColor.setSummary((int) R.string.global_settings_registered_name_color_default);
        }
        this.mChangeContactNameColor.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                Boolean checked = (Boolean) newValue;
                if (checked.booleanValue()) {
                    Prefs.this.onChooseContactNameColor();
                    Prefs.this.mChangeContactNameColor.setSummary((int) R.string.global_settings_registered_name_color_changed);
                } else {
                    Prefs.this.mChangeContactNameColor.setSummary((int) R.string.global_settings_registered_name_color_default);
                }
                Prefs.this.mChangeContactNameColor.setChecked(checked.booleanValue());
                return false;
            }
        });
        this.mFixedWidth = (CheckBoxPreference) findPreference(PREFERENCE_MESSAGEVIEW_FIXEDWIDTH);
        this.mFixedWidth.setChecked(K9.messageViewFixedWidthFont());
        this.mReturnToList = (CheckBoxPreference) findPreference(PREFERENCE_MESSAGEVIEW_RETURN_TO_LIST);
        this.mReturnToList.setChecked(K9.messageViewReturnToList());
        this.mShowNext = (CheckBoxPreference) findPreference(PREFERENCE_MESSAGEVIEW_SHOW_NEXT);
        this.mShowNext.setChecked(K9.messageViewShowNext());
        this.mAutofitWidth = (CheckBoxPreference) findPreference(PREFERENCE_AUTOFIT_WIDTH);
        this.mAutofitWidth.setChecked(K9.autofitWidth());
        this.mQuietTimeEnabled = (CheckBoxPreference) findPreference(PREFERENCE_QUIET_TIME_ENABLED);
        this.mQuietTimeEnabled.setChecked(K9.getQuietTimeEnabled());
        this.mDisableNotificationDuringQuietTime = (CheckBoxPreference) findPreference(PREFERENCE_DISABLE_NOTIFICATION_DURING_QUIET_TIME);
        this.mDisableNotificationDuringQuietTime.setChecked(!K9.isNotificationDuringQuietTimeEnabled());
        this.mQuietTimeStarts = (TimePickerPreference) findPreference(PREFERENCE_QUIET_TIME_STARTS);
        this.mQuietTimeStarts.setDefaultValue(K9.getQuietTimeStarts());
        this.mQuietTimeStarts.setSummary(K9.getQuietTimeStarts());
        this.mQuietTimeStarts.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                Prefs.this.mQuietTimeStarts.setSummary((String) newValue);
                return false;
            }
        });
        this.mQuietTimeEnds = (TimePickerPreference) findPreference(PREFERENCE_QUIET_TIME_ENDS);
        this.mQuietTimeEnds.setSummary(K9.getQuietTimeEnds());
        this.mQuietTimeEnds.setDefaultValue(K9.getQuietTimeEnds());
        this.mQuietTimeEnds.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                Prefs.this.mQuietTimeEnds.setSummary((String) newValue);
                return false;
            }
        });
        this.mNotificationQuickDelete = setupListPreference(PREFERENCE_NOTIF_QUICK_DELETE, K9.getNotificationQuickDeleteBehaviour().toString());
        if (!NotificationController.platformSupportsExtendedNotifications()) {
            ((PreferenceScreen) findPreference("notification_preferences")).removePreference(this.mNotificationQuickDelete);
            this.mNotificationQuickDelete = null;
        }
        this.mLockScreenNotificationVisibility = setupListPreference(PREFERENCE_LOCK_SCREEN_NOTIFICATION_VISIBILITY, K9.getLockScreenNotificationVisibility().toString());
        if (!NotificationController.platformSupportsLockScreenNotifications()) {
            ((PreferenceScreen) findPreference("notification_preferences")).removePreference(this.mLockScreenNotificationVisibility);
            this.mLockScreenNotificationVisibility = null;
        }
        this.mBackgroundOps = setupListPreference(PREFERENCE_BACKGROUND_OPS, K9.getBackgroundOps().name());
        this.mDebugLogging = (CheckBoxPreference) findPreference(PREFERENCE_DEBUG_LOGGING);
        this.mSensitiveLogging = (CheckBoxPreference) findPreference(PREFERENCE_SENSITIVE_LOGGING);
        this.mHideUserAgent = (CheckBoxPreference) findPreference(PREFERENCE_HIDE_USERAGENT);
        this.mHideTimeZone = (CheckBoxPreference) findPreference(PREFERENCE_HIDE_TIMEZONE);
        this.mtoolBarCollasible = (CheckBoxPreference) findPreference(PREFERENCE_TOOLBAR_COLLAPSIBLE);
        this.mChipEnabledOnSingleAccount = (CheckBoxPreference) findPreference(PREFERENCE_ChipEnabledOnSingleAccount);
        this.mDebugLogging.setChecked(K9.DEBUG);
        this.mSensitiveLogging.setChecked(K9.DEBUG_SENSITIVE);
        this.mHideUserAgent.setChecked(K9.hideUserAgent());
        this.mHideTimeZone.setChecked(K9.hideTimeZone());
        this.mtoolBarCollasible.setChecked(K9.isToolBarCollapsible());
        this.mChipEnabledOnSingleAccount.setChecked(K9.isChipEnabledOnSingleAccount());
        this.mAttachmentPathPreference = findPreference(PREFERENCE_ATTACHMENT_DEF_PATH);
        this.mAttachmentPathPreference.setSummary(K9.getAttachmentDefaultPath());
        this.mAttachmentPathPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            FileBrowserHelper.FileBrowserFailOverCallback callback = new FileBrowserHelper.FileBrowserFailOverCallback() {
                public void onPathEntered(String path) {
                    Prefs.this.mAttachmentPathPreference.setSummary(path);
                    K9.setAttachmentDefaultPath(path);
                }

                public void onCancel() {
                }
            };

            public boolean onPreferenceClick(Preference preference) {
                FileBrowserHelper.getInstance().showFileBrowserActivity(Prefs.this, new File(K9.getAttachmentDefaultPath()), 1, this.callback);
                return true;
            }
        });
        this.mWrapFolderNames = (CheckBoxPreference) findPreference(PREFERENCE_FOLDERLIST_WRAP_NAME);
        this.mWrapFolderNames.setChecked(K9.wrapFolderNames());
        this.mVisibleRefileActions = (CheckBoxListPreference) findPreference(PREFERENCE_MESSAGEVIEW_VISIBLE_REFILE_ACTIONS);
        CharSequence[] visibleRefileActionsEntries = {getString(R.string.delete_action), getString(R.string.archive_action), getString(R.string.move_action), getString(R.string.copy_action), getString(R.string.spam_action)};
        boolean[] visibleRefileActionsValues = {K9.isMessageViewDeleteActionVisible(), K9.isMessageViewArchiveActionVisible(), K9.isMessageViewMoveActionVisible(), K9.isMessageViewCopyActionVisible(), K9.isMessageViewSpamActionVisible()};
        this.mVisibleRefileActions.setItems(visibleRefileActionsEntries);
        this.mVisibleRefileActions.setCheckedItems(visibleRefileActionsValues);
        this.mSplitViewMode = (ListPreference) findPreference(PREFERENCE_SPLITVIEW_MODE);
        initListPreference(this.mSplitViewMode, K9.getSplitViewMode().name(), this.mSplitViewMode.getEntries(), this.mSplitViewMode.getEntryValues());
    }

    private static String themeIdToName(K9.Theme theme) {
        switch (theme) {
            case DARK:
                return "dark";
            case USE_GLOBAL:
                return SettingsExporter.GLOBAL_ELEMENT;
            default:
                return "light";
        }
    }

    private static K9.Theme themeNameToId(String theme) {
        if (TextUtils.equals(theme, "dark")) {
            return K9.Theme.DARK;
        }
        if (TextUtils.equals(theme, SettingsExporter.GLOBAL_ELEMENT)) {
            return K9.Theme.USE_GLOBAL;
        }
        return K9.Theme.LIGHT;
    }

    private void saveSettings() {
        boolean z;
        boolean z2 = false;
        Storage storage = Preferences.getPreferences(this).getStorage();
        K9.setK9Language(this.mLanguage.getValue());
        K9.setK9Theme(themeNameToId(this.mTheme.getValue()));
        K9.setUseFixedMessageViewTheme(this.mFixedMessageTheme.isChecked());
        K9.setK9MessageViewThemeSetting(themeNameToId(this.mMessageTheme.getValue()));
        K9.setK9ComposerThemeSetting(themeNameToId(this.mComposerTheme.getValue()));
        K9.setAnimations(this.mAnimations.isChecked());
        K9.setGesturesEnabled(this.mGestures.isChecked());
        K9.setUseVolumeKeysForNavigation(this.mVolumeNavigation.getCheckedItems()[0]);
        K9.setUseVolumeKeysForListNavigation(this.mVolumeNavigation.getCheckedItems()[1]);
        if (this.mHideSpecialAccounts.isChecked() || !this.mStartIntegratedInbox.isChecked()) {
            z = false;
        } else {
            z = true;
        }
        K9.setStartIntegratedInbox(z);
        K9.setNotificationHideSubject(K9.NotificationHideSubject.valueOf(this.mNotificationHideSubject.getValue()));
        int index = 0 + 1;
        K9.setConfirmDelete(this.mConfirmActions.getCheckedItems()[0]);
        int index2 = index + 1;
        K9.setConfirmDeleteStarred(this.mConfirmActions.getCheckedItems()[index]);
        if (NotificationController.platformSupportsExtendedNotifications()) {
            K9.setConfirmDeleteFromNotification(this.mConfirmActions.getCheckedItems()[index2]);
            index2++;
        }
        int index3 = index2 + 1;
        K9.setConfirmSpam(this.mConfirmActions.getCheckedItems()[index2]);
        int i = index3 + 1;
        K9.setConfirmDiscardMessage(this.mConfirmActions.getCheckedItems()[index3]);
        K9.setMeasureAccounts(this.mMeasureAccounts.isChecked());
        K9.setCountSearchMessages(this.mCountSearch.isChecked());
        K9.setHideSpecialAccounts(this.mHideSpecialAccounts.isChecked());
        K9.setMessageListPreviewLines(Integer.parseInt(this.mPreviewLines.getValue()));
        K9.setMessageListCheckboxes(this.mCheckboxes.isChecked());
        K9.setMessageListStars(this.mStars.isChecked());
        K9.setShowCorrespondentNames(this.mShowCorrespondentNames.isChecked());
        K9.setMessageListSenderAboveSubject(this.mSenderAboveSubject.isChecked());
        K9.setShowContactName(this.mShowContactName.isChecked());
        K9.setShowContactPicture(this.mShowContactPicture.isChecked());
        K9.setColorizeMissingContactPictures(this.mColorizeMissingContactPictures.isChecked());
        K9.setUseBackgroundAsUnreadIndicator(this.mBackgroundAsUnreadIndicator.isChecked());
        K9.setThreadedViewEnabled(this.mThreadedView.isChecked());
        K9.setChangeContactNameColor(this.mChangeContactNameColor.isChecked());
        K9.setMessageViewFixedWidthFont(this.mFixedWidth.isChecked());
        K9.setMessageViewReturnToList(this.mReturnToList.isChecked());
        K9.setMessageViewShowNext(this.mShowNext.isChecked());
        K9.setAutofitWidth(this.mAutofitWidth.isChecked());
        K9.setQuietTimeEnabled(this.mQuietTimeEnabled.isChecked());
        boolean[] enabledRefileActions = this.mVisibleRefileActions.getCheckedItems();
        K9.setMessageViewDeleteActionVisible(enabledRefileActions[0]);
        K9.setMessageViewArchiveActionVisible(enabledRefileActions[1]);
        K9.setMessageViewMoveActionVisible(enabledRefileActions[2]);
        K9.setMessageViewCopyActionVisible(enabledRefileActions[3]);
        K9.setMessageViewSpamActionVisible(enabledRefileActions[4]);
        if (!this.mDisableNotificationDuringQuietTime.isChecked()) {
            z2 = true;
        }
        K9.setNotificationDuringQuietTimeEnabled(z2);
        K9.setQuietTimeStarts(this.mQuietTimeStarts.getTime());
        K9.setQuietTimeEnds(this.mQuietTimeEnds.getTime());
        K9.setWrapFolderNames(this.mWrapFolderNames.isChecked());
        if (this.mNotificationQuickDelete != null) {
            K9.setNotificationQuickDeleteBehaviour(K9.NotificationQuickDelete.valueOf(this.mNotificationQuickDelete.getValue()));
        }
        if (this.mLockScreenNotificationVisibility != null) {
            K9.setLockScreenNotificationVisibility(K9.LockScreenNotificationVisibility.valueOf(this.mLockScreenNotificationVisibility.getValue()));
        }
        K9.setSplitViewMode(K9.SplitViewMode.valueOf(this.mSplitViewMode.getValue()));
        K9.setAttachmentDefaultPath(this.mAttachmentPathPreference.getSummary().toString());
        boolean needsRefresh = K9.setBackgroundOps(this.mBackgroundOps.getValue());
        if (!K9.DEBUG && this.mDebugLogging.isChecked()) {
            Toast.makeText(this, (int) R.string.debug_logging_enabled, 1).show();
        }
        K9.DEBUG = this.mDebugLogging.isChecked();
        K9.DEBUG_SENSITIVE = this.mSensitiveLogging.isChecked();
        K9.setHideUserAgent(this.mHideUserAgent.isChecked());
        K9.setHideTimeZone(this.mHideTimeZone.isChecked());
        K9.setToolBarCollapsible(this.mtoolBarCollasible.isChecked());
        K9.setChipEnabledOnSingleAccount(this.mChipEnabledOnSingleAccount.isChecked());
        StorageEditor editor = storage.edit();
        K9.save(editor);
        editor.commit();
        if (needsRefresh) {
            MailService.actionReset(this, null);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        saveSettings();
        super.onPause();
    }

    /* access modifiers changed from: private */
    public void onFontSizeSettings() {
        FontSizeSettings.actionEditSettings(this);
    }

    /* access modifiers changed from: private */
    public void onChooseContactNameColor() {
        new ColorPickerDialog(this, new ColorPickerDialog.OnColorChangedListener() {
            public void colorChanged(int color) {
                K9.setContactNameColor(color);
            }
        }, K9.getContactNameColor()).show();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Uri fileUri;
        String filePath;
        switch (requestCode) {
            case 1:
                if (!(resultCode != -1 || data == null || (fileUri = data.getData()) == null || (filePath = fileUri.getPath()) == null)) {
                    this.mAttachmentPathPreference.setSummary(filePath.toString());
                    K9.setAttachmentDefaultPath(filePath.toString());
                    break;
                }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
