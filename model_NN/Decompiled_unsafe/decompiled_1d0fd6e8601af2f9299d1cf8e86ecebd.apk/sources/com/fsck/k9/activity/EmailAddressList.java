package com.fsck.k9.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import com.fsck.k9.helper.ContactItem;
import yahoo.mail.app.R;

public class EmailAddressList extends K9ListActivity implements AdapterView.OnItemClickListener {
    public static final String EXTRA_CONTACT_ITEM = "contact";
    public static final String EXTRA_EMAIL_ADDRESS = "emailAddress";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.email_address_list);
        ContactItem contact = (ContactItem) getIntent().getSerializableExtra(EXTRA_CONTACT_ITEM);
        if (contact == null) {
            finish();
            return;
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, (int) R.layout.email_address_list_item, contact.emailAddresses);
        ListView listView = getListView();
        listView.setOnItemClickListener(this);
        listView.setAdapter((ListAdapter) adapter);
        setTitle(contact.displayName);
    }

    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String item = (String) parent.getItemAtPosition(position);
        Toast.makeText(this, item, 1).show();
        Intent intent = new Intent();
        intent.putExtra(EXTRA_EMAIL_ADDRESS, item);
        setResult(-1, intent);
        finish();
    }
}
