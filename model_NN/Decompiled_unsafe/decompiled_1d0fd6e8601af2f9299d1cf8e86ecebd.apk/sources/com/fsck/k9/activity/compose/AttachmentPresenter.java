package com.fsck.k9.activity.compose;

import android.annotation.TargetApi;
import android.app.LoaderManager;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import com.fsck.k9.activity.compose.ComposeCryptoStatus;
import com.fsck.k9.activity.loader.AttachmentContentLoader;
import com.fsck.k9.activity.loader.AttachmentInfoLoader;
import com.fsck.k9.activity.misc.Attachment;
import com.fsck.k9.mailstore.AttachmentViewInfo;
import com.fsck.k9.mailstore.MessageViewInfo;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;

public class AttachmentPresenter {
    private static final String LOADER_ARG_ATTACHMENT = "attachment";
    private static final int LOADER_ID_MASK = 64;
    public static final int MAX_TOTAL_LOADERS = 63;
    public static final int REQUEST_CODE_ATTACHMENT_URI = 1;
    private static final String STATE_KEY_ATTACHMENTS = "com.fsck.k9.activity.MessageCompose.attachments";
    private static final String STATE_KEY_NEXT_LOADER_ID = "nextLoaderId";
    private static final String STATE_KEY_WAITING_FOR_ATTACHMENTS = "waitingForAttachments";
    private WaitingAction actionToPerformAfterWaiting = WaitingAction.NONE;
    /* access modifiers changed from: private */
    public final AttachmentMvpView attachmentMvpView;
    /* access modifiers changed from: private */
    public LinkedHashMap<Uri, Attachment> attachments;
    /* access modifiers changed from: private */
    public final Context context;
    /* access modifiers changed from: private */
    public final LoaderManager loaderManager;
    private LoaderManager.LoaderCallbacks<Attachment> mAttachmentContentLoaderCallback = new LoaderManager.LoaderCallbacks<Attachment>() {
        public /* bridge */ /* synthetic */ void onLoadFinished(Loader loader, Object obj) {
            onLoadFinished((Loader<Attachment>) loader, (Attachment) obj);
        }

        public Loader<Attachment> onCreateLoader(int id, Bundle args) {
            return new AttachmentContentLoader(AttachmentPresenter.this.context, (Attachment) AttachmentPresenter.this.attachments.get((Uri) args.getParcelable("attachment")));
        }

        public void onLoadFinished(Loader<Attachment> loader, Attachment attachment) {
            AttachmentPresenter.this.loaderManager.destroyLoader(loader.getId());
            if (AttachmentPresenter.this.attachments.containsKey(attachment.uri)) {
                if (attachment.state == Attachment.LoadingState.COMPLETE) {
                    AttachmentPresenter.this.attachmentMvpView.updateAttachmentView(attachment);
                    AttachmentPresenter.this.attachments.put(attachment.uri, attachment);
                } else {
                    AttachmentPresenter.this.attachments.remove(attachment.uri);
                    AttachmentPresenter.this.attachmentMvpView.removeAttachmentView(attachment);
                }
                AttachmentPresenter.this.postPerformStalledAction();
            }
        }

        public void onLoaderReset(Loader<Attachment> loader) {
        }
    };
    private LoaderManager.LoaderCallbacks<Attachment> mAttachmentInfoLoaderCallback = new LoaderManager.LoaderCallbacks<Attachment>() {
        public /* bridge */ /* synthetic */ void onLoadFinished(Loader loader, Object obj) {
            onLoadFinished((Loader<Attachment>) loader, (Attachment) obj);
        }

        public Loader<Attachment> onCreateLoader(int id, Bundle args) {
            return new AttachmentInfoLoader(AttachmentPresenter.this.context, (Attachment) AttachmentPresenter.this.attachments.get((Uri) args.getParcelable("attachment")));
        }

        public void onLoadFinished(Loader<Attachment> loader, Attachment attachment) {
            AttachmentPresenter.this.loaderManager.destroyLoader(loader.getId());
            if (AttachmentPresenter.this.attachments.containsKey(attachment.uri)) {
                AttachmentPresenter.this.attachmentMvpView.updateAttachmentView(attachment);
                AttachmentPresenter.this.attachments.put(attachment.uri, attachment);
                AttachmentPresenter.this.initAttachmentContentLoader(attachment);
            }
        }

        public void onLoaderReset(Loader<Attachment> loader) {
        }
    };
    private int nextLoaderId = 0;

    public interface AttachmentMvpView {
        void addAttachmentView(Attachment attachment);

        void dismissWaitingForAttachmentDialog();

        void performSaveAfterChecks();

        void performSendAfterChecks();

        void removeAttachmentView(Attachment attachment);

        void showMissingAttachmentsPartialMessageWarning();

        void showPickAttachmentDialog(int i);

        void showWaitingForAttachmentDialog(WaitingAction waitingAction);

        void updateAttachmentView(Attachment attachment);
    }

    public enum WaitingAction {
        NONE,
        SEND,
        SAVE
    }

    public AttachmentPresenter(Context context2, AttachmentMvpView attachmentMvpView2, LoaderManager loaderManager2) {
        this.context = context2;
        this.attachmentMvpView = attachmentMvpView2;
        this.loaderManager = loaderManager2;
        this.attachments = new LinkedHashMap<>();
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putString(STATE_KEY_WAITING_FOR_ATTACHMENTS, this.actionToPerformAfterWaiting.name());
        outState.putParcelableArrayList(STATE_KEY_ATTACHMENTS, createAttachmentList());
        outState.putInt(STATE_KEY_NEXT_LOADER_ID, this.nextLoaderId);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        this.actionToPerformAfterWaiting = WaitingAction.valueOf(savedInstanceState.getString(STATE_KEY_WAITING_FOR_ATTACHMENTS));
        this.nextLoaderId = savedInstanceState.getInt(STATE_KEY_NEXT_LOADER_ID);
        Iterator it = savedInstanceState.getParcelableArrayList(STATE_KEY_ATTACHMENTS).iterator();
        while (it.hasNext()) {
            Attachment attachment = (Attachment) it.next();
            this.attachments.put(attachment.uri, attachment);
            this.attachmentMvpView.addAttachmentView(attachment);
            if (attachment.state == Attachment.LoadingState.URI_ONLY) {
                initAttachmentInfoLoader(attachment);
            } else if (attachment.state == Attachment.LoadingState.METADATA) {
                initAttachmentContentLoader(attachment);
            }
        }
    }

    public boolean checkOkForSendingOrDraftSaving() {
        if (this.actionToPerformAfterWaiting != WaitingAction.NONE) {
            return true;
        }
        if (!hasLoadingAttachments()) {
            return false;
        }
        this.actionToPerformAfterWaiting = WaitingAction.SEND;
        this.attachmentMvpView.showWaitingForAttachmentDialog(this.actionToPerformAfterWaiting);
        return true;
    }

    private boolean hasLoadingAttachments() {
        for (Attachment attachment : this.attachments.values()) {
            Loader loader = this.loaderManager.getLoader(attachment.loaderId);
            if (loader != null && loader.isStarted()) {
                return true;
            }
        }
        return false;
    }

    public ArrayList<Attachment> createAttachmentList() {
        ArrayList<Attachment> result = new ArrayList<>();
        for (Attachment attachment : this.attachments.values()) {
            result.add(attachment);
        }
        return result;
    }

    public void onClickAddAttachment(RecipientPresenter recipientPresenter) {
        ComposeCryptoStatus.AttachErrorState maybeAttachErrorState = recipientPresenter.getCurrentCryptoStatus().getAttachErrorStateOrNull();
        if (maybeAttachErrorState != null) {
            recipientPresenter.showPgpAttachError(maybeAttachErrorState);
        } else {
            this.attachmentMvpView.showPickAttachmentDialog(1);
        }
    }

    private void addAttachment(Uri uri) {
        addAttachment(uri, null);
    }

    public void addAttachment(AttachmentViewInfo attachmentViewInfo) {
        if (this.attachments.containsKey(attachmentViewInfo.internalUri)) {
            throw new IllegalStateException("Received the same attachmentViewInfo twice!");
        }
        addAttachmentAndStartLoader(Attachment.createAttachment(attachmentViewInfo.internalUri, getNextFreeLoaderId(), attachmentViewInfo.mimeType).deriveWithMetadataLoaded(attachmentViewInfo.mimeType, attachmentViewInfo.displayName, attachmentViewInfo.size));
    }

    public void addAttachment(Uri uri, String contentType) {
        if (!this.attachments.containsKey(uri)) {
            addAttachmentAndStartLoader(Attachment.createAttachment(uri, getNextFreeLoaderId(), contentType));
        }
    }

    public boolean loadNonInlineAttachments(MessageViewInfo messageViewInfo) {
        boolean allPartsAvailable = true;
        for (AttachmentViewInfo attachmentViewInfo : messageViewInfo.attachments) {
            if (!attachmentViewInfo.inlineAttachment) {
                if (!attachmentViewInfo.isContentAvailable) {
                    allPartsAvailable = false;
                } else {
                    addAttachment(attachmentViewInfo);
                }
            }
        }
        return allPartsAvailable;
    }

    public void processMessageToForward(MessageViewInfo messageViewInfo) {
        if (!loadNonInlineAttachments(messageViewInfo)) {
            this.attachmentMvpView.showMissingAttachmentsPartialMessageWarning();
        }
    }

    private void addAttachmentAndStartLoader(Attachment attachment) {
        this.attachments.put(attachment.uri, attachment);
        this.attachmentMvpView.addAttachmentView(attachment);
        if (attachment.state == Attachment.LoadingState.URI_ONLY) {
            initAttachmentInfoLoader(attachment);
        } else if (attachment.state == Attachment.LoadingState.METADATA) {
            initAttachmentContentLoader(attachment);
        } else {
            throw new IllegalStateException("Attachment can only be added in URI_ONLY or METADATA state!");
        }
    }

    private void initAttachmentInfoLoader(Attachment attachment) {
        if (attachment.state != Attachment.LoadingState.URI_ONLY) {
            throw new IllegalStateException("initAttachmentInfoLoader can only be called for URI_ONLY state!");
        }
        Bundle bundle = new Bundle();
        bundle.putParcelable("attachment", attachment.uri);
        this.loaderManager.initLoader(attachment.loaderId, bundle, this.mAttachmentInfoLoaderCallback);
    }

    /* access modifiers changed from: private */
    public void initAttachmentContentLoader(Attachment attachment) {
        if (attachment.state != Attachment.LoadingState.METADATA) {
            throw new IllegalStateException("initAttachmentContentLoader can only be called for METADATA state!");
        }
        Bundle bundle = new Bundle();
        bundle.putParcelable("attachment", attachment.uri);
        this.loaderManager.initLoader(attachment.loaderId, bundle, this.mAttachmentContentLoaderCallback);
    }

    private int getNextFreeLoaderId() {
        if (this.nextLoaderId >= 63) {
            throw new AssertionError("more than 63 attachments? hum.");
        }
        int i = this.nextLoaderId;
        this.nextLoaderId = i + 1;
        return i | 64;
    }

    /* access modifiers changed from: private */
    public void postPerformStalledAction() {
        new Handler().post(new Runnable() {
            public void run() {
                AttachmentPresenter.this.performStalledAction();
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void performStalledAction() {
        this.attachmentMvpView.dismissWaitingForAttachmentDialog();
        WaitingAction waitingFor = this.actionToPerformAfterWaiting;
        this.actionToPerformAfterWaiting = WaitingAction.NONE;
        switch (waitingFor) {
            case SEND:
                this.attachmentMvpView.performSendAfterChecks();
                return;
            case SAVE:
                this.attachmentMvpView.performSaveAfterChecks();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: package-private */
    @TargetApi(16)
    public void addAttachmentsFromResultIntent(Intent data) {
        ClipData clipData;
        if (Build.VERSION.SDK_INT < 19 || (clipData = data.getClipData()) == null) {
            Uri uri = data.getData();
            if (uri != null) {
                addAttachment(uri);
                return;
            }
            return;
        }
        int end = clipData.getItemCount();
        for (int i = 0; i < end; i++) {
            Uri uri2 = clipData.getItemAt(i).getUri();
            if (uri2 != null) {
                addAttachment(uri2);
            }
        }
    }

    public void attachmentProgressDialogCancelled() {
        this.actionToPerformAfterWaiting = WaitingAction.NONE;
    }

    public void onClickRemoveAttachment(Uri uri) {
        Attachment attachment = this.attachments.get(uri);
        this.loaderManager.destroyLoader(attachment.loaderId);
        this.attachmentMvpView.removeAttachmentView(attachment);
        this.attachments.remove(uri);
    }

    public void onActivityResult(int resultCode, int requestCode, Intent data) {
        if (requestCode != 1) {
            throw new AssertionError("onActivityResult must only be called for our request code");
        } else if (resultCode == -1 && data != null) {
            addAttachmentsFromResultIntent(data);
        }
    }
}
