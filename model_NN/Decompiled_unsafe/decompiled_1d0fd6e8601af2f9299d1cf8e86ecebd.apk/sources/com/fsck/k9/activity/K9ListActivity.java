package com.fsck.k9.activity;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.widget.ListView;
import com.fsck.k9.K9;
import com.fsck.k9.activity.K9ActivityCommon;
import com.fsck.k9.activity.misc.SwipeGestureDetector;

public class K9ListActivity extends ListActivity implements K9ActivityCommon.K9ActivityMagic {
    private K9ActivityCommon mBase;

    public void onCreate(Bundle savedInstanceState) {
        this.mBase = K9ActivityCommon.newInstance(this);
        super.onCreate(savedInstanceState);
    }

    public boolean dispatchTouchEvent(MotionEvent event) {
        this.mBase.preDispatchTouchEvent(event);
        return super.dispatchTouchEvent(event);
    }

    public void onResume() {
        super.onResume();
    }

    public void setupGestureDetector(SwipeGestureDetector.OnSwipeGestureListener listener) {
        this.mBase.setupGestureDetector(listener);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (!K9.useVolumeKeysForListNavigationEnabled() || (keyCode != 24 && keyCode != 25)) {
            return super.onKeyDown(keyCode, event);
        }
        ListView listView = getListView();
        int currentPosition = listView.getSelectedItemPosition();
        if (currentPosition == -1 || listView.isInTouchMode()) {
            currentPosition = listView.getFirstVisiblePosition();
        }
        if (keyCode == 24 && currentPosition > 0) {
            listView.setSelection(currentPosition - 1);
        } else if (keyCode == 25 && currentPosition < listView.getCount()) {
            listView.setSelection(currentPosition + 1);
        }
        return true;
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (!K9.useVolumeKeysForListNavigationEnabled() || (keyCode != 24 && keyCode != 25)) {
            return super.onKeyUp(keyCode, event);
        }
        return true;
    }
}
