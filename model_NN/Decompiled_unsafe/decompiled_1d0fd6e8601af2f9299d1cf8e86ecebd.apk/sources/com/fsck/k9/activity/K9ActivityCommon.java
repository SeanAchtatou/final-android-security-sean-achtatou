package com.fsck.k9.activity;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.view.GestureDetector;
import android.view.MotionEvent;
import com.fsck.k9.K9;
import com.fsck.k9.activity.misc.SwipeGestureDetector;
import java.util.Locale;

public class K9ActivityCommon {
    private Activity mActivity;
    private GestureDetector mGestureDetector;

    public interface K9ActivityMagic {
        void setupGestureDetector(SwipeGestureDetector.OnSwipeGestureListener onSwipeGestureListener);
    }

    public static K9ActivityCommon newInstance(Activity activity) {
        return new K9ActivityCommon(activity);
    }

    public static void setLanguage(Context context, String language) {
        Locale locale;
        if (TextUtils.isEmpty(language)) {
            locale = Locale.getDefault();
        } else if (language.length() == 5 && language.charAt(2) == '_') {
            locale = new Locale(language.substring(0, 2), language.substring(3));
        } else {
            locale = new Locale(language);
        }
        Configuration config = new Configuration();
        config.locale = locale;
        Resources resources = context.getResources();
        resources.updateConfiguration(config, resources.getDisplayMetrics());
    }

    private K9ActivityCommon(Activity activity) {
        this.mActivity = activity;
        setLanguage(this.mActivity, K9.getK9Language());
        this.mActivity.setTheme(K9.getK9ThemeResourceId());
    }

    public void preDispatchTouchEvent(MotionEvent event) {
        if (this.mGestureDetector != null) {
            this.mGestureDetector.onTouchEvent(event);
        }
    }

    public int getThemeBackgroundColor() {
        TypedArray array = this.mActivity.getTheme().obtainStyledAttributes(new int[]{16842801});
        int backgroundColor = array.getColor(0, 16711935);
        array.recycle();
        return backgroundColor;
    }

    public void setupGestureDetector(SwipeGestureDetector.OnSwipeGestureListener listener) {
        this.mGestureDetector = new GestureDetector(this.mActivity, new SwipeGestureDetector(this.mActivity, listener));
    }
}
