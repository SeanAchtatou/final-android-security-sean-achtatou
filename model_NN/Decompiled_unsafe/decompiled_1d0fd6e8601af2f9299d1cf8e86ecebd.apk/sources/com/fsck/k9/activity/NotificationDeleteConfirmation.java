package com.fsck.k9.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import com.fsck.k9.Account;
import com.fsck.k9.K9;
import com.fsck.k9.Preferences;
import com.fsck.k9.R;
import com.fsck.k9.controller.MessagingController;
import com.fsck.k9.notification.NotificationActionService;
import java.util.ArrayList;
import java.util.Iterator;

public class NotificationDeleteConfirmation extends Activity {
    private static final int DIALOG_CONFIRM = 1;
    private static final String EXTRA_ACCOUNT_UUID = "accountUuid";
    private static final String EXTRA_MESSAGE_REFERENCES = "messageReferences";
    private Account account;
    private ArrayList<MessageReference> messagesToDelete;

    public static Intent getIntent(Context context, MessageReference messageReference) {
        ArrayList<MessageReference> messageReferences = new ArrayList<>(1);
        messageReferences.add(messageReference);
        return getIntent(context, messageReferences);
    }

    public static Intent getIntent(Context context, ArrayList<MessageReference> messageReferences) {
        String accountUuid = messageReferences.get(0).getAccountUuid();
        Intent intent = new Intent(context, NotificationDeleteConfirmation.class);
        intent.setFlags(335544320);
        intent.putExtra(EXTRA_ACCOUNT_UUID, accountUuid);
        intent.putExtra(EXTRA_MESSAGE_REFERENCES, messageReferences);
        return intent;
    }

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setTheme(K9.getK9Theme() == K9.Theme.LIGHT ? R.style.Theme_K9_Dialog_Translucent_Light : R.style.Theme_K9_Dialog_Translucent_Dark);
        extractExtras();
        showDialog(1);
    }

    private void extractExtras() {
        Intent intent = getIntent();
        String accountUuid = intent.getStringExtra(EXTRA_ACCOUNT_UUID);
        ArrayList<MessageReference> messagesToDelete2 = intent.getParcelableArrayListExtra(EXTRA_MESSAGE_REFERENCES);
        if (accountUuid == null) {
            throw new IllegalArgumentException("accountUuid can't be null");
        } else if (messagesToDelete2 == null) {
            throw new IllegalArgumentException("messageReferences can't be null");
        } else if (messagesToDelete2.isEmpty()) {
            throw new IllegalArgumentException("messageReferences can't be empty");
        } else {
            Account account2 = getAccountFromUuid(accountUuid);
            if (account2 == null) {
                throw new IllegalStateException("accountUuid couldn't be resolved to an account");
            }
            this.account = account2;
            this.messagesToDelete = messagesToDelete2;
        }
    }

    public Dialog onCreateDialog(int dialogId) {
        switch (dialogId) {
            case 1:
                return createDeleteConfirmationDialog(dialogId);
            default:
                return super.onCreateDialog(dialogId);
        }
    }

    public void onPrepareDialog(int dialogId, @NonNull Dialog dialog) {
        AlertDialog alert = (AlertDialog) dialog;
        switch (dialogId) {
            case 1:
                int messageCount = this.messagesToDelete.size();
                alert.setMessage(getResources().getQuantityString(yahoo.mail.app.R.plurals.dialog_confirm_delete_messages, messageCount, Integer.valueOf(messageCount)));
                break;
        }
        super.onPrepareDialog(dialogId, dialog);
    }

    private Account getAccountFromUuid(String accountUuid) {
        return Preferences.getPreferences(this).getAccount(accountUuid);
    }

    private Dialog createDeleteConfirmationDialog(int dialogId) {
        return ConfirmationDialog.create(this, dialogId, yahoo.mail.app.R.string.dialog_confirm_delete_title, "", yahoo.mail.app.R.string.dialog_confirm_delete_confirm_button, yahoo.mail.app.R.string.dialog_confirm_delete_cancel_button, new Runnable() {
            public void run() {
                NotificationDeleteConfirmation.this.deleteAndFinish();
            }
        }, new Runnable() {
            public void run() {
                NotificationDeleteConfirmation.this.finish();
            }
        });
    }

    /* access modifiers changed from: private */
    public void deleteAndFinish() {
        cancelNotifications();
        triggerDelete();
        finish();
    }

    private void cancelNotifications() {
        MessagingController controller = MessagingController.getInstance(this);
        Iterator<MessageReference> it = this.messagesToDelete.iterator();
        while (it.hasNext()) {
            controller.cancelNotificationForMessage(this.account, it.next());
        }
    }

    private void triggerDelete() {
        startService(NotificationActionService.createDeleteAllMessagesIntent(this, this.account.getUuid(), this.messagesToDelete));
    }
}
