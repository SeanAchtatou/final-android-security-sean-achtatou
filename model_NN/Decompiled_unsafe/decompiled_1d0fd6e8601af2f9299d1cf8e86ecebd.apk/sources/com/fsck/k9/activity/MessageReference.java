package com.fsck.k9.activity;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.fsck.k9.Account;
import com.fsck.k9.K9;
import com.fsck.k9.Preferences;
import com.fsck.k9.mail.Flag;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mail.filter.Base64;
import com.fsck.k9.mailstore.LocalFolder;
import com.fsck.k9.mailstore.LocalMessage;
import java.util.StringTokenizer;

public class MessageReference implements Parcelable {
    public static final Parcelable.Creator<MessageReference> CREATOR = new Parcelable.Creator<MessageReference>() {
        public MessageReference createFromParcel(Parcel source) {
            String uid = source.readString();
            String accountUuid = source.readString();
            String folderName = source.readString();
            String flag = source.readString();
            if (flag != null) {
                return new MessageReference(accountUuid, folderName, uid, Flag.valueOf(flag));
            }
            return new MessageReference(accountUuid, folderName, uid, null);
        }

        public MessageReference[] newArray(int size) {
            return new MessageReference[size];
        }
    };
    private static final String IDENTITY_SEPARATOR = ":";
    private static final String IDENTITY_VERSION_1 = "!";
    private final String accountUuid;
    private final Flag flag;
    private final String folderName;
    private final String uid;

    public MessageReference(String accountUuid2, String folderName2, String uid2, Flag flag2) {
        this.accountUuid = accountUuid2;
        this.folderName = folderName2;
        this.uid = uid2;
        this.flag = flag2;
    }

    public MessageReference(String identity) throws MessagingException {
        if (identity == null || identity.length() < 1) {
            throw new MessagingException("Null or truncated MessageReference identity.");
        }
        String accountUuid2 = null;
        String folderName2 = null;
        String uid2 = null;
        Flag flag2 = null;
        if (identity.charAt(0) == IDENTITY_VERSION_1.charAt(0)) {
            StringTokenizer tokens = new StringTokenizer(identity.substring(2), IDENTITY_SEPARATOR, false);
            if (tokens.countTokens() >= 3) {
                accountUuid2 = Base64.decode(tokens.nextToken());
                folderName2 = Base64.decode(tokens.nextToken());
                uid2 = Base64.decode(tokens.nextToken());
                if (tokens.hasMoreTokens()) {
                    String flagString = tokens.nextToken();
                    try {
                        flag2 = Flag.valueOf(flagString);
                    } catch (IllegalArgumentException ie) {
                        throw new MessagingException("Could not thaw message flag '" + flagString + "'", ie);
                    }
                }
                if (K9.DEBUG) {
                    Log.d("k9", "Thawed " + toString());
                }
            } else {
                throw new MessagingException("Invalid MessageReference in " + identity + " identity.");
            }
        }
        this.accountUuid = accountUuid2;
        this.folderName = folderName2;
        this.uid = uid2;
        this.flag = flag2;
    }

    public String toIdentityString() {
        StringBuilder refString = new StringBuilder();
        refString.append(IDENTITY_VERSION_1);
        refString.append(IDENTITY_SEPARATOR);
        refString.append(Base64.encode(this.accountUuid));
        refString.append(IDENTITY_SEPARATOR);
        refString.append(Base64.encode(this.folderName));
        refString.append(IDENTITY_SEPARATOR);
        refString.append(Base64.encode(this.uid));
        if (this.flag != null) {
            refString.append(IDENTITY_SEPARATOR);
            refString.append(this.flag.name());
        }
        return refString.toString();
    }

    public boolean equals(Object o) {
        if (!(o instanceof MessageReference)) {
            return false;
        }
        MessageReference other = (MessageReference) o;
        return equals(other.accountUuid, other.folderName, other.uid);
    }

    public boolean equals(String accountUuid2, String folderName2, String uid2) {
        return (accountUuid2 == this.accountUuid || (accountUuid2 != null && accountUuid2.equals(this.accountUuid))) && (folderName2 == this.folderName || (folderName2 != null && folderName2.equals(this.folderName))) && (uid2 == this.uid || (uid2 != null && uid2.equals(this.uid)));
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((((this.accountUuid == null ? 0 : this.accountUuid.hashCode()) + 31) * 31) + (this.folderName == null ? 0 : this.folderName.hashCode())) * 31;
        if (this.uid != null) {
            i = this.uid.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        return "MessageReference{accountUuid='" + this.accountUuid + '\'' + ", folderName='" + this.folderName + '\'' + ", uid='" + this.uid + '\'' + ", flag=" + this.flag + '}';
    }

    public LocalMessage restoreToLocalMessage(Context context) {
        try {
            Account account = Preferences.getPreferences(context).getAccount(this.accountUuid);
            if (account != null) {
                LocalFolder folder = account.getLocalStore().getFolder(this.folderName);
                if (folder != null) {
                    LocalMessage message = folder.getMessage(this.uid);
                    if (message != null) {
                        return message;
                    }
                    Log.d("k9", "Could not restore message, uid " + this.uid + " is unknown.");
                } else {
                    Log.d("k9", "Could not restore message, folder " + this.folderName + " is unknown.");
                }
            } else {
                Log.d("k9", "Could not restore message, account " + this.accountUuid + " is unknown.");
            }
        } catch (MessagingException e) {
            Log.w("k9", "Could not retrieve message for reference.", e);
        }
        return null;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.uid);
        dest.writeString(this.accountUuid);
        dest.writeString(this.folderName);
        dest.writeString(this.flag == null ? null : this.flag.name());
    }

    public String getAccountUuid() {
        return this.accountUuid;
    }

    public String getFolderName() {
        return this.folderName;
    }

    public String getUid() {
        return this.uid;
    }

    public Flag getFlag() {
        return this.flag;
    }

    public MessageReference withModifiedUid(String newUid) {
        return new MessageReference(this.accountUuid, this.folderName, newUid, this.flag);
    }

    public MessageReference withModifiedFlag(Flag newFlag) {
        return new MessageReference(this.accountUuid, this.folderName, this.uid, newFlag);
    }
}
