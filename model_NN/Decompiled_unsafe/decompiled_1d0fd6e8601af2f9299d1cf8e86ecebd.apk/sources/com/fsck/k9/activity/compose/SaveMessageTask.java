package com.fsck.k9.activity.compose;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import com.fsck.k9.Account;
import com.fsck.k9.controller.MessagingController;
import com.fsck.k9.helper.Contacts;
import com.fsck.k9.mail.Message;

public class SaveMessageTask extends AsyncTask<Void, Void, Void> {
    Account account;
    Contacts contacts;
    Context context;
    long draftId;
    Handler handler;
    Message message;
    boolean saveRemotely;

    public SaveMessageTask(Context context2, Account account2, Contacts contacts2, Handler handler2, Message message2, long draftId2, boolean saveRemotely2) {
        this.context = context2;
        this.account = account2;
        this.contacts = contacts2;
        this.handler = handler2;
        this.message = message2;
        this.draftId = draftId2;
        this.saveRemotely = saveRemotely2;
    }

    /* access modifiers changed from: protected */
    public Void doInBackground(Void... params) {
        MessagingController messagingController = MessagingController.getInstance(this.context);
        this.draftId = messagingController.getId(messagingController.saveDraft(this.account, this.message, this.draftId, this.saveRemotely));
        this.handler.sendMessage(android.os.Message.obtain(this.handler, 4, Long.valueOf(this.draftId)));
        return null;
    }
}
