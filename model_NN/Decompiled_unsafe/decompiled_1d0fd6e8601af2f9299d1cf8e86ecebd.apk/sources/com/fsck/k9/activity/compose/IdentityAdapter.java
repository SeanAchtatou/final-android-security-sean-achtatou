package com.fsck.k9.activity.compose;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.fsck.k9.Account;
import com.fsck.k9.Identity;
import com.fsck.k9.Preferences;
import java.util.ArrayList;
import java.util.List;
import yahoo.mail.app.R;

public class IdentityAdapter extends BaseAdapter {
    private List<Object> mItems;
    private LayoutInflater mLayoutInflater;

    public IdentityAdapter(Context context) {
        this.mLayoutInflater = (LayoutInflater) context.getSystemService("layout_inflater");
        List<Object> items = new ArrayList<>();
        for (Account account : Preferences.getPreferences(context.getApplicationContext()).getAvailableAccounts()) {
            items.add(account);
            for (Identity identity : account.getIdentities()) {
                items.add(new IdentityContainer(identity, account));
            }
        }
        this.mItems = items;
    }

    public int getCount() {
        return this.mItems.size();
    }

    public int getViewTypeCount() {
        return 2;
    }

    public int getItemViewType(int position) {
        return this.mItems.get(position) instanceof Account ? 0 : 1;
    }

    public boolean isEnabled(int position) {
        return this.mItems.get(position) instanceof IdentityContainer;
    }

    public Object getItem(int position) {
        return this.mItems.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public boolean hasStableIds() {
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        Object item = this.mItems.get(position);
        View view2 = null;
        if (item instanceof Account) {
            if (convertView == null || !(convertView.getTag() instanceof AccountHolder)) {
                view2 = this.mLayoutInflater.inflate((int) R.layout.choose_account_item, parent, false);
                AccountHolder holder = new AccountHolder();
                holder.name = (TextView) view2.findViewById(R.id.name);
                holder.chip = view2.findViewById(R.id.chip);
                view2.setTag(holder);
            } else {
                view2 = convertView;
            }
            Account account = (Account) item;
            AccountHolder holder2 = (AccountHolder) view2.getTag();
            holder2.name.setText(account.getDescription());
            holder2.chip.setBackgroundColor(account.getChipColor());
        } else if (item instanceof IdentityContainer) {
            if (convertView == null || !(convertView.getTag() instanceof IdentityHolder)) {
                view = this.mLayoutInflater.inflate((int) R.layout.choose_identity_item, parent, false);
                IdentityHolder holder3 = new IdentityHolder();
                holder3.name = (TextView) view.findViewById(R.id.name);
                holder3.description = (TextView) view.findViewById(R.id.description);
                view.setTag(holder3);
            } else {
                view = convertView;
            }
            Identity identity = ((IdentityContainer) item).identity;
            IdentityHolder holder4 = (IdentityHolder) view2.getTag();
            holder4.name.setText(identity.getDescription());
            holder4.description.setText(getIdentityDescription(identity));
        }
        return view2;
    }

    private static String getIdentityDescription(Identity identity) {
        return String.format("%s <%s>", identity.getName(), identity.getEmail());
    }

    public static class IdentityContainer {
        public final Account account;
        public final Identity identity;

        IdentityContainer(Identity identity2, Account account2) {
            this.identity = identity2;
            this.account = account2;
        }
    }

    static class AccountHolder {
        public View chip;
        public TextView name;

        AccountHolder() {
        }
    }

    static class IdentityHolder {
        public TextView description;
        public TextView name;

        IdentityHolder() {
        }
    }
}
