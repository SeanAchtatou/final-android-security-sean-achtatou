package com.fsck.k9.activity;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;
import atomicgonza.AccountUtils;
import com.fsck.k9.Account;
import com.fsck.k9.AccountStats;
import com.fsck.k9.BaseAccount;
import com.fsck.k9.FontSizes;
import com.fsck.k9.K9;
import com.fsck.k9.Preferences;
import com.fsck.k9.activity.compose.MessageActions;
import com.fsck.k9.activity.setup.AccountSettings;
import com.fsck.k9.activity.setup.FolderSettings;
import com.fsck.k9.activity.setup.Prefs;
import com.fsck.k9.controller.MessagingController;
import com.fsck.k9.controller.MessagingListener;
import com.fsck.k9.helper.SizeFormatter;
import com.fsck.k9.mail.Folder;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mail.power.TracingPowerManager;
import com.fsck.k9.mailstore.LocalFolder;
import com.fsck.k9.search.LocalSearch;
import com.fsck.k9.search.SearchSpecification;
import com.fsck.k9.service.MailService;
import exts.whats.Constants;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import yahoo.mail.app.R;

public class FolderList extends K9ListActivity {
    private static final String EXTRA_ACCOUNT = "account";
    private static final String EXTRA_FROM_SHORTCUT = "fromShortcut";
    private static final boolean REFRESH_REMOTE = true;
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public Account mAccount;
    private ActionBar mActionBar;
    /* access modifiers changed from: private */
    public View mActionBarProgressView;
    /* access modifiers changed from: private */
    public TextView mActionBarSubTitle;
    /* access modifiers changed from: private */
    public TextView mActionBarTitle;
    /* access modifiers changed from: private */
    public TextView mActionBarUnread;
    /* access modifiers changed from: private */
    public FolderListAdapter mAdapter;
    /* access modifiers changed from: private */
    public FontSizes mFontSizes = K9.getFontSizes();
    /* access modifiers changed from: private */
    public FolderListHandler mHandler = new FolderListHandler();
    /* access modifiers changed from: private */
    public LayoutInflater mInflater;
    private ListView mListView;
    /* access modifiers changed from: private */
    public MenuItem mRefreshMenuItem;
    /* access modifiers changed from: private */
    public int mUnreadMessageCount;

    class FolderListHandler extends Handler {
        FolderListHandler() {
        }

        public void refreshTitle() {
            FolderList.this.runOnUiThread(new Runnable() {
                public void run() {
                    FolderList.this.mActionBarTitle.setText(FolderList.this.getString(R.string.folders_title));
                    if (FolderList.this.mUnreadMessageCount == 0) {
                        FolderList.this.mActionBarUnread.setVisibility(8);
                    } else {
                        FolderList.this.mActionBarUnread.setText(String.format("%d", Integer.valueOf(FolderList.this.mUnreadMessageCount)));
                        FolderList.this.mActionBarUnread.setVisibility(0);
                    }
                    String operation = FolderList.this.mAdapter.mListener.getOperation(FolderList.this);
                    if (operation.length() < 1) {
                        FolderList.this.mActionBarSubTitle.setText(FolderList.this.mAccount.getEmail());
                    } else {
                        FolderList.this.mActionBarSubTitle.setText(operation);
                    }
                }
            });
        }

        public void newFolders(final List<FolderInfoHolder> newFolders) {
            FolderList.this.runOnUiThread(new Runnable() {
                public void run() {
                    FolderList.this.mAdapter.mFolders.clear();
                    FolderList.this.mAdapter.mFolders.addAll(newFolders);
                    List unused = FolderList.this.mAdapter.mFilteredFolders = FolderList.this.mAdapter.mFolders;
                    FolderList.this.mHandler.dataChanged();
                }
            });
        }

        public void workingAccount(final int res) {
            FolderList.this.runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(FolderList.this.getApplication(), FolderList.this.getString(res, new Object[]{FolderList.this.mAccount.getDescription()}), 0).show();
                }
            });
        }

        public void accountSizeChanged(long oldSize, long newSize) {
            final long j = oldSize;
            final long j2 = newSize;
            FolderList.this.runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(FolderList.this.getApplication(), FolderList.this.getString(R.string.account_size_changed, new Object[]{FolderList.this.mAccount.getDescription(), SizeFormatter.formatSize(FolderList.this.getApplication(), j), SizeFormatter.formatSize(FolderList.this.getApplication(), j2)}), 1).show();
                }
            });
        }

        public void folderLoading(final String folder, final boolean loading) {
            FolderList.this.runOnUiThread(new Runnable() {
                public void run() {
                    FolderInfoHolder folderHolder = FolderList.this.mAdapter.getFolder(folder);
                    if (folderHolder != null) {
                        folderHolder.loading = loading;
                    }
                }
            });
        }

        public void progress(final boolean progress) {
            if (FolderList.this.mRefreshMenuItem != null) {
                FolderList.this.runOnUiThread(new Runnable() {
                    public void run() {
                        if (progress) {
                            FolderList.this.mRefreshMenuItem.setActionView(FolderList.this.mActionBarProgressView);
                        } else {
                            FolderList.this.mRefreshMenuItem.setActionView((View) null);
                        }
                    }
                });
            }
        }

        public void dataChanged() {
            FolderList.this.runOnUiThread(new Runnable() {
                public void run() {
                    FolderList.this.mAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    private void checkMail(FolderInfoHolder folder) {
        final TracingPowerManager.TracingWakeLock wakeLock = TracingPowerManager.getPowerManager(this).newWakeLock(1, "FolderList checkMail");
        wakeLock.setReferenceCounted(false);
        wakeLock.acquire(600000);
        MessagingController.getInstance(getApplication()).synchronizeMailbox(this.mAccount, folder.name, new MessagingListener() {
            public void synchronizeMailboxFinished(Account account, String folder, int totalMessagesInMailbox, int numNewMessages) {
                if (account.equals(FolderList.this.mAccount)) {
                    wakeLock.release();
                }
            }

            public void synchronizeMailboxFailed(Account account, String folder, String message) {
                if (account.equals(FolderList.this.mAccount)) {
                    wakeLock.release();
                }
            }
        }, null);
        sendMail(this.mAccount);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static Intent actionHandleAccountIntent(Context context2, Account account, boolean fromShortcut) {
        Intent intent = new Intent(context2, FolderList.class);
        intent.setFlags(67108864);
        intent.putExtra("account", account.getUuid());
        if (fromShortcut) {
            intent.putExtra(EXTRA_FROM_SHORTCUT, true);
        }
        return intent;
    }

    public static void actionHandleAccount(Context context2, Account account) {
        context2.startActivity(actionHandleAccountIntent(context2, account, false));
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().addFlags(Integer.MIN_VALUE);
            getWindow().setStatusBarColor(getResources().getColor(R.color.actionbar));
        }
        if (UpgradeDatabases.actionUpgradeDatabases(this, getIntent())) {
            finish();
            return;
        }
        this.mActionBarProgressView = getActionBarProgressView();
        this.mActionBar = getActionBar();
        initializeActionBar();
        setContentView((int) R.layout.folder_list);
        this.mListView = getListView();
        this.mListView.setScrollBarStyle(0);
        this.mListView.setLongClickable(true);
        this.mListView.setFastScrollEnabled(true);
        this.mListView.setScrollingCacheEnabled(false);
        this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                FolderList.this.onOpenFolder(((FolderInfoHolder) FolderList.this.mAdapter.getItem(position)).name);
            }
        });
        registerForContextMenu(this.mListView);
        this.mListView.setSaveEnabled(true);
        this.mInflater = getLayoutInflater();
        this.context = this;
        onNewIntent(getIntent());
        if (!isFinishing()) {
            AccountUtils.guideUserToWidget(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 25) {
            Toast.makeText(this, "msg", 0).show();
        }
    }

    @SuppressLint({"InflateParams"})
    private View getActionBarProgressView() {
        return getLayoutInflater().inflate((int) R.layout.actionbar_indeterminate_progress_actionview, (ViewGroup) null);
    }

    private void initializeActionBar() {
        boolean z;
        setProgressBarIndeterminateVisibility(false);
        this.mActionBar.setDisplayHomeAsUpEnabled(true);
        ActionBar actionBar = this.mActionBar;
        if (Build.VERSION.SDK_INT <= 15) {
            z = true;
        } else {
            z = false;
        }
        actionBar.setDisplayShowHomeEnabled(z);
        this.mActionBar.setDisplayShowTitleEnabled(false);
        this.mActionBar.setDisplayUseLogoEnabled(false);
        this.mActionBar.setDisplayShowCustomEnabled(true);
        this.mActionBar.setCustomView((int) R.layout.actionbar_custom);
        View customView = this.mActionBar.getCustomView();
        this.mActionBarTitle = (TextView) customView.findViewById(R.id.actionbar_title_first);
        this.mActionBarSubTitle = (TextView) customView.findViewById(R.id.actionbar_title_sub);
        this.mActionBarUnread = (TextView) customView.findViewById(R.id.actionbar_unread_count);
    }

    public void onNewIntent(Intent intent) {
        setIntent(intent);
        this.mUnreadMessageCount = 0;
        this.mAccount = Preferences.getPreferences(this).getAccount(intent.getStringExtra("account"));
        if (this.mAccount == null) {
            finish();
        } else if (!intent.getBooleanExtra(EXTRA_FROM_SHORTCUT, false) || K9.FOLDER_NONE.equals(this.mAccount.getAutoExpandFolderName())) {
            initializeActivityView();
        } else {
            onOpenFolder(this.mAccount.getAutoExpandFolderName());
            finish();
        }
    }

    private void initializeActivityView() {
        this.mAdapter = new FolderListAdapter();
        restorePreviousData();
        setListAdapter(this.mAdapter);
        getListView().setTextFilterEnabled(this.mAdapter.getFilter() != null);
    }

    private void restorePreviousData() {
        Object previousData = getLastNonConfigurationInstance();
        if (previousData != null) {
            List unused = this.mAdapter.mFolders = (ArrayList) previousData;
            List unused2 = this.mAdapter.mFilteredFolders = Collections.unmodifiableList(this.mAdapter.mFolders);
        }
    }

    public Object onRetainNonConfigurationInstance() {
        if (this.mAdapter == null) {
            return null;
        }
        return this.mAdapter.mFolders;
    }

    public void onPause() {
        super.onPause();
        MessagingController.getInstance(getApplication()).removeListener(this.mAdapter.mListener);
        this.mAdapter.mListener.onPause(this);
    }

    public void onResume() {
        super.onResume();
        if (!this.mAccount.isAvailable(this)) {
            Log.i("k9", "account unavaliabale, not showing folder-list but account-list");
            Accounts.listAccounts(this);
            finish();
            return;
        }
        if (this.mAdapter == null) {
            initializeActivityView();
        }
        this.mHandler.refreshTitle();
        MessagingController.getInstance(getApplication()).addListener(this.mAdapter.mListener);
        MessagingController.getInstance(getApplication()).getAccountStats(this, this.mAccount, this.mAdapter.mListener);
        onRefresh(false);
        MessagingController.getInstance(getApplication()).cancelNotificationsForAccount(this.mAccount);
        this.mAdapter.mListener.onResume(this);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case 8:
                setDisplayMode(Account.FolderMode.FIRST_CLASS);
                return true;
            case 9:
                setDisplayMode(Account.FolderMode.FIRST_AND_SECOND_CLASS);
                return true;
            case 10:
                setDisplayMode(Account.FolderMode.NOT_SECOND_CLASS);
                return true;
            case 11:
                setDisplayMode(Account.FolderMode.ALL);
                return true;
            case 36:
                Toast.makeText(this, (int) R.string.folder_list_help_key, 1).show();
                return true;
            case 45:
                onAccounts();
                return true;
            case 47:
                onEditAccount();
                return true;
            default:
                return super.onKeyDown(keyCode, event);
        }
    }

    private void setDisplayMode(Account.FolderMode newMode) {
        this.mAccount.setFolderDisplayMode(newMode);
        this.mAccount.save(Preferences.getPreferences(this));
        if (this.mAccount.getFolderPushMode() != Account.FolderMode.NONE) {
            MailService.actionRestartPushers(this, null);
        }
        this.mAdapter.getFilter().filter(null);
        onRefresh(false);
    }

    private void onRefresh(boolean forceRemote) {
        MessagingController.getInstance(getApplication()).listFolders(this.mAccount, forceRemote, this.mAdapter.mListener);
    }

    private void onEditPrefs() {
        Prefs.actionPrefs(this);
    }

    private void onEditAccount() {
        AccountSettings.actionSettings(this, this.mAccount);
    }

    private void onAccounts() {
        Accounts.listAccounts(this);
        finish();
    }

    private void onEmptyTrash(Account account) {
        this.mHandler.dataChanged();
        MessagingController.getInstance(getApplication()).emptyTrash(account, null);
    }

    private void onClearFolder(Account account, String folderName) {
        LocalFolder localFolder = null;
        if (!(account == null || folderName == null)) {
            try {
                if (account.isAvailable(this)) {
                    localFolder = account.getLocalStore().getFolder(folderName);
                    localFolder.open(0);
                    localFolder.clearAllMessages();
                    if (localFolder != null) {
                        localFolder.close();
                    }
                    onRefresh(false);
                    return;
                }
            } catch (Exception e) {
                Log.e("k9", "Exception while clearing folder", e);
                if (localFolder != null) {
                    localFolder.close();
                }
            } catch (Throwable th) {
                if (localFolder != null) {
                    localFolder.close();
                }
                throw th;
            }
        }
        Log.i("k9", "not clear folder of unavailable account");
        if (localFolder != null) {
            localFolder.close();
        }
    }

    private void sendMail(Account account) {
        MessagingController.getInstance(getApplication()).sendPendingMessages(account, this.mAdapter.mListener);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                onAccounts();
                return true;
            case R.id.empty_trash /*2131755438*/:
                onEmptyTrash(this.mAccount);
                return true;
            case R.id.account_settings /*2131755439*/:
                onEditAccount();
                return true;
            case R.id.search /*2131755445*/:
                onSearchRequested();
                return true;
            case R.id.check_mail /*2131755446*/:
                MessagingController.getInstance(getApplication()).checkMail(this, this.mAccount, true, true, this.mAdapter.mListener);
                return true;
            case R.id.compose /*2131755447*/:
                MessageActions.actionCompose(this, this.mAccount);
                return true;
            case R.id.display_all /*2131755457*/:
                setDisplayMode(Account.FolderMode.ALL);
                return true;
            case R.id.display_1st_class /*2131755458*/:
                setDisplayMode(Account.FolderMode.FIRST_CLASS);
                return true;
            case R.id.display_1st_and_2nd_class /*2131755459*/:
                setDisplayMode(Account.FolderMode.FIRST_AND_SECOND_CLASS);
                return true;
            case R.id.display_not_second_class /*2131755460*/:
                setDisplayMode(Account.FolderMode.NOT_SECOND_CLASS);
                return true;
            case R.id.send_messages /*2131755461*/:
                MessagingController.getInstance(getApplication()).sendPendingMessages(this.mAccount, null);
                return true;
            case R.id.compact /*2131755462*/:
                onCompact(this.mAccount);
                return true;
            case R.id.list_folders /*2131755463*/:
                onRefresh(true);
                return true;
            case R.id.app_settings /*2131755465*/:
                onEditPrefs();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean onSearchRequested() {
        Bundle appData = new Bundle();
        appData.putString(MessageList.EXTRA_SEARCH_ACCOUNT, this.mAccount.getUuid());
        startSearch(null, false, appData, false);
        return true;
    }

    /* access modifiers changed from: private */
    public void onOpenFolder(String folder) {
        LocalSearch search = new LocalSearch(folder);
        search.addAccountUuid(this.mAccount.getUuid());
        search.addAllowedFolder(folder);
        MessageList.actionDisplaySearch(this, search, false, false);
    }

    private void onCompact(Account account) {
        this.mHandler.workingAccount(R.string.compacting_account);
        MessagingController.getInstance(getApplication()).compact(account, null);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.folder_list_option, menu);
        this.mRefreshMenuItem = menu.findItem(R.id.check_mail);
        configureFolderSearchView(menu);
        return true;
    }

    private void configureFolderSearchView(Menu menu) {
        final MenuItem folderMenuItem = menu.findItem(R.id.filter_folders);
        SearchView folderSearchView = (SearchView) folderMenuItem.getActionView();
        folderSearchView.setQueryHint(getString(R.string.folder_list_filter_hint));
        folderSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            public boolean onQueryTextSubmit(String query) {
                folderMenuItem.collapseActionView();
                FolderList.this.mActionBarTitle.setText(FolderList.this.getString(R.string.filter_folders_action));
                return true;
            }

            public boolean onQueryTextChange(String newText) {
                FolderList.this.mAdapter.getFilter().filter(newText);
                return true;
            }
        });
        folderSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
            public boolean onClose() {
                FolderList.this.mActionBarTitle.setText(FolderList.this.getString(R.string.folders_title));
                return false;
            }
        });
    }

    public boolean onContextItemSelected(MenuItem item) {
        FolderInfoHolder folder = (FolderInfoHolder) this.mAdapter.getItem(((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).position);
        switch (item.getItemId()) {
            case R.id.refresh_folder /*2131755453*/:
                checkMail(folder);
                break;
            case R.id.clear_local_folder /*2131755454*/:
                onClearFolder(this.mAccount, folder.name);
                break;
            case R.id.folder_settings /*2131755455*/:
                FolderSettings.actionSettings(this, this.mAccount, folder.name);
                break;
        }
        return super.onContextItemSelected(item);
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.folder_context, menu);
        menu.setHeaderTitle(((FolderInfoHolder) this.mAdapter.getItem(((AdapterView.AdapterContextMenuInfo) menuInfo).position)).displayName);
    }

    class FolderListAdapter extends BaseAdapter implements Filterable {
        private Filter mFilter = new FolderListFilter();
        /* access modifiers changed from: private */
        public List<FolderInfoHolder> mFilteredFolders = Collections.unmodifiableList(this.mFolders);
        /* access modifiers changed from: private */
        public List<FolderInfoHolder> mFolders = new ArrayList();
        /* access modifiers changed from: private */
        public ActivityListener mListener = new ActivityListener() {
            public void informUserOfStatus() {
                FolderList.this.mHandler.refreshTitle();
                FolderList.this.mHandler.dataChanged();
            }

            public void accountStatusChanged(BaseAccount account, AccountStats stats) {
                if (account.equals(FolderList.this.mAccount) && stats != null) {
                    int unused = FolderList.this.mUnreadMessageCount = stats.unreadMessageCount;
                    FolderList.this.mHandler.refreshTitle();
                }
            }

            public void listFoldersStarted(Account account) {
                if (account.equals(FolderList.this.mAccount)) {
                    FolderList.this.mHandler.progress(true);
                }
                super.listFoldersStarted(account);
            }

            public void listFoldersFailed(Account account, String message) {
                if (account.equals(FolderList.this.mAccount)) {
                    FolderList.this.mHandler.progress(false);
                }
                super.listFoldersFailed(account, message);
            }

            public void listFoldersFinished(Account account) {
                if (account.equals(FolderList.this.mAccount)) {
                    FolderList.this.mHandler.progress(false);
                    MessagingController.getInstance(FolderList.this.getApplication()).refreshListener(FolderList.this.mAdapter.mListener);
                    FolderList.this.mHandler.dataChanged();
                }
                super.listFoldersFinished(account);
            }

            public void listFolders(Account account, List<LocalFolder> folders) {
                if (account.equals(FolderList.this.mAccount)) {
                    List<FolderInfoHolder> newFolders = new LinkedList<>();
                    List<FolderInfoHolder> topFolders = new LinkedList<>();
                    Account.FolderMode aMode = account.getFolderDisplayMode();
                    for (LocalFolder folder : folders) {
                        Folder.FolderClass fMode = folder.getDisplayClass();
                        if ((aMode != Account.FolderMode.FIRST_CLASS || fMode == Folder.FolderClass.FIRST_CLASS) && ((aMode != Account.FolderMode.FIRST_AND_SECOND_CLASS || fMode == Folder.FolderClass.FIRST_CLASS || fMode == Folder.FolderClass.SECOND_CLASS) && !(aMode == Account.FolderMode.NOT_SECOND_CLASS && fMode == Folder.FolderClass.SECOND_CLASS))) {
                            FolderInfoHolder holder = null;
                            int folderIndex = FolderListAdapter.this.getFolderIndex(folder.getName());
                            if (folderIndex >= 0) {
                                holder = (FolderInfoHolder) FolderListAdapter.this.getItem(folderIndex);
                            }
                            if (holder == null) {
                                holder = new FolderInfoHolder(FolderList.this.context, folder, FolderList.this.mAccount, -1);
                            } else {
                                holder.populate(FolderList.this.context, folder, FolderList.this.mAccount, -1);
                            }
                            if (folder.isInTopGroup()) {
                                topFolders.add(holder);
                            } else {
                                newFolders.add(holder);
                            }
                        }
                    }
                    Collections.sort(newFolders);
                    Collections.sort(topFolders);
                    topFolders.addAll(newFolders);
                    FolderList.this.mHandler.newFolders(topFolders);
                }
                super.listFolders(account, folders);
            }

            public void synchronizeMailboxStarted(Account account, String folder) {
                super.synchronizeMailboxStarted(account, folder);
                if (account.equals(FolderList.this.mAccount)) {
                    FolderList.this.mHandler.progress(true);
                    FolderList.this.mHandler.folderLoading(folder, true);
                    FolderList.this.mHandler.dataChanged();
                }
            }

            public void synchronizeMailboxFinished(Account account, String folder, int totalMessagesInMailbox, int numNewMessages) {
                super.synchronizeMailboxFinished(account, folder, totalMessagesInMailbox, numNewMessages);
                if (account.equals(FolderList.this.mAccount)) {
                    FolderList.this.mHandler.progress(false);
                    FolderList.this.mHandler.folderLoading(folder, false);
                    refreshFolder(account, folder);
                }
            }

            private void refreshFolder(Account account, String folderName) {
                LocalFolder localFolder = null;
                if (!(account == null || folderName == null)) {
                    try {
                        if (!account.isAvailable(FolderList.this)) {
                            Log.i("k9", "not refreshing folder of unavailable account");
                            if (localFolder != null) {
                                localFolder.close();
                                return;
                            }
                            return;
                        }
                        localFolder = account.getLocalStore().getFolder(folderName);
                        FolderInfoHolder folderHolder = FolderListAdapter.this.getFolder(folderName);
                        if (folderHolder != null) {
                            folderHolder.populate(FolderList.this.context, localFolder, FolderList.this.mAccount, -1);
                            folderHolder.flaggedMessageCount = -1;
                            FolderList.this.mHandler.dataChanged();
                        }
                    } catch (Exception e) {
                        Log.e("k9", "Exception while populating folder", e);
                        if (localFolder != null) {
                            localFolder.close();
                            return;
                        }
                        return;
                    } catch (Throwable th) {
                        if (localFolder != null) {
                            localFolder.close();
                        }
                        throw th;
                    }
                }
                if (localFolder != null) {
                    localFolder.close();
                }
            }

            public void synchronizeMailboxFailed(Account account, String folder, String message) {
                super.synchronizeMailboxFailed(account, folder, message);
                if (account.equals(FolderList.this.mAccount)) {
                    FolderList.this.mHandler.progress(false);
                    FolderList.this.mHandler.folderLoading(folder, false);
                    FolderInfoHolder holder = FolderListAdapter.this.getFolder(folder);
                    if (holder != null) {
                        holder.lastChecked = 0;
                    }
                    FolderList.this.mHandler.dataChanged();
                }
            }

            public void setPushActive(Account account, String folderName, boolean enabled) {
                FolderInfoHolder holder;
                if (account.equals(FolderList.this.mAccount) && (holder = FolderListAdapter.this.getFolder(folderName)) != null) {
                    holder.pushActive = enabled;
                    FolderList.this.mHandler.dataChanged();
                }
            }

            public void messageDeleted(Account account, String folder, Message message) {
                synchronizeMailboxRemovedMessage(account, folder, message);
            }

            public void emptyTrashCompleted(Account account) {
                if (account.equals(FolderList.this.mAccount)) {
                    refreshFolder(account, FolderList.this.mAccount.getTrashFolderName());
                }
            }

            public void folderStatusChanged(Account account, String folderName, int unreadMessageCount) {
                if (account.equals(FolderList.this.mAccount)) {
                    refreshFolder(account, folderName);
                    informUserOfStatus();
                }
            }

            public void sendPendingMessagesCompleted(Account account) {
                super.sendPendingMessagesCompleted(account);
                if (account.equals(FolderList.this.mAccount)) {
                    refreshFolder(account, FolderList.this.mAccount.getOutboxFolderName());
                }
            }

            public void sendPendingMessagesStarted(Account account) {
                super.sendPendingMessagesStarted(account);
                if (account.equals(FolderList.this.mAccount)) {
                    FolderList.this.mHandler.dataChanged();
                }
            }

            public void sendPendingMessagesFailed(Account account) {
                super.sendPendingMessagesFailed(account);
                if (account.equals(FolderList.this.mAccount)) {
                    refreshFolder(account, FolderList.this.mAccount.getOutboxFolderName());
                }
            }

            public void accountSizeChanged(Account account, long oldSize, long newSize) {
                if (account.equals(FolderList.this.mAccount)) {
                    FolderList.this.mHandler.accountSizeChanged(oldSize, newSize);
                }
            }
        };

        FolderListAdapter() {
        }

        public Object getItem(long position) {
            return getItem((int) position);
        }

        public Object getItem(int position) {
            return this.mFilteredFolders.get(position);
        }

        public long getItemId(int position) {
            return (long) this.mFilteredFolders.get(position).folder.getName().hashCode();
        }

        public int getCount() {
            return this.mFilteredFolders.size();
        }

        public boolean isEnabled(int item) {
            return true;
        }

        public boolean areAllItemsEnabled() {
            return true;
        }

        public int getFolderIndex(String folder) {
            FolderInfoHolder searchHolder = new FolderInfoHolder();
            searchHolder.name = folder;
            return this.mFilteredFolders.indexOf(searchHolder);
        }

        public FolderInfoHolder getFolder(String folder) {
            FolderInfoHolder holder;
            int index = getFolderIndex(folder);
            if (index < 0 || (holder = (FolderInfoHolder) getItem(index)) == null) {
                return null;
            }
            return holder;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (position <= getCount()) {
                return getItemView(position, convertView, parent);
            }
            Log.e("k9", "getView with illegal positon=" + position + " called! count is only " + getCount());
            return null;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getItemView(int itemPosition, View convertView, ViewGroup parent) {
            View view;
            String folderStatus;
            CharSequence formattedDate;
            FolderInfoHolder folder = (FolderInfoHolder) getItem(itemPosition);
            if (convertView != null) {
                view = convertView;
            } else {
                view = FolderList.this.mInflater.inflate((int) R.layout.folder_list_item, parent, false);
            }
            FolderViewHolder holder = (FolderViewHolder) view.getTag();
            if (holder == null) {
                holder = new FolderViewHolder();
                holder.folderName = (TextView) view.findViewById(R.id.folder_name);
                holder.newMessageCount = (TextView) view.findViewById(R.id.new_message_count);
                holder.flaggedMessageCount = (TextView) view.findViewById(R.id.flagged_message_count);
                holder.newMessageCountWrapper = view.findViewById(R.id.new_message_count_wrapper);
                holder.flaggedMessageCountWrapper = view.findViewById(R.id.flagged_message_count_wrapper);
                holder.newMessageCountIcon = view.findViewById(R.id.new_message_count_icon);
                holder.flaggedMessageCountIcon = view.findViewById(R.id.flagged_message_count_icon);
                holder.folderStatus = (TextView) view.findViewById(R.id.folder_status);
                holder.activeIcons = (RelativeLayout) view.findViewById(R.id.active_icons);
                holder.chip = view.findViewById(R.id.chip);
                holder.folderListItemLayout = (LinearLayout) view.findViewById(R.id.folder_list_item_layout);
                holder.rawFolderName = folder.name;
                view.setTag(holder);
            }
            if (folder != null) {
                if (folder.loading) {
                    folderStatus = FolderList.this.getString(R.string.status_loading);
                } else if (folder.status != null) {
                    folderStatus = folder.status;
                } else if (folder.lastChecked != 0) {
                    long now = System.currentTimeMillis();
                    if (Math.abs(now - folder.lastChecked) > 604800000) {
                        formattedDate = FolderList.this.getString(R.string.preposition_for_date, new Object[]{DateUtils.formatDateTime(FolderList.this.context, folder.lastChecked, 21)});
                    } else {
                        formattedDate = DateUtils.getRelativeTimeSpanString(folder.lastChecked, now, 60000, 21);
                    }
                    folderStatus = FolderList.this.getString(folder.pushActive ? R.string.last_refresh_time_format_with_push : R.string.last_refresh_time_format, new Object[]{formattedDate});
                } else {
                    folderStatus = null;
                }
                holder.folderName.setText(folder.displayName);
                if (folderStatus != null) {
                    holder.folderStatus.setText(folderStatus);
                    holder.folderStatus.setVisibility(0);
                } else {
                    holder.folderStatus.setVisibility(8);
                }
                if (folder.unreadMessageCount == -1) {
                    folder.unreadMessageCount = 0;
                    try {
                        folder.unreadMessageCount = folder.folder.getUnreadMessageCount();
                    } catch (Exception e) {
                        Log.e("k9", "Unable to get unreadMessageCount for " + FolderList.this.mAccount.getDescription() + ":" + folder.name);
                    }
                }
                if (folder.unreadMessageCount > 0) {
                    holder.newMessageCount.setText(String.format("%d", Integer.valueOf(folder.unreadMessageCount)));
                    holder.newMessageCountWrapper.setOnClickListener(createUnreadSearch(FolderList.this.mAccount, folder));
                    holder.newMessageCountWrapper.setVisibility(0);
                    holder.newMessageCountIcon.setBackgroundDrawable(FolderList.this.mAccount.generateColorChip(false, false, false, false, false).drawable());
                } else {
                    holder.newMessageCountWrapper.setVisibility(8);
                }
                if (folder.flaggedMessageCount == -1) {
                    folder.flaggedMessageCount = 0;
                    try {
                        folder.flaggedMessageCount = folder.folder.getFlaggedMessageCount();
                    } catch (Exception e2) {
                        Log.e("k9", "Unable to get flaggedMessageCount for " + FolderList.this.mAccount.getDescription() + ":" + folder.name);
                    }
                }
                if (!K9.messageListStars() || folder.flaggedMessageCount <= 0) {
                    holder.flaggedMessageCountWrapper.setVisibility(8);
                } else {
                    holder.flaggedMessageCount.setText(String.format("%d", Integer.valueOf(folder.flaggedMessageCount)));
                    holder.flaggedMessageCountWrapper.setOnClickListener(createFlaggedSearch(FolderList.this.mAccount, folder));
                    holder.flaggedMessageCountWrapper.setVisibility(0);
                    holder.flaggedMessageCountIcon.setBackgroundDrawable(FolderList.this.mAccount.generateColorChip(false, false, false, false, true).drawable());
                }
                holder.activeIcons.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        Toast.makeText(FolderList.this.getApplication(), FolderList.this.getString(R.string.tap_hint), 0).show();
                    }
                });
                holder.chip.setBackgroundColor(FolderList.this.mAccount.getChipColor());
                FolderList.this.mFontSizes.setViewTextSize(holder.folderName, FolderList.this.mFontSizes.getFolderName());
                if (K9.wrapFolderNames()) {
                    holder.folderName.setEllipsize(null);
                    holder.folderName.setSingleLine(false);
                } else {
                    holder.folderName.setEllipsize(TextUtils.TruncateAt.START);
                    holder.folderName.setSingleLine(true);
                }
                FolderList.this.mFontSizes.setViewTextSize(holder.folderStatus, FolderList.this.mFontSizes.getFolderStatus());
            }
            return view;
        }

        private View.OnClickListener createFlaggedSearch(Account account, FolderInfoHolder folder) {
            LocalSearch search = new LocalSearch(FolderList.this.getString(R.string.search_title, new Object[]{FolderList.this.getString(R.string.message_list_title, new Object[]{account.getDescription(), folder.displayName}), FolderList.this.getString(R.string.flagged_modifier)}));
            search.and(SearchSpecification.SearchField.FLAGGED, Constants.INSTALL_ID, SearchSpecification.Attribute.EQUALS);
            search.addAllowedFolder(folder.name);
            search.addAccountUuid(account.getUuid());
            return new FolderClickListener(search);
        }

        private View.OnClickListener createUnreadSearch(Account account, FolderInfoHolder folder) {
            LocalSearch search = new LocalSearch(FolderList.this.getString(R.string.search_title, new Object[]{FolderList.this.getString(R.string.message_list_title, new Object[]{account.getDescription(), folder.displayName}), FolderList.this.getString(R.string.unread_modifier)}));
            search.and(SearchSpecification.SearchField.READ, Constants.INSTALL_ID, SearchSpecification.Attribute.NOT_EQUALS);
            search.addAllowedFolder(folder.name);
            search.addAccountUuid(account.getUuid());
            return new FolderClickListener(search);
        }

        public boolean hasStableIds() {
            return true;
        }

        public boolean isItemSelectable(int position) {
            return true;
        }

        public void setFilter(Filter filter) {
            this.mFilter = filter;
        }

        public Filter getFilter() {
            return this.mFilter;
        }

        public class FolderListFilter extends Filter {
            private CharSequence mSearchTerm;

            public FolderListFilter() {
            }

            public CharSequence getSearchTerm() {
                return this.mSearchTerm;
            }

            /* access modifiers changed from: protected */
            public Filter.FilterResults performFiltering(CharSequence searchTerm) {
                this.mSearchTerm = searchTerm;
                Filter.FilterResults results = new Filter.FilterResults();
                Locale locale = Locale.getDefault();
                if (searchTerm == null || searchTerm.length() == 0) {
                    List<FolderInfoHolder> list = new ArrayList<>(FolderListAdapter.this.mFolders);
                    results.values = list;
                    results.count = list.size();
                } else {
                    String[] words = searchTerm.toString().toLowerCase(locale).split(" ");
                    int wordCount = words.length;
                    List<FolderInfoHolder> newValues = new ArrayList<>();
                    for (FolderInfoHolder value : FolderListAdapter.this.mFolders) {
                        if (value.displayName != null) {
                            String valueText = value.displayName.toLowerCase(locale);
                            int k = 0;
                            while (true) {
                                if (k >= wordCount) {
                                    break;
                                } else if (valueText.contains(words[k])) {
                                    newValues.add(value);
                                    break;
                                } else {
                                    k++;
                                }
                            }
                        }
                    }
                    results.values = newValues;
                    results.count = newValues.size();
                }
                return results;
            }

            /* access modifiers changed from: protected */
            public void publishResults(CharSequence constraint, Filter.FilterResults results) {
                List unused = FolderListAdapter.this.mFilteredFolders = Collections.unmodifiableList((ArrayList) results.values);
                FolderListAdapter.this.notifyDataSetChanged();
            }
        }
    }

    static class FolderViewHolder {
        public RelativeLayout activeIcons;
        public View chip;
        public TextView flaggedMessageCount;
        public View flaggedMessageCountIcon;
        public View flaggedMessageCountWrapper;
        public LinearLayout folderListItemLayout;
        public TextView folderName;
        public TextView folderStatus;
        public TextView newMessageCount;
        public View newMessageCountIcon;
        public View newMessageCountWrapper;
        public String rawFolderName;

        FolderViewHolder() {
        }
    }

    private class FolderClickListener implements View.OnClickListener {
        final LocalSearch search;

        FolderClickListener(LocalSearch search2) {
            this.search = search2;
        }

        public void onClick(View v) {
            MessageList.actionDisplaySearch(FolderList.this, this.search, true, false);
        }
    }
}
