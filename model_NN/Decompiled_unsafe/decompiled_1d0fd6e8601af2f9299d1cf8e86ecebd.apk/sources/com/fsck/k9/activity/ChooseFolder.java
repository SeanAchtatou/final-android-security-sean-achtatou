package com.fsck.k9.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.SearchView;
import android.widget.TextView;
import com.fsck.k9.Account;
import com.fsck.k9.K9;
import com.fsck.k9.Preferences;
import com.fsck.k9.controller.MessagingController;
import com.fsck.k9.controller.MessagingListener;
import com.fsck.k9.mail.Folder;
import com.fsck.k9.mailstore.LocalFolder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import yahoo.mail.app.R;

public class ChooseFolder extends K9ListActivity {
    public static final String EXTRA_ACCOUNT = "com.fsck.k9.ChooseFolder_account";
    public static final String EXTRA_CUR_FOLDER = "com.fsck.k9.ChooseFolder_curfolder";
    public static final String EXTRA_MESSAGE = "com.fsck.k9.ChooseFolder_message";
    public static final String EXTRA_NEW_FOLDER = "com.fsck.k9.ChooseFolder_newfolder";
    public static final String EXTRA_SEL_FOLDER = "com.fsck.k9.ChooseFolder_selfolder";
    public static final String EXTRA_SHOW_CURRENT = "com.fsck.k9.ChooseFolder_showcurrent";
    public static final String EXTRA_SHOW_DISPLAYABLE_ONLY = "com.fsck.k9.ChooseFolder_showDisplayableOnly";
    public static final String EXTRA_SHOW_FOLDER_NONE = "com.fsck.k9.ChooseFolder_showOptionNone";
    Account mAccount;
    ArrayAdapter<String> mAdapter;
    String mFolder;
    /* access modifiers changed from: private */
    public ChooseFolderHandler mHandler = new ChooseFolderHandler();
    String mHeldInbox = null;
    boolean mHideCurrentFolder = true;
    private MessagingListener mListener = new MessagingListener() {
        public void listFoldersStarted(Account account) {
            if (account.equals(ChooseFolder.this.mAccount)) {
                ChooseFolder.this.mHandler.progress(true);
            }
        }

        public void listFoldersFailed(Account account, String message) {
            if (account.equals(ChooseFolder.this.mAccount)) {
                ChooseFolder.this.mHandler.progress(false);
            }
        }

        public void listFoldersFinished(Account account) {
            if (account.equals(ChooseFolder.this.mAccount)) {
                ChooseFolder.this.mHandler.progress(false);
            }
        }

        /* JADX INFO: finally extract failed */
        public void listFolders(Account account, List<LocalFolder> folders) {
            if (account.equals(ChooseFolder.this.mAccount)) {
                Account.FolderMode aMode = ChooseFolder.this.mMode;
                List<String> newFolders = new ArrayList<>();
                List<String> topFolders = new ArrayList<>();
                for (Folder folder : folders) {
                    String name = folder.getName();
                    if (!ChooseFolder.this.mHideCurrentFolder || (!name.equals(ChooseFolder.this.mFolder) && (!ChooseFolder.this.mAccount.getInboxFolderName().equalsIgnoreCase(ChooseFolder.this.mFolder) || !ChooseFolder.this.mAccount.getInboxFolderName().equalsIgnoreCase(name)))) {
                        Folder.FolderClass fMode = folder.getDisplayClass();
                        if ((aMode != Account.FolderMode.FIRST_CLASS || fMode == Folder.FolderClass.FIRST_CLASS) && ((aMode != Account.FolderMode.FIRST_AND_SECOND_CLASS || fMode == Folder.FolderClass.FIRST_CLASS || fMode == Folder.FolderClass.SECOND_CLASS) && !(aMode == Account.FolderMode.NOT_SECOND_CLASS && fMode == Folder.FolderClass.SECOND_CLASS))) {
                            if (folder.isInTopGroup()) {
                                topFolders.add(name);
                            } else {
                                newFolders.add(name);
                            }
                        }
                    }
                }
                Comparator<String> comparator = new Comparator<String>() {
                    public int compare(String s1, String s2) {
                        int ret = s1.compareToIgnoreCase(s2);
                        return ret != 0 ? ret : s1.compareTo(s2);
                    }
                };
                Collections.sort(topFolders, comparator);
                Collections.sort(newFolders, comparator);
                List<String> localFolders = new ArrayList<>((ChooseFolder.this.mShowOptionNone ? 1 : 0) + topFolders.size() + newFolders.size());
                if (ChooseFolder.this.mShowOptionNone) {
                    localFolders.add(K9.FOLDER_NONE);
                }
                localFolders.addAll(topFolders);
                localFolders.addAll(newFolders);
                int selectedFolder = -1;
                final List<String> folderList = new ArrayList<>();
                int position = 0;
                try {
                    for (String name2 : localFolders) {
                        if (ChooseFolder.this.mAccount.getInboxFolderName().equalsIgnoreCase(name2)) {
                            folderList.add(ChooseFolder.this.getString(R.string.special_mailbox_name_inbox));
                            ChooseFolder.this.mHeldInbox = name2;
                        } else if (!K9.ERROR_FOLDER_NAME.equals(name2) && !account.getOutboxFolderName().equals(name2)) {
                            folderList.add(name2);
                        }
                        if (ChooseFolder.this.mSelectFolder != null) {
                            if (name2.equals(ChooseFolder.this.mSelectFolder)) {
                                selectedFolder = position;
                            }
                        } else if (name2.equals(ChooseFolder.this.mFolder) || (ChooseFolder.this.mAccount.getInboxFolderName().equalsIgnoreCase(ChooseFolder.this.mFolder) && ChooseFolder.this.mAccount.getInboxFolderName().equalsIgnoreCase(name2))) {
                            selectedFolder = position;
                        }
                        position++;
                    }
                    ChooseFolder.this.runOnUiThread(new Runnable() {
                        public void run() {
                            ChooseFolder.this.mAdapter.clear();
                            for (String folderName : folderList) {
                                ChooseFolder.this.mAdapter.add(folderName);
                            }
                            ChooseFolder.this.mAdapter.notifyDataSetChanged();
                            ChooseFolder.this.getListView().setTextFilterEnabled(true);
                        }
                    });
                    if (selectedFolder != -1) {
                        ChooseFolder.this.mHandler.setSelectedFolder(selectedFolder);
                    }
                } catch (Throwable th) {
                    ChooseFolder.this.runOnUiThread(new Runnable() {
                        public void run() {
                            ChooseFolder.this.mAdapter.clear();
                            for (String folderName : folderList) {
                                ChooseFolder.this.mAdapter.add(folderName);
                            }
                            ChooseFolder.this.mAdapter.notifyDataSetChanged();
                            ChooseFolder.this.getListView().setTextFilterEnabled(true);
                        }
                    });
                    throw th;
                }
            }
        }
    };
    MessageReference mMessageReference;
    /* access modifiers changed from: private */
    public Account.FolderMode mMode;
    private FolderListFilter<String> mMyFilter = null;
    String mSelectFolder;
    boolean mShowDisplayableOnly = false;
    boolean mShowOptionNone = false;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(5);
        setContentView((int) R.layout.list_content_simple);
        getListView().setFastScrollEnabled(true);
        getListView().setItemsCanFocus(false);
        getListView().setChoiceMode(0);
        Intent intent = getIntent();
        this.mAccount = Preferences.getPreferences(this).getAccount(intent.getStringExtra(EXTRA_ACCOUNT));
        this.mMessageReference = (MessageReference) intent.getParcelableExtra(EXTRA_MESSAGE);
        this.mFolder = intent.getStringExtra(EXTRA_CUR_FOLDER);
        this.mSelectFolder = intent.getStringExtra(EXTRA_SEL_FOLDER);
        if (intent.getStringExtra(EXTRA_SHOW_CURRENT) != null) {
            this.mHideCurrentFolder = false;
        }
        if (intent.getStringExtra(EXTRA_SHOW_FOLDER_NONE) != null) {
            this.mShowOptionNone = true;
        }
        if (intent.getStringExtra(EXTRA_SHOW_DISPLAYABLE_ONLY) != null) {
            this.mShowDisplayableOnly = true;
        }
        if (this.mFolder == null) {
            this.mFolder = "";
        }
        this.mAdapter = new ArrayAdapter<String>(this, 17367043) {
            private Filter myFilter = null;

            public Filter getFilter() {
                if (this.myFilter == null) {
                    this.myFilter = new FolderListFilter(this);
                }
                return this.myFilter;
            }
        };
        setListAdapter(this.mAdapter);
        this.mMode = this.mAccount.getFolderTargetMode();
        MessagingController.getInstance(getApplication()).listFolders(this.mAccount, false, this.mListener);
        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent result = new Intent();
                result.putExtra(ChooseFolder.EXTRA_ACCOUNT, ChooseFolder.this.mAccount.getUuid());
                result.putExtra(ChooseFolder.EXTRA_CUR_FOLDER, ChooseFolder.this.mFolder);
                String destFolderName = ((TextView) view).getText().toString();
                if (ChooseFolder.this.mHeldInbox != null && ChooseFolder.this.getString(R.string.special_mailbox_name_inbox).equals(destFolderName)) {
                    destFolderName = ChooseFolder.this.mHeldInbox;
                }
                result.putExtra(ChooseFolder.EXTRA_NEW_FOLDER, destFolderName);
                result.putExtra(ChooseFolder.EXTRA_MESSAGE, ChooseFolder.this.mMessageReference);
                ChooseFolder.this.setResult(-1, result);
                ChooseFolder.this.finish();
            }
        });
    }

    class ChooseFolderHandler extends Handler {
        private static final int MSG_PROGRESS = 1;
        private static final int MSG_SET_SELECTED_FOLDER = 2;

        ChooseFolderHandler() {
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    ChooseFolder.this.setProgressBarIndeterminateVisibility(msg.arg1 != 0);
                    return;
                case 2:
                    ChooseFolder.this.getListView().setSelection(msg.arg1);
                    return;
                default:
                    return;
            }
        }

        public void progress(boolean progress) {
            int i = 1;
            Message msg = new Message();
            msg.what = 1;
            if (!progress) {
                i = 0;
            }
            msg.arg1 = i;
            sendMessage(msg);
        }

        public void setSelectedFolder(int position) {
            Message msg = new Message();
            msg.what = 2;
            msg.arg1 = position;
            sendMessage(msg);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.folder_select_option, menu);
        configureFolderSearchView(menu);
        return true;
    }

    private void configureFolderSearchView(Menu menu) {
        final MenuItem folderMenuItem = menu.findItem(R.id.filter_folders);
        SearchView folderSearchView = (SearchView) folderMenuItem.getActionView();
        folderSearchView.setQueryHint(getString(R.string.folder_list_filter_hint));
        folderSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            public boolean onQueryTextSubmit(String query) {
                folderMenuItem.collapseActionView();
                return true;
            }

            public boolean onQueryTextChange(String newText) {
                ChooseFolder.this.mAdapter.getFilter().filter(newText);
                return true;
            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.display_all /*2131755457*/:
                setDisplayMode(Account.FolderMode.ALL);
                return true;
            case R.id.display_1st_class /*2131755458*/:
                setDisplayMode(Account.FolderMode.FIRST_CLASS);
                return true;
            case R.id.display_1st_and_2nd_class /*2131755459*/:
                setDisplayMode(Account.FolderMode.FIRST_AND_SECOND_CLASS);
                return true;
            case R.id.display_not_second_class /*2131755460*/:
                setDisplayMode(Account.FolderMode.NOT_SECOND_CLASS);
                return true;
            case R.id.send_messages /*2131755461*/:
            case R.id.compact /*2131755462*/:
            default:
                return super.onOptionsItemSelected(item);
            case R.id.list_folders /*2131755463*/:
                onRefresh();
                return true;
        }
    }

    private void onRefresh() {
        MessagingController.getInstance(getApplication()).listFolders(this.mAccount, true, this.mListener);
    }

    private void setDisplayMode(Account.FolderMode aMode) {
        this.mMode = aMode;
        if (this.mMyFilter != null) {
            this.mMyFilter.invalidate();
        }
        MessagingController.getInstance(getApplication()).listFolders(this.mAccount, false, this.mListener);
    }
}
