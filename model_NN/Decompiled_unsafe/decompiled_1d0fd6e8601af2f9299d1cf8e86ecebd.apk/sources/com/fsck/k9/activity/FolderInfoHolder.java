package com.fsck.k9.activity;

import android.content.Context;
import com.fsck.k9.Account;
import com.fsck.k9.mail.Folder;
import com.fsck.k9.mailstore.LocalFolder;
import yahoo.mail.app.R;

public class FolderInfoHolder implements Comparable<FolderInfoHolder> {
    public String displayName;
    public int flaggedMessageCount = -1;
    public Folder folder;
    public boolean lastCheckFailed;
    public long lastChecked;
    public boolean loading;
    public boolean moreMessages;
    public String name;
    public boolean pushActive;
    public String status;
    public int unreadMessageCount = -1;

    public boolean equals(Object o) {
        return (o instanceof FolderInfoHolder) && this.name.equals(((FolderInfoHolder) o).name);
    }

    public int hashCode() {
        return this.name.hashCode();
    }

    public int compareTo(FolderInfoHolder o) {
        String s1 = this.name;
        String s2 = o.name;
        int ret = s1.compareToIgnoreCase(s2);
        return ret != 0 ? ret : s1.compareTo(s2);
    }

    private String truncateStatus(String mess) {
        if (mess == null || mess.length() <= 27) {
            return mess;
        }
        return mess.substring(0, 27);
    }

    public FolderInfoHolder() {
    }

    public FolderInfoHolder(Context context, LocalFolder folder2, Account account) {
        if (context == null) {
            throw new IllegalArgumentException("null context given");
        }
        populate(context, folder2, account);
    }

    public FolderInfoHolder(Context context, LocalFolder folder2, Account account, int unreadCount) {
        populate(context, folder2, account, unreadCount);
    }

    public void populate(Context context, LocalFolder folder2, Account account, int unreadCount) {
        populate(context, folder2, account);
        this.unreadMessageCount = unreadCount;
        folder2.close();
    }

    public void populate(Context context, LocalFolder folder2, Account account) {
        this.folder = folder2;
        this.name = folder2.getName();
        this.lastChecked = folder2.getLastUpdate();
        this.status = truncateStatus(folder2.getStatus());
        this.displayName = getDisplayName(context, account, this.name);
        setMoreMessagesFromFolder(folder2);
    }

    public static String getDisplayName(Context context, Account account, String name2) {
        if (name2.equals(account.getSpamFolderName())) {
            return String.format(context.getString(R.string.special_mailbox_name_spam_fmt), name2);
        } else if (name2.equals(account.getArchiveFolderName())) {
            return String.format(context.getString(R.string.special_mailbox_name_archive_fmt), name2);
        } else if (name2.equals(account.getSentFolderName())) {
            return String.format(context.getString(R.string.special_mailbox_name_sent_fmt), name2);
        } else if (name2.equals(account.getTrashFolderName())) {
            return String.format(context.getString(R.string.special_mailbox_name_trash_fmt), name2);
        } else if (name2.equals(account.getDraftsFolderName())) {
            return String.format(context.getString(R.string.special_mailbox_name_drafts_fmt), name2);
        } else if (name2.equals(account.getOutboxFolderName())) {
            return context.getString(R.string.special_mailbox_name_outbox);
        } else {
            if (name2.equalsIgnoreCase(account.getInboxFolderName())) {
                return context.getString(R.string.special_mailbox_name_inbox);
            }
            return name2;
        }
    }

    public void setMoreMessagesFromFolder(LocalFolder folder2) {
        this.moreMessages = folder2.hasMoreMessages();
    }
}
