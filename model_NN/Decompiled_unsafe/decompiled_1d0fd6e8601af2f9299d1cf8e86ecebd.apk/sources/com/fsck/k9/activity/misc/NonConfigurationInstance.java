package com.fsck.k9.activity.misc;

import android.app.Activity;

public interface NonConfigurationInstance {
    void restore(Activity activity);

    boolean retain();
}
