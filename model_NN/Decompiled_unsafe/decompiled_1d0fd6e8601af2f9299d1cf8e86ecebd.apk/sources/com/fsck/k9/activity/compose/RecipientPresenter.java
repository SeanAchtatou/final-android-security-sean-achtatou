package com.fsck.k9.activity.compose;

import android.app.LoaderManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import com.fsck.k9.Account;
import com.fsck.k9.Identity;
import com.fsck.k9.activity.compose.ComposeCryptoStatus;
import com.fsck.k9.helper.Contacts;
import com.fsck.k9.helper.MailTo;
import com.fsck.k9.helper.ReplyToParser;
import com.fsck.k9.mail.Address;
import com.fsck.k9.mail.Flag;
import com.fsck.k9.mail.Message;
import com.fsck.k9.message.ComposePgpInlineDecider;
import com.fsck.k9.message.PgpMessageBuilder;
import com.fsck.k9.view.RecipientSelectView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.openintents.openpgp.IOpenPgpService2;
import org.openintents.openpgp.util.OpenPgpApi;
import org.openintents.openpgp.util.OpenPgpServiceConnection;
import yahoo.mail.app.R;

public class RecipientPresenter implements OpenPgpApi.PermissionPingCallback {
    private static final int CONTACT_PICKER_BCC = 3;
    private static final int CONTACT_PICKER_CC = 2;
    private static final int CONTACT_PICKER_TO = 1;
    private static final int OPENPGP_USER_INTERACTION = 4;
    private static final String STATE_KEY_BCC_SHOWN = "state:bccShown";
    private static final String STATE_KEY_CC_SHOWN = "state:ccShown";
    private static final String STATE_KEY_CRYPTO_ENABLE_PGP_INLINE = "state:cryptoEnablePgpInline";
    private static final String STATE_KEY_CURRENT_CRYPTO_MODE = "state:currentCryptoMode";
    private static final String STATE_KEY_LAST_FOCUSED_TYPE = "state:lastFocusedType";
    private Account account;
    private ComposeCryptoStatus cachedCryptoStatus;
    private final ComposePgpInlineDecider composePgpInlineDecider;
    private final Context context;
    private boolean cryptoEnablePgpInline = false;
    private String cryptoProvider;
    private CryptoProviderState cryptoProviderState = CryptoProviderState.UNCONFIGURED;
    private CryptoMode currentCryptoMode = CryptoMode.OPPORTUNISTIC;
    private Boolean hasContactPicker;
    private Message.RecipientType lastFocusedType = Message.RecipientType.TO;
    private OpenPgpServiceConnection openPgpServiceConnection;
    private PendingIntent pendingUserInteractionIntent;
    /* access modifiers changed from: private */
    public final RecipientMvpView recipientMvpView;
    private ReplyToParser replyToParser;

    public enum CryptoMode {
        DISABLE,
        SIGN_ONLY,
        OPPORTUNISTIC,
        PRIVATE
    }

    public enum CryptoProviderState {
        UNCONFIGURED,
        UNINITIALIZED,
        LOST_CONNECTION,
        ERROR,
        OK
    }

    public RecipientPresenter(Context context2, LoaderManager loaderManager, RecipientMvpView recipientMvpView2, Account account2, ComposePgpInlineDecider composePgpInlineDecider2, ReplyToParser replyToParser2) {
        this.recipientMvpView = recipientMvpView2;
        this.context = context2;
        this.composePgpInlineDecider = composePgpInlineDecider2;
        this.replyToParser = replyToParser2;
        recipientMvpView2.setPresenter(this);
        recipientMvpView2.setLoaderManager(loaderManager);
        onSwitchAccount(account2);
        updateCryptoStatus();
    }

    public List<Address> getToAddresses() {
        return this.recipientMvpView.getToAddresses();
    }

    public List<Address> getCcAddresses() {
        return this.recipientMvpView.getCcAddresses();
    }

    public List<Address> getBccAddresses() {
        return this.recipientMvpView.getBccAddresses();
    }

    public List<RecipientSelectView.Recipient> getAllRecipients() {
        ArrayList<RecipientSelectView.Recipient> result = new ArrayList<>();
        result.addAll(this.recipientMvpView.getToRecipients());
        result.addAll(this.recipientMvpView.getCcRecipients());
        result.addAll(this.recipientMvpView.getBccRecipients());
        return result;
    }

    public boolean checkRecipientsOkForSending() {
        boolean performedAnyCompletion;
        if (this.recipientMvpView.recipientToTryPerformCompletion() || this.recipientMvpView.recipientCcTryPerformCompletion() || this.recipientMvpView.recipientBccTryPerformCompletion()) {
            performedAnyCompletion = true;
        } else {
            performedAnyCompletion = false;
        }
        if (performedAnyCompletion) {
            return true;
        }
        if (this.recipientMvpView.recipientToHasUncompletedText()) {
            this.recipientMvpView.showToUncompletedError();
            return true;
        } else if (this.recipientMvpView.recipientCcHasUncompletedText()) {
            this.recipientMvpView.showCcUncompletedError();
            return true;
        } else if (this.recipientMvpView.recipientBccHasUncompletedText()) {
            this.recipientMvpView.showBccUncompletedError();
            return true;
        } else if (!getToAddresses().isEmpty() || !getCcAddresses().isEmpty() || !getBccAddresses().isEmpty()) {
            return false;
        } else {
            this.recipientMvpView.showNoRecipientsError();
            return true;
        }
    }

    public void initFromReplyToMessage(Message message, boolean isReplyAll) {
        ReplyToParser.ReplyToAddresses replyToAddresses;
        if (isReplyAll) {
            replyToAddresses = this.replyToParser.getRecipientsToReplyAllTo(message, this.account);
        } else {
            replyToAddresses = this.replyToParser.getRecipientsToReplyTo(message, this.account);
        }
        addToAddresses(replyToAddresses.to);
        addCcAddresses(replyToAddresses.cc);
        if (this.composePgpInlineDecider.shouldReplyInline(message)) {
            this.cryptoEnablePgpInline = true;
        }
    }

    public void initFromMailto(MailTo mailTo) {
        addToAddresses(mailTo.getTo());
        addCcAddresses(mailTo.getCc());
        addBccAddresses(mailTo.getBcc());
    }

    public void initFromSendOrViewIntent(Intent intent) {
        String[] extraEmail = intent.getStringArrayExtra("android.intent.extra.EMAIL");
        String[] extraCc = intent.getStringArrayExtra("android.intent.extra.CC");
        String[] extraBcc = intent.getStringArrayExtra("android.intent.extra.BCC");
        if (extraEmail != null) {
            addToAddresses(addressFromStringArray(extraEmail));
        }
        if (extraCc != null) {
            addCcAddresses(addressFromStringArray(extraCc));
        }
        if (extraBcc != null) {
            addBccAddresses(addressFromStringArray(extraBcc));
        }
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        this.recipientMvpView.setCcVisibility(savedInstanceState.getBoolean(STATE_KEY_CC_SHOWN));
        this.recipientMvpView.setBccVisibility(savedInstanceState.getBoolean(STATE_KEY_BCC_SHOWN));
        this.lastFocusedType = Message.RecipientType.valueOf(savedInstanceState.getString(STATE_KEY_LAST_FOCUSED_TYPE));
        this.currentCryptoMode = CryptoMode.valueOf(savedInstanceState.getString(STATE_KEY_CURRENT_CRYPTO_MODE));
        this.cryptoEnablePgpInline = savedInstanceState.getBoolean(STATE_KEY_CRYPTO_ENABLE_PGP_INLINE);
        updateRecipientExpanderVisibility();
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(STATE_KEY_CC_SHOWN, this.recipientMvpView.isCcVisible());
        outState.putBoolean(STATE_KEY_BCC_SHOWN, this.recipientMvpView.isBccVisible());
        outState.putString(STATE_KEY_LAST_FOCUSED_TYPE, this.lastFocusedType.toString());
        outState.putString(STATE_KEY_CURRENT_CRYPTO_MODE, this.currentCryptoMode.toString());
        outState.putBoolean(STATE_KEY_CRYPTO_ENABLE_PGP_INLINE, this.cryptoEnablePgpInline);
    }

    public void initFromDraftMessage(Message message) {
        initRecipientsFromDraftMessage(message);
        initPgpInlineFromDraftMessage(message);
    }

    private void initRecipientsFromDraftMessage(Message message) {
        addToAddresses(message.getRecipients(Message.RecipientType.TO));
        addCcAddresses(message.getRecipients(Message.RecipientType.CC));
        addBccAddresses(message.getRecipients(Message.RecipientType.BCC));
    }

    private void initPgpInlineFromDraftMessage(Message message) {
        this.cryptoEnablePgpInline = message.isSet(Flag.X_DRAFT_OPENPGP_INLINE);
    }

    /* access modifiers changed from: package-private */
    public void addToAddresses(Address... toAddresses) {
        addRecipientsFromAddresses(Message.RecipientType.TO, toAddresses);
    }

    /* access modifiers changed from: package-private */
    public void addCcAddresses(Address... ccAddresses) {
        if (ccAddresses.length > 0) {
            addRecipientsFromAddresses(Message.RecipientType.CC, ccAddresses);
            this.recipientMvpView.setCcVisibility(true);
            updateRecipientExpanderVisibility();
        }
    }

    public void addBccAddresses(Address... bccRecipients) {
        boolean singleBccRecipientFromAccount;
        boolean z = false;
        if (bccRecipients.length > 0) {
            addRecipientsFromAddresses(Message.RecipientType.BCC, bccRecipients);
            String bccAddress = this.account.getAlwaysBcc();
            boolean alreadyVisible = this.recipientMvpView.isBccVisible();
            if (bccRecipients.length != 1 || !bccRecipients[0].toString().equals(bccAddress)) {
                singleBccRecipientFromAccount = false;
            } else {
                singleBccRecipientFromAccount = true;
            }
            RecipientMvpView recipientMvpView2 = this.recipientMvpView;
            if (alreadyVisible || singleBccRecipientFromAccount) {
                z = true;
            }
            recipientMvpView2.setBccVisibility(z);
            updateRecipientExpanderVisibility();
        }
    }

    public void onPrepareOptionsMenu(Menu menu) {
        boolean isCryptoConfigured;
        boolean z;
        boolean z2;
        boolean noContactPickerAvailable;
        if (this.cryptoProviderState != CryptoProviderState.UNCONFIGURED) {
            isCryptoConfigured = true;
        } else {
            isCryptoConfigured = false;
        }
        MenuItem findItem = menu.findItem(R.id.openpgp_inline_enable);
        if (!isCryptoConfigured || this.cryptoEnablePgpInline) {
            z = false;
        } else {
            z = true;
        }
        findItem.setVisible(z);
        MenuItem findItem2 = menu.findItem(R.id.openpgp_inline_disable);
        if (!isCryptoConfigured || !this.cryptoEnablePgpInline) {
            z2 = false;
        } else {
            z2 = true;
        }
        findItem2.setVisible(z2);
        if (!hasContactPicker()) {
            noContactPickerAvailable = true;
        } else {
            noContactPickerAvailable = false;
        }
        if (noContactPickerAvailable) {
            menu.findItem(R.id.add_from_contacts).setVisible(false);
        }
    }

    public void onSwitchAccount(Account account2) {
        this.account = account2;
        if (account2.isAlwaysShowCcBcc()) {
            this.recipientMvpView.setCcVisibility(true);
            this.recipientMvpView.setBccVisibility(true);
            updateRecipientExpanderVisibility();
        }
        setCryptoProvider(account2.getOpenPgpProvider());
    }

    public void onSwitchIdentity(Identity identity) {
    }

    private static Address[] addressFromStringArray(String[] addresses) {
        return addressFromStringArray(Arrays.asList(addresses));
    }

    private static Address[] addressFromStringArray(List<String> addresses) {
        ArrayList<Address> result = new ArrayList<>(addresses.size());
        for (String addressStr : addresses) {
            Collections.addAll(result, Address.parseUnencoded(addressStr));
        }
        return (Address[]) result.toArray(new Address[result.size()]);
    }

    public void onClickToLabel() {
        this.recipientMvpView.requestFocusOnToField();
    }

    public void onClickCcLabel() {
        this.recipientMvpView.requestFocusOnCcField();
    }

    public void onClickBccLabel() {
        this.recipientMvpView.requestFocusOnBccField();
    }

    public void onClickRecipientExpander() {
        this.recipientMvpView.setCcVisibility(true);
        this.recipientMvpView.setBccVisibility(true);
        updateRecipientExpanderVisibility();
    }

    private void hideEmptyExtendedRecipientFields() {
        if (this.recipientMvpView.getCcAddresses().isEmpty()) {
            this.recipientMvpView.setCcVisibility(false);
            if (this.lastFocusedType == Message.RecipientType.CC) {
                this.lastFocusedType = Message.RecipientType.TO;
            }
        }
        if (this.recipientMvpView.getBccAddresses().isEmpty()) {
            this.recipientMvpView.setBccVisibility(false);
            if (this.lastFocusedType == Message.RecipientType.BCC) {
                this.lastFocusedType = Message.RecipientType.TO;
            }
        }
        updateRecipientExpanderVisibility();
    }

    private void updateRecipientExpanderVisibility() {
        this.recipientMvpView.setRecipientExpanderVisibility(!this.recipientMvpView.isCcVisible() || !this.recipientMvpView.isBccVisible());
    }

    public void updateCryptoStatus() {
        this.cachedCryptoStatus = null;
        if (this.cryptoProviderState == CryptoProviderState.OK && (this.openPgpServiceConnection == null || !this.openPgpServiceConnection.isBound())) {
            this.cryptoProviderState = CryptoProviderState.LOST_CONNECTION;
            this.pendingUserInteractionIntent = null;
        }
        this.recipientMvpView.showCryptoStatus(getCurrentCryptoStatus().getCryptoStatusDisplayType());
        this.recipientMvpView.showPgpInlineModeIndicator(getCurrentCryptoStatus().isPgpInlineModeEnabled());
    }

    public ComposeCryptoStatus getCurrentCryptoStatus() {
        if (this.cachedCryptoStatus == null) {
            ComposeCryptoStatus.ComposeCryptoStatusBuilder builder = new ComposeCryptoStatus.ComposeCryptoStatusBuilder().setCryptoProviderState(this.cryptoProviderState).setCryptoMode(this.currentCryptoMode).setEnablePgpInline(this.cryptoEnablePgpInline).setRecipients(getAllRecipients());
            long accountCryptoKey = this.account.getCryptoKey();
            if (accountCryptoKey != 0) {
                builder.setSigningKeyId(accountCryptoKey);
                builder.setSelfEncryptId(accountCryptoKey);
            }
            this.cachedCryptoStatus = builder.build();
        }
        return this.cachedCryptoStatus;
    }

    public boolean isForceTextMessageFormat() {
        ComposeCryptoStatus cryptoStatus = getCurrentCryptoStatus();
        return cryptoStatus.isEncryptionEnabled() || cryptoStatus.isSigningEnabled();
    }

    public boolean isAllowSavingDraftRemotely() {
        ComposeCryptoStatus cryptoStatus = getCurrentCryptoStatus();
        return cryptoStatus.isEncryptionEnabled() || cryptoStatus.isSigningEnabled();
    }

    public void onToTokenAdded(RecipientSelectView.Recipient recipient) {
        updateCryptoStatus();
    }

    public void onToTokenRemoved(RecipientSelectView.Recipient recipient) {
        updateCryptoStatus();
    }

    public void onToTokenChanged(RecipientSelectView.Recipient recipient) {
        updateCryptoStatus();
    }

    public void onCcTokenAdded(RecipientSelectView.Recipient recipient) {
        updateCryptoStatus();
    }

    public void onCcTokenRemoved(RecipientSelectView.Recipient recipient) {
        updateCryptoStatus();
    }

    public void onCcTokenChanged(RecipientSelectView.Recipient recipient) {
        updateCryptoStatus();
    }

    public void onBccTokenAdded(RecipientSelectView.Recipient recipient) {
        updateCryptoStatus();
    }

    public void onBccTokenRemoved(RecipientSelectView.Recipient recipient) {
        updateCryptoStatus();
    }

    public void onBccTokenChanged(RecipientSelectView.Recipient recipient) {
        updateCryptoStatus();
    }

    public void onCryptoModeChanged(CryptoMode cryptoMode) {
        this.currentCryptoMode = cryptoMode;
        updateCryptoStatus();
    }

    public void onCryptoPgpInlineChanged(boolean enablePgpInline) {
        this.cryptoEnablePgpInline = enablePgpInline;
        updateCryptoStatus();
    }

    private void addRecipientsFromAddresses(Message.RecipientType recipientType, Address... addresses) {
        final Message.RecipientType recipientType2 = recipientType;
        new RecipientLoader(this.context, this.cryptoProvider, addresses) {
            public /* bridge */ /* synthetic */ void deliverResult(Object obj) {
                deliverResult((List<RecipientSelectView.Recipient>) ((List) obj));
            }

            public void deliverResult(List<RecipientSelectView.Recipient> result) {
                RecipientPresenter.this.recipientMvpView.addRecipients(recipientType2, (RecipientSelectView.Recipient[]) result.toArray(new RecipientSelectView.Recipient[result.size()]));
                stopLoading();
                abandon();
            }
        }.startLoading();
    }

    private void addRecipientFromContactUri(Message.RecipientType recipientType, Uri uri) {
        final Message.RecipientType recipientType2 = recipientType;
        new RecipientLoader(this.context, this.cryptoProvider, uri, false) {
            public /* bridge */ /* synthetic */ void deliverResult(Object obj) {
                deliverResult((List<RecipientSelectView.Recipient>) ((List) obj));
            }

            public void deliverResult(List<RecipientSelectView.Recipient> result) {
                if (result.isEmpty()) {
                    RecipientPresenter.this.recipientMvpView.showErrorContactNoAddress();
                    return;
                }
                RecipientPresenter.this.recipientMvpView.addRecipients(recipientType2, result.get(0));
                stopLoading();
                abandon();
            }
        }.startLoading();
    }

    public void onToFocused() {
        this.lastFocusedType = Message.RecipientType.TO;
    }

    public void onCcFocused() {
        this.lastFocusedType = Message.RecipientType.CC;
    }

    public void onBccFocused() {
        this.lastFocusedType = Message.RecipientType.BCC;
    }

    public void onMenuAddFromContacts() {
        this.recipientMvpView.showContactPicker(recipientTypeToRequestCode(this.lastFocusedType));
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 1:
            case 2:
            case 3:
                if (resultCode == -1 && data != null) {
                    addRecipientFromContactUri(recipientTypeFromRequestCode(requestCode), data.getData());
                    return;
                }
                return;
            case 4:
                cryptoProviderBindOrCheckPermission();
                return;
            default:
                return;
        }
    }

    private static int recipientTypeToRequestCode(Message.RecipientType type) {
        switch (type) {
            case CC:
                return 2;
            case BCC:
                return 3;
            default:
                return 1;
        }
    }

    private static Message.RecipientType recipientTypeFromRequestCode(int type) {
        switch (type) {
            case 2:
                return Message.RecipientType.CC;
            case 3:
                return Message.RecipientType.BCC;
            default:
                return Message.RecipientType.TO;
        }
    }

    public void onNonRecipientFieldFocused() {
        hideEmptyExtendedRecipientFields();
    }

    public void onClickCryptoStatus() {
        switch (this.cryptoProviderState) {
            case UNCONFIGURED:
                Log.e("k9", "click on crypto status while unconfigured - this should not really happen?!");
                return;
            case OK:
                this.recipientMvpView.showCryptoDialog(this.currentCryptoMode);
                return;
            case LOST_CONNECTION:
            case UNINITIALIZED:
            case ERROR:
                cryptoProviderBindOrCheckPermission();
                return;
            default:
                return;
        }
    }

    public boolean hasContactPicker() {
        boolean z = false;
        if (this.hasContactPicker == null) {
            if (!this.context.getPackageManager().queryIntentActivities(Contacts.getInstance(this.context).contactPickerIntent(), 0).isEmpty()) {
                z = true;
            }
            this.hasContactPicker = Boolean.valueOf(z);
        }
        return this.hasContactPicker.booleanValue();
    }

    public void showPgpSendError(ComposeCryptoStatus.SendErrorState sendErrorState) {
        switch (sendErrorState) {
            case PROVIDER_ERROR:
                this.recipientMvpView.showErrorOpenPgpConnection();
                return;
            case SIGN_KEY_NOT_CONFIGURED:
                this.recipientMvpView.showErrorMissingSignKey();
                return;
            case PRIVATE_BUT_MISSING_KEYS:
                this.recipientMvpView.showErrorPrivateButMissingKeys();
                return;
            default:
                throw new AssertionError("not all error states handled, this is a bug!");
        }
    }

    public void showPgpAttachError(ComposeCryptoStatus.AttachErrorState attachErrorState) {
        switch (attachErrorState) {
            case IS_INLINE:
                break;
            default:
                throw new AssertionError("not all error states handled, this is a bug!");
        }
        this.recipientMvpView.showErrorAttachInline();
    }

    private void setCryptoProvider(String cryptoProvider2) {
        boolean providerIsBound;
        boolean isSameProvider = true;
        if (this.openPgpServiceConnection == null || !this.openPgpServiceConnection.isBound()) {
            providerIsBound = false;
        } else {
            providerIsBound = true;
        }
        if (cryptoProvider2 == null || !cryptoProvider2.equals(this.cryptoProvider)) {
            isSameProvider = false;
        }
        if (!isSameProvider || !providerIsBound) {
            if (providerIsBound) {
                this.openPgpServiceConnection.unbindFromService();
                this.openPgpServiceConnection = null;
            }
            this.cryptoProvider = cryptoProvider2;
            if (cryptoProvider2 == null) {
                this.cryptoProviderState = CryptoProviderState.UNCONFIGURED;
                return;
            }
            this.cryptoProviderState = CryptoProviderState.UNINITIALIZED;
            this.openPgpServiceConnection = new OpenPgpServiceConnection(this.context, cryptoProvider2, new OpenPgpServiceConnection.OnBound() {
                public void onBound(IOpenPgpService2 service) {
                    RecipientPresenter.this.cryptoProviderBindOrCheckPermission();
                }

                public void onError(Exception e) {
                    RecipientPresenter.this.onCryptoProviderError(e);
                }
            });
            cryptoProviderBindOrCheckPermission();
            this.recipientMvpView.setCryptoProvider(cryptoProvider2);
            return;
        }
        cryptoProviderBindOrCheckPermission();
    }

    /* access modifiers changed from: private */
    public void cryptoProviderBindOrCheckPermission() {
        if (this.openPgpServiceConnection == null) {
            this.cryptoProviderState = CryptoProviderState.UNCONFIGURED;
        } else if (!this.openPgpServiceConnection.isBound()) {
            this.pendingUserInteractionIntent = null;
            this.openPgpServiceConnection.bindToService();
        } else if (this.pendingUserInteractionIntent != null) {
            this.recipientMvpView.launchUserInteractionPendingIntent(this.pendingUserInteractionIntent, 4);
            this.pendingUserInteractionIntent = null;
        } else {
            getOpenPgpApi().checkPermissionPing(this);
        }
    }

    /* access modifiers changed from: private */
    public void onCryptoProviderError(Exception e) {
        this.recipientMvpView.showErrorOpenPgpConnection();
        this.cryptoProviderState = CryptoProviderState.ERROR;
        Log.e("k9", "error connecting to crypto provider!", e);
        updateCryptoStatus();
    }

    public void onPgpPermissionCheckResult(Intent result) {
        switch (result.getIntExtra(OpenPgpApi.RESULT_CODE, 0)) {
            case 1:
                this.cryptoProviderState = CryptoProviderState.OK;
                break;
            case 2:
                this.recipientMvpView.showErrorOpenPgpUserInteractionRequired();
                this.pendingUserInteractionIntent = (PendingIntent) result.getParcelableExtra(OpenPgpApi.RESULT_INTENT);
                this.cryptoProviderState = CryptoProviderState.ERROR;
                break;
            default:
                this.recipientMvpView.showErrorOpenPgpConnection();
                this.cryptoProviderState = CryptoProviderState.ERROR;
                break;
        }
        updateCryptoStatus();
    }

    public void onActivityDestroy() {
        if (this.openPgpServiceConnection != null && this.openPgpServiceConnection.isBound()) {
            this.openPgpServiceConnection.unbindFromService();
        }
        this.openPgpServiceConnection = null;
    }

    public OpenPgpApi getOpenPgpApi() {
        if (this.openPgpServiceConnection == null || !this.openPgpServiceConnection.isBound()) {
            Log.e("k9", "obtained openpgpapi object, but service is not bound! inconsistent state?");
        }
        return new OpenPgpApi(this.context, this.openPgpServiceConnection.getService());
    }

    public void builderSetProperties(PgpMessageBuilder pgpBuilder) {
        pgpBuilder.setOpenPgpApi(getOpenPgpApi());
        pgpBuilder.setCryptoStatus(getCurrentCryptoStatus());
    }

    public void onMenuSetPgpInline(boolean enablePgpInline) {
        this.cryptoEnablePgpInline = enablePgpInline;
        updateCryptoStatus();
        if (enablePgpInline) {
            this.recipientMvpView.showOpenPgpInlineDialog(true);
        }
    }

    public void onClickPgpInlineIndicator() {
        this.recipientMvpView.showOpenPgpInlineDialog(false);
    }
}
