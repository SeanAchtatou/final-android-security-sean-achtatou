package com.fsck.k9.activity.setup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.util.Log;
import com.fsck.k9.Account;
import com.fsck.k9.Preferences;
import com.fsck.k9.activity.FolderInfoHolder;
import com.fsck.k9.activity.K9PreferenceActivity;
import com.fsck.k9.mail.Folder;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mailstore.LocalFolder;
import com.fsck.k9.service.MailService;
import yahoo.mail.app.R;

public class FolderSettings extends K9PreferenceActivity {
    private static final String EXTRA_ACCOUNT = "com.fsck.k9.account";
    private static final String EXTRA_FOLDER_NAME = "com.fsck.k9.folderName";
    private static final String PREFERENCE_DISPLAY_CLASS = "folder_settings_folder_display_mode";
    private static final String PREFERENCE_INTEGRATE = "folder_settings_include_in_integrated_inbox";
    private static final String PREFERENCE_IN_TOP_GROUP = "folder_settings_in_top_group";
    private static final String PREFERENCE_NOTIFY_CLASS = "folder_settings_folder_notify_mode";
    private static final String PREFERENCE_PUSH_CLASS = "folder_settings_folder_push_mode";
    private static final String PREFERENCE_SYNC_CLASS = "folder_settings_folder_sync_mode";
    private static final String PREFERENCE_TOP_CATERGORY = "folder_settings";
    /* access modifiers changed from: private */
    public ListPreference mDisplayClass;
    private LocalFolder mFolder;
    private CheckBoxPreference mInTopGroup;
    private CheckBoxPreference mIntegrate;
    /* access modifiers changed from: private */
    public ListPreference mNotifyClass;
    /* access modifiers changed from: private */
    public ListPreference mPushClass;
    /* access modifiers changed from: private */
    public ListPreference mSyncClass;

    public static void actionSettings(Context context, Account account, String folderName) {
        Intent i = new Intent(context, FolderSettings.class);
        i.putExtra(EXTRA_FOLDER_NAME, folderName);
        i.putExtra(EXTRA_ACCOUNT, account.getUuid());
        context.startActivity(i);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String folderName = (String) getIntent().getSerializableExtra(EXTRA_FOLDER_NAME);
        Account mAccount = Preferences.getPreferences(this).getAccount(getIntent().getStringExtra(EXTRA_ACCOUNT));
        try {
            this.mFolder = mAccount.getLocalStore().getFolder(folderName);
            this.mFolder.open(0);
            boolean isPushCapable = false;
            try {
                isPushCapable = mAccount.getRemoteStore().isPushCapable();
            } catch (Exception e) {
                Log.e("k9", "Could not get remote store", e);
            }
            addPreferencesFromResource(R.xml.folder_settings_preferences);
            findPreference(PREFERENCE_TOP_CATERGORY).setTitle(FolderInfoHolder.getDisplayName(this, mAccount, this.mFolder.getName()));
            this.mInTopGroup = (CheckBoxPreference) findPreference(PREFERENCE_IN_TOP_GROUP);
            this.mInTopGroup.setChecked(this.mFolder.isInTopGroup());
            this.mIntegrate = (CheckBoxPreference) findPreference(PREFERENCE_INTEGRATE);
            this.mIntegrate.setChecked(this.mFolder.isIntegrate());
            this.mDisplayClass = (ListPreference) findPreference(PREFERENCE_DISPLAY_CLASS);
            this.mDisplayClass.setValue(this.mFolder.getDisplayClass().name());
            this.mDisplayClass.setSummary(this.mDisplayClass.getEntry());
            this.mDisplayClass.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    String summary = newValue.toString();
                    FolderSettings.this.mDisplayClass.setSummary(FolderSettings.this.mDisplayClass.getEntries()[FolderSettings.this.mDisplayClass.findIndexOfValue(summary)]);
                    FolderSettings.this.mDisplayClass.setValue(summary);
                    return false;
                }
            });
            this.mSyncClass = (ListPreference) findPreference(PREFERENCE_SYNC_CLASS);
            this.mSyncClass.setValue(this.mFolder.getRawSyncClass().name());
            this.mSyncClass.setSummary(this.mSyncClass.getEntry());
            this.mSyncClass.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    String summary = newValue.toString();
                    FolderSettings.this.mSyncClass.setSummary(FolderSettings.this.mSyncClass.getEntries()[FolderSettings.this.mSyncClass.findIndexOfValue(summary)]);
                    FolderSettings.this.mSyncClass.setValue(summary);
                    return false;
                }
            });
            this.mPushClass = (ListPreference) findPreference(PREFERENCE_PUSH_CLASS);
            this.mPushClass.setEnabled(isPushCapable);
            this.mPushClass.setValue(this.mFolder.getRawPushClass().name());
            this.mPushClass.setSummary(this.mPushClass.getEntry());
            this.mPushClass.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    String summary = newValue.toString();
                    FolderSettings.this.mPushClass.setSummary(FolderSettings.this.mPushClass.getEntries()[FolderSettings.this.mPushClass.findIndexOfValue(summary)]);
                    FolderSettings.this.mPushClass.setValue(summary);
                    return false;
                }
            });
            this.mNotifyClass = (ListPreference) findPreference(PREFERENCE_NOTIFY_CLASS);
            this.mNotifyClass.setValue(this.mFolder.getRawNotifyClass().name());
            this.mNotifyClass.setSummary(this.mNotifyClass.getEntry());
            this.mNotifyClass.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    String summary = newValue.toString();
                    FolderSettings.this.mNotifyClass.setSummary(FolderSettings.this.mNotifyClass.getEntries()[FolderSettings.this.mNotifyClass.findIndexOfValue(summary)]);
                    FolderSettings.this.mNotifyClass.setValue(summary);
                    return false;
                }
            });
        } catch (MessagingException me2) {
            Log.e("k9", "Unable to edit folder " + folderName + " preferences", me2);
        }
    }

    private void saveSettings() throws MessagingException {
        this.mFolder.setInTopGroup(this.mInTopGroup.isChecked());
        this.mFolder.setIntegrate(this.mIntegrate.isChecked());
        Folder.FolderClass oldPushClass = this.mFolder.getPushClass();
        Folder.FolderClass oldDisplayClass = this.mFolder.getDisplayClass();
        this.mFolder.setDisplayClass(Folder.FolderClass.valueOf(this.mDisplayClass.getValue()));
        this.mFolder.setSyncClass(Folder.FolderClass.valueOf(this.mSyncClass.getValue()));
        this.mFolder.setPushClass(Folder.FolderClass.valueOf(this.mPushClass.getValue()));
        this.mFolder.setNotifyClass(Folder.FolderClass.valueOf(this.mNotifyClass.getValue()));
        this.mFolder.save();
        Folder.FolderClass newPushClass = this.mFolder.getPushClass();
        Folder.FolderClass newDisplayClass = this.mFolder.getDisplayClass();
        if (oldPushClass != newPushClass || (newPushClass != Folder.FolderClass.NO_CLASS && oldDisplayClass != newDisplayClass)) {
            MailService.actionRestartPushers(getApplication(), null);
        }
    }

    public void onPause() {
        try {
            saveSettings();
        } catch (MessagingException e) {
            Log.e("k9", "Saving folder settings failed", e);
        }
        super.onPause();
    }
}
