package com.fsck.k9.activity;

import android.annotation.SuppressLint;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Parcelable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.media.TransportMediator;
import android.support.v4.view.PointerIconCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import atomicgonza.AccountUtils;
import atomicgonza.Ads;
import atomicgonza.App;
import billing.IabHelper;
import billing.IabResult;
import billing.Inventory;
import billing.Purchase;
import com.fsck.k9.Account;
import com.fsck.k9.K9;
import com.fsck.k9.Preferences;
import com.fsck.k9.activity.compose.MessageActions;
import com.fsck.k9.activity.misc.SwipeGestureDetector;
import com.fsck.k9.activity.setup.AccountSettings;
import com.fsck.k9.activity.setup.FolderSettings;
import com.fsck.k9.activity.setup.Prefs;
import com.fsck.k9.fragment.MessageListFragment;
import com.fsck.k9.helper.Utility;
import com.fsck.k9.mailstore.StorageManager;
import com.fsck.k9.preferences.SettingsExporter;
import com.fsck.k9.preferences.StorageEditor;
import com.fsck.k9.search.LocalSearch;
import com.fsck.k9.search.SearchAccount;
import com.fsck.k9.search.SearchSpecification;
import com.fsck.k9.ui.messageview.MessageViewFragment;
import com.fsck.k9.view.MessageHeader;
import com.fsck.k9.view.MessageTitleView;
import com.fsck.k9.view.ViewSwitcher;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.android.gms.ads.AdView;
import com.tjeannin.apprate.AppRate;
import java.util.Iterator;
import java.util.List;
import yahoo.mail.app.R;

public class MessageList extends K9AppCompactActivity implements MessageListFragment.MessageListFragmentListener, MessageViewFragment.MessageViewFragmentListener, FragmentManager.OnBackStackChangedListener, SwipeGestureDetector.OnSwipeGestureListener, ViewSwitcher.OnSwitchCompleteListener {
    private static final String ACTION_SHORTCUT = "shortcut";
    private static final String EXTRA_MESSAGE_REFERENCE = "message_reference";
    private static final String EXTRA_NO_THREADING = "no_threading";
    private static final String EXTRA_SEARCH = "search";
    public static final String EXTRA_SEARCH_ACCOUNT = "com.fsck.k9.search_account";
    private static final String EXTRA_SEARCH_FOLDER = "com.fsck.k9.search_folder";
    private static final String EXTRA_SPECIAL_FOLDER = "special_folder";
    private static final int NEXT = 2;
    private static final int PREVIOUS = 1;
    public static final int REQUEST_MASK_PENDING_INTENT = 65536;
    private static final String STATE_DISPLAY_MODE = "displayMode";
    private static final String STATE_MESSAGE_LIST_WAS_DISPLAYED = "messageListWasDisplayed";
    boolean isMenuItemPurchase;
    /* access modifiers changed from: private */
    public Account mAccount;
    private View mActionBarMessageList;
    private View mActionBarMessageView;
    private ProgressBar mActionBarProgress;
    private TextView mActionBarSubTitle;
    private MessageTitleView mActionBarSubject;
    private TextView mActionBarTitle;
    private TextView mActionBarUnread;
    private View mActionButtonIndeterminateProgress;
    private CountDownTimer mCountdownKeepScreen;
    private DisplayMode mDisplayMode;
    View mFab;
    private int mFirstBackStackId = -1;
    private String mFolderName;
    private int mLastDirection;
    private Menu mMenu;
    private MenuItem mMenuButtonCheckMail;
    public MessageListFragment mMessageListFragment;
    private boolean mMessageListWasDisplayed;
    private MessageReference mMessageReference;
    private ViewGroup mMessageViewContainer;
    private MessageViewFragment mMessageViewFragment;
    private View mMessageViewPlaceHolder;
    private boolean mNoThreading;
    IabHelper mPurchaseHelper;
    private LocalSearch mSearch;
    private boolean mSingleAccountMode;
    private boolean mSingleFolderMode;
    private StorageManager.StorageListener mStorageListener = new StorageListenerImplementation();
    private ViewSwitcher mViewSwitcher;

    private enum DisplayMode {
        MESSAGE_LIST,
        MESSAGE_VIEW,
        SPLIT_VIEW
    }

    public MessageList() {
        int i;
        if (K9.messageViewShowNext()) {
            i = 2;
        } else {
            i = 1;
        }
        this.mLastDirection = i;
        this.mMessageListWasDisplayed = false;
        this.isMenuItemPurchase = false;
    }

    public static void actionDisplaySearch(Context context, SearchSpecification search, boolean noThreading, boolean newTask) {
        actionDisplaySearch(context, search, noThreading, newTask, true);
    }

    public static void actionDisplaySearch(Context context, SearchSpecification search, boolean noThreading, boolean newTask, boolean clearTop) {
        context.startActivity(intentDisplaySearch(context, search, noThreading, newTask, clearTop));
    }

    public static Intent intentDisplaySearch(Context context, SearchSpecification search, boolean noThreading, boolean newTask, boolean clearTop) {
        Intent intent = new Intent(context, MessageList.class);
        intent.putExtra(EXTRA_SEARCH, search);
        intent.putExtra(EXTRA_NO_THREADING, noThreading);
        if (clearTop) {
            intent.addFlags(67108864);
        }
        if (newTask) {
            intent.addFlags(268435456);
        }
        return intent;
    }

    public static Intent shortcutIntent(Context context, String specialFolder) {
        Intent intent = new Intent(context, MessageList.class);
        intent.setAction(ACTION_SHORTCUT);
        intent.putExtra(EXTRA_SPECIAL_FOLDER, specialFolder);
        intent.addFlags(67108864);
        intent.addFlags(268435456);
        return intent;
    }

    public static Intent actionDisplayMessageIntent(Context context, MessageReference messageReference) {
        Intent intent = new Intent(context, MessageList.class);
        intent.addFlags(67108864);
        intent.putExtra("message_reference", messageReference);
        return intent;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= 21) {
            Window win = getWindow();
            win.addFlags(Integer.MIN_VALUE);
            win.setStatusBarColor(getResources().getColor(R.color.statusBarBackgroundNormal));
        }
        if (UpgradeDatabases.actionUpgradeDatabases(this, getIntent())) {
            finish();
            return;
        }
        if (useSplitView()) {
            setContentView((int) R.layout.split_message_list);
        } else {
            setTheme((int) com.fsck.k9.R.style.Theme_K9_LightAtomicGonzaFloating);
            setContentView((int) R.layout.message_list);
            this.mViewSwitcher = (ViewSwitcher) findViewById(R.id.container);
            this.mViewSwitcher.setFirstInAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_in_left));
            this.mViewSwitcher.setFirstOutAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_out_right));
            this.mViewSwitcher.setSecondInAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_in_right));
            this.mViewSwitcher.setSecondOutAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_out_left));
            this.mViewSwitcher.setOnSwitchCompleteListener(this);
        }
        initializeActionBar();
        setupGestureDetector(this);
        if (decodeExtras(getIntent())) {
            findFragments();
            initializeDisplayMode(savedInstanceState);
            initializeLayout();
            initializeFragments();
            displayViews();
            AccountUtils.guideUserToWidget(this);
            Ads.checkToInitAds(this);
            if (this.mFab == null) {
                this.mFab = findViewById(R.id.floatAction);
                this.mFab.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        MessageList.this.onCompose(MessageList.this.mAccount);
                    }
                });
            }
            if (this.mFab != null) {
                this.mFab.setVisibility(0);
            }
            new AppRate(this).setShowIfAppHasCrashed(true).setMinDaysUntilPrompt(3).setMinLaunchesUntilPrompt(0).init();
        }
    }

    public void setMenuPurchase(boolean enabled) {
        if (this.mMenu != null) {
            this.mMenu.findItem(R.id.buy).setVisible(enabled);
        }
        this.isMenuItemPurchase = enabled;
    }

    public void purchaseItemAkMail(final SharedPreferences prefs) {
        if (Utility.hasConnectivity(this)) {
            this.mPurchaseHelper = new IabHelper(this, App.getBase64EncodedPublicKey());
            this.mPurchaseHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
                public void onIabSetupFinished(IabResult result) {
                    if (!result.isSuccess()) {
                        MessageList.this.appPurchasedCallback(prefs, false);
                        return;
                    }
                    try {
                        MessageList.this.mPurchaseHelper.queryInventoryAsync(new IabHelper.QueryInventoryFinishedListener() {
                            public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
                                if (result.isFailure()) {
                                    MessageList.this.appPurchasedCallback(prefs, false);
                                } else if (inventory.hasPurchase("")) {
                                    MessageList.this.appPurchasedCallback(prefs, true);
                                } else {
                                    try {
                                        MessageList.this.mPurchaseHelper.launchPurchaseFlow(MessageList.this, "", PointerIconCompat.TYPE_CONTEXT_MENU, new IabHelper.OnIabPurchaseFinishedListener() {
                                            public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
                                                if (result.isFailure()) {
                                                    MessageList.this.appPurchasedCallback(prefs, false);
                                                } else if (purchase.getSku().equals("")) {
                                                    MessageList.this.appPurchasedCallback(prefs, true);
                                                }
                                            }
                                        }, null);
                                    } catch (IabHelper.IabAsyncInProgressException e) {
                                        MessageList.this.appPurchasedCallback(prefs, false);
                                    }
                                }
                            }
                        });
                    } catch (IabHelper.IabAsyncInProgressException e) {
                        MessageList.this.appPurchasedCallback(prefs, false);
                    }
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.mPurchaseHelper != null) {
            try {
                this.mPurchaseHelper.dispose();
            } catch (IabHelper.IabAsyncInProgressException e) {
                Log.e("AtomicGonza", "okAG  " + new Exception().getStackTrace()[0].toString());
            }
        }
        this.mPurchaseHelper = null;
    }

    public void appPurchasedCallback(SharedPreferences prefs, boolean purchased) {
        if (prefs == null) {
            prefs = App.getPref(this);
        }
        if (!purchased || !prefs.edit().putBoolean(App.KEY_APP_PURCHASED, true).commit()) {
            prefs.edit().putBoolean(App.KEY_APP_PURCHASED, false).putBoolean(Ads.key_user_rejected, false).commit();
            Toast.makeText(this, "Error with purchase data, please contact developers", 0).show();
            setMenuPurchase(true);
            Ads.checkToInitAds(this);
            return;
        }
        Toast.makeText(this, (int) R.string.app_purchased_sucess, 0).show();
        AdView ad = (AdView) findViewById(R.id.adView);
        ad.destroy();
        ad.setVisibility(8);
        setMenuPurchase(false);
    }

    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        if (this.mFirstBackStackId >= 0) {
            getFragmentManager().popBackStackImmediate(this.mFirstBackStackId, 1);
            this.mFirstBackStackId = -1;
        }
        removeMessageListFragment();
        removeMessageViewFragment();
        this.mMessageReference = null;
        this.mSearch = null;
        this.mFolderName = null;
        if (decodeExtras(intent)) {
            initializeDisplayMode(null);
            initializeFragments();
            displayViews();
        }
    }

    private void findFragments() {
        FragmentManager fragmentManager = getFragmentManager();
        this.mMessageListFragment = (MessageListFragment) fragmentManager.findFragmentById(R.id.message_list_container);
        this.mMessageViewFragment = (MessageViewFragment) fragmentManager.findFragmentById(R.id.message_view_container);
    }

    private void initializeFragments() {
        boolean hasMessageListFragment;
        boolean z = true;
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.addOnBackStackChangedListener(this);
        if (this.mMessageListFragment != null) {
            hasMessageListFragment = true;
        } else {
            hasMessageListFragment = false;
        }
        if (!hasMessageListFragment) {
            FragmentTransaction ft = fragmentManager.beginTransaction();
            LocalSearch localSearch = this.mSearch;
            if (!K9.isThreadedViewEnabled() || this.mNoThreading) {
                z = false;
            }
            this.mMessageListFragment = MessageListFragment.newInstance(localSearch, false, z);
            ft.add((int) R.id.message_list_container, this.mMessageListFragment);
            ft.commit();
        }
        if (!hasMessageListFragment && this.mMessageViewFragment == null && this.mMessageReference != null) {
            openMessage(this.mMessageReference);
        }
    }

    private void initializeDisplayMode(Bundle savedInstanceState) {
        DisplayMode savedDisplayMode;
        if (useSplitView()) {
            this.mDisplayMode = DisplayMode.SPLIT_VIEW;
        } else if (savedInstanceState != null && (savedDisplayMode = (DisplayMode) savedInstanceState.getSerializable(STATE_DISPLAY_MODE)) != DisplayMode.SPLIT_VIEW) {
            this.mDisplayMode = savedDisplayMode;
        } else if (this.mMessageViewFragment == null && this.mMessageReference == null) {
            this.mDisplayMode = DisplayMode.MESSAGE_LIST;
        } else {
            this.mDisplayMode = DisplayMode.MESSAGE_VIEW;
        }
    }

    private boolean useSplitView() {
        K9.SplitViewMode splitViewMode = K9.getSplitViewMode();
        return splitViewMode == K9.SplitViewMode.ALWAYS || (splitViewMode == K9.SplitViewMode.WHEN_IN_LANDSCAPE && getResources().getConfiguration().orientation == 2);
    }

    private void initializeLayout() {
        this.mMessageViewContainer = (ViewGroup) findViewById(R.id.message_view_container);
        this.mMessageViewPlaceHolder = getLayoutInflater().inflate((int) R.layout.empty_message_view, (ViewGroup) null);
    }

    private void displayViews() {
        switch (this.mDisplayMode) {
            case MESSAGE_LIST:
                showMessageList();
                return;
            case MESSAGE_VIEW:
                showMessageView();
                return;
            case SPLIT_VIEW:
                this.mMessageListWasDisplayed = true;
                if (this.mMessageViewFragment == null) {
                    showMessageViewPlaceHolder();
                    return;
                }
                MessageReference activeMessage = this.mMessageViewFragment.getMessageReference();
                if (activeMessage != null) {
                    this.mMessageListFragment.setActiveMessage(activeMessage);
                    return;
                }
                return;
            default:
                return;
        }
    }

    private boolean decodeExtras(Intent intent) {
        String action = intent.getAction();
        if ("android.intent.action.VIEW".equals(action) && intent.getData() != null) {
            List<String> segmentList = intent.getData().getPathSegments();
            String accountId = segmentList.get(0);
            Iterator<Account> it = Preferences.getPreferences(this).getAvailableAccounts().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                Account account = it.next();
                if (String.valueOf(account.getAccountNumber()).equals(accountId)) {
                    this.mMessageReference = new MessageReference(account.getUuid(), segmentList.get(1), segmentList.get(2), null);
                    break;
                }
            }
        } else if (ACTION_SHORTCUT.equals(action)) {
            String specialFolder = intent.getStringExtra(EXTRA_SPECIAL_FOLDER);
            if (SearchAccount.UNIFIED_INBOX.equals(specialFolder)) {
                this.mSearch = SearchAccount.createUnifiedInboxAccount(this).getRelatedSearch();
            } else if (SearchAccount.ALL_MESSAGES.equals(specialFolder)) {
                this.mSearch = SearchAccount.createAllMessagesAccount(this).getRelatedSearch();
            }
        } else if (intent.getStringExtra("query") == null) {
            this.mSearch = (LocalSearch) intent.getParcelableExtra(EXTRA_SEARCH);
            this.mNoThreading = intent.getBooleanExtra(EXTRA_NO_THREADING, false);
        } else if ("android.intent.action.SEARCH".equals(intent.getAction())) {
            String query = intent.getStringExtra("query").trim();
            this.mSearch = new LocalSearch(getString(R.string.search_results));
            this.mSearch.setManualSearch(true);
            this.mNoThreading = true;
            this.mSearch.or(new SearchSpecification.SearchCondition(SearchSpecification.SearchField.SENDER, SearchSpecification.Attribute.CONTAINS, query));
            this.mSearch.or(new SearchSpecification.SearchCondition(SearchSpecification.SearchField.SUBJECT, SearchSpecification.Attribute.CONTAINS, query));
            this.mSearch.or(new SearchSpecification.SearchCondition(SearchSpecification.SearchField.MESSAGE_CONTENTS, SearchSpecification.Attribute.CONTAINS, query));
            Bundle appData = intent.getBundleExtra("app_data");
            if (appData != null) {
                this.mSearch.addAccountUuid(appData.getString(EXTRA_SEARCH_ACCOUNT));
                if (appData.getString(EXTRA_SEARCH_FOLDER) != null) {
                    this.mSearch.addAllowedFolder(appData.getString(EXTRA_SEARCH_FOLDER));
                }
            } else {
                this.mSearch.addAccountUuid(SearchSpecification.ALL_ACCOUNTS);
            }
        }
        if (this.mMessageReference == null) {
            this.mMessageReference = (MessageReference) intent.getParcelableExtra("message_reference");
        }
        if (this.mMessageReference != null) {
            this.mSearch = new LocalSearch();
            this.mSearch.addAccountUuid(this.mMessageReference.getAccountUuid());
            this.mSearch.addAllowedFolder(this.mMessageReference.getFolderName());
        }
        if (this.mSearch == null) {
            String accountUuid = intent.getStringExtra("account");
            String folderName = intent.getStringExtra(SettingsExporter.FOLDER_ELEMENT);
            this.mSearch = new LocalSearch(folderName);
            LocalSearch localSearch = this.mSearch;
            if (accountUuid == null) {
                accountUuid = "invalid";
            }
            localSearch.addAccountUuid(accountUuid);
            if (folderName != null) {
                this.mSearch.addAllowedFolder(folderName);
            }
        }
        Preferences prefs = Preferences.getPreferences(getApplicationContext());
        String[] accountUuids = this.mSearch.getAccountUuids();
        if (this.mSearch.searchAllAccounts()) {
            List<Account> accounts = prefs.getAccounts();
            this.mSingleAccountMode = accounts.size() == 1;
            if (this.mSingleAccountMode) {
                this.mAccount = accounts.get(0);
            }
        } else {
            this.mSingleAccountMode = accountUuids.length == 1;
            if (this.mSingleAccountMode) {
                this.mAccount = prefs.getAccount(accountUuids[0]);
            }
        }
        this.mSingleFolderMode = this.mSingleAccountMode && this.mSearch.getFolderNames().size() == 1;
        if (!this.mSingleAccountMode || (this.mAccount != null && this.mAccount.isAvailable(this))) {
            if (this.mSingleFolderMode) {
                this.mFolderName = this.mSearch.getFolderNames().get(0);
            }
            this.mActionBarSubTitle.setVisibility(!this.mSingleFolderMode ? 8 : 0);
            return true;
        }
        Log.i("k9", "not opening MessageList of unavailable account");
        onAccountUnavailable();
        return false;
    }

    public void onPause() {
        super.onPause();
        StorageManager.getInstance(getApplication()).removeListener(this.mStorageListener);
        if (this.mCountdownKeepScreen != null) {
            this.mCountdownKeepScreen.cancel();
        }
        getWindow().clearFlags(128);
    }

    public void onResume() {
        super.onResume();
        if (!(this instanceof Search)) {
            Search.setActive(false);
        }
        if (this.mAccount == null || this.mAccount.isAvailable(this)) {
            StorageManager.getInstance(getApplication()).addListener(this.mStorageListener);
            if (this.mCountdownKeepScreen == null) {
                this.mCountdownKeepScreen = new CountDownTimer(60000, 100 + 60000) {
                    public void onTick(long millisUntilFinished) {
                    }

                    public void onFinish() {
                        MessageList.this.getWindow().clearFlags(128);
                    }
                };
            }
            getWindow().addFlags(128);
            this.mCountdownKeepScreen.cancel();
            this.mCountdownKeepScreen.start();
            return;
        }
        onAccountUnavailable();
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(STATE_DISPLAY_MODE, this.mDisplayMode);
        outState.putBoolean(STATE_MESSAGE_LIST_WAS_DISPLAYED, this.mMessageListWasDisplayed);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        this.mMessageListWasDisplayed = savedInstanceState.getBoolean(STATE_MESSAGE_LIST_WAS_DISPLAYED);
    }

    private void initializeActionBar() {
        Toolbar mActionBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mActionBar);
        ActionBar aBar = getSupportActionBar();
        aBar.setDisplayShowHomeEnabled(false);
        aBar.setDisplayShowTitleEnabled(false);
        aBar.setDisplayUseLogoEnabled(false);
        ActionBar bar = getSupportActionBar();
        bar.setDisplayShowHomeEnabled(false);
        bar.setDisplayShowTitleEnabled(false);
        bar.setDisplayUseLogoEnabled(false);
        bar.setDisplayHomeAsUpEnabled(true);
        View customView = mActionBar;
        this.mActionBarMessageList = customView.findViewById(R.id.actionbar_message_list);
        this.mActionBarMessageView = customView.findViewById(R.id.actionbar_message_view);
        this.mActionBarSubject = (MessageTitleView) customView.findViewById(R.id.message_title_view);
        this.mActionBarTitle = (TextView) customView.findViewById(R.id.actionbar_title_first);
        this.mActionBarSubTitle = (TextView) customView.findViewById(R.id.actionbar_title_sub);
        this.mActionBarUnread = (TextView) customView.findViewById(R.id.actionbar_unread_count);
        this.mActionBarProgress = (ProgressBar) customView.findViewById(R.id.actionbar_progress);
        this.mActionButtonIndeterminateProgress = getActionButtonIndeterminateProgress();
    }

    @SuppressLint({"InflateParams"})
    private View getActionButtonIndeterminateProgress() {
        return getLayoutInflater().inflate((int) R.layout.actionbar_indeterminate_progress_actionview, (ViewGroup) null);
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        boolean ret = false;
        if (event.getAction() == 0) {
            ret = onCustomKeyDown(event.getKeyCode(), event);
        }
        if (!ret) {
            return super.dispatchKeyEvent(event);
        }
        return ret;
    }

    public void onBackPressed() {
        if (this.mDisplayMode != DisplayMode.MESSAGE_VIEW || !this.mMessageListWasDisplayed) {
            super.onBackPressed();
        } else {
            showMessageList();
        }
    }

    public boolean onCustomKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case 24:
                if (this.mMessageViewFragment != null && this.mDisplayMode != DisplayMode.MESSAGE_LIST && K9.useVolumeKeysForNavigationEnabled()) {
                    showPreviousMessage();
                    return true;
                } else if (this.mDisplayMode != DisplayMode.MESSAGE_VIEW && K9.useVolumeKeysForListNavigationEnabled()) {
                    this.mMessageListFragment.onMoveUp();
                    return true;
                }
                break;
            case 25:
                if (this.mMessageViewFragment != null && this.mDisplayMode != DisplayMode.MESSAGE_LIST && K9.useVolumeKeysForNavigationEnabled()) {
                    showNextMessage();
                    return true;
                } else if (this.mDisplayMode != DisplayMode.MESSAGE_VIEW && K9.useVolumeKeysForListNavigationEnabled()) {
                    this.mMessageListFragment.onMoveDown();
                    return true;
                }
                break;
            case 29:
                if (this.mMessageViewFragment == null) {
                    return true;
                }
                this.mMessageViewFragment.onReplyAll();
                return true;
            case 31:
                this.mMessageListFragment.onCompose();
                return true;
            case 32:
            case 67:
                if (this.mDisplayMode == DisplayMode.MESSAGE_LIST) {
                    this.mMessageListFragment.onDelete();
                    return true;
                } else if (this.mMessageViewFragment == null) {
                    return true;
                } else {
                    this.mMessageViewFragment.onDelete();
                    return true;
                }
            case 34:
                if (this.mMessageViewFragment == null) {
                    return true;
                }
                this.mMessageViewFragment.onForward();
                return true;
            case 35:
                if (this.mDisplayMode == DisplayMode.MESSAGE_LIST) {
                    this.mMessageListFragment.onToggleFlagged();
                    return true;
                } else if (this.mMessageViewFragment == null) {
                    return true;
                } else {
                    this.mMessageViewFragment.onToggleFlagged();
                    return true;
                }
            case 36:
                Toast.makeText(this, (int) R.string.message_list_help_key, 1).show();
                return true;
            case 37:
                this.mMessageListFragment.onReverseSort();
                return true;
            case 38:
            case 44:
                if (this.mMessageViewFragment == null) {
                    return true;
                }
                showPreviousMessage();
                return true;
            case 39:
            case 42:
                if (this.mMessageViewFragment == null) {
                    return true;
                }
                showNextMessage();
                return true;
            case 41:
                if (this.mDisplayMode == DisplayMode.MESSAGE_LIST) {
                    this.mMessageListFragment.onMove();
                    return true;
                } else if (this.mMessageViewFragment == null) {
                    return true;
                } else {
                    this.mMessageViewFragment.onMove();
                    return true;
                }
            case 43:
                this.mMessageListFragment.onCycleSort();
                return true;
            case 45:
                if (this.mMessageListFragment == null || !this.mMessageListFragment.isSingleAccountMode()) {
                    return true;
                }
                onShowFolderList();
                return true;
            case 46:
                if (this.mMessageViewFragment == null) {
                    return true;
                }
                this.mMessageViewFragment.onReply();
                return true;
            case 47:
                this.mMessageListFragment.toggleMessageSelect();
                return true;
            case 50:
                if (this.mDisplayMode == DisplayMode.MESSAGE_LIST) {
                    this.mMessageListFragment.onArchive();
                    return true;
                } else if (this.mMessageViewFragment == null) {
                    return true;
                } else {
                    this.mMessageViewFragment.onArchive();
                    return true;
                }
            case 53:
                if (this.mDisplayMode == DisplayMode.MESSAGE_LIST) {
                    this.mMessageListFragment.onCopy();
                    return true;
                } else if (this.mMessageViewFragment == null) {
                    return true;
                } else {
                    this.mMessageViewFragment.onCopy();
                    return true;
                }
            case 54:
                if (this.mDisplayMode == DisplayMode.MESSAGE_LIST) {
                    this.mMessageListFragment.onToggleRead();
                    return true;
                } else if (this.mMessageViewFragment == null) {
                    return true;
                } else {
                    this.mMessageViewFragment.onToggleRead();
                    return true;
                }
        }
        return false;
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (!K9.useVolumeKeysForListNavigationEnabled() || (keyCode != 24 && keyCode != 25)) {
            return super.onKeyUp(keyCode, event);
        }
        if (K9.DEBUG) {
            Log.v("k9", "Swallowed key up.");
        }
        return true;
    }

    private void onAccounts() {
        Accounts.listAccounts(this);
        finish();
    }

    private void onShowFolderList() {
        FolderList.actionHandleAccount(this, this.mAccount);
        finish();
    }

    private void onEditPrefs() {
        Prefs.actionPrefs(this);
    }

    private void onEditAccount() {
        AccountSettings.actionSettings(this, this.mAccount);
    }

    public boolean onSearchRequested() {
        return this.mMessageListFragment.onSearchRequested();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        switch (itemId) {
            case 16908332:
                goBack();
                return true;
            case R.id.account_settings /*2131755439*/:
                onEditAccount();
                return true;
            case R.id.search /*2131755445*/:
                this.mMessageListFragment.onSearchRequested();
                return true;
            case R.id.check_mail /*2131755446*/:
                this.mMessageListFragment.checkMail();
                return true;
            case R.id.compose /*2131755447*/:
                this.mMessageListFragment.onCompose();
                return true;
            case R.id.app_settings /*2131755465*/:
                onEditPrefs();
                return true;
            case R.id.delete /*2131755480*/:
                this.mMessageViewFragment.onDelete();
                return true;
            case R.id.archive /*2131755483*/:
            case R.id.refile_archive /*2131755512*/:
                this.mMessageViewFragment.onArchive();
                return true;
            case R.id.move /*2131755484*/:
            case R.id.refile_move /*2131755514*/:
                this.mMessageViewFragment.onMove();
                return true;
            case R.id.copy /*2131755485*/:
            case R.id.refile_copy /*2131755515*/:
                this.mMessageViewFragment.onCopy();
                return true;
            case R.id.spam /*2131755488*/:
            case R.id.refile_spam /*2131755513*/:
                this.mMessageViewFragment.onSpam();
                return true;
            case R.id.select_all /*2131755489*/:
                this.mMessageListFragment.selectAll();
                return true;
            case R.id.reply_all /*2131755492*/:
                this.mMessageViewFragment.onReplyAll();
                return true;
            case R.id.reply /*2131755493*/:
                this.mMessageViewFragment.onReply();
                return true;
            case R.id.forward /*2131755495*/:
                this.mMessageViewFragment.onForward();
                return true;
            case R.id.previous_message /*2131755498*/:
                showPreviousMessage();
                return true;
            case R.id.next_message /*2131755499*/:
                showNextMessage();
                return true;
            case R.id.search_remote /*2131755500*/:
                this.mMessageListFragment.onRemoteSearch();
                return true;
            case R.id.set_sort_date /*2131755502*/:
                this.mMessageListFragment.changeSort(Account.SortType.SORT_DATE);
                return true;
            case R.id.set_sort_arrival /*2131755503*/:
                this.mMessageListFragment.changeSort(Account.SortType.SORT_ARRIVAL);
                return true;
            case R.id.set_sort_subject /*2131755504*/:
                this.mMessageListFragment.changeSort(Account.SortType.SORT_SUBJECT);
                return true;
            case R.id.set_sort_sender /*2131755505*/:
                this.mMessageListFragment.changeSort(Account.SortType.SORT_SENDER);
                return true;
            case R.id.set_sort_flag /*2131755506*/:
                this.mMessageListFragment.changeSort(Account.SortType.SORT_FLAGGED);
                return true;
            case R.id.set_sort_unread /*2131755507*/:
                this.mMessageListFragment.changeSort(Account.SortType.SORT_UNREAD);
                return true;
            case R.id.set_sort_attach /*2131755508*/:
                this.mMessageListFragment.changeSort(Account.SortType.SORT_ATTACHMENT);
                return true;
            case R.id.share /*2131755510*/:
                this.mMessageViewFragment.onSendAlternate();
                return true;
            case R.id.toggle_unread /*2131755516*/:
                this.mMessageViewFragment.onToggleRead();
                return true;
            case R.id.show_headers /*2131755517*/:
            case R.id.hide_headers /*2131755518*/:
                this.mMessageViewFragment.onToggleAllHeadersView();
                updateMenu();
                return true;
            case R.id.mark_all_as_read /*2131755519*/:
                this.mMessageListFragment.confirmMarkAllAsRead();
                return true;
            case R.id.select_text /*2131755521*/:
                this.mMessageViewFragment.onSelectText();
                return true;
            case R.id.toggle_message_view_theme /*2131755522*/:
                onToggleTheme();
                return true;
            case R.id.show_folder_list /*2131755523*/:
                onShowFolderList();
                return true;
            default:
                if (!this.mSingleFolderMode) {
                    return false;
                }
                switch (itemId) {
                    case R.id.folder_settings /*2131755455*/:
                        if (this.mFolderName == null) {
                            return true;
                        }
                        FolderSettings.actionSettings(this, this.mAccount, this.mFolderName);
                        return true;
                    case R.id.send_messages /*2131755461*/:
                        this.mMessageListFragment.onSendPendingMessages();
                        return true;
                    case R.id.expunge /*2131755520*/:
                        this.mMessageListFragment.onExpunge();
                        return true;
                    case R.id.buy /*2131755524*/:
                        purchaseItemAkMail(App.getPref(this));
                        return true;
                    default:
                        return super.onOptionsItemSelected(item);
                }
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.message_list_option, menu);
        this.mMenu = menu;
        this.mMenuButtonCheckMail = menu.findItem(R.id.check_mail);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        configureMenu(menu);
        return true;
    }

    private void configureMenu(Menu menu) {
        int i;
        if (menu != null) {
            setMenuPurchase(this.isMenuItemPurchase);
            if (this.mMessageListFragment == null) {
                menu.findItem(R.id.account_settings).setVisible(false);
                menu.findItem(R.id.folder_settings).setVisible(false);
            } else {
                menu.findItem(R.id.account_settings).setVisible(this.mMessageListFragment.isSingleAccountMode());
                menu.findItem(R.id.folder_settings).setVisible(this.mMessageListFragment.isSingleFolderMode());
            }
            if (this.mDisplayMode == DisplayMode.MESSAGE_LIST || this.mMessageViewFragment == null || !this.mMessageViewFragment.isInitialized()) {
                menu.findItem(R.id.next_message).setVisible(false);
                menu.findItem(R.id.previous_message).setVisible(false);
                menu.findItem(R.id.single_message_options).setVisible(false);
                menu.findItem(R.id.delete).setVisible(false);
                menu.findItem(R.id.compose).setVisible(false);
                menu.findItem(R.id.archive).setVisible(false);
                menu.findItem(R.id.move).setVisible(false);
                menu.findItem(R.id.copy).setVisible(false);
                menu.findItem(R.id.spam).setVisible(false);
                menu.findItem(R.id.refile).setVisible(false);
                menu.findItem(R.id.toggle_unread).setVisible(false);
                menu.findItem(R.id.select_text).setVisible(false);
                menu.findItem(R.id.toggle_message_view_theme).setVisible(false);
                menu.findItem(R.id.show_headers).setVisible(false);
                menu.findItem(R.id.hide_headers).setVisible(false);
            } else {
                if (this.mDisplayMode != DisplayMode.MESSAGE_VIEW) {
                    menu.findItem(R.id.next_message).setVisible(false);
                    menu.findItem(R.id.previous_message).setVisible(false);
                } else {
                    MessageReference ref = this.mMessageViewFragment.getMessageReference();
                    boolean initialized = this.mMessageListFragment != null && this.mMessageListFragment.isLoadFinished();
                    boolean canDoPrev = initialized && !this.mMessageListFragment.isFirst(ref);
                    boolean canDoNext = initialized && !this.mMessageListFragment.isLast(ref);
                    MenuItem prev = menu.findItem(R.id.previous_message);
                    prev.setEnabled(canDoPrev);
                    prev.getIcon().setAlpha(canDoPrev ? 255 : TransportMediator.KEYCODE_MEDIA_PAUSE);
                    MenuItem next = menu.findItem(R.id.next_message);
                    next.setEnabled(canDoNext);
                    Drawable icon = next.getIcon();
                    if (canDoNext) {
                        i = 255;
                    } else {
                        i = TransportMediator.KEYCODE_MEDIA_PAUSE;
                    }
                    icon.setAlpha(i);
                }
                MenuItem toggleTheme = menu.findItem(R.id.toggle_message_view_theme);
                if (K9.useFixedMessageViewTheme()) {
                    toggleTheme.setVisible(false);
                } else {
                    if (K9.getK9MessageViewTheme() == K9.Theme.DARK) {
                        toggleTheme.setTitle((int) R.string.message_view_theme_action_light);
                    } else {
                        toggleTheme.setTitle((int) R.string.message_view_theme_action_dark);
                    }
                    toggleTheme.setVisible(true);
                }
                if (this.mMessageViewFragment.isMessageRead()) {
                    menu.findItem(R.id.toggle_unread).setTitle((int) R.string.mark_as_unread_action);
                } else {
                    menu.findItem(R.id.toggle_unread).setTitle((int) R.string.mark_as_read_action);
                }
                menu.findItem(R.id.select_text).setVisible(Build.VERSION.SDK_INT < 16);
                menu.findItem(R.id.delete).setVisible(K9.isMessageViewDeleteActionVisible());
                if (this.mMessageViewFragment.isCopyCapable()) {
                    menu.findItem(R.id.copy).setVisible(K9.isMessageViewCopyActionVisible());
                    menu.findItem(R.id.refile_copy).setVisible(true);
                } else {
                    menu.findItem(R.id.copy).setVisible(false);
                    menu.findItem(R.id.refile_copy).setVisible(false);
                }
                if (this.mMessageViewFragment.isMoveCapable()) {
                    boolean canMessageBeArchived = this.mMessageViewFragment.canMessageBeArchived();
                    boolean canMessageBeMovedToSpam = this.mMessageViewFragment.canMessageBeMovedToSpam();
                    menu.findItem(R.id.move).setVisible(K9.isMessageViewMoveActionVisible());
                    menu.findItem(R.id.archive).setVisible(canMessageBeArchived && K9.isMessageViewArchiveActionVisible());
                    menu.findItem(R.id.spam).setVisible(canMessageBeMovedToSpam && K9.isMessageViewSpamActionVisible());
                    menu.findItem(R.id.refile_move).setVisible(true);
                    menu.findItem(R.id.refile_archive).setVisible(canMessageBeArchived);
                    menu.findItem(R.id.refile_spam).setVisible(canMessageBeMovedToSpam);
                } else {
                    menu.findItem(R.id.move).setVisible(false);
                    menu.findItem(R.id.archive).setVisible(false);
                    menu.findItem(R.id.spam).setVisible(false);
                    menu.findItem(R.id.refile).setVisible(false);
                }
                if (this.mMessageViewFragment.allHeadersVisible()) {
                    menu.findItem(R.id.show_headers).setVisible(false);
                } else {
                    menu.findItem(R.id.hide_headers).setVisible(false);
                }
            }
            menu.findItem(R.id.search).setVisible(false);
            menu.findItem(R.id.search_remote).setVisible(false);
            if (this.mDisplayMode == DisplayMode.MESSAGE_VIEW || this.mMessageListFragment == null || !this.mMessageListFragment.isInitialized()) {
                menu.findItem(R.id.check_mail).setVisible(false);
                menu.findItem(R.id.set_sort).setVisible(false);
                menu.findItem(R.id.select_all).setVisible(false);
                menu.findItem(R.id.send_messages).setVisible(false);
                menu.findItem(R.id.expunge).setVisible(false);
                menu.findItem(R.id.mark_all_as_read).setVisible(false);
                menu.findItem(R.id.show_folder_list).setVisible(false);
                return;
            }
            menu.findItem(R.id.set_sort).setVisible(true);
            menu.findItem(R.id.select_all).setVisible(true);
            menu.findItem(R.id.compose).setVisible(true);
            menu.findItem(R.id.mark_all_as_read).setVisible(this.mMessageListFragment.isMarkAllAsReadSupported());
            if (!this.mMessageListFragment.isSingleAccountMode()) {
                menu.findItem(R.id.expunge).setVisible(false);
                menu.findItem(R.id.send_messages).setVisible(false);
                menu.findItem(R.id.show_folder_list).setVisible(false);
            } else {
                menu.findItem(R.id.send_messages).setVisible(this.mMessageListFragment.isOutbox());
                menu.findItem(R.id.expunge).setVisible(this.mMessageListFragment.isRemoteFolder() && this.mMessageListFragment.isAccountExpungeCapable());
                menu.findItem(R.id.show_folder_list).setVisible(true);
            }
            menu.findItem(R.id.check_mail).setVisible(this.mMessageListFragment.isCheckMailSupported());
            if (!this.mMessageListFragment.isRemoteSearch() && this.mMessageListFragment.isRemoteSearchAllowed() && this.mMessageListFragment.isSingleAccountMode()) {
                menu.findItem(R.id.search_remote).setVisible(true);
            } else if (!this.mMessageListFragment.isManualSearch() && this.mMessageListFragment.isSingleAccountMode()) {
                menu.findItem(R.id.search).setVisible(true);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onAccountUnavailable() {
        finish();
        Accounts.listAccounts(this);
    }

    public void setActionBarTitle(String title) {
        this.mActionBarTitle.setText(title);
    }

    public void setActionBarSubTitle(String subTitle) {
        this.mActionBarSubTitle.setText(subTitle);
    }

    public void setActionBarUnread(int unread) {
        if (unread == 0) {
            this.mActionBarUnread.setVisibility(8);
            return;
        }
        this.mActionBarUnread.setVisibility(0);
        this.mActionBarUnread.setText(String.format("%d", Integer.valueOf(unread)));
    }

    public void setMessageListTitle(String title) {
        setActionBarTitle(title);
    }

    public void setMessageListSubTitle(String subTitle) {
        setActionBarSubTitle(subTitle);
    }

    public void setUnreadCount(int unread) {
        setActionBarUnread(unread);
    }

    public void setMessageListProgress(int progress) {
        setProgress(progress);
    }

    public void openMessage(MessageReference messageReference) {
        if (messageReference.getFolderName().equals(Preferences.getPreferences(getApplicationContext()).getAccount(messageReference.getAccountUuid()).getDraftsFolderName())) {
            MessageActions.actionEditDraft(this, messageReference);
            return;
        }
        this.mMessageViewContainer.removeView(this.mMessageViewPlaceHolder);
        if (this.mMessageListFragment != null) {
            this.mMessageListFragment.setActiveMessage(messageReference);
        }
        MessageViewFragment fragment = MessageViewFragment.newInstance(messageReference);
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.message_view_container, fragment);
        this.mMessageViewFragment = fragment;
        ft.commit();
        if (this.mDisplayMode != DisplayMode.SPLIT_VIEW) {
            showMessageView();
        }
    }

    public void onResendMessage(MessageReference messageReference) {
        MessageActions.actionEditDraft(this, messageReference);
    }

    public void onForward(MessageReference messageReference) {
        onForward(messageReference, null);
    }

    public void onForward(MessageReference messageReference, Parcelable decryptionResultForReply) {
        MessageActions.actionForward(this, messageReference, decryptionResultForReply);
    }

    public void onReply(MessageReference messageReference) {
        onReply(messageReference, null);
    }

    public void onReply(MessageReference messageReference, Parcelable decryptionResultForReply) {
        MessageActions.actionReply(this, messageReference, false, decryptionResultForReply);
    }

    public void onReplyAll(MessageReference messageReference) {
        onReplyAll(messageReference, null);
    }

    public void onReplyAll(MessageReference messageReference, Parcelable decryptionResultForReply) {
        MessageActions.actionReply(this, messageReference, true, decryptionResultForReply);
    }

    public void onCompose(Account account) {
        MessageActions.actionCompose(this, account);
    }

    public void showMoreFromSameSender(String senderAddress) {
        LocalSearch tmpSearch = new LocalSearch("From " + senderAddress);
        tmpSearch.addAccountUuids(this.mSearch.getAccountUuids());
        tmpSearch.and(SearchSpecification.SearchField.SENDER, senderAddress, SearchSpecification.Attribute.CONTAINS);
        addMessageListFragment(MessageListFragment.newInstance(tmpSearch, false, false), true);
    }

    public void onBackStackChanged() {
        findFragments();
        if (this.mDisplayMode == DisplayMode.SPLIT_VIEW) {
            showMessageViewPlaceHolder();
        }
        configureMenu(this.mMenu);
    }

    public void onSwipeRightToLeft(MotionEvent e1, MotionEvent e2) {
        if (this.mMessageListFragment != null && this.mDisplayMode != DisplayMode.MESSAGE_VIEW) {
            this.mMessageListFragment.onSwipeRightToLeft(e1, e2);
        }
    }

    public void onSwipeLeftToRight(MotionEvent e1, MotionEvent e2) {
        if (this.mMessageListFragment != null && this.mDisplayMode != DisplayMode.MESSAGE_VIEW) {
            this.mMessageListFragment.onSwipeLeftToRight(e1, e2);
        }
    }

    private final class StorageListenerImplementation implements StorageManager.StorageListener {
        private StorageListenerImplementation() {
        }

        public void onUnmount(String providerId) {
            if (MessageList.this.mAccount != null && providerId.equals(MessageList.this.mAccount.getLocalStorageProviderId())) {
                MessageList.this.runOnUiThread(new Runnable() {
                    public void run() {
                        MessageList.this.onAccountUnavailable();
                    }
                });
            }
        }

        public void onMount(String providerId) {
        }
    }

    private void addMessageListFragment(MessageListFragment fragment, boolean addToBackStack) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.message_list_container, fragment);
        if (addToBackStack) {
            ft.addToBackStack(null);
        }
        this.mMessageListFragment = fragment;
        int transactionId = ft.commit();
        if (transactionId >= 0 && this.mFirstBackStackId < 0) {
            this.mFirstBackStackId = transactionId;
        }
    }

    public boolean startSearch(Account account, String folderName) {
        if (account == null || folderName == null) {
            startSearch(null, false, null, false);
            return true;
        }
        Bundle appData = new Bundle();
        appData.putString(EXTRA_SEARCH_ACCOUNT, account.getUuid());
        appData.putString(EXTRA_SEARCH_FOLDER, folderName);
        startSearch(null, false, appData, false);
        return true;
    }

    public void showThread(Account account, String folderName, long threadRootId) {
        showMessageViewPlaceHolder();
        LocalSearch tmpSearch = new LocalSearch();
        tmpSearch.addAccountUuid(account.getUuid());
        tmpSearch.and(SearchSpecification.SearchField.THREAD_ID, String.valueOf(threadRootId), SearchSpecification.Attribute.EQUALS);
        addMessageListFragment(MessageListFragment.newInstance(tmpSearch, true, false), true);
    }

    private void showMessageViewPlaceHolder() {
        removeMessageViewFragment();
        if (this.mMessageViewPlaceHolder.getParent() == null) {
            this.mMessageViewContainer.addView(this.mMessageViewPlaceHolder);
        }
        this.mMessageListFragment.setActiveMessage(null);
    }

    private void removeMessageViewFragment() {
        if (this.mMessageViewFragment != null) {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.remove(this.mMessageViewFragment);
            this.mMessageViewFragment = null;
            ft.commit();
            showDefaultTitleView();
        }
    }

    private void removeMessageListFragment() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.remove(this.mMessageListFragment);
        this.mMessageListFragment = null;
        ft.commit();
    }

    public void remoteSearchStarted() {
        configureMenu(this.mMenu);
    }

    public void goBack() {
        FragmentManager fragmentManager = getFragmentManager();
        if (this.mDisplayMode == DisplayMode.MESSAGE_VIEW) {
            showMessageList();
        } else if (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStack();
        } else if (this.mMessageListFragment.isManualSearch()) {
            finish();
        } else if (!this.mSingleFolderMode) {
            onAccounts();
        } else {
            onShowFolderList();
        }
    }

    public void enableActionBarProgress(boolean enable) {
        if (this.mMenuButtonCheckMail == null || !this.mMenuButtonCheckMail.isVisible()) {
            if (this.mMenuButtonCheckMail != null) {
                this.mMenuButtonCheckMail.setActionView((View) null);
            }
            if (enable) {
                this.mActionBarProgress.setVisibility(0);
            } else {
                this.mActionBarProgress.setVisibility(8);
            }
        } else {
            this.mActionBarProgress.setVisibility(8);
            if (enable) {
                this.mMenuButtonCheckMail.setActionView(this.mActionButtonIndeterminateProgress);
            } else {
                this.mMenuButtonCheckMail.setActionView((View) null);
            }
        }
    }

    public void displayMessageSubject(String subject) {
        if (this.mDisplayMode == DisplayMode.MESSAGE_VIEW) {
            this.mActionBarSubject.setText(subject);
        }
    }

    public void showNextMessageOrReturn() {
        if (!K9.messageViewReturnToList() && showLogicalNextMessage()) {
            return;
        }
        if (this.mDisplayMode == DisplayMode.SPLIT_VIEW) {
            showMessageViewPlaceHolder();
        } else {
            showMessageList();
        }
    }

    private boolean showLogicalNextMessage() {
        boolean result = false;
        if (this.mLastDirection == 2) {
            result = showNextMessage();
        } else if (this.mLastDirection == 1) {
            result = showPreviousMessage();
        }
        if (!result) {
            return showNextMessage() || showPreviousMessage();
        }
        return result;
    }

    public void setProgress(boolean enable) {
        setProgressBarIndeterminateVisibility(enable);
    }

    public void messageHeaderViewAvailable(MessageHeader header) {
        this.mActionBarSubject.setMessageHeader(header);
    }

    private boolean showNextMessage() {
        MessageReference ref = this.mMessageViewFragment.getMessageReference();
        if (ref == null || !this.mMessageListFragment.openNext(ref)) {
            return false;
        }
        this.mLastDirection = 2;
        return true;
    }

    private boolean showPreviousMessage() {
        MessageReference ref = this.mMessageViewFragment.getMessageReference();
        if (ref == null || !this.mMessageListFragment.openPrevious(ref)) {
            return false;
        }
        this.mLastDirection = 1;
        return true;
    }

    private void showMessageList() {
        this.mMessageListWasDisplayed = true;
        this.mDisplayMode = DisplayMode.MESSAGE_LIST;
        this.mViewSwitcher.showFirstView();
        this.mMessageListFragment.setActiveMessage(null);
        showDefaultTitleView();
        configureMenu(this.mMenu);
        if (this.mFab != null) {
            this.mFab.setVisibility(0);
        }
        ((AppBarLayout.LayoutParams) findViewById(R.id.toolbar).getLayoutParams()).setScrollFlags(0);
    }

    private void showMessageView() {
        this.mDisplayMode = DisplayMode.MESSAGE_VIEW;
        if (!this.mMessageListWasDisplayed) {
            this.mViewSwitcher.setAnimateFirstView(false);
        }
        this.mViewSwitcher.showSecondView();
        showMessageTitleView();
        if (this.mFab != null) {
            this.mFab.setVisibility(4);
        }
        configureMenu(this.mMenu);
        if (K9.isToolBarCollapsible()) {
            ((AppBarLayout.LayoutParams) findViewById(R.id.toolbar).getLayoutParams()).setScrollFlags(5);
        } else {
            ((AppBarLayout.LayoutParams) findViewById(R.id.toolbar).getLayoutParams()).setScrollFlags(0);
        }
    }

    public void updateMenu() {
        invalidateOptionsMenu();
    }

    public void disableDeleteAction() {
        this.mMenu.findItem(R.id.delete).setEnabled(false);
    }

    private void onToggleTheme() {
        if (K9.getK9MessageViewTheme() == K9.Theme.DARK) {
            K9.setK9MessageViewThemeSetting(K9.Theme.LIGHT);
        } else {
            K9.setK9MessageViewThemeSetting(K9.Theme.DARK);
        }
        new Thread(new Runnable() {
            public void run() {
                StorageEditor editor = Preferences.getPreferences(MessageList.this.getApplicationContext()).getStorage().edit();
                K9.save(editor);
                editor.commit();
            }
        }).start();
        recreate();
    }

    private void showDefaultTitleView() {
        this.mActionBarMessageView.setVisibility(8);
        this.mActionBarMessageList.setVisibility(0);
        if (this.mMessageListFragment != null) {
            this.mMessageListFragment.updateTitle();
        }
        this.mActionBarSubject.setMessageHeader(null);
    }

    private void showMessageTitleView() {
        this.mActionBarMessageList.setVisibility(8);
        this.mActionBarMessageView.setVisibility(0);
        if (this.mMessageViewFragment != null) {
            displayMessageSubject(null);
            this.mMessageViewFragment.updateTitle();
        }
    }

    public void onSwitchComplete(int displayedChild) {
        if (displayedChild == 0) {
            removeMessageViewFragment();
        }
    }

    public void startIntentSenderForResult(IntentSender intent, int requestCode, Intent fillInIntent, int flagsMask, int flagsValues, int extraFlags) throws IntentSender.SendIntentException {
        super.startIntentSenderForResult(intent, requestCode | 65536, fillInIntent, flagsMask, flagsValues, extraFlags);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ((requestCode & 65536) == 65536) {
            int requestCode2 = requestCode ^ 65536;
            if (this.mMessageViewFragment != null) {
                this.mMessageViewFragment.onPendingIntentResult(requestCode2, resultCode, data);
            }
        } else if (requestCode == 25) {
            Toast.makeText(this, "msg", 0).show();
        }
    }

    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this);
    }

    public void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this);
    }
}
