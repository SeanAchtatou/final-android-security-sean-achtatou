package com.fsck.k9.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.TextView;
import com.fsck.k9.Account;
import com.fsck.k9.K9;
import com.fsck.k9.Preferences;
import com.fsck.k9.service.DatabaseUpgradeService;
import yahoo.mail.app.R;

public class UpgradeDatabases extends K9Activity {
    private static final String ACTION_UPGRADE_DATABASES = "upgrade_databases";
    private static final String EXTRA_START_INTENT = "start_intent";
    private UpgradeDatabaseBroadcastReceiver mBroadcastReceiver;
    private IntentFilter mIntentFilter;
    private LocalBroadcastManager mLocalBroadcastManager;
    /* access modifiers changed from: private */
    public Preferences mPreferences;
    private Intent mStartIntent;
    /* access modifiers changed from: private */
    public TextView mUpgradeText;

    public static boolean actionUpgradeDatabases(Context context, Intent startIntent) {
        if (K9.areDatabasesUpToDate()) {
            return false;
        }
        Intent intent = new Intent(context, UpgradeDatabases.class);
        intent.setAction(ACTION_UPGRADE_DATABASES);
        intent.putExtra(EXTRA_START_INTENT, startIntent);
        intent.addFlags(536903680);
        context.startActivity(intent);
        return true;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (K9.areDatabasesUpToDate()) {
            launchOriginalActivity();
            return;
        }
        this.mPreferences = Preferences.getPreferences(getApplicationContext());
        initializeLayout();
        decodeExtras();
        setupBroadcastReceiver();
    }

    private void initializeLayout() {
        setContentView((int) R.layout.upgrade_databases);
        this.mUpgradeText = (TextView) findViewById(R.id.databaseUpgradeText);
    }

    private void decodeExtras() {
        this.mStartIntent = (Intent) getIntent().getParcelableExtra(EXTRA_START_INTENT);
    }

    private void setupBroadcastReceiver() {
        this.mLocalBroadcastManager = LocalBroadcastManager.getInstance(this);
        this.mBroadcastReceiver = new UpgradeDatabaseBroadcastReceiver();
        this.mIntentFilter = new IntentFilter(DatabaseUpgradeService.ACTION_UPGRADE_PROGRESS);
        this.mIntentFilter.addAction(DatabaseUpgradeService.ACTION_UPGRADE_COMPLETE);
    }

    public void onResume() {
        super.onResume();
        if (K9.areDatabasesUpToDate()) {
            launchOriginalActivity();
            return;
        }
        this.mLocalBroadcastManager.registerReceiver(this.mBroadcastReceiver, this.mIntentFilter);
        DatabaseUpgradeService.startService(this);
    }

    public void onPause() {
        this.mLocalBroadcastManager.unregisterReceiver(this.mBroadcastReceiver);
        super.onPause();
    }

    /* access modifiers changed from: private */
    public void launchOriginalActivity() {
        finish();
        if (this.mStartIntent != null) {
            startActivity(this.mStartIntent);
        }
    }

    class UpgradeDatabaseBroadcastReceiver extends BroadcastReceiver {
        UpgradeDatabaseBroadcastReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (DatabaseUpgradeService.ACTION_UPGRADE_PROGRESS.equals(action)) {
                Account account = UpgradeDatabases.this.mPreferences.getAccount(intent.getStringExtra("account_uuid"));
                if (account != null) {
                    UpgradeDatabases.this.mUpgradeText.setText(String.format(UpgradeDatabases.this.getString(R.string.upgrade_database_format), account.getDescription()));
                }
            } else if (DatabaseUpgradeService.ACTION_UPGRADE_COMPLETE.equals(action)) {
                UpgradeDatabases.this.launchOriginalActivity();
            }
        }
    }
}
