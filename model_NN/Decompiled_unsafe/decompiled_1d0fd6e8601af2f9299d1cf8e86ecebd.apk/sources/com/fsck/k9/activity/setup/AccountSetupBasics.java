package com.fsck.k9.activity.setup;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.XmlResourceParser;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import atomicgonza.Dialogs;
import atomicgonza.GmailUtils;
import atomicgonza.Views;
import com.fsck.k9.Account;
import com.fsck.k9.EmailAddressValidator;
import com.fsck.k9.Globals;
import com.fsck.k9.K9;
import com.fsck.k9.Preferences;
import com.fsck.k9.account.AccountCreator;
import com.fsck.k9.activity.K9Activity;
import com.fsck.k9.activity.setup.AccountSetupCheckSettings;
import com.fsck.k9.helper.UrlEncodingHelper;
import com.fsck.k9.helper.Utility;
import com.fsck.k9.mail.AuthType;
import com.fsck.k9.mail.ConnectionSecurity;
import com.fsck.k9.mail.ServerSettings;
import com.fsck.k9.mail.Transport;
import com.fsck.k9.mail.oauth.AuthorizationException;
import com.fsck.k9.mail.oauth.OAuth2TokenProvider;
import com.fsck.k9.mail.store.RemoteStore;
import com.fsck.k9.preferences.SettingsExporter;
import com.fsck.k9.provider.MessageProvider;
import com.fsck.k9.view.ClientCertificateSpinner;
import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Locale;
import yahoo.mail.app.R;

public class AccountSetupBasics extends K9Activity implements View.OnClickListener, TextWatcher, ClientCertificateSpinner.OnClientCertificateChangedListener {
    private static final int DIALOG_NOTE = 1;
    private static final String EXTRA_ACCOUNT = "com.fsck.k9.AccountSetupBasics.account";
    private static final String STATE_KEY_CHECKED_INCOMING = "com.fsck.k9.AccountSetupBasics.checkedIncoming";
    private static final String STATE_KEY_PROVIDER = "com.fsck.k9.AccountSetupBasics.provider";
    List<Account> accountsRegistered;
    /* access modifiers changed from: private */
    public Account mAccount;
    private View mAccountSpinner;
    private boolean mCheckedIncoming = false;
    /* access modifiers changed from: private */
    public CheckBox mClientCertificateCheckBox;
    /* access modifiers changed from: private */
    public ClientCertificateSpinner mClientCertificateSpinner;
    private EmailAddressValidator mEmailValidator = new EmailAddressValidator();
    /* access modifiers changed from: private */
    public EditText mEmailView;
    private Button mManualSetupButton;
    private Button mNextButton;
    private EditText mPasswordView;
    private Provider mProvider;
    private CheckBox mShowPasswordCheckBox;

    public static class Provider implements Serializable {
        private static final long serialVersionUID = 8511656164616538989L;
        public String domain;
        public String id;
        public URI incomingUriTemplate;
        public String incomingUsernameTemplate;
        public String label;
        public String note;
        public URI outgoingUriTemplate;
        public String outgoingUsernameTemplate;
    }

    public static void actionNewAccount(Context context) {
        context.startActivity(new Intent(context, AccountSetupBasics.class));
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(8);
        getActionBar().hide();
        if (Build.VERSION.SDK_INT >= 21) {
            Window win = getWindow();
            win.addFlags(Integer.MIN_VALUE);
            win.setStatusBarColor(getResources().getColor(R.color.statusBarBackgroundNormal));
        }
        setContentView((int) R.layout.account_setup_basics);
        findViewById(R.id.textViewPrivacyPolicy).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Dialogs.displayLicense(AccountSetupBasics.this.getLayoutInflater(), AccountSetupBasics.this);
            }
        });
        this.mEmailView = (EditText) findViewById(R.id.account_email);
        this.mPasswordView = (EditText) findViewById(R.id.account_password);
        this.mClientCertificateCheckBox = (CheckBox) findViewById(R.id.account_client_certificate);
        this.mClientCertificateSpinner = (ClientCertificateSpinner) findViewById(R.id.account_client_certificate_spinner);
        this.mAccountSpinner = findViewById(R.id.account_view);
        OAuth2TokenProvider oAuth2TokenProvider = Globals.getOAuth2TokenProvider();
        this.accountsRegistered = Preferences.getPreferences(this).getAccounts();
        final List<String> accountsCandidates = oAuth2TokenProvider.getAccounts();
        if (accountsCandidates == null || accountsCandidates.size() < 1) {
            Views.gone(findViewById(R.id.linearLayoutGmailAccounts));
        } else if (this.accountsRegistered != null && this.accountsRegistered.size() > 0) {
            for (Account accountRegistered : this.accountsRegistered) {
                accountsCandidates.remove(accountRegistered.getEmail());
            }
        }
        if (accountsCandidates == null || accountsCandidates.size() < 1) {
            Views.gone(findViewById(R.id.linearLayoutGmailAccounts));
        } else {
            ((TextView) findViewById(R.id.textViewAccount)).setText(accountsCandidates.get(0));
            if (accountsCandidates.size() == 1) {
                findViewById(R.id.imglogoGLeft).setVisibility(4);
                this.mAccountSpinner.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        if (accountsCandidates.size() == 1) {
                            ((ProgressBar) AccountSetupBasics.this.findViewById(R.id.progressBarOAuth)).setVisibility(0);
                            AccountSetupBasics.this.mEmailView.setText((CharSequence) accountsCandidates.get(0));
                            AccountSetupBasics.this.onNext();
                        }
                    }
                });
            } else {
                final LayoutInflater inf = getLayoutInflater();
                final ListAdapter adapter = new BaseAdapter() {
                    public long getItemId(int position) {
                        return 0;
                    }

                    public Object getItem(int position) {
                        return accountsCandidates.get(position);
                    }

                    public int getCount() {
                        return accountsCandidates.size();
                    }

                    public View getView(int position, View convertView, ViewGroup parent) {
                        if (convertView != null) {
                            return convertView;
                        }
                        View convertView2 = inf.inflate((int) R.layout.item_list, (ViewGroup) null);
                        ((TextView) convertView2.findViewById(R.id.textViewAccount)).setText((CharSequence) accountsCandidates.get(position));
                        return convertView2;
                    }
                };
                this.mAccountSpinner.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        ListView listAccounts = new ListView(AccountSetupBasics.this);
                        listAccounts.setAdapter(adapter);
                        final Dialog d = new AlertDialog.Builder(AccountSetupBasics.this).setView(listAccounts).show();
                        listAccounts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                                ((ProgressBar) AccountSetupBasics.this.findViewById(R.id.progressBarOAuth)).setVisibility(0);
                                d.cancel();
                                AccountSetupBasics.this.mEmailView.setText(adapter.getItem(position).toString());
                                AccountSetupBasics.this.onNext();
                            }
                        });
                    }
                });
            }
        }
        this.mNextButton = (Button) findViewById(R.id.next);
        this.mManualSetupButton = (Button) findViewById(R.id.manual_setup);
        this.mShowPasswordCheckBox = (CheckBox) findViewById(R.id.show_password);
        this.mNextButton.setOnClickListener(this);
        this.mManualSetupButton.setOnClickListener(this);
    }

    private void initializeViewListeners() {
        this.mEmailView.addTextChangedListener(this);
        this.mPasswordView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus && !AccountSetupBasics.this.mClientCertificateCheckBox.isChecked() && GmailUtils.isGmailAccount(AccountSetupBasics.this.mEmailView.getText().toString())) {
                    AccountSetupBasics.this.onNext();
                }
            }
        });
        this.mPasswordView.addTextChangedListener(this);
        this.mClientCertificateCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                AccountSetupBasics.this.updateViewVisibility(AccountSetupBasics.this.mClientCertificateCheckBox.isChecked());
                AccountSetupBasics.this.validateFields();
                if (isChecked) {
                    AccountSetupBasics.this.mClientCertificateSpinner.chooseCertificate();
                }
            }
        });
        this.mClientCertificateSpinner.setOnClientCertificateChangedListener(this);
        this.mShowPasswordCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                AccountSetupBasics.this.showPassword(isChecked);
            }
        });
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (this.mAccount != null) {
            outState.putString(EXTRA_ACCOUNT, this.mAccount.getUuid());
        }
        if (this.mProvider != null) {
            outState.putSerializable(STATE_KEY_PROVIDER, this.mProvider);
        }
        outState.putBoolean(STATE_KEY_CHECKED_INCOMING, this.mCheckedIncoming);
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState.containsKey(EXTRA_ACCOUNT)) {
            this.mAccount = Preferences.getPreferences(this).getAccount(savedInstanceState.getString(EXTRA_ACCOUNT));
        }
        if (savedInstanceState.containsKey(STATE_KEY_PROVIDER)) {
            this.mProvider = (Provider) savedInstanceState.getSerializable(STATE_KEY_PROVIDER);
        }
        this.mCheckedIncoming = savedInstanceState.getBoolean(STATE_KEY_CHECKED_INCOMING);
        updateViewVisibility(this.mClientCertificateCheckBox.isChecked());
        showPassword(this.mShowPasswordCheckBox.isChecked());
    }

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        initializeViewListeners();
        validateFields();
    }

    public void afterTextChanged(Editable s) {
        validateFields();
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    public void onClientCertificateChanged(String alias) {
        validateFields();
    }

    /* access modifiers changed from: private */
    public void updateViewVisibility(boolean usingCertificates) {
        if (usingCertificates) {
            this.mPasswordView.setText("");
            this.mClientCertificateSpinner.setVisibility(0);
        } else if (GmailUtils.isGmailAccount(this.mEmailView.getText().toString())) {
            this.mAccountSpinner.setVisibility(0);
            this.mClientCertificateCheckBox.setEnabled(false);
        } else {
            this.mEmailView.setVisibility(0);
            this.mPasswordView.setVisibility(0);
            this.mShowPasswordCheckBox.setVisibility(0);
            this.mClientCertificateSpinner.setVisibility(8);
            this.mClientCertificateCheckBox.setEnabled(true);
        }
    }

    /* access modifiers changed from: private */
    public void showPassword(boolean show) {
        int cursorPosition = this.mPasswordView.getSelectionStart();
        if (show) {
            this.mPasswordView.setInputType(128);
        } else {
            this.mPasswordView.setInputType(129);
        }
        this.mPasswordView.setSelection(cursorPosition);
    }

    /* access modifiers changed from: private */
    public void validateFields() {
        boolean clientCertificateChecked = this.mClientCertificateCheckBox.isChecked();
        boolean valid = GmailUtils.isGmailAccount(this.mEmailView.getText().toString()) || (Utility.requiredFieldValid(this.mEmailView) && ((!clientCertificateChecked && Utility.requiredFieldValid(this.mPasswordView)) || (clientCertificateChecked && this.mClientCertificateSpinner.getAlias() != null)));
        this.mNextButton.setEnabled(valid);
        this.mManualSetupButton.setEnabled(valid);
        Utility.setCompoundDrawablesAlpha(this.mNextButton, this.mNextButton.isEnabled() ? 255 : 128);
    }

    private String getOwnerName() {
        String name = null;
        try {
            name = getDefaultAccountName();
        } catch (Exception e) {
            Log.e("k9", "Could not get default account name", e);
        }
        if (name == null) {
            return "";
        }
        return name;
    }

    private String getDefaultAccountName() {
        Account account = Preferences.getPreferences(this).getDefaultAccount();
        if (account != null) {
            return account.getName();
        }
        return null;
    }

    public Dialog onCreateDialog(int id) {
        if (id != 1 || this.mProvider == null || this.mProvider.note == null) {
            return null;
        }
        return new AlertDialog.Builder(this).setMessage(this.mProvider.note).setPositiveButton(getString(R.string.okay_action), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                AccountSetupBasics.this.finishAutoSetup();
            }
        }).setNegativeButton(getString(R.string.cancel_action), (DialogInterface.OnClickListener) null).create();
    }

    /* access modifiers changed from: private */
    public void finishAutoSetup() {
        URI outgoingUri;
        boolean usingXOAuth2 = GmailUtils.isGmailAccount(this.mEmailView.getText().toString());
        String email = this.mEmailView.getText().toString();
        String password = this.mPasswordView.getText().toString();
        String[] emailParts = splitEmail(email);
        String user = emailParts[0];
        String domain = emailParts[1];
        try {
            String userEnc = UrlEncodingHelper.encodeUtf8(user);
            String passwordEnc = UrlEncodingHelper.encodeUtf8(password);
            String incomingUsername = this.mProvider.incomingUsernameTemplate.replaceAll("\\$email", email).replaceAll("\\$user", userEnc).replaceAll("\\$domain", domain);
            URI incomingUriTemplate = this.mProvider.incomingUriTemplate;
            String incomingUserInfo = incomingUsername + ":" + passwordEnc;
            if (usingXOAuth2) {
                incomingUserInfo = AuthType.XOAUTH2 + ":" + incomingUserInfo;
            }
            URI incomingUri = new URI(incomingUriTemplate.getScheme(), incomingUserInfo, incomingUriTemplate.getHost(), incomingUriTemplate.getPort(), null, null, null);
            String outgoingUsername = this.mProvider.outgoingUsernameTemplate;
            URI outgoingUriTemplate = this.mProvider.outgoingUriTemplate;
            if (outgoingUsername != null) {
                String outgoingUserInfo = outgoingUsername.replaceAll("\\$email", email).replaceAll("\\$user", userEnc).replaceAll("\\$domain", domain) + ":" + passwordEnc;
                if (usingXOAuth2) {
                    outgoingUserInfo = outgoingUserInfo + ":" + AuthType.XOAUTH2;
                }
                outgoingUri = new URI(outgoingUriTemplate.getScheme(), outgoingUserInfo, outgoingUriTemplate.getHost(), outgoingUriTemplate.getPort(), null, null, null);
            } else {
                outgoingUri = new URI(outgoingUriTemplate.getScheme(), null, outgoingUriTemplate.getHost(), outgoingUriTemplate.getPort(), null, null, null);
            }
            if (this.mAccount == null) {
                this.mAccount = Preferences.getPreferences(this).newAccount();
            }
            this.mAccount.setName(getOwnerName());
            this.mAccount.setEmail(email);
            this.mAccount.setStoreUri(incomingUri.toString());
            this.mAccount.setTransportUri(outgoingUri.toString());
            setupFolderNames(incomingUriTemplate.getHost().toLowerCase(Locale.US));
            ServerSettings decodeStoreUri = RemoteStore.decodeStoreUri(this.mAccount.getStoreUri());
            this.mAccount.setDeletePolicy(AccountCreator.getDefaultDeletePolicy(RemoteStore.decodeStoreUri(incomingUri.toString()).type));
            if (usingXOAuth2) {
                Globals.getOAuth2TokenProvider().authorizeAPI(this.mAccount.getEmail(), this, new OAuth2TokenProvider.OAuth2TokenProviderAuthCallback() {
                    public void success() {
                        ((ProgressBar) AccountSetupBasics.this.findViewById(R.id.progressBarOAuth)).setVisibility(8);
                        AccountSetupCheckSettings.actionCheckSettings(AccountSetupBasics.this, AccountSetupBasics.this.mAccount, AccountSetupCheckSettings.CheckDirection.INCOMING, true);
                    }

                    public void failure(AuthorizationException e) {
                        ((ProgressBar) AccountSetupBasics.this.findViewById(R.id.progressBarOAuth)).setVisibility(8);
                        if (AccountSetupBasics.this.getString(R.string.xoauth2_account_doesnt_exist).equals(e.getMessage())) {
                            GmailUtils.displayDialogOauth2(AccountSetupBasics.this, AccountSetupBasics.this.mAccount, true);
                        } else {
                            Toast.makeText(AccountSetupBasics.this.getApplication(), AccountSetupBasics.this.getString(R.string.account_setup_bad_uri, new Object[]{e.getMessage()}) + " " + AccountSetupBasics.this.mAccount.getEmail(), 1).show();
                        }
                        Log.e("k9", "Failure", e);
                    }
                });
            } else {
                AccountSetupCheckSettings.actionCheckSettings(this, this.mAccount, AccountSetupCheckSettings.CheckDirection.INCOMING, true);
            }
        } catch (URISyntaxException e) {
            onManualSetup();
        }
    }

    /* access modifiers changed from: private */
    public void onNext() {
        if (this.mClientCertificateCheckBox.isChecked()) {
            onManualSetup();
            return;
        }
        String email = this.mEmailView.getText().toString();
        if (this.accountsRegistered != null && this.accountsRegistered.size() > 0) {
            for (Account accountRegistered : this.accountsRegistered) {
                if (accountRegistered.getEmail() != null && accountRegistered.getEmail().equalsIgnoreCase(email)) {
                    this.mEmailView.setText("");
                    Toast.makeText(this, (int) R.string.account_already_present_msj, 0).show();
                    return;
                }
            }
        }
        this.mProvider = findProviderForDomain(splitEmail(email)[1]);
        if (this.mProvider == null) {
            onManualSetup();
            return;
        }
        Log.i("k9", "Provider found, using automatic set-up");
        if (this.mProvider.note == null) {
            finishAutoSetup();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == -1) {
            if (!this.mCheckedIncoming) {
                this.mCheckedIncoming = true;
                AccountSetupCheckSettings.actionCheckSettings(this, this.mAccount, AccountSetupCheckSettings.CheckDirection.OUTGOING, true);
                return;
            }
            this.mAccount.setDescription(this.mAccount.getEmail());
            this.mAccount.save(Preferences.getPreferences(this));
            K9.setServicesEnabled(this);
            AccountSetupNames.actionSetNames(this, this.mAccount);
            finish();
        } else if (resultCode == -11) {
            this.mAccount = null;
        }
    }

    private void onManualSetup() {
        AuthType authenticationType;
        String email = this.mEmailView.getText().toString();
        String user = email;
        String domain = splitEmail(email)[1];
        String password = null;
        String clientCertificateAlias = null;
        if (this.mClientCertificateCheckBox.isChecked()) {
            authenticationType = AuthType.EXTERNAL;
            clientCertificateAlias = this.mClientCertificateSpinner.getAlias();
        } else if (GmailUtils.isGmailAccount(this.mEmailView.getText().toString())) {
            authenticationType = AuthType.XOAUTH2;
        } else {
            authenticationType = AuthType.PLAIN;
            password = this.mPasswordView.getText().toString();
        }
        if (this.mAccount == null) {
            this.mAccount = Preferences.getPreferences(this).newAccount();
        }
        this.mAccount.setName(getOwnerName());
        this.mAccount.setEmail(email);
        ServerSettings storeServer = new ServerSettings(ServerSettings.Type.IMAP, "mail." + domain, -1, ConnectionSecurity.SSL_TLS_REQUIRED, authenticationType, user, password, clientCertificateAlias);
        ServerSettings transportServer = new ServerSettings(ServerSettings.Type.SMTP, "mail." + domain, -1, ConnectionSecurity.SSL_TLS_REQUIRED, authenticationType, user, password, clientCertificateAlias);
        String storeUri = RemoteStore.createStoreUri(storeServer);
        String transportUri = Transport.createTransportUri(transportServer);
        this.mAccount.setStoreUri(storeUri);
        this.mAccount.setTransportUri(transportUri);
        setupFolderNames(domain);
        AccountSetupAccountType.actionSelectAccountType(this, this.mAccount, false);
        finish();
    }

    private void setupFolderNames(String domain) {
        this.mAccount.setDraftsFolderName(getString(R.string.special_mailbox_name_drafts));
        this.mAccount.setTrashFolderName(getString(R.string.special_mailbox_name_trash));
        this.mAccount.setSentFolderName(getString(R.string.special_mailbox_name_sent));
        this.mAccount.setArchiveFolderName(getString(R.string.special_mailbox_name_archive));
        if (domain.endsWith(".yahoo.com")) {
            this.mAccount.setSpamFolderName("Bulk Mail");
        } else {
            this.mAccount.setSpamFolderName(getString(R.string.special_mailbox_name_spam));
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.next /*2131755153*/:
                onNext();
                return;
            case R.id.manual_setup /*2131755434*/:
                onManualSetup();
                return;
            default:
                return;
        }
    }

    private String getXmlAttribute(XmlResourceParser xml, String name) {
        int resId = xml.getAttributeResourceValue(null, name, 0);
        if (resId == 0) {
            return xml.getAttributeValue(null, name);
        }
        return getString(resId);
    }

    private Provider findProviderForDomain(String domain) {
        try {
            XmlResourceParser xml = getResources().getXml(R.xml.providers);
            Provider provider = null;
            while (true) {
                int xmlEventType = xml.next();
                if (xmlEventType == 1) {
                    break;
                } else if (xmlEventType != 2 || !"provider".equals(xml.getName()) || !domain.equalsIgnoreCase(getXmlAttribute(xml, "domain"))) {
                    if (xmlEventType == 2) {
                        if ("incoming".equals(xml.getName()) && provider != null) {
                            provider.incomingUriTemplate = new URI(getXmlAttribute(xml, MessageProvider.MessageColumns.URI));
                            provider.incomingUsernameTemplate = getXmlAttribute(xml, SettingsExporter.USERNAME_ELEMENT);
                        }
                    }
                    if (xmlEventType == 2 && "outgoing".equals(xml.getName()) && provider != null) {
                        provider.outgoingUriTemplate = new URI(getXmlAttribute(xml, MessageProvider.MessageColumns.URI));
                        provider.outgoingUsernameTemplate = getXmlAttribute(xml, SettingsExporter.USERNAME_ELEMENT);
                    } else if (xmlEventType == 3 && "provider".equals(xml.getName()) && provider != null) {
                        return provider;
                    }
                } else {
                    provider = new Provider();
                    provider.id = getXmlAttribute(xml, "id");
                    provider.label = getXmlAttribute(xml, "label");
                    provider.domain = getXmlAttribute(xml, "domain");
                    provider.note = getXmlAttribute(xml, "note");
                }
            }
        } catch (Exception e) {
            Log.e("k9", "Error while trying to load provider settings.", e);
        }
        return null;
    }

    public String[] splitEmail(String email) {
        String[] retParts = new String[2];
        String[] emailParts = email.split("@");
        retParts[0] = emailParts.length > 0 ? emailParts[0] : "";
        retParts[1] = emailParts.length > 1 ? emailParts[1] : "";
        return retParts;
    }
}
