package com.fsck.k9.activity.compose;

import com.fsck.k9.activity.compose.RecipientMvpView;
import com.fsck.k9.activity.compose.RecipientPresenter;
import com.fsck.k9.view.RecipientSelectView;
import java.util.ArrayList;
import java.util.List;

public class ComposeCryptoStatus {
    /* access modifiers changed from: private */
    public boolean allKeysAvailable;
    /* access modifiers changed from: private */
    public boolean allKeysVerified;
    /* access modifiers changed from: private */
    public RecipientPresenter.CryptoMode cryptoMode;
    /* access modifiers changed from: private */
    public RecipientPresenter.CryptoProviderState cryptoProviderState;
    /* access modifiers changed from: private */
    public boolean enablePgpInline;
    /* access modifiers changed from: private */
    public boolean hasRecipients;
    /* access modifiers changed from: private */
    public String[] recipientAddresses;
    /* access modifiers changed from: private */
    public Long selfEncryptKeyId;
    /* access modifiers changed from: private */
    public Long signingKeyId;

    public enum AttachErrorState {
        IS_INLINE
    }

    public enum SendErrorState {
        PROVIDER_ERROR,
        SIGN_KEY_NOT_CONFIGURED,
        PRIVATE_BUT_MISSING_KEYS
    }

    public long[] getEncryptKeyIds() {
        if (this.selfEncryptKeyId == null) {
            return null;
        }
        return new long[]{this.selfEncryptKeyId.longValue()};
    }

    public String[] getRecipientAddresses() {
        return this.recipientAddresses;
    }

    public Long getSigningKeyId() {
        return this.signingKeyId;
    }

    public RecipientMvpView.CryptoStatusDisplayType getCryptoStatusDisplayType() {
        switch (this.cryptoProviderState) {
            case UNCONFIGURED:
                return RecipientMvpView.CryptoStatusDisplayType.UNCONFIGURED;
            case UNINITIALIZED:
                return RecipientMvpView.CryptoStatusDisplayType.UNINITIALIZED;
            case LOST_CONNECTION:
            case ERROR:
                return RecipientMvpView.CryptoStatusDisplayType.ERROR;
            case OK:
                switch (this.cryptoMode) {
                    case PRIVATE:
                        if (!this.hasRecipients) {
                            return RecipientMvpView.CryptoStatusDisplayType.PRIVATE_EMPTY;
                        }
                        if (this.allKeysAvailable && this.allKeysVerified) {
                            return RecipientMvpView.CryptoStatusDisplayType.PRIVATE_TRUSTED;
                        }
                        if (this.allKeysAvailable) {
                            return RecipientMvpView.CryptoStatusDisplayType.PRIVATE_UNTRUSTED;
                        }
                        return RecipientMvpView.CryptoStatusDisplayType.PRIVATE_NOKEY;
                    case OPPORTUNISTIC:
                        if (!this.hasRecipients) {
                            return RecipientMvpView.CryptoStatusDisplayType.OPPORTUNISTIC_EMPTY;
                        }
                        if (this.allKeysAvailable && this.allKeysVerified) {
                            return RecipientMvpView.CryptoStatusDisplayType.OPPORTUNISTIC_TRUSTED;
                        }
                        if (this.allKeysAvailable) {
                            return RecipientMvpView.CryptoStatusDisplayType.OPPORTUNISTIC_UNTRUSTED;
                        }
                        return RecipientMvpView.CryptoStatusDisplayType.OPPORTUNISTIC_NOKEY;
                    case SIGN_ONLY:
                        return RecipientMvpView.CryptoStatusDisplayType.SIGN_ONLY;
                    case DISABLE:
                        return RecipientMvpView.CryptoStatusDisplayType.DISABLED;
                    default:
                        throw new AssertionError("all CryptoModes must be handled!");
                }
            default:
                throw new AssertionError("all CryptoProviderStates must be handled!");
        }
    }

    public boolean shouldUsePgpMessageBuilder() {
        return (this.cryptoProviderState == RecipientPresenter.CryptoProviderState.UNCONFIGURED || this.cryptoMode == RecipientPresenter.CryptoMode.DISABLE) ? false : true;
    }

    public boolean isEncryptionEnabled() {
        return this.cryptoMode == RecipientPresenter.CryptoMode.PRIVATE || this.cryptoMode == RecipientPresenter.CryptoMode.OPPORTUNISTIC;
    }

    public boolean isEncryptionOpportunistic() {
        return this.cryptoMode == RecipientPresenter.CryptoMode.OPPORTUNISTIC;
    }

    public boolean isSigningEnabled() {
        return (this.cryptoMode == RecipientPresenter.CryptoMode.DISABLE || this.signingKeyId == null) ? false : true;
    }

    public boolean isPgpInlineModeEnabled() {
        return this.enablePgpInline;
    }

    public boolean isCryptoDisabled() {
        return this.cryptoMode == RecipientPresenter.CryptoMode.DISABLE;
    }

    public boolean isProviderStateOk() {
        return this.cryptoProviderState == RecipientPresenter.CryptoProviderState.OK;
    }

    public static class ComposeCryptoStatusBuilder {
        private RecipientPresenter.CryptoMode cryptoMode;
        private RecipientPresenter.CryptoProviderState cryptoProviderState;
        private Boolean enablePgpInline;
        private List<RecipientSelectView.Recipient> recipients;
        private Long selfEncryptKeyId;
        private Long signingKeyId;

        public ComposeCryptoStatusBuilder setCryptoProviderState(RecipientPresenter.CryptoProviderState cryptoProviderState2) {
            this.cryptoProviderState = cryptoProviderState2;
            return this;
        }

        public ComposeCryptoStatusBuilder setCryptoMode(RecipientPresenter.CryptoMode cryptoMode2) {
            this.cryptoMode = cryptoMode2;
            return this;
        }

        public ComposeCryptoStatusBuilder setSigningKeyId(long signingKeyId2) {
            this.signingKeyId = Long.valueOf(signingKeyId2);
            return this;
        }

        public ComposeCryptoStatusBuilder setSelfEncryptId(long selfEncryptKeyId2) {
            this.selfEncryptKeyId = Long.valueOf(selfEncryptKeyId2);
            return this;
        }

        public ComposeCryptoStatusBuilder setRecipients(List<RecipientSelectView.Recipient> recipients2) {
            this.recipients = recipients2;
            return this;
        }

        public ComposeCryptoStatusBuilder setEnablePgpInline(boolean cryptoEnableCompat) {
            this.enablePgpInline = Boolean.valueOf(cryptoEnableCompat);
            return this;
        }

        public ComposeCryptoStatus build() {
            boolean hasRecipients;
            if (this.cryptoProviderState == null) {
                throw new AssertionError("cryptoProviderState must be set!");
            } else if (this.cryptoMode == null) {
                throw new AssertionError("crypto mode must be set!");
            } else if (this.recipients == null) {
                throw new AssertionError("recipients must be set!");
            } else if (this.enablePgpInline == null) {
                throw new AssertionError("enablePgpInline must be set!");
            } else {
                ArrayList<String> recipientAddresses = new ArrayList<>();
                boolean allKeysAvailable = true;
                boolean allKeysVerified = true;
                if (!this.recipients.isEmpty()) {
                    hasRecipients = true;
                } else {
                    hasRecipients = false;
                }
                for (RecipientSelectView.Recipient recipient : this.recipients) {
                    RecipientSelectView.RecipientCryptoStatus cryptoStatus = recipient.getCryptoStatus();
                    recipientAddresses.add(recipient.address.getAddress());
                    if (!cryptoStatus.isAvailable()) {
                        allKeysAvailable = false;
                    } else if (cryptoStatus == RecipientSelectView.RecipientCryptoStatus.AVAILABLE_UNTRUSTED) {
                        allKeysVerified = false;
                    }
                }
                ComposeCryptoStatus result = new ComposeCryptoStatus();
                RecipientPresenter.CryptoProviderState unused = result.cryptoProviderState = this.cryptoProviderState;
                RecipientPresenter.CryptoMode unused2 = result.cryptoMode = this.cryptoMode;
                String[] unused3 = result.recipientAddresses = (String[]) recipientAddresses.toArray(new String[0]);
                boolean unused4 = result.allKeysAvailable = allKeysAvailable;
                boolean unused5 = result.allKeysVerified = allKeysVerified;
                boolean unused6 = result.hasRecipients = hasRecipients;
                Long unused7 = result.signingKeyId = this.signingKeyId;
                Long unused8 = result.selfEncryptKeyId = this.selfEncryptKeyId;
                boolean unused9 = result.enablePgpInline = this.enablePgpInline.booleanValue();
                return result;
            }
        }
    }

    public SendErrorState getSendErrorStateOrNull() {
        boolean isSignKeyMissing;
        boolean isPrivateModeAndNotAllKeysAvailable = true;
        if (this.cryptoProviderState != RecipientPresenter.CryptoProviderState.OK) {
            return SendErrorState.PROVIDER_ERROR;
        }
        if (this.signingKeyId == null) {
            isSignKeyMissing = true;
        } else {
            isSignKeyMissing = false;
        }
        if (isSignKeyMissing) {
            return SendErrorState.SIGN_KEY_NOT_CONFIGURED;
        }
        if (this.cryptoMode != RecipientPresenter.CryptoMode.PRIVATE || this.allKeysAvailable) {
            isPrivateModeAndNotAllKeysAvailable = false;
        }
        if (isPrivateModeAndNotAllKeysAvailable) {
            return SendErrorState.PRIVATE_BUT_MISSING_KEYS;
        }
        return null;
    }

    public AttachErrorState getAttachErrorStateOrNull() {
        if (this.cryptoProviderState != RecipientPresenter.CryptoProviderState.UNCONFIGURED && this.enablePgpInline) {
            return AttachErrorState.IS_INLINE;
        }
        return null;
    }
}
