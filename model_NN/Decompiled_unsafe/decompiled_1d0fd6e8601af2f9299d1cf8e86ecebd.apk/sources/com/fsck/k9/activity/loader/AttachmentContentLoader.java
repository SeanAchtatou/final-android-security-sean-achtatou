package com.fsck.k9.activity.loader;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.util.Log;
import com.fsck.k9.K9;
import com.fsck.k9.activity.misc.Attachment;
import de.cketti.safecontentresolver.SafeContentResolverCompat;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.io.IOUtils;

public class AttachmentContentLoader extends AsyncTaskLoader<Attachment> {
    private static final String FILENAME_PREFIX = "attachment";
    private Attachment cachedResultAttachment;
    private final Attachment sourceAttachment;

    public AttachmentContentLoader(Context context, Attachment attachment) {
        super(context);
        if (attachment.state != Attachment.LoadingState.METADATA) {
            throw new IllegalArgumentException("Attachment provided to content loader must be in METADATA state");
        }
        this.sourceAttachment = attachment;
    }

    /* access modifiers changed from: protected */
    public void onStartLoading() {
        if (this.cachedResultAttachment != null) {
            deliverResult(this.sourceAttachment);
        }
        if (takeContentChanged() || this.cachedResultAttachment == null) {
            forceLoad();
        }
    }

    public Attachment loadInBackground() {
        FileOutputStream out;
        Context context = getContext();
        try {
            File file = File.createTempFile("attachment", null, context.getCacheDir());
            file.deleteOnExit();
            if (K9.DEBUG) {
                Log.v("k9", "Saving attachment to " + file.getAbsolutePath());
            }
            InputStream in = SafeContentResolverCompat.newInstance(context).openInputStream(this.sourceAttachment.uri);
            try {
                out = new FileOutputStream(file);
                IOUtils.copy(in, out);
                out.close();
                in.close();
                this.cachedResultAttachment = this.sourceAttachment.deriveWithLoadComplete(file.getAbsolutePath());
                return this.cachedResultAttachment;
            } catch (Throwable th) {
                in.close();
                throw th;
            }
        } catch (IOException e) {
            Log.e("k9", "Error saving attachment!", e);
            this.cachedResultAttachment = this.sourceAttachment.deriveWithLoadCancelled();
            return this.cachedResultAttachment;
        }
    }
}
