package com.fsck.k9.notification;

import android.content.Context;
import android.support.v4.app.NotificationManagerCompat;
import com.fsck.k9.Account;
import com.fsck.k9.mail.Folder;
import yahoo.mail.app.R;

class SyncNotifications {
    private static final boolean NOTIFICATION_LED_WHILE_SYNCING = false;
    private final NotificationActionCreator actionBuilder;
    private final NotificationController controller;

    public SyncNotifications(NotificationController controller2, NotificationActionCreator actionBuilder2) {
        this.controller = controller2;
        this.actionBuilder = actionBuilder2;
    }

    public void showSendingNotification(Account account) {
        Context context = this.controller.getContext();
        String accountName = this.controller.getAccountName(account);
        String title = context.getString(R.string.notification_bg_send_title);
        String tickerText = context.getString(R.string.notification_bg_send_ticker, accountName);
        int notificationId = NotificationIds.getFetchingMailNotificationId(account);
        getNotificationManager().notify(notificationId, this.controller.createNotificationBuilder().setSmallIcon(R.drawable.ic_notify_check_mail).setWhen(System.currentTimeMillis()).setOngoing(true).setTicker(tickerText).setContentTitle(title).setContentText(accountName).setContentIntent(this.actionBuilder.createViewFolderPendingIntent(account, account.getOutboxFolderName(), notificationId)).setVisibility(1).build());
    }

    public void clearSendingNotification(Account account) {
        getNotificationManager().cancel(NotificationIds.getFetchingMailNotificationId(account));
    }

    public void showFetchingMailNotification(Account account, Folder folder) {
        String accountName = account.getDescription();
        String folderName = folder.getName();
        Context context = this.controller.getContext();
        String tickerText = context.getString(R.string.notification_bg_sync_ticker, accountName, folderName);
        String title = context.getString(R.string.notification_bg_sync_title);
        int notificationId = NotificationIds.getFetchingMailNotificationId(account);
        getNotificationManager().notify(notificationId, this.controller.createNotificationBuilder().setSmallIcon(R.drawable.ic_notify_check_mail).setWhen(System.currentTimeMillis()).setOngoing(true).setTicker(tickerText).setContentTitle(title).setContentText(accountName + context.getString(R.string.notification_bg_title_separator) + folderName).setContentIntent(this.actionBuilder.createViewFolderPendingIntent(account, folderName, notificationId)).setVisibility(1).build());
    }

    public void clearFetchingMailNotification(Account account) {
        getNotificationManager().cancel(NotificationIds.getFetchingMailNotificationId(account));
    }

    private NotificationManagerCompat getNotificationManager() {
        return this.controller.getNotificationManager();
    }
}
