package com.fsck.k9.notification;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.internal.view.SupportMenu;
import com.fsck.k9.Account;
import com.fsck.k9.K9;
import com.fsck.k9.activity.setup.AccountSetupIncoming;
import com.fsck.k9.activity.setup.AccountSetupOutgoing;
import yahoo.mail.app.R;

class AuthenticationErrorNotifications {
    private final NotificationController controller;

    public AuthenticationErrorNotifications(NotificationController controller2) {
        this.controller = controller2;
    }

    public void showAuthenticationErrorNotification(Account account, boolean incoming) {
        int notificationId = NotificationIds.getAuthenticationErrorNotificationId(account, incoming);
        Context context = this.controller.getContext();
        PendingIntent editServerSettingsPendingIntent = createContentIntent(context, account, incoming);
        String title = context.getString(R.string.notification_authentication_error_title);
        String text = context.getString(R.string.notification_authentication_error_text, account.getDescription());
        NotificationCompat.Builder builder = this.controller.createNotificationBuilder().setSmallIcon(R.drawable.notification_icon_warning).setWhen(System.currentTimeMillis()).setAutoCancel(true).setTicker(title).setContentTitle(title).setContentText(text).setContentIntent(editServerSettingsPendingIntent).setStyle(new NotificationCompat.BigTextStyle().bigText(text)).setVisibility(1);
        this.controller.configureNotification(builder, null, null, Integer.valueOf((int) SupportMenu.CATEGORY_MASK), 1, true);
        getNotificationManager().notify(notificationId, builder.build());
    }

    public void clearAuthenticationErrorNotification(Account account, boolean incoming) {
        getNotificationManager().cancel(NotificationIds.getAuthenticationErrorNotificationId(account, incoming));
    }

    /* access modifiers changed from: package-private */
    public PendingIntent createContentIntent(Context context, Account account, boolean incoming) {
        Intent editServerSettingsIntent;
        if (incoming) {
            editServerSettingsIntent = AccountSetupIncoming.intentActionEditIncomingSettings(context, account);
        } else {
            editServerSettingsIntent = AccountSetupOutgoing.intentActionEditOutgoingSettings(context, account);
        }
        return PendingIntent.getActivity(context, account.getAccountNumber(), editServerSettingsIntent, K9.MAX_ATTACHMENT_DOWNLOAD_SIZE);
    }

    private NotificationManagerCompat getNotificationManager() {
        return this.controller.getNotificationManager();
    }
}
