package com.fsck.k9.notification;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.text.TextUtils;
import com.fsck.k9.Account;
import com.fsck.k9.K9;
import com.fsck.k9.activity.MessageReference;
import com.fsck.k9.mail.Folder;
import com.fsck.k9.mailstore.LocalMessage;

public class NotificationController {
    static final int NOTIFICATION_LED_BLINK_FAST = 1;
    static final int NOTIFICATION_LED_BLINK_SLOW = 0;
    static final int NOTIFICATION_LED_FAILURE_COLOR = -65536;
    private static final int NOTIFICATION_LED_FAST_OFF_TIME = 100;
    private static final int NOTIFICATION_LED_FAST_ON_TIME = 100;
    private static final int NOTIFICATION_LED_OFF_TIME = 2000;
    private static final int NOTIFICATION_LED_ON_TIME = 500;
    private final AuthenticationErrorNotifications authenticationErrorNotifications = new AuthenticationErrorNotifications(this);
    private final CertificateErrorNotifications certificateErrorNotifications = new CertificateErrorNotifications(this);
    private final Context context;
    private final NewMailNotifications newMailNotifications;
    private final NotificationManagerCompat notificationManager;
    private final SendFailedNotifications sendFailedNotifications;
    private final SyncNotifications syncNotifications;

    public static NotificationController newInstance(Context context2) {
        Context appContext = context2.getApplicationContext();
        return new NotificationController(appContext, NotificationManagerCompat.from(appContext));
    }

    public static boolean platformSupportsExtendedNotifications() {
        return Build.VERSION.SDK_INT >= 16;
    }

    public static boolean platformSupportsLockScreenNotifications() {
        return Build.VERSION.SDK_INT >= 21;
    }

    NotificationController(Context context2, NotificationManagerCompat notificationManager2) {
        this.context = context2;
        this.notificationManager = notificationManager2;
        NotificationActionCreator actionBuilder = new NotificationActionCreator(context2);
        this.syncNotifications = new SyncNotifications(this, actionBuilder);
        this.sendFailedNotifications = new SendFailedNotifications(this, actionBuilder);
        this.newMailNotifications = NewMailNotifications.newInstance(this, actionBuilder);
    }

    public void showCertificateErrorNotification(Account account, boolean incoming) {
        this.certificateErrorNotifications.showCertificateErrorNotification(account, incoming);
    }

    public void clearCertificateErrorNotifications(Account account, boolean incoming) {
        this.certificateErrorNotifications.clearCertificateErrorNotifications(account, incoming);
    }

    public void showAuthenticationErrorNotification(Account account, boolean incoming) {
        this.authenticationErrorNotifications.showAuthenticationErrorNotification(account, incoming);
    }

    public void clearAuthenticationErrorNotification(Account account, boolean incoming) {
        this.authenticationErrorNotifications.clearAuthenticationErrorNotification(account, incoming);
    }

    public void showSendingNotification(Account account) {
        this.syncNotifications.showSendingNotification(account);
    }

    public void clearSendingNotification(Account account) {
        this.syncNotifications.clearSendingNotification(account);
    }

    public void showSendFailedNotification(Account account, Exception exception) {
        this.sendFailedNotifications.showSendFailedNotification(account, exception);
    }

    public void clearSendFailedNotification(Account account) {
        this.sendFailedNotifications.clearSendFailedNotification(account);
    }

    public void showFetchingMailNotification(Account account, Folder folder) {
        this.syncNotifications.showFetchingMailNotification(account, folder);
    }

    public void clearFetchingMailNotification(Account account) {
        this.syncNotifications.clearFetchingMailNotification(account);
    }

    public void addNewMailNotification(Account account, LocalMessage message, int previousUnreadMessageCount) {
        this.newMailNotifications.addNewMailNotification(account, message, previousUnreadMessageCount);
    }

    public void removeNewMailNotification(Account account, MessageReference messageReference) {
        this.newMailNotifications.removeNewMailNotification(account, messageReference);
    }

    public void clearNewMailNotifications(Account account) {
        this.newMailNotifications.clearNewMailNotifications(account);
    }

    /* access modifiers changed from: package-private */
    public void configureNotification(NotificationCompat.Builder builder, String ringtone, long[] vibrationPattern, Integer ledColor, int ledSpeed, boolean ringAndVibrate) {
        int ledOnMS;
        int ledOffMS;
        if (!K9.isQuietTime()) {
            if (ringAndVibrate) {
                if (ringtone != null && !TextUtils.isEmpty(ringtone)) {
                    builder.setSound(Uri.parse(ringtone));
                }
                if (vibrationPattern != null) {
                    builder.setVibrate(vibrationPattern);
                }
            }
            if (ledColor != null) {
                if (ledSpeed == 0) {
                    ledOnMS = NOTIFICATION_LED_ON_TIME;
                    ledOffMS = NOTIFICATION_LED_OFF_TIME;
                } else {
                    ledOnMS = 100;
                    ledOffMS = 100;
                }
                builder.setLights(ledColor.intValue(), ledOnMS, ledOffMS);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public String getAccountName(Account account) {
        String accountDescription = account.getDescription();
        return TextUtils.isEmpty(accountDescription) ? account.getEmail() : accountDescription;
    }

    /* access modifiers changed from: package-private */
    public Context getContext() {
        return this.context;
    }

    /* access modifiers changed from: package-private */
    public NotificationManagerCompat getNotificationManager() {
        return this.notificationManager;
    }

    /* access modifiers changed from: package-private */
    public NotificationCompat.Builder createNotificationBuilder() {
        return new NotificationCompat.Builder(this.context);
    }
}
