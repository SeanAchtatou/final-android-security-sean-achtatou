package com.fsck.k9.notification;

import android.app.KeyguardManager;
import android.app.Notification;
import android.support.v4.app.NotificationCompat;
import com.fsck.k9.Account;
import com.fsck.k9.K9;
import com.fsck.k9.NotificationSetting;
import yahoo.mail.app.R;

class DeviceNotifications extends BaseNotifications {
    private final LockScreenNotification lockScreenNotification;
    private final WearNotifications wearNotifications;

    DeviceNotifications(NotificationController controller, NotificationActionCreator actionCreator, LockScreenNotification lockScreenNotification2, WearNotifications wearNotifications2) {
        super(controller, actionCreator);
        this.wearNotifications = wearNotifications2;
        this.lockScreenNotification = lockScreenNotification2;
    }

    public static DeviceNotifications newInstance(NotificationController controller, NotificationActionCreator actionCreator, WearNotifications wearNotifications2) {
        return new DeviceNotifications(controller, actionCreator, LockScreenNotification.newInstance(controller), wearNotifications2);
    }

    public Notification buildSummaryNotification(Account account, NotificationData notificationData, boolean silent) {
        NotificationCompat.Builder builder;
        String str;
        long[] jArr;
        Integer num = null;
        int unreadMessageCount = notificationData.getUnreadMessageCount();
        if (isPrivacyModeActive() || !NotificationController.platformSupportsExtendedNotifications()) {
            builder = createSimpleSummaryNotification(account, unreadMessageCount);
        } else if (notificationData.isSingleMessageNotification()) {
            builder = createBigTextStyleSummaryNotification(account, notificationData.getHolderForLatestNotification());
        } else {
            builder = createInboxStyleSummaryNotification(account, notificationData, unreadMessageCount);
        }
        if (notificationData.containsStarredMessages()) {
            builder.setPriority(1);
        }
        builder.setDeleteIntent(this.actionCreator.createDismissAllMessagesPendingIntent(account, NotificationIds.getNewMailSummaryNotificationId(account)));
        this.lockScreenNotification.configureLockScreenNotification(builder, notificationData);
        boolean ringAndVibrate = false;
        if (!silent && !account.isRingNotified()) {
            account.setRingNotified(true);
            ringAndVibrate = true;
        }
        NotificationSetting notificationSetting = account.getNotificationSetting();
        NotificationController notificationController = this.controller;
        if (notificationSetting.shouldRing()) {
            str = notificationSetting.getRingtone();
        } else {
            str = null;
        }
        if (notificationSetting.shouldVibrate()) {
            jArr = notificationSetting.getVibration();
        } else {
            jArr = null;
        }
        if (notificationSetting.isLed()) {
            num = Integer.valueOf(notificationSetting.getLedColor());
        }
        notificationController.configureNotification(builder, str, jArr, num, 0, ringAndVibrate);
        return builder.build();
    }

    private NotificationCompat.Builder createSimpleSummaryNotification(Account account, int unreadMessageCount) {
        String accountName = this.controller.getAccountName(account);
        CharSequence newMailText = this.context.getString(R.string.notification_new_title);
        String unreadMessageCountText = this.context.getString(R.string.notification_new_one_account_fmt, Integer.valueOf(unreadMessageCount), accountName);
        return createAndInitializeNotificationBuilder(account).setNumber(unreadMessageCount).setTicker(newMailText).setContentTitle(unreadMessageCountText).setContentText(newMailText).setContentIntent(this.actionCreator.createViewFolderListPendingIntent(account, NotificationIds.getNewMailSummaryNotificationId(account)));
    }

    private NotificationCompat.Builder createBigTextStyleSummaryNotification(Account account, NotificationHolder holder) {
        int notificationId = NotificationIds.getNewMailSummaryNotificationId(account);
        NotificationCompat.Builder builder = createBigTextStyleNotification(account, holder, notificationId).setGroupSummary(true);
        NotificationContent content = holder.content;
        addReplyAction(builder, content, notificationId);
        addMarkAsReadAction(builder, content, notificationId);
        addDeleteAction(builder, content, notificationId);
        return builder;
    }

    private NotificationCompat.Builder createInboxStyleSummaryNotification(Account account, NotificationData notificationData, int unreadMessageCount) {
        String summary;
        NotificationHolder latestNotification = notificationData.getHolderForLatestNotification();
        int newMessagesCount = notificationData.getNewMessagesCount();
        String accountName = this.controller.getAccountName(account);
        String title = this.context.getResources().getQuantityString(R.plurals.notification_new_messages_title, newMessagesCount, Integer.valueOf(newMessagesCount));
        if (notificationData.hasAdditionalMessages()) {
            summary = this.context.getString(R.string.notification_additional_messages, Integer.valueOf(notificationData.getAdditionalMessagesCount()), accountName);
        } else {
            summary = accountName;
        }
        NotificationCompat.Builder builder = createAndInitializeNotificationBuilder(account).setNumber(unreadMessageCount).setTicker(latestNotification.content.summary).setGroup("newMailNotifications").setGroupSummary(true).setContentTitle(title).setSubText(accountName);
        NotificationCompat.InboxStyle style = createInboxStyle(builder).setBigContentTitle(title).setSummaryText(summary);
        for (NotificationContent content : notificationData.getContentForSummaryNotification()) {
            style.addLine(content.summary);
        }
        builder.setStyle(style);
        addMarkAllAsReadAction(builder, notificationData);
        addDeleteAllAction(builder, notificationData);
        this.wearNotifications.addSummaryActions(builder, notificationData);
        int notificationId = NotificationIds.getNewMailSummaryNotificationId(account);
        builder.setContentIntent(this.actionCreator.createViewMessagesPendingIntent(account, notificationData.getAllMessageReferences(), notificationId));
        return builder;
    }

    private void addMarkAsReadAction(NotificationCompat.Builder builder, NotificationContent content, int notificationId) {
        builder.addAction(getMarkAsReadActionIcon(), this.context.getString(R.string.notification_action_mark_as_read), this.actionCreator.createMarkMessageAsReadPendingIntent(content.messageReference, notificationId));
    }

    private void addMarkAllAsReadAction(NotificationCompat.Builder builder, NotificationData notificationData) {
        int icon = getMarkAsReadActionIcon();
        String title = this.context.getString(R.string.notification_action_mark_as_read);
        Account account = notificationData.getAccount();
        builder.addAction(icon, title, this.actionCreator.createMarkAllAsReadPendingIntent(account, notificationData.getAllMessageReferences(), NotificationIds.getNewMailSummaryNotificationId(account)));
    }

    private void addDeleteAllAction(NotificationCompat.Builder builder, NotificationData notificationData) {
        if (K9.getNotificationQuickDeleteBehaviour() == K9.NotificationQuickDelete.ALWAYS) {
            int icon = getDeleteActionIcon();
            String title = this.context.getString(R.string.notification_action_delete);
            Account account = notificationData.getAccount();
            int notificationId = NotificationIds.getNewMailSummaryNotificationId(account);
            builder.addAction(icon, title, this.actionCreator.createDeleteAllPendingIntent(account, notificationData.getAllMessageReferences(), notificationId));
        }
    }

    private void addDeleteAction(NotificationCompat.Builder builder, NotificationContent content, int notificationId) {
        if (isDeleteActionEnabled()) {
            builder.addAction(getDeleteActionIcon(), this.context.getString(R.string.notification_action_delete), this.actionCreator.createDeleteMessagePendingIntent(content.messageReference, notificationId));
        }
    }

    private void addReplyAction(NotificationCompat.Builder builder, NotificationContent content, int notificationId) {
        builder.addAction(getReplyActionIcon(), this.context.getString(R.string.notification_action_reply), this.actionCreator.createReplyPendingIntent(content.messageReference, notificationId));
    }

    private boolean isPrivacyModeActive() {
        boolean privacyModeAlwaysEnabled;
        boolean privacyModeEnabledWhenLocked;
        KeyguardManager keyguardService = (KeyguardManager) this.context.getSystemService("keyguard");
        if (K9.getNotificationHideSubject() == K9.NotificationHideSubject.ALWAYS) {
            privacyModeAlwaysEnabled = true;
        } else {
            privacyModeAlwaysEnabled = false;
        }
        if (K9.getNotificationHideSubject() == K9.NotificationHideSubject.WHEN_LOCKED) {
            privacyModeEnabledWhenLocked = true;
        } else {
            privacyModeEnabledWhenLocked = false;
        }
        boolean screenLocked = keyguardService.inKeyguardRestrictedInputMode();
        if (privacyModeAlwaysEnabled || (privacyModeEnabledWhenLocked && screenLocked)) {
            return true;
        }
        return false;
    }

    private int getMarkAsReadActionIcon() {
        return R.drawable.notification_action_mark_as_read;
    }

    private int getDeleteActionIcon() {
        return R.drawable.notification_action_delete;
    }

    private int getReplyActionIcon() {
        return R.drawable.notification_action_reply;
    }

    /* access modifiers changed from: protected */
    public NotificationCompat.InboxStyle createInboxStyle(NotificationCompat.Builder builder) {
        return new NotificationCompat.InboxStyle(builder);
    }
}
