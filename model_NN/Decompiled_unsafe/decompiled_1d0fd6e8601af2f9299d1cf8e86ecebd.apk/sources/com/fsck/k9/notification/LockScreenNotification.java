package com.fsck.k9.notification;

import android.app.Notification;
import android.content.Context;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import com.fsck.k9.Account;
import com.fsck.k9.K9;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import yahoo.mail.app.R;

class LockScreenNotification {
    static final int MAX_NUMBER_OF_SENDERS_IN_LOCK_SCREEN_NOTIFICATION = 5;
    private final Context context;
    private final NotificationController controller;

    LockScreenNotification(NotificationController controller2) {
        this.context = controller2.getContext();
        this.controller = controller2;
    }

    public static LockScreenNotification newInstance(NotificationController controller2) {
        return new LockScreenNotification(controller2);
    }

    public void configureLockScreenNotification(NotificationCompat.Builder builder, NotificationData notificationData) {
        if (NotificationController.platformSupportsLockScreenNotifications()) {
            switch (K9.getLockScreenNotificationVisibility()) {
                case NOTHING:
                    builder.setVisibility(-1);
                    return;
                case APP_NAME:
                    builder.setVisibility(0);
                    return;
                case EVERYTHING:
                    builder.setVisibility(1);
                    return;
                case SENDERS:
                    builder.setPublicVersion(createPublicNotificationWithSenderList(notificationData));
                    return;
                case MESSAGE_COUNT:
                    builder.setPublicVersion(createPublicNotificationWithNewMessagesCount(notificationData));
                    return;
                default:
                    return;
            }
        }
    }

    private Notification createPublicNotificationWithSenderList(NotificationData notificationData) {
        NotificationCompat.Builder builder = createPublicNotification(notificationData);
        if (notificationData.getNewMessagesCount() == 1) {
            builder.setContentText(notificationData.getHolderForLatestNotification().content.sender);
        } else {
            builder.setContentText(createCommaSeparatedListOfSenders(notificationData.getContentForSummaryNotification()));
        }
        return builder.build();
    }

    private Notification createPublicNotificationWithNewMessagesCount(NotificationData notificationData) {
        NotificationCompat.Builder builder = createPublicNotification(notificationData);
        builder.setContentText(this.controller.getAccountName(notificationData.getAccount()));
        return builder.build();
    }

    private NotificationCompat.Builder createPublicNotification(NotificationData notificationData) {
        Account account = notificationData.getAccount();
        int newMessages = notificationData.getNewMessagesCount();
        int unreadCount = notificationData.getUnreadMessageCount();
        return this.controller.createNotificationBuilder().setSmallIcon(R.drawable.notification_icon_new_mail).setColor(account.getChipColor()).setNumber(unreadCount).setContentTitle(this.context.getResources().getQuantityString(R.plurals.notification_new_messages_title, newMessages, Integer.valueOf(newMessages)));
    }

    /* access modifiers changed from: package-private */
    public String createCommaSeparatedListOfSenders(List<NotificationContent> contents) {
        Set<CharSequence> senders = new LinkedHashSet<>(5);
        for (NotificationContent content : contents) {
            senders.add(content.sender);
            if (senders.size() == 5) {
                break;
            }
        }
        return TextUtils.join(", ", senders);
    }
}
