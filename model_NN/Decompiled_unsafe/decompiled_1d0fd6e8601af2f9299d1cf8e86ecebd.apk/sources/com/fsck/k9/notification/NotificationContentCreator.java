package com.fsck.k9.notification;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.TextAppearanceSpan;
import com.fsck.k9.Account;
import com.fsck.k9.K9;
import com.fsck.k9.activity.MessageReference;
import com.fsck.k9.helper.Contacts;
import com.fsck.k9.helper.MessageHelper;
import com.fsck.k9.mail.Address;
import com.fsck.k9.mail.Flag;
import com.fsck.k9.mail.Message;
import com.fsck.k9.mailstore.LocalMessage;
import com.fsck.k9.message.extractors.PreviewResult;
import yahoo.mail.app.R;

class NotificationContentCreator {
    private final Context context;
    private TextAppearanceSpan emphasizedSpan;

    public NotificationContentCreator(Context context2) {
        this.context = context2;
    }

    public NotificationContent createFromMessage(Account account, LocalMessage message) {
        MessageReference messageReference = message.makeMessageReference();
        String sender = getMessageSender(account, message);
        String displaySender = getMessageSenderForDisplay(sender);
        String subject = getMessageSubject(message);
        return new NotificationContent(messageReference, displaySender, subject, getMessagePreview(message), buildMessageSummary(sender, subject), message.isSet(Flag.FLAGGED));
    }

    private CharSequence getMessagePreview(LocalMessage message) {
        boolean isSnippetPresent;
        String subject = message.getSubject();
        String snippet = getPreview(message);
        boolean isSubjectEmpty = TextUtils.isEmpty(subject);
        if (snippet != null) {
            isSnippetPresent = true;
        } else {
            isSnippetPresent = false;
        }
        if (isSubjectEmpty && isSnippetPresent) {
            return snippet;
        }
        String displaySubject = getMessageSubject(message);
        SpannableStringBuilder preview = new SpannableStringBuilder();
        preview.append((CharSequence) displaySubject);
        if (isSnippetPresent) {
            preview.append(10);
            preview.append((CharSequence) snippet);
        }
        preview.setSpan(getEmphasizedSpan(), 0, displaySubject.length(), 0);
        return preview;
    }

    private String getPreview(LocalMessage message) {
        PreviewResult.PreviewType previewType = message.getPreviewType();
        switch (previewType) {
            case NONE:
            case ERROR:
                return null;
            case TEXT:
                return message.getPreview();
            case ENCRYPTED:
                return this.context.getString(R.string.preview_encrypted);
            default:
                throw new AssertionError("Unknown preview type: " + previewType);
        }
    }

    private CharSequence buildMessageSummary(String sender, String subject) {
        if (sender == null) {
            return subject;
        }
        SpannableStringBuilder summary = new SpannableStringBuilder();
        summary.append((CharSequence) sender);
        summary.append((CharSequence) " ");
        summary.append((CharSequence) subject);
        summary.setSpan(getEmphasizedSpan(), 0, sender.length(), 0);
        return summary;
    }

    private String getMessageSubject(Message message) {
        String subject = message.getSubject();
        return !TextUtils.isEmpty(subject) ? subject : this.context.getString(R.string.general_no_subject);
    }

    private String getMessageSender(Account account, Message message) {
        Contacts contacts;
        Address[] recipients;
        boolean isSelf = false;
        if (K9.showContactName()) {
            contacts = Contacts.getInstance(this.context);
        } else {
            contacts = null;
        }
        Address[] fromAddresses = message.getFrom();
        if (fromAddresses != null && !(isSelf = account.isAnIdentity(fromAddresses)) && fromAddresses.length > 0) {
            return MessageHelper.toFriendly(fromAddresses[0], contacts).toString();
        }
        if (!isSelf || (recipients = message.getRecipients(Message.RecipientType.TO)) == null || recipients.length <= 0) {
            return null;
        }
        return this.context.getString(R.string.message_to_fmt, MessageHelper.toFriendly(recipients[0], contacts).toString());
    }

    private String getMessageSenderForDisplay(String sender) {
        return sender != null ? sender : this.context.getString(R.string.general_no_sender);
    }

    private TextAppearanceSpan getEmphasizedSpan() {
        if (this.emphasizedSpan == null) {
            this.emphasizedSpan = new TextAppearanceSpan(this.context, com.fsck.k9.R.style.TextAppearance_StatusBar_EventContent_Emphasized);
        }
        return this.emphasizedSpan;
    }
}
