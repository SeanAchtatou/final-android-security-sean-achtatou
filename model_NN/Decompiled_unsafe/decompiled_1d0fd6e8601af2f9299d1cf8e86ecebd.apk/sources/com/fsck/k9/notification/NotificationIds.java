package com.fsck.k9.notification;

import com.fsck.k9.Account;

class NotificationIds {
    private static final int NUMBER_OF_DEVICE_NOTIFICATIONS = 7;
    private static final int NUMBER_OF_NOTIFICATIONS_PER_ACCOUNT = 15;
    private static final int NUMBER_OF_STACKED_NOTIFICATIONS = 8;
    private static final int OFFSET_AUTHENTICATION_ERROR_INCOMING = 3;
    private static final int OFFSET_AUTHENTICATION_ERROR_OUTGOING = 4;
    private static final int OFFSET_CERTIFICATE_ERROR_INCOMING = 1;
    private static final int OFFSET_CERTIFICATE_ERROR_OUTGOING = 2;
    private static final int OFFSET_FETCHING_MAIL = 5;
    private static final int OFFSET_NEW_MAIL_STACKED = 7;
    private static final int OFFSET_NEW_MAIL_SUMMARY = 6;
    private static final int OFFSET_SEND_FAILED_NOTIFICATION = 0;

    NotificationIds() {
    }

    public static int getNewMailSummaryNotificationId(Account account) {
        return getBaseNotificationId(account) + 6;
    }

    public static int getNewMailStackedNotificationId(Account account, int index) {
        if (index >= 0 && index < 8) {
            return getBaseNotificationId(account) + 7 + index;
        }
        throw new IndexOutOfBoundsException("Invalid value: " + index);
    }

    public static int getFetchingMailNotificationId(Account account) {
        return getBaseNotificationId(account) + 5;
    }

    public static int getSendFailedNotificationId(Account account) {
        return getBaseNotificationId(account) + 0;
    }

    public static int getCertificateErrorNotificationId(Account account, boolean incoming) {
        return getBaseNotificationId(account) + (incoming ? 1 : 2);
    }

    public static int getAuthenticationErrorNotificationId(Account account, boolean incoming) {
        return getBaseNotificationId(account) + (incoming ? 3 : 4);
    }

    private static int getBaseNotificationId(Account account) {
        return account.getAccountNumber() * 15;
    }
}
