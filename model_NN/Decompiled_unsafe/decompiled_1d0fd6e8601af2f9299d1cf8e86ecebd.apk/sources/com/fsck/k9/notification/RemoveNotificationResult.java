package com.fsck.k9.notification;

class RemoveNotificationResult {
    private final NotificationHolder notificationHolder;
    private final int notificationId;
    private final boolean unknownNotification;

    private RemoveNotificationResult(NotificationHolder notificationHolder2, int notificationId2, boolean unknownNotification2) {
        this.notificationHolder = notificationHolder2;
        this.notificationId = notificationId2;
        this.unknownNotification = unknownNotification2;
    }

    public static RemoveNotificationResult createNotification(NotificationHolder notificationHolder2) {
        return new RemoveNotificationResult(notificationHolder2, notificationHolder2.notificationId, false);
    }

    public static RemoveNotificationResult cancelNotification(int notificationId2) {
        return new RemoveNotificationResult(null, notificationId2, false);
    }

    public static RemoveNotificationResult unknownNotification() {
        return new RemoveNotificationResult(null, 0, true);
    }

    public boolean shouldCreateNotification() {
        return this.notificationHolder != null;
    }

    public int getNotificationId() {
        if (!isUnknownNotification()) {
            return this.notificationId;
        }
        throw new IllegalStateException("getNotificationId() can only be called when isUnknownNotification() returns false");
    }

    public boolean isUnknownNotification() {
        return this.unknownNotification;
    }

    public NotificationHolder getNotificationHolder() {
        if (shouldCreateNotification()) {
            return this.notificationHolder;
        }
        throw new IllegalStateException("getNotificationHolder() can only be called when shouldCreateNotification() returns true");
    }
}
