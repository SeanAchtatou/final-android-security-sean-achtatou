package com.fsck.k9.notification;

class AddNotificationResult {
    private final boolean cancelNotificationBeforeReuse;
    private final NotificationHolder notificationHolder;

    private AddNotificationResult(NotificationHolder notificationHolder2, boolean cancelNotificationBeforeReuse2) {
        this.notificationHolder = notificationHolder2;
        this.cancelNotificationBeforeReuse = cancelNotificationBeforeReuse2;
    }

    public static AddNotificationResult newNotification(NotificationHolder notificationHolder2) {
        return new AddNotificationResult(notificationHolder2, false);
    }

    public static AddNotificationResult replaceNotification(NotificationHolder notificationHolder2) {
        return new AddNotificationResult(notificationHolder2, true);
    }

    public boolean shouldCancelNotification() {
        return this.cancelNotificationBeforeReuse;
    }

    public int getNotificationId() {
        if (shouldCancelNotification()) {
            return this.notificationHolder.notificationId;
        }
        throw new IllegalStateException("getNotificationId() can only be called when shouldCancelNotification() returns true");
    }

    public NotificationHolder getNotificationHolder() {
        return this.notificationHolder;
    }
}
