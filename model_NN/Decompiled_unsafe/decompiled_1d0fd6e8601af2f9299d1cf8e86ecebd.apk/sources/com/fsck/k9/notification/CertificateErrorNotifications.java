package com.fsck.k9.notification;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.internal.view.SupportMenu;
import com.fsck.k9.Account;
import com.fsck.k9.K9;
import com.fsck.k9.activity.setup.AccountSetupIncoming;
import com.fsck.k9.activity.setup.AccountSetupOutgoing;
import yahoo.mail.app.R;

class CertificateErrorNotifications {
    private final NotificationController controller;

    public CertificateErrorNotifications(NotificationController controller2) {
        this.controller = controller2;
    }

    public void showCertificateErrorNotification(Account account, boolean incoming) {
        int notificationId = NotificationIds.getCertificateErrorNotificationId(account, incoming);
        Context context = this.controller.getContext();
        PendingIntent editServerSettingsPendingIntent = createContentIntent(context, account, incoming);
        String title = context.getString(R.string.notification_certificate_error_title, account.getDescription());
        NotificationCompat.Builder builder = this.controller.createNotificationBuilder().setSmallIcon(getCertificateErrorNotificationIcon()).setWhen(System.currentTimeMillis()).setAutoCancel(true).setTicker(title).setContentTitle(title).setContentText(context.getString(R.string.notification_certificate_error_text)).setContentIntent(editServerSettingsPendingIntent).setVisibility(1);
        this.controller.configureNotification(builder, null, null, Integer.valueOf((int) SupportMenu.CATEGORY_MASK), 1, true);
        getNotificationManager().notify(notificationId, builder.build());
    }

    public void clearCertificateErrorNotifications(Account account, boolean incoming) {
        getNotificationManager().cancel(NotificationIds.getCertificateErrorNotificationId(account, incoming));
    }

    /* access modifiers changed from: package-private */
    public PendingIntent createContentIntent(Context context, Account account, boolean incoming) {
        Intent editServerSettingsIntent;
        if (incoming) {
            editServerSettingsIntent = AccountSetupIncoming.intentActionEditIncomingSettings(context, account);
        } else {
            editServerSettingsIntent = AccountSetupOutgoing.intentActionEditOutgoingSettings(context, account);
        }
        return PendingIntent.getActivity(context, account.getAccountNumber(), editServerSettingsIntent, K9.MAX_ATTACHMENT_DOWNLOAD_SIZE);
    }

    private int getCertificateErrorNotificationIcon() {
        return R.drawable.notification_icon_new_mail;
    }

    private NotificationManagerCompat getNotificationManager() {
        return this.controller.getNotificationManager();
    }
}
