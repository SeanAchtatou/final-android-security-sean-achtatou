package com.fsck.k9.provider;

import android.annotation.TargetApi;
import android.app.Application;
import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.CharArrayBuffer;
import android.database.ContentObserver;
import android.database.CrossProcessCursor;
import android.database.Cursor;
import android.database.CursorWindow;
import android.database.DataSetObserver;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.util.Log;
import com.fsck.k9.Account;
import com.fsck.k9.AccountStats;
import com.fsck.k9.K9;
import com.fsck.k9.Preferences;
import com.fsck.k9.activity.FolderInfoHolder;
import com.fsck.k9.activity.MessageInfoHolder;
import com.fsck.k9.activity.MessageReference;
import com.fsck.k9.controller.MessagingController;
import com.fsck.k9.controller.MessagingListener;
import com.fsck.k9.helper.MessageHelper;
import com.fsck.k9.mail.Flag;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mailstore.LocalFolder;
import com.fsck.k9.mailstore.LocalMessage;
import com.fsck.k9.preferences.SettingsExporter;
import com.fsck.k9.provider.AttachmentProvider;
import com.fsck.k9.search.SearchAccount;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.Semaphore;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class MessageProvider extends ContentProvider {
    public static final String AUTHORITY = "yahoo.mail.app.messageprovider";
    public static final Uri CONTENT_URI = Uri.parse("content://yahoo.mail.app.messageprovider");
    /* access modifiers changed from: private */
    public static final String[] DEFAULT_MESSAGE_PROJECTION = {AttachmentProvider.AttachmentProviderColumns._ID, "date", MessageColumns.SENDER, "subject", "preview", "account", MessageColumns.URI, MessageColumns.DELETE_URI, MessageColumns.SENDER_ADDRESS};
    /* access modifiers changed from: private */
    public MessageHelper mMessageHelper;
    private List<QueryHandler> mQueryHandlers = new ArrayList();
    ScheduledExecutorService mScheduledPool = Executors.newScheduledThreadPool(1);
    Semaphore mSemaphore = new Semaphore(1);
    private UriMatcher mUriMatcher = new UriMatcher(-1);

    public interface FieldExtractor<T, K> {
        K getField(T t);
    }

    public interface MessageColumns extends BaseColumns {
        public static final String ACCOUNT = "account";
        public static final String ACCOUNT_COLOR = "accountColor";
        public static final String ACCOUNT_NUMBER = "accountNumber";
        public static final String DELETE_URI = "delUri";
        public static final String HAS_ATTACHMENTS = "hasAttachments";
        public static final String HAS_STAR = "hasStar";
        @Deprecated
        public static final String INCREMENT = "id";
        public static final String PREVIEW = "preview";
        public static final String SENDER = "sender";
        public static final String SENDER_ADDRESS = "senderAddress";
        public static final String SEND_DATE = "date";
        public static final String SUBJECT = "subject";
        public static final String UNREAD = "unread";
        public static final String URI = "uri";
    }

    protected interface QueryHandler {
        String getPath();

        Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) throws Exception;
    }

    public static class IdExtractor implements FieldExtractor<MessageInfoHolder, Long> {
        public Long getField(MessageInfoHolder source) {
            return Long.valueOf(source.message.getId());
        }
    }

    public static class CountExtractor<T> implements FieldExtractor<T, Integer> {
        private Integer mCount;

        public CountExtractor(int count) {
            this.mCount = Integer.valueOf(count);
        }

        public Integer getField(T t) {
            return this.mCount;
        }
    }

    public static class SubjectExtractor implements FieldExtractor<MessageInfoHolder, String> {
        public String getField(MessageInfoHolder source) {
            return source.message.getSubject();
        }
    }

    public static class SendDateExtractor implements FieldExtractor<MessageInfoHolder, Long> {
        public Long getField(MessageInfoHolder source) {
            return Long.valueOf(source.message.getSentDate().getTime());
        }
    }

    public static class PreviewExtractor implements FieldExtractor<MessageInfoHolder, String> {
        public String getField(MessageInfoHolder source) {
            return source.message.getPreview();
        }
    }

    public static class UriExtractor implements FieldExtractor<MessageInfoHolder, String> {
        public String getField(MessageInfoHolder source) {
            return source.uri;
        }
    }

    public static class DeleteUriExtractor implements FieldExtractor<MessageInfoHolder, String> {
        public String getField(MessageInfoHolder source) {
            LocalMessage message = source.message;
            return MessageProvider.CONTENT_URI.buildUpon().appendPath("delete_message").appendPath(Integer.toString(message.getAccount().getAccountNumber())).appendPath(message.getFolder().getName()).appendPath(message.getUid()).build().toString();
        }
    }

    public static class SenderExtractor implements FieldExtractor<MessageInfoHolder, CharSequence> {
        public CharSequence getField(MessageInfoHolder source) {
            return source.sender;
        }
    }

    public static class SenderAddressExtractor implements FieldExtractor<MessageInfoHolder, String> {
        public String getField(MessageInfoHolder source) {
            return source.senderAddress;
        }
    }

    public static class AccountExtractor implements FieldExtractor<MessageInfoHolder, String> {
        public String getField(MessageInfoHolder source) {
            return source.message.getAccount().getDescription();
        }
    }

    public static class AccountColorExtractor implements FieldExtractor<MessageInfoHolder, Integer> {
        public Integer getField(MessageInfoHolder source) {
            return Integer.valueOf(source.message.getAccount().getChipColor());
        }
    }

    public static class AccountNumberExtractor implements FieldExtractor<MessageInfoHolder, Integer> {
        public Integer getField(MessageInfoHolder source) {
            return Integer.valueOf(source.message.getAccount().getAccountNumber());
        }
    }

    public static class HasAttachmentsExtractor implements FieldExtractor<MessageInfoHolder, Boolean> {
        public Boolean getField(MessageInfoHolder source) {
            return Boolean.valueOf(source.message.hasAttachments());
        }
    }

    public static class HasStarExtractor implements FieldExtractor<MessageInfoHolder, Boolean> {
        public Boolean getField(MessageInfoHolder source) {
            return Boolean.valueOf(source.message.isSet(Flag.FLAGGED));
        }
    }

    public static class UnreadExtractor implements FieldExtractor<MessageInfoHolder, Boolean> {
        public Boolean getField(MessageInfoHolder source) {
            return Boolean.valueOf(!source.read);
        }
    }

    @Deprecated
    public static class IncrementExtractor implements FieldExtractor<MessageInfoHolder, Integer> {
        private int count = 0;

        public Integer getField(MessageInfoHolder source) {
            int i = this.count;
            this.count = i + 1;
            return Integer.valueOf(i);
        }
    }

    protected class MessagesQueryHandler implements QueryHandler {
        protected MessagesQueryHandler() {
        }

        public String getPath() {
            return "inbox_messages/";
        }

        public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) throws Exception {
            return getMessages(projection);
        }

        /* access modifiers changed from: protected */
        public MatrixCursor getMessages(String[] projection) throws InterruptedException {
            String[] projectionToUse;
            BlockingQueue<List<MessageInfoHolder>> queue = new SynchronousQueue<>();
            MessagingController.getInstance(MessageProvider.this.getContext()).searchLocalMessages(SearchAccount.createUnifiedInboxAccount(MessageProvider.this.getContext()).getRelatedSearch(), new MesssageInfoHolderRetrieverListener(queue));
            List<MessageInfoHolder> holders = queue.take();
            Collections.sort(holders, new ReverseDateComparator());
            if (projection == null) {
                projectionToUse = MessageProvider.DEFAULT_MESSAGE_PROJECTION;
            } else {
                projectionToUse = projection;
            }
            LinkedHashMap<String, FieldExtractor<MessageInfoHolder, ?>> extractors = resolveMessageExtractors(projectionToUse, holders.size());
            int fieldCount = extractors.size();
            MatrixCursor cursor = new MatrixCursor((String[]) extractors.keySet().toArray(new String[fieldCount]));
            for (MessageInfoHolder holder : holders) {
                Object[] o = new Object[fieldCount];
                int i = 0;
                for (FieldExtractor<MessageInfoHolder, ?> extractor : extractors.values()) {
                    o[i] = extractor.getField(holder);
                    i++;
                }
                cursor.addRow(o);
            }
            return cursor;
        }

        /* access modifiers changed from: protected */
        public LinkedHashMap<String, FieldExtractor<MessageInfoHolder, ?>> resolveMessageExtractors(String[] projection, int count) {
            LinkedHashMap<String, FieldExtractor<MessageInfoHolder, ?>> extractors = new LinkedHashMap<>();
            for (String field : projection) {
                if (!extractors.containsKey(field)) {
                    if (AttachmentProvider.AttachmentProviderColumns._ID.equals(field)) {
                        extractors.put(field, new IdExtractor());
                    } else if ("_count".equals(field)) {
                        extractors.put(field, new CountExtractor(count));
                    } else if ("subject".equals(field)) {
                        extractors.put(field, new SubjectExtractor());
                    } else if (MessageColumns.SENDER.equals(field)) {
                        extractors.put(field, new SenderExtractor());
                    } else if (MessageColumns.SENDER_ADDRESS.equals(field)) {
                        extractors.put(field, new SenderAddressExtractor());
                    } else if ("date".equals(field)) {
                        extractors.put(field, new SendDateExtractor());
                    } else if ("preview".equals(field)) {
                        extractors.put(field, new PreviewExtractor());
                    } else if (MessageColumns.URI.equals(field)) {
                        extractors.put(field, new UriExtractor());
                    } else if (MessageColumns.DELETE_URI.equals(field)) {
                        extractors.put(field, new DeleteUriExtractor());
                    } else if (MessageColumns.UNREAD.equals(field)) {
                        extractors.put(field, new UnreadExtractor());
                    } else if ("account".equals(field)) {
                        extractors.put(field, new AccountExtractor());
                    } else if (MessageColumns.ACCOUNT_COLOR.equals(field)) {
                        extractors.put(field, new AccountColorExtractor());
                    } else if (MessageColumns.ACCOUNT_NUMBER.equals(field)) {
                        extractors.put(field, new AccountNumberExtractor());
                    } else if (MessageColumns.HAS_ATTACHMENTS.equals(field)) {
                        extractors.put(field, new HasAttachmentsExtractor());
                    } else if (MessageColumns.HAS_STAR.equals(field)) {
                        extractors.put(field, new HasStarExtractor());
                    } else if ("id".equals(field)) {
                        extractors.put(field, new IncrementExtractor());
                    }
                }
            }
            return extractors;
        }
    }

    protected class AccountsQueryHandler implements QueryHandler {
        private static final String FIELD_ACCOUNT_COLOR = "accountColor";
        private static final String FIELD_ACCOUNT_NAME = "accountName";
        private static final String FIELD_ACCOUNT_NUMBER = "accountNumber";
        private static final String FIELD_ACCOUNT_UUID = "accountUuid";

        protected AccountsQueryHandler() {
        }

        public String getPath() {
            return SettingsExporter.ACCOUNTS_ELEMENT;
        }

        public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) throws Exception {
            return getAllAccounts(projection);
        }

        public Cursor getAllAccounts(String[] projection) {
            if (projection == null) {
                projection = new String[]{"accountNumber", FIELD_ACCOUNT_NAME};
            }
            MatrixCursor ret = new MatrixCursor(projection);
            for (Account account : Preferences.getPreferences(MessageProvider.this.getContext()).getAccounts()) {
                Object[] values = new Object[projection.length];
                int fieldIndex = 0;
                for (String field : projection) {
                    if ("accountNumber".equals(field)) {
                        values[fieldIndex] = Integer.valueOf(account.getAccountNumber());
                    } else if (FIELD_ACCOUNT_NAME.equals(field)) {
                        values[fieldIndex] = account.getDescription();
                    } else if (FIELD_ACCOUNT_UUID.equals(field)) {
                        values[fieldIndex] = account.getUuid();
                    } else if ("accountColor".equals(field)) {
                        values[fieldIndex] = Integer.valueOf(account.getChipColor());
                    } else {
                        values[fieldIndex] = null;
                    }
                    fieldIndex++;
                }
                ret.addRow(values);
            }
            return ret;
        }
    }

    protected class UnreadQueryHandler implements QueryHandler {
        protected UnreadQueryHandler() {
        }

        public String getPath() {
            return "account_unread/#";
        }

        public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) throws Exception {
            int accountId = Integer.parseInt(uri.getPathSegments().get(1));
            long identityToken = Binder.clearCallingIdentity();
            try {
                return getAccountUnread(accountId);
            } finally {
                Binder.restoreCallingIdentity(identityToken);
            }
        }

        private Cursor getAccountUnread(int accountNumber) {
            MatrixCursor ret = new MatrixCursor(new String[]{"accountName", MessageColumns.UNREAD});
            Object[] values = new Object[2];
            for (Account account : Preferences.getPreferences(MessageProvider.this.getContext()).getAvailableAccounts()) {
                if (account.getAccountNumber() == accountNumber) {
                    Account myAccount = account;
                    try {
                        AccountStats myAccountStats = account.getStats(MessageProvider.this.getContext());
                        values[0] = myAccount.getDescription();
                        if (myAccountStats == null) {
                            values[1] = 0;
                        } else {
                            values[1] = Integer.valueOf(myAccountStats.unreadMessageCount);
                        }
                        ret.addRow(values);
                    } catch (MessagingException e) {
                        Log.e("k9", e.getMessage());
                        values[0] = "Unknown";
                        values[1] = 0;
                    }
                }
            }
            return ret;
        }
    }

    protected static class MonitoredCursor implements CrossProcessCursor {
        private AtomicBoolean mClosed = new AtomicBoolean(false);
        private CrossProcessCursor mCursor;
        private Semaphore mSemaphore;

        protected MonitoredCursor(CrossProcessCursor cursor, Semaphore semaphore) {
            this.mCursor = cursor;
            this.mSemaphore = semaphore;
        }

        public void close() {
            if (this.mClosed.compareAndSet(false, true)) {
                this.mCursor.close();
                Log.d("k9", "Cursor closed, null'ing & releasing semaphore");
                this.mCursor = null;
                this.mSemaphore.release();
            }
        }

        public boolean isClosed() {
            return this.mClosed.get() || this.mCursor.isClosed();
        }

        /* access modifiers changed from: protected */
        public void finalize() throws Throwable {
            close();
            super.finalize();
        }

        /* access modifiers changed from: protected */
        public void checkClosed() throws IllegalStateException {
            if (this.mClosed.get()) {
                throw new IllegalStateException("Cursor was closed");
            }
        }

        public void fillWindow(int pos, CursorWindow winow) {
            checkClosed();
            this.mCursor.fillWindow(pos, winow);
        }

        public CursorWindow getWindow() {
            checkClosed();
            return this.mCursor.getWindow();
        }

        public boolean onMove(int oldPosition, int newPosition) {
            checkClosed();
            return this.mCursor.onMove(oldPosition, newPosition);
        }

        public void copyStringToBuffer(int columnIndex, CharArrayBuffer buffer) {
            checkClosed();
            this.mCursor.copyStringToBuffer(columnIndex, buffer);
        }

        public void deactivate() {
            checkClosed();
            this.mCursor.deactivate();
        }

        public byte[] getBlob(int columnIndex) {
            checkClosed();
            return this.mCursor.getBlob(columnIndex);
        }

        public int getColumnCount() {
            checkClosed();
            return this.mCursor.getColumnCount();
        }

        public int getColumnIndex(String columnName) {
            checkClosed();
            return this.mCursor.getColumnIndex(columnName);
        }

        public int getColumnIndexOrThrow(String columnName) throws IllegalArgumentException {
            checkClosed();
            return this.mCursor.getColumnIndexOrThrow(columnName);
        }

        public String getColumnName(int columnIndex) {
            checkClosed();
            return this.mCursor.getColumnName(columnIndex);
        }

        public String[] getColumnNames() {
            checkClosed();
            return this.mCursor.getColumnNames();
        }

        public int getCount() {
            checkClosed();
            return this.mCursor.getCount();
        }

        public double getDouble(int columnIndex) {
            checkClosed();
            return this.mCursor.getDouble(columnIndex);
        }

        public Bundle getExtras() {
            checkClosed();
            return this.mCursor.getExtras();
        }

        public float getFloat(int columnIndex) {
            checkClosed();
            return this.mCursor.getFloat(columnIndex);
        }

        public int getInt(int columnIndex) {
            checkClosed();
            return this.mCursor.getInt(columnIndex);
        }

        public long getLong(int columnIndex) {
            checkClosed();
            return this.mCursor.getLong(columnIndex);
        }

        public int getPosition() {
            checkClosed();
            return this.mCursor.getPosition();
        }

        public short getShort(int columnIndex) {
            checkClosed();
            return this.mCursor.getShort(columnIndex);
        }

        public String getString(int columnIndex) {
            checkClosed();
            return this.mCursor.getString(columnIndex);
        }

        public boolean getWantsAllOnMoveCalls() {
            checkClosed();
            return this.mCursor.getWantsAllOnMoveCalls();
        }

        @TargetApi(23)
        public void setExtras(Bundle extras) {
            this.mCursor.setExtras(extras);
        }

        public boolean isAfterLast() {
            checkClosed();
            return this.mCursor.isAfterLast();
        }

        public boolean isBeforeFirst() {
            checkClosed();
            return this.mCursor.isBeforeFirst();
        }

        public boolean isFirst() {
            checkClosed();
            return this.mCursor.isFirst();
        }

        public boolean isLast() {
            checkClosed();
            return this.mCursor.isLast();
        }

        public boolean isNull(int columnIndex) {
            checkClosed();
            return this.mCursor.isNull(columnIndex);
        }

        public boolean move(int offset) {
            checkClosed();
            return this.mCursor.move(offset);
        }

        public boolean moveToFirst() {
            checkClosed();
            return this.mCursor.moveToFirst();
        }

        public boolean moveToLast() {
            checkClosed();
            return this.mCursor.moveToLast();
        }

        public boolean moveToNext() {
            checkClosed();
            return this.mCursor.moveToNext();
        }

        public boolean moveToPosition(int position) {
            checkClosed();
            return this.mCursor.moveToPosition(position);
        }

        public boolean moveToPrevious() {
            checkClosed();
            return this.mCursor.moveToPrevious();
        }

        public void registerContentObserver(ContentObserver observer) {
            checkClosed();
            this.mCursor.registerContentObserver(observer);
        }

        public void registerDataSetObserver(DataSetObserver observer) {
            checkClosed();
            this.mCursor.registerDataSetObserver(observer);
        }

        public boolean requery() {
            checkClosed();
            return this.mCursor.requery();
        }

        public Bundle respond(Bundle extras) {
            checkClosed();
            return this.mCursor.respond(extras);
        }

        public void setNotificationUri(ContentResolver cr, Uri uri) {
            checkClosed();
            this.mCursor.setNotificationUri(cr, uri);
        }

        public void unregisterContentObserver(ContentObserver observer) {
            checkClosed();
            this.mCursor.unregisterContentObserver(observer);
        }

        public void unregisterDataSetObserver(DataSetObserver observer) {
            checkClosed();
            this.mCursor.unregisterDataSetObserver(observer);
        }

        public int getType(int columnIndex) {
            checkClosed();
            return this.mCursor.getType(columnIndex);
        }

        public Uri getNotificationUri() {
            return null;
        }
    }

    protected class ThrottlingQueryHandler implements QueryHandler {
        private QueryHandler mDelegate;

        public ThrottlingQueryHandler(QueryHandler delegate) {
            this.mDelegate = delegate;
        }

        public String getPath() {
            return this.mDelegate.getPath();
        }

        public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) throws Exception {
            MessageProvider.this.mSemaphore.acquire();
            Cursor cursor = null;
            try {
                cursor = this.mDelegate.query(uri, projection, selection, selectionArgs, sortOrder);
                if (!(cursor instanceof CrossProcessCursor)) {
                    Log.w("k9", "Unsupported cursor, returning null: " + cursor);
                    MessageProvider.this.mSemaphore.release();
                    return null;
                }
                MonitoredCursor wrapped = new MonitoredCursor((CrossProcessCursor) cursor, MessageProvider.this.mSemaphore);
                final WeakReference<MonitoredCursor> weakReference = new WeakReference<>(wrapped);
                MessageProvider.this.mScheduledPool.schedule(new Runnable() {
                    public void run() {
                        MonitoredCursor monitored = (MonitoredCursor) weakReference.get();
                        if (monitored != null && !monitored.isClosed()) {
                            Log.w("k9", "Forcibly closing remotely exposed cursor");
                            try {
                                monitored.close();
                            } catch (Exception e) {
                                Log.w("k9", "Exception while forcibly closing cursor", e);
                            }
                        }
                    }
                }, 30, TimeUnit.SECONDS);
                return wrapped;
            } finally {
                if (cursor == null) {
                    MessageProvider.this.mSemaphore.release();
                }
            }
        }
    }

    protected class MesssageInfoHolderRetrieverListener extends MessagingListener {
        private List<MessageInfoHolder> mHolders = new ArrayList();
        private final BlockingQueue<List<MessageInfoHolder>> queue;

        public MesssageInfoHolderRetrieverListener(BlockingQueue<List<MessageInfoHolder>> queue2) {
            this.queue = queue2;
        }

        public void listLocalMessagesAddMessages(Account account, String folderName, List<LocalMessage> messages) {
            MessageHelper helper = MessageProvider.this.mMessageHelper;
            List<MessageInfoHolder> holders = this.mHolders;
            Context context = MessageProvider.this.getContext();
            for (LocalMessage message : messages) {
                MessageInfoHolder messageInfoHolder = new MessageInfoHolder();
                LocalFolder messageFolder = message.getFolder();
                Account messageAccount = messageInfoHolder.message.getAccount();
                helper.populate(messageInfoHolder, message, new FolderInfoHolder(context, messageFolder, messageAccount), messageAccount);
                holders.add(messageInfoHolder);
            }
        }

        public void searchStats(AccountStats stats) {
            try {
                this.queue.put(this.mHolders);
            } catch (InterruptedException e) {
                Log.e("k9", "Unable to return message list back to caller", e);
            }
        }
    }

    public boolean onCreate() {
        this.mMessageHelper = MessageHelper.getInstance(getContext());
        registerQueryHandler(new ThrottlingQueryHandler(new AccountsQueryHandler()));
        registerQueryHandler(new ThrottlingQueryHandler(new MessagesQueryHandler()));
        registerQueryHandler(new ThrottlingQueryHandler(new UnreadQueryHandler()));
        K9.registerApplicationAware(new K9.ApplicationAware() {
            public void initializeComponent(final Application application) {
                Log.v("k9", "Registering content resolver notifier");
                MessagingController.getInstance(application).addListener(new MessagingListener() {
                    public void folderStatusChanged(Account account, String folderName, int unreadMessageCount) {
                        application.getContentResolver().notifyChange(MessageProvider.CONTENT_URI, null);
                    }
                });
            }
        });
        return true;
    }

    public int delete(Uri uri, String selection, String[] selectionArgs) {
        if (K9.app == null) {
            return 0;
        }
        if (K9.DEBUG) {
            Log.v("k9", "MessageProvider/delete: " + uri);
        }
        List<String> segments = uri.getPathSegments();
        int accountId = Integer.parseInt(segments.get(1));
        String folderName = segments.get(2);
        String msgUid = segments.get(3);
        Account myAccount = null;
        for (Account account : Preferences.getPreferences(getContext()).getAccounts()) {
            if (account.getAccountNumber() == accountId) {
                myAccount = account;
                if (!account.isAvailable(getContext())) {
                    Log.w("k9", "not deleting messages because account is unavailable at the moment");
                    return 0;
                }
            }
        }
        if (myAccount == null) {
            Log.e("k9", "Could not find account with id " + accountId);
        }
        if (myAccount != null) {
            MessagingController.getInstance(getContext()).deleteMessage(new MessageReference(myAccount.getUuid(), folderName, msgUid, null), null);
        }
        return 0;
    }

    public String getType(Uri uri) {
        if (K9.app != null && K9.DEBUG) {
            Log.v("k9", "MessageProvider/getType: " + uri);
        }
        return null;
    }

    public Uri insert(Uri uri, ContentValues values) {
        if (K9.app != null && K9.DEBUG) {
            Log.v("k9", "MessageProvider/insert: " + uri);
        }
        return null;
    }

    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        if (K9.app == null) {
            return null;
        }
        if (K9.DEBUG) {
            Log.v("k9", "MessageProvider/query: " + uri);
        }
        int code = this.mUriMatcher.match(uri);
        if (code == -1) {
            throw new IllegalStateException("Unrecognized URI: " + uri);
        }
        try {
            return this.mQueryHandlers.get(code).query(uri, projection, selection, selectionArgs, sortOrder);
        } catch (Exception e) {
            Log.e("k9", "Unable to execute query for URI: " + uri, e);
            return null;
        }
    }

    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        if (K9.app != null && K9.DEBUG) {
            Log.v("k9", "MessageProvider/update: " + uri);
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public void registerQueryHandler(QueryHandler handler) {
        if (!this.mQueryHandlers.contains(handler)) {
            this.mQueryHandlers.add(handler);
            this.mUriMatcher.addURI(AUTHORITY, handler.getPath(), this.mQueryHandlers.indexOf(handler));
        }
    }

    public static class ReverseDateComparator implements Comparator<MessageInfoHolder> {
        public int compare(MessageInfoHolder object2, MessageInfoHolder object1) {
            if (object1.compareDate == null) {
                return object2.compareDate == null ? 0 : 1;
            }
            if (object2.compareDate == null) {
                return -1;
            }
            return object1.compareDate.compareTo(object2.compareDate);
        }
    }
}
