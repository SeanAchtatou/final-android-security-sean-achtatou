package com.fsck.k9.provider;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;
import android.support.v4.content.FileProvider;
import android.util.Log;
import com.fsck.k9.K9;
import com.fsck.k9.provider.EmailProvider;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Locale;
import okio.ByteString;
import org.apache.commons.io.IOUtils;

public class AttachmentTempFileProvider extends FileProvider {
    private static final String AUTHORITY = "yahoo.mail.app.tempfileprovider";
    private static final String CACHE_DIRECTORY = "temp";
    private static final long FILE_DELETE_THRESHOLD_MILLISECONDS = 180000;
    private static AttachmentTempFileProviderCleanupReceiver cleanupReceiver = null;
    private static final Object cleanupReceiverMonitor = new Object();
    private static final Object tempFileWriteMonitor = new Object();

    @WorkerThread
    public static Uri createTempUriForContentUri(Context context, Uri uri) throws IOException {
        Context applicationContext = context.getApplicationContext();
        File tempFile = getTempFileForUri(uri, applicationContext);
        writeUriContentToTempFileIfNotExists(context, uri, tempFile);
        Uri tempFileUri = FileProvider.getUriForFile(context, AUTHORITY, tempFile);
        registerFileCleanupReceiver(applicationContext);
        return tempFileUri;
    }

    @NonNull
    private static File getTempFileForUri(Uri uri, Context context) {
        Context applicationContext = context.getApplicationContext();
        return new File(getTempFileDirectory(applicationContext), getTempFilenameForUri(uri));
    }

    private static String getTempFilenameForUri(Uri uri) {
        return ByteString.encodeUtf8(uri.toString()).sha1().hex();
    }

    private static void writeUriContentToTempFileIfNotExists(Context context, Uri uri, File tempFile) throws IOException {
        synchronized (tempFileWriteMonitor) {
            if (!tempFile.exists()) {
                FileOutputStream outputStream = new FileOutputStream(tempFile);
                InputStream inputStream = context.getContentResolver().openInputStream(uri);
                if (inputStream == null) {
                    throw new IOException("Failed to resolve content at uri: " + uri);
                }
                IOUtils.copy(inputStream, outputStream);
                outputStream.close();
                IOUtils.closeQuietly(inputStream);
            }
        }
    }

    public static Uri getMimeTypeUri(Uri contentUri, String mimeType) {
        if (!AUTHORITY.equals(contentUri.getAuthority())) {
            throw new IllegalArgumentException("Can only call this method for URIs within this authority!");
        } else if (contentUri.getQueryParameter(EmailProvider.InternalMessageColumns.MIME_TYPE) == null) {
            return contentUri.buildUpon().appendQueryParameter(EmailProvider.InternalMessageColumns.MIME_TYPE, mimeType).build();
        } else {
            throw new IllegalArgumentException("Can only call this method for not yet typed URIs!");
        }
    }

    public static boolean deleteOldTemporaryFiles(Context context) {
        File tempDirectory = getTempFileDirectory(context);
        boolean allFilesDeleted = true;
        long deletionThreshold = new Date().getTime() - FILE_DELETE_THRESHOLD_MILLISECONDS;
        for (File tempFile : tempDirectory.listFiles()) {
            long lastModified = tempFile.lastModified();
            if (lastModified >= deletionThreshold) {
                if (K9.DEBUG) {
                    Log.e("k9", "Not deleting temp file (for another " + String.format(Locale.ENGLISH, "%.2f", Double.valueOf(((double) ((lastModified - deletionThreshold) / 1000)) / 60.0d)) + " minutes)");
                }
                allFilesDeleted = false;
            } else if (!tempFile.delete()) {
                Log.e("k9", "Failed to delete temporary file");
                allFilesDeleted = false;
            }
        }
        return allFilesDeleted;
    }

    private static File getTempFileDirectory(Context context) {
        File directory = new File(context.getCacheDir(), CACHE_DIRECTORY);
        if (!directory.exists() && !directory.mkdir()) {
            Log.e("k9", "Error creating directory: " + directory.getAbsolutePath());
        }
        return directory;
    }

    public String getType(Uri uri) {
        return uri.getQueryParameter(EmailProvider.InternalMessageColumns.MIME_TYPE);
    }

    public int delete(Uri uri, String selection, String[] selectionArgs) {
        throw new UnsupportedOperationException();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0005, code lost:
        r0 = getContext();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onTrimMemory(int r4) {
        /*
            r3 = this;
            r1 = 80
            if (r4 >= r1) goto L_0x0005
        L_0x0004:
            return
        L_0x0005:
            android.content.Context r0 = r3.getContext()
            if (r0 == 0) goto L_0x0004
            com.fsck.k9.provider.AttachmentTempFileProvider$1 r1 = new com.fsck.k9.provider.AttachmentTempFileProvider$1
            r1.<init>(r0)
            r2 = 0
            java.lang.Void[] r2 = new java.lang.Void[r2]
            r1.execute(r2)
            unregisterFileCleanupReceiver(r0)
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fsck.k9.provider.AttachmentTempFileProvider.onTrimMemory(int):void");
    }

    /* access modifiers changed from: private */
    public static void unregisterFileCleanupReceiver(Context context) {
        synchronized (cleanupReceiverMonitor) {
            if (cleanupReceiver != null) {
                if (K9.DEBUG) {
                    Log.d("k9", "Unregistering temp file cleanup receiver");
                }
                context.unregisterReceiver(cleanupReceiver);
                cleanupReceiver = null;
            }
        }
    }

    private static void registerFileCleanupReceiver(Context context) {
        synchronized (cleanupReceiverMonitor) {
            if (cleanupReceiver == null) {
                if (K9.DEBUG) {
                    Log.d("k9", "Registering temp file cleanup receiver");
                }
                cleanupReceiver = new AttachmentTempFileProviderCleanupReceiver();
                IntentFilter intentFilter = new IntentFilter();
                intentFilter.addAction("android.intent.action.SCREEN_OFF");
                context.registerReceiver(cleanupReceiver, intentFilter);
            }
        }
    }

    private static class AttachmentTempFileProviderCleanupReceiver extends BroadcastReceiver {
        private AttachmentTempFileProviderCleanupReceiver() {
        }

        @MainThread
        public void onReceive(Context context, Intent intent) {
            if (!"android.intent.action.SCREEN_OFF".equals(intent.getAction())) {
                throw new IllegalArgumentException("onReceive called with action that isn't screen off!");
            }
            if (K9.DEBUG) {
                Log.d("k9", "Cleaning up temp files");
            }
            if (AttachmentTempFileProvider.deleteOldTemporaryFiles(context)) {
                AttachmentTempFileProvider.unregisterFileCleanupReceiver(context);
            }
        }
    }
}
