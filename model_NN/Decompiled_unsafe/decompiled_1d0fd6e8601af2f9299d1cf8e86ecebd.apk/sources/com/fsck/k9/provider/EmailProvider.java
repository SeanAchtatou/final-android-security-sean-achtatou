package com.fsck.k9.provider;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.CursorWrapper;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.text.TextUtils;
import com.fsck.k9.Account;
import com.fsck.k9.Preferences;
import com.fsck.k9.cache.EmailProviderCacheCursor;
import com.fsck.k9.helper.Utility;
import com.fsck.k9.mail.MessagingException;
import com.fsck.k9.mailstore.LockableDatabase;
import com.fsck.k9.mailstore.UnavailableStorageException;
import com.fsck.k9.provider.AttachmentProvider;
import com.fsck.k9.search.SqlQueryBuilder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EmailProvider extends ContentProvider {
    public static final String AUTHORITY = "yahoo.mail.app.provider.email";
    public static final Uri CONTENT_URI = Uri.parse("content://yahoo.mail.app.provider.email");
    /* access modifiers changed from: private */
    public static final String[] FIXUP_AGGREGATED_MESSAGES_COLUMNS = {"date", MessageColumns.INTERNAL_DATE, MessageColumns.ATTACHMENT_COUNT, MessageColumns.READ, MessageColumns.FLAGGED, MessageColumns.ANSWERED, MessageColumns.FORWARDED};
    /* access modifiers changed from: private */
    public static final String[] FIXUP_MESSAGES_COLUMNS = {"id"};
    /* access modifiers changed from: private */
    public static final String[] FOLDERS_COLUMNS = {"id", "name", FolderColumns.LAST_UPDATED, "unread_count", FolderColumns.VISIBLE_LIMIT, "status", FolderColumns.PUSH_STATE, FolderColumns.LAST_PUSHED, "flagged_count", "integrate", FolderColumns.TOP_GROUP, FolderColumns.POLL_CLASS, FolderColumns.PUSH_CLASS, FolderColumns.DISPLAY_CLASS};
    private static final String FOLDERS_TABLE = "folders";
    private static final int MESSAGES = 0;
    private static final String MESSAGES_TABLE = "messages";
    private static final int MESSAGES_THREAD = 2;
    private static final int MESSAGES_THREADED = 1;
    private static final int MESSAGE_BASE = 0;
    private static final int STATS = 100;
    private static final int STATS_BASE = 100;
    private static final String[] STATS_DEFAULT_PROJECTION = {"unread_count", "flagged_count"};
    private static final String THREADS_TABLE = "threads";
    /* access modifiers changed from: private */
    public static final Map<String, String> THREAD_AGGREGATION_FUNCS = new HashMap();
    private static final UriMatcher sUriMatcher = new UriMatcher(-1);
    private Preferences mPreferences;

    public interface FolderColumns {
        public static final String DISPLAY_CLASS = "display_class";
        public static final String FLAGGED_COUNT = "flagged_count";
        public static final String ID = "id";
        public static final String INTEGRATE = "integrate";
        public static final String LAST_PUSHED = "last_pushed";
        public static final String LAST_UPDATED = "last_updated";
        public static final String NAME = "name";
        public static final String POLL_CLASS = "poll_class";
        public static final String PUSH_CLASS = "push_class";
        public static final String PUSH_STATE = "push_state";
        public static final String STATUS = "status";
        public static final String TOP_GROUP = "top_group";
        public static final String UNREAD_COUNT = "unread_count";
        public static final String VISIBLE_LIMIT = "visible_limit";
    }

    private interface InternalMessageColumns extends MessageColumns {
        public static final String DELETED = "deleted";
        public static final String EMPTY = "empty";
        public static final String MIME_TYPE = "mime_type";
    }

    public interface MessageColumns {
        public static final String ANSWERED = "answered";
        public static final String ATTACHMENT_COUNT = "attachment_count";
        public static final String BCC_LIST = "bcc_list";
        public static final String CC_LIST = "cc_list";
        public static final String DATE = "date";
        public static final String FLAGGED = "flagged";
        public static final String FLAGS = "flags";
        public static final String FOLDER_ID = "folder_id";
        public static final String FORWARDED = "forwarded";
        public static final String ID = "id";
        public static final String INTERNAL_DATE = "internal_date";
        public static final String MESSAGE_ID = "message_id";
        public static final String PREVIEW = "preview";
        public static final String PREVIEW_TYPE = "preview_type";
        public static final String READ = "read";
        public static final String REPLY_TO_LIST = "reply_to_list";
        public static final String SENDER_LIST = "sender_list";
        public static final String SUBJECT = "subject";
        public static final String TO_LIST = "to_list";
        public static final String UID = "uid";
    }

    public interface SpecialColumns {
        public static final String ACCOUNT_UUID = "account_uuid";
        public static final String FOLDER_NAME = "name";
        public static final String INTEGRATE = "integrate";
        public static final String THREAD_COUNT = "thread_count";
    }

    public interface StatsColumns {
        public static final String FLAGGED_COUNT = "flagged_count";
        public static final String UNREAD_COUNT = "unread_count";
    }

    public interface ThreadColumns {
        public static final String ID = "id";
        public static final String MESSAGE_ID = "message_id";
        public static final String PARENT = "parent";
        public static final String ROOT = "root";
    }

    static {
        THREAD_AGGREGATION_FUNCS.put("date", "MAX");
        THREAD_AGGREGATION_FUNCS.put(MessageColumns.INTERNAL_DATE, "MAX");
        THREAD_AGGREGATION_FUNCS.put(MessageColumns.ATTACHMENT_COUNT, "SUM");
        THREAD_AGGREGATION_FUNCS.put(MessageColumns.READ, "MIN");
        THREAD_AGGREGATION_FUNCS.put(MessageColumns.FLAGGED, "MAX");
        THREAD_AGGREGATION_FUNCS.put(MessageColumns.ANSWERED, "MIN");
        THREAD_AGGREGATION_FUNCS.put(MessageColumns.FORWARDED, "MIN");
        UriMatcher matcher = sUriMatcher;
        matcher.addURI(AUTHORITY, "account/*/messages", 0);
        matcher.addURI(AUTHORITY, "account/*/messages/threaded", 1);
        matcher.addURI(AUTHORITY, "account/*/thread/#", 2);
        matcher.addURI(AUTHORITY, "account/*/stats", 100);
    }

    public boolean onCreate() {
        return true;
    }

    public String getType(Uri uri) {
        throw new RuntimeException("not implemented yet");
    }

    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor cursor;
        int match = sUriMatcher.match(uri);
        if (match < 0) {
            throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        ContentResolver contentResolver = getContext().getContentResolver();
        switch (match) {
            case 0:
            case 1:
            case 2:
                List<String> segments = uri.getPathSegments();
                String accountUuid = segments.get(1);
                List<String> dbColumnNames = new ArrayList<>(projection.length);
                Map<String, String> specialColumns = new HashMap<>();
                for (String columnName : projection) {
                    if ("account_uuid".equals(columnName)) {
                        specialColumns.put("account_uuid", accountUuid);
                    } else {
                        dbColumnNames.add(columnName);
                    }
                }
                String[] dbProjection = (String[]) dbColumnNames.toArray(new String[0]);
                if (match == 0) {
                    cursor = getMessages(accountUuid, dbProjection, selection, selectionArgs, sortOrder);
                } else if (match == 1) {
                    cursor = getThreadedMessages(accountUuid, dbProjection, selection, selectionArgs, sortOrder);
                } else if (match == 2) {
                    cursor = getThread(accountUuid, dbProjection, segments.get(3), sortOrder);
                } else {
                    throw new RuntimeException("Not implemented");
                }
                cursor.setNotificationUri(contentResolver, Uri.withAppendedPath(CONTENT_URI, "account/" + accountUuid + "/messages"));
                return new EmailProviderCacheCursor(accountUuid, new SpecialColumnsCursor(new IdTrickeryCursor(cursor), projection, specialColumns), getContext());
            case 100:
                String accountUuid2 = uri.getPathSegments().get(1);
                Cursor cursor2 = getAccountStats(accountUuid2, projection, selection, selectionArgs);
                cursor2.setNotificationUri(contentResolver, Uri.withAppendedPath(CONTENT_URI, "account/" + accountUuid2 + "/messages"));
                return cursor2;
            default:
                return null;
        }
    }

    public int delete(Uri uri, String selection, String[] selectionArgs) {
        throw new RuntimeException("not implemented yet");
    }

    public Uri insert(Uri uri, ContentValues values) {
        throw new RuntimeException("not implemented yet");
    }

    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        throw new RuntimeException("not implemented yet");
    }

    /* access modifiers changed from: protected */
    public Cursor getMessages(String accountUuid, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        try {
            final String str = selection;
            final String[] strArr = projection;
            final String str2 = sortOrder;
            final String[] strArr2 = selectionArgs;
            return (Cursor) getDatabase(getAccount(accountUuid)).execute(false, new LockableDatabase.DbCallback<Cursor>() {
                public Cursor doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException, UnavailableStorageException {
                    String where;
                    if (TextUtils.isEmpty(str)) {
                        where = "deleted = 0 AND empty = 0";
                    } else {
                        where = "(" + str + ") AND " + InternalMessageColumns.DELETED + " = 0 AND " + "empty" + " = 0";
                    }
                    if (Utility.arrayContainsAny(strArr, (Object[]) EmailProvider.FOLDERS_COLUMNS)) {
                        StringBuilder query = new StringBuilder();
                        query.append("SELECT ");
                        boolean first = true;
                        for (String columnName : strArr) {
                            if (!first) {
                                query.append(",");
                            } else {
                                first = false;
                            }
                            if ("id".equals(columnName)) {
                                query.append("m.");
                                query.append("id");
                                query.append(" AS ");
                                query.append("id");
                            } else {
                                query.append(columnName);
                            }
                        }
                        query.append(" FROM messages m JOIN threads t ON (t.message_id = m.id) LEFT JOIN folders f ON (m.folder_id = f.id) WHERE ");
                        query.append(SqlQueryBuilder.addPrefixToSelection(EmailProvider.FIXUP_MESSAGES_COLUMNS, "m.", where));
                        query.append(" ORDER BY ");
                        query.append(SqlQueryBuilder.addPrefixToSelection(EmailProvider.FIXUP_MESSAGES_COLUMNS, "m.", str2));
                        return db.rawQuery(query.toString(), strArr2);
                    }
                    return db.query(EmailProvider.MESSAGES_TABLE, strArr, where, strArr2, null, null, str2);
                }
            });
        } catch (UnavailableStorageException e) {
            throw new RuntimeException("Storage not available", e);
        } catch (MessagingException e2) {
            throw new RuntimeException("messaging exception", e2);
        }
    }

    /* access modifiers changed from: protected */
    public Cursor getThreadedMessages(String accountUuid, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        try {
            final String[] strArr = projection;
            final String str = selection;
            final String[] strArr2 = selectionArgs;
            final String str2 = sortOrder;
            return (Cursor) getDatabase(getAccount(accountUuid)).execute(false, new LockableDatabase.DbCallback<Cursor>() {
                public Cursor doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException, UnavailableStorageException {
                    StringBuilder query = new StringBuilder();
                    query.append("SELECT ");
                    boolean first = true;
                    for (String columnName : strArr) {
                        if (!first) {
                            query.append(",");
                        } else {
                            first = false;
                        }
                        String aggregationFunc = (String) EmailProvider.THREAD_AGGREGATION_FUNCS.get(columnName);
                        if ("id".equals(columnName)) {
                            query.append("m.id AS id");
                        } else if (aggregationFunc != null) {
                            query.append("a.");
                            query.append(columnName);
                            query.append(" AS ");
                            query.append(columnName);
                        } else {
                            query.append(columnName);
                        }
                    }
                    query.append(" FROM (");
                    EmailProvider.this.createThreadedSubQuery(strArr, str, strArr2, query);
                    query.append(") a ");
                    query.append("JOIN threads t ON (t.root = a.thread_root) JOIN messages m ON (m.id = t.message_id AND m.empty=0 AND m.deleted=0 AND m.date = a.date) ");
                    if (Utility.arrayContainsAny(strArr, (Object[]) EmailProvider.FOLDERS_COLUMNS)) {
                        query.append("JOIN folders f ON (m.folder_id = f.id) ");
                    }
                    query.append(" GROUP BY root");
                    if (!TextUtils.isEmpty(str2)) {
                        query.append(" ORDER BY ");
                        query.append(SqlQueryBuilder.addPrefixToSelection(EmailProvider.FIXUP_AGGREGATED_MESSAGES_COLUMNS, "a.", str2));
                    }
                    return db.rawQuery(query.toString(), strArr2);
                }
            });
        } catch (UnavailableStorageException e) {
            throw new RuntimeException("Storage not available", e);
        } catch (MessagingException e2) {
            throw new RuntimeException("messaging exception", e2);
        }
    }

    /* access modifiers changed from: private */
    public void createThreadedSubQuery(String[] projection, String selection, String[] selectionArgs, StringBuilder query) {
        query.append("SELECT t.root AS thread_root");
        for (String columnName : projection) {
            String aggregationFunc = THREAD_AGGREGATION_FUNCS.get(columnName);
            if (SpecialColumns.THREAD_COUNT.equals(columnName)) {
                query.append(",COUNT(t.root) AS thread_count");
            } else if (aggregationFunc != null) {
                query.append(",");
                query.append(aggregationFunc);
                query.append("(");
                query.append(columnName);
                query.append(") AS ");
                query.append(columnName);
            }
        }
        query.append(" FROM messages m JOIN threads t ON (t.message_id = m.id)");
        if (Utility.arrayContainsAny(projection, (Object[]) FOLDERS_COLUMNS)) {
            query.append(" JOIN folders f ON (m.folder_id = f.id)");
        }
        query.append(" WHERE (t.root IN (SELECT root FROM messages m JOIN threads t ON (t.message_id = m.id) WHERE m.empty = 0 AND m.deleted = 0)");
        if (!TextUtils.isEmpty(selection)) {
            query.append(" AND (");
            query.append(selection);
            query.append(")");
        }
        query.append(") AND deleted = 0 AND empty = 0");
        query.append(" GROUP BY t.root");
    }

    /* access modifiers changed from: protected */
    public Cursor getThread(String accountUuid, final String[] projection, final String threadId, final String sortOrder) {
        try {
            return (Cursor) getDatabase(getAccount(accountUuid)).execute(false, new LockableDatabase.DbCallback<Cursor>() {
                public Cursor doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException, UnavailableStorageException {
                    StringBuilder query = new StringBuilder();
                    query.append("SELECT ");
                    boolean first = true;
                    for (String columnName : projection) {
                        if (!first) {
                            query.append(",");
                        } else {
                            first = false;
                        }
                        if ("id".equals(columnName)) {
                            query.append("m.id AS id");
                        } else {
                            query.append(columnName);
                        }
                    }
                    query.append(" FROM threads t JOIN messages m ON (m.id = t.message_id) ");
                    if (Utility.arrayContainsAny(projection, (Object[]) EmailProvider.FOLDERS_COLUMNS)) {
                        query.append("LEFT JOIN folders f ON (m.folder_id = f.id) ");
                    }
                    query.append("WHERE root = ? AND deleted = 0 AND empty = 0");
                    query.append(" ORDER BY ");
                    query.append(SqlQueryBuilder.addPrefixToSelection(EmailProvider.FIXUP_MESSAGES_COLUMNS, "m.", sortOrder));
                    return db.rawQuery(query.toString(), new String[]{threadId});
                }
            });
        } catch (UnavailableStorageException e) {
            throw new RuntimeException("Storage not available", e);
        } catch (MessagingException e2) {
            throw new RuntimeException("messaging exception", e2);
        }
    }

    private Cursor getAccountStats(String accountUuid, String[] columns, String selection, final String[] selectionArgs) {
        String[] sourceProjection;
        LockableDatabase database = getDatabase(getAccount(accountUuid));
        if (columns == null) {
            sourceProjection = STATS_DEFAULT_PROJECTION;
        } else {
            sourceProjection = columns;
        }
        final StringBuilder sql = new StringBuilder();
        sql.append("SELECT ");
        boolean first = true;
        for (String columnName : sourceProjection) {
            if (!first) {
                sql.append(',');
            } else {
                first = false;
            }
            if ("unread_count".equals(columnName)) {
                sql.append("SUM(read=0) AS unread_count");
            } else if ("flagged_count".equals(columnName)) {
                sql.append("SUM(flagged) AS flagged_count");
            } else {
                throw new IllegalArgumentException("Column name not allowed: " + columnName);
            }
        }
        sql.append(" FROM messages");
        if (containsAny(selection, FOLDERS_COLUMNS)) {
            sql.append(" JOIN folders ON (folders.id = messages.folder_id)");
        }
        sql.append(" WHERE (deleted = 0 AND empty = 0)");
        if (!TextUtils.isEmpty(selection)) {
            sql.append(" AND (");
            sql.append(selection);
            sql.append(")");
        }
        try {
            return (Cursor) database.execute(false, new LockableDatabase.DbCallback<Cursor>() {
                public Cursor doDbWork(SQLiteDatabase db) throws LockableDatabase.WrappedException, UnavailableStorageException {
                    return db.rawQuery(sql.toString(), selectionArgs);
                }
            });
        } catch (UnavailableStorageException e) {
            throw new RuntimeException("Storage not available", e);
        } catch (MessagingException e2) {
            throw new RuntimeException("messaging exception", e2);
        }
    }

    private Account getAccount(String accountUuid) {
        if (this.mPreferences == null) {
            this.mPreferences = Preferences.getPreferences(getContext().getApplicationContext());
        }
        Account account = this.mPreferences.getAccount(accountUuid);
        if (account != null) {
            return account;
        }
        throw new IllegalArgumentException("Unknown account: " + accountUuid);
    }

    private LockableDatabase getDatabase(Account account) {
        try {
            return account.getLocalStore().getDatabase();
        } catch (MessagingException e) {
            throw new RuntimeException("Couldn't get LocalStore", e);
        }
    }

    static class IdTrickeryCursor extends CursorWrapper {
        public IdTrickeryCursor(Cursor cursor) {
            super(cursor);
        }

        public int getColumnIndex(String columnName) {
            if (AttachmentProvider.AttachmentProviderColumns._ID.equals(columnName)) {
                return super.getColumnIndex("id");
            }
            return super.getColumnIndex(columnName);
        }

        public int getColumnIndexOrThrow(String columnName) {
            if (AttachmentProvider.AttachmentProviderColumns._ID.equals(columnName)) {
                return super.getColumnIndexOrThrow("id");
            }
            return super.getColumnIndexOrThrow(columnName);
        }
    }

    static class SpecialColumnsCursor extends CursorWrapper {
        private int[] mColumnMapping;
        private String[] mColumnNames;
        private String[] mSpecialColumnValues;

        public SpecialColumnsCursor(Cursor cursor, String[] allColumnNames, Map<String, String> specialColumns) {
            super(cursor);
            int columnIndex;
            this.mColumnNames = allColumnNames;
            this.mColumnMapping = new int[allColumnNames.length];
            this.mSpecialColumnValues = new String[specialColumns.size()];
            int i = 0;
            int specialColumnCount = 0;
            int len = allColumnNames.length;
            int columnIndex2 = 0;
            while (i < len) {
                String columnName = allColumnNames[i];
                if (specialColumns.containsKey(columnName)) {
                    this.mSpecialColumnValues[specialColumnCount] = specialColumns.get(columnName);
                    this.mColumnMapping[i] = -(specialColumnCount + 1);
                    specialColumnCount++;
                    columnIndex = columnIndex2;
                } else {
                    columnIndex = columnIndex2 + 1;
                    this.mColumnMapping[i] = columnIndex2;
                }
                i++;
                columnIndex2 = columnIndex;
            }
        }

        public byte[] getBlob(int columnIndex) {
            int realColumnIndex = this.mColumnMapping[columnIndex];
            if (realColumnIndex >= 0) {
                return super.getBlob(realColumnIndex);
            }
            throw new RuntimeException("Special column can only be retrieved as string.");
        }

        public int getColumnCount() {
            return this.mColumnMapping.length;
        }

        public int getColumnIndex(String columnName) {
            int len = this.mColumnNames.length;
            for (int i = 0; i < len; i++) {
                if (this.mColumnNames[i].equals(columnName)) {
                    return i;
                }
            }
            return super.getColumnIndex(columnName);
        }

        public int getColumnIndexOrThrow(String columnName) throws IllegalArgumentException {
            int index = getColumnIndex(columnName);
            if (index != -1) {
                return index;
            }
            throw new IllegalArgumentException("Unknown column name");
        }

        public String getColumnName(int columnIndex) {
            return this.mColumnNames[columnIndex];
        }

        public String[] getColumnNames() {
            return (String[]) this.mColumnNames.clone();
        }

        public double getDouble(int columnIndex) {
            int realColumnIndex = this.mColumnMapping[columnIndex];
            if (realColumnIndex >= 0) {
                return super.getDouble(realColumnIndex);
            }
            throw new RuntimeException("Special column can only be retrieved as string.");
        }

        public float getFloat(int columnIndex) {
            int realColumnIndex = this.mColumnMapping[columnIndex];
            if (realColumnIndex >= 0) {
                return super.getFloat(realColumnIndex);
            }
            throw new RuntimeException("Special column can only be retrieved as string.");
        }

        public int getInt(int columnIndex) {
            int realColumnIndex = this.mColumnMapping[columnIndex];
            if (realColumnIndex >= 0) {
                return super.getInt(realColumnIndex);
            }
            throw new RuntimeException("Special column can only be retrieved as string.");
        }

        public long getLong(int columnIndex) {
            int realColumnIndex = this.mColumnMapping[columnIndex];
            if (realColumnIndex >= 0) {
                return super.getLong(realColumnIndex);
            }
            throw new RuntimeException("Special column can only be retrieved as string.");
        }

        public short getShort(int columnIndex) {
            int realColumnIndex = this.mColumnMapping[columnIndex];
            if (realColumnIndex >= 0) {
                return super.getShort(realColumnIndex);
            }
            throw new RuntimeException("Special column can only be retrieved as string.");
        }

        public String getString(int columnIndex) {
            int realColumnIndex = this.mColumnMapping[columnIndex];
            if (realColumnIndex < 0) {
                return this.mSpecialColumnValues[(-realColumnIndex) - 1];
            }
            return super.getString(realColumnIndex);
        }

        public int getType(int columnIndex) {
            int realColumnIndex = this.mColumnMapping[columnIndex];
            if (realColumnIndex < 0) {
                return 3;
            }
            return super.getType(realColumnIndex);
        }

        public boolean isNull(int columnIndex) {
            int realColumnIndex = this.mColumnMapping[columnIndex];
            if (realColumnIndex < 0) {
                return this.mSpecialColumnValues[(-realColumnIndex) + -1] == null;
            }
            return super.isNull(realColumnIndex);
        }
    }

    private static boolean containsAny(String haystack, String[] needles) {
        if (haystack == null) {
            return false;
        }
        for (String needle : needles) {
            if (haystack.contains(needle)) {
                return true;
            }
        }
        return false;
    }
}
