package com.fsck.k9.search;

import android.content.Context;
import com.fsck.k9.BaseAccount;
import com.fsck.k9.search.SearchSpecification;
import exts.whats.Constants;
import yahoo.mail.app.R;

public class SearchAccount implements BaseAccount {
    public static final String ALL_MESSAGES = "all_messages";
    public static final String UNIFIED_INBOX = "unified_inbox";
    private String mDescription;
    private String mEmail;
    private String mId;
    private LocalSearch mSearch;

    public static SearchAccount createAllMessagesAccount(Context context) {
        String name = context.getString(R.string.search_all_messages_title);
        LocalSearch tmpSearch = new LocalSearch(name);
        tmpSearch.and(SearchSpecification.SearchField.SEARCHABLE, Constants.INSTALL_ID, SearchSpecification.Attribute.EQUALS);
        return new SearchAccount(ALL_MESSAGES, tmpSearch, name, context.getString(R.string.search_all_messages_detail));
    }

    public static SearchAccount createUnifiedInboxAccount(Context context) {
        String name = context.getString(R.string.integrated_inbox_title);
        LocalSearch tmpSearch = new LocalSearch(name);
        tmpSearch.and(SearchSpecification.SearchField.INTEGRATE, Constants.INSTALL_ID, SearchSpecification.Attribute.EQUALS);
        return new SearchAccount(UNIFIED_INBOX, tmpSearch, name, context.getString(R.string.integrated_inbox_detail));
    }

    public SearchAccount(String id, LocalSearch search, String description, String email) throws IllegalArgumentException {
        if (search == null) {
            throw new IllegalArgumentException("Provided LocalSearch was null");
        }
        this.mId = id;
        this.mSearch = search;
        this.mDescription = description;
        this.mEmail = email;
    }

    public String getId() {
        return this.mId;
    }

    public synchronized String getEmail() {
        return this.mEmail;
    }

    public synchronized void setEmail(String email) {
        this.mEmail = email;
    }

    public String getDescription() {
        return this.mDescription;
    }

    public void setDescription(String description) {
        this.mDescription = description;
    }

    public LocalSearch getRelatedSearch() {
        return this.mSearch;
    }

    public String getUuid() {
        return this.mId;
    }
}
