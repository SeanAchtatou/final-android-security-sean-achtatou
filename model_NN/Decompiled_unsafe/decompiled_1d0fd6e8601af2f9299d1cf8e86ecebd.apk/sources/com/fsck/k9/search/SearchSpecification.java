package com.fsck.k9.search;

import android.os.Parcel;
import android.os.Parcelable;

public interface SearchSpecification extends Parcelable {
    public static final String ALL_ACCOUNTS = "allAccounts";

    public enum Attribute {
        CONTAINS,
        NOT_CONTAINS,
        EQUALS,
        NOT_EQUALS,
        STARTSWITH,
        NOT_STARTSWITH,
        ENDSWITH,
        NOT_ENDSWITH
    }

    public enum SearchField {
        SUBJECT,
        DATE,
        UID,
        FLAG,
        SENDER,
        TO,
        CC,
        FOLDER,
        BCC,
        REPLY_TO,
        MESSAGE_CONTENTS,
        ATTACHMENT_COUNT,
        DELETED,
        THREAD_ID,
        ID,
        INTEGRATE,
        READ,
        FLAGGED,
        DISPLAY_CLASS,
        SEARCHABLE
    }

    String[] getAccountUuids();

    ConditionsTreeNode getConditions();

    String getName();

    public static class SearchCondition implements Parcelable {
        public static final Parcelable.Creator<SearchCondition> CREATOR = new Parcelable.Creator<SearchCondition>() {
            public SearchCondition createFromParcel(Parcel in) {
                return new SearchCondition(in);
            }

            public SearchCondition[] newArray(int size) {
                return new SearchCondition[size];
            }
        };
        public final Attribute attribute;
        public final SearchField field;
        public final String value;

        public SearchCondition(SearchField field2, Attribute attribute2, String value2) {
            this.value = value2;
            this.attribute = attribute2;
            this.field = field2;
        }

        private SearchCondition(Parcel in) {
            this.value = in.readString();
            this.attribute = Attribute.values()[in.readInt()];
            this.field = SearchField.values()[in.readInt()];
        }

        public SearchCondition clone() {
            return new SearchCondition(this.field, this.attribute, this.value);
        }

        public String toHumanString() {
            return this.field.toString() + this.attribute.toString();
        }

        public boolean equals(Object o) {
            if (o instanceof SearchCondition) {
                SearchCondition tmp = (SearchCondition) o;
                if (tmp.attribute == this.attribute && tmp.field == this.field && tmp.value.equals(this.value)) {
                    return true;
                }
                return false;
            }
            return false;
        }

        public int hashCode() {
            return ((((this.attribute.hashCode() + 31) * 31) + this.field.hashCode()) * 31) + this.value.hashCode();
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.value);
            dest.writeInt(this.attribute.ordinal());
            dest.writeInt(this.field.ordinal());
        }
    }
}
