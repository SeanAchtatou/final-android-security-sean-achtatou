package com.fsck.k9.search;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import com.fsck.k9.search.SearchSpecification;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;

public class ConditionsTreeNode implements Parcelable {
    public static final Parcelable.Creator<ConditionsTreeNode> CREATOR = new Parcelable.Creator<ConditionsTreeNode>() {
        public ConditionsTreeNode createFromParcel(Parcel in) {
            return new ConditionsTreeNode(in);
        }

        public ConditionsTreeNode[] newArray(int size) {
            return new ConditionsTreeNode[size];
        }
    };
    public SearchSpecification.SearchCondition mCondition;
    public ConditionsTreeNode mLeft;
    public int mLeftMPTTMarker;
    public ConditionsTreeNode mParent;
    public ConditionsTreeNode mRight;
    public int mRightMPTTMarker;
    public Operator mValue;

    public enum Operator {
        AND,
        OR,
        CONDITION
    }

    public static ConditionsTreeNode buildTreeFromDB(Cursor cursor) {
        Stack<ConditionsTreeNode> stack = new Stack<>();
        ConditionsTreeNode tmp = null;
        if (cursor.moveToFirst()) {
            tmp = buildNodeFromRow(cursor);
            stack.push(tmp);
        }
        while (cursor.moveToNext()) {
            tmp = buildNodeFromRow(cursor);
            if (tmp.mRightMPTTMarker < ((ConditionsTreeNode) stack.peek()).mRightMPTTMarker) {
                ((ConditionsTreeNode) stack.peek()).mLeft = tmp;
                stack.push(tmp);
            } else {
                while (((ConditionsTreeNode) stack.peek()).mRightMPTTMarker < tmp.mRightMPTTMarker) {
                    stack.pop();
                }
                ((ConditionsTreeNode) stack.peek()).mRight = tmp;
            }
        }
        return tmp;
    }

    private static ConditionsTreeNode buildNodeFromRow(Cursor cursor) {
        SearchSpecification.SearchCondition condition = null;
        Operator tmpValue = Operator.valueOf(cursor.getString(5));
        if (tmpValue == Operator.CONDITION) {
            condition = new SearchSpecification.SearchCondition(SearchSpecification.SearchField.valueOf(cursor.getString(0)), SearchSpecification.Attribute.valueOf(cursor.getString(2)), cursor.getString(1));
        }
        ConditionsTreeNode result = new ConditionsTreeNode(condition);
        result.mValue = tmpValue;
        result.mLeftMPTTMarker = cursor.getInt(3);
        result.mRightMPTTMarker = cursor.getInt(4);
        return result;
    }

    public ConditionsTreeNode(SearchSpecification.SearchCondition condition) {
        this.mParent = null;
        this.mCondition = condition;
        this.mValue = Operator.CONDITION;
    }

    public ConditionsTreeNode(ConditionsTreeNode parent, Operator op) {
        this.mParent = parent;
        this.mValue = op;
        this.mCondition = null;
    }

    /* access modifiers changed from: package-private */
    public ConditionsTreeNode cloneTree() {
        ConditionsTreeNode conditionsTreeNode = null;
        if (this.mParent != null) {
            throw new IllegalStateException("Can't call cloneTree() for a non-root node");
        }
        ConditionsTreeNode copy = new ConditionsTreeNode(this.mCondition.clone());
        copy.mLeftMPTTMarker = this.mLeftMPTTMarker;
        copy.mRightMPTTMarker = this.mRightMPTTMarker;
        copy.mLeft = this.mLeft == null ? null : this.mLeft.cloneNode(copy);
        if (this.mRight != null) {
            conditionsTreeNode = this.mRight.cloneNode(copy);
        }
        copy.mRight = conditionsTreeNode;
        return copy;
    }

    private ConditionsTreeNode cloneNode(ConditionsTreeNode parent) {
        ConditionsTreeNode conditionsTreeNode = null;
        ConditionsTreeNode copy = new ConditionsTreeNode(parent, this.mValue);
        copy.mCondition = this.mCondition.clone();
        copy.mLeftMPTTMarker = this.mLeftMPTTMarker;
        copy.mRightMPTTMarker = this.mRightMPTTMarker;
        copy.mLeft = this.mLeft == null ? null : this.mLeft.cloneNode(copy);
        if (this.mRight != null) {
            conditionsTreeNode = this.mRight.cloneNode(copy);
        }
        copy.mRight = conditionsTreeNode;
        return copy;
    }

    public ConditionsTreeNode and(ConditionsTreeNode expr) throws Exception {
        return add(expr, Operator.AND);
    }

    public ConditionsTreeNode and(SearchSpecification.SearchCondition condition) {
        try {
            return and(new ConditionsTreeNode(condition));
        } catch (Exception e) {
            return null;
        }
    }

    public ConditionsTreeNode or(ConditionsTreeNode expr) throws Exception {
        return add(expr, Operator.OR);
    }

    public ConditionsTreeNode or(SearchSpecification.SearchCondition condition) {
        try {
            return or(new ConditionsTreeNode(condition));
        } catch (Exception e) {
            return null;
        }
    }

    public void applyMPTTLabel() {
        applyMPTTLabel(1);
    }

    public SearchSpecification.SearchCondition getCondition() {
        return this.mCondition;
    }

    public Set<ConditionsTreeNode> getLeafSet() {
        return getLeafSet(new HashSet<>());
    }

    public List<ConditionsTreeNode> preorder() {
        List<ConditionsTreeNode> result = new ArrayList<>();
        Stack<ConditionsTreeNode> stack = new Stack<>();
        stack.push(this);
        while (!stack.isEmpty()) {
            ConditionsTreeNode current = (ConditionsTreeNode) stack.pop();
            if (current.mLeft != null) {
                stack.push(current.mLeft);
            }
            if (current.mRight != null) {
                stack.push(current.mRight);
            }
            result.add(current);
        }
        return result;
    }

    private ConditionsTreeNode add(ConditionsTreeNode node, Operator op) throws Exception {
        if (node.mParent != null) {
            throw new Exception("Can only add new expressions from root node down.");
        }
        ConditionsTreeNode tmpNode = new ConditionsTreeNode(this.mParent, op);
        tmpNode.mLeft = this;
        tmpNode.mRight = node;
        if (this.mParent != null) {
            this.mParent.updateChild(this, tmpNode);
        }
        this.mParent = tmpNode;
        node.mParent = tmpNode;
        return tmpNode;
    }

    private void updateChild(ConditionsTreeNode oldChild, ConditionsTreeNode newChild) {
        if (this.mLeft == oldChild) {
            this.mLeft = newChild;
        } else if (this.mRight == oldChild) {
            this.mRight = newChild;
        }
    }

    private Set<ConditionsTreeNode> getLeafSet(Set<ConditionsTreeNode> leafSet) {
        if (this.mLeft == null && this.mRight == null) {
            leafSet.add(this);
        } else {
            if (this.mLeft != null) {
                this.mLeft.getLeafSet(leafSet);
            }
            if (this.mRight != null) {
                this.mRight.getLeafSet(leafSet);
            }
        }
        return leafSet;
    }

    private int applyMPTTLabel(int label) {
        this.mLeftMPTTMarker = label;
        if (this.mLeft != null) {
            label = this.mLeft.applyMPTTLabel(label + 1);
        }
        if (this.mRight != null) {
            label = this.mRight.applyMPTTLabel(label + 1);
        }
        int label2 = label + 1;
        this.mRightMPTTMarker = label2;
        return label2;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mValue.ordinal());
        dest.writeParcelable(this.mCondition, flags);
        dest.writeParcelable(this.mLeft, flags);
        dest.writeParcelable(this.mRight, flags);
    }

    private ConditionsTreeNode(Parcel in) {
        this.mValue = Operator.values()[in.readInt()];
        this.mCondition = (SearchSpecification.SearchCondition) in.readParcelable(ConditionsTreeNode.class.getClassLoader());
        this.mLeft = (ConditionsTreeNode) in.readParcelable(ConditionsTreeNode.class.getClassLoader());
        this.mRight = (ConditionsTreeNode) in.readParcelable(ConditionsTreeNode.class.getClassLoader());
        this.mParent = null;
        if (this.mLeft != null) {
            this.mLeft.mParent = this;
        }
        if (this.mRight != null) {
            this.mRight.mParent = this;
        }
    }
}
