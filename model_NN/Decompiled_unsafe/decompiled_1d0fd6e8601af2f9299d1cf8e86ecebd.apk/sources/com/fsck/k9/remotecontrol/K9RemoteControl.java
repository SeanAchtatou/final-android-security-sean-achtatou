package com.fsck.k9.remotecontrol;

import android.content.Context;
import android.content.Intent;
import exts.whats.Constants;

public class K9RemoteControl {
    public static final String K9_ACCOUNT_DESCRIPTIONS = "yahoo.mail.app.K9RemoteControl.accountDescriptions";
    public static final String K9_ACCOUNT_UUID = "yahoo.mail.app.K9RemoteControl.accountUuid";
    public static final String K9_ACCOUNT_UUIDS = "yahoo.mail.app.K9RemoteControl.accountUuids";
    public static final String K9_ALL_ACCOUNTS = "yahoo.mail.app.K9RemoteControl.allAccounts";
    public static final String K9_BACKGROUND_OPERATIONS = "yahoo.mail.app.K9RemoteControl.backgroundOperations";
    public static final String K9_BACKGROUND_OPERATIONS_ALWAYS = "ALWAYS";
    public static final String K9_BACKGROUND_OPERATIONS_NEVER = "NEVER";
    public static final String K9_BACKGROUND_OPERATIONS_WHEN_CHECKED_AUTO_SYNC = "WHEN_CHECKED_AUTO_SYNC";
    public static final String K9_DISABLED = "false";
    public static final String K9_ENABLED = "true";
    public static final String K9_FOLDERS_ALL = "ALL";
    public static final String K9_FOLDERS_FIRST_AND_SECOND_CLASS = "FIRST_AND_SECOND_CLASS";
    public static final String K9_FOLDERS_FIRST_CLASS = "FIRST_CLASS";
    public static final String K9_FOLDERS_NONE = "NONE";
    public static final String K9_FOLDERS_NOT_SECOND_CLASS = "NOT_SECOND_CLASS";
    public static final String K9_NOTIFICATION_ENABLED = "yahoo.mail.app.K9RemoteControl.notificationEnabled";
    public static final String K9_POLL_CLASSES = "yahoo.mail.app.K9RemoteControl.pollClasses";
    public static final String[] K9_POLL_FREQUENCIES = {"-1", Constants.INSTALL_ID, "5", "10", "15", "30", "60", "120", "180", "360", "720", "1440"};
    public static final String K9_POLL_FREQUENCY = "yahoo.mail.app.K9RemoteControl.pollFrequency";
    public static final String K9_PUSH_CLASSES = "yahoo.mail.app.K9RemoteControl.pushClasses";
    public static final String K9_REMOTE_CONTROL_PERMISSION = "yahoo.mail.app.permission.REMOTE_CONTROL";
    public static final String K9_REQUEST_ACCOUNTS = "yahoo.mail.app.K9RemoteControl.requestAccounts";
    public static final String K9_RING_ENABLED = "yahoo.mail.app.K9RemoteControl.ringEnabled";
    public static final String K9_SET = "yahoo.mail.app.K9RemoteControl.set";
    public static final String K9_THEME = "yahoo.mail.app.K9RemoteControl.theme";
    public static final String K9_THEME_DARK = "DARK";
    public static final String K9_THEME_LIGHT = "LIGHT";
    public static final String K9_VIBRATE_ENABLED = "yahoo.mail.app.K9RemoteControl.vibrateEnabled";
    protected static final String LOG_TAG = "K9RemoteControl";

    public static void set(Context context, Intent broadcastIntent) {
        broadcastIntent.setAction(K9_SET);
        context.sendBroadcast(broadcastIntent, "yahoo.mail.app.permission.REMOTE_CONTROL");
    }

    public static void fetchAccounts(Context context, K9AccountReceptor receptor) {
        Intent accountFetchIntent = new Intent();
        accountFetchIntent.setAction(K9_REQUEST_ACCOUNTS);
        context.sendOrderedBroadcast(accountFetchIntent, "yahoo.mail.app.permission.REMOTE_CONTROL", new AccountReceiver(receptor), null, -1, null, null);
    }
}
