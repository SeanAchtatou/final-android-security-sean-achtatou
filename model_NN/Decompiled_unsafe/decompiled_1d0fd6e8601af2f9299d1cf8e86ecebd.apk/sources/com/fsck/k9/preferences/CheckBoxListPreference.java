package com.fsck.k9.preferences;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.preference.DialogPreference;
import android.util.AttributeSet;

public class CheckBoxListPreference extends DialogPreference {
    private boolean[] mCheckedItems;
    private CharSequence[] mItems;
    /* access modifiers changed from: private */
    public boolean[] mPendingItems;

    public CheckBoxListPreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public CheckBoxListPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialogBuilder(AlertDialog.Builder builder) {
        this.mPendingItems = new boolean[this.mItems.length];
        System.arraycopy(this.mCheckedItems, 0, this.mPendingItems, 0, this.mCheckedItems.length);
        builder.setMultiChoiceItems(this.mItems, this.mPendingItems, new DialogInterface.OnMultiChoiceClickListener() {
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                CheckBoxListPreference.this.mPendingItems[which] = isChecked;
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDialogClosed(boolean positiveResult) {
        if (positiveResult) {
            System.arraycopy(this.mPendingItems, 0, this.mCheckedItems, 0, this.mPendingItems.length);
        }
        this.mPendingItems = null;
    }

    public void setItems(CharSequence[] items) {
        this.mItems = items;
    }

    public void setCheckedItems(boolean[] items) {
        this.mCheckedItems = items;
    }

    public boolean[] getCheckedItems() {
        return this.mCheckedItems;
    }
}
