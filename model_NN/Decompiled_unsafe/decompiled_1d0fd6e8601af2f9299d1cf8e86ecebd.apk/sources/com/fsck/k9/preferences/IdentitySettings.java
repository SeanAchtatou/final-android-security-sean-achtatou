package com.fsck.k9.preferences;

import com.fsck.k9.EmailAddressValidator;
import com.fsck.k9.K9;
import com.fsck.k9.preferences.Settings;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import org.openintents.openpgp.util.OpenPgpApi;
import yahoo.mail.app.R;

public class IdentitySettings {
    public static final Map<String, TreeMap<Integer, Settings.SettingsDescription>> SETTINGS;
    public static final Map<Integer, Settings.SettingsUpgrader> UPGRADERS = Collections.unmodifiableMap(new HashMap<>());

    static {
        Map<String, TreeMap<Integer, Settings.SettingsDescription>> s = new LinkedHashMap<>();
        s.put(OpenPgpApi.RESULT_SIGNATURE, Settings.versions(new Settings.V(1, new SignatureSetting())));
        s.put("signatureUse", Settings.versions(new Settings.V(1, new Settings.BooleanSetting(true))));
        s.put("replyTo", Settings.versions(new Settings.V(1, new OptionalEmailAddressSetting())));
        SETTINGS = Collections.unmodifiableMap(s);
    }

    public static Map<String, Object> validate(int version, Map<String, String> importedSettings, boolean useDefaultValues) {
        return Settings.validate(version, SETTINGS, importedSettings, useDefaultValues);
    }

    public static Set<String> upgrade(int version, Map<String, Object> validatedSettings) {
        return Settings.upgrade(version, UPGRADERS, SETTINGS, validatedSettings);
    }

    public static Map<String, String> convert(Map<String, Object> settings) {
        return Settings.convert(settings, SETTINGS);
    }

    public static Map<String, String> getIdentitySettings(Storage storage, String uuid, int identityIndex) {
        Map<String, String> result = new HashMap<>();
        String prefix = uuid + ".";
        String suffix = "." + Integer.toString(identityIndex);
        for (String key : SETTINGS.keySet()) {
            String value = storage.getString(prefix + key + suffix, null);
            if (value != null) {
                result.put(key, value);
            }
        }
        return result;
    }

    public static boolean isEmailAddressValid(String email) {
        return new EmailAddressValidator().isValidAddressOnly(email);
    }

    public static class SignatureSetting extends Settings.SettingsDescription {
        public SignatureSetting() {
            super(null);
        }

        public Object getDefaultValue() {
            return K9.app.getResources().getString(R.string.default_signature2);
        }

        public Object fromString(String value) throws Settings.InvalidSettingValueException {
            return value;
        }
    }

    public static class OptionalEmailAddressSetting extends Settings.SettingsDescription {
        private EmailAddressValidator mValidator = new EmailAddressValidator();

        public OptionalEmailAddressSetting() {
            super(null);
        }

        public Object fromString(String value) throws Settings.InvalidSettingValueException {
            if (value == null || this.mValidator.isValidAddressOnly(value)) {
                return value;
            }
            throw new Settings.InvalidSettingValueException();
        }

        public String toString(Object value) {
            if (value != null) {
                return value.toString();
            }
            return null;
        }

        public String toPrettyString(Object value) {
            return value == null ? "" : value.toString();
        }

        public Object fromPrettyString(String value) throws Settings.InvalidSettingValueException {
            if ("".equals(value)) {
                return null;
            }
            return fromString(value);
        }
    }
}
